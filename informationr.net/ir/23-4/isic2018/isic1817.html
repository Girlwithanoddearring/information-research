<!doctype html>  
 <html lang="en">  
 <head>  
 <title>Information literacy at workplace: the organizational leadership perspective</title>  
<meta charset="utf-8" />
 <link href="../../IRstyle4.css" rel="stylesheet" media="screen" title="serif" />  

 <!--Enter appropriate data in the content fields-->  
 <meta name="dcterms.title" content="Information literacy at workplace: the organizational leadership perspective" />  
 <meta name="author" content="Farhan Ahmad, Gunilla Widén" />  
 <meta name="dcterms.subject" content="The aim of this paper is to outline and discuss the significance of organizational leadership in fostering employees’ information literacy at the workplace." />  
 <meta name="description" content="The aim of this paper is to outline and discuss the significance of organizational leadership in fostering employees’ information literacy at the workplace. The conceptual discussion is based on the organizational leadership and information literacy research. It is argued that organizational leadership behaviour characterized by task delegation, intellectual stimulation and individualized employee support will enhance employees’ information literacy. Organizational leadership is an architect of workplace environment including the information environment. As organizational leadership defines polices, shapes culture and determines goals, its role in employees’ information literacy development and practices is quite important. Therefore, future research on information literacy in workplace contexts should pay close attention to organizational leadership norms and behaviours." />  
 <meta name="keywords" content="information literacy, workplace information literacy, organizational leadership," />
 
 
 <!--leave the following to be completed by the Editor-->  
 <meta name="robots" content="all" />  
 <meta name="dcterms.publisher" content="University of Borås" />
 <meta name="dcterms.type" content="text" />  
 <meta name="dcterms.identifier" content="ISSN-1368-1613" />  
 <meta name="dcterms.identifier" content="http://www.informationr.net
/ir/23-4/isic2018/isic1817.html" />  
 <meta name="dcterms.IsPartOf" content="http://www.informationr.net
/ir/23-4/infres234.html" />  
 <meta name="dcterms.format" content="text/html" />  <meta name="dc.language" content="en" />  
 <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />  
 <meta  name="dcterms.issued" content="2018-04-15" /> 
  <meta name="geo.placename" content="global" />  

</head> 
  
 <body> 
 
<header>
 <img height="45" alt="header" src="http://www.informationr.net/ir//mini_logo2.gif"  width="336" /><br />
 <span style="font-size: medium; font-variant: small-caps; font-weight: bold;">published quarterly by the university of bor&aring;s, sweden<br /><br />
Vol. 23  no. 4, December, 2018</span>
<br /><br />
<div class="button">
 <ul>
 <li><a href="isic2018.html">Contents</a> |  </li>
 <li><a href="http://www.informationr.net/ir//iraindex.html">Author index</a> |  </li>
 <li><a href="../../irsindex.html">Subject index</a> |  </li>
 <li><a href="../../search.html">Search</a> |  </li>
 <li><a href="../../index.html">Home</a> </li>
 </ul>
 </div>
 <hr />
  Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 1.
 <hr />
 </header>
 <article>
 <h1>Information literacy at workplace: the organizational leadership perspective</h1>  <br />  
 
 <h2 class="author"><a href="isic1817.html#author">Farhan Ahmad</a> and <a href="isic1817.html#author">Gunilla Wid&eacute;n</a>
</h2>  

 <br /> 
 
 <blockquote>
 <strong>Introduction</strong>.  The aim of this paper is to outline and discuss the significance of organizational leadership in fostering employees’ information literacy at the workplace.<br />
<strong>Method</strong>.  The conceptual discussion is based on the organizational leadership and information literacy research.<br />
<strong>Findings</strong>. It is argued that organizational leadership behaviour characterized by task delegation, intellectual stimulation and individualized employee support will enhance employees’ information literacy.<br />
<strong>Conclusion</strong>. Organizational leadership is an architect of workplace environment including the information environment. As organizational leadership defines polices, shapes culture and determines goals, its role in employees’ information literacy development and practices is quite important. Therefore, future research on information literacy in workplace contexts should pay close attention to organizational leadership norms and behaviours.
 </blockquote>    
 
 <br /> 

 <section>
 
 <h2>Introduction</h2> 

<p>Information literacy in the workplace is &lsquo;<em>learning, experienced as task-focused information need and its fulfilment through effective information engagement&rsquo;</em> (<a href="isic1817.html#for17">Forster, 2017</a>, p.15). Information literacy enhances reflective thought and awareness of potential information sources and stakeholders in an information environment (<a href="isic1817.html#add13">Addison and Meyers, 2013</a>; <a href="isic1817.html#che17">Cheuk, 2017</a>) or in Lloyd&rsquo;s (<a href="isic1817.html#llo10">2010</a>) words, information landscape. Such awareness is critical to function effectively in a workplace. As today&rsquo;s workplaces are digital, global and competitive, employees&rsquo; capability to navigate the information environment is important for knowledge creation and organizational success (<a href="isic1817.html#ahm17">Ahmad, 2017</a>, <a href="isic1817.html#cra09">Crawford &amp; Irving, 2009</a>). Previous research has shown that information literacy in the workplace context is an evolving and changing experience (e.g. <a href="isic1817.html#for17">Forster, 2017</a>; <a href="isic1817.html#llo08">Lloyd and Williamson, 2008</a>; <a href="isic1817.html#whi14">Whitworth, 2014</a>). In different environments, information literacy is experienced differently underlining the importance of context such as works tasks, social aspects and learning experiences (<a href="isic1817.html#huv12">Huvila, 2012</a>; <a href="isic1817.html#wid18">Wid&eacute;n and Karim, 2018</a>). The information landscape or environment is governed by conditions, such as what information practices are acceptable and how information is utilized and transformed into learning and knowledge. As organizational leadership defines polices (<a href="isic1817.html#fos15">Foss, 2015</a>), codifies culture (<a href="isic1817.html#sch10">Schein, 2010</a>) and determines goals (<a href="isic1817.html#ber04">Berson and Avolio, 2004</a>), its role in employees&rsquo; information literacy development and practices become quite important. Nevertheless, previous research lacks any major effort investigating workplace information literacy from an organizational leadership perspective. The objective of this paper is to explore how organizational leadership behaviour can influence the development of employees&rsquo; information literacy.</p>
 </section>
 <section>
 <h2>Workplace information literacy</h2>

<p>In her seminal work, Bruce (<a href="isic1817.html#bru99">1999</a>) presented different faces of information literacy, which emphasized the capacity to utilize information to engage with professional responsibilities in diverse ways rather than specific skills. Bruce (<a href="isic1817.html#bru99">1999</a>) also proposed information literacy as a characteristic of a learning organization, and therefore, underlined the importance of organisational support to develop employees&rsquo; information literacy. In line with the Bruce&rsquo;s work, Lloyd&rsquo;s recent research has also conceptualized information literacy more than an individual&rsquo;s information handling skills. According to Lloyd and Williamson (<a href="isic1817.html#llo08">2008</a>), information literacy is experienced differently in work settings. Information skills learned during school or university education may not be transferable to the work context (<a href="isic1817.html#for17">Forster, 2017</a>). The performance of work activities requires employees to engage with information in diverse ways, which means the employees&rsquo; capability to understand the setting and its commonly accepted practices must be developed and nourished (<a href="isic1817.html#llo17">Lloyd, 2017</a>). According to Somerville and Bruce (<a href="isic1817.html#som17">2017</a>), information literacy is a component of an organization&rsquo;s informed learning system, which allows individuals to use information to learn within the workplace ecosystem. Such a learning system can work only if employees are capable of knowing their information environment, social practices and goals (<a href="isic1817.html#lup08">Lupton, 2008</a>). As the nature of work and boundaries are constantly changing, efficient performance of tasks requires the capability to learn and adapt to new information environments (<a href="isic1817.html#for17">Forster, 2017</a>; <a href="isic1817.html#vei07">Veinot, 2007</a>). Abdi (<a href="isic1817.html#abd17">2017</a>) provides an example of a virtual work environment with its own information landscape different from the traditional physical workplace. In her research on web developers, she shows that the information landscape of virtual environments is wide and extend beyond traditional organizational boundaries. Consequently, employees are required to learn new rules of the game, for example pertaining to information sources, evaluation and utilization in their work. Overall, workplace information literacy has been studied in different professional groups and from different perspectives. There are two common characteristics of the major work done on workplace information literacy. First, context is important, and therefore generic information skills may not be transferred from one context to another (<a href="isic1817.html#bru11">Bruce, 2011</a>). Second, an individual can be information literate in the workplace only when (s)he develops the capability to build an understanding of an organisation&rsquo;s changing and complex information environment (<a href="isic1817.html#llo10">Lloyd, 2010</a>).</p>

<p>Previous research has expanded our understanding of information literacy in the workplace context, nevertheless, much remains to be explored. Particularly the role of organizational leadership in the development of employee information literacy has not been studied. Although the importance of context is highly valued in workplace information literacy, the forces that can govern and shape organizational information environment are not always thoroughly discussed. Organizational leadership is one such force that programs the organisational context and consequently can shape employees&rsquo; information literacy experiences.</p>
</section>
<section>
<h2>Organizational leadership and information literacy</h2>

<p>Organizational leadership is responsible for directing the activities of employees and achievement of organizational goals (<a href="isic1817.html#col08">Colbert, Kristof-Brown, Bradley and Barrick, 2008</a>). Leadership exists at different organizational levels ranging from the top management to supervisors managing the work of few individuals or small teams (<a href="isic1817.html#noh10">Nohria and Khurana, 2010</a>). As a decision-making authority, it can shape the information sharing patterns, define rule regarding information access, retrieval, privacy and ethics, and support the adoption of certain information tools, such as information systems. Nevertheless, beyond defining organizational rules and regulations, the main impact of leadership lies in its behaviour, that is, interaction with employees (<a href="isic1817.html#nor17">Northouse, 2017</a>). According to Bryman (<a href="isic1817.html#bry13">2013</a>), in the actions of leaders, certain values are subtly transmitted to individuals, which develop their understanding of work and success. In the following discussion, we identify three leadership behaviours that play critical roles in the development of employees&rsquo; information literacy.</p>
</section>
<section>
<h2>Task delegation and information literacy</h2>

<p>Task delegation means individuals have substantial responsibility and discretion in identifying and carrying out work activities (<a href="isic1817.html#yuk12">Yukl, 2012</a>). Delegating leadership leads to the provision of new tasks to employees and shows managers&rsquo; trust in employees and a tendency to distribute power (<a href="isic1817.html#hug02">Hughes, Curphy, and Ginnett, 2002</a>). According to organizational leadership research, delegation of new tasks provides learning and development opportunities (<a href="isic1817.html#kle06">Klein, Ziegert, Knight and Xiao, 2006</a>; <a href="isic1817.html#mac09">MacBeath, 2009</a>). Employees build new skills in the performance of new tasks and responsibilities. Every task has specific information requirements. According to Cheuk (<a href="isic1817.html#che17">2017</a>), in the process of getting work done, organizational employees encounter information gaps accompanied by questions, worries, uncertainties and struggles. New tasks trigger and widen such information gaps, pushing employees to find ways to fill these gaps by reaching through diverse channels and sources of information. For unstandardized and newly defined tasks, employees often need to consult with individuals beyond their established information network to acquire relevant information and advice (<a href="isic1817.html#let12">Letmathe, Schweitzer and Zielinski, 2012</a>). Evaluation of obtained comments, facts and answers will be carried out (<a href="isic1817.html#che17">Cheuk, 2017</a>). Moreover, new tasks require reflecting on previous information sharing and processing strategies, leading to anticipation of modifications necessary to perform that new task in an efficient way. Overall, the information experiences gained while engaging in new tasks develop employees&rsquo; capability to handle information in new and diverse ways. As employees continuously perform delegated tasks, they enhance their exposure to diverse information needs and requirements, consequently learning the way to learn and navigate in the organizational information landscape. Therefore, delegating leadership behaviour will have a positive impact on the development of employees&rsquo; information literacy.</p>
</section>
<section>
<h2>Intellectual stimulation and information literacy</h2>

<p>Intellectual stimulation is another behaviour of leadership that has implications for information literacy. Intellectual stimulation is the degree to which a leader questions old assumptions, traditions and beliefs (<a href="isic1817.html#raf04">Rafferty and Griffin, 2004</a>). Such leaders focus on creativity and constant improvement. Employees are encouraged to adopt out-of-the-box thinking to solve problems and generate new work processes (<a href="isic1817.html#bol11">Bolkan et al., 2011</a>). Leaders who promote intellectual stimulation among subordinates will influence employee information literacy positively. Information literacy is an evolving capability. As Lloyd (<a href="isic1817.html#llo10">2010</a>) puts forward, information literacy is not a mere passive knowledge of database and information seeking skills, instead it involves a continuous development of experience through self-assessment and reflection on theinformation landscape. Intellectual stimulation can encourage employees to better understand the information needs and limitations of the current information environment. What information sources exist and how information can be created and converted into new knowledge for better performance and efficiency require thinking and a curious mind. Intellectual stimulation drives toward knowledge creation and innovation (<a href="isic1817.html#bry03">Bryant, 2003</a>), which depends on new information, its reinterpretation and application.</p>

<p>Consequently, intellectual stimulation supported by organizational leadership is bound to enhance employees&rsquo; information literacy capabilities.</p>
</section>
<section>
<h2>Individualized support and information literacy</h2>

<p>Individualized support is the degree to which a leader acts as a mentor and responds to employees&rsquo; individual learning needs (<a href="isic1817.html#jud04">Judge &amp; Piccolo, 2004</a>; <a href="isic1817.html#sav17">Savović, 2017</a>). Employees have different competencies, skills and learning needs, which require individualized support from their seniors who can help them in dealing with their deficiencies and solving problems (<a href="isic1817.html#mar09">Marsick, 2009</a>). Individualized support behaviour is particularly of great value for employee information literacy. Leaders with individualized support behaviour can be attentive to employees&rsquo; information needs, direct them to relevant information sources and help in making sense of information while performing new tasks. In terms of developing awareness of information landscape, individualized support is highly important for novice employees or for those who enter into new roles (<a href="isic1817.html#raf04">Rafferty &amp; Griffin, 2004</a>). Previous research has shown that the leaders depicting individualized support help employees in organizational socialization by developing their understanding of the organisational environment and connecting them to organizational social information networks (<a href="isic1817.html#dej07">De Jong and Den Hartog, 2007</a>). According to Taormina (<a href="isic1817.html#tao08">2008</a>), with leadership support, employees are capable of identifying and procuring information needed to perform their tasks. They are better able to understand their task responsibilities and find relevant persons in the organization for help, for example for problem solving. In a relatively different context, Lloyd, Kennan, Thompson and Qayyum (<a href="isic1817.html#llo13">2013</a>) noted that an immigrant&rsquo;s capability to operate as information literate in the new information landscape of host country is strongly influenced by customized support available to them. As modern workplaces are complex and dynamic, the extent of leaders&rsquo; customized support behaviour will influence employees&rsquo; capability to develop information literacy and adapt to new information environments.</p>
</section>
<section>

<h2>Conclusion</h2>

<p>The main aim of this short paper was to explicitly outline the impact of organizational leadership on employees&rsquo; information literacy. In the workplace information literacy literature, organizational leadership has been mentioned occasionally as an important factor in the development of employees&rsquo; information literacy. Nevertheless, we do not find any concrete efforts in this regard. In this paper, we have explained how delegating, intellectually stimulating and supportive leadership behaviours can foster information literacy. Although such leadership behaviours are encouraged, nevertheless they are not always practiced in organizations (<a href="isic1817.html#bry13">Bryman, 2013</a>). Therefore, future investigation of workplace information literacy should pay closer attention to existing leadership norms and behaviour in the workplace. A better understanding of the relationship between information literacy and organizational leadership behaviour will allow us to comprehend how information literacy capabilities are developed and practiced in workplace contexts. The potential relationship proposed in this paper will be empirically investigated in an ongoing Academy of Finland funded research project in Finnish organizations. The findings will be reported in the form of publications and research reports during 2019-2020.</p>
</section>


<section>
<h2>Acknowledgements</h2>

<p>This research has been funded by the Academy of Finland grant 295743 for the project <em>The Impact of Information Literacy in the Digital Workplace</em>.</p>


<h2 id="author">About the authors</h2>

<p><strong>Farhan Ahmad</strong> is a researcher in Åbo Akademi University, Fänriksgatan 3 B, 20500 Åbo, Finland. He received his PhD from Åbo Akademi University and his research interests are within information and knowledge management, workplace information literacy, information behavior, organizational diversity and information networks. He can be contacted at farhan.ahmad@abo.fi<br />
<strong>Gunilla Wid&eacute;n</strong> is a Professor of Information Studies, Åbo Akademi University, Fänriksgatan 3 B, 20500 Åbo, Finland. She received her PhD from Åbo Akademi University and her research interests are within information and knowledge management as well as information behavior. She has led several research projects focusing information culture, social capital theory, and information literacy. She can be contacted at gunilla.widen@abo.fi</p>

</section>
<section class="refs">

<h2>References</h2>
<ul class="refs"> 
<li id="abd17">Abdi, E. (2017). Virtuality at work: an enabler of professional information In M. Forster (Ed.), <em>Information literacy in the workplace </em>(pp. 57- 66). London: Facet Publishing</li>
<li id="add13">Addison, C. &amp; Meyers, (2013). <a href="http://www.informationr.net
/ir/18-3/colis/paperC27.html">Perspectives on information literacy: a framework for conceptual understanding</a>. <em>Information Research</em>, <em>18</em>(3), paper C27. Retrieved from http://www.informationr.net
/ir/18-3/colis/paperC27.html</li>
<li id="ahm17">Ahmad, (2017). Knowledge sharing in a non-native language context: challenges and strategies. <em>Journal of Information Science</em>, <em>24</em>(2), 248-264.</li>
<li id="ber04">Berson, &amp; Avolio, B.J. (2004). Transformational leadership and the dissemination of organizational goals: a case study of a telecommunication firm. <em>The Leadership Quarterly</em>, <em>15</em>(5), 625-646.</li>
<li id="bol11">Bolkan,, Goodboy, A.K. &amp; Griffin, D.J. (2011). Teacher leadership and intellectual stimulation: improving students' approaches to studying through intrinsic motivation. <em>Communication Research Reports</em>, <em>28</em>(4), 337-346.</li>
<li id="bru11">Bruce, (2011). Information literacy programs and research: an international review.<em> The Australian Library Journal, 60</em>(4), 326-333.</li>
<li id="bru99">Bruce,S. (1999). Workplace experiences of information literacy. <em>International Journal of Information Management</em>, <em>19</em>(1), 33-47.</li>
<li id="bry03">Bryant,E. (2003). The role of transformational and transactional leadership in creating, sharing and exploiting organizational knowledge. <em>Journal of Leadership &amp; Organizational Studies</em>, <em>9</em>(4), 32-44.</li>
<li id="bry13">Bryman, (Ed.). (2013). <em>Leadership and organizations</em>. London: Routledge.</li>
<li id="che17">Cheuk, (2017). The hidden value of information literacy in the workplace context: how to unlock and create value<em>.</em> In M. Forster (Ed.), <em>Information literacy in the workplace </em>(pp. 131-148). London: Facet Publishing.</li>
<li id="col08">Colbert,E., Kristof-Brown, A.L., Bradley, B.H. &amp; Barrick, M.R. (2008). CEO transformational leadership: the role of goal importance congruence in top management teams. <em>Academy of Management Journal</em>, <em>51</em>(1), 81-96.</li>
<li id="cra09">Crawford, &amp; Irving, C. (2009). Information literacy in the workplace: a qualitative exploratory study. <em>Journal of Librarianship and Information Science</em>, <em>41</em>(1), 29- 38.</li>
<li id="dej07">De Jong, P. &amp; Den Hartog, D.N. (2007). How leaders influence employees' innovative behaviour. <em>European Journal of Innovation Management</em>, <em>10</em>(1), 41- 64.</li>
<li id="for17">Forster, (2017). How is information literacy experienced in the workplace<em>.</em> In M. Forster (Ed.), <em>Information literacy in the workplace </em>(pp. 11-28). London: Facet Publishing.</li>
<li id="fos15">Foss,J. (2015). <em>Organizational leadership: new studies in strategy, innovation, and entrepreneurship</em>. Dj&oslash;f/Jurist-og &Oslash;konomforbundet.</li>
<li id="hug02">Hughes,L., Curphy, G J. &amp; Ginnett, R.C. (2002<em>).</em><em> Leadership: enhancing the lessons of experience </em>(4. ed.). Boston, MA: Irwin.</li>
<li id="huv12">Huvila, I. (2012). <em>Information Services and Digital Literacy: In search of the boundaries of knowing</em>. Oxford: Chandos Publishing.</li>
<li id="jud04">Judge, &amp; Piccolo, R. (2004). Transformational and transactional leadership: a meta- analytic test of their relative validity. <em>Journal of Applied Psychology</em>, <em>89</em>(5), 755- 768</li>
<li id="kle06">Klein,J., Ziegert, J.C., Knight, A.P. &amp; Xiao, Y. (2006). Dynamic delegation: shared, hierarchical, and deindividualized leadership in extreme action teams. <em>Administrative Science Quarterly</em>, <em>51</em>(4), 590-621.</li>
<li id="let12">Letmathe,, Schweitzer, M. &amp; Zielinski, M. (2012). How to learn new tasks: shop floor performance effects of knowledge transfer and performance feedback. <em>Journal of Operations Management</em>, <em>30</em>(3), 221-236.</li>
<li id="llo10">Lloyd, (2010). <em>Information literacy landscapes: information literacy in education, workplace and everyday contexts</em>. Oxford: Chandos Publishing.</li>
<li id="llo17">Lloyd, (2017). Learning within for beyond: exploring a workplace information literacy design<em>.</em> In M. Forster (Ed.), <em>Information literacy in the workplace </em>(pp. 97-112). London: Facet Publishing.</li>
<li id="llo08">Lloyd, &amp; Williamson, K. (2008). Towards an understanding of information literacy in context: implications for research. <em>Journal of Librarianship and Information Science</em>, <em>40</em>(1), 3-12.</li>
<li id="llo13">Lloyd,, Kennan, M.A., Thompson, K.M. &amp; Qayyum, A. (2013). Connecting with new information landscapes: information literacy practices of refugees. <em>Journal of Documentation</em>, <em>69</em>(1), 121-144.</li>
<li id="lup08">Lupton, (2008). <em>Information literacy and learning</em>. Adelaide, Australia: Auslib Press.</li>
<li id="mac09">MacBeath, (2009). Distributed leadership: paradigms, policy, and paradox. In <em>Distributed leadership according to the evidence </em>(pp. 59-76). London: Routledge.</li>
<li id="mar09">Marsick, J. (2009). Toward a unifying framework to support informal learning theory, research and practice. <em>Journal of Workplace Learning</em>, 21(4), 265-275.</li>
<li id="noh10">Nohria, &amp; Khurana, R. (Eds.). (2010). <em>Handbook of leadership theory and practice</em>. Boston, MA: Harvard Business Press.</li>
<li id="nor17">Northouse,G. (2017). <em>Introduction to leadership: concepts and practice. </em>Los Angeles, CA: Sage Publications.</li>
<li id="raf04">Rafferty,E. &amp; Griffin, M.A. (2004). Dimensions of transformational leadership: conceptual and empirical extensions. <em>The Leadership Quarterly</em>, <em>15</em>(3), 329-354.</li>
<li id="sav17">Savović, (2017). The impact of dimensions of transformational leadership on post- acquisition performance of acquired company<em>. Ekonomski Horizonti</em>, <em>19</em>(2), 95- 108.</li>
<li id="sch10">Schein,H. (2010). <em>Organizational culture and leadership </em>(Vol. 2). San Francisco, CA: John Wiley &amp; Sons.</li>
<li id="som17">Somerville &amp; Bruce, B. (2017). From transaction to transformation: organizational learning and knowledge creation experience within informed systems. In M. Forster (Ed.), <em>Information Literacy in the Workplace</em> (pp. 41-56). London: Facet Publishing.</li>
<li id="tao08">Taormina,J. (2008). Interrelating leadership behaviors, organizational socialization, and organizational culture. <em>Leadership &amp; Organization Development Journal</em>, <em>29</em>(1), 85-102.</li>
<li id="vei07">Veinot,C. (2007). &ldquo;The eyes of the power company&rdquo;: workplace information practices of a vault inspector. <em>The Library Quarterly</em>, <em>77</em>(2), 157-179.</li>
<li id="whi14">Whitworth, (2014). <em>Radical information literacy: reclaiming the political heart of the IL movement</em>. San Diego, CA: Elsevier.</li>
<li id="wid18">Wid&eacute;n , Karim M. (2018) Role of information culture in workplace information literacy: a literature review. In S. Kurbanoğlu, J. Boustany, S. &Scaron;piranec, E. Grassian, D. Mizrachi &amp; L. Roy (Eds.), <em>Information literacy in the workplace</em> (pp. 21-29). London: Facet Publishing.</li>
<li id="yuk12">Yukl, G. (2012). <em>Leadership in organizations.</em> Westford, MA: Pearson.</li>
</ul>

</section>
<section>
<hr />
 <h4 style="text-align:center;">How to cite this paper</h4>
   <div class="citing"> Ahmad, F. &amp; Wid&eacute;n, G. (2018). Information literacy at workplace: the organizational leadership perspective In <em>Proceedings of ISIC, The Information Behaviour Conference, Krakow, Poland, 9-11 October: Part 1. Information Research, 23</em>(4), paper isic1817. Retrieved from http://www.informationr.net/ir/23-4/isic2018/isic1817.html (Archived by WebCite® at http://www.webcitation.org/74FAovLgA)</div>  

</section>

</article><br />
<section>
 
 <table class="footer" style="border-spacing:10px;">  
 <tr>  
 <td colspan="3" style="text-align:center; background-color: #5E96FD; color: white; font-family: verdana; font-size: small; font-weight: bold;">Find other papers on this subject</td></tr>  
 <tr><td style="text-align:center; vertical-align:top;"><form method="get" action="http://scholar.google.com/scholar" target="_blank">
 <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"> <input type="hidden" name="q" value="(&quot;information seeking&quot; or &quot;information searching&quot; or &quot;information behaviour&quot;)" /><br />  
<input type="submit" name="sa" value="Scholar Search"  style="font-size: small; font-family: Verdana; font-weight: bold;" /><input type="hidden" name="num" value="100" />  </td>  </tr></table></form></td>  
 <td style="vertical-align:top; text-align:center;">  <!-- Search Google --><form method="get" action="http://www.google.com/custom" target="_blank">
 <table class="footer">
 <tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="(&quot;information seeking&quot; or &quot;information searching&quot; or &quot;information behaviour&quot;)" /><br />  
 <input type="submit" name="sa" value="Google Search" style="font-family: Verdana; font-weight: bold; font-size: small;" /><input type="hidden" name="client" value="pub-5081678983212084" /><input type="hidden" name="forid" value="1" /><input type="hidden" name="ie" value="ISO-8859-1" /><input type="hidden" name="oe" value="ISO-8859-1" /><input type="hidden" name="cof" value="GALT:#0066CC;GL:1;DIV:#999999;VLC:336633;AH:center;BGC:FFFFFF;LGC:FF9900;LC:0066CC;LC:0066CC;T:000000;GFNT:666666;GIMP:666666;FORID:1;" /><input type="hidden" name="hl" value="en" /></td></tr>  
  </table></form></td>  
 <td style="vertical-align:top; text-align:center;"><form method="get" action="http://www.bing.com" target="_blank">
 <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="(&quot;information seeking&quot; or &quot;information searching&quot; or &quot;information behaviour&quot;)" /> <br /><input type="submit" name="sa" value="Bing"  style="font-size: small; font-family: Verdana; font-weight: bold;" /> <input type="hidden" name="num" value="100" /></td></tr>  
 </table></form></td></tr>  
 </table> 
 
 <div style="text-align:center;">Check for citations, <a href="http://scholar.google.co.uk/scholar?hl=en&amp;q=http://www.informationr.net
/ir/23-4/isic2018/isic1817.html&amp;btnG=Search&amp;as_sdt=2000">using Google Scholar</a></div>
 <br />
 <!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_inline_share_toolbox" style="text-align:center;"></div>
<hr />
</section>

<table class="footer" style="padding:10px;"><tr><td style="text-align:center; vertical-align:top;">
<br />
<div>  <a href="https://www.digits.net" target="_blank">
    <img src="https://counter.digits.net/?counter={e5965006-a8d8-b9c4-cd16-a7a60428d943}&template=simple" 
     alt="Hit Counter by Digits" />
  </a>
</div>
 </td> 
<td class="footer" style="text-align:center; vertical-align:middle;">
  <div>  &copy; the author, 2018. <br />Last updated: 17 September, 2018 </div></td> 
 
 <td style="text-align:center; vertical-align:middle;">&nbsp; 
 
  </td></tr>  </table>  
 
 <footer>

<hr /> 
 <table class="footer"><tr><td>
<div class="button"> 
 <ul style="text-align: center;">
	<li><a href="isic2018.html">Contents</a> | </li>
	<li><a href="http://www.informationr.net/ir//iraindex.html">Author index</a> | </li>
	<li><a href="../../irsindex.html">Subject index</a> | </li>
	<li><a href="../../search.html">Search</a> | </li>
	<li><a href="../../index.html">Home</a></li>
</ul> 
</div></td></tr></table>
 <hr />
</footer>
 <script src="http://www.google-analytics.com/urchin.js">  </script>  <script>  _uacct =
 "UA-672528-1"; urchinTracker(); 
 </script>  
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5046158704890f2e"></script>  
</body>  
 </html> 



