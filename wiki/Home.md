# Information Research Migration Project Wiki

---

## Content

* [Clean up and convert html-documents](clean-and-convert.md)
* [Register Meta Data in XML](xml-meta-data.md)
* [XML Mall (Swedish)](XML-Mall.md)

---
In 2018-2019 we will migrate Informationr.net into a cms. The CMS of choice is Open Journal System. This Wiki contains information on how to import articles from html through xml to open journal system.

## About Information Research Journal
Information Research, is an open access, international, peer-reviewed, scholarly journal, dedicated to making accessible the results of research across a wide range of information-related disciplines. It is published by the [University of Borås](http://www.hb.se/en/)], Sweden, with the financial support of an [NOP-HS Scientific Journal Grant](https://www.nos-hs.org/)]. It is edited by Professor T.D. Wilson, and is hosted, and given technical support, by Lund University Libraries, Sweden.

## Wiki features

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax. The [MarkDownDemo tutorial](https://bitbucket.org/tutorials/markdowndemo) shows how various elements are rendered. The [Bitbucket documentation](https://confluence.atlassian.com/x/FA4zDQ) has more information about using a wiki.

The wiki itself is a [git repository](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository), which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

How to clone this wiki:

```js
$ git clone git@bitbucket.org:christerjohansson/information-research.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.