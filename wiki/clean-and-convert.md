# First process - Cleanup & Convert

---

This page also have a instructional video at [HB Play](https://play.hb.se/media/t/0_e6had5wk) (right click, open in new tab)

---

* [First process - Cleanup & Convert](#first-process---cleanup--convert)
* [The process](#the-process)
* [Quality Control](#quality-control)
* [Tools we use](#tools-we-use)
* [List of tools](#list-of-tools)
* [Cleaning up documents](#cleaning-up-documents)
* [Conversion process](#conversion-process)
* [Trello & the KanBan Board](#trello--the-kanban-board)
* [Meta data](#meta-data)
* [Note on quality control](#note-on-quality-control)
* [Step one - Checking code](#step-one-checking-code)
* [Step two - Convert HTML to Markdown](#step-two-convert-html-to-markdown)
* [Step three - Convert Markdown to HTML5](#step-three-convert-markdown-to-html5)
* [Step four - Move the CSS](#step-four-move-the-css)
* [Step five - Meta data, files and quality check](#step-five-meta-data-files-and-quality-check)

---

In this process we focus on cleaning up the raw HTML documents from unwanted HTML elements tha may remain from the earlier clean up process. Unwanted elements are things that do not belong to the actual article, such as statistic counters, navigation menus and "decorative code" of old standard.

Anything that is not related to a paper, or an article is to be deleted from the raw code, leaving only valid structure in HTML/Markdown for the information itself.

#### The process

The actual process explained in simple format:

**1.** Clean up
**2.** Preserve Meta Data in HTML
**3.** Convert to Markdown (Browserling)
**4.** Quality Control (through all steps)
**5.** Convert to HTML 5 (VS Extension: Markown PDF)

#### Quality Control

Throughout all of the steps in this process, it is important that we maintain all data intact and that quality of documents are of good standard. Here are a few things we want you to check thorugh the conversion process,

- Tables - Make sure they aren´t breaking or are displayed wrong.
- Images - Make sure links references are intact to where document is stored.
- References - Check that all references in the document are intact and presented in a good visual manner. (compare to original)
- CSS - Check that overall styles work well.

In the process of quality controll you will have manually edit some of the code by hand, some you have to remove, some you have to manually update to current HTML5 standard.

## Tools we use

All data is version controlled by Git, and hosted at Bitbucket in a public repository. It is therefore good if you use Git locally in your workspace to manage and track changes to content in this project.

All planning and communication is done through our Slack channel, and we use Trello KanBan board to track the progress in these two processes.

The list below contains all links you require to work with this process. Please ask to be invited to our Slack channel, Bitbucket repository and our KanBan board at Trello.

### List of tools

- [Git](http://git-scm.com) - version control system
- [Slack](https://slack.com) - communications tool
- [Microsoft Visual Code](https://code.visualstudio.com) - Text editor
- [VS Code Extension: Markdown PDF](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf) - Extension to save Markdown as HTML along with CSS
- [Markdown Preview Enahnced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced) - Preview tool for Markdown.
- [Trello KanBan](https://trello.com/b/Pw94hUVD/information-research) - Planning tool
- [Pandoc](https://pandoc.org) - Powerful converison tool
- [Browserling Conversion Service](https://www.browserling.com/tools/html-to-markdown) - Simplified conversion tool

## Cleaning up documents

Before we convert any documents into Markdown, we need to ensure quality of code and that no unwanted elements remain in code. As we stated earlier in this document, all elements that is not related to the actual article/paper is essentially unwanted. In order to better understand this procedure, I suggest that you have a look into the early issues of IR, and check their source code to see what we mean by content focused elements only.

All HTML documents that make up a paper or issue is essentially stripped down to bare HTML for structure, as the CMS we later will import this to, will handle most of the CSS etc. We will however apply some standardized CSS through our conversion process in order to "prettify" the articles a bit.

## Conversion process

The actual conversion process is fast and easy to perform, all you have to do is to keep an eye on the quality of data, make sure that tables and data are displayed correct in markdown and later HTML. This is important because some papers have data presented in HTML tables of various sorts, and it is important that they are preserved and presented in a correct way in both formats.

The conversion between formats is done in 3 steps between 3 tools, once you have a CSS for the first HTML document in a issue, you can utilize Pandoc to convert documents faster.

The reason we convert between formats back and forth is simply because it makes the document better and more readable. We use Browserling, due to the fact that they have a JavaScript-based conversion tool that generates a high quality Markdown for us.

We can then use this Markdown to generate high quality HTML5 along with a custom CSS to make our papers look better and be more readable, we then use Pandoc to generate the HTML5-documents faster when we have a set CSS-file for styles.

Before starting to work on a specific issue, make sure you have assigned yourself to it on the KanBan board at Trello, so that you are not overwriting or double-working on a specific issue.

#### Trello & the KanBan Board

We manage all issues and their progress through our [KanBan board at Trello](https://trello.com/b/Pw94hUVD/information-research). Make sure you pick the issue you are working with, and set up the list of tasks according to the template we have set. (See video and previous issues on the board).

#### Meta data

All original HTML documents contain Meta data in the form of ```<meta>```-elements in the HTML-document ```<head>```-section. It is important that these elements are preserved and transferred over to the final HTML-document, for each of the papers and articles.

#### Note on quality control

It is important that you check for errors when converting between formats, especially data tables and references in the papers. HTML tables are often prone to break between conversion. How to handle this? You can most of the time copy the source table over to the final document and make sure they follow html 5 standards. Pandoc is usually very good at keeping these intact.

### Step one - Checking code

Select the issue you are going to work with, open the first paper in the issue and make sure no unwanted elements are present in the code. Remove any unwanted elements, compare with earlier already converted documents.

### Step two - Convert HTML to Markdown

Open the HTML document and select all code between the `<body>` tags. Copy all of it and head over to the browser. We will now use Browserlings JavaScript based converter to convert the HTML to Markdown. Head over to https://www.browserling.com/tools/html-to-markdown and paste all code into the text box on the page. Then click convert, and then copy all Markdown code from the text box.

Go back to Visual Code and create a new folder under the issue you are working with, name this folder `Markdown`. Now, create a new file inside this folder and name the file according to the current paper or other HTML document you just converted, and paste the Markdown code in the new document. If you just converted code from a `paperxx.html`, name this new markdown file `paperxx.md`. (If you are unsure, look at previous issues structure and files)

Make sure that you have Markdown Preview Enhanced installed and activated. Now click the preview button in the upper right corner of the document, it should be a split screen icon with a magnifier.

I everything went well, you should now have all the data in Markdown. Perform a quality control check and make sure that tables, lists etc. are displayed correctly. Compare with original HTML-document. If everything looks fine, continue with the rest of the HTML-papers in the issue.

**NOTE**
`If you encounter any problems that you are unable to resolve by yourself, please make notes about it in Trello on the corresponding card. Don't get stuck and spend hours trying to resolve smaller issues, leave them for later or ask for help.`

### Step three - Convert Markdown to HTML5

If your previous step "passed with a green light" and quality control looks good, you can now convert this Markdown document to HTML5. In Visual Studio Code you should now have installed and activated a extension called `Markdown PDF`. We will now use this extension to make a prettified HTML5-document along with CSS. On Windows you press `Ctrl + Shift + P` to open the command palette. 

**NOTE**
`You can find more keyboard shortcuts for VS Code here: https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf`

In the command palette you type Markdown PDF and select `Markdown PDF:Export (html)`.
VS Code and the extension will now generate a HTML5 document with the same name in the Markdown folder.

### Step four - Move the CSS

Now that we have converted it all to HTML5, you have to perform another quality control to check that everything is in order. If all seems fine and data is presented in a good manner, proceed to select the major `<style>`-block in the `<head>`-section of the document.

Cut it out from the document and create a new file in the Markdown folder, and name it style.css. Paste all the CSS code you just cut into this CSS-file.
You then need to link the new CSS-file to this dcoument by entering this code snippet into the `<head>`-section.

_Link the CSS-file to the document_
```
<link rel="stylesheet" href="style.css">
```

**NOTE**
Sometimes there are two `<style>`-elements in the `<head>`-section. One that is the primary inline CSS, and one smaller, often containing CSS classes like code,span and div. It might look something like this code below:

```CSS
<style>
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
</style>
```

You can leave these shorter inline CSS snippets intact in the document, and just move the bigger main CSS to an external file.

### Step five - Meta data, files and quality check

When you are done converting all files to Markdown and HTML5, it is time to insert the Meta Data we are missing from the opriginal files. Open the corresponding original html document you are working with, and select all `<meta>`-elements. Copy them into the `<head>`-section of the new document. (If you are unsure, please refer to earlier already finishes issues)

Then delete the original (old) files from the issue, and move your new files into their place. Please remember to check all image links, and perform a quality control on all documents prior to moving them back into the issue folder.

Make sure all images remain where they are, and they are displayed correct in the documents you worked with before moving on to the next issue. If you are unsure, check earlier already converted and finished issues folders for reference.

Don't forget to update the KanBan board at Trello with any problems you might have encountered, and move the issue to its corresponding status in the project before moving on.

When all is done, and quality check is cleared, it is time to move on to process two, where we register meta data for import to our content management system, Open Journal System.
