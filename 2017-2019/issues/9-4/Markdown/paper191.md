# Bringing human information behaviour into information systems research: an application of systems modelling

#### [David Johnstone](mailto:david.johnstone@vuw.ac.nz), [Mary Tate](mailto:mary.tate@vuw.ac.nz),  
School of Information Management, Victoria University of Wellington, Wellington, New Zealand  

and Marcus Bonner  
Cap Gemini Ernst & Young Group  
76 Wardour St, London, W1F 0UU, UK

#### **Abstract**

> In their influential paper, Dervin and Nilan compared and contrasted the _traditional_ and _alternative_ paradigms for human information behaviour research, highlighting the inadequacies of the former and promoting the importance of the latter. In this paper, we argue that the two paradigms are not irreconcilable. We offer a research framework that allows qualitative and quantitative views of the same problem to be combined using systems models. We demonstrate how this approach can be used to reconcile the six key differences between the two paradigms as argued by Dervin and Nilan.

## Introduction

People seek out and use information constantly as part of their daily life. Information relating to work, leisure, health, money, family, and a host of other topics, is sought from a huge range of sources. Increasingly, at least some of these sources are digital in nature. The use of information in the form of digital media is becoming ubiquitous in homes, libraries, offices, and also in industries that have not traditionally been seen as primarily information-based, such as manufacturing, construction and farming. Yet digital information is not the be-all and end-all of information provisioning. Many studies in decision making, marketing and informatics have shown that people apply a rich context when seeking for information, making decisions, and forming opinions.

Dervin and Nilan ([1986](#dervin)) suggested, in their influential paper, that people _construct_ sense and meaning appropriate to their context from the systems available to them, and from other sources. They contend that a research paradigm that is human-centred, situational, constructivist, and holistic is essential for studying the process by which people interact with the information in their environment. They argue that this approach is fundamentally incompatible with traditional approaches, particularly within the field of Information Systems, when studying the way people interact with information.

Yet many fields rely on some ability to generalise about and predict the way human beings behave with information. For decades, developers, designers, and often researchers of Information Systems have assumed that users of data systems employ a standard and shared set of interpretative structures to gain meaning from the data (Boland [1987](#boland)). Any introductory systems analysis and design textbook will describe the process for working with business users to capture and document what are implicitly assumed to be shared definitions and rules about organizational data, so these data can be captured in databases and processed by computer systems.

Beyond the purview of Information Systems, many popular approaches to organizational design and management, such as business process mapping, business process re-engineering, and activity-based costing, rely on a level of predictability in the inputs and outputs produced by both individuals and business units. These inputs and outputs may include tangible goods and/or information in a variety of media. The process modeller or activity-based costing accountant also need to be able to assume that people interacting with information will do so with some degree of consistency.

This needs to be balanced with an awareness of the rich context that people apply to interpreting the information they receive. A human-centred view of information processing examines the roles humans play in translating received information into action. This will include judgement calls (do I act on it? ...ignore it? ...pass it on?) People may develop behavioural habits when interacting with information that could either support or inhibit both organizational efficiency and effectiveness.

This richness has been largely ignored by Information Systems researchers. Dervin and Nilan argued that a major reason for this neglect is that studying the way people interact with digital information requires a fundamentally different paradigm, which cannot be reconciled with the _traditional_ IS research paradigm.

In this paper, we respond to the issues raised by Dervin and Nilan. We try to answer the questions: how can we usefully study human information behaviour? What people do with information from digital and other sources once they have it? How can we use insights from Information Science research to inform disciplines like Information Systems if they use irreconcilable paradigms? How can we respond to the challenging issues raised by Dervin and Nilan and develop a research framework that is structured enough to meet the needs of information systems and organizational process designers, yet flexible enough to incorporate consideration of the rich context of human behaviour?

First, we define human information behaviour and provide some examples. Then, in the literature review section, we briefly review Information Systems literature, noting that several researchers have identified a gap in understanding between the production of information and its effective use. Then, we review literature from the Information Science discipline, which has taken a strong interest in information behaviour, with more research using a human-centred, interpretivist paradigm. We note that this has lent some useful insights into human information behaviour, but has two main limitations for informing information systems research. The first we have already identified as the use of apparently irreconcilable paradigms, the second is that Information Science researchers tend to concentrate on a particular sub-set of human information behaviour, that is, _information seeking_.

The following section provides an overview of the paradigm incompatibility problem, and how it can be resolved. This is developed in more detail with a discussion about the usefulness of systems modelling for mixed paradigm research. We propose systems modelling as a tool to develop a research framework capable of coping with these fundamentally different approaches. The phenomena (both computer-based and human information processing) lend themselves well to systems thinking. Other advantages, such as decomposition, ease of representation and analysis, and the incorporation of external system moderators, are discussed in this light.

The next section illustrates our proposed approach by developing a systems model that specifically addresses each point raised by Dervin and Nilan. Finally, we discuss implications for research and practice, followed by a conclusion.

## What is human information behaviour?

We argue that the evolution of information systems, from automation of highly structured tasks, to support for semi-structured tasks, has opened up a gap between the production of information by information systems, and the processing and use of that information. We suggest that this gap is filled by observable human behaviour.

Highly structured administrative tasks were easy targets for automation exercises, and as the processes and outputs did not fundamentally change, efficiency gains were relatively straightforward to quantify. Now organizations are filled with information (or _knowledge_) workers, who make extensive use of information retrieved from a computer-based IS to perform many tasks that contribute to organizational processes. This transformation from automating to augmenting human effort has shifted the problem domain from structured decision-making to semi-structured decision-making, from clerical work to case and managerial work, from understanding and supporting procedures to understanding and supporting how people work with information.

A great deal of effort is spent on both what information end-users should receive, or at least have access to, and in what form should this information be presented. Business needs and human-computer interaction are both taken into account. But what do people do with the information once they have it? Presumably they examine it and apply judgement and/or procedure to determine the next course of action. The many options open to them will be human, process and context specific: they may use it to update previous information, or they may store it, discard it, pass it on to someone else, combine it with other information, embed it in a report, etc.

These observable actions are collectively referred to as _human information behaviour_. They form part of the overall phenomenon of _human information processing_. The totality of human information processing also includes internal cognitive processes, which do not necessarily result in observable behaviour. In this paper we concern ourselves with the observable aspects of information behaviour.

Taylor defined information behaviour as "... the sum of activities through which information becomes useful ..." ([Taylor, 1991](#taylor): 221). Taylor's view was that an individual's information behaviour is dependent upon the type of person, the problem being resolved, the setting (of both people and problem) and what constitutes resolution of the problem.

The most significant call for more attention to be paid to information behaviour has come from the writings of Thomas Davenport. In his book _Information Ecology_, he describes information behaviour in terms of:

> "...how individuals approach and handle information. This includes searching for it, using it, modifying it, sharing it, hoarding it, even ignoring it. Consequently, when we manage information behavior, we're attempting to improve the overall effectiveness of an organization's information environment through concerted action." ([Davenport, 1997](#davenport97): 83-84)

Any patterns in information behaviour and attitudes across the organization (and groups within the organization) help define what he calls the _information culture_ (and subcultures). Davenport argued strongly for the need to manage information behaviour, whereby the information culture would better support organizational objectives:

> "Some widely publicized technologies ...can help capture and disseminate organizational knowledge, but they're of little help if the people involved aren't already predisposed to use information effectively." ([Davenport, 1997](#davenport97): 85)

He goes on to identify "three critical types of information behavior that improve a company's information environment": [p.87]

1.  **Sharing information**: "...the voluntary act of making information available to others." [p.87] Often this involves hierarchically horizontal information transfer (peer-to-peer), as opposed to the involuntary information reporting structures based on vertical information transfer.
2.  **Handling information overload**: filtering an overabundance of available information so that attention (limited as it is) can be directed to that which is most useful.
3.  **Dealing with multiple meanings**: recognising the fact that business-relevant information items may have different meanings across different functional groups within the organization.

In a preliminary study that classified types of information behaviour within an organizational context, Bonner _et al._ ([1996](#bonnercasey96)) raise some interesting questions. Is it possible to identify the relative combination of strengths of type for individual (and group) information behaviour? Would it be possible to establish combinations of information behaviour type as being more suitable to certain types of work? Can training programmes be developed to strengthen desirable information behaviour types?

Bonner _et al._ defined an individual's information behaviour in terms of their behaviour at a point in time in dealing with information within a given information environment (or context). It includes the person's behaviour with respect to:

*   determining the existence of, and locating needed information related to a specific problem or issue;
*   recognizing new potential uses to which particular information may be put;
*   combining and integrating disparate information;
*   evaluating the worth of information;
*   determining the cost of information;
*   communicating information appropriately;
*   distilling key information from a larger collection; and
*   storing and archiving information effectively.

This working definition formed the basis of an exploratory survey designed to examine what information workers did with information. Questions outlining different generic situations were used to establish likely behaviour. From the collected data, factor analysis identified seven information behaviour _types_:

*   Information Opportunity Recognition (_Pioneer_)
*   Retrieval Confidence (_Librarian_)
*   Resource Consideration (_Accountant_)
*   Awareness of Others' Information Needs (_Communicator_)
*   Extraction Effectiveness (_Prospector_)
*   Discernment (_Gourmet_)
*   Hoarding (_Squirrel_)

This study suggests that it is possible to identify useful categories of human information behaviour in an organizational context, yet relatively little attention has been paid to information behaviour by information systems researchers.

In the next section we review previous IS studies that support the need for research into human information behaviour, and suggest some reasons why IS research has been slow to address that need.

## Literature review

### Information behaviour: an information systems view

Very little attention has been paid in Information Systems (IS) research to human information behaviour. An information technology-based approach to IS research may be seen to emphasise the functional capabilities of the information technology available. It is difficult to see where human information processing, beyond interaction with the information technology-based system, naturally fits in.

Generally, an information technology-based IS covers those parts of the business processes where information handling can be effectively automated. The nature of this IS requires every information processing option to be specified and programmable. The modern IS relies on the establishment of generalised, systematic behaviour and cannot recognise the diversity of human preferences and behaviour. Nor would we necessarily expect, or even want it to; Zuboff ([1988](#)) argues this is what humans are good at.

The information technology-based IS also requires every data item used by the IS to be uniquely and precisely defined, and storable on a computer. This represents an information engineer's view: a _hard_ view of information.

_Received wisdom_ tends to set the boundary of an IS at the point at which it produces outputs for end-users. This is pervasive in traditional analysis and design literature, in which a key task is identifying the system scope. This is defined as bound by _interfaces_ to the external world through which the system receives inputs and produces outputs.

In this view, the usefulness of these outputs in creating meaning and information for the person receiving them are not of concern to the IS development team, once the requirements for the input or output have been agreed. The transmission of output is viewed as an end in itself. This appears to be consistent with what Davenport ([1994](#davenport94)) refers to as _IT-Centred Information Management_ (ITCIM), represented in Figure 1 below.

<figure>

![Figure 1: IT-centred Information Management](../p191fig1.gif)

<figcaption>

**Figure 1: Information technology centred information management**</figcaption>

</figure>

This is not to say that human involvement with IS has been completely ignored. Some IS researchers have identified the gap in understanding of how people work with the information produced by computer systems, although we were not able to identify any studies that addressed the gap.

Kling had frequently written about the need for a better understanding of how people work before improved productivity from new information technologies can be expected (Kling [1987](#kling87), [1991](#kling91); Iacono and Kling [1987](#iacono)). He observed, "...it is common for organizations to underinvest in the support for helping people effectively integrate computerised systems into their work." (Kling [1996](#kling96): 302)

To integrate information technology-based- and human information processing more effectively, Bacon argues for information provision to take into account how end-users translate information into action. His research demonstrated how information needs to be made "...purposeful, relevant, and of optimal value". He defined the type of information designed to optimise these desirable attributes as _kinetic information_ - "...that which is oriented to or energises purposeful and relevant action in critical or key areas of the organization" ([Bacon, 1994](#bacon): 448).

A focus on information behaviour could help identify the kind of information that lends itself to 'purposeful and relevant action'. Furthermore, aspects of exhibited information behaviour could serve as measures reflecting the extent of the information worker's information awareness.

Boland noted with concern that, "The researcher must... assume that the users of data systems employ a standard and shared set of interpretive structures to gain meaning from the data". ([Boland, 1987](#boland)): 364)

As Davenport concludes:

> "Humans prefer information and knowledge over data. For 40 years we've managed data and called it information. But people prefer richer information diets - information with human context, experience, insight and elaboration." ([Davenport, 1997](#davenport97): 7)

There are implicit assumptions about the information workers who use these information technology-based support systems:

> 1\. that they know what to do with the information, once delivered; and  
> 2\. that they are aware of how their work affects business objectives, and  
> 3\. that they can prioritise and modify their information processing accordingly.

DeLone and McLean ([1992](#delone)) published a widely quoted causal model for judging IS success, based on a detailed consideration of 180 (mainly positivist) seminal IS articles. Six critical dimensions of success are proposed, including information use (the other five are: system quality, information quality, user satisfaction, individual impact, and organization impact). However, 'use' in this case refers to whether or not the information received is used, not how or how well it is used.

Bonner applied DeLone and McLean's model to a case study and found some evidence supporting an additional dimension of success: _information awareness_ - "...an individual's level of awareness for the existence, purpose and value of information and of its probable impact at the individual and organizational level" ([Bonner, 1996](#bonner96): 8). Not only is an important information behaviour component missing from DeLone and McLean's model, but it was missed because the articles reviewed did not consider information behaviour to be important in IS research.

Landauer ([1995](#landauer)), having argued how unproductive modern computer-based systems are, went on to strongly advocate _user-centred design_, designing human-computer interfaces to be both useful and usable to the end-user. His book represents a rare, thoughtful, pragmatic look at the issues and practicalities surrounding user-centred design. In particular, human information behaviour is examined—albeit on the periphery of the system—in a way that relates to design issues. This appears to be consistent with what Davenport ([1994](#davenport94)) refers to as _Human-Centred Information Management_ (HCIM), represented in Figure 2 below.

<figure>

![Figure 2: Human-Centred Information Management](../p191fig2.gif)

<figcaption>

**Figure 2: Human-centred information management**</figcaption>

</figure>

Checkland ([1981](#checkland)) developed the Soft Systems Methodology (SSM), which employs 'rich pictures' to help users define the problem and explain their needs from their own perspective, using their own images and language, and communicating this to the system designers. It is a mechanism by which organizational factors can be incorporated into system design for both IT-based and human-oriented issues, including contextual factors.

Using an SSM perspective, Winter _et al._ highlight the fact that an information system "...is one that necessarily involves two systems, a 'serving' system ...and a 'served' system of purposeful action." ([Winter _et al._, 1995](#winter): 130) While the primary purpose of the paper was to examine the role for SSM in system development methodologies, the concept of the serving/served dichotomy is useful to us. It highlights the fact that there is another system to take into account with respect to IS development.

The interesting thing about this SSM representation of the relationship between the serving and the served system is that as well as illustrating the gap between information systems and human information behaviour, it also incorporates an approach for bridging the gap using systems models.

Winter _et al._ also examine appropriate paradigms (or _metatheories_ as they call them) for modelling such systems, applying Checkland's argument for two basic paradigms in the systems movement. Their conclusion was that:

> "The served system, which represents purposeful human action in real-world organizations, is rooted in the theory of Paradigm II, better known as 'soft' systems thinking, while the serving information system, with its link to computer technology, is rooted in the theory of Paradigm I, better known as 'hard' systems thinking. More generally, [the 'served' system] is rooted in the...interpretive paradigm, while [the 'serving' system] is rooted in the...functionalist paradigm." ([Winter _et al._, 1995](#winter): 134)

These paradigmatic differences echo the dichotomy identified by Dervin and Nilan, which is discussed in detail in the next section of the paper. But this discussion by Winter _et al._ _also_ describes a systems model incorporating two sub-systems, one studied using an interpretativist paradigm and one studied using a positivist paradigm. This foreshadows the approach we present in the "Paradigm incompatibility and reconciliation" section below. Unfortunately, despite the potential of SSM as a framework to address the gap we have identified, we are not aware of any studies that have used it in this way.

### Information behaviour - an information science view

The discipline of Information Science offers us different ways of conceptualising information, but articulates many of the same issues.

An interdisciplinary paper by McCreadie and Rice established a taxonomy classifying a range of previous studies examining information seeking behaviour. In one classification, access to information is described in terms of the technology used to produce it. They note that such an IT-centred perspective leads to "...a common, but mistaken assumption that access to technology equals access to information" ([McCreadie and Rice, 1999](#mccreadie): 51).

However, McCreadie and Rice also identified a more constructivist, sense-making approach to defining information, which equates more closely with Davenport's concept of human-centred information management. This approach defines

> "...information as part of the communication process" which needs to be understood "based on observation of human behaviour in the information seeking and sense making process and on the meanings intended and interpreted by the participants." ([McCreadie and Rice, 1999](#mccreadie): 48-49)

If human information processing describes what must be done, information behaviour describes how they do it. This human-centred perspective on information sees the way people gather and handle information as an integral part of the information itself, not a separate, distinguishable activity. According to McCreadie and Rice, this alternative approach sees information behaviour not as

> "...physical or cognitive activities separate from work (that is as preparation for accomplishing their real tasks), but inherent regular activities that constitute the very nature of what people do in organizations." ([McCreadie and Rice, 1999](#mccreadie): 48)

This view underlines the importance of considering the human aspects of information behaviour within the context of an information system.

> "...understanding must be based on observation of human behaviour in the information seeking and sense making process and on the meanings intended and interpreted by the participants." ([McCreadie and Rice, 1999](#mccreadie): 49)

The value of this perspective is that it brings considerations of cognitive influences and constraints into the discourse about information behaviour. This might include domain understanding, literacy, skill-level, learning styles and the importance of the interaction between system and user.

Wilson ([1997](#wilson)) developed a _revised general model of information behaviour_, based on an integrated review of information seeking behaviour in various disciplines outside Information Science (including studies of: personality in psychology, consumer behaviour, innovation research, health communication studies, organizational decision-making, and information systems design). The model was essentially made up of two constructs, _information seeking_ and _information processing and use_, represented in a circular, iterative system. The review comprehensively expanded upon the former construct, while more or less ignoring the latter. This may be appropriate in an information service environment, such as in a library, but is significantly less useful in an organizational context. This focus on information seeking, as opposed to information use, is pervasive in the Information Science literature.

Allen, in his book _Information tasks_, brings together much of the academic literature advocating the need for user-centred approaches to information system development. A _user-centred approach_ is one where "the needs of the users play a more influential role than data or technology." ([Allen, 1996](#allen): 1). He provides a rich insight into how a user's 'knowledge structures' and 'abilities, styles and preferences' influence their information behaviour. He argues that these factors must be incorporated into the system design process.

Allen claims that systems developed using traditional approaches are "...opaque to the user. They are complex, because of built in functional complexity and because they are designed to accommodate multiple agents: authors, value-added information professionals, and (perhaps least of all) users." ([Allen, 1996](#allen): 16).

By contrast, he argues the user-centred design "...begins with the user rather than the data. User-centred design emphasizes the process by which users become informed, rather than the information things that are used in the process." ([Allen, 1996](#allen): 16)

It appears that Davenport's concerns about singularity of meaning and contextual concerns, and Landauer's strong argument for more user-centred design, have been issues for information scientists as well. Certainly both Information Systems and Information Science, as disciplines, have research interests in human information processing - albeit from different contexts (information systems versus information services). But to what degree can the interdisciplinary research be integrated?

In an oft-quoted seminal paper, Dervin and Nilan ([1986](#dervin)) saw only limited value in information systems research. The problem, they argued, was fundamental - IS academics tended to see information, information processing, and consequently information systems from an engineering perspective. IS research was based on what they called the _traditional paradigm_, "...one in which information is seen as objective... It is one that searches for trans-situational propositions about the nature and use of information systems. It does this by focusing on externally observable dimensions of behaviour and events." ([Dervin and Nilan, 1986](#dervin): 16)

By contrast, their _alternative paradigm_

> "...posits information as something constructed by human beings. It sees users as beings who are free...to create from systems and situations whatever they choose. It focuses on how people construct sense,...It focuses on understanding information use in particular situations and is concerned with what leads up to and what follows intersections with systems." ([Dervin and Nilan, 1986](#dervin): 16)

Dervin and Nilan outlined six categories where the _traditional_ and _alternative_ paradigms are most seriously contrasted in the literature.

1\. **Objective versus subjective information**: the traditional view of information reflects _absolute_ meaning; whereas the alternative view is that meaning is constructed in relation to the context. This echoes Davenport's multiplicity of information meaning.

2\. **Mechanistic, passive users versus constructivist, active users**: the former, traditional view treats users as passive recipients of necessary data from the information system, whereas the latter, alternative view sees them as "...purposive, self-controlling, sense-making beings." [p.16] Thus, the mechanistic view ignores the intervening user behaviour required to become informed, and the influence of other moderators such as culture, access and context.

3\. **Trans-situationality versus situationality**: the discovery of predictable causality across different situations is an ideal which is difficult to achieve in IS research. Yet this is often the primary objective of the traditional paradigm. The alternative paradigm accepts that a user may exhibit different information behaviour in different contexts, in spite of identical input.

4\. **Atomistic versus holistic views of experience**: the traditional paradigm focuses on the computerised aspects of an information system; the alternative paradigm views an information system holistically, accepting that information is processed both before and after the computer is involved.

5\. **External behaviour versus internal cognition**: the traditional view is that only externally measurable behaviour should be used in research, as internal cognitive states are not readily measurable. Nevertheless, Dervin and Nilan noted that there were increasing calls for incorporating cognitive psychology into research into information behaviour (the alternative spproach).

A middle ground may exist that recognition of individual cognitive processes affecting the observable behaviour, while also accepting that we would not seek to determine exactly what those cognitive processes are. This constraint could be relaxed at a later date, but at present we are concerned with the _what_, as opposed to the _why_.

6\. **Chaotic vs. systematic individuality**: the traditional paradigm seeks to establish sufficiently systematic behaviour that some generic assertions can still be made. The alternative paradigm points out that human information behaviour is necessarily individualistic, and thus generic assertions may not be particularly helpful in any given situation.

Dervin and Nilan contrasted these research approaches and found them to be effectively irreconcilable - being based on fundamentally different paradigms. So how is human information processing (incorporating information behaviour) going to be integrated with traditional IS design methods?

## Paradigm incompatibility and reconciliation

### Overview

So far, this paper has established that:

> a) there is a gap between production of information by computer systems, and purposeful use of information, that is filled by human information behaviour;  
> b) both Information Systems practitioners and academics appear to give information behaviour little attention;  
> c) Information Science academics recognise the significance of information behaviour, and often employ an interpretivist paradigm - which is appropriate for the study of human behavioural phenomena - but this research tends to focus mostly on information seeking;  
> d) the challenge for academics is to find an effective way to incorporate both positivist and interpretivist paradigms in information behaviour research, to suit both the IT-centred and human-centred components of system design respectively.

Is it possible that the two paradigms can be reconciled, and more specifically, be employed to:

*   Describe a theoretical model of human information behaviour that incorporates both IT-centred and human-centred elements that can be readily tested?
*   Be flexible enough to deal with both collective and individual perspectives, from both a high and detailed level of design?
*   Ultimately lead to improved methods of system design and implementation?

The Information Science literature appears the more flexible of the two disciplines, with a generally _softer_ view of information; where meaning is constructed by individuals according to their contextual factors. End-users are not seen simply as passive receptors of _hard_ information at the periphery of a computerised information system. Instead, they are at the centre of attention, retrieving information from a variety of sources (including computerised ones) and, based n personal and environmental factors, will translate their interpretations of the information received into actions. It does not seem likely that this process can be readily automated without human intervention at key points. This reframing of information processing elevates information behaviour to a level that invites more serious attention.

However, regardless of the merits of the HCIM perspective, organizations today still overwhelmingly use an ITCIM approach to IS design. End-users may be involved in determining the sort of information they want and how they want to receive it, but we still know little about how that information is to be effectively communicated and translated into actions that form part of the business processes. Some elements of the human-oriented task may be prescribed, but the issue of individual variation of meaning construction is generally ignored.

Thus the IS and Information Science literatures provide us with both _hard_ and _soft_ views of information. These views lead to correspondingly differing views of information systems. Both views have their merits. The IT-based component may deliver _hard_ information to the end-user, who will interpret it (rendering it _soft_) and act upon it. Both IS perspectives incorporate the same information processing tasks (to receive, store, retrieve, transform, and distribute information), but they contrast singularity with multiplicity of information meaning in an organizational environment. Structured translation of data into action (such as printing cheques) is contrasted with the semi-structured and unstructured approach (such as devising budgets and strategic planning).

The problem with understanding the value added to the delivered, _hard_ information by human involvement is that it has a _softening_ effect. The application of rich contextual judgement, creating and working with _soft_ information, places human information processing outside the aegis of the traditional IS engineers and designers.

### Systems thinking

Systems thinking is based on the idea that the whole may be more than the sum of the parts. The systems movement emerged in the 1940s, advanced considerably by the works of biologist Ludwig von Bertalanffy, founder of General Systems Theory, including the generic rules of system behaviour (von Bertalanffy [1968](#vonbertalanffy)), and Norbert Wiener, founder of _cybernetics_, focusing on system control processes (Wiener [1948](#wiener)).

Checkland provides a detailed history of the systems movement and the variety of classification systems available. In particular, he focuses on what he calls _human activity systems_, which are "...sets of human activities more or less consciously ordered in wholes as a result of some underlying purpose or mission" ([Checkland, 1981](#checkland): 111). He points out that such a system would be capable of purposeful activity if certain conditions were met. These include the need for:

*   an on-going purpose;
*   a measure of performance (to gauge how well the purpose was met);
*   a decision-making process, as part of the transformation of inputs into outputs;
*   a _system environment_, representing components external to the system that still have an impact on the system;
*   a _system boundary_, which separates the system from its environment; and
*   _subsystems_, representing a series of interconnected systems which, together, comprise the system under scrutiny.

These concepts provide the basis for the description and analysis of human activity systems. They have proven to be particularly useful in areas where the more traditional scientific approaches have failed; where the problem domain is unstructured, poorly understood, complex or based on human behaviour.

Hartman ([1988](#hartman)), in _Conceptual foundations of organization theory_, presents a carefully argued justification for the use and co-existence of multiple theories and paradigms in research on organizations and suggests this can be done within an overall systems approach.

Influential systems thinking theorist Midgley also argues for the compatibility of systems thinking with a variety of research paradigms (Midgley [, 2000](#midgley)).

Hartman notes that one of the traditions of organization theory is to conceive of an organization as:

> "...an entity that is subject to functional analysis, that is, analysing systems into their functions is one way of explaining what they do...A flowchart...expresses the functional analysis of a system's capacity; the capacity itself can be expressed as the relationship between the system's input and its output. This is a good way to explain a wide range of phenomena that can be viewed as input followed by output. But the strength of that sort of explanation is that it is perfectly compatible with further explanation of a different kind. ...At a certain point, one may say 'don't tell me what role this subsystem plays, tell me about its components and what causes them to be in the state they are in'...an organization theorist may turn from saying how subsystems and sub-functions contribute to the activity of the whole and go into detail about how people in the system do their job and why." ([Hartman, 1988](#hartman): 15)

By conceptualising both human information work and information systems as sub-systems within an organizational context, we now have a framework that allows us to explore their inter-relationships, while preserving the flexibility to use different paradigms to analyse the internal behaviour of each.

In the second part of this section, we introduce systems modelling, and illustrate its capabilities by focusing on a model of information behaviour.

### Use of system modelling

Systems thinking provides a convenient approach to studying a variety of phenomena where the system's boundary, inputs, outputs and objectives can be identified. Figure 3 represents a system model in its simplest form, where the process transforms the input into the output.

<figure>

![Figure 3: the simplest system model](../p191fig3.gif)

<figcaption>

**Figure 3: The simplest system model**</figcaption>

</figure>

A system is typically evaluated in two ways: how well the system achieves its objectives in terms of its outputs (_effectiveness_); and how few resources are required as inputs to be processed to achieve its objectives (_efficiency_). Appropriate measures are required in both cases (in organizations, effectiveness is often based on quality measures, while efficiency is normally based on financial measures).

A systems model provides a diagrammatic method for exploring the nature of a system. Almost all organizational activities could be considered to be parts of different systems and subsystems.

First, we consider the information system component of the framework. As its name would suggest, an information system would seem to be an ideal candidate for systems modelling - particularly considering its increasing complexity and cost. Many IS textbooks use a systems model to introduce the information systems concept. In a typical example, Alter defines an information system in terms of the IT-based processing of information:

> "a system that uses information technology to capture, transmit, store, retrieve, manipulate, or display information used in one or more business processes." ([Alter 1996](#alter): 714)

This can be represented as a system model (Figure 4).

<figure>

![Figure 4: Alter's definition of an information system represented as a system model](../p191fig4.gif)

<figcaption>

**Figure 4: Alter's definition of an information system represented as a system model**</figcaption>

</figure>

This system, clearly rooted in the ITCIM perspective, will be used to capture or retrieve input data, manipulate the input data in some way, and store, display or transmit the output information.

A system model offered by Schultheis and Sumner provides a more flexible, integrative view (Figure 5). Here the employment of IT, represented by the hardware and software components, is still seen as important, but no more so than other essential components. The model explicitly introduces a human element (people), an organizational element (procedures) and an information element (stored data).

<figure>

![Figure 5: Schultheis & Sumner's system model of an information system [p.40]](../p191fig5.gif)

<figcaption>

**Figure 5: Schultheis & Sumner's system model of an information system ([Schultheis, 1998: 40](#Schultheis))**</figcaption>

</figure>

Comparing the two system models can be instructive. Alter's model emphasises process, while Schultheis and Sumner's model identifies key components. Other system orientations are possible, depending on the modeller's purpose.

The nature of system processes can be formally represented as interconnected subsystems - known as decomposition - to any level of detail required. Decomposition is an essential tool for the analysis and design of systems. Some subsystem processes may require further elaboration (i.e., further decomposition), but this may not be possible (if the process details are unclear) or desirable (if only a higher level view is required). When this happens, the process is left as a _black box_, still clearly identified in the overall system, but possibly requiring further investigation to outline the process mechanisms.

## Applications of systems modelling to information behaviour research

### Response to Dervin and Nilan

To illustrate the power and flexibility of systems thinking, a system model incorporating both IT-based and human information processing is developed in direct response to the six issues raised by Dervin and Nilan ([1986](#dervin)), discussed earlier in the paper.

In fact, Dervin and Nilan were particularly critical of systems thinking, relegating this approach to a tool useful for _hard_ disciplines like engineering, incapable of coping with human-oriented issues. This assumption is understandable, given the dominant IT focus at present, and appears to be held true by other Information Science academics (e.g., Hjorland [2000](#hjorland)).

The following considers each of Dervin and Nilan's paradigmatic concerns in turn, providing the basis for an evolving system model that incorporates both IT-based and human information processing.

1\. **Chaotic versus systematic individuality**: organizations typically require some degree of systematic individuality to ensure necessary tasks are done to a minimum standard. Even so, chaotic individuality may be significant in some situations and can be addressed in a systems model by simply differentiating between the two types of information processing. Figure 6 outlines how the classic DATA -> PROCESS -> INFORMATION model of an IS can be modified to take this into account. There are clear parallels with Winter _et al._'s ([1995](#winter)) serving and served systems.

The model is arguably simplistic, given that human information processing may well include systematic tasks requiring little, if any, decision-making. However, such structured information-based tasks are often candidates for IT-based automation, and are of less interest here.

<figure>

![Figure 6: An IS model incorporating both systematic (IT-based) and chaotic (human) information processing.](../p191fig6.gif)

<figcaption>

**Figure 6: An IS model incorporating both systematic (IT-based) and chaotic (human) information processing.**</figcaption>

</figure>

2\. **Objective versus subjective information**: the simple model is further differentiated by identifying the source of different types of information (Figure 7).

<figure>

![Figure 7: The modified IS model incorporating both objective (IT-sourced) and subjective (human-sourced) information.](../p191fig7.gif)

<figcaption>

**Figure 7: The modified IS model incorporating both objective (IT-sourced) and subjective (human-sourced) information.**</figcaption>

</figure>

3\. **Mechanistic, passive users versus constructivist, active users**: Dervin and Nilan point out that users do not process information in the same mechanistic way that computers do, and that both personal and contextual moderators will affect what actually happens (Figure 8). This enables the organization to identify moderators that may improve the quality of the information, such as training and job rotation (personal) and ergonomics and computer response time (environmental).

<figure>

![Figure 8: The modified IS model incorporating the moderating factors (personal & environmental) affecting the production of subjective information.](../p191fig8.gif)

<figure>

**Figure 8: The modified IS model incorporating the moderating factors (personal & environmental) affecting the production of subjective information.**</figure>

</figure>

4\. **Atomistic versus holistic views of experience**: the IT-based component assumes information can be broken down into fundamental data units that can be stored and retrieved without alteration. However, the human perspective suggests information is richer than the atomistic view allows. System decomposition allows for models to be broken down into further levels of detail. These levels may be described atomistically (such as the data flow diagrams used in IS design) or holistically (identifying interactive components relevant to each user). Note that human information processing would have access to atomised data as well (in the form of objective information) through the IT-based information processing subsystem.

<figure>

![Figure 9: The modified IS model incorporating both atomised data and rich data as system inputs, processed by the IT-based and human components.](../p191fig9.gif)

<figcaption>

**Figure 9: The modified IS model incorporating both atomised data and rich data as system inputs, processed by the IT-based and human components.**</figcaption>

</figure>

5\. **External behaviour versus internal cognition**: there is a need to accept the relevance of cognitive factors when examining information behaviour. By identifying it as a component in the system model, cognition can be considered at a variety of levels.

For example, it could be placed within the Human Information Processing box as a subsystem transforming rich data into subjective information at either an individual information worker level, or at a higher level in an attempt to model such cognitive behaviour in broader terms. The field of Cognitive Psychology could be explored to provide models focusing on the mechanisms of such transformations.

Alternatively, the cognitive process could be recognised in the model, but left unexplored, applying it purely as a _black box_ that identifies input and output, but omits any details about the processing. For example, it could be placed amongst the Personal Moderators - thereby identifying it as an important factor, but placing it outside the system boundary. This places the emphasis on its effect on human information processing, without concern for the mechanisms leading to these effects - as they are deemed outside system control.

6\. **Trans-situationality versus situationality**: Dervin and Nilan were concerned about the assumption of predictability with respect to human information processing. While IS professionals need to be reminded of this, organizational requirements normally expect a certain degree of predictability to ensure acceptable functionality. Normally these would be expressed in terms of the objectives set for the information worker, with corresponding performance criteria used to assess how well those objectives were being met.

For example, a common form of human information processing involves making decisions based on the information (both atomised and rich) available. In this case the subjective information output would be the decision, and the quality of the decisions would need to be assessed. The criteria used to base these assessments can be seen as a feedback function (another systems concept) that assesses system effectiveness and, if necessary, adjusts the process to improve quality. Figure 10 incorporates such feedback control loops for each of the information processing methods - recognising the criteria would be different for each subsystem.

<figure>

![Figure 10: The IS model now incorporates feedback loops, which pass on performance information to quality control functions, which may lead to changes in both external process moderators and internal processes.](../p191fig10.gif)

<figcaption>

**Figure 10: The IS model now incorporates feedback loops, which pass on performance information to quality control functions, which may lead to changes in both external process moderators and internal processes.**</figcaption>

</figure>

Now we have an analytical framework that is broad enough to encompass both information systems and human information processing, yet readily open to more detailed component analysis. Even so, individual components cannot now be considered in isolation. This includes not only the input and output functions of the component subsystem, but contextual effects from the system environment as well.

The _human information processing_ component in Figure 10 can be broken down into behavioural components (even as subsystems in their own right), or may be better tackled using different classification schemes. Either way, subcomponents can be studied using either positivist or interpretativist research paradigms. A variety of both quantitative and qualitative methods can be used. Either single or multiple case enquiry is appropriate.

The model could serve as a framework for identifying and analysing empirical data about individual information workers in terms of their perspectives on information, their information behaviour, and their interaction with the IT-based system. The same framework would work equally well as a collective view of information workers, representing a summative perspective of information behaviour. As more is learned about the human-based system, this bottom-up approach may help determine which factors are, to a degree, generalisable, and which remain stubbornly unique.

Contextual factors (personal and environmental moderators) can be identified, prioritised and related to different subprocesses as appropriate. Useful and usable feedback (whether hard and objective or soft and subjective) can be identified, collected and employed to improve processes and the effects of the moderators.

For example, we can return to Davenport's three major information behaviour issues and test the framework's usefulness.

a) **Information sharing** : Davenport suggests there is insufficient voluntary, horizontal information transfer, as opposed to involuntary, vertical reporting. Where Figure 10 represents the information processing based upon the job of one information worker, improving the degree information is shared becomes a system objective. Automated sharing is normally picked up when user requirements are established. However, issues like recognition of information value, and both the ability and desire to communicate effectively will affect human information sharing. This would suggest a focus on the personal and environmental moderators of the sharing function, including say, institutional knowledge, organizational culture, and training.

b) **Information overload**: Davenport notes the lack of effective filters to deal with an overabundance of available information. Here the focus needs to be on both automated filters examining the nature of the atomised data, and human filters examining both objective information from the IT-based component and rich data from elsewhere.

c) **Multiple Meanings**: Davenport observes business information items may have different meanings across different functional groupings. Yet database design normally assumes singularity of meaning. Perhaps the framework could be applied at the functional grouping level. Differences in information meaning could be isolated, evaluated, and where appropriate, incorporated into the information processing, with particular attention paid to the human component - where multiple meanings can be accommodated.

In each case above, the quality control components in Figure 10 can be used as an aid to analysing performance and instigating measures to improve performance.

It is doubtful Dervin and Nilan had such a model (or its method of representation) in mind when outlining their _alternative_ paradigm. Nor are any claims being made that this is the _right_ framework for analysing either system design in general, or information behaviour in particular.

Rather, it highlights how simply theoretical constructs can be represented in a systems model, thereby lending it to further elaboration and analysis.

## Implications for research

The main objectives of this paper have been to:

*   Urge recognition of the validity of both the collective, positivist view of human information behaviour (recognising the dominant, ITCIM approach) and the individual, interpretivist view of information processing (arguably more appropriate for the HCIM approach).
*   Demonstrate the advantages of systems modelling, including its ability to be flexible (even at the paradigmatic level) and inclusive in terms of past research from disparate disciplines;
*   Provide examples of how mixed-paradigm system models of human information behaviour can be constructed and analysed, incorporating both traditional information systems and human information processing.

Roughly paraphrasing the argument previously presented by Hartman, we could say that systems models allow us to model the flow of information, in various media, through an organization as it passes from subsystem to subsystem. At any point, the researcher can go into detail about how people respond to the information and why. This approach allows the human information behaviour researcher to _drill down_ into the context of use to explain, from a human-centred point of view, the range of intervening variables that affect information use, and create observable information behaviour.

A framework for this integration can be provided using a systems approach, as this paper has demonstrated. A model was developed in direct response to the call for IS research to incorporate paradigms more appropriate to human phenomena.

## Implications for practice

Further research and development of this modelling approach offers the potential of a range of benefits to the IS practitioner community, including:

*   Explaining why it can be difficult to translate information into productive action, providing a framework that allows practitioners to model, predict, and ultimately improve the effectiveness of this transformation process;
*   Describing and informing systems design - especially to assist in deciding when structured information, or semi-structured information using ICT as a communication medium is most appropriate;
*   Diagnose and correct problems with the use and effectiveness of information systems;
*   Provide a modelling approach that can usefully incorporate the various media and sources utilised by knowledge workers; and
*   Provide a modelling approach for the semi-structured processes and decision-making, usually at least partly supported by the outputs of information systems, which are typical of knowledge work.

The concepts behind systems thinking are not complex. System models are not difficult to draw and modify. Generic system models can be tailored to describe very specific situations in practice. System modelling provides a method for organizing thought and untangling complexity. It allows one to reveal what is important and hide what is not.

Finally, practitioners are used to using systems approaches for design and implementation applications. Communicating academic endeavour to industry might be easier if system models were used.

## Limitations

A model can be described as "a simplified representation (or abstraction) of reality" (Turban [1995](#turban): 42). This _simplicity_ is frequently seen as a strength of modelling tools. Models and diagrams are used extensively in a wide range of fields to assist in visualising and schematising complex phenomena, ranging from object oriented software to various educational, linguistic, architectural and industrial applications ([Blackwell & Englehardt 2002](#blackwell)).

However, this approach does have its limitations. Simplification and abstraction, in the form of a system model, inherently involves a process of selection by the researcher. It is generally impossible to include every possible relevant factor in the model. Alternatively, over-simplification risks irrelevancy. The challenge is to find the right balance between _simplified_ (for easier digestion and analysis) and _representation of reality_ (for applicability and usefulness).

If the aim of the research is to conceptualise the richness of an individual's lived experience, or elucidate exceptions and alternatives, then an approach such as Dervin's sense-making methodology may be more appropriate ([Cardillo 1999](#cardillo)); ([Cheuk 1998](#cheuk)). However, systems modelling does encourage a high degree of reflection and self-awareness in the process of making selections, which may partly mitigate these disadvantages.

In a highly simplified form, ([Midgley 2000](#midgley)) suggests that at the heart of system modelling is the making of system _boundary judgements_. The process of making these boundary judgements must imply a degree of separation between the subjects of the study and the system that is the object of the study. However, Midgley argues that all research paradigms require boundary judgements of some kind. Systems thinking provides a process for negotiating these boundary judgements that is very flexible. This flexibility allows a system modelling approach to be used in conjunction with a wide range of research paradigms and theoretical approaches. System model diagrams emphasise the nature of these boundary judgements.

## Conclusion

Both Information Systems and Information Science researchers have asserted that a shift is required in the study of human information behaviour. The IS community have lamented the frequent failure of information systems to transform precise, and unique, atomic information, as stored in a computer, into kinetic information and relevant action in organizations, yet have offered only limited explanations or solutions.

One explanation of the problem sees a lack of attention to the human side of information processing. Davenport, and others, have argued for a human-centred approach to information processing, relegating the IT-based information system to simply one, albeit an important one, of the ways users receive, process and distribute information. In particular, information behaviour was an area highlighted as requiring more attention. It was proposed that the concept of an information system should be expanded to include human, as well as IT-based, information processing.

The positivist approach to research, reflecting practitioners' concerns over the best use of expensive IT, is dominant amongst IS academics. Dervin and Nilan, in a seminal work, asserted that a paradigm shift was required in the study of information behaviour, to embrace qualitative methods with the user at the centre.

Dervin and Nilan further argued that systems thinking ignores or sidelines the user with its emphasis on IT and quantitative methods in research. They concluded that the paradigm conflict is irreconcilable.

This paper addresses these concerns, pointing out that systems thinking does not preclude qualitative and multi-disciplinary studies, but rather should be seen as a highly effective tool for enabling such studies. To illustrate, a systems model incorporating both IT-based and human-based information processing is developed.

## References

*   <a id="allen"></a>Allen, B. (1996). _Information tasks_. San Diego, CA: Academic Press.
*   <a id="alter"></a>Alter, S. (1996). _Information systems: a management perspective_. Menlo Park, CA: Benjamin Cummings.
*   <a id="bacon"></a>Bacon, J. (1994). The nature of information. In Graeme Shanks and David Arnott, (Eds.) _Proceedings of the 5th Australasian Conference on Information Systems, Melbourne, 27-29 September, 1994._ Caulfield East, Victoria, Australia: Department of Information Systems, Monash University
*   <a id="blackwell"></a>Blackwell, A. & Englehardt, Y. (2002). A meta taxonomy for diagram research. In M. Anderson, M. Bernd & P. Olivier (Eds.), _Diagrammatic representation and reasoning. (pp. 47-64)_ London: Springer-Verlag.
*   <a id="boland"></a>Boland, R. (1987). The in-formation of information systems. In R. Boland (Ed.), _Critical issues in information systems research_ (pp. 363-379). Chichester: John Wiley and Sons.
*   <a id="bonner96"></a>Bonner, M. (1996). _DeLone and McLean's model for judging information systems success - a retrospective application in manufacturing._ Wellington: Victoria University of Wellington, Information Systems Group. (Working paper)
*   <a id="bonnercasey96"></a>Bonner, M., Casey, M-E., Greenwood, J., Johnstone, D., Keane D., & Huff, S. (1998). Information behaviour - a preliminary investigation. In Medhi Khosrow-Pour (Ed.), _Proceedings of IRMA 1998, Effective Utilisation and Management of Emergent Technologies_ (pp. 68-79). Boston, MA: Idea Group Publishing
*   <a id="cardillo"></a>Cardillo, L. W. (1999). [Sense-making as theory and method for researching lived experience: an exemplar in the context of health communication and adolescent illness.](http://www.cios.org/getfile/cardillo_V9N23499) _Electronic Journal of Communication_ **9**(2-4). Retrieved 10 December 2003 from http://www.cios.org/getfile/cardillo_V9N23499.
*   <a id="checkland"></a>Checkland, P. B. (1981). _Systems thinking, systems practice_. Chichester: John Wiley and Sons.
*   <a id="cheuk"></a>Cheuk, W.-Y. (1998). An information seeking and using process model in the workplace: a constructivist approach. _Asian Libraries_ **7**(12), 375-390.
*   <a id="davenport94"></a>Davenport, T. (1994). Saving IT's soul: human-centred information management. _Harvard Business Review_, **72**(2), 119-131.
*   <a id="davenport97"></a>Davenport, T. (1997). _Information ecology_. New York, NY: Oxford University Press.
*   <a id="delone"></a>DeLone, W. & McLean, E. (1992). Information systems success: the quest for the dependent variable. _Information Systems Research_ **3**(1): 60-95.
*   <a id="dervin"></a>Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual review of Information Science and Technology_, **21**, 3-33.
*   <a id="hartman"></a>Hartman, E. (1988). _Conceptual Foundations of Organization Theory_. Cambridge, MA: Ballinger.
*   <a id="hjorland"></a>Hjorland, B. (2000). "Library and information science: practice, theory and philosophical basis." _Information Processing and Management_ **36**(3), 501-531.
*   <a id="iacono"></a>Iacono, S. & Kling, R. (1987). Changing office technologies and the transformation of clerical jobs. In R. Kraut (Ed.), _Technology and the transformation of white collar work_. Hillsdale, NJ: Lawrence Erlbaum.
*   <a id="kling87"></a>Kling, R. (1987). Defining the boundaries of computing across complex organizations. In R. Boland & R. Hirschheim (Eds.), _Critical issues in information systems research_ (pp. 307-362). New York: John Wiley & Sons..
*   <a id="kling91"></a>Kling, R. (1991). "Cooperation, coordination and control in computer-supported work." _Communications of the ACM_ **34**(12), 83-88.
*   <a id="kling96"></a>Kling, R. (1996). Computerization at work. In R. Kling (Ed.), _Computerization and controversy: value conflicts and social choices_ (2nd ed.), (pp. 278-308). San Diego, CA: Academic Press.
*   <a id="landauer"></a>Landauer, T. (1995). _The trouble with computers: usefulness, usability, and productivity_. Cambridge, MA: MIT Press.
*   <a id="mccreadie"></a>McCreadie, M. and Rice, R. (1999). Trends in analysing access to information. Part I: cross-disciplinary conceptualisations of access. _Information Processing and Management_ **35** (1), 45-76.
*   <a id="midgley"></a>Midgley, G. (2000). _Systemic intervention: philosophy, methodology, and practice._ New York, NY: Klewer Academic/Plenum.
*   <a id="schultheis"></a>Schultheis, R. and Sumner, M. (1998). _Management information systems: the manager's view_. Boston, MA: Irwin/McGraw-Hill.
*   <a id="taylor"></a>Taylor, R. (1991). Information use environments. _Progress in Communication Science_ **10**, 217-251.
*   <a id="turban"></a>Turban, E. (1995) _Decision support and expert systems: management support systems_, Englewood Cliffs, NJ: Prentice-Hall.
*   <a id="vonbertalanffy"></a>von Bertalanffy, L. (1968). _General systems theory_. Harmondsworth, Middlesex: Penguin.
*   <a id="wiener"></a>Wiener, N. (1948). _Cybernetics_. New York, NY: John Wiley and Sons.
*   <a id="wilson"></a>Wilson, T.D. (1997). Information behaviour: an interdisciplinary perspective. _Information Processing and Management_, **33**(4), 551-572.
*   <a id="winter"></a>Winter, M. C., Brown, D. H. & Checkland, P. (1995). A role for soft systems methodology in information systems development. _European Journal of Information Systems_ **4**(3) 130-142.
*   <a id="zuboff"></a>Zuboff, S. (1988). _In the age of the smart machine: the future of work and power_. New York, NY: Basic Books.