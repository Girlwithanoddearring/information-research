#### Information Research, Vol. 9 No. 4, July, 2004

# Special issue: Research at the School of Information Management, Victoria University of Wellington, New Zealand

# Issue Editorial

[Kia ora, Tēnā Koutou Katoa](#TeReo)

It seems a long time (but in fact was only 2002) since I was on research and study leave in the UK and, while wandering the corridors of the Department of Information Studies at the University of Sheffield, stopped for a chat with Tom Wilson. Tom suggested that an issue of _Information Research_ could be devoted to research from VUW. I accepted the challenge, thinking that the target date of 2004 was a long way away, and that it was time that my colleagues' antipodean research labours should be presented in a truly cyberspatial journal.

The [School of Information Management](http://www.sim.vuw.ac.nz/) was formed in 1998 by combining three existing groups at Victoria University of Wellington: the Information Systems group in the Faculty of Commerce and Administration, the Department of Library and Information Studies, and the Department of Communications. On the teaching side, the merger has given rise to a new degree, the Master of Information Management, and has strengthened the delivery of existing degrees. On the research side, the proximity of the discipline groups has facilitated research links between the disciplines. This issue of _Information Research_ illustrates some of the areas in which the School carries out research.

The main discipline groups of the school are:

*   Communications
*   Electronic commerce
*   Information systems
*   Library and information management

As with other institutions in the information research area, the development of the Web and its impact on the use of the information is a strong research interest in the School.

Research into Website evaluation is a strong theme in the School, for example in Rowena Cullen's work on Government Websites (e.g., Cullen and Houghton, [2000](#Cullen)). Matthew Conway and Dan Dorner's paper is developed from a research project carried out as part of Conway's Master of Library and Information Studies degree, and is a good example of how the research focus of this degree flows on to practice in the workplace. While this research into political party Websites falls broadly into the communications field, it arises from Conway's work in the New Zealand Parliamentary Library. Conway and Dorner examine the effectiveness of New Zealand party political Websites. It identified the functions carried out by the Websites, and aims to generate a methodology of quantitative evaluation of political Websites that will allow for longitudinal comparisons. Clearly this has implications for other areas of research into the effectiveness of Websites. Interestingly the research identified that most parties made poor use of their sites. Unsurprisingly, the major parties had more effective sites than the minor parties, although a minority party, the Greens, had the most effective Website.

Although Beverley Hope and Zhiru Li are in the Information Systems discipline, their paper on the nature of online newspapers follows a similar research strand to Conway and Dorner, assessing the effectiveness of Websites. In this case they studied the reactions of users to a sample of online newspapers in order to develop quality factors that can be used in evaluating Websites. The study distinguished between hygiene factors, essential requirements whose absence causes dissatisfaction, and motivating factors, desirable elements that add value and increase user satisfaction. Hygiene factors identified were Timeliness, Content attractiveness, Content coverage, Usefulness, and Navigation; while motivators were Writing style, Layout, Archives, Services, Interactivity, and Multimedia presentation. The study also examined differences across ethnicity (the sample included people with Chinese as a first language), sex and age. For example, Chinese readers classified content attractiveness as a hygiene factor, while this factor was borderline for English readers.

An emerging area in the study of Websites is Webmetrics, sometimes called Cybermetrics. Alastair Smith has investigated a number of aspects of the "_sitation_" or linking between Websites. In his paper, Smith reports on an ongoing study into the nature of links made between Websites. A classification of links is developed which categorises three aspects: the nature of the originating and target pages, and the reasons for linking. This is contrasted with the literature on motivation for citing in traditional print literature. To explore the use of the classification, links to a small sample of research oriented Websites were examined. The nature of these links is largely different from those made to print research publications, but about 20% of the links could be regarded as serving similar functions to citations in traditional print publications.

Another aspect of the commonality between the different disciplines in the School is shown in the paper by David Johnstone, Mary Tate and Marcus Bonner. Although they are examining a traditional Information Systems area, systems modeling; they respond to the work of Dervin and Nilan in information behaviour which has been widely used in Library and Information Management and in Communications. In doing so, they argue that a "traditional" systems modeling approach is not necessarily incompatible with an "alternative" human behaviour approach, and suggest ways in which the two models can be reconciled.

The impact of the digital revolution on organisations is an important theme in the School. Pak Yoong and David Pauleen have an interest in digital communication to facilitate both face-to-face meetings and distributed meetings. Facilitating meetings of this nature is a learned skill, but the relatively small number of practitioners poses problems for traditional research methodologies in studying e-facilitation. Pak and Yoong have combined two approaches, grounded theory and action learning, into a new approach: grounded action learning. They apply grounded action learning to two case studies, and demonstrate how the grounded action learning approach can provide insights into the application of new technologies.

The experience of putting this issue together was valuable in helping the members of the group the better to appreciate the common ground between the different research areas of the School. Thanks to the reviewers who gave valuable feedback on the papers, and to Tom for his patience and sage advice in bringing the issue to fruition; and for the opportunity to acquaint a wider audience with the work of the School of Information Management.

## Reference

<a id="Cullen"></a>Cullen, R., & Houghton, C. (2000). Democracy online: an assessment of New Zealand government web sites. _Government Information Quarterly_, 17(3), 243-267.

## Note

<a id="TeReo"></a>While this editorial and issue is aimed at an international audience, I couldn't resist the opportunity to use a greeting in Te Reo, the language of the indigenous Maori of New Zealand/Aotearoa, to open the editorial.