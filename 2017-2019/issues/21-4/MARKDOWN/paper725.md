#### vol. 21 no. 4, December, 2016

# Publishers' responses to the e-book phenomenon: survey results from three 'small language' markets

## [T.D. Wilson](#author) and [Elena Maceviciute](#author).

#### Abstract

**Introduction**. This paper reports on a study of publishers' attitudes towards e-books in the context of the global situation of e-book publishing. Comparative data are drawn from a replication of a survey carried out in Sweden, in Lithuania and in Croatia.  
**Method**. A self-completed questionnaire survey was undertaken, offering respondents the choice of a printed questionnaire or a response through an online SurveyMonkey alternative.  
**Analysis**. Quantitative analysis was performed using the descriptive statistics capability of SurveyMonkey.  
**Results**. The three country survey reveals a number of similar responses from publishers on several key issues, i.e., self-publishing, the future role of bookshops, and relationships with public libraries. The results also reveal that publishers have a certain ambivalence on these issues.  
**Conclusion**. In 'small language' markets, the take-up of e-books represents a much smaller proportion of total sales than in the English language market. Responses to questions on publishers' relationships with authors, booksellers and libraries show a high degree of unanimity of opinion.

## Introduction

In 1971 Michael Hart typed into a computer at the University of Illinois the text of the American Declaration of independence and sent it to everyone on the network, and this became the founding text of Project Gutenberg, the first digital library ([Hart, 1992](#har92)).

For as long as digital texts were restricted to the screens of dumb terminals or, later, desk-top computers, their impact on the world of publishing was limited. After all, Project Gutenberg only digitised texts that were out of copyright, and publishers had nothing to fear. However, the e-book, when combined with more portable devices such as laptop computers and, subsequently, tablet computers and smart phones, emerged as a disruptive technology ([Christensen, 1997](#chr97)) for the publishing industry.

We can describe the e-book and its associated technologies as disruptive because of the uncertainty and ambivalence it appears to have introduced into the publishing industry ([Wilson, 2014](#wil14)). On the one hand it offers an additional revenue stream, since both electronic and printed versions of a book can be sold, but, on the other hand, it offers opportunities for file sharing and piracy that could not exist to the same extent with the printed version.

The e-book also has what Winston ([1998](#win98)) describes as _radical potential_, which, in this case, means the ability to transform the publishing business completely, in various ways. For example, through a massive increase in self-publishing, or through educational institutions producing their own electronic textbooks, or through some companies demonstrating success in publishing _only_ e-books. Winston also suggests that existing businesses will attempt to _suppress_ the radical potential of an innovation, and we see signs of this in publishers' attitudes (particularly in the UK and the USA) towards the lending of e-books by public libraries and using digital rights management technology or pursuing the implementation of restrictive legislation.

Major publishers, such as Harper Collins, McGraw-Hill, Simon & Schuster, and Warner Books participated in the creation, testing and launching of the first e-readers, such as Softbook and Rocket eBook launched in 1998\. They produced the first books to be used by these new devices, but, with the lack of success of those prototypes, the interest of publishers appears to have declined.

Since these first attempts the commercial e-book has established itself and begun to change the book trade around the world, but not equally or similarly in different countries. The publishers' reactions to the new situation is far from uniform as they are experiencing high levels of uncertainty: they see the potential of the new market, but the direction of development is not entirely clear.

The aim of this paper is to compare the results of three surveys of publishers' opinions about the opportunities and threats coming from e-books and the changed relationships of publishers with different actors within the book sector. The survey was carried out in 2014 in three European countries with book markets limited by a small number of local language speakers. Sweden has a population of 9,593,000; Lithuania only 2,900,000 and Croatia one of 4,253,000 (all 2013 data from the [World Bank, 2014](#wor14)).

The research questions we seek to answer are: 1) what are the similarities and differences in how the publishers in different book markets, limited by small numbers of local language speakers, perceive the drivers of and barriers to e-book publishing? 2) How do the publishers in small book markets see their relations with traditional actors in the book sector in relation to the changes caused by the e-book?

<section>

## Related research

Our review of related research is presented in the following sections. First, we review the global situation of e-book publishing, as far as this is possible, given the lack of data for many countries and the lack of any standardisation in data collection and presentation for those countries where data exist.

Next, we examine the publishers' perception of the e-book phenomenon, first generally, and then examining existing surveys of e-book production, sales and distribution, perceptions of the greatest challenges faced by publishers, and, finally, views on the future of the e-book.



### The global situation in e-book publishing

Most work on the impact of e-books has been done in the USA, where a relatively high proportion of the population has used e-books, but other countries have experienced even greater penetration than the USA. For example, a study by Bowker ([2012](#bow12)) reported on the extent to which e-books were known of and used in ten countries. As may be seen from Figure 1, India emerged as the country with the greatest proportion of people having used e-books, at 24%, compared with the UK and Australia at 21% and the USA at 20%.

Two countries, however, had below 10% usage: Japan and France. These latter figures are rather surprising, given that French, if not a world language on the scale of English or Spanish, at least has speakers of the language around the world, and the position of Japan appears oddly at variance with the known adoption of advanced technologies in that country.

<figure class="centre">![Figure 1: Awareness and use of e-books in ten countries](p725fig1.%20png)

<figcaption>  
Figure 1: Awareness and use of e-books in ten countries ([Bowker, 2012](#bow12))</figcaption>

</figure>

From the publishers' perspective, awareness and use are not the critical issues; rather, overall market growth and market share are more important. Obtaining comparable international data on these measures, however, is rather problematical. Probably the best source is the _Global E-book report_ ([Wischenbart, _et al._, 2014](#wis14)) but, unfortunately, this does not present data in a standardised manner for each country covered. No doubt this is because no international standards for the presentation of such data exist and the e-book market in many countries is still at a very low state of development. Table 1 shows the data on market share of e-books in 2013, drawn from the _Global E-book report_. The data should be treated cautiously, however, since in some cases the figures are directly from the tables in the report and in other cases they are derived from information in the accompanying text. The authors of the report give their own note of caution, commenting that they have had to:

> struggle with scarce data and, even worse, inconsistent measures and definitions of parameters, so that any comparison resembles at first a puzzle with not only many pieces missing, but also pieces cut out without bothering at all how they might fit together, ([Wischenbart, _et al._, 2014](#wis14)).

The situation has not improved with the publication of the 2016 edition of the _Report_ and, as a result, the table below includes data from both editions. Some countries have been added in 2016, but the figure for the USA is from the 2014 edition, as no clear indication is given of the present situation. However, Nielsen is reported as stating that e-books' share of book sales was 24% in 2015 ([Hoffelder, 2016](#hof16)). The range given for Croatia is rather wide; the _Report_ states: '_Ebooks are estimated to represent less than 10% of the Croatian book market_' ([Wischenbart, _et al._, 2014](#wis14), p. 81), however, the data reported by Velagic and Pehar (2013) for Croatia, suggest that e-books constitute approximately 10% of annual production.

<table class="center" style="width:40%;"><caption>  
Table 1: E-book share of the book market in various countries (Source: derived from [Wischenbart, _et al._, 2016](#wis16) and [2014*](#wis14)).</caption>

<tbody>

<tr>

<th style="width:50%;">Country</th>

<th>E-book percentage of book market</th>

</tr>

<tr>

<td>UK</td>

<td style="text-align:center;">21.0</td>

</tr>

<tr>

<td>USA</td>

<td style="text-align:center;">21.0*</td>

</tr>

<tr>

<td>Croatia</td>

<td style="text-align:center;"><10.0</td>

</tr>

<tr>

<td>Netherlands</td>

<td style="text-align:center;">5.7</td>

</tr>

<tr>

<td>France</td>

<td style="text-align:center;">5.4</td>

</tr>

<tr>

<td>Spain</td>

<td style="text-align:center;">5.0</td>

</tr>

<tr>

<td>Germany</td>

<td style="text-align:center;">4.7</td>

</tr>

<tr>

<td>Brazil</td>

<td style="text-align:center;">4.3</td>

</tr>

<tr>

<td>Italy</td>

<td style="text-align:center;">3.4</td>

</tr>

<tr>

<td>Poland</td>

<td style="text-align:center;">2.4</td>

</tr>

<tr>

<td>Estonia</td>

<td style="text-align:center;">2 - 7</td>

</tr>

<tr>

<td>Slovakia</td>

<td style="text-align:center;">1 - 2</td>

</tr>

<tr>

<td>Czech republic</td>

<td style="text-align:center;">1.7</td>

</tr>

<tr>

<td>Russia</td>

<td style="text-align:center;">>1.0*</td>

</tr>

<tr>

<td>Sweden</td>

<td style="text-align:center;">>1.0</td>

</tr>

<tr>

<td>Denmark</td>

<td style="text-align:center;">1.0*</td>

</tr>

<tr>

<td>Lithuania</td>

<td style="text-align:center;">1.0*</td>

</tr>

<tr>

<td>Romania</td>

<td style="text-align:center;">1.0*</td>

</tr>

<tr>

<td>Slovenia</td>

<td style="text-align:center;">1.0</td>

</tr>

<tr>

<td>India</td>

<td style="text-align:center;"><1.0</td>

</tr>

<tr>

<td>Serbia</td>

<td style="text-align:center;"><1.0</td>

</tr>

</tbody>

</table>

There is a curious disparity here between the figure for India in Table 1 and the proportion of the population that has used e-books in Figure 1\. The explanation is, perhaps, that 'use' will include the use of free e-book resources from sites such as Project Gutenberg and, of course, it may also include the use of pirated materials. There is also the fact that English is spoken by 24% of the population ([Vyas, 2014](#vya14)), which amounts to more than 304 million people, meaning that the English language e-book market is strong in India.

The conclusion we can draw from these figures is that, while the impact of the e-book is world wide, it is everywhere different. Particularly noticeable is the fact that 'small language' countries in Europe have low market share, suggesting that the overall scale of the book market does not encourage publishers to experiment with a new technology, the impact of which they are unable, at this point in its development, to forecast.



### The publishers' position on the publication of e-books

Papers on e-books and their effect on publishing are numerous, but few are based on research studies, though more of these are beginning to appear (e.g., [Cordón-García, Linder, Gómez-Díaz and Alonso-Arévalo, 2014](#cor14); [Ueda, 2014](#ued14)). One should remember that publishing studies, as a discipline, follows the humanities tradition of publishing monographs. Thus, Thompson ([2011](#tho11)) presents the opinions of publishers on e-books in _Merchants of Culture._ One of his respondents explains that Sony reader and Kindle represented a '_watershed_' and within ten years the revenues of publishers from e-books could be as high as 50% ([Thompson 2011](#tho11), p. 317). However, most of the other interviewees did not risk predicting anything or regarded e-books as a technology that degraded the reading experience. '_Trying to predict the pattern of book sales over the next 3-5 years is like trying to predict the weather in six months' time_' ([Thompson, 2011](#tho11), p. 319). Phillips ([2014](#phi14)) produced a book based on literature studies and interviews with researchers and participants in the book trade. His work examines the changes brought by e-book to the English language publishing world.

We have also found studies exploring publishers' attitudes. Buschow, Noelle and Schneider ([2014](#bus14)) explored the factors influencing the decisions taken by German book publishing companies on whether or not to enter the e-book market, using a survey study of managers. They compared various limiting factors in publishing houses and in the publishing environment and their impact on the e-book adoption process. Rogers's model of the diffusion of innovation allowed researchers to compare the relevance of barriers to adoption. On the individual level, the attitudes of managers towards e-books, the support of their managers for e-books and the professionalisation of management processes predicted the adoption of e-books. Organizations as adopters were characterised by communication with, and participation in, industry networks. The unexplained finding was that the innovativeness of a company could be a strong predictor of non-adoption of e-books ([Buschow _et al._, 2014](#bus14), p. 74).

Rimm ([2014](#rim14)) compared the phenomenon of vertical integration in publishing in two European countries Germany and Sweden. Both are outside the Anglo-American market, but Germany has fixed book prices, while Sweden does not. The paper is based on a broad overview of the two markets and interviews with several actors in the German and Swedish book trades. Concentration of media ownership results in large international media groups consolidating at a fast pace, with more and more links in the value chain of books being taken over by them. One of the '_foremost examples is Amazon, which is established in Germany and fast expanding in Europe, but not yet present in Sweden_' ([Rimm, 2014](#rim14), p. 77).

Amazon's impact on the respective book markets as well as strategies for independent publishers are discussed in the paper. The author finds that Swedish respondents, especially small publishers, view the vertical integration of the industry as a threat, while German respondents are not worried. However, both groups regard Amazon as an actor with which it is impossible to compete. There is also a common tendency for small publishers to identify with quality literature as a survival strategy in both countries ([Rimm, 2014](#rim14), p. 91). Here the author does not explore e-book publishing or technology impact. Mussinelli ([2010](#mus10)) makes a comparison of technological and economic developments on French, German, Spanish and Italian e-book markets, but does not explore publishers' opinions and attitudes.

There are few surveys of publishers' opinions on e-books published in languages accessible to the authors of this paper. We have not found any such comparative survey, except some attempts to confirm the results in one survey by the results of another (e.g., [Collinson, 2015](#col15); [BookNetCanada, 2014](#boo14)). Nevertheless, some surveys published in 2009-2015 include questions requiring expression of opinions, rather than providing factual information. They present data on e-book production, sales and distribution, challenges and advantages found in five such surveys done by Aptara, KPMG, BookNetCanada, the Association of German Publishers and Booksellers, and a publisher, Collinson. What follows is our interpretation and re-presentation of the data.



#### Surveys of e-book production

The Publishers Weekly survey conducted by Aptara between 2009 and 2012 captured the process of the growth of the e-book market in the USA, or at least of 83% of publishers distributing their product in the USA, Canada and the UK ([Aptara, 2012](#apt12), p. 5). The survey of 2011 reported the responses from 1,350 publishers ([Aptara, 2011](#apt11), p. 1). According to the report, the number of respondents was growing 100% each year. The survey recorded, in three years, an increase of 30% of publishers (four out of five respondents) producing e-books, but 86% were combining e-book and printed book production ([Aptara, 2012](#apt12), p. 1, 3). Fifty-eight per cent of publishers were producing fixed layout books, while 34% were using non-fixed layout. Twenty-six per cent of respondents at the time had converted 26-75% of their backlist to e-books, while 18% had more than 75% of their backlist as e-books and 40% between 1% and 25%. Nine per cent had not started the conversion. Over half of respondents were outsourcing (or intending to outsource) their e-book production for the lack of internal resources and competence ([Aptara, 2012](#apt12)).

KPMG conducted a very exhaustive surveys of the opinions of publishers in France from November 2013 to January 2014 ([KPMG, 2014](#kpm14)), between February and May, 2015 ([KPMG, 2014](#kpm15)), and in March to June, 2016 ([KPMG, 2016](#kpm14)). The surveys used slightly different questionnaires on each occasion, in some cases probing a topic more deeply on one study than on other occasions, making year-by-year comparisons impossible. The latest survey was sent to 127 publishers and answered by 86 (79 independent publisher and 7 groups) representing all publishing sectors. E-books were published by 62.5% of the respondents in 2014 and by 62% in 2015; and 57% of those not yet publishing e-books in 2014 planned to do so within three years. By 2015, that figure had fallen to 23%. Almost 63% in 2014 outsource the production of e-books to publishing technology or multimedia companies, and more than 31% (usually, big companies) produce them in-house ([KPMG, 2014](#kpm14)).

BookNet Canada has produced an annual survey The state of digital publishing in Canada since 2013\. So far, two reports are available for the years 2013 and 2014 ([BookNet Canada, 2014](#boo14), [2015](#boo15)). Seventy publishers have participated in the survey, most of them small publishers, but 32% were medium and large publishers. E-books are produced by 93% of the respondents and many are actively digitising their titles: almost half have more than 50% of their titles available digitally and 24% of publishers have converted two thirds of their backlist titles to digital format. Two per cent of the respondents were digital-only publishers and the numbers of publishers having programmes for the production of only digital books is slightly increasing. Enhanced e-books were produced by 23% of respondents and 35% are exploring the possibility of such production, while 25% have developed mobile apps. Despite the growing digital publication, the number of staff dedicated to e-book production in publishing houses was diminishing (from 44% in 2013 to 39% in 2014) and 52% of respondents outsourced the production of e-books. Fixed layout books for Apple iPad, Kindle Fire, Kobo and Nook (listed in the order of popularity) were produced by 36% of respondents (only trade publishers) ([BookNet Canada, 2015](#boo15)).

The Association of German Publishers and Booksellers has so far conducted four surveys of the e-book situation in the German book market (the latest published in 2014 reports the data from 2013), with one of the sections reporting the situation of e-book publishers. According to this survey two thirds of German publishers had an e-book programme (an increase of 12% from 2012) with 100% of big publishers having such a programme. Nearly half of publishers were producing fiction e-books, 35% non-fiction and scholarly e-books and 15% were producing e-textbooks ([Börsenverein des Deutschen Buchhandels, 2014](#bor14)).

Simon Collinson, an Australian editor and e-book producer based in London ran an online survey of small publishers in 2015 and gathered fifty-nine responses ([Collinson, 2015](#col15)). The majority of answers came from the UK publishers (54%), but there were some from Spain, the USA and Ireland. 40% of publishers were producing their e-books externally, only 30% were doing this in-house and the rest were mixing both approaches. Fifty per cent of the respondents were satisfied or very satisfied with the level of e-book production skills and only 14% were dissatisfied or very dissatisfied. These answers were directly related to the levels of revenue derived from e-books: the greater the revenue, the more satisfied the respondent was with the level of skills. It is interesting to note that over 39% of the publishers thought that e-book production costs are low or very low and almost half of them thought that it was average. However, this perception had nothing to do with the actual expenditure related to e-books production ([Collinson, 2015](#col15)).

</section>

<section>

#### Sales and distribution

The Aptara survey established that Amazon was, at the time of the survey, the most popular sales channel, while Apple's iPad was the most popular reading device for the publishers themselves. The publishers regarded consumer demand (41%) and increasing revenue (41%) as the drivers of e-book production. ([Aptara, 2012](#apt12)). The direct price setting model was favoured by 63% of respondents, while 16% preferred the agency model and 9% subscription models. Library access to their e-book production (through Ingram, Backer & Taylor, netLibrary, ebrary and Overdrive) was allowed by 57% of publishers and not allowed by 43%. ([Aptara, 2012](#apt12)).

According to the 2014 KMPG survey ([KPMG, 2014](#kpm14)) 80% of responding French publishers set the price of an e-book at the same level as that of a paperback, by 2016 that figure had fallen to 71% ([KPMG, 2016](#kpm16)). While 77% of e-book publishers in 2014 made them available together with the main publication, the others delayed it by some weeks or until the paperback format was published. Almost one third of e-book publishers distribute them on the platform of an intermediary, 26% (24% in 2016) sell through e-book retail outlets (e.g., Amazon, Google Play, Apple, etc), but 21% (43% in 2016) sell directly to readers. E-books are promoted using all online possibilities: blogs, newsletters, social networks, etc. ([KPMG, 2014](#kpm14)). Seventy-one per cent of French producers protect their books from piracy by using DRM (43%), watermarking (35%), anti-piracy surveillance (13.5%), and streaming e-books (3%). However, almost 30% do not use any means of protection because they find it too expensive or ineffective ([KPMG, 2014](#kpm14), p. 22-23). These figures had changed slightly by the time of the 2016 study, with watermarking used by 36%, DRM 32%, surveillance 17%, and streaming 8% ([KPMG, 2016](#kpm16)).

As the BNC survey has shown, most of the respondents were making e-books available at the same time as printed books (65%), some (29%) later than printed versions and 6% were releasing them before printed books. ([BookNet Canada, 2015](#boo15)). Eighty-eight per cent of publishers were selling e-books in Canada and 52% reported increased sales. The majority of e-book producers (95%) sell them through retailers, such as Kobo, Amazon, Apple, Barnes & Noble, Google, and Sony (also named as the best revenue channel by 65%); 66% sell directly from the corporate Website (an increase from 42% in 2013); 43% use wholesalers and 19% started distributing e-books through subscription services in 2014\. The most popular are the direct pricing model (72%) and the agency model (55%) with the subscription (20%) model appearing in 2014\. Almost 70% of companies claim that they get between 1% and 10% of their revenues from e-books, while 8% state that they get more than 30%. Seventy-five per cent of the respondents sell e-books to libraries (20% do not and 5% are unsure) with 49% doing this on the same conditions as retail sales, while 25% sell for multiples of the retail price and the rest introduce a variety of limitations for use (e.g., time or loan limits). OverDrive, Ingram and Baker & Taylor are the most popular distributors to libraries, with 3M and ebrary following. ([BookNet Canada, 2015](#boo15)).

In Germany, the online booksellers dominated the distribution channels (74%), with diminishing numbers selling directly from their Websites (26% in 2010 and 14% in 2013). 85% of publishers offer their e-books cheaper than printed versions, nearly half of them more than 20% below the price of a paper book. The usage of hard DRM decreases and over two thirds of publishers were using soft DRM in 2013\. ([Börsenverein des Deutschen Buchhandels, 2014](#bor14)).

Collinson found, in his survey, that most of the respondents were distributing their e-books in the USA, UK and Australia. Thirty-six per cent of respondents were publishing non-fiction, 24% fiction, 14% academic publications, and 7% children's books. All in all, twenty-one categories of publications were named. Thirty-two per cent of publishers were selling e-books from their own website, but also distributing them to major wholesalers (Ingram, OverDrive, Gardners) and directly to retailers (Amazon, Apple, Kobo and Nook). Almost 60% of publishers thought that their books are sold with DRM, but 30% thought that this was not the case and 10% were unsure. Almost 48% of respondents believed that DRM is a deterrent against piracy, but close to 30% disagreed with this statement. At the same time almost 40% wanted to sell their books without DRM protection, while close to 28% objected to this idea ([Collinson, 2015](#col15)).



#### The greatest challenges

The greatest challenges in bringing the e-books to the market were: content format and device compatibility (34%), distribution channels (20%), quality of the converted content (20%), DRM (12%), overall cost of e-book production (9%) ([Aptara, 2012](#apt12), p. 38). All these challenges were diminishing over the four years of the surveys and the piracy issue (DRM) was finally outranked by distribution and quality concerns ([Aptara, 2012](#apt12)).

The main reasons for not publishing e-books for French publishers were: difficulty of getting digital rights, cost of production, modes of commercialisation, conversion, and determination of the selling price ([KPMG, 2014](#kpm14), p. 8). According to the publishers of e-books, the main barriers to the development of e-books are: price, lack of interoperability between formats and platforms, technical difficulties, insufficient supply, and accessibility. Those publishers who do not publish e-books see different barriers: fear of losing control of the market to Internet giants, legal difficulties, technological difficulties and price ([KPMG, 2014](#kpm14), p. 38).

</section>

<section>

#### Advantages and future predictions

The forces driving the production of e-books named in the BNC survey were: increase in sales (77%), customer demand (63%), accessibility (55%), and author expectations (42%). Testing a new production method was named only by 15% and the potential for reducing costs only by 5% ([BookNet Canada, 2015](#boo15)). According to the KMPG survey of 2014 over 25% of French publishers work with e-books as a unique format to save costs or to test the market. A majority of the publishers predict that by 2020 e-books will expand to 15-30% of the book market ([KPMG, 2014](#kpm14)), by 2016, however, publishers were rather less optimistic, 43% believed that it would be less than 10%, while 54% believed it would be 10-29% ([KPMG, 2016](#kpm16)).

In Collinson's ([2015](#col15)) survey, two thirds of the publishers expected that their e-book production will expand, the rest thought that it will stay the same, and no one predicted a decrease in production.

As one can see, all five surveys differ from one another, though all address some central issues of production and distribution. Some attitudes towards the other actors, e.g. commercial distributors and libraries or co-producers of e-books, can be derived from the data, though not always directly. The questions focusing on opinions deal with the barriers and drivers and to some extent with the content protection issues.



## A three country survey of publishers

We have explored some of the issues of e-book publishing through a survey of publishers in Sweden and, with the help of colleagues in these countries, Lithuania and Croatia. These are countries, like Sweden, with a 'small language market', but there are quite significant differences among them.

<table class="center" style="width:95%;"><caption>  
Table 2: The characteristics of the countries in the survey.</caption>

<tbody>

<tr>

<th> </th>

<th>Sweden</th>

<th>Lithuania</th>

<th>Croatia</th>

</tr>

<tr>

<td>Population (2015 forecast) Source: Wikipedia</td>

<td style="text-align:center;">9,816,666</td>

<td style="text-align:center;">2,906,666</td>

<td style="text-align:center;">4,230,000</td>

</tr>

<tr>

<td>Number of books and pamphlets published in 2014</td>

<td style="text-align:center;">12,613  
(Source: Branchstatistik 2014)</td>

<td style="text-align:center;">3,292  
(Source: LNMMB, 2015)</td>

<td style="text-align:center;">c.6000  
(Source: Nemec, n.d.)</td>

</tr>

<tr>

<td>Broadband penetration in 2012 (Source: eurostat)</td>

<td style="text-align:center;">31.7%</td>

<td style="text-align:center;">21.1%</td>

<td style="text-align:center;">20.3%  
(Source: Wikipedia)</td>

</tr>

<tr>

<td>Internet access (% of pop.) (Source: Internet World Stats)</td>

<td style="text-align:center;">70.9%</td>

<td style="text-align:center;">68.5%</td>

<td style="text-align:center;">70.%</td>

</tr>

<tr>

<td>Level of literacy (Source: Wikipedia)</td>

<td style="text-align:center;">99%</td>

<td style="text-align:center;">99.8%</td>

<td style="text-align:center;">99.3%</td>

</tr>

<tr>

<td>Students in tertiary education as proportion of population (Source: eurostat).</td>

<td style="text-align:center;">3.71%</td>

<td style="text-align:center;">6.02%</td>

<td style="text-align:center;">4.61%</td>

</tr>

</tbody>

</table>



## Method

A questionnaire was designed based in part on two case studies undertaken by Master's students at the University of Borås and, in part on the theoretical framework relating to the adoption of innovations developed by Winston ([1998](#win98)).

The questionnaire designed for the survey in Sweden was translated into Lithuanian and Croatian, by colleagues there, and administered in the same way in all three countries: publishers were sent a printed questionnaire (an English language version of which is given as the Appendix) and offered the alternative of completing an online Survey Monkey<sup style="font-size:10px;">-</sup> questionnaire. When printed questionnaires were returned, the data were input to the Survey Monkey<sup style="font-size:10px;">-</sup> version, to enable easy analysis. This mode of operation was used because the pilot survey in Sweden had a low response rate to the online questionnaire alone. The response rates for the surveys were, 55.6% (110 replies) in Sweden, 58.6% (85) in Lithuania, and 23.6% (55) in Croatia. An article based on the Lithuanian survey ([Gudinavicius, Suminas and Maceviciute, 2015](#gud15)) has already been published and Swedish survey data was used in earlier articles (e.g., [Nilsson, Maceviciute, Wilson, Bergström and Höglund,, 2015](#gud15)).



## Results

From the results, three issues are selected for comparison. First, the Swedish results showed that a small majority (51%) had published e-books, whereas in both Croatia and Lithuania a minority had done so: 40% in Croatia and only 34% in Lithuania. In all three countries, however, a majority of those who had not already published e-books intended to do so, usually over the next two years: 54% in Sweden, 72% in Croatia and 64% in Lithuania. The direction of change is obvious, over the next two to three years it is likely that the great majority of publishers will be producing in e-books. In all three countries, the preferred format for e-book production is EPUB, followed by PDF.

The nature of the e-book makes it a relatively easy proposition for publishers to sell directly to consumers through their Websites. The Swedish publishers lead in this, with 50% of those producing e-books selling directly, while in Croatia and Lithuania the proportions are lower at 27% in both countries.

The benefits of direct selling are perceived differently in the three countries: in Sweden 67% of those selling directly think that the chief benefit is an increase in sales, while in Croatia 67% think that it is increased knowledge of buyers, and in Lithuania 83% regard the opportunity to sell other products at the same time is the main benefit.



### Assessment of drivers and barriers

Winston's theory of technological innovation ([1998](#win98)) includes two key concepts: supervening social necessity and suppression of radical potential. The supervening social necessity is the concatenation of factors that results in the widespread adoption of an innovation, while the suppression of radical potential is the result of actions by the various players in the field that inhibit development and adoption.

These two theoretical ideas were explored in the questionnaire through two questions: one (the surrogate for the social necessity) asked what factors were driving the adoption of e-books in the publisher's country, while the other asked what barriers existed that were preventing the further implementation of the innovation. Tables 3 and 4 present the results from the three countries.

<table class="center"><caption>  
Table 3: Factors driving the development of the market - percentage agreement</caption>

<tbody>

<tr>

<th>How important do you consider the following for the development of the e-book market in ...?</th>

<th>Sweden (n=100)</th>

<th>Lithuania (n=61)</th>

<th>Croatia (n=45)</th>

</tr>

<tr>

<td>Consumers' demand for a portable and convenient format</td>

<td style="text-align:center; background-color:#F5D90C;">85%</td>

<td style="text-align:center; background-color:#F5D90C;">92%</td>

<td style="text-align:center; background-color:#F5D90C;">91%</td>

</tr>

<tr>

<td>Pressure from technology manufacturers</td>

<td style="text-align:center;">53%</td>

<td style="text-align:center;">57%</td>

<td style="text-align:center; background-color:#F5D90C;">88%</td>

</tr>

<tr>

<td>The economic potential of the e-book for the publishing industry</td>

<td style="text-align:center; background-color:#F5D90C;">62%</td>

<td style="text-align:center; background-color:#F5D90C;">76%</td>

<td style="text-align:center; background-color:#F5D90C;">89%</td>

</tr>

<tr>

<td>Copyright protection through DRM</td>

<td style="text-align:center;">41%</td>

<td style="text-align:center;">64%</td>

<td style="text-align:center; background-color:#F5D90C;">79%</td>

</tr>

<tr>

<td>The ability of authors to self-publish</td>

<td style="text-align:center; background-color:#F5D90C;">67%</td>

<td style="text-align:center; background-color:#F5D90C;">73%</td>

<td style="text-align:center;">50%</td>

</tr>

<tr>

<td>The development of technology use in education</td>

<td style="text-align:center; background-color:#F5D90C;">69%</td>

<td style="text-align:center; background-color:#F5D90C;">90%</td>

<td style="text-align:center; background-color:#F5D90C;">84%</td>

</tr>

</tbody>

</table>

As Table 3 shows, there is general agreement across all three countries that consumer demand for portability is the primary driving force in the adoption of e-books. However the fact that in all three countries almost all of the factors find a majority in agreement (except for the role of DRM in Sweden and self-publishing in Croatia), suggests that it is this concatenation of factors that at least contributes to the supervening social necessity, at least in the perception of the responding publishers. There is some difference between the countries on the exact constitution of the social necessity: thus, in both Sweden and Lithuania, the primary forces appear to be consumer demand, technology use in education, the potential of self-publishing and the economic potential of the e-book. In Croatia, on the other hand, self-publishing is seen as less significant, copyright protection is seen as more significant, and the remaining factors are very close in perceived significance.

<table class="center"><caption>  
Table 4: Barriers to the development of the market for e-books - percentage agreement</caption>

<tbody>

<tr>

<th>How important do you consider the following 'barriers' to be in the development of the e-book market in ...?</th>

<th>Sweden (n=100)</th>

<th>Lithuania (n=62)</th>

<th>Croatia (n=45)</th>

</tr>

<tr>

<td>The limited size of the domestic market</td>

<td style="text-align:center; background-color:#F5D90C;">71%</td>

<td style="text-align:center; background-color:#F5D90C;">90%</td>

<td style="text-align:center; background-color:#F5D90C;">86%</td>

</tr>

<tr>

<td>Lack of an export market</td>

<td style="text-align:center;">57%</td>

<td style="text-align:center; background-color:#F5D90C;">91%</td>

<td style="text-align:center;">73%</td>

</tr>

<tr>

<td>Readers' preference for printed books</td>

<td style="text-align:center; background-color:#F5D90C;">76%</td>

<td style="text-align:center;">79%</td>

<td style="text-align:center;">69%</td>

</tr>

<tr>

<td>The cost of e-readers or their alternatives</td>

<td style="text-align:center;">47%</td>

<td style="text-align:center;">73%</td>

<td style="text-align:center;">71%</td>

</tr>

<tr>

<td>Lack of a common model for library lending of e-books</td>

<td style="text-align:center; background-color:#F5D90C;">71%</td>

<td style="text-align:center;">74%</td>

<td style="text-align:center; background-color:#F5D90C;">81%</td>

</tr>

</tbody>

</table>

The main barriers to the development of the e-book market are perceived rather differently in the three countries. In Sweden, three factors have very similar majorities: the limited size of the domestic market, readers' preference for printed books, and the lack of a suitable model for library lending of e-books. In Lithuania, two factors dominate: the limited size of the domestic market and the lack of an export market, while in Croatia, the first of these is ranked first, but the lack of a suitable library lending model is ranked second. Taking these factors to represent the suppression of radical potential, it seems clear that, in the publishers' view, the limited market is the most serious barrier to achieving the potential of the e-book in the market. Respondents had the opportunity to comment; only six did so and, of these, only one made a substantive suggestion that another factor was the integration of digital distribution channels and services into other digital services.



### Opinions about changing relationships in the book sector

Both theoretical concepts can be addressed not only in the direct questions, but also explored by looking into the opinions of the publishers about the relationships with other actors in the book sector. We have chosen to present here three of these partners: authors, book-sellers, and libraries.



#### Self-publishing



One of the major outcomes of the rise of the e-book has been the increase in author self-publishing ([Flood, 2013](#flo13); [eBook sales, 2014](#ebo14); [Young, 2014](#you14)) and a number of self-published authors have been extremely successful, some going on to be picked up by publishers and published in print. Such authors are in the minority, as Weinberg notes (hybrid' authors are those both self-publishing and traditionally published):

> While most of the survey respondents clustered at the lower end of the income distribution, some authors did report earning $200,000 or more from their writing, the highest income choice on the survey: less than one percent (0.6%) of self-published authors, 4.5% of traditionally published authors, and 6.7% of hybrid authors who reported on their income ([Weinberg, 2013](#wei13)).

Our survey sought responses from publishers on their opinions towards self-publishing, with the result shown in Table 5.

<table class="center" style="width:95%;"><caption>  
Table 5: Publishers' views on self-publishing</caption>

<tbody>

<tr>

<th rowspan="2">Statement</th>

<th colspan="2">Sweden (n=98)</th>

<th colspan="2">Lithuania (n=59)</th>

<th colspan="2">Croatia (n=43)</th>

</tr>

<tr>

<th>% Agree</th>

<th>% Disagree</th>

<th>% Agree</th>

<th>% Disagree</th>

<th>% Agree</th>

<th>% Disagree</th>

</tr>

<tr>

<td>Self-publishing has little relevance for the publishing industry</td>

<td style="text-align:center; background-color:#F5D90C;">54</td>

<td style="text-align:center;">46</td>

<td style="text-align:center; background-color:#F5D90C;">78</td>

<td style="text-align:center;">22</td>

<td style="text-align:center; background-color:#F5D90C;">65</td>

<td style="text-align:center;">35</td>

</tr>

<tr>

<td>Self-publishing can help to identify new authors</td>

<td style="text-align:center; background-color:#F5D90C;">84</td>

<td style="text-align:center;">16</td>

<td style="text-align:center; background-color:#F5D90C;">74</td>

<td style="text-align:center;">26</td>

<td style="text-align:center; background-color:#F5D90C;">72</td>

<td style="text-align:center;">28</td>

</tr>

<tr>

<td>Self-publishing forces us to market products more effectively</td>

<td style="text-align:center;">34</td>

<td style="text-align:center; background-color:#F5D90C;">66</td>

<td style="text-align:center; background-color:#9DF208">57</td>

<td style="text-align:center;">43</td>

<td style="text-align:center;">40</td>

<td style="text-align:center; background-color:#F5D90C;">60</td>

</tr>

<tr>

<td>Self-publishing is a threat to our market position</td>

<td style="text-align:center;">11</td>

<td style="text-align:center; background-color:#F5D90C;">89</td>

<td style="text-align:center;">24</td>

<td style="text-align:center; background-color:#F5D90C;">76</td>

<td style="text-align:center;">9</td>

<td style="text-align:center; background-color:#F5D90C">91</td>

</tr>

<tr>

<td>We need to develop our own self-publishing channel</td>

<td style="text-align:center;">28</td>

<td style="text-align:center; background-color:#9DF208">72</td>

<td style="text-align:center; background-color:#F5D90C;">69</td>

<td style="text-align:center;">31</td>

<td style="text-align:center; background-color:#F5D90C;">66</td>

<td style="text-align:center;">34</td>

</tr>

</tbody>

</table>

As the cells shaded in yellow indicate, there is a fair degree of consensus among the respondents from the three countries. The two obvious differences (shaded green) are, first, that Lithuanian publishers appear to be more likely to perceive self-publishing as a reason to market more effectively, whereas publishers in both Sweden and Croatia tend to disagree with this proposition. Secondly, Swedish publishers disagree with the statement, '_We need to develop our own self-publishing channel_', which may be attributable to the existence of the collaborative eLib platform.

The same ambivalence about self-publishing appears to exist in all three countries, thus, the majority agree that self-publishing has little relevance for them, but also agree that it can help them to identify new authors, which should be considered important for publishers. The majority also do not feel that self-publishing is a threat to their market position but, at least in Lithuania and Croatia, feel a need to develop their own self-publishing channel. Though in Sweden the majority does not agree with this proposition, it is worth noting that the biggest publisher in Sweden, Bonnier, recently introduced its own self-publishing channel, _[Type and Tell](https://www.typeandtell.se/)_: a news item ([Ekman, 2015](#ekm15)) reports that, after a month in the market, 400 users had registered and three books had been published.



#### E-books and booksellers

Bookshops in both the UK and the USA have experienced a reduction in numbers since the rise of the online retailer, Amazon, and it appears that the development of the e-book has increased the pace of decline. The situation in Sweden seems to have been somewhat more stable, according to the Nordic book statistics report ([Nordic..., 2012](#nor12)), which shows book stores in Sweden holding steady at about 400 between 2003 and 2012, however, the distribution is changing, with 86 communities having lost their only bookshop since 1970 ([Olsson, 2015](#ols15)). The trend may be downwards, but there are no firm data on this, as far as we can discover. At the end of 2013, there were 207 bookshops in different municipalities of Lithuania as compared to 184 bookshops and 200 kiosks selling books in 1985 ([Gudinavicius and Nagyte, 2014](#gud14)).

<table class="center" style="width:95%;"><caption>  
Table 6: Publishers' views on the role of bookselling</caption>

<tbody>

<tr>

<th rowspan="2">Statement</th>

<th colspan="2">Sweden (n=98)</th>

<th colspan="2">Lithuania (n=63)</th>

<th colspan="2">Croatia (n=44)</th>

</tr>

<tr>

<th>% Agree</th>

<th>% Disagree</th>

<th>% Agree</th>

<th>% Disagree</th>

<th>% Agree</th>

<th>% Disagree</th>

</tr>

<tr>

<td>Booksellers will continue to function as outlets for both printed books and e-books</td>

<td style="text-align:center; background-color:#F5D90C;">53</td>

<td style="text-align:center;">47</td>

<td style="text-align:center; background-color:#F5D90C;">64</td>

<td style="text-align:center;">36</td>

<td style="text-align:center; background-color:#F5D90C;">63</td>

<td style="text-align:center;">37</td>

</tr>

<tr>

<td>Only online bookshops will sell e-books</td>

<td style="text-align:center;">48</td>

<td style="text-align:center; background-color:#9DF208">52</td>

<td style="text-align:center; background-color:#F5D90C;">53</td>

<td style="text-align:center;">47</td>

<td style="text-align:center; background-color:#F5D90C;">54</td>

<td style="text-align:center;">47</td>

</tr>

<tr>

<td>The role of the bookseller will decline as e-book sales increase</td>

<td style="text-align:center; background-color:#F5D90C;">64</td>

<td style="text-align:center;">36</td>

<td style="text-align:center; background-color:#F5D90C;">59</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">46</td>

<td style="text-align:center; background-color:#9DF208">54</td>

</tr>

<tr>

<td>Sales of e-books will increasingly shift to direct sales from publishers to readers</td>

<td style="text-align:center; background-color:#F5D90C;">57</td>

<td style="text-align:center;">43</td>

<td style="text-align:center; background-color:#F5D90C;">82</td>

<td style="text-align:center;">18</td>

<td style="text-align:center; background-color:#F5D90C;">89</td>

<td style="text-align:center;">11</td>

</tr>

</tbody>

</table>

As in the case of the self-publishing issue, publishers are ambivalent about the future role of booksellers. A majority in each country hold that '_Booksellers will continue to function as outlets for both printed books and e-books_', but on the other hand a greater majority believe that, '_Sales of e-books will increasingly shift to direct sales from publishers to readers_'. In Lithuania and Croatia the majority believes that only the online bookstores will sell e-books. In the case of Sweden and Lithuania a majority also believe that the role of the bookseller will decline as e-book sales increase. These views, if not entirely mutually contradictory, do seem to be incompatible: booksellers who wish to engage in the sale of e-books must have the appropriate technology to link to publisher's download sites and must be able to respond to demand from readers with many different kinds of device from smartphones to e-readers and the various models of tablet computer, which would probably require the retraining of staff to support such a service. The probability, therefore, that booksellers will be able to engage in selling e-books, seems rather low.



#### The role of public libraries

Finally, the relationship between public libraries and publishers has been a troubled one, since the introduction of the e-book, especially in the English language book market, where publishing is dominated by a small number of publishers. Publishers have been reluctant to make e-books available to public libraries on the same basis as printed books, and have either limited the number of loans that may be made of a copy, or have priced the e-books several times higher than the printed book ([American Library Association, 2013](#ame13); [Sullivan, 2012](#sul12); [Vinjamuri, 2012](#vin12)).

Table 7 shows an interesting consistency across the three countries in the attitudes of publishers towards public library lending of e-books. There is a clear majority in favour of selling e-books on the same basis as printed books. On the other hand, not exactly in the same way: a slightly smaller majority is in favour of limiting the number of loans for each 'copy'. With regard to differential pricing (where e-books may cost multiples of the printed book price) there was only a 6% difference between those for and against in Sweden and Lithuania, whereas the majority opposed to this was much stronger in Croatia. Only a very small minority believe that e-books should not be made available at all to public libraries.

<table class="center" style="width:95%;"><caption>  
Table 7: Publishers' views on the role of public libraries</caption>

<tbody>

<tr>

<th rowspan="2">Statement</th>

<th colspan="2">Sweden (n=99)</th>

<th colspan="2">Lithuania (n=63)</th>

<th colspan="2">Croatia (n=44)</th>

</tr>

<tr>

<th>% Agree</th>

<th>% Disagree</th>

<th>% Agree</th>

<th>% Disagree</th>

<th>% Agree</th>

<th>% Disagree</th>

</tr>

<tr>

<td>E-books should be sold to libraries in the same way as printed books</td>

<td style="text-align:center; background-color:#F5D90C;">67</td>

<td style="text-align:center;">33</td>

<td style="text-align:center; background-color:#F5D90C;">67</td>

<td style="text-align:center;">32</td>

<td style="text-align:center; background-color:#F5D90C;">77</td>

<td style="text-align:center;">23</td>

</tr>

<tr>

<td>E-books should be allowed to be loaned for a limited number of loans</td>

<td style="text-align:center; background-color:#F5D90C;">57</td>

<td style="text-align:center;">47</td>

<td style="text-align:center; background-color:#F5D90C;">53</td>

<td style="text-align:center;">46</td>

<td style="text-align:center; background-color:#F5D90C;">58</td>

<td style="text-align:center;">42</td>

</tr>

<tr>

<td>E-books should be priced higher for libraries than printed books because of the possibility of an unlimited number of loans</td>

<td style="text-align:center;">47</td>

<td style="text-align:center; background-color:#F5D90C;">53</td>

<td style="text-align:center;">47</td>

<td style="text-align:center; background-color:#F5D90C;">53</td>

<td style="text-align:center;">39</td>

<td style="text-align:center; background-color:#F5D90C;">61</td>

</tr>

<tr>

<td>E-books should not be made available to public libraries</td>

<td style="text-align:center;">9</td>

<td style="text-align:center; background-color:#F5D90C;">91</td>

<td style="text-align:center;">9</td>

<td style="text-align:center; background-color:#F5D90C;">90</td>

<td style="text-align:center;">17</td>

<td style="text-align:center; background-color:#F5D90C;">83</td>

</tr>

</tbody>

</table>



## Discussion

Although the sales of e-books as a proportion of all book sales is very low in all three of the countries surveyed it is evident, from the survey responses, that publishers are uncertain about how to treat this newcomer in the industry. The publishers in three small book markets limited by national language speakers seem to be thinking about e-books along the same lines.

The review of earlier surveys has revealed that though not receiving high revenues from e-books a large number of American, Canadian and French publishers were increasingly converting their backlist titles to e-books. However, this was done rather cautiously without big investment and mainly outsourcing the activity. Consumer demand seems to be the main driver of this development in larger international markets, and it is also named as the main driver by the publishers in our survey. However, the impact of this driver must be relatively small, given the market share of e-books, by the fact that (see Table 4) Readers' preference for printed books is seen as one of the main barriers to further market growth.

The review of the surveys shows that the most popular channels for e-book sales in Canada, France and Germany are online retail outlets or wholesale intermediaries. However, a number of publishers sell e-books directly to consumers from corporate Websites. One must point out though, that in different countries the tendencies are different: in Canada the number of directly selling publishers increased from 42% in 2013 to 66% in 2014, while in Germany (where the Tolino online bookseller is owned by the major publishers) it decreased from 26% in 2010 to 14% in 2013\. The publishers in the small book markets of our survey are still undecided about the best sales channels for e-books, though online retail outlets and direct sales seem to win over the traditional publishers' loyalty to physical bookshop.

Internationally, publishers are selling directly to the consumer and the subject has been a feature of the publishing trade press over the past couple of years. For example, HarperCollins redesigned its Website in 2014 to allow for more direct sales ([HarperCollins..., 2014](#har14)), a move that was not welcomed by at least one bookseller (and former HarperCollins publishing executive):

> Consumers are not looking for publishers, they're looking to retailers to aggregate and recommend titles. Harper is disaggregating our audience. They can't offer an array of topics and publications. While I do understand what they are trying to do, they should be working to amplify their existing retail channels'. ([HarperCollins..., 2014](#har14))

Of course, HarperCollins is not the only publisher to engage in direct sales: the same article noted that, 'Currently, Hachette is the only Big Five publisher that does not sell its print books directly to consumers', and in a forecast for 2015, Tappuni ([2015](#tap15)) suggested that,

> As more publishers roll-out consumer friendly websites and enter partnerships with platforms designed to make buying and selling easier, it's possible that 2015 will be the year that direct-to-consumer sales break through.

In Sweden the practice varies: most publishers sell through the eLib platform (part-owned by four of largest publishers), which also serves as the public libraries' e-book lending platform, and/or through one or other of the online bookshops, such as [Bokus.com](http://www.bokus.com/) and [Adlibris.com](http://www.adlibris.com/se). For example, the [Norstedts](http://www.norstedts.se/) site directs the user to Bokus.com for online sales, while [Albert Bonniers Förlag](http://www.albertbonniersforlag.se/), a member of the Bonnier group, directs to Adlibris. One of the largest publishers, however, [Natur och Kultur](https://www.nok.se/), sells directly from its Website. Lithuanian and Croatian publishers also practice book sales through their own Websites, though in Lithuania e-books are usually sold through specialised online bookstores (e.g., [Skaitykle.lt](http://www.skaitykle.lt/), [Milzinas.lt](https://milzinas.lt/)) and in Croatia distributed by online libraries (e.g., eknjizara.vip.hr or [TookBook.com](https://library.tookbook.com/)).

Ultimately, of course, economics will decide the situation: if publishers decide that they can manage without the booksellers' ability to select books from diverse sources for the tastes of their local community, while retaining that part of overall profit that would have gone to the bookseller, they are likely to take the direct sales route and the neighbourhood bookshop will continue to decline.

The use of DRM and other antipiracy means may be an indicator of the nervousness of publishers about illegal copying and distribution. Aptara ([2012](#apt12)) found that 71% of publishers used some antipiracy mechanisms, but in Germany the use of DRM had diminished in 2014\. Collinson ([2015](#col15)) found that 48% of publishers in his survey did not like DRM as a deterrent to e-book pirates, mainly because this hinders the legal users and its efficiency seems to be questionable.

The position of publishers, at this point in the emergence of e-books is similar to that of music publishers when the Napster program and Website introduced file-sharing to the world of pop music. Music publishers reacted in various ways to the perceived threat of piracy, with very little in the way of supporting research evidence to justify their actions. According to a study by Agular and Martens of more than 16,000 European consumers, '_our findings indicate that digital music piracy does not displace legal music purchases in digital format_' ([Agular and Martens, 2013](#agu13), p. 17).

They also note that, '_clicks on legal purchase websites would have been 2% lower in the absence of illegal downloading websites_' (p. 16), suggesting that people are using illegal downloading to 'filter' what they want to buy.

One study has suggested something similar for e-books: a publishing consultant, Brian O'Leary conducted a study for O'Reilly Media:

Surprisingly, he found that sales actually increased after their books showed up on pirate sites. Piracy seems to have boosted sales. O'Leary says people may have been using the pirated editions to sample books before they actually opened up their wallets ([Misener, 2011](#mis11), April 19).

That notion of sampling books is very close to the idea of Agular and Martens that music lovers were using download sites to filter before buying.

Piracy, in any field, is a matter of concern for the producer of creative products such as music, books and films, but the fear that, necessarily, it will be detrimental to profits does not appear to have much support. The fear is perhaps fear of the unknown and of uncertainty about how to respond to the development of a new medium, rather than of some objective threat. The growth of self-publishing, however, caused much less fear for the publishers in the three surveyed countries.

These are not the only concerns of publishers; as noted below in the comments on the results, they are also concerned that the free lending of e-books by public libraries may eat into their profits as well as reducing visits to booksellers, which are, at present, their main retail outlets.

The views on relationships with public libraries contrast somewhat with the practice of publishers in the English-speaking market, particularly in the USA where, initially, none of the Big Five publishers was prepared to allow the lending of e-books at all. Several meetings took place between the American Library Association and the Publishers' Association (as well as with individual publishers and book distributors) (see, e.g., [Terry, 2011](#ter11); [Clark, 2012](#cla12)). Ultimately all of the Big Five have allowed library lending of their e-books but they impose various constraints, such as limiting the number of users or the total number of loans for 'a copy'. The Aptara ([2012](#cla12)) survey has shown that 57% of publishers were allowing access to e-books through library in 2012 and 43% were not. However, the BookNetCanada ([2014](#boo14)) survey suggested that 75% of publishers were making their books accessible through libraries and almost half of those were selling them on the same terms as for retail.

The fear of publishers in the English-speaking market is that lending by public libraries will lead to a reduction in sales and, as a secondary effect, a reduction in the number of booksellers. Recently, the Society of Chief Librarians and the Publishers Association in the UK published a report ([2015](#soc15)) on a pilot study of remote e-book lending (i.e., not requiring a visit to the library). The project was rather timid in its approach, only 893 titles were made available by publishers and the project ran in only four locations, two urban and two rural. The results (which must be highly suspect from a statistical analysis perspective) suggest that lending e-books is unlikely to affect sales, since e-book borrowers buy more books than other library users. However, they are less likely to visit the library. Thirty-nine per cent of e-book borrowers said that they were less likely to visit bookshops if access to e-books through library lending was increased, and 37 per cent said that they were less likely to buy printed books. Publishers remain suspicious of public library lending of e-books, which, in the UK, has not achieved the penetration and level of loans that exists in the USA. It may be that in 'small-language' markets, which, by definition, are small markets in general, the same suspicions will arise as the demand for e-book lending grows. So far, the attitudes of publishers towards libraries in all three surveyed countries, though ambiguous, are cautious.

However, the overall situation of e-books distribution in Sweden may be affected by the role of public libraries, which, according to the latest version of the public library law ([Sweden..., 2013](#swe13)), are required to provide literature and information in whatever medium it is published. Wischenbart, _et al._ ([2014](#wis14)) note that the 'strong role of libraries, and their popularity with many readers, has resulted in significant skepticism from publishers, on how to commercialize ebooks in meaningful ways.

Collinson's study ([2015](#col15)) found that none of the publishers surveyed expected their output of e-books to decline, although about one third expected the output to remain about the same. In our study of Swedish publishers, 64% (63 publishers) believed that the market will grow only slowly, while 32% (32) thought it would grow quickly, the remaining 4% (4) believed it would continue to grow at the same rate as at present. The data are not directly comparable, since different questions were asked in the two surveys, but it is interesting that 96% of the Swedish publishers believed that the market would grow, whether slowly or more quickly than at present. Barriers to the growth of the market are found to be very different in the reviewed surveys and are clearly related to the questions asked. Our survey suggested barriers that were not mentioned in any other study, thus, the driver _Technology use in education_ is significant in our results, but does not feature in other research.



## Conclusion

Our first research question was: _What are the similarities and differences in how the publishers in different book markets, limited by small numbers of local language speakers, perceive the drivers of and barriers to e-book publishing?_

This question has been answered earlier in this paper, where we have shown that there was a considerable degree of similarity in the responses from three _small language markets_ when asked about the drivers of e-book adoption. In all three countries, consumer demand for portability was seen as the main driver, with the adoption of the technology in education following closely behind.

There were also similarities in the perceived barriers to the adoption and use of e-books: all three countries saw the limited size of the domestic market for books of any kind as the most, or second most, important factor. In Lithuania, the lack of an export market was seen as most significant, because previously, publishers were exporting their production to Russia but a number of economic factors, including the financial crisis in Russia in 1998, effectively killed that market ([Misiunas, 2002](#mis02)). Beyond this level of agreement, the responses differed, although all three, again, attached significance to the lack of an appropriate model for public library lending of e-books.

Our second question, _How do the publishers in small book markets see their relations with traditional actors in the book sector in relation to the changes caused by the e-book?_ has also been answered above, with very similar opinions being expressed in all three countries about the impact of self-publishing, the role of the traditional bookshop, and the role of libraries.

The overall conclusion from this study is that the results are remarkably similar from all three countries on the issues selected for investigation. The slight variation in the Swedish position on developing their own self-publishing channel may result from seeing the possibility of developing the eLib platform in that direction, and the Swedish publishers' belief that not only online booksellers will sell e-books may relate to the majority believing that direct selling from publisher to reader will increase. The Lithuanian perception that self-publishing is a reason to market more effectively may result from local conditions in the e-book market.

The small market in Sweden for books in general and for e-books in particular is clearly a strong inhibitor to the development of the e-book. We suspect that the findings of the Aptara and Publisher's Weekly for the USA are likely to apply to small language markets to an even greater degree:

> As previously stated, publishers are 'functioning in the high-tech society' of eBooks, but this year's survey data indicates that plenty of room remains for publishers to optimize their digital operations to fully capitalize on eBooks' potential:  
> - 60% of eBook publishers still employ print-based editorial and production workflows that add time and cost to each eBook.  
> - More than half of publishers' content is going to 'digital waste': 65% of eBook publishers have converted less than half of their legacy titles (backlist) into eBooks.  
> - 86% of eBook publishers still produce a print version of every eBook title. ([Aptara and Publisher's Weekly, 2012](#apt12))

In Sweden and elsewhere, the e-book is driving some restructuring in the industry. For example, in Sweden the four major publishers combined to create eLib, the library lending platform, which also acts as a sales outlet. An interest in eLib was subsequently sold to Axiell, but the four publishers retain a significant share of the business. One of the big four, Bonnier has started its own self-publishing site, as noted above, and is also starting a subscription service, BookBeat. Thus, one major publisher is manoeuvring to control the distribution chain and, at least for the subscription service, there is the potential for collaboration with the other major publishers. In Lithuania, the partner of the biggest group of book publishing and distribution companies Alma Litera has just created a software application for reading e-books, Milainas, and opened its own e-book shop, which started competing with the oldest online e-book store Skaitykle.lt

Similarly, in Germany, Bertelsmann is part of the consortium that created Tolino, the online bookshop, and also established Skoobe, the e-book subscription service, together with the other major German publisher, Holzbrinck. Another German company, Droemer Knaur, has started a self-publishing channel, [neobooks](https://www.neobooks.com/home) ([Streichelenheit..., 2013](#str13)).

The potential here for international collaboration in this kind of restructuring of the industry is considerable, since Bertelsmann is the parent company of Penguin Random House, and Holzbrinck is the parent of Macmillan. We can expect that the changes described here will not be the last to occur in the publishing industry and that, even in small language markets, mergers and acquisitions will continue to take place, self-publishing is likely to grow, the number of bookshops will continue to decrease, online retailing will grow, and the whole balance of power in the industry will shift. Exactly how these changes will affect the production, distribution and use of e-books is impossible to determine, but, given the ubiquity of the main reading devices (i.e., smartphones and tablet computers), it seems likely that the demand for e-books will continue to grow, albeit slowly.


## Acknowledgements

We would like to thank the referees for their useful comments, and the Swedish Research Council (Vetenskapsrådet ) for their support under research grant number 2012-5740\. Our thanks also to colleagues in Lithuania and Croatia for permission to use the data from their surveys.

## About the authors

**T.D. Wilson** is Senior Professor, University of Borås, Sweden and Professor Emeritus of the University of Sheffield, U.K. He is also Visiting Professor at the University of Leeds Business School. He holds a BSc degree in Economics and Sociology from the University of London and a Ph.D. from the University of Sheffield. He has received Honorary Doctorates from the Universities of Gothenburg, Sweden and Murcia, Spain. He has carried out research in a variety of fields from the organizational impact of information technologies and information management to human information behaviour. He can be contacted at [wilsontd@gmail.com](mailto:wilsontd@gmail.com).  
**Elena Maceviciute** is a Professor in the Swedish School of Librarianship and Information Science, University of Borås. She is also Professor in the Institute of Book Science and Documentation, Faculty of Communication, Vilnius University, Lithuania. Her research relates to information use in organizations, digital libraries and resources, and, currently, the role of e-books in modern society. She can be contacted at [elena.maceviciute@gmail.com.](mailto:elena.maceviciute@gmail.com)



#### References

*   Aptara. (2011). _[Uncovering e-books real impact: Aptara's third annual eBook survey of publishers.](http://www.webcitation.org/6db3nqcJU)_ Falls Church, VA: Aptara Corporation. Retrieved from http://stream.aptaracorp.com/Aptara_eBook_Survey_3.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6db3nqcJU)
*   Aptara & Publishers' Weekly (2012). _[Revealing the business of eBooks: the fourth annual survey of eBook publishers.](http://www.webcitation.org/6VgCDmGA3)_ Falls Church, VA: Aptara Corporation. Retrieved from http://valordecambio. com/files/2012/11/Aptara_eBook_Survey_4.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6VgCDmGA3)
*   Agular, L. & Martens, B. (2013). _[Digital music consumption on the Internet: evidence from clickstream data](http://www.webcitation.org/6db9k2DqB)._ Luxembourg: European Commission, Joint Research Centre, Institute for Prospective Technological Studies. Retrieved from http://bit.ly/1OPEAOI (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6db9k2DqB)
*   American Library Association. (2013). [The state of America's libraries.](http://www.webcitation.org/6db9oXcyR) _American Libraries_, (Special issue). Retrieved from http://www.ala.org/news/sites/ala.org.news/files/content/2013-State-of-Americas-Libraries-Report.pdf Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6db9oXcyR)
*   BookNet Canada. (2014). _[The state of digital publishing in Canada 2013](http://www.webcitation.org/6Vg87yJIW)_. Toronto, ON: BookNet Canada. Retrieved from http://issuu.com/booknetcanada/docs/bnc_research_state_of_digital_ publi_1fa17fa049804b/3?e=19249060/30399442f (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6Vg87yJIW).
*   BookNet Canada. (2015). _[The state of digital publishing in Canada 2014](http://www.webcitation.org/6db3AuRxL)._ Toronto, ON: BookNet Canada. Retrieved from http://issuu.com/booknetcanada/docs/bnc_research_state_of_digital _publi/1?e=19249060/30399429 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6db3AuRxL)
*   Börsenverein des Deutschen Buchhandels. (2014). _[Verankert im Markt: das E-book in Deutschland 2013](http://www.webcitation.org/6XGTjjZrK)_. [Anchored in the market - the e-book in Germany 2013.] Frankfurt am Main, Germany: Börsenverein des Deutschen Buchhandels. Retrieved from http://www.boersenverein. de/sixcms/media.php/976/Kurzversion_E-Book-Studie2014.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6XGTjjZrK)
*   Bowker. (2012, March 27). _[Bowker releases results of global ebook research.](http://www.webcitation.org/6OVlFHLON)_ [Press release]. Retrieved from "http://bit.ly/1ip0L21"http://bit.ly/1ip0L21 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6OVlFHLON)
*   Buschow, C., Noelle, I. & Schneider, B. (2014) German book publishers' barriers to disruptive innovations: the case of e-book adoption. _Publishing Research Quarterly, 30_(1), 63-76.
*   Christensen, C.M. (1997). _The innovator's dilemma: when new technologies cause great firms to fail_. Boston, MA: Harvard Business School Press.
*   Clark, L. (2012, March 20). [ALA President Molly Raphael releases report on e-book talks with e-book distributors](http://www.webcitation.org/6ZJJTt5l7). _ALA News_. Retrieved from http://www.ala.org/news/press-releases/2012/03/ala-president-molly-raphael-releases-report-e-book-talks-e-book-distributors (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6ZJJTt5l7)
*   Collinson, S. (2015). _[The small publisher ebook report](http://www.webcitation.org/6db9ys2vQ)_. London. Retrieved November 3, 2015, from http://simoncollinson.com/report/index.html. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6db9ys2vQ)
*   Cordón-García, A. J. , Linder, D., Gómez-Díaz, R., Alonso-Arévalo J. (2014) E-Book publishing in Spain: the paradoxes of a dual model. _The Electronic Library, 32_(4), 567-582
*   [eBook sales growth - where it's really coming from (an analysis of author earnings).](http://www.webcitation.org/6dbA73sQu) (2014, February 14). _Publishing Technology._ Retrieved from http://www.publishingtechnology.com/ 2014/02/ebook-sales-growth-where-its-really-coming-from-an-analysis-of-author-earnings/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbA73sQu)
*   Ekman, M. (2015, September 28). [Siffror viktigare än bokstäver på bokmässan.](http://www.webcitation.org/6mHob2AbI) [Numbers more important than characters in the book fair]. _SvD Näringsliv_. Retrieved from http://www.svd.se/siffror-viktigare-an-bokstaver-pa-bokmassan. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6mHob2AbI)
*   Flood, A. (2013, June 13). [Self-publishing boom lifts sales by 79% in a year](http://www.webcitation.org/6dbAOjtOK). _The Guardian_. Retrieved from http://www.theguardian.com/books/2014/jun/13/self-publishing-boom-lifts-sales-18m-titles-300m. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbAOjtOK)
*   Gudinavičius A. & Nagyte, G. (2014). [Knygynu geografija Lietuvoje 2013 metais](http://www.webcitation.org/6l3froLoF) [Bookstore geography in Lithuania in 2013]. _Istorija, 96_(4), 98-132\. Retrieved from http://istorijoszurnalas.lt/index.php?option=com_content&view=article&id=568&Itemid=539\. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6l3froLoF)
*   Gudinavičius, A. & Šuminas, A. & Maceviciute, E. (2015). [E-book publishing in Lithuania: the publisher's perspective](http://www.webcitation.org/6dbAWCFYb). _Information Research, 20_(2), paper 672\. Retrieved from http://informationr.net/ir/20-2/paper672.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbAWCFYb)
*   [HarperCollins tries direct sales.](http://www.webcitation.org/6ZHiQESYk)) (2014, July 8). _Publishers Weekly_. Retrieved from http://www.publishersweekly.com/pw/by-topic/industry-news/publisher-news/article/63227-new-hc-site-promotes-direct-sales.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6mHoxaYPy)
*   Hart, M. (1992). _[The history and philosophy of Project Gutenberg](http://www.webcitation.org/6SYPBTTdp)_. Retrieved from http://www.gutenberg.org/wiki/Gutenberg:The_History_and_Philosophy_of_Project_ Gutenberg_by_Michael_Hart (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6SYPBTTdp)
*   Hoffelder, N. (2016, June 2). [Nielsen: trad pub ebook unit sales down in 2015](http://www.webcitation.org/6l3g4XJWu). _The Digital Reader_. Retrieved from http://the-digital-reader.com/2016/06/02/nielsen-trad-pub-ebook-unit-sales-down-in-2015/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6l3g4XJWu)
*   KPMG. (2014). _[Baromètre 2014 de l'offre de livres numériques en France](http://www.webcitation.org/6Vg9wWswT)_. [Barometer 2014 of the offer of e-books in France.] Paris: KPMG. Retrieved from http://www.youscribe.com/catalogue/tous/litterature/barometre-kpmg-offre-de-livres-numeriques-en-france-2410821 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6Vg9wWswT).
*   KPMG. (2015). _[Baromètre 2015 de l'offre de livres numériques en France](http://www.webcitation.org/6l2RA3RAY)_. [Barometer 2014 of the offer of e-books in France.] Paris: KPMG. Retrieved from https://assets.kpmg.com/content/dam/kpmg/pdf/2016/06/fr-barometre-livre-numerique-2015.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6l2RA3RAY).
*   KPMG. (2016). _[Baromètre 3016 de l'offre de livres numériques en France](http://www.webcitation.org/6l2R4WjWj)_. [Barometer 2016 of the offer of e-books in France.] Paris: KPMG. Retrieved from https://assets.kpmg.com/content/dam/kpmg/fr/pdf/2016/09/fr-barometre-offre-livre-numerique.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6l2R4WjWj).
*   Languages of India. (2014). _Wikipedia_. Retrieved from http://en.wikipedia.org/wiki/ Languages_of_India
*   Lietuvos Nacionaline Martyno Mažvydo Biblioteka. (2015). _Lietuvos spaudos statistika 2014_. [Lithuananian press statistics, 2014] Vilnius: Lietuvos Nacionaline Martyno Mažvydo Biblioteka.
*   Misener, D. (2011, April 19). [E-book piracy may have unexpected benefits for publishers](http://www.webcitation.org/6S2w4au8M). Retrieved from _CBC News, Technology and Science_, http://bit.ly/1BOBogF Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6S2w4au8M)
*   Misiunas R. (2002). Leidyba Lietuvoje. [Publishing in Lithuania] _Daile, 1_, 86-90
*   Mussinelli C. (2010) Digital publishing in Europe: a focus on France, Germany, Italy and Spain. _Publishing Research Quarterly, 26_(3), 168-175.
*   Nemec, D.D. (n.d.). _[Croatian book market and book policies of the Ministry of Culture-current status and perspectives](http://www.webcitation.org/6dbAig5pf)_. [Powerpoint presentation]. Retrieved from http://old.bibf.net/WebSite/Logo/File/09c5317f-2252-44d4-ae17-b2ff42091dd2.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbAig5pf)
*   Nilsson, K,, Maceviciute, E,, Wilson, T, Bergström, A. & Höglund, L, (2015) The tensions of e-book creation and distribution in a small-language culture. _Northern Lights: Film and Media Studies Yearbook, 13_(1), 29-47.
*   [Nordic book statistics report, 2012.](http://www.webcitation.org/6dbAo0ioy) (2012). Retrieved from http://www.forlaggare.se/sites/ default/files/Nordic%20Book%20Statistics%20Report%202012.pdf. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbAo0ioy).
*   Olsson, L. (2015). [En studie av allmänbokhandel i Sverige våren 2015](http://www.webcitation.org/6dbAyVEYB). [A study of general bookshops in Sweden in 2015.] Retrieved from http://svenskabokhandlareforeningen.se/wp-content/uploads/2015/05/0.pdf. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbAyVEYB)
*   Phillips, A. (2014). _Turning the page: the evolution of the book_. London: Routledge.
*   Rimm, A-M. (2014). Conditions and survival: views on the concentration of ownership and vertical integration in German and Swedish publishing. _Publishing Research Quarterly, 30_(1), 77-92.
*   Society of Chief Librarians and Publishers Association. (2015). _[Report on the remote ebook lending pilots.](http://www.webcitation.org/6dbB4w92K)_ London: Publishers Association Retrieved from http://www.publishers.org.uk/ EasySiteWeb/GatewayLink.aspx?alId=18916\. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbB4w92K).
*   _[Statistics about reading habits in Croatia.](http://www.webcitation.org/6dbBBc18b)_ (2012, February 16). Retrieved from http://www.culturenet.hr/default.aspx?id=43252\. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbBBc18b).
*   [Streicheleinheit für Selfpublisher](http://www.webcitation.org/6dbbBwscq) [A caress for self-publisher.] (2013, July 24). _boersenblatt.net das Portal der Buchbranche_. [Web log entry]. Retrieved from http://www.boersenblatt.net/ 630760/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbbBwscq)
*   Sullivan, M. (2012, September 28). [An open letter to America's publishers from ALA President Maureen Sullivan](http://www.webcitation.org/6dbBHg5Ws). Retrieved from American Library Association Web site http://www.ala.org/news/2012/09/open-letter-america%E2%80%99s-publishers-ala-president-maureen-sullivan (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbBHg5Ws).
*   Svenska Förlagföreningen. (2014). _[Branschstatistik 2014: rapport från Svenska Förlagföreningen.](http://www.webcitation.org/6eRIA8Tt8)_ [Industry statistics 2014: report from the Swedish Publishers' Assocation.] Stockholm: Svenska Förläggare AB. Retrieved from http://www.forlaggare.se/sites/default/files/branschstatistik_2014_web_0.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6eRIA8Tt8)
*   Sweden. _Parliament_. (2013). _[Bibliotekslag (2013:801)](http://www.webcitation.org/6l1zokV0N)_ [Library law (2013:801)]. Stockholm: Sveriges Riksdaghttp://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/bibliotekslag-2013801_sfs-2013-801 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6l1zokV0N)
*   Tappani, J. (2015, January 8). [Five publishing predictions for 2015: #4 publishers take direct to consumer sales to heart.](http://www.webcitation.org/6ZHjYAVP4) _Publishing Technology_. Retrieved from http://www.publishingtechnology.com/2015/01/five-publishing-predictions-for-2015-4-publishers-take-direct-to-consumer-sales-to-heart/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6ZHjYAVP4)
*   Terry, J. (2011, June 28). [American Library Association E-books Taskforce continues open dialogue with HarperCollins.](http://www.webcitation.org/6ZJJszcWU) _ALA News_. Retrieved from http://www.ala.org/news/press-releases/2011/06/american-library-association-e-books-taskforce-continues-open-dialogue (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6ZJJszcWU)
*   Thompson, John. (2011). _Merchants of culture: the publishing business in twenty first century._ Cambridge: Polity Press.
*   Ueda, M. (2014). _[A study on business structure of e-book in non-English language: case study of Japan.](http://www.webcitation.org/6dbBRQRpe)_ Paper presented at 20th ITS Biennial Conference, Rio de Janeiro, Brazil, 30 Nov. - 03 Dec. 2014: the Net and the Internet - Emerging Markets and Policies. Retrieved from Econstor https://www.econstor.eu/dspace/bitstream/10419/106829/1/816918554.pdf. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbBRQRpe).
*   Velagic, Z. & Pehar, F. (2013). [An overview of the digital publishing market in Croatia.](http://www.webcitation.org/6l3da48Kf) _Libellarium, 6_(1-2), 55-64\. Retrieved from http://www.libellarium.org/index.php/libellarium/article/view/184/188 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6l3da48Kf)
*   Vinjamuri, D. (2012, December 11). [The wrong war over ebooks: publishers vs. libraries.](http://www.forbes.com/sites/davidvinjamuri/2012/12/11/the-wrong-war-over-ebooks-publishers-vs-libraries/) _Forbes.com_. Retrieved from http://www.forbes.com/sites/davidvinjamuri/2012/12/11/the-wrong-war-over-ebooks-publishers-vs-libraries/ [Unable to archive.]
*   Vyas, H. (2014, September 11). [India's ebook industry shows great potential.](http://www.webcitation.org/6SYp7cON8) _Digital Book World_. Retrieved from http://www.digitalbookworld.com/2014/indias-ebook-industry-shows-great-potential/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6SYp7cON8)
*   Weinberg, D.B. (2013, December 4). [The self-publishing debate: a social scientist separates fact from fiction.](http://www.webcitation.org/6dbBi3xXZ) (Part 3 of 3). _Digital Book World_. Retrieved from http://www.digitalbookworld. com/2013/self-publishing-debate-part3/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6dbBi3xXZ).
*   Wilson, T.D. (2014). [The e-book phenomenon: a disruptive technology.](http://www.webcitation.org/6h8Eaav5M) _Information Research, 19_(2) paper 612\. Retrieved from http://InformationR.net/ir/19-2/paper612.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6h8Eaav5M)
*   Winston, B. (1998). _Media technology and society. A history: from the telegraph to the Internet._ London: Routledge.
*   Wischenbart, R., Carrenho, C., Kovac, M., Licher, V., Mallya, V. & Celaya, J. (2014). _Global ebook: a report on market trends and developments._ Vienna: Ruediger Wischenbart Content and Consulting.
*   Wischenbart, R., Carrenho, C., Chen, D., Celaya, J., Kong, Y, Kovac, M. & Mallya, V. (2016). Global eBook: a report on market trends and developments. Vienna: Ruediger Wischenbart Content and Consulting.
*   World Bank. (2014). _[Data: countries and economies](http://www.webcitation.org/6SZ3JSajk)_. Washington, DC: The World Bank. Retrieved from http://data.worldbank.org/data-catalog/population-projection-tables (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6SZ3JSajk)
*   Young, S. (2014). Me, myself, I: revaluing self-publishing in the electronic age. In John Potts, (Ed.). _The future of writing_. (pp. 33-45). Basingstoke, UK: Palgrave Macmillan.

## Appendix

[English version of the questionnaire](PublisherQuestionnaire.pdf) (Opens in a new window)