#### vol. 14 no. 3, September, 2009

* * *

# Open access publishing in Indian premier research institutions

#### [Mohammad Hanief Bhat](#author)  
Islamia College of Science & Commerce,  
Srinagar (J&K),  
India 190002

#### Abstract

> **Introduction**. Publishing research findings in open access journals is a means of enhancing visibility and consequently increasing the impact of publications. This study provides an overview of open access publishing in premier research institutes of India.  
> **Method**. The publication output of each institution from 2003 to 2007 was ascertained through Scopus and the name of source journals along with the number of publications recorded. All 4232 journal titles were searched in the Directory of Open Access Journals and then using the Google search engine to find out which journals are openly accessible.  
> **Analysis**. The data are tabulated and analysed in a systematic way to reveal findings in accordance with desired objectives.  
> **Results**. The 17,516 research articles are contributed by the five institutions and appear in 4232 journals. The Indian Institute of Science publishes 8.26% of its research output in open access journals, All India Institute of Medical Sciences 19.37%, Baba Atomic Research Centre 4.84%, Indian Institute of Technology, Delhi 3.04% and Indian Institute of Technology, Kharagpur 3.26%.  
> **Conclusions**. The study reveals that a small portion of research publications of Indian research institutes is published in open access journals, the majority in journals of Indian origin. The medical institutions are contributing more of their publications to open access journals compared to other institutions.

## Introduction

Traditional avenues of publishing are closed to many authors in developing countries. As a result, much of the research done in these countries is lost to researchers elsewhere ([Fernandez 2006](#fer06)). In India there are a number of internationally-reputed institutions which are producing a good number of research documents that are expanding the frontier of knowledge and scope for technological innovation ([Das _et al._ 2005](#das05)). As a country known internationally for its information technology industries, India is now also hosting the corporate R&D centres of some major multinational enterprises owing to its global reputation for academic and research excellence ([Das _et al._ 2007](#das07)). Poor access to international journals and the consequential low visibility of papers the papers they write are major problems facing Indian researchers. Open access is viewed as a solution to remedy this deficit ([Fernandez 2006](#fer06)). Open access calls for the free availability of scholarly literature on the Internet. The open access movement has gained significant momentum over the past few years. It maintains that all scientific and scholarly literature should be available to all for free via the Internet ([Bluh 2006](#blu06); [Mark & Shearer 2006](#mar06)). What makes open access so important is its potential effect on visibility, usage and impact of research. The careers of researchers and funding of research depend upon the up-take of their findings, as does the progress of research itself. Academic institutions, federal agencies, publishers, editors, authors and librarians increasingly rely on citation analysis for promotion, tenure funding, and reviewer and selection decisions ([Meho & Yang 2007](#meh07)). It has now been established that research impact is increased by open access ([Lawrence 2001](#law01); [Antelman 2004](#ant04); [Harnad & Brody 2004](#har04a); Harnad [2004](#har04b); Hajjem _et al._ 2005; Hajjem, Harnad & Gingras 2005; Eysenbach 2006; Norris, Oppenheim, & Rowland 2008 ). There are two ways to achieve open access to scholarly articles: by publishing in an open access journal or by depositing in an open access repository, known as open access publishing and open access archiving respectively ([Chan 2004](#cha04)). The present study attempts to ascertain the trends in open access publishing in premier research institutes of India.

## Objectives

The following objectives were laid down for the study:

1.  To assess the growth and trends of open access publications in select Indian research institutes.
2.  To assess and compare the use of open access publishing across select Indian research institutions.

## Scope

The scope of the present study is limited to research articles (excluding conference papers, reviews, letters, short surveys and editorials) published from 2003 to 2007 by the five premier research institutions of India.

## Methods

Elsevier's Scopus database is used to identify the top research contributing institutions of India and their corresponding research output covering a time period of 2003 to 2007\. Scopus claims to index 15,000 peer-reviewed journal titles including 1200 open access journals from 4000 publishers (www.info.scopus.com). The top five research institutions of India, by output, located by Scopus affiliation identifier are:

*   Indian Institute of Science
*   All India Institute of Medical Sciences
*   Baba Atomic Research Centre
*   Indian Institute of Technology, Delhi
*   Indian Institute of Technology, Kharagpur

The research article output of each of these institutions from 2003 to 2007 was ascertained through Scopus and the name of source journals along with the number of articles recorded (as of August 2008). All the 4,232 journal titles were searched in the [Directory of Open Access Journals](http://www.doaj.org/) and then using the [Google search engine](www.google.com) to find out which are openly accessible. The data were tabulated and analysed in a systematic way to reveal findings in accordance with the research objectives.

## Related literature

Kingsley ([2007](#kin07)) argues that the traditional scholarly journal system is outdated and in need of re-organization and that the new internet technologies provide opportunities for change. Ylotis ([2005](#ylo05)) looks open access archives and open access journals as steps towards the democratization of information and knowledge by removing access restrictions. McCulloch ([2006](#mcc06)) observes that open access initiative is dramatically transforming the process of scholarly communication, bringing great benefits to academic world. Chan and Costa ([2005](#cha05)) argue that open access enriches the global knowledge base by incorporating the missing research from the less developed world and improves the south-north and south-south knowledge flow. Haider ([2007](#hai07)) looks open access as a way to connect the developing world to the system of science, by providing access to scientific literature published in the developed world. Arunachalam ([2008](#aru08)) stresses the need for an open access mandate by various research organizations in India for their own research output and for projects funded by them. Krishnamurthy ([2008](#kri08)) sees open access, open source software and digital libraries as the natural result of the open models of exchange that help societies to grow and prosper. Falk ([2004](#fal04)) observes that open access is gaining momentum with very broad support from library and professional groups, university faculties and even journal publishers.

Zhang ([2007](#zha07)) believes that authors may take longer to fully realize the benefits of open access movement as presently they are confused by different open access platforms. However Herb and Muller ([2008](#her08)) discovered that the scientists use open access services to an increasing extent after becoming familiar with them. Ramachandran and Scaria ([2004](#ram04)) argue that majority of academics in the developing world are not well informed on how they could improve the visibility of their publications by making them open access. According to an international survey by Rowlands and Nicholas ([2005](#row05)), the proportion of authors publishing in an open access journals has grown considerably from 11% in 2004 to 29% in 2005\. Using the ISI database Bjork _et al._ ([2008](#bjo08)) found that 4.6% of articles published in 23,750 peer reviewed journals in 2006 were openly available on the Internet in primary open access journals, and that this percentage increased to 8.1% after an embargo period of one year. Bhat ([2008](#bha08)) found that a substantial number of research publications are available through open access journals in the Indian state of Jammu and Kashmir. The study further reveals that open access publishing is much popular in medical institutions than in other institutions of the state.

## Results and discussion

Table 1 shows that 17,516 research articles are contributed by the five institutions and that these articles appear in 4232 journals.Out of 17,516 articles, 1367 (7.8%) are published in 245 open access journals with 884 (64.66%) published in 77 Indian open access journals and 483 (35.33%) articles published in 168 foreign open access journals. The Chi-squared test was applied to determine whether the difference between open access publications of the All India Institute of Medical Sciences and the other institutions is statistically significant. Chi-squared = 667.52, which is significant at the 0.05 probability level, meaning that we can claim that the Institute publishes significantly more in open access journals than the rest.

Out of 2873 research articles published in 2003 by all the institutions 274 (9.53%) are in open access journals. In 2004 the percentage of open access articles decreases to 7.02% and in 2005 it slightly increases to 7.46%. In 2006 and 2007 the percentage of open access articles is 7.36% and 7.87% respectively (Table 2). Most of these articles (884 or 64.66%) appear in Indian open access journals.

<table><caption>

**Table 1: Research output of Indian research institutions**  
(Figures in parenthesis indicate the number of journals)</caption>

<tbody>

<tr>

<th rowspan="2">Name of the institution</th>

<th rowspan="2">No. of research articles</th>

<th rowspan="2">No. of non- open access research articles</th>

<th colspan="3">open access research articles</th>

</tr>

<tr>

<th colspan="1">Indian</th>

<th colspan="1">Foreign</th>

<th colspan="1">Total</th>

</tr>

<tr>

<td>Indian Institute of Science</td>

<td>4789 (1033)</td>

<td>4393 (979)</td>

<td>197 (11) 49.74%</td>

<td>199 (43) 50.25%</td>

<td>396 (54)</td>

</tr>

<tr>

<td>All India Institute of Medical Sciences</td>

<td>3185 (839)</td>

<td>2568 (743)</td>

<td>482 (36) 78.11%</td>

<td>135 (60) 21.88%</td>

<td>617 (96)</td>

</tr>

<tr>

<td>Baba Atomic Research Centre</td>

<td>3117 (623)</td>

<td>2966 (595)</td>

<td>101 (13) 66.88%</td>

<td>50 (15) 33.11%</td>

<td>151 (28)</td>

</tr>

<tr>

<td>Indian Institute of Technology, Delhi</td>

<td>3058 (863)</td>

<td>2965 (829)</td>

<td>40 (8) 43.01%</td>

<td>53 (26) 56.98%</td>

<td>93 (34)</td>

</tr>

<tr>

<td>Indian Institute of Technology, Kharagpur</td>

<td>3367 (874)</td>

<td>3257 (841)</td>

<td>64 (9)58.18%</td>

<td>46 (24) 41.81%</td>

<td>110 (33)</td>

</tr>

<tr>

<td>Totals</td>

<td>17516 (4232)</td>

<td>16149 (3987)</td>

<td>884 (77) 64.66%</td>

<td>483 (168) 35.33%</td>

<td>1367 (245)</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 2: Annual research output of Indian research institutions**</caption>

<tbody>

<tr>

<th>Year</th>

<th>Total no. of research articles</th>

<th>No. of open access articles</th>

<th>No. of non- open access articles</th>

</tr>

<tr>

<td>2003</td>

<td>2,873</td>

<td>274 (9.53%)</td>

<td>2,599 (90.46%)</td>

</tr>

<tr>

<td>2004</td>

<td>3,119</td>

<td>219 (7.02%)</td>

<td>2,900 (92.97%)</td>

</tr>

<tr>

<td>2005</td>

<td>3,403</td>

<td>254 (7.46%)</td>

<td>3,149 (92.53%)</td>

</tr>

<tr>

<td>2006</td>

<td>4,020</td>

<td>297 (7.38%)</td>

<td>3,723 (92.61%)</td>

</tr>

<tr>

<td>2007</td>

<td>4,101</td>

<td>323 (7.87%)</td>

<td>3,778 (92.12%)</td>

</tr>

<tr>

<td>Totals</td>

<td>17,516</td>

<td>1,367 (7.8%)</td>

<td>16,149 (92.19%)</td>

</tr>

</tbody>

</table>

## Indian Institute of Science

This institution's total research output from 2003 to 2007 was 4,789 articles, out of which 396 (8.26%) were in open access journals. The yearly research output was in the range of 836-1,058 articles, with open access articles in the range of 67-88 articles (Table 3). The 396 open access articles were published in 54 open access journals out of which 11 were Indian and 43 were of foreign origin (Table 1). The distribution of open access articles was not balanced as 199 (50.25%) articles appear in 43 foreign journals while 197 (49.74%) publications appeared in just 11 Indian journals.

<table><caption>

**Table 3: Research output of the Indian Institute of Science**</caption>

<tbody>

<tr>

<th>Year</th>

<th>Total no. of research articles</th>

<th>No. of open access articles</th>

<th>No. of non- open access articles</th>

</tr>

<tr>

<td>2003</td>

<td>836</td>

<td>88 (10.52%)</td>

<td>748 (89.48%)</td>

</tr>

<tr>

<td>2004</td>

<td>874</td>

<td>67 (7.66%)</td>

<td>807 (92.33%)</td>

</tr>

<tr>

<td>2005</td>

<td>966</td>

<td>76 (7.86%)</td>

<td>890 (92.13%)</td>

</tr>

<tr>

<td>2006</td>

<td>1,058</td>

<td>89 (8.41%)</td>

<td>969 (91.58%)</td>

</tr>

<tr>

<td>2007</td>

<td>1,055</td>

<td>76 (7.20%)</td>

<td>979 (92.79%)</td>

</tr>

<tr>

<td>Totals</td>

<td>4,789</td>

<td>396 (8.26%)</td>

<td>4393 (91.73%)</td>

</tr>

</tbody>

</table>

## All India Institute of Medical Sciences

The institution has published 19.37% of its publications in open access journals. The total research output of the institution was 3,185 research articles from 2003 to 2007\. The research output of the institution increased each year from 555 articles in 2003 to 743 articles in 2007\. The highest percentage of articles published in open access journals was 21.21% during 2006 and the lowest was 16.37% during 2004 (Table 4). The 617 open access articles appeared in 96 journals, out of which 60 were foreign and 36 were of Indian origin. Indian open access journals published 482 (78.11%) articles and foreign journals published 135 (21.88%) articles. The average number of articles in Indian journals was 13.38 whereas it was just 2.25 in foreign journals (Table 1).

<table><caption>

**Table 4: Research output of the All India Institute of Medical Sciences**</caption>

<tbody>

<tr>

<th>Year</th>

<th>Total no. of research articles</th>

<th>No. of open access articles</th>

<th>No. of non- open access articles</th>

</tr>

<tr>

<td>2003</td>

<td>555</td>

<td>115 (20.72%)</td>

<td>440 (79.27%)</td>

</tr>

<tr>

<td>2004</td>

<td>574</td>

<td>94 (16.37%)</td>

<td>480 (83.62%)</td>

</tr>

<tr>

<td>2005</td>

<td>653</td>

<td>112 (17.15%)</td>

<td>541 (82.84%)</td>

</tr>

<tr>

<td>2006</td>

<td>660</td>

<td>140 (21.21%)</td>

<td>520 (78.78%)</td>

</tr>

<tr>

<td>2007</td>

<td>743</td>

<td>156 (20.99%)</td>

<td>587 (79.00%)</td>

</tr>

<tr>

<td>Totals</td>

<td>3,185</td>

<td>617 (19.37%)</td>

<td>2568 (80.62%)</td>

</tr>

</tbody>

</table>

## Baba Atomic Research Centre

The research output of the Baba Atomic Research Centre increased from 556 articles in 2003 to 757 in 2006 and then decreased to 652 in 2007\. The total research output of the institution from 2003 to 2007 stands at 3,117 research articles, out of which 151 (4.84%) were published in open access journals. The highest numbers of open access articles were published in 2003 and 2007 (36 in each year). During 2004-2006 the range of open access articles was 23-32 (Table 5). Of these, 66.88% appeared in Indian open access journals whereas 33.11% appearede in foreign open access journals. The 151 open access articles appeared in 13 Indian and 15 foreign journals. The average number of open access articles in Indian and foreign journals was 7.76 and 3.33 respectively (Table 1).

<table><caption>

**Table 5: Research output of the Baba Atomic Research Centre**</caption>

<tbody>

<tr>

<th>Year</th>

<th>Total no. of research articles</th>

<th>No. of open access articles</th>

<th>No. of non- open access articles</th>

</tr>

<tr>

<td>2003</td>

<td>556</td>

<td>36 (6.47%)</td>

<td>520 (93.52%)</td>

</tr>

<tr>

<td>2004</td>

<td>571</td>

<td>24 (4.20%)</td>

<td>547 (95.79%)</td>

</tr>

<tr>

<td>2005</td>

<td>581</td>

<td>32 (5.50%)</td>

<td>549 (94.49%)</td>

</tr>

<tr>

<td>2006</td>

<td>757</td>

<td>23 (3.03%)</td>

<td>734 (96.96%)</td>

</tr>

<tr>

<td>2007</td>

<td>652</td>

<td>36 (5.52%)</td>

<td>616 (94.47%)</td>

</tr>

<tr>

<td>Totals</td>

<td>3,117</td>

<td>151 (4.84%)</td>

<td>2966 (95.15%)</td>

</tr>

</tbody>

</table>

## Indian Institute of Technology, Delhi

The total research output of this institute from 2003 to 2007 was 3,058 research articles, out of which only 93 (3.04%) are open access. The research output of the institution increased from 463 articles in 2003 to 775 articles in 2007\. The contribution to open access journals also increased gradually from 2.8% in 2003 to 3.35% in 2007 (Table 6). The 93 open access publications appeared in 34 journals, of which 8 were Indian and 26 were of foreign origin. There were 56.98% publications in foreign open access journals and 43.01% in Indian open access journals. The average number of open access articles in Indian journals was 5.0 whereas for foreign journals it was 2.03 (Table 1).

<table><caption>

**Table 6: Research output of Indian Institute of Technology, Delhi**</caption>

<tbody>

<tr>

<th>Year</th>

<th>Total no. of research articles</th>

<th>No. of open access articles</th>

<th>No. of non- open access articles</th>

</tr>

<tr>

<td>2003</td>

<td>463</td>

<td>13 (2.80%)</td>

<td>450 (97.19%)</td>

</tr>

<tr>

<td>2004</td>

<td>522</td>

<td>13 (2.49%)</td>

<td>509 (97.50%)</td>

</tr>

<tr>

<td>2005</td>

<td>575</td>

<td>17 (2.95%)</td>

<td>558 (97.04%)</td>

</tr>

<tr>

<td>2006</td>

<td>723</td>

<td>24 (3.31%)</td>

<td>699 (96.68%)</td>

</tr>

<tr>

<td>2007</td>

<td>775</td>

<td>26 (3.35%)</td>

<td>749 (96.64%)</td>

</tr>

<tr>

<td>Totals</td>

<td>3,058</td>

<td>93 (3.04%)</td>

<td>2965 (96.95%)</td>

</tr>

</tbody>

</table>

## Indian Institute of Technology, Kharagpur

The research output of the Indian Institute of Technology at Kharagpur in 2003 was 463 articles, increasing to 876 articles in 2007\. The total number of articles published between 2003 and 2007 was 3,367, out of which 110 (3.26%) were open access. The highest proportion of open access articles was for the year 2003 (4.75%) and the lowest for 2006 (2.55%) (Table 7). The 110 open access articles appeared in 64 Indian and 46 foreign journals. Indian journals accounted for 58.18% of total open access articles while foreign journals accounted for 41.81%. The average number of open access articles in Indian journals was 7.11 and in foreign journals it was 1.91 (Table 1).

<table><caption>

**Table 7: Research output of Indian Institute of Technology, Kharagpur**</caption>

<tbody>

<tr>

<th>Year</th>

<th>Total no. of research articles</th>

<th>No. of open access articles</th>

<th>No. of non- open access articles</th>

</tr>

<tr>

<td>2003</td>

<td>463</td>

<td>22 (4.75%)</td>

<td>441 (95.24%)</td>

</tr>

<tr>

<td>2004</td>

<td>578</td>

<td>21 (3.63%)</td>

<td>557 (96.36%)</td>

</tr>

<tr>

<td>2005</td>

<td>628</td>

<td>17 (2.70%)</td>

<td>611 (97.29%)</td>

</tr>

<tr>

<td>2006</td>

<td>822</td>

<td>21 (2.55%)</td>

<td>801 (97.44%)</td>

</tr>

<tr>

<td>2007</td>

<td>876</td>

<td>29 (3.31%)</td>

<td>847 (96.68%)</td>

</tr>

<tr>

<td>Totals</td>

<td>3367</td>

<td>110 (3.26%)</td>

<td>3257 (96.73%)</td>

</tr>

</tbody>

</table>

Collectively the 1,367 open access articles appeared in 184 journals with the average of 7.42 articles per journal. The Indian Institute of Science and Indian Institute of Technology, Delhi contribute most of their articles to foreign open access journals whereas the other three institutes contribute most of their articles to Indian open access journals. _Current Science_ is the most popular open access journal amongst all the institutions with 189 (13.82%) open access articles in it. Second comes _Acta Crystallographica Section E Structure Reports Online_ with 125 (19.14%) open access articles. _Neurology India_, and _Pramana Journal of Physics_ are also popular open access journals in the Indian scientific community (Table 8).

<table><caption>

**Table 8: Top open access source journals with number of articles**</caption>

<tbody>

<tr>

<th>Journal title</th>

<th>Country</th>

<th>Starting year</th>

<th>No. of articles</th>

</tr>

<tr>

<td>Current Science</td>

<td>India</td>

<td>1932</td>

<td>189</td>

</tr>

<tr>

<td>Acta Crystallographica; Section E Structure Reports Online</td>

<td>UK</td>

<td>2001</td>

<td>125</td>

</tr>

<tr>

<td>Bulletin of Materials Science</td>

<td>India</td>

<td>1979</td>

<td>68</td>

</tr>

<tr>

<td>Indian Pediatrics</td>

<td>India</td>

<td>1997</td>

<td>65</td>

</tr>

<tr>

<td>Neurology India</td>

<td>India</td>

<td>1999</td>

<td>55</td>

</tr>

<tr>

<td>Pramana Journal of Physics</td>

<td>India</td>

<td>1973</td>

<td>49</td>

</tr>

<tr>

<td>Indian Journal of Pathology and Microbiology</td>

<td>India</td>

<td>2006</td>

<td>47</td>

</tr>

<tr>

<td>Indian Journal of Medical Research</td>

<td>India</td>

<td>2003</td>

<td>45</td>

</tr>

<tr>

<td>Indian Journal of Ophthalmology</td>

<td>India</td>

<td>2005</td>

<td>41</td>

</tr>

<tr>

<td>Journal of Chemical Sciences</td>

<td>India</td>

<td>1977</td>

<td>31</td>

</tr>

<tr>

<td>Nucleic Acids Research</td>

<td>UK</td>

<td>1996</td>

<td>31</td>

</tr>

<tr>

<td>Journal of Anaesthesiology Clinical Pharmacology</td>

<td>India</td>

<td>2002</td>

<td>25</td>

</tr>

<tr>

<td>Indian Journal of Cancer</td>

<td>India</td>

<td>2002</td>

<td>22</td>

</tr>

<tr>

<td>Indian Journal of Gastroenterology</td>

<td>India</td>

<td>2004</td>

<td>22</td>

</tr>

<tr>

<td>Proceedings of the Indian Academy of Sciences; Chemical Sciences</td>

<td>India</td>

<td>1977</td>

<td>21</td>

</tr>

<tr>

<td>Electronic Journal of Geotechnical Engineering</td>

<td>USA</td>

<td>1996</td>

<td>19</td>

</tr>

<tr>

<td>Indian Journal of Dermatology Venereology and Leprology</td>

<td>India</td>

<td>1995</td>

<td>19</td>

</tr>

<tr>

<td>Indian Journal of Medical Sciences</td>

<td>India</td>

<td>2002</td>

<td>19</td>

</tr>

<tr>

<td>Journal of Radiation Research</td>

<td>Japan</td>

<td>1999</td>

<td>18</td>

</tr>

<tr>

<td>Indian Journal of Chest Diseases Allied Sciences</td>

<td>India</td>

<td>2000</td>

<td>17</td>

</tr>

<tr>

<td>Journal of Biosciences</td>

<td>India</td>

<td>1998</td>

<td>17</td>

</tr>

</tbody>

</table>

## Conclusion

The present study reveals that only a small portion of research articles of Indian research institutes is published in open access journals with no substantial increase over the period of time. The medical institutions are contributing more of their publications in open access journals and the Engineering institutions the least. This confirms a trend identified in earlier research ([Bhat 2008](#bha08)). The higher number of open access articles appear in Indian journals, although there are more source journals of foreign origin. The study also reveals that only standard, peer reviewed open access journals (such as _Current Science, Acta Crystallographica Section E Structure Reports Online, Bulletin of Materials Science, Indian Pediatrics, Neurology India,_ and _Pramana Journal of Physics_) are used by the Indian scientific community for dissemination of their research findings.

## Limitations and Future Research

Publications in journals not covered by Scopus are not included in the study (as the Scopus database is used for identifying the research publications). Further studies need to be undertaken to ascertain the actual proportion of authors publishing in open access journals along with the underlying motivating factors.

## <a id="author"></a>About the author

Mohammad Hanief Bhat is Senior Librarian at Islamia College of Science & Commerce, Srinagar, India. He received his Masters and M. Phil in Library and Information Science from University of Kashmir, Srinagar, India. He can be contacted at: [mhanief30@yahoo.co.in](mailt:mhanief30@yahoo.co.in)

## References

*   <a id="andt04"></a>Antelman, K. (2004). [Do open-access articles have a greater research impact?](http://www.webcitation.org/5j1oWZDoY) _College & Research Libraries_, **65**(5), 372-382\. Retrieved 26 November, 2007, from http://eprints.rclis.org/archive/00002309/01/do_open_access_CRL.pdf (Archived by WebCite® at http://www.webcitation.org/5j1oWZDoY)
*   <a id="aru08"></a>Arunachalam, S. (2008). Open access to scientific knowledge. _DESIDOC Journal of Library and Information Technology_, **28**(1). 7-14.
*   <a id="bha08"></a>Bhat, M. H. (2008). Open access publishing in Jammu and Kashmir State: an assessment. _Trends in Information Management_ (TRIM), **4**(1), 20-37.
*   <a id="bjo08"></a>Björk, B., Roosr, A., & Lauri, M. (2008). [Global annual volume of peer reviewed scholarly articles and the share available via different open access options.](http://www.webcitation.org/5j1pvuUmN) In _Proceedings ELPUB 2008 Conference on Electronic Publishing, Toronto, Canada, June 2008_. (pp. 178-186), Retrieved 13 December, 2008 from http://elpub.scix.net/data/works/att/178_elpub2008.content.pdf (Archived by WebCite® at http://www.webcitation.org/5j1pvuUmN)
*   <a id="blu06"></a>Bluh, P. (2006). [Open access, legal publishing, and online repositories](http://www.webcitation.org/5j1q0PLzE). _Journal of Law, Medicine & Ethics_, **34**(1), 126-130\. Retrieved 20 March, 2008 from http://digitalcommons.law.umaryland.edu/cgi/viewcontent.cgi?article=1048&context=fac_pubs (Archived by WebCite® at http://www.webcitation.org/5j1q0PLzE)
*   <a id="cha04"></a>Chan, L. (2004). [Supporting and enhancing scholarship in the digital age: the role of open-access institutional repositories](http://www.webcitation.org/5j1zydmsY). _Canadian Journal of Communication_, **29**, 277-300\. Retrieved 28 November, 2007 from http://eprints.rclis.org/archive/00002590/01/Chan_CJC_IR.pdf (Archived by WebCite® at http://www.webcitation.org/5j1zydmsY)
*   <a id="cha05"></a>Chan, L., & Costa, S. (2005). Participation in the global knowledge commons: challenges and opportunities for research dissemination in developing countries. _New Library World_, **106**(3/4), 141-163\.
*   <a id="das05"></a>Das, A.K., Sen, B.K., & Dutta, C. (2005). [Collection development in digital information repositories in India](http://www.webcitation.org/5j20K5MyS). _Vishwabharat@TDIL_, No. 17, 91-96\. Retrieved 26 November, 2007, from http://eprints.rclis.org/archive/00005682/01/Das_Sen_Dutta_TDIL_05.pdf (Archived by WebCite® at http://www.webcitation.org/5j20K5MyS)
*   <a id="das07"></a>Das, A.K., Sen, B.K., & Dutta, C. (2007). _[ETD policies, strategies and initiatives in India: a critical appraisal](http://www.webcitation.org/5j20PUG8R)_. Paper presented at the 10th International Symposium on Electronic Theses and Dissertations, Uppsala, Sweden, 13-16 June 2007\. Retrieved 29 November, 2007, from http://eprints.rclis.org/archive/00010657/01/Das_Dutta_Sen_India__ETD_2007_Paper.pdf (Archived by WebCite® at http://www.webcitation.org/5j20PUG8R)
*   <a id="eys06"></a>Eysenbach, G. (2006). [Citation advantage of open access articles.](http://www.webcitation.org/5j20uVZhX) _PLOS Biology_, **4**(5), 0692-0608\. Retrieved 27 November, 2007 from http://www.plosbiology.org/article/info:doi/10.1371/journal.pbio.0040157 (Archived by WebCite® at http://www.webcitation.org/5j20uVZhX)
*   <a id="fal04"></a>Falk, H. (2004). [Open access gains momentum.](http://www.webcitation.org/5j216pf8u) _The Electronic Library_, **22**(6), 527-530\. Retrieved 14 August, 2009 from http://www.scribd.com/doc/6645366/Open-Access-Gains-Momentum (Archived by WebCite® at http://www.webcitation.org/5j216pf8u)
*   <a id="fer06"></a>Fernandez, L. (2006). [Open access initiatives in India-an evaluation.](http://www.webcitation.org/5j21aDoUf) _Partnership: the Canadian Journal of Library and Information Practice and Research_, **1**(1), 1-22\. Retrieved 22 November, 2007, from http://journal.lib.uoguelph.ca/index.php/perj/article/view/110/172 (Archived by WebCite® at http://www.webcitation.org/5j21aDoUf)
*   <a id="hai07"></a>Haider, J. (2007). Of the rich and the poor and other curious minds: on open access and "development". _Aslib Proceedings_, **59**(4/5), 449-461\.
*   <a id="haj05a"></a>Hajjem, C., Harnad, S., & Gingras, Y. (2005). [Ten-year cross-disciplinary comparison of the growth of open access and how it increases research citation impact.](http://arxiv.org/ftp/cs/papers/0606/0606079.pdf) _IEEE Data Engineering Bulletin_, **28**(4), 39-47\. Retrieved 26 November, 2007 from http://arxiv.org/ftp/cs/papers/0606/0606079.pdf
*   <a id="haj05b"></a>Hajjem, C., Gingras, Y., Brody, T., Carr, L., & Harnad, S. (2005). _[Open access to research increases citation impact.](http://www.webcitation.org/5j2eegNrg)_ Montréal. Canada: Université du Québec à Montréal, Institut des sciences cognitives. Retrieved 8 December, 2007 from http://eprints.ecs.soton.ac.uk/11687/ (Archived by WebCite® at http://www.webcitation.org/5j2eegNrg)
*   <a id="har04a"></a>Harnad, S., & Brody, T. (2004). [Comparing the impact of open access (OA) vs. non-OA articles in the same journals.](http://www.webcitation.org/5j2iP3QEw) _D-lib Magazine_, **10**(6). Retrieved 15 August, 2007 from http://www.dlib.org/dlib/june04/harnad/06harnad.html (Archived by WebCite® at http://www.webcitation.org/5j2iP3QEw)
*   <a id="har04b"></a>Harnad, S., Brody, T., Vallieres, F., Carr, L., Hitchock, S., Gingras,Y. _et al._. (2004). [The access/impact problem and the green and gold roads to open access](http://www.webcitation.org/5j2iafkQh). _Serials Review_, **30**(4), 310-314\. Retrieved 26 November, 2007 from http://eprints.ecs.soton.ac.uk/10209/1/impact.html (Archived by WebCite® at http://www.webcitation.org/5j2iafkQh)
*   <a id="her08"></a>Herb, U., & Muller, M. (2008). [The long and winding road: institutional and disciplinary repository at Saarland University and State Library.](http://www.webcitation.org/5j2iqmfR5) _OCLC Systems & Services_, **24**(1). Retrieved 15 August, 2009 from http://scidok.sulb.uni-saarland.de/volltexte/2008/1445/pdf/fertig_v1.pdf (Archived by WebCite® at http://www.webcitation.org/5j2iqmfR5)
*   <a id="kin07"></a>Kingsley, D. (2007). [The journal is dead, long live the journal.](http://www.webcitation.org/5j2j0nFIq) _On the Horizon_, **15**(4). Retrieved 15 August, 2009 from http://dspace.anu.edu.au/bitstream/1885/44522/1/OnTheHorizon%20Submitted.pdf (Archived by WebCite® at http://www.webcitation.org/5j2j0nFIq)
*   <a id="kri08"></a>Krishnamurthy, M. (2008). Open access, open source and digital libraries: a current trend in university libraries around the world. _Program: electronic library and information systems_, **42**(1), 48-55.
*   <a id="law01"></a>Lawrence, S. (2001). [Online or invisible?](http://www.webcitation.org/5j7fVIChu) _Nature_, **411**(6837). 521\. Retrieved 18 August, 2009 from http://citeseer.ist.psu.edu/online-nature01/ (Archived by WebCite® at http://www.webcitation.org/5j7fVIChu)
*   <a id="mcc06"></a>McCulloch, E. (2006). [Taking stock of open access: progress and issues.](http://www.webcitation.org/5j2jP52de) _Library Review_, **55**(6), 337-343\. Retrieved 15 August, 2009 from http://strathprints.strath.ac.uk/2325/1/strathprints002325.pdf (Archived by WebCite® at http://www.webcitation.org/5j2jP52de)
*   <a id="mar06"></a>Mark, T., & Shearer, K. (2006). [_Institutional repositories: a review of content recruitment strategies._](http://www.webcitation.org/5j2jqMAvL) Paper presented at the World Library and Information Congress: 72nd IFLA General Conference and Council, Seoul, Korea. Retrieved 26 November, 2007 from http://www.ifla.org/IV/ifla72/papers/155-Mark_Shearer-en.pdf (Archived by WebCite® at http://www.webcitation.org/5j2jqMAvL)
*   <a id="meh07"></a>Meho, L. I. & Yang, K. (2007). [A new era in citation and bibliometric analysis: Web of science, Scopus and Google Scholar.](http://arxiv.org/ftp/cs/papers/0612/0612132.pdf) _Journal of American Society for Information Science and Technology_, **58**(13), 2105-2125\. Retrieved December 8, 2007 from http://arxiv.org/ftp/cs/papers/0612/0612132.pdf
*   <a id="nor08"></a>Norris, M., Oppenheim, C., & Rowland, F. (2008). The citation advantage of open-access articles. _Journal of the American Society of Information Science and Technology_, **59**(12), 1963-1972
*   <a id="ram04"></a>Ramachandran, P. V., & Scaria, V. (2004). [Open access publishing in the developing world: making a difference.](http://www.webcitation.org/5j35by0VQ) _Journal of Orthopaedics_, **1**(1). Retrieved June 17, 2008, from http://jortho.org/2004/1/1/e1/index.htm (Archived by WebCite® at http://www.webcitation.org/5j35by0VQ)
*   <a id="row05"></a>Rowlands, I., & Nicholas, D. (2005). Scholarly communication in the digital environment: the 2005 survey of journal author behaviour and attitudes. _Aslib Proceedings_, **57**(6), 481-497\.
*   <a id="ylo05"></a>Ylotis, K. (2005). The open access initiative: a new paradigm for Scholarly Communications. _Information Technology and Libraries_, **24**(4), 157-162\.
*   <a id="zha07"></a>Zhang, S. L. (2007). The flavors of open access. _OCLC Systems & Services_, **23**(3), 229-234\.