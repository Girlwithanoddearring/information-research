#### vol. 14 no. 3, September, 2009

* * *

# Resúmenes en Español / Abstracts in Spanish

#### [La distribución de información de noticias a través del etiquetado social: un examen de noticias compartidas en el sitio web Delicious](paper405.html)  

Deborah Soun Chung

> **Introducción**. Este estudio examinó la selección y como se comparten las noticias de Delicious, un sitio popular de etiquetado social para identificar las fuentes de información de noticias consultadas frecuentemente y la temática de las noticias.  
> **Método**. Focalizando fuentes específicas de EE.UU. a través de la exploración inicial de URLs mediante ordenador, empleamos el análisis de contenido para analizar los temas y fuentes de las noticias que no fueron clasificadas a través del método de exploración inicial.  
> **Análisis**. Después de examinar las frecuencias y porcentajes de las variables, se empleó el análisis cualitativo para evaluar las relaciones entre las fuentes y temas de la noticia.  
> **Resultados**. Los resultados documentan la naturaleza diversa de las noticias de los canales de los medios de comunicación tradicionales y nuevos. Las fuentes de los medios de comunicación sociales, principalmente los blogs, están creciendo como una fuente principal de noticias. No se usaron exclusivamente los medios de comunicación tradicionales prominentes, y el volumen total de páginas no categorizadas sugiere que los buscadores de noticias en línea consideren como fuentes de noticias las páginas Web alternativas encontradas durante la navegación casual.  
> **Conclusiones**. Lo que las audiencias de las noticias en línea consideran como noticias está aumentando en amplitud y complejidad, con fuentes sin clasificar dominando las noticias etiquetadas. Las nuevas fuentes de información parecen recuperar los temas de la noticia deficientes en los medios de comunicación principales.

#### [El perfil del profesional de la información: un análisis de las ofertas de trabajo brasileñas en Internet](paper407.html)  

Miriam Vieira da Cunha

> **Introducción**. Informe de un estudio para descubrir y describir las ofertas de trabajo para los profesionales de la información disponibles en línea en sitios específicos y listas de discusión entre enero de 2005 y febrero de 2008.  
> **Método**. El estudio usa la técnica de análisis de contenido de Bardin y los siguientes criterios de análisis: fuente de información, tipo institucional, tipo profesional, situación, cualificaciones, descripción del trabajo, experiencia y requisitos del idioma extranjero.  
> **Análisis**. Los resultados se comparan con estudios nacionales e internacionales sobre los perfiles y habilidades de los profesionales de la información.  
> **Resultados**. Los resultados muestran que la expansión en Brasil del campo de la información y sus profesionales todavía es lenta y esto puede verse en los resultados presentados.  
> **Conclusiones**. El perfil en las ofertas de trabajo analizadas para un profesional de la información tipo es el de un bibliotecario, con un grado en estudios de biblioteconomía, para realizar funciones técnicas y de gestión en una institución privada de la ciudad de Sao Paulo.

#### [El Diferencias categóricas y de especificidad entre las etiquetas de usuario y los términos de consulta de búsqueda de imágenes. Un análisis de etiquetas de Flickr y consultas de búsqueda Web de imágenes.](paper408.html)  

EunKyung Chung and JungWon Yoon

> **Introducción**. El propósito de este estudio es comparar las características y rasgos de las etiquetas proporcionadas por usuario y los términos de búsquedas en el sitio web Flickr en términos de categorías de significados pictóricos y el grado de especificidad de término.  
> **Método**. Este estudio se centra en las comparaciones entre las etiquetas y consultas de búsqueda usando los esquemas de categorización de Shatford y el nivel de especificidad basado en la teoría de nivel básico.  
> **Análisis**. Se realizaron con los datos las distribuciones de frecuencia y los análisis chi-cuadrado. Los resultados de los análisis estadísticos demostraron que había diferencias significativas en las categorías entre las etiquetas y las diferentes fases de términos de consulta de búsqueda. Las distribuciones globales de los niveles de especificidad de término también tenían un patrón similar en etiquetas y términos de consulta de búsqueda, pero estadísticamente se encontraron diferencias significativas entre etiquetas y términos de consulta de búsqueda.  
> **Resultados**. Los resultados de este estudio demostraron que las etiquetas de Flickr tienen sus propios rasgos únicos comparados con las consultas de los usuarios para la búsqueda de imágenes. Los resultados sugieren que generalmente no deben aplicarse las etiquetas de imagen a otras colecciones de imágenes aunque hayan sido consideradas como datos útiles en el desarrollo de un sistema de indización centrado en el usuario.  
> **Conclusiones**. Al utilizar las etiquetas de usuario para los sistemas de indización centrados en el usuario, es deseable considerar las funciones y las tareas de los usuarios aplicadas en las etiquetas en lugar de depender meramente de los rasgos estadísticos obtenidos del análisis de etiquetas.

#### [La publicación en acceso abierto en las principales instituciones indias de investigación](paper409.html)  

Mohammad Hanief Bhat

> **Introducción**. La publicación de resultados de investigación en revistas de acceso abierto es un medio para mejorar la visibilidad y por consiguiente aumentar el impacto de las publicaciones. Este estudio proporciona una apreciación global de la publicación en acceso abierto en los principales institutos de investigación de la India.  
> **Método**. Se determinó mediante Scopus la producción de cada institución entre 2003 y 2007 y el nombre de la revista fuente junto con el número de publicaciones registradas. Se buscaron los 4232 títulos de revistas en el Directorio de Revistas de Acceso Abierto (Directory of Open Access Journals) y entonces se uso el sistema de búsqueda de Google para localizar las revistas que están accesibles en abierto.  
> **Análisis**. Los datos se clasificaron y analizaron de una manera sistemática para revelar los resultados de acuerdo con los objetivos deseados.  
> **Resultados**. Los 17516 artículos de investigación fueron aportados por las cinco instituciones y aparecen en 4232 revistas. El Instituto Indio de Ciencia publica el 8.26% de sus resultados de investigación en revistas de acceso abierto, el Instituto de India de Ciencias Médicas el 19.37%, el Centro de Investigación Atómico Baba el 4.84%, el Instituto Indio de Tecnología Delhi el 3.04% y el Instituto Indio de Tecnología Kharagpur el 3.26%.  
> **Conclusiones**. El estudio revela que una pequeña porción de publicaciones de investigación de institutos de la investigación indios se publica en los revistas de acceso abierto, la mayoría en revistas de origen indio. Las instituciones médicas están contribuyendo más con sus publicaciones a revistas de acceso abierto en comparación con otras instituciones.

#### [La práctica del blogging académico como género situacional: un marco analítico basado en la teoría de género](paper410.html)  

Sara Kjellberg

> **Introducción**. Examina cómo un marco analítico de análisis de género situacional puede usarse para estudiar cómo se construyen y usan los blogs de investigación como herramientas en la comunicación académica.  
> **Método**. Se extrajo un marco de las teorías de investigación de género consistentes en cuatro conceptos: objetivo, forma, contenido y contexto. Se usó el término género situacional para centrarse en las prácticas sociales. El contexto se elaboró posteriormente combinando la comunidad del discurso con el concepto de culturas epistémicas.  
> **Análisis**. El propósito principal era perfilar, discutir y probar el marco. Se seleccionaron y usaron tres blogs de investigadores del campo de la física para probar cómo este marco operaba.  
> **Resultados**. Los resultados preliminares mostraron que los blogs académicos podrían aproximarse como un género situacional que es parte de la práctica de comunicación académica y que este marco puede usarse para analizar las características sociales y técnicas de los blogs. Sin embargo, el marco tiene algunas restricciones que tienen que ser solucionadas. Los cuatro conceptos se entrelazan y pueden beneficiarse de la aplicación de varios métodos diferentes.  
> **Conclusiones**. El marco propuesto es útil como una herramienta para el análisis de blogs de investigación y para hacer visible su carácter socio-técnico. Los géneros existentes en una cultura epistémica particular, o las diferencias entre culturas epistémicas diferentes, son cuestiones que podrían estudiarse con este marco.

#### [Necesidades percibidas de información y disponibilidad: resultados de un estudio sobre granjeros de pequeñas lecherías en Mongolia Interior](paper411.html)  

Yuanfeng Zhao y Ruijin Zhang

> **Introducción**. El propósito de este estudio era determinar las necesidades percibidas de diferentes tipos de información, por los granjeros de pequeñas lecherías en la parte central de la Región Autónoma de Mongolia Interior y la efectividad con la que se proporciona y usa.  
> **Método**. Se realizó una encuesta en cuatro pueblos en dos regiones clave productoras de leche en esta parte de Mongolia Interior. Se recibieron las respuestas estructuradas de 167 granjeros de pequeñas lecherías.  
> **Análisis**. Se realizó un análisis descriptivo, basado en las comparaciones entre los pueblos mediante un análisis desglosado según niveles educativos y de ingresos.  
> **Resultados**. La mayoría de los granjeros de pequeñas lecherías consideraron de más uso para ellos la información sobre política y comercialización que la información sobre tecnologías. Aquéllos que tenían niveles educativos superiores o que tenían una proporción mayor de ganancias por sus ingresos de la empresa de lechería tenían necesidades mayores de información sobre mercados y tecnologías. La televisión era el cauce principal por el que los granjeros recibieron su información, seguido por la información de los funcionarios gubernamentales del pueblo, los vecinos y las estaciones lecheras, con muy poca información que viene de las radios, periódicos e Internet.  
> **Conclusiones**. Los granjeros en pequeña escala del área importante de producción de leche de Mongolia Interior todavía operan sus negocios en base a una información de industria limitada pero la mayoría ha reconocido la necesidad de mejorar la disponibilidad de información precisa y parecen favorables a compartir el costo de proporcionarla. Parece ser una oportunidad para que el gobierno y las organizaciones privadas trabajen juntos en el desarrollo de sistemas de diseminación de información avanzados para los pequeños productores de leche en Mongolia Interior.

#### [Una profesión en transición: hacia el desarrollo y aplicación de normas para la gestión de recursos visuales. Parte A - punto de vista de la organización](paper412.html)  

Hemelata Iyer

> **Introducción.** Esta investigación forma parte de un proyecto más amplio cuyo objetivo es mejorar la educación y la formación, para proporcionar a la comunidad bibliotecaria con la información necesaria, el apoyo para el desarrollo de programas profesionales de recursos visuales.  
> **Método.** Se llevó a cabo un análisis del contenido de 394 anuncios de trabajo en el campo de recursos visuales enviados durante los últimos cinco años. .  
> **Análisis.** Fu empleado el análisis de contenido, utilizando dos categorías principales de análisis: los medios de información en los puestos de trabajo existentes y los requisitos de calificación para los puestos de trabajo. .  
> **Resultados.** En general, la digitalización, la gestión y catalogación se destacan como las habilidades más deseadas para los profesionales de recursos visuales. La gestión de activos digitales y el conocimiento de las tecnologías digitales son mencionadas específicamente, tales como: la comunicación eficaz y la capacidad de gestión de proyectos. Las áreas de conocimiento incluyen las normas y prácticas pertinentes, la catalogación y la teoría de la clasificación, las normas y prácticas en digitalización, derechos y permisos, la gestión de documentos, fotografía, conservación, exhibición y bases de datos relacionales y objeto. .  
> **Conclusión.** La gestión de recursos visuales parece ser un área emergente de conocimiento y experiencia que necesita ser abordado de manera sistemática. La profesión se está moviendo de los restauradores y bibliotecarios de diapositivas a un campo que abarca una extensa gama de habilidades aplicables en una amplia gama de entornos.

#### Traducciones realizadas por [José Vicente Rodríguez](mailto:jovi@um.es) y [Pedro Díaz](mailto:diazor@um.es), Universidad de Murcia, España.

##### Last updated 10 September, 2009