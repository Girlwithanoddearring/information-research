#### vol. 14 no. 3, September, 2009

* * *

# Distribution of news information through social bookmarking: an examination of shared stories in the _Delicious_ Website

#### [Deborah Soun Chung](#authors)  
School of Journalism and Telecommunications,  
University of Kentucky, Lexington,  
KY 40506,  
USA

#### [Kwan Yi](#authors)  
School of Library and Information Science,  
University of Kentucky, Lexington,  
KY 40506,  
USA

#### Abstract

> **Introduction.** This study examined the selection and sharing of news stories from **Delicious**, a popular social bookmarking site, in order to identify the most frequently consulted news information sources and news topics.  
> **Method.** Targeting US-specific sources through initial computer screening of URLs, we employed content analysis to further analyse story topics and sources that were unclassified through the initial computer screening method.  
> **Analysis.** After examining frequencies and percentages of the variables, a qualitative analysis was employed to assess relationships between story sources and story topics.  
> **Results.** Findings document the diverse nature of stories from both traditional and new media channels. Social media sources, primarily blogs, are growing as a major news source. Prominent traditional news media were not exclusively used, and the sheer volume of uncategorized pages suggests that online news seekers consider alternative Web pages encountered during casual navigation as news sources.  
> **Conclusions.** What online news audiences consider to be news is becoming increasingly broad and complex with unclassified sources dominating tagged stories. New information sources appear to make up for deficient story topics in the mainstream media.

## Introduction

New information and communication technologies, such as social bookmarking and blogs, allow traditionally passive news audiences to become active information consumers. Online news publications can now provide audiences with tools to participate in the news consumption process more actively.

The popularity of interactive tools are evidence that online information consumers welcome the potential of these technologies, which allow greater opportunities for socialization and information-sharing online. These interactive tools have been described as facilitating a new form of communication, which shifts control of information to the audience. They allow many online news audiences to express views and also engage in meaningful conversation, thereby empowering them to become content creators and have an increased sense of agency in their news consumption experiences. Some have described blogs, for example, as emancipatory tools for communication with the transformative potential of challenging traditional hegemonic notions of information delivery ([Herring _et al._ 2004](#her04)).

Mainstream online news sites are now linking to user-centred news sites, social bookmarking sites and social networking sites, such as _[Digg](http://digg.com/)_, _[Delicious](http://delicious.com/)_ and _[Facebook](http://www.facebook.com/)_ respectively, which offer options for news consumers easily to share news stories of interest with other users. Of particular significance in such interactions is the prominent role that the news audience potentially plays in judging the value of information, a critical shift in directional control of information by the traditionally passive news audiences. That is, in the past media outlets have provided news audiences with a top down, one-way news consumption experience, with distinct roles for senders of information and information receivers ([Castells 2007](#cas07), [McMillan 1999](#mcm99), [Nip 2006](#nip06), [Picone 2007](#pic07), [Singer 2005](#sin05)). However, online news media, empowered by the interactive and participatory characteristics of new collaborative tools and services available on the Web, provide unique opportunities for the expression of ideas and facilitation of interpersonal communication.

This study examines one particular type of user-centred news selection and filtering activity, social bookmarking through _Delicious_, to identify the informational pursuits of news audiences in news selection and evaluation among sources of U.S. origin. Specifically, 1) Which news information sources do audiences rely on? 2) Which news information topics are they interested in? and 3) What is the relationship between the two? The goal of this study is to provide an analysis of audiences' filtering and selection of stories that are labelled as news. The findings will help us to understand the active role of news audiences in the distribution and consumption of online news in the context of interactive and social information networks.

## Traditional and new communication models

Traditionally, media outlets have functioned as the gatekeepers of information for society ([Matheson 2004](#mat04), [Singer 2005](#sin05)). Newspapers and broadcast radio present a selection of the day's events to the public, and each day's news presents a finite perspective of what has happened in the community and around the world ([McQuail 1994](#mcq94), [Singer 2001](#sin01)). Traditional media follow a one-to-many communication model where one producer attempts to reach as many readers or viewers as possible ([Castells 2007](#cas07), [Nip 2006](#nip06), [Picone 2007](#pic07), [Singer 2005](#sin05)). The senders and the receivers of the information are distinct ([McMillan 1999](#mcm99)). The dominant mass communication paradigm is based on Shannon and Weaver's model ([1949](#sha49)), which describes communication channels for information and their technical efficiency. This model suggests that there is a sequential process for information, which starts with a communication source that produces or selects a message that is subsequently transmitted by a signal through a channel that carries the signal to a receiver who then transforms the signal back to a message for the destination. Rogers refers to this model as 'the single most important turning point in the history of communication science' ([1986: 86-7](#rog86)). Mass communication research has focused on this linear communication model for decades.

However, the Internet, with its interactive features, allows for various types of communication, including one-to-one communication, such as e-mail; one-to-few or many and few-to-few communication, such as chats; and even many-to-many communication, such as electronic bulletin boards ([Morris and Ogan 1996](#mor96)). The Internet allows the audience to be active, prompting them to make choices. In addition, there is no single source and no single destination ([Ha and James 1998](#hal98)). These transformations have significant implications for traditional mass communication theories ([Delwiche 2005](#del05), [Roberts _et al._ 2002](#rob02), [Thelwall _et al._ 2007](#the07)). For example, mass media have acted as gatekeepers of information, selecting stories for audiences to consume and placing prominence on news that is deemed more relevant. In doing so, the press suggests which issues and events are deserving of attention. Graber ([1988](#gra88)) found newspaper readers follow cues provided by editors, such as article location, size of headlines and images and story length, to guide their decisions of what to read. In contrast, with online presentation of news, the mainstream media may no longer solely determine the importance of issues. Althaus and Tewksbury ([2002](#alt02)) state the relative flexibility of online news formats and the unsuitable conventional story prominence cues for Web newspapers are relevant to studying the changing agenda-setting processes online. In their study, Althaus and Tewksbury found that people exposed to _The New York Times_ for five days adjusted their agenda to the issues suggested by the newspaper. They also found the print version of the _Times_ to be more influential than the online version in guiding the audience towards a particular agenda. Delwiche ([2005](#del05)) also found support that bloggers construct an alternative agenda within the blogosphere. That is, aside from stories about the Iraq war, Delwiche found little correspondence on other issues between indicators of the media and blog agenda.

Thus, newer media tools can potentially undermine the elite dominance of the mainstream press and address media critics' concerns about traditional mass media producing messages independently of the news audiences they serve ([Habermas 1962](#hab62), [Schultz 1999](#sch99), [Schudson 2005](#sch05), [Wall 2005](#wal05)). The new capabilities of the Internet, in principle, can provide the audience with more opportunities to participate in modifying the form and content of the news. With the rise of new communication technologies built with qualities such as interactivity and asynchronicity, there has been even greater enthusiasm about the audience's role as information producers through mass communication channels. Feedback from the news audience has not been entirely discounted, as is evident in traditional audience research on radio markets ([Beville 1988](#bev88), [Buzzard 1990](#buz90)), talk radio ([Hofstetter and Gianos 1997](#hof97), [Hollander 1997](#hol97), [Squires 2000](#squ00)) and letters-to-the-editor ([Renfro 1979](#ren79), [Richardson and Franklin 2004](#ric04)). Yet the means for audiences to take on an active role in their news consumption experiences have been limited in opportunity and accessibility thus far.

## Online news publications

The exponential growth of online news publications and the subsequent popularity and mass consumption of blogs have demonstrated that a proportion of news audiences are indeed open to increased user control and welcome interactive communication opportunities. Such technological changes have challenged the journalism profession to rethink its practices of news delivery and presentation ([Chung _et al._ 2007](#chu07a)). Furthermore, it has transformed some audiences' reception of the news. The news profession is undergoing massive change and is evolving by the minute. Users are provided with an array of features that allow them to actively choose information to their liking, tailor a news site with favourite topics, post comments on news articles or share pictures and stories with other users. Such features are part of a larger collection of tools called social media, which have contributed to the citizen and community journalism movements and participatory communication. Wikis, blogs and podcasting are just a few tools that have emerged from this phenomenon ([Bowman and Willis 2005](#bow05)).

The Internet has been credited with giving audiences more engaging news experiences powered by the implementation of various interactive features and social networking tools. Research suggests that news publications were initially slow to respond to or adopt such technologies ([Chan-Olmsted and Park 2000](#cha00), [Kenney _et al._ 2000](#ken00), [Massey and Levy 1999](#mas99), [Rosenberry 2005](#ros05), [Schultz 1999](#sch99), [Ye and Li 2006](#yex06)) with scholars identifying the problems facing news producers in implementing features such as forums, 'questions and answers' and blogs ([Chung 2007](#chu07a), [Thurman 2008](#thu08)). However, the news industry appears to have gradually warmed to employing interactive tools and has become more sophisticated in its use of them over time ([Chung 2004](#chu04), [Greer and Mensing 2006](#gre06), [Salwen 2005](#sal05)).

While the progression of online news has been documented, whether this significant transformation of news delivery and presentation has influenced what online audiences consume has not been systematically examined. Social bookmarking, in particular, allows audiences to choose news of their liking and to store or share it with other users. With increased control over information online, news consumers are able to select news to their liking over news filtered through mainstream media and may potentially propagate customized news preferences. Thus, an examination into the potentially diversified news sources offered to news audiences through social bookmarking activities warrants investigation. Research into social networking and user-created content is increasing, but social bookmarking activities appear to be studied exclusively by the information science discipline from a technical perspective rather than from the social sciences or mass communication perspective. However, it is important to note that _Delicious_ is included as a story sharing tool in many major news publications, including the online publications for _[CNN](http://www.cnn.com/)_, _[Chicago Tribune](http://www.chicagotribune.com/)_, _[Los Angeles Times](http://www.latimes.com/)_, _[The Washington Post](http://www.washingtonpost.com/)_ and _[The Wall Street Journal](http://online.wsj.com/)_ among many others.

## Social bookmarking

Social bookmarking sites allow users to store and organize Web resources and share them with other participants through the Web, rather than keeping them in an individual's Web browser as personal bookmarks. Recently, such social bookmarking activity has become particularly popular in storing and distributing online news information. Examples of social bookmarking applications include: _[Delicious](http://delicious.com)_ for Web resources, [_Flickr_](http://www.flickr.com) for photographs, [_Technorati_](http://technorati.com) for blogs, and [_Connotea_](http://www.connotea.org) for bibliographic data. Among them, _Delicious_ is believed to have started the social bookmarking trend.

While social bookmarking features have become a popular trend in online news publications, there is limited research examining exactly how news audiences are using social bookmarking tools. The Project for Excellence in Journalism ([2007](#pej07)) reports one such study, which examined the top ten stories featured on the homepages of three prominent user-news sites (_Delicious_, [_Reddit_](http://www.reddit.com/), and _Digg_) during a random week. A total of 644 stories was coded and analysed. The study examined what individuals do with the power to be information disseminators and how the events and issues users choose differ from mainstream news. It reports that the news agenda of the three user-sites was significantly different from the mainstream media. Many of the stories that appeared in the three sites did not appear as top stories in the mainstream press. The study found 70% of the stories on user sites came from blogs or other social media, such as [_YouTube_](https://youtube.com).

In particular the study found that _Delicious_ had the most fragmented mix of stories and the least overlap with the mainstream news topics (3%). It drew more from blogs and non-news information sites than the other two user-news sites. _Delicious_ and _Digg_ (about 40%) had a stronger concentration on technology related topics than _Reddit_ (22%).

This study concludes that the news agenda for user-news sites was more diverse in terms of story topics yet also more fragmented in terms of story sources. However, the authors state this does not necessarily mean that users disapprove of the mainstream media but that user-sites may act as supplemental news sources.

Johnson and Yang ([2008](#joh08)) conducted a content analysis of the characteristics of linked news items and titles and summaries of news items by _Digg_ users. They examined these features against characteristics of newsworthiness, including deviance, social significance and complexity. They found deviance and social significance to be predictors of popularity of news titles and summaries while complexity was not. They conclude that users are attracted to stimulating content, such as odd or interesting items, or arts and/or entertainment and/or technology that take less cognitive processing.

## Key terms

The following section defines key concepts central to social bookmarking.

A _tag_ refers to keywords assigned to or associated with an information resource. Multiple keywords are generally allowed as tags for a single resource. However, acceptable types and formats of tags are subject to the rules defined in systems in which tags are used. For example, in _Delicious_, a phrase is not allowed as a single tag. That is, a phrase is treated as a number of individual words, each of which would be a tag.

_Tagging_ refers to an action or practice of assigning keywords or labels to information resources, such as digital resources or items, with the aim of organizing, accessing and retrieving information later. User tagging refers to tagging activity by the general public or users rather than by professionals.

_Collaborative tagging_, or social tagging, is the practice of tagging open to the public so that anyone can participate in tagging activities through systems (called collaborative tagging systems) available on the Web.

_Folksonomy_ refers to the collection of all tags created and assigned through a collaborative tagging system. The word _folksonomy_ is a blend of the words _folk_ and _taxonomy_ and stands for the outcome of a collaborative tagging system as well as a conceptual structure of user vocabularies. A folksonomy is often referred to and displayed as a cloud of tags in collaborative tagging systems. Research on the functional and linguistic aspects of tags ([Spiteri 2007](#spi07), [Yi and Chan in press](#yik09)), tags as indexing terms ([Kipp and Campbell 2006](#kip06), [Spiteri 2007](#spi07), [Yi 2008a](#yik08a) and [2008b](#yik08b)) and analyses of structural and dynamic aspects of tagging systems, such as user activity and tag frequencies ([Golder and Huberman 2006](#gol06)) have been examined.

## Research questions

For this project, we targeted _Delicious_ for several reasons. First, it is the oldest and one of the most widely used social bookmarking Websites ([Boswell 2009](#bos09)). It is continuously growing at a rapid pace with the size of the site having more than tripled over a 12-month period in 2007 ([Arrington 2007](#arr07)). In 2006 it was reported to have more than three million users ([Gilbertson 2006](#gil06)). As of November 2008, it had grown to include 5,300,000 registered users and over 180 million distinct URLs ([_Delicious_ blog, Nov. 2008](#del08)).

Secondly, _Delicious_ has been frequently used in the study of social tagging ([Golder and Huberman 2006](#gol06), [Kipp and Campbell 2006](#kip06), [Yi 2008a](#yik08a), [2008b](#yik08b) and [Yi and Chan in press](#yik09)). Most importantly, _Delicious_ is a unique social bookmarking site in that it supports users' tagging activities; that is, users can assign tags to general types of online resources, including news articles and Web pages and save them to share with others. Other social bookmarking sites, such as _Reddit_ and [Stumbleupon](http://www.stumbleupon.com/) support user voting for selected resources instead of tagging ([Ives 2008](#ive08)). An online resource reports that "Del.icio.us has 1.1 million unique visitors a month from the U.S. and the audience is male (53%) and about 47% of them are over 45" ([Wilson 2008](#wil08)). _Delicious_ registered users do not have a detailed user profile as found in social networking sites, such as _Facebook_ or _[MySpace](http://www.myspace.com/)_.

As an extension of the study from the Project for Excellence in Journalism ([2007](#pej07)), this study examines all Web pages tagged with the word _news_ over a more extensive time-frame with detailed analysis of information sources. Based on the above review of literature and the limited research available about news audiences and social bookmarking activities, we propose the following three research questions:

RQ1: _What are the most frequently tagged and shared news sources in _Delicious_ among U.S.-specific sources?_  
RQ2: _What are the most frequently tagged and shared news story topics in _Delicious_ among U.S.-specific sources?_  
RQ3: _Is there a relationship between most frequently tagged and shared news sources and story topics in _Delicious_?_

We are particularly interested in sources because they serve as the basic building blocks of information in stories and have the power to shape the news ([Gans 1979](#gan79), [Sigal 1973](#sig73)). Thus, the media's power derives from the ability to amplify views from sources. Transforming communication paradigms also raise questions about where audiences may turn to for certain topics of news. The State of the News Media reports ([Project for Excellence in Journalism 2004](#pej04), [2005](#pej05), [2006](#pej06), and [2008](#pej08), [Pew Project for Excellence in Journalism 2009](#pew09)) illustrate that over the years, newspapers and network television channels have consistently featured government, foreign and military stories most prominently with very littler coverage of lifestyle, business and science-related topics. Magazines also featured stories on national affairs and international issues but featured culture and business related stories more prominently than newspapers and network broadcast channels. Research on source credibility shows that newspapers and television have generally been considered the most credible sources of news ([Abel and Wirth 1977](#abe77), [Flanagin and Metzger 2000](#fla00), [Gaziano and McGrath 1986](#gaz86), [Pew Research center for the People and the Press 2004](#pew04)).

However, increasingly, online news is becoming a major source for information as many are now serious publications incorporating various story-telling techniques and providing various opportunities for audience engagement ([Li 2006](#lix06)). Johnson and Kaye ([1998](#joh98)) found that Internet news is viewed as credible or even more credible than traditional news media. Kiousis ([2001](#kio01)) found that people perceived online news more credible than television news. Given the interactive nature of online media, the topics that audiences read about and turn to may be determined by individuals' selections in addition to the perceived credibility of the source. Thus, the relationship between source and topic continue to be a significant issue to explore with increased audience selectivity and specialization of news outlets.

Findings from the analysis will help explain the informational activities of active news audiences using collaborative media online and also report on the differences from news audiences using traditional news media.

## Method

In the last two weeks of January 2008, Web pages tagged with the word _news_ were collected in _Delicious_. In order to minimize the collection of duplicate Web pages, we captured the data on two days over a one-week interval from the URL `http://_Delicious_.com/search?context=all&p=news&lc=*` where * is replaced by the page number. _Delicious_ does not provide access to all tagged Web pages with the word _news_ despite displaying a total number for these items. Thus, our capture is based on what is made available to the users through the site. On both days upon which data were captured, all possible Web pages tagged with the word _news_ were collected and saved. On the first day, the URLs from sixty-three pages (maximum number of pages displayed) were collected, and on the second day, URLs from fifty-seven pages (maximum number of pages displayed) were collected yielding a total of 8,380 Web pages. From this, 5,760 distinct Web pages were found. This indicates that about 31% of the Web pages collected during this period were tagged more than once. In this study, we targeted U.S.-based Web pages through initial computer screening of URLs. To eliminate non-U.S. Web pages, we inspected the URLs of the 5,760 distinct pages and those with top-level domain names of non-U.S. countries, such as .jp (Japan) and .it (Italy), were excluded from further analysis. The elimination of non-U.S. Web pages resulted in a reduced sample of 4,443 pages. While every effort was made to eliminate foreign sources, the analysis reveals that several foreign stories are nonetheless included in the data as computer screening was unable to identify foreign sources that did not follow the above described pattern in the URLs. Table 1 shows the general composition of the sample analysed for this study.

<table><caption>

**Table 1: Preliminary URL analysis**</caption>

<tbody>

<tr>

<th colspan="3">Category</th>

<th>Frequency</th>

<th>Percentage within identified URLs</th>

<th>Total percentage</th>

</tr>

<tr>

<td rowspan="7">Classified URLs</td>

<td rowspan="4">Traditional News Media</td>

<td>Newspaper</td>

<td>222</td>

<td>19.2%</td>

<td>5.0%</td>

</tr>

<tr>

<td>Broadcasting</td>

<td>150</td>

<td>13.0%</td>

<td>3.4%</td>

</tr>

<tr>

<td>Magazine</td>

<td>102</td>

<td>9.0%</td>

<td>2.3%</td>

</tr>

<tr>

<td>News Wire</td>

<td>44</td>

<td>4.0%</td>

<td>1.0%</td>

</tr>

<tr>

<td colspan="2">Social media</td>

<td>460</td>

<td>39.90%</td>

<td>10.4%</td>

</tr>

<tr>

<td colspan="2">News portal</td>

<td>90</td>

<td>8.0%</td>

<td>2.0%</td>

</tr>

<tr>

<td colspan="2">Indepedent news Website</td>

<td>86</td>

<td>7.0%</td>

<td>2.0%</td>

</tr>

<tr>

<td colspan="3">Unclassified URLs</td>

<td>3,289</td>

<td>N/A</td>

<td>74%</td>

</tr>

<tr>

<td colspan="3">Total</td>

<td>4,443</td>

<td>100%</td>

<td>100%</td>

</tr>

</tbody>

</table>

From the 4,443 Web pages, we first identified prominent source categories based on the URL analysis, again with the help of computer screening: major print publications, including the top 100 U.S. newspapers based on unique visitors from the [Nielson/Net Ratings](http://www.naa.org/TrendsandNumbers/Newspaper-Websites.aspx) (Retrieved March 10, 2008), and major news magazines, such as _Time_; broadcast media, such as the three network channels (ABC, CBS and NBC), CNN and MSNBC; wire services, such as the Associated Press; news portals, such as Yahoo.com; independently operated prominent online publications, such as _[Salon](http://www.salon.com/)_ and _[Slate](http://www.slate.com/)_; and popular social media, including blogs and social networking sites (see Table 1 and 2 for specific categories). This classification resulted in 3,289 Web pages that did not belong to any of these categories.

As a large percentage of the data was expected to fall outside of the prominent news source categories identified above, we examined a random sample of this content and developed five additional source categories for the analysis of this content as these sources do not align with typical media sources. These include commercial sites (e.g., [Best Buy](http://www.bestbuy.com/)), government-related sites (e.g., [the U.S. Department of Defense](http://www.defenselink.mil/news/) ), resource pages (e.g., [The WWW Virtual Library: International Affairs Resources](http://www2.etown.edu/vl/newsourc.html)), specific-topic sites (e.g., [The Root](http://theroot.com) ), and foreign sites that were unable to be screened in the initial URL analysis (e.g., [Hotell in Italien](http://italien.europahotell.info/Hotell_i_Livigno_i_Italien_-_Alperna)).

Unlike the initial prominent news source categories, which were screened through the computer, the sources for these URLs were not intuitively recognizable through the URLs themselves. Thus, a manual content analysis was employed for two reasons: to assess topics for both initially identified URLs and unclassified URLs and to identify sources from the unclassified URLs.

A coding scheme for categorizing story topics was developed based on prior conceptualizations of story topics ([Thelwall _et al._ 2007](#the07); see also the [Appendix](#appendix)). We also included a category for tagged pages that linked to homepages or front pages of Websites instead of specific stories. In sampling the stories, we randomly selected 288 from the population of identified content and 344 from the population of uncategorized content to achieve an appropriate sample size from each sampling frame. The unit of analysis was the tagged and shared _news_ story. Two graduate students were trained with the coding protocol and, after becoming familiar with the coding scheme, analysed 20% of the sampled stories, which were also selected randomly. Using Holsti's method for assessing inter-rater agreement for nominal level variables, we achieved an intercoder reliability score of .862 for topics and .914 for sources among unclassified URLs, indicating substantial agreement between the coders. It was unnecessary to assess inter-rater agreement for the sources of the initially identified URLs as the sources of the stories sampled here were already finalized in the prior computer-based analysis of news sources. Finally, a qualitative analysis was employed to assess emerging patterns, if any, between story sources and story topics.

## Results

### Preliminary analysis of news sources in _Delicious_

In assessing the most frequently shared news sources in _Delicious_ (RQ1), we analysed the texts of extracted URLs through a computer program (1154 cases). As shown in Table 1, traditional media sources were the most frequently shared sources through the _news_ tag in _Delicious_ (518, 11.7%). However, about 10.4% (460) of the total sources came from social media. News portals (90, 2%) and independent news sites (86, 2%) were shared significantly less. However, a significant number of sources did not fall into the above 1154 cases and preliminary categories. About 74% of the total sample (3,289) did not fall into these traditional media sources, prominent online news media sources, or even social media. Further analyses of this content are discussed later.

In examining the preliminary data, we first closely analysed the traditional media sources to assess what information audiences stored and shared with other individuals. Table 2 shows a detailed analysis of sources by category in rank order. The analysis reveals that among the online newspaper sites, information from the top ten newspapers was by far shared most frequently (177, 80%) with a significant drop after the top ten news sites. These were also the most frequently shared news sources among all traditional media. CNN was the top news source among the broadcast media sites (54, 36%). The three major network sites were used least frequently in this category (13, 9%). Among the independently operated sites, [CNET](http://www.cnet.com/) had a significant lead over other prominent online sources (39, 45%), such as _Slate_ and _Salon_. _Yahoo news_ was the most frequently shared news aggregator (34, 38%), and Reuters was shared significantly more among the wire services (37, 84%). In addition, various magazines were identified in the analysis. _Time_ magazine was the most frequently used source among magazines (18, 18%) followed by the _Financial Times_ (17, 17%).

Most notable in this analysis is the concentration of sources among social media in general and blogs in particular (327, 71%). Information from blogs was shared most frequently amongst all social media but also amongst all sources, including traditional media. Other social media sources came from user-news sites, such as _Digg_, _Reddit_ and [Newsvine](http://www.newsvine.com/) (48, 10%). Video and/or photo sharing sites, such as YouTube and Flickr were also represented among social media sources (26, 6%).

<table><caption>

**Table 2: Detailed preliminary classification of URLs**</caption>

<tbody>

<tr>

<th>Category</th>

<th>Domain</th>

<th>Frequency</th>

</tr>

<tr>

<td rowspan="10">

_NEWSPAPER_</td>

<td>Top 1-10</td>

<td>177</td>

</tr>

<tr>

<td>Top 11-20</td>

<td>12</td>

</tr>

<tr>

<td>Top 21-30</td>

<td>9</td>

</tr>

<tr>

<td>Top 31-40</td>

<td>7</td>

</tr>

<tr>

<td>Top 41-50</td>

<td>6</td>

</tr>

<tr>

<td>Top 51-60</td>

<td>2</td>

</tr>

<tr>

<td>Top 61-70</td>

<td>4</td>

</tr>

<tr>

<td>Top 71-80</td>

<td>1</td>

</tr>

<tr>

<td>Top 81-90</td>

<td>2</td>

</tr>

<tr>

<td>Top 91-100</td>

<td>2</td>

</tr>

<tr>

<td rowspan="10">

_BROADCAST MEDIA_</td>

<td>CNN</td>

<td>54</td>

</tr>

<tr>

<td>MSNBC</td>

<td>22</td>

</tr>

<tr>

<td>PBS</td>

<td>20</td>

</tr>

<tr>

<td>Bloomberg</td>

<td>12</td>

</tr>

<tr>

<td>Fox</td>

<td>11</td>

</tr>

<tr>

<td>NPR</td>

<td>10</td>

</tr>

<tr>

<td>ESPN</td>

<td>8</td>

</tr>

<tr>

<td>CBS</td>

<td>6</td>

</tr>

<tr>

<td>ABC</td>

<td>5</td>

</tr>

<tr>

<td>NBC</td>

<td>2</td>

</tr>

<tr>

<td rowspan="4">

_INDEPEDENT NEWS SITE_</td>

<td>CNET</td>

<td>39</td>

</tr>

<tr>

<td>Wired</td>

<td>24</td>

</tr>

<tr>

<td>Slate</td>

<td>17</td>

</tr>

<tr>

<td>Salon</td>

<td>6</td>

</tr>

<tr>

<td rowspan="4">

_NEWS PORTAL_</td>

<td>Yahoo News</td>

<td>34</td>

</tr>

<tr>

<td>Yahoo.com</td>

<td>22</td>

</tr>

<tr>

<td>Google.com</td>

<td>22</td>

</tr>

<tr>

<td>Google News</td>

<td>12</td>

</tr>

<tr>

<td rowspan="13">

_MAGAZINE_</td>

<td>Time</td>

<td>18</td>

</tr>

<tr>

<td>Ft.com</td>

<td>17</td>

</tr>

<tr>

<td>TheOnion</td>

<td>13</td>

</tr>

<tr>

<td>Businessweek</td>

<td>9</td>

</tr>

<tr>

<td>Newsweek</td>

<td>8</td>

</tr>

<tr>

<td>Economist</td>

<td>8</td>

</tr>

<tr>

<td>Forbes</td>

<td>6</td>

</tr>

<tr>

<td>New Yorker</td>

<td>6</td>

</tr>

<tr>

<td>Nationalgeographic</td>

<td>6</td>

</tr>

<tr>

<td>Rollingstone</td>

<td>5</td>

</tr>

<tr>

<td>timeforKids</td>

<td>3</td>

</tr>

<tr>

<td>Villagevoice</td>

<td>2</td>

</tr>

<tr>

<td>Motherjones</td>

<td>1</td>

</tr>

<tr>

<td rowspan="12">

_SOCIAL MEDIA_</td>

<td>Blogs in general<sup>*</sup></td>

<td>327</td>

</tr>

<tr>

<td>Digg</td>

<td>36</td>

</tr>

<tr>

<td>Slashdot</td>

<td>22</td>

</tr>

<tr>

<td>YouTube</td>

<td>20</td>

</tr>

<tr>

<td>Livejournal</td>

<td>13</td>

</tr>

<tr>

<td>Wikipedia</td>

<td>10</td>

</tr>

<tr>

<td>BoingBoing</td>

<td>8</td>

</tr>

<tr>

<td>Reddit</td>

<td>7</td>

</tr>

<tr>

<td>Flickr</td>

<td>6</td>

</tr>

<tr>

<td>Newsvine</td>

<td>5</td>

</tr>

<tr>

<td>Thinkprogress</td>

<td>4</td>

</tr>

<tr>

<td>Wonkette</td>

<td>2</td>

</tr>

<tr>

<td rowspan="3">

_NEWS WIRE_</td>

<td>Reuters</td>

<td>37</td>

</tr>

<tr>

<td>AP</td>

<td>5</td>

</tr>

<tr>

<td>AFP</td>

<td>2</td>

</tr>

<tr>

<td colspan="3">

<sup>*</sup>URLs containing the word "blogs" were all classified under the "Blogs in general" category after the other social media subcategories were considered.</td>

</tr>

</tbody>

</table>

In addressing RQ1, we also further examined the content that was not classified within the previously identified computerized analysis of host names, which represents 74% of the entire collected Web pages (see details of this analysis in the following sections). Table 3 shows the results of this analysis. Domain names ending in '.com' were by far the most frequently shared sources representing about 76% (2,027). There is a significant drop in use of the next most frequently used domains, '.org' (404, 15.1%) and '.net' (170, 6.3%).

<table><caption>

**Table 3: Preliminary analysis of unclassified URLs**</caption>

<tbody>

<tr>

<th>Domain</th>

<th>Frequency</th>

<th>Percentage</th>

</tr>

<tr>

<td>.com</td>

<td>2,2027</td>

<td>75.5%</td>

</tr>

<tr>

<td>.org</td>

<td>404</td>

<td>15.1%</td>

</tr>

<tr>

<td>.net</td>

<td>170</td>

<td>6.3%</td>

</tr>

<tr>

<td>.edu</td>

<td>28</td>

<td>1.0%</td>

</tr>

<tr>

<td>.gov</td>

<td>20</td>

<td>0.7%</td>

</tr>

<tr>

<td>.info</td>

<td>17</td>

<td>0.6%</td>

</tr>

<tr>

<td>.us</td>

<td>8</td>

<td>0.3%</td>

</tr>

<tr>

<td>.mil</td>

<td>5</td>

<td>0.2%</td>

</tr>

<tr>

<td>.int</td>

<td>2</td>

<td>0.1%</td>

</tr>

<tr>

<td>.biz</td>

<td>1</td>

<td>0.0%</td>

</tr>

<tr>

<td>.name</td>

<td>1</td>

<td>0.0%</td>

</tr>

<tr>

<td>.226</td>

<td>1</td>

<td>0.0%</td>

</tr>

<tr>

<td>Total</td>

<td>2,684</td>

<td>100%</td>

</tr>

</tbody>

</table>

### Preliminary analysis of news topics in _Delicious_

Table 4 shows results from assessing the most frequently assigned topics in _Delicious_ (RQ2) from randomly selected predetermined tagged stories. Most frequent topics shared were from front pages of general news Websites (96, 33.3%). Science/technology (55, 19.1%), economy/business (31, 10.8%), international (17, 5.9%), entertainment (15, 5.2%), and society (13, 4.5%) stories were other frequently shared topics.

<table><caption>

**Table 4: Preliminary story topic analysis**</caption>

<tbody>

<tr>

<th>Topic</th>

<th>Frequency</th>

<th>Percentage</th>

</tr>

<tr>

<td>General news page</td>

<td>96</td>

<td>33.3%</td>

</tr>

<tr>

<td>Science and technology</td>

<td>55</td>

<td>19.1%</td>

</tr>

<tr>

<td>Economy and business</td>

<td>31</td>

<td>10.8%</td>

</tr>

<tr>

<td>International</td>

<td>17</td>

<td>5.9%</td>

</tr>

<tr>

<td>Entertainment</td>

<td>15</td>

<td>5.2%</td>

</tr>

<tr>

<td>Society</td>

<td>13</td>

<td>4.5%</td>

</tr>

<tr>

<td>Law and judicial</td>

<td>10</td>

<td>3.5%</td>

</tr>

<tr>

<td>Environment</td>

<td>8</td>

<td>2.8%</td>

</tr>

<tr>

<td>Politics</td>

<td>7</td>

<td>2.4%</td>

</tr>

<tr>

<td>Sports</td>

<td>7</td>

<td>2.4%</td>

</tr>

<tr>

<td>Health and medicine</td>

<td>6</td>

<td>2.1%</td>

</tr>

<tr>

<td>Arts and Culture</td>

<td>4</td>

<td>1.4%</td>

</tr>

<tr>

<td>Education</td>

<td>3</td>

<td>1.0%</td>

</tr>

<tr>

<td>Military and defense</td>

<td>3</td>

<td>1.0%</td>

</tr>

<tr>

<td>Agriculture</td>

<td>2</td>

<td>0.7%</td>

</tr>

<tr>

<td>Religion</td>

<td>2</td>

<td>0.7%</td>

</tr>

<tr>

<td>Other</td>

<td>9</td>

<td>3.1%</td>

</tr>

<tr>

<td>Total</td>

<td>288</td>

<td>100%</td>

</tr>

</tbody>

</table>

Table 5 shows results of the sampled story sources. Social media sources were shared most frequently (104, 36.1%) followed by newspapers (67, 23.3%) and broadcast media (46, 16.0%). Other sources were shared much less frequently: magazines (30, 10.4%), independent news sites (17, 5.9%), and finally news aggregators and wire services (12, 4.2% each). This is similar to the characteristics of the larger population from which the sample was drawn. Again, when newspaper and broadcast sources are combined (113, 39.3%), they comprise a larger portion of the sources than social media.

<table><caption>

**Table 5: Preliminary URL source analysis**</caption>

<tbody>

<tr>

<th>Source</th>

<th>Frequency</th>

<th>Percentage</th>

</tr>

<tr>

<td>Social media</td>

<td>104</td>

<td>36.1%</td>

</tr>

<tr>

<td>Newspaper</td>

<td>67</td>

<td>23.3%</td>

</tr>

<tr>

<td>Broadcasting</td>

<td>46</td>

<td>16.0%</td>

</tr>

<tr>

<td>Magazine</td>

<td>30</td>

<td>10.4%</td>

</tr>

<tr>

<td>Independent news Website</td>

<td>17</td>

<td>5.9%</td>

</tr>

<tr>

<td>News wire</td>

<td>12</td>

<td>4.2%</td>

</tr>

<tr>

<td>News portal</td>

<td>12</td>

<td>4.2%</td>

</tr>

<tr>

<td>Total</td>

<td>288</td>

<td>100%</td>

</tr>

</tbody>

</table>

### Analysis of associations between source and topic

A qualitative analysis was conducted to assess the relationship between story topics and story sources (RQ3). Emerging patterns indicate that social media and independent online news sites were the choice of source for science/technology related topics. Magazines were sought for economy and business related affairs. Newspapers were the choice of source for environmental issues, and broadcast sites were the most popular sources for society related topics. However, both newspaper and broadcast sites were used most frequently for general news headlines.

### Further analysis of URLs in _Delicious_: topics, sources and associations

To further address RQ1 and RQ2, we analysed the randomly selected tagged stories that were unable to be classified from the initial computer-based analysis. Table 6 shows results from this analysis. Most frequent topics shared were again from front pages of general news Websites (123, 35.8%). science and technology (58, 16.9%), foreign sites (36, 10.5%), economy and business (27, 7.8%), entertainment, arts and culture (17, 4.9% each), and politics (15, 4.4%) were other frequently shared topics. Thus, popular topics were generally consistent with the initial analysis.

<table><caption>

**Table 6: Unclassified URL story topic analysis**</caption>

<tbody>

<tr>

<th>Topic</th>

<th>Frequency</th>

<th>Percentage</th>

</tr>

<tr>

<td>General news page</td>

<td>123</td>

<td>35.8%</td>

</tr>

<tr>

<td>Science and technology</td>

<td>58</td>

<td>16.9%</td>

</tr>

<tr>

<td>Foreign sites</td>

<td>36</td>

<td>10.5%</td>

</tr>

<tr>

<td>Economy and business</td>

<td>27</td>

<td>7.8%</td>

</tr>

<tr>

<td>Entertainment</td>

<td>17</td>

<td>4.9%</td>

</tr>

<tr>

<td>Arts and Culture</td>

<td>17</td>

<td>4.9%</td>

</tr>

<tr>

<td>Politics</td>

<td>15</td>

<td>4.4%</td>

</tr>

<tr>

<td>Society</td>

<td>10</td>

<td>2.9%</td>

</tr>

<tr>

<td>International</td>

<td>9</td>

<td>2.6%</td>

</tr>

<tr>

<td>Environment</td>

<td>7</td>

<td>2.0%</td>

</tr>

<tr>

<td>Health and medicine</td>

<td>7</td>

<td>2.0%</td>

</tr>

<tr>

<td>Education</td>

<td>5</td>

<td>1.5%</td>

</tr>

<tr>

<td>Law and judicial</td>

<td>4</td>

<td>1.2%</td>

</tr>

<tr>

<td>Sports</td>

<td>3</td>

<td>0.9%</td>

</tr>

<tr>

<td>Military and defense</td>

<td>2</td>

<td>0.6%</td>

</tr>

<tr>

<td>Religion</td>

<td>2</td>

<td>0.6%</td>

</tr>

<tr>

<td>Agriculture</td>

<td>0</td>

<td>0.0%</td>

</tr>

<tr>

<td>Other</td>

<td>2</td>

<td>0.6%</td>

</tr>

<tr>

<td>Total</td>

<td>344</td>

<td>100%</td>

</tr>

</tbody>

</table>

Table 7 shows results of story sources from this content. Social media sources were again shared most frequently (76, 22.1%), closely followed by independent news sites (62, 18.0%). Foreign sites (36, 10.5%), magazines and newspapers (28, 8.1%), resource pages (27, 7.8%), specific topic sites (26, 7.6%), and commercial sites (20, 5.8%) followed in popularity. The sources shared here were much more diversely distributed than in the initial category analysis. Whereas traditional media sources dominated in the analysis of the preliminary content, the remaining content tapped into a variety of sources that are generally not considered traditional news sources. Additionally, there was no specific concentration of popularity of source as found in the preliminary analysis.

<table><caption>

**Table 7: Unclassified URL source analysis**</caption>

<tbody>

<tr>

<th>Source</th>

<th>Frequency</th>

<th>Percentage</th>

</tr>

<tr>

<td>Social media</td>

<td>76</td>

<td>22.1%</td>

</tr>

<tr>

<td>Independent news Website</td>

<td>62</td>

<td>18.0%</td>

</tr>

<tr>

<td>Foreign site</td>

<td>36</td>

<td>10.5%</td>

</tr>

<tr>

<td>Newspaper</td>

<td>28</td>

<td>8.1%</td>

</tr>

<tr>

<td>Magazine</td>

<td>28</td>

<td>8.1%</td>

</tr>

<tr>

<td>Resource page</td>

<td>27</td>

<td>7.8%</td>

</tr>

<tr>

<td>Specific topic site</td>

<td>26</td>

<td>7.6%</td>

</tr>

<tr>

<td>Commercial site</td>

<td>20</td>

<td>5.8%</td>

</tr>

<tr>

<td>Broadcasting</td>

<td>19</td>

<td>5.5%</td>

</tr>

<tr>

<td>News portal</td>

<td>11</td>

<td>3.2%</td>

</tr>

<tr>

<td>Government site</td>

<td>6</td>

<td>1.7%</td>

</tr>

<tr>

<td>News wire</td>

<td>3</td>

<td>0.9%</td>

</tr>

<tr>

<td>Other</td>

<td>2</td>

<td>0.6%</td>

</tr>

<tr>

<td>Total</td>

<td>288</td>

<td>100%</td>

</tr>

</tbody>

</table>

A qualitative analysis was then conducted to assess the relationship between story topics and story sources (RQ3). Emerging patterns indicate that social media were the choice of source for arts and culture and science and technology related stories. Independent news sites were also sought for science and technology topics and entertainment stories. Newspapers and broadcast channels were the choice of source for general news headlines found on home pages and front pages of these sites. However, independent news sites, news aggregators and resource pages were also frequently tagged to general news pages.

## Discussion

The objectives of this paper are to examine the general news audience's uses of social bookmarking features as tools for selecting, posting and sharing news articles. We were particularly interested in the sources and topics that people visit, select, and share and the relationships between these two variables.

Most notable findings include the diverse nature of stories from both traditional and new media channels. Additionally, based on the overall tagged source analyses, prominent information sources, such as primary traditional news media, were not exclusively used. The sheer volume of uncategorized pages indicates that online news seekers seem to consider relatively new and informative Web pages encountered during casual navigation as news sources, rather than relying on Web resources from conventionally authoritative media organizations, such as newspaper and broadcasting Websites.

More specifically, the findings reveal that among the initially identified categories, mainstream news sources are still more frequently used than other informational sources, with concentration in the top ten newspaper Websites. It appears that the dominance of the mainstream newspaper press is also reflected in online sharing of information by news audiences. In contrast, however, broadcast news, which has traditionally been deemed the most credible news source, is significantly less popular, and the prominent three main networks represent only a small proportion of the overall sources.

Of particular importance also is the growing presence of information shared from blogs. Stories shared from blogs represent almost twice that from the top ten newspaper sites. This suggests that the online audience is welcoming more commentary and filtering of news from alternative sources as much as traditional newspaper sources. This finding is consistent with the results of the Project for Excellence in Journalism ([2007](#pej07)).

These findings should be interpreted with caution as personal blogs and blogs hosted on mainstream news organizations were not distinguished in this study. For example, _NYTimes.com_ hosts several blogs. These blogs and those created through blogger.com, a popular blog-creating Website, were not differentiated in this study. Additionally, not included in the _Blogs in general_ category are other prominent blogs, such as _[Slashdot](http://slashdot.org/)_ and _[BoingBoing](http://boingboing.net/)_, and the addition of these two major blogs to the analysis would add to the centrality of blogs as a major source of information among news audiences in _Delicious_.

Compared to blogs, other social media sources are used relatively infrequently. While _YouTube_ has grown to become a major part of pop culture, this analysis shows that it is not yet being adopted as seamlessly as blogs have been in news information sharing activities. Wikipedia is also not used as frequently, despite its utility as a source for general information consumers. This is to be expected because information in Wikipedia has been collaboratively edited over an extended period of time, so it might be considered as a resource encyclopedia, rather than as a source for news.

The analysis of the 3,289 news stories that did not neatly fit into the initial computer-based URL analysis identified 2,684 distinct host names, which were spread across twelve different domain names. This indicates that many and diverse Web sources were used by news seekers. This phenomenon might imply that online news consumers do not seem to consider traditionally esteemed news sources or new media (such as blogs) as exclusive places to collect news. Another potential implication is that news audiences' perceptions of what is considered to be news may extend beyond that of traditional conceptions and might even differ from what is normally expected so that something _new_ may be treated as _news_.

Within the preliminary investigation, analysis of the news topics indicate that front pages of news publications and technology related stories were most frequently shared through _Delicious_. The popularity of science and technology stories does not coincide with literature documenting traditional media's prominent coverage of political and governmental and foreign and military affairs. The front pages of general news publications with multiple links to top headlines are most likely shared for information as it appears that quantity of easily accessible stories and the relative impact of leading headlines are still regarded as interesting and important information. Specifically, the popularity of sharing science and technology stories, which are generally slighted by the traditional media when compared to political and governmental affairs, makes intuitive sense in that social bookmarking tools, and especially _Delicious_, may be utilized by more technology aware readers. However, there was a visible shortage of topics discussing politics during a year that has witnessed one of the most intense presidential campaigns in U.S. history. Additionally, military and defense issues are common news stories covered in traditional U.S. media sources, yet the analysis yielded low numbers of such stories being shared. Sports stories, also a generally popular topic, were also shared relatively infrequently.

The relationships between topics and sources also reveal that traditional media sources were used more frequently for society related topics, such as crime and general social issues or economic and business and environmental stories. In addition, audiences sought traditional media channels for a quick look at general news headlines. However, technology and science related issues were, again, popular topics shared through social media sources. Independent online news publications, such as _Wired_ and _CNET_ were also popular sources for science and technology stories because technology is the dominant theme for both online news publications.

The second part of the analysis was significantly more complex, as existing frameworks for news sources did not provide a good fit assessed through initial categorization attempts. Results here indicate first, that what consumers consider as news sources is more varied than traditionally understood. Our analyses reveal that many news consumers sought information from very particular sources, such as topic-specific Websites, foreign sites, commercial sites and governmental sources in addition to traditional mass media outlets. Popular news topics here were similar to those found in the initial analysis with, again, front pages of news publications and technology related stories dominating shared stories. This, too, seems likely, in that we were identifying story topics in the same manner as in the preliminary analysis. Slight differences were found with the addition of arts and culture, and politics as frequently shared story topics among the unclassified URL stories, perhaps making up for lack of perspectives offered from traditional media or picking up on alternative views provided through more diverse sources. However, significant differences emerged when analysing the story sources: they were much more diverse and decentralized. Traditionally considered news sources were used much less frequently than was found within the predetermined content with newspapers and broadcast media making up only 13.6% of the story sources.

The relationships between topic and source yielded generally similar patterns to those found in the preliminary analysis with social media serving as popular sources for science/technology related news. These sources also served as places for finding arts and culture related stories. Independent news sites in this analysis also served as sources for technology and entertainment related news. News aggregators and resource pages, a new source category, were popular places to find general news headlines as they are categorized by having multiple links to various stories and/or information. Traditional media sources, such as broadcast media and newspapers, were again sought primarily for multiple news topics. These findings support that news audiences primarily seek specialized stories through newer and diversified sources but often resort to traditional media outlets when seeking a quick overview of stories. Yet news aggregators, resources pages and even independent news sites were also sought for general news headlines reflecting a more diversified news consumption pattern within this analysis.

Overall, the findings show that mainstream media sources are indeed consulted in _Delicious_, but social media sources, especially blogs, are growing in visibility. While traditional sources included a wide range of media with some concentration among the top ten newspaper Websites, social media sources focused primarily on blogs but included a narrow range of other social media sources. However, and most notably, alternative information sources that do not fall in the realm of traditionally considered news media or prominent social media sources were frequently sought-after places for information. Online news audiences appear to actively seek alternatives to mainstream news and prominent online media to more diversified and specific sources and topics of information. The unclassified sources were also more likely to include story topics that received scant coverage from or were neglected in mainstream, prominent news sources.

Lastly, this study offers empirical evidence pointing to an increasingly proactive news audience seeking news online. These findings have critical implications for traditional communication theories and issues related to source credibility. In addition, these results raise questions about how to define news and discuss journalism. Emergent interactive technologies appear to influence and potentially transform audiences' views of news and will increasingly challenge traditional journalism and its perceived authority.

## Limitations and future research

This study is not without limitations: although all available Web pages tagged with the word news in _Delicious_ were collected over two-weeks, it may be difficult to generalize these findings to all data over time as this study examines only a snapshot of the data from _Delicious_. Additionally, this study focused on U.S.-based Web sources, and non-U.S. sources were excluded as far as possible. Finally, computer-assisted tag analysis may have limited our understanding of the true context of the shared stories.

Future studies should improve the sampling frame and the sampling method in order to increase the generalizability of the findings and further confirm the results of this study. Additionally, blogs may be more systematically analysed and distinguished (for example, between personal blogs and blogs hosted by mainstream media outlets). As discussed previously, social bookmarking as it relates to news consumption has not been examined extensively. This study can serve as a beginning report on how traditionally passive news consumers are becoming active information seekers through social bookmarking tools. Still, more extensive research is needed to understand the potential and limits to social bookmarking activities. Other studies might conduct a comparative analysis of various social bookmarking sites to identify similarities and differences in how different sites are used. Additionally, further investigation into audience characteristics may also provide insightful information on how to better utilize this new communication tool to more meaningfully engage active news consumers.

## Acknowledgements

The authors would like to thank anonymous reviewers for their valuable comments.

## <a id="authors"></a>About the authors

Deborah Chung is an assistant professor in the School of Journalism and Telecommunications, University of Kentucky, USA. She received her Ph.D. from Indiana University at Bloomington, M.S. from the University of Illinois at Urbana-Champaign and B.J. from the University of Missouri at Columbia. Her research interests focus on the participatory opportunities afforded to information consumers through new information and communication technologies and their transformative impact on journalism practice, culture and education. She can be contacted at [dchung@uky.edu](mailto:dchung@uky.edu)

Kwan Yi is an assistant professor in the School of Library and Information Science, University of Kentucky, USA. He received his Ph.D. degree in Information Studies and Master of Science in Computer Science from McGill University, Montreal in Canada. He also received his Master of Science in Applied Mathematics from the University of Illinois at Urbana-Champaign, USA, and Bachelor's degrees in both Computer Science and Mass Communications from Korea University, Republic of Korea. Areas of his research interests include the organization and classification of digital information and information retrieval. He can be contacted at [kwan.yi@uky.edu](mailto:kwan.yi@uky.edu)

## References

*   <a id="abe77"></a>Abel, J.D. & Wirth, M.O. (1977). Newspaper vs. TV credibility for local news. _Journalism Quarterly_, **54**(2), 371-375.
*   <a id="alt02"></a>Althaus, S.L. & Tewksbury, D. (2002). Agenda setting and the "new" news: patterns of issue importance among readers of the paper and online versions of the New York Times. _Communication Research_, **29**(2), 180-207\.
*   <a id="arr07"></a>Arrington, M. (2007, September 6). [Exclusive: screen shots and feature overview of Delicious 2.0 preview](http://www.webcitation.org/5imdXTuF2). _TechCrunch_, Retrieved 15 February, 2009 from http://www.techcrunch.com/2007/09/06/exclusive-screen-shots-and-feature-overview-of-delicious-20-preview/ (Archived by WebCite® at http://www.webcitation.org/5imdXTuF2)
*   <a id="bev88"></a>Beville, H. M. (1988). _Audience ratings; radio, television, cable_. Hillsdale, NH: Lawrence Erlbaum.
*   <a id="bos09"></a>Boswell, W. (2009). [Social bookmarking sites: the top ten social bookmarking sites on the Web.](http://www.webcitation.org/5imduWGbd) _About.com_, Retrieved 15 January, 2009 from http://websearch.about.com/od/bestwebsites/tp/freebookmarks.htm (Archived by WebCite® at http://www.webcitation.org/5imduWGbd)
*   <a id="bow05"></a>Bowman, S & Willis, C. (2005). [The future is here, but do news media companies see it?](http://www.webcitation.org/5ime4TUnf) _Nieman Reports_, **59**(4), 6-10\. Retrieved 4 August, 2009 from http://www.nieman.harvard.edu/reportsitem.aspx?id=100558 (Archived by WebCite® at http://www.webcitation.org/5ime4TUnf)
*   <a id="buz90"></a>Buzzard, K.S. (1990). _Chains of gold: marketing the ratings and rating the markets_. Metuchen, NJ: Scarecrow Press.
*   <a id="cas07"></a>Castells, M. (2007). Communication, power and counter-power in the network society. _International Journal of Communication_, **1**, 238-266\.
*   <a id="cha00"></a>Chan-Olmsted, S. & Park, J. (2000). From on-air to online world: examining the content and structures of broadcast TV stations' Web sites. _Journalism & Mass Communication Quarterly_, **77**(2), 321-39.
*   <a id="chu04"></a>Chung, D.S. (2004). _Into interactivity? How news Websites use interactive features._ Paper presented at the International Communication Association annual convention, New Orleans, LA, May 27-31\.
*   <a id="chu07a"></a>Chung, D.S. (2007). Profits and perils: online news producers' perceptions of interactivity and uses of interactive features. _Convergence: The International Journal of Research into New Media Technologies_, **13**(1), 43-61\.
*   <a id="chu07b"></a>Chung, D. S., Kim, E., Trammell, K. D. & Porter, L. V. (2007). Uses and perceptions of blogs: a report on professional journalists and journalism educators. _Journalism & Mass Communication Educator_, **62**(3), 305-322.
*   <a id="del08"></a>Delicious blog. (2008 November). _[Delicious is 5!](http://www.webcitation.org/5ime4TUnf)_ Retrieved 15 January, 2009 from http://blog.delicious.com/blog/2008/11\. (Archived by WebCite® at http://www.webcitation.org/5imeXed9w)
*   <a id="del05"></a>Delwiche, A. (2005). [Agenda-setting, opinion leadership, and the world of Web logs.](http://www.webcitation.org/5imehE1OP) _First Monday_, **10**(12), Retrieved 15 March, 2009 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1300/1220 (Archived by WebCite® at http://www.webcitation.org/5imehE1OP)
*   <a id="fla00"></a>Flanagin, A.J. & Metzger, M.J. (2000). Perceptions of Internet information credibility. _Journalism & Mass Communication Quarterly_, **77**(3), 515-540\.
*   <a id="gan79"></a>Gans, H. (1979). _Deciding what's news: a study of CBS evening news, NBC Nightly News, Newsweek and Time_. New York, NY: Random House.
*   <a id="gaz86"></a>Gaziano, C. & McGrath, K. (1986). Measuring the concept of credibility. _Journalism Quarterly_, **63**(3), 451-462.
*   <a id="gil06"></a>Gilbertson, S. (2006, November 6). [Social bookmarking showdown.](http://www.webcitation.org/5imf2BDWD) _Wired_, Retrieved 15 March 2008 from http://www.wired.com/techbiz/it/news/2006/11/72070 (Archived by WebCite® at http://www.webcitation.org/5imf2BDWD)
*   <a id="gol06"></a>Golder, S. & Huberman, B.A. (2006). Usage patterns of collaborative tagging systems. _Journal of Information Science_, **32**(2), 198-208.
*   <a id="gra88"></a>Graber, D. A. (1988). _Processing the news: how people tame the information tide_ (2nd ed). White Plains, NY: Longman.
*   <a id="gre06"></a>Greer, J. & Mensing, D. (2006). Evolution of online newspapers: a longitudinal content analysis, 1997-2003\. In X. Li (Ed.) _Internet newspapers: the making of a mainstream medium_ (pp. 13-32). Mahwah, NJ: Lawrence Erlbaum Associates.
*   <a id="hal98"></a>Ha, L. & James, L. (1998). Interactivity reexamined: a baseline analysis of early business Websites. _Journal of Broadcasting & Electronic Media_, **42**(4), 457-474\.
*   <a id="hab62"></a>Habermas, J. (1962). _The structural transformation of the public sphere: an inquiry into a category of bourgeois society_. Trans. by T. Burger with F. Lawrence. Cambridge, MA: The MIT Press.
*   <a id="her04"></a>Herring, S.C., Scheidt, L.A., Bonus, S. & Wright, E. (2004). [Bridging the gap: a genre analysis of Weblogs.](http://www.webcitation.org/5iml5mNLs) In _Proceedings of the 37th Annual Hawaii International Conference on System Sciences (HICSS'04) - Track 4_. (pp.40101b). Piscataway, NJ: IEEE Computer Society Press. Retrieved 15 December 2008, from http://www.ics.uci.edu/~jpd/classes/ics234cw04/herring.pdf (Archived by WebCite® at http://www.webcitation.org/5iml5mNLs)
*   <a id="hof97"></a>Hofstetter, C.R. & Gianos, C. L. (1997). Political talk radio: actions speak louder than words. _Journal of Broadcasting & Electronic Media_, **41**(4), 501-515\.
*   <a id="hol97"></a>Hollander, B.A. (1997). Fuel to the fire: talk radio and the Gamson hypothesis. _Political Communication_, **14**(3), 355-370\.
*   <a id="ive08"></a>Ives, B. (July 2008). [30 top social bookmarking Web sites.](http://www.webcitation.org/5imlKJkxn) _Portals and KM_ Retrieved 15 February, 2009 from http://billives.typepad.com/portals_and_km/2008/07/30-top-social-b.html (Archived by WebCite® at http://www.webcitation.org/5imlKJkxn)
*   <a id="joh98"></a>Johnson, T. J. & Kaye, B. K. (1998). Cruising is believing? Comparing Internet and traditional sources on media credibility measures. _Journalism & Mass Communication Quarterly_, **75**(2), 325- 340\.
*   <a id="joh08"></a>Johnson, P. R. & Yang, S. (2008). _Popularity of news items on Digg.com: toward a definition of newsworthiness for social news sites._ Paper presented at the annual convention of the Association for Education in Journalism and Mass Communication, Chicago, IL, August 6-9.
*   <a id="ken00"></a>Kenney, K., Gorelik, A. & Mwangi, S. (2000). [Interactive features of online newspapers.](http://www.webcitation.org/5imlZKkVH) _First Monday_, **5**(1), Retrieved 15 December 2008 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/720/629 (Archived by WebCite® at http://www.webcitation.org/5imlZKkVH)
*   <a id="kio01"></a>Kiousis, S. (2001). Trust or mistrust? Perceptions of media credibility in the information age. _Mass Communication & Society_, **4**(4), 381-403.
*   <a id="kip06"></a>Kipp, M. E. I. & Campbell, D. G. (2006). [Patterns and inconsistencies in collaborative tagging systems: an examination of tagging practices.](http://www.webcitation.org/5inxWYWel) _Proceedings of American Society for Information Science and Technology_, **43**, 1-18\. Retrieved 15 February 2009 from http://dlist.sir.arizona.edu/1704/01/KippCampbellASIST.pdf (Archived by WebCite® at http://www.webcitation.org/5inxWYWel)
*   <a id="lix06"></a>Li, X. (2006). _Internet newspapers: the making of a mainstream medium_. Mahwah, NJ: Lawrence Erlbaum Associates.
*   <a id="mas99"></a>Massey, B. & Levy, M. (1999). Interactivity, online journalism, and English-language Web newspapers in Asia. _Journalism & Mass Communication Quarterly_, **76**(1), 138-151.
*   <a id="mat04"></a>Matheson, D. (2004). Weblogs and the epistemology of the news: some trends in online journalism. _New Media & Society_, **6**(4), 443-468.
*   <a id="mcm99"></a>McMillian, S.J. (1999). Health communication and the Internet: relations between interactive characteristics of the medium and site creators, content, and purpose. _Health Communication_, **11**(4), 375-390\.
*   <a id="mcq94"></a>McQuail, D. (1994). _Mass communication theory: an introduction_ (3rd ed). Thousand Oaks, CA: Sage Publications.
*   <a id="mor96"></a>Morris, M. & Ogan, C. (1996). The Internet as mass medium. _Journal of Communication_, **46**(1), 39-50.
*   <a id="nip06"></a>Nip, J.Y.M. (2006). Exploring the second phase of public journalism. _Journalism Studies_, **7**(2), 212-236.
*   <a id="pew05"></a>Pew Research centre for the People & the Press. (2004). _[News audiences increasingly politicized: online news audience larger, more diverse](http://www.webcitation.org/5inxzZacM)_. Washington, DC: Pew Research Center for People and the Press. Retrieved 15 March 2009 from http://people-press.org/reports/display.php3?ReportID=215 (Archived by WebCite® at http://www.webcitation.org/5inxzZacM)
*   <a id="pic07"></a>Picone, I. (2007). [Conceptualising online news use.](http://www.webcitation.org/5io5qZLZo) _Observatorio (OBS*) Journal_, **3**(3), 93-114\. Retrieved 5 August, 2009 from http://www.revistas.univerciencia.org/index.php/observatorio/article/view/4034/3785 (Archived by WebCite® at http://www.webcitation.org/5io5qZLZo)
*   <a id="pew09"></a>Project for Excellence in Journalism. (2009). _[The state of the news media 2009: an annual report on American journalism](http://www.webcitation.org/5inxuTNAy) _. Washington, DC: Pew Research Center for People and the Press. Retrieved 15 March 2009 from http://www.stateofthenewsmedia.org/2009/index.htm (Archived by WebCite® at http://www.webcitation.org/5inxuTNAy)
*   <a id="pej08"></a>Project for Excellence in Journalism. (2008). _[The state of the news media 2008: an annual report on American journalism](http://www.stateofthenewsmedia.org/2008/)_. Washington, DC: Pew Research Center for People and the Press.Retrieved 15 March 2009 from http://www.stateofthenewsmedia.org/2008/ (Archived by WebCite® at http://www.webcitation.org/5io6CQ7fO)
*   <a id="pej07"></a>Project for Excellence in Journalism (2007). _T[he state of the news media 2007: an annual report on American journalism](http://www.webcitation.org/5io6Ijfp2) _. Washington, D.C.: Pew Research Centre. Retrieved 15 October 2007 from http://www.stateofthenewsmedia.com/2007/index.asp (Archived by WebCite® at http://www.webcitation.org/5io6Ijfp2)
*   <a id="pej06"></a>Project for Excellence in Journalism. (2006). _[The state of the news media 2006: an annual report on American journalism](http://www.webcitation.org/5io6Q1nW2) _. Washington, DC: Pew Research Center for People and the Press. Retrieved 15March 2009 from http://www.stateofthenewsmedia.org/2006/ (Archived by WebCite® at http://www.webcitation.org/5io6Q1nW2)
*   <a id="pej05"></a>Project for Excellence in Journalism. (2005). _[The state of the news media 2005: an annual report on American journalism](http://www.webcitation.org/5io6Q1nW2) _. Washington, DC: Pew Research Center for People and the Press.Retrieved 15 March 2009 from http://www.stateofthenewsmedia.org/2005/ (Archived by WebCite® at http://www.webcitation.org/5io6Q1nW2)
*   <a id="pej04"></a>Project for Excellence in Journalism. (2004). _[The state of the news media 2004: an annual report on American journalism](http://www.webcitation.org/5io6dbKFu) _. Washington, DC: Pew Research Center for People and the Press.Retrieved 15 March 2009 from http://www.stateofthenewsmedia.org/2004/ (Archived by WebCite® at http://www.webcitation.org/5io6dbKFu)
*   <a id="ren79"></a>Renfro, P. C. (1979). Bias in selection of letters to editor. _Journalism Quarterly_, **56**(4), 822-826\.
*   <a id="ric04"></a>Richardson, J. E. & Franklin, B. (2004). Letters of intent: election campaigning and orchestrated public debate in local newspapers' letters to the editor. _Political Communication_, **21**(4), 459-478\.
*   <a id="rob02"></a>Roberts, M. Wanta, W. & Dzwo, T-H. (2002). Agenda setting and issue salience online. _Communication Research_, **29**(4), 452-465\.
*   <a id="rog86"></a>Rogers, E. M. (1986). _Communication technology: the new media in society_. New York, NY: The Free Press.
*   <a id="ros05"></a>Rosenberry, J. (2005). Few papers use online techniques to improve public communication. _Newspaper Research Journal_, **26**(4), 61-73.
*   <a id="sal05"></a>Salwen, M. B. (2005). Online news trends. In M.B. Salwen, B. Garrison & P. D. Driscoll (Eds.), _Online news and the public_ (pp. 47-79). Mahwah, NJ: Lawrence Erlbaum Associates.
*   <a id="sch05"></a>Schudson, M. (2005). Four approaches to the sociology of news. In J. Curran & M. Gurevitch (Eds.), _Mass media and society_ (4th ed.), (pp. 172-197). London: Hodder Education.
*   <a id="sch99"></a>Schultz, T. (1999). [Interactive options in online journalism: a content analysis of 100 U.S. newspapers.](http://www.webcitation.org/5io6qEcz9) _Journal of Computer-Mediated Communication_, **5**(1), Retrieved 15 December 2008 from http://jcmc.indiana.edu/vol5/issue1/schultz.html (Archived by WebCite® at http://www.webcitation.org/5io6qEcz9)
*   <a id="sha49"></a>Shannon, C. & Weaver, W. (1949). _The mathematical theory of communication_. Urbana, IL: University of Illinois Press.
*   <a id="sig73"></a>Sigal L.V. (1973). _Reporters and officials: the organization and politics of newsmaking_. Lexington, MA: D.C. Heath and Co.
*   <a id="sin01"></a>Singer, J.B. (2001). The metro wide Web: changes in newspapers' gatekeeping role online. _Journalism & Mass Communication Quarterly_, **76**(1), 65-80\.
*   <a id="sin05"></a>Singer, J.B. (2005). The political j-blogger: 'normaling' a new media form to fit old norms and practices. _Journalism_, **6**(2), 173-198.
*   <a id="spi07"></a>Spiteri, L.F. (2007). The structure and form of folksonomy tags: the road to the public library catalog. _Information Technology and Libraries_, **26**(3), 13-24.
*   <a id="squ00"></a>Squires, C.R. (2000). Black talk radio: defining community needs and identity. _The Harvard International Journal of Press/Politics_, **5**(2), 73-95.
*   <a id="the07"></a>Thelwall, M., Byrne, A. & Goody, M. (2007). [Which types of news story attract bloggers?](http://www.webcitation.org/5io73WPVQ) _Information Research_, **12**(4), Retrieved 15 June 2008 from http://informationr.net/ir/12-4/paper327.html (Archived by WebCite® at http://www.webcitation.org/5io73WPVQ)
*   <a id="thu08"></a>Thurman, N. (2008). Forums for citizen journalists? Adoption of user generated content initiatives by online news media. _New Media & Society_, **10**(1), 139-157\.
*   <a id="wal05"></a>Wall, M. (2005). 'Blogs of war': Weblogs as news. _Journalism_, **6**(2) 153-172.
*   <a id="wil08"></a>Wilson, D. (2008, March 5). [Top 10 social bookmarking sites](http://www.webcitation.org/5io7CfdVX). Social media optimization, Retrieved on 15 February 2009 from http://social-media-optimization.com/2008/03/top-10-social-bookmarking-sites/ (Archived by WebCite® at http://www.webcitation.org/5io7CfdVX)
*   <a id="yex06"></a>Ye, X. & Li, X. (2006). Internet Newspapers' Public Forum and User Involvement. In X. Li (Ed.) _Internet Newspapers: The Making of a Mainstream Medium_ (pp. 243-260). Mahwah, NJ: Lawrence Erlbaum Associates.
*   <a id="yik08a"></a>Yi, K. (2008a). Mining a Web2.0 service for the discovery of semantically similar terms: a case study with del.icio.us. In George Buchanan, Masood Masoodian and Sally Jo Cunningham (Eds.) _Digital Libraries: Universal and Ubiquitous Access to Information: Proceedings of the International Conference on Asia-Pacific Digital Libraries ICADL 2008, Bali, Indonesia, December 2-5, 2008_ (pp. 321-326). Berlin, Heidleberg: Springer.
*   <a id="yik08b"></a>Yi, K. (2008b). A conceptual framework for improving information retrieval in folksonomy using Library of Congress subject headings. _Proceedings of the American Society for Information Science and Technology_, **45**(1), 1-6
*   <a id="yik09"></a>Yi, K. & Chan, L. M. (in press). Linking folksonomy to Library of Congress Subject Headings: an exploratory study. _Journal of Documentation_.

* * *

## <a id="appendix"></a>Appendix

### Sources

1.  NEWSPAPERS: such as The New York Times (http://www.NYTimes.com)
2.  BROADCAST MEDIA: radio/TV sites, such as http://www.cnn.com/
3.  INDEPENDENT NEWS SITES: news publications existing solely online without a print/broadcast counterpart.
4.  NEWS PORTALS: aggregators that compile all different kinds of news like Google and Yahoo or sites that compile news from multiple sources and not news from their own at all.
5.  MAGAZINES: such as Time, Newsweek.
6.  SOCIAL MEDIA: blogs, wikis, YouTube Live Journal, Digg, _Delicious_, Reddit, Flickr, Newsvine etc.
7.  NEWS WIRE FEEDS: such as news from the Associated Press (AP) or Reuters
8.  SPECIFIC TOPIC SITES: Must be very specific and narrow in its focus, such as cell phones, travel, about a specific technology (not technology in general), etc.
9.  COMMERCIAL SITES: Websites that sell specific products or advertise products.
10.  GOVERNMENT: government related sites, such as the CIA, Department of Defense
11.  RESOURCE PAGES: provides links, directories to other sources, may also have a list of tags from Digg, _Delicious_, Reddit etc.
12.  FOREIGN SITES: sites that are written in a foreign language.
13.  99\. OTHER: none of the above.

### News topics

1.  AGRICULTURE: farming, fishing, forestry, animal husbandry, hunting for food
2.  SOCIETY: social issues, such as homelessness, ideologies, crime
3.  ECONOMY/BUSINESS: including trade, finance; be careful with the economy/business category (business means company business and not abstract business such as the business of politics)
4.  EDUCATION: about education, training or schools
5.  ENVIRONMENT: including local weather, natural disasters, pollution
6.  HEALTH/MEDICINE: drugs, treatment, medical research, exercise, nutrition
7.  INTERNATIONAL: dealing specifically with international issues only; must have little to no mention of U.S.
8.  LAW/JUDICIAL: legal proceedings, legislative change and including immigration
9.  MILITARY/DEFENSE: must deal with U.S. military/defense issues, such as wars, military operations
10.  POLITICS: political scandal, treaties and negotiations between states, parties, elections, political systems
11.  RELIGION: organized religion, new age religion, spirituality, religious disagreements, sectarian conflict/discrimination
12.  SCIENCE/TECHNOLOGY: scientific research, development, discoveries, exploration, technological innovation
13.  ARTS & CULTURE: Music, books, weddings/celebrations, wining & dining, home & garden, fashion
14.  ENTERTAINMENT: celebrities, TV shows, movies, leisure (travel/tourism), gaming (video, computer)
15.  SPORTS: sporting events, analysis, gambling, organization, development, governing bodies, spectating, records
16.  GENERAL NEWS PAGE: home page or front page of a news Website/not dealing with a specific story and generally characterized by having multiple story links
17.  FOREIGN SITES: main language is not English; generally non U.S. sites
18.  99\. None of above (give your own label in this case)
19.  100\. Broken link or story not available.