#### Information Research, Vol. 8 No. 2, January 2003

# Telecentres and the provision of community based access to electronic information in everyday life in the UK

#### [Debbie Ellen](mailto:D.Ellen@open.ac.uk)  
Department of Telematics  
The Open University  
Milton-Keynes. UK  

#### **Abstract**

> This paper presents research undertaken for a PhD between 1997 and 2000 in the United Kingdom. The study explored three themes, the use of electronic information in everyday life, the use of telecentres by local communities in everyday life and community involvement strategies in the design, development and implementation of telecentres. Results led to development of a framework to inform future community based ICT initiatives. The findings and methods used to analyse the data are relevant to both future study of community-based ICT initiatives and the broader areas of information seeking, information seeking behaviour and user needs. Consequently, it is hoped they will be used by other researchers wishing to take a qualitative approach when studying in these areas. This paper will report findings relating to the use of electronic information in everyday life, setting these findings within the context of the whole study. The [full PhD thesis](http://www.mmu.ac.uk/h-ss/dic/research/ellen/contents.html) is available on-line.

## Introduction

This paper presents research undertaken for a PhD between 1997 and 2000 in the United Kingdom. The study explored three themes, the use of electronic information in everyday life, the use of telecentres by local communities in everyday life and community involvement strategies in the design, development and implementation of telecentres. Results led to development of a framework to inform future community based ICT initiatives. The findings and methods used to analyse the data are relevant to both future study of community based ICT initiatives and the broader areas of information seeking, information seeking behaviour and user needs. Consequently, it is hoped they will be used by other researchers wishing to take a qualitative approach when studying in these areas. This paper will report findings relating to the use of electronic information in everyday life, setting these findings within the context of the whole study. The PhD study referred to in this paper is available online ([Ellen](http://www.mmu.ac.uk/h-ss/dic/research/ellen/contents.html), 2000), and readers are referred to sections of the thesis for additional detail. It should be noted that the paper presents a picture of developments in community based ICT initiatives as they stood when the thesis was submitted in January 2000\. Where appropriate, significant new developments have been mentioned, as have studies in everyday information seeking that have been published since the beginning of 2000.

During the period of the study, the UK Labour government set a target for 25% of government services to be available in electronic format by 2002 ([Central Office of Information](#ref7), 1998: 6). Politicians talked of the 'knowledge economy', and 'the new industrial revolution' when discussing plans to introduce Information and Communication Technologies (ICT) into schools, libraries and learning centres ([Blair](#ref3), 1999). A wide range of different projects are underway (the University for Industry (UfI), the National Grid for Learning, the Public Library Network and UK online are four key developments) aimed at providing community based access (CBA) to ICT and electronic information. However, these projects are being implemented in a fragmented way, without an overall strategy. Indeed, the Library and Information Commission (LIC, now Re:source) published a discussion paper ([1997b](#ref37)) calling for a National Information Policy (NIP), raising concerns about the need for co-ordination of initiatives to avoid duplication. Indeed calls for progress in this area were made in the early 1990s ([Moore and Steele](#ref43b), 1991). A conference was held in March 2000 sponsored by the British Council, The Library Association and the LIC with the aim of raising awareness about the need for a policy and how one might be developed. The LIC published a document _Keystone for the Information Age: A National Policy for the UK_ . The document noted:

> Many of the government's initiatives have a significant information element. Failure to get the information right will reduce the impact of individual initiatives." ([Library and Information Commission](#ref37b), 2000)

In 2001 the Library Association set up a number of Policy Advisory Groups including one which focused on National Information Policies ([Muir and Oppenheim](#ref43c), 2001). Amid such activity implementation of major initiatives by different government departments continue apace.

In the thesis, I argued that these developments were being implemented without due consideration of lessons that should have been learnt from _established_ community based ICT initiatives. Major projects under development (such as the UfI, the National Grid for Learning, the Public Library Network and the development of ICT Learning Centres (since rebranded as UK online centres) were predominantly aimed at providing access to ICT for those who do not have access elsewhere. Similar provision already existed in many communities across the UK in the form of telecentres. In 1998 there were approximately 200 of these centres, many of which offered access to ICT, training courses and advice and support relating to IT. Despite the existence of these centres, planned government provision did not acknowledge the role of telecentres in community based access to ICT. Furthermore, development of government initiatives continues at such a speed that insufficient attention has been paid to learning lessons from existing CBA, such as telecentres. Whilst the motives behind government projects for increasing skills levels and preventing social exclusion are good, the argument here is that time should be spent examining the extent to which people use _existing_ CBA sites. Put simply, if there are factors affecting use of existing CBA sites it is important to identify these factors _before_ further CBA sites are developed. Failure to do so may result in new initiatives failing to meet the needs of the communities that the provision has ostensibly been designed for. Unfortunately, such research had not been carried out to inform the development of government policy in this area. It is within this context that the present study was undertaken and the aims and objectives of the study set to address this gap in research.

## Objectives

The study had everyday life as its focus because government plans for provision of access to electronic information promise to transform everyday life ([Blair](#ref3), 1999). Consequently, everyday information seeking was chosen as the subject of this research. As telecentres are the most long-standing and established example of community based access to ICT they provided the focus for this research. The study was qualitative and exploratory because it was the first of its kind. No previous work was identified which examined use of electronic information and use of telecentres to access electronic information from the viewpoint of users and non-users. This study addressed this gap in knowledge and it is hoped that it will represent a starting point for further research, perhaps on a larger scale, which could confirm or disprove findings from this small scale, in-depth study.

This study explored the use of electronic information in the context of everyday life. This was achieved by focusing on one example of community based access, the telecentre, to investigate the extent to which these facilities were being used by people in communities to access electronic information. The study examined access and use of electronic information and telecentres from the viewpoint of the user and non-user to identify factors affecting access and use of both electronic information and telecentres. The study also examined the use (and non-use) of community involvement strategies in the design, development and implementation of telecentres.

The aims and research questions were based around the three central themes which are outlined below. Firstly, the study sought to examine and analyse the use of electronic information in the context of everyday life, by focusing on five questions:

*   What information do people need in their everyday lives?
*   How do they approach finding it?
*   To what extent is electronic information used in everyday information seeking?
*   Do some information needs lend themselves more favourably to using electronic information?
*   What factors affect use of electronic information?

The second theme of the study was to identify and analyse the use and non-use of telecentres by local communities by addressing two questions:

*   To what extent are telecentres used by people in communities to gain access to electronic information?
*   What factors affect access and use of telecentres?

Finally, the third theme related to identification of community involvement strategies used to include communities in the design, development and implementation of telecentres. This theme was addressed by examining strategies used, and to what extent and in what ways communities had been involved.

Results from the study were then used to propose a framework to inform future community based ICT initiatives. Thus the study's contribution to knowledge consists of pioneering empirical research and the development of a policy framework drawn from a community perspective.

As previously stated there was a need for research which examined what current CBA sites were being used for and the extent to which electronic information was being used by communities so that such research could be used to inform future community based ICT initiatives. Past research ([Ellen](#ref21), 1997) and involvement in [Communities on-line](http://www.communities.org.uk) also pointed to a need to explore the issue of community involvement in the design, development and implementation of CBA sites. The literature review outlined past research which focused on three different examples of CBA sites. A review of previous studies revealed a paucity of research addressing the questions posed for this study. Few studies were identified which focused on users of CBA sites, the majority of work in this area was from an organizational perspective, involving interviews with staff and key actors. This study addressed this gap in research by interviewing users, non-users and key actors to provide a more complete picture of telecentre use in everyday life _from the viewpoint of the community_, rather than from an organizational standpoint, an area which has received little attention either in past research, or by government policy makers. This study demonstrated the contribution that community members can make in informing policy (and thus future development), simply by asking them in a non-intrusive way to talk about _how and for what purpose_ they use CBA. Results were then used to develop a framework to inform future community based ICT initiatives.

## Literature

The literature review was based around the study's three themes. Models of information seeking were explored, focusing on user-centred models and previous studies which included categorisation of information and sources (relevant to the research question of how people approach finding information needed in everyday life). There are various reviews of literature in this area which are useful starting points for identifying relevant models ([Dervin and Nilan](#ref15) (1986), [Hewins](#ref26) (1990), [Siatri](#ref53) (1999) and [Sugar](#ref59) (1995). Amongst the most widely cited user-centred models considered were: [Wilson](#ref62) (1981) whose model has been called the holistic model; Dervin ([1976](#ref11), [1983](#ref13), [1992](#ref16)) whose model is called Sense-making; Kuhlthau ([1991](#ref29), [1990](#ref28), [1993](#ref30)) who developed the 'information search process'; and [Marchionini](#ref41) (1995) who focused specifically on information seeking in electronic environments.

### <a name="sense"></a>Sense-making

This paper will concentrate on Sense-making, which was used as a framework for this study. Sense-making has been utilised in studies across a range of disciplines ([Dervin](#ref13), 1983). The most notable of these relate specifically to everyday information seeking ([Dervin](#ref11) 1976; [Chen and Hernon](#ref9) 1982 and [Dervin](#ref14), 1984). The combination of relevance to the present study and the approach this framework used for data collection made it the most appropriate model amongst those reviewed.

Dervin argues that information seeking and use occur when individuals find themselves unable to progress through a particular situation without forming some kind of new 'sense'. Central to this idea, is the notion that information needs are situationally bound. To examine information seeking and use, Dervin developed the situation-gap-use model. The framework was originally developed to:

> ...assess how patients/audiences/users/clients/citizens make sense of their intersections with institutions, media, messages, and situations and to then apply the results in responsive communication/information system design. ([1995](#ref17): 5).

Dervin's research background is communication studies, which may explain the wide focus of her work. Wilson's call for a holistic approach ([Wilson](#ref62), 1981) focuses on information, whilst Dervin's interest in the relationships between institutions, media, messages, and situations promised a wider view of information seeking in the context of everyday life.

Another aspect of Sense-making that suited this exploratory study, was the approach taken to data collection. Open-ended questions (within a structured interview framework), which avoid leading respondents into talking about what they perceive is required of them is a characteristic of this method ([Dervin](#ref16a), 1989). Data is collected by framing questions which ask respondents to identify situations, gaps faced, gaps bridged, and helps them to make sense of situations. The key technique used in interviews to collect data is the 'micro moment time line interview'. Respondents are asked to reconstruct a situation recounting the steps that occurred (time line steps). Further detail is then requested for each step. The aim of this process is to gain an understanding of how the respondent saw the situation, gap and help; i.e., where they wanted to be after crossing the bridge. This technique has been compared to critical incident technique, developed by [Flanagan](#ref21d) (1954). Critical incident technique has been successfully used in LIS research ([Fisher and Oulton](#ref21c), 1999) and whilst it was considered, Sense-making was chosen in preference to Flanagan's method because of its wider focus. Critical incident technique involves asking respondents to discuss an incident in detail (like the micro moment time line), but it differs from Sense-making in being very focused, whilst Sense-making takes a wider approach, not wanting to impose the researcher's interest on the respondent. This allowed electronic information use and telecentre use to be explored in the context of everyday life, rather than asking respondents to discuss an incident where they had used electronic information or the telecentre.

In assessing the suitability of Sense-making as a framework, critiques of Dervin's work were sought. Only one was identified ([Savolainen](#ref49), 1993) which examined the differences between Sense-making and other approaches. Other researchers that had used Sense-making did not reflect on the utility of Sense-making as a framework for research. For example, [Durrance](#ref20a) (1982) used aspects of a Dervin research instrument, but did not comment on its utility.

### <a name="info"></a>Categorising information sources

A number of studies were explored as part of the process of trying to identify ways of categorising information sources. Broadly speaking three schools of thought emerged:

*   those who broke sources down into 'formal' and 'informal' [Bergeron and Nilan](#ref2a) (1991), [Secker _et al._](#ref1a), (1997), [Vaughan](#ref59b), (1997) and [Eager and Oppenheim](#ref21a) (1996)
*   those who discussed use of 'interpersonal' sources: [Agada](#ref1a) (1999), [Case _et al._](#ref6a), (1985), [Chen and Hernon](#ref9) (1982), [Chatman and Pendleton](#ref9a) (1995), [Cobbledick](#ref10a) (1996) and [Summers, _et al._](#ref59a), (1983)
*   those who categorised sources as 'channels' [Bergeron and Nilan](#ref2a) (1991) and [Bruce](#ref5a) (1995)

Comparing and contrasting these studies was problematic, because a definition of what was meant by the terms formal, informal, interpersonal, etc. was not always present. For the purposes of the study the following definition of formal and informal sources was used:

> A formal source is defined as a source, oral or recorded that responds, and is perceived as - and is intended to be - relevant to a particular problem that is officially put in place by an organisation (the software vendor, the work place, etc.) to help users [to learn how to do word-processing]. An informal source is defined as a source that is used outside the formal channels. ([Bergeron & Nilan](#ref2a), 1991: 24).

Whilst it was possible to utilise a definition of formal and informal information sources no suitable categories for information sources were identified. This lead to the development of tactics categories during the study, which were used to address the research question 'how do people approach finding information in everyday life?' This is [discussed below](#results).

### <a name="eisb"></a>Everyday information seeking

Within the field of information seeking research there is a discrete area variously termed 'everyday information seeking' ([Dervin](#ref11), 1976) 'everyday life information seeking' ([Savolainen, 1995b](#ref51)), 'non work information seeking' ([Chen and Hernon](#ref9), 1982) and 'citizenship information seeking' ([Marcella and Baxter](#ref40), 2000). Closely related to these studies are those which consider 'community information' ([Leech](#ref34), 1999). [Chen and Hernon](#ref9) defined everyday information seeking as:

> ...all knowledge, ideas, facts, data and imaginative works of the mind which are communicated formally and/or informally in any format. ([1982](#ref9): 5)

This definition places information in its widest context and enables an open exploration of everyday information seeking appropriate to the aims of this study.

The origin of studies which explore everyday information seeking can be traced back to the United States, where, in the 1970s and 1980s a number of studies were undertaken which focused on the general population. These studies involved interviewing the general public about their everyday information needs. The most widely cited of these studies is the Baltimore study ([Warner, _et al._](#ref59), 1973). Building on the results and data analysis schemes of the Baltimore study were the Seattle study ([Dervin, _et al._](#ref12), 1976), the New England Study ([Chen & Hernon](#ref9), 1982) and the California study ([Dervin, _et al._](#ref14), 1984). There were three common elements across these four studies. Firstly, the use of a coding scheme for everyday information needs. The scheme was developed to provide a set of 'mutually exclusive and exhaustive categories into which all citizen information needs could be classified' ([Dervin](#ref11) 1976: 23). The scheme consisted of 19 major categories and 154 subcategories. This scheme was the most extensive classification of everyday information needs identified, and was adopted and subsequently extended for this study.

The second common element amongst these studies was the approach taken to examining everyday information seeking, which aimed to see 'information' in its widest sense. This was achieved by avoiding use of the word 'information' during interviews, instead asking respondents to discuss recent situations where they 'needed to find the answer to a question, solve a problem, make a decision...' ([Chen & Hernon](#ref9) 1982, p.25) and to describe what had helped and what had hindered in dealing with the situation. This open-ended approach was designed to develop a picture of the total information environment, rather than just focusing on formal sources. The key reason for this approach was that previous research had identified (and continues to identify) a preference for using informal information sources. A recent study of the information seeking behaviour of senior executives also adopted this approach after a pilot study revealed that use of the term 'information' was limiting the sources that respondents said they used because they did not consider talking to colleagues as part of information gathering ([Keane](#ref28a), 1999).

The third common element across the four studies was their size. The Baltimore study involved around 1,300 participants, the Seattle study interviewed 500, the New England study 2,400 people from six states, and the California study interviewed 1,040 people. Whilst these studies used interviews, rather than surveys for data collection, a further common element was that the analysis and results from these large scale studies were wholly quantitative, comprising statistical tables with little or no examples of information needs and how people had dealt with them. Considering the Sense-making framework within which these studies were undertaken which calls for a user-based perspective, rather than a system-based perspective, this trend was disappointing. No smaller scale studies, using the common framework were identified which took a more qualitative approach. In this respect, the present study differs because it **_is_** small scale and departs from this wholly quantitative approach to analysis by providing a mix of qualitative and quantitative treatment of the data. This allowed for more in-depth analysis of everyday information seeking, and the extent to which electronic information is used.

More recently, little research has been published which examines everyday information needs across the general population, as did the four studies described above. A number of studies were identified which focused on a subset of the general population. In 1996, 300 low income African Americans in Texas were surveyed, and in-depth interviews undertaken with 50 volunteer households ([Spink _et al._](#ref56), 1999). [Harris and Dewdney](#ref25) (1994) examined the effectiveness of information and referral agencies (called formal help systems) in meeting the needs of abused women, concluding that most of the available services were inadequate. Williamson ([1997](#ref60) and [1998](#ref61)) studied 202 older adults (over 60) in Australia focusing on 'incidental information acquisition' (p.23). Williamson termed her study 'ecological' because it built a picture of information in the context of the lives of the people in the sample. The concept of incidental information acquisition distinguishes between behaviour where the search for information is the primary motive and where it is incidental. A similar research theme was explored by [Ross](#ref48) (1999) whose qualitative study of 194 pleasure readers included instances where they had read a book for pleasure that had made a significant difference in their lives (particularly after reading biographies, autobiographies, histories and novels). As a result of this research Ross called for an expanded definition of information seeking and use, to include books that are read for pleasure. Savolainen ([1995a](#ref50), [1995b](#ref51)) interviewed 22 Finnish residents and developed a framework for the study of everyday information seeking (called way of life and mastery of life).

<a name="blstudy"></a>One exception to the trend of selecting a subsection of the population, was a national study of citizenship information needs of the general public ([Marcella and Baxter](#ref40) 2000) undertaken in the UK. The study, funded by the British Library, comprised four parts. Initially a survey by questionnaire was carried out, completed by 1294 respondents. This was followed up by a more in-depth survey by interview of 898 respondents. The third stage involved focus group meetings where major findings from the initial survey were presented for comment. Finally, case studies of 27 organizations were undertaken ([Marcella and Baxter](#ref40) 2000). Whilst Marcella and Baxter did not chose a subset of the population, they did narrow their focus to 'citizenship information'. This was defined as:

> ...information produced by or about national and local government, government departments and public sector organizations which may be of value to the citizen in government and policy formulation.' ([Marcella and Baxter](#ref40d), 1999: 1)

This definition omits an important source of information of value to citizens as part of their everyday life; information provided by non governmental organisations, voluntary groups and community organisations. The more common term for these information sources is 'community information' ([Leech](#ref34), 1999). Marcella and Baxter do not explain the rationale for excluding these information sources. For a more detailed discussion of the Marcella and Baxter study, see [Ellen](http://www.mmu.ac.uk/h-ss/dic/research/ellen/contents.html), 2000: 30-33\.

### Factors affecting everyday information seeking

The review focused on barriers to everyday information seeking. The most comprehensive of those discussed in the literature were those put forward by [Chen and Hernon](#ref9) (1982), which are summarised as follows:

*   **societal**: impede the availability of resources necessary to satisfy needs within the social system
*   **institutional**: the incapacity and/or unwillingness of an information provider to deliver needed information to a certain type of seeker
*   **physical**: impose themselves when an individual is unable to make contact with the appropriate information providers due to some physical consideration
*   **psychological**: when the individual is unable to perceive their needs as informational in nature, obtain needed information from appropriate providers or accept the possibility that the information problems can be solved for psychological reasons
*   **intellectual**: when the individual lacks necessary training and expertise to acquire information ([Chen & Hernon](#ref9) 1982)

Discussion of other barriers was less extensive. [Dervin](#ref11) (1976) discusses barriers to information accessibility between the citizen and the information source, particularly bureaucratic barriers (which links to Chen and Hernon's institutional barrier). Both [Dervin](#ref11) (1976) and [Chen and Hernon](#ref9) (1982) discuss the tendency for agencies to consciously, or unconsciously withhold services from citizens with the most difficult problems to make their success statistics look more favourable. Studies cited by [Chen and Hernon](#ref9) (1982) which discuss withholding services include [Levin and Taube](#ref35)(1970) and [Sjoberg, _et al._](#ref55), (1966).

Williamson ([1997](#ref60), [1998](#ref61)) found that amongst 202 elderly Australians barriers included cost and fear. These findings have implications here in the UK, in terms of government plans to move provision of information and services to electronic format, within a country where a large proportion of the population is over 60, out of the educational loop and for whom lifelong learning may not seem appropriate in retirement ([Blake](#ref4), 1998). Other studies that focused on older people and ICT showed that despite ageist attitudes about older people not wanting to embrace new technology ([Sixsmith & Sixsmith](#ref54), 1995), such attitudes are not supported by research. The BBC programme _You and Yours_ ([Carr](#ref6), 1999) featured 'silver surfers', reporting a growing number of older people using the Web.

Consideration of access to electronic information for those whose first language is not English is also an important issue. The language barrier and strategies to address access to electronic information was the focus of a project which ran between 1997 and 1998 at Toronto Public Library in Canada ([Lavery and Livingston](#ref33), 1999). The project involved developing a programme for introducing adult learners to the Internet, focusing on those with low literacy levels and people for whom English is a second language (ESL). Experience from this project revealed that categories of information of particular interest for ESL users were international daily newspapers, interactive ESL quizzes and puzzles and e-mail (for contacting relatives abroad).

Software that enables blind and partially sighted people to hear the content of electronic information spoken to them (programs such as JAWS and HAL) have been found to be useful to people with low literacy levels and people with dyslexia. However during the period of study these programs were not usually available at public access sites because of their high cost (JAWS costs 2,500 for a five-user licence). In Bury, Greater Manchester the visually impaired persons librarian found that reader software (PW Webspeak) was invaluable for people with low literacy levels and dyslexia because it helped them to practice their readings skills ([Making the Connection seminar](#ref40a), 1998). This demonstrates that there is clearly benefit to a wide range of people who may have difficulty accessing electronic information in having such programs within CBA sites.

### Update on everyday information seeking research

Since the PhD was submitted in January 2000, a number of authors have published papers relating to everyday information seeking. The subject of everyday information seeking has been featured at the third ISIC conference (The Third International Conference on Research in Information Needs, Seeking and Use in Different Contexts, [Hoglund and Wilson](#ref26b), 2001) and in a special journal edition ([Spink & Cole](#ref56a), 2001). This research activity demonstrates that the area of everyday information seeking is one of growing interest. The studies published since January 2000 can be broken down into those which focus on particular groups of the population, those which focus on a topic for information seeking and those that provide a methodological contribution to this area of research. The following sections briefly outline these studies.

#### Studies of particular groups

Within the most recent literature studies of particular groups within the general population form the largest group of papers on everyday information seeking. [Wicks](#ref59c) (2001) focused on Canadian independent older adults in a qualitative study which investigated whether the degree of independence of older adults influenced the types of sources chosen. The study also examined whether electronic information was an effective form of communication amongst older adults. Another study which focused on adults ([Klobas and Clyde](#ref28e), 2000) looked at their Internet use. Three studies examined issues associated with access to information for disabled people ([Craven](#ref9b), 2000; [Schmetzke,](#ref51b), 2002 and [Williamson et al](#ref61a), 2001). Four studies focused on women, ([Davies and Bath](#ref9c), 2002; [Harris _et al._](#ref25a) 2001; [Higgins and Hawamdeh](#ref25c), 2001 and [Marcella](#ref40b), 2001). [Given](#ref22a) (2002) examined the overlap between everyday and academic information seeking behaviour amongst mature undergraduate students. [Hersberger](#ref25b) (2001) utilised Dervin's categories developed during the Seattle study ([outlined above](#eisb)) for a study which focused on the everyday information needs and sources of homeless parents.

In addition to the studies listed above which focus on particular groups within the general population, there are also a growing number of studies which focus on a specific topic. For example, [Cline and Haynes](#ref9aa) (2001) and [Khalil](#ref28d) (2001) focus on consumer health information. [Nicholas _et al._](#ref43b) (2002) have been studying the 'digital information consumer' in a large scale study using log files to examine use of online news sites and health sites such as NHS Direct. [Savolainen](#ref51a) (2001) focused on consumer information within an Internet newsgroup.

#### Methodological contributions

Methodological contributions include [Marcella and Baxter](#ref40c) (2001) who provide a detailed description of their 'random walk sample', which was used during a British Library funded study [outlined above](#blstudy). This sampling method was developed from Market Research providing detailed instructions to interviewers enabling them to be rigorous about their choice of homes to sample. [Carey _et al._](#ref6aa) (2001) discuss the issues associated with studying three distinct groups of the population (pregnant women, self-help support groups and pre school children). These papers provide important details about qualitative studies of everyday information seeking.

### Community based access

In reviewing the literature on community based access three different types of access were examined; telecentres, libraries and community networks. The decision to concentrate on telecentres for the study was made because of these three types of access, telecentres were the only CBA sites where the main focus was provision of ICT access and promotion of the use of ICT. Community networks tend to concentrate on development of community information and libraries main focus is on book lending and paper based reference services. Since the study was completed many branch libraries have opened ICT Learning centres as part of the establishment of the People's Network, but this is still not their primary focus.

Although there is no precise definition of a telecentre, as each offers services relevant to its users, [Graham](#ref23) (1992) found that they shared a common objective:

> ...to promote local social and/or economic development by making available information technology, goods and services and skills to economic and social groups in the local area previously with poor access'.([Graham](#ref23), 1992: 3)

Research into telecentres (also known as telecottages, community technology centres, CTSC or community teleservice centres, electronic village halls and most recently ICT learning centres) has tended to concentrate on organizational aspects of these centres such as the importance of clear aims and objectives, issues around sustainability, balancing economic and social objectives, critical success factors, management and control structures, importance of evaluation, etc. ([Staplehurst](#ref57), 1994; [Burt](#ref5), 1995; [Lancaster](#ref31), 1999). [Graham](#ref23) (1992) was alone in proposing best practice in developing telecentres. This work was based on research carried out in East Manchester by the Centre for Applied Social Research (CASR) in the early 1990s ([Ducatel _et al._](#ref20), 1993; [Ducatel and Halfpenny](#ref19), 1993; [Shenton _et al._](#ref52), 1991). Two reports commissioned by IBM as part of their _Living in the Information Society_ project examined community based ICT initiatives in a wider sense (telecentres and community networks), with a focus on social inclusion ([INSINC](#ref27), 1996; [Day & Harris](#ref10), 1997). [Lancaster](#ref31) (1999) examined telecentres with some association with libraries observing that it had proved difficult to find three case study sites where the telecentres were thriving.

The review identified a significant gap in research in this area; work which examined telecentres from a community-centred, rather than organizational perspective. What were people using telecentres for, why were some people not using them, what prevented use and what helped? Of the studies identified only two ([Mark _et al._](#ref42), 1997 and [Liff _et al._](#ref38), 1999) began to address these issues. [Mark _et al._](#ref42), (1997) examined the impact of Community Technology Centres (CTC). The study focused on the impact of educational programmes, rather than drop-in access to ICT and did not involve interviews with non-users. [Liff _et al._](#ref38), (1999) compared telecottages (another terms for telecentres) and cybercafes in an international study, which in the later stages of the two year project (November 1997-October 1999) included questionnaires for users and non-users. What makes the present study distinctive, when compared with the [Liff _et al._](#ref38), (1999) is that the views of users and non- users take centre stage, making the present study the first truly _community-centred_ study.

At the time of the study, libraries were only just beginning to provide community access to ICT, when compared to telecentres, some of which had been established as early as 1989\. Indeed, in the 1998 edition of Information Technology in Public Libraries Batt recalled that when data was collected for the 1994 edition 'the Internet did not fall within the consciousness of the public librarian' ([Batt](#ref1), 1998, p.15). Since [Ormes and Dempsey](#ref44) (1995) surveyed library authorities in 1995 growth has been rapid, with public access points increasing from 39 in 1995 to 321 two years later ([Batt](#ref1), 1998, p.17). Planned growth with the People's Network is for 40,000 access points as part of a vision for delivering what the public expect from a library in the 21<sup>st</sup> century ([Library and Information Commission](#ref36), 1997a). Since the present study was completed this People's Network is being implemented, with a target of 4,300 UK online centres to be established within public libraries, which form part of an overall figure of 6,000 UK online centres across the country by the end of 2002\.

### Factors affecting access and use of telecentres

[Qvortrup](#ref46) (1994) summarised barriers to access and use of telecentres which are presented in Table 1 below:

<table><caption>Table 1: Qvortrup barriers</caption>

<tbody>

<tr>

<th>Barrier</th>

<th width="462">Solution</th>

</tr>

<tr>

<td>

**network barrier**; a lack of access to telecommunications networks</td>

<td>Where individual connections to the network are not viable, provision of access at a single place (i.e. a community access point such as a telecentre) may be cost effective.</td>

</tr>

<tr>

<td>

**service barrier**; where access to the telecommunications network exists, but where only limited services are available, which may not be appropriate to a community's needs (for example services may be designed for an urban environment and are not applicable to a rural situation)</td>

<td> to provide not only telephone services, but also computer and multimedia-based services such as access to databases, to training via distance learning, etc. within a contact that can be orientated to local needs.</td>

</tr>

<tr>

<td>

**cost barrier**; the cost of purchasing equipment (and ongoing costs) is prohibitive</td>

<td> to provide communal access to IT equipment, reducing the individual costs (i.e., at a community access point such as a telecentre).</td>

</tr>

<tr>

<td>

**qualification barrier**; skills required to use computers are high, and may not always be present. Additionally, some may be sceptical about IT.</td>

<td> by integrating on-the-spot training courses and advisory activities into the telecentre. This requires that telecentres are run by trained and competent persons.</td>

</tr>

</tbody>

</table>

Other research which has discussed factors affecting access and use of community based access to ICT identified lack of interest, relevance and an absence of appropriate skills within the community in order to make services accessible as issues in establishing a telecentre ([Ducatel & Halfpenny](#ref19), 1993). These could be placed within Qvortrup's qualification barrier. A community audit ([Shenton _et al._](#ref52), 1991) revealed a series of concerns amongst community members which indicated a lack of involvement by the community in the process of establishing a telecentre in the area. People did not see why funding was available for the centre when other community services were cut, could not see what benefits the telecentre would bring that were not achievable by other means and were concerned about limited staffing of the centre and an absence of specific outreach workers.

### Community involvement

The example of the East Manchester study demonstrates the importance of community involvement in the design, development and implementation of community based ICT initiatives. The work undertaken by CASR ([Shenton _et al._](#ref52), 1991; [Ducatel & Halfpenny](#ref19), 1993; [Graham](#ref23), 1992) highlighted problems associated with adopting a top down approach to community based ICT initiatives. More recent studies have also indicated the importance of community involvement ([INSINC](#ref27), 1996; [Day & Harris](#ref10), 1997). Examples of good practice in this area were identified. In Manchester, the Women's Electronic Village Hall (at the time only an idea) carried out a feasibility study, asking local women what they thought about establishing a women-centred IT centre ([Girbash](#ref22), 1991). In Hereford and Worcester a network of Community Resource Centres, developed at the request of local communities, are run by the communities themselves. In recent years PCs with Internet access have been added, [but only where communities involved have wanted this to happen](#note2).

In County Durham members of Trimdon village carried out a village appraisal which included a positive response to a question about establishing a 'community based information and communications centre'. The results provided a picture of what villagers hoped Trimdon could become by the year 2000 ([CIRA](#ref8), 1997). These examples counter the commonly held view that communities cannot be involved in the design, development and implementation of community based ICT initiatives because they do not know what can be achieved with technology ([Mitchell](#ref43), 1999). One idea that moves towards addressing the issue of how to involve communities in ICT initiatives, particularly during development, is a game developed by [Mackie and Wilcox](#ref39) (1998) for Communities Online. The game was developed as a way of bridging the gap between communities and IT developers and was designed to facilitate communication by both parties about what was needed/could be achieved.

## Methods

The aims and objectives and how they linked together are shown in Figure 1 which provided a conceptual framework for the study.

<figure>

![Figure1](../p146fig1.jpg)</figure>

The conceptual framework illustrates how the three key themes from the study were developed, and how the different research questions were addressed to generate data used to form a framework for future community based ICT initiatives. Data was collected in 1998, with a pilot study of an urban telecentre undertaken to test all aspects of the research instruments to be used for the main study. Following the pilot, two telecentres were studied, one rural, one urban and the aim was to interview ten users and ten non-users as well as key actors (those involved in establishing telecentres or running them) at each site. The exploratory case study method ([Yin](#ref63), 1994) was chosen, and the study can be described as two exploratory case studies at the level of the telecentres, combined with an exploratory purposive sample at the level of the key actors within telecentres, telecentre users and non-users. A total of forty-seven users and non-users was interviewed across the study. Telecentre sites and respondents were given anonymity. Consequently, the names of the places where the telecentres were located have been changed.

For theme one (use of electronic information in everyday life), Sense-making ([Dervin](#ref13)1983) was used for data collection (see [Sense-making above](#sense)). A research instrument developed from a previous study ([Dervin _et al._](#ref12), 1976) was adapted with respondents asked to detail everyday 'problems, worries and concerns' that they had faced in the past month and how they had dealt with these situations. Respondents were asked open-ended questions without using the terms 'information', 'information needs' and 'sources of information'. This open-ended approach to the question of information needs and use addressed research questions about information needed in everyday life and how people approach finding it.

This method enabled the study to determine the extent to which electronic information had been used to deal with everyday situations without deliberately asking direct questions about electronic information use. Once data relating to information needs and use had been gathered, respondents were asked to talk about use of electronic information in relation to a series of hypothetical questions. Hypothetical situations were used to enable comparisons between respondents information seeking strategies. This generated a number of factors affecting use of electronic information which, in addition to respondents own everyday situations which had involved electronic information use, were used to develop the framework to inform future community based ICT initiatives.

For theme two (use of telecentres in everyday life), interviews with telecentre users **_and_** non-users were carried out, using Sense-making. Use of this method enabled the study to develop a picture of electronic information use at telecentres which did not lead respondents into talking specifically about use of electronic information, thus achieving the aim of establishing the _extent_ to which telecentres were being used to access electronic information. This method was also used to identify factors affecting use of telecentres by asking users what 'helped and hindered them' when using the telecentre. Non-users were asked what hindered them, and what would help them to use the telecentre. This data was used to inform the framework for future community based ICT initiatives.

Finally, the third theme (identification of community involvement strategies used to include communities in the design, development and implementation of telecentres) was addressed by asking key actors about past and current community involvement strategies. In addition, any comments made by users and non-users (i.e. helps and hindrances in using the telecentre) were also utilised. Results from this aspect of the study were also incorporated into the framework to inform future community based ICT initiatives.

### Data analysis

Initial coding was carried out by detailed examination of transcripts using Atlas/ti, a software package designed to process qualitative data for analysis (see [www.atlas.de](http://www.atlas.de), Kelle, [1996](#ref28b), [1997](#ref28c)). Codes were assigned inductively from the data using Atlas/ti which led to generation of summary tables showing occurrences of different tactics (strategies) for dealing with information needs. Detailed analysis of hypothetical situations was possible because of the small scale of the study, enabling analysis by individual as well as across each case study site. This method of initial analysis is illustrated in Table 2 below.

<table><caption>Table 2: Example of Atlas/ti output</caption>

<tbody>

<tr>

<td></td>

<th>Primary Documents</th>

</tr>

<tr>

<th>Codes</th>

<th>35</th>

<th>38</th>

<th>41</th>

<th>43</th>

<th>44</th>

<th>47</th>

<th>48</th>

<th>49</th>

<th>50</th>

<th>52</th>

<th>Totals</th>

</tr>

<tr>

<td>Dog: Action unspecific</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Dog: befriend dog</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Dog: CAB</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Dog: Council (action)</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>6</td>

</tr>

<tr>

<td>Dog: Council (refer)</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Dog: direct action</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Dog: Ignore it</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>3</td>

</tr>

<tr>

<td>Dog: Legal advice</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Dog: mediation</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Dog: Move house</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Dog: move it</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Dog: neighbour again</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Dog: Neighbour Co-op</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Dog: neighbour response</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Dog: Police (action)</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

</tr>

<tr>

<td>Dog: Police (info)</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Dog: Pray</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Dog: reason for noise</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>2</td>

</tr>

<tr>

<td>Dog: Rescue</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Dog: RSPCA?</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>2</td>

</tr>

<tr>

<td>Dog: shut windows</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Dog: speak to neighbour</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>10</td>

</tr>

<tr>

<td>Dog: Walk dog</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Totals</td>

<th>5</th>

<th>5</th>

<th>4</th>

<th>3</th>

<th>3</th>

<th>4</th>

<th>2</th>

<th>3</th>

<th>1</th>

<th>4</th>

<th>34</th>

</tr>

</tbody>

</table>

In Table 2 above columns 2-11 in the table (headed with 'primary documents') represents an individual respondent and the codes in column one show the various tactics given by each respondent for this hypothetical situation (barking dog). For example, respondent 35 mentioned five tactics for dealing with this situation; contact the council for action, seek legal advice, move house, work co-operatively with other neighbours and speak to the neighbour with the dog. Further conceptual analysis was undertaken by developing three new methods for analysis of the strategies used in everyday information seeking; 'tactic analysis', 'tactic categories' and the 'centrality of electronic information use', which are discussed below. Use of Atlas/ti (as a means of managing a large volume of data) enabled development of these three methods of analysis which represent the first qualitative analysis of everyday information seeking. These are discussed below.

#### 'Tactic analysis'

When analysing the data collected on everyday information seeking the basic numeric representation of whether a respondent had mentioned a tactic or not (as shown in Table 2 above) was supplemented by in-depth analysis, which will be called 'tactic analysis'. This was achieved by revisiting verbatim data and noting the stage at which a code was mentioned in a respondent's description. Additional data displays were created by replacing the 0s and 1s (as shown in Table 2 above) with a number that corresponded to the order in which each tactic was used.

This original technique enabled analysis of the point in the information seeking process that each 'tactic' was used - rather than just showing that a tactic had been used. As one of the aims of Sense-making was to elicit step by step descriptions of what people did when dealing with situations (or might do when discussing hypothetical situations) this approach provided a summarised view of each respondents narrative of how they had moved through everyday situations. This technique meant that it was possible to examine each respondents individual path through a situation providing a level of detail about everyday life information seeking not seen before. 'Tactic analysis' also meant that it was possible to identify in what context respondents had used electronic information. Table 3 below provides an example of 'tactic analysis' for the hypothetical situation involving a health problem requiring a second opinion for one respondent. By using the codes developed from the data, and ordering the step by step description in this way, we can clearly see that this 58-year-old woman said that she would initially use the library, looking for subject specific information by reading books. She also said that she would use the Web, and would try to contact other sufferers. The data displays developed provide the full list of tactics developed from the study, so we can not only see the tactics each respondent said that they would use, we can also see the range of tactics other individuals mentioned. For example, whilst the respondent featured in Table 3 mentioned 5 tactics, across the study, 36 different tactics were mentioned.

<table><caption>Table 3: 'Tactic analysis'</caption>

<tbody>

<tr>

<th>Tactics</th>

<th>Steps</th>

</tr>

<tr>

<td>Library use</td>

<td>1</td>

</tr>

<tr>

<td>subject specific information</td>

<td>2</td>

</tr>

<tr>

<td>read books (web)</td>

<td>3</td>

</tr>

<tr>

<td>Internet (web)</td>

<td>4</td>

</tr>

<tr>

<td>other sufferers</td>

<td>5</td>

</tr>

<tr>

<th>Total tactics used</th>

<th>5</th>

</tr>

</tbody>

</table>

With the hypothetical situations, 'tactic analysis' was then used to compare individual responses across the study and to compare frequency information (as shown in Table 3 above) with whether there was agreement across the study regarding which tactics would be used first or second (called prime tactics). Summarised tables were then created for both frequency data and prime tactics. Once again, 'tactic analysis' also provided a way to show the extent to which use of electronic information fitted in to respondents tactics for the hypothetical situations, and whether electronic information was seen as a prime tactic across the study.

#### 'Tactic categories'

Following on from 'tactic analysis', a set of conceptual analysis categories was developed (called 'tactic categories') under which to group tactics: these were utilised for both the real and hypothetical situations. The 'tactic categories' were developed because categories suggested by [Dervin](#ref18) (1999) which used Sense-making did not meet the objectives of the study. [As discussed above](#info), investigation of other studies to identify categories for grouping information seeking strategies also failed to yield any useful categories. Consequently, the categories developed during the present study were:

*   information sources (which included formal and informal sources);
*   assessments (used where respondents talked about what they would want to know or need to find out and then assess to deal with a situation);
*   actions (where respondents mentioned actions they would take as part of dealing with a situation);
*   issues/questions (tactics which involved issues or questions relating to a situation).

These 'tactic categories' enable _all_ information seeking strategies to be examined and contrasted with other studies.

#### Centrality of electronic information use

Use of 'tactic analysis' and 'tactic categories' outlined above also enabled the development of a concept called the 'centrality of electronic information use' for examining the extent to which electronic information was used in respondents' everyday situations. Three levels of centrality have been used, denoting how central use of electronic information was in dealing with the situation:

*   Only electronic information was used
*   electronic information was central to dealing with the situation
*   electronic information was not central to the situation

The 'centrality of electronic information use' was utilised for respondents' real situations and not the hypothetical situations because respondents were hypothesising about how they would deal with a situation, not describing actual strategies they had used. This method of analysis, when used in conjunction with 'tactic analysis' and the 'tactic categories' described above could be used to determine the level of centrality of other media or particular information sources.

## <a id="results"></a>Results and discussion

As mentioned in the introduction, this paper concentrates on results relating to theme one of the study; use of electronic information in everyday life outlined above. Results across the whole range of everyday information needs are presented, to provide a context for those where electronic information was used. Results from the present study are also compared to those from the Seattle study ([Dervin _et al._](#ref12), 1976). Summarised information about one topic (consumer concerns) is provided to demonstrate how results were presented. Situations where electronic information was mentioned are summarised focusing on centrality of electronic information use. Examples of results for both real and hypothetical situations are given to illustrate how tactic analysis and tactic categories were used. Finally results relating to factors affecting use of electronic information are provided.

### Everyday information needs

Building on the topic categories and subcategories called 'topic focus codes' developed during the Seattle Study ([Dervin _et al._](#ref12), 1976) results were broken down into the areas shown in table 5.1 below. Results from the present study are compared with those from the Seattle study ([Dervin _et al._](#ref12), 1976) which involved 500 respondents.

<table><caption>Table 4: Information needs by topic focus categories across two studies</caption>

<tbody>

<tr>

<th colspan="3">Present study n=47</th>

<th>Seattle study n=300</th>

</tr>

<tr>

<th>Topic focus category</th>

<th>Number of mentions</th>

<th>% of mentions</th>

<th>% of mentions</th>

</tr>

<tr>

<td>Consumer</td>

<td>26</td>

<td>20</td>

<td>8</td>

</tr>

<tr>

<td>Employment</td>

<td>23</td>

<td>17</td>

<td>12</td>

</tr>

<tr>

<td>Education and Schooling</td>

<td>21</td>

<td>16</td>

<td>8</td>

</tr>

<tr>

<td>Child care family and personal</td>

<td>12</td>

<td>9</td>

<td>17</td>

</tr>

<tr>

<td>Financial matters</td>

<td>11</td>

<td>8</td>

<td>8</td>

</tr>

<tr>

<td>Miscellaneous</td>

<td>9</td>

<td>7</td>

<td>3</td>

</tr>

<tr>

<td>Recreation</td>

<td>6</td>

<td>5</td>

<td>2</td>

</tr>

<tr>

<td>Crime and safety</td>

<td>5</td>

<td>4</td>

<td>8</td>

</tr>

<tr>

<td>Housing</td>

<td>4</td>

<td>3</td>

<td>4</td>

</tr>

<tr>

<td>Health</td>

<td>4</td>

<td>3</td>

<td>11</td>

</tr>

<tr>

<td>Social Security</td>

<td>3</td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td>Internet related concerns</td>

<td>3</td>

<td>2</td>

<td>n/a</td>

</tr>

<tr>

<td>Transportation</td>

<td>2</td>

<td>2</td>

<td>4</td>

</tr>

<tr>

<td>Immigration</td>

<td>2</td>

<td>2</td>

<td>0.2</td>

</tr>

<tr>

<td>Legal</td>

<td>1</td>

<td>1</td>

<td>2</td>

</tr>

<tr>

<td>Maintenance</td>

<td>1</td>

<td>1</td>

<td>5</td>

</tr>

<tr>

<td>Neighbourhood</td>

<td>-</td>

<td>-</td>

<td>5</td>

</tr>

<tr>

<td>Discrimination and race relations</td>

<td>-</td>

<td>-</td>

<td>1</td>

</tr>

<tr>

<td>Family planning and birth control</td>

<td>-</td>

<td>-</td>

<td>0.1</td>

</tr>

<tr>

<td>Veterans and military</td>

<td>-</td>

<td>-</td>

<td>0.1</td>

</tr>

</tbody>

</table>

A new code category was created to cover 'Internet related concerns' as none of the categories from the Seattle study were suitable. Table 4 reveals that consumer-related situations predominated amongst the 47 people interviewed, with 20% of situations involving consumer-related concerns. As can be seen from the table, both studies had a broad spread of situations across the different topic focus categories, demonstrating the variety of information respondents needed in everyday life. The most frequently mentioned topics differed across the two studies, but three topic categories featured in the top five across both studies (child care, family and personal, employment and financial matters). Whilst the high number of mentions for the childcare family and personal category may be explained by its (too) wide topic focus, encompassing three separate areas of concern, this was not the case for employment or financial concerns.

Looked at another way, results from the present study show that across a range of information needs (broken down into the 16 topic focus categories) none covered more than 20% of the total. This was also true of findings from the Seattle study. The implications of these results for CBA sites relates to provision of access to electronic information (as more information is stored electronically). CBA sites need to be able to meet the wide range of information needs articulated during the present study. It is encouraging to note the similarity of results from a small scale in-depth study involving 47 respondents and a large scale study. Given the added benefits of in-depth analysis of respondents' approaches to finding information (not undertaken during the large scale studies) that were possible with a smaller number of respondents, this was a welcome outcome.

For the real situations, a wide variety of everyday situations was articulated. Fifty-eight different topic focus codes were used for interviews with 47 people, with 100 different situations presented. Whilst it is not possible to report full results in this paper, in order to provide an example of the results of the study, details of consumer concerns are provided.

#### Consumer concerns

Table 5 below shows codes, code descriptions and the number of mentions across the study. Nine different topics were used for consumer concerns. Six of these were within the twenty most frequently mentioned topics in the study. Consumer-Quality was the topic which appeared most frequently (six times). The Consumer category also provided seven out of the thirteen instances of electronic information usage.

<table><caption>Table 1: Qvortrup barriers</caption>

<tbody>

<tr>

<th>Code</th>

<th>Description</th>

<th>Mentions</th>

</tr>

<tr>

<td>Consumer-Quality</td>

<td>Poor quality of service, etc.</td>

<td>6</td>

</tr>

<tr>

<td>Consumer-Product Information</td>

<td>Which products to buy, where to buy, product price, etc.</td>

<td>5</td>

</tr>

<tr>

<td>Consumer-Other</td>

<td>Other consumer</td>

<td>4</td>

</tr>

<tr>

<td>Consumer-Service Availability</td>

<td>Inconvenient service, locations, hours, can't find, service unavailable, etc.</td>

<td>3</td>

</tr>

<tr>

<td>Consumer-Service Information</td>

<td>Which service to get, where to get, service price, etc.</td>

<td>3</td>

</tr>

<tr>

<td>Consumer-Prices</td>

<td>Prices high, cost of living too high, prices go up, prices too high, etc.</td>

<td>2</td>

</tr>

<tr>

<td>Consumer-Billing</td>

<td>Billed for items/services not received, charged to wrong account etc.</td>

<td>1</td>

</tr>

<tr>

<td>Consumer-Product Availability</td>

<td>Getting particular sizes, brands, etc.</td>

<td>1</td>

</tr>

<tr>

<td>Consumer-Product Quality</td>

<td>Fell apart, badly made, problems getting exchange or refund, repairs.</td>

<td>1</td>

</tr>

</tbody>

</table>

The 'Consumer-Other' concerns were about the following:

*   looking for a company logo (included using the Web);
*   looking for an image of a lobster (included using the Web)
*   trying to read a foreign language newspaper on the Web
*   concern about picking up computer viruses from telecentre PCs

Electronic information was mentioned under Consumer-Product-Information for hotels in New York and information about booking tickets at the Sydney Opera House; Consumer-Quality for problems related to finding information on the web about camping in Montpellier; Consumer-Service Information for information about travel times on buses and trains and Consumer-Other outlined above.

### Use of electronic information in real situations

Thirteen situations were described which involved using electronic information given by nine respondents out of a group of forty-seve, with two people mentioning more than one situation involving electronic information. Where these situations were described in detail, using Dervin's 'micro moment timeline interview' it was possible to place tactics in the order in which they were used because respondents were asked to describe the situation step by step. In addition, because of the small scale of the study it was also possible to present summarised results under tactic categories with each step numbered, so that it was possible to see what an individual did first, second, third and so on. While it is not possible to present these results in full, to illustrate this, one respondent's situation is presented here.

#### 'Betty's tactics'

Betty (a 60-year-old woman) described a situation where she had needed to know about local yoga classes. She had been advised by her physiotherapist to take up yoga, and wanted to attend local classes because she relied on public transport.

A wide range of both formal and informal information sources was used for this situation. The library was used as the seventh tactic, after she had exhausted other ways of finding out about yoga classes. It was when she made enquiries at the library that Betty began to feel frustrated. A Community Information Network (CIN), (a computer based information system) had replaced a box file of local information that Betty had used many times in the past. With the help of a library assistant, she looked on the CIN to see if there was any information about local yoga classes. The only information available was about a national organisation based in London. The library assistant suggested she ask at the local health clinic, which she did, without success. During another visit to the library she mentioned that the health clinic had not helped, and staff suggested she try the CAB.

Much of Betty's account of this situation involved issues and questions that it had raised for her, most of which related to her initial visit to the library. For example, she asked if the information from the box file had been transferred on to the CIN. She was so frustrated by the lack of local information on the CIN that she said she thought of volunteering to do it herself. The library Betty used had a sign over its entrance which read 'information point', yet, despite two visits, staff there were unable to assist her in finding the information she needed.

<table><caption>Table 6: 'Betty's tactics'</caption>

<tbody>

<tr>

<th>Information sources</th>

<th>Actions</th>

<th>Issues/Questions</th>

</tr>

<tr>

<td>Asked friends (2)</td>

<td>Felt frustrated (7-10)</td>

<td>Local yoga classes? (1)</td>

</tr>

<tr>

<td>Searched local newspapers (3)</td>

<td> </td>

<td>Locations of yoga classes (1)</td>

</tr>

<tr>

<td>Searched local free paper (4)</td>

<td> </td>

<td>Information focus (8)</td>

</tr>

<tr>

<td>Looked at cards on health food shop window (5)</td>

<td> </td>

<td>No box file? (7)</td>

</tr>

<tr>

<td>Asked health food shop owner (6)</td>

<td> </td>

<td>Lack of local information on CIN (8)</td>

</tr>

<tr>

<td>Asked in library (7/10)</td>

<td> </td>

<td>Lack of Burlington information on CIN re yoga (8)</td>

</tr>

<tr>

<td>Used Community Information Network (CIN) (8)</td>

<td> </td>

<td>Transfer of box file information to CIN? (7)</td>

</tr>

<tr>

<td>Asked in health clinic (9)</td>

<td> </td>

<td>Volunteer to help re CIN information? (8)</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Is information on yoga classes available? (7)</td>

</tr>

</tbody>

</table>

### Centrality of electronic information use

Table 7 below summarises real situations where respondents mentioned using electronic information in terms of centrality of electronic information use. Three levels of centrality were developed, as follows:

*   Only electronic information was used;
*   electronic information was central to dealing with the situation;
*   electronic information was not central to the situation.

<table><caption>Table 7: 'Centrality of electronic information use' for real situations</caption>

<tbody>

<tr>

<th>Level of centrality</th>

<th>Brief summary of situation</th>

<th>Topic focus</th>

</tr>

<tr>

<td>Only</td>

<td>Urgent search for cheap accommodation in Montpellier, France due to last minute cancellation</td>

<td>Consumer-Quality  
Misc-Factual</td>

</tr>

<tr>

<td>Only</td>

<td>Concern expressed following a search on the Web for fractals which threw up pornography sites</td>

<td>Internet-Pornography</td>

</tr>

<tr>

<td>Only</td>

<td>Search for source code and algorithms for maze program looking for source code or Java applets to download</td>

<td>Misc-Other</td>

</tr>

<tr>

<td>Central</td>

<td>Looking for images of lobsters showing an underside view.</td>

<td>Consumer-Other</td>

</tr>

<tr>

<td>Central</td>

<td>Looking for a company logo for a campaign organization.</td>

<td>Consumer-Other</td>

</tr>

<tr>

<td>Central</td>

<td>Wanting to read a foreign language newspaper and needing to download special fonts to read the paper.</td>

<td>Consumer-Other</td>

</tr>

<tr>

<td>Central</td>

<td>Finding a place to stay in New York (and details about its location)</td>

<td>Consumer-Product-Information</td>

</tr>

<tr>

<td>Central</td>

<td>Finding information about train and bus times</td>

<td>Consumer-Service Information</td>

</tr>

<tr>

<td>Central</td>

<td>Wanting information about dental implant</td>

<td>Health-Information</td>

</tr>

<tr>

<td>Central</td>

<td>Wanting information about caring for a pregnant guinea pig and her babies.</td>

<td>Misc-Factual</td>

</tr>

<tr>

<td>Not central</td>

<td>Information about post graduate courses</td>

<td>Education-Information</td>

</tr>

<tr>

<td>Not central</td>

<td>Wanting to find out about the programme and booking tickets at the Sydney Opera House in Australia before finalising holiday arrangements.</td>

<td>Consumer-Product Information</td>

</tr>

<tr>

<td>Not central</td>

<td>Wanting to find out about local yoga classes.</td>

<td>Recreation-Information</td>

</tr>

</tbody>

</table>

These results show that for ten of the thirteen situations where electronic information was used (77%), it was either the only way information was sought, or was central to the situation. The differences between the three situations where electronic information was not central related to problems with using electronic information (for information about post-graduate courses, and wanting information about yoga classes). Electronic information was not central to dealing with contacting the Sydney Opera House because the e-mail she had sent remained unanswered, offering no help or information to her. An attempt to use a Community Information Network to find out about local yyoga classes was unsuccessful (and only one of a number of different tactics used in the search). For information about post graduate courses electronic information use would be restricted to sending an e-mail to an academic. The Web would not be used, because it would take too long and cost too much.

These results indicate that _when used_, electronic information was an important element in dealing with real situations. At the same time, when all 100 situations are considered, the extent that electronic information was used across this study was limited.

### Use of electronic information for the hypothetical situations

Four hypothetical situations were provided; looking for a new home some distance away (new home); a health problem requiring a second opinion (health); a situation involving a neighbour's barking dog (barking dog) and a need for a new television (new TV).

As outlined in the Methods section above, data from hypothetical situations were examined in terms of frequency of use and prime tactics (those most frequently mentioned first or second). Prime tactics were derived from tactic analysis of individual respondents step by step descriptions of how they would deal with the hypothetical situations. This data was then summarised to generate prime tactics across respondents at each site. Results showed that there were similarities between tactics which were frequently mentioned and prime tactics. Table 8 below summarises results across the four situations by looking at prime tactics as a proportion of all tactics mentioned for each of the four hypothetical situations. Results show that there were similarities in the approaches respondents said they might use when dealing with each of these situations, but that each situation was approached _very differently_.

<table><caption>Table 8: Prime tactics across all hypothetical situations</caption>

<tbody>

<tr>

<td></td>

<th>New Home (n=47)</th>

<th>Health (n=39)</th>

<th>Barking dog (n=47)</th>

<th>New TV (n=47)</th>

</tr>

<tr>

<td>

Prime tactics

</td>

<td>21</td>

<td>21</td>

<td>17</td>

<td>22</td>

</tr>

<tr>

<td>

All tactics

</td>

<td>63</td>

<td>36</td>

<td>24</td>

<td>37</td>

</tr>

<tr>

<td>

Percentage

</td>

<td>33%</td>

<td>58%</td>

<td>71%</td>

<td>59%</td>

</tr>

</tbody>

</table>

Finally, tactics were put into different tactic categories developed from the data (as defined in the Methods section above). The development of tactic categories as a way of examining approaches to dealing with information needed in everyday life represents a new way of analysing approaches to finding everyday information. As discussed earlier, previous research did not offer a means of categorising everyday information seeking strategies which took account of the full range of strategies mentioned by respondents in this study, rather than focusing on 'information'. This can be illustrated by focusing on one of the hypothetical situations, the new home. The situation posed was as follows:

> 'You want to find a new place to live, somewhere that is a hundred miles from your current home.  
> What steps would you go through in order to deal with this situation?  
> Is there anything in particular that you would try to find out or understand that would help you deal with this?  
> What would you try to find out?  
> [Then] How would you go about finding this out... or understanding this?'

Table 9 below summarises results for this situation. Respondents provided nineteen different assessment tactics. These ranged from very specific assessments, such as what the library was like, to more general assessments, for example, what the community in an area was like. Eleven tactics were categorised as actions, with a visit to the area being the most frequently mentioned tactic for the situation. Thirteen tactics were categorised as issues or questions.

<table><caption>Table 9: 'Tactic categories': New Home</caption>

<tbody>

<tr>

<th>Information Sources</th>

<th>No.</th>

<th>Assessments</th>

<th>No.</th>

<th>Actions</th>

<th>No.</th>

<th>Issues/Questions</th>

<th>No.</th>

</tr>

<tr>

<td>Estate Agent</td>

<td>28</td>

<td>Areas</td>

<td>21</td>

<td>Visit area</td>

<td>32</td>

<td>Cost of Property</td>

<td>15</td>

</tr>

<tr>

<td>Talk to locals</td>

<td>21</td>

<td>Amenities</td>

<td>13</td>

<td>Not moving</td>

<td>6</td>

<td>Property types</td>

<td>3</td>

</tr>

<tr>

<td>Internet (Web)</td>

<td>15</td>

<td>Transportation</td>

<td>13</td>

<td>Stay there</td>

<td>6</td>

<td>Rural Location</td>

<td>4</td>

</tr>

<tr>

<td>Local paper</td>

<td>14</td>

<td>Cultural</td>

<td>9</td>

<td>Visit property</td>

<td>6</td>

<td>Distrust</td>

<td>2</td>

</tr>

<tr>

<td>Library (for info)</td>

<td>13</td>

<td>Schools</td>

<td>8</td>

<td>Surveyor</td>

<td>4</td>

<td>Housing Associations?</td>

<td>2</td>

</tr>

<tr>

<td>Tourist Info.</td>

<td>10</td>

<td>Crime</td>

<td>6</td>

<td>Mortgage</td>

<td>3</td>

<td>Welsh language</td>

<td>2</td>

</tr>

<tr>

<td>EAs mailing list</td>

<td>9</td>

<td>Friendliness</td>

<td>5</td>

<td>Visit churches</td>

<td>2</td>

<td>State of house</td>

<td>2</td>

</tr>

<tr>

<td>Council info</td>

<td>8</td>

<td>Shops</td>

<td>5</td>

<td>Builder</td>

<td>1</td>

<td>Internet expectations</td>

<td>2</td>

</tr>

<tr>

<td>Use maps</td>

<td>5</td>

<td>Further/adult ed</td>

<td>4</td>

<td>Do voluntary work</td>

<td>1</td>

<td>Countryside or Sea</td>

<td>1</td>

</tr>

<tr>

<td>Bars/Pubs (info)</td>

<td>4</td>

<td>Community</td>

<td>3</td>

<td>Move and decide</td>

<td>1</td>

<td>Low flying</td>

<td>1</td>

</tr>

<tr>

<td>Friends/rels there</td>

<td>4</td>

<td>Employment</td>

<td>2</td>

<td>Sell current home</td>

<td>1</td>

<td>Special housing</td>

<td>1</td>

</tr>

<tr>

<td>Internet (e-mail)</td>

<td>3</td>

<td>Library</td>

<td>2</td>

<td> </td>

<td> </td>

<td>TEC</td>

<td>1</td>

</tr>

<tr>

<td>Yellow Pages</td>

<td>3</td>

<td>Churches</td>

<td>1</td>

<td> </td>

<td> </td>

<td>First time buyer?</td>

<td>1</td>

</tr>

<tr>

<td>Police (info)</td>

<td>2</td>

<td>Community Centres</td>

<td>1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>University info</td>

<td>2</td>

<td>Racial mix</td>

<td>1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Internet (images)</td>

<td>1</td>

<td>Special shops</td>

<td>1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Notices etc.</td>

<td>1</td>

<td>Telematics</td>

<td>1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Info. Broker</td>

<td>1</td>

<td>Unemployment</td>

<td>1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>PiP</td>

<td>1</td>

<td>Walking</td>

<td>1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Use radio</td>

<td>1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

In some cases these assessment tactics involved information sources, but for others personal observation played an important part. For example, a number of respondents spoke of 'assessing areas' by driving around. Equally, 'actions' were other tactics not usually mentioned in studies of information seeking. One woman described how she would do voluntary work in an area to get a sense of what the community was like. The present study clearly demonstrates that by concentrating on information sources important strategies can be neglected. The contention here is that actions, assessments, issues and questions need to be given as much attention as information sources when studying everyday information seeking. This point is illustrated by Table 10 below, which summarises the number of tactics mentioned for each of the four hypothetical situations, using the four 'tactic categories' developed during the present study. Almost as many assessment tactics (19) were mentioned across the study as information sources (20) for the new home situation. Equally, with the barking dog situation concentration on information sources required to deal with the situation would provide a very incomplete picture of ways respondents might approach the situation. Only three information sources were mentioned compared to seventeen actions.

<table><caption>Table 10 'Tactic categories' and hypothetical situations</caption>

<tbody>

<tr>

<th>'Tactic categories'/Situations</th>

<th>New home</th>

<th>Health</th>

<th>Barking dog</th>

<th>New TV</th>

</tr>

<tr>

<td>Information sources</td>

<td>20</td>

<td>23</td>

<td>3</td>

<td>12</td>

</tr>

<tr>

<td>Assessments</td>

<td>19</td>

<td>-</td>

<td>-</td>

<td>6</td>

</tr>

<tr>

<td>Actions</td>

<td>11</td>

<td>9</td>

<td>17</td>

<td>6</td>

</tr>

<tr>

<td>Issues and Questions</td>

<td>13</td>

<td>4</td>

<td>4</td>

<td>12</td>

</tr>

</tbody>

</table>

Table 11 below summarises results showing the number of respondents that mentioned using the Internet as one of their tactics for dealing with the hypothetical situations.

<table><caption>Table 11: Use of electronic information for hypothetical situations</caption>

<tbody>

<tr>

<th>Hypothetical situation</th>

<th>Pilot site (n=8)</th>

<th>Burlington (n=19)</th>

<th>Glasfryn (n=20)</th>

<th>Totals</th>

</tr>

<tr>

<td>New Home</td>

<td>2</td>

<td>5</td>

<td>8</td>

<td>15</td>

</tr>

<tr>

<td>Health</td>

<td>N/A</td>

<td>5</td>

<td>9</td>

<td>14</td>

</tr>

<tr>

<td>Barking Dog</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>New TV</td>

<td>1</td>

<td>2</td>

<td>2</td>

<td>5</td>

</tr>

</tbody>

</table>

These summarised results indicate that respondents across the study could envisage using electronic information for three of these everyday situations. The most prevalent use of electronic information was found for the new home and health situation. This indicated that the new home and health situations lend themselves more favourably to electronic information use than the barking dog and new TV situations. No one mentioned using electronic information for dealing with the barking dog situation. Fifteen people mentioned using some aspect of the Internet for finding a new home a distance away from where they currently lived. At the two main study sites 14 respondents mentioned using the Internet as one tactic in dealing with a health situation. A smaller number (5) mentioned using the Internet in relation to buying a new TV.

Use of 'tactic analysis' as a means of examining responses to the hypothetical situations challenged the view that hypothetical situations are poor indicators for system design because respondents may say they would do something, when in practice they may not [Dervin _et al._](#ref12), 1976: 505). Tactic analysis provided an _indication_ of information seeking strategies that people might use. Results using this technique could be utilised for early stages of system design, addressing the user-centred design paradox described by [Marchionini](#ref41):

> ...we cannot discover how users best work with systems until the systems are built, yet we should build the systems based on knowledge of users and how they work. (1995: 75).

The hypothetical situations posed a series of non-threatening questions, which, in conjunction with 'tactic analysis' provide the means for basing early prototypes of electronic information system design on knowledge of users and how they might approach situations. The method used in the present study of collecting and analysing data from hypothetical situations provided a richness of information that _can_ be used in system design. Taking the new home situation as an example, 'tactic analysis' could provide the basis for a Web-based system which enables people to gather information about buying a new home at a location a distance away from where they currently live. Since the field research was carried out a number of sites have been developed which have gone some way towards offering this type of information (e.g. www.homesight.co.uk) but many estate agents sites on the Web have simply reproduced the service they provide on the high street, despite the potential that a hypertext environment such as the Web can provide in terms of information provision. This method also offers the potential for those without experience of IT to participate in system design. In this study, asking relevant, non-threatening questions which concentrate on respondents' everyday experiences rather than technology provided the basis for a framework to inform future community-based ICT initiatives.

### Factors that affect use of electronic information

This question was explored by asking respondents a series of direct questions at the end of the interview about their use or non-use of electronic information for the four hypothetical situations. Responses to these questions gave a wealth of information about factors affecting use of electronic information. These results are drawn from the main study only (n=39) as pilot respondents were not asked about use or non-use of the Internet for the hypothetical situations.

Responses were grouped into 17 different codes developed from the data. Table 12 presents a summary of factors affecting use of electronic information, sorted by frequency. The most common factor respondents said would prevent them from using electronic information to deal with the hypothetical situations was that they would need support to be able to use the Internet effectively. This was mentioned by 15 respondents. Closely linked to this was a perceived lack of experience (12 respondents). Respondents had certain perceptions of the Internet; about how it worked, feelings about content, etc. (12 respondents). Financial factors were grouped under four separate codes to capture the variety of issues raised relating to finance and accessing electronic information. Fifteen respondents mentioned one or more of the financial factors. Five respondents discussed more than one financial factor as reasons for not considering using electronic information for the hypothetical situations. The cost associated with looking for information was one reason given for needing support. For example, a woman, who was a telecentre user in Burlington said:

> I would have to ask...the staff, because I really don't understand how the Internet is set up. And I do know that there is such a vast amount of information that.. it is almost as if you can get lost in it there is so much stuff there, and I wouldn't, because I would also be paying for the time I used it, I wouldn't want to waste too much time looking through things that were totally inappropriate. So I think I would have to ask somebody who knew about it, so that I could find out what I needed to know in the shortest space of time." (30 year old woman)

<table><caption>Table 12: Factors affecting use of electronic information</caption>

<tbody>

<tr>

<td>Codes</td>

<th>Burlington (n=19)</th>

<th>Glasfryn (n=20)</th>

<th>Totals</th>

</tr>

<tr>

<td>Need support</td>

<td>11</td>

<td>4</td>

<td>15</td>

</tr>

<tr>

<td>Lack experience</td>

<td>3</td>

<td>9</td>

<td>12</td>

</tr>

<tr>

<td>Perception</td>

<td>6</td>

<td>6</td>

<td>12</td>

</tr>

<tr>

<td>Interface Problems</td>

<td>8</td>

<td>1</td>

<td>9</td>

</tr>

<tr>

<td>Financial Factors</td>

<td>2</td>

<td>5</td>

<td>7</td>

</tr>

<tr>

<td>Financial Factors (other sources)</td>

<td>6</td>

<td>2</td>

<td>8</td>

</tr>

<tr>

<td>Financial Factors (no access)</td>

<td>6</td>

<td>0</td>

<td>6</td>

</tr>

<tr>

<td>Distrust</td>

<td>4</td>

<td>0</td>

<td>4</td>

</tr>

<tr>

<td>E-comm. Difficulties</td>

<td>3</td>

<td>1</td>

<td>4</td>

</tr>

<tr>

<td>Lack of immediacy</td>

<td>2</td>

<td>2</td>

<td>4</td>

</tr>

<tr>

<td>Problems finding info.</td>

<td>3</td>

<td>0</td>

<td>3</td>

</tr>

<tr>

<td>Fear</td>

<td>0</td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td>Fin Factors (info fast)</td>

<td>1</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Inappropriate</td>

<td>0</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>Lack of time</td>

<td>0</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>Mental block</td>

<td>0</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>No personal touch</td>

<td>0</td>

<td>1</td>

<td>1</td>

</tr>

</tbody>

</table>

Examples of interface problems highlighted the need for CBA sites to include technologies for disabled people and those with different needs. Within a small group of 47 two respondents had difficulty accessing electronic information. One of these was a visually impaired man interviewed in Burlington. As a regular library user he had asked staff about computers, he said:

> I've asked about computers, if they've got anything specific that blind people can use - you know a computer that is going to have an audio facility and they haven't got anything anyway. I asked if they knew of any courses where I could learn about computers and they didn't really. So I have never got any further with it. (44 year old man)

In response to the question about what would get in the way of using the Internet for the hypothetical situations he said:

> ...there's loads of information that can be had off of it, but I have not found anyone yet who is up for teaching me. (44 year old man)

A dyslexic woman was enthusiastic about using the Internet, but the normal interface meant that she was only able to use it with the help of her nephew. Talking generally about what would prevent her from using the Internet for the hypothetical situations she said:

> If only I could sit in front of a computer go on the Internet and find out lots and lots of things I want to know, I would love it, I really would.  
> [So, what would get in the way of you using the Internet to deal with those situations?]  
> Well, it's the, it's typing up what I want to ask, you know, spelling what I want to ask.  
> [Anything else?]  
> No, just the actual spelling, because if you can't spell it right, you can't get the information. This is why it is so important to ask, you know, verbally for what you want." (58 year old woman)

Factors affecting use of electronic information had similarities with factors affecting use or non-use of telecentres. Briefly, use of telecentres to access electronic information for the everyday situations outlined was low across the telecentres. These results were also reflected in a telephone survey of UK telecentres, which demonstrated low levels of Internet usage. Factors that hindered usage included cost - people found other ways of addressing their information needs, either by using sources which did not involve ICT, or by using access elsewhere (college, friends and family), where connection costs were borne by others. Problems with opening hours and reliability of equipment also hindered people's use of telecentres. The resource issues that these results raise suggest that co-operation between various Community based access sites within communities would be a more effective way of providing access to ICT than for each location (telecentre, school, library, healthy living centre, etc.) setting out to offer training, technical support, search techniques and so on. Sharing the resource implications of community based access to ICT would be a more effective use of limited resources.

## Conclusions

This paper has presented results from PhD research, focusing on one of three research themes; the use of electronic information in everyday life. As with the larger-scale Seattle study ([Dervin _et al._](#ref12), 1976) a wide range of information needs were presented by 47 respondents, across a broad spread of different topic focus categories. The implications of these results for CBA sites relates to provision of access to electronic information (as more information is stored electronically). CBA sites need to be able to meet the wide range of information needs articulated during the present study. It was encouraging to note the similarity of results between a small-scale study involving 47 respondents and a larger scale study involving 300\. Given the added benefits of in-depth analysis of respondents approaches to finding information, this outcome suggests that there is value in conducting further small-scale studies.

Development of tactic analysis, tactic categories and centrality of electronic information use provide a series of tools for such in-depth studies. When used in conjunction with a framework such as Sense-making for data collection, these methods provide a means of undertaking qualitative studies. Results presented here have demonstrated that by widening the scope of a study beyond 'information', additional strategies can be revealed which affect information seeking behaviour. The contention here is that actions, assessments, issues and questions need to be given as much attention as information sources when studying everyday information seeking. It is suggested that the data analysis methods outlined here can be applied across a broad range of disciplines, including systems design and are particularly appropriate for qualitative studies of information seeking, information seeking behaviour and user needs.

In terms of use of electronic information in the context of everyday life the need for support, a perceived lack of experience in using electronic information and a series of financial factors were all factors hindering use of electronic information amongst respondents both at CBA sites and elsewhere. The cost of using electronic information within telecentres meant that alternative ways of addressing everyday information needs were sought, either by using more traditional methods or by using electronic information at locations where the cost of access was borne by others (college, friends, etc.). Amongst a small group of respondents two people mentioned a lack of assistive technologies (such as screen readers) as a factor preventing them accessing electronic information independently, as well as training in their use. Such technology should be available at CBA sites as it can be expensive for individuals to buy, thus creating a barrier to using electronic information in their own homes. These results are important for future CBA sites, because they clearly point to the need for staff resources to support people in using electronic information. They also suggest that unless access to electronic information is free people will continue to find other ways of addressing their everyday information needs. Consequently for CBA sites to be meet the needs of communities and to be sustainable secure revenue funding will be essential.

Since the study was completed UK Government initiatives such as the People's Network and the development of ICT Learning Centres (called UK online centres) has occurred quickly, and as argued at the beginning of this paper, little has been learnt from existing CBA sites such as telecentres. An evaluation of pilot UK online centres called for a balance of revenue to capital funding for further centres of at least 4:1 ([Hall Aiken Associates](#ref24), 2001). Unfortunately this balance did not materialise. Instead, the ratio was reversed with 1:4 being the average with revenue funding under New Opportunities Fund (NOF) and capital funding through the Capital Modernisation Fund. This leads to serious concerns about how new centres will be able to provide support to new users that this study clearly showed was needed. Charging for access to the Internet is something that has been left to individual centres, meaning that a lack of adequate revenue funding may mean that some centres are unable to provide free access. Development of access points in libraries through the People's Network has also raised concerns, with no universal free access to electronic information across the UK libraries at present ([Baigent](#ref2), 2001). NOF have indicated that they expect free access in libraries to be the norm ([Time...](#ref32), 2001), yet there appears to be no commitment to funding connectivity or revenue costs adequately.

## Acknowledgements

The work reported here was undertaken as a Ph.D. student in the Department of Information and Communications at Manchester Metropolitan University. I would like to thank my supervisory team, Shelagh Fisher, Dr. Francis Johnson and Jonathan Willson for their guidance and support. I would also like to acknowledge the interest, encouragement and support given by Professor Peter Halfpenny (University of Manchester). Thanks also to the anonymous referees for their comments on the original draft of this paper.

## <a id="note"></a>Note

The _Making the Connection Seminar_ on providing public access to the Internet in Greater Manchester public libraries, was organised by Bury Metro Libraries and Isaware. It was held in Bury Town Hall, July 8th 1998, but no proceedings or papers were published.

<a id="note2"></a>Reed, R (1998) "Community Resource Centres in Hereford and Worcester." Paper presented at the _Telecottages seminar_ held at Manchester Town Hall, September 9th 1998.

**To discuss this paper, join the IR-DISCUSS discussion list by subscribing at [http://www.jiscmail.ac.uk/lists/IR-DISCUSS.html](http://www.jiscmail.ac.uk/lists/IR-DISCUSS.html)**

## References

*   <a id="ref1a"></a>Agada, J. (1999) "Inner-city gatekeepers: an exploratory survey of their information use environment". _Journal of the American Society for Information Science_ **50**(1), 74-85.
*   <a id="ref1"></a>Batt, C. (1998) _Information technology in public libraries_, 6th ed. London: Library Association.
*   <a id="ref2"></a>Baigent, H. (2001) [Latest statistics from Re:source.] [Personal e-mail] (28 Sept. 2001)
*   <a id="ref2a"></a>Bergeron, P. and Nilan, M.S. (1991) "Users' information needs in the process of learning word-processing: a user-based approach looking at source use." _The Canadian Journal of Information Science_ **16**(2), 13-27.
*   <a id="ref3"></a>Blair, T. (1999) "Why the Internet years are vital". _The Guardian_ 25 October 1999, p.8.
*   <a id="ref4"></a>Blake, M. (1998) "Internet access for older people." _ASLIB Proceedings_ **50**(10), 308-315.
*   <a id="ref5a"></a>Bruce, C.S. (1995) "Information literacy: a framework for higher education." _The Australian Library Journal_ **44**(3), 158-170.
*   <a id="ref5"></a>Burt, L. (1995) _British and Irish telecottages: a new phenomenon_. Edinburgh: University of Edinburgh. (Honours degree in Commerce dissertation.)
*   <a id="ref6"></a>Carr, R. (1999) Silver surfers feature on BBC Radio 4 _You and Yours_ magazine programme broadcast 20th October 1999.
*   <a id="ref6a"></a>Case, D., Borgman, C.L. & Meadow, C.T. (1985) "Information seeking in the energy research field: the DOE/OAK project." _In_: _ASIS 85 Proceedings for the 48th ASIS Annual Meeting 1985_, **22**, pp. 311-336\. New York, NY: American Society for Information Science.
*   <a id="ref6aa"></a>Carey, R. F., McKechnie, L., & McKenzie, P. J. (2001) "Gaining access to everyday life information seeking." _Library and Information Science Research_, **23**(4), 319-334.
*   <a id="ref7"></a>Central Office of Information (1998) _Our Information Age: the government's vision_. London: Central Office of Information
*   <a id="ref8"></a>CIRA (1997) _Trimdon Village Appraisal_. Middlesborough: University of Teesside, Community Informatics Research and Applications Unit. (Video recording)
*   <a id="ref9a"></a>Chatman, E.A. & Pendleton, V.E. (1995) " Knowledge gap, information-seeking and the poor." _Reference Librarian_ Nos. 49/50, 135-146.
*   <a id="ref9"></a>Chen, C.C. & Hernon, P. (1982) _Information seeking: assessing and anticipating user needs_, London: Neal-Schuman Publishers, Inc.
*   <a id="ref9aa"></a>Cline, R. J. W. & Haynes, K. M. (2001) " Consumer health information seeking on the Internet: the state of the art." _Health Education Research_, **16**(6), 671-692.
*   <a id="ref9b"></a>Craven, J. (2000) "Good Web design principles for the library website: a study of accessibility issues in UK university libraries." _The New Review of Information and Library Research_, **6**, 25-41.
*   <a id="ref10a"></a>Cobbledick, S. (1996) "The information seeking behaviour of artists: exploratory interviews." _The Library Quarterly_ **66**(4), 343-372.
*   <a id="ref10"></a>Day, P. & Harris, K. (1997) _Down to earth vision: community based IT initiatives and social inclusion._ London: IBM and Community Development Foundation.
*   <a id="ref9c"></a>Davies, M. M. & Bath, P. A. (2002) "Interpersonal sources of health and maternity information for Somali women living in the UK: information seeking and evaluation," _Journal of Documentation_, **58**(3), 302-18.
*   <a id="ref11"></a>Dervin, B. (1976) "The everyday information needs of the average citizen: a taxonomy for analysis," in: Kochen, M., _ed._ _Information for the community_, pp. 19-38\. Chicago, IL: American Library Association
*   <a id="ref12"></a>Dervin, B., Zweizig, D., Banister, M., Gabriel, M., Hall, E., Kwan, C., Bowes, J. & Stamm, K. (1976) _The development of strategies for dealing with the information needs of urban residents: Phase I - citizen's study._ Seattle, WA: University of Washington, School of Communications. (Final report of project No. L0035JA for the U.S Department of Health, Education and Welfare.)
*   <a id="ref13"></a>Dervin, B. (1983) An overview of sense-making research: concepts, methods and results to date. _Paper presented at the International Communication Association meeting, Dallas, Texas_, May 1983.
*   <a id="ref14"></a>Dervin, B. (1984) _Information needs of Californians — 1984\. Report 1: Technical report. Report 2: Context, summary, conclusions, implications, applications_. Davis, CA: University of California, Institute of Governmental Affairs.
*   <a id="ref15"></a>Dervin, B. and Nilan, M. (1986) "Information needs and uses" _Annual Review of Information Science and Technology_, **21**, 3-33\.
*   <a id="ref16a"></a>Dervin, B. (1989) "Users as research inventions: how research categories perpetuate inequities." _Journal of Communication_ **39**(3), 216-232.
*   <a id="ref16"></a>Dervin, B. (1992) "From the mind's eye of the user: the sense-making qualitative-quantitative methodology." _In:_ Glazier, J.D., _ed._ _Qualitative research in information management_, pp. 61-84\. Englewood, CO: Libraries Unlimited Inc.
*   <a id="ref17"></a>Dervin, B. (1995) "Chaos, order and sense-making: a proposed theory for information design." _In_: Jacobson, R., ed. (1999) _Information Design_, Cambridge, MA: MIT Press.
*   <a id="ref18"></a>Dervin, B. (1999b) Personal communication between Debbie Ellen and Brenda Dervin, 10 August, 1999.
*   <a id="ref19"></a>Ducatel, K. & Halfpenny, P. (1993) "Telematics for the community? An electronic village hall for East Manchester." _Environment and Planning C: Government and Policy_ **11**, 367-379.
*   <a id="ref20"></a>Ducatel, K., Shenton, N., Graham, S., Halfpenny, P. & Scott, D. (1993) _Community Teleservices Project: an evaluation of the Manchester electronic village hall initiative_. Manchester: University of Manchester, Centre for Applied Social Research.
*   <a id="ref20a"></a>Durrance, J.C., (1982) "A model for the selection of factors which affect the public policy information needs of citizen groups." _Library Research_ **3**, 23-49\.
*   <a id="ref21a"></a>Eager, C. & Oppenheim, C. (1996) "An observational method for undertaking user needs studies." _Journal of Librarianship and Information Science_ **28**(1), 15-23\.
*   <a id="ref21"></a>Ellen, D. (1997) _An evaluation of Manchester Community Information Network_. Manchester: Manchester Metropolitan University (M.Sc. dissertation).
*   <a id="ref21b"></a>Ellen, D (2000) _[Telecentres and the provision of community based access to electronic information in everyday life](http://www.mmu.ac.uk/h-ss/dic/research/ellen/contents.html)_, Manchester: Manchester Metropolitan University (Ph.D. thesis) http://www.mmu.ac.uk/h-ss/dic/research/ellen/contents.html (28 September 2002)
*   <a id="ref21c"></a>Fisher, S. & Oulton, T. (1999) "The critical incident technique in library and information management research." _Education for Information_ **17**(2), 113-125\.
*   <a id="ref21d"></a>Flanagan, J.C. (1954) "The critical incident technique." _Psychological Bulletin_ **51**, (4), 327-359\.
*   <a id="ref22"></a>Girbash, C. (1991) _Feasibility study for a Women's Electronic Village Hall in Manchester_. Manchester: Pankhurst Centre.
*   <a id="ref22a"></a>Given, L. M. (2002) "The academic and the everyday: investigating the overlap in mature undergraduates' information-seeking behaviors," _Library and Information Science Research_, **24**(1), 17-29.
*   <a id="ref23"></a>Graham, S. (1992) _Best practice in developing community teleservice centres_. Manchester: University of Manchester, Centre for Applied Social Research.
*   <a id="ref24"></a>Hall Aiken Associates (2001) _[ICT Learning Centres: Formative Evaluation of Pioneer and Pathfinder projects Final report](http://www.dfes.gov.uk/research/data/uploadfiles/RRX3.pdf)_. http://www.dfes.gov.uk/research/data/uploadfiles/RRX3.pdf (27 September 2002)
*   <a id="ref25"></a>Harris, R.M. and Dewdney, P. (1994) _Barriers to information: how formal help systems fail battered women_, Westport, CN: Greenwood Publishing Group.
*   <a id="ref25a"></a>Harris, R., Stickney, J., Grasley, C., Hutchinson, G., Greaves, L., & Boyd, T. (2001) "Searching for help and information: abused women speak out." _Library and Information Science Research_, **23**( 2), 123-141.
*   <a id="ref25b"></a>Hersberger, J. (2001) "Everyday information needs and information sources of homeless parents." _New Review of Information Behaviour Research_, **2**, 119-134.
*   <a id="ref26"></a>Hewins, E.T. (1990) Information need and use studies. _Annual Review of Information Science and Technology_, **25**, 145-172.
*   <a id="ref26a"></a>Higgins, S., & Hawamdeh, S. (2001) "Gender and cultural aspects of information seeking and use." _New Review of Information Behaviour Research_, **2**, 17-28.
*   <a id="ref26b"></a>Hoglund, L. & Wilson, T.D. (2001) "Introduction: a special issue on studies of information seeking in context." _New Review of Information Behaviour Research_, **2**, i-iii.
*   <a id="ref28a"></a>Keane, D. (1999) "The information behaviour of senior executives," _in_: Wilson, T.D. & Allen, D.K., _eds._ _Exploring the contexts of information behaviour: proceedings of the Second International Conference on Research in Information Needs, Seeking and Use in Different Contexts, 13-15 August, 1998, Sheffield, U.K._, London: Taylor Graham.
*   <a id="ref28b"></a>Kelle, U. (1996) "Computer-assisted qualitative data analysis in Germany." _Current Sociology_ **44**(3) 225-241\.
*   <a id="ref28c"></a>Kelle, U. (1997) "[Theory building in qualitative research and computer programs for the management of textual data.](http://www.socresonline.org.uk/socresonline/2/2/1.html)" _Sociological Research On-line_ **2**(2), 1-18\. http://www.socresonline.org.uk/socresonline/2/2/1.html (23 October 2002)
*   <a id="ref28"></a>Khalil, F. E. M. (2001) "Consumer health information: a brief critique on information needs and information seeking behaviour." _Malaysian Journal of Library and Information Science_, **6**(2), 83-99.
*   <a id="ref28"></a>Kuhlthau, C.C., Turock, B.J., George, M.W. & Belvin, R.J. (1990) "Validating a model of the search process: a comparison of academic, public and school library users." _Library and Information Science Research_ **12**, 5-31.
*   <a id="ref29"></a>Klobas, J. E. & Clyde, L. A. (2000) "Adults learning to use the Internet: a longitudinal study of attitudes and other factors associated with intended Internet use," _Library and Information Science Research_, **22**(1), 5-34.
*   <a id="ref29"></a>Kuhlthau, C.C. (1991) "Inside the search process: information seeking from the user's perspective." _Journal of the American Society for Information Science_ **42**(5), 361-371.
*   <a id="ref30"></a>Kuhlthau, C.C. (1993) "A principle of uncertainty for information seeking". _Journal of Documentation_ **49**(4), 339-355.
*   <a id="ref31"></a>Lancaster, S. (1999) _Telecentres and libraries: do they work together?_ Loughborough: Loughborough University, Department of Information Science. (M.A Dissertation)
*   <a id="ref33"></a>Lavery, J. & Livingston, B. (1999) "Introducing the Internet to adult learners." _Computers in Libraries_ **19**(4), 52-53.
*   <a id="ref34"></a>Leech, H. (1999) _CIRCE: Better Communities through Better Information_ London: Library and Information Commission. (Library and Information Commission Research report 1)
*   <a id="ref35"></a>Levin, J. & Taibe, G. (1970) "Bureaucracy and the socially handicapped: a study of lower status tenants in public housing." _Sociology and Social Research_ **54**, 209-219 [Cited in: Chen, C.C. & Hernon, P. (1982) _Information seeking: assessing and anticipating user needs_, London: Neal-Schuman Publishers, Inc.]
*   <a id="ref37"></a>Library and Information Commission (1997b) _[Towards a national information policy](www.lic.gov.uk/publications/policyreports/nip.html)_. London: Library and Information Commission http://www.lic.gov.uk/publications/policyreports/nip.html (27 September 2002)
*   <a id="ref36"></a>Library and Information Commission. _Working Group on Information Technology_ (1997a) _New library: the people's network_. London: Library and Information Commission.
*   <a id="ref37b"></a>Library and Information Commission (2000) _[Keystone for the information age: a national policy for the U.K.](http://www.lic.gov.uk/publications/policyreports/keystone.html)_. London: Library and Information Commission. http://www.lic.gov.uk/publications/policyreports/keystone.html (27 September 2002)
*   <a id="ref38"></a>Liff, S, Watts,P. & Steward, F. (1999) "E-Gateways as innovative organisations - different approaches to social inclusion". Paper delivered at _Conference on Inclusion in the information society: e-gateways as new social places for cyberaccess_, Aston University Business School, Birmingham, 16 December, 1999.
*   <a id="ref39"></a>Mackie, D & Wilcox, D. (1998) _[The communities online game.](http://www.partnerships.org.uk/game/index.htm)_. [Partnerships Online](http://www.partnerships.org.uk/). (D. Wilcox, Maint.) http://www.partnerships.org.uk/game/index.htm (27 September 2002)
*   <a id="ref40d"></a>Marcella, R. & Baxter, G. (1999) _Citizenship information: final report of a project funded by the British Library Research and Innovation Centre_. London: British Library Research and Innovation Centre. (BLRIC Report no. 173).
*   <a id="ref40"></a>Marcella, R. & Baxter, G. (2000) "The information needs and the information seeking behaviour of a national sample of the population in the united kingdom, with special reference to needs related to citizenship." _Journal of Documentation_ **55**(2), 159-183.
*   <a id="ref40c"></a>Marcella, R. & Baxter, G. (2001) "A random walk around Britain: a critical assessment of the random walk sample as a method for collecting data on the public's citizenship information needs," _The New Review of Information Behaviour_, **2**, 87-104.
*   <a id="ref40b"></a>Marcella, R. (2002), "Women on the Web: a critical appraisal of a sample reflecting the range and content of women's sites on the Internet, with particular reference to the support of women's interaction and participation." _Journal of Documentation_, **58**(1), 79-103.
*   <a id="ref41"></a>Marchionini, G. (1995) _Information seeking in electronic environments_, Cambridge: Cambridge University press.
*   <a id="ref42"></a>Mark, J., Cornebise, J. & Ellen Wahl Education Development Center, Inc. (1997) _[Community technology centres: impact on individual participants and their communities.](http://www.ctcnet.org/eval.html)_ Newton, MA: Ellen Wahl Education Development Center, Inc. http://www.ctcnet.org/eval.html (23 October 2002)
*   <a id="ref43"></a>Mitchell, H. (1999, October 28) "Community information audits." CONet (Communities Online Network). http://omniforum.ukco.org.uk/virtual/groups/conet/month/bydate/, [28 October 1999, no longer available on 25 October 2002]
*   <a id="43b"></a>Moore, N. & Steele, J. (1991) _Information intensive Britain: an analysis of the policy issues: a study_, London: Policy Studies Institute.
*   <a id="43c"></a>Muir, A. & Oppenheim, C., (2001) _[Report on developments worldwide on national information policy](http://www.la-hq.org.uk/directory/prof_issues/nip/title.htm)_. London: Library Association. http://www.la-hq.org.uk/directory/prof_issues/nip/title.htm (27 September 2002)
*   <a id="ref27"></a>National Working Party on Social Inclusion in the Information Society (INSINC) (1996) The net result: social inclusion in the information society. Report of the National working party on Social Inclusion. London: IBM
*   <a id="ref43a"></a>Nicholas, D., Gunter, B. & Withey, R. (2002) "The digital information consumer." _Library + Information Update_, **1**(1), 32-34.
*   <a id="ref44"></a>Ormes, S. and Dempsey, L. (1995) _[The Library and Information Commission public library Internet survey](http://www.ukoln.ac.uk/publib/lic.html)_. Bath: University of Bath, UK Office for Library and Information Networking. http://www.ukoln.ac.uk/publib/lic.html (25 October 2002)
*   <a id="ref46"></a>Qvortrup, L. (1994) _[Community Teleservice Centres: a means to social, cultural and economic development of rural communities and low-income urban settlements](http://www.icbl.hw.ac.uk/telep/telework/ttpfolder/tcfolder/ctc.html)_. Edinburgh: Heriot-Watt University, Teleprompt Project. http://www.icbl.hw.ac.uk/telep/telework/ttpfolder/tcfolder/ctc.html (26 September 2002)
*   <a id="ref48"></a>Ross, S.R. (1999) "Finding without seeking: what readers say about the role of pleasure-reading as a source of information." _In_: Wilson, T.D. and Allen, D.K., _eds._ _Exploring the contexts of information behaviour: proceedings of the Second International Conference on Research in Information Needs, Seeking and Use in Different Contexts, 13-15 August, 1998, Sheffield, U.K._. pp. 343-355\. London: Taylor Graham.
*   <a id="ref49"></a>Savolainen, R. (1993) "The sense making theory: reviewing the interests of a user centred approach to information seeking and use." _Information Processing and Management_ **29**(1), 13-28.
*   <a id="ref50"></a>Savolainen, R. (1995a) "Everyday life information seeking: findings and methodological questions of an empirical study." _In_: Hancock-Beaulieu, M. and Pors, N. _eds._ _The First British-Nordic Conference on Library and information Science_, 22-24 May 1995, Copenhagen, pp. 313-331\. Copenhagen: The Royal School of Librarianship.
*   <a id="ref51"></a>Savolainen, R. (1995b) "Everyday life information seeking: approaching information seeking in the context of 'way of life'." _Library and Information Science Research_ **17**, 259-294.
*   <a id="ref51a"></a>Savolainen, R. (2001) "'Living encyclopedia' or idle talk? Seeking and providing consumer information in an Internet newsgroup, "_Library and Information Science Research_, **23**(1), 67-90.
*   <a id="ref51b"></a>Schmetzke, A. (2002) "Accessibility of web-based information resources for people with disabilities, " _Library Hi Tech_, **20**(2), 135-136.
*   <a id="ref52a"></a>Secker, J., Stoker, D. & Tedd, L. (1997) "Attitudes of library and information science professionals to current awareness services: results from a user needs survey, using focus groups for the NewsAgent project." _In:_ Beaulieu, M. _ed._ _Library and information Sstudies: research and professional practice, Proceedings of the 2nd British-Nordic Conference on Library and Information Studies, Queen Margaret College, Edinburgh, 1997_, pp. 249-258\. London: Taylor Graham.
*   <a id="ref52"></a>Shenton, N., Ducatel, K., Halfpenny, P. & Scott, D. (1991) _Community technology audit: East Manchester Electronic Village Hall_ Manchester: University of Manchester, Centre for Applied Social Research.
*   <a id="ref53"></a>Siatri, R. (1999) "The evolution of user studies." _Libri_ **49**(3) 132-141.
*   <a id="ref54"></a>Sixsmith, A. & Sixsmith, J. (1995) "'Gerontechnology': new technology and the older person." _Generations Review: Journal of the British Society of Gerontology_ **5**(3), 11-12.
*   <a id="ref55"></a>Sjoberg, G. Brymer, R.A., & Harris, B. (1966) "Bureaucracy and the lower class." _Sociology and Social Research_ **50**, 325-337\. [Cited in: Chen, C.C. and Hernon, P. (1982) _Information Seeking: Assessing and Anticipating User Needs_, London: Neal-Schuman Publishers, Inc.
*   <a id="ref56"></a>Spink, A., Bray, K.E., Jaeckel, M. & Sidberry, G. (1999) "Everyday life information seeking by low-income African American households: Wynnewood Healthy Neighbourhood Project." _In_: Wilson, T.D. and Allen, D.K., _eds._ _Exploring the contexts of information behaviour: Proceedings of the Second International Conference on research in information needs, seeking and use in different contexts, 13-15 August, 1998, Sheffield, U.K._, London: Taylor Graham.
*   <a id="ref56a"></a>Spink, A. & Cole, C. (2001) "Introduction to the special issue: everyday information-seeking research," _Library and Information Science Research_, **23**(4), 301-304.
*   <a id="ref57"></a>Staplehurst, B. (1994) _Telecentres: lessons from Europe._ Stoneleigh Park: National Rural Enterprise Centre,
*   <a id="ref58"></a>Sugar, W. (1995) "User-centred perspective of information retrieval research and analysis methods." _Annual Review of Information Science and Technology_, **30**, 77-109.
*   <a id="ref59a"></a>Summers, E.G., Matheson, J. & Conry, R. (1983) "The effect of personal, professional, and psychological attributes, and information seeking behaviour on the use of information sources by educators." _Journal of the American Society for Information Science_ **34**(1), 75-85\.
*   <a id="ref32"></a>" Time to let out the ICT secret." (2001) _Library Association Record_, **103**(10), 580.
*   <a id="ref59b"></a>Vaughan, L.Q. (1997) "Information search patterns of business communities: a comparison between small and medium sized businesses." _Reference and User Services Quarterly_ **37**(1), 71-78\.
*   <a id="ref59"></a>Warner, E.S., Murray, A.D. & Palmour, V.E. (1973) _Information Needs of Urban Residents_. Baltimore, MD: Regional Planning Council. (Final report to the U.S. Department of Health, Education and Welfare, Office of Education, Division of Library Programs, Contract No. OEC-O-71-4555.)
*   <a id="ref59c"></a>Wicks, Don A. (2001). "'Go with the flow: independent older adults and their information seeking." _In_Campbell, D.G. _ed_. _Beyond the web: technologies, knowledge and people. Proceedings of the 29th Annual Conference of the Canadian Association for Information Science, Universite Laval, Quebec City, Quebec, May 26-29, 2001_. pages 149-164\. Toronto: Canadian Association for Information Science.
*   <a id="ref60"></a>Williamson, K. (1997) "The information needs and information-seeking behaviour of older adults: an Australian study." _In_: Vakkari, P., Savolainen, R & Dervin, B., _eds._ _Information Seeking in Context: Proceedings on research on Information Needs, seeking and use in different contexts, 14-16 August, 1996, Tampere, Finland_, pp. 337-350\. London: Taylor Graham.
*   <a id="ref61"></a>Williamson, K. (1998) "Discovered by chance: the role of incidental information acquisition in an ecological model of information use." _Library and Information Science Research_ **20**(1), 23-40.
*   <a id="ref61a"></a>Williamson, K., Schauder, D., Stockfield, L., Wright, S., & Bow, A. (2001) ["The role of the Internet for people with disabilities: issues of access and equity for public libraries."](http//www.alia.org.au/.alj/50.2/full.text/access.equity.html) _The Australian Library Journal_, **50**(2), 157-174\. http//www.alia.org.au/.alj/50.2/full.text/access.equity.html (28 September 2002)
*   <a id="ref62"></a>Wilson, T.D. (1981) "On user studies and information needs." _Journal of Documentation_ **37**(1), 3-15.
*   <a id="ref63"></a>Yin, R. K. (1994) _Case study research: design and methods_, 2nd Edition. London: Sage Publications Ltd.