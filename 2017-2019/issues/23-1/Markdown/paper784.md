<header>

#### vol. 23 no. 1, March, 2018

</header>

<article>

# Becoming an online editor: perceived roles and responsibilities of Wikipedia editors

## [Allison Littlejohn](#author) and [Nina Hood](#author).

> **Introduction.** We report on the experiences of a group of people as they become Wikipedia editors. We test Benkler’s (2002) theory that commons-based production processes accelerate the creation of capital, questioning what knowledge production processes do people engage in as they become editors? The analysis positions the development of editing expertise within broader social contexts to explore how the act of editing supports other types of commons-based production that foster and support and counteract issues of empowerment, equality and social justice.  
> **Method.** Nine editathon participants were interviewed using a semi-structured interview transcript.  
> **Analysis.** In a first round of analysis, data was coded into content areas. Then thematic coding examined: (1) the actions of participants as they became Wikipedia editors and the values and sentiments they ascribed to the role; (2) the emerging beliefs and emotions around becoming an editor and the agency and actions triggered by perceived responsibilities.  
> **Results.** The study surfaced a range of emotions and beliefs editors developed as they took on editing responsibilities and ascribed different values to their new role. These distinct values and beliefs perceived led editors to engage in distinctive forms of action. Some participants developed a sense of empowerment to shape societal agendas, viewing editing as a form of continued activism.  
> **Conclusion.** The findings pinpoint actions people engage in as they move into the editing role, highlighting the values they ascribe to editing. The study also surfaced the perceived responsibilities associated with editing as well as the emotions and beliefs associated with these obligations.

<section>

## Becoming a Wikipedia editor

Wikipedia is one of the frontline, _go to_ sources of information that influences and informs contemporary life. Unlike conventional encyclopedias, Wikipedia is continually edited through the unseen actions of hundreds of thousands of volunteer editors ([Sundin, 2011](#sun11)). Almost anyone with internet access can volunteer to edit, yet people are seldom financially compensated for their work. In the English version alone, over five thousand of these people have made over fifteen thousand edits ([Wikipedia Statistics, 2017](#wik17)). As they create wiki pages and make edits, each individual editor becomes more expert in the collaborative activity associated with online peer production ([Chui _et al._, 2012](#chu12); [Dede, 2000](#ded00)). These emerging ways of working raise questions about the different ways knowledge can be generated, who contributes and how they develop the ability to become contributors ([Fenwick, Nerland and Jensen, 2012](#fen12); [Knorr Cetina, 2007](#kno07)).

Wikipedia editing is a social activity where editors shape articles by engaging, online and offline, with other people around the world who mutually create and edit text ([Jemielniak, 2014](#jem14)). The Wikipedia platform supports editors in a collaborative production process, because the site is divisible into small components that are individual wiki pages or edits on each page, each of which can be produced independently by a different editor ([Benkler and Nissenbaum, 2006](#ben06)). This allows for ongoing, incremental, asynchronous peer production through the activity of editors who search and filter resources, materials, projects and collaborations, and combine these into new constellations ([Benkler, 2003](#ben03)).

In this paper, we explore the experiences of a group of people as they engage in these production processes and develop as Wikipedia editors. We ask the question _what knowledge production processes do people engage in as they become Wikipedia editors?_ In particular we question how emerging responsibilities are perceived by asking _Are Wikipedia production processes aimed towards social influence?_ By asking this question, the study positions the development of editing expertise within broader organiz ational, political, economic, and social contexts. This allows us to explore how the act of editing fosters, supports and counteracts issues of empowerment, equality and social justice ([Bakardjieva and Smith, 2001](#bak01); [Selwyn, 2010](#sel10)).

</section>

<section>

## Roles and responsibilities of Wikipedia editors

The role of the Wikipedia editor has been documented through a number of studies focused on a number of factors, including technical processes (see for example [Bryant, Forte and Bruckman, 2005](#bry05); [Wattenberg, Viégas and Hollenbach, 2007](#wat07)); incentives that encourage individuals to contribute to Wikipedia (see for example [Forte and Bruckman, 2005](#for05)); and self and social organization processes (see for example [Sundin, 2011](#sun11); [Viégas _et al._, 2007](#vie07); [Panciera, Halfaker and Terveen, 2009](#pan09); [Benkler, 2003](#ben03)).

Benkler ([2003](#ben03),376–377) has termed Wikipedia editing as ‘commons-based peer production’ which is shaped by a number of common characteristics. The editing process is a form of voluntary social production emerging alongside contract or market-based, paid-for capitalist production methods ([Sundin, 2011](#sun11)). Peer production is decentralised, rather than being organized by a hierarchical structure, such as a company ( [Benkler, 2003](#ben03)). Because editing is voluntary, the motivations that encourage people to engage in peer production are different from the financial rewards and directives associated with paid work, and are associated more with personal dispositions and attitudes ([Benkler and Nissenbaum, 2006](#ben06); [Forte and Bruckman, 2005](#for05)). Editors co-operate to produce ‘something of value to all’ and can share a sense of purpose or excitement ([Benkler and Nissenbaum, 2006](#ben06)). Thus, rewards of editing range from an altruistic belief in open knowledge to having the ability to project internal self-concepts ([Kuznetsov, 2006](#kuz06); [Yang, 2010](#yan10)). Arguably the combined efforts of Wikipedia editors not only produce common knowledge but also a common influence on society ([Benkler and Nissenbaum, 2006](#ben06)). While commons-based peer production has been positioned as a radical alternative to capitalist production processes, allowing much faster production of knowledge ([Benkler, 2003](#ben03); [Benkler, 2011](#ben11)), it can be argued that editing is not only about the production of knowledge artefacts, but editing is also a form of social influence and activism.

There are various ways editors work together. Editors’ engagement may be mediated online, through working on the wiki pages, or offline, through sourcing and interpreting artefacts ([Jemielniak, 2015](#jem15)). To ensure Wikipedia is a valid information source, each statement has to be verifiable and editors are required to spend a great deal of time and effort in finding and citing primary sources to validate the knowledge claims they make ( [Bryant, Forte and Bruckman, 2005](#bry05)). Thus, co-operation around Wikipedia editing requires much more collaboration than simply mutual cooperation around behavioural, editing tasks. It also requires a commitment to a particular approach to interpreting and describing artefacts ( [Maharg and Owen, 2007](#mah07)). Primary sources of knowledge are open to interpretation, so the content of each article is subject to editorial discretion developed via consensus ([Sundin, 2011](#sun11)). If an interpretation is considered to be inaccurate, another editor can elect to change it using a number of peer production processes ( [Panciera, Halfaker and Terveen, 2009](#pan09)). Editing actions range from creating new pages to repairing or updating pages created by other editors, or engaging in systematic, repetitive activity, such as making similar edits to a sequence of related pages ( [Wattenberg, Viégas and Hollenbach, 2007](#wat07)). Editors might focus on editing pages with specific content categories or, alternatively, they may elect to edit multiple pages at the same time _(ibid)_.

Each Wikipedia editor has opportunity to influence social opinion. The influence that each Wikipedia editor can contribute to the commons affords a unique effect that is not generally found in conventional capitalist, work-based production processes, such as making a product ([Benkler, 2003](#ben03)). It does, however, have parallels with the social influence of journalism, arguably on a bigger scale. Some Wikipedia editors choose to use their influence as a form of activism, imposing their personal views and interpretations instead of offering an unbiased perspective ([Konieczny, 2014](#kon14)). Not every editor views an article in the same way, and their diverse perspectives can lead to conflicts about the knowledge produced ([Jemielniak, 2016](#jem16); [Kittur _et al._, 2007](#kit07)). Some editors routinely remove or add text produced by others ([Halfaker _et al._, 2011](#hal11)). The stakes are high since Wikipedia is read every day by millions of people worldwide and articles can influence social perception ( [Konieczny, 2009](#kon09); [Tkacz, 2010](#tka10)). Disputes over text can create interaction orders where ‘power editors’ continually (and sometimes aggressively) remove text and replace it with their own views ([Viégas _et al._, 2004](#vie04)). These editors’ overly-influence articles, impacting the quality and quantity of contributions ([Jemielniak, 2016](#jem16); [Jemielniak, 2013](#jem13); [Panciera _et al_., 2009](#pan09)). This activity is sometimes referred to as ‘trolling’.

It has been recognised by the Wikipedia Foundation that biases governing the presentation and production of Wikipedia pages can lead to the marginalisation of under-represented social groups, including women ([Antin _et al._, 2011](#ant11); [Collier _et al._, 2012](#col12); [Jemielniak and Wilamowski, 2017](#jem17)). To address this problem, organizations such as national libraries and universities have taken action to actively encourage more people, particularly those from marginalised groups, to become Wikipedia editors through editathon training events. The next section describes the editathon event that forms the context of this study.

</section>

<section>

## Context: the Edinburgh Seven Editathon

### The Edinburgh Seven

In 1869 Sophia Jex-Blake, along with a group of other women, applied to the University of Edinburgh to study Medicine. Before then no British University had enrolled a woman as a student since women were not considered appropriate for admission to university, irrespective of ability. The application was eventually accepted by the University Court, paving the way for women’s entry to university education in Britain. For this group, known as the Edinburgh Seven, admission to university was a milestone in the personal journey towards becoming medics and also in the campaign for women’s rights to access university. Although this story represents a landmark moment for British universities, it was not well represented online in social media. Therefore, in 2015 staff from the University’s Learning, Teaching and Web Services Division selected this event for documentation in Wikipedia. A new Wikipedia entry about the Edinburgh Seven was to be created during a dedicated ‘editathon’ event at the University of Edinburgh.

</section>

<section>

### The Editathon

An editathon is an organized event where people come together at a scheduled time to create or edit Wikipedia entries on a specific topic. Participants engage in all kinds of collaborative activity around the creation of wiki pages, aided by information specialists including librarians, archivists and ‘Wikimedians in residence’ (professionals employed to carry out all sorts of tasks associated with Wikipedia). Editathons are underpinned by two broad intentions; first to contribute to Wikipedia by creating or editing content, and second, to support people in developing the knowledge and expertise needed to act as Wikipedia editors. Participation is voluntary, with individuals determining the nature and level of their engagement, and activities include offline and online actions.

The editathon took place in February 2015 on the Edinburgh University campus over four afternoons. The event was led by the University's Information Services in association with the School of Literature, Languages and Cultures, the Moray House School of Education, EDINA, and the National Library of Scotland. The editathon was open to everyone, and was particularly targeted at university students, staff and faculty as well as members of the public who had an interest in becoming Wikipedia editors. A total of 47 participants were active during one day or across multiple days. Participants determined how much of the event they attended as well as their level of engagement.

The event was situated in a large classroom. The room was organized such that each participant could select a specific topic of interest and volunteer to lead or contribute to the creation and editing of the wiki page. Flip charts were made available in the physical space to support the participants in organizing their activity. The participants were purposefully co-located with and had access to a range of participating experts including local archivists who supported access to original materials, media specialists who helped with documenting relevant locations, academic colleagues with specialist knowledge on women’s history and Wikimedia experts, including a ‘Wikimedian trainer in residence’, who provided training on how to edit Wikipedia and participate in an open knowledge community. Participants also had access to a range of physical artefacts - including archived materials such as newspaper reports and photographs and books – to help them write the articles. Following the editathon event nine participants were invited to participate in an interview to discuss their involvement in and experience of the event (see the [Women, Science and Scottish History Editathon Series, 2017](#wom17)).

</section>

<section>

## Method

### Method selection

The study uses a quasi-ethnographic method, using narrative accounts of how editathon participants develop editing expertise as they engage in practical activities (Cf. [Hammersley and Atkinson, 2007](#ham07); ). The study takes a critical perspective ([Selwyn, 2010](#sel10)), using the ethnographic data to examine how roles and perceived responsibilities emerge within a community of editors.

The editathon event is viewed as a community of people who are carrying out digital work. This approach fits with Selwyn’s ([2010](#sel10))proposition that studies of the use of social technologies, such as Wikipedia, should be focused on developing narratives as ‘thick descriptions of the present uses of technologies in situ’ (p 69), in order to develop analyses that are ‘context-rich’. Examining experiences of how people engage with digital tools through the use of qualitative methods enables analysis of how online activity is construed in participants’ ‘everyday lifeworlds’ ([Bakardjieva and Smith, 2001](#bak01), p67). This approach provides a way to contextualise knowledge construction within the broader contexts of each individual’s life and work as they become a Wikipedia editor.

Drawing on the work of ([Cristancho and Fenwick, 2015](#cri15))we posit that critical analysis of the act of ‘becoming’ offers a more nuanced understanding of expertise development than examining competency development and improvement. The process of ‘becoming’ an expert, such as a Wikipedia editor, shapes the individual’s capability, confidence and identity _(ibid.)_. As they develop as editors, individuals experience tensions and conflicts related to intentions and motivations as well as practice. This approach facilitates examination of the individual’s critical awareness of how contemporary social media resources, such as Wikipedia, are shaping how people engage with information and construct knowledge. It questions how social media is shaping an individual’s lived experiences and system-wide expectations, patterns of behaviour and modes of thinking. The approach gives insight into how the act of becoming an editor facilitates a movement beyond the immediate context of expertise development.

Interviews were selected as a method to gather narrative accounts of participants’ collective experiences and strategies, rather than to identify each individual’s particular approach to becoming an editor. Therefore, to allow for a diversity in the sample, each interviewee was selected based on his or her position within the network.

</section>

<section>

### Participant selection

Nine interview participants were selected according to their online activity, reported earlier in a Social Network Analysis of the online interactions of the editathon participants ([Rehm, Littlejohn and Rientes, 2017](#reh17)). The results reported in this paper use anonymised names. All nine had exhibited different editing behaviour. Two people were event organizers (Melanie, Margaux) and central editors (Anita, Jessica), two had created new wiki pages not co-edited by others (Greg, Elisabeth), two had made minor changes to wiki pages initiated by other editors (Carolyn, Natalie), one (Sarah) had not created or edited a wiki page.

Interviews lasting up to one hour were conducted via Skype, which allowed time for the interviewees to reflect and elaborate on their experiences, while, at the same time, limiting the demands on their time. The interviews were semi-structured and allowed for open ended answers such that the data took into account participants’ background, culture and motivations as well as actions (Cf. [Hammersley and Atkinson, 2007](#ham07),p67). In the first phase of the interview each participant was questioned about his or her background, intentions and motivations for participation as well as experiences of the editathon. In the next phase participants were asked to comment on what they did during the event, their engagement with other participants, and their behaviour and activity since the editathon. The interviews were audio-recorded and transcribed verbatim.

</section>

<section>

### Data analysis

The narrative accounts of each participant’s experience in the editathon were analysed through two successive rounds of analysis. The first stage of analysis involved coding the data into initial content areas, what Miles and Huberman([1994](#mil94))have termed the descriptive, interpretive stage. The second stage moved from descriptive to thematic coding, focused on two overarching themes that emerged from the data: (1) the actions of participants as they became Wikipedia editors and the values and sentiments they ascribed to the role and (2) the emerging beliefs and emotions around becoming a Wikipedia editor and the agency and actions triggered by perceived responsibilities.

There are limitations associated with the method. First, the qualitative study uses self-report data, which may be influenced by honesty and image management as well as inconsistencies between perception and reality. Second, the data was reported retrospectively, which is likely to increase data inconsistency since memory recall could be impaired. In the following section, the results are outlined and discussed in relation to the published literature on Wikipedia editing.

</section>

<section>

## Results and discussion

In the opening section we highlighted Benkler and Nissenbaum’s ([2006](#ben06)) hypothesis that motivations to engage in Wikipedia editing were likely to be associated with personal dispositions and attitudes. This proposition was validated by the editathon participants, who expressed a range of motivations that led them to participate in the event. These motivations often were shaped by past experiences or current concerns. As organizers of the event, Melissa and Margaux were motivated to increase the number of women editors and to improve the representation of women within Wikipedia in an unbiased and representative way. These aims were associated with their leadership roles within the University where they worked and their commitment to an equality agenda. Greg’s motivation to participate in the event was also influenced by his work as an archivist. His aim was to ‘guide people to university’s digital collection’. The archives provided an authoritative historical evidence base for Wikipedia and other forms of contemporary media. Greg realised that exposure from social media could draw attention to the archives as a rich source of useful information. Carolyn was also motivated by her job role as an academic. Her aim was to explore different forms of writing and her interest was in how information could be communicated via social media. Anita joined the editathon to help a colleague, but both she and Jessica were concerned with equality issues. Sarah specifically was concerned about the under-representation of women scientists in online media and wanted to redress this imbalance by contributing to Wikipedia. Carolyn and Natalie were interested in social media and its influence on society.

As they developed editing expertise, each of the participants become more and more invested in the sorts of peer production activities described in the literature, including self and social organization activities such as organizing the division of labour (Cf. [Sundin, 2011](#sun11)), activities around sourcing and interpreting artefacts to validate the knowledge claims (Cf. [Bryant, Forte and Bruckman, 2005](#bry05))and technical editing activity (Cf. [Wattenberg, Viégas and Hollenbach, 2007](#wat07)). Many of the participants had anticipated that they would engage in technical activity, as they learned to create and modify Wikipedia pages. Yet few had appreciated the need to engage in processes validating knowledge claims, nor had they anticipated how difficult these activities and social engagement tasks might be. The interviewees highlighted three distinct areas of activity that they found particularly stimulating or challenging: social engagement, sourcing information from primary sources, and creating accurate representations online. The different motivations for participation were associated with distinct forms of social engagement, described in the next section.

</section>

<section>

### Forms of social engagement

The editathon was planned as a social event where people organized and carried out activities together in a shared space. However, people tended to engage in commons-based peer production processes with others who wanted to achieve mutual outcomes. Therefore, groups of people with shared goals, values and beliefs tended to work together. This finding both validated and expanded Benkler and Nissenbaum’s ([2006](#ben06))proposition that personal dispositions and attitudes impact engagement by establishing that sub-groups of people within a single community focus on specific types of activity, depending on their shared social values representation.

One group of participants focused on creating the knowledge artefacts, while another group worked together more closely to focus on ways to represent each of the Edinburgh Seven online. This second group were particularly concerned about how women are depicted online and how they might positively influence the representation of women.

Two sub-groups of people began to emerge, either working on specific processes such as creating the wiki pages or sourcing and interpreting primary data. Wattenberg and colleagues ([2007](#wat07)) previously observed that editors choose to engage in different forms of activity. This study extends these findings by connecting the actions of the editors to their explicit motivations for editing.

Anita and Jessica, who were concerned with equality issues, described similar social engagement in the event, recounting it as a positive experience:

> "On the day we were in a teaching studio room, so nobody really -unless you deliberately chose to - nobody was really sitting on their own. Although you might be working on your own computer, you would tend to sit in clusters of 2, 3, 4 and so just as you find stuff there’s that element of ‘Oh look what I found’ type thing and a lot of chat across the table." (Anita)

> "[We were] agreeing around a table ‘right you go and look what’s in the Scotsman archive and I’ll have a look at what’s in here’ and then we’d just share it and talk and then we’d all come up with some fantastic thing that we’d just found. It was a very, very nice experience." (Jessica)

Anita described how deepening personal relationships further encouraged participation:

> "People got hooked into it and particularly when we came back for more than one session. The personal relationships evolved and we have a ‘Wiki hour of power’ every month, that’s sort of kept some of those personal relationships going as well … It’s much about cementing social bonds that we’ve made, but we have drawn new people in, started editing by being part of that little community that we’ve got ....we have a sort of come and buddy up with somebody if you want to go and see what somebody else is working on and talk about what you want to work on and how you might approach it."(Anita)

Carolyn, who had been motivated to participate to explore new forms of writing, also described the positive experience of working with others:

> "You had some periods where you were just working by yourself, but when you come together there’s a shared activity and bringing it together and often food and coffee and things like that are significant parts of this. It’s a social occasion as well as a productive time." (Carolyn)

Those who talked more about working by themselves focused on creating or editing specific wiki pages. Those who described working with others tended to outline how they sourced primary material then worked together to interpret these materials as text for the wiki pages. These two distinct vocations were not mutually exclusive but represented ways of working on different but related goals. Sarah described this phenomenon as:

> "Two separate sides [to the room]. There was a noisy side that sounded like it was collaborating a lot… and then there was a quiet side I get the feeling was doing some editing." (Sarah)

Others tended to engage in tasks sourcing primary materials and negotiating meaning around these data. Those who were ‘quiet’ tended to be engaged in direct editing and creation of Wikipedia pages:

> “As soon as we started editing it was like pick a person, edit them, add them or whatever you need to do.”

> “There was the very chatty side [of the room]… I got the feeling that the ‘chatter’ wasn’t doing much editing and that it was actually if you got your head down and did it then you did your Wikipedia editing, but that obviously wasn't a collaborative thing.”

> “The chatty side was more looking up source material, there were bits of paper and there were people researching on computers. There was a lot of chatter and excitement about things people were finding, but I don’t get the feeling that that went on to Wikipedia in the end.”

> “It was like loads of kids at school again, we’d all got this skill and were learning new things and that was really exciting and I think some people respond to that by shouting out everything they just learnt about [one of the Edinburgh Seven].“(Sarah)

This narrative account describes distinct pathways to becoming a Wikipedia editor influenced by the shared values, beliefs and goals of the participants. These findings also validate Benkler’s ([2003](#ben03)) theory that commons-based production processes accelerate the production of capital. The study also tested Benkler’s ([2003](#ben03)) theory that the act of editing supports commons-based production aimed towards fostering, supporting, and counteracting issues of empowerment, equality and social justice, as outlined in the next section.

</section>

<section>

### Editing to influence

For Wikipedia editors it is important to be able to interpret primary historical data in ways that make the information relevant for a contemporary readership. Several participants found this task challenging. Anita expressed her surprise at how difficult this was:

> “I thought well you know it’s just writing text on a Wiki page – how hard can this be? When you’re writing about a person that first sentence, making it clear. I hadn’t really anticipated that [level of difficulty].” (Anita)

Several of the participants focused on sourcing and interpreting primary resources in ways that would positively represent the Edinburgh Seven. Wikipedia requires direct reference to primary resources. Five of the nine interviewees perceived that sourcing and interpreting information from primary sources was more challenging than they had expected. The primary resources were distributed across a range of different media; official (written) records, archived photographs, newspaper articles. They had not anticipated the expertise and effort required to interrogate archive material to find the resources needed and interpret these data sources to draw together the information needed for each wiki page.

In their descriptions of how they sourced primary data Anita and Elizabeth both express the shared values and positive emotions shared with others. Anita exercised a high degree of effort to search for and source archived information that provided (previously unknown) insight into how one of the historical figures joined the Edinburgh Seven:

> “I had to dig and dig and dig and dig and eventually I found a tiny, tiny, tiny, little classified advert in a Birmingham newspaper that was offering a £50 scholarship for a lady to go and study medicine in Edinburgh with Sophia Jex-Blake. It was a leading suffragette at the time who sponsored this and the little advert was to say that the scholarship had been awarded to Emily Bovell to come to Edinburgh.

> I had to read an awful lot around the rest of the story to work out the time period in which she must have arrived in to search to find this. But it filled in a jigsaw piece in that larger story. So a lot of the books that has been written about it since [said] she was one of the original Edinburgh Seven. We knew that couldn’t be true. But it had been perpetuated for so many years in so many of the texts that … we really had to dig back into the primary sources to find it.

> It was a whoopie moment when we found it. Everybody clustered round the table. I mean it was the most boring little advert you’ve ever seen in your life, but you know the impact of it was big. So that sort of playing detective element was really engaging.”(Anita)

> “I did like the idea of finding things that people hadn’t found before. I also enjoyed seeing colleagues really excited about finding things. It was nice to get involved in a community in that way.”(Elizabeth)

Several participants were interested in learning how to represent physical media online. Carolyn expressed her interest as follows:

> “I’m very interested in… how we take old forms of print and even pre-print and even things from the oral tradition as well, how we take them forward into new media and incorporate them and change them in the process. It’s really interesting.” (Carolyn)

Two participants recognised that that the media they selected from the archives for the Wikipedia pages was likely to influence how the Edinburgh Seven are perceived by today’s society and how this perception might be shaped by bias and intolerance. Jessica described why a colleague’s reaction to a photograph of one of the Edinburgh Seven prompted her to find a different image:

> “[In a conversation] I was having with a colleague who was talking about Sophie Jex-Blake as a ‘battleaxe’. Then somebody else was saying ‘Yeah but she does look like a ‘battleaxe’ in the picture… [I was] thinking we don’t have to use this one image that is used everywhere. There are other, better images of her which we can use, we can upload.”

> “I changed her picture to a much younger one... I think it was worth having [a photograph of] her at the time when she would have been a part of the medical school. If you Google image of her there’s one of her sort of older, with her finger on her face. So that’s the one I removed.” (Jessica)

The participants were attentive to the influence of media in shaping how prominent women are perceived by today’s society. As Jessica explained:

> “I just really hate it when you see these pictures that get used and you just think there’s no need - you’ve made a decision about a picture you’re going to use, like that one of Sherri Blair at the front door of number 10, it’s like these decisions get made by picture editors and some of the times they are designed to make women be laughed at.” (Jessica)

Some participants expressed an emerging feeling of responsibility associated with the ways the Wiki pages might influence public opinion about the Edinburgh Seven and about women in general. Carolyn is one example and she explained how this growing sense of responsibility around how women are perceived encouraged her to contribute further to the collective:

> “I think on the day that I did lots of editing I got really into finding more about the person I was looking up and really interested in the subject.... So there’s a certain amount of ownership of the subject then that became quite interesting and did make me think I should go and do more kind of digging into particular individuals, but I think that’s partly because I don’t do a lot of…I was doing this about historical stuff on Wednesday but that’s normally what I focus, I don’t normally do a lot of historical stuff.”

These emotions associated with relating historical events to contemporary issues appeared to provoke a sense of responsibility and agency in influencing the prevailing societal norms, particularly the ways women are represented in the media. However, not everyone shared this experience and, as Anita observed “Some people were there just to dip into the experience and maybe weren't grabbed by the subject matter in quite the same way” (Anita).

</section>

<section>

### Editing to produce artefacts

Some of the editathon participants were more concerned with publishing and production processes than in social influence. For example, Greg described how he focused on peer production processes:

> “We prepared a long list of people, most of them were women who were kind of notable and didn’t have any presence on Wikipedia. I basically just churned through a few of those until I found one that didn’t have a page. [The subject matter] was quite interesting but… I was much more interested in learning about the processes... Then, when you ‘Google’ the person, your Wikipedia page is at the top of the results”.

> “There was a small group of people who I was kind of on the periphery of, I wasn't a core member of that group that were very excited about [the topic]. So I suppose they were probably more collaborative. I just turned up and did my editing and left... Most people probably were more connected and sharing and adding to different articles and that kind of thing.” (Greg)

Greg’s excitement was sparked by creating and sharing knowledge widely within the public domain. Sarah also elected to research and write a new wiki page, though she did not create the page herself:

> “I wanted to go and research somebody new. A lot of the pages that we’d been given as examples to edit seemed to have quite a lot of information on them already. We only had 3 hours or 2 hours or something and I thought that a lot of my time, if I was going to edit a page, would be just searching for something that wasn't on that page already. So with a brand new one I felt I could make a bit more impact and it was great to get the information up there. Yeah it was a bit nerve-wracking, but it was fun.” (Sarah)

Greg and Sarah both were motivated to produce unique contributions to the collective set of wiki pages, rather than to engage in other types of activity. Thus, there were two distinct types of editing activity. First, some people focused on the production of new artefacts, selecting a specific page through indirect cooperation, then working alone to produce the page. Second, others focused on sourcing and interpreting primary data to ensure the representations of the wiki pages influenced positively and accurately. This activity often involved searching alone alongside direct collaboration with a group of people. These different ways of editing validate Sundin’s ([2011](#sun11)) observation that no single entity owns a Wikipedia site or manages its direction. Instead, the site emerges from the collaboration and contributions of the editors as they volunteer in their spare time. This study provides additional insight into the ways the contributions loosely are associated with values and beliefs about editing responsibilities. The different activities editors engage in represent distinct and equally valid ways of contributing to the wiki site as an editor. Editors’ perceptions of their editing responsibilities are associated with these values.

</section>

<section>

### Perceptions of editing responsibilities

Most participants we talked with described how their involvement in the editathon had prompted a change in their beliefs and influenced their actions. One powerful influencing force was the context and the co-location of the editathon event in the place where the Edinburgh Seven had studied and where many of the editathon participants worked. Five out of nine of the interviewees described how they could relate to the experiences of the Edinburgh Seven. Anita made direct comparison between the Edinburgh Seven and her personal experiences:

> “I learned stuff about the history of the institution I work in and it’s the institution I studied in as well. So there’s an extra dimension there for me because 150 years before I might not have had that opportunity.” (Anita)

Specific responsibilities related to the representation of women in social media resonated with almost all of the female participants we interviewed. Most drew comparisons between the difficulties faced by the Edinburgh Seven to their own experiences as women and to the ways women are viewed by society in general. Jessica drew parallels between the use of newspaper media to oppress the Edinburgh Seven and the ways contemporary social media sometimes are used to suppress women today:

> “One thing that I realised was that newspapers at the time were not like newspapers are now. They were [the equivalent of] social media. Big public fall outs would be published in the newspapers. You know, conversations, letter writing between people would exist in the newspapers and gossip as well… It made me realise the parallels between the kind of trolling you get online now and the same sort of thing there, you know the slight intimidation. They’d have obscene letters put through their letter box and all of this is sort of lost in history somehow.” (Jessica)

> “That whole approach of making people look at Wikipedia critically rather than passively I think is really important and knowing that you can change it, you can be part of that.“ (Jessica)

This growing understanding of how media representations were used in the past to suppress women alongside comparison with how social media is used in current society to subdue women led to the conclusion that little has changed. This realisation led some of the participants to want to influence how women are represented within social media spaces, like Wikipedia. Some of the participants explained their anxiety about modern-day forms of public shaming such as web _trolling_. Half the women we interviewed expressed anxiety about ‘trolling’ in Wikipedia, fearing that people (reviewers and editors) from outside the editathon group might take down or rewrite their contributions. These anxieties associated with trolling led to increased effort in terms of proofing and checking the Wiki pages before they were made public. Sarah described in detail how the anxiety of publishing influenced her actions:

> “As soon as I [edited the wiki page] I think I was scared to press the publish button to begin with because Wikipedia is such a massive resource and anybody can access it… So pressing the publish button …did feel a bit nerve-wracking. I definitely proof read it a million times… and edited it a million times. There were a few things that made it more nerve-wracking. Firstly, the Wikipedia community were immediately on to it, so I was worried I might get it taken down or something.” (Sarah)

Trolling was perceived as a significant issue for women who may have been discouraged from expressing their views from a young age. Jessica perceived this problem could have a lasting effect and that _“women editors… feel that our voice isn’t perhaps as important or as valuable’_.

Several of these women articulated strong emotions associated with publishing wiki pages and putting ideas in the public domain:

> “It’s an emotional connection… Within, I’d say, less than 2 hours of me putting her page in place it was the top hit that came back in Google when I Googled it and I just thought that’s it, that’s impact right there!” (Anita)

Some participants expressed unease that men participating in the editathon might feel marginalised. In their view, a follow up event should focus around a topic that could be adopted and experienced equally by male participants. As Greg noted:

> “The whole drive behind it was trying to improve the representation of women on Wikipedia and trying to get more female editors to redress the balance of that problem. So it would have been hard to have those aims without maybe coming across as a little bit excluding of men. But I think that was just the nature of it and I think the next Editathon that we’re looking at doing would probably be a bit more of a neutral topic.” (Greg)

The editathon had two overarching objectives: improving the representation of women online and increasing the number of women Wikipedia editors. Elizabeth considered these objectives sometimes were conflated, leaving participants confused about the purpose:

> “If we’re trying to achieve getting our women on Wikipedia, great. If we’re trying to achieve getting more female Wikipedia editors, great. But is it both?” “I think … trying to fix it with the same brush or bolt … was confusing and potentially alienating for some.” (Elizabeth)

This confusion could be because individual participants perceived the editing role and the associated responsibilities differently. For some becoming an editor opened up a sense of responsibility to ensure women are represented positively in online, social media. Editing provided an opportunity to engage in a form of social activism. Others viewed their responsibility as contributing information online. These different perceptions of the responsibilities led to the distinct forms of action. For some participants production processes engender a sense of agency and responsibility to (re)construct history on Wikipedia in ways that communicate shared values and beliefs.

</section>

<section>

## Conclusions

This study used narrative accounts of editathon participants to trace the actions people engaged in an editathon as they become Wikipedia editors. This study investigated the _actions people engaged in as they become Wikipedia editors_ when they participated in the Edinburgh Seven editathon that took place at the University of Edinburgh in 2015.

Although the participants shared a common goal of creating the wiki sites, they did not contribute in the same way. Instead they engaged in a range of related but distinctly different commons-based processes depending on each individual’s emotions and beliefs and how these influenced the ways they perceived and enacted their emerging roles and responsibilities. This observation is in agreement with previous studies that identified a range of different actions Wikipedia editors engage in as they produce wiki pages (see for example [Sundin, 2011](#sun11); [Viégas _et al._, 2007](#vie07); [Panciera, Halfaker and Terveen, 2009](#pan09)).

We were specifically interested in testing Benkler’s ([2003](#ben03)) theory that social media sites enable people to engage in commons-based processes that accelerate the production of capital as wiki-based knowledge artefacts. The findings confirm this theory, but also provide insight into the different forms of production activity that editors engage in and why they choose to engage in specific ways.

The findings identified two distinctly different forms of production loosely associated with values and beliefs about editing responsibilities and represent distinct and equally valid ways of contributing to the wiki site as an editor. First, commons-based production of new artefacts where individuals indirectly cooperated with others by selecting a wiki page to focus on, then produced the page, largely by working alone.

Benkler and Nissenbaum ([2006](#ben06)) imagined that the emergence of new forms of peer production offers an opportunity for more people to engage in practices that permit them to exhibit and experience virtuous behaviour. This hypothesis is validated by the second type of production process observed; the production of artefacts in ways that not only intended to produce knowledge artefacts, but were deliberate actions intended to influence the ways women are portrayed through social media. These production processes included sourcing and interpreting primary data to ensure the representations of the wiki pages influenced positively and accurately. Thus, some commons-based production processes were aimed at fostering, supporting, and counteracting issues of empowerment, equality and social justice.

All participants interviewed became more aware of the ways in which the Internet and digital media shape the information with which society engages and how this information is interpreted and used to create particular [historical] narratives.

Some perceived their role as taking on responsibility for ensuring accurate representation of offline, physical resources in an online form. This responsibility motivated many of the editors to develop more critical understandings of content and information in the digital age. Engagement within the editathon left some participants with a sense that they can shape societal agendas through simple editing actions and, for some, editing has become a form of continued activism.

These actions included social engagement with others, both online and offline, sourcing primary information and interpreting information in ways that can be represented online. Actions also included navigating the complex relationship between online and offline artefacts. These include primary physical resources, such as archived news reports, photographs and historical buildings, and the (re)constructed digital artefacts created as wiki pages.

The findings of this study validate Benkler and Nissenbaum’s ([2006](#ben06)) idea that commons-based peer production processes, such as Wikipedia editing, serve as a form of social influence and that volunteers can be motivated to change societal views.

</section>

<section>

## Acknowledgements

The authors would like to thank the University of Edinburgh for funding this project and Melissa Highton, Director of Learning, Teaching and Web Services, for support.

## <a id="author"></a>About the authors

**Professor Allison Littlejohn** is Academic Director for Digital Innovation at The Open University, UK and Chair of Learning Technology, researching professional and digital learning. Allison has held research Chairs at three UK universities and has worked throughout her career in the area of learning innovation, technology, knowledge creation and academic-business partnerships. She can be contacted at [allison.littlejohn@open.ac.uk](mailto:allison.littlejohn@open.ac.uk)  
**Dr Nina Hood** is a Lecturer in the Faculty of Education and Social Work at the University of Auckland, New Zealand where she specialises in new technologies in education. Nina undertook her doctoral work at the University of Oxford where she was a member of the Learning and New Technologies Research Group. Her current research explores how technology can be used to support teacher learning and knowledge mobilisation. Nina can be contacted at [n.hood@auckland.ac.nz](mailto:n.hood@auckland.ac.nz)

</section>

<section>

## References

<ul> 

<li id="ant11">Antin, J., Yee, R., Cheshire, C. &amp; Nov, O. (2011). Gender differences in Wikipedia editing. In <em>Proceedings of the 7th International Symposium on Wikis and Open Collaboration</em> (pp. 11-14) New York, NY: ACM.</li>

<li id="bak01">Bakardjieva, M. &amp; Smith, R. (2001). The Internet in everyday life: computer networking from the standpoint of the domestic user. <em>New Media &amp; Society, 3</em>(1), 67-83.</li>

<li id="ben03">Benkler, Y. (2003). Freedom in the commons: towards a political economy of information. <em>Duke Law Journal, 52</em>(6), 1245-1276.</li>

<li id="ben11">Benkler, Y. (2011). Network Theory. Networks of power, degrees of freedom. <em>International Journal of Communication, 5</em>(39), 721–755.</li>

<li id="ben06">Benkler, Y. &amp; Nissenbaum, H. (2006). Commons-based peer production and virtue. <em>Journal of Political Philosophy, 14</em>(4), 394-419.</li>

<li id="bry05">Bryant, S. L., Forte, A. &amp; Bruckman, A. (2005). Becoming Wikipedian: transformation of participation in a collaborative online encyclopedia. In <em>Proceedings of the 2005 international ACM SIGGROUP conference on supporting group work</em> (pp. 1-10) New York, NY: ACM.</li>

<li id="chu12">Chui, M., Manyika, J., Bughin, J., Dobbs, R., Roxburgh, C., Sarrazin, H., Sands, G. &amp; Westergren, M. (2012).<a href="https://www.mckinsey.com/~/media/McKinsey/Industries/High%20Tech/Our%20Insights/The%20social%20economy/MGI_The_social_economy_Full_report.ashx"><em>The social economy: unlocking value and productivity through social technologies</em> </a> Report for McKinsey Global Institute.  Retrieved from https://www.mckinsey.com/~/media/McKinsey/Industries/High%20Tech/Our%20Insights/The%20social%20economy/MGI_The_social_economy_Full_report.ashx.</li>

<li id="cri15">Cristancho, S. &amp; Fenwick, T. (2015). Mapping a surgeon’s becoming with Deleuze. <em>Medical humanities, 41</em>(2), 128-135.</li>

<li id="col12">Collier, B. &amp; Bear, J. (2012). Conflict, criticism, or confidence: an empirical examination of the gender gap in wikipedia contributions. In <em>Proceedings of the ACM 2012 conference on computer supported cooperative work</em> (pp. 383-392) New York, NY: ACM.</li>

<li id="ded00">Dede, C. (2000). <em>The role of emerging technologies for knowledge mobilization, dissemination and use in education.</em> Commissioned by the Office of Educational Research and Improvement, U.S. Department of Education.</li>

<li id="edi17">Edinburgh Seven. (2017). In <em><a href="http://www.webcitation.org/6vsK3IqIm">Wikpedia: the free encyclopedia.</a></em> Retrieved from  https://en.wikipedia.org/wiki/Edinburgh_Seven (Archived by WebCite® at http://www.webcitation.org/6vsK3IqIm)</li>

<li id="fen12">Fenwick, T., Nerland, M. &amp; Jensen, K. (2012). Sociomaterial approaches to conceptualising professional learning and practice. <em>Journal of Education and Work, 25</em>(1), 1-13.</li>

<li id="for05">Forte, A. &amp; Bruckman, A. (2005). <a href="http://jellis.org/work/group2005/papers/forteBruckmanIncentivesGroup.pdf">Why do people write for Wikipedia? Incentives to contribute to open–content publishing</a>. <em>Proceedings of Sustaining Community: The role and design of incentive mechanisms in online systems, Workshop at Group 2005, Sanibel Island, FL USA.</em> Retrieved from http://jellis.org/work/group2005/papers/forteBruckmanIncentivesGroup.pdf (Archived by WebCite® at http://www.webcitation.org/6vsKMB5CH)</li>

<li id="hal11">Halfaker, A., Kittur, A. &amp; Riedl, J. (2011, October). Don't bite the newbies: how reverts affect the quantity and quality of Wikipedia work. In <em>Proceedings of the 7th international symposium on wikis and open collaboration</em> (pp. 163-172)New York, NY: ACM.</li>

<li id="ham07">Hammersley, M. &amp; Atkinson, P. (2007). Ethnography: principles in practice. (3. ed.) Milton Park, Abingdon, Oxon: Routledge.</li>

<li id="jem13">Jemielniak, D. (2013). <a href="https://works.bepress.com/jemielniak/25/">Change, Rebellion, or Else? Wikimedia Movement Governance</a>. <em>Conference of the International Network of Business and Management Journals</em>, 17-19 Jun 2013, Universidade De Lisboa, Lisbon, Portugal. Retrieved from https://works.bepress.com/jemielniak/25/ (Archived by WebCite® at http://www.webcitation.org/6vsL53ywe)</li>

<li id="jem16">Jemielniak, D. (2016). Wikimedia movement governance: the limits of a-hierarchical organization. <em>Journal of Organizational Change Management, 29</em>(3), 361-378.</li>

<li id="jem14">Jemielniak, D. (2014). <em>Common knowledge? An ethnography of Wikipedia.</em> Stanford, CA: Stanford University Press.</li>

<li id="jem15">Jemielniak, D. (2015). Naturally emerging regulation and the danger of delegitimizing conventional leadership: drawing on the example of Wikipedia. In H. Bradbury (Ed.), <em>The SAGE Handbook of Action Research.</em> (pp.522-528) London, UK – New Delphi, India - Thousand Oaks, CA: Sage.</li>

<li id="jem17">Jemielniak, D. &amp; Wilamowski, M. (2017). Cultural diversity of quality of information on Wikipedias. <em>Journal of the Association for Information Science and Technology. 68</em>(10), 2460-2470.</li>

<li id="kit07">Kittur, A., Suh, B., Pendleton, B. A. &amp; Chi, E. H. (2007, April). He says, she says: conflict and coordination in Wikipedia. In <em>Proceedings of the SIGCHI conference on Human factors in computing systems</em> (pp. 453-462)New York, NY: ACM.</li>

<li id="kno07">Knorr Cetina, K. (2007). Culture in global knowledge societies: knowledge cultures and epistemic cultures. <em>Interdisciplinary Science Reviews, 32</em>(4), 361-375.</li>

<li id="kon09">Konieczny, P. (2009). Wikipedia: community or social movement? <em>Interface: a journal for and about social movements, 1</em>(2), 212-232.</li>

<li id="kon14">Konieczny, P. (2014) The day Wikipedia stood still, <em>Current Sociology, 62</em>(7), 994-1016</li>

<li id="kuz06">Kuznetsov, S. (2006). Motivations of contributors to Wikipedia. <em>ACM SIGCAS computers and society, 36</em>(2), 1-7.</li>

<li id="mah07">Maharg, P. &amp; Owen, M. (2007).<a href="http://www2.warwick.ac.uk/fac/soc/law/elj/jilt/2007_1/maharg_owen/maharg_owen.pdf">Simulations, learning and the metaverse: changing cultures in legal education.</a><em>Journal of Information Law and Technology 12</em>(1). Retrieved from http://www2.warwick.ac.uk/fac/soc/law/elj/jilt/2007_1/maharg_owen/maharg_owen.pdf (Archived by WebCite® at http://www.webcitation.org/6vsLwvguw)</li>

<li id="mil94">Miles, M.B. &amp; Huberman, A.M. (1994). <em>Qualitative data analysis: an expanded sourcebook.</em>(2. ed.) Thousand Oaks, CA: Sage</li>

<li id="pan09">Panciera, K., Halfaker, A. &amp; Terveen, L. (2009, May). Wikipedians are born, not made: a study of power editors on Wikipedia. In <em>Proceedings of the ACM 2009 international conference on supporting group work</em> (pp. 51-60)New York, NY: ACM.</li>

<li id="reh17">Rehm, Martin. Rienties, B. &amp; Littlejohn, A. (2017). Does a formal Wiki event contribute to the formation of a network of practice? a social capital perspective on the potential for informal learning. <em>Interactive Learning Environments</em> 1-12.</li>

<li id="sel10">Selwyn, N. (2010). Looking beyond learning: notes towards the critical study of educational technology. <em>Journal of Computer Assisted Learning, 26</em>(1), 65-73. </li>

<li id="sun11">Sundin, O. (2011). Janitors of knowledge: constructing knowledge in the everyday life of Wikipedia editors. <em>Journal of Documentation, 67</em>(5),  840-862.</li>

<li id="tka10">Tkacz, N. (2010). Wikipedia and the politics of mass collaboration. <em>Journal of Media and Communication, 2</em>(2), 41-53.</li>

<li id="vie04">Viégas, F., Wattenberg, M. &amp; Dave, K. (2004) Studying cooperation and conflict between authors with history flow visualizations. <em>Proceedings of the SIGCHI Conference on Human Factors in Computing Systems</em> (pp 575-582) New York, NY: ACM.</li> 

<li id="vie07">Viégas, F., Wattenberg, M., Kriss, J. &amp; van Ham, F. (2007). <a href="http://users.ece.utexas.edu/~perry/education/382v-s08/papers/viegas07.pdf">Talk before you type: Coordination in Wikipedia</a>, <em>Proceedings of the 40th Hawaii International Conference on System Sciences</em> (pp 1-10) Retrieved from  http://users.ece.utexas.edu/~perry/education/382v-s08/papers/viegas07.pdf (Archived by WebCite® at http://www.webcitation.org/6vsMkfZ0z)</li>

<li id="wat07">Wattenberg, M., Viégas, F. B. &amp; Hollenbach, K. (2007). Visualizing activity on wikipedia with chromograms. In Cécilia Baranauskas, Philippe Palanque, Julio Abascal &amp; Simone Diniz Junqueira Barbosa (Eds.), Human-Computer Interaction – INTERACT 2007: <em>Proceedings of the 11th IFIP TC13 International Conference on Human-Computer Interaction.</em> Berlin: Springer. (Lecture Notes in Computer Science, 4663)</li>

<li id="wik17">Wikipedia: list of Wikipedians by number of edits (2017). In <a href="https://en.wikipedia.org/wiki/Wikipedia:List_of_Wikipedians_by_number_of_edits"><em>Wikpedia: the free encyclopedia.</em></a> Retrieved from https://en.wikipedia.org/wiki/Wikipedia:List_of_Wikipedians_by_number_of_edits (Archived by WebCite® at http://www.webcitation.org/6vsN54TYF)</li>

<li id="wom17">Women, Science and Scottish History Editathon Series (2017).In <a href="https://wikimedia.org.uk/wiki/Women,_Science_and_Scottish_History_editathon_series"><em>Wikimedia UK: open knowledge for all.</em></a> Retrieved from https://wikimedia.org.uk/wiki/Women,_Science_and_Scottish_History_editathon_series (Archived by WebCite® at http://www.webcitation.org/6vsNLR9Lc)</li>

<li id="yan10">Yang, H. L. &amp; Lai, C. Y. (2010). Motivations of Wikipedia content contributors. <em>Computers in human behavior, 26</em>(6), 1377-1383.</li>
</ul>

</section>

</article>