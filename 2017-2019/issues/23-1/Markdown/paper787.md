<header>

#### vol. 23 no. 1, March, 2018

</header>

<article>

# Visual presentation and communication of Croatian academic Websites

## [Josipa Selthofer](#author)

> **Introduction**. The aim of the research is to analyse and compare visual identity elements of Croatian academic Websites with ones of European countries using Hofstede's model of cultural dimensions. The purpose of the research is to point to the influence a culture has on the design of Websites.  
> **Method**. Graphical elements of university home pages from Croatia and a sample of 10 European countries are compared using the visual content analysis method. Element frequency scores were correlated with indexes from Hofstede's Model of National Culture and interpreted on the basis of the existing literature using analysis of variance, independent T-test and chi-squared test.  
> **Analysis**. Both quantitative (using SPSS) and qualitative analyses (with the custom-created Web application) were carried out on the data (1,017 Websites).  
> **Results**. Many graphic elements of visual identity are common among the academic Web pages in Croatia and Europe. In a comparison of Web pages' cultural characteristics and G. Hofstede's model between Croatia and other countries, results show a statistically significant correlation through all the dimensions between the countries, depending on each visual graphic element.  
> **Conclusions**. Research results contribute to the body of theoretical and practical knowledge of visual communication. They also shed light on the influence of culture on visual communication in general and argue in favour of the fact that a visual communication, in order to be effective and informative, has to take into account underlying cultural and social premises that will influence creation and interpretation of Websites.

<section>

## Introduction

The visual identity of the academic institution in each country is culturally and socially determined. A country's culture can be defined with the objective - material creations and subjective psychosocial perception based on tradition and experience. ([McCort and Malhotra, 1993](#mcc93)) Quantity, positioning, organization and interpretation of the symbols, characters, colours, images and other visual graphic elements on the website reveal a lot about a culture. Cultural characteristics of the Websites are recognisable and characteristic for each country, but they differ between the countries.

Academic Websites are often first encountering points with a specific country, culture, and education system and/or academic institution. Therefore their design should take into account the culture of the country they are representing. University and college website design is the process of creating a meeting point for people from different culturalbackgrounds, with different inherited or learned preferences of s pecific and conditioned cultural elements rooted in historical, linguistic, national origins, as well as level of technological advancement. The term GILT (globalization, internationalization, localization, translation) is often used to describe intercultural Web design. Culture in internet globalisation represents the way that people from a certain culture see and interpret the meaning of the images and messages.

When developing the aim and purpose of this research, potential relations have been considered between information presented on the Web pages and the cultural context that underlines representation but also interpretation of information. Although this may not be immediately recognisable, this research is grounded in the core concept of information science as defined by [Saracevic (2009, p. 2570)](#sar09) as '_the science and practice dealing with the effective collection, storage, retrieval, and use of information… concerned with recordable information and knowledge, and the technologies and related services that facilitate their management and use_'. He adds that:

> more specifically, information science is a field of professional practice and scientific inquiry addressing the effective communication of information and information objects, particularly knowledge records, among humans in the context of social, organizational, and individual need for and use of information. (Saracevic, 2009, p. 2570)

It is this social and cultural background that is always present in any representation of information whether intentional or not, and that may influence how information is perceived and interpreted. This research focused on the effective visual communication and its cultural context.

The aim of this research is to analyse and compare visual identity elements of Croatian academic Websites with ones of European countries. This study examines differences and similarities in the visual presentation and communication of academic Websites using Hofstede's model of cultural dimensions with a purpose to identify and critically analyse Croatia's academic visual identity, and the influence of culture on its design.

This study addressed the following research questions:

1.  In what way is the visual information about Croatia as a culture and a country organised and presented in a virtual environment through the academic Websites?
2.  What are the similarities and differences in the elements of the visual identity of university and college Websites in Croatia and other countries, depending on the affiliation to the certain Hofstede dimension?

There are a few limitations that were taken into account when interpreting results and these refer to the fact that the study researched only academic Websites and analysed only graphical elements of Websites. Further on, Croatian visual elements are compared only with elements of the selected country from each of Hofstede dimension and an influence of the designers that made academic Websites in each country is unknown.

The following section will refer to theoretical background and previous research since they were used extensively in the first phase of research to define categories and properties for the content analysis.

</section>

<section>

## Theoretical background

### Applicability of Hofstede's model of national culture to communication of Web pages' visual identity

There are several models on the intercultural diversity of visual communication that can be applied in a virtual environment. The model of cultural dimensions (N-factor) measures the different cultures on the basis of the number of variables or factors: low versus high-speed flow of messages, a little content vs. much content, physical distance and environment for performing multiple tasks or one task (multitasking and single-tasking). A pioneer of this model is Edward Hall ([Hall 1990](#hal90)).

According to [Hoft (1996)](#hof96), Trompenaars has introduced a seven-factor model, grouped into three types: problems in relationships with other people, problems related to time and problems related to the environment. [Khaslavsky (1998)](#kha98) suggested a combined model with nine factors including the rate and context of messages, time, power distance, collectivism vs. individualism, anxiety, specificity vs. diffusivity, universality vs. particularity.

Hofstede's model of national culture is often used in human-computer interaction and in identifying differences of visual elements on websites (see, for example, [Barber and Badre 2001](#bar01); [Karande, Almurshidee and Al-Olayan 2006](#kar06)). The interpretation of the meaning of symbols and signs, structure of information, use of graphic design elements (such as typography, colours, shapes, images) can be associated with Hofstede's dimensions ([Marcus and Gould 2000](#margo00); [Cyr and Haizley 2004](#cyr04); [Callahan 2006](#cal06); [Gevorgyan and Porter 2008](#gev08)). In countries with a high power-distance index, information on the website is structured and national symbols are present. The use of image elements that are accentuated by the success, progress, change and the presence of personal information can indicate the high level of individualism in a culture ([Marcus 2000](#mar00)). The Hofstede dimension of masculinity is often related to the organization and efficiency of the elements on the website - navigation (which is oriented toward greater control) and interactive elements (games, animation) - while femininity is often associated with the care for others, environment, quality of life, cooperation, exchange of information and aesthetically pleasing websites ([Marcus and Kitayama 1991](#mar91); [Sen, Lindgaard and Patrick 2007](#sen07)).

Masculine cultures, such as Japan and Italy, place values on challenge, advancement, social recognition, and acquisition of wealth, while feminine cultures (e.g., Norway, Sweden) value quality of life, security, taking care of others, and the environment. Uncertainty avoidance measures the degree to which people tend to stay away from uncertain situations. It is often mistakenly interpreted as risk avoidance, but actually avoiding uncertain situations could be accomplished by setting strict rules, or by actually taking risks to prevent long-term uncertainty. High uncertainty-avoidance countries (e.g., Greece, Portugal) prefer formal rules and regulations in order to reduce the amount of uncertainty, and are less inclined toward change than countries with a low uncertainty avoidance index (e.g., Jamaica, Singapore).

Long-term orientation stands for the fostering of virtues oriented towards future rewards - in particular, perseverance and thrift. Its opposite pole, short-term orientation, stands for the fostering of virtues related to the past and present, in particular, respect for tradition, preservation of 'face' and fulfilling social obligations ([Hofstede, 2001](#hof01)).

Hofstede's research was conducted in international subsidiaries of IBM between 1967 and 1973, and was based on a survey,the results of which were entered into an IBM database. Although the IBM survey was conducted in organizational contexts, Hofstede's model of culture is often used in cross-cultural research to interpret a large variety of research findings. According to the Social Science Citation index, Hofstede's book _Culture's consequences_ (1980) and _Cultures and organizations_ (1991) have together been cited over 3,500 times since their first publication and are used in a variety of disciplines: cross-cultural and organizational psychology, sociology, management, and communication ([Callahan, 2006](#cal06)).

From further research ([Hofstede, Hofstede and Minkov, 2010](#hof10)), two new dimensions were derived on the basis of the data from the World Values Survey on a representative sample of the national populations. One dimension was completely new and the other one was a perfected fifth Hofstede dimension that allowed a greater sample of countries (93) to be measured and to be named pragmatism versus normativism. This dimension describes how people from a certain country experience their culture, past and present. The second, new dimension is named Indulgence versus Restraint. Indulgence stands for a society that allows relatively free gratification of basic and natural human drives related to enjoying life and having fun. Restraint stands for a society that suppresses gratification of needs and regulates it by means of strict social norms ([Hofstede Insights, 2018](#hof18)).

Hofstede's model of national culture is often used in human-computer interaction and in identifying differences of visual elements on Websites. The interpretation of the meaning of symbols and signs, structure of information, use of graphic design elements (such as typography, colours, shapes, images) can be associated with Hofstede's dimensions. In countries with a high power-distance index, information on the website is structured and national symbols are present. The use of image elements that are accentuated by the success, progress, change and the presence of personal information can indicate the high level of individualism in a culture. The Hofstede dimension of masculinity is often related to the organization and efficiency of the elements on the website - navigation (which is oriented toward greater control) and interactive elements (games, animation) - while femininity is often associated with the care for others, environment, quality of life, cooperation, exchange of information and aesthetically pleasing Websites.

The model of cultural markers determines those visual signs or symbols on the website that are typical for a particular culture.These cultural markers on the site usually consist of colours, advertisements, metaphors, navigation and other elements that are expected and desirable in a certain culture ([Smith, Dunckley, French, Minocha and Chang, 2004](#smi04)).

Marcus ([2000](#mar00)) lists five components of website interface design: metaphor, mental model, navigation, interaction, and appearance. Metaphor means the simplification of complex information, mental model refers to individual differences in the perception of the individual seeing, navigation marks the movement conditioned upon the mental model, interaction is related to all means of communication between humans and computers, and appearance is related to visual, sensory or tactile stimuli created by the presented content. Marcus combined Hofstede's cultural dimensions model with the elements of the interface design and created a matrix of twenty-five points of interest.

### Previous research

Previous research in the field of cultural differences on design between countries using Hofstede's model of national cultures was oriented towards marketing research of advertisements or on particular visual elements of Websites: the meaning of colours, symbols and characters for each national culture ([Barber, Badre, 2001](#bar01)). For example, Cutler, Javalgi and Rajshekhar ([1992](#cut92)) explored the diversity in advertisement design (size, colour, quantity of image elements) between Europe and the United States. Karande, Almurshidee and Al-Olayan ([2006](#kar06)) proposed a standardisation framework in advertising, based on cultural and socio-economic similarities between the Arab countries. The study was conducted on a sample of 949 advertisements from Egypt, Lebanon and the United Arab Emirates. The research results showed that advertising, when used in culturally similar markets, is uniform. Research on cultural markers on Korean and English Websites has shown how graphic elements and aesthetic principles can differ between different cultures ([Juric, Kim and Kuljis, 2003](#jur03)).

Many authors in their research on cultural diversity in the design of Websites have used Hofstede's model. For example, Fletcher ([2006](#fle06)) points out the need for cultural sensitivity in designing visual elements on a website for the purpose of communication. He concluded that effective communication on Websites between different ethnic groups requires knowledge of their cultural characteristics. Visual content design largely depends on the type and use of the website, but also on the target user groups. To reach out to different ethnic groups, design elements - such as the language on the site, symbolism and characters - must be considered (e.g., number 13 has a negative connotation in many countries).

Kim and Kulis ([2010](#kim10)) compared the Websites of charities in South Korea and the United Kingdom. They conclude that there are elements of cultural differences visible from the graphic design. Results from their survey were compared with Hofstede's dimensions and it was concluded that the colour white is dominant in both countries as it refers to common features of style in graphic design Websites globally. Research has shown that Websites of South Korea's charities used more multimedia elements, such as sound, video and animation. South Korea is a highly collectivistic country, fostering social values and group decision-making. On the Websites from the research sample in South Korea, online groups are present to a greater extent than in the UK as an individualistic society.

In their research conducted on academic Websites Marcus and Gould ([2000](#margo00)) point out that the first Hofstede dimension (power- distance) is closely connected with the organisation, structure and hierarchy of the website; a different distribution of power in society affects the different emphasis on moral and religious symbols, which directly influence the credibility and effectiveness of the visual message. Marcus and Gould connect the second and third dimension (individualism/collectivism and femininity/masculinity) with themes, type and amount of image elements on the Web page, and the fourth dimension (uncertainty avoidance) with the meaning of metaphors and symbols on the website. The fifth dimension (long- vs. short-term orientation) is highly correlated with the mission, vision and philosophy of the entire website (i.e., a background institution). The number of links and multimedia varies depending on the particular culture. When a designer is from a culture that values material goods, there is greater use of animations and graphics on the website. This is in close connection with Hofstede's dimension of masculinity where the cultures in which characteristics of masculinity are desirable e.g., Japan, Germany, USA.

Dormann and Chisalita ([2002](#dor02)) distinguish 'visual-based' and 'index-based' Websites. In 'visual-based' Websites the emphasis is on graphic design, and in 'index-based' ones the emphasis is on the number of links and search options on the website. In their study, Websites are determined by the culture and they use Hofstede's model for the analysis and interpretation of the research results. In their research of academic Web sites, they linked the masculinity dimension with emphasis on tradition and authority and frequent images of buildings. The opposite feminine dimension is, according to authors, linked with frequent images of people, especially showing them laughing, talking or studying together.

Ackerman ([2002](#ack02)) in his research concludes that high uncertainty avoidance dimension is connected with references to daily life and redundancy of material on academic Web sites.

Callahan ([2006](#cal06)) explores the cultural differences between the layouts of academic Websites in different countries of the world. She managed to connect visual content analysis with Hofstede's dimensions: power distance with the theme, amount and type of image content on the website, and the individualism/collectivism dimension with the symmetry on the Websites.

Sen, Lindgaard and Patrick ([2007](#sen07)) in their study of cultural differences of symbolic elements on academic Websites concluded that individuals from different cultures perceive them in different ways, depending on their own origin. Comparing the results obtained with Hofstede's dimensions, the authors stated:

> 1.  Power Distance indicates the uniformity of the distribution of power in society (culture). Countries with highdefinition obey authority figures (teachers, parents, bosses) and the superiority of the leader is also reflected in the credibility of the message on the website of the university, as well as in their effectiveness.
> 2.  Hofstede's dimension of Individualism is linked to personal freedom, concern about the individual's rights, freedom of thought, competitive spirit, self-realization, self-actualization. In collective cultures, the emphasis is on group harmony, the ideology of group dialogue over the rights of the individual.
> 3.  Linked to the dimension of Uncertainty Avoidance, the authors wonder whether increased /decreased risk strengthens or weakens the credibility of the message.

Other conclusions of their research were:

> 1.  For students from Western countries most important was the feeling that arises from the overall impression generated by looking at the website of the desired university or college. For students who are coming from Eastern countries, most important was to discover from the Web page aesthetics the quality of education that the institution provides.
> 2.  The symbolic effect of the university's building image on the website is different for students from different cultures. In Eastern cultures the image of the institution's buildings is appreciated; the age of the building indicates the history of the institution, older and more experienced teachers, and thus the quality of education.
> 3.  Images of people from the management structures on the Web pages are interpreted as fraud by students from Western countries - creating the false impression that they can rely on them leads to disbelief. On the contrary, in Eastern cultures where people prize authority, the image of such leaders is perceived as 'sacred' because - the president never lies. ([Sen, Lindgaard and Patrick 2007, 65-71](#sen07))

Marcus and Kitayama's ([1991](#mar91)) research highlights how people in collective cultures are defined through group membership and the group is deemed to be homogeneous. Marcus ([2000](#mar00)) indicates that the dimension of power distance stands in close connection with the emphasis of the moral and religious symbols on the website. Kim, Lee and Choi ([2003](#kim03)) researched the connection between the aesthetics of the Web pages and user satisfaction; they observed the navigation and organisation of the visual graphic elements on Web pages in different cultures. Gevorgyan and Porter ([2008](#gev08)) had explored the impact of cultural background on priorities in website design. The research samples were composed from Chinese students, and US students. The results were compared with Hofstede's dimensions of power distance and uncertainty avoidance, and the authors concluded that Chinese students prefer website elements that emphasize the power-distance dimension. US students give preference to the dimension of uncertainty avoidance associated with risk.

Cyr and Haizley ([2004](#cyr04)) applied Hofstede's model in their study of intercultural website visual graphic elements of the government organisations from Japan, Germany and the United States. They concluded that Websites from the countries from the sample differ in advertisement positioning and search engines, by typographic features, as well as highlighted elements on the page. The use of national symbols and signs is present on German and Japanese Websites. All Websites from the sample use passive graphic elements in the form of maps and signposts.

### Methods

The research was conducted in three phases in 2014\. The first phase was defining categories and properties for the analysis on the basis of theoretical background and the previous research; the second phase was an analysis of these properties on a research sample; and the third phase was a comparison with the Hofstede's dimensions. Methods used were visual content analysis, Hofstede's model of national culture and checks on hypotheses: chi-squared test, ANOVA (analysis of variance) and T-test.

Categories and properties of visual elements on the website are created according to previous studies conducted in the area of cultural diversity research. Table 1 presents website characteristics related to Hofstede's dimensions of culture based on previous research from the field.

The following visual identity elements were analysed:

*   Trademark (symbol, emblem, typography)
*   Colour (front of the page and the background)
*   The organisation of elements on the website (different Web architectures and composition; hierarchy, symmetry…)
*   The number of links on the website - to other academic institutions and to social networks
*   Photography (topic - figurative, abstract images, number)
*   Multimedia - presence of animated, video or graphic elements on the website

The method of visual content analysis ([Bell, 2002](#bel02)) is used most frequently in the research of mass media and message in the communication process. It is a systematic research method that allows inductive and deductive inference and the testing of hypotheses based on observations of visual elements through various measurable categories according to certain theoretical assumptions ([Rose, 2012](#ros12)).

<table><caption>

Table 1: Web site characteristics in relation to Hofstede's dimensions of culture ([Callahan, 2006, p. 248](#cal06))</caption>

<thead>

<tr>

<th>Dimension</th>

<th>High</th>

<th>Low</th>

<th>Source</th>

</tr>

</thead>

<tbody>

<tr>

<td>Power distance</td>

<td>

- symmetry  
- tall hierarchies  
- focus on official seal, national emblems  
- photographs of leaders  
- monumental buildings  
- monuments</td>

<td>

- asymmetry  
- shallow hierarchies  
- photos of students rather than faculty  
- images of both sexes  
- mages of public spaces  
- images of everyday activities  
</td>

<td>

[Marcus and Gould (2000)](#margo00)  
[Ackerman (2002)](#ack02)</td>

</tr>

<tr>

<td>Individualism</td>

<td>

- images of individuals  
- images of young  
- emphasis on action</td>

<td>

- images of groups  
- images of aged and experienced  
- emphasis on state of being</td>

<td>

[Marcus and Gould (2000)](#margo00)</td>

</tr>

<tr>

<td>Masculinity</td>

<td>

- limited choices  
- orientation toward goals  
- emphasis on tradition and authority  
- frequent images of buildings  
- graphics used for utilitarian purposes</td>

<td>

- multiple choices  
- orientation toward relationships  
- frequent images of people, especially showing them laughing, talking or studying together  
- attention gained by visual aesthetics</td>

<td>

[Ackerman (2002)](#ack02)  
[Dormann and Chisalita (2002)](#dor02)  
[Marcus and Gould (2000)](#margo00)</td>

</tr>

<tr>

<td>Uncertainty avoidance</td>

<td>

- limited choices  
- restricted amounts of data  
- limited scrolling  
- references to daily life  
- redundancy</td>

<td>

- variety of choices  
- long pages with scrolling  
- abstract images</td>

<td>

[Marcus and Gould (2000)](#margo00)  
[Ackerman (2002)](#ack02)</td>

</tr>

</tbody>

</table>

### Research sample

Data for the study were collected from all Croatian public university Websites as well as from a comparative sample of ten European countries. University Websites from the sample of other European countries were chosen according to the affiliation of a country to certain Hofstede dimensions; those with the highest/lowest index in each dimension (Table 2). The sample of chosen countries is also geographically, culturally and politically relevant for comparison with similar Croatian university Websites (Austria, Germany, France, Hungary, Romania, Latvia, Spain, Sweden, Ireland, Greece).

European countries were chosen for comparison on the basis of following criteria:

*   Geographical location within Europe
*   European countries in which Croatian students mostly go ([Institut za razvoj obrazovanja, 2013](#ins13))
*   European countries whose students mostly come to Croatia ([Institut za razvoj obrazovanja, 2013](#ins13))
*   European countries that, according to Hofstede's dimensions, have emphasized diversity

<table><caption>

Table 2: Research sample and Hofstede index by each dimension (indexes from [https://geert-hofstede.com/](https://geert-hofstede.com/))</caption>

<thead>

<tr>

<th>Country</th>

<th>Power  
distance</th>

<th>Individualism/  
collectivism</th>

<th>Masculinity/  
Femininity</th>

<th>Uncertainty  
avoidance</th>

<th>Long-term  
vs. short-term  
orientation</th>

<th>Pragmatism</th>

<th>Indulgence</th>

</tr>

</thead>

<tbody>

<tr>

<td>Austria</td>

<td>11</td>

<td>55</td>

<td>79</td>

<td>70</td>

<td>31</td>

<td>60</td>

<td>63</td>

</tr>

<tr>

<td>Croatia</td>

<td>73</td>

<td>33</td>

<td>40</td>

<td>80</td>

<td>X</td>

<td>58</td>

<td>33</td>

</tr>

<tr>

<td>France</td>

<td>68</td>

<td>71</td>

<td>43</td>

<td>86</td>

<td>39</td>

<td>63</td>

<td>48</td>

</tr>

<tr>

<td>Germany</td>

<td>35</td>

<td>67</td>

<td>66</td>

<td>65</td>

<td>31</td>

<td>83</td>

<td>40</td>

</tr>

<tr>

<td>Greece</td>

<td>60</td>

<td>35</td>

<td>57</td>

<td>112</td>

<td>X</td>

<td>45</td>

<td>50</td>

</tr>

<tr>

<td>Hungary</td>

<td>46</td>

<td>80</td>

<td>88</td>

<td>82</td>

<td>50</td>

<td>58</td>

<td>31</td>

</tr>

<tr>

<td>Ireland</td>

<td>28</td>

<td>70</td>

<td>68</td>

<td>35</td>

<td>43</td>

<td>24</td>

<td>65</td>

</tr>

<tr>

<td>Latvia</td>

<td>44</td>

<td>70</td>

<td>9</td>

<td>63</td>

<td>25</td>

<td>69</td>

<td>13</td>

</tr>

<tr>

<td>Romania</td>

<td>90</td>

<td>30</td>

<td>42</td>

<td>90</td>

<td>X</td>

<td>52</td>

<td>20</td>

</tr>

<tr>

<td>Spain</td>

<td>57</td>

<td>51</td>

<td>42</td>

<td>86</td>

<td>19</td>

<td>48</td>

<td>44</td>

</tr>

<tr>

<td>Sweden</td>

<td>31</td>

<td>71</td>

<td>5</td>

<td>29</td>

<td>20</td>

<td>53</td>

<td>78</td>

</tr>

</tbody>

</table>

The research sample of 1,017 Websites of the public universities and associated departments in eleven countries was analysed in the native language of the country or, in the event that there is no page in the native language, in English (Table 3).

<table><caption>Table 3: Number of Websites for analysis from the research sample.</caption>

<thead>

<tr>

<th>Initial website</th>

<th>Austria</th>

<th>Croatia</th>

<th>France</th>

<th>Germany</th>

<th>Greece</th>

<th>Hungary</th>

<th>Ireland</th>

<th>Latvia</th>

<th>Romani</th>

<th>Spain</th>

<th>Sweden</th>

</tr>

</thead>

<tbody>

<tr>

<td>Number of universities</td>

<td>9</td>

<td>7</td>

<td>27</td>

<td>17</td>

<td>8</td>

<td>6</td>

<td>9</td>

<td>3</td>

<td>8</td>

<td>18</td>

<td>20</td>

</tr>

<tr>

<td>Number of components</td>

<td>46</td>

<td>121</td>

<td>186</td>

<td>143</td>

<td>83</td>

<td>59</td>

<td>57</td>

<td>20</td>

<td>42</td>

<td>60</td>

<td>69</td>

</tr>

<tr>

<td>Total</td>

<td>55</td>

<td>128</td>

<td>213</td>

<td>160</td>

<td>91</td>

<td>65</td>

<td>66</td>

<td>23</td>

<td>50</td>

<td>78</td>

<td>89</td>

</tr>

</tbody>

</table>

### Data collection

Data collection for qualitative analysis in visual communication studies is very complex and time consuming. Specifically, researchers need a single interface for qualitative visual analysis and quantitative data collection of certain variables. Although there are many software tools and solutions designed for the analysis of large amounts of data by helping to organise documents on topics of interest and placing them in their larger context, at the time of conducting this study there were no tools available for visual content analysis of Websites.

<figure>

![Figure 1: Web application interface](../p787fig1.png)

<figcaption>Figure 1: Web application interface.</figcaption>

</figure>

Since this specific visual communication research project consisted of analysing and validating visual elements on a large number of Web pages (1,017), it was difficult to conduct research manually. In this specific visual research, the most important thing for the researcher was to have an application that is organised in a way that allows the researcher a full visual control of a Web page s/he is observing and the ability to mark and save their observations directly on screen. Web application for the specific research project (Image 1) was built using agile software development method on LAMP stack and is available online. It offers three main sections: list of Websites to evaluate, visual representation of loaded website and list of attributes grouped by categories for quantifying data. Proposed customised IT tool allows data export to widely accepted Microsoft Excel format for further data analysis ([Selthofer and Jakopec 2014](#sel14)).

### Visual content analysis through categories and properties

The analysis included five categories: website, colour, website organisation, image and visual identity of institution (see Appendix 1), each consisting of properties. When visual content analysis was conducted in Web application, results were further analysed and compared in SPSS statistical software. Results were compared between the countries and according to affiliation to Hofstede's dimensions. Furthermore, results on properties for Croatian sample were compared with other countries from the research sample and according to affiliation with Hofstede's dimensions.

</section>

<section>

## Research results

### Visual presentation of the Croatian Websites

In Croatia a total of seven universities and its 121 components (Faculties and Departments) were analysed. Research was conducted on Websites in the native language of the country or, in the event that there was no page in the native language, in English. Other than in Croatian language, academic Websites in Croatia are in English (N=75, 58.6%), two of them in Italian and one of them in German and Chinese.

The predominant foreground colour of website is white (N=72, 56.2%), except of sites in University of Zadar where it is blue. The predominant background colour of website is grey (N=56, 43.8%), except at the University of Pula where it is black.

Organisation of the visual content on the Websites from the sample in Croatia was mainly symmetrical (N=83, 64.8%). University of Pula has no symmetrical Web pages. The prevalent type of composition on Websites is 1_2_1 (N=66, 51.6%).

The largest number of Websites use simple menus (N=93, 72.7%), except of University of Osijek that have complex ones, and have a vertical orientation (N=104, 81.3%).

Regarding the number of images in Croatia (total of 697), most are at the University of Zagreb Websites. The modality of images is mainly high (N=85, 66.4%). Medium modality is present on 31.6% (N=40) and low on 2.3% (N=3).

The types of images that are prevalent on Websites are photographs presenting mainly figurative images of people (52.3%). Images of buildings are present on 64.8% of Websites, artefacts on 25.8% of webpages and nature on 13.3% of Websites in Croatia.

Figurative images of a people are mainly mixed sexes (69.6%), then of men (13%) and women (8.7%). Images of students are present on 31.9% Websites, students and professors on 17.4% Websites. The social distance of the people on the images is mainly public (46.4%), then social (24.6%), personal (23.2%) and intimate (5.8%).

The number of links to other institutions or to social networks and images on the Websites is 1134; the highest number is in University of Zagreb (288) and lowest in University of Pula (23).

The highest numbers of links to the social networks are present in University of Dubrovnik (8) - Facebook, Twitter and YouTube. On the University of Pula Websites there are no links to social networks. Facebook is mostly present on Croatian universities Websites (27), then Twitter (18), YouTube (12), Google and LinkedIn (2), and, finally, Flickr (1).

The visual identity of academic institution is present on 93 or 72.7% of Websites as coat of arms. Symbol, as part of visual identity, is present on 56 or 43.8% of Websites. A visual identity manual (i.e., a handbook providing guidelines for using visual identity elements) is present on only 4 or 3.1% of Websites.

From the research results, 78 or 60.9% Websites have some sort of animation, video or graphic material.

### Comparison of visual identity characteristics of Croatian and European universities Websites and Hofstede's dimensions

The visual characteristics of Croatian universities Websites were then compared with those of other countries from the research sample that had a significant difference in each of Hofstede's dimensions (Table 4).

<table><caption>Table 4: Hofstede dimensions and values for Croatia and other countries from the research sample that had the most significant difference in each dimension.</caption>

<tbody>

<tr>

<th>Dimension</th>

<th>Croatia</th>

<th>Austria</th>

<th>Romania</th>

</tr>

<tr>

<td>Power distance</td>

<td>73</td>

<td>11</td>

<td>90</td>

</tr>

<tr>

<td></td>

<th>Croatia</th>

<th>France</th>

<th>Greece</th>

</tr>

<tr>

<td>Individualism/collectivism</td>

<td>33</td>

<td>71</td>

<td>35</td>

</tr>

<tr>

<td></td>

<th>Croatia</th>

<th>Hungary</th>

<th>Sweden</th>

</tr>

<tr>

<td>Masculinity/femininity</td>

<td>40</td>

<td>88</td>

<td>5</td>

</tr>

<tr>

<td></td>

<th>Croatia</th>

<th>Ireland</th>

<th>Greece</th>

</tr>

<tr>

<td>Short-term / long-term orientation</td>

<td>80</td>

<td>35</td>

<td>112</td>

</tr>

<tr>

<td></td>

<th>Croatia</th>

<th>Ireland</th>

<th>Germany</th>

</tr>

<tr>

<td>Pragmatism/normativism</td>

<td>58</td>

<td>24</td>

<td>83</td>

</tr>

<tr>

<td></td>

<th>Croatia</th>

<th>Sweden</th>

<th>Latvia</th>

</tr>

<tr>

<td>Indulgence/restraint</td>

<td>33</td>

<td>78</td>

<td>13</td>

</tr>

</tbody>

</table>

### Croatia and the power distance dimension

Croatia is high a power distance country (73) and the Pearson Chi-Squared test has shown a significant difference between Croatia and Austria (which has a low Power distance - 11) in animation or graphic elements on the Websites (Table 5) and in social distance on figurative images of person (Table 24) with p=0.000.

<table><caption>Table 5: Social distance of persons on figurative images and animation/graphic elements on Websites in Croatia and Austria.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th rowspan="2">Presence of animation  
or graphics</th>

<th colspan="4">Figurative images of person - social distance</th>

</tr>

<tr>

<th>Intimate</th>

<th>Personal</th>

<th>Social</th>

<th>Public</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Austria</td>

<td>14</td>

<td>5</td>

<td>17</td>

<td>3</td>

<td>1</td>

</tr>

<tr>

<td>26.40%</td>

<td>19.20%</td>

<td>65.40%</td>

<td>11.50%</td>

<td>3.80%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>78</td>

<td>4</td>

<td>16</td>

<td>17</td>

<td>32</td>

</tr>

<tr>

<td>60.90%</td>

<td>5.80%</td>

<td>23.20%</td>

<td>24.60%</td>

<td>46.40%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0</td>

<td>0</td>

<td colspan="3"> </td>

</tr>

</tbody>

</table>

Romania is extremely high power distance country (90). In comparison with Croatia, there is a significant difference in the number of coats of arms on Websites (Table 6).

<table><caption>Table 6: Presence of coats of arms on Websites in Croatia and Romania.</caption>

<thead>

<tr>

<th></th>

<th>Presence of  
coats of arms</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Croatia</td>

<td>93</td>

</tr>

<tr>

<td>72.70%</td>

</tr>

<tr>

<td rowspan="2">Romania</td>

<td>20</td>

</tr>

<tr>

<td>40.00%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0</td>

</tr>

</tbody>

</table>

### Croatia and individualism vs. collectivism dimension

Croatia is collectivistic society (33) and the Pearson Chi-Squared test shows a significant difference between Croatia and Greece, which has almost similar score on collectivism (35), on the number of images of persons and artefacts on Websites, in status and social distance of persons on figurative images and in presence of coats of arms and animation or graphic elements (Table 7 and 8).

<table><caption>Table 7: Figurative images of persons and artefacts, coats of arms and animation or graphic elements on Websites in Croatia and Greece.</caption>

<thead>

<tr>

<th></th>

<th>Figurative images of person</th>

<th>Figurative images of artefacts</th>

<th>Presence of coats of arms</th>

<th>Presence of animation or graphics</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Croatia</td>

<td>67</td>

<td>33</td>

<td>93</td>

<td>78</td>

</tr>

<tr>

<td>52.30%</td>

<td>25.80%</td>

<td>72.70%</td>

<td>60.90%</td>

</tr>

<tr>

<td rowspan="2">Greece</td>

<td>36</td>

<td>33</td>

<td>30</td>

<td>34</td>

</tr>

<tr>

<td>39.60%</td>

<td>36.30%</td>

<td>33.00%</td>

<td>37.40%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.062</td>

<td>0.096</td>

<td>0</td>

<td>0.001</td>

</tr>

</tbody>

</table>

<table><caption>Table 8: Status and social distance of persons on figurative images on Websites in Croatia and Greece.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th colspan="4">Figurative images of person - status</th>

<th colspan="4">Figurative images of person - social distance</th>

</tr>

<tr>

<th>Student</th>

<th>Faculty</th>

<th>Mixed</th>

<th>Unidentified </th>

<th>Intimate</th>

<th>Personal</th>

<th>Social</th>

<th>Public</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Croatia</td>

<td>22</td>

<td>1</td>

<td>12</td>

<td>34</td>

<td>4</td>

<td>16</td>

<td>17</td>

<td>32</td>

</tr>

<tr>

<td>31.90%</td>

<td>1.40%</td>

<td>17.40%</td>

<td>49.30%</td>

<td>5.80%</td>

<td>23.20%</td>

<td>24.60%</td>

<td>46.40%</td>

</tr>

<tr>

<td rowspan="2">Greece</td>

<td>18</td>

<td>2</td>

<td>8</td>

<td>9</td>

<td>5</td>

<td>16</td>

<td>13</td>

<td>3</td>

</tr>

<tr>

<td>48.60%</td>

<td>5.40%</td>

<td>21.60%</td>

<td>24.30%</td>

<td>13.50%</td>

<td>43.20%</td>

<td>35.10</td>

<td>8.10%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.07</td>

<td colspan="3"></td>

<td>0.001</td>

<td colspan="3"></td>

</tr>

</tbody>

</table>

France is a highly individual society (71) in comparison with Croatia, and statistically significant differences are noted in website symmetry, colour modality, presence of coats of arms: p=0.000 (Table 9) and figurative images of persons p=0.002 and their status: p=0.000 (Table 10).

<table><caption>Table 9: Symmetry, colour modality and presence of coats of arms on Websites in Croatia and France.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th rowspan="2">Symmetry</th>

<th colspan="3">Colour modality</th>

<th rowspan="2">Coats of arms</th>

</tr>

<tr>

<th>High</th>

<th>Medium</th>

<th>Low</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">France</td>

<td>59</td>

<td>2</td>

<td>99</td>

<td>100</td>

<td>8</td>

</tr>

<tr>

<td>29.40%</td>

<td>1.00%</td>

<td>49.30%</td>

<td>49.80%</td>

<td>4.00%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>83</td>

<td>3</td>

<td>40</td>

<td>85</td>

<td>93</td>

</tr>

<tr>

<td>64.80%</td>

<td>2.30%</td>

<td>31.30%</td>

<td>66.40%</td>

<td>72.70%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.000</td>

<td>0.000</td>

<td colspan="2"></td>

<td>0.000</td>

</tr>

</tbody>

</table>

<table><caption>Table 10: Figurative images of persons and their status on Websites in Croatia and France.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th rowspan="2">Figurative images of person</th>

<th colspan="4">Figurative images of person - status</th>

</tr>

<tr>

<th>Student</th>

<th>Faculty</th>

<th>Mixed</th>

<th>Unidentified </th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">France</td>

<td>139</td>

<td>78</td>

<td>7</td>

<td>25</td>

<td>29</td>

</tr>

<tr>

<td>69.20%</td>

<td>56.10%</td>

<td>5.00%</td>

<td>18.00%</td>

<td>20.90%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>67</td>

<td>22</td>

<td>1</td>

<td>12</td>

<td>34</td>

</tr>

<tr>

<td>52.30%</td>

<td>31.90%</td>

<td>1.40%</td>

<td>17.40%</td>

<td>49.30%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.002</td>

<td>0.000</td>

<td colspan="3"></td>

</tr>

</tbody>

</table>

### Croatia and the femininity vs. masculinity dimension

Croatia is relatively feministic society (40). In comparison with highly feministic Sweden (5), a statistically significant difference is noted on figurative images of persons and their sex (Table 11). In comparison with highly masculine Hungary (88), a statistically significant difference is noted on figurative images of persons and their social distance (Table 12).

<table><caption>Table 11: Figurative images of persons and their sex on Websites in Croatia and Sweden.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th rowspan="2">Figurative images of person</th>

<th colspan="4">Figurative images of person - sex</th>

</tr>

<tr>

<th>M</th>

<th>F</th>

<th>Mixed</th>

<th>Unidentified</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Sweden</td>

<td>52</td>

<td>4</td>

<td>11</td>

<td>71</td>

<td>2</td>

</tr>

<tr>

<td>78.70%</td>

<td>4.50%</td>

<td>12.50%</td>

<td>80.70%</td>

<td>2.30%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>67</td>

<td>9</td>

<td>6</td>

<td>48</td>

<td>6</td>

</tr>

<tr>

<td>52.30%</td>

<td>13.00%</td>

<td>8.70%</td>

<td>69.60%</td>

<td>8.70%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.000</td>

<td>0.054</td>

<td colspan="3"></td>

</tr>

</tbody>

</table>

<table><caption>Table 12: Figurative images of persons and their social distance on Websites in Croatia and Hungary.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th rowspan="2">Figurative images of person</th>

<th colspan="4">Figurative images of person - social distance</th>

</tr>

<tr>

<th>Intimate</th>

<th>Personal</th>

<th>Social</th>

<th>Public</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Hungary</td>

<td>52</td>

<td>1</td>

<td>28</td>

<td>15</td>

<td>8</td>

</tr>

<tr>

<td>78.70%</td>

<td>1.90%</td>

<td>53.80%</td>

<td>28.80%</td>

<td>15.40%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>67</td>

<td>4</td>

<td>16</td>

<td>17</td>

<td>32</td>

</tr>

<tr>

<td>52.30%</td>

<td>5.80%</td>

<td>23.20%</td>

<td>24.60%</td>

<td>46.40%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.000</td>

<td>0.001</td>

<td colspan="3"></td>

</tr>

</tbody>

</table>

### Croatia and the uncertainty avoidance dimension (short-term vs. long-term orientation)

Croatia is high uncertainty avoidance country (80). The Pearson Chi-Squared test shows a significant difference between Croatia and Ireland (35) in Web page orientation and composition: p=0.000, p=0.001 (Table 13).

<table><caption>Table 13: Webpage orientation and composition in Croatia and Ireland.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th colspan="3">Page orientation</th>

<th colspan="6">Page composition</th>

</tr>

<tr>

<th>Horizontal</th>

<th>Vertical and horizontal</th>

<th>Vertical</th>

<th>1_2_1</th>

<th>1_3</th>

<th>F</th>

<th>OF</th>

<th>Other</th>

<th>Z</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Ireland</td>

<td>0</td>

<td>1</td>

<td>104</td>

<td>27</td>

<td>12</td>

<td>20</td>

<td>18</td>

<td>19</td>

<td>9</td>

</tr>

<tr>

<td>0%</td>

<td>1%</td>

<td>99%</td>

<td>25.70%</td>

<td>11.40%</td>

<td>19%</td>

<td>17.10%</td>

<td>18.10%</td>

<td>8.60%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>2</td>

<td>19</td>

<td>107</td>

<td>66</td>

<td>4</td>

<td>18</td>

<td>11</td>

<td>22</td>

<td>7</td>

</tr>

<tr>

<td>1.60%</td>

<td>14.80%</td>

<td>83.60%</td>

<td>51.60%</td>

<td>3.10%</td>

<td>14.10%</td>

<td>8.60%</td>

<td>17.20%</td>

<td>5.50%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.000</td>

<td colspan="2"></td>

<td>0.001</td>

<td colspan="5"></td>

</tr>

<tr>

<td colspan="10">Note: for explanation of the composition types (1_2_1, etc.) see [Appendix 2](#app2)</td>

</tr>

</tbody>

</table>

### Croatia and the pragmatism vs normative dimension

Croatia is pragmatic society (58). In comparison with Germany (83), statistically significant difference is noted on figurative images of persons - social distance and status, p= 0.001 (Table 14), in symbols, coats of arms (p=0.00), page composition, p=0.001 and in number of links (Table 15).

<table><caption>Table 14: Figurative images of persons and their social distance and status in Croatia and Germany.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th colspan="4">Figurative images of person - social distance</th>

<th colspan="4">Figurative images of person - status</th>

</tr>

<tr>

<th>Intimate</th>

<th>Personal</th>

<th>Social</th>

<th>Public</th>

<th>Student</th>

<th>Faculty</th>

<th>Mixed</th>

<th>Unidentified </th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Croatia</td>

<td>4</td>

<td>16</td>

<td>17</td>

<td>32</td>

<td>22</td>

<td>1</td>

<td>12</td>

<td>34</td>

</tr>

<tr>

<td>5.80%</td>

<td>23.20%</td>

<td>24.60%</td>

<td>46.40%</td>

<td>31.90%</td>

<td>1.40%</td>

<td>17.40%</td>

<td>49.30%</td>

</tr>

<tr>

<td rowspan="2">Germany</td>

<td>12</td>

<td>41</td>

<td>22</td>

<td>17</td>

<td>36</td>

<td>8</td>

<td>29</td>

<td>19</td>

</tr>

<tr>

<td>13.00%</td>

<td>44.65%</td>

<td>23.90%</td>

<td>18.50%</td>

<td>39.10%</td>

<td>8.70%</td>

<td>31.50%</td>

<td>20.70%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.001</td>

<td colspan="3"></td>

<td>0.001</td>

<td colspan="3"></td>

</tr>

</tbody>

</table>

<table><caption>Table 15: Symbols, coats of arms, page composition and number of links on Websites in Croatia and Germany.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th rowspan="2">Symbol</th>

<th rowspan="2">Coats of arms</th>

<th colspan="6">Page composition</th>

<th colspan="2">Number of links</th>

</tr>

<tr>

<th>1_2_1</th>

<th>1_3</th>

<th>F</th>

<th>OF</th>

<th>Other</th>

<th>Z</th>

<th>Mean</th>

<th>Sig.</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Croatia</td>

<td>56</td>

<td>93</td>

<td>66</td>

<td>4</td>

<td>18</td>

<td>11</td>

<td>22</td>

<td>7</td>

<td rowspan="2">9.33</td>

<td rowspan="2">0.008</td>

</tr>

<tr>

<td>43.80%</td>

<td>72.70%</td>

<td>51.60%</td>

<td>3.10%</td>

<td>14.10%</td>

<td>8.60%</td>

<td>17.20%</td>

<td>5.50%</td>

</tr>

<tr>

<td rowspan="2">Germany</td>

<td>109</td>

<td>49</td>

<td>87</td>

<td>9</td>

<td>33</td>

<td>17</td>

<td>11</td>

<td>0</td>

<td rowspan="2" colspan="2">12.51</td>

</tr>

<tr>

<td>69.40%</td>

<td>31.20%</td>

<td>55.40%</td>

<td>5.70%</td>

<td>21%</td>

<td>10.80%</td>

<td>7%</td>

<td>0%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0</td>

<td>0</td>

<td>0.002</td>

<td colspan="7"></td>

</tr>

<tr>

<td colspan="10">

Note: for explanation of the composition types (1_2_1, etc.) see [Appendix 2](#app2)</td>

</tr>

</tbody>

</table>

<table><caption>Table 16: Types of figurative images and page symmetry on Websites in Croatia and Ireland.</caption>

<thead>

<tr>

<th></th>

<th>Figurative images of person</th>

<th>Figurative images of artefact</th>

<th>Symmetry</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Croatia</td>

<td>67</td>

<td>33</td>

<td>83</td>

</tr>

<tr>

<td>52.30%</td>

<td>25.80%</td>

<td>64.80%</td>

</tr>

<tr>

<td rowspan="2">Ireland</td>

<td>58</td>

<td>34</td>

<td>22</td>

</tr>

<tr>

<td>87.90%</td>

<td>51.50%</td>

<td>33.30%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

</tbody>

</table>

<table><caption>Table 17: Status and social distance on figurative images on Websites in Croatia and Ireland.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th colspan="4">Figurative images of person - social distance</th>

<th colspan="4">Figurative images of person - status</th>

</tr>

<tr>

<th>Intimate</th>

<th>Personal</th>

<th>Social</th>

<th>Public</th>

<th>Student</th>

<th>Faculty</th>

<th>Mixed</th>

<th>Unidentified</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Croatia</td>

<td>4</td>

<td>16</td>

<td>17</td>

<td>32</td>

<td>22</td>

<td>1</td>

<td>12</td>

<td>34</td>

</tr>

<tr>

<td>5.80%</td>

<td>23.20%</td>

<td>24.60%</td>

<td>46.40%</td>

<td>31.90%</td>

<td>1.40%</td>

<td>17.40%</td>

<td>49.30%</td>

</tr>

<tr>

<td rowspan="2">Ireland</td>

<td>11</td>

<td>31</td>

<td>11</td>

<td>4</td>

<td>30</td>

<td>1</td>

<td>19</td>

<td>7</td>

</tr>

<tr>

<td>19.30%</td>

<td>54.40%</td>

<td>19.30%</td>

<td>7.00%</td>

<td>53.60%</td>

<td>1.80%</td>

<td>33.30%</td>

<td>12.30%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0</td>

<td colspan="3"></td>

<td>0</td>

<td colspan="3"></td>

</tr>

</tbody>

</table>

In comparison with Ireland (24), a statistically significant difference is noted on types of figurative images and on page symmetry (Table 16), and on figurative images of persons - social distance and status (Table 17) with p=0.000.

### Croatia and the indulgence vs. restraint dimension

Croatia is a restraint country (33). Cultural Web characteristics were compared with high indulgence Sweden (78) and high restraint country Latvia (13). Statistically significant differences are noted in many characteristics in comparison with Sweden (Tables 18, 19, 20).

<table><caption>Table 18: Symmetry, coats of arms, visual identity manual and page composition in Croatia and Sweden.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th rowspan="2">Symmetry</th>

<th rowspan="2">Coats of arms</th>

<th rowspan="2">Visual identity manual</th>

<th colspan="6">Composition</th>

</tr>

<tr>

<th>1_2_1</th>

<th>1_3</th>

<th>F</th>

<th>OF</th>

<th>Other</th>

<th>Z</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Sweden</td>

<td>23</td>

<td>49</td>

<td>21</td>

<td>27</td>

<td>12</td>

<td>20</td>

<td>18</td>

<td>19</td>

<td>9</td>

</tr>

<tr>

<td>21.90%</td>

<td>46.70%</td>

<td>20.00%</td>

<td>25.70%</td>

<td>11.40%</td>

<td>19.00%</td>

<td>17.10%</td>

<td>18.10%</td>

<td>8.60%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>83</td>

<td>93</td>

<td>4</td>

<td>66</td>

<td>4</td>

<td>18</td>

<td>11</td>

<td>22</td>

<td>7</td>

</tr>

<tr>

<td>64.80%</td>

<td>72.70%</td>

<td>3.10%</td>

<td>51.60%</td>

<td>3.10%</td>

<td>14%</td>

<td>8.60%</td>

<td>17%</td>

<td>6%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.000</td>

<td>0.000</td>

<td>0.000</td>

<td>0.001</td>

<td colspan="5"></td>

</tr>

<tr>

<td colspan="10">

Note: for explanation of the composition types (1_2_1, etc.) see [Appendix 2](#app2)</td>

</tr>

</tbody>

</table>

<table><caption>Table 19: Types of figurative images in Croatia and Sweden.</caption>

<thead>

<tr>

<th></th>

<th>Figurative images of person</th>

<th>Figurative images of buildings</th>

<th>Figurative images of artefact</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Sweden</td>

<td>87</td>

<td>30</td>

<td>46</td>

</tr>

<tr>

<td>82.90%</td>

<td>28.60%</td>

<td>43.80%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>67</td>

<td>83</td>

<td>33</td>

</tr>

<tr>

<td>52.30%</td>

<td>64.80%</td>

<td>25.80%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.000</td>

<td>0.000</td>

<td>0.004</td>

</tr>

</tbody>

</table>

<table><caption>Table 20: Figurative images of person social distance and status in Croatia and Sweden.</caption>

<thead>

<tr>

<th rowspan="2"></th>

<th colspan="4">Figurative images of person - social distance</th>

<th colspan="4">Figurative images of person - status</th>

</tr>

<tr>

<th>Intimate</th>

<th>Personal</th>

<th>Social</th>

<th>Public</th>

<th>Student</th>

<th>Faculty</th>

<th>Mixed</th>

<th>Unidentified</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="2">Sweden</td>

<td>15</td>

<td>47</td>

<td>20</td>

<td>6</td>

<td>44</td>

<td>4</td>

<td>31</td>

<td>9</td>

</tr>

<tr>

<td>17.00%</td>

<td>53.40%</td>

<td>22.70%</td>

<td>6.80%</td>

<td>50.00%</td>

<td>4.50%</td>

<td>35.20%</td>

<td>10.20%</td>

</tr>

<tr>

<td rowspan="2">Croatia</td>

<td>4</td>

<td>16</td>

<td>17</td>

<td>32</td>

<td>22</td>

<td>1</td>

<td>12</td>

<td>34</td>

</tr>

<tr>

<td>5.80%</td>

<td>23.20%</td>

<td>24.60%</td>

<td>46.40%</td>

<td>31.90%</td>

<td>1.40%</td>

<td>17.40%</td>

<td>49.30%</td>

</tr>

<tr>

<td>Pearson Chi-Squared</td>

<td>0.000</td>

<td colspan="3"></td>

<td>0.000</td>

<td colspan="3"></td>

</tr>

</tbody>

</table>

</section>

<section>

## Discussion

Table 21 represents visual characteristics of academic Websites in Croatia grouped in each Hofstede dimension that research results have shown to have some statistically significant difference.

<table><caption>Table 21 Croatia compared to other countries from the research sample and visual characteristics of academic Websites (Croatia's orientation in each dimension is marked in yellow).</caption>

<tbody>

<tr>

<th>Croatia compared to High Power Distance country</th>

<th>Croatia compared to Low Power Distance country</th>

</tr>

<tr>

<td>

- Greater number of animation and/or graphic elements  

</td>

<td>

- Greater number of coat of arms  

</td>

</tr>

<tr>

<th>Croatia compared to an Individualistic country</th>

<th>Croatia compared to a Collectivistic country</th>

</tr>

<tr>

<td>

- More symmetrical Websites  
- Greater number of coat of arms  
- Fewer images on Websites  
- Fewer images of students and mixed - students and professors</td>

<td>

- Greater number of images of persons  
- Greater number of coat of arms  
- Greater number of animation/graphic elements  
- More images of unidentified persons on figurative images  
- Public social distance of persons on figurative images</td>

</tr>

<tr>

<th>Croatia compared to a Masculine country</th>

<th>Croatia compared to a Feminist country</th>

</tr>

<tr>

<td>

- Fewer figurative images of persons  
- Public social distance of persons on figurative images  

</td>

<td>

- Fewer figurative images of persons  

</td>

</tr>

<tr>

<th>Croatia compared to a Short-term Orientation country</th>

<th>Croatia compared to a Long-term Orientation country</th>

</tr>

<tr>

<td>

- 1_2_1 composition prevalent  

</td>

<td>

-</td>

</tr>

<tr>

<th>Croatia compared to a Pragmatic country</th>

<th>Croatia compared to a Normative country</th>

</tr>

<tr>

<td>

- Fewer mixed-sex images of persons on figurative images  
- Public social distance of persons on figurative images  
- Greater number of coat of arms  

</td>

<td>

- More symmetrical Websites  
- Greater number of images of persons  
- Fewer images of students and mixed - students and professors  
- Public social distance of persons on figurative images  

</td>

</tr>

<tr>

<th>Croatia compared to an Indulgence country</th>

<th>Croatia compared to a Restraint country</th>

</tr>

<tr>

<td>

- Greater number of coat of arms  
- More symmetrical Websites  
- 1_2_1 composition prevalent  
- Fewer corporate identity manuals  
- Fewer images of persons and artefact  
- Fewer images of students and mixed - students and professors  
- More images of buildings on Websites  
- Public social distance of persons on figurative images</td>

<td>

-</td>

</tr>

</tbody>

</table>

Dormann and Chisalita ([2002](#dor02)) linked the masculinity dimension with an emphasis on tradition and authority and frequent images of buildings. The opposite, feminine dimension is, according to the authors, linked with frequent images of persons, especially showing them laughing, talking or studying together. Croatia has fewer images of persons than masculine or feminist countries.

Callahan ([2006](#cal06)) linked the individualism vs. collectivism dimension with the symmetry on the Websites. Croatia has more symmetrical Web sites than individualistic country.

Sen, Lindgaard and Patrick ([2007](#sen07)) connected high power distance dimension with the frequent images of the institution's buildings on the web. Research results did not show a correlation in this dimension, but Croatia has a greater number of images of buildings than indulgence countries.

The fact that Croatian academic Web sites have lots of animation and graphic is, according to the research results, opposite to the previous findings of Marcus and Gould ([2000](#margo00)). The authors connected these elements with masculinity dimension. On the other hand, Croatian academic Web sites have greater number of coats of arms than low power distance countries, which is consistent with the authors' findings.

Most variation in the visual cultural characteristics of Web pages were found when comparing Croatian sites with those of normative and indulgence countries. Since there is no previous research done on these two dimensions, results from this study contribute to the body of knowledge for all future research in information science regarding the presentation and communication of information on academic Web sites.

</section>

<section>

## Conclusion

As Fletcher ([2006](#fle06)) in his research has concluded, an effective communication on Websites between different ethnic groups requires knowledge of their cultural characteristics. Research results from this study about visual characteristics of Croatian academic Websites can serve to better understand Croatian culture, but also shed light on the influence of culture on visual communication in general, whether that influence is recognised and intentionally used in the design process, or not.

The research results provided answers to the research questions. The analysis and comparison of visual graphic characteristics on Croatian academic Websites (Table 21) and those of other countries from the research sample has shown similarities and differences in almost every property and Hofstede dimension. However, there are few visual elements on Croatian Websites that could indicate affiliation with Croatian culture, and have some recognisable characteristics in relation to other countries of the sample. Compared with other countries from the research sample, Croatian culture is not recognisable enough or visible through the universities' Websites.

From the research data, common visual characteristics from the countries and affiliation to a certain Hofstede's dimension can be distinguished. The main characteristics of Croatian visual presentation on academic Websites are: more coats of arms, symmetrical Websites, 1_2_1 composition prevalent, only few identity manuals on Websites, few images of persons and artefacts or of students and professors, more images of buildings, and public social distance of persons on figurative images.

Croatia has, according to the research results, cultural characteristics similar to the countries from the sample belonging to the same Hofstede's dimensions, and partly similar to the countries from the sample belonging to the different dimensions.

Information on the Croatian academic Websites is influenced not only by cultural dimensions but also by lack of skills and understanding of knowledge organization and structuring on the Websites, which creates a negative image of the country. This argument is manifested in a large number of institutionally-led and strict hierarchical relationships of graphic elements on the Website, a number of coats of arms of Croatian academic institutions, symmetry, absence of colour, a small number of links to social networks, and a lack of logos and a graphic standards manual.

However, a large number of images and animated content and the status of students supports the fact that the institutions have recognised the importance of appropriate visual presentation and communication on Websites for users, mainly students. Although there are a large number of links to other countries on the Websites, there are mainly links to domestic institutions. All these facts are still only a weak argument to conclude that Croatian academic institutions use this new medium in the best possible manner for the purpose of visual communication on their Websites.

The analysis also includes research data for two Hofstede's dimensions, indulgence vs. restraint and normative vs. pragmatic, for which there are no studies regarding the cultural characteristics of the Websites.

On most Websites in countries from the sample there are no clear symbols of cultural affiliation to that particular country. But the identity of the university in those countries is more visible than in Croatia. Other university Websites, in addition, offer users a variety of activities that are not primarily related to information about studies but support promotion, visibility and competitiveness of the university and its constituents. There are e-bookstores, gift-shops, portals intended for the promotion of science in children and young people with a variety of interactive games and many other activities that are not covered by the research categories and properties, but their presence on the Websites is of great importance.

Since the study only analyses and compares graphical elements of academic Websites, research results are not enough of an argument to make conclusions about the Croatian culture, but could be starting point for further research in the field of information presentation and communication.

To conclude with a systematic reflection on the level of individual institutions, Croatia's universities could make better use of the advantages offered by the new medium for the purpose of visibility, recognition and promotion of science and higher education as the key factor in building a better society and prosperity of the country.

A concluding remark is related to the role and influence a culture has on the design of Web pages. Visual communication, in order to be effective and informative, has to take into account underlying cultural and social premises that will influence the creation and interpretation of Websites. Hofstede's model can be used as one possible option.

</section>

<section>

## <a id="author"></a>About the author

**Josipa Selthofer** is post-doctoral research and teaching assistant at the Department of Information Sciences, Faculty of Humanities and Social Sciences in Osijek, Croatia. She holds PhD in information sciences and a BA in graphic design. Her main points of interest are visual communication, history of publishing and graphic design, and contemporary trends in publishing. She can be contacted at [jselthofer@ffos.hr](mailto:jselthofer@ffos.hr).

</section>

<section>

## References

<ul>

<li id="ack02">Ackerman, S. K. (2002). <em><a href="http://www.webcitation.org/6gstHHgK1">Mapping user interface design to culture dimensions</a></em>. Paper presented at the 4th Annual International Workshop on Internationalization of Products and Systems &quot;Ahead of the Curve: Converting Global Knowledge into Global Success&quot;, Austin Texas. Retrieved from http://www.iwips.org/iwips2002/downloads/AMA_XCult_13Jul02.ppt (Archived by WebCite® at http://www.webcitation.org/6gstHHgK1)</li>

<li id="bar01">Barber, W. &amp; Badre, A. (2001). <em><a href="http://www.webcitation.org/6gsstSb2p">Culturability: the merging of culture and usability</a></em>. Paper presented at 4th Conference on Human Factors and the Web Culturability &quot;The Merging of Culture and Usability&quot;. Retrieved from http://zing.ncsl.nist.gov/hfweb/att4/proceedings/barber/ (Archived by WebCite® at  http://www.webcitation.org/6gsstSb2p)</li>

<li id="bel02">Bell, P. (2001; 2002). <em>Handbook of content analysis</em>. London: Sage Publications.</li>

<li id="cal06">Callahan, E. (2006). <a href="http://www.webcitation.org/6gsshvRQb">Cultural similarities and differences in the design of university Websites</a>. <em>Journal of Computer-Mediated Communication, 11</em>(1), 239-273. Retrieved from http://jcmc.indiana.edu/vol11/issue1/callahan.html (Archived by WebCite® at http://www.webcitation.org/6gsshvRQb) </li>

<li id="cut92">Cutler, D. B., Javalgi, G. R. &amp; Rajshekhar, M. K. (1992). The visual components of print advertising: a five-country cross-cultural analysis. <em>European Journal of Marketing, 26</em>(4), 7-20.</li>

<li id="cyr04">Cyr, D. &amp;, Haizley, T. S. (2004). Localization of Web design: an empirical comparison of German, Japanese and U.S. website characteristics. <em>Journal of the American Society for Information Science and Technology, 55</em>(13), 1199-1208.</li>

<li id="dor02">Dormann, C., Chisalita, C. (2002). <a href="http://www.eace.net/proceedings/ECCE%202002.pdf">Cultural values in Web site design</a>. In Pozzi, S., Rizzo, A., &amp; Wrigth, P. (Eds.), <em>Proceedings of the 11th European conference on cognitive ergonomics, ECCE-11-Cognition, culture and design, September 8-11, Catania, Italy</em> (pp. 63-70). Roma: Instituto di Scienze e Technologie della Cognizione. Retrieved from http://www.eace.net/proceedings/ECCE%202002.pdf [Unable to archive]</li>

<li id="fle06">Fletcher, R. (2006). The impact of culture on Web site content, design, and structure: an international and a multicultural perspective. <em>Journal of Communication Management, 10</em>(3), 259-273.</li>

<li id="gev08">Gevorgyan, G. &amp; Porter, V. L. (2008). One size does not fit all: culture and perceived importance of Web design features. <em>Journal of Website Promotion 3</em>(1/2), 25-38.</li>

<li id="hal90">Hall, E.T. &amp; Hall, M.R. (1990). <em>Understanding cultural differences, Germans, French and Americans</em>. Boston, MA: Intercultural Press Inc.</li>

<li id="hof18">Hofstede Insights. (2018). The 6 dimensions of national culture. Retrieved from https://www.hofstede-insights.com/models/national-culture/.</li>

<li id="hof01">Hofstede, G. (1980; 2001). <em>Culture's consequences: international differences in work-related values</em>. Newbury Park, CA: Sage Publications.</li>

<li id="hof97">Hofstede, G. (1997). <em>Cultures and organizations: software of the mind.</em> New York, NY, USA: McGraw-Hill. </li>

<li id="hof10">Hofstede, G., Hofstede, J. &amp; Minkov. M. (2010). <em>Cultures and organizations: software of the mind</em> (3rd. ed.). New York, NY, USA: McGraw-Hill. </li>

<li id="hof96">Hoft, N. L. (1996). Developing a cultural Model. In E. M. del Galdo &amp; J. Nielsen (Eds.), <em>International user interfaces</em> (pp. 41-71). New York, NY, USA: John Wiley &amp; Sons.</li>

<li id="ins13">Institut za razvoj obrazovanja. (2013). <a href="http://www.webcitation.org/6xWCqy2pd"><em>Znacajan porast mobinosti u visokom obrazovanju</em></a>.  [Significant increase in mobility in higher education]. Retrieved from http://www.iro.hr/hr/javne-politike-visokog-obrazovanja/kolumna/znacajan-porast-mobilnosti-u-visokom-obrazovanju/ (Archived by WebCite® at http://www.webcitation.org/6xWCqy2pd)</li>

<li id="jur03">Juric, R., Kim, I. &amp; Kuljis, J. (2003). <a href="http://www.webcitation.org/6grHmrauO">Cross cultural Web design: an experience of developing UK and Korean cultural markers</a>. In L. Budin, V. Luzar-Stiffler, Z. Bekic &amp; V. Hljuz Dobric (Eds.). <em>ITI 2003: Proceedings of the 25th International Conference on Information Technology Interfaces, Cavtat, Croatia</em> (pp. 309-313). Zagreb: SRCE University Computing Centre, University of Zagreb. Retrieved from http://www.westminster.ac.uk/westminsterresearch (Archived by WebCite® at http://www.webcitation.org/6grHmrauO) </li>

<li id="kar06">Karande, K., Almurshidee, A. K. &amp; Al-Olayan, F. (2006). Advertising standardisation in culturally similar markets: can we standardise all components? <em>International Journal of Advertising, 25</em>(4), 489-511.</li>

<li id="kha98">Khaslavsky, J. (1998). Integrating culture into interface design. In C. Karat, A. Lund, J. Coutaz &amp; J. Carat (Eds.). <em>Proceedings of the ACM CHI 98 Human Factors in Computing Systems Conference, Los Angels, CA., USA.</em>, (pp. 365-366). New York, NY: ACM Press.</li>

<li id="kim10">Kim, I. &amp; Kuljis, J. (2010). Manifestations of culture in website design. <em>Journal of Computing and InformationTechnology, 18</em>(2), 125-132.</li>

<li id="kim03">Kim, J., Lee, J. &amp; Choi, D. (2003). Designing emotionally evocative homepages. An empirical study of the quantitative relations between design factors and emotional dimensions.  <em>International Journal of Human-Computer Studies, 59</em>(6), 899-940.</li>

<li id="mcc93">McCort, D. J. &amp; Malhotra, N. K. (1993). Culture and consumer behavior: toward an understanding of cross-cultural consumer behavior in international marketing. <em>Journal of International Consumer Marketing, 6</em>(2), 91-127.</li>

<li id="mar00">Marcus, A. (2000). <a href="http://www.webcitation.org/6xWD1pEHI">Cultural dimensions and global Web user-interface design. What? So What? Now What?</a> In: <em>Proceedings of the Sixth Conference on Human Factors and the Web. Austin, TX. 19 June</em>. Retrieved from http://ideal-group.org/demonstrations/hfweb00.marcus.html (Archived by WebCite® at http://www.webcitation.org/6xWD1pEHI)</li>

<li id="margo00">Marcus, A. &amp; Gould, E. (2000). Crosscurrents: cultural dimensions and global Web user-interface design. <em>Interactions, 7</em>(4), 32-46.</li>

<li id="mar91">Markus, R. H. &amp; Kitayama, S. (1991). Culture and the self: implications for cognition, emotion, and motivation. <em>Psychological Review, 98</em>(2), 224-253.</li>

<li id="ros12">Rose, G. (2012). <em>Visual methodologies: an introduction to researching with visual materials</em>. London: Sage Publications.</li>

<li id="sar09">Saracevic, T. (2009). Information science. In M. J. Bates (Ed.)., <em>Encyclopedia of library and information sciences</em> (3rd ed.) (pp. 2570-2585). New York, NY: Taylor and Francis.</li>

<li id="sel14">Selthofer, J. &amp; Jakopec, T. (2014). <a href="http://www.webcitation.org/6xiUBYtef">How can customized IT system support qualitative methods in Website validation: application for visual content analysis.</a> In S. Faletar Tanacković and B. Bosančić (Eds.). <em>Proceedings of the 13th International Cconference Libraries in the Digital Age (LIDA), Zadar, 16-20 June</em> (pp. 157-162). Retrieved from http://ozk.unizd.hr/proceedings/index.php/lida/article/view/119/216  (Archived by WebCite® at http://www.webcitation.org/6xiUBYtef)</li>

<li id="sen07">Sen D., Lindgaard, G. &amp; Patrick, A. (2007). Symbolic aspects of university homepage design: what appeals to different cultures? <em>MMI-Interaktiv, 13</em>(8), 65-71. </li>

<li id="smi04">Smith, A., Dunckley, L., French, T., Minocha, S. &amp; Chang, Y. (2004). Process model for developing usable cross-cultural Websites. <em>Interacting with Computers, 16</em>(1), 63-91.</li>
</ul>

</section>

<section>

## Appendix 1: Categories and properties for the visual content analysis

<table>

<tbody>

<tr>

<th colspan="2">Website</th>

</tr>

<tr>

<td rowspan="2">Home page</td>

<td>University</td>

</tr>

<tr>

<td>Faculty/Department</td>

</tr>

<tr>

<td rowspan="6">Official language</td>

<td>English</td>

</tr>

<tr>

<td>German</td>

</tr>

<tr>

<td>French</td>

</tr>

<tr>

<td>Italian</td>

</tr>

<tr>

<td>Chinese</td>

</tr>

<tr>

<td>Another</td>

</tr>

<tr>

<td>Official languages – number  

</td>

<td>Numerical  

</td>

</tr>

<tr>

<th colspan="2">Colour</th>

</tr>

<tr>

<td rowspan="3">Modality</td>

<td>High</td>

</tr>

<tr>

<td>Medium</td>

</tr>

<tr>

<td>Low</td>

</tr>

<tr>

<td rowspan="9">Prevalent foreground colour</td>

<td>Yellow</td>

</tr>

<tr>

<td>Orange</td>

</tr>

<tr>

<td>Red</td>

</tr>

<tr>

<td>Green</td>

</tr>

<tr>

<td>Blue</td>

</tr>

<tr>

<td>Purple</td>

</tr>

<tr>

<td>White</td>

</tr>

<tr>

<td>Grey</td>

</tr>

<tr>

<td>Black</td>

</tr>

<tr>

<td rowspan="9">Prevalent background colour</td>

<td>Yellow</td>

</tr>

<tr>

<td>Orange</td>

</tr>

<tr>

<td>Red</td>

</tr>

<tr>

<td>Green</td>

</tr>

<tr>

<td>Blue</td>

</tr>

<tr>

<td>Purple</td>

</tr>

<tr>

<td>White</td>

</tr>

<tr>

<td>Grey</td>

</tr>

<tr>

<td>Black</td>

</tr>

<tr>

<th colspan="2">Organization of the website</th>

</tr>

<tr>

<td>Symmetrical</td>

<td>Yes/No</td>

</tr>

<tr>

<td rowspan="7">Composition</td>

<td>1_2_1</td>

</tr>

<tr>

<td>F</td>

</tr>

<tr>

<td>OF</td>

</tr>

<tr>

<td>MC</td>

</tr>

<tr>

<td>Z</td>

</tr>

<tr>

<td>1_3</td>

</tr>

<tr>

<td>Other</td>

</tr>

<tr>

<td rowspan="3">Page orientation</td>

<td>Vertical</td>

</tr>

<tr>

<td>Horizontal</td>

</tr>

<tr>

<td>Vertical and horizontal</td>

</tr>

<tr>

<td rowspan="2">Menus</td>

<td>Simple</td>

</tr>

<tr>

<td>Complex</td>

</tr>

<tr>

<td>Number of links</td>

<td>Numeric value</td>

</tr>

<tr>

<td>Social network  

</td>

<td>Descriptive  

</td>

</tr>

<tr>

<th colspan="2">Images</th>

</tr>

<tr>

<td>Animation or graphics</td>

<td>Yes/No</td>

</tr>

<tr>

<td>Figurative images – person</td>

<td>Yes/No</td>

</tr>

<tr>

<td>Figurative images – building</td>

<td>Yes/No</td>

</tr>

<tr>

<td>Figurative images – artefacts</td>

<td>Yes/No</td>

</tr>

<tr>

<td>Figurative images – nature</td>

<td>Yes/No</td>

</tr>

<tr>

<td rowspan="4">Figurative images – status</td>

<td>Student</td>

</tr>

<tr>

<td>Faculty</td>

</tr>

<tr>

<td>Mixed</td>

</tr>

<tr>

<td>Unidentified</td>

</tr>

<tr>

<td rowspan="4">Figurative images of person – sex</td>

<td>Female</td>

</tr>

<tr>

<td>Male</td>

</tr>

<tr>

<td>Mixed</td>

</tr>

<tr>

<td>Unidentified</td>

</tr>

<tr>

<td rowspan="4">Figurative images of person – social distance</td>

<td>Intimate</td>

</tr>

<tr>

<td>Personal</td>

</tr>

<tr>

<td>Social</td>

</tr>

<tr>

<td>Public</td>

</tr>

<tr>

<td>Number of figurative and abstract images  

</td>

<td>Numeric value  

</td>

</tr>

<tr>

<th colspan="2">Visual identity of the university – corporate design</th>

</tr>

<tr>

<td>Coat of arms</td>

<td>Yes/No</td>

</tr>

<tr>

<td>Symbol</td>

<td>Yes/No</td>

</tr>

<tr>

<td rowspan="2">Typography</td>

<td>Name</td>

</tr>

<tr>

<td>Monogram</td>

</tr>

<tr>

<td>Visual identity manual</td>

<td>Yes/No</td>

</tr>

<tr>

<td>National item  

</td>

<td>Yes/No  

</td>

</tr>

<tr>

<th colspan="2">Notes</th>

</tr>

<tr>

<td>Characteristics that are relevant, but were not included in previous categories  

</td>

<td>Descriptive  

</td>

</tr>

</tbody>

</table>

## Appendix 2

Definitions of the composition types used in the analysis

1_2_1 composition – header at the top of the page, menus are on the left and right side of the page under the header, in the middle - news, photos or some other content.  
1_3 or one-third rule (or golden ratio) - sets the elements on the web page in the ratio of the gold section.  
F composition – the visual elements on the page form the shape of the letter F. The menus are mostly below the header, on left side.  
OF composition – opposite of the F composition (menus are on the right side of the page)  
MC (middle and corners) - the main element on the page (also the most dominant in shape, color or size) is located at the center of the web page. On the upper and lower parts of the web page are menus, links or other contents.  
Z composition - refers to the type of composition in which the user's view is led from the upper left corner of the web page (where the name of the institution, symbol or coat is usually located) continues to the horizontal width of the page and slides down to the left, continuing again horizontally.

</section>

</article>