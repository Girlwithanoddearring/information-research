<header>

#### vol. 23 no. 1, March, 2018

</header>

<article>

# Justifying the use of Internet sources in school assignments on controversial issues

## [Teemu Mikkonen](#author)

> **Introduction.** This study concerns students’ criteria in the evaluation of Internet sources for a school assignment requiring reflections on a controversial issue. The findings are elaborated by analysing students’ discursive accounts in justifying the use or non-use of sources.  
> **Method.** The interview data was collected in a Finnish upper secondary school during classes on religion and ethics. Thirty-nine students were interviewed in one to three person groups after they had given the presentation in the classroom.  
> **Analysis.** The interviews were analysed using the discourse analysis in social psychology approach. The analysis concerned how different accounts related to information evaluation are used to justify the truthfulness of knowledge.  
> **Results.** The most used evaluation criteria were the authority and neutrality of information. Various types of evaluation criteria and arguments were used simultaneously. The arguments were, in part, comparable to evaluation criteria presented in previous research.  
> **Conclusion.** The evaluation criteria are cultural objects that can be utilized in various ways within different discursive contexts. The criteria were used in students’ accounts alternately, not as mutually exclusive. Personal interests and high motivation with regard to the school assignment’s subject were essential for students to identify and use more reflective and diverse argumentation related to evaluation criteria.

<section>

## Introduction

The information environment in schools is changing rapidly. The growing number of networked sources calls for using better and more explicit methods to evaluate information that is available on the Internet. For example, previous research has recommended that students use various checklists, which helps them evaluate information (e.g., [Kapoun, 1998](#kap98) ; [Alexander and Tate, 1999](#ale99)). These checklists have been criticized in several studies, but they still form the basis for education on evaluating Internet sources in schools (e.g.,[Meola, 2004](#meo04); [Metzger, 2007](#met07)). In this paper, the role of the items included in checklists and other information evaluation criteria are analysed as part of the students’ accounts about information evaluation tasks. This perspective provides the possibility to discover the processes behind criteria that students actually refer to while seeking information for their school assignments.

In Internet-based information environments, argumentation related to the identification, selection and use of different information sources becomes more important, especially in the context of self-governed school assignments. This paper empirically examines how students evaluate and justify the use of Internet sources in a class on religion and ethics. In this class, students were assigned to make a presentation from controversial topics of their choice. Because topics were controversial in nature, the justification of choices was critically important. The study of argumentation related to information evaluation promotes the understanding of how students identify and use arguments related to the evaluation of Internet sources. Consequently, the results have practical implications for teaching critical evaluations of information in schools. Further, it also presents an example of how justifications of evaluation criteria employed by the students can be investigated using the discourse analysis in social psychology research method. The overall aim of the present investigation is to specify how students evaluate and justify the use of Internet-based information in school assignments dealing with controversial issues. To this end, the study addresses the following research questions:

RQ1\. What kind of criteria do students employ to evaluate information available in the networked sources?

RQ2\. What kind of discursive accounts do students provide to justify the use or non-use of such sources?

The rest of the paper is organized as follows. First, a short background review of research related to evaluation criteria and judgement factors is presented. Second, discourse analysis in social psychology is characterized as a research method. Third, the findings are reported by specifying the criteria and the arguments by which the students evaluated the Internet sources. Fourth, drawing on discourse analysis, the ways in which such criteria were constructed as meaningful are specified by focusing on diverse accounts presented by the students. Finally, the findings are discussed and some conclusions are presented.

</section>

<section>

## Background

There are a number of studies examining the use of Internet sources with a focus on evaluation criteria and judgement factors (e.g., [Metzger, 2007](#met07); [Rieh, 2002](#rie02); [Meola, 2004](#meo04)). Kapoun ([1998](#kap98)) has presented a checklist of five criteria for evaluating Internet sites: accuracy, authority, objectivity, currency and coverage. Variations of this checklist model have been the basis for librarians to teach the evaluation of Internet sites ([Alexander and Tate, 1999](#ale99)). Metzger ([2007](#met07)) summarized previous research related to the checklist model. Her conclusion is that not all of the five checklist criteria are used in everyday information seeking. Further, most respondents used a criterion—Website appearance—that is not included in the list at all. Meola ([2004](#meo04)) criticized the checklist model, because it is unwieldy for users to perform and is unrealistic regarding teaching critical evaluation. He offered a model of contextual credibility assessment that emphasized the external characteristics of Websites. Rieh (2002) identified and characterized the facets of information quality based on users’ own words, and received results that were different from previous studies. Her study showed that quality and authority are important issues for users when they are interacting with information in an uncontrolled environment such as the Internet. The authority of the sources was closely related to the quality of the information. Metzger ([2007](#met07)) introduced a dual processing model of credibility assessment, which was based on Chen and Chaiken’s ([1999](#che99)) dual processing theory. The model emphasizes the importance of motivation in information evaluation by proposing that the evaluation of Internet sources depends on users’ motivation for seeking information.

Here, the focus is on argumentation that occurs after information sources have been selected and used in students’ schoolwork. This perspective is different from the studies reviewed above. In the checklist model, contextual credibility assessment, and analysis of cognitive authority, the focus is on the analysis of decision-making situations before the use of Internet sources. The argumentation on the qualities or choice of information sources afterwards is not studied (see [Metzger, 2007](#met07); [Meola, 2004](#meo04); [Rieh, 2002](#rie02), p. 146). When students are required—by the school assignment or by an external researcher—to argue for the information sources they have used, they need to reconsider the quality, authority and topicality of the sources, especially since there is no general quality control mechanism on the Internet. The retrospective evaluation of information sources is an integral part of students’ accounts that they employ while explaining why they have used certain information sources. The analysis of the accounts related to information use on the Internet will give new insights and complementary information to the research of evaluation criteria and judgement factors.

</section>

<section>

## Method

### Discourse Analysis

The goal in the discourse analytic perspective is to explore the possibilities of language use in different contexts (e.g., [Talja, 1999](#tal99)). In the late 1990s, discourse analysis was introduced to library and information science as a method that allows a wider inclusion of context dependency and variability in qualitative interviews ([Talja, 1999](#tal99)). The discourse analytic perspective concentrates on the regularities of language use: the kinds of descriptions and accounts of a topic that are possible, the type of evaluations they are based on, how the different modes of accounting construct different versions of the topic or produce different kinds of truths, and the accomplishments of these versions ([Talja, 1999](#tal99); [Wetherell and Potter, 1988](#wet88) ).

Discourse analysis contains various approaches. The present study draws on the ideas of discourse analysis in social psychology, which is based on the writings by Jonathan Potter and Margaret Wetherell ([Potter and Wetherell, 1987](#pot87); [Wetherell and Potter, 1988](#wet88); [Potter, 1996](#pot96); see also [McKenzie, 2003](#mck03)). Discourse analysis in social psychology is a mixture of methods and theories (including ethnomethodology, semiology and conversation analysis) and it ‘seeks to incorporate insights from a variety of discourse analytic approaches’, ([McKenzie, 2003](#mck03), p. 268). In the present study, discourse analysis in social psychology provides a methodological approach, which helps to interpret the different justification processes as part of social practices (see [Potter, 1996](#pot96)). The analysis of the argumentation related to information evaluation helps to understand the process behind the everyday use of different criteria.

Discourse analysis in social psychology proceeds from the assumption that there is no speech, text or any other communication free of argumentation. Arguments are acting somehow to accomplish something. From this perspective, for example, concepts like identity and fact are interpreted differently than in everyday contexts. Identities are not understood as pre-existing mental states. Rather, they are subject positions that are constructed during the conversation. Similar to identities, facts are understood as constructed descriptions. Facts are used in conversation to convince other debaters that the speaker is reporting true knowledge presented as an objective description of reality ([Potter, 1996](#pot96); [Billig, 1996](#bil96)).

Validity and reliability in discourse analysis are interpreted differently than, for example, in quantitative research. In discourse analysis, the criteria of validity and reliability are related to the specimen perspective. In the specimen perspective, the research data is not taken to describe reality. Rather, research data is the specimen of interpretative practices. The reliability of the findings depends on the verifiability of the researcher’s interpretations and they must, in a consistent and identifiable way, be based on the research data ([Talja, 1999](#tal99); [Potter and Wetherell, 1987](#pot87)). In the present study, the use of text extracts as the specimens of the data strengthens the reliability and validity of the interpretations in that the extracts provide concrete examples of how the students evaluated information available in the networked sources. In addition, through the texts extracts, the reader can evaluate how consistently the researcher has interpreted the research data.

### Data collection

The empirical data were gathered in autumn 2012 from an upper secondary school situated in the city of Tampere, Finland. The study was focused on a class on religion and ethics—an obligatory course for third grade students. In this class, all students were divided into groups and each group chose a topic on which they made a presentation. Argumentation was an important part of this class, because most topics were controversial in nature. The topics included, for example, medical ethics, women’s rights, gay rights, divorce, ethical consumerism, euthanasia and genetic engineering. Students made online research about topical issues, and discussed them first in their own group and then with the whole class under the guidance of the teacher. In this context, it was appropriate to examine the role of argumentation in information seeking and especially the justifications of the use or non-use of Internet sources on controversial issues.

Students prepared their presentations in twenty-one groups of one to three members. There were twenty-one student groups that were interviewed after their presentation. The total number of students was forty-two: twenty-five girls and seventeen boys. The interviews were conducted in groups after the assignment was completed. All except three groups participated as full groups to the interviews. One girl and two boys could not participate. The interviews were recorded and they lasted approximately fourteen minutes a group. All interviews were transcribed before the analysis. Two researchers conducted the interviews using a structured interview guide (see Appendix). The interviews consisted of five themes: motives, group work strategy, information-seeking practices, conception of information sources and the justification of contradictory views.

In addition to interviews, the research data consist of the source notes of the group meetings, which students had two to three times before the presentation. In the source notes, students described which source of information they did and did not use. They also had to describe why they decided to use particular sources. The teacher did not assign the source notes and it was voluntary for the student groups to fill in and return the structured forms. The source notes were done in groups, and ten student groups (altogether twenty students) returned the source notes. The interviewers used the source notes to present as much concrete questions as possible. The source notes were also used to support the analysis of the interviews when it was difficult to trace from the transcriptions which or why particular sources had been used.

In the present study, discourse analysis in social psychology is used to analyse the ways in which the students provide accounts of criteria whereby they evaluate the credibility and relevance of Internet sources for the needs of school assignments that focus on controversial issues. The context of the assignment persuades the students to justify using particular Internet sources in many ways, and therefore it is appropriate to study the argumentation processes. The identification of different arguments is bound to the context of where they were expressed. This means that the endpoint of the analysis is not, for example, a list of repertoires or rhetorical strategies, but a demonstration of how the different arguments are linked to the social practices, and to the perspective in which they were created (see [Savolainen, 2004](#sav04)).

### Identification of discursive accounts

The role of text extracts is different in discourse analysis than in other qualitative research methods. Text extracts are not seen as descriptions of the object of research; they are the objects of research ([Talja, 1999](#tal99)) and provide the linguistic evidence for the researcher’s interpretations. Because the aim is to explore the possibilities of language use, the focus is not in generalizability, but rather in how the phenomenon can be seen or interpreted. ([Talja, 1999](#tal99); [Potter and Wetherell, 1987](#tal99)).

Discourse analysis in social psychology emphasizes two features of the discourse analysis: the epistemological orientation and the action orientation. The epistemological orientation focuses on analysing the ways that discourses are constructed and made to appear factual, and the action orientation focuses on the discursive functions that accounts are meant to perform ([Potter, 1996](#pot96)). In the present study, the main emphasis is laid on the action orientation of discourse analysis, because it focuses on students’ accounts that justify the use or non-use of a source type, that is, Internet sources in school assignments. However, the epistemological orientation of discourse analysis is also relevant. The accounts that justify the use or non-use of Internet sources draw on the evaluation of the information’s credibility available in them. Thus, the ways that students convinced others (action orientation) and constructed information as factual or credible (epistemological orientation) with different criteria were intertwined ([Potter, 1996](#pot96)).

The discursive accounts provided by the students were identified by scrutinizing the transcribed interviews several times until certain themes could be discerned. The most commonly used themes were named, and the text extracts were chosen to illustrate the findings and provide linguistic evidence of the accounts. The extracts were selected on the basis of their density, thoroughness, accuracy and depth of explanation. Each text extract refers to a group (e.g., Group 2) to which the interviewed students (e.g., Mathilda and Olga) belonged. The students’ names are pseudonyms to protect their identities.

The checklist model augmented with the criterion of Website appearance has been taken into account when the discursive accounts were categorized. Also the comparison and corroboration techniques from Meola’s contextual approach have been included in the categorization. Furthermore, the use of the concept of cognitive authority defined by Wilson (1983) was observed when the category of authoritativeness of information was analysed. Finally, the relevance of motivation in information evaluation was taken into account when the discussion and final conclusions were made (see [Metzger, 2007](#met07); [Chen and Chaiken, 1999](#che99)). While specifying the discursive accounts of evaluation criteria, quantitative measures such as about half of the groups (ten out of twenty-one) were provided to substantiate the background information. Most importantly, these measures indicate the popularity of individual evaluation criteria among the students.

</section>

<section>

## Results

Overall, the students justified information available in the networked sources by employing diverse arguments that were bound to the context of a school assignment. First, the findings specify the use of different evaluation criteria in students’ discursive accounts. Next, the findings reported in this section indicate how the evaluation of networked sources is part of the representations and justifications in the students’ speech.

### Discursive accounts of evaluation criteria

The criterion that the student groups used most in their accounts was based on the authority of the information source. For example, if the information source was an authoritative writer or institution, it was seen as credible. Almost all groups (twenty out of twenty-one) used this criterion in their accounts. Another common criterion was to use the neutrality of the information source as a justification of its truthfulness. In this account, the student was claiming that the source was non-biased, or if the sources were biased, different views were presented. This criterion was used in almost all groups (nineteen out of twenty-one). About half of the groups (ten out of twenty-one) used their own experience and prior knowledge as an account to justify the truthfulness of the source. Arguably, these groups were motivated to do their school assignment well, because the topic was somehow connected to their personal interests. The accounts in this category were more reflective and critical than in other categories. Arguments including accounts that justify the truth by saying that there were multiple sites with similar information were categorized under the category consensus. About half of the groups (ten out of twenty-one) used this explanation. The visual appearance of the information source was also used as a justification in about half of the cases (ten out of twenty-one). The student groups, who in an interview said that they had less motivation to get good grades, used this explanation more often than others. This account was used mostly along with other accounts. There were also criteria, like freshness or the viewpoint of the site, which were only used in some cases (four out of twenty-one).

#### Authoritativeness of information source

In almost all groups, students argued for using a particular information source by referring to the characteristics related to the authoritativeness of the source. This could mean an authoritative institution, author, publication, organization or some other information source that they considered credible (e.g., [Rieh, 2002](#rie02)). The following quotation is an example showing how an official institution acts as a self-evident warrant about the credibility of information.

> Interviewer: How did you evaluate these? Why exactly were these useful? Was it just that it was easy to find?  
> Paul: No, it was the speech. Because it was the former sports minister and… In any case, I didn’t bother to take anything from blogs and any other single person’s points of views that … to form the overall picture that could be a little one-sided. (Group 21)

At first, the interviewer asks why students have used these exact sources, and suggests that the reason could be that those sources were easy to find. Behind this suggestion is an account that interviewees have made before to explain the use of certain sources. Nevertheless, the interviewee does not use this as an explanation. The student, Paul, says that the reason why they used this source was that it was a speech by a former sports minister. The sports minister, a representative of an official institution, is used as evidence to justify the truthfulness of the information. Even though the information is in a format (blog) that has, in other contexts, been considered unreliable, in this case the official title and institution warrants the credibility. Therefore it is not seen as ‘a single person’s point of view’, although the information is contained in the former representative’s personal blog post.

The next text extract is another example of how authoritativeness is used to justify the credibility of an information source. It demonstrates how academic role models can act as a self-evident testimony to the truthfulness of knowledge.

> Mathilda: Well, maybe because I’m so interested in [Jacques] Lacan it came to my mind ... because I couldn’t find anything from the net. I had to find books and read about Lacan ... and also from Julia Kristeva, I got the kind of symbolic register that is to say the points of view related to liberal feminism, which were not new, but … that information, which could have changed my own views, maybe.  
> Interviewer: Did you find any … books have a little different view, but any books, which could have had claims that are opposite to yours?  
> Mathilda: Yes, there was. There were a few. I can’t remember who, but some Finnish male researchers who had at some time in the [19]80s made the research; it was certainly fun. (Group 2)

Mathilda takes the floor and talks fast over the others about her own interests. She introduces her knowledge about Jacques Lacan and Julia Kristeva. Here and later during the interview, the talk about Jacques Lacan and Julia Kristeva acts as a supposedly credible argumentation that the student uses to convince others about the truthfulness of knowledge. In this quotation, Jacques Lacan and Julia Kristeva belong to the category of authors that is seen to be trustworthy. The interviewee uses the specific vocabulary that is used in the works of the authors, and the use of the vocabulary can be read as a way to stand out from her peers by convincing the interviewers about the knowledge in these academic theories.

Jacques Lacan and Julia Kristeva are authorities that the interviewee uses to stress the distinction between book-based knowledge and Internet-based knowledge. First the student refers to the relation of Jacques Lacan and book-based knowledge when she says ‘because I couldn’t find anything from the Internet. I had to find books and read about [Jacques] Lacan’. In this way she expresses that she is accountable, because she uses books as a main information source, but also expresses that this kind of information is not Internet-friendly. The avoidance of Internet-based information can be read as a distinctive feature that helps a student stand out from the others.

When the interviewer asks about the opposite claims, the interviewee answers that there are ‘some Finnish male researchers who had at some time in the [19]80s made the research; it was certainly fun’. In this quotation, the interviewee sees that the claim does not need any specific authors to be credible. The mention of ‘Finnish male researchers from [19]80s’ is enough. In contrast to Jacques Lacan and Julia Kristeva, the negative examples will not need any specific names or use specific vocabulary to convince listeners about the knowledge the interviewee has on the work of the authors. The vague expression in this context works as an example that will not need any further explanation.

#### Neutrality of information source

Another evaluation criterion, which was used as part of an argument, was the neutrality of the information source. In almost all groups students used this evaluation criterion when they wanted to convince the interviewer that they had used information sources from different sides equitably without taking only the sources that support their own views. In the following text extract, neutrality acts as a self-evident evaluation criterion and as an argument to justify the truthfulness of information. Still, the interviewee wants to bring up her own point of view.

> Interviewer: Did you find any views from the Internet or books, which were different from yours, and if you did, from where mostly?  
> Olga: I found about the ordination of women. I support the ordination of women, and then from the Internet it is possible to find those who are against the ordination of women.  
> Interviewer: How did you take them into account to this?  
> Olga: Well, they have to be taken into account, because they are an important part of the presentation and I can’t leave them out because they have an opposite point of view. And I tried to speak about them ... even though I thought that this is so wrong, but after a while I got used to it and I could look at it more objectively. (Group 2)

The interviewer persuades the interviewees to give their own views by asking which opposite opinions they’ve found from the information sources. An interviewee, Olga, follows this and tells her view about the ordination of women. She sets herself in the defensive position when she says that even if she wanted, she couldn’t ignore opposite opinions. Thus, she distances herself to be an objective observer who must notice opposite views. The reference to her views with the expression ‘this is so wrong…’ is mitigated by saying it with a smile. In this way the contrast between her own opinion and the neutrality is softened. The neutrality is accompanied by the obligation to acknowledge opposing views, even though the identity of the interviewee is (self-) categorized as biased. Therefore, the neutrality is strengthened by professing her point of view.

Olga was pointing to neutrality as an obligatory viewpoint that is more binding in this context than her own point of view. Neutrality as an obligatory feature of discourse is the framework in which the student has to act in the school, even though she wants to bring up her own point of view.

#### Students’ own experience and prior knowledge

Some students had studied the subject before and were interested in studying it more closely. They had discussed it with different people, and may have had certain opinions about it. In the following quotation, the student uses her own prior knowledge and values as an evaluation criterion and an account.

> Interviewer: Could you say about these, which are the most essential sources? Were there some particular sites?  
> Mary: Well then, I can’t… It is impossible to say, because anyway, those all… These values are discussed over a long period that it is difficult to tell any exact site, because it was… Because I’ve had so much information already about this case. That is the reason why we hadn’t really sought that [information]... (Group 5)

The interviewer asks about the most essential information sources and the interviewee, Mary, does not answer this question, but begins to talk about values. She ties her values to her prior knowledge, which is the reason why she cannot name any exact source. The knowledge she brings to the school assignment is a result of values that are discussed over a long period of time. Mary is reflecting her own position as a person who is interested about the things that this presentation is about, and that is why she does not have to seek information or remember any exact sites.

In the next quotation, she continues to explain which kind of sites she has browsed, and how most of the sites are in some way biased.

> Mary: Or, like I’ve searched information from somewhere; those sites are biased in some direction depending on which side they are… Like for example, some animal rights’ sites or Greenpeace. So they present their case from only one point of view. That, it presents facts, but not necessarily everything. They won’t give those arguments that are against their own. So you have to be critical with these. (Group 5)

Mary explains that she has searched information from biased sites like animal rights’ organizations and Greenpeace, but she also brings up her understanding of the viewpoints that these Internet sites have. She explains that those sites might give you one-sided arguments, and that is why she also calls for criticality. This is an example of how the recognition of a student’s own interests can support the understanding of others’ interests and points of view. Thus, her personal interests act as a tool to interpret and evaluate the partiality of networked sources.

#### Appearance of information source

In some student groups, the appearance of the Internet sites was a common evaluation criterion that students used when they justified the non-use of certain information sources. In the next quotation, a group of two boys explains which kind of Internet sites they avoided.

> Interviewer 1: Well, have you discussed together that credibility? Have you evaluated it?  
> Interviewer 2: Generally about these things…  
> Matt: Yes, we have. I found an Internet site and there was a tank drawn nicely with [Microsoft] Paint, and a little bit of small text, and some Harry from the Hervanta neighbourhood has written. Then we noticed that it wouldn’t be from here.  
> David: It is possible to see it from its general appearance then. (Group 18)

When the interviewers ask if the students have discussed the credibility of the sources, the interviewee, Matt, answers that they have, and he illustrates by giving an example of a site that they have not used. The illustration consists of appearance details that the interviewees think are self-evident characteristics of a non-credible Internet source.

This example shows how a site’s appearance is used as an argument to exclude certain sites. This explanation was often used, particularly to exclude non-credible Internet material. The groups that said they were not motivated to get good grades and just wanted to get the job done used more explanations related to the appearance, freshness or other factors that are external to information content. The groups, who said in the interview that they were motivated to do a good presentation, used this argument in most cases along with other arguments.

#### Consensus between information sources

The similarity of information occurring in multiple sites is named the consensus between information sources criterion. It refers to accounts in which students think that there is a consensus of truthfulness in different information sources. This is the main reason why students in the next quotation trust certain Internet sources.

> Interviewer: How about these sources? Do you think they are trustworthy?  
> Lucy: Well those, which we sifted through, which were slightly unreliable sources, those were surprisingly correct.  
> Interviewer: Okay. How did you evaluate that those were reliable?  
> Lucy: We were just looking—that it is possible to find the same information from multiple sites. (Group 9)

When the interviewer asks if some sources are trustworthy, the interviewee, Lucy, answers on the behalf of both that unreliable sources were also correct because it is possible to find the same information from multiple sites. Still, Lucy doesn’t explain which kind of sites had similar information.

Lucy recognizes and uses this criterion to justify the chosen information and later refers to this consensus criterion by saying that they have ‘a scientific point of view’. This corroborates the recognition of the criterion, even though the evaluation of the individual sites is based mostly on their appearance. In the next quotation, Lucy continues the explanation of why some sites were unreliable.

> Interviewer: Okay. But you also sifted through something… Which kind were those? Which were unreliable?  
> Lucy: Well, the kind that has, for example, a lot of adverts, those are a little vague. And if the language is like¬—that it isn’t literary language; it is also a little questionable.  
> Interviewer: Okay. You think so also?  
> Anna: Yeah. And then also because of the appearance—that site.  
> Interviewer: Okay.  
> Anna: Like Lucy said about the adverts, but anyway. (Group 9)

Even though both students use consensus criterion in their accounts, they exclude the sites by appearance. Consensus criterion was used as an argument to convince others that the evaluated information was credible, but the process, how the evaluation was done, and how the similar sites were evaluated individually was unclear. When it was asked which kinds of sites were unreliable, the argument was justified differently. The students justified the non-use of information by referring to the appearance of the sites, not to the contradictions in the different information sources.

</section>

<section>

## Discussion

Previous research on the evaluation of Internet sources is focused on the decision-making situations before the use of information (e.g., [Rieh, 2002](#rie02); [Metzger, 2007](#met07)). The analysis of argumentation occurring after the use of information sources gives an opportunity to look closely at how information evaluation takes place in everyday discussions. In this paper, the use of justifications with evaluation criteria of use and non-use of networked sources were studied using discourse analysis. Discourse analysis, which examines the production of knowledge as a linguistic rather than cognitive process, helps to develop deeper understanding of the everyday fact construction with related justifications ([Tuominen, Talja and Savolainen, 2002](#tuo02); [Potter, 1996](#pot96)).

Students used different kinds of criteria and arguments when they evaluated and justified the information they had used in their school assignments. They recognized similar criteria as presented in, for example, the checklist model ([Kapoun, 1998](#kap98)) or the contextual credibility assessment ([Meola, 2004](#meo04)), and used them in their accounts. The criteria were used to make an account appear factual and as an argument to convince others that the networked sources were truthful. Thus, the ways that students convinced others (action orientation) and constructed the truth (epistemological orientation) with different criteria were intertwined (see [Potter, 1996](#pot96)).

The most popular criterion and argument concerned the authoritativeness of information sources. The academic authors or government institutions were considered credible, because they looked academic or official. This result corresponds to Rieh’s ([2002](#rie02)) findings in which she noticed that the participants perceived cognitive authority when information looked scholarly and was from academic or government institutions. Although students used this argumentation in most cases, they used also other arguments simultaneously.

Another often used argument was related to the neutrality of information sources. Students used this argument when they reflected either their own views on the topic, or the perspective of the information source. Controversial information sources and opposing views were identified by recognizing their own points of view. In this study, the students’ arguments have coincidental similarities with evaluation criteria presented in the checklist model proposed by Kapoun ([1998](#kap98)). This category is close to the objectivity criteria in the checklist model ([Kapoun, 1998](#kap98); [Metzger, 2007](#met07)). The difference is that the students did not only evaluate information sources in their accounts; they also evaluated their own views and partiality with respect to the topic.

The students who justified the sources by referring to their prior knowledge reflected their own position as an information evaluator more often than others. It was easier for them to understand the context and viewpoint of the information source, because they could evaluate the impact of their own position to the subject. Therefore a student’s awareness of his or her own motivation and position to the subject helped to understand the opinions behind the information source. This result was partly in line with Metzger’s ([2007](#met07)) dual processing model in which the evaluation of Internet sources depends on users’ motivation.

The argument that there were multiple sites that had a consensus of trustworthy information was used to assure the accuracy of information. This category is close to what Meola ([2004](#meo04)) described in his contextual approach to Website evaluation. Meola’s critical approach to the checklist model emphasized the external characteristics of the sites, like promoting reviewed sources, comparing of different sources and corroborating from trusted sources. Students used justifications that included comparison of different sources or corroboration from trusted sources; however, when they was asked specifically which sites had corroborated the information and why they were credible, the students usually relied on the appearance of Internet sites. Consensus was recognized as a credible way to convince others that the information was truthful, but the evaluation behind the consensus was ambiguous.

Some groups said that they did not have much motivation to contribute to the school assignment, and that is why they did not spend much time evaluating information either. This result validates Metzger’s ([2007](#met07)) conclusion related to dual processing models, in which motivation and ability are keys to critical information evaluation. These groups justified the choice of information sources mostly by appealing to the Internet site’s appearance. This result corresponds with Metzger’s ([2007](#met07)) review of research related to the checklist model, in which she presents that design and presentational elements are, in some cases, more important factors than the criteria in the checklist model. The appearance of the Internet site was rarely the only argument that students used, but rather a part of a set of arguments. Nevertheless, students did not find it as convincing an argument as other arguments, and it was used mostly to exclude non-credible Internet sources.

</section>

<section>

## Conclusions

If information evaluation is understood only as cognitive skills, which can be measured with surveys, the potential to understand students’ own ways to interpret and create knowledge is narrower than in the constructionist approaches (see [Tuominen, Talja and Savolainen, 2002](#tuo02)). Here, discourse analysis is used to present and explore the argumentation related to the evaluation of information use in the contexts of an uncontrolled environment, such as the Internet. The aim was to study how students identify various evaluation criteria, and how they actively use them in their arguments. Thus, discourse analysis enabled the study of claims as cultural objects, which are not properties of individuals, but ways of talking in the public discourse ([Roth and Lucas, 1997](#rot97)).

During the study, it was noticed that most students used different evaluation criteria and arguments simultaneously depending on the situation and context. This finding confirmed the premise that the evaluation criteria are cultural objects that can be used in various ways in different discursive contexts. The same student may shift from one criterion to another within the same account, and in some cases these criteria can be conflicting (see [Savolainen, 2004](#sav04)). This suggests that the criteria are used in students’ arguments alternately, not as mutually exclusive.

Another significant finding was to recognize the role of personal interests and motivation related to the argumentation of evaluation criteria. If the group did not have personal interests to study the subject and/or own perspectives to the subject, they used less diverse evaluation criteria in their arguments (see [Metzger, 2007](#met07)). This also meant that they did not reflect on the perspective of the information source. In some cases, motivated students did not reflect their viewpoint, which could prevent them from seeing the subject from other points of view. Therefore it is important to first map students’ own views about the subject. After that, it is possible to teach them how to evaluate the perspectives behind the information sources, and how it is possible to construct more balanced and reflective argumentation. Without the awareness of their own viewpoints, students may just corroborate their biased prior knowledge.

</section>

<section>

## Acknowledgements

This study was part of the Know-Id, which was project funded by the Academy of Finland (grants number 132341) and by the Finnish Cultural Foundation. The author wishes to thank the teacher of the case course and the researcher Tuulikki Alamettälä who assisted with data collection during the case course. The author would also like to thank professors Eero Sormunen and Reijo Savolainen for their constructive feedback to improve the paper.

## <a id="author"></a>About the author

Teemu Mikkonen received his Master of Social Science degree in Sociology in 2010 from the University of Tampere, Finland. He can be contacted at: [teemu.mikkonen@staff.uta.fi](mailto:teemu.mikkonen@staff.uta.fi)

</section>

<section>

## References

<ul> 
 
 <li id="ale99">Alexander, J. E., &amp; Tate, M. A. (1999). <em>Web wisdom: how to evaluate and create information quality on the Web.</em> Mahwah, NJ: Lawrence Erlbaum Associates.</li>
 
 <li id="bil96">Billig, M. (1996). <em>Arguing and thinking: a rhetorical approach to social psychology.</em> Cambridge: Cambridge University Press.</li>
 
 <li id="che99">Chen, S., &amp; Chaiken, S. (1999). The heuristic-systematic model in its broader context. In S. Chaiken and Y. Trope (Eds.), <em>Dual-process theories in social psychology</em> (pp. 73-96). New York, NY: The Guilford Press.</li>
 
 <li id="kap98">Kapoun, J. (1998). Teaching undergrads WEB evaluation: a guide for library instruction. <em>College &amp; Research Libraries News, 59</em>(7), 522-523.</li>
 
 <li id="mck03">McKenzie, J. (2003). Justifying cognitive authority decisions: discursive strategies of information seekers.<em>The Library Quarterly 73</em>(3), 261-288.</li>
 
 <li id="meo04">Meola, M. (2004). Chucking the checklist: a contextual approach to teaching undergraduates Website evaluation. <em>Libraries and the Academy, 4</em>(3), 331-344.</li>
 
 <li id="met07">Metzger, M. J. (2007). Making sense of credibility on the Web: models for evaluating online information and recommendations for future research. <em>Journal of the American Society for Information Science and Technology, 58 </em>(13), 2078-2091.</li> 
 
 <li id="pot96">Potter, J. (1996). <em>Representing reality: Discourse, rhetoric and social construction.</em> London &amp; Thousand Oaks, CA: Sage.</li>
 
 <li id="pot87">Potter, J., &amp; Wetherell, M. (1987). <em>Discourse and social psychology: beyond attitudes and behaviour.</em>London: Sage.</li> 
 
 <li id="rie02">Rieh, S. Y. (2002). Judgment of information quality and cognitive authority in the Web. <em>Journal of the American Society for Information Science and Technology, 53</em>(2), 145-161.</li> 
 
 <li id="rot97">Roth, W. M., &amp; Lucas, K. B. (1997). From ‘truth’ to ‘invented reality’: a discourse analysis of high school physics students’ talk about scientific knowledge.<em>Journal of Research in Science Teaching, 34</em>(2), 145-179.</li> 
 
 <li id="sav04">Savolainen, R. (2004). <a href="http://www.webcitation.org/6wpQJU4II">Enthusiastic, realistic and critical: discourses of Internet use in the context of everyday life information seeking</a>. <em>Information Research, 10</em>(1) paper 198. Retrieved from http://InformationR.net/ir/10-1/paper198.html (Archived by WebCite&amp;reg; at http://www.webcitation.org/6wpQJU4II)</li> 
 
 <li id="tal99">Talja, S. (1999). Analyzing qualitative interview data: the discourse analytic method. <em>Library &amp; Information Science Research, 21</em>(4), 459-477.</li>
 
 <li id="tuo02">Tuominen, K., Talja, S., &amp; Savolainen, R. (2002). Discourse, cognition, and reality: toward a social constructionist metatheory for library and information science. In Harry Bruce, Ray Fidel, Peter Ingwersen, and Pertti Vakkari (Eds.), <em>Proceedings of the Fourth International Conference on Conceptions of Library and Information Science (CoLIS): Emerging Frameworks and Methods.</em> Libraries Unlimited, Greenwich, CT (pp. 271-283).</li>
 
 <li id="wet88">Wetherell, M., &amp; Potter, J. (1988). Discourse analysis and the identification of interpretive repertoires. In Charles Antaki (Ed.), <em>Analysing everyday experience: a casebook of methods</em> (pp. 168-183). London: Sage.</li>
 
 <li id="wil83">Wilson, P. (1983). <em>Second-hand knowledge: an inquiry into cognitive authority.</em> Westport, CT: Greenwood Press.</li>
 </ul>

</section>

<section>

## Appendix: Interview guide

1.  What was your topic? [Identifying a group]
2.  How interesting was your topic? [Motives]
    *   What made your topic interesting or non-interesting?
    *   Do you all agree that the topic was interesting?
3.  How did you organize your group’s work? What did each person do? [Teamwork strategy]
    *   Did you work mostly together or separately? How did you communicate with each other?
    *   How often did you meet?
4.  Where did you get the material? [Information-seeking practices]
    *   Did you use, for example, the Internet or library?
    *   Did you get the material from the teacher or from another person?
    *   Did you have applicable material from school or home, for example, books, etc., before?
5.  What material did you have?
6.  Was it easy to find information?
7.  Which information sources were the key sources for your presentation and where did you get them? [Conception of information sources]
    *   Do you think that these information sources were trustworthy?
    *   Why do you think that these particular information sources seemed to be useful?
    *   If you compare the useful information sources, how do you think the usefulness differs from each other?
    *   Why is the information of the useful sources trustworthy?
    *   Were there any other information sources that you haven’t mentioned?
8.  Did you leave out any non-relevant information sources? [See source notes]
    *   Why did you reject them?
    *   [If nothing is rejected] So, you included all relevant information (from Internet, books and teacher)?
9.  What kind of discussion did you have about this topic when making the presentation? [Conciliation and justification of conflicting views]
    *   What other issues did you discuss when making the presentation?
    *   Were there any conflicting views within the group?
    *   Was there anyone other than group members involved in the discussions?
10. Did you find any conflicting views from, for example, Internet or books?
    *   From where did you mostly find them?
    *   Did you consider these different views when you made your presentation?
11. Do you think that this way of working was reasonable?
    *   How or why was this kind of assignment reasonable?
    *   What did you like best in your group’s work?
    *   How much did you contribute to this task?
    *   How was your contribution reflected in your work?
    *   Did you gain any new interesting insights when working together?
    *   Was there something that was particularly difficult?
12.  Were you satisfied with the result?
13.  Did you get an enthusiastic reception for your presentation?
        *  Were there interesting discussions after your presentation?

</section>

</article>