<header>

#### vol. 17 no. 4, December, 2012

# How does customer's product expertise moderate the usefulness of information recommendation agents in online stores?

#### [Hyung Jun Ahn](#authors)  
School of Business, Hongik University, Seoul, Korea  
[Sangmoon Park](#authors)  
Department of Business Administration, Kangwon National University, Chuncheon city, Gangwon-do, Korea


<h4>Abstract</h4>

> **Introduction.** Recommendation agents are software programs used by Internet stores to help customers find relevant information and make better purchasing decisions. This study investigates the moderating effect of customers' product expertise on the relationship between user evaluation of recommendation agents and its antecedents.  
> **Method.** An experimental shopping mall of digital cameras was implemented for empirical examination of the hypotheses. Participants were asked to conduct virtual shopping in the shopping mall using the information recommendation agents.  
> **Analysis.** The hypotheses were tested with the partial least squares method using the collected data. The samples were divided into high and low product expertise groups and the results of the analysis were compared between the two groups to see the moderating effect of product expertise.  
> **Results.** Product expertise does moderate some of the relationships, showing difference between high and low product expertise groups. Analysis of the results shows that familiarity with recommendation agents and frequency of online shopping affect only the high expertise group, while specificity of shopping intention affects only the low expertise group.  
> **Conclusions.** The difference in customers' product expertise needs to be taken into account when developing and providing information recommendation agents to customers to improve their shopping experience.

## Introduction

With the wide acceptance of online shopping, many Internet businesses have developed and deployed customer information aid tools often called recommendation agents (hereafter 'agents'). Agents are software programs that are usually embedded in Internet stores to help customers search and find products. Considering that there are a huge number of choices available on the Internet and that real-time human assistance is not usually available at online stores, agents are now regarded as an essential tool for making the purchasing process efficient and effective ([Adomavicius and Tuzhilin 2005](#ado05); [Ahn 2006](#ahn06); [Burke 2002](#bur02); [Kwon _et al._ 2009](#kwo09); [Wang and Doong 2010](#wan10); [Xiao and Benbasat 2007](#xia07)).

Most agents consist of two key components: eliciting user preference and matching the preference with product profiles. For example, in a recommendation system used at Amazon.com, user preferences are implicitly collected whenever a user clicks on or purchases a product ([Linden _et al._ 2003](#lin03)). This information is passed to an internal mechanism that finds other products that are most likely to be preferred and purchased by the user. Users may also explicitly enter preferences regarding, for example, desired price ranges, authors or key words.

There have been many studies about agents from various perspectives. Among them, technical studies have developed algorithms or mechanisms of recommendation that can more precisely reflect user preferences ([Ahn 2008](#ahn08); [Adomavicius and Tuzhilin 2005](#ado05); [Ahn 2006](#ahn06); [Burke 2002](#bur02)). On the other hand, behavioural researchers in the information systems or marketing fields have studied factors that affect the adoption or effectiveness of recommendation agents. Studies of this type often conduct empirical investigation involving experiments with real or virtual Internet stores and questionnaires ([Cohen and Fan 2000](#coh00); [Greco _et al._ 2004](#gre04); [Wang and Benbasat 2005](#wan05); [Xiao and Benbasat 2007](#xia07)). The present study falls into the second category.

Although previous studies have found some common factors influencing the usefulness or acceptance of agents, there have been relatively few studies about how the relationships can be moderated by user characteristics. The goal of this study is to investigate the relationship between the user evaluation of agents and its antecedents, particularly focusing on the moderating effect of customers' product expertise. This is believed to be a significant issue since the existence of such a moderating effect can require Internet stores to develop and apply different personalisation approaches to distinct types of customers. In other words, the result of this research can help businesses to better customise agents or use different agents for heterogeneous types of customers for more effective information recommendation. For the investigation, an experimental shopping mall for digital cameras was built and used for shopping experiments and data collection. The collected data were analysed using the partial least squares method.

The paper is organized as follows. The next section provides theoretical background of the research and explains the hypotheses. The third section introduces the experiments and data collection. The fourth section presents findings from the analysis. The final section concludes with a summary, implications for businesses, and limitations of the study.

## Theoretical background and hypotheses development

### Studies on recommendation agents

Similar to the definitions of recommendation agents found in literature ([Adomavicius and Tuzhilin 2005](#ado05); [Burke 2002](#bur02); [Xiao and Benbasat 2007](#xia07)), this paper defines an agent as a software program embedded in an Internet store to help customers find products by providing guidance and/or recommending products. In this sense, previous studies have regarded agents as decision support systems for customers ([Grenci and Todd 2002](#gre02); [Haubl and Trifts 2000](#hau00); [Kim _et al._ 2010](#kim10)).

Many studies have shown that agents help customers to make better, more efficient, purchasing decisions ([Hostler _et al._ 2004](#hos04); [Pedersen 2000](#ped00)). Agents can significantly improve both decision process and decision outcome, especially when the choice set is huge and customers have limited ability and time in evaluating all the alternatives. For these reasons, agents are also believed to help businesses by enabling up-selling and cross-selling of products. Hence, agents are increasingly being regarded as an essential tool by many Internet stores.

There have been several different classifications of agents. The most common classification is based on whether recommendation is made using content similarities (content-based filtering) or user similarities (collaborative filtering) ([Cohen and Fan 2000](#coh00); [Greco _et al._ 2004](#gre04); [Kim and Kim 2001](#kim01); [Li _et al._ 2005](#liy05)). There are many variations to these recommendation methods such as item-based collaborative filtering where item similarity is used instead of user similarity. Another popular scheme of classification is based on whether the inputs to an agent are at the feature level or the needs level ([Felix _et al._ 2001](#fel01); [Komiak and Benbasat 2006](#kom06); [Stolze and Nart 2004](#sto04)). The feature-based agents require selection of product features by customers to generate recommendations; the needs-based agents produce recommendations based on the specific type of needs chosen by a user such as, for example, a camera for family or a camera for sports. This paper adopts the latter typology and provides the two different types of agent for experimental shopping. Still more classifications of agents are available in literature ([Adomavicius and Tuzhilin 2005](#ado05); [Burke 2002](#bur02); [Xiao and Benbasat 2007](#xia07)).

### Hypotheses development

Despite the abundance of literature on the factors that determine the users' acceptance or evaluation of agents, there have not been many studies investigating the moderating role of personal characteristics, especially customers' product expertise. Based on a literature review, this sub-section develops the hypotheses for our research goal. We introduce the antecedents, the moderating variables, and the dependent variables for our suggested model of users' evaluation of agents. The hypotheses are largely based on empirical findings in related studies as well as theories such as task-technology fit, technology acceptance model and consumer decision-making.

#### Antecedent variables: intention specificity, agent-intention fit, familiarity with agents, and online shopping frequency

Some studies show the influence of the type of agent on the effectiveness or users' evaluation of agents ([Felix _et al._ 2001](#fel01); [Komiak and Benbasat 2006](#kom06); [Stolze and Nart 2004](#sto04)). The general conclusion from the studies is that some agents are preferred or perceived to be more effective by users. For example, the empirical study by Felix _et al._ ([2001](#fel01)) shows that needs-based agents are evaluated to be more effective by the participants of the experiment for purchasing digital cameras. Also, in the review by Xiao and Benbasat ([2007](#xia07)), several propositions were presented that claim the superiority of one type of agent over others in terms of users' decision quality and decision effort.

However, the preference for a specific type of agent may vary according to the intention of a user. For example, a user who has a very specific shopping goal may know the desired attributes of the target product very well, in which case a feature-based agent can be more suitable. The user can input values or select options for the features to generate recommendations easily. On the other hand, if the intention of a user is ambiguous or the user is not sure of the specific features of the desired product, a needs-based agent can be more helpful. The task-technology fit theory also supports this idea and many studies have tested the theory in the information systems field ([Gebauer and Tang 2008](#geb08); [Klopping and McKinney 2004](#klo04)). For instance, in Klopping and McKinney's study ([2004](#klo04)), the theory was found to have a significant effect on the perceived usefulness of an e-commerce system. Therefore, it is proposed that:

> H1: Higher perceived fit between a user's shopping intention and the type of agent positively impacts the evaluation of the agent.

In addition to the above, the specificity of a user's intention can alone have an impact on the relationship. When users have a specific goal and know what type of products to search for, they may just browse through the categories in the Internet store to find the products. For example, most online digital camera shops provide browsing categories that correspond to different numbers of pixels, zoom capabilities, brands and price ranges, together with some sorting functionality for each category, making it easy to find products once users know what to search for. Empirical studies on the acceptance of e-commerce or Internet services also support this. For example, in the study by Kim _et al._ ([2008](#kim08)) the acceptance of e-commerce by users is shown to be affected by the equivocality of online tasks. In the work of Jarvelainen ([2007](#jar07)), task ambiguity was found to have negative impact on the acceptance of online booking for passenger cruise services. In and experiment by Tam _et al._ ([2006](#tam06)), more specific goals lead users to react more sensitively to advertisements. Therefore, it is proposed that:

> H2: Higher perceived specificity of a user's shopping intention has a negative impact on the evaluation of the agent.

We also include variables that have been found often to affect the relationship in the literature on agents: frequency of user's online shopping experience ([Castaneda _et al._ 2007](#cas07); [Jarvelainen 2007](#jar07); [Kim _et al._ 2008](#kim08); [Klopping and McKinney 2004](#klo04); [Stern _et al._ 2008](#ste08)) and user's familiarity with agents in general ([Komiak and Benbasat 2006](#kom06); [Stern _et al._ 2008](#ste08)).

> H3: Higher frequency of online shopping of a user has positive impact on the evaluation of the agent.  
> H4: Higher familiarity of a user with agents in general has positive impact on the evaluation of the agent.

#### Product expertise as a moderator

There have been studies about the effect of customer's product expertise, or category expertise, on the preference of agents or acceptance of recommendation results. As high expertise groups are more capable of selecting products themselves based on their knowledge of the products, they can rely less on recommendations from agents ([Kramer 2007](#kra07); [Urban _et al._ 1999](#urb99)). In addition to this, higher expertise groups have also been found to favour agents that are feature-based, attribute-oriented, or use content-based filtering compared with needs-based, benefit-oriented, or collaborating-filtering methods ([Pereira 2000](#per00); [Rathnam 2005](#rat05); [Xiao and Benbasat 2007](#xia07); [Su _et al._ 2008](#suh08)). This is because the former types of agent give more control to customers allowing them to specify or choose detailed features of products so that they can pinpoint the desired products. The latter types of agent can be more relevant for low expertise users because they do not need detailed knowledge about product attributes, but can just request recommendations based on their broad needs.

We extend and apply the above insights about product expertise to the other relationships in our model to test its moderating role. For a high product expertise group, agent-intention fit may be less important because they rely less on an agent for shopping; in contrast, for a low product expertise group, agent-intention fit can be more significant because they are more likely to need the help of agent recommendations ([Kramer 2007](#kra07); [Urban _et al._ 1999](#urb99); [Xiao and Benbasat 2007](#xia07)). Similarly, the effect of intention specificity can also be moderated. Given a broad, non-specific shopping goal, a low product expertise group may rely more on recommendations from an agent, whereas a high expertise group need not be so reliant ([Kramer 2007](#kra07); [Urban _et al._ 1999](#urb99); [Xiao and Benbasat 2007](#xia07)). Next, agents can be easier to use for a high expertise group if those users are familiar with agents in general. In contrast, low expertise groups can still find it more difficult because of their limited knowledge of the products. Finally, online shopping frequency can also impact the two groups differently. High expertise groups with enough shopping frequency can find agents unnecessary because they can browse through the categories and find the products without the help of agents. Low expertise groups may nevertheless need the guidance of agents.

Therefore, it is proposed that:

> H5: Customer's product expertise moderates the influence of the antecedent variables on the evaluation of an agent.

#### User's evaluation of an agent and the effectiveness of decisions

In the extensive review by Xiao and Benbasat ([2007](#xia07)), it was found that two groups of constructs usually appear as outcome variables of agent use: consumer decision making and user evaluation of agents. The first group represents how the process and result of decision making are affected by use of agents, while the latter represents users' own direct evaluation of agents. This research also uses similar variables as dependent variables.

Basically the technology acceptance model was adopted and modified for this study. This model has been used widely in studies on the acceptance of many different types of information systems ([Park _et al._ 2009](#par09); [Stern _et al._ 2008](#ste08); [Van Dijk _et al._ 2008](#van08); [Wann-Yih and Chia-Ying 2007](#way07)). Recently, some studies on agents have also adopted variables related to the model ([Wang and Benbasat 2005](#wan05)).

The original model has three variables: perceived ease of use, perceived usefulness, and acceptance of the system. Following the approaches in similar studies ([Kowatsch and Maass 2010](#kow10); [Wang and Benbasat 2005](#wan05)), these three variables and their relationship were adopted as a whole for users' evaluation of agents. The first two variables, ease of use and usefulness, were directly adopted. Hypothesis 6 is also from the original model, which states the influence of perceived ease of use on perceived usefulness. Next, unlike many studies that use this model in an organizational management information systems context, this study focuses on the purchasing decision of consumers. Hence, rather than using the original variable about the acceptance of a system, we took the approach by Kamis and Davern ([2004](#kam04)) to include a variable about the effectiveness of decisions. That is, if an agent is perceived to be easier to use, it can make the purchase decision more effective by allowing better utilisation; similarly, if an agent is perceived to be more useful, it leads to a more effective purchase decision.

> H6: The perceived ease of use of an agent has positive impact on the perceived usefulness of the agent.  
> H7: The perceived ease of use of an agent has positive impact on the perceived effectiveness of purchase decision.  
> H8: The perceived usefulness of an agent has positive impact on the perceived effectiveness of purchase decision.

See [Table 1](#tab1) for the summary of the constructs.

<figure>![The conceptual model](p550fig1.jpg)

<figcaption>  
Figure 1: The conceptual model</figcaption>

</figure>

## Research method

### Experiment

An experimental online store for digital cameras was built for collecting data to test the proposed model (Figure 2). The shop has one hundred cameras from eleven brands, ranging from low-price compact products to high-end professional cameras. Users can browse the shop according to three attribute categories: brands, number of pixels, and price ranges. Users can find products and put as many products as they want into the shopping cart before they finish the experiment.

<figure>![The main screen of the experimental store](p550fig2.jpg)

<figcaption>  
Figure 2: The main screen of the experimental store</figcaption>

</figure>

The online store has two different types of agent: feature-based and needs-based. The feature-based one, shown on the left-hand side of Figure 3, allows users to select features of a desired camera and recommends cameras that match the selection. The results are ordered by average customer ratings. We used the ratings data available from [www.dpreview.com](http://www.dpreview.com) at the time of the experiment for the sorting. The needs-based agent is also shown on the right-hand side of Figure 3\. It presents users with a list of typical consumer needs of cameras so that users can select one in the list for the given shopping goal. The list was also adopted from an online shop ([www.imaging-resource.com](http://www.imaging-resource.com)) at the time of the experiment. It also provides a list of three basic features so that users can narrow down their search by selecting the values of the basic features. During the experiment, only one of the two agents is randomly assigned to each participant so that about half the users could use the feature-based one, and the other half the needs-based one.

Each user is also randomly given a shopping task of either buying a camera that is most similar to a specific product, or buying a camera for a family member (Figure 4). The former is intended to provide participants with a specific goal, whereas the latter is somewhat ambiguous, while still providing a realistic shopping purpose.

This way, each user belongs to one of the four _agent, shopping task_ combinations. Note, however, that this study is not assuming any definite fit between a specific agent and a specific shopping task. The reason for providing two agents and two tasks is mainly to create diversity in the variables so that the participants can have varying levels of perception about such variables as agent-intention fit and intention specificity. Using definite fits for studies like this is very hard because the perceived fit by each user can be different despite being given the same agent and task. Therefore, this study also measures the perceived fit rather than using fixed fits as has been done in previous studies ([Larsen _et al._ 2009](#lar09); [Lin and Huang 2008](#lin08)).

<figure>![Feature-based (left) versus needs-based (right) agents](p550fig3.jpg)

<figcaption>  
Figure 3: Feature-based (left) versus needs-based (right) agents</figcaption>

</figure>

Participants were instructed to put as many products as they like into the shopping cart, but they had to choose two as their final choice at the end of the experiment. The reason for asking participants to purchase two products was to encourage them to use and experience the system, and the agents assigned to them, more extensively.

<figure>![Two different shopping goals given to users randomly](p550fig4.jpg)

<figcaption>  
Figure 4: Two different shopping goals given to users randomly</figcaption>

</figure>

### Data from the experiment

Before the experiment, a pilot test was performed involving about twenty graduate and undergraduate students at a management school in an English speaking country to find and fix possible problems of the experiment settings. The actual experiment was performed with 220 undergraduate students at the same school taking a particular compulsory course. A tutor instructed the participants on the overview of the experiment and how to use the system. Any questions from the students regarding the task were also answered prior to the experiments. Students were given marks of about 1 percent of the course for the participation. No other compensation was given to the participants.

Although the use of student samples is sometimes questioned, the use of student subjects in this study can be justified as follows. First, online consumers are known to be generally younger and to have a higher education level, which is similar to the characteristics of university students ([McKnight _et al._ 2002](#mck02)). For this reason, many other studies published in the e-commerce field have used students for experiments or survey ([Ahuja _et al._ 2003](#ahu03); [Calisir _et al._ 2010](#cal10); [Choi and Lee 2003](#cho03); [Constantinides _et al._ 2010](#con10); [Hasan 2010](#has10); [McKnight _et al._ 2002](#mck02)). It has also been reported that research in the e-commerce field shows similar results between student samples and non-student samples ([Ahuja _et al._ 2003](#ahu03)). Moreover, the experiment in this study does not require the students to be in an unrealistic or unfamiliar setting. Rather, the participants are just asked to conduct a virtual online shopping in a way very similar to real e-commerce.

Out of the total 220 participants, 138 who used the given agent at least once were selected for the analysis. Others were excluded because they either finished the experiment without the help of agent, just browsing the categories, or submitted unusable values to some of the questions in the questionnaire. The reason why some of the participants did not use the agents is because the experiment did not force all the participants to use the agents, which was to provide a realistic online shopping environment. Otherwise, the experiment could have imposed unrealistic constraints on the participants. Pearson's chi-squared test was conducted on both the included and the excluded samples to see whether a specific task-agent combination caused the users to avoid using the agents or not finish the experiment properly. The results for the 138 included samples showed no significant difference (p=0.224), where the proportion of the participants ranged from 20% to 30% for each of the four combinations. The results for the 82 excluded samples were also similar, showing no significant difference (p=0.278), with the proportion of each combination ranging from 20% to 30%.

### Measures

The measures used by this study are listed in Table 1\. The existing validated measures were used where available (see [Appendix 1](#app1) for individual instruments). All the variables were measured using an online questionnaire that followed each individual shopping experiment. The data were split into two groups of high and low product expertise respectively, based on the median value of the item average of the construct. This led to seventy-four users of low expertise and sixty-four of high, where the inequality in the two groups was caused by tie values among some users.

<table class="center" id="tab1" style="width:95%;"><caption>  
Table 1: Summary of the measures <details><summary>Note</summary> See Appendix-1 for the specific items used for each construct</details></caption>

<tbody>

<tr>

<th style="width:34%;">Variable</th>

<th style="width:66%;">Definition</th>

</tr>

<tr>

<td>1\. Intention specificity</td>

<td>The degree to which a user perceives a shopping goal is specific.</td>

</tr>

<tr>

<td>2\. Agent-intention fit</td>

<td>The degree to which a user perceives a given agent is suitable for the given shopping intention compared with the other agent.</td>

</tr>

<tr>

<td>3\. Familiarity with agents</td>

<td>How familiar a user is with agents in general.</td>

</tr>

<tr>

<td>4\. Online shopping frequency</td>

<td>How frequently a user shops online.</td>

</tr>

<tr>

<td>5\. Ease-of-use of agents</td>

<td>How easy a user perceives the agent to be to use.</td>

</tr>

<tr>

<td>6\. Usefulness of agents</td>

<td>How useful a user perceives the agent to be.</td>

</tr>

<tr>

<td>7\. Effectiveness of decision</td>

<td>How a user perceives about the effectiveness of the final purchase decision</td>

</tr>

<tr>

<td>8\. Product expertise</td>

<td>How well a user knows about the products</td>

</tr>

</tbody>

</table>

### Method

This study used the partial least squares method for analysing the data. Similar to LISREL (a program for LInear Structural RELations), partial least squares is a popular method for analysing causal relationships and was chosen for the following reasons: first, unlike LISREL, it can be used reliably for relatively small data samples ([Barclay _et al._ 1995](#bar95)). Secondly, it does not require strict normality of data for analysis unlike many other methods ([Wold 1985](#wol85)). Thirdly, it can combine regression and factor analysis within the same statistical procedure: this allows the test of a model and the generation of reliability measures simultaneously, which makes it easy to apply the method to complicated causal models ([Barclay _et al._ 1995](#bar95); [Hulland 1999](#hul99)). Finally, it has been widely and successfully used in the e-commerce and management information systems field ([Goodhue _et al._ 2006](#goo06); [Marcoulides _et al._ 2009](#mar09)), as well as other fields in business studies ([Fornell and Bookstein 1982](#for82); [Hulland 1999](#hul99)).

### Measurement results: reliability and validity

In many applications of partial least squares in management studies, reliability is usually checked with the loadings of the measures with their associated constructs ([Hulland 1999](#hul99)). It is generally accepted that individual measures are statistically reliable if the loadings of them are larger than 0.7\. In other words, loadings of a measure above the threshold are considered to show enough variance is accounted for by the respective constructs. In Table A2 of [Appendix 2](#app2), it is shown that all the measures have loadings greater than 0.7.

In addition to the reliability of the individual measures, it is important to check whether the measures of each construct together show convergent validity. Two measures of convergent validity are often used: Cronbach's alpha and composite reliability ([Hulland 1999](#hul99)). Again the threshold value 0.7 is generally accepted for both alpha and composite reliability. It can be seen from Table A3 of [Appendix 2](#app2) that all the constructs satisfy these requirements.

Next, it is important to assure that the measures of each construct are different from the measures of the other constructs, which requires the test of discriminant validity. In studies using partial least squares, average variance extracted (i.e., the correlation between a construct and its measures) is often used for the test ([Fornell and Bookstein 1982](#for82)). Simply, the square root of the average variance of each construct needs to be greater than the correlation between the construct and any other. In Table A3 of [Appendix 2](#app2), it is observed that the square root values (the diagonal elements in the correlation matrix) of all the constructs exceed the correlation values of the other constructs (the off-diagonal elements), showing strong discriminant validity.

## Data analysis and results

### Full model (hypotheses 1, 2, 3, 4, 6, 7, and 8)

Table 2 summarises the findings of the analysis. First, the analysis of the full model showed partial support for the hypotheses. The effects of online shopping frequency appeared to be non-significant on its dependent variables. Also, agent-intention fit, agent familiarity, and intention specificity all appeared to have no significant influence on the perceived usefulness of agent. While the perceived ease of use turns out to be significant on the decision effectiveness, the perceived usefulness of agent does not.

</section>

<table class="center" style="width:95%; font-size:x-small;"><caption>  
Table 2: Results of the partial least squares analysis (for both full and moderated models) <details>Note *p <0.05.  ** p <0.01.  �p <0.001</details></caption>

<tbody>

<tr>

<th rowspan="3"> </th>

<th rowspan="2" colspan="2">Full model  
(n=138)</th>

<th colspan="4">Moderated models (H5)</th>

<th rowspan="3" style="vertical-align:bottom; width:5%;">Difference in path coefficients between subgroups (t-value)</th>

</tr>

<tr>

<th colspan="2">High expertise group (n=64)</th>

<th colspan="2">Low expertise group (n=74)</th>

</tr>

<tr>

<th>Path coefficient</th>

<th>t-value</th>

<th>Path coefficient</th>

<th>t-value</th>

<th>Path coefficient</th>

<th>t-value</th>

</tr>

<tr>

<td>[H1.1] Online shopping freq. - usefulness of agent</td>

<td style="text-align:center">-0.01</td>

<td style="text-align:center">-0.09</td>

<td style="text-align:center">0.11</td>

<td style="text-align:center">1.29</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">-0.56</td>

<td style="text-align:center">0.15 (1.29)</td>

</tr>

<tr>

<td>[H1.2] Online shopping freq. - ease of use of agent</td>

<td style="text-align:center">-0.15</td>

<td style="text-align:center">-1.55</td>

<td style="text-align:center">**-0.44**</td>

<td style="text-align:center">**-3.77�**</td>

<td style="text-align:center">**0.03**</td>

<td style="text-align:center">**0.26**</td>

<td style="text-align:center">**-0.47 (-2.88**)**</td>

</tr>

<tr>

<td>[H1.2] Online shopping freq. - ease of use of agent</td>

<td style="text-align:center">-0.15</td>

<td style="text-align:center">-1.55</td>

<td style="text-align:center">**-0.44**</td>

<td style="text-align:center">**-3.77�**</td>

<td style="text-align:center">**0.03**</td>

<td style="text-align:center">**0.26**</td>

<td style="text-align:center">**-0.47 (-2.88**)**</td>

</tr>

<tr>

<td>[H2.1] Familiarity with agents - ease of use of agent</td>

<td style="text-align:center">0.26</td>

<td style="text-align:center">2.99**</td>

<td style="text-align:center">**0.37**</td>

<td style="text-align:center">**2.97****</td>

<td style="text-align:center">**0.20**</td>

<td style="text-align:center">**1.45**</td>

<td style="text-align:center">0.17 (0.90)</td>

</tr>

<tr>

<td>[H2.2] Familiarity with agents - usefulness of agent</td>

<td style="text-align:center">0.14</td>

<td style="text-align:center">2.09*</td>

<td style="text-align:center">0.14</td>

<td style="text-align:center">1.23</td>

<td style="text-align:center">0.14</td>

<td style="text-align:center">1.69</td>

<td style="text-align:center">0.01 (0.04)</td>

</tr>

<tr>

<td>[H3.1] Agent-intention fit - ease of use of agent</td>

<td style="text-align:center">-0.25</td>

<td style="text-align:center">-3.41�</td>

<td style="text-align:center">-0.25</td>

<td style="text-align:center">2.65**</td>

<td style="text-align:center">-0.28</td>

<td style="text-align:center">-2.28*</td>

<td style="text-align:center">0.03 (0.17)</td>

</tr>

<tr>

<td>[H3.2] Agent-intention fit - usefulness of agent</td>

<td style="text-align:center">-0.06</td>

<td style="text-align:center">-1.01</td>

<td style="text-align:center">-0.02</td>

<td style="text-align:center">0.14</td>

<td style="text-align:center">-0.08</td>

<td style="text-align:center">-1.06</td>

<td style="text-align:center">0.07 (0.52)</td>

</tr>

<tr>

<td>[H4.1] Intention specificity - ease of use of agent</td>

<td style="text-align:center">0.28</td>

<td style="text-align:center">3.67�</td>

<td style="text-align:center">**.18**</td>

<td style="text-align:center">**1.19**</td>

<td style="text-align:center">**0.28**</td>

<td style="text-align:center">**2.48***</td>

<td style="text-align:center">-0.11 (-0.60)</td>

</tr>

<tr>

<td>[H4.2] Intention specificity - usefulness of agent</td>

<td style="text-align:center">-0.06</td>

<td style="text-align:center">-1.13</td>

<td style="text-align:center">-0.04</td>

<td style="text-align:center">0.32</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">-0.89</td>

<td style="text-align:center">0.02 (0.13)</td>

</tr>

<tr>

<td>[H6] Ease of use of agent - usefulness of agent</td>

<td style="text-align:center">0.77</td>

<td style="text-align:center">12.84�</td>

<td style="text-align:center">0.77</td>

<td style="text-align:center">7.08�</td>

<td style="text-align:center">0.80</td>

<td style="text-align:center">10.98�</td>

<td style="text-align:center">-0.03 (-0.24)</td>

</tr>

<tr>

<td>[H7] Ease of use of agent - decision effectiveness</td>

<td style="text-align:center">0.59</td>

<td style="text-align:center">5.14�</td>

<td style="text-align:center">0.61</td>

<td style="text-align:center">3.13**</td>

<td style="text-align:center">0.46</td>

<td style="text-align:center">3.44�</td>

<td style="text-align:center">0.17 (0.72)</td>

</tr>

<tr>

<td>[H8] Usefulness of agent - decision effectiveness</td>

<td style="text-align:center">0.06</td>

<td style="text-align:center">0.52</td>

<td style="text-align:center">**-0.10**</td>

<td style="text-align:center">**-0.52**</td>

<td style="text-align:center">**0.28**</td>

<td style="text-align:center">**2.09***</td>

<td style="text-align:center">-0.38 (-1.64)</td>

</tr>

</tbody>

</table>

<section>

Overall, we can observe that agent familiarity and intention specificity have the most impact on the ease of use, and the ease of use leads to decision effectiveness, while the usefulness of agent may not. Simply put, the results show that when the users perceive their intention as being more specific, and are more familiar with an agent, they find it easier to use and this leads to more effective purchase decisions.

### Moderated models (hypothesis 5)

To assess the moderating effect of product expertise on the relationships, further PLS analyses were performed for the two sub-groups of high and low product expertise. The result is also summarised in Table 2 which shows whether the statistical significance of a relationship differs between the two groups. Also, we can check whether the differences in the two path coefficients of a relationship in the two groups are statistically significant (see the t-values in the last column).

Table 2 shows that product expertise does moderate four of the relationships (see the rows that have values in bold in the columns of the moderated models). First, agent familiarity appeared to have significant influence on the ease of use of agent only for the high expertise group. That is the high expertise group appeared to find the agents easier to use as they get more familiar with agents, whereas the low expertise group did not. It can be interpreted that the limited product knowledge of the low expertise group makes it difficult for the group to adapt to the recommendation agents of cameras regardless of their general knowledge about agents. Second, the results show that more specific intentions make the agent easier to use for the low expertise group, while it does not for the other group, which is consistent with what was expected. Third, online shopping frequency appeared to have negative significant influence on the perceived ease of use of agent for the high expertise group, while not being significant in the other group. This is also consistent with the expectation that the high expertise group with better online shopping skills can easily find the desired products without the help of the given agent. In contrast, shopping frequency had no influence on the other group with lower product knowledge. Finally, the relationship between the perceived usefulness of the agents and the decision effectiveness is also being moderated, where it is significant only for the low expertise group. That is, for the high expertise group, a useful agent may not necessarily lead to an improved purchase decision, while it can for the other group. The other relationships appeared not to be moderated by product expertise.

The last column of Table 2 shows the results of the t-tests regarding the significance of the difference between the path coefficients of the two moderated models. Only the fourth relationship showed significant difference, suggesting a stronger moderating effect of product expertise on this relationship.

The above results suggest that product expertise plays an important role in explaining when and what type of users perceive agents to be easy to use and helpful for improving decision effectiveness.

## Discussion and conclusion

### Summary

The main goal of this study was to investigate the moderating role of product expertise on the relationship between users' evaluation of information recommendation agents and its antecedents. We hypothesised that product expertise moderates the relationship such that high and low expertise groups will perceive the influence of the independent variables differently. An experimental shopping mall of digital cameras was constructed to conduct a virtual shopping experiment for investigating the hypotheses. Two agents and two shopping tasks were prepared to create diversity in the experiment variables. Participants were randomly assigned one of the agents and one of the tasks.

The empirical investigation and analysis of the experiment showed that product expertise does moderate some of the impacts of agent familiarity, intention specificity, and online shopping frequency on the ease of use of agents. Agent familiarity and online shopping frequency influenced the ease of use only in the high expertise group. On the other hand, intention specificity affected the perceived ease of use only for the low expertise group. The influence of agent-intention fit on the perceived ease of use of the agent appeared not to be moderated by product expertise. Also, it was found that the impacts of the independent variables on the perceived usefulness of agents are not moderated by product expertise.

The main contribution of this research can be summarised as follows. First, there have been only a limited number of studies on the role of product expertise in the broad field of research on agents. Thus, the results of this research contribute to the advancement of our understanding of how different users with different product expertise may find the usefulness of various agents. Second, more specifically, no study so far has studied the moderating role of product expertise on the influence of intention specificity and agent-intention fit on users' evaluation of agents. The result of the analysis showed that users with different product expertise can react to agents differently when the specificity of shopping intention varies.

### Implications for research and practice

First, we could clearly see that users with different characteristics or intentions perceive the ease of use of the given agents differently. Although this research just used two types of agents, there are many others such as those based on past purchase history or those that recommend products based on clicks. Moreover the research suggests that many of the users may find other methods of searching for products more effective than agents, for example, different listing or categorisation of products. Therefore, businesses need to understand that a single agent cannot make online shopping effective for everyone, and should consider providing multiple customer-aid information functions that can cater for different user characteristics and shopping goals ([Ahn 2010](#ahn10)).

Secondly, in line with the above, research is also needed on how online stores can effectively provide multiple recommendation functions. At most online stores, it is currently unusual to see many recommendation functions being provided. Academic research on the problem is also hard to find. Therefore, there exists a challenging and novel research problem of finding out how different types of customers can be identified correctly, and how different recommendation functions can be provided to users, distinctively and dynamically, without causing confusion to users. For example, further research is needed to find ways to identify whether a customer is an expert or not in digital cameras, and to lead that customer to the proper information recommendation function so that the whole shopping experience can be improved. The problem can be even more complicated for stores with diverse products because an expert in one product may not be knowledgeable about others.

Thirdly, more research is needed on the moderating relationship between the effectiveness of recommendation agents and its antecedents. There exist more variables for customers and products that may possibly moderate the relationship such as, for example, time spent on research before shopping and product complexity ([Wang and Benbasat 2007](#wan07)). Hence, as more studies are conducted, we can expect to develop an integrated framework for online stores that leads to a proper selection and mix of shopping aid agents according to the characteristics of the stores and users.

### Limitations of the study

This study has several limitations. First, because there is a huge variety of agents and many different ways of building them, one can not expect to produce exactly the same result from the present model when it is applied to different types of agent or different implementations. Second, we used only perceptive measures for the constructs. Experiments that involve direct measures can possibly yield more precise results. Third, the experiment was conducted with a single product type, digital camera. Therefore, care needs to be taken for the generalisation of the results.

## Acknowledgement

This work was supported by 2010 Hongik University Research Fund, together with the Contestable Research Fund of Waikato Management School, New Zealand.

## About the authors

**Hyung Jun Ahn** is an Associate Professor at School of Business, Hongik University, Korea. He received his PhD in Management Information Systems from Korea Advanced Institute of Science and Technology. Before joining Hongik University, he worked as a Senior Lecturer at Waikato Management School, New Zealand. He can be contacted at: [hjahn@hongik.ac.kr](mailto:hjahn@hongik.ac.kr)  
**Sangmoon Park** is an Associate Professor at Department of Business Administration, Kangwon National University, Korea. He received his PhD in Entrepreneurship and Innovation from Korea Advanced Institute of Science and Technology. Before joining Kangwon National University, he served as a manager at Samsung Corporate Research Centre. He can be contacted at: [venture@kangwon.ac.kr](mailto:venture@kangwon.ac.kr)

</section>

<section class="refs">

<form>

<fieldset><legend style="color: white; background-color: #5E96FD; font-size: medium; padding: .1ex .5ex; border-right: 1px solid navy; border-bottom: 1px solid navy; font-weight: bold;">References</legend>

*   Adomavicius, G. & Tuzhilin, A. (2005). Toward the next generation of recommender systems: a survey of the state-of-the-art and possible extensions. _IEEE Transactions on Knowledge & Data Engineering_, **17**(6), 734-749.
*   Ahn, H. J. (2006). Utilizing popularity characteristics for product recommendation. _International Journal of Electronic Commerce_, **11**(2), 57-78.
*   Ahn, H. J. (2008). A new similarity measure for collaborative filtering to alleviate the new user cold-starting problem. _Information Sciences_, **178**(1), 37-51.
*   Ahn, H. J. (2010). Evaluating customer aid functions of online stores with agent-based models of customer behavior and evolution strategy. _Information Sciences_, **180**(9), 1555-1570.
*   Ahuja, M., Gupta, B. & Raman, P. (2003). An empirical investigation of online consumer purchasing behavior. _Communications of the ACM_, **46**(12), 145-151.
*   Barclay, D., Higgins, C. & Thompson, R. (1995). The partial least squares (PLS) approach to causal modeling: personal computer adoption and use as an illustration. _Technology studies_, **2**(2), 285-309.
*   Burke, R. (2002). Hybrid recommender systems: survey and experiments. _User Modeling and User-Adapted Interaction_, **12**(4), 331-370.
*   Calisir, F., Bayraktaroglu, A.E., Gumussoy, C.A., Topcu, Y.I. & Mutlu, T. (2010). The relative importance of usability and functionality factors for online auction and shopping web sites. _Online Information Review_, **34**(3), 420-439.
*   Castaneda, J., Munoz-Leiva, F. & Luque, T. (2007). Web acceptance model (WAM): moderating effects of user experience. _Information & Management_, **44**(4), 384-396.
*   Choi, J. & Lee, K. H. (2003). Risk perception and e-shopping: a cross-cultural study. _Journal of Fashion Marketing and Management_, **7**(1), 49-64.
*   Cohen, W. W. & Fan, W. (2000). Web-collaborative filtering: recommending music by crawling the Web. _Computer Networks_, **33**(1-6), 685-698.
*   Constantinides, E., Lorenzo-Romero, C. & G�mez, M. A. (2010). Effects of web experience on consumer choice: a multicultural approach. _Internet Research_, **20**(2), 188-209.
*   Felix, D., Niederberger, C., Steiger, P. & Stolze, M. (2001). Feature-oriented vs. needs-oriented product access for non-expert online shoppers. In: _Proceedings of the 1st IFIP Conference on e-commerce, e-business and e-government, Zurich, Switzerland_. (pp. 399-406). Laxenburg, Austria: International Federation for Information Processing.
*   Fornell, C. & Bookstein, F. L. (1982). Two structural equation models: LISREL and PLS applied to consumer exit-voice theory. _Journal of Marketing research_, **19**(4), 440-452.
*   Gebauer, J. & Tang, Y. (2008). Applying the theory of task-technology fit to mobile technology: the role of user mobility. _International Journal of Mobile Communications_, **6**(3), 321-344.
*   Goodhue, D., Lewis, W. & Thompson, R. (2006). PLS, small sample size, and statistical power in MIS research. In _HICSS '06\. Proceedings of the 39th Annual Hawaii International Conference on System Sciences, 2006._ (p. 202b). Washington, DC: IEEE Press.
*   Greco, G., Greco, S. & Zumpano, E. (2004). Collaborative filtering supporting Web site navigation. _AI Communications_, **17**(3), 155-166.
*   Grenci, R. & Todd, P. (2002). Solutions-driven marketing. _Communications of the ACM_, **45**(3), 64-71.
*   Hasan, B. (2010). Exploring gender differences in online shopping attitude. _Computers in Human Behavior_, **26**(4), 597-601.
*   Haubl, G. & Trifts, V. (2000). Consumer decision making in online shopping environments: the effects of interactive decision aids. _Marketing Science_, **19**(1), 4-21.
*   Hostler, R. E., Yoon, V. Y. & Guimaraes, T. (2004). Assessing the impact of Internet agent on end users' performance. _Decision Support Systems_, **41**(1), 313-323.
*   Hulland, J. (1999). Use of partial least squares (PLS) in strategic management research: a review of four recent studies. _Strategic management journal_, **20**(2), 195-204.
*   Jarvelainen, J. (2007). Online purchase intentions: an empirical testing of a multiple-theory model. _Journal of Organizational Computing and Electronic Commerce_, **17**(1), 53-74.
*   Kamis, A. & Davern, M. J. (2004). Personalizing to product category knowledge: exploring the mediating effect of shopping tools on decision confidence. In Ralph A. Sprague, (Ed.). _Proceedings of the 37th Annual Hawaii International Conference on System Sciences, 2004._ (10 p.) Washington, DC: IEEE. [CD-ROM publication.]
*   Kim, B.-D. & Kim, S.-O. (2001). A new recommender system to combine content-based and collaborative filtering systems. _Journal of Database Marketing_, **8**(3), 244-252.
*   Kim, B., Park, S. & Lee, K. (2008). A structural equation modeling of the Internet acceptance in Korea. _Electronic Commerce Research and Applications_, **6**(4), 425-432.
*   Kim, J. K., Kim, H. K., Oh, H. Y. & Ryu, Y. U. (2010). A group recommendation system for online communities. _International Journal of Information Management_, **30**(3), 212-219.
*   Klopping, I. M. & Mckinney, E. (2004). Extending the technology acceptance model and the task-technology fit model to consumer e-commerce. _Information Technology, Learning & Performance Journal_, **22**(1), 35-48.
*   Komiak, S.Y.X. & Benbasat, I. (2006). The effects of personalization and familiarity on trust and adoption of recommendation agents. _MIS Quarterly_, **30**(4), 941-960.
*   Kowatsch, T. & Maass, W. (2010). In-store consumer behavior: how mobile recommendation agents influence usage intentions, product purchases, and store preferences. _Computers in Human Behavior_, **26**(4), 697-704.
*   Kramer, T. (2007). The effect of measurement task transparency on preference construction and evaluations of personalized Recommendations. _Journal of Marketing Research_, **44**(2), 224-233.
*   Kwon, K., Cho, J. & Park, Y. (2009). Influences of customer preference development on the effectiveness of recommendation strategies. _Electronic Commerce Research and Applications_, **8**(5), 263-275.
*   Larsen, T. J., Sorebo, A. M. & Sorebo, O. (2009). The role of task-technology fit as users' motivation to continue information system use. _Computers in Human Behavior_, **25**(3), 778-784.
*   Li, Y., Lu, L. & Xuefeng, L. (2005). A hybrid collaborative filtering method for multiple-interests and multiple-content recommendation in E-Commerce. _Expert Systems with Applications_, **28**(1), 67-77.
*   Lin, T. C. & Huang, C. C. (2008). Understanding knowledge management system usage antecedents: An integration of social cognitive theory and task technology fit. _Information & Management_, **45**(6), 410-417.
*   Linden, G., Smith, B. & York, J. (2003). Amazon.com recommendations: item-to-item collaborative filtering. _IEEE Internet Computing_, **7**(1), 76-80.
*   Marcoulides, G. A., Chin, W. W. & Saunders, C. (2009). A critical look at partial least squares modeling. _MIS Quarterly_, **33**(1), 171-175.
*   Mcknight, D. H., Choudhury, V. & Kacmar, C. (2002). Developing and validating trust measures for e-commerce: an integrative typology. _Information systems research_, **13**(3), 334-359.
*   Park, N., Roman, R., Lee, S. & Chung, J. E. (2009). User acceptance of a digital library system in developing countries: an application of the technology acceptance model. _International Journal of Information Management_, **29**(3), 196-209.
*   Pedersen, P. E. (2000). Behavioral effects of using software agents for product and merchant brokering: an experimental study of consumer decision-making. _International Journal of Electronic Commerce_, **5**(1), 125-141.
*   Pereira, R. E. (2000). Optimising human-computer interaction for the electronic commerce environment. _Journal of Electronic Commerce Research_, **1**(1), 23-44.
*   Rathnam, G. (2005). Interaction effects of consumers' product class knowledge and agent search strategy on consumer decision making in electronic commerce. _IEEE Transactions on Systems, Man and Cybernetics, Part A: Systems and Humans_, **35**(4), 556-573.
*   Stern, B. B., Royne, M. B., Stafford, T. F. & Bienstock, C. C. (2008). Consumer acceptance of online auctions: an extension and revision of the TAM. _Psychology & Marketing_, **25**(7), 619-636.
*   Stolze, M. & Nart, F. (2004). Well-integrated needs-oriented recommender components regarded as helpful. In _CHI EA '04: CHI '04 Extended Abstracts on Human Factors in Computing Systems_, (p. 1571). New York, NY: ACM Press.
*   Su, H., Comer, L. & Lee, S. (2008). The effect of expertise on consumers' satisfaction with the use of interactive recommendation agents. _Psychology and Marketing_,**25**(9), 859-880.
*   Tam, K. (2006). Understanding the impact of Web personalization on user information processing and decision outcomes. _Management Information Systems Quarterly_, **30**(1), 32.
*   Urban, G. L., Sultan, F. & Qualls, W. (1999). _[Design and evaluation of a trust based advisor on the Internet](http://www.webcitation.org/6CZ6XJ0eG)._ Boston, MA: Massachusetts Institute of Technology, Sloan School of Management. Retrieved 30 November, 2012 from http://bit.ly/ONu26K (Archived by WebCite �at http://www.webcitation.org/6CZ6XJ0eG)
*   Van Dijk, J. A. G. M., Peters, O. & Ebbers, W. (2008). Explaining the acceptance and use of government Internet services: a multivariate analysis of 2006 survey data in the Netherlands. _Government Information Quarterly_, **25**(3), 379-399.
*   Wang, H. & Doong, H. (2010). Online customers' cognitive differences and their impact on the success of recommendation agents. _Information & Management_, **47**(2), 109-114.
*   Wang, W. & Benbasat, I. (2005). Trust in and adoption of online recommendation agents. _Journal of the Association for Information Systems_, **6**(3), 72-100.
*   Wang, W. & Benbasat, I. (2007). Recommendation agents for electronic commerce: effects of explanation facilities on trusting beliefs. _Journal of Management Information Systems_, **23**(4), 217-246.
*   Wann-Yih, W. & Chia-Ying, L. (2007). A contingency approach to incorporate human, emotional and social influence into a TAM for KM programs. _Journal of Information Science_, **33**(3), 275-297.
*   Wold, H. (1985). Partial least squares. _International Journal of Cardiology_, **147**(2), 581-591.
*   Xiao, B. & Benbasat, I. (2007). E-commerce product recommendation agents: use, characteristics, and impact. _MIS Quarterly_, **31**(1), 137-209.

</fieldset>

</form>

</section>

</article>

<section>

<table class="footer" style="border-spacing:10;">

<tbody>

<tr>

<td colspan="3" style="text-align:center; background-color: #5E96FD; color: white; font-family: verdana; font-size: small; font-weight: bold;">Find other papers on this subject</td>

</tr>

<tr>

<td style="text-align:center; vertical-align:top;">

<form method="get" action="http://scholar.google.com/scholar" target="_blank">

<table class="footer">

<tbody>

<tr>

<td style="white-space: nowrap; vertical-align:top; text-align:center; height:32;"><input type="hidden" name="q" value="&quot;recommendation agents&quot; &quot;online stores&quot; expertise">  
<input type="submit" name="sa" value="Scholar Search" style="font-size: small; font-family: Verdana; font-weight: bold;"> <input type="hidden" name="num" value="100"></td>

</tr>

</tbody>

</table>

</form>

</td>

<td style="vertical-align:top; text-align:center;">

<form method="get" action="http://www.google.com/custom" target="_blank">

<table class="footer">

<tbody>

<tr>

<td style="white-space: nowrap; vertical-align:top; text-align:center; height:32;"><input type="hidden" name="q" value="&quot;recommendation agents&quot; &quot;online stores&quot; expertise">  
<input type="submit" name="sa" value="Google Search" style="font-family: Verdana; font-weight: bold; font-size: small;"> <input type="hidden" name="client" value="pub-5081678983212084"> <input type="hidden" name="forid" value="1"> <input type="hidden" name="ie" value="ISO-8859-1"> <input type="hidden" name="oe" value="ISO-8859-1"> <input type="hidden" name="cof" value="GALT:#0066CC;GL:1;DIV:#999999;VLC:336633;AH:center;BGC:FFFFFF;LGC:FF9900;LC:0066CC;LC:0066CC;T:000000;GFNT:666666;GIMP:666666;FORID:1;"> <input type="hidden" name="hl" value="en"></td>

</tr>

</tbody>

</table>

</form>

</td>

<td style="vertical-align:top; text-align:center;">

<form method="get" action="http://www.bing.com/" target="_blank">

<table class="footer">

<tbody>

<tr>

<td style="white-space: nowrap; vertical-align:top; text-align:center; height:32;"><input type="hidden" name="q" value="&quot;recommendation agents&quot; &quot;online stores&quot; expertise">  
<input type="submit" name="sa" value="Bing" style="font-size: small; font-family: Verdana; font-weight: bold;"> <input type="hidden" name="num" value="100"></td>

</tr>

</tbody>

</table>

</form>

</td>

</tr>

</tbody>

</table>

<div style="text-align:center;">Check for citations, [using Google Scholar](http://scholar.google.co.uk/scholar?hl=en&q=http://informationr.net/ir/17-4/paper550.html&btnG=Search&as_sdt=2000)</div>

<div style="text-align:center;">

<div style="text-align:center;" class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a><a class="addthis_button_tweet"></a><a class="addthis_counter addthis_pill_style"></a></div>

</div>

* * *

## Appendices

### Appendix 1: Measurement items

The following items were used to measure the constructs. The participants were asked to mark each response on a seven-point Likert scale ranging from _Strongly disagree_ to _Strongly agree_.

<table class="center"><caption>  
Table A1: Measurement items</caption>

<tbody>

<tr>

<th>Construct</th>

<th>Measure</th>

<th>References</th>

</tr>

<tr>

<td rowspan="3">Intention specificity</td>

<td>IS1\. Before the shopping, I knew very well what type of camera I have to purchase.</td>

<td rowspan="3"> </td>

</tr>

<tr>

<td>IS2\. Before the shopping, I knew very well what attributes of camera I have to look for.</td>

</tr>

<tr>

<td>IS3\. Before the shopping, I had clear ideas on how to find the cameras for my shopping goal.</td>

</tr>

<tr>

<td rowspan="3">Agent-intention fit</td>

<td>AIF1\. If you had a choice, which one do you think could be more useful for your shopping for the given shopping objective?</td>

<td rowspan="3"> </td>

</tr>

<tr>

<td>AIF2\. If you had a choice, which one do you think would improve your shopping result more for the given shopping objective?</td>

</tr>

<tr>

<td>AIF3\. If you had a choice, which one would you choose next time as your decision aid for the same shopping objective?</td>

</tr>

<tr>

<td rowspan="3">Familiarity with agents</td>

<td>FA1\. I often use recommendation systems when purchasing products online.</td>

<td rowspan="3"> </td>

</tr>

<tr>

<td>FA2\. I find recommendation systems very helpful when purchasing products online.</td>

</tr>

<tr>

<td>FA3\. I am very satisfied with the products recommended by the recommendation systems.</td>

</tr>

<tr>

<td>Online shopping frequency</td>

<td>OSF. How often do you buy things online? 1\. Never, 2\. Less often, 3\. 1-6 times a year, 4\. More than 6 times a year, 5\. A few times a month, 6\. A few times a week, 7\. Almost everyday</td>

<td> </td>

</tr>

<tr>

<td rowspan="4">Ease of use of agents</td>

<td>EUA1\. I found it easy to use the recommendation system for my purpose.</td>

<td rowspan="4">(Kamis and Davern 2004, adapted)</td>

</tr>

<tr>

<td>EUA2\. My interaction with the recommendation system was clear and understandable.</td>

</tr>

<tr>

<td>EUA3\. I found the recommendation system to be flexible to interact with.</td>

</tr>

<tr>

<td>EUA4\. It would be easy for me to become skilful at using the recommendation system.</td>

</tr>

<tr>

<td rowspan="3">Usefulness of agents</td>

<td>UA1\. Using the recommendation system in this experiment improved my decision making</td>

<td rowspan="3">(Kamis and Davern 2004, adapted)</td>

</tr>

<tr>

<td>UA2\. Using the recommendation system in this experiment improved my decision making efficiency</td>

</tr>

<tr>

<td>UA3\. In a real shopping situation, the recommendation system in this web store would be useful.</td>

</tr>

<tr>

<td rowspan="3">Effectiveness of decision</td>

<td>ED1\. I am very confident that the product I have just purchased in the experiment is really the best choice for my purchasing goal given by the direction.</td>

<td rowspan="3">(Pereira 2000, adapted)</td>

</tr>

<tr>

<td>ED2\. I am very satisfied with the item I have chosen.</td>

</tr>

<tr>

<td>ED3\. I would like to recommend this item to other people purchasing digital cameras for a similar purpose.</td>

</tr>

<tr>

<td rowspan="4">Product expertise</td>

<td>PE1\. I am an expert in digital cameras.</td>

<td rowspan="4">(Pereira 2000; Wang and Benbasat 2007, adapted)</td>

</tr>

<tr>

<td>PE2\. I feel very comfortable using digital cameras.</td>

</tr>

<tr>

<td>PE3\. I am very satisfied with my current digital camera skills.</td>

</tr>

<tr>

<td>PE4\. I know very well about digital cameras compared with average users.</td>

</tr>

</tbody>

</table>

### Appendix 2: Reliability and validity of the measurement

<table class="center"><caption>  
Table A2: Factor loadings and cross-loadings</caption>

<tbody>

<tr>

<td> </td>

<th>Intention specificity</th>

<th>Agent-intention fit</th>

<th>Familiarity with agents</th>

<th>Online shopping frequency</th>

<th>Ease of use of agent</th>

<th>Usefulness of agent</th>

<th>Effectiveness of decision</th>

<th>Product expertise</th>

</tr>

<tr>

<td>IS1</td>

<td style="text-align:center">**0.89**</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">0.11</td>

<td style="text-align:center">0.12</td>

<td style="text-align:center">0.27</td>

<td style="text-align:center">0.13</td>

<td style="text-align:center">0.37</td>

<td style="text-align:center">0.34</td>

</tr>

<tr>

<td>IS2</td>

<td style="text-align:center">**0.88**</td>

<td style="text-align:center">0.01</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">0.19</td>

<td style="text-align:center">0.20</td>

<td style="text-align:center">0.12</td>

<td style="text-align:center">0.38</td>

<td style="text-align:center">0.33</td>

</tr>

<tr>

<td>IS3</td>

<td style="text-align:center">**0.88**</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">0.26</td>

<td style="text-align:center">0.19</td>

<td style="text-align:center">0.28</td>

<td style="text-align:center">0.21</td>

<td style="text-align:center">0.44</td>

<td style="text-align:center">0.32</td>

</tr>

<tr>

<td>AIF1</td>

<td style="text-align:center">0.03</td>

<td style="text-align:center">**0.96**</td>

<td style="text-align:center">-0.07</td>

<td style="text-align:center">0.11</td>

<td style="text-align:center">-0.30</td>

<td style="text-align:center">-0.32</td>

<td style="text-align:center">-0.25</td>

<td style="text-align:center">0.02</td>

</tr>

<tr>

<td>AIF2</td>

<td style="text-align:center">0.06</td>

<td style="text-align:center">**0.87**</td>

<td style="text-align:center">-0.06</td>

<td style="text-align:center">0.13</td>

<td style="text-align:center">-0.17</td>

<td style="text-align:center">-0.17</td>

<td style="text-align:center">-0.04</td>

<td style="text-align:center">0.09</td>

</tr>

<tr>

<td>AIF3</td>

<td style="text-align:center">0.04</td>

<td style="text-align:center">**0.95**</td>

<td style="text-align:center">-0.01</td>

<td style="text-align:center">0.15</td>

<td style="text-align:center">-0.23</td>

<td style="text-align:center">-0.25</td>

<td style="text-align:center">-0.20</td>

<td style="text-align:center">0.07</td>

</tr>

<tr>

<td>FA1</td>

<td style="text-align:center">0.28</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">**0.81**</td>

<td style="text-align:center">0.36</td>

<td style="text-align:center">0.17</td>

<td style="text-align:center">0.21</td>

<td style="text-align:center">0.26</td>

<td style="text-align:center">0.30</td>

</tr>

<tr>

<td>FA2</td>

<td style="text-align:center">0.13</td>

<td style="text-align:center">-0.08</td>

<td style="text-align:center">**0.95**</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">0.30</td>

<td style="text-align:center">0.38</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">0.23</td>

</tr>

<tr>

<td>FA3</td>

<td style="text-align:center">0.11</td>

<td style="text-align:center">-0.08</td>

<td style="text-align:center">**0.93**</td>

<td style="text-align:center">0.27</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">0.31</td>

<td style="text-align:center">0.18</td>

<td style="text-align:center">0.20</td>

</tr>

<tr>

<td>OSF</td>

<td style="text-align:center">0.19</td>

<td style="text-align:center">0.14</td>

<td style="text-align:center">0.31</td>

<td style="text-align:center">**1.00**</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">-0.02</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">0.36</td>

</tr>

<tr>

<td>EUA1</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">-0.26</td>

<td style="text-align:center">0.26</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">**0.90**</td>

<td style="text-align:center">0.74</td>

<td style="text-align:center">0.56</td>

<td style="text-align:center">0.12</td>

</tr>

<tr>

<td>EUA2</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">-0.30</td>

<td style="text-align:center">0.22</td>

<td style="text-align:center">-0.06</td>

<td style="text-align:center">**0.90**</td>

<td style="text-align:center">0.72</td>

<td style="text-align:center">0.62</td>

<td style="text-align:center">0.15</td>

</tr>

<tr>

<td>EUA3</td>

<td style="text-align:center">0.24</td>

<td style="text-align:center">-0.18</td>

<td style="text-align:center">0.29</td>

<td style="text-align:center">-0.04</td>

<td style="text-align:center">**0.86**</td>

<td style="text-align:center">0.70</td>

<td style="text-align:center">0.52</td>

<td style="text-align:center">0.08</td>

</tr>

<tr>

<td>EUA4</td>

<td style="text-align:center">0.27</td>

<td style="text-align:center">-0.21</td>

<td style="text-align:center">0.21</td>

<td style="text-align:center">0.00</td>

<td style="text-align:center">**0.87**</td>

<td style="text-align:center">0.70</td>

<td style="text-align:center">0.57</td>

<td style="text-align:center">0.23</td>

</tr>

<tr>

<td>UA1</td>

<td style="text-align:center">0.12</td>

<td style="text-align:center">-0.25</td>

<td style="text-align:center">0.29</td>

<td style="text-align:center">-0.04</td>

<td style="text-align:center">0.69</td>

<td style="text-align:center">**0.91**</td>

<td style="text-align:center">0.41</td>

<td style="text-align:center">0.02</td>

</tr>

<tr>

<td>UA2</td>

<td style="text-align:center">0.13</td>

<td style="text-align:center">-0.25</td>

<td style="text-align:center">0.34</td>

<td style="text-align:center">0.00</td>

<td style="text-align:center">0.73</td>

<td style="text-align:center">**0.94**</td>

<td style="text-align:center">0.50</td>

<td style="text-align:center">0.02</td>

</tr>

<tr>

<td>UA3</td>

<td style="text-align:center">0.24</td>

<td style="text-align:center">-0.26</td>

<td style="text-align:center">0.32</td>

<td style="text-align:center">-0.01</td>

<td style="text-align:center">0.78</td>

<td style="text-align:center">**0.90**</td>

<td style="text-align:center">0.56</td>

<td style="text-align:center">0.10</td>

</tr>

<tr>

<td>ED1</td>

<td style="text-align:center">0.43</td>

<td style="text-align:center">-0.15</td>

<td style="text-align:center">0.22</td>

<td style="text-align:center">0.08</td>

<td style="text-align:center">0.59</td>

<td style="text-align:center">0.45</td>

<td style="text-align:center">**0.93**</td>

<td style="text-align:center">0.21</td>

</tr>

<tr>

<td>ED2</td>

<td style="text-align:center">0.40</td>

<td style="text-align:center">-0.23</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">0.04</td>

<td style="text-align:center">0.62</td>

<td style="text-align:center">0.55</td>

<td style="text-align:center">**0.96**</td>

<td style="text-align:center">0.19</td>

</tr>

<tr>

<td>ED3</td>

<td style="text-align:center">0.46</td>

<td style="text-align:center">-0.18</td>

<td style="text-align:center">0.24</td>

<td style="text-align:center">0.01</td>

<td style="text-align:center">0.60</td>

<td style="text-align:center">0.52</td>

<td style="text-align:center">**0.93**</td>

<td style="text-align:center">0.23</td>

</tr>

<tr>

<td>PE1</td>

<td style="text-align:center">0.38</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">0.21</td>

<td style="text-align:center">0.31</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">-0.01</td>

<td style="text-align:center">0.18</td>

<td style="text-align:center">**0.83**</td>

</tr>

<tr>

<td>PE2</td>

<td style="text-align:center">0.27</td>

<td style="text-align:center">0.09</td>

<td style="text-align:center">0.23</td>

<td style="text-align:center">0.34</td>

<td style="text-align:center">0.18</td>

<td style="text-align:center">0.08</td>

<td style="text-align:center">0.19</td>

<td style="text-align:center">**0.90**</td>

</tr>

<tr>

<td>PE3</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">0.04</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">0.23</td>

<td style="text-align:center">0.19</td>

<td style="text-align:center">0.08</td>

<td style="text-align:center">0.20</td>

<td style="text-align:center">**0.85**</td>

</tr>

<tr>

<td>PE4</td>

<td style="text-align:center">0.42</td>

<td style="text-align:center">0.02</td>

<td style="text-align:center">0.22</td>

<td style="text-align:center">0.37</td>

<td style="text-align:center">0.15</td>

<td style="text-align:center">0.03</td>

<td style="text-align:center">0.21</td>

<td style="text-align:center">**0.91**</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table A3: Construct description and correlation relationships <details><summary>Note</summary> The diagonal figures of correlation matrix indicate the square root of AVE. Other figures off diagonal indicate correlation between constructs.</details></caption>

<tbody>

<tr>

<td rowspan="2"> </td>

<td rowspan="2"> </td>

<th rowspan="2">Mean</th>

<th rowspan="2">S.D</th>

<th rowspan="2">Cronbach's alpha</th>

<th rowspan="2">Composite reliability</th>

<th rowspan="2">AVE</th>

<th colspan="8">Correlation matrix</th>

</tr>

<tr>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">4</td>

<td style="text-align:center">5</td>

<td style="text-align:center">6</td>

<td style="text-align:center">7</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td style="text-align:center">1</td>

<td>Intention specificity</td>

<td style="text-align:center">4.73</td>

<td style="text-align:center">1.49</td>

<td style="text-align:center">0.86</td>

<td style="text-align:center">0.91</td>

<td style="text-align:center">0.78</td>

<td style="text-align:center">0.88</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td style="text-align:center">2</td>

<td>Agent-intention fit</td>

<td style="text-align:center">4.14</td>

<td style="text-align:center">2.09</td>

<td style="text-align:center">0.92</td>

<td style="text-align:center">0.95</td>

<td style="text-align:center">0.86</td>

<td style="text-align:center">0.04</td>

<td style="text-align:center">0.92</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td style="text-align:center">3</td>

<td>Familiarity with agents</td>

<td style="text-align:center">4.40</td>

<td style="text-align:center">1.46</td>

<td style="text-align:center">0.88</td>

<td style="text-align:center">0.93</td>

<td style="text-align:center">0.81</td>

<td style="text-align:center">0.17</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">0.90</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td style="text-align:center">4</td>

<td style="text-align:center">Online shopping frequency</td>

<td style="text-align:center">2.88</td>

<td style="text-align:center">1.32</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0.19</td>

<td style="text-align:center">0.14</td>

<td style="text-align:center">0.31</td>

<td style="text-align:center">1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td style="text-align:center">5</td>

<td style="text-align:center">Ease of use of agent</td>

<td style="text-align:center">5.39</td>

<td style="text-align:center">1.29</td>

<td style="text-align:center">0.91</td>

<td style="text-align:center">0.94</td>

<td style="text-align:center">0.78</td>

<td style="text-align:center">0.29</td>

<td style="text-align:center">-0.27</td>

<td style="text-align:center">0.28</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">0.88</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td style="text-align:center">6</td>

<td style="text-align:center">Usefulness of agent</td>

<td style="text-align:center">5.34</td>

<td style="text-align:center">1.48</td>

<td style="text-align:center">0.90</td>

<td style="text-align:center">0.94</td>

<td style="text-align:center">0.83</td>

<td style="text-align:center">0.18</td>

<td style="text-align:center">-0.28</td>

<td style="text-align:center">0.35</td>

<td style="text-align:center">-0.02</td>

<td style="text-align:center">0.81</td>

<td style="text-align:center">0.91</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td style="text-align:center">7</td>

<td style="text-align:center">Effectiveness of decision</td>

<td style="text-align:center">5.48</td>

<td style="text-align:center">1.30</td>

<td style="text-align:center">0.93</td>

<td style="text-align:center">0.96</td>

<td style="text-align:center">0.88</td>

<td style="text-align:center">0.46</td>

<td style="text-align:center">-0.20</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">0.64</td>

<td style="text-align:center">0.54</td>

<td style="text-align:center">0.94</td>

<td> </td>

</tr>

<tr>

<td style="text-align:center">8</td>

<td style="text-align:center">Product Expertise</td>

<td style="text-align:center">4.08</td>

<td style="text-align:center">1.08</td>

<td style="text-align:center">0.90</td>

<td style="text-align:center">0.93</td>

<td style="text-align:center">0.77</td>

<td style="text-align:center">0.38</td>

<td style="text-align:center">0.06</td>

<td style="text-align:center">0.26</td>

<td style="text-align:center">0.36</td>

<td style="text-align:center">0.16</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">0.22</td>

<td style="text-align:center">0.87</td>

</tr>

</tbody>

</table>
