#### vol. 21 no. 3, September, 2016

# Simultaneous and comparable numerical indicators of international, national and local collaboration practices in English-medium astrophysics research papers

## [David I. Méndez](#author) and [M. Ángeles Alcaraz](#author)

#### Abstract

**Introduction.** We report an investigation on collaboration practices in research papers published in the most prestigious English-medium astrophysics journals.  
**Method.** We propose an evaluation method based on three numerical indicators to study and compare, in absolute terms, three different types of collaboration (international, national and local) and authors' mobility on the basis of co-authorship.  
**Analysis.**We analysed 300 randomly selected research papers in three different time periods and used the student's t-test to determine whether the paired two-sample differences observed were statistically significant or not.  
**Results.** International collaboration is more common than national and local collaboration. International, national and local authors' mobility and intra-national collaboration do not seriously affect the indicators of the principal levels of collaboration. International collaboration and authors' mobility are more relevant for authors publishing in European journals, whereas national and intra-national collaboration and national mobility are more important for authors publishing in US journals.  
**Conclusions.** We explain the observed differences and patterns in terms of the specific scope of each journal and the socio-economic and political situation in both geographic contexts (Europe and the USA). Our study provides a global picture of collaboration practices in astrophysics and its possible application to many other sciences and fields would undoubtedly help bring into focus the really big issues for overall research management and policy.

## Introduction

Scientific collaboration, understood as '_a means to advance research_' ([Pao, 1992](#pao92), p.99), has existed since the beginning of science and it consists of the specific scientific activities (observation, data collection, experimentation, analysis, interpretation and publication) performed by scientists working together on a common research project. Due to its importance, scientific collaboration and its changing patterns have been extensively studied from sociological and bibliometric standpoints, as well as in studies of research policy and research ethics ([Beaver, 2001](#beaver2001); [Birnholtz, 2006](#birn2006); [Chompalov _et al._, 2002](#chompalov2002); [Cronin, 2012](#cronin2012); [Glänzel and Schubert, 2004](#glan2004); [Harsanyi, 1993](#harsan93); [Katz and Hicks, 1997](#katzhicks97); [Leyesdorff and Wagner, 2008](#leydesdorff2008); [Marshakova-Shaikevich, 2006](#marshakova2006); [Wagner _et al._, 2001](#wagner2001); [Wuchty _et al._, 2007](#wuchty2007); among others). It has been observed that in broader disciplines such as sciences, there is a trend towards increasing collaboration in general (Glänzel, 2002) and international collaboration in particular ([Wagner and Leydesdorff, 2005](#wagner2005)).

The growth of scientific collaboration has now become an area of interest to researchers to the point that it was the topic of a conference held at the University of Valencia (Spain) in November 2013 ([González Alcaide _et al._, 2013](#gonz2013)). An international network, the aim of which is to facilitate collaboration in scientometrics, infometrics and webometrics, should also be mentioned here: it is the [COLLNET network](http://www.collnet.de), which has organised an annual international meeting since the year 2000.

Scientists are members not only of local and national, but also international scientific communities ([Crane, 1972](#crane72); [de Solla Price, 1986](#desolla86); [Schott, 1991](#schott91)). These different levels of collaboration are sometimes difficult to evaluate since many factors have to be taken into account, i.e., it is not always easy to decide what is collaboration and what is not ([Katz and Martin, 1997](#katzmartin97); [Laudel, 2002](#laudel2002)). Fortunately this difficulty has not prevented many authors from trying to measure collaboration, mainly international collaboration, by using co-authorship networks as the standard way to reach comparable numerical results ([Glänzel, 2002](#glan2002); [Glänzel and Schubert, 2004](#glan2004); [Wagner and Leyesdorff, 2005](#wagner2005), to name just a few).

With the exception of a few studies which considered that the different types of collaboration were not exclusive (for example, [Bordons Gangas _et al._, 2013](#bordons2013)), the vast majority of studies have considered them from an exclusive standpoint by measuring the different levels of collaboration separately (e.g. [Leimu and Koricheva, 2005](#leimu2005); [Sin, 2011](#sin2011)) without taking into account the fact that in global and multidisciplinary sciences they usually occur simultaneously. This is the case for astrophysics, a discipline with a dual nature in the sense that it combines astronomy, an observational science related to the description and the classification of the universe, and physics, which is concerned with the basic properties of celestial objects ([Pedersen, 1993](#pedersen1993)). Moreover, physics not only incorporates theory and practice ([Newman, 2004](#newman2004)), but also separate branches of expertise, each one with its own characteristics: high-energy physics, particle physics, relativistic physics, solid-state physics, biology, chemistry, aerospace, electrical and mechanical engineering, mathematics, etc. This is why we think that astrophysics is the perfect touchstone to sketch an overall picture of the complex collaboration scenarios involved in scientific research. An inclusive study may also help find the most noteworthy issues so as to modify collaboration practices (if required or desired) within the scope of research management and policy.

To sum up, although the literature on collaboration has generally paid great attention to detail, it seems that the following global question has been left unaddressed (1): is it possible to compare numerically and simultaneously the different levels of collaboration in a given corpus of research papers, or even in a single research paper? In order to answer this question, it is necessary to look at the issue from a less detailed point of view, i.e., to approach it from a more global standpoint that does not focus specifically on collaboration networks or links between researchers. In our opinion, what is needed is the establishment of different indicators that may allow a simultaneous measure of the main types of collaboration (international, national and local) in any given corpus. The referred indicators are introduced in the _methods_ section.

After computing simultaneously the different types of collaboration, further interesting questions such as these may be addressed:

(2) Are there any time variations in collaboration patterns in astrophysics as already reported in other fields?  
(3) Do collaboration patterns depend on the scope of the journals selected for our corpus?  
(4) What conditions (economic, social, etc.) are responsible for the variations in collaboration patterns, if any?

The best approach to answering these questions is the analysis of different time periods and journals.

## Methods

First, we recorded the different countries, cities and institutions mentioned in the bylines of all the research papers included in our corpus. The numerical indicators that we propose to measure the different types of collaboration (international, national and local) in our whole corpus or in a single research paper are as follows:

*   International collaboration is studied in terms of the mean number of countries per research paper.
*   National collaboration is studied in terms of the mean number of cities per each country. The numerical indicator refers to the mean quotient between the number of cities and the number of countries per research paper, i.e., it corresponds to the average of the individual national collaboration index for every country included in the bylines of each research paper.
*   Local collaboration is studied in terms of the mean number of institutions per city. The numerical indicator refers to the mean quotient between the number of institutions and the number of cities per research paper, i.e., it corresponds to the average of the individual local collaboration index for every city included in the bylines of each research paper.

A clarification is in order here: whenever a given country, city or institution is indicated more than once in the bylines of a single research paper, we counted them as a unique item; by contrast, whenever the same country, city or institution is indicated in the bylines of different research papers, we counted them as different items. The very definition of any of the three numerical indicators implies that a value of '1' corresponds to the absence of collaboration (international, national and local). The way of computing our collaboration indicators leads to two further research questions:

(5) Are the proposed numerical indicators affected by authors' mobility?  
(6) Are the proposed numerical indicators affected by intra-national collaboration, i.e., collaboration among given institutions with different branches located in different cities of the same country?

To answer both questions, it is necessary to separately study the different types of mobility (international, national and local) together with intra-national collaboration. International mobility refers to authors working in different countries at the same time, national mobility to authors working in different cities in the same country and local mobility to authors working at different institutions, mainly universities, in the same city. Since it may happen that on some occasions researchers only list an affiliation with the host institution, our mobility data must be considered as lower estimates of the actual values. Another level of collaboration that is also worth studying refers to intra-institutional collaboration, i.e., collaboration among different departments in the same institution.

Moreover, although the three proposed numerical indicators are always sample-affected, it is worth stressing that national and local collaboration indicators are doubly affected by the sample. While the international collaboration indicator only considers a single variable (the number of countries per research paper), the national and local collaboration indicators include two different variables (the number of cities divided by the number of countries per research paper in the case of national collaboration, and the number of institutions divided by the number of cities per research paper in the case of local collaboration).

Finally, so as to determine whether the paired two-sample differences observed are statistically significant or not, we analysed our data by means of the student's t-test. The alpha value has been set at 0.05.

## The corpus

Journal citation impact and prestige were taken into account in the journal selection process. The study incorporated three selection criteria: journals must (1) have the highest impact factors; (2) publish papers on observational data and/or theoretical analyses; (3) be freely accessible online. Four journals were found to meet the three criteria and were selected for this study: two European journals, _Astronomy and Astrophysics_ and _Monthly Notices of the Royal Astronomical Society_ (RAS); and two US-based journals, _The Astronomical Journal_ and _The Astrophysical Journal_. _Astronomy and Astrophysics_ (impact factor: 5.084) publishes papers on theoretical, observational, and instrumental astronomy and astrophysics, and is published by Édition Diffusion Presse (EDP) Sciences. _Monthly Notices of the RAS_ (impact factor: 5.521) covers research on astronomy and astrophysics. This journal is published on behalf of the Royal Astronomical Society and is often preferred by astronomers from the United Kingdom and the Commonwealth. _The Astronomical Journal_ (impact factor: 4.965) publishes papers on astronomical research, while _The Astrophysical Journal_ (impact factor: 6.733) has a more global focus and publishes papers in astronomy and astrophysics. Both journals are published on behalf of the American Astronomical Society. All impact factors refer to the year 2012 (the information was obtained from each journal's home page).

Since 1998 was the year when free online access began for the four journals, we chose that year as the starting point of our analysis. We randomly selected 300 research papers from three different time periods comprising 100 research papers each: Block A (1998), Block B (2004), and Block C (2012). In other words, the 100 research papers per block comprise 25 research papers per journal, i.e., a total of 75 research papers per journal. We then manually recorded the authors' institutional affiliation data mentioned in each of the 300 research paper bylines.

## Results

### International collaboration

[Table 1](#table1) displays the total number of country items recorded in all the research paper bylines per journal and time period. It also includes the effect of international mobility (see [Table 6](#table6) ).

<table id="table1" class="center"><caption>  
Table 1: Number of country items per journal and time period  
</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Country items recorded</th>

</tr>

<tr>

<th>Journal</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">Total</th>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">51</td>

<td style="text-align:center">53</td>

<td style="text-align:center">66</td>

<td style="text-align:center">170</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">48</td>

<td style="text-align:center">47</td>

<td style="text-align:center">73</td>

<td style="text-align:center">168</td>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">41</td>

<td style="text-align:center">52</td>

<td style="text-align:center">45</td>

<td style="text-align:center">138</td>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">38</td>

<td style="text-align:center">41</td>

<td style="text-align:center">45</td>

<td style="text-align:center">124</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">178</td>

<td style="text-align:center">193</td>

<td style="text-align:center">229</td>

<td style="text-align:center">600</td>

</tr>

</tbody>

</table>

As can be seen in [Table 1](#table1), 600 country items were recorded in the whole corpus. The highest numbers of country items are found in the European journals _Monthly Notices of the RAS_ (28.3%) and _Astronomy and Astrophysics_ (28%), whereas the US-based journals _The Astronomical Journal_ (23%) and _The Astrophysical Journal_ (20.7%) contain the lowest figures. The mean number of country items per research paper in the whole sample is 2\. From a general diachronic standpoint, the mean number of country items per research paper increases from 1.8 in Block A to 2.3 in Block C (p=0.003). This is clearly plotted in [Figure 1](#Figure1).

<figure id="figure1" class="centre">![Figure 1: Mean number of country items per journal and time period](p719fig1.jpg)

<figcaption>Figure 1: Mean number of country items per journal and time period</figcaption>

</figure>

[Table 1](#table1) and [Figure 1](#figure1) also show that the numbers of country items in _Monthly Notices of the RAS_ and _The Astrophysical Journal_ exhibit rising trends from Block A to Block C, although this increase is not statistically significant. By contrast, _The Astronomical Journal_ and _Astronomy and Astrophysics_ show the same erratic behaviour already reported in a study on the number of authors per research paper (see [Méndez _et al._, 2014b](#mendez2014b)), i.e., up and down in _The Astronomical Journal_ and the reverse in _Astronomy and Astrophysics_. The difference in the mean number of country items per research paper between Block A (1.9) and Block C (2.9) is only statistically significant in _Astronomy and Astrophysics_ (p=0.017).

[Table 2](#table2) shows the different countries mentioned in the research paper bylines and the number of times they are mentioned (ordered by number of mentions).

<table id="table2" class="center" style="width:95%;"><caption>  
Table 2: Number of country appearances per journal</caption>

<tbody>

<tr>

<th> </th>

<th colspan="5">Number of times a country is mentioned in each sample</th>

</tr>

<tr>

<th>Country</th>

<th style="text-align:center">_Monthly Notices of the RAS_</th>

<th style="text-align:center">_Astronomy and Astrophysics_</th>

<th style="text-align:center">_The Astronomical Journal_</th>

<th style="text-align:center">_The Astrophysical Journal_</th>

<th style="text-align:center">TOTAL</th>

</tr>

<tr>

<td>USA</td>

<td style="text-align:center">23</td>

<td style="text-align:center">19</td>

<td style="text-align:center">61</td>

<td style="text-align:center">51</td>

<td style="text-align:center">154</td>

</tr>

<tr>

<td>UK</td>

<td style="text-align:center">36</td>

<td style="text-align:center">8</td>

<td style="text-align:center">4</td>

<td style="text-align:center">12</td>

<td style="text-align:center">60</td>

</tr>

<tr>

<td>Germany</td>

<td style="text-align:center">17</td>

<td style="text-align:center">29</td>

<td style="text-align:center">10</td>

<td style="text-align:center">2</td>

<td style="text-align:center">58</td>

</tr>

<tr>

<td>France</td>

<td style="text-align:center">11</td>

<td style="text-align:center">25</td>

<td style="text-align:center">2</td>

<td style="text-align:center">5</td>

<td style="text-align:center">43</td>

</tr>

<tr>

<td>Australia</td>

<td style="text-align:center">8</td>

<td style="text-align:center">4</td>

<td style="text-align:center">14</td>

<td style="text-align:center">5</td>

<td style="text-align:center">31</td>

</tr>

<tr>

<td>Canada</td>

<td style="text-align:center">6</td>

<td style="text-align:center">5</td>

<td style="text-align:center">12</td>

<td style="text-align:center">8</td>

<td style="text-align:center">31</td>

</tr>

<tr>

<td>Italy</td>

<td style="text-align:center">9</td>

<td style="text-align:center">10</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

<td style="text-align:center">25</td>

</tr>

<tr>

<td>Japan</td>

<td style="text-align:center">5</td>

<td style="text-align:center">4</td>

<td style="text-align:center">7</td>

<td style="text-align:center">8</td>

<td style="text-align:center">24</td>

</tr>

<tr>

<td>Spain</td>

<td style="text-align:center">5</td>

<td style="text-align:center">12</td>

<td style="text-align:center">2</td>

<td style="text-align:center">5</td>

<td style="text-align:center">24</td>

</tr>

<tr>

<td>Chile</td>

<td style="text-align:center">3</td>

<td style="text-align:center">4</td>

<td style="text-align:center">8</td>

<td style="text-align:center">5</td>

<td style="text-align:center">20</td>

</tr>

<tr>

<td>The Netherlands</td>

<td style="text-align:center">3</td>

<td style="text-align:center">8</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

<td style="text-align:center">15</td>

</tr>

<tr>

<td>Russia</td>

<td style="text-align:center">5</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

<td style="text-align:center">11</td>

</tr>

<tr>

<td>Switzerland</td>

<td style="text-align:center">5</td>

<td style="text-align:center">6</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">11</td>

</tr>

<tr>

<td>Sweeden</td>

<td style="text-align:center">3</td>

<td style="text-align:center">5</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">9</td>

</tr>

<tr>

<td>China</td>

<td style="text-align:center">4</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

<td style="text-align:center">-</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>Mexico</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>Poland</td>

<td style="text-align:center">3</td>

<td style="text-align:center">4</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>South Korea</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>Brazil</td>

<td style="text-align:center">-</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Denmark</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Argentina</td>

<td style="text-align:center">3</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Belgium</td>

<td style="text-align:center">-</td>

<td style="text-align:center">4</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Finland</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>India</td>

<td style="text-align:center">3</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Ireland</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Israel</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Austria</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>Portugal</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Ukraine</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Colombia</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Croatia</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Greece</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Iran</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Norway</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Slovenia</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>South Africa</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Taiwan</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Venezuela</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">170</td>

<td style="text-align:center">168</td>

<td style="text-align:center">138</td>

<td style="text-align:center">124</td>

<td style="text-align:center">600</td>

</tr>

</tbody>

</table>

The total number of different countries mentioned in all the research paper bylines is 38\. The countries most frequently involved in the research are USA, UK, Germany, France, Australia and Canada. It is worth pointing out that of the 154 research paper bylines in which the USA is represented, 112 correspond to the American journals, against only forty-two occurrences in the European ones. The UK is more frequently mentioned in _Monthly Notices of the RAS_ and in _The Astrophysical Journal_, whereas France and Germany are much more frequent in _Astronomy and Astrophysics_ and _Monthly Notices of the RAS_. Moreover, Australia and Canada are more often cited in the American journals than in their European counterparts. Altogether, _The Astronomical Journal_, _The Astrophysical Journal_ and _Monthly Notices of the RAS_ (journals published in English-speaking countries), mention Australia, Canada, UK and USA (where English is the official language) on 240 occasions, whereas the same countries are represented on only thirty-six occasions in _Astronomy and Astrophysics_ (a journal published in a non-English speaking country).

[Table 3](#table3) shows that the number of different countries mentioned in the research paper bylines in the whole corpus varies from one journal to another, _Monthly Notices of the RAS_ containing the highest number of countries (31) and _The Astronomical Journal_ the lowest (19).

<table id="table3" class="center" style="width:95%;"><caption>  
Table 3: Country variations between journals</caption>

<tbody>

<tr>

<th>Journal</th>

<th style="text-align:center">Number of different countries mentioned</th>

<th style="text-align:center">Research papers with one single country</th>

<th style="text-align:center">Research papers with two countries</th>

<th style="text-align:center">Research papers with three countries</th>

<th style="text-align:center">Research papers with four or more countries</th>

<th style="text-align:center">Highest number of country appearances in one single research paper</th>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">31</td>

<td style="text-align:center">24</td>

<td style="text-align:center">24</td>

<td style="text-align:center">17</td>

<td style="text-align:center">10</td>

<td style="text-align:center">7</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">30</td>

<td style="text-align:center">26</td>

<td style="text-align:center">24</td>

<td style="text-align:center">15</td>

<td style="text-align:center">10</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">22</td>

<td style="text-align:center">36</td>

<td style="text-align:center">22</td>

<td style="text-align:center">8</td>

<td style="text-align:center">9</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">19</td>

<td style="text-align:center">37</td>

<td style="text-align:center">22</td>

<td style="text-align:center">10</td>

<td style="text-align:center">6</td>

<td style="text-align:center">5</td>

</tr>

</tbody>

</table>

[Table 3](#table3) also shows that the two American journals include more research papers with one single country (73). Conversely, the two European journals contain more research papers with two, three or more countries (100). Of the 37 research papers with one single country in _The Astronomical Journal_, it is interesting to note that on 26 occasions this country is the USA. In the case of the 36 research papers with one single country in _The Astrophysical Journal_, the presence of the USA as a top contributing country is also particularly notable as it is mentioned on 25 occasions. The highest numbers of countries in one single research paper are found in _Astronomy and Astrophysics_ and _The Astrophysical Journal_ (eight countries each).

[Table 4](#table4) illustrates that over time some countries do not appear in certain blocks. Austria, Finland, India and Portugal do not appear in Block A. Colombia, Croatia, Greece, Slovenia and Taiwan are only present in Block C. Argentina, Ukraine, Iran and South Africa do not appear in Block C, the latter two are not present in Block A either. Israel, Norway and Venezuela are not present in Block B, the latter two are not present in Block C either.

<table id="table4" class="center"><caption>  
Table 4: Number of country appearances by time period</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Number of times a country is mentioned in each sample</th>

</tr>

<tr>

<th>Country</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">TOTAL</th>

</tr>

<tr>

<td>USA</td>

<td style="text-align:center">50</td>

<td style="text-align:center">54</td>

<td style="text-align:center">50</td>

<td style="text-align:center">154</td>

</tr>

<tr>

<td>UK</td>

<td style="text-align:center">17</td>

<td style="text-align:center">24</td>

<td style="text-align:center">19</td>

<td style="text-align:center">60</td>

</tr>

<tr>

<td>Germany</td>

<td style="text-align:center">20</td>

<td style="text-align:center">21</td>

<td style="text-align:center">17</td>

<td style="text-align:center">58</td>

</tr>

<tr>

<td>France</td>

<td style="text-align:center">11</td>

<td style="text-align:center">15</td>

<td style="text-align:center">17</td>

<td style="text-align:center">43</td>

</tr>

<tr>

<td>Australia</td>

<td style="text-align:center">12</td>

<td style="text-align:center">8</td>

<td style="text-align:center">11</td>

<td style="text-align:center">31</td>

</tr>

<tr>

<td>Canada</td>

<td style="text-align:center">7</td>

<td style="text-align:center">4</td>

<td style="text-align:center">20</td>

<td style="text-align:center">31</td>

</tr>

<tr>

<td>Italy</td>

<td style="text-align:center">7</td>

<td style="text-align:center">8</td>

<td style="text-align:center">10</td>

<td style="text-align:center">25</td>

</tr>

<tr>

<td>Japan</td>

<td style="text-align:center">5</td>

<td style="text-align:center">7</td>

<td style="text-align:center">12</td>

<td style="text-align:center">24</td>

</tr>

<tr>

<td>Spain</td>

<td style="text-align:center">6</td>

<td style="text-align:center">7</td>

<td style="text-align:center">11</td>

<td style="text-align:center">24</td>

</tr>

<tr>

<td>Chile</td>

<td style="text-align:center">5</td>

<td style="text-align:center">6</td>

<td style="text-align:center">9</td>

<td style="text-align:center">20</td>

</tr>

<tr>

<td>The Netherlands</td>

<td style="text-align:center">6</td>

<td style="text-align:center">5</td>

<td style="text-align:center">4</td>

<td style="text-align:center">15</td>

</tr>

<tr>

<td>Russia</td>

<td style="text-align:center">1</td>

<td style="text-align:center">5</td>

<td style="text-align:center">5</td>

<td style="text-align:center">11</td>

</tr>

<tr>

<td>Switzerland</td>

<td style="text-align:center">3</td>

<td style="text-align:center">2</td>

<td style="text-align:center">6</td>

<td style="text-align:center">11</td>

</tr>

<tr>

<td>Sweeden</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">5</td>

<td style="text-align:center">9</td>

</tr>

<tr>

<td>China</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

<td style="text-align:center">4</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>Mexico</td>

<td style="text-align:center">4</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>Poland</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

<td style="text-align:center">2</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>South Korea</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">5</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>Brazil</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Denmark</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Argentina</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Belgium</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Finland</td>

<td style="text-align:center">-</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>India</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Ireland</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Israel</td>

<td style="text-align:center">2</td>

<td style="text-align:center">-</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Austria</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>Portugal</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Ukraine</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Colombia</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Croatia</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Greece</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Iran</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Norway</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Slovenia</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>South Africa</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Taiwan</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Venezuela</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">178</td>

<td style="text-align:center">193</td>

<td style="text-align:center">229</td>

<td style="text-align:center">600</td>

</tr>

</tbody>

</table>

[Table 5](#table5) shows that the range of countries represented in the research paper bylines steadily increases from Block A (27) to Block C (32). Likewise, there are more research papers involving four or more countries in Block C (13) than in Block A (7). The maximum number of countries in one single research paper also increases from Block A (4) to Block C (8). Conversely, the number of research papers with two countries is characterised by a downward trend, from 34 in Block A to 26 in Block C.

<table id="table5" class="center"><caption>  
Table 5: Country variation by time period</caption>

<tbody>

<tr>

<th>Period</th>

<th style="text-align:center">Number of different countries mentioned</th>

<th style="text-align:center">Research papers with a single country</th>

<th style="text-align:center">Research papers with two countries</th>

<th style="text-align:center">Research papers with three countries</th>

<th style="text-align:center">Research papers with four or more countries</th>

<th style="text-align:center">Highest number of country appearances in a single research paper</th>

</tr>

<tr>

<td>Block A</td>

<td style="text-align:center">27</td>

<td style="text-align:center">44</td>

<td style="text-align:center">34</td>

<td style="text-align:center">15</td>

<td style="text-align:center">7</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Block B</td>

<td style="text-align:center">30</td>

<td style="text-align:center">42</td>

<td style="text-align:center">32</td>

<td style="text-align:center">15</td>

<td style="text-align:center">11</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Block C</td>

<td style="text-align:center">32</td>

<td style="text-align:center">41</td>

<td style="text-align:center">26</td>

<td style="text-align:center">20</td>

<td style="text-align:center">13</td>

<td style="text-align:center">8</td>

</tr>

</tbody>

</table>

Of the 44 research papers with a single country in Block A, the USA is mentioned on 24 occasions, while the UK and Germany are mentioned on 4 occasions each. Of the 42 research papers with a single country in Block B, the USA is mentioned on 16 occasions and the UK on 4 occasions. Of the 41 research papers with a single country in Block C, the USA appears on 14 occasions and the UK on merely 2 occasions. These data suggest that international collaboration tends to increase over time in the USA and the UK.

### International mobility

[Table 6](#table6) displays that only 96 authors (6.7% of the total number of authors found in our sample) are working in different countries at the same time. Such a low percentage indicates that our international collaboration indicator is not seriously affected by international mobility. Authors publishing in _Monthly Notices of the RAS_ exhibit the highest level of international mobility (9.9%), followed by _Astronomy and Astrophysics_ (7.2%), _The Astronomical Journal_ (5%), and _The Astrophysical Journal_ (4.9%). In other words, international mobility is greater among researchers who publish in the two European journals (8.4%) than in those who publish in the two American journals (5%).

<table id="table6" class="center"><caption>  
Table 6: Number of authors with international mobility per journal and time period</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Number of authors with international mobility</th>

</tr>

<tr>

<th>Journal</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">Total</th>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">7</td>

<td style="text-align:center">3</td>

<td style="text-align:center">24</td>

<td style="text-align:center">34</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">7</td>

<td style="text-align:center">8</td>

<td style="text-align:center">12</td>

<td style="text-align:center">27</td>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">9</td>

<td style="text-align:center">5</td>

<td style="text-align:center">5</td>

<td style="text-align:center">19</td>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">9</td>

<td style="text-align:center">0</td>

<td style="text-align:center">7</td>

<td style="text-align:center">16</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">32</td>

<td style="text-align:center">16</td>

<td style="text-align:center">48</td>

<td style="text-align:center">96</td>

</tr>

</tbody>

</table>

*******************

Diachronically speaking, the international mobility patterns for the four journals together show an erratic pattern: a decline from Block A (8.4%) to Block B (3.8%) and a rise in Block C (7.7%). Percentage-wise, _Monthly Notices of the RAS_, _The Astronomical Journal_ and _The Astrophysical Journal_ show a down and up trend, whereas _Astronomy and Astrophysics_ shows the opposite behaviour. It is important to stress that all the percentages given for the different types of mobility have been calculated by taking into account the data about the number of authors per journal and time period previously reported by Méndez _et al._ ([2014b](#mendez2014a)).

### National collaboration

The estimation of national collaboration is based not only on the number of country items ([Table 1](#table1)), but also on the number of cities ([Table 7](#table7)), which in turn includes the effect of national mobility ([Table 8](#table8)) and intra-national collaboration ([Table 9](#table9)).

[Table 7](#table7) shows that the total number of cities rises steadily from Block A (27.5%) to Block C (41.7%). This increase is in line with that observed in the number of authors (see [Méndez _et al._, 2014b](#mendez2014b)) and of country items ([Table 1](#table1)) per journal and time period. In _The Astronomical Journal_ we find an up and down pattern, whereas the opposite pattern is noted in _The Astrophysical Journal_, _Astronomy and Astrophysics_ and _Monthly Notices of the RAS_.

<table id="table7" class="center"><caption>  
Table 7: Number of cities per journal and time period</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Number of cities</th>

</tr>

<tr>

<th>Journal</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">Total</th>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">69</td>

<td style="text-align:center">107</td>

<td style="text-align:center">74</td>

<td style="text-align:center">250</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">60</td>

<td style="text-align:center">58</td>

<td style="text-align:center">121</td>

<td style="text-align:center">239</td>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">66</td>

<td style="text-align:center">64</td>

<td style="text-align:center">102</td>

<td style="text-align:center">232</td>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">62</td>

<td style="text-align:center">59</td>

<td style="text-align:center">92</td>

<td style="text-align:center">213</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">257</td>

<td style="text-align:center">288</td>

<td style="text-align:center">389</td>

<td style="text-align:center">934</td>

</tr>

</tbody>

</table>

Furthermore, [Figure 2](#figure2) reveals that the mean quotient of the number of cities and the number of countries per research paper increases from 1.47 in Block A to 1.66 in Block C (p=0.030). These data clearly indicate that there has been an incremental rise in the number of cities per country over time. The overall mean value of this indicator is 1.56.

<figure id="figure2" class="centre">![Figure 2: Mean number of cities per number of countries per research paper, per journal and time period](p719fig2.jpg)

<figcaption>Figure 2: Mean number of cities per number of countries per research paper, per journal and time period</figcaption>

</figure>

From a cross-journal perspective, the highest ratio is reached by the research papers published in _The Astronomical Journal_ in Block B (2.18) and the lowest ratio by the research papers published in _Monthly Notices of the RAS_ during the same time period (1.19). An interesting outlier case is that of a research paper published in Block B in _The Astronomical Journal_ that mentions nine cities in only two countries.

### National mobility

[Table 8](#table8) shows that only 0.6% of authors have national mobility. It is interesting to note that the research papers published in _Astronomy and Astrophysics_ do not include any authors with national mobility. Conversely, the research papers published in _The Astronomical Journal_ contain this type of mobility both in Blocks A and B, while the research papers published in _Monthly Notices of the RAS_ and _The Astrophysical Journal_ only display it in Block C. Percentage-wise, a slight increase can be appreciated from Block A (0.5%) to Block C (0.8%) across all the journals.

<table id="table8" class="center"><caption>  
Table 8: Number of authors with national mobility per journal and time period</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Number of authors with national mobility</th>

</tr>

<tr>

<th>Journal</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">Total</th>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">5</td>

<td style="text-align:center">8</td>

</tr>

</tbody>

</table>

[Table 9](#table9) (together with [Table 10](#table10)) indicates that there is an incremental rise in the percentage of institutions located in more than one city in the same country (intra-national collaboration) from Block A (0%) to Block C (0.7%), although only a small percentage (0.5%) of all the institutions show this effect.

<table id="table9" class="center"><caption>  
Table 9: Number of institutions located in two or more cities (in one country) per journal and time period</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Number of institutions located in two or more cities</th>

</tr>

<tr>

<th>Journal</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">Total</th>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">5</td>

</tr>

</tbody>

</table>

It is interesting to note a case of ‘inter-intra-national collaboration', which refers to the National Astronomical Observatory of Japan with three branches in different cities and countries (Tokyo and Okayama in Japan, and Hilo in Hawai'i). Finally, it is important to remark that the small figures in [Table 8](#table8) and [Table 9](#table9) (when compared to the figures in [Table 7](#table7)), together with the small figures in [Table 6](#table6) (in comparison with the figures in [Table 1](#table1)), reveal that our national collaboration indicator is not seriously affected either by national mobility, intra-national collaboration, or international mobility.

### Local collaboration

The estimation of local collaboration is based not only on the number of cities ([Table 7](#table7)), but also on the number of institutions ([Table 10](#table10)), which in turn includes the effects of local mobility ([Table 11](#table11)) and intra-national collaboration ([Table 9](#table9)).

[Table 10](#table10) highlights a rise in the total number of institutions mentioned in the research paper bylines from Block A (27.1%) to Block C (42.8%). Here again, _The Astronomical Journal_ is characterised by an up and down pattern, whereas _Astronomy and Astrophysics_, _Monthly Notices of the RAS_ and _The Astrophysical Journal_ exhibit the opposite pattern.

<table id="table10" class="center"><caption>  
Table 10: Number of institutions per journal and time period</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Number of institutions mentioned</th>

</tr>

<tr>

<th>Journal</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">Total</th>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">73</td>

<td style="text-align:center">115</td>

<td style="text-align:center">84</td>

<td style="text-align:center">272</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">65</td>

<td style="text-align:center">58</td>

<td style="text-align:center">133</td>

<td style="text-align:center">256</td>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">68</td>

<td style="text-align:center">67</td>

<td style="text-align:center">107</td>

<td style="text-align:center">242</td>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">62</td>

<td style="text-align:center">58</td>

<td style="text-align:center">100</td>

<td style="text-align:center">220</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">268</td>

<td style="text-align:center">298</td>

<td style="text-align:center">424</td>

<td style="text-align:center">990</td>

</tr>

</tbody>

</table>

[Figure 3](#figure3) displays that the mean quotient of the number of institutions and the number of cities per research paper shows a down and up pattern: from 1.06 in Block A to 1.04 in Block B and to 1.07 in Block C, although the figures are not statistically significant. If we focus on each journal, a steady increment is found in _The Astronomical Journal_ and _The Astrophysical Journal_, whereas the European journals show an erratic pattern (down and up in _Astronomy and Astrophysics_ and the reverse in _Monthly Notices of the RAS_). The highest ratio is reached in _Astronomy and Astrophysics_ in Block A (1.13) and the lowest in _The Astrophysical Journal_ in Block B (0.99). These figures may be more clearly understood if we take into account that Block A in _Astronomy and Astrophysics_ includes two research papers with two institutions in a single city and two research papers with three institutions in two cities. Conversely, one of the research papers published in _The Astrophysical Journal_ in Block B mentions the same institution (California State University) in two different cities (Riverside and Santa Barbara).

The overall mean value of this indicator is 1.06.

<figure id="figure3" class="centre">![Figure 3: Mean number of institutions per number of cities per research paper, per journal and time period](p719fig3.jpg)

<figcaption>Figure 3: Mean number of institutions per number of cities per research paper, per journal and time period</figcaption>

</figure>

### Local mobility

[Table 11](#table11) shows that only 1.1% of the authors have local mobility. The research papers published in _Astronomy and Astrophysics_ and _The Astrophysical Journal_ do not include any local mobility in Blocks A and B. The research papers published in _Monthly Notices of the RAS_ show no local mobility in Block B and those in _The Astronomical Journal_ show none in Block C. Percentage-wise and contrary to the findings recorded in national mobility, the figures show a slight decrease from Block A to Block C (from 1.6% to 1.3%).

<table id="table11" class="center"><caption>  
Table 11: Number of authors with local mobility per journal and time period</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Number of authors with local mobility</th>

</tr>

<tr>

<th>Journal</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">Total</th>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">4</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0</td>

<td style="text-align:center">5</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">4</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">2</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">6</td>

<td style="text-align:center">1</td>

<td style="text-align:center">8</td>

<td style="text-align:center">15</td>

</tr>

</tbody>

</table>

As in the previous types of collaboration, the small figures in [Table 11](#table11) and [Table 9](#table9) (when compared to the figures in [Table 10](#table10)), together with the small figures in [Table 8](#table8) and [Table 9](#table9) (in comparison with the figures in [Table 7](#table7)), reveal that our local collaboration indicator is not seriously affected either by local mobility or intra-national collaboration.

### Intra-institutional collaboration

[Table 12](#table12) indicates the collaboration carried out within different departments in the same institution. Only 3.5% of all the institutions (see [Table 10](#table10)) show this kind of collaboration.

<table id="table12" class="center" style="width:95%;"><caption>  
Table 12: Number of institutions with two or more dependencies (or departments) per journal and time period</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Number of institutions undertaking intra-institutional collaboration</th>

</tr>

<tr>

<th>Journal</th>

<th style="text-align:center">Block A</th>

<th style="text-align:center">Block B</th>

<th style="text-align:center">Block C</th>

<th style="text-align:center">Total</th>

</tr>

<tr>

<td>_The Astrophysical Journal_</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

<td style="text-align:center">7</td>

<td style="text-align:center">13</td>

</tr>

<tr>

<td>_The Astronomical Journal_</td>

<td style="text-align:center">5</td>

<td style="text-align:center">3</td>

<td style="text-align:center">2</td>

<td style="text-align:center">10</td>

</tr>

<tr>

<td>_Astronomy and Astrophysics_</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>_Monthly Notices of the RAS_</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">9</td>

<td style="text-align:center">9</td>

<td style="text-align:center">17</td>

<td style="text-align:center">35</td>

</tr>

</tbody>

</table>

The journals that include the most intra-institutional collaboration are the US-based ones (4.7% altogether), nearly twice the amount of the European journals (2.4% altogether). From a diachronic standpoint, the global data reveal a small downward trend from Block A (3.4%) to Block B (3%) and a slight upward trend in Block C (4%). Per journal, _The Astrophysical Journal_, _Astronomy and Astrophysics_ and _Monthly Notices of the RAS_ increase steadily from Block A to Block C, whereas _The Astronomical Journal_ decreases steadily.

Finally, [Figure 4](#figure4) summarises the results obtained for the three main types of collaboration in the journals analysed (the collaboration indicator on the vertical axis stands for the mean number of countries per research paper in the case of international collaboration, for the mean number of cities per number of countries per research paper in the case of national collaboration and for the mean number of institutions per number of cities per research paper in the case of local collaboration). As it is clearly plotted in Figure 4, international collaboration prevails over national collaboration (p=0.000), which in turn prevails over local collaboration (p=0.000). In addition, _Monthly Notices of the RAS_ has the highest rates of international collaboration, whereas the highest rates of national and local collaboration practices are characteristic of _The Astronomical Journal_.

<figure id="figure4" class="centre">![Figure 4: Collaboration practices per journal ](p719fig4.jpg)

<figcaption>Figure 4: Collaboration practices per journal</figcaption>

</figure>

## Discussion

### International collaboration

Our findings reveal that international collaboration is more characteristic of the European journals than of the US ones ([Table 1](#table1), [Table 3](#table3), [Figure 1](#figure1), [Figure 4](#figure4)), a fact that may be interpreted in a two-fold way:

1.  _Monthly Notices of the RAS_ has no page charges for all authors, and _Astronomy and Astrophysics_ has no (direct) page charge for most European, Argentinian, Brazilian and Chilean researchers;
2.  Europe is formed by many different countries, which means that more countries are involved in research projects. Moreover, many British scientists tend to collaborate with researchers from Commonwealth countries. Consequently, a wider range of different countries are included in the European sphere of research.

The USA is cited most frequently in the research paper bylines ([Table 2](#table2)), which corresponds to the fact that the largest funds for astronomical research are provided by this country. Moreover, since the USA is more frequently mentioned in the American journals than in the European ones, it may be claimed that US researchers, who have page charges in all the journals (except in _Monthly Notices of the RAS_), tend to publish their papers mainly in their own and more well-known journals ([Table 2](#table2)). British scientists also tend to publish in _Monthly Notices of the RAS_, the UK-based journal. From the point of view of international mobility ([Table 6](#table6)), the highest scores are again reached by authors who publish in the European-based journals.

When examined from a diachronic perspective, our quantitative data also reveal that international collaboration has been constantly growing over time ([Table 4](#table4) and [Table 5](#table5)). In this sense, the overall increase observed in international collaboration, as well as the variations noticed per journal and time period, run parallel with the overall increment and the variations per journal and time period recorded in the number of authors study (see [Méndez _et al._, 2014b](#mendez2014b)).

The scope of each journal may also be responsible for the variations observed in the international collaboration indicator. The highest value found in _The Astronomical Journal_ may be attributed to the fact that this journal focuses primarily on observational matters (the most experimental part of astrophysics) that require complex instrumentation (telescopes, detection devices, space missions, etc.) which have to be managed by multidisciplinary teams that are probably working in different countries.

Although international collaboration has increased from Block A to Block C (as well as the increase in authors with international mobility), its variation patterns differ from one journal to another since each has its own peculiarities. In _Monthly Notices of the RAS_ and _The Astrophysical Journal_, international collaboration rose steadily between Block A and Block C, mainly in Block C when compared to Block A. Conversely, _The Astronomical Journal_ and _Astronomy and Astrophysics_ are characterised by an erratic pattern, up and down in _The Astronomical Journal_ and the reverse in _Astronomy and Astrophysics_. We could speculate that the decline in _The Astronomical Journal_ in Block C may be accounted for by the worldwide economic crisis that started in the USA in 2006 ([Tully, 2006](#tully2006)). The crisis provoked a substantial reduction of the funding allocated to astronomical research. This meant that smaller projects were launched and fewer scientists needed, hence the lower value found in Block C. Due to its more general and less observationally-oriented trend, _The Astrophysical Journal_ may not have suffered from the economic crisis to the same degree as _The Astronomical Journal_.

In connection to _Astronomy and Astrophysics_, another economic crisis should be mentioned, the one that started at the beginning of the year 2000, mainly in Germany. After German reunification, the country had to face an excessive deficit and huge economic problems to the point that it was known as the sick man of Europe until 2005, when its economy began to recover ([Dustmann _et al._, 2014](#dustmann2014)). This may explain why international collaboration did not increase in the time period 2000-2004\. Once the crisis passed, so-called big science, which requires financial support from different countries, began again and resulted in the significant growth of international collaboration noticed in Block C. Furthermore, this increase would evidently imply higher levels of cooperation between the countries of the European Union on the one hand and between Europe and the USA on the other, a fact that would corroborate the findings by Méndez and Alcaraz ([2015a](#mendez2015a)) in their study on abbreviations in astrophysics research paper titles. This increase is not reflected in the research papers published in _The Astronomical Journal_ in Block C, however (see [Figure 1](#figure1)). This apparent contradiction may be overcome if we consider that the astronomers who work in American institutions and who handle the most experimental devices (i.e., those that usually publish in _The Astronomical Journal_) are still suffering from economic problems. They thus tend to collaborate with European authors and publish in European journals, which are totally free of charge. This idea may be reinforced by the fact that the number of authors decreases in _The Astronomical Journal_ and increases in _Astronomy and Astrophysics_ from Block B to Block C (see [Méndez _et al._, 2014b](#mendez2014b)).

Finally, it is worth pointing out that an identical variation pattern in the values of the international collaboration indicator and in the average title length (see [Méndez _et al._, 2014a](#mendez2014a)) is found in both _The Astronomical Journal_ and _Monthly Notices of the RAS_. In the case of _The Astrophysical Journal_ and _Astronomy and Astrophysics_, the behaviour is also similar if we do not take Block B into account.

### National collaboration

The overall increase of national collaboration over time ([Figure 2](#figure2)) is similar to that observed in the number of authors (see [Méndez _et al._, 2014b](#mendez2014b)) and international collaboration. Likewise, the overall increase may also be observed in national mobility ([Table 8](#table8)) and intra-national collaboration ([Table 9](#table9)).

Unlike international collaboration, national collaboration is more relevant in the US-based journals than in the European ones ([Figure 2](#figure2) and [Figure 4](#figure4)). This should come as no surprise if we take into account that, as previously stated, US authors mainly publish in US journals, and if we consider the numerous astrophysical research centres located in different cities all over the USA in comparison to the few astrophysical research institutions found in each European country. Furthermore, it is worth stressing that national mobility ([Table 8](#table8)) is less important for authors publishing in the European journals than in the US journals to the point that authors who publish in _Astronomy and Astrophysics_ do not have any national mobility at all.

_The Astronomical Journal_ is the only journal that follows a similar pattern in national and international collaboration. The other journals display the same behaviour if we do not take Block B into account. The discrepancies in Block B may be attributed to the double uncertainty of the numerical indicator used to describe national collaboration (number of cities divided by number of countries). In our opinion, _The Astronomical Journal_ would not be so affected by this effect since it displays the highest value of this indicator ([Figure 4](#figure4)). This result may be explained once more in terms of the more experimental tendency of _The Astronomical Journal_, which implies a more multidisciplinary approach to scientific research.

### Local collaboration

Local collaboration shows an overall increase over time ([Figure 3](#figure3)) like both international and national collaboration. Similarly, this slight overall increase is also observed in the global number of authors with local mobility ([Table 11](#table11)) and in intra-institutional collaboration ([Table 12](#table12)).

From a diachronic and cross-journal perspective, the behaviour is totally different from that observed in international and national collaboration. The differences, the degree of significance of which is even lower than in the case of national collaboration, may once more be accounted for by the double uncertainty of the indicator used to describe local collaboration (number of institutions divided by number of cities). Again, _The Astronomical Journal_ is the journal with the highest degree of local collaboration ([Figure 4](#figure4)) and local mobility ([Table 11](#table11)), which may be once more attributed to its more experimental scope. Conversely, intra-institutional collaboration in _The Astronomical Journal_ comes second after _The Astrophysical Journal_ ([Table 12](#table12)), the journal with the highest impact factor in our sample.

### Sketching a global picture of collaboration practices in astrophysics

As can be seen in [Figure 4](#figure4), international collaboration is the most common type of collaboration in astrophysics. This should come as no surprise because financing and implementing research projects in this discipline implies the construction of very expensive tools (telescopes, space observatories, etc.) which requires heavy investment that is not feasible for an individual country alone. In this sense, collaboration in astrophysics differs from other disciplines like medicine, where science is usually funded not only by grants from government agencies, institutions and foundations, but also by private pharmaceutical companies whose headquarters are usually located in a single country. For example, Bodenheimer ([2000](#boden2000), citing data from Mathieu, 1999 and Centerwatch, 1998) states that 70% of clinical trials in medicine in the USA were financed by private companies. In contrast, public funding was acknowledged in more than 95% of research papers published in the field of astrophysics (see [Méndez and Alcaraz, 2015b](#mendez2015b)). The places selected for the building of telescopes are also very specific because they must offer the best atmospheric conditions for excellent visibility of celestial objects. These include the Canary Islands (Spain), Hawai'i (USA) or Chile with astrophysics installations renowned at international level. Moreover, the launching of space observatories also relies on very complex platforms in very precise locations. Additionally, given the highly specific nature of astrophysics which involves a specific body of knowledge, i.e., the aforementioned _big science_, if astrophysicists want to achieve their research objectives, they have no choice but to undertake international multi-organisational collaborations. Apart from gathering the best experts in the field, those institutions that sometimes may lack sufficient resources and the latest technology ([Shrum _et al._, 2007](#shrum2007)) will need to collaborate mostly internationally. In our opinion, all these features do not usually characterise disciplines such as chemistry, computer science, medicine or sociology where researchers have more opportunities to do research on a local or national level.

Furthermore, since innovative sciences such as astrophysics or, for example, particle physics ([Ortoll _et al._, 2014](#ortoll2014)), are at the forefront of advances in science, the best way to approach multidisciplinary research together with the existence of specific bodies of knowledge that characterise them is to focus mainly on international collaboration. This is a similar finding to a study in the field of European patent data where technological variety was found to serve in support of international innovation collaboration, whereas local and national collaborations were shown to be negatively associated with national technological specialisation and related technological variety ([Ebersberger _et al._, 2014](#ebers2014)). Nevertheless, it is important to remember that none of the local, national, or international collaboration contexts is inherently better than the other since each provides the appropriate space for solving different types of issues ([Waibel, 2010](#waibel2010)). This statement is directly related to the lower degree of local and intra-institutional collaboration observed in our analysis.

Our results also reveal that public and private funding is more readily available for US astrophysics research than it is for European research. Likewise, national and local collaborations are more relevant in the USA than in Europe, although it has to be stressed that US national collaboration would in some cases be equivalent to European international collaboration. However, the increase of international collaboration noticed both in the USA and the UK is a clear reflection of the advance of society towards a more globalised world. Another point that our detailed analysis has clearly disclosed is that economic crises have a large influence on collaboration patterns in the case of astrophysics which depends mainly on public funding, especially in research published in more experimental journals.

Finally, the scenario sketched above would support the assertion that international mobility is, in terms of percentages, the most important type of mobility in astrophysics. In spite of the sparse data regarding the different types of mobility found in our sample (see [Tables 6](#table6), [8](#table8) and [9](#table9)), this is clearly corroborated by our results. In the case of national and local collaboration, local mobility prevails over national mobility, a fact that may be explained in purely economic and logistic terms. Moreover, local mobility tends to decrease over time, while national mobility behaves the other way round, once more reflecting the ongoing globalisation of academic research.

## Conclusions

In this study, we adopted a diachronic standpoint to explore collaboration practices in English-language research papers published in the principal scholarly English-language journals in astrophysics. We introduced three numerical indicators (based on the number of countries, cities and institutions indicated in the bylines of the research papers) in order to study and compare, in absolute terms, the different types of collaboration practices (international, national and local).

The findings of our study can be summarised as follows:

1.  The USA is the country with the highest level of research involvement;
2.  International collaboration occurs more often than national collaboration, which in turn occurs more often than local collaboration. International mobility is more common than local mobility, which in turn is more common than national mobility;
3.  In absolute terms, international collaboration and international mobility increase over time;
4.  The increase in international collaboration runs parallel with the increase in number of authors per research paper from both a diachronic and cross-journal standpoint;
5.  National and intra-national collaboration and national mobility also show an overall increase over time;
6.  Local and intra-institutional collaboration and local mobility also tend to increase over time;
7.  International, national and local collaboration are not seriously affected by international, national or local mobility, respectively;  

8.  Neither national nor local collaboration are seriously affected by intra-national collaboration;
9.  International collaboration and international mobility are more relevant for authors publishing in European journals, whereas national and intra-national collaboration and national mobility are more important for authors publishing in US journals;
10.  Local mobility and intra-institutional collaboration seem to be more important for authors publishing in US journals.

Especially noteworthy is the case of _The Astronomical Journal_, which may be considered the most experimentally-oriented journal in our sample. From a diachronic perspective, _The Astronomical Journal_ has exactly the same pattern in international and national collaboration practices. It also has the highest number of cities and institutions. Conversely, it is the journal with the lowest number of different countries. In addition, it has the highest degree of national and local collaboration, as well as the highest level of national (together with _Monthly Notices of the RAS_) and local mobility. Furthermore, it displays the lowest maximum number of countries included in one single research paper, as well as the lowest percentage of international mobility (together with _The Astrophysical Journal_), the highest international collaboration and mobility being displayed by _Monthly Notices of the RAS_.

Finally, a last point to comment upon is that innovation in astrophysics is conducive to the development of very specialised research groups that collaborate on international projects, the majority of which are publicly funded. This should come as no surprise since astrophysics is a science that deals with the pure advancement of knowledge that often moves away from economic pragmatism and that is probably pioneering real technological innovation in human society. In difficult socio-economic times like the present, we think that a simple inclusive study which simultaneously measures the different types of collaboration would be very interesting to many other sciences and fields. Undoubtedly, this will help bring into focus the really big issues for overall research management and policy.

## Acknowledgements

We are very grateful to the reviewers, the editor and the copyeditor for their valuable suggestions that have substantially helped improve the paper. We are also very grateful to Noemí Gómez-Sáez for her valuable help in preparing the html version of the paper.

## About the authors

**David I. Méndez** holds a Ph. D. in astrophysics from La Laguna University. He teaches undergraduate courses in Fundamental Engineering Physics in the Department of Physics, System Engineering and Signal Theory, Polytechnic University College, University of Alicante, Spain. He has published numerous research articles on star formation and ionized gas in Wolf-Rayet Galaxies, photopolymer optics, non-linear oscillations and information science, among others. He can be contacted at [david.mendez@ua.es](mailto:david.mendez@ua.es)  
**M. Ángeles Alcaraz** holds a Ph. D. in linguistics from the University of Alicante, where she teaches both undergraduate and postgraduate courses in English for Tourism in the Department of English Studies, Faculty of Arts, University of Alicante, Spain. She has published numerous articles on the linguistic and pragmatico-rhetorical analysis of written medical and astrophysics discourses in international journals such as _English for Specific Purposes, JASIST, Scientometrics_, etc. She can be contacted at [ariza@ua.es](mailto:ariza@ua.es)

#### References

*   Beaver, D.D. (2001). Reflections on scientific collaboration (and its study): past, present, and future. _Scientometrics, 52_(3), 365-377.
*   Birnholtz, J.P. (2006). What does it mean to be an author? The instersection of credit, contribution and collaboration in science. _Journal of the American Society for Information Science and Technology, 57_(13), 1758-1770.
*   Bodenheimer, T. (2000). Uneasy alliance: clinical investigators and the pharmaceutical industry. _The New England Journal of Medicine, 342_(20), 1539-1544.
*   Bordons Gangas, M., González-Arbo Manglano, B. & Díez-Faes, A.A. (2013). Colaboración científica e impacto de la investigación. In G. González Alcaide, J. Gómez Ferri & V. Agulló Calatayud (Eds.), _La colaboración científica: una aproximación multidisciplinaria_ (pp. 169-181). Valencia, Spain: Nau Llibres.
*   Chompalov, I., Genuth, J. & Shrum, W. (2002). The organization of scientific collaboration. _Research Policy, 31_(5), 749-767.
*   Crane, D. (1972). _Invisible Colleges_. Chicago, IL: University of Chicago Press.
*   Cronin, B. (2012). Collaboration in art and in science: approaches to attribution, authorship and acknowledgment. _Information and Culture, 47_(1), 18-37.
*   De Solla Price, D.J. (1986). _Little science, big science – and beyond_ . New York, NY: Columbia University Press.
*   Dustmann C., Fitzenberger, B., Schönberg, U. & Spitz-Oener, A. (2014). From sick man of Europe to economic superstar: Germany's resurgent economy. _Journal of Economic Perspectives, 28_(1): 167-188.
*   Ebersberger, B., Herstad, S. J. & Koller, C. (2014). Does the composition of regional knowledge bases influence extra-regional collaboration for innovation? _Applied Economics Letters, 21_(3), 201-204.
*   Glänzel, W. (2002). Coauthorship patterns and trends in the sciences (1980-1998): a bibliometric study with implications for database indexing and search strategies. _Library Trends, 50_(3), 461-473.
*   Glänzel, W. & Schubert, A. (2004). Analyzing scientific networks through co-authorship. In H.F. Moed, W. Glänzel & U. Smoch (Eds.), _Handbook of quantitative science and technology research. The use of publication and patent statistics in studies of S&T systems_ (pp. 257-276). Dordrecht, The Netherlands: Kluwer Academic Publishers.
*   González Alcaide, G., Gómez Ferri, J. & Agulló Calatayud V. (Eds.) (2013). _La colaboración científica: una aproximación multidisciplinaria_. Valencia, Spain: Nau Llibres.
*   Harsanyi, M.A. (1993). Multiple authors, multiple problems – bibliometrics and the study of scholarly collaboration: a literature review. _Library and Information Science Research, 15_(4), 325-354.
*   Katz, J.S. & Hicks, D. (1997). How much is a collaboration worth? A calibrated bibliometric model. _Scientometrics, 40_(3), 541-554.
*   Katz, J.S. & Martin, B.R. (1997). What is research collaboration? _Research Policy, 26_, 1-18.
*   Laudel, G. (2002). What do we measure by co-authorships? _Research Evaluation, 11_(1), 3-15.
*   Leimu R. & Koricheva, J. (2005). Does scientific collaboration increase the impact of ecological articles? _Bio Science, 55_(5), 438-443.
*   Leydesdorff, L. & Wagner, C.S. (2008). International collaboration in science and the formation of a core group. _Journal of Informetrics, 2_(4), 317-325.
*   Marshakova-Shaikevich, I. (2006). Scientific collaboration of new 10 EU countries in the field of social sciences. _Information Processing and Management, 42_(6), 1592-1598.
*   Méndez, D.I., Alcaraz, M.Á. & Salager-Meyer, F. (2014a). Titles in English-medium astrophysics research articles. _Scientometrics, 98_(3), 2331-2351.
*   Méndez, D.I., Alcaraz, M.Á. & Salager-Meyer, F. (2014b). Evaluating authorship variation in English-medium Astrophysics research papers: an across journal and diachronic study (1998-2012). _Revista Canaria de Estudios Ingleses, 69_, 51-63.
*   Méndez, D.I. & Alcaraz, M.Á. (2015a). The use of abbreviations in English-medium astrophysics research papers titles: a problematic issue. _Advances in Language and Literary Studies, 6_(3), 185-196.
*   Méndez, D.I. & Alcaraz, M.Á. (2015b). Exploring acknowledgement practices in English-medium research papers: implications on authorship. _Revista de Lenguas para Fines Específicos, 21_(1), 132-159.
*   Newman, M.E.J. (2004). Coauthorship networks and patterns of scientific collaboration. _Proceedings of the National Academy Of Sciences of the United States of America, 101_(suppl. 1), 5200-5205.
*   Ortoll, E., Canals, A., García, A. & Cobarsí, J. (2014). Principales parámetros para el estudio de la colaboración científica en Big Science. _Revista española de Documentación Científica, 37_(4), e062.
*   Pao, L.M. (1992). Global and local collaborators: a study of scientific collaboration. _Information Processing & Management, 28_(1), 99-109
*   Pedersen, O. (1993). _Early physics and astronomy: a historical introduction_ (2<sup>nd</sup> ed). Cambridge, UK: Cambridge University Press.
*   Schott, T. (1991). The world scientific community: globality and globalisation. _Minerva, 29_(4), 440-462.
*   Shrum, W., Genuth, J. & Chompalov, I. (2007). _Structures of scientific collaboration_ . Cambridge, MA: MIT Press.
*   Sin, S.C.J. (2011). Longitudinal trends in internationalisation, collaboration types and citation impact: a bibliometric analysis of seven LIS journals (1980-2008). _Journal of Library and Information Studies, 9_(1), 27-49.
*   Tully, S. (2006). Real state survival guide. _Fortune Magazine, 153_(9).
*   Wagner, C.S, Brahmakulam, I., Jackson, B., Wong, A. & Yoda, T. (2001). _Science and technology collaboration: building capacity in developing countries_. Santa Monica, CA: RAND Corporation.
*   Wagner, C. & Leydesdorff, L. (2005). Network structure, self-organization and the growth of international collaboration in science. _Research Policy, 3_(10), 1608-1618.
*   Waibel, G. (2010). _Collaboration contexts: framing local, group and global solutions_. Dublin, OH: OCLC Research.
*   Wuchty, S., Jones, B.F. & Uzzi, B. (2007). The increasing dominance of teams in production of knowledge. _Science, 316_(5827), 1036-1039.