# The applicability of constructivist user studies: how can constructivist inquiry inform service providers and systems designers?

#### [Alison Pickard](mailto:alison.pickard@unn.ac.uk) and [Pat Dixon](mailto:pat.dixon@northumbria.ac.uk)  
Division of Information & Communication Management  
School of Informatics, Northumbria University  
Newcastle upon Tyne, United Kingdom

#### **Abstract**

> This paper has attempted to clarify the ways in which individual, holistic case studies, produced through constructivist inquiry, can be tested for trustworthiness and applied to other, similar situations. Service providers and systems designers need contextual information concerning their users in order to design and provide systems and services that will function effectively and efficiently within those contexts. Abstract models can only provide abstract insight into human behaviour and this is rarely sufficient detail upon which to base the planning and delivery of a service. The methodological issues which surround the applicability of individual, holistic case studies are discussed, explaining the concept of 'contextual applicability.' The relevance and usefulness of in-depth case study research to systems designers and service providers is highlighted.

## Introduction

Library and information science studies and, more specifically, studies of information behaviour, have tended to focus on modelling as a means of 'contributing to a better understanding of the user's perspective' ([Yang, 1997:](#yang) 73). The generalisability of these models has long been accepted as a valuable tool for service providers and systems designers, even though they are an abstract approximation of reality. Some studies of information behaviour have attempted to take a constructivist approach to the research. Because, as ([Budd, 2002:](#budd) 135) puts it, 'the terms used in descriptions can have different meanings that are not consistent across theories', the use of the term 'constructivism' as intended within this discussion will be explained. Constructivism here refers to the form of research encompassed within the interpretativist paradigm, constructivism being the belief that 'the knowable world is that of the meaning attributed by individuals. The radical constructivist position virtually excludes the existence of an objective world (each individual produces his own reality).' ([Corbetta, 2003](#corbetta): 24). This view accepts that research findings are themselves partly the construction of the research process.

This approach presents 'rich, descriptive narratives at a micro level to provide detailed descriptions which will allow readers to make sufficient contextual judgements to transfer outcomes, themes and understanding emerging from the case studies to alternative settings.' ([Pickard, 2002:](#pickard) 2). The most frequent challenge to constructivist inquiry is the question of applicability to other contexts. How can 'multiple, holistic, competing and often conflictual realities of multiple stakeholders and research participants (including the inquirer's) ([Lincoln, 1992:](#lincoln) 73) be applied to any situation other than the situation in which the research was grounded? If it is possible to apply an approximation of reality in the form of a model to a given context, then it would naturally follow that it is also possible to apply these multiple realities in the form of 'rich pictures', to that context.

Whether or not either of these forms of research results could be applied to another given context has to be based upon the 'fittingness' of those findings to the new context ([Erlandson, _et al._, 1993](#erlandson)). Fittingness is defined as the degree of congruence between sending and receiving contexts where context A is the sender, that is, the context witnessed and described by the research study. Context B is the receiver, that is, any context to which the reader of the research is attempting to apply the findings ([Lincoln & Guba, 1985](#lincoln85) ). The belief that contextual applicability should be the driving force behind the use of research development in systems design is not a new one. Andrew Dillon, a former member of the HUSAT Research Institute, aimed to develop a framework that could be applied based on 'fittingness' to the situation.

> 'A second aim... is to develop a framework for considering user issues that is potentially applicable to the earliest stages of electronic text design. Though it should be emphasized here and now, it is not expected that such a framework could just be handed over to non-human scientists and flawlessly applied by them as so often appears to be the intention of HCI professionals.' ([Dillon, 1994:](#dillon) 4)

This would imply that any framework would then be applied based upon its fittingness to a given context, not as it stands but applied as a working hypothesis. Constructivist inquiries present 'rich pictures' of individual realities that can be applied in the same way that a model should be applied. Applicability of a model is based on the external validity of the research in order to claim generalisability of the findings, applicability of a 'rich picture' is based on the credibility of the research in order to claim transferability of findings.

It is unlikely that designers or practitioners would attempt to apply a model to their own situations without first making themselves familiar with that situation and identifying similarities between the situation researched and the situation to which the research findings are to be applied. Why then is there so much controversy when it comes to applying 'rich pictures' of individual realities? The answer, it would appear, lies in the basic axioms that guide the research paradigm: if individual, holistic case studies cannot be tested for rigour in the same way as traditional (or conventional positivist) studies, then how can rigour be established? Why should the research findings be believed and used as a working hypothesis unless they are trustworthy? Is it possible to apply traditional tests of rigour or not, and if not, then how does constructivist inquiry demonstrate trustworthiness and why should the findings be trusted and applied by others?

David Silverman poses two questions that are fundamental to any research study; 'have the researchers demonstrated successfully why we should believe them? And does the research problem tackled have theoretical and/or practical significance?' ([Silverman, 1997:](#silverman) 25). Or put another way, 'How can we be sure that an 'earthy', 'undeniable', 'serendipitous' finding is not, in fact, wrong? ([Miles, 1979:](#miles) 590). This paper will attempt to answer these questions and demonstrate that constructivist research findings demand no more of the reader than do conventional research findings; the researcher assumes as much responsibility for the rigour or trustworthiness of the study and of the findings.

## Methodological dualism

The use of qualitative methodology in LIS research has increased considerably over the past two decades, this research often combines both qualitative and quantitative methodologies, being labelled as post-positivism. Post-positivism is seen as a compromise between the traditional positivist form of inquiry and the more recent alternative forms of inquiry, such as constructivism. Each research paradigm has its own basic axioms that not only guide the research process, but also the way research is perceived and applied.

The question is, does the choice of a methodology imply adhesion to the axioms of an individual paradigm or is it possible to mix and match methodologies to achieve a research goal? 'Typically books about research treat techniques and method together, thereby implicitly limiting the use of a particular technique to a certain method,' ([Harvey, 2000:](#harvey) xv). Figure 1 is a map of the research hierarchy showing the various elements of a research study and the relationship they have with each other.

<figure>

![Figure 1](../p175fig1.jpg)</figure>

A paradigm, is taken here to be; 'the entire constellation of beliefs.' ([Kuhn, 1970:](#kuhn) 146), or 'a basic set of beliefs that guide action, whether the everyday garden variety or action taken in connection with a disciplined inquiry.' ([Guba, 1992:](#guba) 17). A paradigm by this definition must therefore imply a choice of methodology, a methodology being; 'the fundamental, or regulative principles' ([Seale, 1998:](#seale) 8) which guide the research process, qualitative or quantitative. A methodology does not imply an individual method, although there are methods which have been traditionally associated with each overlying methodology. A research method is the overall approach used to co-ordinate data concerning the focus of the research study and may imply the use of particular research techniques, or data collection tools. Research techniques may imply the use of a specific research instrument, the research instrument being the tool used to apply the chosen method/s and technique/s. The choice may not be implied by specific method, for example, an experiment could be carried out without apparatus, although it may not often be the case.

> 'Paradigms do imply methodologies, and methodologies are simply meaningless congeries of mindless choices and procedures unless they are rooted in the paradigms.' ([Guba & Lincoln, 1988:](#guba2) 114)

This argument is valid when the methodologies apply to the choice between qualitative and quantitative, it is when we start to associate individual methods with a methodology that the argument becomes distorted. There are many research methods, but very few of them can be directly linked with an overlying methodology, although there have been frequent attempts to do so in the research literature (for example, Flick, 2002; Erlandson, _et al._, 1993). It is true that each methodology favours its own set of individual methods, and that each researcher will favour a particular instrument to apply that method, but at this level there is no concrete connection; such a connection does not appear until an overlying methodology is selected, when a paradigm choice has to be made,unless already made.

This is demonstrated by Gorman and Clayton when they present a summary of the qualitative and the quantitative methodologies, although they do not argue necessarily for paradigmatic purity, it appears implicit in the distinctions between the two. They begin by examining the basic assumptions of each methodology: quantitative research assumes the objective reality of social facts, qualitative research assumes social constructions of reality ([Gorman & Clayton, 1997:](#gorman) 24-28). These assumptions are in fact two basic axioms of two separate belief systems, two conflicting paradigms. How then could it be argued that two methodologies, which such conflicting views of reality, be applied by the same individual research in the same inquiry? Flick argues that 'different research perspectives may be combined and supplemented' (Flick, [2002:](#flick) 25). This may well be the case; methodological dualism is arguably possible. Patton supports this view and adds that 'narrow socialisation into one paradigm or the other typically involves adoption of a world view that limits the kinds of questions that are asked and the strategies used to answer those questions' (Patton, [1988:](#patton) 135). This is in direct conflict with Guba and Lincoln's view of methodological choices;

> 'Like water and oil, they do not mix; indeed, to put them together is to adulterate each with the other. Like similar magnetic poles, they repel one another; to hold them in contact requires force, and when the force is released, the methodologies fly apart.' ([Guba & Lincoln, 1988:](#guba2) 111)

The incommensurability of opposing paradigms highlighted by Kuhn ([1970](#kuhn)) has been interpreted as leading to diametrically opposing worldviews, which requires a total change of ontological and epistemological perspective for many researchers. Yet others claim it is more to do with the inability of individual disciplines to 'talk' across those disciplines, language and terminology being the barrier (Budd, [2001](#budd)). What is clear is that if methodological dualism is possible, then it is vital that researchers seeking to apply methodologies should be aware of the cosmology of the paradigm to which the methodology is linked. Without this understanding it would be difficult to establish the rigour of the research using criteria that took into account the basic axioms of the paradigm. Figure 2 outlines the basic axioms of the traditional positivist, post-positivist and constructivist research paradigms and the research implications connected to those beliefs (Cronbach, [1975,](#cronbach) Corbetta, [2003](#corbetta), Guba, [1992](#guba) & Lincoln, [1992](#lincoln)). This is undisputedly an oversimplification of the diversity of research paradigms, this diversity may be in part a dialectic issue. Constructivism is one element of interpretativism, positivism is, or if generally understood to be, an element of scientism. Positivism falls under the realist banner, whilst constructivism is seen as relativist (Budd, [2001](#budd)). The authors make no apology for this oversimplification as it provides sufficient detail to highlight the methodological issues under discussion here.

<table><caption>

**Figure 2: Axiomatic contrasts of research paradigms.**</caption>

<tbody>

<tr>

<th> </th>

<th>Positivism</th>

<th>Post-positivism</th>

<th>Constructivism</th>

</tr>

<tr>

<th>Ontology</th>

<td>Realist, singular reality</td>

<td>Critical realist.Social reality is 'real' but only knowable in a probabilistic sense.</td>

<td>Relativist. Multiple realities constructed by individuals. Multiple/Holistic</td>

</tr>

<tr>

<th>Epistemology</th>

<td>Objectivist.Dualist (knower can be independent of the known)</td>

<td>Modified objectivist (objectivity approximated by external verification.)</td>

<td>Subjectivist.Interactive. Researcher and subject are interdependent.</td>

</tr>

<tr>

<th>Methodology</th>

<td>Experimental. Manipulative. Verification/falsification.</td>

<td>Modified experimental. Manipulative. Verification/falsification. Discovery.</td>

<td>Hermeneutics. Empathetic interaction between researcher and subject. Interpretation and interaction.</td>

</tr>

<tr>

<th>Outcomes of the research</th>

<td>Context & time independent generalisations leading to 'natural' immutable laws or predictions.</td>

<td>Context & time dependent generalisations leading to models for predictions. Probabilistically true laws.</td>

<td>Context & time dependent working hypotheses leading to understanding.</td>

</tr>

</tbody>

</table>

Ontologically, the constructivist paradigm takes a relativist stance; there is no single, tangible reality that can be reduced and approximated, there are only multiple, constructed realities. Epistemologically, constructivism sees subjectivity as the only option in the research process, the only way the unknown can become known is through our own, individual, belief system. 'Subjectivity is not only forced upon us by the human condition... but because it is the only means of unlocking the constructions held by individuals.' (Guba, [1992:](#guba) 26). This is in direct contrast to the realist ontology associated with positivism (and, therefore, with quantitative methodology), and the epistemological belief that it is possible to obtain objective knowledge ([Popper, 1979](#popper)). This divergence of basic beliefs would then indicate that it is necessary to examine the traditional criteria for judging research and ensure that rigour is tested in accordance with the aims of the research. The question being; how then does the researcher demonstrate successfully that a relativist, subjective, constructivist inquiry should be believed, and more importantly, trusted and applied?

Wolcott's comments on the application of validity criteria within ethnography demonstrate the inappropriateness of applying criteria established for one research paradigm to another, conflicting paradigm; 'a discussion of validity signals a retreat to that pre-existing vocabulary originally designed to lend precision to one arena of dialogue and too casually assumed to be adequate for another.' ([Wolcott, 1990:](#wolcott) 168-169). If traditional criteria of rigour are not adequate for constructivist paradigm then more appropriate criteria have to be developed to demonstrate the trustworthiness of the inquiry.

## 'The trinity of validity, reliability and generalisability.' ([Jansick, 1994](#jansick))

The criteria of rigour developed for use in positivist research clearly, as demonstrated by Wolcott, does not satisfy the needs of qualitative methodology and, in particular, constructivist inquiry. Conventional inquiry traditionally relied upon a set of four criteria for establishing the rigour of any individual inquiry;

*   truth value, established by internal validity,
*   applicability (or generalisability), determined by external validity,
*   consistency, determined by reliability, and
*   neutrality, determined by objectivity.

Although there has been a considerable paradigm shift since the late 1950s, there remains 'an almost constant obsession with the trinity of validity, reliability, and generalizability' ([Jansick, 1994:](#jansick) 215).

The differences in the basic axioms of positivism, post-positivism and constructivism have already been outlined. It would follow, then, that such intrinsic differences between the paradigms make it impossible to judge one by criteria established to judge the rigour of the other, 'accommodation between and among paradigms on axiomatic grounds is simply not possible.' ([Lincoln, 1992:](#lincoln) 81) Because of this it was necessary to develop criteria which could be used to judge the quality of constructivist inquiry, and Guba, in 1981, began to investigate criteria to judge constructivist (then labelled 'naturalistic') inquiries. The resulting criteria judged the methodological and analytical soundness of an inquiry and were given the title 'criteria of trustworthiness.' ([Guba, 1992](#guba)) Trustworthiness criteria were defined as credibility, transferability, dependability and confirmability.

**Credibility** is established 'by having (the findings) approved by the constructors of the multiple realities being studied.' ([Lincoln & Guba, 1985:](#lincoln85) 296). Credibility is shown by prolonged engagement with the research participants, persistent observation of those participants, triangulation of the techniques used to study those participants and their contexts, peer debriefing, and member checks. Member checking is a vital part of a constructivist inquiry in order 'to check with the actors who are the subject of (the research) focus how they interpret (the researchers) interpretations. But, at the same time, to not marginalize (the researchers) voice.' ([Dervin, 1997:](#dervin) 31)

**Transferability**; 'The trouble with generalizations is that they don't apply to particulars.' ([Lincoln & Guba, 1985:](#lincoln85) 110). In constructivist inquiry, the goal is to allow for transferability of the findings rather than wholesale generalisation of those findings. Here the researcher provides 'rich pictures' on an individual level, and the reader then gathers, or already has, empirical evidence concerning the cases to which they wish to apply the findings.

> Because transferability in a naturalistic study depends on similarities between sending and receiving contexts, the researcher collects sufficiently detailed descriptions of data in context and reports them with sufficient detail and precision to allow judgements about transferability. ([Erlandson, Harris, Skipper & Allen, 1993:](#erlandson) 33)

If sufficient similarities between the two contexts are identified by the reader, then it is reasonable for that reader to apply the research findings to his/her own context. The positivist response to this is that the researcher is somehow passing responsibility on to the reader when the responsibility for 'discovery' should ultimately be that of the researcher. This argument can only be justified if we begin from the assumption that only a single reality exists and it is the researcher who should discover it, this can never be the case when we are looking at individuals interacting with their own, exclusive environment. 'Further, every context is by definition different, an intersection of a host of nameless factors. Because of this, research can only be particularized and generalization in the traditional scientific sense is impossible.' ([Dervin, 1997:](#dervin) 14)

**Dependability**; is established by the 'inquiry audit', and external 'auditor' is asked to examine the inquiry process, the way in which the research was carried out. In order to allow for this an audit trail ([Schwandt & Halpen, 1988](#schwandt)) has to be maintained by the researcher along with their own research journal. This ensures that 'proceedings and developments in the process of the research can be revealed and assessed' ([Flick, 2002:](#flick) 229). There is also the task of examining the data produced by the research in terms of accuracy relating to transcripts and levels of saturation in document collection.

**Confirmability**; is vital in terms of limiting investigator bias, limiting bias is as far as it is possible to go from the stand point of not only accepting subjectivity, but using it as a research tool. The notion of objectivity is still often used in post-positivist, qualitative research but it is becoming increasing more difficult to defend even in the modified form, in which it is now applied. 'How, when each researcher is embedded in prejudices, values and specific cognitive frameworks, can we move, however tentatively, towards something which might be called objectivity? ([Lazar, D., 1998:](#lazar) 17). The concept that there has to be a way of studying human behaviour which could generate objective results is rejected by constructivist epistemology. The alternative is to ensure that the results, accepted as the subjective knowledge of the researcher, can be traced back to the raw data of the research, that they are not merely as a product of the 'observer's worldview, disciplinary assumptions, theoretical proclivities and research interests.' ([Charmaz, K., 1995:](#charmaz) 32) this is done by use of the audit trail, which provides a means of ensuring that constructions can be seen to have emerged directly from the data, thereby confirming the research findings and grounding them in the evidence ([Schwandt & Halpen, 1988](#schwandt) ).

"Confirmability; is vital in order to demonstrate that investigator bias has not unduly influenced the research outcome. It is accepted that in constructivist research the knowledge and experience of the investigator will impact on the findings, but it is important to demonstrate that tacit knowledge has not been transferred from the researcher to the findings to such an extent that meaning has been changed. "

Once readers of the research are in a position to understand the basic axioms which guided the research process and are aware that the research has demonstrated trustworthiness, then they are in a position to apply the research findings in an appropriate manner, one suited to the paradigm.

## How can individual, in-depth studies inform practice?

Having established the basic axiomatic differences between the two major paradigms, and outlined how rigour and trustworthiness can be established, we return to the concept of 'fittingness'. The sending context has to be examined and then applied to the receiving context based upon similarities between the two, or 'fittingness' of one to the other. This would be the case when applying models developed through post-positivist research to a particular service or system. The overriding difference with constructivist inquiry is that more contextual detail will be provided than is possible with more general models that attempt to approximate reality. Wilson identifies the 'starting point of user studies to be the individual information user who, in response to some perceived 'need' engages in information-seeking behaviour' ([Wilson, 1994:](#wilson) 35). The individual is at the heart of any constructivist inquiry and the researcher aims to represent the individual's construction of their own reality. The reader will be provided with more contextual substance upon which to form his or her own opinions of fittingness to the receiving context. 'Professionals need to know why and how people think and act as they do if they are to respond effectively to the information needs of those who use library and information services' ([Kirk, 1997:](#kirk)261). Very often models concentrate on actions but do not always provide insight into the meaning behind the actions.

A major advantage of constructivist inquiry is that it can offer understanding of the meanings behind the actions of individuals. 'From this perspective, meaning depends upon context, and the interpretation of action or opinion must take account of the setting in which it is produced' ([Dey, 1993:](#dey) 110). Constructivist inquiry seeks to understand the entire context, both at the macro- and micro-environmental level ([Dixon & Banwell, 1998](#dixon)). The constructivist approach emphasis the shift from a simple, single reality, to the complex, multiple realities of the individual. This shift highlights the need to move away from studies which aim to abstract one element, or just a few elements, of the subject under study, whilst holding every other element constant. Any act of experimentation or observation will inevitably alter the state of the subject being studied. Any research activity will leave the subject of that research in an altered state. Heisenberg claims that, 'what we observe is not nature itself, but nature exposed to our method of questioning' ([1958:](#heisenberg) 288). The data, which are gathered from that research, might be, in part, a product of the research process. The time and context in which the data were gathered will also influence those data. 'Context is something you swim in like a fish. You are in it. It is you.' ([Dervin, 1997:](#dervin) 32) In order to make sense of individual behaviour, this behaviour needs to be studied in a natural setting. It is misleading to assume 'that from what we can learn about people in a very limited, unusual, and often very anxious situation we can make reliable judgements about what they do in very different and more usual situations.' ([Holt, 1983:](#holt) 8)

Only by providing the reader with this rich, contextualised picture can the researcher claim sufficient detail for transfer of findings. Fittingness is the basis for the application of research findings: the responsibility of the researcher lies in producing rich pictures that allow for transfer based on contextual applicability. The responsibility of the reader lies in knowing enough about their context to determine the levels of congruence between the sending and receiving contexts before applying the findings tentatively as a working hypothesis.

## Conclusion

This paper has attempted to clarify the ways in which individual, holistic case studies, produced by constructivist inquiry, can be tested for trustworthiness and applied to other, similar situations. 'The problem of how to assess qualitative research has not yet been solved' ([Flick, 2002:](#flick) 218). However, we are moving towards a solution. If modelling is to continue as the desired result of research, then models must be placed in context. This would provide a model of particular behaviour, in a particular context, subject to environmental flux; that is to say, an individual model that would stand alone to provide insight into the particular whilst also taking its place within a wider collection of models. From these models themes could be established that are concepts common to all cases. These themes would become all the more relevant for having emerged from diverse and often conflicting realities. These models then are those 'multiple, holistic, competing, and often conflictual realities of multiple stakeholders and research participants,' ([Lincoln, 1992:](#lincoln) 73) mentioned at the beginning of this paper. Service providers and systems designers need contextual information concerning their users in order to design and provide systems and services that will function effectively and efficiently within those contexts. Abstract models can only provide abstract insight into human behaviour and this is rarely sufficient detail upon which to base the planning and delivery of a service.

A final word on the polarity of research paradigms and their resulting methodology, and a word on the authors' personal construction has to be included here in order to place this very individual and personal construction into context. This paper has relied on the work of Lincoln and Guba, and it has been said of them that 'their strong stance on the non-miscibility of methodologies places them at the extreme end of the qualitative spectrum' ([Fetterman, 1998:](#fetterman) 279). This is undoubtedly true; it is also true that any extreme polarity, positivist or constructivist, can work towards exclusion. However, it is such extreme polarisation that maintains the quality of research and provides a level of specialism that, although not always practical in real world situations, acts as a bench mark for paradigm purity. The authors have attempted to demonstrate and defend an extreme position but accept that the argument will continue; methodological dualism or purity? For those wishing to take 'the road less travelled', it is hoped that this paper can contribute to their journey.

## References

*   <a id="budd"></a>Budd, J.M. (2001). _Knowledge and knowing in library and information science. A philosophical framework._ New York, NY: Scarecrow Press.
*   <a id="charmaz"></a>Charmaz, K. (1995). Grounded theory. In J. A. Smith, R. Harre and L. V. Langenhove. (Eds.) _Rethinking methods in psychology._ (pp. 29-49.) London: Sage.
*   <a id="corbetta"></a>Corbetta, P. (2003). _Social research, theory, methods and techniques._ London: Sage.
*   <a id="cronbach"></a>Cronbach, L.J. (1975). Beyond the two disciplines of scientific psychology. _American Psychologist,_ **30**(4), 116-127.
*   <a id="dervin"></a>Dervin, B. (1997). Given a context by any other name: methodological tools for taming the unruly beast. In P. Vakkari, R. Savolainen & B. Dervin, Brenda. (Eds.) _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts, 14-16 August, 1996, Tampere, Finland._ (pp. 13-38) London: Taylor Graham.
*   <a id="dey"></a>Dey, I. (1993). _Qualitative data analysis: a user-friendly guide for social scientists._ London: Routledge.
*   <a id="dillon"></a>Dillon, A. (1994). _Designing usable electronic text: ergonomic aspects of human information usage._ London: Taylor & Francis.
*   <a id="dixon"></a>Dixon, P. and Banwell, L. (1998). School Governors and effective decision making. Information Seeking in Context: Exploring the contexts of information behaviour. In T.D. Wilson & D. K. Allen. (Eds.), _Exploring the contexts of information behaviour: Proceedings of the 2nd International Conference on Information Seeking in Context, August 12-15, 1998\. Sheffield, UK._ (pp. 384-392). London: Taylor Graham.
*   <a id="erlandson"></a>Erlandson, D. A., Harris, E. L., Skipper, B. L. & Allen, S. D. (1993). _Doing naturalistic inquiry. A guide to methods._ London: Sage.
*   <a id="fetterman"></a>Fetterman, D. M. (Ed.) (1988). _Qualitative approaches to evaluation in education: the silent scientific revolution._ London: Praeger.
*   <a id="flick"></a>Flick, Uwe (2002) An introduction to qualitative research. (2nd ed.) London: Sage.
*   <a id="gorman"></a>Gorman, G.E. and P. Clayton (1997). _Qualitative research for the information professional: A practical handbook._ London: The Library Association.
*   <a id="guba"></a>Guba, E.C., (Ed.) (1992). _The alternative paradigm._ London: Sage.
*   <a id="guba2"></a>Guba, E. G. and Y. S. Lincoln. (1988). Do inquiry paradigms imply inquiry methodologies? In D. M. Fetterman. (ed.) _Qualitative approaches to evaluation in education: the silent scientific revolution._ (pp. 89-115.) London: Praeger.
*   <a id="harvey"></a>Harvey, A. (2002). _Social research._ London: Sage.
*   <a id="heisenberg"></a>Heisenberg, W. (1958). _Physics and philosophy._ New York, NY: Harper Row.
*   <a id="holt"></a>Holt, J. (1983). _How children learn._ London: Penguin.
*   <a id="jansick"></a>Jansick, V.J. (1994). The dance of qualitative research design. In Norman K. Denzin and Yvonna S. Lincoln. (eds.) _Handbook of qualitative research,_ (pp. 209-219) London: Sage.
*   <a id="kirk"></a>Kirk, J. (1997). Managers' use of information: a grounded theory approach. In P. Vakkari, R. Savolainen & B. Dervin. (Eds.) _Information seeking in context: Proceedings of an international conference on research in information needs, seeking and use in different contexts 14-16 August, 1996, Tampere, Finland_ (pp. 257-267). London: Taylor Graham.
*   <a id="kuhn"></a>Kuhn, T., S (1970). _The structure of scientific revolutions._ Chicago, IL: Chicago University Press.
*   <a id="lazar"></a>Lazar, D. (1998). Selected issues in the philosophy of social science. In C. Seale. (Ed) _Researching society and culture._ (pp. 8-22) London: Sage.
*   <a id="lincoln"></a>Lincoln, Y. (1992). The making of a constructivist. In E. Guba. (Ed.) _The alternative paradim._ (pp. 67-87) London: Sage.
*   <a id="lincoln85"></a>Lincoln, Y. S. and E. G. Guba (1985). _Naturalistic inquiry_. London: Sage.
*   <a id="miles"></a>Miles, M.B. (1979). Qualitative data as an attractive nuisance: the problem of analysis. _Administrative Science Quarterly,_ **24**(), 590-601.
*   <a id="patton"></a>Patton, M.Q. (1988). Paradigms and pragmatism. In . D. M. Fetterman. (Ed.) _Qualitative approaches to evaluation in education: the silent scientific revolution._ (pp. 116-137) London: Praeger.
*   <a id="pickard"></a>Pickard, A.J. (2002) _Access to electronic information resources: their role in the provision of learning opportunities to young people. A constructivist inquiry._ Unpublished doctoral dissertation, Northumbria University, Newcastle upon Tyne, U.K.
*   <a id="popper"></a>Popper, K.R. (1979) _Objective knowledge: an evolutionary approach._ Oxford: Oxford University Press.
*   <a id="schwandt"></a>Schwandt, T.A. & Halpen, E.S. (1988). _Linking auditing and metaevaluation: enhancing quality in applied research._ London: Sage.
*   <a id="seale"></a>Seale, C. (Ed.) (1998). _Researching society and culture._ London: Sage.
*   <a id="silverman"></a>Silverman, D. (1997). Validity and credibility in qualitative research. In G. Miller & R. Dingwall (Eds.) _Context and method in qualitative research. The alternative paradigm_ (pp. 12-25) London: Sage.
*   <a id="wilson"></a>Wilson, T.D. (1994). Information needs and uses: fifty years of progress? In B. Vickery. (Ed.) _Fifty years of information progress: a Journal of Documentation review._ (pp. 15-51) London: Aslib.
*   <a id="wolcott"></a>Wolcott, H.F. (1990). On seeking and rejecting - validity in qualitative research. In E. Eisner, W and A. Preshkin. (Eds.) _Qualitative inquiry in education: the continuing debate._ (pp. 121-152) New York, NY: Teachers College Press.
*   <a id="yang"></a>Yang, S.C. (1997). Information seeking as problem-solving using a qualitative approach to uncover the novice learners' information-seeking processes in a Perseus hypertext system. _Library and Information Science Research_ **19**(1), 71-92\.