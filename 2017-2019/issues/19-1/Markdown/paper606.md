<header>

#### vol. 19 no. 1, March, 2014

</header>

<article>

# The role of motivators in improving knowledge-sharing among academics

#### [Christine Nya-Ling Tan](#author)  
Faculty of Business, Multimedia University, Melaka, Malaysia

#### [T. Ramayah](#author)  
School of Management, Universiti Sains Malaysia, Penang, Malaysia

#### Abstract

> **Introduction.** This research addresses a primary issue that involves motivating academics to share knowledge. Adapting the theory of reasoned action, this study examines the role of motivation that consists of intrinsic motivators (commitment; enjoyment in helping others) and extrinsic motivators (reputation; organizational rewards) to determine and explain the behaviour of Malaysian academics in sharing knowledge.  
> **Method.** A self-administered questionnaire was distributed using a non-probability sampling technique. A total of 373 completed responses were collected with a total response rate of 38.2%.  
> **Analysis.** The partial least squares analysis was used to analyse the data.  
> **Results.** The results indicated that all five of the hypotheses were supported. Analysis of data from the five higher learning institutions in Malaysia found that commitment and enjoyment in helping others (i.e., intrinsic motivators) and reputation and organizational rewards (i.e., extrinsic motivators) have a positive and significant relationship with attitude towards knowledge-sharing. In addition, the findings revealed that intrinsic motivators are more influential than extrinsic motivators. This suggests that academics are influenced more by intrinsic motivators than by extrinsic motivators.  
> **Conclusions.** The findings provided an indication of the determinants in enhancing knowledge-sharing intention among academics in higher education institutions through extrinsic and intrinsic motivators.

<section>

## Introduction

Knowledge-sharing, viewed from an educational context as a social interaction culture that ensures best practices and profound sustainability, is primarily related to activities of exchanging both existing and new knowledge, contributing research and teaching experiences and a myriad of skills among academics for succeeding in educational competitiveness. For Davenport and Prusak ([2000](#dav00)), knowledge-sharing involves the interaction of activities that include dissimilation, feedback and absorption between individuals. With this, higher education institutions are aiming to help and assist their academics in generating new ideas by encouraging them to work together, to facilitate the exchange of knowledge and to further enhance the institutional learning competency and ability of its faculty members, particularly in achieving institutional goals ([Dyer and Nobeoka, 2000](#dye00)).

Likewise, higher education institutions are making sure that their faculty members not only continue to generate new knowledge, but should at the same time share their existing knowledge with others. As a consequence they should be able to achieve long-term institutional success and increase competitiveness and responsiveness in attaining greater university standards and excellence ([Howell and Annansingh, 2012](#how12)). Indeed, the sharing of knowledge is recognised as a main and vital component of knowledge management, which requires academics' willingness to exchange and disseminate knowledge, consequently ensuring knowledge becomes available and is made known to academics ([Sohail and Daud, 2009](#soh09)). Once begun, educators' and researchers' intention to share their knowledge would be further intensified to boost academic and research excellence. Since knowledge-sharing is a part of knowledge management, higher education institutions are eager to carry out knowledge-sharing practices to improve the quality of knowledge in each of their institutional settings in order to improve competitiveness.

In recent years, higher education institutions have played the part of knowledge creators, innovation accelerators and providers of highly skilled and expert researchers that help foster innovation in technology and contribute towards knowledge industries. These institutions are continuously being scrutinised as learning communities, being involved in a collaborative process to achieve _shared creation and shared understanding_ through community-building and observational learning, actively sharing knowledge to improve creativity and innovativeness ([Yeh, Yeh and Chen, 2012](#yeh12)).

For this reason, the Ministry of Higher Education Malaysia has encouraged higher education institutions to practise knowledge-sharing behaviour by applying knowledge-sharing as a mainstream business function ([Suhaimee, Abu Bakar and Alias, 2006](#suh06)). One of the most significant current discussions about effective knowledge-sharing behaviour regards the integration of academics' teaching not merely with critical knowledge, but with skills and abilities to accomplish complex and innovative teaching and research work ([Breu and Hemingway, 2004](#bre04)). By practising knowledge-sharing behaviour, faculty members in higher education institutions can have better access to, and use of, viable knowledge to improve institutional performance. Being the key producers of knowledge, higher education institutions have become the primary drivers in the knowledge-based economy ([Breu and Hemingway, 2004](#bre04)) contributing to today's economic and business research ([Muscio, Quaglione and Scarpinato, 2011](#mus11)). This is done through various applied research and scientific breakthroughs, whereby the creation of new terminologies of knowledge gathered from these researchers helps to enhance and strengthen the society and economy as a whole. In Malaysia, institutions of higher education have a crucial role to play in supporting Malaysia's economy, both in the areas of research and development and creating qualified individuals through education; not only to boost Malaysia's economy but also to increase the number of highly skilled and knowledgeable individuals working in knowledge-based industrialised sectors.

Therefore, to achieve a knowledge-based economy to enhance Malaysia's progress, knowledge shared through education, training, learning and skill development within education institutions must not be taken lightly. Knowledge-sharing behaviour is able to provide opportunities to equip academics not only with knowledge, but also skills and professionalism to meet the requirements of human resources in achieving a knowledge-based economy in Malaysia ([SEAMEO Regional Centre for Higher Education and Development, 2010](#sea10)). However, one of the significant barriers preventing individuals sharing knowledge is insufficient motivation or lack of reward, either monetary or non-monetary ([Azudin, Ismail and Taherali, 2009](#azu09); [Chen, Sandhu and Jain, 2009](#che09)).

So far, however, there has been little discussion about the underlying factors influencing intrinsic (enjoyment in helping others, commitment) and extrinsic motivators (reputation, organizational rewards), the key determinants of knowledge-sharing intentions in higher education institutions. In this paper we therefore seek to address the following research questions:

1.  What types of motivators enhance knowledge-sharing intentions in higher education institutions?
2.  Does motivation affect academics sharing knowledge?

This research seeks to map out the influences of intrinsic and extrinsic motivational determinants in encouraging knowledge-sharing intentions among academics. As a result, the purpose of this paper is to review recent research in understanding the extent of both intrinsic and extrinsic motivators that encourage academics to share knowledge in higher education institutions. The motivators that were investigated were commitment, enjoyment in helping others, reputation, and organizational rewards. The findings provide evidence of how academics could be motivated or encouraged to share knowledge, consequently contributing to greater intention among academics to share knowledge.

## Theory and research model

The theory of reasoned action, proposed by Ajzen and Fishbein ([1980](#ajz80)), which has been widely used in the past, is adapted in this study to capture the key determinants of knowledge-sharing. The theory of reasoned action considers individuals' behaviour to be dependent on their intention to perform a particular behaviour. The theory of reasoned action has been effectively applied in numerous studies (e.g. [Chang 1998](#cha98); [Sheppard, Hartwick and Warshaw 1998](#she98)) in fields including knowledge management, medical studies, social psychology and information technology adoption. In a study conducted by Albarq and Alsughayir ([2013](#alb13)) adapting the theory of reasoned action model helped create a better understanding of internet banking behaviour among Saudi consumers in Riyadh. The theory of reasoned action consists of three separate assumptions, which are: (1) individuals' positive attitudes towards a behaviour are determined strongly by their attitude in participating in that behaviour; (2) an individual's keen intention to perform a behaviour is influenced by subjective norms; (3) individuals are more likely to perform a behaviour if they are keen to engage in that behaviour. This theory is found to be suitable in forecasting a variety of behavioural intentions and actual behaviour. Even though previous research has been carried out on the motivational model of knowledge-related behaviour ([Bock and Kim, 2002](#boc02)), very few studies examine the key determinants of knowledge-sharing behaviour, which include both intrinsic and extrinsic motivators ([Bock, _et al._, 2005](#boc05); [Ramayah, Yeap and Ignatius, 2013](#ram13)).

To address this issue, a research model (Figure 1) using a causal study with factors relating to intrinsic motivators (commitment, enjoyment in helping others) and extrinsic motivators (reputation, organizational rewards) was proposed to assess the role of motivation in explaining the behaviour of knowledge-sharing among academics in higher education institutions. This study examines how commitment, enjoyment in helping others, reputation and organizational rewards, through attitude towards knowledge-sharing, affect academics' intentions to engage in knowledge-sharing. Five hypotheses will be tested with respect to this model. The factors and hypotheses of the research model are discussed in the following section.

## Commitment

Commitment signifies an obligation or duty of an individual to assist and engage in knowledge-sharing through regular collaboration with other individuals in an institution ([Wasko and Faraj, 2005](#was05)). Commitment is a three-dimensional model that comprises of affective commitment, continuance commitment, and normative commitment that may influence workplace behaviour and attitude of individuals to share their knowledge ([Akroyd, _et al._, 2009](#akr09)). Affective commitment concerns emotional dependency, identity and connection of the individual in the institution; continuance commitment is the assessment the individual makes of the harmful effects of leaving the institution, thus increasing their desire to remain, while normative commitment is the individual's moral obligation to remain in the institution ([Scheible and Bastos, 2013](#sch13)). Commitment has a direct effect on performance and altruism ([Neininger, Lehmann-Willenbrock, Kauffeld and Henschel, 2010](#nei10)) particularly when it comes to academics' coordinating and communicating in work groups ([Pillai and Williams, 2004](#pil04)). Consequently, commitment does have a huge effect in motivating academics to intrinsically share their knowledge, thereby boosting their participation in activities related to knowledge-sharing ([Hislop, 2003](#his03)). This does not only decrease academics' turnover, but at the same time increases communication and interaction ([Culpepper, 2011](#cul11)) with other academics in contributing their knowledge to benefit the academic institutions as a whole ([Chiang, 2011](#chi11)). Commitment is known as a symbol of power that has a strong significant positive relationship with knowledge-sharing attitude ([Chen and Cheng, 2012](#cheW12); [Hooff and Weenen, 2004](#hoo04)). Therefore, commitment has a significant positive relationship with knowledge-sharing ([Abili, Thani, Mokhtarian and Rashidi, 2011](#abi11)). Based on this, the following hypothesis is proposed:

**H1.** Commitment to share knowledge has a positive effect on attitude towards knowledge-sharing.

## Enjoyment in helping others

Academics share more ideas and knowledge only if their ideas are viewed as useful ([Hunga, Durcikova, Lai and Lin, 2011](#hun11)). Enjoyment in helping others originates from the perception of altruism ([Kankanhalli, Tan and Wei, 2005](#kan05)), which includes the principal or practice of unselfish concern leading to intrinsic enjoyment by practising knowledge-sharing ([Davenport and Prusak, 2000](#dav00)). As supported by Dinther, Docky and Segers ([2011](#din11)), enjoyment in helping others encourages interaction between individuals to share knowledge. It is strongly believed that enjoyment in helping others and sharing knowledge with others would eventually influence career development ([Song and Chon, 2012](#son12)). Thus, enjoyment in helping others reinforces academics' willingness to share knowledge with the prime purpose of helping their colleagues ([Lin, 2007](#lin07)). Hence, academics' enjoyment in helping others further motivates them to share their knowledge with each other ([Endres, Endres, Chowdhury and Alam, 2007](#end07)). Therefore:

**H2.** Enjoyment in helping others has a positive effect on attitude towards knowledge-sharing.

## Reputation

Reputation is considered by Wasko and Faraj ([2005](#was05)) as a significant motivator to academics' active participation in knowledge-sharing, allowing them to achieve and maintain recognisable status within their institutions. Reputation is the opinion of others of an individual's ability to provide the services expected in regards to work-related activities. Reputation affects the effectiveness of knowledge-sharing ([Lucas, 2005](#luc05)) as it is considered as an extrinsic motivator that will affect faculty members to share their knowledge. Particularly, reputation consists of four types, which are specific skill payment, performance pay, pay based on seniority and job-based pay ([Ferguson and Reio, 2010](#fer10)). Milne ([2007](#mil07)), however, has divided reputation into two categories that can affect knowledge-sharing: general reputation and specific reputation. General reputation refers to the overall abilities of employees while specific reputation relates to the ability of an employee to meet expectations in particular instances. In conclusion, Lucas and Ogilvie ([2006](#luc06)) agreed that there is a positive relationship between reputation and knowledge-sharing. Thus, it is hypothesised that:

**H3.** Reputation has a positive effect on attitude towards knowledge-sharing.

## Organizational rewards

It is found that individuals will only share their knowledge with others if they see a direct return on their action; thus supporting the notion that knowledge-sharing will only happen if rewards exceed costs ([Constant, Kiesler and Sproull, 1994](#con94)). Hence, one of the ways in which education institutions can extrinsically motivate knowledge-sharing practices between academics is through the design and implementation of a viable organizational reward system. Normally, institutions design organizational reward systems to tap into the valuable knowledge owned by academics with the intention of increasing institutional performance. Organizational rewards can encourage academic staff to contribute valuable knowledge made available in universities. Recent research has proven that the application of a reward system for sharing knowledge is vital in increasing knowledge-sharing practices in university settings ([Purwanti, Pasaribu and Lumbantobing, 2010](#pur10)). Reward systems, which can either be monetary or non-monetary, are necessary to further push and encourage academics to share their knowledge ([Susanty and Wood, 2011](#sus11)). The results shown by Cockrell ([2007](#coc07)) indicate that monetary incentives do increase the strength of motivation among individuals to share their professional knowledge, which in turn intensify useful knowledge-sharing. In addition, a reward system in universities should be implemented in a more comprehensive way so that it does not only increase the magnitude of contributions, but also maintains a high quality of knowledge being contributed ([Purwanti _et al._, 2010](#pur10)). Therefore, reward systems can positively increase knowledge-sharing among academics in higher education institutions ([Lee and Ahn, 2007](#lee07)). Following these arguments, the hypothesis is stated as follows:

**H4.** Organizational rewards have a positive effect on attitude towards knowledge-sharing.

## Attitude and intention to share knowledge

Extrinsic motivators and intrinsic motivators can directly affect attitude towards knowledge-sharing behaviour ([Jeon, Kim and Koh, 2011](#jeo11)). Attitude to share knowledge is a personal positive behaviour or intention of an individual to willingly or openly share knowledge with others ([Bock _et al._, 2005](#boc05)). Chow and Chan ([2008](#cho08)) have attempted to explain that the intention of academics to engage in a behaviour is determined by their personal attitude towards that behaviour, in which personal attitude is observed as an enabler that is situated between beliefs and intention. There are two factors that influence the attitude of an academic to share knowledge that include both relational and structural dimensions ([Kim and Ju, 2008](#kim08)). The relational factors include academics' attitude and perception in regards to the value and necessity for sharing knowledge through course and research materials, academics' trust toward their colleagues, academics' willingness to cooperate with each other and their openness in communication. The structural factors include evaluations, reward systems and the communication channel-based information technology infrastructure of the university as a whole.

It is thought that attitude is influenced by individuals' perception and intentions. However, it can also be influenced by social norms and attitude ([Chen, Chuang and Chen, 2012](#cheS12)). If individuals believe that knowledge-sharing is relevant and necessary, they will intend to share knowledge with others from time to time under particular circumstances. However, if individuals believe that knowledge-sharing is not necessary, they will not intend to share knowledge with others. Chow and Chan ([2008](#cho08)) claimed that academics' attitudes towards knowledge-sharing are a significant determinant of intention to share. It is also widely debated whether the behavioural intention to share knowledge is determined by an academic's attitude towards knowledge-sharing. Therefore, the following hypothesis is proposed:

**H5.** Attitude towards knowledge-sharing has a positive effect on intention to share knowledge among academics.

<figure>

![Figure 1: Research model](../p606fig1.jpg)

<figcaption>Figure 1: Research model.</figcaption>

</figure>

## Research method and data collection

The self-administered questionnaire was distributed using a non-probability sampling technique that solicited information around the research model and the background of the academics. The measurement items in the study (Table 1) were adapted from prior studies. It comprised of commitment ([Hooff and Weenen, 2004](#hoo04)), enjoyment in helping others ([Lin, 2007](#lin07)), reputation ([Wasko and Faraj, 2005](#was05)), organizational rewards ([Lin, 2007](#lin07)), attitude towards knowledge-sharing ([Bock _et al._, 2005](#boc05)), and intention to share knowledge ([Bock _et al._, 2005](#boc05)). A seven-point Likert-type scale was utilised for all measures, ranging from (1) strongly disagree to (7) strongly agree.

<table><caption>Table 1: Constructs and measurement.</caption>

<tbody>

<tr>

<th>Construct</th>

<th>Item</th>

</tr>

<tr>

<td>Commitment</td>

<td>CO1 This university is a good institution for me to work for.</td>

</tr>

<tr>

<td> </td>

<td>CO2 I am concerned about how this university is doing.</td>

</tr>

<tr>

<td> </td>

<td>CO3 I put extra effort in to make this university successful.</td>

</tr>

<tr>

<td> </td>

<td>CO4 I talk to my friends and acquaintances about this university as a nice institution to work for.</td>

</tr>

<tr>

<td> </td>

<td>CO5 I take pride in telling others that I work for this university.</td>

</tr>

<tr>

<td> </td>

<td>CO6 Most of the time, I can agree with top management's general direction of the university's management.</td>

</tr>

<tr>

<td>Enjoyment in helping others</td>

<td>EN1 I enjoy sharing my knowledge with colleagues in this university.</td>

</tr>

<tr>

<td> </td>

<td>EN2 I enjoy helping colleagues in this university by sharing my knowledge.</td>

</tr>

<tr>

<td> </td>

<td>EN3 It feels good to help colleagues in this university by sharing my knowledge.</td>

</tr>

<tr>

<td> </td>

<td>EN4 Sharing my knowledge with colleagues in this university is pleasurable.</td>

</tr>

<tr>

<td>Reputation</td>

<td>RE1 I earn respect from other colleagues in this university by sharing my knowledge.</td>

</tr>

<tr>

<td> </td>

<td>RE2 I feel that knowledge sharing improves my status as an academic in this university.</td>

</tr>

<tr>

<td> </td>

<td>RE3 I participate in knowledge sharing to improve my reputation as an academic in this university.</td>

</tr>

<tr>

<td>Organizational rewards</td>

<td>Sharing my knowledge with other colleagues in this university is rewarded…</td>

</tr>

<tr>

<td> </td>

<td>OR1 with a higher salary.</td>

</tr>

<tr>

<td> </td>

<td>OR2 with a higher bonus</td>

</tr>

<tr>

<td> </td>

<td>OR3 with a promotion.</td>

</tr>

<tr>

<td> </td>

<td>OR4 with an increased job security.</td>

</tr>

<tr>

<td>Attitude towards knowledge-sharing</td>

<td>My knowledge sharing with other colleagues in this university…</td>

</tr>

<tr>

<td> </td>

<td>AT1 is good.</td>

</tr>

<tr>

<td> </td>

<td>AT2 is an enjoyable experience.</td>

</tr>

<tr>

<td> </td>

<td>AT3 is valuable to me.</td>

</tr>

<tr>

<td> </td>

<td>AT4 is a wise move.</td>

</tr>

<tr>

<td>Intentions to share knowledge</td>

<td>IN1 I will share work reports and official documents with colleagues in this university.</td>

</tr>

<tr>

<td> </td>

<td>IN2 I will provide manuals, methodologies and models for colleagues in this university.</td>

</tr>

<tr>

<td> </td>

<td>IN3 I will share my experience or know-how with other colleagues in this university.</td>

</tr>

<tr>

<td> </td>

<td>IN4 I will provide know-where or know-whom at the request of colleagues in this university.</td>

</tr>

<tr>

<td> </td>

<td>IN5 I will share expertise from education or training with other colleagues in this university in a more effective way.</td>

</tr>

</tbody>

</table>

A pre-test of the instruments was conducted with seven academics, in which the participants were requested to finish the instruments and offer their comments on the wordings of the items and also to comment on the appearance and arrangement of the instruments. In addition, minor formatting changes (wordings and typesetting) and expert reviews (from academics) were conducted before the instruments were deemed ready to be distributed.

In this research, the selection of the population was done primarily through quota sampling. Specifically, the targeted respondents were academics that were associated with teaching and research activities. The respondents originated from five higher education institutions in Malaysia that were selected based on the Quacquarelli Symonds World University ranking for Asia's Universities in the year 2012, which had ranked these universities as in the top 100 universities in Asia. These five higher learning institutions include Universiti Malaya, Universiti Kebangsaan Malaysia, Universiti Sains Malaysia, Universiti Putra Malaysia and Universiti Teknologi Malaysia. The questionnaires were sent and distributed to the academics from 1 May to 31 August, 2012\. Only one of the returned questionnaires was deemed invalid (due to many uncompleted sections of the questionnaire), resulting in a total of 373 completed answers, with a total response rate of 38.2%. Table 2 summarises the demographic and characteristic profiles of the respondents.

<table><caption>Table 2: Demographic and characteristics profile.</caption>

<tbody>

<tr>

<th>Demographic/characteristics</th>

<th>Category</th>

<th>Frequency</th>

<th>Percentage(%)</th>

</tr>

<tr>

<td>Sex</td>

<td>Male</td>

<td>183</td>

<td>49.1</td>

</tr>

<tr>

<td> </td>

<td>Female</td>

<td>190</td>

<td>50.9</td>

</tr>

<tr>

<td>Age</td>

<td>Below 25 years</td>

<td>9</td>

<td>2.4</td>

</tr>

<tr>

<td> </td>

<td>25 - 35 years</td>

<td>86</td>

<td>23.1</td>

</tr>

<tr>

<td> </td>

<td>36 - 45 years</td>

<td>147</td>

<td>39.4</td>

</tr>

<tr>

<td> </td>

<td>46 - 55 years</td>

<td>100</td>

<td>26.8</td>

</tr>

<tr>

<td> </td>

<td>More than 55 years</td>

<td>31</td>

<td>8.3</td>

</tr>

<tr>

<td>Race</td>

<td>Malay</td>

<td>268</td>

<td>71.8</td>

</tr>

<tr>

<td> </td>

<td>Chinese</td>

<td>58</td>

<td>15.5</td>

</tr>

<tr>

<td> </td>

<td>Indian</td>

<td>34</td>

<td>9.1</td>

</tr>

<tr>

<td> </td>

<td>Others</td>

<td>13</td>

<td>3.5</td>

</tr>

<tr>

<td>Academic position</td>

<td>Professor</td>

<td>78</td>

<td>20.9</td>

</tr>

<tr>

<td> </td>

<td>Associate professor</td>

<td>62</td>

<td>16.6</td>

</tr>

<tr>

<td> </td>

<td>Senior lecturer</td>

<td>110</td>

<td>29.5</td>

</tr>

<tr>

<td> </td>

<td>Lecturer</td>

<td>72</td>

<td>19.3</td>

</tr>

<tr>

<td> </td>

<td>Instructor or tutor</td>

<td>51</td>

<td>13.7</td>

</tr>

<tr>

<td>Length of service</td>

<td>Less than 5 years</td>

<td>85</td>

<td>22.8</td>

</tr>

<tr>

<td> </td>

<td>5 - 10 years</td>

<td>89</td>

<td>23.9</td>

</tr>

<tr>

<td> </td>

<td>11 - 20 years</td>

<td>143</td>

<td>38.3</td>

</tr>

<tr>

<td> </td>

<td>More than 20 years</td>

<td>56</td>

<td>15.0</td>

</tr>

<tr>

<td>Level of education</td>

<td>PhD</td>

<td>235</td>

<td>63.0</td>

</tr>

<tr>

<td> </td>

<td>Masters</td>

<td>123</td>

<td>33.0</td>

</tr>

<tr>

<td> </td>

<td>Degree</td>

<td>15</td>

<td>4.0</td>

</tr>

<tr>

<td>Institution</td>

<td>Universiti Kebangsaan Malaysia</td>

<td>152</td>

<td>40.8</td>

</tr>

<tr>

<td> </td>

<td>Universiti Malaya</td>

<td>24</td>

<td>6.4</td>

</tr>

<tr>

<td> </td>

<td>Universiti Putra Malaysia</td>

<td>43</td>

<td>11.5</td>

</tr>

<tr>

<td> </td>

<td>Universiti Sains Malaysia</td>

<td>104</td>

<td>27.9</td>

</tr>

<tr>

<td> </td>

<td>Universiti Teknologi Malaysia</td>

<td>50</td>

<td>13.4</td>

</tr>

<tr>

<td>Number of conference papers  
published in an average year</td>

<td>1 - 5</td>

<td>244</td>

<td>65.4</td>

</tr>

<tr>

<td> </td>

<td>6 - 10</td>

<td>53</td>

<td>14.2</td>

</tr>

<tr>

<td> </td>

<td>11 - 20</td>

<td>36</td>

<td>9.7</td>

</tr>

<tr>

<td> </td>

<td>21 - 30</td>

<td>23</td>

<td>6.2</td>

</tr>

<tr>

<td> </td>

<td>31 - 40</td>

<td>9</td>

<td>2.4</td>

</tr>

<tr>

<td> </td>

<td>41 - 50</td>

<td>4</td>

<td>1.1</td>

</tr>

<tr>

<td> </td>

<td>51 - 60</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td> </td>

<td>61 - 70</td>

<td>3</td>

<td>0.8</td>

</tr>

<tr>

<td> </td>

<td>71 - 80</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td> </td>

<td>81 and above</td>

<td>1</td>

<td>0.3</td>

</tr>

<tr>

<td>Number of journal papers  
published in an average year</td>

<td>1 - 5</td>

<td>267</td>

<td>71.6</td>

</tr>

<tr>

<td> </td>

<td>6 - 10</td>

<td>37</td>

<td>9.9</td>

</tr>

<tr>

<td> </td>

<td>11 - 20</td>

<td>34</td>

<td>9.1</td>

</tr>

<tr>

<td> </td>

<td>21 - 30</td>

<td>16</td>

<td>4.3</td>

</tr>

<tr>

<td> </td>

<td>31 - 40</td>

<td>13</td>

<td>3.5</td>

</tr>

<tr>

<td> </td>

<td>41 - 50</td>

<td>2</td>

<td>0.5</td>

</tr>

<tr>

<td> </td>

<td>51 - 60</td>

<td>2</td>

<td>0.5</td>

</tr>

<tr>

<td> </td>

<td>61 - 70</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td> </td>

<td>71 - 80</td>

<td>1</td>

<td>0.3</td>

</tr>

<tr>

<td> </td>

<td>81 and above</td>

<td>1</td>

<td>0.3</td>

</tr>

<tr>

<td>Area</td>

<td>Arts</td>

<td>177</td>

<td>47.5</td>

</tr>

<tr>

<td> </td>

<td>Science</td>

<td>196</td>

<td>52.5</td>

</tr>

<tr>

<td>Average number of hours  
spent teaching in a typical week</td>

<td>1 - 5</td>

<td>121</td>

<td>32.4</td>

</tr>

<tr>

<td> </td>

<td>6 - 10</td>

<td>180</td>

<td>48.3</td>

</tr>

<tr>

<td> </td>

<td>11 - 20</td>

<td>72</td>

<td>19.3</td>

</tr>

</tbody>

</table>

In respect to non-response bias, the early and late participants were verified to ensure that they were not significantly different ([Armstrong and Overton, 1977](#arm77)). Therefore, the participants were distributed into two groups, in which a comparison was made on sex, age, race, academic position, level of education and length of service. The results indicated that there were no significant differences based on the comparison t-test (p-values are 0.958, 0.891, 0.937, 0.218, 0.390 and 0.308 respectively). Thus, the non-response bias is not an issue in this research.

## Data analysis and results

The partial least squares structural equation modelling using SmartPLS version 2.0.M3 was selected to assess the two-stage analytical procedures by first examining the measurement model and then scrutinising the structural model ([Anderson and Gerbing, 1988](#and88)). The reasons for the use of partial least squares instead of covariance-based structural equation modelling is that partial least squares is more robust, as less restriction is placed on the unbiased estimates of the sample size ([Falk and Miller, 1992](#fal92)). In addition, partial least squares analysis is useful in identifying the research model's constructs' relationships and measurement ([Wold, 1989](#wol89)). Furthermore, in partial least squares not many rigid assumptions were made concerning the population, scale measurement or distribution ([Haenlein and Kaplan, 2004](#hae04)). As the data collected are self-reported through a similar questionnaire conducted throughout a similar time, the common method variance that is attributed to the measurement method rather than the constructs of interest may cause systematic measurement error and further bias the estimates of the actual relationship among the constructs ([Podsakoff, MacKenzie, Lee and Podsakoff, 2003](#pod03)). Thus, this study has examined the common method bias using Harman's single-factor test. The results revealed six factors with eigenvalues more than one that accounted for 67.6% of the total variance. No single factor was dominant, nor did one general factor account for most of the variance, demonstrating that common method bias is not a great concern and thus is unlikely to confound the interpretation of results.

## Measurement model

In observing the stability of estimates and developing strong confidence intervals ([Chin, 1998](#chi98)) a partial least squares bootstrapping procedure was undertaken with 1000 re-samples to assess the significance of the path analysis and hypotheses as suggested by Efron and Tibshirani ([1993](#efr93)) as both researchers agreed that this would be adequate for a typical bootstrap method. The goodness of measures was exposed to both reliability and validity testing before conducting the hypothesis test. Reliability looks at how consistently an instrument measures the concept it is supposed to measure, while validity looks at how well a developed instrument measures a concept that it is intended to measure ([Sekaran and Bougie, 2010](#sek10)).

<table><caption>Table 3: Results of measurement model.</caption>

<tbody>

<tr>

<th>Model construct</th>

<th>Measurement item</th>

<th>Loading</th>

<th>CRa</th>

<th>AVEb</th>

</tr>

<tr>

<td>Commitment</td>

<td>CO1</td>

<td>0.649</td>

<td>0.854</td>

<td>0.543</td>

</tr>

<tr>

<td> </td>

<td>CO2</td>

<td>0.602</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>CO3</td>

<td>0.758</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>CO4</td>

<td>0.850</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>CO5</td>

<td>0.798</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Enjoyment in helping others</td>

<td>EN1</td>

<td>0.846</td>

<td>0.899</td>

<td>0.691</td>

</tr>

<tr>

<td> </td>

<td>EN2</td>

<td>0.859</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>EN3</td>

<td>0.862</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>EN4</td>

<td>0.752</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Reputation</td>

<td>RE1</td>

<td>0.865</td>

<td>0.894</td>

<td>0.738</td>

</tr>

<tr>

<td> </td>

<td>RE2</td>

<td>0.876</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>RE3</td>

<td>0.836</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Organizational rewards</td>

<td>OR1</td>

<td>0.896</td>

<td>0.946</td>

<td>0.814</td>

</tr>

<tr>

<td> </td>

<td>OR2</td>

<td>0.910</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>OR3</td>

<td>0.928</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>OR4</td>

<td>0.873</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Attitude towards knowledge-sharing</td>

<td>AT1</td>

<td>0.814</td>

<td>0.900</td>

<td>0.692</td>

</tr>

<tr>

<td> </td>

<td>AT2</td>

<td>0.848</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>AT3</td>

<td>0.851</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>AT4</td>

<td>0.814</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Intentions to share knowledge</td>

<td>IN1</td>

<td>0.774</td>

<td>0.911</td>

<td>0.673</td>

</tr>

<tr>

<td> </td>

<td>IN2</td>

<td>0.830</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>IN3</td>

<td>0.850</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>IN4</td>

<td>0.832</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>IN5</td>

<td>0.815</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="5">

`a = composite reliability (CR) = (square of the summation of the factor loadings)/{(square of the summation of the factor loadings) + (square of the summation of the error variances)}  
b = average variance extracted (AVE) = (summation of the square of the factor loadings)/{(summation of the square of the factor loadings) + (summation of the error variances)}`</td>

</tr>

</tbody>

</table>

All loadings of 25 standardised indicators in Tables 3 and 4 exceeded the recommended value of 0.6 ([Chin, Gopal and Salisbury, 1997](#chi97)), signifying that the reliability of the measurement items are acceptable.

<table><caption>Table 4: Loadings and cross loadings.</caption>

<tbody>

<tr>

<th></th>

<th>Commitment</th>

<th>Enjoyment in helping others</th>

<th>Reputation</th>

<th>Organizational rewards</th>

<th>Attitude towards knowledge-sharing</th>

<th>Intentions to share knowledge</th>

</tr>

<tr>

<td>CO1</td>

<td>

**0.649**</td>

<td>0.278</td>

<td>0.225</td>

<td>0.062</td>

<td>0.206</td>

<td>0.139</td>

</tr>

<tr>

<td>CO2</td>

<td>

**0.602**</td>

<td>0.300</td>

<td>0.236</td>

<td>0.014</td>

<td>0.161</td>

<td>0.230</td>

</tr>

<tr>

<td>CO3</td>

<td>

**0.758**</td>

<td>0.303</td>

<td>0.267</td>

<td>0.049</td>

<td>0.269</td>

<td>0.229</td>

</tr>

<tr>

<td>CO4</td>

<td>

**0.850**</td>

<td>0.226</td>

<td>0.330</td>

<td>0.121</td>

<td>0.378</td>

<td>0.291</td>

</tr>

<tr>

<td>CO5</td>

<td>

**0.798**</td>

<td>0.344</td>

<td>0.322</td>

<td>0.057</td>

<td>0.346</td>

<td>0.239</td>

</tr>

<tr>

<td>EN1</td>

<td>0.316</td>

<td>

**0.846**</td>

<td>0.130</td>

<td>-0.055</td>

<td>0.309</td>

<td>0.300</td>

</tr>

<tr>

<td>EN2</td>

<td>0.311</td>

<td>

**0.859**</td>

<td>0.234</td>

<td>-0.022</td>

<td>0.237</td>

<td>0.270</td>

</tr>

<tr>

<td>EN3</td>

<td>0.339</td>

<td>

**0.862**</td>

<td>0.285</td>

<td>-0.042</td>

<td>0.276</td>

<td>0.233</td>

</tr>

<tr>

<td>EN4</td>

<td>0.291</td>

<td>

**0.752**</td>

<td>0.232</td>

<td>0.033</td>

<td>0.269</td>

<td>0.208</td>

</tr>

<tr>

<td>RE1</td>

<td>0.354</td>

<td>0.228</td>

<td>

**0.865**</td>

<td>0.093</td>

<td>0.312</td>

<td>0.178</td>

</tr>

<tr>

<td>RE2</td>

<td>0.299</td>

<td>0.257</td>

<td>

**0.876**</td>

<td>0.112</td>

<td>0.259</td>

<td>0.201</td>

</tr>

<tr>

<td>RE3</td>

<td>0.322</td>

<td>0.191</td>

<td>

**0.836**</td>

<td>0.228</td>

<td>0.285</td>

<td>0.239</td>

</tr>

<tr>

<td>OR1</td>

<td>0.052</td>

<td>-0.021</td>

<td>0.176</td>

<td>

**0.896**</td>

<td>0.146</td>

<td>0.066</td>

</tr>

<tr>

<td>OR2</td>

<td>0.029</td>

<td>-0.107</td>

<td>0.133</td>

<td>

**0.910**</td>

<td>0.109</td>

<td>0.023</td>

</tr>

<tr>

<td>OR3</td>

<td>0.117</td>

<td>-0.013</td>

<td>0.129</td>

<td>

**0.928**</td>

<td>0.170</td>

<td>0.097</td>

</tr>

<tr>

<td>OR4</td>

<td>0.105</td>

<td>0.013</td>

<td>0.163</td>

<td>

**0.873**</td>

<td>0.168</td>

<td>0.136</td>

</tr>

<tr>

<td>AT1</td>

<td>0.371</td>

<td>0.248</td>

<td>0.260</td>

<td>0.172</td>

<td>

**0.814**</td>

<td>0.395</td>

</tr>

<tr>

<td>AT2</td>

<td>0.340</td>

<td>0.299</td>

<td>0.265</td>

<td>0.157</td>

<td>

**0.848**</td>

<td>0.414</td>

</tr>

<tr>

<td>AT3</td>

<td>0.330</td>

<td>0.293</td>

<td>0.270</td>

<td>0.139</td>

<td>

**0.851**</td>

<td>0.490</td>

</tr>

<tr>

<td>AT4</td>

<td>0.263</td>

<td>0.260</td>

<td>0.315</td>

<td>0.096</td>

<td>

**0.814**</td>

<td>0.498</td>

</tr>

<tr>

<td>IN1</td>

<td>0.271</td>

<td>0.238</td>

<td>0.152</td>

<td>0.079</td>

<td>0.410</td>

<td>

**0.774**</td>

</tr>

<tr>

<td>IN2</td>

<td>0.199</td>

<td>0.194</td>

<td>0.160</td>

<td>0.075</td>

<td>0.440</td>

<td>

**0.830**</td>

</tr>

<tr>

<td>IN3</td>

<td>0.274</td>

<td>0.236</td>

<td>0.224</td>

<td>0.065</td>

<td>0.460</td>

<td>

**0.850**</td>

</tr>

<tr>

<td>IN4</td>

<td>0.257</td>

<td>0.254</td>

<td>0.188</td>

<td>0.059</td>

<td>0.406</td>

<td>

**0.832**</td>

</tr>

<tr>

<td>IN5</td>

<td>0.271</td>

<td>0.324</td>

<td>0.247</td>

<td>0.112</td>

<td>0.497</td>

<td>

**0.815**</td>

</tr>

</tbody>

</table>

<small>Bold values are loadings for items which are above the recommended value of 0.5</small>

Table 3 indicates the composite reliability values of the factors ranging from 0.854 (commitment) to 0.946 (organizational rewards), all exceeding the recommended benchmark of 0.7 ([Gefen, Detmar and Boudreau, 2000](#gef00)). The average variance extracted values for all the constructs ranged from 0.543 to 0.814, which indicated that all the values were higher than the cut-off value of 0.5 ([Bagozzi and Youjae, 1988](#bag88)). Table 4 clearly indicates that each construct shares greater variance with its own measurement items as compared with other constructs.

To evaluate the discriminant validity (the extent to which the items measure the intended or other related constructs), the square root of the average variance extracted for each construct should be greater than the correlations between constructs, indicating adequate discriminant validity ([Chin, 1998](#chi98); [Fornell and Larcker, 1981](#for81)). Table 5 illustrates the correlations among the constructs with the square root of the average variance extracted on the diagonal. The results indicated that all of the diagonal values were larger than their correlations with other constructs, presenting that the values of diagonal elements exceed the off-diagonal elements. This demonstrates that the measurement items have good discriminant validity. Overall, the results of testing for validity and reliability in this study demonstrated that all measures have adequate and sufficient reliability, convergent validity and discriminant validity.

<table><caption>Table 4: Loadings and cross loadings.</caption>

<tbody>

<tr>

<th>Model construct</th>

<th>1</th>

<th>2</th>

<th>3</th>

<th>4</th>

<th>5</th>

<th>6</th>

</tr>

<tr>

<td>

1\. Attitude towards knowledge-sharing</td>

<td>

**0.832**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

2\. Commitment</td>

<td>0.391</td>

<td>

**0.737**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

3\. Enjoyment in helping others</td>

<td>0.331</td>

<td>0.379</td>

<td>

**0.831**</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

4\. Intentions to share knowledge</td>

<td>0.542</td>

<td>0.310</td>

<td>0.306</td>

<td>

**0.820**</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

5\. Organizational rewards</td>

<td>0.169</td>

<td>0.091</td>

<td>-0.028</td>

<td>0.097</td>

<td>

**0.902**</td>

<td> </td>

</tr>

<tr>

<td>

6\. Reputation</td>

<td>0.334</td>

<td>0.380</td>

<td>0.261</td>

<td>0.239</td>

<td>0.167</td>

<td>

**0.859**</td>

</tr>

</tbody>

</table>

<small>Diagonals (in bold) represent square roots of average variance extracted (AVE) while off-diagonal represent correlations</small>

<figure>

![Figure 2: Results of the partial least squares analysis](../p606fig2.jpg)

<figcaption>Figure 2: Results of the partial least squares analysis.</figcaption>

</figure>

<table><caption>Table 6: Partial least squares structural model results.</caption>

<tbody>

<tr>

<th>Hypothesis</th>

<th>Relationship</th>

<th>Beta</th>

<th>SE</th>

<th>t-value</th>

<th>Result</th>

</tr>

<tr>

<td>H1</td>

<td>Commitment ' Attitude towards knowledge-sharing</td>

<td>0.239</td>

<td>0.053</td>

<td>4.515*</td>

<td>Supported</td>

</tr>

<tr>

<td>H2</td>

<td>Enjoyment in helping others ' Attitude towards knowledge-sharing</td>

<td>0.200</td>

<td>0.059</td>

<td>3.410*</td>

<td>Supported</td>

</tr>

<tr>

<td>H3</td>

<td>Reputation ' Attitude towards knowledge-sharing</td>

<td>0.170</td>

<td>0.055</td>

<td>3.110*</td>

<td>Supported</td>

</tr>

<tr>

<td>H4</td>

<td>Organizational rewards ' Attitude towards knowledge-sharing</td>

<td>0.124</td>

<td>0.046</td>

<td>2.695*</td>

<td>Supported</td>

</tr>

<tr>

<td>H5</td>

<td>Attitude towards knowledge-sharing ' Intentions to share knowledge</td>

<td>0.542</td>

<td>0.072</td>

<td>7.494*</td>

<td>Supported</td>

</tr>

<tr>

<td colspan="6">

`Beta = regression weight, SE = standard error, t-values are computed through bootstrapping procedure with 373 cases and 1000 re-samples   *p < 0.01`</td>

</tr>

</tbody>

</table>

## Structural model

Figure 2 and Table 6 provide the structural model results with the coefficients for each path that indicates the causal relations among the constructs in the model ([Sang, Lee and Lee, 2010](#san10)). The tests on the significance of the path and hypothesis in the path model were performed using the SmartPLS's bootstrap re-sampling technique (1000 re-samples). All the five hypothesised relationships were supported with path coefficients larger than 2.33 and significant p < 0.01\. Overall, the model explains 29.4% of the variance in the dependent variable, intentions to share knowledge among academics. The model also explains over 23.7% of the variance in the attitude of academics to share knowledge.

The research results confirmed that commitment had a significant and positive effect on the attitude to share knowledge, with the path coefficient (B = 0.239) and t-value = 4.515 at p < 0.01 significance level.This result suggests that maintaining academics' commitment toward knowledge-sharing would positively encourage their attitude to share with others. Thus, Hypothesis 1 _Commitment to share knowledge has a positive effect on attitude towards knowledge-sharing_ is supported.

A statistical positive relationship between enjoyment in helping others and attitude toward knowledge-sharing is found in this research having path coefficient (B = 0.200) and t-statistic = 3.410 at p < 0.01 level, which leads to the conclusion that academics' enjoyment in helping others strengthens their intentions to share knowledge positively. Thus, Hypothesis 2, that _Enjoyment in helping others has a positive effect on attitude towards knowledge-sharing_, is supported in the research results.

The results also support Hypothesis 3, _Reputation has a positive effect on attitude towards knowledge-sharing_, with the path coefficient B = 0.170 and t-value of 3.110 at p < 0.01, indicating that if academics' reputation increases, their knowledge-sharing attitude towards other academics in higher education institutions improves.

Hypothesis 4 theorised that _Organizational rewards have a positive effect on attitude towards knowledge-sharing_and is supported by this study's data results. The path coefficient between the two constructs was 0.124 with t-statistic 2.695 at p < 0.01 significance level. The statistical positive relationship indicates that high rewards given to academics in universities would encourage them to share knowledge with each other.

Finally, Hypothesis 5, that _Attitude towards knowledge-sharing has a positive effect on intentions to share_, is also supported in the results of this study. The results indicate that the path coefficient was 0.542 with t-value of 7.494 at p < 0.01 significance level. The academics' attitude towards knowledge-sharing strengthens their intention to share knowledge among academics in higher education institutions.

In summary, Hypotheses 1, 2, 3, 4, and 5 of this study were supported. A closer examination revealed that commitment was the key motivator of academics' knowledge-sharing behaviour, followed by enjoyment in helping others.

## Discussions and implications

This research attempted to address the significant issue in knowledge management that involves motivating individuals to share knowledge with others. To address this essential issue a research model based on the theory of reasoned action has been developed to provide a more comprehensive understanding of the intrinsic motivators (commitment, enjoyment in helping others) and extrinsic motivators (reputation, organizational rewards) that influence academics to share knowledge. The objective is to deepen the understanding of how higher education institutions are able to motivate their academics to engage in knowledge-sharing behaviour.

The results in this research indicate that all five of the hypotheses were supported, consistent with findings of other studies of knowledge-sharing using theory of reasoned action ([Bock and Kim, 2002](#boc02); [Lin and Lee, 2004](#lin04)). Analysis of data from the five higher education institutions found that commitment (H1), enjoyment in helping others (H2), reputation (H3) and organizational rewards (H4) have a positive and significant relationship with attitude towards knowledge-sharing. Together these four predictors explained 23.7% of the variance of knowledge-sharing attitudes among academics. These are significant findings in that these predictors are able to explain a large part of the variance of attitude towards knowledge-sharing, and thus provide insights into the predictors that affect the sharing of knowledge among academics. Hence, the finding answers the first research question.

In explaining the hypotheses, this research found that knowledge-sharing behaviour is influenced by academics' commitment to their institution. Commitment is indeed an important determinant of knowledge-sharing. This is because academics are more willing to commit their extra effort into sharing their knowledge once they are convinced that such actions will be appreciated by their institutions so that their knowledge will be beneficial and can be used for a good cause. As confirmed by Dewitte and Cremer ([2001](#dew01)), commitment does encourage voluntary sharing of knowledge among academics in universities. By sharing willingly, a knowledge-sharing culture among these individuals would be inculcated in higher education institutions ([Hall, 2001](#hal01)).

In addition, this study also found that academics' enjoyment and the desire to help others act as intrinsic motivators in facilitating knowledge-sharing behaviour. Academics cultivate the desire to share knowledge due to the pleasure in helping others, and in turn become satisfied by doing so. As a result, these academics are more inclined to share knowledge with others ([Wasko and Faraj, 2005](#was05)) through altruism, and such enjoyment flourishes through helping others ([Ba, 2001](#ba01)). As such, academics that enjoy helping others are always keen to share their knowledge with their fellow colleagues.

Also, this study has proven that reputation has a significant effect on attitude and intention of academics to participate in knowledge-sharing. This result is consistent with Lakhani and von Hippel ([2003](#lak03)) and Baines ([2011](#bai11)) who indicated that building reputation is a strong motivator for improving academics' knowledge-sharing behaviour in academic institutions. Academics offer useful advice to others since they perceive that they gain status by answering frequently and intelligently. This will further enhance their status in their profession and motivate them to contribute their valuable personal knowledge to others.

Organizational reward is positively associated with attitude and intention to engage in knowledge-sharing, which supports the results of Hall ([2001](#hal01)). As suggested by the researcher, rewarding individuals for their correct behaviour is crucial, especially when it comes to knowledge-sharing. Therefore, educational institutions need to reward academics for embracing and practising sharing behaviour by intrinsically motivating academics through acknowledgements over increase in salary ([Mohamed, Sapuan, Ahmad, Hamouda and Baharudin, 2009](#moh09)) since it is considered to be an effective means for academic institutions to encourage knowledge sharing among academics ([Bartol and Srivastava, 2002](#bar02)).

The results of this study also showed that attitude towards knowledge-sharing (H5) has a positive and significant effect on the intentions to share knowledge in universities and was able to explain 29.4% of the variance of knowledge-sharing. Thus, the findings above answer the second research question.

It is shown that the intentions to engage in a particular behaviour are determined by an academic's attitude towards that behaviour. The finding suggests that as attitude towards knowledge-sharing becomes more positive, the intention to share becomes greater ([Bock _et al._, 2005](#boc05)). Academics' attitude and intention towards knowledge-sharing were strongly related with their intrinsic and extrinsic motivation to share knowledge. This suggests that a sense of capability and self-confidence in academics is needed in order for them to have the tendency and motivation to share their valuable knowledge with their colleagues.

We conclude that attitude towards knowledge-sharing among academics is influenced by a combination of commitment and enjoyment in helping others as well as reputation and organizational rewards. When differentiating the degree of the effect, it is found that commitment and enjoyment in helping others (intrinsic motivators) are more influential than reputation or organizational rewards (extrinsic motivators) on knowledge-sharing. From a managerial viewpoint, given the viable significance of knowledge-sharing in higher education institutions, findings of this study are intended to allow policy-makers and practitioners in academic institutions to formulate policies and to target higher education institutions appropriately to ensure the effective conception and the further evolution of a knowledge-sharing culture among faculty members. Academic institutions should pay attention to nurturing a culture of sharing, due to the importance of knowledge-sharing in today's communities, so as to encourage academics to stay committed to the institution and to those around them.

It is necessary to promote a positive institutional atmosphere by arranging social activities with participation from both universities' management and academics to cultivate reciprocal relationships among academics and to widen communication channels by encouraging open communication in faculties for instant, interactive communication. Indeed, academics' positive experiences and their level of satisfaction as they assist one another through various knowledge-sharing activities could be inculcated and further intensified in higher education institutions. The universities' management should encourage informal social gatherings in workplaces for relaxed communication between academics and should also give priority to improving academics' skills and expertise through various workshops and training. Likewise, reward systems can be used to encourage participation in teaching, research and collaboration activities.

The findings of this study on the effects of commitment, enjoyment in helping others, reputation, organizational rewards and attitude toward knowledge-sharing are not only consistent with the results from earlier research, but have also shown that knowledge-sharing activities are significantly determined by commitment, enjoyment in helping others, reputation and organizational rewards. In summary, this research statistically demonstrates that the motivational predictors, both intrinsic and extrinsic, had a positive significant impact on knowledge-sharing behaviour among academics. Hence, the practical implication is that good practice is encouraged by developing positive and active common values to enhance knowledge-sharing among academics in higher education institutions.

## Conclusions and future research

Effective knowledge-sharing behaviour cannot be forced but must be fostered with the help of both intrinsic and extrinsic motivators associated with academics' intentions to share knowledge with others. The findings of this study have shown the importance of the underlying factors of academics' extrinsic motivators (commitment, enjoyment in helping others) and intrinsic motivators (reputation, organizational rewards) when it comes to knowledge-sharing behaviour. Higher education institutions, particularly in Malaysia, should understand the motivational effects that influence knowledge-sharing intentions among their academics. At the same time, the findings would be able to provide both theoretical and empirical suggestions in determining and explaining knowledge-sharing behaviour of academics. Educational institutions would also be able to motivate their faculty members to participate in knowledge-sharing that helps to improve institutional growth and efficiency, which will catapult them as a world-renowned knowledge hub to further boost their reputation as education and research universities.

Future studies could discover the influence of other types of intrinsic and extrinsic motivators on the intention and attitude to share knowledge. In addition, studies to be conducted in the future could also determine the effects of motivational factors and intention for knowledge-sharing on institutional performance. This research model could be further verified and applied in future studies that focus on academics to verify the research model of commitment, enjoyment in helping others, reputation, organizational rewards, attitude toward knowledge-sharing and intention to participate in knowledge-sharing. Finally, this paper offered comprehensive details and established a theoretical model for future studies.

## Acknowledgements

The authors wish to express their gratitude to those who contributed their effort, support and guidance in making the current research a success.

</section>

<section>

<ul>
<li id="abi11">Abili, K., Thani, F. N., Mokhtarian, F. &amp; Rashidi, M. M. (2011). The role of effective factors on organizational knowledge sharing. <em>Procedia - Social and Behavioral Sciences, 29</em>(1), 1701-1706.
</li>
<li id="ajz80">Ajzen, I. &amp; Fishbein, M. (1980). <em>Understanding attitudes and predicting social behavior.</em> Englewood Cliffs, NJ: Prentice-Hall.
</li>
<li id="akr09">Akroyd, D., Legg, J., Jackowski, M. B. &amp; Adams, R. D. (2009). The impact of selected organizational variables and managerial leadership on radiation therapists' organizational commitment. <em>Radiography, 15</em>(2), 113-120.
</li>
<li id="alb13">Albarq, A. N. &amp; Alsughayir, A. (2013). Examining theory of reasoned action in internet banking using SEM among Saudi consumers. <em>International Journal of Marketing Practices, 1</em>(1), 16-30.
</li>
<li id="and88">Anderson, J. C. &amp; Gerbing, D. W. (1988). Structural equation modeling in practice: a review and recommended two-step approach. <em>Psychological Bulletin, 103</em>(3), 411-423.
</li>
<li id="arm77">Armstrong, J. S. &amp; Overton, T. S. (1977). Estimating nonresponse bias in mail surveys. <em>Journal of Marketing Research, 14</em>(3), 396-402.
</li>
<li id="azu09">Azudin, N., Ismail, M. N. &amp; Taherali, Z. (2009). Knowledge sharing among workers: a study on their contribution through informal communication in Cyberjaya, Malaysia. <em>Knowledge Management &amp; E-Learning: an International Journal, 1</em>(2), 139-164.
</li>
<li id="bag88">Bagozzi, R. P. &amp; Youjae, Y. (1988). On the evaluation of structural equation models. <em>Journal of the Academy of Marketing Science, 16</em>, 74-94.
</li>
<li id="ba01">Ba, S. (2001). Research commentary: introducing a third dimension in information systems design - the case for incentive alignment. <em>Information Systems Research, 12</em>(3), 225-239.
</li>
<li id="bai11">Baines, L. (2011). Knowledge sharing: why reputation matters for R&amp;D in multinational firms. <em>The International Journal of Entrepreneurship and Innovation, 12</em>(1), 76.
</li>
<li id="bar02">Bartol, K.M. &amp; Srivastava, A. (2002). Encouraging knowledge sharing: the role of organizational reward systems. <em>Journal of Leadership &amp; Organizational Studies, 9</em>(1), 64-77.
</li>
<li id="boc02">Bock, G. W. &amp; Kim, Y. G. (2002). Breaking the myths of rewards: an exploratory study of attitudes about knowledge sharing. <em>Information Resource Management Journal, 15</em>(2), 14-21.
</li>
<li id="boc05">Bock, G. W., Zmud, R. W., Kim, Y. G. &amp; Lee, J. N. (2005). Behavioral intention formation in knowledge sharing: examining the roles of extrinsic motivators, social-psychological forces, and organizational climate. <em>MIS Quarterly, 29</em>(1), 87-111.
</li>
<li id="bre04">Breu, K. &amp; Hemingway, C. J. (2004). Making organizations virtual: the hidden cost of distributed teams. <em>Journal of Information Technology, 19</em>(3), 191-202.
</li>
<li id="cha98">Chang, M. K. (1998). Predicting unethical behavior: a comparison of the theory of reasoned action and the theory of planned behavior. <em>Journal of Business Ethics, 17</em>(16), 1825-1834.
</li>
<li id="cheS12">Chen, S.-S., Chuang, Y.-W. &amp; Chen, P.-Y. (2012). Behavioral intention formation in knowledge sharing: examining the roles of KMS quality, KMS self-efficacy, and organizational climate. <em>Knowledge-Based Systems, 31</em>, 106-118.
</li>
<li id="cheW12">Chen, W.-J. &amp; Cheng, H.-Y. (2012). Factors affecting the knowledge sharing attitude of hotel service personnel. <em>International Journal of Hospitality Management, 31</em>(2), 468-476.
</li>
<li id="che09">Chen, W. L., Sandhu, M. S. &amp; Jain, K. K. (2009). Knowledge sharing in an American multinational company based in Malaysia. <em>Journal of Workplace Learning, 21</em>(2), 125-142.
</li>
<li id="chi11">Chiang, H.-H., Han, T.-S. &amp; Chuang, J.-S. (2011). The relationship between high-commitment HRM and knowledge-sharing behavior and its mediators. <em>International Journal of Manpower, 32</em>(5/6), 604-622.
</li>
<li id="chi98">Chin, W. W. (1998). The partial least squares approach to structural equation modeling. In G. A. Marcoulides (Ed.) <em>Modern business research methods</em>, Mahwah, NJ: Lawrence Erlbaum Associates.
</li>
<li id="chi97">Chin, W. W., Gopal, A. &amp; Salisbury, W. D. (1997). Advancing the theory of adaptive structuration: the development of a scale to measure faithfulness of appropriation. <em>Information Systems Research, 8</em>(4), 342-367.
</li>
<li id="cho08">Chow, W. S. &amp; Chan, L. S. (2008). Social network, social trust and shared goals in organizational knowledge sharing. <em>Information &amp; Management, 45</em>(7), 458-465.
</li>
<li id="coc07">Cockrell, C. (2007). <em>Quality and quantity of motivation in functional and dysfunctional knowledge sharing.</em> Unpublished doctoral dissertation, University of Kentucky, Lexington, KY, USA
</li>
<li id="con94">Constant, D., Kiesler, S. &amp; Sproull, L. (1994). What's mine is ours, or is it? A study of attitudes about information sharing. <em>Information Systems Research, 5</em>(4), 400-421.
</li>
<li id="cul11">Culpepper, R. A. (2011). Three-component commitment and turnover: an examination of temporal aspects. <em>Journal of Vocational Behavior, 79</em>(2), 517-527.
</li>
<li id="dav00">Davenport, T. H. &amp; Prusak, L. (2000). <em>Working knowledge: how organizations manage what they know.</em> Boston, MA: Harvard Business School Press.
</li>
<li id="dew01">Dewitte, S. &amp; Cremer, D. d. (2001). Self-control and cooperation: different concepts, similar decisions? A question of the right perspective. <em>The Journal of Psychology, 135</em>(2), 133-153.
</li>
<li id="din11">Dinther, M.v., Dochy, F. &amp; Segers, M. (2011). Factors affecting students' self-efficacy in higher education. <em>Educational Research Review, 6</em>(2), 95-108.
</li>
<li id="dye00">Dyer, J. H. &amp; Nobeoka, K. (2000). Creating and managing a high performance knowledge-sharing network: the Toyota case. <em>Strategic Management Journal, 21</em>(3), 345-367.
</li>
<li id="efr93">Efron, B. &amp; Tibshirani, R. (1993). <em>An introduction to the bootstrap.</em>. New York, NY: Chapman and Hall.
</li>
<li id="end07">Endres, M. L., Endres, S. P., Chowdhury, S. K. &amp; Alam, I. (2007). Tacit knowledge sharing, self-efficacy theory, and application to the Open Source community. <em>Journal of Knowledge Management, 11</em>(3), 92-103.
</li>
<li id="fal92">Falk, R.F. &amp; Miller, N. B. (1992). <em>A primer for soft modelling.</em>. Akron, OH: University of Akron Press.
</li>
<li id="fer10">Ferguson, K. L. &amp; Reio, T. G. (2010). Human resource management systems and firm performance. <em>Journal of Management Development, 29</em>(5), 471-494.
</li>
<li id="for81">Fornell, C. &amp; Larcker, D. F. (1981). Evaluating structural equation models with unobservable variables and measurement error. <em>Journal of Marketing Research, 18</em>(1), 39-50.
</li>
<li id="gef00">Gefen, D., Detmar, S. &amp; Boudreau, M.-C. (2000). Structural equation modeling and regression: guidelines for research practice. <em>Communications of the Association for Information Systems, 4</em>(1), 1-70.
</li>
<li id="hae04">Haenlein, M. &amp; Kaplan, A. M. (2004). A beginner's guide to partial least squares analysis. <em>Understanding Statistics, 3</em>(4), 283-297.
</li>
<li id="hal01">Hall, H. (2001). Input-friendliness: motivating knowledge sharing across intranets. <em>Journal of Information Science, 27</em>(3), 139-146.
</li>
<li id="sea10">SEAMEO Regional Centre for Higher Education and Development. (2010). <em><a href="http://www.webcitation.org/6FNLGsmuG">Malaysia higher education system</a>.</em> , Retrieved from http://www.rihed.seameo.org/wp-content/uploads/2012/04/2010-05.pdf (Archived by WebCite® at http://www.webcitation.org/6FNLGsmuG)
</li>
<li id="his03">Hislop, D. (2003). Linking human resource management and knowledge management via commitment: a review and research agenda. <em>Employee Relations, 25</em>(2), 182-202.
</li>
<li id="hoo04">Hooff, B. v,d, &amp; Weenen, F.d.L.v (2004). Committed to share: commitment and CMC use as antecedents of knowledge sharing. <em>Knowledge and Process Management, 11</em>(1), 13-24.
</li>
<li id="how12">Howell, K. &amp; Annansingh, F. (2012). Knowledge generation and sharing in UK universities: a tale of two cultures? <em>International Journal of Information Management, 33</em>(1), 32-39.
</li>
<li id="hun11">Hunga, S.-Y., Durcikova, A., Lai, H.-M. &amp; Lin, W.-M. (2011). The influence of intrinsic and extrinsic motivation on individuals' knowledge sharing behavior. <em>International Journal of Human-Computer Studies, 69</em>(6), 415-427.
</li>
<li id="jeo11">Jeon, S., Kim, Y.-G. &amp; Koh, J. (2011). An integrative model for knowledge sharing in communities-of-practice. <em>Journal of Knowledge Management, 15</em>(2), 251-269.
</li>
<li id="kan05">Kankanhalli, A., Tan, B. C. Y. &amp; Wei, K. K. (2005). Contribution knowledge to electronic repositories: an empirical investigation. <em>MIS Quarterly, 29</em>(1), 113-143.
</li>
<li id="kim08">Kim, S. &amp; Ju, B. (2008). An analysis of faculty perceptions: attitudes toward knowledge sharing and collaboration in an academic institution. <em>Library &amp; Information Science Research, 30</em>(4), 282-290.
</li>
<li id="lak03">Lakhani, K. R. &amp; von Hippel, E. (2003). How open source software works: "free" user-to-user assistance. <em>Research Policy, 32</em>(6), 923-943.
</li>
<li id="lee07">Lee, D.-J. &amp; Ahn, J.-H. (2007). Reward systems for intra-organizational knowledge sharing. <em>European Journal of Operational Research, 180</em>, 938-956.
</li>
<li id="lin07">Lin, H.-F. (2007). Knowledge sharing and firm innovation capability: an empirical study. <em>International Journal of Manpower, 28</em>(3/4), 315-332.
</li>
<li id="lin04">Lin, H.-F. &amp; Lee, G.-G. (2004). Perceptions of senior managers toward knowledge-sharing behaviour. <em>Management Decision, 42</em>(1), 108-125.
</li>
<li id="luc05">Lucas, L. M. (2005). The impact of trust and reputation on the transfer of best practices. <em>Journal of Knowledge Management, 9</em>(4), 87-101.
</li>
<li id="luc06">Lucas, L. M. &amp; Ogilvie, D.T. (2006). Things are not always what they seem: how reputations, culture, and incentives influence knowledge transfer. <em>The Learning Organization, 13</em>(1), 7-24.
</li>
<li id="mil07">Milne, P. (2007). Motivation, incentives and organisational culture. <em>Journal of Knowledge Management, 11</em>(6), 28-38.
</li>
<li id="moh09">Mohamed, A. S., Sapuan, S. M., Ahmad, M. M. H. M., Hamouda, A. M. &amp; Baharudin, B.T. H. T. B. (2009). The effect of technology transfer factors on performance: an empirical study of Libyan petroleum industry. <em>American Journal of Applied Sciences, 6</em>(9), 1763-1769.
</li>
<li id="mus11">Muscio, A., Quaglione, D. &amp; Scarpinato, M. (2011). The effects of universities' proximity to industrial districts on university-industry collaboration. <em>China Economic Review, 23</em>(3), 639-650.
</li>
<li id="nei10">Neininger, A., Lehmann-Willenbrock, N., Kauffeld, S. &amp; Henschel, A. (2010). Effects of team and organizational commitment: a longitudinal study. <em>Journal of Vocational Behavior, 76</em>(3), 567-579.
</li>
<li id="nun94">Nunnally, J. C. &amp; Bernstein, I. H. (1994). <em>Psychometric theory</em> . New York, NY: McGraw-Hill.
</li>
<li id="pil04">Pillai, R. &amp; Williams, E. A. (2004). Transformational leadership, self-efficacy, group cohesiveness, commitment, and performance. <em>Journal of Organizational Change Management, 17</em>(2), 144-159.
</li>
<li id="pod03">Podsakoff, P.M., MacKenzie, S.B., Lee, J.Y. &amp; Podsakoff, N.P. (2003). Common method biases in behavioral research: A critical review of the literature and recommended remedies. <em>Journal of Applied Psychology, 88</em>(5), 879-903.
</li>
<li id="pur10">Purwanti, Y., Pasaribu, N. R. &amp; Lumbantobing, P. (2010). <em>Leveraging the quality of knowledge sharing by implementing a reward program and performance management system.</em> Paper presented at the 2nd European Conference on Intellectual Capital, ISCTE, Lisbon University Institute, Lisbon, Portugal.
</li>
<li id="ram13">Ramayah, T., Yeap, J. A. L. &amp; Ignatius, J. (2013). An empirical inquiry on knowledge sharing among academicians in higher learning institutions. <em>Minerva: a Review of Science, Learning and Policy, 51</em>(2), 131-154.
</li>
<li id="rei06">Reichert, S. (2006). <em>The rise of knowledge regions: emerging opportunities and challenges for universities.</em> Brussels: EUA Publications.
</li>
<li id="san10">Sang, S., Lee, J.-D. &amp; Lee, J. (2010). E-government adoption in Cambodia: a partial least squares approach. <em>Transforming Government: People, Process and Policy, 4</em>(2), 138-157.
</li>
<li id="sch13">Scheible, A. C. F. &amp; Bastos, A. V. B. (2013). An examination of human resource management practices' influence on organizational commitment and entrenchment. <em>Brazilian Administration Review, 10</em>, 57-76.
</li>
<li id="sek10">Sekaran, U. &amp; Bougie, R. (2010). <em>Business research methods for managers: a skill-building approach</em> (5th. ed.). New York, NY: Wiley.
</li>
<li id="seo08">Seonghee, K. &amp; Boryung, J. (2008). An analysis of faculty perceptions: attitudes toward knowledge sharing and collaboration in an academic institution. <em>Library &amp; Information Science Research, 30</em>(4), 282-290.
</li>
<li id="she98">Sheppard, B. H., Hartwick, J. &amp; Warshaw, P. R. (1998). The theory of reasoned action: a meta-analysis of past research with recommendations for modifications and future research. <em>Journal of Consumer Research, 15</em>(3), 325-343.
</li>
<li id="soh09">Sohail, M. S. &amp; Daud, S. (2009). Knowledge sharing in higher education institutions: perspectives from Malaysia. <em>VINE: the Journal of Information and Knowledge Management Systems, 39</em>(2), 125-142.
</li>
<li id="son12">Song, Z. &amp; Chon, K. (2012). General self-efficacy's effect on career choice goals via vocational interests and person-job fit: a mediation model. <em>International Journal of Hospitality Management, 31</em>(3), 798-808.
</li>
<li id="suh06">Suhaimee, S., Abu Bakar, A. Z. &amp; Alias, R. A. (2006).? <a href="http://www.webcitation.org/6Nkn6uoQY">Knowledge sharing culture in Malaysian public institution of higher?education: an overview.?</a> In <em>Proceedings of the Postgraduate Annual Research Seminar 2006</em>, (pp. 354-359). Skudai, Malaysia: Universiti Teknologi Malaysia. Retrieved from http://eprints.utm.my/3367/1/Knowledge_Sharing_Culture_in_Malaysian_Public_Institution_of_Higher.pdf (Archived by WebCite® at http://www.webcitation.org/6Nkn6uoQY)
</li>
<li id="sus11">Susanty, A.I. &amp; Wood, P. C. (2011). <a href="http://www.webcitation.org/6NknazsOf">The motivation to share knowledge of the employees in the telecommunication service providers in Indonesia.</a> <em>International Proceedings of Economics Development &amp; Research, 5</em>(2), V2159-V2162, Retrieved from http://www.ipedr.com/vol5/no2/36-H10117.pdf (Archived by WebCite® at http://www.webcitation.org/6NknazsOf)
</li>
<li id="was05">Wasko, M. M. &amp; Faraj, S. (2005). Why should I share? Examining social capital and knowledge contribution in electronic networks of practices. <em>MIS Quarterly, 29</em>(1), 35-57.
</li>
<li id="wol89">Wold, H. (1989). Introduction to the second generation of multivariate analysis. In H. Wold (Ed.), <em>Theoretical empiricism</em> (pp. vii-xi). New York, NY: Paragon House.
</li>
<li id="yeh12">Yeh, Y.-C., Yeh, Y.-L. &amp; Chen, Y.-H. (2012). From knowledge sharing to knowledge creation: a blended knowledge-management model for improving university students' creativity. <em>Thinking Skills and Creativity, 7</em>(3), 245-257.
</li>
</ul>

</section>

</article>