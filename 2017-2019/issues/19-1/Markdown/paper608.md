<header>

#### vol. 19 no. 1, March, 2014

* * *

</header>

<article>

# Inner circles and outer reaches: local and global information-seeking habits of authors in acknowledgment paratext

#### [Nadine Desrochers](#author)  
École de bibliothéconomie et des sciences de l'information, Université de Montréal, Montreal, QC, H3T 1N8, Canada  
#### [Jen Pecoskie](#author)  
School of Library and Information Science, Wayne State University, Detroit, MI, 48202, USA

#### Abstract

> **Introduction.** This research investigates paratextual acknowledgements in published codices in order to study how relationships inform the information-seeking habits of authors, an understudied group in library and information science.  
> **Method.** A purposive sample consisting of the books from the 2010 nominations list of the Canadian Governor General's Literary Awards was chosen. An in-hand examination of the books was performed to identify the acknowledgement paratext. The sum of these paratextual parts formed the dataset.  
> **Analysis.** A qualitative content analysis of the acknowledgement paratext was performed. Throughout this inductive analysis, memos were used to record the creation and refinement of categories, as well as the emerging results.  
> **Results.** The research reveals that authors rely on people as information resources as well as for moral and emotional support. Sources include personal allies, communities, and publishing staff. Libraries and informational professionals are generally absent from the acknowledgement paratext.  
> **Conclusion.** Authors seek and find support in their local or personal circles and in global or distant horizons. An information model, based on Robert Darnton's communication circuit, is proposed. Information practitioners may be able to build on cues from the acknowledged relationships in order to tailor services to this group of users.

<section>

## Introduction

Commentary and intertextual inclusions have been around for much longer than hyperlinks. In fact, even in its most traditional form, the printed book is generally the container of many more utterances than those which constitute the text proper. As French theorist Gérard Genette ([1997, p. 1](#gen1997)) pointed out, texts reach the reader along with a ‘certain number of verbal or other productions, such as author's name, a title, a preface, illustrations'. The book also generally includes a cover and other elements designed to inform (but also entice) the reader; these elements are designed and added in the hopes that the book will be acquired, read, understood, and appreciated, even recommended. These wrappings of the main narrative, or text proper, form the paratext: a means of stepping into the text. Paratextual elements were compared by Genette to thresholds; in French, _‘seuils'_, used as the title for his 1987 book ([Genette, 1987](#gen1987), [1997, p. 2](#gen1997)). Quoting Philippe Lejeune, Genette further positioned these thresholds as a fringe, one which ‘controls one's whole reading of the text' ([Genette, 1997, p. 2](#gen1997)) and, ultimately, fashions the reading experience by influencing the reader's perception of the text.

In the printed book format, the paratext is divided into two distinct categories, made clear in Genette's ([1997, p. 5](#gen1997)) formula: ‘paratext = peritext + epitext'. The peritext consists of the elements that fall outside of the text proper but are included within the codex (the book-as-object): front and back matter, illustrations, introductions, prefaces, notes, tables of content, blurbs, dedications, etc. The epitext, on the other hand, is composed of utterances surrounding the text but created and made public outside of the book: reviews, marketing inclusions in media, interviews, etc.

This research forms phase two of a project investigating the information and meaning communicated through the paratext of a set of codices. The project used a sample consisting of the 2010 Governor General's Literary Awards list of nominated and award-winning codices, in all categories and in both official languages of Canada ([Canada Council for the Arts n.d.](#can)). This paper therefore builds on a previous analysis of the sample studied here ([Pecoskie and Desrochers, 2013](#pec2013)). Findings from the first phase focused on the peritext for its role as a record of information conveyed by authors and publishers contained within the book-as-object. It situated the book as a portal, a point of access leading to information on the creators' information-sharing habits.

The first phase analysed the peritext of the sample as a complete dataset. While it was not its main focus, this initial analysis revealed a strong penchant for the mention of people, in part due to the established convention of acknowledging help or support received. This emerging theme was therefore explored through a second phase of qualitative analysis, using only the acknowledgment peritext as the dataset. ‘Acknowledgments', in the paratextual context as well as in the context of this study, are understood to be comprised not only of sections bearing that title, but of any form of peritext which includes a statement of thanks, the recognition of a debt, or an expression of gratitude. This outlook accounts for differences in practices and amongst cultures, as identified by Genette himself ([1997, p. 212](#gen1997)).

In this second phase, the analysis focused on the three aspects of the acknowledgement peritext which offered a direct link to the research questions: people as sources in the writing process, the grouping of these people into categories based on their contributions, and the mention, if any, of information professionals. Findings from this second phase show that support and personal connections frequently outweigh professional services or the provision of services aimed at generating specific results, at least in terms of the value given in acknowledgments. This movement results not only in the reshaping of information-sharing habits, but in an intricate array of relationships (personal and professional, intimate and public) that shape the solitary act of writing into the more communal act of cultural production. These relationships create a continuum between the inner circle of the author (the local sphere) and encounters with the public (or global) sphere. The notably sporadic and vague mentions of information professionals as key providers of support is an opportunity to consider where in this local-global continuum information professionals may find more pertinent ways to intervene. A theoretical framework based on the works of Genette and book historian Robert Darnton help inform the findings by providing the tools with which to question the people-related information-seeking trends found within the acknowledgment paratext. An information model, based on Darnton's communication circuit ([1982, p. 68](#dar1982)) is proposed.

## Literature review

### The study of the paratext

The rich framework offered by Genette's paratext theory has brought it from its original field, literary studies, to applications in many other disciplines. Pecoskie and Desrochers ([2013](#pec2013)) offer a literature review of the use of the paratext in research in library and information science, further pointing to its use in the study of books and print culture ([Mak, 2011](#mak2011)), at times in very culturally specific contexts ([Fisher, 2011](#fis2011)), and to its export to media studies ([Gray, 2010](#gra2010)). It is now being used to study e-readers as well as the e-reading experience ([McCracken ,2013](#mcc2013)).

Specific types of peritext have also been studied. Fiction book covers have been examined for the messages they transmit to readers, publishers, and marketers ([Matthews and Moody, 2007](#mat2007)). Cronin and La Barre ([2005](#cro2005b)) focused on _blurbing_ (using advance praise statement) in non-fiction. O'Connor and O'Connor ([1998](#oco1998)) positioned the book jacket as a user-centred access mechanism, creating a direct link between the peritext and library services.

Cultural conventions surrounding the giving of thanks through the acknowledgement peritext were analysed by Cronin ([1995](#cro1995)) in _The scholar's courtesy_. Cronin, along with various collaborators, reiterated the cultural significance of these utterances in subsequent studies ([Cronin, Shaw and La Barre 2003](#cro2003); [Cronin _et al._, 2004](#cro2004); [Cronin, 2005](#cro2005a); [Cronin and Franks, 2006](#cro2006)). These studies converge in establishing that these utterances act not only as statements of debt or gratitude, but also as a testimony to academic conventions and accolades, thereby offering insight into how members of this community form ties and relationships through the culturally ratified research and publication processes.

Salager-Meyer _et al._ ([2011](#sal2011)) analysed the use of acknowledgements in medical articles published in five countries over an extended time period, from 1950 to 2010\. They found that acknowledgement practices differ depending on language and geographic location, over time, and between academic genres. Funding acknowledgements seen in academic databases have also been the focus of research ([Costas and van Leeuwen, 2011](#cos2011); [Wang and Shapira, 2011](#wan2011)).

Another direct link between the paratextual theory regarding acknowledgements and library and information services was made by Laurie Scrivener ([2009](#scr2009)), who argued that library and archival practices could be enhanced by the study of dissertation acknowledgements.

### Persons as sources in the field of cultural production

Though it has yielded somewhat of a divide, the field of information studies has been studying the information behaviour and practices of scholars and scientists since the early 1960s. As noted above a strong emphasis on academia persists ([Fisher and Julien, 2009](#fis2009); [Julien, Pecoskie and Reed, 2011](#jul2011)). The writing world is, of course, much broader than the academy and some scholars have called for a research corpus on the complex connections between the creative process and information behaviour ([Hemmig, 2008](#hem2008); [Case, 2007](#cas2007); [Paling, 2006](#pal2006); [Russell, 1995](#rus1995)).

Fiction authors, interestingly, have been given sparse attention in library and information science while many studies have examined the information sources of other creative groups, such as art researchers ([Layne, 1994](#lay1994)), university artists and teachers ([Van Zijl and Gericke, 2001](#van2001); [Reed and Tanner, 2001](#ree2001)), and student artists ([Frank, 1999](#fra1999)). Marini ([2007](#mar2007)) offered a librarians' and archivists' view of the practices of theatre professionals, while Davies and McKenzie ([2004](#dav2004)) showed how the theatrical prompt book acts as an information output. Davies ([2007](#dav2007)) analysed practices linked to reading, classifying, inscribing, gathering, representing, remembering, and standardizing among a group of theatre artists.

Where authors are concerned, Paling's work ([2006](#pal2006), [2008](#pal2008), [2009](#pal2009), [Paling and Martin, 2011](#pal2011b)) is a notable exception. Using a framework based on the Bourdieusian concept of consecration, it presents a strong focus on literary authors' use of social informatics and information technologies, echoing a study by Svensson ([1991](#sve1991)) on writers' use of word processors. Paling ([2011](#pal2011a)) also surveyed members of the American literary community on their means of discovering new works of literature, in libraries or elsewhere, in order to determine which metadata they might find most useful. Shannon Russell's ([1995](#rus1995)) exploratory study remains the most specific source on the information habits of fiction authors, as it offers some insight into the typology of sources they use as well as their relationship with library services; people as experts sources and writers' organizations are briefly mentioned.

The importance of persons as sources emerges in other research. Cobbledick ([1996](#cob1996)) studied the information needs of practicing visual artists and found that the information used originated from a variety of sources, including non-art-related sources, library browsing, print sources and interpersonal contact. [Cowan's 2004](#cow2004) study of a visual artist's inspirational sources found that these included the natural environment and the work itself, but also relationships and self-inquiry. Mason and Robinson ([2011](#mas2011)) explored the information habits of emerging artists and found that the sources used included the Internet at large as well as social networks. Medaille's [2010](#med2010) study of theatre artists demonstrated that they rely heavily on traditional print sources, interpersonal networks and professional connections.

## Theoretical framework

### Paratextuality as communication

Communication theorist James Carey ([2009, p. 20](#car2009)), in discussing cultural formation and collective understanding, spoke of the power of messages when he wrote, ‘reality is brought into existence, is produced, by communication - by, in short, the construction, apprehension, and utilization of symbolic forms'. Using Genette as a point of reference, we can see that the paratext exists in a ‘situation of communication', between the author (or representative, or collaborator, or commentator) and the reader ([Genette, 1997, p. 8](#gen1997)), which guides some of its characteristics. One of these is for the message conveyed to have what Genette describes as an ‘illocutionary force'; this strong semantic image posits that the paratext can, indeed should, impact the reader (p. 8). This impact can only happen if both parties understand and accept the aforementioned ‘situation of communication'. This explains why acknowledgment practices within the paratext vary from culture to culture, following trends in labeling and placement identified by Genette himself (p. 212).

This theoretical framework shows that acknowledgements can bring to light the reality they help to shape and to expose: personal and contextual practices and trends, as well as relationships (within the publishing world, with libraries, with the readership, etc.). They can also be understood as information, since they can alter their reader's context or reality ([Case, 2007](#cas2007), [2012](#cas2012)).

### Paratextuality as a potential path to metatheory

The field of information-seeking studies is bound by two main concepts, information behaviour and information practices. Although both focus on how people ‘deal with information' ([Savolainen, 2007, p. 126](#sav2007)), there is a discursive and theoretical divide between them. Savolainen ([2007](#sav2007)) posits that the information behaviour approach focuses on the cognitive stance while information practice is based in social constructionism; yet Wilson (in [‘_The behaviour/practice debate_', 2009](#the2009)) notes that human behaviour is, in fact, ‘_composed of cognitive, physical and social activities_'. Case adopts the term ‘_information behavior_'; yet he also speaks of a ‘_theoretical sprawl_' ([2012, p. 196, 372](#cas2012)) in information behaviour research and positions ‘_information practices_' as a synonym, albeit with ‘_some differences_' to ‘_information behavior_', adding that it is more readily used in European and Canadian settings ([2012, p. 5](#cas2012)).

Savolainen ([2007](#sav2007)) suggests the potentially beneficial use of metatheoretical perspectives in order to bridge the behaviour and practice divide when conducting research. Bates ([2005, p. 2](#bat2005)) defines metatheory as ‘_the philosophy behind the theory, the fundamental set of ideas about how phenomena of interest in a particular field should be thought about and researched_'. While paratextual theory cannot claim metatheoretical status, since it is not ‘theory about theory' ([Case, 2012, p. 164](#cas2012)), it certainly exists at a convergence point between individual expression and socially-constructed constraints. Pierre Bourdieu ([1996](#bou1996)) exposed this tug-of-war in _The rules of art_ by illustrating how the author, can use writing to achieve emancipation from societal rule whilst espousing the rules of the literary field in order to achieve success. The acknowledgement paratext reflects this dual position, by placing very personal, intimate messages adjacently to the socially-expected, even obligatory, show of thanks.

Therefore, using the acknowledgement peritext (and perhaps other parts of the paratext) as a lens for the study of information-seeking habits may be relevant to both the information behaviour and information practices approaches. On the one hand, acknowledgements bear witness to information behaviour, since they contain traces or records linked to the ways contributors seek and interact with information in relation to queries that are ‘seen to be triggered by needs and motives' ([Savolainen, 2007, p. 126](#sav2007)); on the other hand, these paratextual utterances reveal information practices, as they bear witness to the ways information activities are _‘affected and shaped by social and cultural factors_' (p. 126).

This paper seeks neither to resolve the theoretical divide nor to dismiss the importance of the debate. It simply seeks to explore one potential avenue for a discussion between what Savolainen ([2007](#sav2007)) calls the two ‘_umbrella concepts_'. The authors therefore use either _behaviour_ or _practices_ or both, while weighing each use; in other instances, syntagms formed of such terms as _information-seeking_ or _information-sharing_ and _habits_ or _trends_ are suggested as pertaining to the findings from the dataset and any new theory emerging from it.

### Robert Darnton's communications circuit

Darnton ([2009](#dar2009)) asserts the importance of the physical book-as-object, even in this digital age. In his earlier work ([1982 p. 67](#dar1982)), he proposed a ‘_general model for analyzing the way books come into being and spread through society_'. This model includes the wider system of the production process, a ‘_communications circuit_' ([Darnton, 1982, p. 68](#dar1982)) that can also be described as an articulation of the cultural field of writing, publishing, and reception as related to Bourdieu's ([1996](#bou1996)) analysis of the literary field. The circuit follows the full cycle from authors to readers and back again and includes publisher, printers, suppliers, shippers, and booksellers, among other players. The author, in this model, begins the circuit and the reader completes it; this final relationship comes with the added mention that, as Darnton puts it, ‘_Authors are readers themselves_' ([1982, p. 67](#dar1982)).

Expertise, in the circuit, exists as intellectual influence which pervades the whole sphere. The infrastructure supporting book production is presented from a historical perspective within the context of its intrinsic relation with ‘other systems, economic, social, political, and cultural, in the surrounding environment' ([Darnton, 1982, p. 67](#dar1982)). These systems and the environment that hosts, creates and hones them, influence all actors in the model. Although Darnton's circuit was not intended to be read with a top-down approach, and should instead be seen as a holistic model, according to the author's intention, it is revealing that the beginning and top of the model involves the contributors (author and publisher), therefore placing all others in the literary field as subordinate to these actors. Furthermore, the ‘readers' category, while it connects back to the author, does so through a dotted line, while other relationships are expressed through solid lines; this dotted line thereby seems to mark a somewhat weaker relationship.

The potential for slight twists in the holistic presentation is especially interesting when looking at the placement of libraries in the model. The same ‘readers' category includes the sub-categories of purchasers, borrowers, clubs, and, lastly, libraries. It can therefore be said that within Darnton's circuit, libraries, though not ignored, are presented as agents of somewhat lesser significance, holding little influence within the literary field, and are hence given limited focus. This, along with the reader-author relationship, will be of particular interest given the findings reported here.

## Method

As noted above, this paper builds upon a previous examination of a purposive sample, consisting of the 2010 nominations list of the prestigious, peer-bestowed Canadian Governor General's Literary Awards ([Pecoskie and Desrochers, 2013](#pec2013)). These awards are given to creators (authors, illustrators, translators) of books in seven categories: fiction, literary non-fiction, poetry, drama, children's literature (text), children's literature (illustration), and translation ([Canada Council for the Arts n.d.](#can)). Since Canada has two official languages, the process is the same for each language, making the sample bilingual: thirty-five titles in English and thirty-four titles in French, for a total of sixty-nine books (Turcotte was nominated in two categories, bringing the total down from the usual seventy books. See [Appendix 1](#appendix1) for a list of the books). All the books were accessed through national, research, or public libraries. The French content of interest to the research was translated by one of the researchers who is a professional, published, and produced translator of drama.

In this second phase, the researchers examined acknowledgement peritext. They analysed the following paratextual elements in order to identify all acknowledgement-related inclusions: covers, and front and back matter (e.g., title page, title page verso, dedications, acknowledgements, genesis statements, epigraphs, introductions, notes, and references). The text proper was excluded from the dataset.

A qualitative content analysis of the acknowledgement peritext was conducted through an in-hand examination of the books. This naturalistic method ([White and Marsh, 2006](#whi2006)) uses inductive analysis that allows the codes to emerge from the dataset. The analysis was directed in its approach, since it was both guided by the first phase's findings and used refined, pre-existing research questions ([Hsieh and Shannon, 2005](#hsi2005)). The inductive coding was also compared to a listing of types of help given to authors found within Genette's work ([1997](#gen1997)). However, the labels provided by Genette were not used, either because they did not match the data or because the terms were dated. This still allowed for further scrutinizing of the data and the fine-tuning of the codes. No initial hypothesis was proposed and the research questions were:

1.  What can the acknowledgement peritext reveal of people as information sources in the writing process?
2.  Can the people identified as information sources in the acknowledgement peritext be categorized, and if so, how?
3.  In what ways are information professionals mentioned in the acknowledgement peritext?

In should be noted that this is not a quantitative study and that, for example, the categories which are referred to in the second question were not established by type or profession, but rather in terms of their existing or developing relationship to the author. The constant comparative method ([Glaser and Strauss 1967](#gla1967)) permitted the codes to be established and refined in a coding manual as the analysis advanced, with memos used to maintain consistency. Both researchers engaged with the data over long periods and through repeated readings; coding categories were often revisited and revised, giving credibility to the final codes ([Lincoln and Guba, 1985](#lin1985)).

The findings are presented through quotations and examples, an established reporting style in qualitative studies ([Krippendorff, 2004](#kri2004), [2013](#kri2013)). Thick, or detailed, description is further used in order to provide context for the reader ([Geertz, 1973](#gee1973); [Patton, 2002](#pat2002)).

Finally, although Darnton's ([1982](#dar1982)) model was used as a point of reference, the relationships examined here were assessed from an information-sharing perspective. The proposed model, while inspired by the relationships highlighted by Darnton, is not meant as a reflection of the current book industry, but rather as a visualization of the flow of information exchanges.

## Findings

From the dataset, it is clear that authors make use of a variety of persons as information sources and experts. Through study and analysis of the data, a local-to-global continuum emerges; the term continuum is understood here as a continuous and, in varying degrees, seamless motion or shift between the local and global spheres. Indeed, the writing process seems to render permeable the boundaries between the intimate, or local, realm of the author and the public, or global, field of creation. The flux works in both directions, as the inner circle is revealed to the public and previously unknown influences become embedded in the authors' personal and creative circles through the outreach for information or support. Together, these motions shed light on the development of local and global relationships pertaining to personal writing practices, which then fall, in the scope of this study, into four categories: personal allies, communities, publishing staff, and librarians and information professionals.

### Personal allies

Individuals are often named in the acknowledgments from the sample, as one might expect. These individuals are thanked for a variety of reasons which are outlined in Table 1.

<table id="tab1"><caption>

**Table 1\. Books featuring acknowledgements of contributions from individuals**</caption>

<tbody>

<tr>

<th>

Author, _Title (short form)_/Language, GG Category</th>

<th>Individuals offering a skill or knowledge</th>

<th>Individuals as sources of inspiration</th>

<th>Individuals as sources of support</th>

<th>Individuals sharing an experience</th>

<th>Individuals as early readers</th>

<th>Individuals thanked for reasons undefined or cryptic</th>

</tr>

<tr>

<td>

Abbott, _Marriage_/English, non-fiction</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Birdsell, _Waiting_/English, fiction</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Blais, _Mai_/French, fiction</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Brown, _Boy_/English, non-fiction</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Casey, _Lakeland_/English, non-fiction</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Connelly, _Burmese_/English, non-fiction</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Denman, _Me_/English, children's text</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Desjardins, _Maleficium_/French, fiction</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Donoghue, _Room_/English, fiction</td>

<td>X</td>

<td> </td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

English, _Just_/English, non-fiction</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Fairfield, _Tyranny_/English, children's text</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Flett, _Michif_/English, children's ill.</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Fortier, _Proper_/French to English transl.</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

</tr>

<tr>

<td>

Greene, _Boxing_/English, poetry</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Haché, _Trafiquée_/French, drama</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Healey, _Courageous_/English, drama</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Heath, _Sale_/English to French transl.</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Lavoie, _Seigneurie_/French, non-fiction</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

McAndrew, _Majorités_/French, non-fiction</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

McMurchy-Barber, _Free_/English, children's text</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Noël, _Nishka_/French, children's text</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Phillips, _Fishtailing_/English, children's text</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Pool, _Exploding_/English, poetry</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Rainfield, _Scars_/English, children's text</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Robitaille, _Chenil_/French, children's text</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Siebert, _Deepwater_/English, poetry</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Taylor, _Motorcycles_/English, fiction</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Thompson, _Such_/English, drama</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Toews, _Troutman_/English to French transl.</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Tremblay, _Blue_/French to English transl.</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Tremblay, _Dernier_/French, children's text</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Turcotte, _Rose_/French, children's ill. and text</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Warren, _Cool_/English, fiction</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Winter, _Annabel_/English, fiction</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Yee, _Lady_/English, drama</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**Number of titles featuring this type of acknowledgement**</td>

<td>

**25**</td>

<td>

**11**</td>

<td>

**22**</td>

<td>

**9**</td>

<td>

**9**</td>

<td>

**12**</td>

</tr>

</tbody>

</table>

The data show that the individuals thanked may offer precise, time-specific help in the form of a skill or knowledge; they may also act as informants by offering testimony to their own experience or as readers of early versions of the work. A few people are thanked in poetic or cryptic ways, making it difficult to pinpoint their exact contribution, which does not in any way make it any less valuable. Along the same lines, individuals are often thanked for providing inspiration and support for the writing process, revealing that information needs and emotional needs are often intertwined, and celebrating the strength of the relationships, pre-existing or newly formed.

### Known personal allies

Sources of support are often identified as individuals who offered guidance perceived by the author as essential to the process of writing. These include family and friends, with this common wording being, in fact, used _verbatim_ by Allan Casey ([2009, p. xi](#cas2009)), who writes, ‘_Without the advice and support of family and friends, I could not have finished this task_'. Poet Melanie Siebert ([2010](#sie2010)) speaks of another type of relationship, the teaching relationship, as one that both brings information to the process and supports this process through guidance. She uses acknowledgments to create a link between the personal nature of these relationships and the professional or creative support they can bring. The known persons she names include three poets, Tim Lilburn, Lorna Crozier, and Patrick Lane; in her acknowledgement paratext she classifies them as ‘_wise teachers, steady guides, always_' ([Siebert, 2010, p. 87](#sie2010)). Through this characterization, Siebert offers a dual statement: first, the known and close individuals provide professional guidance through the teaching of a craft in which they hold expertise; second, the privileged nature of this relationship has effects in the more intimate realms of support and motivation. This is realized through faith in the student and embodied in the work itself, a work which can pay tribute to the relationship in an explicit way through the paratext.

Other types of expertise, found in an author's inner circle, can be noted. Emma Donoghue ([2010, p. 323](#don2010)) acknowledges her ‘_brother-in-law Jeff Miles for his unnervingly insightful advice on the practicalities of Room_'. _Room_ is a work of fiction whose themes include the bond between mother and child, but also the construction of confined spaces, human imprisonment, as well as anxiety and post-traumatic stress.

Some authors focus their acknowledgements on the inspirational help they received from various persons as direct contributions to the genesis of the work. Cheryl Rainfield's work _Scars_ is about a gay teen survivor of sexual abuse who uses self-harm to cope with the trauma. Rainfield ([2010, p. 247](#rai2010)) writes about one named individual, _‘who is like the kind, nurturing mom I never had and who is the most loving person I know... I couldn't have written some of the caring characters and interactions in this book without having known you first_'. Turcotte and Sylvestre ([2009](#tur2009)), whose children's book earned them a dual win in the text and illustrations categories, thanked one individual for inspiration and specific contributions through a highly original spiral design. Alain Ulysse Tremblay discusses his father, a painter whose work he observed and emulated before turning to writing. Of his father as a source of inspiration, he says, ‘_I came to painting through the eyes of my father. And when I write instead of paint, colours emerge from the words. I owe a lot to this father, against whom, however, I so bitterly fought during my adolescence_' (researchers' translation of ‘_Et quand j'écris au lieu de peindre, ce sont des couleurs qui surgissent des mots. Je dois beaucoup à ce père contre qui, pourtant, j'ai si âprement lutté durant mon adolescence_'; [Tremblay, 2009, p. 160](#tre2009a)).

Tremblay's testimony shows just how complex or even tumultuous the ally relationship can be; it also shows how this complexity may, in turn, become part of the creative process, albeit with some critical distance. This emotional issue notwithstanding, close family members can be part of an inner circle that offers more to the authors than base support, and they are often perceived as key influences in the actual formation of the creative idea or the final product, or both.

### Personal allies met through the research process

While the peritext reveals that authors readily acknowledge those individuals closest to them as aiding their writing practice, it further reveals that authors often look outside of their intimate inner circle for information sources. For example, many authors acknowledged personal informants for sharing their first-hand, lived experiences through story and memories, positioning them as key information sources in the production of the work. K.L. Denman ([2009, p. n.p.](#den2009)) acknowledges a ‘_wonderful young man and his mother who were willing to speak to [her] about their experiences with psychosis_'. In a similar way, the background research done by John English for his biography of now-deceased politician and former Canadian Prime Minister Pierre Elliott Trudeau (_Just watch me: the life of Pierre Elliot Trudeau, 1968-2000_) is based in facts, but also in human memory through the individuals who chose to share their remembrances as the author researched his subject. English ([2009, p. 749](#eng2009)) notes that his informants included the Trudeau family and ‘_Many friends, colleagues, and associates... My sources for this book are, therefore, often personal memory and impressions, formed long ago but mediated by time and historical interpretation_'. English's acknowledgments are revealing in that they recognize the importance of information derived from the first-hand experiences of newfound allies to his research, yet they also reflect on the effects of memory on the reconstruction of the story and testify to the limitations inherent to these encounters.

Expertise emerges as a recurring theme as authors speak of their process-related encounters. There are multiple mentions of technical skills, including computer or software help, such as help with software and applications, whether Word ([Rainfield, 2010](#rai2010)) or Photoshop ([Fairfield, 2009](#fai2009)). Michel Lavoie ([2010](#lav2010)) acknowledges a geographer who drew maps for the work. The role of the translator is also acknowledged in the paratext, as Lavoie ([2010 p. 499](#lav2010)) names and thanks a translator who ‘_succeeded in making the original English citations clear and was faithful to them_' (researchers' translation of the original French, ‘_qui a très bien rendu l'esprit et la lettre des citations originales anglaises_'). The acknowledgement utterances in _Burmese lessons: a love story_ ([Connelly, 2009](#con2009)) and _A history of marriage_ ([Abbott, 2010](#abb2010)) include thanks for translation expertise in the research and writing of the manuscript. Further to this theme, but with a slightly more artistic slant, Dominique Fortier ([2010, p. 267](#for2010)), author of _On the proper use of stars_, thanks the translator ‘_who agreed to lend her talent to the task_' of translating the work from French to English.

It should be noted that in certain cases, the translation process permits the addition of the translator's thanks to those of the author. Translators Lazer Lederhendler of _The breakwater house_ ([Quiviger, 2010](#qui2010)) and Linda Gaboriau of _Forests_ ([Mouawad, 2009](#mou2009)) offer their respective acknowledgements to individuals for support. This can even create an interesting flow back to the author: Gaboriau, for instance, thanks author Mouawad for ‘_entrusting me with this adventure_'.

People with specific knowledge were cited for providing content that supported the authors' exploration of the subject matter or its expression in the work. Some of these citations do not articulate the specific role of the informants, but discuss instead the contribution itself. For example, in the English-to-French translated work, _Sale argent: petit traité d'économie à l'intention des détracteurs du capitalisme_ ([Heath, 2009](#hea2009); translated by Saint-Martin and Gagné), the author names two people who provided specific semantic contributions in the form of two separate analogies that guided the work; however, he does not offer information on what credentials these people held or in what capacity they acted. This omission notwithstanding, many other informants' names are present in the paratext of various codices, along with their role and professional designations (or a clear label which places them as experts in specific fields). For example, Elizabeth Abbott ([2010](#abb2010)), in the English non-fiction title _A history of marriage_, acknowledges a historian who provided her with supplementary sources for use in the work.

Whether known or met through the actions and informational process of the authors, the acknowledgements show that individuals are being perceived as essential allies, acting as sources for helping with the writing practice of authors, often in terms of providing specific information or inspiration. Those who are known intimately by the author then become known to the public through published acknowledgement within the paratext; they go from the local to the global sphere. Those who are met through the creative labour process travel both ways: while they enter the local sphere as a result of the information-seeking process, they also rejoin the global sphere through the inscription and acknowledgment of their contributions in the subsequent publication process.

### Communities

Within the acknowledgement paratext, there is evidence of interactions with communities. Like individuals, communities provide information for the authors, support for their writing practice, or both (see Table 2). While the publishing staff may also count as a form of community, it is treated separately.

<table id="tab2"><caption>

**Table 2\. Books featuring acknowledgements of contributions by communities**</caption>

<tbody>

<tr>

<th>

Author, _Title (short form)_/Language, GG Category</th>

<th>Communities of experts offering a skill or knowledge</th>

<th>Communities as sources of inspiration (NB: XX indicates the reading public)</th>

<th>Communities as sources of support</th>

<th>Communities of informants sharing an experience</th>

<th>Communities as readers of early versions (NB: XX indicates writing group)</th>

</tr>

<tr>

<td>

Abbott, _Marriage_/English, non-fiction</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Brown, _Boy_/English, non-fiction</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Casey, _Lakeland_/English, non-fiction</td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Connelly, _Burmese_/English, non-fiction</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td>XX</td>

</tr>

<tr>

<td>

English, _Just_/English, non-fiction</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Fairfield, _Tyranny_/English, children's text</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Flett, _Michif_/English, children's ill.</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Greene, _Boxing_/English, poetry</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>XX</td>

</tr>

<tr>

<td>

Gruda, _Onze_/French, fiction</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Haché, _Trafiquée_/French, drama</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Mc Andrew, _Majorités_/French, non-fiction</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

McMurchy-Barber, _Free_/English, children's text</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Mouawad, _Forests_/French to English transl.</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Nepveu, _Verbes_/French, poetry</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Noël, _Nishka_/French, children's text</td>

<td> </td>

<td>XX</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Phillips, _Fishtailing_/English, children's text</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Rainfield, _Scars_/English, children's text</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>XX</td>

</tr>

<tr>

<td>

Rivard, _Idée_/French, non-fiction</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Robitaille, _Chenil_/French, children's text</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Siebert, _Deepwater_/English, poetry</td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Toews, _Troutman_/English to French transl.</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Tremblay, _Blue_/French to English transl.</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Winter, _Annabel_/English, fiction</td>

<td> </td>

<td>XX</td>

<td> </td>

<td> </td>

<td>XX</td>

</tr>

<tr>

<td>

Yee, _Lady_/English, drama</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**Number of titles featuring this type of acknowledgement**</td>

<td>

**5**</td>

<td>

**7**</td>

<td>

**9**</td>

<td>

**4**</td>

<td>

**5**</td>

</tr>

</tbody>

</table>

### Existing communities

Just as [Table 1](#tab1) shows that the acknowledgement paratext testifies to the contribution of helpful feedback by readers of the manuscript, Table 2 reveals the specific importance, for certain authors, of established collectives and writing groups. These groups create an environment that aids the craft and can offer the impression of writing in parallel with others. Kathleen Winter ([2010, p. 463](#win2010)), in the acknowledgements of _Annabel_, thanks the writing group ‘_enginistas_' and clearly names four women. Cheryl Rainfield ([2010](#rai2010)), author of _Scars_, discusses her connections to multiple critique groups whose membership includes other authors; she specifically alludes to one online e-group of young adult writers. She thanks the various members of these groups for _‘giving me good feedback_' and ‘_for reading multiple drafts over the years_' ([Rainfield, 2010, p. 248](#rai2010)). Elizabeth Abbott ([2010, p. xii](#abb2010)) offers thanks to a manuscript focus group by saying, ‘_you are great readers and talkers, and I learned so much from each of you!_'

The drama genre offers a particular angle in this regard. Indeed, given the dual nature of the genre (as a textual work, suitable for reading, and as a canvas for an eventual performance), multiple titles provide general acknowledgement and credits of the original stage production, which often precedes the published version. However, of the ten titles in the drama category, only one clearly offers thanks to the people who worked on the original stage production for help in bringing the work to the book format. In his acknowledgement paratext, playwright David Yee ([2010](#yee2010)) mentions his creative team ‘_whose creativity and generosity were instrumental to the play's final evolution_'. Interestingly, he also writes, ‘_I loathe sentiment, but I do owe a tremendous debt to the people who championed this play. They were, in the end, louder than those who tried to shout it down_'. While it is impossible to establish whether all participants in the production were already part of the author's professional circle at the time of creation, his two-fold acknowledgement of their role clearly indicates the importance of the theatre community that supported him.

Such statements document the evidence of fully formed or structural networks and reflect the specific type of support provided to authors by communities that they both helped to build and towards which they feel a sense of belonging. Here, the authors are participating members of the group and intrinsic elements of these established communities.

### Communities encountered or formed

An author's readership is also, of course, a community. Two authors used paratextual acknowledgements to thank the wider reading public and to document the relationship they have (or had) with previously unknown individual readers, and the way it may have influenced the writing process. Kathleen Winter ([2010 p. 463](#win2010)) provides the only instance of a straightforward yet general expression of gratitude towards the reading public with her statement, ‘_And thank you, dear reader_'. This statement illustrates an ethic of care for those readers who will consume the text (and peritext, in which they will see themselves being addressed directly), all the while contributing to the livelihood and success of the author. More pointedly, Michel Noël discusses the importance of the reader as provider of information and direct influence on his writing practice. He indicates that the genesis of his novel _Nishka_ stemmed from his encounters with previously unknown readers, who provided the impetus for this sequel to a previous work. He documents how, after the publication of his previous book, readers contacted him with so many questions about and concerns over the characters that he decided to provide them with answers. He writes, ‘_I therefore wrote _Nishka_. I now realize that their [the characters'] life is dense and that there is still a lot to say about them!_' (researchers' translation of ‘_J'ai donc écrit Nishka. Et je prends aujourd'hui conscience que leur vie est dense et qu'il y a encore beaucoup à raconter à leur sujet!_'; [Noël, 2009, p. 9](#noe2009)).

Authors described other types of communities, such as those they encountered as part of their writing practice but in which they did not thoroughly integrate themselves. Allan Casey travelled across Canada as part of his research for _Lakeland_ and speaks of the sources he met as a group, a unit, voices meshed into one information source: ‘As to sources, I am indebted to many scores of people, not all of whom I can thank by name. Often the briefest casual encounters yielded valuable information. My deepest thanks go to all those people who shared their knowledge and passion for _their_ lake' ([Casey, 2009, p. x](#cas2009); emphasis in original). These individuals therefore make up a community of knowledge, and the author acknowledges both this group of likeminded people and their stories as essential to the development of his award-winning non-fiction work. Similarly, Karen Connelly, author of the English language non-fiction title, _Burmese lessons: a love story_, encountered groups who acted as information sources for her work based on the lives of people who struggle on the Thai-Burmese border. She writes, ‘_I am grateful to everyone, without exception, who appears in these pages, but I am especially grateful to Burmese friends, colleagues and strangers who told me their stories and sometimes made me promise to publish them_' ([Connelly, 2009, p. 464](#con2009)). After sharing their discussions, memory, and narratives, geographically distant groups who had ties to the political situation in Burma became local to Connelly through her own understanding of these essential sources and their plight. The fact that these sources are documented in the paratext is significant because it shows that inspiration or genesis sources become information sources through the research and writing processes. The authors weave these experiences and interactions together with other people's perceptions and input, folding the information they glean from them into their own creative labour.

In the memoir _The boy in the moon_, author Ian Brown writes about life with his son Walker, who, at the age of thirteen, had the mental capabilities of a three-year-old. Brown ([2009, p. 291](#bro2009)) acknowledges that, throughout his journey and writing process, he used experts ‘_to explain the complications and implications of Walker's condition_'. These expert information resources included medical doctors and nurses, youth service workers, teachers, and a representative from L'Arche Canada, an organization for those with developmental disabilities. Together, they form a group, a community of expertise and support. Similarly, Julie Flett ([2010](#fle2010)), author and illustrator of _Owls see clearly at night: a Michif alphabet_ , written in English and Michif (a mixed Cree-French language of the Canadian Métis) acknowledges contributions from language and linguistics experts by noting their role in the Métis community or in established places of higher education: Native Elder Grace (Ledoux) Zoldy, Métis language activist Heather Souter, MA in Michif Language and Linguistics (University of Lethbridge), and Dr. Nicole Rosen, Assistant Professor in the Department of Modern Languages, University of Lethbridge. Placing these types of detailed credentials in the acknowledgement utterances reassures the reader that the translations within the book have been evaluated and verified as authentic, complete, and suitable for use. These paratextual utterances inform the reader about the information or expertise provided and indicates _why_ these people were suitable informants; furthermore, presented together, they form a community of knowledge, giving the author's work a well-rounded, validated credibility.

In fact, the acknowledgement process in itself creates communities, as writers bring together the people who supported their work in various ways. For example, in _The proper use of stars_, the author writes of three early readers of the manuscript and how ‘_their comments helped improve it considerably_' ([Fortier, 2010, p. 267](#for2010)). This is a different type of information strategy, one that integrates a perceived expert or at the very least a valued opinion into the text being written. The fact that the readers are perceived as a group is telling: the comments, as information on the status and quality of the text, are at the core of the interaction and weave the three readers into a community. Emma Donoghue ([2010](#don2010)) identified early readers as her partner and her agent; placing these two readers together show that an author's support community can be made up of widely differing individuals, and that the nature of their connection to the author can vary broadly.

Interestingly, two types of groupings were found ([see Table 3](#tab3)). Individuals may be listed without any clue as to why they are featured together, other than through a general sense of gratitude. More frequently, individuals are assembled in groups, or even subgroups, which clearly show that it is their shared contribution that answered one of the author's specific needs, whether informational or emotional. Martine Desjardins ([2009](#des2009)) offers a prototypical example in the paratext of the work of fiction _Maleficium_. Her acknowledgements are divided in seven sections, ending with thanks for a fox-terrier and one named individual, each meriting their own sentence. The first five sections, however, introduce named individuals by category: editor and publishing staff, first readers, the team from the magazine _L'actualité_, people who enquired about the progress of the book, and a group formed of one person who provided a photograph _‘among other things_' (researchers' translation of ‘_entre autres pour la photo_') along with the author's sisters and parents. These categories are labelled and followed by the list of names which comprises them. Such lists thereby form subgroups within the author's overarching community of knowledge-sharing and support. This practice, found in quite a few titles, adds to the types of communities thanked in the sample.

<table id="tab3"><caption>

**Table 3\. Books featuring communities formed by listing individuals**</caption>

<tbody>

<tr>

<th>

Author, _Title (short form)_/Language, GG Category</th>

<th>Communities as list of individuals thanked without details</th>

<th>Communities as lists of individuals thanked for similar contribution</th>

</tr>

<tr>

<td>

Abbott, _Marriage_/English, non-fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Brown, _Boy_/English, non-fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Casey, _Lakeland_/English, non-fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Connelly, _Burmese_/English, non-fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Desjardins, _Maleficium_/French, fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Donoghue, _Room_/English, fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

English, _Just_/English, non-fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Flett, _Michif_/English, children's ill.</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Fortier, _Proper_/French to English transl.</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Haché, _Trafiquée_/French, drama</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Heath, _Sale_/English to French transl.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Lavoie, _Seigneurie_/French, non-fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Mc Andrew, _Majorités_/French, non-fiction</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Pool, _Exploding_/English, poetry</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Rainfield, _Scars_/English, children's text</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Robitaille, _Chenil_/French, children's text</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Siebert, _Deepwater_/English, poetry</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Warren, _Cool_/English, fiction</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Yee, _Lady_/English, drama</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

**Number of titles featuring this type of acknowledgement**</td>

<td>

**3**</td>

<td>

**16**</td>

</tr>

</tbody>

</table>

The various types of communities present in the acknowledgement paratext negate the image of the solitary author, labouring alone. Instead, the interaction with communities, whether existing, encountered, or built from individual contributions, suggest that the solitary author seeks to break that solitude, and that exchange-driven information-seeking is truly embedded in the writing process.

### Publishing staff

The publishing staff, key in making the book become a cultural object suitable for distribution, is featured in the data as a category in its own right, even as the dual pattern of expertise and support emerges again (see Table 4).

<table id="tab4"><caption>

**Table 4\. Books featuring acknowledgements of publishing staff**</caption>

<tbody>

<tr>

<th>

Author, _Title (short form)_/Language, GG Category</th>

<th>As experts</th>

<th>As support</th>

<th>For reasons undefined or cryptic</th>

<th>Publishers of earlier versions or excerpts</th>

</tr>

<tr>

<td>

Abbott, _Marriage_/English, non-fiction</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Brown, _Boy_/English, non-fiction</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Casey, _Lakeland_/English, non-fiction</td>

<td>X</td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Connelly, _Burmese_/English, non-fiction</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Denman, _Me_/English, children's text</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Desjardins, _Maleficium_/French, fiction</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Donoghue, _Room_/English, fiction</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

English, _Just_/English, non-fiction</td>

<td>X</td>

<td>X</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Fairfield, _Tyranny_/English, children's text</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Flett, _Michif_/English, children's ill.</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Fortier, _Proper_/French to English transl.</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Greene, _Boxing_/English, poetry</td>

<td>X</td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Heath, _Sale_/English to French transl.</td>

<td>X</td>

<td>X</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Hine, _&_/English, poetry</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Nepveu, _Verbes_/French, poetry</td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

Pool, _Exploding_/English, poetry</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Rainfield, _Scars_/English, children's text</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Siebert, _Deepwater_/English, poetry</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Taylor, _Motorcycles_/English, fiction</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Toews, _Troutman_/English to French transl.</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Underwood, _Quiet_/English, children's ill.</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Warren, _Cool_/English, fiction</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Winter, _Annabel_/English, fiction</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

**Number of titles featuring this type of acknowledgement**</td>

<td>

**14**</td>

<td>

**15**</td>

<td>

**4**</td>

<td>

**5**</td>

</tr>

</tbody>

</table>

Indeed, authors frequently note the help and expertise provided by individuals from their publishing houses or their production teams. This type of acknowledgement was characteristically different because in acknowledging their publishing staff, authors tended to focus on the support provided, the contributions made to the evolution of the text proper and the efforts deployed to transform the text into a book-as-object. Ian Brown ([2009](#bro2009)) acknowledges the skill of the copyeditor and John English ([2009](#eng2009)) notes the skill and contributions of the copyeditor, proofreader, and indexer. English ([2009, p. 755](#eng2009)) also highlights the contribution of his editor whom he characterizes as ‘the best historical editor in Canada, who regularly turns dross into elegant prose and insightful observation'. In the paratext of _Lakeland_, Allan Casey ([2009](#cas2009)) acknowledges the editor for wrestling with the manuscript and the copy editor for bringing law and order to pages. Interestingly, few acknowledgements in the French language credit the skill of those who produce the work. In fact, only one is originally French, as the others are English-to-French translations. In the French work _Maleficium_, Martine Desjardins ([2009, p. 187 and n.p.](#des2009)) thanks the team members of her publishing house, naming certain people who are identified in the publication credits as the designer (‘_conception graphique_'), layout artist (‘_composition_'), and proofreader (‘_révision_'). She does qualify two of these contributors as either inexhaustible (‘_inépuisable_') or impeccable (‘_impeccable_'); that being said, her statements remain vague, especially given the fact that the named people's responsibilities have to be pieced together with other paratextual elements in order to make sense to the reader. It is also interesting to note that five authors thank (and do not simply list) publishers of earlier versions or excerpts of the work.

It becomes evident, through the analysis of the acknowledgement peritext, that regardless of whether a personal relationship preceded the work or was born of it, there are varying degrees of emotional ties at play in the relationship between author and publisher. In fact, even the most professional of expertise is often qualified by a comment pertaining to the mood created as information was exchanged. For example, John English ([2009, p. 755](#eng2009)) speaks of his publisher, editor, and agent by saying, ‘_I must confess that I came to enjoy missing a deadline because it would occasion a meeting that brought together these brilliant but kind women, who rebuked me gently, entertained me with stories, and sent me back to the manuscript with a renewed energy_'. K.L. Denman ([2009](#den2009)) notes, ‘_I deeply appreciate the work of Sarah Harvey, my editor at Orca; her professional expertise, insightful commentary and cheerful approach were vital to completing this story_'. Martine Desjardins ([2009, p. 187](#des2009)) speaks of her publishing team as a ‘_formidable circle_' (‘_formidable cercle_'). In writing the acknowledgements with clear affective characteristics, or in characterizing members of the publishing house as a ‘circle', authors suggest that the exchanges, though they may have been grounded in technical expertise at first, evolved into a relationship akin to the bond of family.

These people, who may have entered the writing and publication process as outsiders and through a set of information practices imposed by the literary field, become entwined not only in the labour and creative process, but also in the authors' lives and emotional worlds, thereby offering, like the previously known allies, both information and support.

### Librarians and information professionals

Within the paratextual acknowledgements of information resources, libraries and librarians stand out, not because they were prevalent and oft-cited, but because there were only six utterances pertaining to libraries in the sixty-nine texts examined (see Table 5).

<table id="tab5"><caption>

**Table 5\. Books featuring acknowledgements of librarians and information professionals**</caption>

<tbody>

<tr>

<th>

Author, _Title (short form)_/Language, GG Category</th>

<th>As providers of infrastructure (NB: XX indicates writer-in-residence programs)</th>

<th>Named, along with institution, but no reason provided</th>

<th>Named, along with institution, and reason provided</th>

<th>Institution named but no individual mentioned</th>

</tr>

<tr>

<td>

Connelly, _Burmese_/English, non-fiction</td>

<td>XX</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

David, _Manuel_/French, poetry</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

English, _Just_/English, non-fiction</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>

Flett, _Michif_/English, children's ill.</td>

<td> </td>

<td> </td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>

McMurchy-Barber, _Free_/English, children's text</td>

<td>XX</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Winter, _Annabel_/English, fiction</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**Number of titles featuring this type of acknowledgement**</td>

<td>

**2**</td>

<td>

**2**</td>

<td>

**1**</td>

<td>

**1**</td>

</tr>

</tbody>

</table>

Both Gina McMurchy-Barber and Karen Connelly were involved with Writer-in-Residence programmes at libraries and acknowledge those programmes. Connelly ([2009, p. 464-465](#con2009)) writes, ‘I finished a good portion of the manuscript while Non-Fiction Writer-in-Residence at the Toronto Reference Library, where many good people made being a writer more enjoyable and less solitary'. Connelly homes in on the idea that the author's craft is seen as a lonely process, but the inclusion of this acknowledgement clearly indicates there are ways of making this solitary endeavour less solitary by writing in a social context. This is also one of the few acknowledgements that focus on the library as an institution capable of supporting the author's craft, through both infrastructure and social ties.

The other four acknowledgements are more vague in terms of the support offered. Vancouver Public Library is cited by name in _Owls see clearly at night_ ([Flett, 2010](#fle2010)) but with no stated reason or background information on the role it may have played. In three titles, _Just watch me: the life of Pierre Elliot Trudeau, 1968-2000_, _Annabel_, and _Manuel de poétique à l'intention des jeunes filles_, one individual is named in conjunction with libraries, but again, there is no clarification as to the named individuals' role, or even to their professional status. In _Just watch me_, English ([2009, p. 754](#eng2009)) acknowledges Libraries and Archives Canada for helping with a set of meetings and refers to the ‘_leadership_' of Ian Wilson without clearly identifying him as the then National Archivist of Canada. In _Annabel_, the acknowledgements include a ‘_Thank you to Lynn Verge and the kind staff of Montreal's Atwater Library_' ([Winter, 2010, p. 463](#win2010)); what the reference to ‘_kind staff_' implies is not clear, as it could describe either the staff's research and information skills, their customer service skills and welcoming attitude, or another characteristic altogether. The wording is similar in _Manuel de poétique à l'intention des jeunes filles_, with one named library and one named individual, but no qualifying remarks as to the nature of the support offered ([David, 2010](#dav2010a)). It is important to note that in both works that thank libraries and note staff names, the acknowledgement is presented at a global level: while it is a public statement of thanks, it is generalized, vague; it does not reveal intimacy of any sort, nor does it suggest that the library was central to the work and information needs of the author.

## Discussion

The research questions can therefore be answered as follows:

1.  What can the acknowledgement peritext reveal of people as information sources in the writing process?

    The exploration of paratextual acknowledgements found within the codices reveals that authors rely on information resources that are socially focused.

2.  Can the people identified as information sources in the acknowledgement peritext be categorized, and if so, how?

    They can be categorized by the kind of support they provide, whether in terms of information, expertise, or skills, or in terms of moral or emotional support. Individuals can be singled out or grouped to form communities of support, thereby adding to the existing and encountered communities that are also identified in the sample. Publishing staff form a specific category as co-creators of the cultural object that is the published book.

3.  In what ways are information professionals mentioned in the acknowledgement peritext?

    Mentions are rare and vague. Some institutions are mentioned for providing writer-in-residence programmes.

In his acknowledgements, Drew Hayden Taylor ([2010, p. 347](#tay2010)) illustrates the importance of people and outside expertise to the author's craft by saying, ‘_In many ways a novel is like a pyramid, with several people laying stones to build up the story. The writer is merely the architect or perhaps foreman. With that in mind, there are many people to thank who have provided this humble architect/foreman with the raw materials and expertise to construct the story you have just read_'. Here, the labels ‘_raw materials_' and ‘_expertise_' point to the fact that contributions from people can take many forms. Taking this idea to the realm of non-fiction, Allen Casey ([2009, p. xi](#cas2009)), author of _Lakeland: journeys into the soul of Canada_, writes, ‘As with many works of nonfiction, the front cover of this one is make-believe in that it credits only one person'. No matter the degree of expertise or skills acknowledged, each indicated use of a person as a noted research source reinforces the richness and importance of socially-based information sources by authors, reinforcing the metaphor of author as architect or foreman cited above. This seems to dispel the myth of author as solitary figure, while reinforcing the idea that the author's research, information, and writing practices occur in a wider, culturally-bound realm.

The strong presence of readers, at various stages of the process, further suggests that when looked at from an information perspective, the relationship between readers and authors, expressed in Darnton's ([1982, p. 68](#dar1982)) ‘communications circuit' through a dotted line, may in fact prove more solid than when viewed, as Darnton did, from a publishing perspective. The information perspective further renders the ‘readers' category too narrow to reflect the range of people who act as information sources, for these people can be identified as readers, non-readers, potential readers, or be named without any reference to the readership. The ‘_allies and informants_' category ([Figure 1](#fig1)) is therefore necessary, even as it intersects with the ‘_readers_' category. Together, whether individually or collectively, these people contribute to the writing process; in so doing, they form small and broad communities of support and knowledge, which may in turn be acknowledged as formal groups, as lists of individuals, or both.

Based on the sample studied here, Darnton's model could then serve as a departure point for a representation of the information and support system of authors (see Figure 1).

<figure>

![Representation of the information relationship between authors and people as sources](../p608fig1.png)

<figcaption>  

**Figure 1\. Representation of the information relationship between authors and people as sources**</figcaption>

</figure>

Indeed, through the writing process and as the authors either seek or discover new sources, a merging of the local and the global occurs. The public act of including a mention in the peritext makes the relationship public, placing it in the global sphere; yet the often personal, even intimate texture of the acknowledgement often brings the relationship, professional though it may be, in to a more private, local sphere.

This continuum is interesting in the light of the findings pertaining to libraries and information professionals. While some authors acknowledged the library (and some library staff), there really was very little discussion of the role of libraries in the sample. This seems to echo Darnton's ([1982](#dar1982)) positioning of libraries as a minor player in the book's communications circuit. The peritextual sample studied here reflects this by illustrating a cultural trend in which libraries are not worthy of mention in the documentation of the research process.

It may be that the lack of acknowledgments of libraries and informational professionals is a reflection of a cultural convention, anchored in a more widespread perception or in, perhaps, a societal information practice: the lack of necessity to acknowledge the assumed, perhaps even taken-for-granted resource that is the library; for the fact that libraries and librarians are not often mentioned in the sample does not reveal whether libraries are used or not; it only reveals that they are not acknowledged. A cultural shift may have to occur for information resources, professionals, and organizations to be acknowledged in paratextual utterances as significant to the production and dissemination of creative works, which are, of course, part of the very fabric of the libraries' collections. The presence of books in libraries' collections explains why, in the information model, the relationship between librarians and authors travels in both directions ([see Figure 1](#fig1)). It further explains why librarians remain linked to readers, even though they are not included in that category, as they were in Darton's ([1982](#dar1982)) circuit.

Nevertheless, the question remains whether librarians are, in fact, used at all as information or support sources in the creative process, other than by providing infrastructure and dissemination services. This line of questioning can be expanded to ask whether or not the library (as an institution, ultimately) is assumed not to be a local resource, but is instead understood as a disembodied source that sits outside the creative process. From the common understanding of the library as ‘local' (in terms of the physical space of the library branch or a resource that is easily accessed), this institution becomes ‘_global_' in the peritext in that it remains at arm's length, its professionals not entering the intimate ‘circle' that supports the creative process. Writer-in-residence programmes may be the exception to that rule, providing a sustained type of support that combines infrastructure, social interactions, and access to information services.

Libraries do also serve communities of users who happen to be the readership of authors, and who could, perhaps, offer support to these writers at the creative stage. Libraries can encourage the formation of communities of exchanges, perhaps by targeting user groups through certain activities: the authors themselves (through writing groups or writers' residencies) and readers, both lay and expert (through author talks or works-in-progress workshops). The latter type of programme might assist authors in making connections at the writing stage, creating communities that would then become local to these authors. In so doing, libraries might claim a new role in the creative process of authors, which would allow for stronger relationships in the information model proposed here.

The lack of importance given to libraries in the sample is also telling in comparison to the immense care and nuance brought to the acknowledgements of the publishing staff. While thanks for accepting the work and bringing it into the realm of cultural products is understandable and perhaps expected (although not all authors do it), it is the richness of the nature of the relationship between authors and publishers that is the most striking, on both fronts: publishers are information providers as expert book makers, but they are also support and knowledge providers in the creation process, thereby echoing the types of help provided by other players. There is no reason to think that personal relationships with supportive undertones do not also develop between authors and information professionals; however, within the sample, none was clearly acknowledged.

Further research, including the study of a broader sample, could help provide potential avenues regarding the question of support-as-information. A quantitative study would also prove beneficial in establishing trends in a more statistical sense, especially in terms of the relationship between professions and types of help provided, or across genres, readership, languages, etc. Given the findings, a combination of quantitative and qualitative analyses of a targeted sample would likely prove fruitful. Furthermore, it would be interesting to compare the acknowledgment peritext included in the book itself with any form of acknowledgement epitext created by the direct, online exchanges between authors and their community of followers, critics, or readers.

## Conclusion

There is a strong focus in the acknowledgment peritext on persons as information and support resources, regardless of whether these persons were known prior to the writing process or encountered during this process. Various individuals, communities, and publishers provide information, and this information differs, along with its context. Librarians are rarely mentioned. Gratitude towards those who provide moral and emotional support throughout the writing process becomes intertwined with thanks given for help in information-seeking. Authors thank people for contributing their skills, knowledge, experience, expertise, and support, and for providing them with inspiration or comments on their work. Communities can exist as formal groups or be created as authors bring individuals together as part of their information-seeking and information-sharing endeavours. An information model thereby emerges which can complement the study of the relationships identified by Robert Darnton ([1982](#dar1982)) in his communications circuit.

Whether or not one agrees that support is a form of information, it is clear from the sample studied here that support is a form of contribution. This begs the question of whether information professionals can be contributors on that supportive level. In fact, this question can be asked on two counts: first, can librarians and information professionals be a part of the nurturing, supportive, personal relationships that seem to exist as an underlying, community-building factor for the individuals that create cultural works? And second, what could these relationships entail?

The landscape of relationships presented in the acknowledgment peritext of the sixty-nine Canadian books studied creates a sense that the information exchanges recorded in the paratext exist within a local-global continuum. This continuum brings the local or personal realm of the author into the global scene, but also brings the global context back again into the author's local realm. Indeed, the sample further consistently reveals that the interactions are or become personal, even intimate, in nature. These personal interactions then gain or regain public status as their existence and nature become acknowledged in the published peritext. The qualitative content analysis performed here shows how the more and more permeable boundaries of the local and the global create an ebb and flow between these two spaces. In fact, judging by the findings, it becomes apparent that the local-global continuum consists less of a linear progression than of a malleable rapport resembling a Möbius strip, a continuous shape of fluid motion forever linking the local and the global through private connections and public acknowledgements, through intimacy and community.

We can spend much time, in this digital age, reflecting on the community-building aspects of the online document as ‘_fixed and fluid_', to retain Brown and Duguid's ([1996](#bro1996)) expression of ‘_interplay_'; this is indeed a valuable exercise in which Gérard Genette's ([1997](#gen1997)) concept of epitext could prove extremely useful. It is no less interesting, however, to see what communities and relationships are built as or even before the document (here, the book) is created, as well as how these are linked to information-seeking habits, behaviour and practices. The paratext, as defined by Genette in [1987](#gen1987) and claimed by many fields since, reveals much about these relationships. It may therefore hold the key to a renewed discussion about the potential roles of librarians as agents in new types of relationships with authors or as facilitators in the forming of supportive communities, both of which are roles which could lead to public acknowledgement in the paratext and thereby make information professionals more present, or perhaps simply more visible, on the Möbius strip.

## Aknowledgements

The authors gratefully acknowledge the financial support of the Fonds de recherche Société et culture du Québec (Établissement de nouveaux professeurs-chercheurs) and of the Université de Montréal (Fonds de démarrage). They also wish to thank Marie Reibel and Catherine Saint-Arnaud-Babin for their assistance in formatting the paper.

An earlier and shorter version of this research was presented at the Canadian Association for Information Science (CAIS) annual conference in 2012 ([http://www.cais-acsi.ca/conf_proceedings_2012.htm](http://www.cais-acsi.ca/conf_proceedings_2012.htm)).

## <a id="author"></a>About the authors

**Nadine Desrochers** holds an MLIS from the University of Western Ontario and a PhD in French Literature from the University of Ottawa. She is currently an assistant professor at the Université de Montréal's École de Bibliothéconomie et des Sciences de l'Information. Her research examines the paratext and perceptions surrounding cultural products (both in print and online), readers' advisory, and the information-seeking habits of creators. Through this work, she seeks to build bridges with her humanities background and to foster an interdisciplinary outlook into the role of libraries and other information providers as cultural agents.  
**Jen Pecoskie** is an assistant professor in the School of Library and Information Science at Wayne State University in Detroit, Michigan. She holds a PhD in Library and Information Science and MLIS from the University of Western Ontario (London, Ontario, Canada). Her research interests home in on the theme of engagement through investigations of pleasure reading activities and readers' advisory, contemporary print culture, and LIS education.

</section>

<section>

## References

<ul>
<li>(Please note that the references to the books in the dataset are listed in <a href="#appendix1">Appendix 1</a>)<br></li>
<li id="bat2005">Bates, M. J. (2005). An introduction to metatheories, theories, and models. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), <em>Theories of information behavior: a researcher's guide</em> (pp. 1-24). Medford, NJ: Information Today.</li>
<li id="the2009"><a href="http://www.webcitation.org/5us1nyhnT">The behaviour/practice debate: a discussion prompted by Tom Wilson's review of Reijo Savolainen's ‘Everyday information practices: a social phenomenological perspective'. Lanham, MD: Scarecrow Press, 2008.</a> (2009). <em>Information Research, 14</em>(2), paper 403. Retrieved from http://informationr.net/ir/14-2/paper403.html (Archived by WebCite&reg; at http://www.webcitation.org/5us1nyhnT)</li>
<li id="bou1996">Bourdieu, P. (1996). <em>The rules of art: genesis and structure of the literary field.</em> (S. Emanuel, Trans.). Stanford, CT: Stanford University Press.</li>
<li id="bro1996">Brown, J. S. & Duguid, P. (1996). <a href="http://www.webcitation.org/6M2lKfFGe">The social life of documents.</a> <em>First Monday, 1</em>(1). Retrieved from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/466/387 (Archived by WebCite&reg; at http://www.webcitation.org/6M2lKfFGe)</li>
<li id="can">Canada Council for the Arts. (n.d.). <a href="http://www.webcitation.org/6M2ljiyYi">Cumulative List of Finalists for the Governor General's Literary Awards.</a> Retrieved from http://ggbooks.canadacouncil.ca/~/media/ggla/downloads/ggla%20cumulative%20finalists%20list%202013%20rev.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6M2ljiyYi)</li>
<li id="car2009">Carey, J. W. (2009). <em>Communication as culture: essays on media and society.</em> Boston, MA: Unwin Hyman.</li>
<li id="cas2007">Case, D. O. (2007). <em>Looking for information: a survey of research on information seeking, needs, and behavior</em> (2nd ed.). Burlington, MA: Academic Press.</li>
<li id="cas2012">Case, D. O. (2012). <em>Looking for information: a survey of research on information seeking, needs, and behavior</em> (3rd ed.). Bingley: Emerald Group.</li>
<li id="cob1996">Cobbledick, S. (1996). The information-seeking behavior of artists: exploratory interviews. <em>Library Quarterly, 66</em>(4), 343-372.</li>
<li id="cos2011">Costas, R. & van Leeuwen, T. N. (2011). <a href="http://www.webcitation.org/6M5KFNpfV">Unraveling the complexity of thanking: preliminary analyses on the ‘funding acknowledgment' field of Web of Science database.</a> <em>Proceedings of the 16th Nordic Workshop on Bibliometrics and Research Policy</em>. Aalborg, Denmark: Royal School of Library and Information Science. Retrieved from http://itlab.dbit.dk/~nbw2011/abstracts_NBW.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6M5KFNpfV)</li>
<li id="cow2004">Cowan, S. (2004). Informing visual poetry: information needs and sources of artists. <em>Art Documentation: Bulletin of the Art Libraries Society of North America, 23</em>(2), 14-20.</li>
<li id="cro1995">Cronin, B. (1995). <em>The scholar's courtesy: the role of acknowledgement in the primary communication process</em>. London: Taylor Graham.</li>
<li id="cro2005a">Cronin, B. (2005). <em>The hand of science: academic writing and its rewards</em>. Lanham, MD: Scarecrow Press.</li>
<li id="cro2005b">Cronin, B. & La Barre, K. (2005). Patterns of puffery: an analysis of non-fiction blurbs. <em>Journal of Librarianship & Information Science, 37</em>(1), 17-24.</li>
<li id="cro2006">Cronin, B. & Franks, S. (2006). Trading cultures: resource mobilization and service rendering in the life sciences as revealed in the journal article's paratext. <em>Journal of the American Society for Information Science and Technology, 57</em>(14), 1909-1918.</li>
<li id="cro2003">Cronin, B., Shaw, D. & La Barre, K. (2003). A cast of thousands: coauthorship and subauthorship collaboration in the 20th century as manifested in the scholarly journal literature of psychology and philosophy. <em>Journal of the American Society for Information Science & Technology, 54</em>(9), 855-871.</li>
<li id="cro2004">Cronin, B., Shaw, D. & La Barre, K. (2004). Visible, less visible, and invisible work: patterns of collaboration in 20th century chemistry. <em>Journal of the American Society for Information Science & Technology, 55</em>(2), 160-168.</li>
<li id="dar1982">Darnton, R. (1982). What is the history of books? <em>Daedalus, 111</em>(3), 65-83.</li>
<li id="dar2009">Darnton, R. (2009). <em>The case for books: past, present, and future.</em> New York, NY: PublicAffairs.</li>
<li id="dav2007">Davies, E. (2007). <em>Epistemic practices of theatre production professionals: an activity theory approach.</em> Unpublished doctoral dissertation, The University of Western Ontario, London, Ontario, Canada.</li>
<li id="dav2004">Davies, E. & McKenzie, P. J. (2004). <a href="http://www.webcitation.org/5vMu2P3Ik">Preparing for opening night: temporal boundary objects in textually-mediated professional practice.</a> <em>Information Research, 10</em>(1), paper 211. Retrieved from http://informationr.net/ir/10-1/paper211.html (Archived by WebCite&reg; at http://www.webcitation.org/5vMu2P3Ik)</li>
<li id="fis2011">Fisher, A. O. (2011). Adapting paratextual theory to the soviet context: publishing practices and the readers of Il'f and Petrov's Ostap Bender novels. In M. Remnek (Ed.), <em>The space of the book: print culture in the Russian social imagination</em> (pp. 252-280). Toronto: University of Toronto Press Inc.</li>
<li id="fis2009">Fisher, K. E. & Julien, H. (2009). Information behavior. <em>Annual Review of Information Science & Technology, 43</em>(1), 1-73.</li>
<li id="fra1999">Frank, P. (1999). Student artists in the library: an investigation of how they use general academic libraries for their creative needs.<em>Journal of Academic Librarianship, 25</em>(6), 445-455.</li>
<li id="gee1973">Geertz, C. (1973). Thick description: toward an interpretive theory of culture. In <em>The interpretation of cultures: selected essays</em> (pp.3-30). New York: Basic Books.</li>
<li id="gen1987">Genette, G. (1987). <em>Seuils</em>. Paris: Seuil.</li>
<li id="gen1997">Genette, G. (1997). <em>Paratexts: thresholds of interpretation</em>. (J. E. Lewin, Trans.). Cambridge: Cambridge University Press.</li>
<li id="gla1967">Glaser, B. G. & Strauss, A. L. (1967). <em>The discovery of grounded theory: strategies for qualitative research.</em> Chicago, IL: Aldine Pub. Co.</li>
<li id="gra2010">Gray, J. (2010). <em>Show sold separately: promos, spoilers, and other media paratexts.</em> New York: New York University Press.</li>
<li id="hem2008">Hemmig, W. S. (2008). The information-seeking behavior of visual artists: a literature review. <em>Journal of Documentation, 64</em> (3), 343-362.</li>
<li id="hsi2005">Hsieh, H.-F. & Shannon, S. E. (2005). Three approaches to qualitative content analysis. <em>Qualitative Health Research, 15</em>(9), 1277-1288.</li>
<li id="jul2011">Julien, H., Pecoskie, J. L. & Reed, K. (2011). Trends in information behavior research, 1999-2008: a content analysis. <em>Library & Information Science Research, 33</em>(1), 19-24.</li>
<li id="kri2004">Krippendorff, K. (2004). <em>Content analysis: an introduction to its methodology</em> (2nd ed.). Thousand Oaks, CA: Sage.</li>
<li id="kri2013">Krippendorff, K. (2013). <em>Content analysis: an introduction to its methodology</em> (3rd ed.). Thousand Oaks, CA: Sage.</li>
<li id="lay1994">Layne, S. S. (1994). Artists, art historians, and visual art information. <em>Reference Librarian, 22</em>(47), 23-36.</li>
<li id="lin1985">Lincoln, Y. S. & Guba, E. G. (1985). <em>Naturalistic inquiry</em>. Newbury Park, CA: Sage Publications.</li>
<li id="mak2011">Mak, B. (2011). <em>How the page matters</em>. Toronto: University of Toronto Press.</li>
<li id="mar2007">Marini, F. (2007). Archivists, librarians, and theatre research. <em>Archivaria, 63</em>(Spring), 7-33.</li>
<li id="mas2011">Mason, H. & Robinson, L. (2011). The information-related behaviour of emerging artists and designers: inspiration and guidance for new practitioners. <em>Journal of Documentation, 67</em>(1), 159-180.</li>
<li id="mat2007">Matthews, N. & Moody, N. (2007). <em>Judging a book by its cover: fans, publishers, designers, and the marketing of fiction</em>. Burlington, VT: Ashgate Publishing.</li>
<li id="mcc2013">McCracken, E. (2013). Expanding Genette's epitext/peritext model for transitional electronic literature: centrifugal and centripetal vectors on kindles and iPads. <em>Narrative, 21</em>(1), 105-124.</li>
<li id="med2010">Medaille, A. (2010). Creativity and craft: the information-seeking behaviour of theatre artists. <em>Journal of Documentation, 66</em>(3), 327-347.</li>
<li id="oco1998">O'Connor, B. C. & O'Connor, M. K. (1998). <a href="http://www.webcitation.org/6M5KTQQF2">Book jacket as access mechanism: an attribute rich resource for functional access to academic books.</a> <em>First Monday, 3</em>(9). Retrieved from http://journals.uic.edu/ojs/index.php/fm/article/view/616/537 (Archived by WebCite&reg; at http://www.webcitation.org/6M5KTQQF2)
</li>
<li id="pal2006">Paling, S. (2006). <a href="http://www.webcitation.org/6M5Kf3PvJ"><em>Artistic use of information technology: toward a definition of literature and art informatics</em></a>. Retrieved from http://arizona.openrepository.com/arizona/handle/10150/106472 (Archived by WebCite&reg; at http://www.webcitation.org/6M5Kf3PvJ)</li>
<li id="pal2008">Paling, S. (2008). Technology, genres, and value change: literary authors and artistic use of information technology. <em>Journal of the American Society for Information Science & Technology, 59</em>(9), 1238-1251.</li>
<li id="pal2009">Paling, S. (2009). <a href="http://www.webcitation.org/6M2ncJ5NA">Mapping techno-literary spaces: adapting multiple correspondence analysis for literature and art informatics.</a> <em>Information Research, 14</em> (2), 6. Retrieved from http://www.informationr.net/ir/14-2/paper401.html (Archived by WebCite&reg; at http://www.webcitation.org/6M2ncJ5NA)</li>
<li id="pal2011a">Paling, S. (2011). Developing a Metadata Element Set for Organizing Literary Works: A Survey of the American Literary Community. <em>Knowledge Organization, 38</em> (3), 262‑277.</li>
<li id="pal2011b">Paling, S. & Martin, C. (2011). Transformative use of information technology in Amercian literary writing: a pilot survey of literary community members. <em>Journal of the American Society for Information Science & Technology, 62</em>(5), 947-962.</li>
<li id="pat2002">Patton, M. Q. (2002). <em>Qualitative research & evaluation methods</em>. Thousand Oaks, CA: Sage.</li>
<li id="pec2013">Pecoskie, J. & Desrochers, N. (2013). Hiding in plain sight: paratextual utterances as tools for information-related research and practice. <em>Library & Information Science Research, 35</em>(2), 232-240.</li>
<li id="ree2001">Reed, B. & Tanner, D. R. (2001). Information needs and library services for the fine arts faculty. <em>Journal of Academic Librarianship, 27</em>(3), 229-233.</li>
<li id="rus1995">Russell, S. M. (1995). <em>Research needs of fiction writers</em>. Unpublished master's thesis, University of North Carolina at Chapel Hill, Chapel Hill, NC, USA</li>
<li id="sal2011">Salager-Meyer, F., Alcaraz-Ariza, M., Luzardo Briceño, M. & Jabbour, G. (2011). Scholarly gratitude in five geographical contexts: a diachronic and cross-generic approach of the acknowledgment paratext in medical discourse (1950-2010). <em>Scientometrics, 86</em>(3), 763-784.</li>
<li id="sav2007">Savolainen, R. (2007). Information behavior and information practice: reviewing the ‘umbrella concepts' of information-seeking studies. <em>The Library Quarterly, 77</em>(2), 109-132.</li>
<li id="scr2009">Scrivener, L. (2009). An exploratory analysis of history students' dissertation acknowledgments. <em>Journal of Academic Librarianship, 35</em>(3), 241-251.</li>
<li id="sve1991">Svensson, C. (1991). “No horror of the blank sheet”: the word processor in literary use. <em>Poetics, 20</em>(1), 27-51.</li>
<li id="van2001">Van Zijl, C. & Gericke, E. M. (2001). Methods used by South African visual artists to find information. <em>Mousaion, 19</em>(1), 3-24.</li>
<li id="wan2011">Wang, J. & Shapira, P. (2011). Funding acknowledgement analysis: an enhanced tool to investigate research sponsorship impacts: the case of nanotechnology. <em>Scientometrics, 87</em>(3), 563–586.</li>
<li id="whi2006">White, M. D. & Marsh, E. E. (2006). Content analysis: a flexible methodology. <em>Library Trends, 55</em>(1), 22-45.</li>
</ul>

</section>

* * *

## Appendices

<section>

<h2 id="appendix1">Appendix 1: Dataset titles (the 2010 Governor General's Literary Awards finalists, with language and categories in parentheses)</h2>
<ul>
<li id="abb2010">Abbott, E. (2010). <em>A history of marriage</em>. Toronto: Penguin Canada. (English; Non-fiction).</li>
<li id="bil2010">Billette, G. (2010). <em>Les ours dorment enfin</em>. Carni&egrave;res-Morlanwelz, Belgium: Lansman &Eacute;diteur. (French; Drama).</li>
<li id="bir2010">Birdsell, S. (2010). <em>Waiting for Joe: a novel</em>. Toronto: Random House. (English; Fiction).</li>
<li id="bla2010">Blais, M. (2010). <em>Mai au bal des pr&eacute;dateurs: roman</em>. Montr&eacute;al: Les &Eacute;ditions du Bor&eacute;al. (French; Fiction).</li>
<li id="bro2009">Brown, I. (2009). <em>The boy in the moon: a father's search for his disabled son</em>. Toronto: Random House Canada. (English; Non-fiction).</li>
<li id="cas2009">Casey, A. (2009). <em>Lakeland: journeys into the soul of Canada</em>. Vancouver: Greystone Books. Copublished by the David Suzuki Foundation. (English; Non-fiction).</li>
<li id="cat2010">Catalano, F. (2010). <em>qu'une lueur des lieux</em>. Montr&eacute;al: &Eacute;ditions de l'Hexagone. (French; Poetry).</li>
<li id="cha2010a">Chafe, R. (2010). <em>afterimage: adapted from the short story by Michael Crummey</em>. Toronto: Playwrights Canada Press. (English; Drama).</li>
<li id="cha2010b">Charest, M. (2010). <em>Rien que la guerre, c'est tout</em>. Montr&eacute;al: &Eacute;ditions Les Herbes Rouges. (French; Poetry).</li>
<li id="che2010">Chenelière, E. de la (2009). <em>L'imposture</em>. Ottawa and Montr&eacute;al: Lem&eacute;ac &Eacute;diteur. (French; Drama).</li>
<li id="con2009">Connelly, K. (2009). <em>Burmese lessons: a love story</em>. Toronto: Random House Canada. (English; Non-fiction).</li>
<li id="cor2009">Corbeil-Coleman, C. (2009). <em>Scratch</em>. Toronto: Playwrights Canada Press. (English; Drama).</li>
<li id="cro2010a">Croteau, M. (2010). <em>Le funambule: un conte sur Marc Chagall</em>. (J. Bisaillon, Illus.). Montr&eacute;al: Les &Eacute;ditions des 400 coups. (French; Children's Literature – Illustration).</li>
<li id="cro2010b">Croza, L. (2010). <em>I know here</em>. (M. James, Illus.). Toronto: Groundwood Books/House of Anansi Press. (English; Children's Literature – Illustration).</li>
<li id="dav2010a">David, C. (2010). <em>Manuel de po&eacute;tique à l'intention des jeunes filles</em>. Montr&eacute;al: &Eacute;ditions Les Herbes Rouges. (French; Poetry).</li>
<li id="dav2010b">Davidts, J. (2010). <em>Triste sort ou l'hurluberlu de Morneville</em>. (M. Gauthier, Illus.). Montr&eacute;al: Les &Eacute;ditions des 400 coups. (French; Children's Literature – Illustration).</li>
<li id="den2009">Denman, K. L. (2009). <em>Me, myself and Ike</em>. Victoria, BC: Orca Book Publishers. (English; Children's Literature – Text).</li>
<li id="des2009">Desjardins, M. (2009). <em>Maleficium</em>. Qu&eacute;bec: &Eacute;ditions Alto. (French; Fiction).</li>
<li id="don2010">Donoghue, E. (2010). <em>Room: a novel</em>. Toronto: HarperCollins. (English; Fiction).</li>
<li id="dub2009">Dubois, D. & Dubois, R-D. (2009). <em>Morceaux. Entretiens sur l'&eacute;cho du monde, l'imaginaire et l'&eacute;criture</em>. Montr&eacute;al: Lem&eacute;ac &Eacute;diteur. (French; Non-fiction).</li>
<li id="dup2009">Dupr&eacute;, L. (2009). <em>High-wire summer: stories</em>. (L. Hawke, Trans.). Toronto: Cormorant Books. (Translation – French to English; original title: L'&eacute;t&eacute; funambule).</li>
<li id="eng2009">English, J. (2009). <em>Just watch me: the life of Pierre Elliott Trudeau 1968-2000</em>. Toronto: Alfred A. Knopf Canada. (English; Non-fiction).</li>
<li id="fai2009">Fairfield, L. (2009). <em>Tyranny. Toronto: Tundra Books</em>. (English; Children's Literature – Text).</li>
<li id="fle2010">Flett, J. (2010). <em>Lii yiiboo nayaapiwak lii swer: l'alfabet di michif = Owls see clearly at night: a michif alphabet</em>. Vancouver: Simply Read Books. (English; Children's Literature – Illustration).</li>
<li id="for2010">Fortier, D. (2010). <em>On the proper use of stars</em>. (S. Fischman, Trans.). Toronto: McClelland & Stewart Ltd. (Translation – French to English; original title: Du bon usage des &eacute;toiles).</li>
<li id="fou2009">Fournier, D. (2009). <em>effleur&eacute;s de lumi&egrave;re</em>. Montr&eacute;al: &Eacute;ditions de l'Hexagone. (French; Poetry).</li>
<li id="gal2009">Gallant, M. (2009). <em>Rencontres fortuites: roman</em>. (G. Letarte & A. Strayer, Trans.). Montr&eacute;al: Les Allusifs. (Translation – English to French; original title: A Fairly Good Time).</li>
<li id="gre2009">Greene, R. (2009). <em>Boxing the compass</em>. Montr&eacute;al: V&eacute;hicule Press. (English; Poetry).</li>
<li id="gru2010">Gruda, A. (2010). <em>Onze petites trahisons: nouvelles</em>. Montr&eacute;al: Les &Eacute;ditions du Bor&eacute;al. (French; Fiction).</li>
<li id="hac2010">Hach&eacute;, E. (2010). <em>Trafiqu&eacute;e</em>. Carni&egrave;res-Morlanwelz, Belgium: Lansman &Eacute;diteur. (French; Drama).</li>
<li id="hag2009">Hage, R. (2009). <em>Le cafard</em>. (S. Voillot, Trans.). Qu&eacute;bec: &Eacute;ditions Alto. (Translation – English to French; original title: Cockroach).</li>
<li id="har2010">Harris, M. (2010). <em>Circus</em>. Montr&eacute;al: V&eacute;hicule Press. (English; Poetry).</li>
<li id="hea2010">Healey, M. (2010). <em>Courageous</em>. Toronto: Playwrights Canada Press. (English; Drama).</li>
<li id="hea2009">Heath, J. (2009). <em>Sale argent: petit trait&eacute; d'&eacute;conomie &agrave; l'intention des d&eacute;tracteurs du capitalisme</em>. (L. Saint-Martin & P. Gagn&eacute;, Trans.). Montr&eacute;al: Les &Eacute;ditions Logiques. (Translation – English to French; original title: Filthy Lucre: Economics for People Who Hate Capitalism).</li>
<li id="hin2010">Hine, D. (2010). <em>&amp;: a serial poem</em>. Markham, ON: Fitzhenry and Whiteside Limited. (English; Poetry).</li>
<li id="jos2009">Josie, M. (2009). <em>Le g&eacute;ranium</em>. (M. Josie, Illus.). Montr&eacute;al: Marchand de feuilles. (French; Children's Literature – Illustration).</li>
<li id="laf2009">Laferrière, D. (2009). <em>L'&eacute;nigme du retour: roman</em>. Montr&eacute;al: Les &Eacute;ditions du Bor&eacute;al. (French; Fiction).</li>
<li id="lav2010">Lavoie, M. (2010). <em>C'est ma seigneurie que je r&eacute;clame: la lutte des Hurons de Lorette pour la seigneurie de Sillery, 1650-1900</em>. Montr&eacute;al: Les &Eacute;ditions du Bor&eacute;al. (French; Non-fiction).</li>
<li id="mca2010">Mc Andrew, M. (2010). <em>Les majorit&eacute;s fragiles et l'&eacute;ducation: Belgique, Catalogne, Irlande du Nord, Qu&eacute;bec</em>. Montr&eacute;al: Les Presses de l'Universit&eacute; de Montr&eacute;al. (French; Non-fiction).</li>
<li id="mcm2010">McMurchy-Barber, G. (2010). <em>Free as a bird</em>. Toronto: Dundurn Press. (English; Children's Literature – Text).</li>
<li id="mou2009">Mouawad, W. (2009). <em>Forests</em>. (L. Gaboriau, Trans.). Toronto: Playwrights Canada Press. (Translation – French to English; original title: Forêts).</li>
<li id="nep2009">Nepveu, P. (2009). <em>Les verbes majeurs</em>. Montr&eacute;al: &Eacute;ditions du Noro&icirc;t. (French; Poetry).</li>
<li id="noe2009">Noël, M. (2009). <em>Nishka: roman</em>. Montr&eacute;al: Les &Eacute;ditions Hurtubise. (French; Children's Literature – Text).</li>
<li id="oue2010">Ouellet, P. (2010). <em>O&ugrave; suis-je? Parole des &eacute;gar&eacute;s</em>. Montr&eacute;al: VLB &Eacute;diteur. (French; Non-fiction).</li>
<li id="pag2010">Page, P. K. (2010). <em>Uirapur&uacute;: based on a brazilian legend</em>. (K. Bridgeman, Illus.). Fernie, BC: Oolichan Books. (English; Children's Literature – Illustration).</li>
<li id="pap2009">Papineau, L. (2009). <em>Le journal secret de Lulu Papino: mon premier amour</em>. (V. Egger, Illus.). Saint-Lambert, QC: Les &eacute;ditions H&eacute;ritage Inc. (French; Children's Literature – Illustration).</li>
<li id="paq2009">Paquet, D. (2009). <em>Porc-&eacute;pic</em>. Montr&eacute;al: Dramaturges &Eacute;diteurs. (French; Drama).</li>
<li id="phi2010">Phillips, W. (2010). <em>Fishtailing</em>. Regina, SK: Coteau Books for Teens. (English; Children's Literature – Text).</li>
<li id="poo2009">Pool, S. (2009). <em>Exploding into night</em>. Toronto: Guernica Editions. (English; Poetry).</li>
<li id="pou2009">Poulin-Denis, G. (2009). <em>Rearview</em>. Montr&eacute;al: Dramaturges &Eacute;diteurs. (French; Drama).</li>
<li id="qui2010">Quiviger, P. (2010). <em>The breakwater house</em>. (L. Lederhendler, Trans.). Toronto: House of Anansi Press. (Translation – French to English; original title: La maison des temps rompus).</li>
<li id="rai2010">Rainfield, C. (2010). <em>Scars</em>. Lodi, NJ: WestSide Books. (English; Children's Literature – Text).</li>
<li id="riv2010">Rivard, Y. (2010). <em>Une id&eacute;e simple: essai</em>. Montr&eacute;al: Les &Eacute;ditions du Bor&eacute;al. (French; Non-fiction).</li>
<li id="rob2009">Robitaille, P. (2009). <em>Le chenil</em>. Ottawa: Les &Eacute;ditions L'Interligne. (French; Children's Literature – Text).</li>
<li id="sie2010">Siebert, M. (2010). <em>Deepwater vee</em>. Toronto: McClelland & Stewart Ltd. (English; Poetry).</li>
<li id="ste2009">Steinmetz, Y. (2009). <em>La chamane de bois-rouge: roman</em>. Saint-Laurent, QC: &Eacute;ditions Pierre Tisseyre. (French; Children's Literature – Text).</li>
<li id="stew2009">Stewart, S. (2009). <em>L'exode des loups: roman</em>. (C. Vivier, Trans.). Montr&eacute;al: Les &Eacute;ditions du Bor&eacute;al. (Translation – English to French; original title: Wolf Rider).</li>
<li id="stu2010">Stutson, C. (2010). <em>Cats' night out</em>. (J. Klassen, Illus.). New York: Simon & Schuster Books for Young Readers. (English; Children's Literature – Illustration).</li>
<li id="tay2010">Taylor, D. H. (2010). <em>Motorcycles & sweetgrass</em>. Toronto: Alfred A. Knopf. (English; Fiction).</li>
<li id="tho2010">Thompson, J. (2010). <em>Such creatures</em>. Toronto: Playwrights Canada Press. (English; Drama).</li>
<li id="thu2009">Th&uacute;y, K. (2009). <em>Ru</em>. Montr&eacute;al: Les &Eacute;ditions Libre Expression. (French; Fiction).</li>
<li id="tre2009a">Tremblay, A. U. (2009). <em>Le dernier &eacute;t&eacute; ou l'odyss&eacute;e de Sarg-XI</em>. Saint-Lambert, QC: Souli&egrave;re &eacute;diteur. (French; Children's Literature – Text).</li>
<li id="tre2009b">Tremblay, M. (2009). <em>The blue notebook</em>. (S. Fischman, Trans.). Vancouver: Talonbooks. (Translation – French to English; original title: Le cahier bleu).</li>
<li id="toe2009">Toews, M. (2009). <em>Les Troutman volants: roman</em>. (L. Saint-Martin & P. Gagn&eacute;, Trans.). Montr&eacute;al: Les &Eacute;ditions du Bor&eacute;al. (Translation – English to French; original title: The Flying Troutmans).</li>
<li id="tur2009">Turcotte, &Eacute;. (2009). <em>Rose, derri&egrave;re le rideau de la folie</em>. (D. Sylvestre, Illus.). Montr&eacute;al: Les &eacute;ditions de la courte &eacute;chelle. (French; Children's Literature – Text and French; Children's Literature - Illustration).</li>
<li id="und2010">Underwood, D. (2010). <em>The quiet book</em>. (R. Liwska, Illus.). New York: Houghton Mifflin Books for Children. (English; Children's Literature – Illustration).</li>
<li id="war2010">Warren, D. (2010). <em>Cool water</em>. Toronto: HarperCollins. (English; Fiction).</li>
<li id="win2010">Winter, K. (2010). <em>Annabel</em>. Toronto: Anansi Press. (English; Fiction).</li>
<li id="yee2010">Yee, D. (2010). <em>lady in the red dress</em>. Toronto: Playwrights Canada Press. (English; Drama)</li>
</ul>
</section>

</article>