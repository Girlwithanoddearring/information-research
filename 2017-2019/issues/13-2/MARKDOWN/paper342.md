#### Information Research vol. 13 no. 2, June 2008



# Citation counts and the Research Assessment Exercise, part VI: Unit of assessment 67 (music)

#### [Charles Oppenheim](mailto:c.oppenheim@lboro.ac.uk)  
Department of Information Science, Loughborough University, Loughborough, Leicestershire, LE11 3TU, United Kingdom

#### [Mark A.C.Summers](mailto:m.a.c.summers@lboro.ac.uk)  
LISU, Department of Information Science, Loughborough University, Loughborough, Leicestershire, LE11 3TU, United Kingdom

#### Abstract

> **Introduction.** This study aimed to explore research assessment within the field of music and, specifically, to investigate whether citation counting could be used to replace or inform the peer review system currently in use in the UK.  
> **Method**. A citation analysis of academics submitted for peer review in Unit of Assessment 67 in the 2001 Research Assessment Exercise was performed using the Arts and Humanities Citation Index and checked for correlations with the Assessment scores. A Spearman rank order correlation coefficient test was used to assess the significance of correlations between citations and scores.  
> **Results**. At a departmental level, citation counts correlated strongly with scores awarded by the Assessment Exercise. A weaker correlation was found between scores and individual counts. The correlations were significant at the 0.01% level. Types of submission were analysed and trends were found within the author group. However, the Arts and Humanities Citation Index was found to be unrepresentative of music research activity in UK universities due to its choice of source material.  
> **Conclusion**. The Arts and Humanities Citation Index alone is not a suitable data source for citation analysis in the field of music. However, if an alternative data source could be found, there is potential for the use of citation analysis in research assessment in music.

</fieldset>

</form>

## Introduction

This paper reports the results of a study that aimed to explore research assessment within the field of music, especially music academics' citation counts and how they relate to departmental Research Assessment Exercise scores. This is the sixth in what has become a series of papers dealing with citation analysis and the UK Research Assessment Exercise: Oppenheim <a href="" id="#opp95">1995</a>, <a href="" id="#opp96">1996</a> and <a href="" id="#opp97">1997</a>, <a href="" id="#hoop01">Holmes & Oppenheim 2001</a>, <a href="" id="#noop03">Norris & Oppenheim 2003</a>

Music, unit of assessment 67, was deemed to be an interesting subject for this study as the output of the sector is mixed, consisting of a much wider range of formats than just the traditional journal article or monograph, etc. A significant part of the sector is involved in the creation of new material in composition and performance. These creative outputs are not expected to make the same impact as written works in journal-based citation indexes, making the sector as a whole seem, at first glance, to be far less suited to citation-based measures of quality.

This study tested the null hypothesis that there is no correlation between music departmental Research Assessment Exercise scores and citation counts achieved and that, therefore, citation counts could not provide a comparable measure of quality.

Scores given by the Research Assessment Exercise in 2001 (hereafter, 'the 2001 Exercise') were taken as the best assessment of music research available at this time. Usefully, at least one music department gained each of the seven available assessment scores (1-5*) at the 2001 Exercise. These scores were correlated (using similar methods to previous studies to facilitate meaningful comparison – see below) with the citation counts for each department (based on publications produced by their constituent staff members during the census period of 1994-2000 inclusive).

The specific outputs of authors, as shown on the 2001 Exercise Website, were recorded and analysed to show characteristics of various author groups. This provides a better light in which to view the results of the preceding correlation tests.

## Background

### The UK Research Assessment Exercise

The first Exercise was held in 1986 to determine the distribution of research funding to UK universities. Indeed, about £5 billion of research funds was distributed in response to the results of the 2001 Exercise.

In order to award scores that show research excellence, 'the Research Assessment Exercise is concerned with making a qualitative judgement of the research output of those university departments who submit themselves to the procedure' ([Norris & Oppenheim 2003](#noop03): 709). These scores reflect research quality with a scale, which, along with the assessment methods, has developed over time. The 2001 scale and the associated descriptions are shown in Table 1\.

<table width="85%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Research Assessment Exercise2001 scoring scale with descriptions** ([Research Assessment Exercise 1999](#Research Assessment Exercise99): 9)</caption>

<tbody>

<tr>

<th>Score</th>

<th align="center">Description</th>

</tr>

<tr>

<td>5*</td>

<td align="left">Quality that equates to attainable levels of international excellence in more than half of the research activity submitted and attainable levels of national excellence in the remainder</td>

</tr>

<tr>

<td>5</td>

<td align="left">Quality that equates to attainable levels of international excellence in up to half of the research activity submitted and to attainable levels of national excellence in virtually all of the remainder</td>

</tr>

<tr>

<td>4</td>

<td align="left">Quality that equates to attainable levels of national excellence in virtually all of the research activity submitted, showing some evidence of international excellence</td>

</tr>

<tr>

<td>3a</td>

<td align="left">Quality that equates to attainable levels of national excellence in over two-thirds of the research activity submitted, possibly showing evidence of international excellence</td>

</tr>

<tr>

<td>3b</td>

<td align="left">Quality that equates to attainable levels of national excellence in more than half of the research activity submitted</td>

</tr>

<tr>

<td>2</td>

<td align="left">Quality that equates to attainable levels of national excellence in up to half of the research activity submitted</td>

</tr>

<tr>

<td>1</td>

<td align="left">Quality that equates to attainable levels of national excellence in none, or virtually none, of the research activity submitted</td>

</tr>

</tbody>

</table>

The assessment process, based on peer review, has been developing since the Exercise's inception, becoming more open with each exercise ([A guide to the 2001 Research Assessment Exercise 2002](#agu02)). Along with the setting of criteria by each successive group of panels, the Roberts Review ([Roberts 2004](#rob04)) looked at the running of the Exercise and suggested further changes that could be made to the process. In the review report, Roberts stated his belief that, 'it is time to move away from a 'one-size-fits-all' assessment, to a model which concentrates assessment effort where the stakes are highest', aiming at, 'efficiency and fairness' ([Roberts 2004](#rob04)).

However, Roberts's proposals for the running of the Exercise appear to have been ignored, with a new metrics-based assessment system being announced in 2006 ([HM Treasury 2006](#hmt06): 61). That science was the main area in mind during the formulation of this new direction is evident in the announcement of the change, it having been made within the section 'Science and innovation' of the 2006 UK Budget ([HM Treasury 2006](#hmt06): 61).

The future for research assessment for the arts and humanities following the 2008 Exercise is as yet unclear, except that they will 'be assessed through a light-touch process, based on peer review and informed by statistical indicators in common with the science-based disciplines' ([Eastwood 2007](#eas07)). There will be further consultation for non-science based subjects in 2009/10\.

### Citation analysis

> **Citation analysis**. A wide-ranging area of bibliometrics that studies the citations to and from documents ([Diodato 1994](#dio94): 33).

The common currency of citation analysis is the citation. Diodato states that the term 'citation count' (also called citation rate or citation frequency) 'refers to the number of citations an author, document, or journal has received during a certain period of time' ([Diodato 1994](#dio94): 39).

The normative theory of citation is that 'bibliographies are lists of influences and that authors cite in order to give credit where credit is due; that is, when an author uses information from another's work, he will cite that work' ([MacRoberts & MacRoberts 1989](#mcmc89): 342). Following on from this, number of citations is taken to equate to the quality of the work being cited with larger numbers of citations reflecting better quality. In this way, Cole and Cole suggest that 'citations are not a measure of the absolute quality of work, they are an adequate measure of the quality of work socially defined' ([Cole & Cole 1973](#coco73): 24).

As a tool for analysis, Garfield ([1979](#gar79)) suggests that 'the simplicity of citation indexing is one of its main strengths' ([Garfield 1979](#gar79): 1). He states that 'a citation index is built around these linkages [i.e. citations]. It lists publications that have been cited and identifies the sources of the citations' ([Garfield 1979](#gar79): 1). Baird and Oppenheim ([1994](#baop94)) state that:

> citation indexing gives insight into the way science—including social sciences and humanities—is carried out, and provides material for studying the prestige of academics, the importance of universities, and the efficiency of entire countries' scientific research ([Baird & Oppenheim 1994](#baop): 3).

However, MacRoberts and MacRoberts ([1996](#mcmc96): 439) question this underlying basis of citation analysis. They suggest a possible social constructivist view of science where cultural factors have a significant role in the shaping of knowledge and the outcomes are 'subjective, contingent, social, and historical'. In this view of science, citations are part of a compromised creative process and, as such, cannot provide reliable data for objective measures.

Indeed, MacRoberts and MacRoberts ([1989](#mcmc89): 343) put forward a number of potential weaknesses of citation analysis:

*   Formal & informal influences not cited.
*   Biased- or self-citing.
*   Different types of citations.
*   Variations in citation rate related to type of publication, nationality, time period, and size and type of speciality.
*   Technical limitations of citation indices and bibliographies.
    *   Multiple authorship.
    *   Synonyms.
    *   Homonyms.
    *   Clerical errors.
    *   Coverage of literature.

They concluded that 'any results obtained by using citations as data will, at best, have to be considered tentative' ([MacRoberts & MacRoberts 1989](#mcmc89): 347).

### Citation analysis and research assessment

Citation analysis can be used to quantify past performance and van Raan ([1996](#raa96)) suggests that past performance is a good predictor of future performance. Moed ([2005](#moe05)) explores in depth aspects of accuracy, theory and the practical use of citation analysis, whilst critically evaluating its strengths and weaknesses.

Holmes and Oppenheim ([2001](#hoop01)) suggest that citation analysis could be useful to assessed institutions in their preparations for an assessment exercise, informing decisions about the inclusion of staff based on citation count. In another paper, Oppenheim has further suggested that the Exercise could be replaced by citation analysis as it is quicker and cheaper than the existing system ([Oppenheim 1996](#opp96): 161).

In a study that analysed data from the Proceedings of the International Communication Association, So ([1998](#so98)) concurs with Oppenheim, advocating 'the use of citation data as an alternative and even a substitute for peer review exercise' because 'citation results correlate highly with expert review results' ([So 1998](#so98): 332). Smith and Eysenck ([2002](#smey02)) have come to the same conclusion.

However, van Raan suggests that the 'ranking of research institutions by bibliometric methods is an improper tool for research performance evaluation' ([van Raan 2005](#raa05): 133). He puts forward the view that a system employing advanced bibliometric indicators should be used alongside a peer-based evaluation procedure, concurring with Holmes & Oppenheim ([2001](#hoop01)) and Warner ([2000](#war00)).

### Citation analysis and the Research Assessment Exercise

There have been a number of studies that look at possible correlations between citation counts and scores given by various Research Assessment Exercises to subject departments. These studies have used different methods, yet returned statistically significant correlations in each case. No studies showing no correlation have been found. The existence of significant correlations between the results of citation counts and the decisions made by expert peer-review in these studies leads to the conclusion that citation counts could be a reliable indicator of research quality for use in the Research Assessment Exercise. The examples listed below formed the basis for comparison with the present study.

<table width="60%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size:
smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif;
background-color: #fdffdd"><caption align="bottom">  
**Table 2: Studies showing positive correlations between citation counts and Research Assessment Exercise scores**</caption>

<tbody>

<tr>

<th>Author(s)</th>

<th>Date</th>

<th>Subject area</th>

</tr>

<tr>

<td>Oppenheim</td>

<td>[1995](#opp95)</td>

<td>Library & Information Science</td>

</tr>

<tr>

<td>Seng & Willett</td>

<td>[1995](#sewi95)</td>

<td>Library & Information Science</td>

</tr>

<tr>

<td>Oppenheim</td>

<td>[1997](#opp97)</td>

<td>Genetics, Anatomy, Archaeology</td>

</tr>

<tr>

<td>Thomas & Watkins</td>

<td>[1998](#)</td>

<td>Business & Management</td>

</tr>

<tr>

<td>Sarwar*</td>

<td>[2000](#sar00)</td>

<td>Civil Engineering</td>

</tr>

<tr>

<td>Smith & Eysenck</td>

<td>[2002](#smey02)</td>

<td>Psychology</td>

</tr>

<tr>

<td>Norris & Oppenheim</td>

<td>[2003](#noop03)</td>

<td>Archaeology</td>

</tr>

<tr>

<td colspan="3">*Sarwar does not give any evidence regarding correlations. Calculations based on Sarwar's data can be found in Summers ([2007](#sum07): 17).</td>

</tr>

</tbody>

</table>

However, potential problems with the use of correlations between citation analysis and Exercise scores are raised by Warner ([2000](#war00)). He suggests that there is 'a weak, and unsatisfactorily treated, correlation between citation aggregates and research quality for individual entities' and suggests that 'the future value of citation analysis could be to inform, but not to determine, judgments of research quality' ([Warner 2000](#war00): 453).

Oppenheim ([2000](#opp00): 459) counters this view by suggesting that Warner's conclusions come more from focusing on potential weaknesses rather than viewing the evidence of an inherent robustness that is displayed by citation study results, echoing Hemlin ([1996](#hem96)) who notes that correlations exist despite valid criticisms of citation analysis.

### Citation metrics and the humanities

'Science on the one hand and humanities on the other are two distinct domains of scholarship with essentially different substantive contents' ([Moed 2005](#noe05): 12). It follows that the application of citation indexing to these differing domains may yield outcomes of differing usefulness because of the favoured publication type of each subject area. In science, there is the 'consistently demonstrated primacy of the journal article', whereas the monograph is 'the leading medium of scholarly communication in the humanities' ([Cullars 1998](#cul98): 42). In relation to this, 'the application of citation index data depends on the role of journal articles in the different fields' ([van Raan 2005](#raa05): 138).

The Web of Knowledge citation indexes, including the Arts and Humanities Citation Index, use journals to provide their citation data. Moed describes as moderate the adequacy of coverage of the humanities in the citation indexes ([Moed 2005](#moe05): 138). Coverage is defined as '...the extent to which the sources processed by Thomson for its Citation Indexes (mainly scholarly journals) cover the written scholarly literature in a field'. ([Moed 2005](#moe05): 119). He suggests that 'a principal cause of non-excellent coverage is the importance of sources other than international journals, such as books and conference proceedings' ([Moed 2005](#moe05): 3).

Another critical factor for the improvement of coverage may be the feasibility of compiling any list of core journals. Within music, this factor arose with a heated debate when the Arts and Humanities Research Council proposed that academics nominate their (ten) choices for a list of core journals. The primary concern of academics was that ten journals cannot adequately represent a sub-field of music, let alone music as a whole. Unsurprisingly, this project was abandoned (Laura Lugg, AHRC, personal communication, 15 August, 2006)

Since then, the European Science Foundation Standing Committee for the Humanities has instigated the European Reference Index for the Humanities. This aims to provide 'quantitative criteria and to advance an evaluation of the research productivity in terms of bibliometrics' ([Peyraube 2005](#pey05): 1) and to provide an international reference tool that would succeed where the Arts and Humanities Citation Index does not, in terms of providing a comprehensive coverage of the humanities.

### Music research

The nature of music and its study is an area that lends itself to a large research footprint with many overlaps with other disciplines and many different directions of proceeding. Beyond the traditional fields in music (historical musicology, theory, composition, etc.), there are overlaps with, for example, analytical studies, psychology, artificial intelligence, medicine, education, sociology and cultural studies. Additionally,

> ...research on all these topics is distinct from creative work in them and can be separated off for inclusion under the 'humanities research' banner. In practice, the distinction has often been blurred, with an uncertain boundary between research and creative work ([Meadows 1998](#mea98): 41).

As it is not always easy to distinguish between research and practice, research assessment is not straight forward as, in many cases, any assessor must first decide how much research is contained in any given submission.

One factor that could affect the success of citation analysis is music citing practice. Traditional outputs such as articles and monographs are broadly similar to other subject areas. Any music-specific outputs (compositions, performances, etc.) are likely to have differing practices, given that music itself (that is, the sound or notational playing directions) cannot give explicit reference except in the musical sense. However, music-specific outputs can be cited as existing or having happened, especially in reviews, and analyses of compositions feature in analysis-focused journals.

### Music and the Research Assessment Exercise

Given that it must assess music research, the Research Assessment Exercise offers an overview of what constitutes music research for each assessment. Reflecting the wide-ranging work of music academics, this description has become increasingly inclusive over successive Exercises. The published criteria of the music sub-panel for the 2008 Exercise give a good overview of music as follows:

> The sub-panel will assess research from all areas of music, which include (but are not confined to):
> 
> *   composition and creative practice
> *   performance
> *   musicology (including historical, critical, empirical, ethnographic, theoretical, analytical and organological approaches)
> *   scientific approaches to the study of music
> *   new technology and music
> *   musical acoustics and audio engineering (where the subject matter is music-related)
> *   appropriate pedagogic research in any of the areas identified above ([Research Assessment Exercise 2006](#RAE06): 65)

The phrase 'not confined to' in the above will allow the flexibility necessary in a field where the nature of its research output is seen to be diverse and continually expanding. The record of submissions for the 2001 Exercise shows a cross-section of music research outputs in all of these areas.

As a contribution to the Roberts review, Banfield ([2003](#ban03)) wrote on behalf of National Association for Music in Higher Education to the Higher Education Funding Council for England. He detailed some specifics about metrics that could be included in possible assessment:

> Greater use of algorithms seems to appeal to creative arts departments and practitioners more than scholarly ones, perhaps in line with audience and community reception as opposed to readership and citation—but these are a minority ([Banfield 2003](#ban03): 2).

Banfield confirms that 'peer review remains the favoured method of assessment', stating that 'citation measurement is deeply distrusted', not least because peer review may do most to retain the individual nature of music departments, where 'there is a general repugnance towards standardisation' ([Banfield 2003](#ban03): 2).

The criteria above reflect a flexible method of assessment developed in conjunction with music academics for their subject area in the form of an inclusive peer assessment. Indeed, many believe that despite the flaws that may exist at present, 'without peer review the chances of any suitable assessment of the work of music staff across the sector seem bleak.'[Johnson 2006](#joh06)

## Methods adopted

### Data collection

At the core of this study is a citation analysis that counted citations received by academics who were submitted for peer review in Unit of Assessment 67 in the 2001 Exercise. The collected citation counts were then checked for correlations with the Assessment Exercise scores awarded to each department.

Until now, studies of the correlation of citation counts and Assessment Exercise scores have concentrated on subjects from either science or from the more scientific end of the humanities (e.g., archaeology). Music provides a different set of circumstances to consider, especially its non-standard outputs, which seem far less suited to citation-based measures of quality.

Various databases with citation information exist, but often they do not cover music (e.g., Scopus) or cover it in an unreliable way (e.g., Google Scholar, the unreliability of which is demonstrated by Jacso ([2006](#jac06))) and the European Reference Index previously mentioned is not yet beyond the consultation stage. As the longest established and most comprehensive citation index in terms of coverage, the Arts and Humanities Citation Index was used for the collection of citation statistics relating to music.

#### Research Assessment Exercise data

Details of the 2001 Exercise submissions were taken from the Website ([HERO 2002](#her02)). Relevant data were:

*   Names of UK universities that made submissions for music.
*   Names of academics in music departments of those UK universities.
*   Types of material submitted by each academic (journal, monograph, performance etc.).

Fifty-nine departments with 724 named academics were returned for music in 2001\. Academics with no listed submissions were excluded, providing a second list with a total of 670 names. Details of the 670 sets of submissions were entered into a spreadsheet, listing department name, the academics' names and the type of each of their four submissions (journal article, authored book, composition, etc.).

The standard submission was of four pieces of research for each member of staff, in which could be shown a representative sample of research and its quality. Some academics submitted two (as permitted in some instances). A small number of academics had three submissions and one had just one. Whether these odd numbers were intentional or simply clerical errors is not clear.

The final results of the 2001 Exercise were also taken from HERO ([2002](#her02)), recording a score for each department ranging from 1 to 5*.

#### Citation data

The permissible dates for publications assessable in music for the 2001 Exercise were 1994-2000 inclusive. Citations to _any_ material produced by the 670 academics within these dates were counted, not just citations to Research Assessment Exercise submissions. The citations counted were produced between 1994 and the date of searching. The searches were carried out in June and July 2006 using the Arts and Humanities Citation Index.

Each citation was checked to ascertain whether it was attributable to the chosen music author, using their submissions as a general guide to the author's research interests. Where a citation was unclear, a check of the record of the article from which it came gave confirmation in most cases. Further help was obtained from authors' homepages and from publishers' catalogues in the case of composers.

Citations to theses were included but citations to items listed as 'unpublished' or 'in press' were ignored. Citations without dates were included only if they were verifiable and related to material published within the study period. This applied mostly to compositions, the dates of which were easily checked. Citations which seemed to have been indexed under the wrong dates were included only if there was very little doubt that an input error had occurred.

The names of the music authors were entered in the format found on the Research Assessment Exercise Website except for hyphenated and compound names, where standard Web of Knowledge contractions were used. Checks often had to be made for authors with multiple initials and those who went by a middle name.

For cited works listed as 'performance' or 'performances' that were undated (as seen in Figure 1), the count was included if the citing article was published between April 1994 and the end of 2000\.

<div align="center">![Figure1.jpg](p342fig1.jpg)</div>

<div align="center">  
**Figure 1: example of 'performance' citations**</div>

### Issues and observations

Separating authors in the Arts and Humanities Citation Index with the same surname and initial who write in different areas of the arts and humanities was a relatively simple process. However, separating those within similar or even the same disciplines was more difficult, demanding subject knowledge, which, fortunately, was possessed by one of the authors, Summers, a former professional musician with a Master's degree in music.

Some authors were listed under more than one department, with twenty-nine duplicates, two triplicates and one quadruplicate. It was decided that it was not necessary to make special allowances for this case as Research Assessment Exercise rules allow staff to be counted in more than one department.

Now that the Arts and Humanities Citation Index generally includes co-authors, a significant earlier objection to the citation counting process has been removed ([Norris & Oppenheim 2003](#noop03): 717). However, this only applies to the authors of articles from journals indexed by the Arts and Humanities Citation Index and not to the citations contained within those articles. During the searching procedure, some instances of second authors being disadvantaged were found. For example, _Rethinking music_ ( [Cook & Everist 1999](#coev99)) was jointly edited, but Cook received fourteen citations for it and Everist only one.

This could be a significant problem in obtaining accurate counts. However, as Oppenheim ( [1997](#opp97)) points out, the aim is to achieve a comparative ranking rather than an absolute count, as the specific count data is lost when rankings are compiled for correlation.

Self-citation, whilst present, did not have undue influence on the citation counts. Snyder and Bonzi show that the humanities have a very low self-citation rate (3%) compared to other fields and suggest that this is because humanities scholarship is more non-incremental in nature when compared with the sciences ([Snyder & Bonzi 1998](#snbo98): 436).

The length of time between the end of the survey period (i.e., 1994-2000) and the counting process (2006) was deemed sufficient to allow even the material published at the very end of the period to accrue citations, removing any disadvantages to those submitting newer material. Furthermore, it was found that the date range of submissions was relatively even across all departments, suggesting that if counting were to take place closer to the end of the survey period all departments would be comparably affected.

A study of the citing patterns of sociologists found that authors form two distinct groups, one favouring journals and the other monographs ([Cronin _et al._ 1997](#crsnat97)) and this has been noted as a possible influencing feature of such studies ([Norris & Oppenheim 2003](#noop03): 718). There is a larger number of permissible submission types in music compared to other and the authors studied here have submitted a full range of these. Given differing citation practices within music, it is possible that some departments may be disadvantaged unduly by the nature of their constituent authors' output. This is discussed below.

As noted above, Moed ([2005](#moe05)) notes that the Arts and Humanities Citation Index has only a moderate (below 40%) coverage of core sources. He suggests that in such moderate cases, 'citation analysis based on the WoK sources plays a limited role or no role at all in a research assessment study' ([Moed 2005](#moe05): 142). However, with the difficulties in conducting source-expanded analyses ([Moed 2005](#moe05): 142), the case for using Web of Knowledge sources is compelling as it provides a relatively quick and straight-forward method of testing a citation-based hypothesis, an important consideration in any time-limited study.

This study takes all the above factors into account, leading to the most representative ranking of departments possible from the data available.

### Calculations

#### Citation data

If N is the number of authors in a department and C is the number of citations an author receives, then the total number of citations received by all authors in a department is T<sub>D</sub>, the departmental total:

<div align="center">![departmental total equation.jpg](p342fig2.jpg)</div>

where a particular author in question within a department is represented by the index _i_ such that all authors can be considered by letting _i_=1,2,3,',N-1,N.

For each department, A<sub>D</sub> denotes the number of authors in that department, expressed as a full time equivalent. This information was supplied by the Research Assessment Exercise in the publication of its results ([HERO 2002](#her02)).

Four values were calculated for each of the fifty-nine departments:

1.  T<sub>D</sub> (departmental total citation count)
2.  T<sub>D</sub>/N (departmental mean citation count)
3.  T<sub>D</sub>/A<sub>D</sub> (departmental total divided by FTE of authors)
4.  (T<sub>D</sub>/N)A<sub>D</sub> (departmental mean multiplied by FTE of authors)

The list of departments was then ordered seven times: by the 2001 Exercise score; by each of the results of the four calculations above; by the total number of submitted articles that each department published in Arts and Humanities Citation Index-indexed journals; and by A<sub>D</sub>. Ranks were assigned according to those orders. Additionally, authors were ranked according to their individual citation counts.

#### Correlation tests

This study performed the same statistical test used by other studies (for example, [Seng & Willet 1995](#sewi95), [Smith & Eysenck 2002](#smey02), [Norris & Oppenheim 2003](#noop03)), namely the Spearman rank order correlation coefficient. This test indicates whether a correlation exists between two sets of rankings, giving a value of r<sub>s</sub> where r<sub>s</sub>=±1 indicates a perfect correlation and r<sub>s</sub>=0 indicates no correlation.

The Research Assessment Exercise rankings were paired with each of the departmental rankings. For the ranking of individual authors, their ranks were paired with the Research Assessment Exercise rank of their department. The correlation calculations for each set of pairs were run using SPSS software (version 14) and a figure for r<sub>s</sub> was returned in each case, along with a figure for the level of statistical significance.

## Results

### Citation count results

<table width="85%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: : departmental Research Assessment Exercise scores and citation count rankings**  

N.B. Where Research Assessment Exercise scores are shared by two or more departments, the departments are displayed in the order given in the results list made publicly available by the Research Assessment Exercise on HERO ([2002](#her02)). The underlying raw data can be seen in Summers ([2007](#sum07): 70), along with data for individual authors.</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th width="288" colspan="6" align="center">Rankings</th>

</tr>

<tr>

<th>Department</th>

<th>RAE score</th>

<th>T<sub>D</sub></th>

<th>T<sub>D</sub>/N</th>

<th>T<sub>D</sub>/A<sub>D</sub></th>

<th>(T<sub>D</sub>/N)A<sub>D</sub></th>

<th>AHCI journal</th>

<th>A<sub>D</sub></th>

</tr>

<tr>

<td>University of Birmingham</td>

<td align="center">5*</td>

<td align="center">26</td>

<td align="center">25</td>

<td align="center">26</td>

<td align="center">29</td>

<td align="center">17=</td>

<td align="center">23=</td>

</tr>

<tr>

<td>University of Cambridge</td>

<td align="center">5*</td>

<td align="center">4</td>

<td align="center">8</td>

<td align="center">9</td>

<td align="center">4</td>

<td align="center">1</td>

<td align="center">3</td>

</tr>

<tr>

<td>City University</td>

<td align="center">5*</td>

<td align="center">13</td>

<td align="center">5</td>

<td align="center">7</td>

<td align="center">10</td>

<td align="center">28=</td>

<td align="center">33</td>

</tr>

<tr>

<td>University of Manchester</td>

<td align="center">5*</td>

<td align="center">9</td>

<td align="center">13</td>

<td align="center">8</td>

<td align="center">9</td>

<td align="center">46=</td>

<td align="center">15=</td>

</tr>

<tr>

<td>University of Newcastle</td>

<td align="center">5*</td>

<td align="center">29</td>

<td align="center">31</td>

<td align="center">31</td>

<td align="center">30</td>

<td align="center">10</td>

<td align="center">23=</td>

</tr>

<tr>

<td>University of Nottingham</td>

<td align="center">5*</td>

<td align="center">12</td>

<td align="center">14</td>

<td align="center">10</td>

<td align="center">14</td>

<td align="center">17=</td>

<td align="center">23=</td>

</tr>

<tr>

<td>University of Oxford</td>

<td align="center">5*</td>

<td align="center">3</td>

<td align="center">9</td>

<td align="center">5</td>

<td align="center">5</td>

<td align="center">4=</td>

<td align="center">1=</td>

</tr>

<tr>

<td>Royal Holloway, University of London</td>

<td align="center">5*</td>

<td align="center">5</td>

<td align="center">7</td>

<td align="center">3</td>

<td align="center">6</td>

<td align="center">7=</td>

<td align="center">8=</td>

</tr>

<tr>

<td>University of Southampton</td>

<td align="center">5*</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">8=</td>

</tr>

<tr>

<td>University of Bristol</td>

<td align="center">5</td>

<td align="center">18</td>

<td align="center">17</td>

<td align="center">17</td>

<td align="center">17</td>

<td align="center">11=</td>

<td align="center">23=</td>

</tr>

<tr>

<td>Goldsmiths College</td>

<td align="center">5</td>

<td align="center">15=</td>

<td align="center">21</td>

<td align="center">22</td>

<td align="center">15</td>

<td align="center">3</td>

<td align="center">12</td>

</tr>

<tr>

<td>University of Huddersfield</td>

<td align="center">5</td>

<td align="center">37=</td>

<td align="center">35</td>

<td align="center">35</td>

<td align="center">33</td>

<td align="center">34=</td>

<td align="center">23=</td>

</tr>

<tr>

<td>University of Hull</td>

<td align="center">5</td>

<td align="center">22</td>

<td align="center">15</td>

<td align="center">14</td>

<td align="center">22</td>

<td align="center">34=</td>

<td align="center">39=</td>

</tr>

<tr>

<td>King's College London</td>

<td align="center">5</td>

<td align="center">6</td>

<td align="center">3</td>

<td align="center">6</td>

<td align="center">3</td>

<td align="center">17=</td>

<td align="center">13</td>

</tr>

<tr>

<td>School of Oriental and African Studies</td>

<td align="center">5</td>

<td align="center">20</td>

<td align="center">18</td>

<td align="center">15</td>

<td align="center">23</td>

<td align="center">17=</td>

<td align="center">36</td>

</tr>

<tr>

<td>University of Sheffield</td>

<td align="center">5</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">7=</td>

<td align="center">15=</td>

</tr>

<tr>

<td>University of Sussex</td>

<td align="center">5</td>

<td align="center">35=</td>

<td align="center">10</td>

<td align="center">27</td>

<td align="center">24</td>

<td align="center">34=</td>

<td align="center">52=</td>

</tr>

<tr>

<td>University of York</td>

<td align="center">5</td>

<td align="center">14</td>

<td align="center">30</td>

<td align="center">24</td>

<td align="center">20</td>

<td align="center">15=</td>

<td align="center">7</td>

</tr>

<tr>

<td>University of Wales, Bangor</td>

<td align="center">5</td>

<td align="center">21</td>

<td align="center">23</td>

<td align="center">21</td>

<td align="center">27</td>

<td align="center">34=</td>

<td align="center">22</td>

</tr>

<tr>

<td>Cardiff University</td>

<td align="center">5</td>

<td align="center">11</td>

<td align="center">11</td>

<td align="center">11</td>

<td align="center">11</td>

<td align="center">17=</td>

<td align="center">20=</td>

</tr>

<tr>

<td>The Queen's University of Belfast</td>

<td align="center">5</td>

<td align="center">25</td>

<td align="center">24</td>

<td align="center">25</td>

<td align="center">28</td>

<td align="center">4=</td>

<td align="center">23=</td>

</tr>

<tr>

<td>Dartington College of Arts</td>

<td align="center">4</td>

<td align="center">40</td>

<td align="center">41=</td>

<td align="center">40=</td>

<td align="center">42</td>

<td align="center">46=</td>

<td align="center">34=</td>

</tr>

<tr>

<td>De Montfort University</td>

<td align="center">4</td>

<td align="center">39</td>

<td align="center">29</td>

<td align="center">30</td>

<td align="center">36</td>

<td align="center">34=</td>

<td align="center">50</td>

</tr>

<tr>

<td>University of Durham</td>

<td align="center">4</td>

<td align="center">17</td>

<td align="center">16</td>

<td align="center">18=</td>

<td align="center">12</td>

<td align="center">4=</td>

<td align="center">15=</td>

</tr>

<tr>

<td>University of East Anglia</td>

<td align="center">4</td>

<td align="center">42=</td>

<td align="center">36</td>

<td align="center">36=</td>

<td align="center">43</td>

<td align="center">17=</td>

<td align="center">52=</td>

</tr>

<tr>

<td>University of Exeter</td>

<td align="center">4</td>

<td align="center">34</td>

<td align="center">34</td>

<td align="center">33</td>

<td align="center">32</td>

<td align="center">11=</td>

<td align="center">30=</td>

</tr>

<tr>

<td>Keele University</td>

<td align="center">4</td>

<td align="center">23=</td>

<td align="center">22</td>

<td align="center">23</td>

<td align="center">21</td>

<td align="center">11=</td>

<td align="center">23=</td>

</tr>

<tr>

<td>Lancaster University</td>

<td align="center">4</td>

<td align="center">23=</td>

<td align="center">4</td>

<td align="center">13</td>

<td align="center">13</td>

<td align="center">11=</td>

<td align="center">46=</td>

</tr>

<tr>

<td>University of Leeds</td>

<td align="center">4</td>

<td align="center">8</td>

<td align="center">12</td>

<td align="center">12</td>

<td align="center">7</td>

<td align="center">7=</td>

<td align="center">10</td>

</tr>

<tr>

<td>University of Liverpool</td>

<td align="center">4</td>

<td align="center">19</td>

<td align="center">20</td>

<td align="center">18=</td>

<td align="center">18</td>

<td align="center">46=</td>

<td align="center">20=</td>

</tr>

<tr>

<td>Open University</td>

<td align="center">4</td>

<td align="center">10</td>

<td align="center">6</td>

<td align="center">4</td>

<td align="center">8</td>

<td align="center">15=</td>

<td align="center">30=</td>

</tr>

<tr>

<td>Royal Academy of Music</td>

<td align="center">4</td>

<td align="center">7</td>

<td align="center">28</td>

<td align="center">16</td>

<td align="center">16</td>

<td align="center">28=</td>

<td align="center">1=</td>

</tr>

<tr>

<td>Royal College of Music</td>

<td align="center">4</td>

<td align="center">27</td>

<td align="center">41=</td>

<td align="center">36=</td>

<td align="center">31</td>

<td align="center">28=</td>

<td align="center">4</td>

</tr>

<tr>

<td>Royal Northern College of Music</td>

<td align="center">4</td>

<td align="center">31=</td>

<td align="center">48</td>

<td align="center">42</td>

<td align="center">40</td>

<td align="center">46=</td>

<td align="center">6</td>

</tr>

<tr>

<td>University of Salford</td>

<td align="center">4</td>

<td align="center">35=</td>

<td align="center">39=</td>

<td align="center">38</td>

<td align="center">35</td>

<td align="center">28=</td>

<td align="center">19</td>

</tr>

<tr>

<td>University of Edinburgh</td>

<td align="center">4</td>

<td align="center">15=</td>

<td align="center">26</td>

<td align="center">20</td>

<td align="center">19</td>

<td align="center">28=</td>

<td align="center">14</td>

</tr>

<tr>

<td>University of Glasgow</td>

<td align="center">4</td>

<td align="center">37=</td>

<td align="center">19</td>

<td align="center">32</td>

<td align="center">26</td>

<td align="center">34=</td>

<td align="center">37=</td>

</tr>

<tr>

<td>Bath Spa University College</td>

<td align="center">3a</td>

<td align="center">54=</td>

<td align="center">55</td>

<td align="center">55</td>

<td align="center">55</td>

<td align="center">34=</td>

<td align="center">32</td>

</tr>

<tr>

<td>Bretton Hall</td>

<td align="center">3a</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">46=</td>

<td align="center">46=</td>

</tr>

<tr>

<td>University of Central England in Birmingham</td>

<td align="center">3a</td>

<td align="center">28</td>

<td align="center">46</td>

<td align="center">40=</td>

<td align="center">37</td>

<td align="center">17=</td>

<td align="center">5</td>

</tr>

<tr>

<td>Oxford Brookes University</td>

<td align="center">3a</td>

<td align="center">45=</td>

<td align="center">37=</td>

<td align="center">45=</td>

<td align="center">39</td>

<td align="center">17=</td>

<td align="center">39=</td>

</tr>

<tr>

<td>University of Reading</td>

<td align="center">3a</td>

<td align="center">42=</td>

<td align="center">33</td>

<td align="center">43</td>

<td align="center">34</td>

<td align="center">34=</td>

<td align="center">37=</td>

</tr>

<tr>

<td>University of Surrey</td>

<td align="center">3a</td>

<td align="center">30</td>

<td align="center">27</td>

<td align="center">34</td>

<td align="center">25</td>

<td align="center">17=</td>

<td align="center">15=</td>

</tr>

<tr>

<td>University of Surrey Roehampton</td>

<td align="center">3a</td>

<td align="center">45=</td>

<td align="center">45</td>

<td align="center">45=</td>

<td align="center">44</td>

<td align="center">17=</td>

<td align="center">39=</td>

</tr>

<tr>

<td>University of Aberdeen</td>

<td align="center">3a</td>

<td align="center">33</td>

<td align="center">32</td>

<td align="center">28</td>

<td align="center">38</td>

<td align="center">28=</td>

<td align="center">49</td>

</tr>

<tr>

<td>University of Ulster</td>

<td align="center">3a</td>

<td align="center">52=</td>

<td align="center">43=</td>

<td align="center">45=</td>

<td align="center">47=</td>

<td align="center">17=</td>

<td align="center">57=</td>

</tr>

<tr>

<td>Anglia Polytechnic University</td>

<td align="center">3b</td>

<td align="center">42=</td>

<td align="center">49=</td>

<td align="center">44</td>

<td align="center">50</td>

<td align="center">34=</td>

<td align="center">34=</td>

</tr>

<tr>

<td>Canterbury Christ Church University College</td>

<td align="center">3b</td>

<td align="center">49=</td>

<td align="center">49=</td>

<td align="center">50</td>

<td align="center">53</td>

<td align="center">46=</td>

<td align="center">45</td>

</tr>

<tr>

<td>University of Hertfordshire</td>

<td align="center">3b</td>

<td align="center">31=</td>

<td align="center">37=</td>

<td align="center">29</td>

<td align="center">41</td>

<td align="center">46=</td>

<td align="center">44</td>

</tr>

<tr>

<td>Kingston University</td>

<td align="center">3b</td>

<td align="center">47=</td>

<td align="center">47</td>

<td align="center">51=</td>

<td align="center">46</td>

<td align="center">34=</td>

<td align="center">39=</td>

</tr>

<tr>

<td>Liverpool Hope</td>

<td align="center">3b</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">46=</td>

<td align="center">57=</td>

</tr>

<tr>

<td>London Guildhall University</td>

<td align="center">3b</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">46=</td>

<td align="center">51</td>

</tr>

<tr>

<td>University College Northampton</td>

<td align="center">3b</td>

<td align="center">49=</td>

<td align="center">43=</td>

<td align="center">45=</td>

<td align="center">45</td>

<td align="center">34=</td>

<td align="center">52=</td>

</tr>

<tr>

<td>Royal Scottish Academy of Music and Drama</td>

<td align="center">3b</td>

<td align="center">41</td>

<td align="center">54</td>

<td align="center">53</td>

<td align="center">49</td>

<td align="center">46=</td>

<td align="center">11</td>

</tr>

<tr>

<td>Liverpool John Moores University</td>

<td align="center">2</td>

<td align="center">54=</td>

<td align="center">53</td>

<td align="center">54</td>

<td align="center">54</td>

<td align="center">46=</td>

<td align="center">52=</td>

</tr>

<tr>

<td>St Martin's College</td>

<td align="center">2</td>

<td align="center">52=</td>

<td align="center">39=</td>

<td align="center">39</td>

<td align="center">47=</td>

<td align="center">34=</td>

<td align="center">59</td>

</tr>

<tr>

<td>Thames Valley University</td>

<td align="center">2</td>

<td align="center">49=</td>

<td align="center">49=</td>

<td align="center">51=</td>

<td align="center">51</td>

<td align="center">46=</td>

<td align="center">39=</td>

</tr>

<tr>

<td>Napier University</td>

<td align="center">2</td>

<td align="center">47=</td>

<td align="center">49=</td>

<td align="center">49</td>

<td align="center">52</td>

<td align="center">46=</td>

<td align="center">48</td>

</tr>

<tr>

<td>University College Chichester</td>

<td align="center">1</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">56=</td>

<td align="center">46=</td>

<td align="center">52=</td>

</tr>

</tbody>

</table>

### Correlation test results

<table width="50%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size:
smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif;
background-color: #fdffdd"><caption align="bottom">  
**Table 4: Spearman rank order correlation coefficient test results**</caption>

<tbody>

<tr>

<th>Test for correlation between Research Assessment Exercise scores and:</th>

<th>r<sub>S</sub></th>

</tr>

<tr>

<td>T<sub>D</sub></td>

<td>0.80</td>

</tr>

<tr>

<td>T<sub>D</sub>/A<sub>D</sub></td>

<td>0.81</td>

</tr>

<tr>

<td>(T<sub>D</sub> x A<sub>D</sub>)/N</td>

<td>0.81</td>

</tr>

<tr>

<td>Number of articles in Arts and Humanities Citation Index-indexed journals</td>

<td>0.60</td>

</tr>

<tr>

<td>A<sub>D</sub></td>

<td>0.56</td>

</tr>

<tr>

<td>Individual citation counts</td>

<td>0.46</td>

</tr>

</tbody>

</table>

Results for r<sub>s</sub> are shown rounded to two decimal places. All correlation scores are highly statistically significant at the 0.01% level.

### Hypothesis

The null hypothesis tested in this study (that there is no correlation between citation counts of authors working in UK university music departments and the scores awarded to those departments by the 2001 Exercise) was disproved. The results show that there is a strong correlation at departmental level and a weaker correlation at individual level.

## Analysis and discussion

### Citations

The 670 authors had a total count of 3487 citations. The range of individual counts was 0-140 citations, with a mean of 5.2 citations. The mean for the 372 cited authors (56%) was 9.4 citations. 298 authors (44%) had a count of 0 and 221 authors (33%) had a count of 1-6\.

Twenty authors appear twice and one author appears three times, accounting for 258 or 7.4% of the total number of citations (an author's total is counted each time their name appears in the list, the non-duplicated count for these authors being 129). Removing duplications, the authors received a collective total of 3348 citations, with a range of 0-140 and a mean of 5.2 citations (9.3 for cited authors).

The top count of 140 citations was achieved by Davidson JW of the University of Sheffield. The ten highest scoring authors account for 730 citations, representing around 20% of all citations (see Table 5 below). An additional 52 citations are accounted for if the duplication of Birtwistle H (King's College, London and Royal Academy of Music) is included.

<table width="85%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size:
smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif;
background-color: #fdffdd"><caption align="bottom">  
**Table 5: ten most cited authors**  
* Historical musicology, theory, empirical musicology/psychology of music  
** Both theory and composition.</caption>

<tbody>

<tr>

<th>Author name</th>

<th>Department</th>

<th>Field</th>

<th>Count</th>

</tr>

<tr>

<td>Davidson JW</td>

<td>University of Sheffield</td>

<td>Music psychology</td>

<td>140</td>

</tr>

<tr>

<td>Cook NJ</td>

<td>University of Southampton</td>

<td>Musicology (various*)</td>

<td align="center">127</td>

</tr>

<tr>

<td>Clarke EF</td>

<td>University of Sheffield</td>

<td>Music psychology</td>

<td align="center">90</td>

</tr>

<tr>

<td>Bent M</td>

<td>University of Oxford</td>

<td>Historical musicology</td>

<td align="center">77</td>

</tr>

<tr>

<td>Fallows DN</td>

<td>University of Manchester</td>

<td>Historical musicology</td>

<td align="center">70</td>

</tr>

<tr>

<td>Birtwistle H</td>

<td>King's College London/Royal Academy of Music</td>

<td>Composition</td>

<td align="center">52</td>

</tr>

<tr>

<td>Burden M</td>

<td>University of Oxford</td>

<td>Historical musicology</td>

<td align="center">43</td>

</tr>

<tr>

<td>Strohm R</td>

<td>University of Oxford</td>

<td>Historical musicology</td>

<td align="center">41</td>

</tr>

<tr>

<td>Emmerson ST</td>

<td>City University</td>

<td>Electroacoustics**</td>

<td align="center">39</td>

</tr>

<tr>

<td>Burrows DJ</td>

<td>Open University</td>

<td>Historical musicology</td>

<td align="center">38</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>(Total 730)</td>

</tr>

</tbody>

</table>

Music psychology can be seen to be an area of music in which it is possible to receive a noticeably larger number of citations, work in this field accounting for the first- and third-placed counts and a proportion of the second-placed count.

The citation counts show a distribution that approximately follows the '80/20 rule' (Pareto's Law). The top 20% or 134 authors account for 2821 citations or 80.9% of the total number of citations.

For comparison with a similar study ([Norris & Oppenheim 2003](#noop03)), Unit of Assessment 58 (archaeology) had 682 authors with a collective total of 6213 citations. The range of individual counts was 0-565, with a mean of 9.1 citations. The mean for the 420 cited authors (62%) was 14.8, with just over 50% of these having a count ranging from one to six citations.

### Correlations

The values for r<sub>s</sub> shown in Table 4 above indicate a strong correlation between citation counts and the 2001 Exercise scores for UK university music departments in four tests. For the remaining three tests, between Research Assessment Exercise scores and individual counts, numbers for authors and Arts and Humanities Citation Index articles, there is a weaker correlation.

Four of the correlations between departmental citation counts and respective Research Assessment Exercise scores are all reasonably similar, each with a value of r<sub>s</sub>=0.80 or r<sub>s</sub>=0.81\. Below is an example of a bivariate scatterplot of the underlying data for one of these results (see Figure 2 below), in this case the correlation between Research Assessment Exercise scores and T<sub>D</sub> where r<sub>s</sub>=0.80\. These correlations are again seen to be similar to the findings of Norris and Oppenheim (2003: 722). These compare with two results of r<sub>s</sub>=0.80 for music.

<div align="center">![Figure2.jpg](p342fig3.jpg)</div>

<div>  
**Figure 2: bivariate scatterplot for the correlation between Research Assessment Exercise scores and T<sub>D</sub>**</div>

In this scatterplot, one can see the general trend for higher Research Assessment Exercise score correlating with higher citation count total. Apart from the highest total in Research Assessment Exercise score 5, there is a continuous upward trend for both upper and lower edges of each score grouping. It is interesting to note that departments only achieve more than the departmental mean total (59 citations) at Research Assessment Exercise score 4 and above, the point at which funding is awarded by HEFCE ([2006](#hef06)).

However, Figure 2 shows the amount of detail that can be lost in a value for r<sub>s</sub>. Especially notable are a number of anomalous outliers, the most striking of which is Sheffield University at the top of the 5 group. This department employs the top- and third-placed authors, who together account for over two-thirds (230) of the departmental total (316). This raises the possibility that, if research monies were handed out according to citation counts, a citation count-led 'transfer market' could result, where authors could take their high counts and offer them to the highest bidders, who in turn could make financial gains.

Conversely, examples can be seen of departments achieving low scores despite the collective excellence of the constituent authors (as measured by the 2001 Exercise). The Universities of Birmingham and Newcastle in the 5* group and University of Huddersfield in the 5s stand out at the bottom, the simple fact that they are there casting doubt on citation counting as a sound basis for a system for assessing research, with each department doing comparatively badly in the first four rankings.

Two possible factors in these results are discussed below, namely Arts and Humanities Citation Index coverage and authors' output types.

The continuous upward trend is again striking when looking at the mean departmental citation count within Research Assessment Exercise score groups, as seen in Table 6 below. Here, there is an exact correlation (r<sub>s</sub>=1).

<table width="40%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size:
smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif;
background-color: #fdffdd"><caption align="bottom">  
**Table 6: mean departmental citation count for Research Assessment Exercise score groups**</caption>

<tbody>

<tr>

<th>RAE score</th>

<th>Mean no of citations per dept.</th>

</tr>

<tr>

<td align="center">5*</td>

<td align="center">148.00</td>

</tr>

<tr>

<td align="center">5</td>

<td align="center">88.08</td>

</tr>

<tr>

<td align="center">4</td>

<td align="center">55.25</td>

</tr>

<tr>

<td align="center">3a</td>

<td align="center">12.22</td>

</tr>

<tr>

<td align="center">3b</td>

<td align="center">7.50</td>

</tr>

<tr>

<td align="center">2</td>

<td align="center">4.25</td>

</tr>

<tr>

<td align="center">1</td>

<td align="center">0.00</td>

</tr>

</tbody>

</table>

An approximation of personal scores could be made by assigning the score of an author's department to that author. This approximation is based on the explanations of the range of scores, which show an incremental nature, stating the proportion of national and international standard research activity within each department (see Table 1 above). Increases in a department's score indicate increases in the quality of the research activity within that department as a whole (in the judgement of the panel). The assessment of the quality of the research output of a department's authors plays a prominent role in the calculation of the Assessment score. (There is no public information to suggest that individual authors are graded but, having been on the music sub-panel for the 2001 Exercise, one of the academics interviewed during this study confirmed that such gradings are used.) Therefore, it can be assumed that there would be a strong correlation between the score of a department and any score given to each author by the sub-panel assessing it.

Using this approximation to run a further correlation test, there was a weaker correlation between the constituent authors' citation counts and the Exercise score of their department, with r<sub>s</sub>=0.46, but this is still significant at the 0.01% level.

Looking at the correlation of approximate personal scores in conjunction with the two groupings seen above (authors summed into their departments and departments summed with others of the same Research Assessment Exercise score), a pattern can be seen to emerge. Where the resolution of the analysis is most detailed (i.e., at the individual author count level where N is a large number) the correlation is weakest. Conversely, where the resolution is at its least detailed (i.e., at the Research Assessment Exercise score level where N is small) the correlation is strongest. This is likely to be due to the averaging effect of the summation of counts into larger units where the small scale detail in the variation of counts is ironed-out (see Figure 3 below).

<div align="center">![Figure3.jpg](p342fig4.jpg)</div>

<div>  
**Figure 3: effect of summation on correlations**  
(r=Research Assessment Exercise score group, d=department, a=author)</div>

#### Submission types

Given that the music sub-panel allows a greater number of submission types than other panels, it is possible to categorise authors in into distinct groups. At the most general level, one can see a split between research that involves publishing written material on the one hand and, on the other hand, practice-based research which produces performances and compositions. A summary of authors' submission types is shown in Table 8 below.

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 7: submission types**</caption>

<tbody>

<tr>

<th>Submission type</th>

<th>Instances</th>

</tr>

<tr>

<td>_Written research_  
     Authored book  
     Chapter in book  
     Edited book  
     Journal article</td>

<td align="right">   
178       
367       
35       
445     </td>

</tr>

<tr>

<td>_Practice-based research_  
     Composition  
     Performance  
     Artefact  
     Other (CD)  
     Design  
     Conference contribution</td>

<td valign="bottom" nowrap="nowrap" align="right">   
650       
429       
8       
221       
1       
77     </td>

</tr>

<tr>

<td valign="bottom" nowrap="nowrap">_Other_  
     Chapter/software  
     Exhibition  
     Internet publication  
     Other (not CD)  
     Report  
     Scholarly edition  
     Software  
      – (no submission)</td>

<td align="right">   
1       
3       
28       
50       
1       
76       
9       
101     </td>

</tr>

<tr>

<td>Total</td>

<td align="right">2680     </td>

</tr>

</tbody>

</table>

#### Submission predominance—authors

The collected data were analysed to identify trends regarding submission type. In the case of each author, a 'submission predominance' was assigned to show their favoured output type. A summary of these predominances is shown in Table 8 below, grouping them into four wider categories: 'composition', 'performance', 'writing' and 'other'. (The last of these is a short form of the Research Assessment Exercise formulation, 'other form of assessable output'.)

<table width="40%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 8: Authors' submission type predominances**</caption>

<tbody>

<tr>

<th>Predominance categories</th>

<th>Instances</th>

</tr>

<tr>

<td>Composition</td>

<td align="right">153     </td>

</tr>

<tr>

<td>Performance</td>

<td align="right">156     </td>

</tr>

<tr>

<td>Writing  
     Books  
     Journal articles  
     Mixed writing</td>

<td align="right">253       
(99)       
(60)       
(94)     </td>

</tr>

<tr>

<td>Other  
     Artefact  
     Conference contribution  
     Internet publication  
     Mixed  
     Mixed practice  
     Scholarly edition  
     Other</td>

<td align="right">108       
(2)       
(7)       
(1)       
(83)       
(9)       
(4)       
(2)     </td>

</tr>

<tr>

<td valign="bottom">Total</td>

<td valign="bottom" align="right">670     </td>

</tr>

</tbody>

</table>

This method gives an approximate view of the music community which is useful for purposes of analysis. It should not be taken as an accurate categorisation but one that allows a good general view from which some conclusions can be drawn.

Arranging the authors into groups by their submission types and examining the citation counts achieved by each group reveals a marked difference in citation count ranges and averages (see Table 9 below).

<table width="85%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 9: Citation count ranges for submission type predominances**  
* number of authors receiving one citation or more.</caption>

<tbody>

<tr>

<th>Predominant submission type</th>

<th>Total no. citations</th>

<th>Mean count</th>

<th>Range</th>

<th>No. authors</th>

<th>No. cited authors*</th>

<th>% of total citations</th>

</tr>

<tr>

<td>Composition</td>

<td align="center">274</td>

<td align="center">1.8</td>

<td align="center">0-52</td>

<td align="center">153</td>

<td align="center">46 (30%)</td>

<td align="center">7.9</td>

</tr>

<tr>

<td>Performance</td>

<td align="center">136</td>

<td align="center">0.9</td>

<td align="center">0-12</td>

<td align="center">156</td>

<td align="center">54 (35%)</td>

<td align="center">3.9</td>

</tr>

<tr>

<td>Writing  
     Books  
     Journal articles  
     Mixed writing</td>

<td align="center">2717  
(1168)  
(653)  
(896)</td>

<td align="center">10.7  
(11.8)  
(10.9)  
(9.5)</td>

<td align="center">0-140  
0-127  
0-140  
0-70</td>

<td align="center">253  
99  
60  
94</td>

<td align="center">211 (84%)  
85 (86%)  
52 (87%)  
74 (79%)</td>

<td align="center">77.9   

 </td>

</tr>

<tr>

<td>Other  
     Artefact  
     Conference contrib.  
     Internet publication  
     Mixed  
     Mixed practice  
     Other  
     Scholarly edition</td>

<td align="center">360  
(0)  
(23)  
(1)  
(291)  
(18)  
(0)  
(27)</td>

<td align="center">3.33  
(0)  
(3.3)  
(1)  
(3.5)  
(2)  
(0)  
(6.6)</td>

<td align="center">0-26  
0  
0-14  
1  
0-26  
0-16  
0  
0-15</td>

<td align="center">108  
2  
7  
1  
83  
9  
2  
4</td>

<td align="center">61 (56%)  
0 (0%)  
3 (43%)  
1 (100%)  
50 (60%)  
3 (33%)  
0 (0%)  
4 (100%)</td>

<td align="center">10.3   

</td>

</tr>

<tr>

<td>Total</td>

<td align="center">3487</td>

<td align="center">–</td>

<td align="center">–</td>

<td align="center">670</td>

<td align="center">372 (56%)</td>

<td align="center">100</td>

</tr>

</tbody>

</table>

Based on these predominance categorisations, it can be seen that those producing practice-based research (notably composers and performers) are much less likely to receive citations than those producing published writing. Those submitting predominantly written materials (i.e., books, journals and mixed writing) receive a much greater proportion of the total citations (77.9% of the total) and have the highest average of the main submission type groups (10.7 citations overall). Furthermore, the proportion of authors receiving at least one citation is significantly higher in these groups.

Incidentally, the significance of books in the study of music can be seen in the fact that within the data collected those writings predominantly in books are cited almost twice as often as journals. This implies that a solely journal-based data source cannot truly reflect music as a subject.

That composers and performers received fewer citations than writers is mirrored by their lower production of citations. A performer performs a piece of music, either in public or on a recording. This performance cannot cite research in any meaningfully explicit way, except perhaps in writings associated with a performance (such as programme and CD booklet notes) and most citations would come from concert reviews. A composition is a piece of music that can exist either on the page in notation or in a listener's perception. It can be played in public, recorded and sold or broadcast and it can be cited in written work by others. However, as with performance, it cannot cite other work in the way seen in traditional text-based scholarly research and thus cannot contribute to citation indexes. This leaves practice-based authors at a disadvantage in citation analysis.

#### Submission predominance—departments

Similar indications of predominance were produced at a departmental level, using the categories of writing, composition, performance or mixed. Again, this produces a crude approximation of the typical output of each department, which is instructive in analysis (see Table 10 below).

<table width="40%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 10: submission type predominances for departments**</caption>

<tbody>

<tr>

<th>Predominant submission types</th>

<th>Number</th>

</tr>

<tr>

<td>Writing</td>

<td align="center">26</td>

</tr>

<tr>

<td>Mixed</td>

<td align="center">21</td>

</tr>

<tr>

<td>Performance</td>

<td align="center">7</td>

</tr>

<tr>

<td>Composition</td>

<td align="center">5</td>

</tr>

<tr>

<td>Total</td>

<td align="center">59</td>

</tr>

</tbody>

</table>

It can be seen in Table 11 below that those departments with high values for T<sub>D</sub> are most likely to be classed as writing departments. Conversely, those with low values for T<sub>D</sub> are most likely not to be writing departments. Also, writing departments are more likely to have a higher Research Assessment Exercise score: seven writing departments with 5*, nine with 5, seven with 4 and three with 3a (there are no departments with 1–3b).

<table width="80%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 11: departments and their predominant submission types, in order of decreasing T<sub>D</sub>**</caption>

<tbody>

<tr>

<th>Department</th>

<th>RAE score</th>

<th>T<sub>D</sub></th>

<th>DPr</th>

<th>Department</th>

<th>RAE score</th>

<th>T<sub>D</sub></th>

<th>DPr</th>

</tr>

<tr>

<td>University of Sheffield</td>

<td align="center">5</td>

<td>316</td>

<td>W</td>

<td>University of Hertfordshire</td>

<td align="center">3b</td>

<td>25</td>

<td>C</td>

</tr>

<tr>

<td>University of Southampton</td>

<td align="center">5*</td>

<td>279</td>

<td>W</td>

<td>Royal Northern College of Music</td>

<td align="center">4</td>

<td>25</td>

<td>P</td>

</tr>

<tr>

<td>University of Oxford</td>

<td align="center">5*</td>

<td>260</td>

<td>W</td>

<td>University of Aberdeen</td>

<td align="center">3a</td>

<td>23</td>

<td>W</td>

</tr>

<tr>

<td>University of Cambridge</td>

<td align="center">5*</td>

<td>218</td>

<td>W</td>

<td>University of Exeter</td>

<td align="center">4</td>

<td>22</td>

<td>M</td>

</tr>

<tr>

<td>Royal Holloway</td>

<td align="center">5*</td>

<td>197</td>

<td>W</td>

<td>University of Salford</td>

<td align="center">4</td>

<td>20</td>

<td>M</td>

</tr>

<tr>

<td>King's College London</td>

<td align="center">5</td>

<td>162</td>

<td>M</td>

<td>University of Sussex</td>

<td align="center">5</td>

<td>20</td>

<td>W</td>

</tr>

<tr>

<td>Royal Academy of Music</td>

<td align="center">4</td>

<td>156</td>

<td>P</td>

<td>University of Huddersfield</td>

<td align="center">5</td>

<td>19</td>

<td>M</td>

</tr>

<tr>

<td>University of Leeds</td>

<td align="center">4</td>

<td>132</td>

<td>W</td>

<td>University of Glasgow</td>

<td align="center">4</td>

<td>19</td>

<td>M</td>

</tr>

<tr>

<td>University of Manchester</td>

<td align="center">5*</td>

<td>124</td>

<td>M</td>

<td>De Montfort University</td>

<td align="center">4</td>

<td>17</td>

<td>M</td>

</tr>

<tr>

<td>Open University</td>

<td align="center">4</td>

<td>113</td>

<td>W</td>

<td>Dartington College of Arts</td>

<td align="center">4</td>

<td>12</td>

<td>M</td>

</tr>

<tr>

<td>Cardiff University</td>

<td align="center">5</td>

<td>99</td>

<td>W</td>

<td>Royal Scottish Academy of Music and Drama</td>

<td align="center">3b</td>

<td>10</td>

<td>P</td>

</tr>

<tr>

<td>University of Nottingham</td>

<td align="center">5*</td>

<td>92</td>

<td>W</td>

<td>Anglia Polytechnic University</td>

<td align="center">3b</td>

<td>9</td>

<td>M</td>

</tr>

<tr>

<td>City University</td>

<td align="center">5*</td>

<td>90</td>

<td>W</td>

<td>University of East Anglia</td>

<td align="center">4</td>

<td>9</td>

<td>W</td>

</tr>

<tr>

<td>University of York</td>

<td align="center">5</td>

<td>82</td>

<td>M</td>

<td>University of Reading</td>

<td align="center">3a</td>

<td>9</td>

<td>W</td>

</tr>

<tr>

<td>Goldsmiths College</td>

<td align="center">5</td>

<td>80</td>

<td>W</td>

<td>Oxford Brookes University</td>

<td align="center">3a</td>

<td>7</td>

<td>M</td>

</tr>

<tr>

<td>University of Edinburgh</td>

<td align="center">4</td>

<td>80</td>

<td>W</td>

<td>University of Surrey Roehampton</td>

<td align="center">3a</td>

<td>7</td>

<td>W</td>

</tr>

<tr>

<td>University of Durham</td>

<td align="center">4</td>

<td>72</td>

<td>W</td>

<td>Kingston University</td>

<td align="center">3b</td>

<td>6</td>

<td>M</td>

</tr>

<tr>

<td>University of Bristol</td>

<td align="center">5</td>

<td>68</td>

<td>W</td>

<td>Napier University</td>

<td align="center">2</td>

<td>6</td>

<td>P</td>

</tr>

<tr>

<td>University of Liverpool</td>

<td align="center">4</td>

<td>65</td>

<td>W</td>

<td>Canterbury Christ Church Univ. College</td>

<td align="center">3b</td>

<td>5</td>

<td>P</td>

</tr>

<tr>

<td>School of Oriental and African Studies</td>

<td align="center">5</td>

<td>57</td>

<td>W</td>

<td>University College Northampton</td>

<td align="center">3b</td>

<td>5</td>

<td>C</td>

</tr>

<tr>

<td>University of Wales, Bangor</td>

<td align="center">5</td>

<td>56</td>

<td>W</td>

<td>Thames Valley University</td>

<td align="center">2</td>

<td>5</td>

<td>M</td>

</tr>

<tr>

<td>University of Hull</td>

<td align="center">5</td>

<td>54</td>

<td>W</td>

<td>St Martin's College</td>

<td align="center">2</td>

<td>4</td>

<td>M</td>

</tr>

<tr>

<td>Keele University</td>

<td align="center">4</td>

<td>51</td>

<td>M</td>

<td>University of Ulster</td>

<td align="center">3a</td>

<td>4</td>

<td>M</td>

</tr>

<tr>

<td>Lancaster University</td>

<td align="center">4</td>

<td>51</td>

<td>W</td>

<td>Bath Spa University College</td>

<td align="center">3a</td>

<td>2</td>

<td>M</td>

</tr>

<tr>

<td>Queen's University, Belfast</td>

<td align="center">5</td>

<td>44</td>

<td>W</td>

<td>Liverpool John Moores Univ.</td>

<td align="center">2</td>

<td>2</td>

<td>C</td>

</tr>

<tr>

<td>University of Birmingham</td>

<td align="center">5*</td>

<td>43</td>

<td>M</td>

<td>Bretton Hall</td>

<td align="center">3a</td>

<td>0</td>

<td>M</td>

</tr>

<tr>

<td>Royal College of Music</td>

<td align="center">4</td>

<td>40</td>

<td>P</td>

<td>Liverpool Hope</td>

<td align="center">3b</td>

<td>0</td>

<td>C</td>

</tr>

<tr>

<td>Univ. of Central England in Birmingham</td>

<td align="center">3a</td>

<td>31</td>

<td>P</td>

<td>London Guildhall University</td>

<td align="center">3b</td>

<td>0</td>

<td>M</td>

</tr>

<tr>

<td>University of Newcastle</td>

<td align="center">5*</td>

<td>29</td>

<td>W</td>

<td>University College Chichester</td>

<td align="center">1</td>

<td>0</td>

<td>C</td>

</tr>

<tr>

<td>University of Surrey</td>

<td align="center">3a</td>

<td>27</td>

<td>M</td>

<td colspan="4">**Note**: _DPr = departmental predominance  
W = writing, M = mixed, P = performance, C = composition_</td>

</tr>

</tbody>

</table>

Again, this is perhaps as much a reflection of the data source as the ability of citations to provide a measure of quality, but equally it may be that departments with a higher quality of research (as assessed by the Exercise) are more likely to produce written material than any other submission type.

However, being a writing department does not guarantee a high score (e.g., University of Newcastle), nor does being a performance department guarantee a low score (e.g., Royal Academy of Music) and, to repeat, such predominance designations are approximate.

### Arts and Humanities Citation Index-indexed journals

When this study was carried out, sixty-three journals (listed as being in the subject of music) were indexed by Arts and Humanities Citation Index ([Thomson [n.d.]](#thom)) (As of 1st June 2008, there were seventy.) The 445 submissions for the 2001 Exercise, which were classed as journal articles appeared in a total of 181 different journals. Of these, 194 were submitted to a total of 34 of the 63 music journals indexed by the Arts and Humanities Citation Index. The top ten journals by numbers of articles submitted for the 2001 Exercise are shown in Table 12 below.

<table width="50%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 12: Top ten journals by number of submitted articles**</caption>

<tbody>

<tr>

<th>Journal title</th>

<th>No. of submissions</th>

</tr>

<tr>

<td>Music Analysis*</td>

<td align="center">28</td>

</tr>

<tr>

<td>Music and Letters*</td>

<td align="center">25</td>

</tr>

<tr>

<td>Journal of the Royal Musicological Association*</td>

<td align="center">24</td>

</tr>

<tr>

<td>Early Music*</td>

<td align="center">22</td>

</tr>

<tr>

<td>Cambridge Opera Journal</td>

<td align="center">14</td>

</tr>

<tr>

<td>Musical Times*</td>

<td align="center">13</td>

</tr>

<tr>

<td>Organised Sound</td>

<td align="center">10</td>

</tr>

<tr>

<td>British Journal of Ethnomusicology</td>

<td align="center">9</td>

</tr>

<tr>

<td>British Journal of Music education</td>

<td align="center">9</td>

</tr>

<tr>

<td>Contemporary Music Review</td>

<td align="center">9</td>

</tr>

<tr>

<td>Total</td>

<td align="center">163</td>

</tr>

<tr>

<td colspan="2">* Journals indexed by Arts and Humanities Citation Index</td>

</tr>

</tbody>

</table>

A list of total numbers of articles submitted to music journals indexed by the Arts and Humanities Citation Index by each department was made to calculate a mean across each Research Assessment Exercise score group. A perfect correlation between Research Assessment Exercise scores and the number of articles submitted to Arts and Humanities Citation Index-indexed journals can be seen at Research Assessment Exercise score level (see Table 13 below). This is another example of summation hiding low-level variation of counts and thus showing a stronger correlation.

<table width="50%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 13: Mean for RAE score groups of the number of articles per department submitted to music journals indexed by the Arts and Humanities Citation Index**</caption>

<tbody>

<tr>

<th>RAE score</th>

<th>Mean no.  
of articles</th>

</tr>

<tr>

<td align="center">5*</td>

<td align="center">7.11</td>

</tr>

<tr>

<td align="center">5</td>

<td align="center">3.92</td>

</tr>

<tr>

<td align="center">4</td>

<td align="center">2.94</td>

</tr>

<tr>

<td align="center">3a</td>

<td align="center">2.11</td>

</tr>

<tr>

<td align="center">3b</td>

<td align="center">0.38</td>

</tr>

<tr>

<td align="center">2</td>

<td align="center">0.25</td>

</tr>

<tr>

<td align="center">1</td>

<td align="center">0.00</td>

</tr>

</tbody>

</table>

It is tempting to see numbers of articles submitted to music journals indexed by the Arts and Humanities Citation as a useful, synchronically-available measure that would be as robust as citation counting. However, at r<sub>s</sub>=0.60 the correlation is weaker and, again, the hidden detail shows enough irregularities as to be unhelpful.

A number of submissions by authors identified as music psychologists were published in journals indexed in the Social Science Citation Index as opposed to Arts and Humanities Citation Index. These include _British Journal of Psychology_, _British Journal of Developmental Psychology_ and _Behavioral and Brain Sciences_. This suggests that music psychologists belong to a different area of publication and have a corresponding difference in citing behaviour (these non-music journals have not been counted in the correlation test for journals indexed by the Arts and Humanities Citation Index, which used figures for music journals only).

A number of other authors had submissions that were published in journals indexed by the Arts and Humanities Citation Index that are listed in categories other than music. These journals include _Critical Quarterly_, _Cultural Studies_, _Historical Journal_, _New German Critique_ and _Comparative Literature_. This shows that authors within music do not constrain themselves to purely music-related journals, with over 30% of all articles being submitted to journals that are not music-specific. This behaviour may possibly be seen among authors in other Units of Assessment, but the wide range of subject areas covered by music departments could suggest that it is perhaps more common within music.

There is currently no available research that explores the specific citing behaviour of music academics. If, as seems likely, certain areas of research have a greater expectation of citing and being cited (e.g. music psychology), this factor should be taken into account in a bibliometric assessment process. If such an assessment process were to involve citation counting as a constituent element, the analysis or calculating algorithm should be weighted in some way according to the citing practice of the subject area being assessed.

Crucial to the validity of citation analysis is the selection of journals to be included in the citation index from which data are taken. Three academics interviewed to inform this discussion all raised the following points:

*   The list of journals indexed by the Arts and Humanities Citation Index, which provided the data for the citation count in this study causes some concern, the unanimous opinion being that the list was flawed, in that, with only sixty-three journals, it was simply too small to reflect the breadth of the music subject area and it includes journals that are not considered to be of a sufficiently high quality
*   It disadvantages those outside mainstream areas of interest whose main journals are less frequently cited
*   The nature of music as a subject is to be in continual flux and that any fixed list of journals could only inhibit the free movement of the subject's boundaries.

An example of how a particular publication's inclusion in or exclusion from the list can influence the counting process can be seen in the citation pattern of those showing a 'performance' predominance. Here, fifty-four authors are cited between one and twelve times. Of these, there are twenty performers who play bowed string instruments, altogether receiving 77 citations, 57% of total citations for performers (136). Also, there are seventeen pianists, nine of whom perform with string players (as indicated by their submissions). These nine account for a further 22 citations (16%).

The majority of citations for string players and collaborating pianists came from _The Strad_, a magazine for professional bowed-string players, teachers and the general public, but which cannot be classed as a research publication in the same way as, for example, _Music Analysis_, the journal with the most submissions. In this instance, the inclusion of _The Strad_ noticeably increases the representation of one of the constituent groups of authors and it is an example of how non-academic or non-research journals are necessary to ensure the whole subject is covered. Whilst scholarly journals in music carry book reviews and reviews of recordings and music editions (a prime example being _Early Music_), _The Strad_'s exclusion from the Arts and Humanities Citation Index would lower the citation count of performers considerably.

A potentially rich data-source can be found in the [Répertoire International de Littérature Musicale](http://www.rilm.org/) (RILM) _Abstracts of Music Literature_ database. It contains a large amount of bibliographical information that could serve as a source list for a music citation index. At present, it does not list or index references so a citation count cannot be implemented. However, it provides a far more detailed picture of current music research than the Arts and Humanities Citation Index as it is not restricted to journal articles. It includes a large number of books (shown here to be cited twice as often as journals) in its current 400,000 entries (written in 140 languages), with around 30,000 further entries being added every year.

### Citation analysis and research assessment

The practical use of citation analysis for research assessment in music would be problematic. Any system would have to be thought out very carefully in order to gain enough widespread support among the assessed to give it credibility.

Correlating Research Assessment Exercise scores with T<sub>D</sub>, T<sub>D</sub>/N, T<sub>D</sub>/A<sub>D</sub> and (T<sub>D</sub> x A<sub>D</sub>)/N, the first four correlation results all show a similar value for rs but in each the underlying data is â€˜speltâ€™ slightly differently. The fourth calculation, (TD x AD)/N, provided a crudely weighted total that may or may not be akin to the FTE weighting used by the Research Assessment Exercise system. The question would be whether a â€˜spellingâ€™ could be found that was agreeable to most or all stakeholders.

Looking at the scatterplot in Figure 2 above, one can see large amount of overlapping in the distribution of departments in the y-direction. This indicates the potential for problems if these counts were used for the assignment of Research Assessment Exercise scores. As an example, below are two different possible methods of score assignment, showing clearly that there is a large margin of error when results predicted by these models are compared to the actual the 2001 Exercise scores.

In method one, the spread of scores from the results of Research Assessment Exercise1996 was mapped onto a sample ranking by citation count (in this case by TD) to give a prediction for the 2001 Exercise scores. Only 37% of departments received the correct score with 22% predicted higher scores and 41% predicted lower scores than were actually received.

However, as Holmes and Oppenheim ([2001](#hoop01)) point out, â€œit must be stressed that there is no particular reason why the 2001 distribution of scores should be similar to that for the 1996 Research Assessment Exercise ([Holmes & Oppenheim 2001](#hoop01)). This being the case, the second prediction method uses system of attainment targets rather than fixed score quotas.

In method two, using the mean number of citations per department per Research Assessment Exercise score group (as seen in Table 6), the midpoint between each mean was found (the mean of two adjacent means), providing citation-range limits by which to separate the departments into specific Research Assessment Exercise score groups: x=a+b/2, e.g., (mean of 5* plus mean of 5) /2 = (148 + 88.08) / 2 = 118.04.

These calculations give the ranges shown in Table 15 below:

<table width="40%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 14: Citation ranges for predictions by mean**</caption>

<tbody>

<tr>

<th>Research Assessment Exercise score</th>

<th>Citation range</th>

</tr>

<tr>

<td align="center">5*</td>

<td align="center">118+</td>

</tr>

<tr>

<td align="center">5</td>

<td align="center">72-117</td>

</tr>

<tr>

<td align="center">4</td>

<td align="center">34-71</td>

</tr>

<tr>

<td align="center">3a</td>

<td align="center">10-33</td>

</tr>

<tr>

<td align="center">3b</td>

<td align="center">6-9</td>

</tr>

<tr>

<td align="center">2</td>

<td align="center">2-5</td>

</tr>

<tr>

<td align="center">1</td>

<td align="center">0-1</td>

</tr>

</tbody>

</table>

The predicted scores were accurate in 36% of cases, with 17% of departments having higher predicted scores and 47% having lower lower scores than those actually received in the 2001 Exercise.

Both systems require fixed markers, either in the form of group sizes or the number of citation counts needed to attain a certain score. Of these, the second is nearer to the the 2001 Exercise system as it allows all departments to gain the highest score simultaneously, should they each have the number of citations needed. However, the setting of the count-ranges could be seen to be an arbitrary process, as in this example. Potentially, this could lead to a change in research behaviour as departments chase citations, given that 'a performance indicator immediately becomes a performance objective especially if money is involved' ([Lewis 2000](#lew00): 372).

Another option would be the use of citation analysis as a part of peer assessment, giving a guide for each panel as to the relative standing of each department in terms of citation counts. Norris and Oppenheim recommend that citation counts 'should be adopted as the primary procedure for the initial ranking of university departments' ([Norris & Oppenheim 2003](#noop03): 728). However, with such divergent results as seen clearly in the scatterplot (Figure 2 above), it is likely that if this was used in practice with the current data set (in the subject of music at least), it would serve only to add a further layer to the Assessment process, adding complications and expense.

However, despite shortcomings such as those detailed above, the correlations shown are strong enough to suggest that, with a more comprehensive data set, citation counts could yet prove to show an even stronger correlation with Research Assessment Exercise scores and, therefore, be more useful in research assessment.

## Conclusions and recommendations

### Conclusions

Contrary to the null hypothesis posited at the outset, a strong correlation was in fact found between citation count totals at departmental level and the Assessment scores awarded to the departments. This result demonstrates once again that citation counts could be a surrogate for peer review, even in the most unlikely of subject areas. The correlation test results themselves compare favourably with previous citation-based research carried out in different subject areas by Thomas and Watkins ([1998](#thwa98)), Smith and Eysenck ([2002](#smey02)) and Norris and Oppenheim ([2003](#noop03)), amongst others.

However, the correlation test results hide specific details in the data, especially where the range of departmental citation count totals in any given Research Assessment Exercise score group shows a large overlap with other score groups. Therefore, these counts must be used with caution in the assessment of research quality and could not be used alone to assign quality-related scores in subjects such as music. Indeed, this method does not show itself to give a rational assessment of research quality as compared to the peer review process of the Research Assessment Exercise.

Despite the strong correlations we obtained, it is also clear that the coverage of the Arts and Humanities Citation Index is unrepresentative of music research. A significant proportion of music research output has little or no representation in the Index, which, therefore, gives a partial view of music research. This is partly because of the choice of journals indexed and the fact that all other materials, most notably books, are excluded as sources. Therefore, even if citation counts were adopted as part of a system for evaluating the quality of music research, the Index should not be used as the sole data source.

The method and data source used in this study under-represent a very large proportion of the studied population in terms of citations. Moreover, many authors cannot participate in the citing process because of the nature of their (often practical) work. Thus, it is highly likely that, if used as presented in this study, citation analysis would not gain widespread acceptance amongst the assessed as a tool for research assessment. High correlations alone are not enough. Academics need to have confidence in the evaluation process.

However, if an alternative data source could be found that is more representative of the subject area, there is potential for the use of citation analysis in research assessment in music.

### Recommendations for further research

It is suggested to those involved in the administration of the Research Assessment Exercise that a similar study should be carried out to assess the correlation between citation counts and the scores for the 2008 Exercise. This would provide further evidence as to whether citation analysis could become a useful part of the metrics proposed for the humanities in general (and music specifically).

It is recommended that future research should include investigation of:

*   the correlation between peer regard and citation counts for individual authors in the humanities;
*   the strength of correlations with Research Assessment Exercise scores at individual author citation count level in other subject areas, especially those already studied;
*   the practical use of citation data in the assignment of scores for the assessment of research quality in a consistent manner;
*   publication and citing practices of music academics; and
*   the [Répertoire Internationale de Littérature Musicale](http://www.rilm.org/index.php) (RILM) Abstracts of Music Literature database, which has excellent coverage of academic music literature, as a source for citation data for the assessment of music research.

### Acknowledgements

We wish to thank Lizzie Gadd for her help in drafting this paper, the three music academics who informed the discussion and the two anonymous referees for their insightful comments. One of us (MS) also wishes to thank the Arts and Humanities Research Council for a scholarship.

### Notes

<a name="note1" id="note1"></a>1\. Part V is ([Norris & Oppenheim 2003](#noop03)).

<form action="">

<fieldset><legend style="border-right: 1px solid navy; border-bottom: 1px solid navy; padding: 0.1ex 0.5ex; color: white; background-color: #5E96FD; font-size: medium; font-weight: bold;">References</legend>

*   <a name="agu02" id="agu02"></a>[_A guide to the 2001 Research Assessment Exercise._](http://www.Webcitation.org/5YKIf6mLd) (2002). Retrieved 1st August, 2006 from www.hero.ac.uk/Research Assessment Exercise/Pubs/other/Research Assessment Exerciseguide.pdf (Archived by WebCite® at http://www.webcitation.org/5YKIf6mLd)
*   <a name="baop94" id="baop04"></a>Baird, L.M. & Oppenheim, C. (1994). Do citations matter? _Journal of Information Science_, **20**(1), 2-15.
*   <a name="ban03" id="ban03"></a>Banfield, S. (2003, Spring). [RAE letters. RAE Review: Invitation to contribute](http://www.Webcitation.org/5YKIiJYFu) _NAMHE newsletter_, (Spring), 1-3\. Retrieved 25th July, 2006 from http://www.namhe.ac.uk/publications/newsletters/Spring%202003.pdf. (Archived by WebCite® at http://www.webcitation.org/5YKIiJYFu)
*   <a name="ben05" id="ben05"></a>Bence, V. (2005). _Research assessment and scholarly communication: a demographic analysis of journal submissions to ten Units of Assessment in the 2001 Research Assessment Exercise._ Unpublished PhD thesis, Loughborough University, Loughborough, UK.
*   <a name="coco73" id="coco73"></a>Cole, J.R. & Cole S. (1973). _Social stratification in science._ Chicago: University of Chicago Press.
*   <a name="coev99" id="coev99"></a>Cook, N. & Everist, M. (Eds.). (1999). _Rethinking music._ Oxford: Oxford University Press.
*   <a name="crsnat97" id="crsnat97"></a>Cronin, B., Snyder, H., & Atkins, H. (1997). Comparative citation rankings of authors in monographic and journal literature: a study of sociology. _Journal of Documentation,_ **53**(3), 263-273.
*   <a name="cul98" id="cul98"></a>Cullars, J.M. (1998). Citation characteristics of English-language monographs in philosophy. _Library and Information Science Research,_ **20**(1), 41-68.
*   <a name="dio94" id="dio94"></a>Diodato, V. (1994). _Dictionary of bibliometrics._ New York, NY: Haworth.
*   <a name="eas07" id="eas07"></a>Eastwood, D. (2007). [_Future framework for research assessment and funding._](http://www.Webcitation.org/5YKMkHIm1) Retrieved 30th September, 2007 from http://www.hefce.ac.uk/pubs/circlets/2007/cl06_07/ (Archived by WebCite® at http://www.webcitation.org/5YKMkHIm1)
*   <a name="gar79" id="gar79"></a>Garfield, E. (1979). _Citation indexing: its theory, and application in science, technology, and humanities_. New York, NY: Wiley.
*   <a name="hem96" id="hem96"></a>Hemlin, S. (1996). Research on research evaluation. _Social Epistemology,_ **10**(2), 209-250.
*   <a name="her02" id="her02"></a>HERO. (2002). [_Research Assessment Exercise 2001._](http://www.Webcitation.org/5YLX1wY0L) Retrieved 3rd May, 2006 from http://www.hero.ac.uk/Research Assessment Exercise/. (Archived by WebCite® at http://www.webcitation.org/5YLX1wY0L)
*   <a name="hoop01" id="hoop01"></a>Holmes, A. and Oppenheim, C. (2001). [Use of citation analysis to predict the outcome of the 2001 Research Assessment Exercise for unit of assessment (UoA) 61: library and information management.](http://www.Webcitation.org/5YKMqVVHH) _Information Research,_ **6**(2). Retrieved 21st February, 2006 from http://informationr.net/ir/6-2/paper103.html.(Archived by WebCite® at http://www.webcitation.org/5YKMqVVHH)
*   <a name="hmt06" id="hmt06"></a>HM Treasury. (2006). [_Budget 2006._](http://www.Webcitation.org/5YKN0cIFd) Retrieved 31st March, 2006 from http://www.hm-treasury.gov.uk/media/26E/0F/bud06_completereport_2320.pdf. (Archived by WebCite® at http://www.webcitation.org/5YKN0cIFd)
*   <a name="jac07" id="jac07"></a>Jacso, P. (2007). Deflated, inflated and phantom citation counts. _Online information review,_ **30**(3), 297-309.
*   <a name="joh06" id="joh06"></a>Johnson, P. (2006, October 21). Re: Report of AHRC HEFCE Expert Group on post 2008 RAE Metrics. Message posted to the National Association for Music in Higher Education Network mailing list, archived at http://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=ind06&L=namhe&T=0&O=D&P=11740
*   <a name="lan01" id="lan01"></a>Lange, L. (2001). Citation counts of multi-authored papers â€“ first-named authors and further authors. _Scientometrics,_ **52**(3), 457-470.
*   <a name="lew00" id="lew00"></a>Lewis, J. (2000). Funding social science research in academia. _Social Policy Administration,_ **34**(4), 365-376.
*   <a name="mcmc89" id="mcmc89"></a>MacRoberts, M.H., & MacRoberts, B.R. (1989). Problems of citation analysis: a critical review. _Journal of the American Society for Information Science,_ **40**(5), 342-349.
*   <a name="mcmc96" id="mcmc96"></a>MacRoberts, M.H., & MacRoberts, B.R. (1996). Problems of citation analysis. _Scientometrics,_ **36**(3), 435-444.
*   <a name="hef06" id="hef06"></a>HEFCE (2006). [_Mainstream QR rates of funding by unit of assessment and rating._](http://www.Webcitation.org/5YKNJDJpH) Retrieved 10 March, 2006 from http://www.hefce.ac.uk/research/funding/qrfunding/2006/rates0607.xls (Archived by WebCite® at http://www.webcitation.org/5YKNJDJpH)
*   <a name="mea98" id="mea98"></a>Meadows, A.J. (1998). _Communicating research._ San Diego, CA: Academic Press.
*   <a name="moe05" id="moe05"></a>Moed, H.F. (2005). _Citation analysis in research evaluation._ Dordrecht, The Netherlands: Springer.
*   <a name="ned06" id="ned06"></a>Nederhof, A.J. (2006). Bibliometric monitoring of research performance in the social sciences and the humanities: a review. _Scientometrics,_ **66**(1), 81-100.
*   <a name="noop03" id="noop03"></a>Norris, M. & Oppenheim, C. (2003). Citation counts and the Research Assessment Exercise V: archaeology and the 2001 Research Assessment Exercise. _Journal of Documentation,_ **59**(6), 709-730.
*   <a name="opp95" id="opp95"></a>Oppenheim, C. (1995). The correlation between citation counts and the 1992 Research Assessment Exercise ratings for British library and information science university departments. _Journal of Documentation,_ **51**(1), 18-27.
*   <a name="opp96" id="opp96"></a>Oppenheim, C. (1996). Do citations count?: citation indexing and the Research Assessment Exercise (Research Assessment Exercise). _Serials,_ **9**(2), 155-161.
*   <a name="opp97" id="opp97"></a>Oppenheim, C. (1997). The correlation between citation counts and the 1992 Research Assessment Exercise ratings for British research in genetics, anatomy and archaeology. _Journal of Documentation,_ **53**(5), 477-487.
*   <a name="opp00" id="opp00"></a>Oppenheim, C. (2000). Comment. _Journal of Information Science,_ **26**(6), 459-460.
*   <a name="pey05" id="pey05"></a>Peyraube, A. (2005). [_Project for building a European citation index in the domain of the humanities._](http://www.Webcitation.org/5YLwKUEA8) Retrieved 16 August, 2006 from www.esf.org/generic/2466/historyrationaleofERIH.pdf (Archived by WebCite® at http://www.webcitation.org/5YLwKUEA8)
*   <a name="RAE99" id="RAE99"></a>Research Assessment Exercise. (1999). [_Research Assessment Exercise 2001: assessment panels' criteria and working methods._](http://www.Webcitation.org/) Retrieved 21 February, 2006 from http://www.hero.ac.uk/Research Assessment Exercise/Pubs/5_99/Research Assessment Exercise5_99.doc (Archived by WebCite® at http://www.webcitation.org/5YKO5I0CQ)
*   <a name="RAE06" id="RAE06"></a>Research Assessment Exercise. (2006). [_Research Assessment Exercise 2008 Panel criteria and working methods: panel O._](http://www.Webcitation.org/) Retrieved 26 January, 2006 from http://www.Research Assessment Exercise.ac.uk/pubs/2006/01/docs/oall.pdf (Archived by WebCite® at http://www.webcitation.org/5YKO7G21e)
*   <a name="rob04" id="rob04"></a>Roberts, G. (2004). [_Review of research assessment._](http://www.Webcitation.org/5YKOAIkvz) Retrieved 28 May, 2006 from http://www.ra-review.ac.uk/reports/roberts.asp (Archived by WebCite® at http://www.webcitation.org/5YKOAIkvz)
*   <a name="sar00" id="sar00 "></a>Sarwar, S. (2000). _A publication and citation analysis of civil engineering departments in the UK which participated in the 1996 Research Assessment Exercise (Research Assessment Exercise)._ Unpublished MSc dissertation, Department of Information Studies, University of Sheffield, Sheffield, U.K.
*   <a name="sewi95" id="sewi95"></a>Seng, L.B., & Willett, P. (1995). The citedness of publications by United Kingdom library schools. _Journal of Information Science,_ **21**(1), 68-71.
*   <a name="smey02" id="smey02"></a>Smith, A., & Eysenck, M. (2002). [_The correlation between Research Assessment Exercise ratings and citation counts in psychology._](http://cogprints.org/2749/01/citations.pdf) Retrieved 12 June, 2006 from http://cogprints.org/2749/01/citations.pdf
*   <a name="snbo98" id="snbo98"></a>Snyder, H. & Bonzi, S. (1998). Patterns of self-citation across disciplines (1980-1989). _Journal of Information Science,_ **24**(6), 431-435.
*   <a name="so98" id="so98"></a>So, C.Y.K. (1998). Citation ranking versus expert judgment in evaluating communication scholars: effects of research specialty size and individual prominence. _Scientometrics,_ **41**(3), 325-333.
*   <a name="sum07" id="sum07"></a>Summers, M.A.C. (2007). [_Analysis of research within unit of assessment 67 (music) of the UK Research Assessment Exercise._](http://hdl.handle.net/2134/2722) Loughborough University. Retrieved 30 August, 2007 from http://dspace.lboro.ac.uk/dspace/handle/2134/2722
*   <a name="thwa98" id="thwa98"></a>Thomas, P.R. & Watkins, D.S. (1998). Institutional research rankings via bibliometric analysis and direct peer review: a comparative case study with policy implication. _Scientometrics,_ **41**(3), 335-355.
*   <a name="thom" id="thom"></a>Thomson. (n.d.). [_Arts & Humanities Citation Index – music journal list._](http://www.Webcitation.org/5YKOY6zv9) Retrieved 17 February, 2007 from http://sunWeb.isinet.com/cgi-bin/jrnlst/jlresults.cgi?PC=H&SC=RP (Archived by WebCite® at http://www.webcitation.org/5YKOY6zv9)
*   <a name="raa96" id="raa96"></a>van Raan, A.F.J. (1996). Advanced bibliometric methods as quantitative core of peer-review-based evaluation and foresight exercises. _Scientometrics,_ **36**(3), 397-420.
*   <a name="raa05" id="raa05"></a>van Raan, A.F.J. (2005). Fatal attraction: conceptual and methodological problems in the ranking of universities by bibliometric methods. _Scientometrics,_ **62**(1), 133-143.
*   <a name="war00" id="war00"></a>Warner, J. (2000). A critical review of the application of citation studies to the Research Assessment Exercises. _Journal of Information Science,_ **26**(6), 453-459.

