#### vol. 13 no. 2, June 2008



# The current state of decision support in Lithuanian business



#### [Rimvydas Skyrius](mailto:rimvydas.skyrius@ef.vu.lt)  
Department of Economic Informatics, Faculty of Economics, Vilnius University, Vilnius, Lithuania 

#### Abstract

> **Introduction**. A study of information technology use for decision information needs in the community of Lithuanian business users has been performed to learn about actual ways of using the technology and user attitudes towards its efficiency.  
> **Method**. A survey has been used to elicit responses from business decision makers and provide insights into the state of support for decision information needs.  
> **Analysis**. The survey yielded 250 responses on issues of general information needs, environment monitoring, decision making circumstances and information use, and preservation and re-use of decision experience.  
> **Results**. The respondents confirmed the use of information technology as a problem-solving management tool, positioning its use closer to its known basic strengths. The more sophisticated part of problem-solving functions (detection of important changes, sense-making, creativity) are left to human actors, thus ensuring efficiency and flexibility.  
> **Conclusions**. The suggested approach for the providers of information services for decision making would be more with less: stressing the proximity of simple support tools and principal information sources to the decision makers and ensuring the convenient use of more sophisticated functionality whenever required.


## Introduction

The call for computer-based decision support is and has always been one of the principal demands of the business user community to the providers of information services. The prominence of once-promising decision support systems that came into existence because of this demand, seems to have diminished over the last decade or so. However, the need for business to make decisions did not disappear, and the importance of decision making support has not diminished. The increasing complexity of the business environment (global markets and players, importance of fast reactions and operations, e-activities with their opportunities and threats) and, accordingly, growing volumes of available information, put extra strain on decision makers. A business problem or situation generates related information needs, and to cope with the situation a decision maker has to undergo some or all phases of information behaviour (seeking, searching, using) as defined by Wilson ([2000](#wil00)), and further by Niedzwiedzka ([2003](#nie03)) and Maceviciute ([2006](#mac06)). Cohen's framework for information delivery expands the decision making environment by including context together with decision maker and information processing ([Cohen 2000](#coh00)). Culkin_et al._ ([1999](#cul99)) point to the panorama principle in decision making: all data is qualitative and need to be interpreted in context. Increasing complexity of business environment information invokes changes in the structure of business information needs, driven by increasing role of unstructured content ([Cohen 2007](#coh07)). This growing diversity of information creates approaches called information fusion ([Ferguson, _et al._ 2005](#fer05)) where multiple information sources are combined to achieve clarity through an integrated context. Other authors (for example, [McKenzie 2005](#mck05)) point to the importance of informal sources, such as personal network contacts; this is especially true for small and medium enterprises ([Hill and Scott 2004](#hil04)). Though information technology does not cover all the functionality required by the decision making process, it plays an important role and is expected to provide considerable support for problem understanding and solving.

The activities of monitoring, analysis and providing business intelligence are performed all the time by people whose job is to notice or detect important things and phenomena, and provide insight and make sense of these, for example, the analysts and business area managers. The process of making important decisions requires the appropriate tools to support high-level information needs such as awareness, communication, sense-making, and evaluation of alternatives. There is an abundance of technology-based tools and techniques which, even if not directly called decision support tools, aim to support roughly the same set of information needs: reliable and convincing arguments for decision making ([Sheff 2006](#she06)), monitoring and awareness of the environment ([Zhong _et al._ 2007](#zho07)), and gaining insight into the business's own activities and those of the competition and other related players, to name a few.

Management decision making, being one of the principal sources of complex information needs, has driven the emergence of many sophisticated technologies aimed at meeting these needs, and it continues to do so. The technologies meant for serving these purposes are sometimes called by the collective name of intelligent technologies, and usually include data warehousing, data mining, online analytical processing, business intelligence as well as others. Some of these sophisticated analytical technologies are aimed at the monitoring of the business environment; others support the search for possible problems or opportunities; yet other technologies provide analytical and modelling tools. All of them at times seem to balance between support of the mental activities of users on the one hand, and substitution for these activities by automating analytical functions and programming complex information processing patterns on the other. The estimation of the real value of this (often expensive) support and process automation to the problem-solving user community is a question that seems to arise with every new wave of technological sophistication. The confusion of the user community over the actual value of this support is further fuelled by the cases where different companies using the same or similar technologies achieve quite different results ([Malhotra 2005](#mal05)).

The possible gap between decision information needs and information technology support of these needs was the impetus for this research in the community of business users in Lithuania. It aimed to investigate such issues as: What are the actual attitudes of the user community towards information technology use for decision support, and how much do they use it? How do the users see the role of technology in satisfying the diverse range of their information needs? What approaches to efficient use of technology might have the potential to raise the level of user benefit and satisfaction?

### Statement of objectives and method

The objective of this research is to explore the current state of the actual use of information technology to satisfy information needs for decision making and related activities, using a questionnaire-based survey to elicit responses and opinions of the users (the business decision makers), and to provide some initial insights into the state of support of decision information needs.

The method used is a survey of business decision makers, which, although it contains several closed questions, aims mostly at eliciting opinions and attitudes by open-ended questions. It implies a user-centred approach throughout the survey.

The paper is organized into four sections. First, it describes the structure of information needs to more accurately position the direction of the survey. Then, it discusses the role of information technology in supporting decision making and other complex information needs. The third section presents and comments on the results of the survey. Finally, it discusses the findings and conclusions, and possible directions of further research are defined.

## Structure of information needs

For the purposes of this research, simple everyday decisions will be excluded from further consideration, and the paper will concentrate on the semi-structured and unstructured decisions of medium and high complexity. Complexity, as it is understood here, can be estimated by evaluating features such as:

*   the number of procedures and stages required to produce the result,
*   the number of information sources to be used, and
*   the number of dimensions to be considered (for example, business, technical, social, political and environmental.)

The information needs for decision making are complicated to satisfy for reasons that are more or less obvious and have been named in a number of works from early stages of decision support system research ([Keen and Morton 1978](#kee78); [Sprague and Watson 1982](#spr82)) through to recent work in the field ([Ashill and Jobber 2001](#ash01); [Melchert and Winter 2004](#mel04) and [Nakatsu 2004](#nak04)). Among the most often mentioned reasons are: multi-faceted information from assorted sources was required; limited time-frame (usually); the need to decide with incomplete information; and the need to adapt to environment changes. For these reasons we tend to put decision support information needs at the complex end of the whole spectrum of management information needs, its opposite being simple needs. The principal differences between the two extremities can be defined as follows:

*   **Simple needs:** clearly structured questions as a prerequisite for routine actions or simple decisions; initial data from (mostly) a single source; procedures to produce results are few, tried, and tested; mostly controlled by one specific information system; results can be defined as a direct product or simple by-product of an existing information system.
*   **Complex needs:** vaguely structured questions; require highly composite results drawn from multiple and eclectic (and often external) data sources; heterogeneous data and procedures; increased role of soft information and judgment; not controlled by its own information system; often such needs cannot be exactly determined beforehand and are hard to plan.

Simple needs are usually attributed to routine situations and activities (though exceptions exist), where an information user is permanently monitoring the environment and searching for any issues worth paying attention to: possible problems, opportunities, and uncertainties. When an important issue comes up, it requires extra attention and creates additional information needs to have a better understanding of the situation; that is, it becomes complex.

We will introduce one more dimension for information needs regarding specific attention for a certain situation, gained experience and its re-use. The routine, repeating needs can be named _common needs_, and, respectively, non-routine, non-repeating (or little-repeating) needs that concentrate on a certain problem can be called _special needs_.

**Common needs:** usually serve the needs of performance management; are of a permanent nature (for example, monitoring) and are known beforehand; procedures to produce results are well-determined and have a high degree of re-use; roughly equal attention is given to all relevant areas of activity; some monitoring policy might be present.

Special needs: arise for a specific situation or problem; usually deeper and more concentrated analysis is required; are random and hard to plan; degree of procedural re-use is partial.

Several examples of information needs alongside simple/complex and common/special dimensions are presented in the Table 1 below:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Relation of simple-complex and common-special needs**  
</caption>

<tbody>

<tr>

<th align="center"> </th>

<th>Simple needs</th>

<th>Complex needs</th>

</tr>

<tr>

<td>Special needs (problem-specific)</td>

<td valign="top">Importers of article X  
Legal conditions for business in a new market</td>

<td valign="top">Our potential in a new market  
Possible actions by competition</td>

</tr>

<tr>

<td>Common needs (available permanently)</td>

<td valign="top">Cash-at-hand  
Today's completed orders  
Fully-serviced customer complaints  
Levels of inventory</td>

<td valign="top">Financial ratios  
Market share  
Benchmarking against competition</td>

</tr>

</tbody>

</table>

In order to have a clearer view of how these needs are related to decision making, we can use Simon's famous four-phase decision model to try to see whether there is a relationship between the two ([Simon 1960](#sim60)). As the decision making process uses a considerable variety of information, including available experience from previous problems and decisions, we can consider this process as having a cyclical nature, where the additional phases of experience accumulation and its subsequent use are equally important to decision making, alongside the traditional phases. The expanded structure of the decision making process, together with relevant information needs, might include the following stages:

1.  Monitoring: the environment, both internal and external, is being watched to notice things worth attention; _simple_ and _common_ information needs prevail.
2.  In the case of **recognizing** a situation of interest (problem, opportunity) the situation is evaluated and given extra attention to achieve desired understanding. At this stage _special_ information needs arise.
3.  Additional **analysis and decision development** is required if the situation is complex enough (semi-structured or unstructured); _simple_ needs are complemented by _complex_ needs; more information is brought into decision making environment; specific problem-solving tools such as formal approaches and models are likely to be used.
4.  The **decision-making** stage involves formerly available as well as newly-gained understanding of the situation, and the decision maker or makers will use all available knowledge to arrive at the best possible decision, time or other circumstances permitting. In this paper, the term _knowledge_ is deliberately avoided most of the time, but here it serves to show that data or information alone are insufficient for decision making; all that is known will be used in its entirety, and new knowledge most likely will be gained.
5.  The **experience accumulation** stage records the newly gained experience from both decision making and its implementation and keeps it for possible re-use. _Special needs_ become _common_, adding new material to the already available body of experience and the need to capture the essential features of the recorded case keeps this sort of information need in the complex segment. This phase should also include the practical experience in decision implementation, which can sometimes reveal additional circumstances of the problem.
6.  The **use of new experience**, along with that formerly accumulated, brings the process back to stage 1 - monitoring.

As we can see, during the decision making process the focus of information needs moves around the quadrants of Table 1: stage one concentrates in the simple/common sector; stage two moves on to simple/special sector, stages three and four concentrate in the special/complex sector, stage five moves into complex common sector, and finally stage six brings the focus back to the simple/common sector. The next issue then is to look how well these needs are supported by the support tools and technologies.

The potential of information technology to serve the range of information needs is distributed unevenly, especially along the simple/complex dimension. This point is supported by numerous other sources such as Raggad ([1997](#rag97)); McAffee ([2004](#mca04)), and Malhotra and Galletta ([2004](#mal04)). The programmed nature of information technology lends itself rather efficiently to the simple part of the information needs spectrum, as well as for the common part of the common-special continuum of information needs. The uncertain and discontinuous nature of complex needs complicates matters in such a way that programmed information technology-based solutions are at best only partially reusable (if they are flexible enough), or become single-case throwaway solutions. For the more complex part of the information needs spectrum, the importance of external information grows, as so does the variety and complexity of sources, thus adding complications to the task of providing consistent understanding of the problem at hand.

## Role of information technology

Since the early examples of applying information technology to support the more sophisticated information needs and opinions, results have not been uniform. The representatives of the more deterministic approaches assigned (and continue to assign) prime importance to information technology tools and possibilities, seeing the principal sources of value in highly automated information technology-based analytical procedures ([Davenport 2006](#dav06); [Harris 2002](#har02)). Some sources ([Lacity and Sauter 1999](#lac99); [Riege 2005](#rie05)) attempt to explain the inadequate use of information technology-based decision support to low motivation in the user community. They point to the need for increasing awareness of analytical tools and methods among the business decision makers, stating that the users of current analytical technologies either are not knowledgeable enough to embrace the technologies' full potential, or use it in the wrong way. The followers of behavioural principles identify human skills and competencies as a top priority when solving problems and making decisions, thus leaving the secondary roles for information technology ([Dhebar 1993](#dhe93); [Kling 1996](#kli96); [Sjoberg 2002](#sjo02)). A number of sources (including [Culkin _et al._ 1999](#cul99), [Sauter 1999](#sau99), [Johnstone _et al._ 2004](#joh04), and [Turpin and Du Plooy 2004](#tur04)) point to a set of issues dealing with the human features of business decision makers: perception, intuition, experiences and other factors influencing decision making, and state, in effect, that the majority of existing support tools are technology-centric rather than user-centric. This position has gained ground because the multiple, existing information technologies for high end of the needs spectrum often seem to be off-target, requiring substantial investment and learning, being complicated to use, and having inadequate flexibility to serve a variety of situations and limited re-use.

The role of information technology in the analytical activities of these users depends upon a number of factors, such as volumes of data to be analysed, level of its homogeneity, and re-use of programmed solutions, existing practices and experiences. Some earlier research carried out by the author ([Skyrius and Winer 2000](#sky00)) has shown that user responses towards general usefulness of information technology-based tools and techniques fall mostly into categories of "highly useful" and "useful in most cases", that is, if it works, it will be used. The other part of the same research has shown (although in rather general terms) that simple and problem-independent information technology-based decision tools and techniques (spreadsheets, search engines) prevail, providing processing muscle in standard procedures such as sorting, graphing, querying, arranging and matching. Fewer cases of efficient use have been noted when flexibility and intelligent tools and functions have been used.

The above considerations have initiated a need to investigate the attitudes of business users towards the information technology-based decision support. The research has been structured upon the six-stage decision making model, described in the previous section, joining the issues regarding each stage in three principal areas:

*   environment monitoring (stages one and two: monitoring and recognizing),
*   decision making (stages three and four: analysis and choice),
*   experience preservation (stages five and six: experience accumulation and re-use).

## Survey results

### Materials and methods

A questionnaire was designed to elicit the responses of the business decision makers in the Lithuanian business community. The questionnaire covered four related areas:

1.  general most important information needs,
2.  environmental monitoring activities,
3.  decision making and information use, and
4.  preservation and re-use of experience.

The questionnaire itself is presented as an appendix to this paper. It was distributed to a group of Master's level students in the School of International Business at the University of Vilnius, Lithuania. Having in mind that all students in the group are employed as middle-to-senior managers in businesses operating in Lithuania, the surveyed group can be regarded as a convenience sample in the community of business managers and/or decision makers. The survey yielded 250 responses. The respondents represent all principal areas of business activity and public sector, and their distribution is given below:

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: The distribution of respondents by field of activity**  
</caption>

<tbody>

<tr>

<td>Trade - wholesale and retail</td>

<td align="center">83</td>

</tr>

<tr>

<td>Services</td>

<td align="center">48</td>

</tr>

<tr>

<td>Production</td>

<td align="center">22</td>

</tr>

<tr>

<td>Communications, media, publishing</td>

<td align="center">20</td>

</tr>

<tr>

<td>Finance</td>

<td align="center">15</td>

</tr>

<tr>

<td>Transportation, logistics</td>

<td align="center">15</td>

</tr>

<tr>

<td>Information technology</td>

<td align="center">14</td>

</tr>

<tr>

<td>Public sector</td>

<td align="center">10</td>

</tr>

<tr>

<td>Construction</td>

<td align="center">6</td>

</tr>

<tr>

<td>Energy</td>

<td align="center">5</td>

</tr>

<tr>

<td>Health care</td>

<td align="center">2</td>

</tr>

<tr>

<td>Did not specify</td>

<td align="center">10</td>

</tr>

<tr>

<td align="right">Total</td>

<td align="center">250</td>

</tr>

</tbody>

</table>

The mean size of the surveyed organizations is 950 employees, with a median of fifty. The absolute majority of respondents fall into the size category of fewer than 200 employees, and there are 181 of them, or 72.4% of the total sample. The somewhat high mean size can be explained by the presence of several large infrastructure companies (telecommunications and energy) and representatives of large foreign companies.

Some open-ended questions (numbers 8, 10, 11, 12, 14, and 16) have yielded textual responses whose overall volume would be too big to present in a whole, so the variety of responses has been grouped into most typical responses. That is, alongside plain _yes_ or _no_ responses there are groups of most frequent responses of the type _it depends_. Some questions permitted multiple answers, so the totals of responses in the tables 3, 5, 8, and 12 sum to more than total number of respondents.

### General most important information needs

**_Question 1\. What are the general most important information needs that should be satisfied in the first place?_**

Under this static assessment, the most important information needs that would be required to meet in the first place have been rated in the responses using the rating scale where for each respondent the most important type of information has received 7 points, second most important 6 points, and so on, down to the least rated type receiving 1 point.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Rating of the most important information needs**  
</caption>

<tbody>

<tr>

<th align="center">Information need</th>

<th>Points</th>

</tr>

<tr>

<td>Current status information - cash flow, liquidity, inventory, payables/receivables etc.</td>

<td align="center">1429</td>

</tr>

<tr>

<td>Market information - dynamics, competition, innovations, trends</td>

<td align="center">1188</td>

</tr>

<tr>

<td>Own performance information - how efficient is the creation of value; ratio of outputs to inputs</td>

<td align="center">1079</td>

</tr>

<tr>

<td>Competence information - principal drivers of competence and competitive advantage, and their status</td>

<td align="center">1048</td>

</tr>

<tr>

<td>Assets availability and use - what assets are used and how (in the first place - inappropriate use)</td>

<td align="center">917</td>

</tr>

<tr>

<td>Macroeconomic information - figures and trends: inflation, interest rates, currency rates, prices of raw materials etc.</td>

<td align="center">786</td>

</tr>

<tr>

<td>Other information - news, legal acts, etc.</td>

<td align="center">24</td>

</tr>

</tbody>

</table>

The most important information needs, according to the results, are related to monitoring the organization's own status or its close environment, while the information on macro conditions has been given only a sixth place. This distribution represents the attention given to the competitive environment in which an organization acts.

### Environment monitoring activities

**_Question 2\. What are the most important information needs for monitoring of internal environment?_**

Question 2 expands the _current status_ information (Table 3) into most important issues to be monitored. These issues are routine to the majority of information systems (IS) operating in organizations. Here the respondents indicated the issues to which most attention had been paid.

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: The most important internal information needs**  
</caption>

<tbody>

<tr>

<td>Financial status</td>

<td align="center">218</td>

</tr>

<tr>

<td>Performance data</td>

<td align="center">194</td>

</tr>

<tr>

<td>Payables and receivables</td>

<td align="center">190</td>

</tr>

<tr>

<td>Unfinished work</td>

<td align="center">151</td>

</tr>

<tr>

<td>Other - inventory, quality, actual vs. planned</td>

<td align="center">13</td>

</tr>

</tbody>

</table>

**_Question 3\. What is the use of information technology for monitoring the above issues?_**

The results show the use of information technology for monitoring the issues presented in Table 4:

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Use of information technology for monitoring the internal environment**  
</caption>

<tbody>

<tr>

<td>For all data</td>

<td align="center">161</td>

</tr>

<tr>

<td>For some of the data</td>

<td align="center">86</td>

</tr>

<tr>

<td>None</td>

<td align="center">3</td>

</tr>

</tbody>

</table>

The responses to questions 2 and 3 show that the users are quite comfortable using information technology for monitoring key data about their organization's activities. Information is easily provided from their in-house information system, created precisely to monitor these activities. The absolute majority of responders (161 or 64.4%) indicated that information technology is used to monitor all of the above named issues relating to an organization's internal information needs. According to the structure of information needs presented earlier in this paper, such needs are attributed mostly to the simple common needs. The information system-based information tasks are largely routine and satisfaction of the needs does not pose any significant problems. _The use of information technology here is undisputed and considered commonplace_.

**_Question 4\. What are the most important information needs for monitoring of external environment?_**

When monitoring the external environment, the following most relevant areas were pointed out:

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 6: The most important information needs on external environment**  
</caption>

<tbody>

<tr>

<td>Prospects and opportunities</td>

<td align="center">201</td>

</tr>

<tr>

<td>Competition behaviour</td>

<td align="center">189</td>

</tr>

<tr>

<td>Customer behaviour</td>

<td align="center">186</td>

</tr>

<tr>

<td>Field or industry news</td>

<td align="center">167</td>

</tr>

<tr>

<td>State macro policy</td>

<td align="center">117</td>

</tr>

<tr>

<td>Foreign markets' situation</td>

<td align="center">111</td>

</tr>

<tr>

<td>Other - news stories, subscription to field newsletters, legal information, environmental factors</td>

<td align="center">9</td>

</tr>

</tbody>

</table>

The most watched types of external data (prospects and opportunities, competition and customer behaviour) delineate in general terms the competitive environment that is being monitored, both for opportunities and threats. Figuratively speaking, we can describe the mission of external monitoring as _striving to understand the market as well as the company's own warehouse_, and this being no simple task, various tradeoffs have to be introduced regarding goals and available resources. One of the possible approaches to increase the efficiency of external monitoring is to achieve _more with less_, that is, the best possible results with least relative effort, concentrating on the principal issues.

**_Question 5\. Who performs the monitoring function?_**

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 7: Cases of organizing the monitoring function**  
</caption>

<tbody>

<tr>

<td>Specially appointed unit or people</td>

<td align="center">40</td>

</tr>

<tr>

<td>Employees in appropriate area</td>

<td align="center">201</td>

</tr>

<tr>

<td>Other - summed up by marketing division; management; everybody in the organization</td>

<td align="center">9</td>

</tr>

</tbody>

</table>

The responses indicate the spread of external monitoring functions throughout the organization, in very few cases delegating these functions to a special unit.

**_Question 6\. What is the use of information technology for monitoring the external environment?_**

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 8: Use of information technology for monitoring the external environment**  
</caption>

<tbody>

<tr>

<td>Information technology used for all data</td>

<td align="center">125</td>

</tr>

<tr>

<td>Information technology used for some of the data</td>

<td align="center">122</td>

</tr>

<tr>

<td>None</td>

<td align="center">3</td>

</tr>

</tbody>

</table>

The survey did not ask why there is less use of information technology for external monitoring. However, the lower numbers of use do not point to the second-rate importance of external monitoring. As data in Table 3 have shown, the importance of external monitoring ranks second only to the vital data about the organization's own situation. However, the sources of external information are not under the control of a single information system, as in the case of internal information sources. Compared to the internal environment, the external environment is much more turbulent and unpredictable; there is a greater variety of issues to be monitored, information sources, formats, and access modes. _These conditions complicate the use of information technology for external monitoring._ It should be also noted that because the volumes of available external information are growing, in order to extract meaningful and important information the users have to perform specific functions requiring additional resources (e.g., integration, filtering or context management).

**_Question 7\. What information technology-based techniques are used for external monitoring?_**

<table width="55%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 9: Use of information technology-based techniques for external monitoring**  
</caption>

<tbody>

<tr>

<td>Analysis of other organizations Web pages and public information</td>

<td align="center">228</td>

</tr>

<tr>

<td>Internet and general search in it</td>

<td align="center">228</td>

</tr>

<tr>

<td>Search in public databases</td>

<td align="center">149</td>

</tr>

<tr>

<td>Collection and matching of separate data by own resources</td>

<td align="center">146</td>

</tr>

<tr>

<td>Other - industry newsletters etc.</td>

<td align="center">12</td>

</tr>

</tbody>

</table>

Together with the ubiquitous use of Internet, the equally important source is directed and more focused analysis of public information from available sources. The activity of collection and matching separate information from different sources requires information integration and use the _mosaic_ principle (here it is understood as putting separate pieces on the same subject together to obtain a coherent view), and so can be attributed to business intelligence function. We should be aware, however, that if combined with responses to the question 5 (Table 7), the responses show that the business intelligence function as such is mostly not recognized to a point that it would have a dedicated unit inside an organization; instead, monitoring and intelligence functions are spread throughout line units.

**_Question 8\. Does your external monitoring mode allow to forecast important changes? If so, what is the role of information technology?_**

The responses to this open-ended question were expected to provide some insight into actual implementation of the monitoring function, and the role of information technology in supporting this function. Although the variety of provided responses is somewhat significant, they have been grouped into several dominating types, as presented below in Table 10.

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 10: Ability to forecast important changes and information technology role**  
</caption>

<tbody>

<tr>

<td>Yes; information technology is a vital component</td>

<td align="center">57</td>

<td align="center">22.8%</td>

</tr>

<tr>

<td>Yes; information technology provides required technical support for search. retrieval. data accumulation and processing functions</td>

<td align="center">105</td>

<td align="center">42.0%</td>

</tr>

<tr>

<td>Yes; information technology provides support in detecting changes. but not so in forecasting them</td>

<td align="center">14</td>

<td align="center">5.6%</td>

</tr>

<tr>

<td>No</td>

<td align="center">27</td>

<td align="center">10.8%</td>

</tr>

<tr>

<td>No opinion</td>

<td align="center">4</td>

<td align="center">1.6%</td>

</tr>

<tr>

<td>Did not specify</td>

<td align="center">43</td>

<td align="center">17.2%</td>

</tr>

<tr>

<td align="right">**Total:**</td>

<td align="center">250</td>

<td align="center">100.0%</td>

</tr>

</tbody>

</table>

According to the above responses, information technology is considered a helpful aid in monitoring and detecting changes, but limited in supporting such needs as recognition of important changes or sense-making. The absolute majority of responses (105 out of 207, or about 51%) stressed the role of information technology as a principal technical support tool. No responses stated that information technology had significantly supported the function of sense-making (revealing important changes in the environment). Very few cases have been encountered when the use of special analytical or related software has been mentioned, although there were a couple of Customer Relationship Management solutions, the AutoMaster reporting tool, and domestic DocLogix software for monitoring the competition.

**_Question 9\. How well do the information technology-based external monitoring functions adapt to changing conditions and needs?_**

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 11: Ability of monitoring functions to adapt to changing conditions and needs**  
</caption>

<tbody>

<tr>

<td>Flexibly enough</td>

<td align="center">130</td>

</tr>

<tr>

<td>Partially</td>

<td align="center">105</td>

</tr>

<tr>

<td>Hard to adapt</td>

<td align="center">15</td>

</tr>

<tr>

<td align="right">Total</td>

<td align="center">250</td>

</tr>

</tbody>

</table>

The responses, combined with the earlier answers (question 7 in particular), suggest the continuous use of the tested and rather simple tools, such as general browsing and directed searches. The significant share of _partially flexible_ cases suggests the reasons for avoiding the use of more advanced environment monitoring tools, like intelligent agents: there is a risk of substantial and sensitive investment becoming obsolete. These results suggest that the sense-making and discovery part of the monitoring function leans towards the human side of user-technology union. This point has been stated in earlier sources ([Raggad 1997](#rag97)): the efficiency versus flexibility trade-off usually leans towards efficiency, leaving flexibility to users.

### Decision making and information use

The general purpose of this part of the questionnaire was to evaluate how easy it is to anticipate, plan and fulfil decision information needs. One of the first issues of interest regarding decision making has been to find out whether cases had been encountered when decision information needs had been completely satisfied to a sufficient understanding of a problem, or, in other words, a developed decision model had explicitly covered the cognitive gap (that is, the pieces finally fit). A number of respondents indicated that they encountered such a situation, and although this question was an open one, the most typical responses are presented below.

**_Question 10\. When making important decisions, have there been any cases with complete information to support the decision?_**

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 12: Number of decision cases with complete information**  
</caption>

<tbody>

<tr>

<td>Yes</td>

<td align="center">163</td>

<td align="center">65%</td>

</tr>

<tr>

<td>No</td>

<td align="center">48</td>

<td align="center">19%</td>

</tr>

<tr>

<td>It depends</td>

<td align="center">29</td>

<td align="center">12%</td>

</tr>

<tr>

<td>No answer</td>

<td align="center">10</td>

<td align="center">4%</td>

</tr>

<tr>

<td align="right">**Total:**</td>

<td align="center">250</td>

<td align="center">100%</td>

</tr>

</tbody>

</table>

Below are several more typical responses related to the _It depends_ responses:

*   Actually, most of the decisions are made with incomplete information, although the basic information should be present in any case.
*   Information is the basis for decision, but uncertainty is always there because one cannot examine all issues in a limited time.
*   If the situation is similar to the previous experience, then decision information might be sufficient. If there's something new, information is mostly incomplete.
*   It happens, but most often in making important decision it seems there could be more information.
*   Not all the time, because there are factors which cannot be foreseen, and the situations do not repeat.

These figures and responses, of course, have to be taken cautiously. There might be significant bias in the sense that, for example, some action-inclined decision makers would consider their understanding of a problem sufficient enough and turn to act, while the more cautious ones in a similar situation would look around for more information and either delay the decision or consider it faulty.

**_Question 11\. Have there been cases when information needs have been known beforehand? If so, what kind of information?_**

The responses to this open-ended question have been grouped into the following groups:

<table width="65%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 13: The number of decision cases with known information needs**  
</caption>

<tbody>

<tr>

<td>Market information - customers, sales, needs, opportunities</td>

<td align="center">49</td>

<td align="center">31%</td>

</tr>

<tr>

<td>Competition information - competitors' status, strength, intentions, actions</td>

<td align="center">29</td>

<td align="center">18%</td>

</tr>

<tr>

<td>Internal information - financials, capacity, inventory</td>

<td align="center">27</td>

<td align="center">17%</td>

</tr>

<tr>

<td>Legal information - laws, regulations, standards</td>

<td align="center">26</td>

<td align="center">16%</td>

</tr>

<tr>

<td>No such cases</td>

<td align="center">26</td>

<td align="center">16%</td>

</tr>

<tr>

<td>Technical information</td>

<td align="center">2</td>

<td align="center">1%</td>

</tr>

<tr>

<td>Did not specify</td>

<td align="center">91</td>

<td align="center">36,4%</td>

</tr>

<tr>

<td align="right">**Total:**</td>

<td align="center">250</td>

<td align="center">100%</td>

</tr>

</tbody>

</table>

These responses mostly include information whose content and location are well known and generally accessible because of earlier experience in related cases. It is usually easy to plan and prepare, and this information or its access points can be contained in close proximity to the decision makers.

**_Question 12\. Have there been cases when information needs have not been known beforehand, having emerged only while making a decision or too late altogether? If so, what kind of information?_**

The responses to this open-ended question have been grouped as shown in Table 14:

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 14: The number of decision cases with unexpected information needs**  
</caption>

<tbody>

<tr>

<td>No such cases</td>

<td align="center">86</td>

<td align="center">34.4%</td>

</tr>

<tr>

<td>Yes. there have (without specifying the information)</td>

<td align="center">46</td>

<td align="center">18.4%</td>

</tr>

<tr>

<td>Market information</td>

<td align="center">23</td>

<td align="center">9.2%</td>

</tr>

<tr>

<td>Internal information</td>

<td align="center">15</td>

<td align="center">6.0%</td>

</tr>

<tr>

<td>Competition information</td>

<td align="center">14</td>

<td align="center">5.6%</td>

</tr>

<tr>

<td>Legal information</td>

<td align="center">14</td>

<td align="center">5.6%</td>

</tr>

<tr>

<td>Technical information</td>

<td align="center">14</td>

<td align="center">5.6%</td>

</tr>

<tr>

<td>Informal, _soft_ information - opinions. foresights</td>

<td align="center">12</td>

<td align="center">4.8%</td>

</tr>

<tr>

<td>Confidential information - e.g., reliability checks</td>

<td align="center">5</td>

<td align="center">2.0%</td>

</tr>

<tr>

<td>Did not specify</td>

<td align="center">21</td>

<td align="center">8.4%</td>

</tr>

<tr>

<td align="right">**Total:**</td>

<td align="center">250</td>

<td align="center">100%</td>

</tr>

</tbody>

</table>

This group of responses can be explained by the novelty of the problem or changing conditions. It is hard to plan for these conditions, and information cannot be kept prepared and handy; instead, some generic information sources that are most often used in such situations can be made ready to use whenever required, including electronic public archives, search engines and directories. A decision support environment providing a set of such sources should be quite helpful. As this activity requires more creative involvement from the users' side, a side product of satisfaction of such needs is intensive learning and gaining of new experience for decision makers.

### Preservation of competence and valuable experiences

**_Question 13\. How is competence information expressed, recorded and accumulated (valuable information about performed assignments, completed projects, work setup, customers, markets)?_**

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 15: Forms of recording the competence information**  
</caption>

<tbody>

<tr>

<td>In hard form - formal records and reports in paper or digital media</td>

<td align="center">32</td>

</tr>

<tr>

<td>In soft form - as notes, specialist knowledge, experience, opinions</td>

<td align="center">15</td>

</tr>

<tr>

<td>Both</td>

<td align="center">202</td>

</tr>

<tr>

<td>Did not specify</td>

<td align="center">1</td>

</tr>

<tr>

<td align="right">Total</td>

<td align="center">250</td>

</tr>

</tbody>

</table>

Combined records dominate, and the respondents usually use whatever media is most convenient at the moment.

**_Question 14\. What is the role of information technology in preserving the competence information?_**

The responses to this open-ended question have been grouped as shown in Table 16:

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 16: The groups of responses regarding the role of information technology in preserving competence information**  
</caption>

<tbody>

<tr>

<td>Vital - widely used. analytical approaches dominate</td>

<td align="center">109</td>

<td align="center">43.6%</td>

</tr>

<tr>

<td>Technical - storage and retrieval functions dominate</td>

<td align="center">57</td>

<td align="center">22.8%</td>

</tr>

<tr>

<td>Limited - small-scale use; few procedures implemented</td>

<td align="center">5</td>

<td align="center">2.0%</td>

</tr>

<tr>

<td>None</td>

<td align="center">2</td>

<td align="center">0.8%</td>

</tr>

<tr>

<td>Did not specify</td>

<td align="center">77</td>

<td align="center">30.8%</td>

</tr>

<tr>

<td align="right">**Total**</td>

<td align="center">250</td>

<td align="center">100%</td>

</tr>

</tbody>

</table>

The responses indicate efficient use of information technology potential for organized and concise records. The problems lie in a changing environment which influences the value of accumulated competence information; soft information is hard to maintain and needs different approaches.

**_Question 15\. Are important decisions and their development steps recorded and preserved? If so, in what form?_**

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 17: The ways of formally recording competence information**  
</caption>

<tbody>

<tr>

<td>None</td>

<td align="center">11</td>

</tr>

<tr>

<td>Paper documents</td>

<td align="center">166</td>

</tr>

<tr>

<td>Computer text (reports)</td>

<td align="center">188</td>

</tr>

<tr>

<td>Computer models, procedures, computations</td>

<td align="center">158</td>

</tr>

<tr>

<td>Other - e.g., experience in persons' minds</td>

<td align="center">7</td>

</tr>

</tbody>

</table>

This information is accumulated as a part of the experience and competence records. With exception of a few cases, the need to produce experience records is undisputed. It provides insurance for future cases and in some cases is the only source of experience information and proof of facts.

**_Question 16\. Are there any cases of re-using important decision experience? If so, with what success?_**

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 18: The groups of cases of reusing important decision experience**  
</caption>

<tbody>

<tr>

<td>Yes; all the time</td>

<td align="center">81</td>

<td align="center">32.4%</td>

</tr>

<tr>

<td>Yes; sometimes</td>

<td align="center">113</td>

<td align="center">45.2%</td>

</tr>

<tr>

<td>No</td>

<td align="center">33</td>

<td align="center">13.2%</td>

</tr>

<tr>

<td>No opinion</td>

<td align="center">4</td>

<td align="center">1.6%</td>

</tr>

<tr>

<td>Did not specify</td>

<td align="center">19</td>

<td align="center">7.6%</td>

</tr>

<tr>

<td align="right">**Total**</td>

<td align="center">250</td>

<td align="center">100%</td>

</tr>

</tbody>

</table>

The responses indicate that the re-use of important problem-solving and decision making experience is of mixed success; recorded practice is considered, but needs to be constantly re-evaluated. The experience records can include anything from anecdotal information to hard facts and are recorded in all convenient ways: free text format in digital media, structured format (with some standardized features and values) in digital media, and the same on paper. The recorded experience is not mass data such as data on principal operations and activities, but it needs arranging to be properly used afterwards. Information technology's role can be seen mostly in arranging, managing structures, imposing standards and allowing easy filtering and retrieval. Level of re-use is limited because of the changing context, although the re-use of templates, structures, models and other procedural issues is commonplace. Lately the importance of related context has been more and more recognized regarding recorded experience. The introduction of managed context is expected to increase the richness of preserved experience information and to avoid the limitations of stereotypes. In the author's opinion, there is, though, an apparent trade-off between the recorded richness (how much) and efficiency (how basic and manageable). This interesting area of related research deserves special attention that goes beyond the goals of this paper.

### Discussion and conclusions

The approach, based on the structure of information needs and its connection to decision support and problem solving activities, allows us to highlight some issues concerning the actual use of information technology in satisfying the information needs for decision making. The results of the survey point to several findings.

First of all, information technology is accepted as a _de facto_ management tool, which creates the basis for various management techniques such as electronic monitoring, decision analysis and support, and extensive records. The principal issue here is: how much support is expected from information technology and how much space for initiative and flexibility is left to the users? The dominant response here seems to be _more with less_, meaning that by focusing on business performance and supporting it with the known strengths of information technology (store information; direct user to information sources; retrieve, filter and present information; browse and combine; provide communication tools), the users have more initiative so they can achieve more in terms of creativity and flexibility, and be less distracted from the problem by the need to master complicated support tools. information technology is seen by respondents as an enabler, shifting from technology push to user pull and letting the users act more freely and creatively.

One possible general approach to the problem of the efficient satisfaction of decision information needs might be an exploitation of _the bicycle principle_: a bicycle is used to expand the power of human muscle, while information technology is supposed to expand the power of a human brain. The attempts to substitute intelligent human activities in not-so-trivial situations so far have shown limited success (for example, expert systems in business, natural language systems). Instead, the responses show that in most cases, specifically regarding decision support, what is needed most is a base of a limited number of foundation points, that is, key reliable data on the problem to be solved. Reliability can be reinforced by introducing a triangulation principle ([Culkin _et al._ 1999](#cul99)) where data on the same subject from several independent sources are more reliable. A similar point is expressed in the author's earlier work ([Skyrius 2002](#sky02)), where the interviewed decision makers pointed out an ability to check the same information from different sources as an important feature of decision support.

The experience of meeting complex and special information needs for important problems and decisions, though recorded and preserved in literally all cases, has shown limited re-use, mostly because of the non-repetitive nature of such problems and decisions. The reluctant use of intelligent information technology-based analysis and discovery systems can be explained by the risk of substantial and sensitive investment into systems that can become obsolete, as well as the need for significant time and managerial resources required to master such systems.

The recent and growing interest in human-centred information systems advocates (among other things) user-tailored systems and environments. Finding this quite in line with the results presented in this paper, several recommendations can be made concerning the development of an efficient information environment for decision makers. The features of such environment should be:

*   a simple set of support tools that are close and easy to use (_first tier_): information sources and search engines, simple models, procedures, arranging tools, recorded experience and archives;
*   easy access to second tier: more distant and more complicated sources and techniques that are required much less often;
*   manageable support environment that allows easy switching of items between tiers, similar to the form of managerial dashboards with interchangeable items on display;
*   information feed of appropriate quality and timeliness to this environment.

_The first tier_ should contain the close support environment that is required most of the time: uncomplicated to use and able to be configured according to users' needs.

*   Principal pieces of data: for example, sales, market share, cash-at-hand, completed orders, outstanding payments and currency rates;
*   Search tools: simple search in own sources (archives, competence and experience bases); simple search in public sources, both general (e.g., Google) and specialized (e.g., _Financial Times_ archive); arranging tools (sorting of results by relevance); hand-rating of the results; easy classification and annotation;
*   Tools for simple calculations: templates, financial models, other simple models.

The second tier might, for example, include:

*   more distant and complex information sources with advanced search tools;
*   modelling tools for forecasting, regression, simulation;
*   data analysis technologies - drill-down tools, OLAP, data mining facilities;
*   presentation, graphing and visualization tools.

The more defined set of features for both tiers of the support environment could lead to a possible set of requirements for the interface design of an information environment for decision makers.

Further research should encompass issues that are more specific but stay in the directions delineated by this paper. Three groups of issues are considered at the moment of writing. First, regarding monitoring activities, it is intended to research how environmental changes influence changes in monitoring techniques and information use for decision making. Secondly, the research on simple and efficient decision support should investigate what information has to be at hand, or in _the first tier_, as described above, as well as how recent and encompassing it should be. Thirdly, there is interest in what experience information should be preserved, regarding important problems and decisions: what key points would be sufficient to record a brief but essential context and what experience items are reusable in the most cases.

## Acknowledgements

The author would like to thank the anonymous referees of this paper for their constructive and well-pointed comments and suggestions that have helped a great deal in upgrading this paper. The author would also like to thank the students of Master of International Commerce program at the School of International Business at the University of Vilnius for their tremendous help in distributing the questionnaire and eliciting the results.

<form action="">

<fieldset><legend style="color: white; background-color: #5E96FD; font-size: medium; padding: .1ex .5ex; border-right: 1px solid navy; border-bottom: 1px solid navy; font-weight: bold;">References</legend>

*   <a id="ash01" name="ash01"></a>Ashill, J. & Jobber, D. (2001). Defining the information needs of senior marketing executives: an exploratory study. _Qualitative Market Research: An International Journal,_ , **4**(1), 52-60.
*   <a id="coh00" name="coh00"></a>Cohen, E.B. (2000). Failure to inform: errors in informing systems. In M. Chung, (Ed.) (2000). _Proceedings of the 2000 Americas Conference on Information Systems (AmCIS 2000), August 10-13th, 2000, Long Beach, California_, (pp. 1057-1061). Long Beach, CA: California State University.
*   <a id="coh07" name="coh07"></a>Cohen, R. (2007, March 8). [BI strategy: optimizing your enterprise information environment, part 1: issues and first steps.](http://www.webcitation.org/5Yb0QWIh4) _DMReview Online_. Retrieved 10 June 2008 from http://www.dmreview.com/article_sub.cfm?articleId=1077725 (Archived by WebCite® at http://www.webcitation.org/5Yb0QWIh4)
*   <a id="cul99" name="cul99"></a>Culkin, N., Smith, D. & Fletcher, J. (1999). Meeting the information needs of marketing in the twenty-first century. _Marketing Intelligence & Planning,_ , **17** (1), 6-12.
*   <a id="dav06" name="dav06"></a>Davenport, T. (2006). [Competing on analytics.](http://www.webcitation.org/5Yb0iDuiR) _Harvard Business Review_, **84**(1), 99-107\. Retrieved 15 June, 2008 from http://www.revenueanalytics.com/pdf/CompetingOnAnalyticsHBR2006.pdf (Archived by WebCite® at http://www.webcitation.org/5Yb0iDuiR)
*   <a id="dhe93" name="dhe93"></a>Dhebar, A. (1993). Managing the quality of quantitative analysis. _Sloan Management Review_, **34**(2), 69-75\.
*   <a id="fer05" name="fer05"></a>Ferguson G., Mathur, S. & Shah, B. (2005). Evolving from information to insight. _Sloan Management Review_, **46**(2), 51-58\.
*   <a id="har02" name="har02"></a>Harris, J.G. (2002). [Overcoming management attention deficit disorder: Research note.](http://www.webcitation.org/5Yb2hsc8Y) _Accenture Institute for High Performance Business,_ March 14, 2002\. Retrieved 10 June 2008 from http://tinyurl.com/3ho4em (Archived by WebCite® at http://www.webcitation.org/5Yb2hsc8Y)
*   <a id="hil04" name="hil04"></a>Hill, J. & Scott, T. (2004). A consideration of the roles of business intelligence and e-business in management and marketing decision making in knowledge-based and high-tech start-ups. _Qualitative Market Research_, **7**(1), 48-57\.
*   <a id="joh04" name="joh04"></a>Johnstone, D., Tate, M. & Bonner, M. (2004). [Bringing human information behaviour into information systems research: an application of systems modelling.](http://www.webcitation.org/5Yb2vBP2B) _Information Research_, **9**(4), paper 191\. Retrieved 10 January 2007 from http://informationr.net/ir/9-4/paper191.html (Archived by WebCite® at http://www.webcitation.org/5Yb2vBP2B)
*   <a id="kee78" name="kee78"></a>Keen, P.G.W. & Morton, M.S. (1978). Decision support systems: an organizational perspective. Reading, MA: Addison Wesley.
*   <a id="kli96" name="kli96"></a>Kling, R. (1996). _Computerization and controversy: value conflicts and social choices._ (2nd ed.) San Diego, CA: Academic Press.
*   <a id="lac99" name="lac99"></a>Lacity, M. & Sauter, V. (1999). [Why general managers need to understand information systems.](http://www.webcitation.org/5Yb367NzJ) Saint-Louis, MO: University of Missouri, July 1999\. Retrieved 10 June 2008 from http://www.umsl.edu/~lacity/whymis.html (Archived by WebCite® at http://www.webcitation.org/5Yb367NzJ)
*   <a id="mac06" name="mac06"></a>Maceviciute, E. (2006). [Information needs research in Russia and Lithuania, 1965-2003\.](http://www.webcitation.org/5Yb3D5Qv9) _Information Research_, **11**(3), paper 256\. Retrieved 10 June 2008 from http://informationr.net/ir/11-3/paper256.html (Archived by WebCite® at http://www.webcitation.org/5Yb3D5Qv9)
*   <a id="mal05" name="mal05"></a>Malhotra, Y. (2005). Integrating knowledge management technologies in organizational business processes: getting real time enterprises to deliver real business performance. _Journal of Knowledge Management_, **9**(1), 7-28\.
*   <a id="mal04" name="mal04"></a>Malhotra, Y. & Galletta, D. (2004). Building systems that users want to use. _Communications of the ACM_, **47**(12), 89-94\.
*   <a id="mca04" name="mca04"></a>McAfee, A. (2004). Do you have too much IT? _Sloan Management Review,_ , **45**(3), 18-22\.
*   <a id="mck05" name="mck05"></a>McKenzie, M.L. (2005). [Managers look to the social network to seek information.](http://www.webcitation.org/5Yb3JZrVY) _Information Research_, **10**(2), paper 216\. Retrieved 10 June 2008 from http://informationr.net/ir/10-2/paper216.html (Archived by WebCite® at http://www.webcitation.org/5Yb3JZrVY)
*   <a id="mel04" name="mel04"></a>Melchert, F. & Winter, R. (2004). [The enabling role of information technology for business performance management.](http://www.webcitation.org/5Yb3VOGNJ) In _The 2004 IFIP International Conference on Decision Support Systems (DSS2004), Prato, Italy, July 1-3, 2004_, (pp. 535-546). Caulfield East, Victoria, Australia: Monash University. Retrieved 15 June, 2008 from http://tinyurl.com/3nkdg9 (Archived by WebCite® at http://www.webcitation.org/5Yb3VOGNJ)
*   <a id="nak04" name="nak04"></a>Nakatsu, R. (2004). [Explanatory power of intelligent systems: a research framework](http://www.webcitation.org/5Yb84Btjg). In _The 2004 IFIP International Conference on Decision Support Systems (DSS2004), Prato, Italy, July 1-3, 2004_, (pp. 568-577). Caulfield East, Victoria, Australia: Monash University. Retrieved 15 June, 2008 from http://tinyurl.com/6buosh (Archived by WebCite® at http://www.webcitation.org/5Yb84Btjg)
*   <a id="nie03" name="nie03"></a>Niedzwiecka B. [A proposed general model of information behaviour.](http://www.webcitation.org/5Yb8CXgYS) _Information Research_, **9**(1), paper 164\. Retrieved 10 January 2007 from http://informationr.net/ir/9-1/paper164.html (Archived by WebCite® at http://www.webcitation.org/5Yb8CXgYS)
*   <a id="rag97" name="rag97"></a>Raggad, B.G. (1997). Decision support system: use IT or skip IT. _Industrial Management & Data Systems,_ **97**(2), 43-50.
*   <a id="rie05" name="rie05"></a>Riege, A. (2005). Three-dozen knowledge-sharing barriers managers must consider. _Journal of Knowledge Management_, **9**(3), 18-35\.
*   <a id="sau99" name="sau99"></a>Sauter, V. (1999). Intuitive decision making. _Communications of the ACM_, **42**(6), 109-116.
*   <a id="she06" name="she06"></a>Sheff, H. (2006, February 1). [Making sense of analytics.](http://www.webcitation.org/5Yb8Kj8hd) _Customer Management Insight_. Retrieved 10 June 2008 from http://www.callcentermagazine.com/showArticle.jhtml?articleID=177101987 (Archived by WebCite® at http://www.webcitation.org/5Yb8Kj8hd)
*   <a id="sim60" name="sim60"></a>Simon, H. (1960). _The new science of management decision._ New York, NY: Harper and Row.
*   <a id="sjo02" name="sjo02"></a>Sjöberg, L. (2002). _[The distortion of beliefs in the face of uncertainty](http://www.webcitation.org/5Yb8eZGN0)_. Stockholm: Stockholm School of Economics, Department of Economic Psychology. (SSE/EFI Working Paper Series in Business Administration, No. 2002:9). Retrieved 15 June, 2008 from http://swoba.hhs.se/hastba/papers/hastba2002_009.pdf (Archived by WebCite® at http://www.webcitation.org/5Yb8eZGN0)
*   <a id="sky02" name="sky02"></a>Skyrius, R. (2002). [Human factors in person-technology relations in business decision making.](http://www.webcitation.org/5Yb8rv4u0) In _IS2002 Informing Science + IT Education conference. University of Cork. Cork, Ireland._ (pp. 1419-1426). Retrieved 10 June 2008 from http://proceedings.informingscience.org/IS2002Proceedings/papers/Skyri191Human.pdf. (Archived by WebCite® at http://www.webcitation.org/5Yb8rv4u0)
*   <a id="sky00" name="sky00"></a>Skyrius R. & Winer, C.R. (2000). Information technology and management decision support in two different economies: a comparative study. In _Challenges of Information Technology Management in the 21st Century: 2000 Information Resources Management Association International Conference. Anchorage, Alaska, USA,_ (pp. 714-716). Hershey, PA: IGI Publishing.
*   <a id="spr82" name="spr82"></a>Sprague R. & Carlson, E. (1982). _Building effective decision support systems._ . Englewood Cliffs, NJ: Prentice Hall.
*   <a id="tur04" name="tur04"></a>Turpin M. & du Plooy, N. (2004). [Decision-making biases and information systems](http://www.webcitation.org/5Yb93v7YB). In _The 2004 IFIP International Conference on Decision Support Systems (DSS2004), Prato, Italy, July 1-3, 2004_ (pp. 782-792). Caulfield East, Victoria, Australia: Monash University. Retrieved 15 June, 2008 from http://tinyurl.com/3qxvrt (Archived by WebCite® at http://www.webcitation.org/5Yb93v7YB)
*   <a id="wil00" name="wil00"></a>Wilson, T.D. (2000). Human information behavior. _Informing Science,_ **3**(2), 49-55\.
*   <a id="zho07" name="zho07"></a>Zhong, N., Liu, J. & Yao, Y. (2007). Envisioning intelligent information technologies through the prism of web intelligence. _Communications of the ACM,_ **50**(3), 89-94\.




## Appendix

### Questionnaire on Business management and decision making information needs

**I. Monitoring of internal and external environments of an organization**

1.  What are the general most important information needs that should be satisfied in the first place?

*   Current status information - cash flow, liquidity, inventory, payables/receivables etc.
*   Market information
*   Own performance information
*   Competence information
*   Assets availability and their use
*   Macroeconomic information
*   Other information - please indicate

3.  What are the most important information needs for monitoring the internal environment?

*   Financial status
*   Performance data
*   Payables and receivables
*   Unfinished work
*   Other - please indicate

5.  How does the use of IT cover the monitoring of internal issues?

*   For all data
*   For some of the data
*   None

7.  What are the most important information needs for monitoring the external environment?

*   Prospects and opportunities
*   Customer behavior
*   Competition behavior
*   Field/Industry news
*   State macro policy
*   Foreign markets situation
*   Other information - please indicate

9.  Who performs the monitoring function?

*   Specially appointed unit or people
*   Employees in the appropriate area
*   Other approaches - please indicate

11.  How does the use of IT cover the monitoring of the external environment?

*   IT used for all data
*   IT used for some of the data
*   None

13.  What IT-based techniques are used for external monitoring?

*   Internet and general Web search
*   Analysis of other organizations Web pages and public information
*   Search in public databases
*   Collection and matching of separate data by own resources
*   Other - please indicate

15.  Does your external monitoring mode allow the forecasting of important changes? If so, what is the role of IT?
16.  How well do the IT-based external monitoring functions adapt to changing conditions and needs?

*   Flexibly enough
*   Partially
*   Hard to adapt

**II. Decision making and information use**

19.  When making important decisions, have there been any cases with complete information to support the decision?

*   Yes
*   No
*   It depends (comment if possible)
*   No answer

21.  If there have been cases, described in answers to question 10, what kind of information completed the decision needs?

*   Formal information (own or received from outside)
*   Informal information (opinions, estimates, other soft information)
*   Hard to tell

23.  Have there been cases when information needs have been known beforehand? If so, what kind of information?
24.  Have there been cases when information needs have not been known beforehand, having emerged only while making a decision or too late altogether? If so, what kind of information?

**III. Preservation of competence and valuable experiences**

26.  How is competence information expressed, recorded and accumulated (valuable information about performed assignments, completed projects, work setup, customers, markets)?

*   In hard form - formal records and reports in paper or digital media
*   In soft form - as notes, specialist knowledge, experience, options
*   Both

28.  What is the IT role in preserving competence information?
29.  Are important decisions and their development steps recorded and preserved? If so, in what form?

*   None
*   Paper documents
*   Computer text (reports)
*   Computer models, procedures, computations
*   Other forms - e.g., experience in persons' minds

31.  Are there any cases in re-using important decision experience? If so, with what success?

If you have had cases with wrong decisions, please answer the following questions.

33.  If lack of information was the principal cause of the wrong decision, what kind of information was it?
34.  Was it possible to obtain this information?
35.  Why was this information not received?
36.  Was it possible to receive this information with the help of IT?

_Please provide general facts about your organization:_

1.  Field of activity
2.  Number of employees
3.  organization age in years



