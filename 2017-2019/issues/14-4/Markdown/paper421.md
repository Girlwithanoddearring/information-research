#### vol. 14 no. 4, December, 2009

* * *

# Visualization and evolution of the scientific structure of fuzzy sets research in Spain

#### [A.G. López-Herrera](#authors), [M.J. Cobo](#author), [E. Herrera-Viedma](#authors), [F. Herrera](#authors)  
Department of Computer Science and Artificial Intelligence,  
CITIC-UGR (Research Center on Information and Communications Technology),  
University of Granada,  
E-18071-Granada,  
Spain

#### [R. Bailón-Moreno](#author)  
Departmento de Ingeniería Química,  
University of Granada,  
E-18071-Granada,  
Spain

#### [E. Jiménez-Contreras](#author)  
Departmento de Biblioteconomía y Documentación University of Granada,  
E-18071-Granada,  
Spain

#### Abstract

> **Introduction.** Presents the first bibliometric study on the evolution of the fuzzy sets theory field. It is specially focused on the research carried out by the Spanish comunity.  
> **Method.** The CoPalRed software, for network analysis, and the co-word analysis technique are used.  
> **Analysis.** Bibliometric maps showing the main associations among the main concepts in the field are provided for the periods 1965-1993, 1994-1998, 1999-2003 and 2004-2008.  
> **Results.** The bibliometric maps obtained provide insight into the structure of the fuzzy sets theory research in the Spanish community, visualize the research subfields, and show the existing relationships between those subfields. Furthermore, we compare the Spanish community with other countries (the USA and Canada; the UK and Germany; and Japan and Peoples Republic of China).  
> **Conclusions.** As a result of the analysis, a complete study of the evolution of the Spanish fuzzy sets community and an analysis of its international importance are presented.

## <a id="section-1"></a>Introduction

Fuzzy set theory, which was founded by [Zadeh (1965)](#zadeh1965), has emerged as a powerful way of representing quantitatively and manipulating the imprecision in problems. The theory has been studied extensively over the past forty years and satisfactorily applied to problems (for examples, see [Dubois and Prade 1985](#dubois1985), [Kahraman _et al._ 2006](#kahraman2006), [Klir and Yuan 1995](#klir1995), [Zimmermann 2001](#zimmermann2001) or [Zadeh 2008](#zadeh2008)).

In Spain, the first paper on fuzzy set theory was published by Trillas and Riera ([1978](#trillas1978)). From 1978 to the present, more than 1,500 papers on the topic have been published by the Spanish fuzzy set research community, as shown by the ISI Web of Science. The Spanish fuzzy set community ranks seventh in the "top-ten" ranking of the most productive countries on fuzzy set theory research, and it is the second European country (according to data from ISI Web of Science).

According ISI Web of Science, from 1965 more than one hundred thousand papers on fuzzy set theory foundations and applications have been published in journals. In spite of this huge number of published papers there has been no bibliometric study on the field.

In this paper, the first bibliometric study is presented analysing the research carried out by the Spanish fuzzy set community. It is done by means of bibliometric maps, which show the associations between the main concepts studied by the Spanish community. The maps provide insight into the structure of the field, visualize the research subfields and indicate the existing relationships among these subfields. To generate these maps we use the software [CoPalRed](http://www.webcitation.org/query?url=http%3A%2F%2Fec3.ugr.es%2Fcopalred%2F&date=2009-12-15) which is based on the longitudinal mapping defined by [Garfield (1994)](#garfield1994) and the co-word analysis ([Callon _et al._ 1983](#callon1983), [Callon _et al._ 1991](#callon1991), [Coulter _et al._ 1998](#coulter1998), [Whittaker 1989](#whittaker1989)), which is a useful and powerful tool for describing and showing up the internal structure of a scientific field ([Bailón-Moreno _et al._ 2005](#bailon2005), [Bailón-Moreno _et al._ 2006](#bailon2006), [Leydesdorff and Zhou 2008](#leydesdorff2008), [Zhang _et al._ 2008](#zhang2008)).

The study reveals the main themes treated by the Spanish fuzzy set theory community from 1978 to 2008\. It also allows comparison with those themes treated by other important international communities such as the USA, Canada, the United Kingdom, Germany, Japan, the Peoples Republic of China and Spain, which are collectively known as the G7 group. These seven countries were selected because they satisfy three conditions:

1.  they must be in the top ten of the most productive countries (according to data in the ISI Web of Science);
2.  just two countries for each geographical area (America, Europe and Asia) are considered; and
3.  their first paper on the topic had to be published before 1980 (inclusive).

We complete our study by showing a quantitative analysis of the visibility of the Spanish fuzzy set community according to ISI's Web of Science, and showing a quantitative analysis on the importance of Spanish fuzzy research on the international fuzzy research according to ISI Web of Science. Doing those studies and comparisons, the impact of the Spanish fuzzy set theory community is also measured.

## <a id="section-2"></a>Methods

### <a id="section-2.1"></a>The Co-word analysis

Co-word analysis is a content analysis technique that is effective in mapping the strength of association between information items in textual data ([Callon _et al._ 1983](#callon1983), [Callon _et al._ 1991](#callon1991), [Coulter _et al._ 1998](#coulter1998), [Whittaker 1989](#whittaker1989)). It is a powerful technique for discovering and describing the interactions between different fields in scientific research ([Callon _et al._ 1991](#callon1991), [Bailón-Moreno _et al._ 2006](#bailon2006), [Leydesdorff and Zhou 2008](#leydesdorff2008), [Zhang _et al._ 2008](#zhang2008)). Co-word analysis reduces a space of descriptors (or keywords) to a set of network graphs that effectively illustrate the strongest associations between descriptors (Coulter _et al._ 1998).

According to Krsul:

"this technique illustrates associations between keywords by constructing multiple networks that highlight associations between keywords, and where associations between networks are possible". ([Krsul 1998: 80](#krsul1998))

The basic assumption in bibliometric mapping ([Börner _et al._ 2003](#borner2003)) is that each research field can be characterized by a list of the important keywords. Each publication in the field can in turn be characterized by a sub-list of these global keywords. Such sub-lists are like DNA fingerprints of these published articles.

[Börner _et al._ (2003: 185)](#borner2003) suggest that these 'fingerprints' can be used as a similarity measure: "The more keywords two documents have in common, the more similar the two publications are, and the more likely they come from the same research area or research specialty at a higher level".

[Garfield (1994, 1)](#garfield1994) suggests that longitudinal mapping can be used to plot the evolution of a field, and can be used by 'analysts and domain' experts to forecast trends in the field. Novices might also discover the key research areas and their interconnection.

According to [Börner _et al._ (2003: 188)](#borner2003), the process of constructing a bibliometric map can be divided into the following six steps:

1.  collection of raw data,
2.  selection of the type of item to analyse,
3.  extraction of relevant information from the raw data,
4.  calculation of similarities between items based on the extracted information,
5.  positioning of items in a low-dimensional space based on the similarities, and
6.  visualization of the low-dimensional space.

We now discuss how we implement each of these steps.

The first step in the process of bibliometric mapping, is the collection of raw data. In this paper, the raw data consist of a corpus containing 16,344 original papers, about fuzzy set theory produced by the G7 group from 1965 to 2008\. These original papers were extracted from ISI Web of Science with the query number 1 on 7th January 2009:

`query 1: (TS=fuzz* or SO=("FUZZY SETS AND SYSTEMS" OR "IEEE TRANSACTIONS ON FUZZY SYSTEMS")) AND CU=(SCOTLAND OR JAPAN OR CANADA OR SPAIN OR ENGLAND OR "NORTH IRELAND" OR USA OR "PEOPLES OR CHINA" OR WALES OR GERMANY) AND Document Type=(Article).`

Where _TS_ field is a search based on the _Topic_, _SO_ field is a search based on _Journal Title_ (_Fuzzy Sets and Systems_ and _IEEE Transactions on Fuzzy Systems_ are the two most important scientific international journals on the topic) and _CU_ field is a search based on the _Country_.

In Figure 1, the numbers of retrieved papers from ISI Web of Science from 1965 to 2008 is shown.

<figure id="fig-1">

![Retrieved papers from ISI Web of Science from 1965 to 2008 by query #1](../p421fig1.png)

<figcaption>

**Figure 1: Retrieved papers from ISI Web of Science from 1965 to 2008 by query 1.**</figcaption>

</figure>

Two blocks of papers were collected, one for analysing the evolution of the fuzzy set theory field in all of the above mentioned countries taken together and another block to analyse the Spanish case. From these two blocks of papers, four subsets of papers are extracted, one for each studied sub-period: 1965-1993 (although the fuzzy sets based research in Spain started in 1978), 1994-1998, 1999-2003 and 2004-2008\. In co-word analysis, in a longitudinal study, it is usual for the first sub-period to be the most long-lasting to get a representative number of published papers. For this reason, in this paper, the first studied sub-period includes twenty-nine years (from 1965 to 1993) and the other three sub-periods just five years. In this way, separate bibliometric maps can be constructed for each one of the four studied sub-periods. The number of papers in each block and sub-period is shown in [Figure 2](#fig-2).

<figure id="fig-2">

![Figure 2: Papers per block and sub-period.](../p421fig2.png)

<figcaption>

**Figure 2: Number of papers in each block and sub-period.**</figcaption>

</figure>

The second step is the selection of the type of item to analyse. According to [Börner _et al._ (2003)](#borner2003), journals, papers, authors, and descriptive terms or words are most commonly selected as the type of item to analyse. Each type of item provides a different visualization of a field of science and results in a different analysis. In this paper, we chose to analyse descriptive words, i.e., keywords. A bibliometric map showing the associations between keywords in a scientific field is referred to as a keywords-based map in this paper.

The third step is the extraction of relevant information from the raw data collected in the first step ([Börner _et al._ 2003](#borner2003)). Here, the relevant information consists of the co-occurrence frequencies of keywords. The co-occurrence frequency of two keywords is extracted from the corpus of papers by counting the number of papers in which both keywords occur in the keywords section.

The fourth step is the calculation of similarities between items based on the information extracted in the third step ([Börner _et al._ 2003](#borner2003)). Here, similarities between items are calculated on the basis of frequencies of keywords co-occurrences. When two keywords frequently occur together, they are said to be linked, and the intensity of the link is indicated by the equivalency index ([Michelet 1988](#michelet1988), [Callon _et al._ 1991](#callon1991)), _e<sub>ij</sub>_ defined as:

<figure>

![Equivalence index equation](../equivalenceIndex.png)</figure>

where _c<sub>ij</sub>_ is the number of documents in which two keywords _i_ and _j_ co-occur and _c<sub>i</sub>_ and _c<sub>j</sub>_ represent the number of documents in which each one appears. When the keywords appear together, the equivalency index equals unity; when they are never associated, it equals zero. Once the links are quantified, by an algorithm called _simple centres_, groupings or themes are produced, which consist of more strongly linked networks representing the centres of interest of the researchers.

As described by [Coulter _et al._ (1998)](#coulter1998), the simple centres algorithm uses two passes through the data to produce the desired networks. The first pass (Pass-1) constructs the networks depicting the strongest associations and links added in this pass are called internal links. The second pass (Pass-2) adds to these networks links of weaker strengths that form associations between networks. The links added during the second pass are called external links.

[Coulter _et al._ (1998)](#coulter1998) note that two keywords that appear infrequently in the corpus, but always appear together, will have higher strength values than keywords that appear many times in the corpus almost never together. Hence, possibly irrelevant or weak associations may dominate the network. A solution to this problem incorporated into the algorithm described in this section is to require that only the keyword pairs that exceed a minimum co-occurrence are considered potential links while building networks during the first pass of the algorithm. As each sub-period and block has different numbers of papers, different minimum co-occurrence values are used: 3, 4, 4 and 6 for the sub-periods 1978-1993, 1994-1998, 1999-2003 and 2004-2008, respectively for the G7 case; and 2 for all sub-periods for the Spanish case.

During the Pass-1, the link with the highest strength is selected first, its nodes becoming the starting nodes of the first Pass-1 network. Other links and their corresponding nodes are added to the graph using a breadth-first search on the strength of the links (i.e., the strongest link connecting a node that is not in any graph to the graph being constructed is added first), until there are no more links that exceed the co-occurrence threshold, or a maximum Pass-1 link limit is exceeded. The next network is generated in a similar manner starting with the link with the highest strength that is not in any existing graph.

Networks are interconnected by Pass-2 links. The _centrality_ of a network measures the degree of interaction to other networks (Callon _et al._ 1991) and it can be defined as:

<figure>

![Centrality equation](../centrality.png)</figure>

with _k_ a keyword belonging to the theme and _h_ a keyword belonging to other themes.

The _density_ of a network measures the internal strength of the network (Callon _et al._ 1991) and it can be defined as:

<figure>

![Density equation](../density.png)</figure>

with _i_ and _j_ keywords belonging to the theme, and _w_ being the number of keywords in the theme.

_Isolated Networks_ are those that have low centrality values. _Principal Networks_ are those that have high centrality and high density values, (for more detail see Callon _et al._ 1991).

The fifth step is the positioning of items in a low-dimensional space based on the similarities calculated in the fourth step ([Börner _et al._ 2003](#borner2003)). In this paper, the low-dimensional space is referred to as a _keywords based map_ and only two-dimensional keywords based maps are considered. The two dimensions are centrality rank (_cr_) and density rank (_dr_), calculated as:

<figure>

![Centrality and density rank equation](../centralityAndDensityRank.png)</figure>

where _rank<sub>i</sub><sup>c</sup>_ is the position of the theme _i_ in the theme list in ascending sort of centrality, and _rank<sub>i</sub><sup>d</sup>_ is the position of the theme _i_ in the theme list in ascending sort of density. _N_ is the number of themes in the whole network. _N_ is introduced to normalize in [0,1] the centrality rank and density rank values.

The sixth step is the visualization of the low-dimensional space that results from the fifth step ([Börner _et al._ 2003](#borner2003)). In our study, we use CoPalRed computer program, which is briefly described in Subsection The [_The CoPalRed software_](#section-2.2). CoPalRed visualizes the networks in a strategic diagram. A strategic diagram is a two-dimensional space in which themes are localized in strategic positions using the two measures, density rank (values in the ordinate axis) and centrality rank (values in the abscissa axis) (for more detail see [Callon _et al._ 1991](#callon1991)). The abscissa axis is associated to centrality, or the external cohesion index. It represents the most or least central position of a theme within the overall network. The ordinate axis is associated to density, or the index of internal cohesion. It represents the conceptual development of a theme.

Four areas of interest (or quadrants) can be defined in a strategic diagram (see Figure 3). Each quadrant has an associated theoretical meaning. The meaning of the four quadrants is:

*   Quadrant 1 (upper-right): groups the _motor-themes_ of the specialty, given that it presents strong centrality and high density.
*   Quadrant 2 (upper-left): in this quadrant we find very specialised but peripheral themes.
*   Quadrant 3 (lower-left): collects themes with low density and centrality and so mainly represents either emerging or disappearing themes.
*   Quadrant 4 (lower-right): the transversal and the most general basic themes are localized in this quadrant, although with internal development not as high as those of quadrant 1.

<table>

<tbody>

<tr>

<td>

![Figure 3: Quadrants in a strategic diagram](../p421fig3.png)</td>

<td>

![Figure 4: An example of strategic diagram](../p421fig4.png)</td>

</tr>

<tr>

<td>

**Figure 3: Quadrants in a strategic diagram.**</td>

<td>

**Figure 4: An example of strategic diagram.**</td>

</tr>

</tbody>

</table>

CoPalRed visualizes a keywords-based map by displaying a sphere for each theme. This sphere indicates the location of the theme in the strategic diagram. The spheres can group and show representative information of the themes. In this paper, that information includes the name of the themes (identified by the most central keyword of the theme) and the number of associated papers. To do that, CoPalRed assumes a paper belongs to a theme when it presents at least 2 keywords of the theme.

As example, in the strategic diagram in Figure 4, the themes A, B and C are presented. The placement of theme B implies that this theme is quite related externally to concepts applicable to other themes that are conceptually closely related. In addition, the internal cohesion is also rather strong, and therefore we can consider B to be a _motor-theme_ of the studied scientific field.

### <a id="section-2.2"></a>The CoPalRed software

In this paper the CoPalRed software (CoPalRed 2005) is used to generate the bibliometric maps.

CoPalRed has been successfully used to describe the field of physical chemistry of surfactants in ([Bailón-Moreno _et al._ 2005](#bailon2005), [Bailón-Moreno _et al._ 2006](#bailon2006)).

CoPalRed performs three kind of analysis: structural analysis, strategic analysis and dynamic analysis.

*   **Structural Analysis**, shows the analysis in the form of thematic networks in which words and their relationships are drawn. An example of thematic network is drawn in Figure 5\. In it several keywords are interconnected, where the volume of the spheres is proportional to the number of documents corresponding to each keyword, the thickness of the link between two spheres _i_ and _j_ is proportional to the equivalence index _e<sub>ij</sub>_.
*   **Strategic Analysis**, places each thematic network in a relative position within the global thematic network, using two criteria: centrality (or intensity of its external relations) and density (according to their internal cohesion density). An example of strategic diagram is shown in Figure 4.
*   **Dynamic Analysis**. CoPalRed analyses the transformations of the thematic networks over time. It identifies approaches, bifurcations, appearances and disappearances of themes.

<figure>

![Figure 5: Example of thematic network.](../p421fig5.png)

<figcaption>

**Figure 5: Example of thematic network.**</figcaption>

</figure>

## <a id="section-3"></a>Bibliometric study on fuzzy set theory research in Spain

This section analyses the evolution of the fuzzy set theory field over recent years. First, the evolution of the field in Spain is studied (including the analysis of the fuzzy sets research carried out by the G7 countries and a comparison between both Spanish and International cases). Then, a snapshot of the visibility of the Spanish fuzzy community in the ISI Web of Science is presented. Finally, the importance of the Spanish community in the fuzzy set theory field is shown.

### <a id="section-3.1"></a>Evolution of Spanish fuzzy set theory research

To analyse the evolution of the fuzzy set theory field over the last years in Spain, strategic diagrams for the four studied sub-periods are shown in Figure 6\. The volume of the spheres is proportional to the number of documents corresponding to each theme in each sub-period (a number is used to indicate the papers per theme). In the following the four sub-periods are described.

In the first studied sub-period (1978-1993), the longer one, in which there are barely enough papers (134) to get any important result, CoPalRed highlights three studied themes: _connectives_ (five papers), _fuzzy-measure_ (five papers) and _quantity_ (four papers).

In the second sub-period (1994-1998), with 199 papers, CoPalRed shows nine themes, where _linguistic-preference-relations_ (six papers) (the most central and dense theme), _linguistic-modelling_ (six papers), _fuzzy-logic_ (nine papers) and _fuzzy-control_ (eight papers) were the four most studied themes.

<table><caption>

**Figure 6: Strategic diagrams for each sub-period.**</caption>

<tbody>

<tr>

<td>

![(a) 1978-1993](../p421fig6a.png)</td>

<td>

![(b) 1994-1998](../p421fig6b.png)</td>

</tr>

<tr>

<td>

**(a) 1978-1993.**</td>

<td>

**(b) 1994-1998.**</td>

</tr>

<tr>

<td>

![(c) 1999-2003.](../p421fig6c.png)</td>

<td>

![(d) 2004-2008.](../p421fig6d.png)</td>

</tr>

<tr>

<td>

**(c) 1999-2003.**</td>

<td>

**(d) 2004-2008.**</td>

</tr>

</tbody>

</table>

From 1999, a significative increment in published papers is observed, which allows us to carry out a more detail analysis. In the last decade (1999-2008) more themes and bigger ones are detected (see Figure 6, specially 6c and 6d).

From 1999-2003, the three most studied themes by the Spanish fuzzy sets research community were: _genetic-algorithms_ (twenty-two papers), _neural-networks_ (fifteen papers) and _OWA-operators_ (fourteen papers). The themes _OWA-operators_ and _genetic-algorithms_ were the _motor-themes_ of the sub-period (see Figure 6c).

From 2004 to 2008 the three most studied themes were the basic and transversal theme _fuzzy-sets_ (with twenty-two papers), following by the most applied themes _fuzzy-control_ (with twenty-three papers) and _group-decision-making_ (twenty-one papers) (see Figure 6d).

From Figure 6 we can reach several conclusions in the evolution of the most long-lasting themes in the fuzzy sets research in the Spanish community:

*   _Fuzzy-control_ has increased its internal cohesion (its density) in the last five years (2004-2008). It indicates that the theme has been more studied and better developed (twenty-three vs. seven papers in the previous sub-period).
*   _Genetic-algorithms_ has consolidated in the upper-right quadrant. The number of published papers has decreased and the theme has become in a more central, basic and transversal one (see Figures 6c and 6d).
*   The theme _group-decision-making_ has showed up as a main theme (see Figure 7b). Previously, this was a minor topic related to other themes, such as _linguistic-preference-relations_ in the studied sub-period 1994-1998 (see Figure 7a). The strategic position of group-decision-making in the upper-right quadrant indicates currently it is a _motor-theme_ (see Figure 6d).
*   The negative movement of _fuzzy-numbers_, from a position on the middle of the strategic diagram for sub-period 1994-1998 to the lower-left quadrant in 2004-2008, suggests that the theme is about to be discarded, as a research topic, by the Spanish community.
*   Very specific themes have been also detected in the last sub-period; for example, _olive-tree-trimming_, which has been supported by six papers.

<table><caption>

**Figure 7: _Group-decision-making_ thematic networks.**</caption>

<tbody>

<tr>

<td>

![(a) 1994-1998](../p421fig7a.png)</td>

<td>

![(b) 2004-2008](../p421fig7b.png)</td>

</tr>

<tr>

<td>

**(a) 1994-1998.**</td>

<td>

**(b) 2004-2008**</td>

</tr>

</tbody>

</table>

As summary, in [Table 1](#tab-1), the number of papers for each theme in each sub-period is shown. Columns 2 to 5 indicate the number of papers with the theme for each sub-period. Note: CoPalRed assumes a paper belongs to a theme when it presents at least two keywords of the theme.

<table id="tab-1"><caption>

**Table 1: Evolution of themes in number of papers published by Spanish authors.**</caption>

<tbody>

<tr>

<th>Theme</th>

<th>1978-1993  
(134 papers)</th>

<th>1994-1998  
(199 papers)</th>

<th>1999-2003  
(421 papers)</th>

<th>2004-2008  
(656 papers)</th>

</tr>

<tr>

<td>Clustering</td>

<td> </td>

<td> </td>

<td>5</td>

<td> </td>

</tr>

<tr>

<td>Connectives</td>

<td>5</td>

<td>3</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Controller</td>

<td> </td>

<td> </td>

<td> </td>

<td>7</td>

</tr>

<tr>

<td>Data-mining</td>

<td> </td>

<td> </td>

<td> </td>

<td>4</td>

</tr>

<tr>

<td>Design</td>

<td> </td>

<td> </td>

<td> </td>

<td>16</td>

</tr>

<tr>

<td>Dynamics</td>

<td> </td>

<td> </td>

<td> </td>

<td>10</td>

</tr>

<tr>

<td>Function-approximation</td>

<td> </td>

<td> </td>

<td> </td>

<td>5</td>

</tr>

<tr>

<td>Fuzzy-artmap</td>

<td> </td>

<td> </td>

<td>10</td>

<td> </td>

</tr>

<tr>

<td>Fuzzy-clustering</td>

<td> </td>

<td>4</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Fuzzy-control</td>

<td> </td>

<td>7</td>

<td>7</td>

<td>23</td>

</tr>

<tr>

<td>Fuzzy-measure</td>

<td>5</td>

<td>6</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Fuzzy-numbers</td>

<td> </td>

<td>5</td>

<td>10</td>

<td>6</td>

</tr>

<tr>

<td>Fuzzy-logic</td>

<td> </td>

<td>9</td>

<td> </td>

<td>15</td>

</tr>

<tr>

<td>Fuzzy-random-variable</td>

<td> </td>

<td> </td>

<td> </td>

<td>14</td>

</tr>

<tr>

<td>Fuzzy-sets</td>

<td> </td>

<td> </td>

<td>11</td>

<td>22</td>

</tr>

<tr>

<td>Fuzzy-systems</td>

<td> </td>

<td> </td>

<td> </td>

<td>14</td>

</tr>

<tr>

<td>Genetics-algorithms</td>

<td> </td>

<td> </td>

<td>22</td>

<td>14</td>

</tr>

<tr>

<td>Group-decision-making</td>

<td> </td>

<td> </td>

<td> </td>

<td>21</td>

</tr>

<tr>

<td>Inference</td>

<td> </td>

<td> </td>

<td>12</td>

<td> </td>

</tr>

<tr>

<td>Intuitionistic-fuzzy-sets</td>

<td> </td>

<td>4</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Linguistic-modeling</td>

<td> </td>

<td>5</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Linguistic-preference-relations</td>

<td> </td>

<td>6</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Logic</td>

<td> </td>

<td> </td>

<td> </td>

<td>10</td>

</tr>

<tr>

<td>Navigation</td>

<td> </td>

<td> </td>

<td> </td>

<td>6</td>

</tr>

<tr>

<td>Networks</td>

<td> </td>

<td> </td>

<td> </td>

<td>9</td>

</tr>

<tr>

<td>Neural-networks</td>

<td> </td>

<td> </td>

<td>15</td>

<td>10</td>

</tr>

<tr>

<td>Olive-tree-trimmings</td>

<td> </td>

<td> </td>

<td> </td>

<td>6</td>

</tr>

<tr>

<td>Operators</td>

<td> </td>

<td> </td>

<td> </td>

<td>9</td>

</tr>

<tr>

<td>OWA-operators</td>

<td> </td>

<td> </td>

<td>14</td>

<td> </td>

</tr>

<tr>

<td>Pattern-recognition</td>

<td> </td>

<td> </td>

<td> </td>

<td>11</td>

</tr>

<tr>

<td>Quatity</td>

<td>4</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Rules</td>

<td> </td>

<td> </td>

<td>9</td>

<td>2</td>

</tr>

<tr>

<td>Systems</td>

<td> </td>

<td>8</td>

<td> </td>

<td>34</td>

</tr>

<tr>

<td>T-Conorm</td>

<td> </td>

<td>3</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>T-Norm</td>

<td> </td>

<td> </td>

<td> </td>

<td>11</td>

</tr>

<tr>

<td>Vector-median-filter</td>

<td> </td>

<td> </td>

<td> </td>

<td>7</td>

</tr>

<tr>

<td>Wave-functions</td>

<td> </td>

<td> </td>

<td> </td>

<td>8</td>

</tr>

</tbody>

</table>

To complete the description of the evolution of the fuzzy sets based research in Spain, in [Appendix A](#appendice-a), Figures [13](#fig-13), [14](#fig-14), [15](#fig-15) and [16](#fig-16), show the complete relations among keywords and themes for each sub-period. In these networks, the volume of the spheres is proportional to the number of documents corresponding to each keyword, the thickness of the link between two spheres _i_ and _j_ is proportional to the equivalence index _e<sub>ij</sub>_. Keywords belonging to a theme are labelled with the same number and each number represents a theme. Different numbers are used in each sub-period. Only the strongest relations are shown.

#### <a id="section-3.1.1"></a>Evolution of international fuzzy set theory research

To analyse the recent evolution of the fuzzy set theory field in the above mentioned G7 countries, strategic diagrams for the four studied sub-periods (1965-1993, 1994-1998, 1999-2003 and 2004-2008) are shown in Figure 8\. The volume of the spheres is proportional to the number of documents corresponding to each theme in each sub-period (a number is used to indicate the papers per theme). In the following the four sub-periods are described.

<table><caption>

**Figure 8: Strategic diagrams for each sub-period (G7 group).**</caption>

<tbody>

<tr>

<td>

![(a) 1965-1993](../p421fig8a.png)</td>

<td>

![(b) 1994-1998](../p421fig8b.png)</td>

</tr>

<tr>

<td>

**(a) 1965-1993.**</td>

<td>

**(b) 1994-1998.**</td>

</tr>

<tr>

<td>

![(b) 1999-2003](../p421fig8c.png)</td>

<td>

![(b) 2004-2008](../p421fig8d.png)</td>

</tr>

<tr>

<td>

**(c) 1999-2003.**</td>

<td>

**(d) 2004-2008**</td>

</tr>

</tbody>

</table>

In the first sub-period (1965-1993), the longer one, in which 2,397 papers were published, the most important themes in the international context (Considering the countries: USA, Canada, UK, Germany, Spain, Japan and Peoples Republic of China), in relative weight in terms of number of documents, were: _uncertainty_ and _expert-systems_ (both with thirty-eight papers), _applications_ (twenty-three papers) and _decision-making_ (with twenty papers) (see Figure 8a). Because of their strategic situation (lower-right quadrant), _uncertainty_ and _expert-systems_ and _decision-making_ are considered as general basic themes, with high centrality, although with low internal development. In this first sub-period, the most central theme was _uncertainty_, the fuzzy set theory revolve around this concept.

In the second period (1994-1998), with 3,474 papers, _fuzzy-logic_ (ninety-three papers), _adaptive-resonance-theory_ (61 papers), _connectives_ (48 papers), were the three most studied themes.

In the last decade, from 1999 to 2008, a significant increment in published papers is observed (see Figure 2). In these years (sub-periods 1999-2003 and 2004-2008) the number of interest topics of the international fuzzy community was augmented, and a set of different themes has been observed by CoPalRed (see Figure 8).

From 1999-2003, the most studied themes were: _neural-networks_ (423 papers), _genetic-algorithms_ (157 papers), _non-linear-systems_ (120 papers), _decision-making_ (105 papers) and _expert-systems_ (54 papers). All of them were strategically localized in the right quadrants (with high centrality indexes) of the strategic diagram for this sub-period, i.e., they were quite related externally to concepts applicable to other themes that were conceptually closely related. In this sub-period, the most central and dense theme was _non-linear-systems_, and due to its strategic localization it was considered as the _motor-theme_ of the sub-period.

From 2004 to 2008 the studied themes were those shown in the strategic diagram of Figure 8d. In this last sub-period, the principal themes, in number of papers, were _neural-networks_ (392 papers), _design_ (288 papers), _linear-matrix-inequality_ (204 papers), _uncertainty_ (152 papers), _fuzzy-neural-networks_ (95 papers) and _group-decision-making_ (84 papers). In this last sub-period, the theme _linear-matrix-inequality_ was considered as the _motor-theme_ of the sub-period.

From Figure 8 we can appreciate several conclusions in the evolution of the most long-lasting themes in the fuzzy sets based research in the G7 group:

*   Basic and precursor themes as _uncertainty_ have always been important research topics. In Figures 8a and 8d we can see as _uncertainty_ has maintained its basic and traversal feature (lower-right quadrant).
*   The themes _decision-making_ and _group-decision-making_ are ones of the most long-lasting applied themes.
*   _Decision-making_, which was positioned in the lower-right quadrant, with high centrality and very low density (in the sub-period 1965-1993), increased its density and centrality in the lustrum 1999-2003\. It can be seen in Figures 8a and 8c.
*   The theme _group-decision-making_, which was in a very peripheral localization (upper-left quadrant) in 1994-1998 has become in a _motor-theme_ in the last five years (2004-2008).
*   The theme _genetic-algorithms_ consolidated, in the decade 1994-2003, as an important theme in the fuzzy sets research (see Figures 8b and 8c). It increased values of both centrality and density to become one of the most central and studied themes (with 157 papers it was the second most studied theme in 1999-2003). In the last five years (sub-period 2004-2008), this theme has been absorbed by the theme _eural-networks_ (as we can see in the thematic network in Figure 9).
*   The negative movement (to the lower-left quadrant) of the theme _expert-systems_ in the last ten years (1999-2008) is very surprising. _Expert-systems_ was one of the most studied themes in the first sub-period (1965-1993). This decline can be seen in more detail in Figures 8a, 8c and 8d.
*   It is very interesting to see as very recent theories and methods, such as _type-2-fuzzy-sets_ (whose first papers date from the late 1990s) are still considered as very peripheral and specific topics by the international fuzzy sets research community (see Figure 8d).

<figure>

![Figure 9: Neural-Networks thematic network (sub-period 2004-2008)](../p421fig9.png)

<figcaption>

**Figure 9: _Neural-Networks_ thematic network (sub-period 2004-2008).**</figcaption>

</figure>

To complete the description of the evolution of fuzzy sets research in the G7 group, in [Appendix A](#appendice-a), Figures [17](#fig-17), [18](#fig-18), [19](#fig-19) and [20](#fig-20), the whole relations among keywords and themes for each sub-period are shown. In these whole networks, the volume of the spheres is proportional to the number of documents corresponding to each keyword, the thickness of the link between two spheres _i_ and _j_ is proportional to the equivalence index _e<sub>ij</sub>_. Keywords belonging to a theme are labelled with the same number and each number represents a theme. Different numbers are used in each sub-period. Only the strongest relations are shown.

#### <a id="section-3.1.2"></a>Spanish vs international fuzzy set theory research

In this subsection, we show some conclusions about the fuzzy sets research carried out by the Spanish community compared with that of the seven studied countries (the G7 group).

From 1994-1998, whereas in the G7 group, the theme _connectives_ (with forty-eight papers) was the most central theme (upper-right quadrant), in the Spanish research, _connectives_ was situated in the lower-left quadrant (with just three papers). In the G7 group, the most studied theme was _fuzzy-logic_ (with ninety-three papers), in the Spanish community just nine papers were published on this topic (see Figures 8b and 6b).

From 1999-2003, the theme _neural-networks_ was supported by 423 papers in the G7 group, whereas in the Spanish community it received only fifteen papers. In addition, _neural-networks_ in the G7 group, was better located (lower-right quadrant). Another important theme in this sub-period was _genetic-algorithms_, which was located in both cases in the upper-right quadrant. In both case, it was one of the most studied themes in the sub-period.

In the last five years (2004-2008), more similarities can be remarked upon. For example, the themes _fuzzy-random-variable_, _group-decision-making_, _neural-networks_, _fuzzy-systems_ and _rules_ were located in the same positions (in the same quadrants). In both the Spanish and the international communities the same _motor-theme_ was treated (_group-decision-making_). In Figure 10 we compare the thematic networks of the _group-decision-making_ theme. In it we can see there is almost no difference between both thematic networks, so it is possible to conclude that both communities are working on this theme, in the same way.

<table><caption>

**Figure 10: _Group-decision-making_ thematic networks (sub-period 2004-2008).**</caption>

<tbody>

<tr>

<td>

![(a) Spanish case](../p421fig10a.png)</td>

<td>

![(b) G7 case](../p421fig10b.png)</td>

</tr>

<tr>

<td>

**(a) Spanish case.**</td>

<td>

**(b) G7 case.**</td>

</tr>

</tbody>

</table>

### <a id="section-3.2"></a>Visibility of Spanish fuzzy set theory research in [ISI Web of Science](http://scientific.thomson.com/products/wos/)

The ISI Web of Science provides access to current and retrospective multidisciplinary information from approximately 8,700 of the most prestigious, high-impact research journals in the world. It also provides a unique search method, cited reference searching. With it, users can navigate forward and backward through the literature, searching all disciplines and time spans to uncover information relevant to their research. Users can also navigate to electronic full-text journal articles. In the link 'Advanced Search', we consider the query number 1 (see Section [_Methods_](#section-2)), where only the Spanish papers were considered.

The query resulted in 1,442 papers, which, collectively, had received 13,843 citations, or 9.60 citations for each paper.

In Figure 11 we observe an increasing number of publications each year with more than 100 papers a year in recent years. In Figure 12 we observe that the number of citations shows a similar increasing trend in recent years. All these data can allow us to say the field of fuzzy set theory has now reached a stage of maturity after the earliest papers published at 1978; there are also many basic issues yet to be resolved and there is an active and vibrant worldwide community of researchers working on these issues.

<figure id="fig-11">

![Figure 11: Number of published papers per year in Spain (ISI Web of Science)](../p421fig11.png)

<figcaption>

**Figure 11: Number of published papers a year in Spain (ISI Web of Science).**</figcaption>

</figure>

<figure id="fig-12">

![Figure 12: Number of citations per year in Spain (ISI Web of Science)](../p421fig12.png)

<figcaption>

**Figure 12: Number of citations a year in Spain (ISI Web of Science).**</figcaption>

</figure>

Doing a more detailed analysis, in Table 2, below, we show the most productive Spanish institutions (according to ISI Web of Science). In them we can distinguish the _University of Granada_, _University of Oviedo_, _University Complutense of Madrid_, _Consejo Superior de Investigaciones Científicas (CSIC)_ and _Polytechnic University of Madrid_ as the five most productive institutions in Spain. In [Appendix A](#appendice-a), in Tables 5, 6, 7 and 8 more detailed information is shown for each sub-period.

<table><caption>

**Table 2: The twenty most productive Spanish institutions from 1978 to 2008.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of Papers</th>

</tr>

<tr>

<td>UNIV GRANADA</td>

<td>295</td>

</tr>

<tr>

<td>UNIV OVIEDO</td>

<td>142</td>

</tr>

<tr>

<td>UNIV COMPLUTENSE MADRID</td>

<td>77</td>

</tr>

<tr>

<td>CSIC</td>

<td>76</td>

</tr>

<tr>

<td>UNIV POLITECN MADRID</td>

<td>69</td>

</tr>

<tr>

<td>UNIV POLITECN CATALUNYA</td>

<td>62</td>

</tr>

<tr>

<td>UNIV JAEN</td>

<td>53</td>

</tr>

<tr>

<td>UNIV ROVIRA & VIRGILI</td>

<td>53</td>

</tr>

<tr>

<td>UNIV SANTIAGO DE COMPOSTELA</td>

<td>50</td>

</tr>

<tr>

<td>UNIV POLITECN VALENCIA</td>

<td>44</td>

</tr>

<tr>

<td>UNIV MURCIA</td>

<td>38</td>

</tr>

<tr>

<td>UNIV PUBL NAVARRA</td>

<td>38</td>

</tr>

<tr>

<td>UNIV PAIS VASCO</td>

<td>37</td>

</tr>

<tr>

<td>UNIV VALLADOLID</td>

<td>37</td>

</tr>

<tr>

<td>UNIV MALAGA</td>

<td>36</td>

</tr>

<tr>

<td>UNIV VALENCIA</td>

<td>29</td>

</tr>

<tr>

<td>UNIV GIRONA</td>

<td>23</td>

</tr>

<tr>

<td>UNIV SEVILLA</td>

<td>22</td>

</tr>

<tr>

<td>UNIV CARLOS III MADRID</td>

<td>21</td>

</tr>

<tr>

<td>UNIV CORDOBA</td>

<td>21</td>

</tr>

</tbody>

</table>

In [Appendix B](#appendice-b), a study of the pioneering and the most cited papers in Spanish fuzzy community is presented.

### <a id="section-3.3"></a>Importance of Spanish fuzzy sets research community

Spain is one of the most active countries researching on fuzzy set theory; it is in seventh position in the ranking of the top ten most productive countries (as it could be observed in the ISI Web of Science using query number 1 on 7th January 2009). Spain is also one countries working on the topic for the longest period of time. Its first paper is from 1978, whereas other important countries on the topic published their first papers some years after, e.g., _Peoples Republic of China_ (1980) or _Taiwan_ (1983).

With respect to the institutions researching fuzzy set theory, in Table 3 below we can see that two Spanish institutions are in the top-twenty of the most productive. One of the Spanish institutions, the _University of Granada_, is the most active institution researching on the topic in the G7 group. Two Spanish institutions (_University of Granada_ and _University of Oviedo_) were always active on the topic, as can be observed in [Appendix A](#appendice-b) in Tables 9, 10, 11 and 2\. In spite of Spain having published its first paper thirteen years after the first paper on the topic was published (1965), one Spanish institution reached seventh position of the most productive institutions in the first studied sub-period (1965-1993) (see [Table 9](#tab-9) in [Appendix A](#appendice-a)). In addition, the _University of Granada_ and the _University of Oviedo_ were two of the three European institutions in those rankings (including the _University of Sheffield_).

<table><caption>

**Table 3: The twenty most productive internationals institutions from 1965 to 2008.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Country</th>

<th>Numbers of papers</th>

</tr>

<tr>

<td>UNIV GRANADA</td>

<td>Spain</td>

<td>295</td>

</tr>

<tr>

<td>CITY UNIV HONG KONG</td>

<td>Peoples Republic of China</td>

<td>256</td>

</tr>

<tr>

<td>HONG KONG POLYTECH UNIV</td>

<td>Peoples Republic of China</td>

<td>243</td>

</tr>

<tr>

<td>IONA COLL</td>

<td>USA</td>

<td>209</td>

</tr>

<tr>

<td>UNIV ALBERTA</td>

<td>Canada</td>

<td>209</td>

</tr>

<tr>

<td>TSING HUA UNIV</td>

<td>Peoples Republic of China</td>

<td>207</td>

</tr>

<tr>

<td>UNIV ALABAMA</td>

<td>USA</td>

<td>181</td>

</tr>

<tr>

<td>SHANGHAI JIAO TONG UNIV</td>

<td>Peoples Republic of China</td>

<td>176</td>

</tr>

<tr>

<td>CHINESE ACAD SCI</td>

<td>Peoples Republic of China</td>

<td>162</td>

</tr>

<tr>

<td>HARBIN INST TECHNOL</td>

<td>Peoples Republic of China</td>

<td>160</td>

</tr>

<tr>

<td>UNIV TEXAS</td>

<td>USA</td>

<td>149</td>

</tr>

<tr>

<td>UNIV CALIF BERKELEY</td>

<td>USA</td>

<td>144</td>

</tr>

<tr>

<td>UNIV OVIEDO</td>

<td>Spain</td>

<td>142</td>

</tr>

<tr>

<td>TOKYO INST TECHNOL</td>

<td>Japan</td>

<td>137</td>

</tr>

<tr>

<td>PURDUE UNIV</td>

<td>USA</td>

<td>135</td>

</tr>

<tr>

<td>UNIV S FLORIDA</td>

<td>USA</td>

<td>131</td>

</tr>

<tr>

<td>UNIV MISSOURI</td>

<td>USA</td>

<td>130</td>

</tr>

<tr>

<td>UNIV SHEFFIELD</td>

<td>England (UK)</td>

<td>121</td>

</tr>

<tr>

<td>UNIV WATERLOO</td>

<td>Canada</td>

<td>121</td>

</tr>

<tr>

<td>UNIV TORONTO</td>

<td>Canada</td>

<td>120</td>

</tr>

</tbody>

</table>

A deeper study shows us proof that Spanish institutions are leading, in recent years, some _motor-themes_ as for example _group-decision-making_. Thus, twenty-three of the eighty-four published papers treating some aspect of this theme were published by Spanish researchers (i.e., about 27%), and 310 of 820 citations received by the theme were citations to Spanish papers, i.e., almost 38% of received citations. The eight most cited papers on this topic ([Herrera-Viedma _et al._ 2004](#herreraviedma2004), [Herrera-Viedma _et al._ 2005](#herreraviedma2005), [Yager 2004](#yager2004), [Xu 2004](#xu2004), [Torra 2004](#torra2004), [Xu and Da 2005](#xu2005), [Xu and Chen 2007](#xu2007), [Wang _et al._ 2005](#wang2005)) are listed in [Table 4](#tab-4) (where only papers with more than twenty-five citations are included; citation data were retrieved from the ISI Web of Science on September 21, 2009). This list includes three Spanish papers.

With respect to other non-_motor-themes_, the significance of the Spanish community is observed as about 10% in both published papers and citations. Thus, for example, in _neural-network_, a general and basic theme, forty-four of the 392 published papers were contributed by Spanish researchers (i.e., about 11%), and 253 of 2,357 cites got by the theme were cites to Spanish papers (i.e., about 11%).

All these data demonstrate that the Spanish fuzzy set theory research community is a very important community in this field.

<table><caption>

**Table 4: The eight most cited papers in _group-decision-making_ for sub-period 2004-2008.**</caption>

<tbody>

<tr>

<th>Times cited</th>

<th>Publishing data & Institution / Country</th>

</tr>

<tr>

<td>92</td>

<td>

Herrera-Viedma, E., Herrera, F., Chiclana, F., Luque, M.  
[Some Issues on Consistency of Fuzzy Preference Relations.](#herreraviedma2004)  
EUROPEAN JOURNAL OF OPERATIONAL RESEARCH 2004, 154:1, pp. 98-109\.  
Univ Granada, Spain.</td>

</tr>

<tr>

<td>49</td>

<td>

Herrera-Viedma, E., Martinez, L., Mata, F., Chiclana, F.  
[A Consensus Support System Model for Group Decision-Making Problems With Multigranular Linguistic Preference Relations.](#herreraviedma2005)  
IEEE TRANSACTIONS ON FUZZY SYSTEMS 2005, 13:5, pp. 644-658.  
Univ Granada, Spain and Univ Jaen, Spain and De Montfort Univ, England (UK).</td>

</tr>

<tr>

<td>41</td>

<td>

Yager, R. R.  
[Owa Aggregation Over a Continuous Interval Argument With Applications to Decision Making.](#yager2004)
IEEE TRANSACTIONS ON SYSTEMS MAN AND CYBERNETICS PART B-CYBERNETICS 2004, 34:5, pp. 1952-1963.  
Iona Coll, USA.</td>

</tr>

<tr>

<td>40</td>

<td>

Xu, Z. S.  
[Goal Programming Models for Obtaining the Priority Vector of Incomplete Fuzzy Preference Relation.](#xu2004)  
INTERNATIONAL JOURNAL OF APPROXIMATE REASONING 2004, 36:3, pp. 261-270.  
Southeast Univ, Peoples R China.</td>

</tr>

<tr>

<td>30</td>

<td>

Torra, V.  
[Owa Operators in Data Modeling and Reidentification.](#torra2004)  
IEEE TRANSACTIONS ON FUZZY SYSTEMS 2004, 12:5, pp. 652-660.  
CSIC, Spain.</td>

</tr>

<tr>

<td>29</td>

<td>

Xu, Z. H., Da, Q. L.  
[A Least Deviation Method to Obtain a Priority Vector of a Fuzzy Preference Relation.](#xu2005)  
EUROPEAN JOURNAL OF OPERATIONAL RESEARCH 2005, 164:1, pp. 206-216.  
Southeast Univ, Peoples R China.</td>

</tr>

<tr>

<td>27</td>

<td>

Xu ZS, Chen J  
[An interactive method for fuzzy multiple attribute group decision making.](#xu2007)  
INFORMATION SCIENCES 2007, 117:1, pp. 248-263.  
Tsing Hua Univ, Sch Econ & Management, Dept Management Sci & Engn, Beijing, Peoples R China.</td>

</tr>

<tr>

<td>26</td>

<td>

Wang Y. M., Yang H. B., Xu D. L.  
[Two-Stage Logarithmic Goal Programming Method for Generating Weights From Interval Comparison Matrices.](#wang2005)  
FUZZY SETS AND SYSTEMS 2005, 152:3, pp. 475-498.  
Univ Manchester, England (UK) and Fuzhou Univ, Peoples R China.</td>

</tr>

</tbody>

</table>

## <a id="section-4"></a>Conclusions

In this paper, we have presented the first bibliometric study of fuzzy set theory research, focusing on the Spanish fuzzy sets research community. More than 16,344 original research papers have been processed. Based on this analysis, we have drawn the visual structure of fuzzy set theory research carried out by seven of the top-ten most productive countries on the topic.

The results also allow us to compare the Spanish community with the international community:

*   Similar trends have been detected on the evolution of the fuzzy set theory field in both the Spanish and international cases. Data show that both the Spanish and the G7 communities are interested in the same or similar themes, as shown in _Spanish vs. international fuzzy set theory research_. This fact can be demonstrated, for example, with the data in [Figure 10](#fig-10), which reveal the main keywords associated with the theme _group-decision-making_ and the links among them. It can be that the thematic networks in [Figure 10](#fig-10) maintain almost the same internal structure (same keywords, same links and same thickness of links).
*   The Spanish community is an important collective working on fuzzy set theory. Two Spanish institutions always were in high positions in the ranking of the top-twenty of the most productive international institutions (see data in Tables 3, 9, 10, 11 and 12). This fact is also supported by observing that Spanish papers are working (and leading in recent years) some outstanding themes, for example the motor-theme _group-decision-making_ (see Table 4).
*   The most notable topics on fuzzy set theory in both Spanish and international communities can see grouped in (or related to):
    *   theoretical aspects: _fuzzy-logic_, _connectives_, _uncertainty_ and _OWA-operators_.
    *   practical aspects: _group-decision-making_, _neural-networks_, and _design_.

Finally, we have to remark that the analysis is not without problems because of the bias implied. The most important caveat is that co-word analysis concentrates on priority themes and inevitably excludes those that have an anecdotal appearance. On the contrary, the analysis will legitimatize discussion about general tendencies accepted by the majority of the scientific community. So, experts and novices can use these results and maps to know the current situation of the fuzzy set theory field.

## Acknowledgements

This work has been supported by the Spanish project FUZZY-LING, Cod. TIN2007-61079, granted by the Spanish Agency for Education and Science (Ministerio de Educación y Ciencia).

## <a id="authors"></a>About the authors

Dr. A.G. López-Herrera is Assistant Professor of Computer Sciences and Artificial Intelligence at the University of Granada, Granada, Spain and member of CITIC-UGR. His research has focused on Soft Computing, information retrieval and informetrics. He can be contacted at [lopez-herrera@decsai.ugr.es](mailto:lopez-herrera@decsai.ugr.es) and [http://decsai.ugr.es/~agabriel](http://decsai.ugr.es/~agabriel).

Dr. M.J. Cobo is Ph. D. student in the program "Soft Computing and Intelligent Systems" of the University of Granada, Spain and member of CITIC-UGR. His research has focused in the analysis and evaluation of the evolution of scientific knowledge through text mining, graph mining and social networks. He can be contacted at [mjcobo@decsai.ugr.es](mailto:mjcobo@decsai.ugr.es).

Dr. E. Herrera-Viedma is Professor of Computer Sciences and Artificial Intelligence at the University of Granada, Granada, Spain and member of CITIC-UGR. His research has focused on soft computing, decision making, information retrieval, digital libraries, and informetrics. He can be contacted at [viedma@decsai.ugr.es](mailto:viedma@decsai.ugr.es) and [http://decsai.ugr.es/~viedma](http://decsai.ugr.es/~viedma).

Dr. F. Herrera is Professor of Computer Sciences and Artificial Intelligence at the University of Granada, Granada, Spain and member of CITIC-UGR. His research has focused on soft computing, decision making, data mining, and informetrics. He can be contacted at [herrera@decsai.ugr.es](mailto:herrera@decsai.ugr.es) and [http://decsai.ugr.es/~herrera](http://decsai.ugr.es/~herrera).

Dr. R. Bailón-Moreno is Professor of Chemical Engineering at the University of Granada, Granada, Spain. He investigates simultaneously in the Chemistry of Surfactants and Scientometrics. Contact: [bailonm@ugr.es](mailto:bailonm@ugr.es), [http://www.ugr.es/~bailonm](http://www.ugr.es/~bailonm).

Dr. E. Jiménez-Contreras is Professor of Bibliometrics at the Department of Library and Information Science at the University of Granada, Granada, Spain. His research has focused on different aspects of assessment of Science with quantitative methods. He can be contacted at [evaristo@ugr.es](mailto:evaristo@ugr.es). [http://ec3.ugr.es](http://ec3.ugr.es)

## References

*   <a id="alsina1983"></a>Alsina, C., Trillas, E. & Valverde, L. (1983). On some logical connectives for fuzzy-sets theory. _Journal of Mathematical Analysis and Applications_, **93**(1), 15-26.
*   <a id="bailon2006"></a>Bailón-Moreno, R., Jurado-Alameda, E. & Ruiz-Baños, R. (2006). The scientific network of surfactants: structural analysis. _Journal of the American Society for Information Science and Technology_, **57**(7), 949-960.
*   <a id="bailon2005"></a>Bailón-Moreno, R., Jurado-Alameda, E., Ruiz-Baños, R. & Courtial, J. (2005). Analysis of the scientific field of physical chemistry of surfactants with the unified scienctometric model. Fit of relational and activity indicators, _Scientometrics_, **63**(2), 259-276.
*   <a id="batle1979"></a>Batle, N. & Trillas, E. (1979). Entropy and fuzzy integral. _Journal of Mathematical Analysis and Applications_, **69**(2), 469-474.
*   <a id="borner2003"></a>Börner, K., Chen, C. & Boyack, K. (2003). [Visualizing knowledge domains](http://www.webcitation.org/5m2wPl9wN). _Annual Review of Information Science and Technology_, **37**, 179-255\. Retrieved 14 December, 2009 from http://www.cs.sandia.gov/projects/VxInsight/pubs/arist03.pdf (Archived by WebCite® at http://www.webcitation.org/5m2wPl9wN)
*   <a id="callon1991"></a>Callon, M., Courtial, J. & Laville, F. (1991). Co-word analysis as a tool for describing the network of interactions between basic and technological research - the case of polymer chemistry. _Scientometrics_ **22**(1), 155-205.
*   <a id="callon1983"></a>Callon, M., Courtial, J., Turner, W. & Bauin, S. (1983). From translations to problematic networks - an introduction to co-word analysis. _Social Science Information/Sur Les Sciences Socials_, **22**(2), 191-235.
*   <a id="castro1995"></a>Castro, J. L. (1995). Fuzzy-logic controllers are universal approximators. _IEEE Transactions on Systems Man and Cybernetics_, **25**(4), 629-635.
*   <a id="chiclana1998"></a>Chiclana, F., F., H. & E., H.-V. (1998). Integrating three representation models in fuzzy multipurpose decision making based on fuzzy preference relations. _Fuzzy Sets and Systems_, **97**(1), 33-48.
*   <a id="coulter1998"></a>Coulter, N., Monarch, I. & Konda, S. (1998). Software engineering as seen through its research literature: a study in co-word analysis. _Journal of the American Society for Information Science_, **49**(13), 1206-1223.
*   <a id="dubois1985"></a>Dubois, D. & Prade, H. (1985). _Possibility theory: an approach to computerized processing of uncertainty_. New York, NY: Plenum Press.
*   <a id="esteva2001"></a>Esteva, F. & Godo, L. (2001). Monoidal t-norm based logic: towards a logic for left-continuous t-norms. _Fuzzy Sets and Systems_, **124**(3), 271-288.
*   <a id="garfield1994"></a>Garfield, E. (1994). [Scientography: mapping the tracks of science](http://www.webcitation.org/5m2wwaZ1m). _Current Contents: Social & Behavioural Sciences_, **7**(45), 5-10\. Retrieved 14 December, 2009 from http://thomsonreuters.com/products_services/science/free/essays/scientography_mapping_science/ (Archived by WebCite® at http://www.webcitation.org/5m2wwaZ1m)
*   <a id="gil1984"></a>Gil, M., López, M. & Gil, P. (1984). Comparison between fuzzy information-systems. _Kybernetes_, **13**(4), 245-251.
*   <a id="herrera2000a"></a>Herrera, F. & Herrera-Viedma, E. (2000). Linguistic decision analysis: steps for solving decision problems under linguistic information. _Fuzzy Sets and Systems_, **115**(1), 67-82.
*   <a id="herrera2000b"></a>Herrera, F. & Martínez, L. (2000). A 2-tuple fuzzy linguistic representation model for computing with words. _IEEE Transactions on Fuzzy Systems_, **8**(6), 746-752.
*   <a id="herrera1996"></a>Herrera, F., Herrera-Viedma, E. & Verdegay, J. (1996). A model of consensus in group decision making under linguistic assessments. _Fuzzy Sets and Systems_, **79**(1), 73-87.
*   <a id="herreraviedma2005"></a>Herrera-Viedma E., Martínez L., Mata F., Chiclana F. (2005). A consensus support system model for group decision-making problems with multigranular linguistic preference relations. _IEEE Transactions on Fuzzy Systems_, **13**(5), 644-658.
*   <a id="herreraviedma2004"></a>Herrera-Viedma E., Herrera F., Chiclana F., Luque M. (2004). Some issues on consistency of fuzzy preference relations. _European Journal of Operational Research_, **154**(1), 98-109.
*   <a id="kahraman2006"></a>Kahraman, C., Murat G. & Kabak, Ö. (2006). Applications of fuzzy sets in industrial engineering: a topical classification. In Cengiz Kahraman, Murat Gülbay and F. Herrera (Eds.). _Fuzzy Applications in Industrial Engineering_. (pp. 1-55). Berlin: Springer. (Studies in Fuzziness and Soft Computing, Vol. 201),
*   <a id="klir1995"></a>Klir, G. & Yuan, B. (1995). _Fuzzy sets and fuzzy logic: theory and applications._ Englewood Cliffs, NJ: Prentice-Hall.
*   <a id="krsul1998"></a>Krsul, I.V. (1998). _Software vulnerability analysis_. Unpublished doctoral dissertation, Purdue University, West Lafayette, Indiana, USA.
*   <a id="leydesdorff2008"></a>Leydesdorff, L. & Zhou, P. (2008). Co-word analysis using the Chinese character set. _Journal of the American Society for Information Science and Technology_, **59**(9), 1528-1530.
*   <a id="michelet1988"></a>Michelet, B. (1988). _L'analyse des associations_. [The analysis of associations] Unpublished doctoral dissertation, University of Paris VII, Paris, France.
*   <a id="ollero1981"></a>Ollero, A. & Freire, E. (1981). The structure of relations in personnel-management. _Fuzzy Sets and Systems_, **5**(2), 115-125.
*   <a id="torra2004"></a>Torra V. (2004). OWA operators in data modeling and reidentification. _IEEE Transactions on Fuzzy Systems_, **12**(5), 652-660.
*   <a id="trillas1978"></a>Trillas, E. & Riera, T. (1978). Entropies in finite fuzzy sets. _Information Sciences_, **15**(2), 159-168.
*   <a id="valverde1985"></a>Valverde, L. (1985). On the structure of f-indistinguishability operators. _Fuzzy Sets and Systems_, **17**(3), 313-328.
*   <a id="verdegay1984"></a>Verdegay, J. (1984). A dual approach to solve the fuzzy linear-programming problem. _Fuzzy Sets and Systems_, **14**(2), 131-141.
*   <a id="vila1983a"></a>Vila, M. & Delgado, M. (1983a). On medical diagnosis using possibility measures. _Fuzzy Sets and Systems_, **10**(3), 211-222.
*   <a id="vila1983b"></a>Vila, M. & Delgado, M. (1983b). Problems of classification in a fuzzy environment. _Fuzzy Sets and Systems_, **9**(3), 229-239.
*   <a id="wang2005"></a>Wang Y. M., Yang H. B., Xu D.L. (2005). A two-stage logarithmic goal programming method for generating weights from interval comparison matrices. _Fuzzy Sets and Systems_, **152**(3), 475-498.
*   <a id="whittaker1989"></a>Whittaker, J. (1989). Creativity and conformity in science: titles, keywords, and co-word analysis. _Social Studies of Science_, **19**(3), 473-496.
*   <a id="xu2007"></a>Xu Z.S., Chen J. (2007). An interactive method for fuzzy multiple attribute group decision making. _Information Sciences_, **177**(1), 248-263.
*   <a id="xu2004"></a>Xu Z.S. (2004). Goal programming models for obtaining the priority vector of incomplete fuzzy preference relation. _International Journal of Approximate Reasoning_, **36**(3), 261-270.
*   <a id="xu2005"></a>Xu Z.H., Da Q.L. (2005). A least deviation method to obtain a priority vector of a fuzzy preference relation. _European Journal of Operational Research_, **164**(1), 206-216.
*   <a id="yager2004"></a>Yager R.R. (2004). OWA aggregation over a continuous interval argument with applications to decision making. _IEEE Transactions on Systems Man and Cybernetics Part B-Cybernetics_, **34**(5), 1952-1963.
*   <a id="zadeh1965"></a>Zadeh, L. (1965). Fuzzy sets. _Information and Control_, **8**(3), 338-353.
*   <a id="zadeh2008"></a>Zadeh, L. (2008). Is there a need for fuzzy logic?. _Information Sciences_, **178**(13), 2751-2779.
*   <a id="zhang2008"></a>Zhang, J., Wolfram, D., Wang, P., Hong, Y. & Gillis, R. (2008). Visualization of health-subject analysis based on query term co-occurrences. _Journal of the American Society for Information Science and Technology_, **59**(12), 1933-1947.
*   <a id="zimmermann2001"></a>Zimmermann, H. (2001). _Fuzzy sets theory and its applications_. Norwell, MA: Kluwer Academic.

* * *

## <a id="appendices"></a>Appendices

### <a id="appendice-a"></a>Appendix A

<figure id="fig-13">

![Figure 13: Spanish whole thematic network (Sub-period 1978-1993)](../p421fig13.png)

<figcaption>

**Figure 13: Spanish whole thematic network (Sub-period 1978-1993).**</figcaption>

</figure>

<figure id="fig-14">

![Figure 14: Spanish whole thematic network (Sub-period 1994-1998)](../p421fig14.png) 

<figcaption>

**Figure 14: Spanish whole thematic network (Sub-period 1994-1998).**</figcaption>

</figure>

<figure id="fig-15">

![Figure 15: Spanish whole thematic network (Sub-period 1999-2003)](../p421fig15.png)

<figcaption>

**Figure 15: Spanish whole thematic network (Sub-period 1999-2003).**</figcaption>

</figure>

<figure id="fig-16">

![Figure 16: Spanish whole thematic network (Sub-period 2004-2008)](../p421fig16.png)

<figcaption>

**Figure 16: Spanish whole thematic network (Sub-period 2004-2008).**</figcaption>

</figure>

<figure id="fig-17">

![Figure 17: G7 whole thematic network (Sub-period 1978-1993)](../p421fig17.png)

<figcaption>

**Figure 17: G7 whole thematic network (Sub-period 1978-1993).**</figcaption>

</figure>

<figure id="fig-18">

![Figure 18: G7 whole thematic network (Sub-period 1994-1998)](../p421fig18.png)

<figcaption>

**Figure 18: G7 whole thematic network (Sub-period 1994-1998)**</figcaption>

</figure>

<figure id="fig-19">

![Figure 19: G7 whole thematic network (Sub-period 1999-2003)](../p421fig19.png)

<figcaption>

**Figure 19: G7 whole thematic network (Sub-period 1999-2003).**</figcaption>

</figure>

<figure id="fig-20">

![Figure 20: G7 whole thematic network (Sub-period 2004-2008)](../p421fig20.png)

<figcaption>

**Figure 20: G7 whole thematic network (Sub-period 2004-2008)**</figcaption>

</figure>

<table id="tab-5"><caption>

**Table 5: The twenty most productive Spanish institutions in the sub-period 1978-1993.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of papers</th>

</tr>

<tr>

<td>UNIV GRANADA</td>

<td>39</td>

</tr>

<tr>

<td>UNIV OVIEDO</td>

<td>23</td>

</tr>

<tr>

<td>UNIV COMPLUTENSE MADRID</td>

<td>15</td>

</tr>

<tr>

<td>UNIV POLITECN MADRID</td>

<td>13</td>

</tr>

<tr>

<td>UNIV POLITECN CATALUNYA</td>

<td>11</td>

</tr>

<tr>

<td>UNIV SANTIAGO DE COMPOSTELA</td>

<td>10</td>

</tr>

<tr>

<td>UNIV PAIS VASCO</td>

<td>9</td>

</tr>

<tr>

<td>UNIV NACL EDUC DISTANCIA</td>

<td>4</td>

</tr>

<tr>

<td>UNIV SANTIAGO</td>

<td>3</td>

</tr>

<tr>

<td>UNIV AUTONOMA MADRID</td>

<td>2</td>

</tr>

<tr>

<td>UNIV ILLES BALEARS</td>

<td>2</td>

</tr>

<tr>

<td>UNIV MURCIA</td>

<td>2</td>

</tr>

<tr>

<td>UNIV POLITECN BARCELONA</td>

<td>2</td>

</tr>

<tr>

<td>CLIN INFANTIL LA PAZ</td>

<td>1</td>

</tr>

<tr>

<td>COLEGIO UNIV SEGOVIA</td>

<td>1</td>

</tr>

<tr>

<td>CSIC</td>

<td>1</td>

</tr>

<tr>

<td>CTR ESTUDIS AVANCATS BLANES</td>

<td>1</td>

</tr>

<tr>

<td>ESCUELA TECH SUPER ARQUITECTURA BARCELONA</td>

<td>1</td>

</tr>

<tr>

<td>EU POLITECN CORDOBA</td>

<td>1</td>

</tr>

<tr>

<td>FAC INFORMAT BARCELONA</td>

<td>1</td>

</tr>

</tbody>

</table>

<table id="tab-6"><caption>

**Table 6: The twenty most productive Spanish institutions in the sub-period 1994-1998.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of papers</th>

</tr>

<tr>

<td>UNIV GRANADA</td>

<td>62</td>

</tr>

<tr>

<td>CSIC</td>

<td>16</td>

</tr>

<tr>

<td>UNIV POLITECN MADRID</td>

<td>16</td>

</tr>

<tr>

<td>UNIV COMPLUTENSE MADRID</td>

<td>12</td>

</tr>

<tr>

<td>UNIV MURCIA</td>

<td>11</td>

</tr>

<tr>

<td>UNIV OVIEDO</td>

<td>11</td>

</tr>

<tr>

<td>PUBL UNIV NAVARRA</td>

<td>10</td>

</tr>

<tr>

<td>UNIV MALAGA</td>

<td>7</td>

</tr>

<tr>

<td>UNIV POLITECN CATALUNYA</td>

<td>6</td>

</tr>

<tr>

<td>UNIV SEVILLA</td>

<td>6</td>

</tr>

<tr>

<td>UNIV AUTONOMA BARCELONA</td>

<td>5</td>

</tr>

<tr>

<td>UNIV BASQUE COUNTRY</td>

<td>4</td>

</tr>

<tr>

<td>UNIV ROVIRA & VIRGILI</td>

<td>4</td>

</tr>

<tr>

<td>UNIV SANTIAGO DE COMPOSTELA</td>

<td>4</td>

</tr>

<tr>

<td>ESCUELA SUPER INGN</td>

<td>3</td>

</tr>

<tr>

<td>UNIV BARCELONA</td>

<td>3</td>

</tr>

<tr>

<td>UNIV PAIS VASCO</td>

<td>3</td>

</tr>

<tr>

<td>ETS ARQUITECTURA BARCELONA</td>

<td>2</td>

</tr>

<tr>

<td>ETS ARQUITECTURA VALLES</td>

<td>2</td>

</tr>

<tr>

<td>EUROPEAN SPACE TECHNOL CTR</td>

<td>2</td>

</tr>

</tbody>

</table>

<table id="tab-7"><caption>

**Table 7: The twenty most productive Spanish institutions in the sub-period 1999-2003.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of papers</th>

</tr>

<tr>

<td>UNIV GRANADA</td>

<td>101</td>

</tr>

<tr>

<td>UNIV OVIEDO</td>

<td>40</td>

</tr>

<tr>

<td>CSIC</td>

<td>25</td>

</tr>

<tr>

<td>UNIV POLITECN CATALUNYA</td>

<td>23</td>

</tr>

<tr>

<td>UNIV POLITECN MADRID</td>

<td>20</td>

</tr>

<tr>

<td>UNIV ROVIRA & VIRGILI</td>

<td>20</td>

</tr>

<tr>

<td>UNIV MURCIA</td>

<td>19</td>

</tr>

<tr>

<td>UNIV JAEN</td>

<td>18</td>

</tr>

<tr>

<td>UNIV PUBL NAVARRA</td>

<td>17</td>

</tr>

<tr>

<td>UNIV MALAGA</td>

<td>16</td>

</tr>

<tr>

<td>UNIV SANTIAGO DE COMPOSTELA</td>

<td>14</td>

</tr>

<tr>

<td>UNIV VALLADOLID</td>

<td>14</td>

</tr>

<tr>

<td>UNIV COMPLUTENSE MADRID</td>

<td>13</td>

</tr>

<tr>

<td>UNIV SEVILLA</td>

<td>10</td>

</tr>

<tr>

<td>UNIV GIRONA</td>

<td>9</td>

</tr>

<tr>

<td>UNIV POLITECN VALENCIA</td>

<td>9</td>

</tr>

<tr>

<td>UNIV VALENCIA</td>

<td>9</td>

</tr>

<tr>

<td>UNIV CARLOS III MADRID</td>

<td>8</td>

</tr>

<tr>

<td>UNIV AUTONOMA MADRID</td>

<td>7</td>

</tr>

<tr>

<td>UNIV BALEARIC ISL</td>

<td>7</td>

</tr>

</tbody>

</table>

<table id="tab-8"><caption>

**Table 8: The twenty most productive Spanish institutions in the sub-period 2004-2008.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of papers</th>

</tr>

<tr>

<td>UNIV GRANADA</td>

<td>93</td>

</tr>

<tr>

<td>UNIV OVIEDO</td>

<td>68</td>

</tr>

<tr>

<td>CSIC</td>

<td>34</td>

</tr>

<tr>

<td>UNIV JAEN</td>

<td>33</td>

</tr>

<tr>

<td>UNIV POLITECN VALENCIA</td>

<td>33</td>

</tr>

<tr>

<td>UNIV COMPLUTENSE MADRID</td>

<td>30</td>

</tr>

<tr>

<td>UNIV ROVIRA & VIRGILI</td>

<td>29</td>

</tr>

<tr>

<td>UNIV POLITECN MADRID</td>

<td>23</td>

</tr>

<tr>

<td>UNIV POLITECN CATALUNYA</td>

<td>22</td>

</tr>

<tr>

<td>UNIV SANTIAGO DE COMPOSTELA</td>

<td>22</td>

</tr>

<tr>

<td>UNIV VALLADOLID</td>

<td>20</td>

</tr>

<tr>

<td>UNIV ZARAGOZA</td>

<td>19</td>

</tr>

<tr>

<td>UNIV HUELVA</td>

<td>18</td>

</tr>

<tr>

<td>UNIV PAIS VASCO</td>

<td>18</td>

</tr>

<tr>

<td>UNIV VALENCIA</td>

<td>17</td>

</tr>

<tr>

<td>UNIV PUBL NAVARRA</td>

<td>16</td>

</tr>

<tr>

<td>UNIV ALCALA DE HENARES</td>

<td>15</td>

</tr>

<tr>

<td>UNIV CORDOBA</td>

<td>14</td>

</tr>

<tr>

<td>UNIV BASQUE COUNTRY</td>

<td>12</td>

</tr>

<tr>

<td>UNIV CARLOS III MADRID</td>

<td>12</td>

</tr>

</tbody>

</table>

<table id="tab-9"><caption>

**Table 9: The twenty most productive internationals institutions for sub-period 1965-1993.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of papers</th>

</tr>

<tr>

<td>IONA COLL (USA)</td>

<td>76</td>

</tr>

<tr>

<td>UNIV ALABAMA (USA)</td>

<td>63</td>

</tr>

<tr>

<td>FLORIDA STATE UNIV (USA)</td>

<td>48</td>

</tr>

<tr>

<td>UNIV CALIF BERKELEY (USA))</td>

<td>48</td>

</tr>

<tr>

<td>TOKYO INST TECHNOL (Japan)</td>

<td>40</td>

</tr>

<tr>

<td>KANSAS STATE UNIV AGR & APPL SCI (USA)</td>

<td>39</td>

</tr>

<tr>

<td>UNIV GRANADA (Spain)</td>

<td>39</td>

</tr>

<tr>

<td>UNIV OSAKA PREFECTURE (Japan)</td>

<td>39</td>

</tr>

<tr>

<td>SICHUAN UNIV (Peoples R. of China)</td>

<td>33</td>

</tr>

<tr>

<td>UNIV MANITOBA (Canada)</td>

<td>29</td>

</tr>

<tr>

<td>UNIV ILLINOIS (USA)</td>

<td>28</td>

</tr>

<tr>

<td>UNIV MARYLAND (USA)</td>

<td>28</td>

</tr>

<tr>

<td>PURDUE UNIV (USA)</td>

<td>27</td>

</tr>

<tr>

<td>UNIV MISSOURI (USA)</td>

<td>25</td>

</tr>

<tr>

<td>UTAH STATE UNIV (USA)</td>

<td>25</td>

</tr>

<tr>

<td>UNIV TORONTO (Canada)</td>

<td>24</td>

</tr>

<tr>

<td>CREIGHTON UNIV (USA)</td>

<td>23</td>

</tr>

<tr>

<td>UNIV NEBRASKA (USA)</td>

<td>23</td>

</tr>

<tr>

<td>UNIV OVIEDO (Spain)</td>

<td>23</td>

</tr>

<tr>

<td>OSAKA UNIV (Japan)</td>

<td>21</td>

</tr>

</tbody>

</table>

<table id="tab-10"><caption>

**Table 10: The twenty most productive internationals institutions for sub-period 1994-1998.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of papers</th>

</tr>

<tr>

<td>UNIV GRANADA (Spain)</td>

<td>62</td>

</tr>

<tr>

<td>UNIV MANITOBA (USA)</td>

<td>61</td>

</tr>

<tr>

<td>UNIV S FLORIDA (USA)</td>

<td>57</td>

</tr>

<tr>

<td>UNIV TEXAS (USA)</td>

<td>52</td>

</tr>

<tr>

<td>IONA COLL (USA)</td>

<td>46</td>

</tr>

<tr>

<td>UNIV ALABAMA (USA)</td>

<td>46</td>

</tr>

<tr>

<td>UNIV MISSOURI (USA)</td>

<td>42</td>

</tr>

<tr>

<td>UNIV ARIZONA (USA)</td>

<td>40</td>

</tr>

<tr>

<td>PURDUE UNIV (USA)</td>

<td>39</td>

</tr>

<tr>

<td>HIROSHIMA UNIV (Japan)</td>

<td>37</td>

</tr>

<tr>

<td>SUNY BINGHAMTON (USA)</td>

<td>37</td>

</tr>

<tr>

<td>TSING HUA UNIV (Peoples R. of China)</td>

<td>34</td>

</tr>

<tr>

<td>UNIV SHEFFIELD (England, UK)</td>

<td>34</td>

</tr>

<tr>

<td>TOKYO INST TECHNOL (Japan)</td>

<td>33</td>

</tr>

<tr>

<td>UNIV HOUSTON (USA)</td>

<td>31</td>

</tr>

<tr>

<td>UNIV CALIF BERKELEY (USA)</td>

<td>30</td>

</tr>

<tr>

<td>UNIV BRITISH COLUMBIA (Canada)</td>

<td>28</td>

</tr>

<tr>

<td>CREIGHTON UNIV (USA)</td>

<td>27</td>

</tr>

<tr>

<td>OSAKA UNIV (Japan)</td>

<td>27</td>

</tr>

<tr>

<td>UNIV TORONTO (Canada)</td>

<td>27</td>

</tr>

</tbody>

</table>

<table id="tab-11"><caption>

**Table 11: The twenty most productive internationals institutions for sub-period 1999-2003.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of papers</th>

</tr>

<tr>

<td>CITY UNIV HONG KONG (Peoples R. of China)</td>

<td>116</td>

</tr>

<tr>

<td>UNIV GRANADA (Spain)</td>

<td>101</td>

</tr>

<tr>

<td>HONG KONG POLYTECH UNIV (Peoples R. of China)</td>

<td>96</td>

</tr>

<tr>

<td>TSING HUA UNIV (Peoples R. of China)</td>

<td>83</td>

</tr>

<tr>

<td>UNIV ALBERTA (Canada)</td>

<td>69</td>

</tr>

<tr>

<td>HARBIN INST TECHNOL (Peoples R. of China)</td>

<td>56</td>

</tr>

<tr>

<td>UNIV TEXAS (USA)</td>

<td>52</td>

</tr>

<tr>

<td>UNIV S FLORIDA (USA)</td>

<td>46</td>

</tr>

<tr>

<td>IONA COLL (USA)</td>

<td>45</td>

</tr>

<tr>

<td>CHINESE ACAD SCI (Peoples R. of China)</td>

<td>43</td>

</tr>

<tr>

<td>NAGOYA UNIV (Japan)</td>

<td>41</td>

</tr>

<tr>

<td>HUAZHONG UNIV SCI & TECHNOL (Peoples Rep. of China)</td>

<td>40</td>

</tr>

<tr>

<td>UNIV OVIEDO (Spain)</td>

<td>40</td>

</tr>

<tr>

<td>PURDUE UNIV (USA)</td>

<td>39</td>

</tr>

<tr>

<td>SHANGHAI JIAO TONG UNIV (Peoples R. of China)</td>

<td>39</td>

</tr>

<tr>

<td>UNIV ALABAMA (USA)</td>

<td>39</td>

</tr>

<tr>

<td>UNIV HONG KONG (Peoples R. of China)</td>

<td>39</td>

</tr>

<tr>

<td>BEIJING NORMAL UNIV (Peoples R. of China)</td>

<td>38</td>

</tr>

<tr>

<td>UNIV HOUSTON (USA)</td>

<td>38</td>

</tr>

<tr>

<td>HIROSHIMA UNIV (Japan)</td>

<td>37</td>

</tr>

</tbody>

</table>

<table id="tab-12"><caption>

**Table 12: The twenty most productive internationals institutions for sub-period 2004-2008.**</caption>

<tbody>

<tr>

<th>Institution Name</th>

<th>Number of papers</th>

</tr>

<tr>

<td>HONG KONG POLYTECH UNIV (Peoples R. of China)</td>

<td>140</td>

</tr>

<tr>

<td>CITY UNIV HONG KONG (Peoples R. of China)</td>

<td>131</td>

</tr>

<tr>

<td>UNIV ALBERTA (Canada)</td>

<td>126</td>

</tr>

<tr>

<td>SHANGHAI JIAO TONG UNIV (Peoples R. of China)</td>

<td>125</td>

</tr>

<tr>

<td>CHINESE ACAD SCI (Peoples R. of China)</td>

<td>107</td>

</tr>

<tr>

<td>UNIV GRANADA (Spain)</td>

<td>93</td>

</tr>

<tr>

<td>TSING HUA UNIV (Peoples R. of China)</td>

<td>90</td>

</tr>

<tr>

<td>XIAN JIAOTONG UNIV (Peoples R. of China)</td>

<td>76</td>

</tr>

<tr>

<td>UNIV WATERLOO (Canada)</td>

<td>71</td>

</tr>

<tr>

<td>UNIV OVIEDO (Spain)</td>

<td>68</td>

</tr>

<tr>

<td>HARBIN INST TECHNOL (Peoples R. of China)</td>

<td>65</td>

</tr>

<tr>

<td>NORTHEASTERN UNIV (USA)</td>

<td>55</td>

</tr>

<tr>

<td>ZHEJIANG UNIV (Peoples R. of China)</td>

<td>54</td>

</tr>

<tr>

<td>SHAANXI NORMAL UNIV (Peoples R. of China)</td>

<td>53</td>

</tr>

<tr>

<td>UNIV MANCHESTER (England, UK)</td>

<td>53</td>

</tr>

<tr>

<td>BEIJING NORMAL UNIV (Peoples R. of China)</td>

<td>52</td>

</tr>

<tr>

<td>DALIAN UNIV TECHNOL (Peoples R. of China)</td>

<td>49</td>

</tr>

<tr>

<td>HUAZHONG UNIV SCI & TECHNOL (Peoples R. of China)</td>

<td>47</td>

</tr>

<tr>

<td>UNIV CALGARY (Canada)</td>

<td>46</td>

</tr>

<tr>

<td>UNIV REGINA (Canada)</td>

<td>46</td>

</tr>

</tbody>

</table>

### <a id="appendice-b"></a>Appendice B

#### 1 Pioneering Spanish papers in the [ISI Web of Science](http://scientific.thomson.com/products/wos/)

Here we present the eight pioneer papers that introduced, from 1978 to 1984, fuzzy set theory in Spain:

*   Trillas, E. and Riera, T. (1978). Entropies in finite fuzzy sets, _Information Sciences_, **15**(2), pp. 159-168.
*   Batle, N. and Trillas, E. 1979 Entropy and fuzzy integral, _Journal of Mathematical Analysis and Applications_, **69**(2), 469-474.
*   Ollero, A. and Freire, E. (1981). The structure of relations in personnel-management, _Fuzzy Sets and Systems_, **5**(2), 115-125.
*   Vila, M. A. and Delgado, M. (1983). On medical diagnosis using possibility measures, _Fuzzy Sets and Systems_, **10**(3), 211-222.
*   Alsina, C. and Trillas, E. and Valverde, L. (1983). On some logical connectives for fuzzy-sets theory, _Journal of Mathematical Analysis and Applications_, **93**(1), 15-26.
*   Vila, M. A. and Delgado, M. (1983). Problems of classification in a fuzzy environment, _Fuzzy Sets and Systems_, **9**(3), 229-239.
*   Gil, M. A. and Lopez, M. T. and Gil, P. (1984). Comparison between fuzzy information systems, _Kybernetes_, **13**(4), 245-251.
*   Verdegay, J. L. (1984). A dual approach to solve the fuzzy linear-programming problem, _Fuzzy Sets and Systems_, **14**(29), 131-141.

#### 2 The eight most cited Spanish papers in the [ISI Web of Science](http://scientific.thomson.com/products/wos/)

The search on the [ISI Web of Science](http://scientific.thomson.com/products/wos/) allows us to find the eight most cited Spanish papers that can provide a picture on eight important contributions on the topic that are representative approaches of different areas. Figure 21 shows the list of eight papers and they are briefly described below.

<figure id="fig-21">

![Figure 21: The eight Spanish most cited papers (ISI Web of Science)](../p421fig21.png)

<figcaption>

**Figure 21: The eight Spanish most cited papers ([ISI Web of Science](http://scientific.thomson.com/products/wos/)).**</figcaption>

</figure>

*   Esteva, F. and Godo, L. (2001) [213 citations ]. Monoidal t-norm based logic: towards a logic for left-continuous t-norms, _Fuzzy Sets and Systems_, **124**(3), 271-288.

    In this paper MTL-algebras are investigated.

*   Herrera, F. and Herrera-Viedma, E. (2000) [188 citations ]. Linguistic decision analysis: steps for solving decision problems under linguistic information, _Fuzzy Sets and Systems_, **115**(1), 67-82.

    This paper describes the key steps for solving decision making problems with linguistic information. This paper became in a reference paper in _decision-making_ and _group-decision-making_.

*   Herrera, F. and Martínez, L. (2000) [175 citations ]. A 2-tuple fuzzy linguistic representation model for computing with words, _IEEE Transactions on Fuzzy Systems_, **8**(6), 746-752.

    This paper presents a new fuzzy linguistic approach based on 2-tuple linguistic values.

    This paper become in a reference point in _group-decision-making_, which was a _motor-theme_ in both Spanish and G7 cases in the period 1999-2008.

*   Castro, J.L. (1995) [168 citations ]. Fuzzy-logic controllers are universal approximators, _IEEE Transactions on Systems Man and Cybernetics_, **25**(4), 629-635.

    This was an important paper in the 1990s, where the interest for developing the theoretical foundations of fuzzy control was very important, because of the great applicability of these models.

*   Chiclana, F., Herrera, F. and Herrera-Viedma, E. (1998) [165 citations ]. Integrating three representation models in fuzzy multipurpose decision making based on fuzzy preference relations, _Fuzzy Sets and Systems_, **97**(1), 33-48.

    This paper presents the possibility of merging three commonly-studied representation models for solving decision making problems.

*   Valverde, L. (1985) [163 citations ]. On the structure of F-indistinguishability operators, _Fuzzy Sets and Systems_, **17**(3), 313-328.

    This paper is focused on indistinguishability operators and fuzzy operators and, therefore, it is also included in the theme _T-norm_, it used t-norms and t-conorms in the study.

*   Herrera F., Herrera-Viedma E., and Verdegay J.L. (1996) [157 citations ]. A model of consensus in group decision making under linguistic assessments, _Fuzzy Sets and Systems_, **78**(1), 73-87.

    This paper presents a consensus model in group decision making under linguistic assessments. This paper was included in the _group-decision-making_ thematic network, a motor theme from 1993.

*   Alsina, C. and Trillas, E. and Valverde, L. (1983) [136 citations ]. On some logical connectives for fuzzy-sets theory, _Journal of Mathematical Analysis and Applications_, **93**(1), 15-26.

    This paper is included in the theme connectives, such as it proposes a family of connectives using several t-norms and t-conorms.