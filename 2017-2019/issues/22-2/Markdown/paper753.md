<header>

#### vol. 22 no. 2, June, 2017

</header>

<article>

# Untangling the relationship between Internet anxiety and Internet identification in students: the role of Internet self-efficacy

## [Bo Hsiao, Yu-Qian Zhu](#author), and [Li-Yueh Chen](#author).

> **Introduction.** Previous research has identified Internet anxiety and Internet identification as two important factors that predict usage and experience on the Internet. However, little is known about the relationship between them. This research aimed to untangle the relationship between Internet anxiety and Internet identification, and to investigate the role of Internet self-efficacy in the relationship between Internet anxiety and Internet identification in students.  
> **Methods**. The survey method was used with a sample of 212 grade twelve students in an industrial vocational high school in Taipei, Taiwan.  
> **Analysis**. Confirmation factor analysis was used to assess the quality of the measurement model. Multiple regression with moderation and mediation was used to test the hypotheses.  
> **Results**, The results indicated that although Internet anxiety is not directly associated with Internet identification, it has a negative indirect effect on Internet identification of students mediated through Internet self-efficacy. Furthermore, Internet self-efficacy moderates the relationship between Internet anxiety and Internet identification. Internet anxiety is positively related to Internet identification for those with low Internet self-efficacy.  
> **Conclusion**. Educators and parents should proactively identify students that are low in Internet self-efficacy and encourage them to gain more knowledge and training about Internet usage to boost their Internet self-efficacy.

<section>

## Introduction

The Internet has transformed education with innovative practices such as online education and e-learning in schools ([Tsai, 2013](#ref49); [Tsai and Chiang, 2013](#ref50)). Expected gains and benefits promised by e-learning and online education to students and academic institutions, however, cannot be realized unless they are effectively used ([Saadé and Kira, 2009](#ref40)). As these practices become increasingly popular in today’s education, more research is warranted to identify the antecedents that lead to their wide adoption and improved experience ([Saadé and Kira, 2009](#ref40)).

Previous research has explored a number of antecedents of Internet usage, for example, gender, Internet anxiety, Internet identification ([Joiner, Brosnan, Duffield, Gavin, and Maras, 2007](#ref26); [Joiner _et al._, 2012](#ref27)), personality ([Landers and Lounsbury, 2006](#ref33)), perceived usefulness and ease of use ([Hanson, 2010](#ref19); [Teo, 2001](#ref47)), Internet self-efficacy ([Tsai and Tsai, 2003](#ref51)), socioeconomic status ([Hsieh, Rai, and Keil, 2008](#ref24)), subjective norm ([Pan and Jordan-Marsh, 2010](#ref36)), social support ([Shaw and Gant, 2002](#ref42)) and lifestyle characteristics ([Ho and Lee, 2001](#ref23)). Among these, two factors emerged as important predictors of usage and experience on the Internet: Internet anxiety, i.e., the fear or apprehension that individuals experience when using the Internet ([Presno, 1998](#ref37)), and Internet identification, the extent to which an individual’s self-concept is bound with his or her perceived ability to use the Internet ([Cooper and Weaver, 2003](#ref15); Joiner _et al._, [2007](#ref26), [2012](#ref27)). Despite the Internet boom in the past decade or so, Internet anxiety is still deemed as one of the major obstacles in effective Internet usage (Kalwar, Heikkinen, and Porras, [2011](#ref30), [2013](#ref31)). Reports show that as many as fifty percent of adults, including first-year university students, still have some sort of computer-related phobia ([Saadé and Kira, 2009](#ref40)). Anxiety and other similar emotional states affect not only interaction but also performance, productivity, social relationships, learning, health and overall well-being ([Saadé and Kira, 2009](#ref40)). Extensive research has revealed that Internet anxiety is negatively related to Internet use and experience ([Cooper and Weaver, 2003](#ref15); [Joiner et al., 2012](#ref27)), while Internet identification, to the contrary, facilitates Internet use and experience ([Cooper and Weaver, 2003](#ref15); [Joiner et al., 2012](#ref27)). An individual with a high degree of Internet identification is likely to possess a high degree of experience using the Internet, become motivated to spend time learning how to use the Internet, enroll in courses or consume media about how to navigate the Internet and thus exhibit a positive attitude toward the Internet ([Joiner _et al._, 2007](#ref26)).

Although the importance of Internet anxiety and Internet identification to Internet use and experience has been established, little is known about the relationship between them. Identifying this will help us in three ways: first, to achieve a better understanding of the process and mechanism between antecedents of Internet use and experience; second, to understand the nuances in the relationship between Internet anxiety and Internet identification, i.e., what the moderating factor is and how the moderating conditions work; and finally, with the above knowledge, to enable educators to be better equipped to intervene and improve user experiences in online education and e-learning in a more effective and efficient manner.

Based on self-efficacy and affect-as-information theory, we posit that Internet anxiety is negatively related to Internet identification. Further, we investigate Internet efficacy’s role in the relationship between Internet anxiety and Internet identification. Internet self-efficacy is an individual’s self-perceived confidence and expectation of using the Internet ([Tsai and Tsai, 2003](#ref51)). Two mechanisms were examined: Internet self-efficacy as a mediator, as well as a moderator between Internet anxiety and Internet identification. Our study aims to: (1) shed light on the relationship between Internet anxiety, Internet identification and Internet self-efficacy and (2) examine the moderating and mediating effects of Internet self-efficacy. Through this research, we hope to provide a clearer picture of the relationship between the important antecedents of online education and e-learning in schools, thus equipping educators with better knowledge on how to intervene and improve user experiences.

</section>

<section>

## Theoretical background and hypotheses

Although earlier studies have investigated the relationship between Internet anxiety and Internet identification ([Joiner et al., 2007](#ref26); [Rezaei and Shams, 2011](#ref38)), little research has delved further to explore the possible mediators or moderators. While understanding of the direct relationship between variables is the foundation of our knowledge of a certain domain, to advance theories, research and practice, it is important to move beyond the basic questions ([Frazier, Tix, and Barron, 2004](#ref17)). One way of doing this is by examining moderators and mediators ([Frazier et al., 2004](#ref17)). By exploring moderators, we gain more knowledge about the when or for whom a variable most strongly predicts or causes an outcome variable. The identification of moderators of relations indicates the maturity and sophistication of a field of inquiry ([Aguinis, Boik, and Pierce, 2001](#ref3)). Mediators establish how or why one variable predicts or causes an outcome variable, which can enable us to focus on the effective components leading to our desired results, remove the ineffective components ([MacKinnon, 2000](#ref35)) and build and test theories regarding the causal relationship ([Judd and Kenny, 1981](#ref29)). To advance our understanding of Internet identification and Internet anxiety, we explore the theoretical linkage between Internet anxiety and Internet identification, and further propose Internet self-efficacy as both moderator and mediator to this relationship based on self-efficacy and affect-as-information theory. We argue that Internet anxiety, an emotional state, serves as important sources of information in making judgments and decisions, and in liking, efficacy belief and importance evaluation ([Clore, Gasper, and Garvin., 2001](#ref12); [Clore and Storbeck, 2006](#ref13)). Thus, we argue that Internet anxiety is negatively related to Internet identification. Further, Internet anxiety’s influence on Internet identification is both mediated and moderated by Internet self-efficacy. Figure 1 and figure 2 below summarize our research framework. In the following section, we will briefly review relevant literature on Internet anxiety, Internet identification and Internet self-efficacy, and then develop our hypotheses.

<figure>

![Figure 1\. Mediated model of hypotheses 1-4.](../p753fig1.gif)

<figcaption>

Figure 1\. Mediated model of hypotheses 1-4.</figcaption>

</figure>

<figure>

![Figure 2\. Moderated model of hypothesis 4.](../p753fig2.gif)

<figcaption>

Figure 2\. Moderated model of hypothesis 4.</figcaption>

</figure>

</section>

<section>

### Internet anxiety

Anxiety refers to an unpleasant emotional state or condition characterized by tension, apprehension and worry ([Spielberger, Gorsuch, and Lushene, 1970](#ref44)). Individuals experience anxiety when they perceive threatening conditions in the environment. Thus, Internet anxiety is defined as the fear or apprehension that individuals experience when using the Internet ([Presno, 1998](#ref37)). Internet anxiety is a situation-specific anxiety because it is a form of mental distress caused by fear of danger and powerlessness when interacting with others on the Internet ([Joiner et al., 2007](#ref26)). Internet anxiety hinders individuals from using the different avenues of the Internet, such as e-mail, instant messaging and online activities. Previous research has reported that perceived Internet usefulness, enjoyment and efficacy are negatively related to Internet anxiety ([Zhang, 2005](#ref57)); meanwhile, perception of supporting resources and trust in technology reduce Internet anxiety as well ([Thatcher, Loughry, Lim, and McKnight, 2007](#ref48)). The characteristics of Internet anxiety are derived from computer self-efficacy; thus, Internet self-efficacy is also a concept-specific form of anxiety because it is a feeling that is associated with a person’s interaction with the Internet.

</section>

<section>

### Internet identification

Earlier studies have demonstrated that identity is an important factor in computer use and experience ([Cooper and Weaver, 2003](#ref15); [Facer, Furlong, Furlong, and Sutherland, 2003](#ref16); [Holloway and Valentine, 2003](#ref22)). Identification with a domain binds an individual’s self-esteem with his or her ability to perform successfully in that domain ([Cooper and Weaver, 2003](#ref15); [Shen and Chiou, 2009](#ref43)). Internet identification is a type of domain identification inherently bound with images of those who use the Internet ([Gavin et al., 2007](#ref18)). Thus, Internet identification refers to the extent to which an individual’s self-concept is bound with his or her perceived ability to use the Internet ([Joiner et al., 2007](#ref26)). An individual with a high degree of Internet identification is able to use the Internet effectively to maintain his or her sense of self-worth.

Individual identification is derived “from his knowledge of his membership of a social group (or groups) together with the value and emotional significance attached to that membership” ([Tajfel, 1972, p. 292](#ref46)). According to affect-as-information theory, people attend to their feelings as a source of information. Affect such as Internet anxiety can serve as important sources of information and knowledge not only in making judgments and decisions, but also in liking, efficacy belief and importance evaluation ([Clore et al., 2001](#ref12); [Clore and Storbeck, 2006](#ref13)). If people feel anxious and uneasy about using the Internet, they will reason that the Internet may not be good for them, and thus they may not want to be part of the so-called Internet community. Therefore, we propose:

**_Hypothesis 1:_** Internet anxiety will be negatively related to Internet identification.

</section>

<section>

### Internet self-efficacy

Bandura ([1986](#ref4)) conceptualized self-efficacy as individualized self-perceptions that vary based on the activities and situational circumstances rather than a global disposition that can be assessed by an omnibus test. Bandura ([1986](#ref4)) also suggested that self-efficacy measures should be tailored to the domain of interest to maximize prediction. Therefore, efficacy beliefs in different domains are developed, such as computer self-efficacy (e.g. [Johnson, Hornik, and Salas, 2008](#ref28); [Shih, 2006](#ref45)), Internet self-efficacy ([Hsu and Chiu, 2004](#ref25); [Rains, 2008](#ref39)), perceived self-efficacy ([Thatcher et al., 2007](#ref48)), IT self-efficacy ([Wang, Qian, Wang, and Chen, 2011](#ref53)) and so on. Internet self-efficacy can be defined as an individual’s judgement of his or her own capability to interact with the Internet.

Prior studies have investigated the impact of Internet self-efficacy on Internet use ([Hsu and Chiu, 2004](#ref25); [Tsai and Tsai, 2003](#ref51)). Tsai and Tsai ([2003](#ref51)) proved through a Web-based learning task that students with high Internet self-efficacy have better information searching strategies and learn faster than students with low Internet self-efficacy. Hsu and Chiu ([2004](#ref25)) indicated that consumers with high Internet self-efficacy are more likely to use e-services and implied that the increasing Internet self-efficacy of consumers is critical to the success of an e-service. Thatcher et al. ([2007](#ref48)) supposed that computer self-efficacy is negatively associated with Internet anxiety. To summarize, efficacy belief is an important factor in exploring activities, emotions, and cognitions regarding the use of the Internet in various contexts.

Anxiety, an affective response, has a direct influence on self-efficacy beliefs because state anxiety (in this case, Internet anxiety) and specific self-efficacy (Internet self-efficacy) are components of the self-efficacy framework of Bandura ([1997](#ref5)). Chen, Gully, Whiteman, and Kilcullen ([2000](#ref10)) demonstrated a significant relationship between state anxiety, specific self-efficacy and performance. Anxiety has been argued to affect computer-based learning by affecting the levels of self-efficacy ([Saadé and Kira, 2009](#ref40)). Thus, Internet anxiety in the current context is directly related to Internet self-efficacy, which ultimately influences Internet-related behaviour and performance. Therefore, we develop H2 and H3 as follows:

_**Hypothesis 2:** Internet anxiety is negatively related to Internet self-efficacy._

_**Hypothesis 3:** Internet self-efficacy is positively related to Internet identification._

</section>

<section>

### Internet self-efficacy as a mediator and a moderator

Bandura ([1986](#ref4)) indicated that efficacy mediates the effect of skills or other self-beliefs on subsequent performance and accomplishments by influencing effort and persistence in the face of failures, which was later substantiated by empirical findings ([Bandura, 1997](#ref5); [Bandura and Schunk, 1981](#ref6)); [Chen et al., 2000](#ref10); [Zimmerman, 1995](#ref54); [Zimmerman and Bandura, 1994](#ref55)). A number of IS studies have confirmed the claim that the relationship between anxiety and behaviour is mediated by the personal beliefs of an individual ([Chen, Hsiao, Chern, and Chen, 2014](#ref11); [Schlenker and Leary, 1982](#ref41); [Spielberger, Gorsuch, and Lushene, 1970](#ref44)). Students with high perceived self-efficacy are more successful in school activities and utilize more effective learning strategies in any designated domain and level of ability ([Bouffard-Bouchard, Parent, and Larivee, 1991](#ref9); [Zimmerman, 1995](#ref54); [Zhang and Zhang, 2003](#ref56)). In this regard, the proposed hypothesis 4 is related to this mediation by Internet self-efficacy between Internet anxiety and Internet identification.

_**Hypothesis 4:** Internet self-efficacy mediates the effect of Internet anxiety on Internet identification._

Several studies have suggested that efficacy beliefs facilitate integration and the effective use of complex information and that efficacy beliefs have moderating effects ([Bandura and Jourden, 1991](#ref7); [Tsai and Tsai, 2003](#ref51)). Bandura ([1986](#ref4)) clarified the relationship between self-efficacy and performance. Perceptions on efficacy are regarded as behaviour predictors; if an individual possesses the requisite skills for a task, high self-efficacy will promote improved utilization of existing cognitive resources. Tsai and Tsai ([2003](#ref51)) reported that students with high Internet self-efficacy employ better searching strategies and learn more effectively than students with low self-efficacy. They also suggested that high Internet self-efficacy enhances students’ behavioural, procedural and meta cognitive strategies for searching for information in a Web-based environment. This enhancement consequently facilitates learning. Therefore, we propose Internet self-efficacy as a moderator between Internet anxiety and Internet identification: for students with higher Internet self-efficacy, the negative effect of Internet anxiety is less pronounced than those with lower Internet self-efficacy, since high self-efficacy helps to cancel out some of the negative effect of Internet anxiety.

_**Hypothesis 5:** Internet self-efficacy moderates the relationship between Internet anxiety and Internet identification such that the negative relationship is weaker when Internet self-efficacy is high than when it is low._

</section>

<section>

## Methods

### Samples and procedures

This study was conducted in an industrial vocational high school in Taipei, Taiwan. The participants included 212 grade twelve students from eight classes. Students were encouraged to take the survey for course credits and were told that the questionnaires maintained the anonymity of the respondents. We had a 100% response rate as all students asked agreed to participate. Survey questionnaires were distributed to the students in class and collected immediately after completion. We obtained 92% valid responses as some questionnaires had missing data. As most of the educational degrees offered by the school are in the field of engineering, the student body is composed of mostly males. The student body consists of 45 girls (21.2%) and 167 boys (78.8%). The mean age of the participants is approximately 18 years (ages ranging from 17 to 19). The questionnaire included the following sections: (i) an Internet self-efficacy scale, (ii) an Internet anxiety scale, and (iii) an identification with the Internet scale. The participants were asked to write down their age and gender at the end of the questionnaire.

</section>

<section>

### Measures

The questionnaires involved three measures. The first measure, the Internet self-efficacy scale, was developed by Tsai and Tsai ([2003](#ref51)). Five items were used, including for example “I think I am the kind of person who can make good use of the Internet”, “I think I know how to use a Web browser like Internet Explorer, or Google Chrome”. Respondents answered on a five-point Likert scale ranging from “never” to “always” (1=never, 2=rarely, 3=sometimes, 4=often, and 5=always). One item was removed from the original six-item scale because its loading was below 0.6\. The Cronbach’s alpha (a) of the resulting five-item scale was 0.883.

The second measure, the Internet anxiety scale, was developed by Joiner et al. ([2007](#ref26)). Four of the original six items were used: “I always feel anxious when using the Internet”, “I go out of my way to avoid using the Internet”, “My anxiety about using the Internet bothers me” and “I am more anxious about using the Internet than I should be”. The participants were asked to answer through a five-point Likert scale. Two items were removed from the original six-item scale because their loading was below 0.6: “It is easy for me to use the Internet” and “It is important for me to able to use the Internet”. The Cronbach’s a for this four-item scale was 0.831; reliability was found to be adequate on this measure.

The third measure, the Internet identification scale, was developed by Joiner et al. ([2007](#ref26)) based on the previous work of Maras ([2002](#ref35a)). Ten items were used, including “I would describe myself as an Internet user”, “I am very similar to other Internet user” and “When there is an opportunity I always get involved in using the Internet”. According to Joiner et al. ([2007](#ref26)), Internet identification can be measured by the following three factors: the students’ awareness of being part of an Internet community, similarity to the other Internet users and the importance of the Internet to the students. Participants were asked to answer through a five-point Likert scale (1=strongly disagree to 5=strongly agree). Three items were removed from the original ten-item scale because their loading was below 0.6\. The coefficient alpha for this seven-item scale was 0.825; reliability was found to be adequate on this measure.

</section>

<section>

### Analysis

All measures were subjected to confirmatory factor analysis to assess uni-dimensionality and convergent and discriminate validity. Data analysis utilized LISREL 8.8 to assess the quality of the measurement model. This study followed the recommendations of Anderson and Gerbing ([1988](#ref2)) and adopted maximum likelihood with LISREL to assess the convergent and discriminate validity of the three constructs. The relationship between each item and its respective variable was statistically significant in the three-construct confirmatory factor analysis model, with all indicator loadings exceeding 0.5, thus achieving convergent validity. Moreover, the 95% confidence intervals of all the inter-factor correlations were not over 1.00, which exhibits satisfactory discriminate validity ([Hair, Black, Babin, Anderson, and Tatham, 2006](#ref20)). The overall fit of the measurement model was satisfactory (?<sup>2</sup>=238.71, df=101, ?<sup>2</sup>/df=2.36, NFI=0.91, NNFI=0.94, CFI=0.95, IFI=0.95, and RMSEA=0.080). Item loading and reliability are summarized in Table 1.

<table><caption>

**Table 1\. Factor loading and reliability**</caption>

<tbody>

<tr>

<th colspan="2">Internet self-efficacy</th>

<th colspan="2">Internet anxiety</th>

<th colspan="2">Internet identification</th>

</tr>

<tr>

<th>item</th>

<th>factor loading</th>

<th>item</th>

<th>factor loading</th>

<th>item</th>

<th>factor loading</th>

</tr>

<tr>

<td>1</td>

<td>0.844</td>

<td>1</td>

<td>0.779</td>

<td>1</td>

<td>0.677</td>

</tr>

<tr>

<td>2</td>

<td>0.723</td>

<td>2</td>

<td>0.699</td>

<td>2</td>

<td>0.666</td>

</tr>

<tr>

<td>3</td>

<td>0.824</td>

<td>3</td>

<td>0.891</td>

<td>3</td>

<td>0.629</td>

</tr>

<tr>

<td>4</td>

<td>0.871</td>

<td>4</td>

<td>0.880</td>

<td>4</td>

<td>0.744</td>

</tr>

<tr>

<td>5</td>

<td>0.856</td>

<td></td>

<td></td>

<td>5</td>

<td>0.687</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td></td>

<td>6</td>

<td>0.770</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td></td>

<td>7</td>

<td>0.727</td>

</tr>

</tbody>

</table>

Following the procedure in Zhu, Chen, Chen, and Chern ([2011](#ref58)), our hypotheses were tested in two steps. First, we tested Hypothesis 1 to 4 with the mediation model. Second, we examined Hypothesis 5 with the moderation model. All continuous measures were mean-centred ([Aiken and West, 1991](#ref1)) before analysis.

Hypotheses 1, 2, 3 and 4 suggest one indirect effect model, wherein the relationship between Internet anxiety and Internet identification of students is transmitted by Internet self-efficacy. The mediation hypotheses were tested (1-4) with an SPSS macro provided by Hayes ([2012](#ref21)). Internet self-efficacy would moderate the inverse relationship between Internet anxiety and Internet identification for Hypothesis 5\. We also follow Cohen and Cohen’s ([1983](#ref14)) procedure to establish the interactive effects for these constructs and Aiken and West’s ([1991](#ref1)) procedure to test the interactions by simple slope analysis.

</section>

<section>

## Results

Table 2 presents the mean values, standard deviation values and inter-correlations for all the study variables. An inspection of the correlation reveals that Internet identification is positively related to Internet self-efficacy (r = 0.419, p < 0.01), whereas Internet self-efficacy is negatively related to Internet anxiety (r = -0.310, p<0.01).

<table><caption>

**Table 2\. Descriptive statistics and inter-correlations of study variables**</caption>

<tbody>

<tr>

<th>Variable</th>

<th>M</th>

<th>SD</th>

<th>1</th>

<th>2</th>

<th>3</th>

</tr>

<tr>

<td>1. Internet self-efficacy</td>

<td>19.986</td>

<td>3.655</td>

<td>—</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>2. Internet anxiety</td>

<td>8.198</td>

<td>3.072</td>

<td>-0.310**</td>

<td>—</td>

<td> </td>

</tr>

<tr>

<td>3. Internet identification</td>

<td>21.967</td>

<td>4.763</td>

<td>0.419**</td>

<td>-0.062</td>

<td>—</td>

</tr>

<tr>

<td colspan="6">Note. All tests are two-tailed. N=212. **p < 0.01</td>

</tr>

</tbody>

</table>

The results for Hypotheses 1, 2 and 3 are presented in Table 3\. Internet anxiety is not significantly related to Internet identification (B = -0.097, p = 0.366 > 0.05). Internet anxiety is negatively related to the Internet self-efficacy of students (B = -0.368, t = -4.720, and p = 0.000 < 0.001). The inverse relationship between Internet self-efficacy and Internet identification controlled by Internet anxiety supports Hypothesis 3 (B = 0.577, t = 6.723, p = 0.000 < 0.001). Therefore, Hypotheses 2 and 3 are supported, whereas Hypothesis 1 is not supported.

<table>

<tbody>

<tr>

<th rowspan="2">Variable</th>

<th>B</th>

<th>SE</th>

<th>t</th>

<th>p</th>

</tr>

<tr>

<th colspan="4">Direct and total effects</th>

</tr>

<tr>

<td>Internet identification regressed on Internet anxiety:</td>

<td>-0.097</td>

<td>0.107</td>

<td>-0.906</td>

<td></td>

</tr>

<tr>

<td>Internet self-efficacy regressed on Internet anxiety:</td>

<td>-0.368</td>

<td>0.078</td>

<td>-4.72</td>

<td>0.000</td>

</tr>

<tr>

<td>Internet identification regressed on Internet self-efficacy, controlling for Internet anxiety:</td>

<td>0.577</td>

<td>0.858</td>

<td>6.723</td>

<td>0.000</td>

</tr>

<tr>

<td>Internet identification regressed on Internet anxiety, controlling for Internet self-efficacy:</td>

<td>0.116</td>

<td>0.102</td>

<td>1.133</td>

<td>0.258</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 3\. Regression results for mediation**</caption>

<tbody>

<tr>

<th> </th>

<th>Value</th>

<th>SE</th>

<th>LL 95% CI</th>

<th>UL95% CI</th>

<th>z</th>

<th>p</th>

</tr>

<tr>

<td> </td>

<th colspan="6">Indirect effect and significance using normal distribution</th>

</tr>

<tr>

<td>Sobel</td>

<td>-0.212</td>

<td>0.060</td>

<td>-0.344</td>

<td>-0.106</td>

<td>-3.833</td>

<td>0.001</td>

</tr>

<tr>

<td> </td>

<th>Value</th>

<th>

_SE_</th>

<th>LL 99% CI</th>

<th>UL 99% CI</th>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<th colspan="4">Boostrap results for indirect effect</th>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Effect</td>

<td>-0.212</td>

<td>-0.060</td>

<td>-0.346</td>

<td>-0.108</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="7">

Note. N=212\. Unstandardized regression coefficients are reported. Bootstrap sample size = 5,000\. LL = lower limit; CI = confidence interval; UL = upper limit.</td>

</tr>

</tbody>

</table>

Table 3 also shows the results for Hypothesis 4\. According to Kenny, Kashy, and Bolger ([1988](#ref32)), the main effect may be weak or insignificant even though an indirect effect exists. For Hypothesis 4, although the direct effect of Internet anxiety on Internet identification was not significant (B = -0.097, t = -0.906, p > 0.05), an indirect effect may still exist. Internet anxiety is negatively related to the Internet self-efficacy of students (B =- 0.368, t = -4.720, p < 0.001), and the inverse relationship between Internet self-efficacy and students’ Internet identification is significant (B = 0.577, t = 6.723, p = 0.000 < 0.001). Internet anxiety also has a negative indirect effect on the Internet identification of students (-0.212). In addition, the formal two-tailed significance test (assuming a normal distribution) demonstrated that the indirect effect is significant (Sobel z = -3.833, p = 0.001 < 0.01). Thus, Hypothesis 4 is supported.

The result for the moderating effect is reported in Table 4\. Results indicates that the interaction between Internet self-efficacy and Internet anxiety is significantly related to Internet identification (B = -0.059, ß = 0.770, p = 0.022 < 0.05). It also demonstrates the magnitude of the coefficient between Internet anxiety and Internet identification is influenced by the level of Internet self-efficacy. Simple slope analysis was conducted following the suggestion of Aiken and West ([1991](#ref1)). The results are presented in Figure 3\. Contrary to Hypothesis 5, the analysis indicates that Internet anxiety is positively related to the Internet identification of students when Internet self-efficacy is low (ß = 0.232, p = 0.015 < 0.05), but is not significantly related to Internet identification when Internet self-efficacy is high (ß = -0.048, p = 0.571 > 0.05).

<table><caption>

**Table 4\. Regression results for moderation (Internet identification is dependent variable)**</caption>

<tbody>

<tr>

<th>Variable</th>

<th>B</th>

<th>ß</th>

<th>t</th>

<th>p</th>

</tr>

<tr>

<td>Internet self-efficacy (ISE)</td>

<td>1.068</td>

<td>0.820</td>

<td>4.658</td>

<td>0.000</td>

</tr>

<tr>

<td>Internet anxiety (IA)</td>

<td>1.330</td>

<td>0.858*</td>

<td>2.481</td>

<td>0.014</td>

</tr>

<tr>

<td>ISE × IA</td>

<td>-0.059</td>

<td>-0.770*</td>

<td>-2.307</td>

<td>0.022</td>

</tr>

</tbody>

</table>

<figure>

![Figure 3\. Internet self-efficacy and Internet anxiety for Internet identification](../p753fig3.png)

<figcaption>

**Figure 3\. Internet self-efficacy and Internet anxiety for Internet identification**</figcaption>

</figure>

</section>

<section>

## Limitations

Some limitations of this research should be noted. First, similar to Saade and Kira ([2009](#ref40)), our sample focused on young students; however, our sample is biased towards students with an engineering concentration, who are likely have previous experience of the Internet. Thus, it is possible that our sample’s scores of Internet anxiety are lower than students with other study concentrations, while the scores for Internet identification and Internet-efficacy could be higher than for average students. Therefore, our sample may suffer from range restriction, which implies that our results are likely to be conservative estimates of the actual effect size ([McDaniel, Whetzel, Schmidt, and Maurer, 1994](#ref34)). Second, our population sample was all high school students and predominantly male, resulting in limitation generalization and possible sex bias. Future similar studies could therefore complement our findings by investigating a broader and more balanced population sample. Finally, our study focused on overall Internet identification, rather than identification in specific virtual communities. Future studies could therefore investigate similar phenomena in relation to specific Internet identification dimensions.

</section>

<section>

## Discussion and conclusion

The overall aim of this study was to investigate the relationship between three important factors related to Internet usage and experience: Internet anxiety, Internet identification and Internet self-efficacy. We found that Internet anxiety is negatively related to Internet self-efficacy, which predicts Internet identification. Internet anxiety, however, is not directly linked to Internet identification. This is different from what Joiner et al. ([2007](#ref26)) predicted: a negative significant relationship between Internet identification and Internet anxiety of students. The results of our study reveal a much more complex picture: for students high in Internet self-efficacy, Internet anxiety is not significantly related to Internet identification, and for students low in Internet self-efficacy, Internet anxiety is positively related to Internet identification.

These findings are interesting, as previous research has mostly reported negative effects of Internet anxiety on performance, experience and intentions to use ([Cooper and Weaver, 2003](#ref15); [Joiner et al., 2012](#ref27); [Saadé and Kira, 2009](#ref40)). In this study, in the context of low Internet self-efficacy, Internet anxiety is actually positively related to Internet identification. Thus, Internet anxiety, in different contexts, can be positively related to Internet identification, and subsequently, Internet use and experience. How is this possible? We suspect that it may be related to the characteristics of our sample. Teenagers today are considered digital natives, familiar with computers, videos, video games, social media and other sites on the Internet. For them, the Internet is as natural as air and water. Thus, we often overlook the fact that among these digital natives, Internet anxiety still exists, as well as varying levels of Internet self-efficacy beliefs, and although not measured explicitly, varying levels of Internet skills. Students low in Internet self-efficacy may fear being left behind by their peers who can use the Internet fluently, and may have a strong desire to be like other students who can use the Internet to accomplish many tasks including friendship building, homework and casual learning. Thus, for students low in Internet self-efficacy, Internet anxiety is actually positively related to Internet identification: the more they identify with the Internet (e.g., believe that the Internet is important), the more anxious they are (e.g., “I don’t think I know how to use it, and that worries me”).

The above findings provide important implications both in theory and in practice. First, the findings emphasize the effects of efficacy beliefs in enhancing the Internet identification of students both as a mediator and moderator. Internet self-efficacy is a strong predictor of Internet identification, thus, building Internet self-efficacy is an important step towards helping students reap the full benefit of the Internet. Although we may assume that all students today have high Internet self-efficacy, the results show that it is not always the case, As some have low self-efficacy. Second, Internet self-efficacy moderates the relationship between Internet anxiety and Internet identification. For those with low Internet self-efficacy, Internet anxiety may be a reaction from students wishing to be as good as their peers in Internet usage, and the more they identify with the Internet, the more anxious they become. Thus, educators and parents should proactively identify these students that are low in Internet self-efficacy and encourage them to gain more knowledge and training about Internet usage, eventually boosting their Internet self-efficacy.

</section>

<section>

## <a id="author"></a>About the authors

**Bo Hsiao** is an Associate Professor in the Department of Information Management at Chang Jung Christian University, Taiwan. He received his PhD degree in Information Management from National Taiwan University, Taiwan. His research interests include manufacturing information systems, data envelopment analysis, project management, knowledge economy, and pattern recognition. He can be contacted at [bhsiao@mail.cjcu.edu.tw](mailto:bhsiao@mail.cjcu.edu.tw)  
**Yu-Qian Zhu*** is an Assistant Professor in the Department of Information Management, National Taiwan University of Science and Technology. She holds a Ph.D. in Management of Technology from National Taiwan University. Before her academic career, she served as R&D engineer and manager in Fortune 100 and InfoTech 100 firms. Her research interests include social media and online privacy. She can be reached at [yzhu@mail.ntust.edu.tw](mailto:yzhu@mail.ntust.edu.tw) * corresponding author  
**Li-Yueh Chen** is a Ph.D. candidate in the Department of Information Management at National Taiwan University, Taiwan. She received her master degree in Information Management from National Chengchi University, Taiwan. She is also a mathematical teacher at National Tseng-Wen Senior Agricultural and Industrial Vocational School. Her field of proficiency includes education, education psychology and mathematics. She can be contacted at [d96725009@ntu.edu.tw](mailto:d96725009@ntu.edu.tw)

</section>

<section>

## References

<ul>
<li id="ref1">Aiken, L. S. &amp; West, S. G. (Eds.). (1991). Multiple regression: testing and interpreting interactions. Newbury Park, CA: Sage Publications.
</li>
<li id="ref2">Anderson, J. C., &amp; Gerbing, D. W. (1988). Structural equation modeling with practice: a review and recommendation two-step approach. <em>Psychological Bulletin, 103</em> (3), 411-423.
</li>
<li id="ref3">Aguinis, H., Boik, R. J. &amp; Pierce, C. A. (2001). A generalized solution for approximating the power to detect effects of categorical moderator variables using multiple regression. <em>Research Methods, 4</em> 4(4), 291–323.
</li>
<li id="ref4">Bandura, A. (1986). <em>Social foundations of thought and action: a social cognitive theory.</em>Englewood Cliffs, N.J.: Prentice-Hall.
</li>
<li id="ref5">Bandura, A. (1997). <em>Self-efficacy: the exercise of control.</em> New York: W.H. Freeman.
</li>
<li id="ref6">Bandura, A. &amp; Schunk, D. H. (1981). Cultivating competence, self-efficacy, and intrinsic interest through proximal self-motivation. <em>Journal of Personality and Social Psychology, 41</em>(3), 586-598.
</li>
<li id="ref7">Bandura, A. &amp; Jourden, F. J. (1991). Self-regulatory mechanisms governing the impact of social comparsion on complex decision making. <em>Journal of Personality and Social Psychology, 60</em>(6), 941-951.
</li>
<li id="ref8">Baron, R. M.. &amp; Kenny, D. A. (1986). The moderator mediator variable distinction in social psychological-research - conceptual, strategic, and statistical considerations. <em>Journal of Personality and Social Psychology, 51</em>(6), 1173-1182.
</li>
<li id="ref9">Bouffard-Bouchard, T., Parent, S. &amp; Larivee, S. (1991). Influence of self-efficacy on self-regulation and performance among junior and senior high-school aged students. <em>International Journal of Behavioral Development, 14</em>(2), 153-164.
</li>
<li id="ref10">Chen, G., Gully, S. M., Whiteman, J. &amp; Kilcullen, R. N. (2000). Examination of relationships among trait-like individual differences, state-like undividual differences, and learning performance. <em>Journal of Applied Psychology, 85</em>(3), 835-847.
</li>
<li id="ref11">Chen, L.Y., Hsiao, B., Chern, C. C. &amp; Chen, H.G. (2014). Affective mechanisms linking internet use to learning performance in high school students: a moderated mediation study. <em>Computers in Human Behavior, 35,</em>431-443
</li>
<li id="ref12">Clore, G. L., Gasper, K. &amp; Garvin, E. (2001). Affect as information. In J. P. Forgas, (Ed.), <em>Handbook of Affect and Social Cognition</em>(pp. 121-144). Mahwah, NJ: Lawrence Erlbaum Associates.
</li>
<li id="ref13">Clore, G.L. &amp; Storbeck, J. (2006). Affect as information about liking, efficacy, and importance. In J. Forgas (Ed.), <em>Affect in Social Thinking and Behavior</em> (pp. 123-142). New York: Psychology Press.
</li>
<li id="ref14">Cohen, J. &amp; Cohen, P. (Eds.). (1983). <em>Applied multiple regression/correlation analysis for the behavioral sciences.</em>(Second Edition). H illsdale, NJ: Erlbaum.
</li>
<li id="ref15">Cooper, J. &amp; Weaver, K. D. (2003). <em>Gender and computers: understanding the digital divide.</em>Mahwah, NJ: Lawrence Erlbaum Associates.
</li>
<li id="ref16">Facer, K., Furlong, J., Furlong, R. &amp; Sutherland, R. (Eds.). (2003). <em>ScreenPlay: children and computing in the home.</em>London: RoutledgeFalmer.
</li>
<li id="ref17">Frazier, P. A., Tix, A. P. &amp; Barron, K. E. (2004). Testing moderator and mediator effects in counseling psychology research. <em>Journal of Counseling Psychology, 51</em>(1), 115-134.
</li>
<li id="ref18">Gavin, J., Duffield, J., Brosnan, M., Joiner, R., Maras, P. &amp; Scott, A. J. (2007). Drawing the net: Internet identification, Internet use and the image of Internet users. <em>CyberPsychology &amp; Behavior, 10</em>(3), 478-481.
</li>
<li id="ref19">Hanson, V. L. (2010). Influencing technology adoption by older audits. <em>Interacting with Computers, 22</em>(6), 502-509.
</li>
<li id="ref20">Hair, J. F., Black, W. C., Babin, B. J., Anderson, R. E. &amp; Tatham, R. L. (2006). <em>Multivariate data analysis</em>(6th ed.). Uppersaddle River, NJ: Pearson Prentice Hall.
</li>
<li id="ref21">Hayes, A. F. (2012). <em>An analytical primer and computational tool for observed variable moderation, mediation, and conditional process modeling.</em> Manuscript submitted for publication.
</li>
<li id="ref22">Holloway, W. &amp; Valentine, G. (Eds.). (2003). <em>Cyberkids: children in the information age.</em>London: RoutledgeFalmer.
</li>
<li id="ref23">Ho, S. M.. &amp; Lee, T. M. C. (2001). Computer usage and its relationship with adolescent lifestyle in Hong Kong. <em>Journal of Adolescent Health, 29</em>(4), 258–266.
</li>
<li id="ref24">Hsieh, J. J., Rai, A. &amp; Keil, M. (2008). Understanding digital inequality: comparing continued use behavioral models of the socio-economically advantaged and disadvantaged. <em>MIS Quarterly, 32</em>(1), 97-126.
</li>
<li id="ref25">Hsu, M.-H. &amp; Chiu, C.-M. (2004). Internet self-efficacy and electronic service acceptance. <em>Decision Support Systems, 38</em>(3), 369-381.
</li>
<li id="ref26">Joiner, R., Brosnan, M., Duffield, J., Gavin, J. &amp; Maras, P. (2007). The relationship between Internet identification, Internet anxiety and Internet use. <em>Computers in Human Behavior, 23</em>(3), 1408-1420.
</li>
<li id="ref27">Joiner, R., Gavin, J., Brosnan, M., Cromby, J., Gregory, H., Guiller, J., Maras, P. &amp; Moon, A. (2012) Gender, Internet experience, Internet identification and Internet anxiety: a ten year follow-up. <em>Cyberpsychology, Behavior, and Social Networking, 15</em>(7). 370-372.
</li>
<li id="ref28">Johnson, R. D., Hornik, S. &amp; Salas, E. (2008). An empirical examination of factors contributing to the creation of successful elearning environments. <em>International Journal of Human-Computer Studies, 66</em>(5), 356–369.
</li>
<li id="ref29">Judd, C. M. &amp; Kenny, D. A. (1981). Process analysis: estimating mediation in treatment evaluations. <em>Evaluation Review, 5</em>(5), 602–619.
</li>
<li id="ref30">Kalwar, S. K., Heikkinen, K. &amp; Porras, J. (2011). Finding a relationship between Internet anxiety and human behavior. In J.A. Jacko (Ed.), <em>Human-computer interaction: design and development approaches</em> (pp. 359-367). Heidelberg: Springer.
</li>
<li id="ref31">Kalwar, S. K., Heikkinen, K. and Porras, J. (2013). Internet anxiety: myth or reality? In M. Kurosu (Ed.), <em>Human-computer interaction: towards intelligent and implicit interaction</em> (pp. 431-440). Heidelberg: Springer.
</li>
<li id="ref32">Kenny, D. A., Kashy, D. A. &amp; Bolger, N. (1998). <em>Data analysis in social psychology.</em> New York: McGraw-Hill.
</li>
<li id="ref33">Landers, R. N. &amp; Lounsbury, J. W. (2006). An investigation of Big Five and narrow personality traits in relation to Internet usage. <em>Computers in Human Behavior, 22</em>(2), 283–293.
</li>
<li id="ref34">McDaniel, M. A., Whetzel, D. L., Schmidt, F. L. &amp; Maurer, S. D. (1994). The validity of employment interviews: a comprehensive review and meta-analysis. <em>Journal of Applied Psychology, 79</em>(4), 599-616.
</li>
<li id="ref35">MacKinnon, D. P. (2000). Contrasts in multiple mediator models. In J. S. Rose, L. Chassin, C. C. Presson &amp; S. J. Sherman (Eds.), <em>Multivariate applications in substance use research: new methods for new questions</em> (pp. 141–160). Mahwah, NJ: Erlbaum.
</li>
<li id="ref35a">Maras, P. (2002). Identity, social perception and motivation: interdependent or autonomous factors? In British journal of educational psychology cutting edge conference, The Lake District, UK.
</li>
<li id="ref36">Pan, S. &amp; Jordan-Marsh, M. (2010). Internet use intention and adoption among Chinese older adults: from the expanded technology acceptance model perspective. <em>Computers in human nehavior, 26</em>(5), 1111-1119.
</li>
<li id="ref37">Presno, C. (1998). Taking the byte out of Internet anxiety: instructional techniques that reduce computer/Internet anxiety in the classroom. <em>Journal of Educational Computing Research, 18</em>(2), 147-161.
</li>
<li id="ref38">Rezaei, M. &amp; Shams, A. (2011). The relationship between Internet anxiety, Internet self-efficacy, Internet identification and Internet use: case of agricultural students. <em>World Applied Sciences Journal, 13</em>(8), 1852-1859.
</li>
<li id="ref39">Rains, S. A. (2008). Seeking health information in the information age: the role of Internet self-efficacy, <em>Western Journal of Communication, 72</em>(1), 1-18.
</li>
<li id="ref40">Saade, R. &amp; Kira, D. (2009). Computer anxiety in e-learning: the effect of computer self-efficacy. <em>Journal of Information Technology Education, 8</em>(1), 177–191.
</li>
<li id="ref41">Schlenker, B. R. &amp; Leary, M. R. (1982). Social anxiety and self-presentation: conceptualization and model. <em>Psychological Bulletin, 92</em>(3), 641-669.
</li>
<li id="ref42">Shaw, L. H. &amp; Gant, L. M. (2002). In defense of the Internet: the relationship between Internet communication and depression, loneliness, self-esteem, and perceived social support. <em>CyberPsychology &amp; Behavior, 5</em>(2), 157–171.
</li>
<li id="ref43">Shen, C. C. &amp; Chiou, J. S. (2009). The effect of community identification on attitude and intention toward a blogging community. <em>Internet Research, 19</em>(4), 393-407.
</li>
<li id="ref44">Spielberger, C. D., Gorsuch, R. L. &amp; Lushene, R. E. (1970). <em>Manual for the State-Trait Anxiety Inventory.</em>Palo Alto, CA: Consulting Psychologists.
</li>
<li id="ref45">Shih, H. P. (2006). Assessing the effects of self-efficacy and competence on individual satisfaction with computer use: an IT student perspective. <em>Computers in Human Behavior, 22</em>(6), 1012-1026.
</li>
<li id="ref46">Tajfel, H. (1972). La catégorisation sociale. In S. Moscovici (Ed.), <em>Introduction à la Psychologie Sociale Vol. 1</em> (pp. 272–302). Paris: Larousse.
</li>
<li id="ref47">Teo, T. S. H. (2001). Demographic and motivation variables associated with Internet usage activities. <em>Internet Research, 11</em>(2), 125-137.
</li>
<li id="ref48">Thatcher, J. B., Loughry, M. L., Lim, J. &amp; McKnight, D. H. (2007). Internet anxiety: an empirical study of the effects of personality, beliefs, and social support. <em>Information and Management, 44</em>(4), 353-363.
</li>
<li id="ref49">Tsai, C. W. (2013). An effective online teaching method: the combination of collaborative learning with initiation and self-regulation learning with feedback. <em>Behaviour &amp; Information Technology, 32</em>(7), 712-723.
</li>
<li id="ref50">Tsai, C.W. &amp; Chiang, Y.C. (2013). Research trends in problem-based learning (PBL) research in e-learning and online education environments: a review of publications in SSCI-indexed journals from 2004 to 2012. <em>British Journal of Educational Technology, 44</em>(6), E185-E190.
</li>
<li id="ref51">Tsai, M.J. &amp; Tsai, C. C. (2003). Information searching strategies in Web-based science learning: the role of Internet self-efficacy. <em>Innovations in Education &amp; Teaching International, 40</em>(1), 43-50.
</li>
<li id="ref52">Tseng, F.C. &amp; Teng, C.I. (2014). Antecedents for user intention to adopt another auction site. <em>Internet Research, 24</em>(2), 205-222.
</li>
<li id="ref53">Wang, M., Qian, M., Wang, W., &amp; Chen, R. (2011). Effects of group counseling based on self-efficacy for self-regulated learning in students with academic procrastination. <em>Chinese Mental Health Journal, 25</em>(12), 921–926.
</li>
<li id="ref54">Zimmerman, B. J. (1995). Attaining reciprocality between learning and development through self-regulation. <em>Human Development, 38</em>(6), 367–372
</li>
<li id="ref55">Zimmerman, B. J. and Bandura, A. (1994). Impact of self-regulatory influences on writing course attainment. <em>American Educational Research Journal, 31</em>(4), 845–862.
</li>
<li id="ref56">Zhang, L. and Zhang, X. (2003). A study of the relationships among learning strategy-usingself-efficacy, persistence and academic achievement in middle school students. <em>Psychology Science, 26</em>(4), 603-607.
</li>
<li id="ref57">Zhang, Y. (2005). Age, gender, and Internet attitudes among employees in the business world. <em>Computers in Human Behavior, 21</em>(1), 1–10.
</li>
<li id="ref58">Zhu, Y. Q., Chen, L. Y., Chen, H. G. and Chern, C. C. (2011), How does Internet information seeking help academic performance? the moderating and mediating roles of academic self-efficacy. <em>Computers &amp; Education, 57</em>(4), 2476–2484.
</li>
</ul>

</section>

</article>