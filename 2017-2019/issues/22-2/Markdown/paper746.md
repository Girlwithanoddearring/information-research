<header>

#### vol. 22 no. 2, June, 2017

</header>

<article>

# Cultured and popular literary circuits on Facebook: a case study of Singaporean print culture in social media

## [Karryl Kim Sagun](#author), and [Brendan Luyt](#author).

> **Introduction**. This article presents a case study of social media use by the independent Singaporean bookstore, _BooksActually_.  
> **Method**. A genre analysis based on Bhatia’s list of facets was conducted on the bookstore’s Facebook page. _BooksActually_'s Facebook posts, collected in reverse chronological order in March 2015 and cleaned using the software NVivo, were analysed.  
> **Analysis**. Purposive sampling of Facebook posts was initially employed to find out how _BooksActually_ presents facets of print culture. Responses (comments and likes) were left out in the primary analysis, but were able to provide supplemental information that supported our initial investigation.  
> **Results**. The findings show that the page blurs the dichotomy between what Escarpit has referred to as the cultured and popular circuits of the book trade. Such findings suggest the possibility that booksellers can reach out to both circuits. It also suggests that librarians, through social media, can also serve the very different needs of members of the cultured and popular circuits of literary taste.  
> **Conclusion**. We embarked on this study as contributors to current literature and also as documenters of how print culture is not just in existence in the social media realm of the digital landscape, but also how this playing field allows for both cultural and commercial agenda to be achieved with a far greater audience and efficiency compared to what other genres are able to offer.

<section>

## Introduction

Although many have consigned brick and mortar bookstores to the rubbish pile of history, it is clear that the days of the bookstore are not quite over and that perhaps much life is left in them yet. Australian booksellers, for example, have certainly found a place for themselves ([Li, 2010](#li10)). The same holds true for Canadian bookstores ([Williams, 2014](#wil14)), independent bookstores (or _indies_) in the United States ([Rosen, 2014](#ros14)), as well as those located in Singapore ([Luyt and Heok, 2015](#luy15)). The good use of new technologies may play a role in their survival.

Even a cursory examination suggests that independent bookstores in Singapore are avid users of social media. Some examples of popular Facebook pages are that of independent bookstore _[BooksActually](https://www.booksactuallyshop.com)_, with more than 31,000 likes; _Woods in the Books_, another indie, which considers itself more of a picture bookshop, with a little over 5,700 likes; and _Basheer Graphic Books_, which has over 52,000 likes.

This presence on social media, when taken at face value, may come off simply as businesses and institutions trying to leverage social media to broaden their reach, but a closer examination uncovers a very interesting phenomenon that is yet to be explored. Can the square peg that is the printed book fit itself into the round hole of cyberspace? Or, more specifically, can social media play a vital role in the resuscitation of an allegedly dying print culture? This study attempts not just to answer these questions, but also to excavate more of what this phenomenon may reveal. Given time and resource constraints, however, this study examines only the Facebook use of one of these stores, _BooksActually_.

It is hoped that the results can benefit both other independence bookstores, but also libraries and librarians, seeking to incorporate social media into their operations.

## Literature review

Three bodies of literature are relevant to this work: the sociology of print, marketing, and genre analysis. Each is covered in the following sections in order to lay down the contextual foundations of this study.

### The sociology of print

One of the first scholars to acknowledge the various forces that influence the life cycle of a book was Darnton ([1982](#dar82)), who proposed a model of print culture, which he dubbed, '_the communications circuit_'. These forces include: intellectual influences and publicity, economic and social conjuncture, and political and legal sanctions, all of which are engulfed by a circuit of authors, publishers, printers, shippers, booksellers, and finally, readers. Darnton’s framework, while popular in studies of the history of the book since its inception, has been critiqued by fellow book historians Adams and Barker ([1993](#ada93)). Their point of contention lies in the people-centric (instead of book-centric) take on print culture, regarding this factor as the weakness of Darnton’s model. They have, then, proposed a new model to the study of the book, inverting the dimensions of Darnton’s work by encasing the publication circuit within the socio-economic juncture composed of intellectual influences, political, legal, and religious influences, social behaviour and taste, and of course, commercial pressures. Regardless of which perspective one wishes to use, both models reflect an important consideration in the study of books; that such materials cannot be separated from their socio-economic context.

Before Darnton, Escarpit ([1971](#esc71)), another influential book historian, had acknowledged that a strong relationship exists between the existence of a book and the existence of a public. Escarpit further likens the book and the public to currency and population, pointing out that the currency frontiers for the book are language and illiteracy. He stresses that the ability to read a book and to understand the language it was written in are prerequisites to its use. Escarpit argues that an examination of literary distribution must be carried out in relation to social groups, as these groups each possess a unique cultural need. He regards what he calls the '_cultured group_', as having the clearest sense of literary identity. He further describes the members of this group as '_persons having received an intellectual training and an esthetic education advanced enough to enable them to exercise literary and personal judgments, having sufficient time to read, and having enough money to buy books with regularity_' (p. 59). As a closed caste, according to him, they complete a closed circuit, being the ones who make and consume literature themselves.

In contrast to the cultured circuit, Escarpit argues that there exists a popular circuit, whose networks of distribution usually cater to individuals '_whose upbringing endows them with an intuitive literary taste, but not with an explicit or reasoned power of judgment_' ([Escarpit, 1971](#esc71), p. 59). He notes that these individuals tend to belong to the lower middle class, whose working conditions render reading as a chore, and whose lack of resources to purchase books does not make their perspectives on reading any better. While this might be the case, Escarpit still acknowledges that their literary needs are of equal importance and of similar type and quality to those of the cultured circuit. But he notes that these needs have always been satisfied by outsiders, making them almost non-participants in the literary game, pushing booksellers and publishers alike to select only titles that cater to the small sphere of the cultured circuit.

For Escarpit this lack of participation poses a problem, as it creates a succession of limiting choices. He writes that '_the publisher’s choice of his authors’ works limits the choice of the bookseller, who then limits the reader’s choice_' ([Escarpit, 1971](#esc71), p. 65), going on to describe such interaction (or lack thereof) as a regrettable waste, with great talent and material not being distributed because of society’s apparent fixation on didactic concerns. Hence, even as publishers, booksellers or even libraries wish to serve popular culture, they remain enslaved to the literary standards of the cultured circuit in order to keep themselves in operation.

As Coser, Kadushin and Powel ([1982](#cos82)) put it, the publishing industry is engulfed between commercial requirements and restraints, as well as its symbolic cultural responsibilities and obligations, which may be rendered as overlapping yet seemingly opposing roles. On the one hand, the industry is expected to sustain the significance of literature, on the other, they need, in the most basic sense, to make profits. This is similar to the point of Chartier, who asserts that the world of the reader is '_socially defined by the competency, conventions, expectations, and practices of reading that he shares with others_ ([Chartier, 1997](#cha97), p. 10). Escarpit notes that such circumstances place the cultured circuit, too, into some sort of entrapment, as they are expected to behave like connoisseurs, with the threat of being labeled as primitive if found to be in enjoyment of lowbrow materials. Hence, he contends that people must bring literature down from its pedestal and strip it from its social taboos, so that the true potential of literature is revealed.

### Marketing literature

Kotler defines marketing as,

> the business function that identifies current unfulfilled needs and wants, defines and measures their magnitude, determines which target markets they can best serve, and decides on appropriate products, services and programmes to serve these markets. Thus marketing serves as a link between a society’s needs and its pattern of industrial response ([Kotler, 2011](#kot11), p.xiii).

Squires comments that literature has had a '_close yet difficult relationship to marketing_' ([Squires, 2007](#squ07), p. 40) as bookselling, as has been observed by book historians, revolves in a marketplace which balances commercial demands and cultural values. Delany, on the other hand, argues that marketing is actually a good fit for reading matter, with its contemporary focus on market segmentation and cultural niches, in lieu of product differentiation ([2002](#del02)). He emphasises that consumers nowadays are not defined by a vertical structure, as suggested by the work of Leavis ([2011](#lea11)), on the segregation of highbrow, middlebrow, and lowbrow, but a horizontal one that appeals to genres. He says that buyers are now stratified by interests and lifestyle choices and not anymore by social rank, as in previous times. On a related note, Shankar ([2006](#sha06)), in a study on reading groups, found that regardless of the homogeneity of readers in terms of life stage, life experiences, shared history and sociological backgrounds, disparities between '_loving_' and '_hating_' a particular reading material often still exist. Squires interprets this as suggestive of veering away from stereotyping group reactions when it comes to marketing texts. To use her words,

> although marketing activity profoundly influences consumer decisions and readerly interpretations, it is - as an act of representation - still open to debate and argumentation. Marketing, like the texts with which it deals, is open to interpretation by communities and individuals within those communities ([Squires, 2007](#squ07), p. 63).

Bourdieu’s ([1983](#bou83)) viewpoint on the literary marketplace develops from his concept of a field of cultural production, which renders books as cultural artefacts and economic products when seen as possessing value. He stresses that cultural capital is actually the reverse of the audience size, not because the masses will choose works with low cultural value, but rather because the quintessential characteristic of a cultural work that has '_made it_' is disinterestedness; when value does not equate to economic or political profit. In other words, the popularity or mass appeal of a literary work has very little bearing in the cultural field. He further discusses that in the cultural field, the status or position of literary works is dependent on its relationships with other literary works and the other agencies working in the field, such as critics and publishers, among others, reminiscent of the work of Escarpit and Chartier.

As Escarpit ([1971](#esc71)) notes, in the 1950s, it was necessary for bookstores to keep contact with their customers by way of sending personal catalogues, among many other means. Today, we harness the power of the media. Baverstock ([2000](#bav00)) examined the role of media in the marketing and communication of books. She suggests that publishers must bank on public opinion and popular debate, or even the power of word-of-mouth advertising, in getting information on their books to the potential market. As Smith and Vogt ([1995](#smi95)) put it, among all the elements in the marketing communications mix, word of mouth is still the most effective. Escarpit also contends that people heed the recommendations of influential opinion leaders in the literary scene, such as journalists, analysts, critics, judges and members of a governing body. Booksellers and publishers may arguably be added to the list, as Escarpit calls them selectors and filters of materials they wish to promote, and even referring to publishers as ‘_the barometer of popular taste_’ ([Escarpit, 1971](#esc71), p. 64).

### On genre analysis

Bhatia defines genres as,

> language use in a conventionalized communicative setting in order to give expression to the specific set of communicative goals of a disciplinary or social institution, which give rise to stable structural forms by imposing constraints on the use of lexico-grammatical as well as discoursal resources ([Bhatia, 2012](#bha12), p. 241).

This is congruent with Miller’s definition of genres as ‘_a conventional category of discourse based in large-scale typification of rhetorical action; as action, it acquires meaning from situation and from the social context in which that situation arose_‘ ([Miller, 1984](#mil84), p. 163). Bhatia also acknowledges that while genres are concerned with recurring rhetorical contexts with a defined set of communicative purposes, it is also important to consider that they are not static in nature. As Berkenkotter and Huckin ([1995](#ber95)) put it, genres are inherently dynamic structures of rhetoric, malleable to accord with the conditions of how they are intended to be used. They assert that genres are best treated as ‘_a form of situated cognition embedded in disciplinary cultures_‘ (p. 6), suggesting that genres are fashioned by discipline. In addition, Bax ([2011](#bax11)) stresses that certain features such as location, topic focus, visual aspects and layout, length, structure, subjects/agents/focus, style and register, grammar, and lexis (i.e., jargon and technical language) are determined by the function of the genre. Bhatia’s take on the facets of genres, particularly when applied to the professional context is much simpler, with context defining functions, functions driving features, and features shaping structures. Widdowson ([1998](#wid98)) sheds further light on the matter by spelling out the purpose of genre analysis, identifying the conventions in which language is used for domains of activities, in his work, giving emphasis to the ones which are professional and occupational in nature. This provides a two-dimensional characteristic of genre analyses: that of linguistic and social practices.

While Paltridge ([2012](#pal12)) emphasises the linguistic element of genres by simply defining it as a kind of text, Bhatia on the other hand contends that, in order to be able to advance analysis of professional genres, it is imperative for researchers to go beyond texts and to delve deeper into the socio-pragmatic space within which genres operate. His perspective can be described as having professional culture engulfing professional practice, which in turn envelops the genre, which impacts the text. An examination of the bigger picture, according to him, is a prerequisite to answering the quintessential questions in professional genre analysis, such as how members of the specific professional community use language the way they do, how they manipulate these to achieve their corporate objectives, and how they ‘_bend generic norms_’ to accomplish ‘_private intentions_’ in addition to, and within, the framework of shared generic conventions ([Bhatia, 2012](#bha12), p. 250).

Some studies on genre analysis reflect a particular interest in genres in the digital landscape (see [Yates and Orlikowski, 1992](#yat92); [Miller and Shepherd, 2004](#mil04); [Shepherd and Watters, 1998](#she98); [Herring, Scheidt, Bonus and Wright, 2004](#her04)). More contemporary research has started to explore social media as a genre, as in the work of Halavais and Lackaff ([2008](#hal08)) on Wikipedia and Riemer and Richter ([2010](#rie10)) on Twitter. Lomborg ([2011](#lom11)) argues that social media are different from other genres by virtue of their dynamic environment, partly because of their direct feedback structure as well as the number of participators in the production and negotiation of the genre. He further notes that genre analysis on social media must be conducted with much regard and sensitivity to the context from which these genres emerge, and how they are ‘_configured, negotiated, stabilized, and even, over time, possibly destabilized_‘ ([2011](#lom11), p. 69). As Yates and Orlikowski argue, ‘_A particular instance of a genre need not draw on all the rules constituting the genre_‘ ([1992](#yat92), p. 302). It is also interesting to note that apart from their dynamic nature, social media genres such as Facebook have also been gaining popularity with business operations ([Vorvoreanu, 2009](#vor09)), as consumers have been moving to these platforms at staggering rates ([Treadaway and Smith, 2012](#tre12)).

## Method

Singapore is a logical choice for conducting a study on the usage of social media by book stores because of its high literacy rate of 96.7% ([Singapore. _Ministry of Social and Family Development_, 2014](#sin14)), its high usage of social media with 91% of its population using the platform as of the last quarter of 2014 ([Statista, 2015](#sta15)), and its location in Asia, where there exists relatively less research on the subject. A number of bookstores were initially examined for their suitability for the study. In the end _BooksActually_ was selected as it possessed relatively more vibrancy not just in its self-presentation on social media, but also in the perception and reception of it on the Internet. It has been described by many blogs (personal and professional alike) as one of the most ‘_inspiring_‘, ‘_beautiful_‘, and ‘_thriving_‘ bookstores not just in Singapore, but also among those around the world ([Messy Nessy Chic, 2013](#mes13); [McCroskrie, 2017](#mcc17); [Timothy, 2011](#tim11)). It has also been listed in travel websites such as one of the places to visit in the country ([TripAdvisor](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.tripadvisor.com.sg%2FAttraction_Review-g294265-d2338939-Reviews-BooksActually-Singapore.html&date=2017-05-06); [YourSingapore](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.visitsingapore.com%2Fwalking-tour%2Fculture%2Fin-the-neighbourhood-tiong-bahru.html&date=2017-05-06); and [Yelp](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.yelp.com.sg%2Fbiz%2Fbooksactually-singapore&date=2017-05-06)). The vitality of activity on its Facebook page was also evident, with likes and comments frequently appearing.

Although _BooksActually_ also maintains other social media accounts (such as Tumblr and Instagram), we have chosen to conduct this study on Facebook as it has a relatively larger audience, allows for more features to be analysed, and provides a more clear-cut policy on the use of its publicly posted data (see [Facebook, 2015](#fac15)).

The bookstore's Facebook posts, collected in reverse chronological order in March 2015 and cleaned using the software NVivo, were analysed. The cleaning process entailed harvesting only the posts made by _BooksActually_ itself (trimming the data from 1,083 to 415 posts). We initially employed purposive sampling of Facebook posts to find out how _BooksActually_ presents facets of print culture. Responses (comments and likes) were thus left out in the primary analysis, but were able to provide supplemental information that supported our initial investigation. The span of posts collected was from the 19th of February 2013, the date of the first post, to the 18th of March 2015\. The study also employed theoretical sampling through saturation of categories, by virtue of what Charmaz describes as the state when ‘_gathering fresh data no longer sparks new theoretical insights, nor reveals new properties of these core theoretical categories_‘ ([Charmaz, 2006](#cha06), p. 113). This was achieved less than halfway through the data so that coding procedures were consequently stopped at the 200th post. NVivo was selected as the means to harvest the posts as it offers a descriptive quantitative presentation of the data necessary for analysis, while allowing for a qualitative, more in-depth treatment by means of coding.

The characteristics of the posts were first examined based on Bhatia’s guiding list of facets for professionally-used (that is, for corporate or business purposes) genres: context, functions, features, and structure. Following his model, we examined the socio-pragmatic space upon which this particular genre operates (Singapore) and rendered this as the context. This was taken into account at every point of analysis made for the rest of the three facets, but more evidently so in the section on functions. As genre analysis involves analysing the overall purpose of a genre, we also looked at the objective or intention of each post. For this, we relied primarily on what has been said in the literature on marketing print materials and have put the findings of these works into consideration in coding for purpose. Evidence of intertextuality between the posts and other online sources as well as print media were also traced. An analysis of the structure and features of the Page was carried out, deriving cues from the data rather than the research literature, as Facebook, being a dynamic genre, has been changing periodically since its inception and its current structural facets may no longer be reflected in previous studies. Lastly, to allow for unique characteristics from this particular genre to emerge, new coding categories that were not covered in previous literature on the subject matter have been added and were taken into consideration in the analysis.

## Findings

Here we discuss Bhatia’s concepts of structure, features and function as they apply in this particular case. Analyses on the context have been reserved for the discussion part of this paper.

### Structure

Coming to the page of _BooksActually_, certain structural features that are not present in other genres, online or otherwise, are observed. At the top portion rests a cover photo and a profile picture beside the name of the page, __BooksActually__. Below it is an overview of what the Page represents, which says, _Book Shop • Book & Magazine Distribution • Vintage shop_. An option to _like_ the page (i.e., to subscribe to its posts) is also easily accessible, adjacent to a _message_ button, suggesting open communication lines between the Page and its followers. Interestingly, a _Shop Now_ button is also located at the top part of the Page, which directs the user to the actual online bookshop where one can place an order. On the left panel, an overview of the bookstore and the page statistics can be found, including its 31,000 likers (showing us which ones on our Facebook friend lists have also liked the Page), how many people have _been here_ by virtue of using the check-in feature of Facebook (4,189 as of this writing), store hours (with an option to display additional information, which directs us to another page with the bookstore address and date when the bookstore was founded [08 October 2005]), a short description which reads, _ALL BOOKS AND SOME MORE !!_, information on the availability of parking space for visitors, its phone number, e-mail address, website, and a long description, which reads:

> _BooksActually_ (est. 2005) is an independent bookstore specialising in Literature (including obscure and critical works). In our bookstore, you can often find literary trinkets in the form of stationery and other lovely tchotchkes. We publish and distribute books under our imprint Math Paper Press. We also hand-stitch notebooks and produce stationery under Birds & Co.
> 
> _BooksActually_ is now housed at No. 9 Yong Siak Street, in the heart of Tiong Bahru. Come, say hello!
> 
> www.<em>BooksActually</em>shop.com  
> www._BooksActually_.tumblr.com  
> www.twitter.com/_BooksActually_  
> Instagram: @_BooksActually_

A _Suggest Edits_ option for this section is available for users, which allows a user to suggest changes for the Page to the page owner. Going back to the store’s main Facebook page, one can also notice an option to _Invite friends to like this page,_ and to view and write reviews with a five-star scale. Based on 319 reviews, _BooksActually_ has garnered an average of 4.4 out of 5 stars, with the most recent reviews saying ‘_Comprehensive selection of books, friendly staff and was greeted by a friendly cat during my visit :),_‘ ‘_Not just a book store but a good experience of the small space and you can find books that you would read,_‘ and ‘_Not just a bookshop, but actually a shrine for anyone who loves to read, to write and wants to learn more about Singapore._‘ Below this section is an overview of photos posted by the Page in thumbnail format, aptly labelled _Photos_; applications used by the page, labeled _Apps_; then _Videos_; _Reviews_ (a more comprehensive presentation than the one in the overview above); _Posts to Page._ which comprises user messages that are publicly posted on the Page’s timeline; and other Facebook pages _liked_ by the Page, made up mostly of author pages and other book and literature-related pages.

The most recent posts to the bookshop page primarily comprise queries on store hours, such as, ‘_Hi there, what time do you close today? I went at 7:30PM yesterday and was greeted by a closed shop with Cake, Pico & Lemon inside :(_‘, offers on leasing opportunities, and even comments such as ‘_I am watching your documentary. I LOVE it! And I LOVE your store! I am also an independent author: Published my first (non-fiction) book (about my life and the Universe) last year called 'The Elemental Child' @ http://amazon.com/Elemental-Child-Cheshire-Hollows/dp/1508763631 – I want to travel to Singapore now to come to your shop! I am just so happy that places like this exist in the world! LOVE._‘ Cake, Pico & Lemon, the resident cats of _BooksActually_, unsurprisingly, have their own Facebook page as well.

<figure>

![Figure 1](../p746fig1.png)

<figcaption>

Figure 1: _BooksActually_ Facebook page.</figcaption>

</figure>

The aforementioned sections, save from the topmost expanse, are all situated on the left-hand column of the Facebook page, as seen in Figure 1\. The middle column is where the actual posts are situated. On the rightmost part, almost outside the confines of the Page, is an option for users to ‘_Create Page_‘ for their own Facebook pages, an option to advance to posts by year _(Recent, 2015, 2014, 2013...)_, and some sponsored advertisements.

### Features

Quotations from books comprised most of the posts, with photographs of printed book covers occupying much space in its Facebook albums. However, some posts were actually less relevant to the topic of print culture or even literature for that matter, yet were indicative of an image projection of being artsy and cultured. For instance, an article on a documentary on Kurt Cobain, the lead singer of the grunge band Nirvana, was featured on the Page. A few other examples included an article on French artist Roman Moriceau and German actress Maryam Zaree, an interview with a vendor whom they call ‘_The Vinyl Nazi_‘, who refuses to sell long-playing discs to speculative buyers and hipsters, and a video clip of the song Dance With Me by French cover band Nouvelle Vague, all of which having no apparent connection with books, print culture, or literature.

<figure>

![Post sample](../p746fig2.png)

<figcaption>Figure 2: Post sample</figcaption>

</figure>

With a few exceptions, the posts were of but a few lines, no more than a single paragraph, except on occasions when they would relay information on certain events and place all relevant details in a single post. This used about two to three paragraphs of space in their Timeline. As its page matured, however, _BooksActually_ started to employ _Event_ pages (see Figure 3), which took up less space. This shift from paragraphs to events echoes the studies stating that social media content tends to employ bite-sized information ([Safko, 2010](#saf10); [Lowe and Laffey, 2011](#low11)), particularly when used for advertising ([Baker and Green, 2008](#bak08)). Interestingly, this device is also employed by librarians in reaching their young clients ([Dowd, Evangeliste and Silberman, 2010](#dow10); [Mizrachi and Bedoya, 2007](#miz07)).

<figure>

![Event post sample](../p746fig3.png)

<figcaption>Figure 3: Event post sample</figcaption>

</figure>

Hashtags were liberally used for most of the posts, suggestive of the functional aspects hashtagging can provide, such as findability, as mentioned by Daer, Hoffman and Goodman ([2014](#dae14)). Obvious applications for findability are the use of common words (i.e., words that other people might also use for hashtags) such as #books, #language, #travel, #life, #future, #writing, and #words, among others. This entails that people who use and click on hashtags used by _BooksActually_ (regardless of whether they have liked the Page or not) will be able to find and view the posts on the Page, providing an even larger audience than its following of 31,000 Facebook users.

Furthermore, Daer _et al._ have also noted that hashtags have evolved to employ metacommunicative tagging, which they described as ‘_a sub-genre, arising from specific needs or demands perceived by users and determined (in part) by what the medium makes possible_‘ (p.2). They have grouped meta-communicative hashtag functions into (1) emphasising something in the post, (2) iterating to express humour or to exhibit a parody, (3) critiquing or expressing judgment on an issue, (4) identifying characteristics, moods, or reflections of the author of the post, and (5) rallying to raise awareness, gather support, or increase publicity. It is interesting to note that all five of these functions were evident in the _BooksActually_ page, with emphasising hashtags such as:

> If you have any young friends who aspire to become writers, the second greatest favor you can do them is to present them with copies of ‘The Elements of Style’. The first greatest, of course, is to shoot them now, while they’re happy. * Dorothy Parker #WritingSchool101 #truefact #itsahardknocklife #elementsofstyle #printisnotdead #dorothyparker.

some iterating, as in:

> Die die must sell books. #booksellerlogic #bookstorephilosophy

others critiquing the Formula1 event popularity, as opposed to attendance of art-related occasions, as in:

> #whocaresaboutF1 #notjustarts #morethanarts #OffTheRecord #SGArtsFTW  
> #Ifyoutellthetruthyoudonotneedagoodmemory

identifying as exemplified in:

> Change Minds is not like Changing Socks, Mind You! :D #_BooksActually_OnlineStore #thisoneonlyhasonerule #youjustneedtochangeyourmindtobecreative

and rallying, in this case for Singaporean literature, with hashtags:

> #SGArtsFTW,  
> #sglitftw,  
> #SGAuthorsFTW,  
> #SGPoetryFTW, and  
> #SGProseFTW.

A closer inspection also reveals that some hashtags are suggestive of the audience the Page wishes to reach, with the use of:

> #catporn,  
> #andbecausegenerallymostAsianareslightlylactoseintolerant,  
> #wecanallLaoSaifacestogether,  
> #bookcrazy,  
> #respect,  
> #nobullshit, and  
> #suckitup.

As has been suggested by the hashtags, humor and sarcasm were also commonly used in the composition of posts.

### Function

During our coding, we have found evidence of what Escarpit and Coser et al. describe as the seemingly contradicting yet overlapping commercial and cultural functions of the business of books. For one, the most references in the Post belonged to the codes ‘_Quoting an originally print source_‘ (90 references) and ‘_Sharing general life advice or reflections_‘ (41), followed by ‘_Promoting events involving the store_‘ (34) and ‘_Directly selling a book, magazine, or print material_‘ (26). The references which were coded under ‘_Sharing general life advice or reflections_‘ were by and large also quotations from books, which is likewise the case for the nodes ‘_Conveying reflections on love and romance_‘ (10), ‘_Imparting advice on writing_‘ (5), and ‘_Sharing reflections on reading_‘ (5). It can be observed, however, that the print sources referenced were generally by foreign authors, mostly from the West. Some favorites (quoted more often than the others) are American filmmaker and author [Miranda July](https://en.wikipedia.org/wiki/Miranda_July); British writer, [Jeanette Winterson](https://en.wikipedia.org/wiki/Jeanette_Winterson); French philosopher, [Albert Camus](https://en.wikipedia.org/wiki/Albert_Camus), Russian author and philosopher, [Fyodor Dostoyevsky](https://en.wikipedia.org/wiki/Fyodor_Dostoyevsky); American author, [Sylvia Plath](https://en.wikipedia.org/wiki/Sylvia_Plath); English writer, [Virginia Woolf](https://en.wikipedia.org/wiki/Virginia_Woolf); and American author and philosopher [Henry David Thoreau](https://en.wikipedia.org/wiki/Henry_David_Thoreau). The words of more contemporary Western authors such as [Chuck Palahniuk](https://en.wikipedia.org/wiki/Chuck_Palahniuk), [Jack Kerouac](https://en.wikipedia.org/wiki/Jack_Kerouac), [Stephen King](https://en.wikipedia.org/wiki/Stephen_King), and [David Levithan](https://en.wikipedia.org/wiki/David_Levithan) were also among the seventy-one references from foreign works. It can be also noticed that they come from works that require a university or at least a high school level of reading, suggesting a communicative function to an audience with at least that level of education, with an appreciation for the Western use of English as well. Of these references, two were from Japanese writers, [Natsume Soseki](https://en.wikipedia.org/wiki/Natsume_S%C5%8Dseki) and [Banana Yoshimoto](https://en.wikipedia.org/wiki/Banana_Yoshimoto), but the works featured were also written in English. Looking at this with an intertextuality lens, while the Page also employed the sharing of articles from online sources (all Western), these account for only nine references, a measly figure compared to the number of quotes lifted from printed texts.

Singaporean and Singapore-based authors were not left out, albeit having a relatively thinner slice of the quotation pie. Of the sample used, nineteen were quotes from [Michelle Tan](http://tanmichelle.com/), [Boey Kim Cheng](https://en.wikipedia.org/wiki/Boey_Kim_Cheng), [Cheryl Julia Lee](https://singaporepoetry.com/tag/cheryl-julia-lee/), [Cyril Wong](https://en.wikipedia.org/wiki/Cyril_Wong), [Jollin Tan](http://www.webcitation.org/query?url=http%3A%2F%2Farchive.obscured.sg%2F2012%2F05%2Fjollin-tan.html&date=2017-05-06), [Ann Ang](http://www.webcitation.org/query?url=http%3A%2F%2Fmascarareview.com%2Fann-ang%2F&date=2017-05-06), [Joshua Ip](https://en.wikipedia.org/wiki/Joshua_Ip), [Pooja Nansi](http://poojanansi.com), '[Atelier Hoko](https://www.hokostudio.com)' (a collective), [Loh Guan Liang](https://guanliangwrites.wordpress.com/about/), [Alvin Pang](http://eresources.nlb.gov.sg/infopedia/articles/SIP_463_2004-12-23.html), [Stephanie Ye](https://stephanieye.com), [Mayo Martin](http://a-list.sg/one-small-voice-mayo-martin/), and [O Thiam Chin](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.straitstimes.com%2Flifestyle%2Farts%2Fo-comes-full-circle&date=2017-05-30). Interestingly, an examination of the _BooksActually_ online store reveals that all fourteen authors and their quoted works were published by Math Paper Press, the publishing arm of _BooksActually_. In addition, a quote from said authors generally comes with a link to the online bookstore, often juxtaposed with ‘_Buy it Now_‘ or ‘_Get it Now_‘. With very few exceptions, this was not evident in the quotes from foreign authors.

## Context

The different treatment accorded foreign and local authors is a good place to begin a deeper analysis of the use of Facebook by _BooksActually_. Why does the bookstore use an abundance of quotations from foreign authors, i.e., authors whom they do not publish and obviously do not need the bookstore’s help in getting their work into the marketplace?

If one follows Escarpit, Coser and Chartier, it becomes clear why _BooksActually_ quotes foreign authors so extensively regardless of whether or not the store needs to sell those particular books. Instead the association of these authors with the bookstore helps it to maintain the standards of those identifying with the cultured circuit. Further support for this view is given by Chin ([2006](#chi06)) who explains that the use of the English language in postcolonial Singapore is associated with an ideology of freedom, coupled with a perspective that those who write in English have a higher degree of aesthetic sensibility. She relates that, in Singapore, works in English are not merely favoured but are even celebrated by the literati. This is indicative of the market that the _BooksActually_ needs to reach, the small sphere of the cultured circuit, the makers and consumers of literary works themselves.

A similar explanation may be provided for the existence of non-book posts on the Facebook page. While Kurt Cobain may be considered to fall under popular culture, the same cannot be said of French artists, German actresses, vinyl records, and French cover bands. These do not cater to the masses, especially when contextualised in Singapore. And while these appeal to a relatively small audience, it is reflective of Bourdieu’s concept of disinterestedness, with cultural capital inversely proportional to the audience size. This may be interpreted as an attempt by _BooksActually_ to present the bookstore as a highly cultured establishment.

Materials on Singlish (Singaporean English) were promoted and quoted from time to time, such as a post promoting the availability of local poet Joshua Ip’s Sonnets from the Singlish and another post quoting the entirety of his love poem Chope (Singlish for reserve, as in reserving or ‘_choping_‘ a seat), also from the same anthology.

The projection of a cultured image is also evident in that the language used on the Page itself, not just the material it highlighted, was international English. There were very minimal traces of Singlish use on the Page itself. The status updates and even commentaries on shared posts from _BooksActually_ generally employed the Western grammatical rules of the English language. This may be suggestive of the stigma attached to the use of Singlish. As Leong notes in his study on Language and Society in Singapore, ‘_...well-educated Singaporeans can speak standard English as well as the devalued Singlish, while those who receive less education are restricted to Singlish and the vernaculars_‘ ([Leong, 2002](#leo02), p. 363). Leong further comments that Singaporeans tend to look down on individuals who are limited to communicating in Singlish, and would prefer to use the English language the way native speakers would, in order to present themselves as more educated or belonging to the upper class.

However, the use of Facebook by _BooksActually_ is not simply to appeal to Escarpit's cultured circuit. There are several elements that speak of the possibility of an engagement with the mass circuit. To begin with, it is important to note that the ease of penetration for Facebook (as anyone of legal age with an e-mail address can sign up for a free account) renders it a network of distribution for the popular circuit, that is, mass culture. The movement from using the Timeline as a space to advertise events to Event pages and the consequent reduction of space and rendering of information in chunk-sized blocks is another example of catering to what is traditionally still as the mass market. As Barnes, Marateo and Ferris ([2007](#bar07)) suggest, employing multimedia materials in small chunks is the way to cater to an audience with a very short attention span, a young demographic which they refer to as the net generation, but Escarpit would likely call the popular circuit. Finally, a closer examination of the hashtags used by _BooksActually_ suggests appeals to both cultured and mass audiences. It has already been noted that the use of common words as hashtags opens up a much larger audience than the 31,000 Facebook users that officially follow the _BooksActually_ page so that, at least potentially, this informal use of language makes posts, and hence the bookstore, accessible to both popular and cultured circuits.

## Conclusion

That the Facebook page of _BooksActually_ involves a leakage between Escarpit’s two circuits is important, rendering the two seemingly opposing and mutually exclusive groups as more fluid. Escarpit himself contended that people must bring literature down from its pedestal, strip it from its social taboos, in order for it to realise its true potential as a means of communication and understanding. The actual extent of this leakage is of course open to debate and further empirical studies.

Our findings benefit not only booksellers who wish to fulfill their cultural and more so their commercial roles, but also librarians, who, like booksellers, act as gatekeepers for readers, using their own literary barometers of what is suitable for their client base. Librarians who serve wider and more diverse audiences, especially public librarians, may find these findings useful as they cater to the very different needs of the members of cultured and popular circuits. The use of novel technologies such as social media, or in this case, Facebook, offers an efficient means to make this task less demanding. By maintaining a Facebook page, they create for themselves a communication channel that reaches both circuits at the same time.

Another significant aspect of this study is that it contributes to the discussion about what is rendered by some researchers as the ‘_worldwide shift from a 19th-century print culture via a 20th-century electronic culture to a 21st-century digital culture_‘ ([Deuze, 2006](#deu06), p. 63). By showcasing printed materials being sold and promoted through an electronic medium and reacted to by means of a digital culture, this study suggests that it is not necessarily a shift that occurred in centuries past, but a coexistence of print and digital media and cultures in more recent years. The fact that _BooksActually_ as well as the other Singaporean brick and mortar bookstores have a loyal following on an online platform such as Facebook is testament to that. This may serve as a springboard for further discussions on the matter, and may finally put an end to the long-standing debate whether print culture is actually dead, dying, or thriving.

We believe it is fitting to end this study by saying that we present but an examination of the changing field of print culture, the dynamic genre that is Facebook, and the Singaporean literary scene as they are today. As in bookstores, we do so with two roles in mind: as a contributor to current literature, as this phenomenon, to our knowledge, has not yet been explored in this context before, and also as documenters of how print culture is not just in existence in the social media realm of the digital landscape, but also how this playing field allows for both cultural and commercial agenda to be achieved with a far greater audience and efficiency compared to what other genres are able to offer. We further hope that other researchers study the facets that escape the scope of this study, such as the interactivity that is gifted to users by this specific genre, as well as to take future snapshots of the changes in the representation of printed literature in social media, in the context of Singapore or otherwise.

## Acknowledgements

The authors wish to thank Prof. Natalie Pang for her guidance on this manuscript.

## <a id="author"></a>About the authors

**Karryl Kim Sagun** is a PhD candidate at the Wee Kim Wee School of Communication & Information, Nanyang Technological University, Singapore. Her research interests include print culture, publishing, social media, and library and information studies. She can be contacted at [karrylki001@e.ntu.edu.sg](mailto:karrylki001@e.ntu.edu.sg)  
**Brendan Luyt** is Associate Professor and Director of the PhD Programme at the Wee Kim Wee School of Communication & Information, Nanyang Technological University, Singapore. He received both his MLIS and PhD degrees from the University of Western Ontario, London, Ontario, Canada. His research focuses broadly on the social and policy landscape of information access. At the present time he is especially interested in the study of the history of information institutions and Wikipedia as a social phenomenon. He can be contacted at [brendan@ntu.edu.sg](mailto:brendan@ntu.edu.sg)

</section>

<section>

## References

<ul> 

<li id="ada93">Adams, T. &amp; Barker, N. (1993). A new model for the study of the book. A potencie of life: books in society. London: British Library.</li>

<li id="bak08">Baker, S. &amp; Green, H. (2008, February 20). <a href="http://www.webcitation.org/6qNE8OaRi">Social media will change your business.</a> <em>Bloomberg</em>. Retrieved from https://www.bloomberg.com/news/articles/2008-02-20/social-media-will-change-your-businessbusinessweek-business-news-stock-market-and-financial-advice. (Archived at http://www.webcitation.org/6qNE8OaRi).</li>

<li id="bar07">Barnes, K., Marateo, R. C., &amp; Ferris, S. P. (2007). Teaching and learning with the net generation. <em>Innovate: Journal of Online Education, 3</em>(4), 1-8</li>

<li id="bav00">Baverstock, A. (2000). <em>How to market books.</em> London: Kogan Page Publishers.</li>

<li id="bax11">Bax, S. (2011). <em>Discourse and genre: using language in context</em>. Basingstoke, UK: Palgrave Macmillan.</li>

<li id="ber95">Berkenkotter, C. &amp; Huckin, T. N. (1995). <em>Genre knowledge in disciplinary communication: cognition/culture/power.</em> Mahwah, NJ: Lawrence Erlbaum Associates, Inc.</li>

<li id="bha12">Bhatia, V. K. (2012). Professional written genres: I. <em>The Routledge Handbook of Discourse Analysis.</em> London, New York, NY: Routledge, 2012.</li>

<li id="bou83">Bourdieu, P. (1983). The field of cultural production, or: the economic world reversed. <em>Poetics, 12</em>(4), 311-356.</li>

<li id="cha06">Charmaz, K. (2006). <em>Constructing grounded theory: a practical guide through qualitative analysis.</em> London: SAGE Publications.</li>

<li id="cha97">Chartier, R. &amp; Friedman, E.D. (1997). <a href="http://www.webcitation.org/6qJ9GW2OB">The end of the reign of the book</a>.  <em>SubStance, 26</em>(1), 9-11 Retrieved from http://bit.ly/2qSMqRL (Archived by WebCite&reg; at http://www.webcitation.org/6qJ9GW2OB)</li>

<li id="chi06">Chin, G. V. (2006). The anxieties of authorship in Malaysian and Singaporean writings in English: locating the English language writer and the question of freedom in the postcolonial era. <em>Postcolonial Text, 2</em>(4), 1-24.</li>

<li id="cos82">Coser, L. A., Kadushin, C. &amp; Powel, W. W. (1982). <em>Books (the culture commerce of publishing)</em>. New York, NY: Basic Books.</li>

<li id="dae14">Daer, A. R., Hoffman, R. &amp; Goodman, S. (2014). Rhetorical functions of hashtag forms across social media applications. In <em>Proceedings of the 32nd ACM International Conference on the Design of Communication</em>. [CD-ROM] (p. 16). New York, NY: ACM.</li>

<li id="dar82">Darnton, R. (1982). What is the history of books? <em>Daedalus, 11</em>(3), 65-83.</li>

<li id="del02">Delany, P. (2002). <em>Literature, money and the market: from Trollope to Amis</em>. Basingstoke, UK: Palgrave.</li>

<li id="deu06">Deuze, M. (2006). Participation, remediation, bricolage: considering principal components of a digital culture. <em>The Information Society, 22</em>(2), 63-75.</li>

<li id="dow10">Dowd, N., Evangeliste, M. &amp; Silberman, J. (2010). Bite-sized marketing: realistic solutions for the overworked librarian. Chicago, IL: American Library Association.</li>

<li id="esc71">Escarpit, R. (1971). <em>Sociology of literature</em> (2nd ed.). London: Frank Cass &amp; Co. Ltd..</li>

<li id="fac15">Facebook. (2015). <em>Data policy</em>. Retrieved from https://www.facebook.com/policy.php.</li>

<li id="hal08">Halavais, A. &amp; Lackaff, D. (2008). An analysis of topical coverage of Wikipedia. <em>Journal of Computer-Mediated Communication, 13</em>(2), 429-440.</li>

<li id="her04">Herring, S. C., Scheidt, L. A., Bonus, S. &amp; Wright, E. (2004). Bridging the gap: a genre analysis of weblogs. In <em>System Sciences, 2004. Proceedings of the 37th Annual HICSS Conference</em> (pp. 11-pp). Washington, DC: IEEE.</li>

<li id="kot11">Kotler, P. (2011). <em>Marketing insights from A to Z: 80 concepts every manager needs to know</em>. Hoboken, NJ: John Wiley &amp; Sons.</li>

<li id="lea11">Leavis, L. E. O. Q. (2011). <em>Fiction and the reading public</em>. London: Random House.</li>

<li id="leo02">Leong, W. T. L. (2002). Who says what to whom: language and society in Singapore. In C. K. Tong &amp; K. F. Lian (Eds.), <em>The making of Singapore sociology: society and state</em> (pp. 351-370). Singapore: Times Academic Press.</li>

<li id="li10">Li, J. (2010). Choosing the right battles: how independent bookshops in Sydney, Australia compete with chains and online retailers. <em>Australian Geographer, 41</em>(2), 247-262.</li>

<li id="lom11">Lomborg, S. (2011). Social media as communicative genres. <em>MedieKultur: Journal of Media and Communication Research, 27</em>(51), 17.</li>

<li id="low11">Lowe, B. &amp; Laffey, D. (2011). Is Twitter for the birds? Using Twitter to enhance student learning in a marketing course. <em>Journal of Marketing Education, 33</em>(2), 183-192.</li> 

<li id="luy15">Luyt, B. &amp; Heok, A. (2015) David and Goliath: tales of independent bookstores in Singapore. <em>Publishing Research Quarterly, 31</em>(2), 122-131.</li>

<li id="mcc17">McCroskrie, K. (2017, March 2). <a href="http://www.webcitation.org/6nQhsk7wh">22 most beautiful bookshops in the world</a> [Web log post]. Retrieved from http://www.skyscanner.net/news/22-most-beautiful-bookshops-world. (Archived by WebCite&reg; at http://www.webcitation.org/6nQhsk7wh).</li>

<li id="mes13">Messy Nessy Chic. (2013, August 28). <a href="http://www.webcitation.org/6nQi3lkKE">10 inspiring bookshops around the world</a> [Web log post]. Retrieved from http://www.messynessychic.com/2013/08/28/10-inspiring-bookshops-around-the-world/. (Archived by WebCite&reg; at http://www.webcitation.org/6nQi3lkKE).</li>

<li id="mil84">Miller, C. R. (1984). Genre as social action. <em>Quarterly Journal of Speech, 70</em>(2), 151-167.</li>

<li id="mil04">Miller, C. R. &amp; Shepherd, D. (2004). Blogging as social action: a genre analysis of the weblog. <em>Into the Blogosphere: Rhetoric, Community, and Culture of Weblogs, 18</em>(1), 1-24.</li>

<li id="miz07">Mizrachi, D. &amp; Bedoya, J. (2007). LITE bites: broadcasting bite-sized library instruction. <em>Reference Services Review, 35</em>(2), 249-256.</li>

<li id="pal12">Paltridge, B. (2012). <em>Discourse analysis: an introduction</em>. London, New York, NY: Bloomsbury Academic.</li>

<li id="rie10">Riemer, P. D. K., Richter, A. &amp; Bohringer, D. W. I. M. (2010). Enterprise microblogging. <em>Business &amp; Information Systems Engineering, 2</em>(6), 391-394.</li>

<li id="ros14">Rosen, J. (2014, January 24). <a href="http://www.webcitation.org/6qNGNrVYW">So far, so good</a>. <em>Publishers Weekly</em>, Retrieved from http://www.publishersweekly.com/pw/by-topic/industry-news/bookselling/article/60797-so-far-so-good.html. (Archived at http://www.webcitation.org/6qNGNrVYW)</li>

<li id="saf10">Safko, L. (2010). <em>The social media bible: tactics, tools, and strategies for business success. </em>John Wiley &amp; Sons.</li>

<li id="sha06">Shankar, A. (2006). A ‘male outsider’ perspective. <em>Consuming books: the marketing and consumption of literature,</em> 114. London, New York: Routledge.</li>

<li id="she98">Shepherd, M. &amp; Watters, C. (1998). The evolution of cybergenres. In <em>System Sciences, 1998., Proceedings of the Thirty-First Hawaii International Conference on System Sciences</em> (Vol. 2, pp. 97-109). Washington, DC: IEEE.</li>

<li id="sin14">Singapore. <em>Ministry of Social and Family Development</em>. (2014). <em><a href="http://www.webcitation.org/6nQjh6iOS">Education &amp; training: literacy rate</a></em>. Singapore: Ministry of Social and Family Development. Retrieved from https://app.msf.gov.sg/Research-Room/Research-Statistics/Literacy-Rate. (Archived by WebCite&reg; at http://www.webcitation.org/6nQjh6iOS). </li>

<li id="smi95">Smith, R. E. &amp; Vogt, C. A. (1995). The effects of integrating advertising and negative word-of-mouth communications on message processing and response. <em>Journal of Consumer Psychology, 4</em>(2), 133-151.</li>

<li id="squ07">Squires, C. (2007). Marketing literature. In <em>The making of contemporary writing in Britain</em>. Basingstoke, UK: Palgrave Macmillan.</li>

<li id="sta15">Statista. (2015). <em><a href="http://www.webcitation.org/6nQim0ih8">Singapore: social network penetration 2014</a></em>. Retrieved from http://www.statista.com/statistics/284466/singapore-social-network-penetration/. (Archived by WebCite&reg; at http://www.webcitation.org/6nQim0ih8).</li>

<li id="tim11">Timothy, I.T. (2011, April 6). <a href="https://www.techinasia.com/booksactually-a-local-bookstore-that-thrives/">BooksActually – a local bookstore that thrives in a digital world</a> [Web log post]. Retrieved from https://www.techinasia.com/booksactually-a-local-bookstore-that-thrives/. [Unable to archive]. </li>

<li id="tre12">Treadaway, C. &amp; Smith, M. (2012). <em>Facebook marketing: an hour a day</em>. Indianapolis, IN: John Wiley &amp; Sons.</li>

<li id="vor09">Vorvoreanu, M. (2009). Perceptions of corporations on Facebook: an analysis of Facebook social norms. <em>Journal of New Communications Research, 4</em>(1), 67-86. </li>

<li id="wid98">Widdowson, H. (1998). 1 The conditions of contextual meaning. In K. Malkjr &amp; J. Williams (Eds.), <em>Context in language learning and language understanding</em> (pp. 6-23). Cambridge: Cambridge University Press.</li>

<li id="wil00">Williams, K.C.M. (2000). Reproduced and emergent genres of communication on the World Wide Web. <em>The Information Society, 16</em>(3), 201-215.</li>

<li id="wil14">Williams, L. A. (2014, February 7). <a href="http://www.webcitation.org/6qNGytnkd">Canadian Indies report encouraging 2013.</a> <em>Publishers Weekly</em>, Retrieved from http://www.publishersweekly.com/pw/by-topic/industry-news/bookselling/article/60978-canadian-indies-report-encouraging-2013.html.  (Archived at http://www.webcitation.org/6qNGytnkd)</li>

<li id="yat92">Yates, J. &amp; Orlikowski, W. J. (1992). Genres of organizational communication: a structurational approach to studying communication and media. <em>Academy of Management Review, 17</em>(2), 299-326.</li>
</ul>

</section>

</article>