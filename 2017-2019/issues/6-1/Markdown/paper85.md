#### Information Research, Vol. 6 No. 1, October 2000

# Making sense of the Web: a metaphorical approach

#### [Lee Ratzan](mailto:lratzan@scils.rutgers.edu)  
School of Communication, Information and Library Studies  
Rutgers University, New Brunswick,  
New Jersey 08854, USA

#### **Abstract**

> The nature of the World Wide Web is unfamiliar to most people. In order to make sense of this foreign environment people describe the unfamiliar in terms of the familiar. Metaphors are often used for this purpose. Since it is important to use the Web effectively it is important to acquire insight on user perceptions. Preliminary results of the Internet Metaphor Project are presented.

## Introduction

The way we describe something can affect the way we perceive it and way we perceive it can affect the way we use it ([Whorf, 1939](#ref2); [Korzybski, 1948](#ref1); [Thaler, 1970](#ref2); [Ross, 1992](#ref2)). Users perceive the Internet and the Web in different ways and they utilize metaphors to describe this environment. The extent of the Web makes direct user studies problematic. Representative statistical sampling tacitly assumes a homogeneous mix. This is not a valid assumption in the vast on-line environment. There is no typical Web user. This research study addresses two associated problems. How do people perceive the Internet and the Web? How can the Internet become an effective and efficient platform of communication about itself?

Metaphors have power. Metaphors have structure. Metaphors are ubiquitous. The power of metaphors to cast cognitive images has a long and rich history ([Johnson, 1946](#ref1); [Lakoff & Johnson, 1980](#ref1); [Deetz & Mumby, 1985](#ref1); [Smith & Turner, 1995](#ref2)). The impact of computer-based metaphors has been extensive ([Rohrer, 1995](#ref2); [Stefik, 1997](#ref2)). One needs only to consider the paradigm of the computer _desktop_ or consider names such as Netscape _Navigator_ or Internet _Explorer_ to view examples of metaphor penetration ([Rothstein, 1996](#ref2); [Johnson, 1997](#ref1)). Since Web development is expanding and Web use as a mass media platform is also increasing, the acquisition of knowledge on the perceptions of the user is a significant area of applied Internet research.

## Metaphors

Metaphors can be placed into several theoretical categories ([Ortony, 1979](#ref2); [Lakoff & Johnston, 1980](#ref1)). Some common examples appear below (Table 1):

<table><caption>

**Table 1: Metaphor types and examples**</caption>

<tbody>

<tr>

<th>Metaphor Type</th>

<th> </th>

<th>Textual example</th>

</tr>

<tr>

<td>spatial</td>

<td> </td>

<td>I fell into a depression.</td>

</tr>

<tr>

<td>ontological</td>

<td> </td>

<td>A mind is a terrible thing to waste.</td>

</tr>

<tr>

<td>personification</td>

<td> </td>

<td>Life is cheating me.</td>

</tr>

<tr>

<td>metonymy</td>

<td> </td>

<td>She is into dance.</td>

</tr>

<tr>

<td>synecdoche</td>

<td> </td>

<td>Cars are choking our roads.</td>

</tr>

<tr>

<td>literal</td>

<td> </td>

<td>The Turnpike is very heavy this morning.</td>

</tr>

<tr>

<td>homonymic</td>

<td> </td>

<td>I am in the room and I am in love.</td>

</tr>

<tr>

<td>poetic embellishment</td>

<td> </td>

<td>"She was my English rose" (Prince Charles)</td>

</tr>

<tr>

<td></td>

<td></td>

</tr>

</tbody>

</table>

Davidson (as cited in [Coyne, 1995](#ref1), page 262) refers to metaphor groupings as untrue statements that are not lies. Metaphors by their power of extension have the power to trap the unwary by promoting faulty logic ([Wilson, 1961](#ref2); [Budd & Ruben, 1979](#ref1); [Cooper, 1997](#ref1)). This phenomenon is the basis for an extended theory of persuasion and propaganda ([Kelling, 1975](#ref1); [Jamieson, 1980](#ref1)). It is important to recognize that any theoretical foundation of Internet metaphors must be grounded in user responses in their own words. The theory of statistical self-selected sampling is appropriate for a preliminary study of Internet metaphors ([Willsky, 1997](#ref2); [Karen, 1997](#ref1)). It is not evident how traditional representative sampling can be accomplished in the Internet context since there is much user variation even within the same Internet domain. A self-selected sample overcomes this difficulty because it functions as a focus group.

## Related studies

There have been only a few analytic studies of Internet metaphors in general or Web metaphors in particular (e.g., [Palmquist, 1996](#ref2); [Gold, 1997](#ref1)). Other studies have often focused on word play ([Cunningham, 1996](#ref1); [Davis, 1997](#ref1)) or general usage ([Rohrer, 1995; 1997](#ref2)). These initial studies suggested the existence of general topical categories from which more refined studies might be derived. Word play studies demonstrated the ubiquitous use of Internet metaphors by the on-line community. The latter has applications toward computer jargon as a separate and distinct linguistic form. Together these and other related studies showed fairly conclusively that the Internet has become a platform for the communication of information in its own right.

Word play studies, albeit entertaining, are intrinsically language specific and thus have questionable generalization value. The methodologies of prior studies were often hampered by the number of accessible participants, the associated cost of reaching them and the difficulty of creating awareness of the study on a global Internet scale. It is neither effective nor efficient to use a traditional paper-based data collection technique to study users scattered across the globe. In addition, many data collection techniques are structured more toward the convenience of the researcher than that of the user. This is a subtle but important distinction. The single greatest weakness of most prior studies was that they could not or did not reveal why users described the Internet as they did. In the absence of this data prior analyses were potentially incomplete and potentially biased by the perspective of the researcher.

## Research questions

A case can be made that a research study on Internet and Web metaphors is ultimately based on two primary, associated research questions.

1.  How do users describe the on-line environment?
2.  Why do they describe it this way?

Many specific research questions can be posed under the aegis of the above. The unit of analysis is the individual user. The unit of data is the metaphor utilized by the user. Metaphor word-stubs identify themes. Some specific research questions addressed in the project study are listed below:

1.  What are the primary Internet metaphor themes as expressed in the user's own words?
2.  Do novices and experts differ in their descriptions of the Internet?
3.  Is it possible to establish an effective self-selecting on-line sampling methodology to study the perceptions of Internet users?
4.  Are there techniques that are especially productive for on-line research studies?
5.  Do preliminary results suggest the possibility that Bayesian conditional probabilities might serve as a predictive marker?

## Research design

### Data collection

#### Sampling

This study used a self-selecting sampling technique rather than a conventional representational sampling approach. The former has the advantage that motivated users provide a response to the study. The disadvantage is that extrapolation must be done with care. Traditional sampling assumes that a representation of the entire sample can be derived from a homogeneous sub-sample. It is highly problematic to form such a sample on an Internet scale with users grounded in diverse ethnic, cultural, political, economic cultures that also have a spectrum of on-line experience.

A preliminary sample of N=350 users was collected in the first few weeks of data collection. Although this is small on in comparison to the universe of all Internet users it is nevertheless four times greater than any prior Internet metaphor study and the first to collect text-based data of the user perceptions and explanations in their own words.

#### Questionnaire

Since it was important to collect metaphor data in the users own words a questionnaire format was utilized however paper-based questionnaire was recognized to be inadequate on an Internet scale. An automated on-line questionnaire in the form of [a Web page](http://www2.umdnj.edu/~ratzan/im4.html) was developed to collect demographic, scales, categorical and textual data. Variables included level and extent of use, age and gender, extent of training, general metaphor categories, specific descriptions from the user and their explanations. The latter appeared in free text. After the initial pilot study an additional question on the source of awareness of the study was added. The responses of this last query helped track the scope of penetration.

A Web-based questionnaire had the advantage of being readily available at any time to any user anywhere in the world. User responses to the form were automatically transmitted to a designated electronic mailbox of the researcher. Responses arrived at all hours from all over the Internet and continue to arrive to this day.

### Procedures

#### On-line awareness

This study intended to obtain data on user metaphors in their words and with their own explanations. Hence the Internet became a platform for the communication of information about itself. The advantages of this medium included ease of access, global awareness, rapid distribution, automatic data collection, immediate feedback and minimal cost. It is highly unlikely any paper-based methodology could inform potential participants of the study or even attract their attention. on-line special interest groups and on-line discussion forums in the form of Usenet newsgroups and list servers provided a remarkably effective mechanism in which to advertise the study. Relevant newsgroups could be identified from the Look for program (available on many Unix host servers) or by published numerous newsgroup lists. Relevant listservs could be rapidly identified by their corresponding published lists or through on-line repository index sites (e.g., [http://tile.net/lists](http://tile.net/lists) or [http://www.liszt.com](http://www.liszt.com)).

<table><caption>

**Table 2: Examples of Listserv resources used in the study.**</caption>

<tbody>

<tr>

<td>American Library Association-Office of Intellectual Freedom</td>

</tr>

<tr>

<td>American Communication Association</td>

</tr>

<tr>

<td>Chief Information Officers Society</td>

</tr>

<tr>

<td>Communication, Research and Theory Network</td>

</tr>

<tr>

<td>H.W. Wilson Web</td>

</tr>

<tr>

<td>Interpersonal Computing and Technology</td>

</tr>

<tr>

<td>Language Use Society</td>

</tr>

<tr>

<td>Library and Information Technology Association</td>

</tr>

<tr>

<td>Mideastern Michigan Region of Cooperation</td>

</tr>

<tr>

<td>National Happenings Digest</td>

</tr>

<tr>

<td>Net Trainers</td>

</tr>

<tr>

<td>New York Libraries Information Network</td>

</tr>

</tbody>

</table>

The index sites provided information on listserv subscription size. The extent of the potential target audience is important because it provides a multiplicative factor. For example, if a listserv had 10,000 subscribers then a single e-mail announcement message sent to the moderator relayed in turn to all its members would result in an exposure to 10,000 potential participants. By way of comparison, a traditional study with this same level of exposure would require 10,000 forms to be manually distributed, a tedious labor-intensive and costly process.

This distribution scheme has the advantage that the cost of a mass distribution by the researcher is negligible and, in addition, no prior knowledge of the individual names of the potential participants or their addresses was required. The researcher only needed to know only the electronic address of the moderator and not that of the group members individually. The initial procedure thus consisted of identifying relevant newsgroups or lists, sending an announcement of the study to the moderator (editor) and requesting that the moderator submit the message to the subscription list. The moderator almost always cooperated fully and often within minutes. Exploiting the on-line multiplicative effect meant that hundreds of thousands of potential participants could be informed by sending a single announcement message to a few dozen groups.

The method of listserv distribution can also engender a cross-posting effect. Users often subscribe to multiple listservs. A subscriber of listserv #1 would post the announcement message to listserv #2 so now there is an additive effect as well as the aforementioned multiplicative effect. Exploiting this effect could easily increase exposure by an order of magnitude with no increase in researcher effort. The project study URL was also registered with search engines such as AltaVista, Inktomi, Northern Light, Yahoo, Search.com, Web Crawler, Go Network, Excite and various meta-search engines. Since the study of metaphors is an active area of contemporary communication research many respondents encountered the Internet Metaphor study when using these tools for their own work.

#### On-line data collection

It is important to reduce the time delay between participant awareness and the acquisition of data input. The participant may lose interest. Thus, the [Web announcement message](http://www2.umdnj.edu/~ratzan/imeta4.html) contained a direct hypertext link to a data collection instrument. Paper forms can get lost. The announcement message did not get lost because it would reside in the user's own electronic mailbox or newsgroup. People do not always read a paper announcement message when it arrives. An on-line special group distribution will retain the message for the reader until it is ready to be read. Data input was not restricted to researcher availability. Users could submit data at any time.

A common problem with on-line data acquisition is that different versions of different Web browsers can result in pages that are not viewable by all users. This problem was avoided in the study by intentionally creating the data input form in generic HTML. (Contemporary browsers now support advanced features so follow-up studies can use XML and Java applets for an enhanced user interface.) The on-line form incorporated an explanatory section, examples, discrete scales, an e-mail contact address, comparative scales and the opportunity to enter multiple metaphor descriptions and explanations in their own words and in free text. This data was then automatically consolidated into a template file and transmitted by electronic mail to the study repository when the user clicked the Submit button.

#### On-line data consolidation

The data automatically arrived as a text e-mail message to a pre-assigned electronic mailbox and it arrived at all hours. The template imposed a structural framework to the data input. A special indicator was placed in the e-mail response subject line so that data responses could be distinguished from user comments. The data were downloaded daily into a special project directory on a host Unix machine, periodically exported by the FTP protocol to an off-line disk and then consolidated imported into a relational database management software program (Microsoft Access).

### Data analysis

The frequency distribution of discrete variable values was tabulated. Word-stubs were identified in the user metaphor descriptions. This was done on their primary and secondary metaphor responses. Duplicate responses from repetitive submissions and empty responses (no data) were eliminated. The data were pre-screened through a variety of consistency checks. Subsets of the data were extracted as defined by variable parameters. Patterns in data relationships were visualized by graphical representations because this is a preliminary study and based on a self-selecting sampling methodology. Traditional statistical tests such as chi-squared and t-test were not performed since they are based on a representative sampling model.

Conditional probabilities measure the likelihood of one event given another. For example, the probability of a user describing the Internet in terms of a place, given that they were a Novice. This can be calculated by Bayes Rule:

![Figure 1](../p85fig1.gif)

## Results

The study was allowed to run for several weeks and a preliminary analysis was begun when the sample size reached N=350\. This result demonstrates a monotonic, decreasing relationship between the use of a place metaphor by novices to experts. It might suggest that the user cognitive images of the Internet as a location in space changes or evolves as the level of skill increases. On the other hand, based on this sample, the likelihood of describing the Internet as an object (tangible or intangible) seemed to steadily increase. Whether or not this relationship holds for larger samples or for stricter definition of skill level remains to be seen.

<table><caption>

**Table 3: Proportions of skill level by metaphor type**</caption>

<tbody>

<tr>

<th>Perceived skill level</th>

<th>Use of a place metaphor</th>

<th>Use of an object metaphor</th>

</tr>

<tr>

<td>Novice</td>

<td>.87</td>

<td>.12</td>

</tr>

<tr>

<td>Intermediate</td>

<td>.80</td>

<td>.20</td>

</tr>

<tr>

<td>Advanced</td>

<td>.79</td>

<td>.20</td>

</tr>

<tr>

<td>Expert</td>

<td>.72</td>

<td>.27</td>

</tr>

</tbody>

</table>

Men tended to consider themselves as higher skilled users while women tended to perceive themselves as lesser skilled on-line users.

<table><caption>

**Table 4: Skill distribution by gender**</caption>

<tbody>

<tr>

<th>Perceived Skill</th>

<th>Female n=</th>

<th>Male n=</th>

</tr>

<tr>

<td>Novice</td>

<td>19</td>

<td>9</td>

</tr>

<tr>

<td>Intermediate</td>

<td>63</td>

<td>36</td>

</tr>

<tr>

<td>Advanced</td>

<td>78</td>

<td>66</td>

</tr>

<tr>

<td>Expert</td>

<td>22</td>

<td>56</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

**182**</td>

<td>

**167**</td>

</tr>

</tbody>

</table>

These data could also be broken down by metaphor word-stub. The theme of Information dominates user perceptions of the Internet and the Web. The Internet is secondarily described as a library. The phrase "information superhighway" which appeared often in the mass media has not had deep penetration into the user cognitive image.

<table><caption>

**Table 5: Word-stub (theme) distribution**</caption>

<tbody>

<tr>

<td> </td>

<td colspan="4">

**_The Internet is a ..._**</td>

</tr>

<tr>

<th>Gender</th>

<th>Information n=</th>

<th>Network n=</th>

<th>Library n=</th>

<th>Highway n=</th>

</tr>

<tr>

<td>Female</td>

<td>32</td>

<td>5</td>

<td>18</td>

<td>9</td>

</tr>

<tr>

<td>Male</td>

<td>14</td>

<td>4</td>

<td>9</td>

<td>13</td>

</tr>

</tbody>

</table>

Table 6 expresses the likelihood that a user is of low or high skill level GIVEN that they selected a particular type of Place or Object metaphor. While these probabilities are currently too low for clinical prediction nevertheless the values of 0.70 and 0.64 are sufficiently high to warrant further research attention.

<table><caption>

**Table 6: Metaphor conditional probabilities (skill given type)**</caption>

<tbody>

<tr>

<td> </td>

<td colspan="4">

**_Given the Internet is a ..._**</td>

</tr>

<tr>

<th>User Perceived Skill</th>

<th>Open Place</th>

<th>Closed Place</th>

<th>Animate Object</th>

<th>Inanimate Object</th>

</tr>

<tr>

<td>Low</td>

<td>.38</td>

<td>.34</td>

<td>.28</td>

<td>.33</td>

</tr>

<tr>

<td>High</td>

<td>.61</td>

<td>.64</td>

<td>.70</td>

<td>.64</td>

</tr>

</tbody>

</table>

Probability (perceived skill given metaphor type)= Probability (perceived skill and metaphor type)/Probability (metaphor type)

In this data sample with normalized percentages, females were more likely to use a highway metaphor than did males and this held true over all age categories.

<figure>

![Figure 2](../p85fig2.gif)

<figcaption>Figure 2: Frequency of the highway metaphor by age.</figcaption>

</figure>

In this data sample with normalized percentages females were more likely to use a frontier metaphor for the Internet than did males and this held true over all age categories.

<figure>

![Figure 3](../p85fig3.gif)

<figcaption>Figure 3: Frequency of the frontier metaphor by age.</figcaption>

</figure>

Metaphors from novices often bear a sense of confusion, complexity or frustration while experts are much more anchored in reality. One may speculate that the former is an expression of the novelty of the Web experience. This explanation is appealing but is not consistent with the fact intangible metaphysical metaphors were used solely by Experts and never by Novices. This might be indicative of a cognitive paradigm change and a function of the amorphous nature of the Web.

<table><caption>

**Table 7: Internet metaphor by skill (sampler)**</caption>

<tbody>

<tr>

<td colspan="2">

**_The Internet is a ..._**</td>

</tr>

<tr>

<th>Novices</th>

<th>Experts</th>

</tr>

<tr>

<td>bottomless pit</td>

<td>chameleon</td>

</tr>

<tr>

<td>maze</td>

<td>community</td>

</tr>

<tr>

<td>snaggled skein of yarn</td>

<td>idea processor</td>

</tr>

<tr>

<td>wide endless road</td>

<td>haven for free speech</td>

</tr>

<tr>

<td>big bookstore</td>

<td>bookstore with a switchboard</td>

</tr>

<tr>

<td>locked library</td>

<td>huge library</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 8: Sample metaphysical internet metaphors (Experts only)**</caption>

<tbody>

<tr>

<td>

**_The Internet is a..._**</td>

</tr>

<tr>

<td>new dimension</td>

</tr>

<tr>

<td>void of omnipotence</td>

</tr>

<tr>

<td>cooperative chaos</td>

</tr>

<tr>

<td>fractal</td>

</tr>

<tr>

<td>world that exists in consciousness</td>

</tr>

</tbody>

</table>

## Discussion

The preliminary results of this study provide empirical support to the idea that users do indeed utilize metaphors to describe their image of the Internet and the Web. These metaphors appear to manifest only a few dominant themes and may, perhaps, be verbal markers. Men and women appear to project different self-perceptions of themselves as Internet users. The results are important because they suggest the existence of different cognitive images on the part of differently skilled users. If the way the Web is described affects perception and use then Internet metaphors may show a focus for different classes of users.

Knowledge of these images may enhance future Web development. For example, [Lawson](#ref1) (1997) has suggested that men and women navigate differently in unknown territories. Men tend to prefer absolute addressing (4 Arbit Road) while women tend to prefer relative addressing (Second house on the right). The way people navigate in a foreign environment may affect on-line information retrieval and future Web search engine design ([Canter, Rivers & Storrs, 1985](#ref1); [Canter, Powell, Wishart & Roderick, 1986](#ref1)). The fact that more women than men use the highway metaphor may impact on this development.

Bayesian probability can measure the likelihood that one event occurs given another. A preliminary analysis of Bayesian probabilities of several study variables suggests that it may be possible to predict attributes of the users based on their own descriptive language. This may be a significant marker. The predictive power is currently low but future studies with more refined measures may have a stronger foundation.

Novices tended to use finite, tangible, delimited, closed, delineated metaphors while Experts tended to use more metaphysical, intangible, open metaphors. This may indicate the lack of comfort level of the Novice to conceptualize something amorphously vast and the significant ability of Experts to do so. This difference in conceptual imagery may have ramifications for the development of future Web services to target audiences.

Information was the dominant theme associated with Internet and Web metaphors based on word-stub frequency. Those who used this metaphor tended to describe it more often as an _information source_ rather than as an _information conduit_ (as in highway). The second and third most common themes were that of Library and Network.

The Web was often described as _a dysfunctional library_. Users described the Internet as a library with books scattered all over the floor, an uncatalogued library, a library with its lights turned off. This suggests an image of chaotic information access. A library structures its information and so the two metaphors are in conflict.

Some user metaphors defied simple explanation. The Internet-as-Spaghetti may suggest an image of entanglement but it is unclear how to interpret the Internet as bowl of Jello. Sweet rewards? Transparent and hence holds no secrets? Unfortunately the users did not supply an explanation.

More females used frontier metaphors than did males and this occurred irrespective of age distinctions. A frontier is often thought of as a pristine place (female?) as opposed to a jungle that must be conquered (male?). This attitude difference may be a significant aspect of future Web development.

More females thought of the Internet in terms of a highway than did males and this was true over all ages. A highway is associated with structure on many levels (maps, directions, rules, control, etc.) yet highways are fundamentally neutral in sexuality as opposed to ships or planes. The use of the highway metaphor tended to decrease as experience increased.

Novice users tended to describe the Web more in terms of a place, something that which is fixed in space and time. A library is a building fixed in space and associated with information. Thus, it was not surprising to encounter Library themes. It was surprising to see the frequency of this description decreasing consistently with higher skill levels. It may suggest that lower skilled Web users need a cognitive anchor to conceptualize the Web while expert users can free themselves of this mental support. This may have applications to Web education and Web commerce.

## Conclusions

Internet metaphors appear to be a necessary component in the user conceptualization of the Internet and the Web. The concepts so used manifest themselves as common themes that might function be a subtle marker. Metaphor images seem to change as skill level develops. It has been shown that it is possible to create effective and efficient on-line methodology using the resources of the Internet as a tool to study itself.

Data are now available that describe the mind-set of the user in their own words and with their own explanations. Communication theorists can sift through this empirical data and perhaps extract deeper meaning behind the verbalizations and possibly generate a broader theory of the Internet communication.Many users provided their electronic address with their data. A follow-up study on these same users could confirm if the general results continue to apply. A variety of additional and more sophisticated variables could be measured in future studies.

## Acknowledgements

The author wishes to thank Professors David Carr, Lea Stewart, Kathleen Burnett, James Anderson, Jose Perez-Carballo and other faculty and staff of the School of Communication, Information and Library Studies (SCILS) of Rutgers University for supporting the doctoral research. Thank you Karen Novick of SCILS Professional Development whose students helped attune me to the problems of Internet metaphors and Professor Ruth Palmquist of the University of Texas-Austin for permission to use portions of her HTML code. Metaphors Be With You!

The complete set of results, graphs and tables of the Internet Metaphor Project are freely available on computer diskette from the author. Please specify IBM-PC (Microsoft Word) or Apple Macintosh (ClarisWorks) format.

## Appendix

Interested readers may examine or participate in [the project study.](http://www2.umdnj.edu/~ratzan/imeta4.html)

## References

*   <a id="ref1"></a>Budd, Richard & Ruben, Brent., (ed) (1979). _Interdisciplinary approaches to human communication._<u>.</u> Rochelle Park, NJ: Hayden, 71-93.
*   Cameron, C., (December 15, 1997). _[Theories and metaphors of cyberspace](http://pespmc1.vub.ac.be/CYBSPASY/CCameron.html)_. Available at: http://pespmc1.vub.ac.be/CYBSPASY/CCameron.html.
*   Canter, David. , Rivers, Rod. & Storrs, Graham. (1985). "Characterizing user navigation through complex structures". _Behavior and Information Technology_, 4(2), 93-102.
*   Canter, David, Powell, J., Wishart, J., Roderick, C., (1986) "User navigation in complex database systems." _Behaviour and Information Technology_, 5(3), 249-257.
*   Cooper, Alan (1997). "[The myth of metaphor](http://www.cooper.com/articles/vbpj_myth_of_metaphor.html)_"_, Available at: http://www.cooper.com/articles/vbpj_myth_of_metaphor.html.
*   Coyne, Richard (1995). _Designing information technology in the postmodern age: from method to metaphor._ Cambridge, MA: MIT Press, 249-301.
*   Cunningham, Michael (December 30, 1996). "[An A-Z of Internet metaphors](http://www.irish-times.com/irish-time/paper/1996/1230/cmp2.html)". _Irish Times_<u>,</u> Available at: http://www.irish-times.com/irish-time/paper/1996/1230/cmp2.html.
*   Davis, James, (June 11,1997). "Mixed metaphors", <u>alt.humor.puns</u>
*   Deetz, Stanley & Mumby, Dennis, (1985). "Metaphors, information and power." <u>In</u> Ruben, B., ed. _Information and human behavior_. Transactions, 1, New Brunswick, NJ: Rutgers University Press. pp.369-385
*   Gold, David (1997). "[You can't surf a sine wave: metaphors and the future of the Internet](http://ccwf.utexas.edu/~dgold/metaphor.project/)." Available at: http://ccwf.utexas.edu/~dgold/metaphor.project/.
*   Jamieson, Kathleen Hall, (1980). "The metaphoric cluster in the rhetoric of Pope Paul VI and Edmund G. Brown Jr." _Quarterly Journal of Speech_, 66, 51-72.
*   Johnson, Steven, (October 23, 1997). "Metaphor monopoly." _New York Times_, A 27.
*   Johnson, William, (1946). _People in quandaries_, New York, NY: Harper.
*   Karen, Lawrence, (statistician), (December 1997). _Private communication_.
*   Kelling, George, (1975). _Language: mirror, tool & weapon._ Chicago, IL: Nelson-Hall.
*   Korzybski, Alfred, (1948). _Science and sanity._ Lakeville, CN: Neo-Aristotelian Publishing.
*   Lakoff, George, & Johnson, Mark, (1980). _Metaphors we live by._ Chicago, IL: University of Chicago Press.
*   Lawton, Carol, (1997). Indiana University, Fort Wayne, Indiana, cited in Geographica section, _National Geographic Magazine_, 192,(6), xxviii.
*   <a id="ref2"></a>MacCormac, Earl (1976). _Metaphor and myth in science and behavior_. Durham, NC: Duke University Press.
*   Norton, Catherine, (1989_). Life metaphors_. Carbondale. IL: South Illinois University Press.
*   Ortony, Andrew (ed), (1979). _Metaphor and thought_, (2nd ed.), Cambridge: Cambridge University Press.
*   Ortony, Andrew, (1975). "Why metaphors are necessary and not just nice", _Educational Theory, 25_, 45-53.
*   Palmquist, Ruth, (1996). "A qualitative study of Internet metaphors", _17th National On-line Meeting_, May 1996.
*   Palmquist, Ruth, 1996, "[The search for an Internet metaphor: a comparison of literatures](http://www.asis.org/annual-96/ElectronicProceedings/palmquist.html)", _American Society of Information Science Conference_, October 19, 1996\. Available at: http://www.asis.org/annual-96/ElectronicProceedings/palmquist.html
*   Rohrer, Tim, (1995). "[Metaphors we compute by: bringing magic into interface design](http://philosophy.uoregon.edu/metaphor/gui4web.htm)". Available at: http://philosophy.uoregon.edu/metaphor/gui4web.htm
*   Rohrer, Tim (1997) "Conceptual Blending on the Information Highway: How do metaphorical inferences work?" in Discourse and Perspective In Cognitive Linguistics, edited by Wolf-Andreas Liebert, Gisela Redeker and Linda Waugh. Amsterdam: John Benjamins, 1997, pp. 185-205\. [Also available at: [http://philosophy.uoregon.edu/metaphor/iclacnf4.htm](http://philosophy.uoregon.edu/metaphor/iclacnf4.htm)]
*   Ross, P.E. (1992). "New whoof in Whorf"_Scientific American_, 266 (2), 24-25.
*   Rothstein, Edward, (October 28, 1996) "Connections: metaphors for Internet may affect its use." New York Times.
*   Smith, Ruth C. & Turner, Paige K. (June, 1995). "A social constructionist reconfiguration of metaphor analysis", _Communication Monographs_, 62, 152-181.
*   Stefik, Mark, (1997). _Internet dreams: archetypes, myths and metaphors_. Cambridge, MA: MIT Press.
*   Thaler, Lee (ed), (1970). _Communication: general semantics perspectives,_ New York, NY: Spartan Books.
*   White, Peter, (October 29, 1996). "[On-line services and transaction space: conceptualizing the issues](http://dpub36.pub.sbg.ac.at/ectp/WHITE_P.htm)". Available at: http://dpub36.pub.sbg.ac.at/ectp/WHITE_P.htm
*   Whorf, Benjamin, (1939). Science and Linguistics, _Technology Review,_ 42, 247-248.
*   Whorf, Benjamin, (1956). _Language, Thought and Reality_, New York, NY: Wiley
*   Wilson, Meredith (1961). _The Music Man_, (play), New York, NY: Samuel French.
*   Willsky, Stanley, (1997). _private communication_. [S. Willsky is a statistician.]

## Additional Readings

*   Clarke, Roger, (1996). "[A more refined populist metaphor](http://www.anu.edu.au/mail-archives/link/linl9604/0032.html)". Available at: http://www.anu.edu.au/mail-archives/link/linl9604/0032.html
*   Coblentz, Clayton, (1997). "[Metaphorically interactive: the rhetorical examination of World Wide Web pages](http://www.regent.edu/acad/schcom/rojc/coblentz/coblentz.html)". Available at: http://www.regent.edu/acad/schcom/rojc/coblentz/coblentz.html
*   Lawler, John, "[Metaphors we compute by](http://www.lsa.umich.edu/ling/jlawler/meta4compute.html)", Lecture to the Informational Technology Division of the University of Michigan. Available at: (http://www.lsa.umich.edu/ling/jlawler/meta4compute.html). [Original version of the essay appearing in, Hickey, D.J. (1999) Figures of thought: for college writers. Mountain View, CA: Mayfield Publishing.)
*   MacCormac, Earl (1976) _Metaphor and myth in science and behavior_, Durham, NC: Duke University Press.
*   Norton, Catherine, (1989) _Life Metaphors_, Carbondale, IL: South Illinois University Press.
*   Osborne, Michael, (December 1977). "The evolution of the archetypical sea in rhetoric and poetic", _Quarterly Journal of Speech, 63_(4), 347-363.
*   Ostman, Charles, "[The Internet as organism.](http://pespmc1.vub.ac.be/CYBSPASY/COstman.html)" Available at: http://pespmc1.vub.ac.be/CYBSPASY/COstman.html.
*   Sawhney, Harmeet (1990). "Information superhighways, metaphors as midwives", _Media, Culture and Society, 18(_29),314.
*   Seligman, Doree Duncan and Laporte, Cati & Bugaj, Stephan Vladimir, (1998). "[The message is the medium.](http://www.6conf.org/HyperNews/get/PAPER119.html)" _Sixth International World Wide Web Conference_, Bell Laboratories, Lucent Technologies. Available at: http://www.6conf.org/HyperNews/get/PAPER119.html.
*   Simkins, Ronald (1994) Creator & creation: nature in the worldview of ancient Israel. Peabody, MA: Hendrickson Publishers.
*   White, Peter, (October 29, 1996). "Online services and transaction space: conceptualizing the issues", Available: (http://dpub36.pub.sbg.ac.at/ectp/WHITE_P.htm).