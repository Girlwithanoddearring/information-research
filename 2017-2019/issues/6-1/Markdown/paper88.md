#### Information Research, Vol. 6 No. 1, October 2000

# A bibliometric analysis of select information science print and electronic journals in the 1990s

#### [Wallace Koehler](mailto:wkoehler@valdosta.edu), Paulita Aguilar, Sharon Finarelli, Charles Gaunce, Susan Hatchette,  
Rebecca Heydon, Emily McEwen, Wendy Mahsetky-Poolaw, Charles T. Melson,  
Rory Patterson, Mark Stahl, Mary Ann Walker, JoAnna Wall, and Gabe Wingfield  
School of Library and Information Studies  
The University of Oklahoma  
Norman, OK 73019 USA

#### **Abstract**

> This paper examines three e-journals and one paper journal begun in the 1990s within the information science genre. In addition, these journals are compared to what is perhaps the leading information science journal, one that has been published continuously for fifty years.  The journals we examine are _CyberMetrics_, _Information Research_, the _Journal of Internet Cataloging_, _Libres,_ and the _Journal of the American Society for Information Science._ We find that there are a number of important differences among the journals. These include frequency of publication, publication size, number of authors, and the funding status of articles. We also find differences among journals for distributions of authors by gender and corporate authors by region. Some of the regional differences can be explained by journal maturation -- the more mature the journal the greater the dispersion. We also find that women are more likely to publish in the newer journals than in _JASIS_.  The fact that a journal is or is not an e-journal does not appear to affect its presence or "behaviour" as an information science journal.

## Introduction

Articles published in scholarly journals, including those in the library and information sciences, reflect changes in the interests and concerns of their author constituencies, and the discipline. These changes can be documented through bibliometric analyses of journal content (e.g. [Jarvelin & Vakkari](#jarvelin-90), 1990; [Jarvelin & Vakkari](#jarvelin-93), 1993; [Buckland](#buckland), 1999; or [Cano](#cano, V.), 1999). That analysis can be done in several ways. One is to do content analysis on articles, their titles, and/or other characteristics of those journals. A second approach would be to examine the characteristic, the "demographics" if you will, of the articles, their authors, and of the journals. It is this latter approach we take here.

It is true that there are many journals in the information and library sciences disciplines. Given our resources and time constraints, we elected to select from among the newer journals. Newer journals, we hypothesize, are more likely to innovate and reflect changing conditions on the "fringes" of the disciplines they represent. Are they more innovative than more "mainstream" journals? Are their authors more likely to adopt or accept new formats or styles – like citations to the Internet – than older journals? Will new journals with more narrow interests be more or less successful than those with more general ones? Will there be resistance on the part of authors to e-journals and a preference for those in print? Are there author number, nationality, or gender differences among these journals. If so, can these differences be explained?

We have chosen four new journals from the information science literature for analysis. These are _CyberMetrics_, _Information Research_, the _Journal of Internet Cataloging_, and _Libres_. Each of the journals was first published in the 1990s or, in the case of _Libres_, became a peer reviewed journal in that decade. _CyberMetrics ([CM](http://www.cindoc.csic.es/cybermetrics/))_, _Information Research ([IR](../index.html))_, and _[Libres](http://aztec.lib.utk.edu/libres/contents.html)_ are all electronic journals ("e-journal"), while the _Journal of Internet Cataloging ([JIC](http://www.haworthpressinc.com:8081/jic/))_ is published in print format ("p-journal"). _CyberMetrics_ and the _Journal of Internet Cataloging_ were selected because they address the Internet and the World Wide Web, a new phenomenon. To balance this specialized focus, _Information Research_ and _Libres_ were chosen because theirs is a more general concern.

For comparative and baseline purposes, we also include 1990s bibliometric data for the _Journal of the American Society for Information Science (JASIS)._ In recent years _JASIS_ has been published in both print and electronic formats, and may therefore be considered a hybrid journal ("h-journal").  Each of these five journals have had very different publication histories. _JASIS_ was published throughout the decade in multiple numbers and articles, as [Koehler et al](#koehler)  (2000) demonstrate. _CyberMetrics_, published in Spain, _Information Research,_ published in the United Kingdom, and the _Journal of Internet Cataloging_, published in the United States, began publication in 1997. _Libres_ converted to a peer reviewed journal in October 1993\. It is difficult to say exactly where _Libres_ is published, since it is found on mirrored servers in Australia and the United States, and members of its editorial board reside in the United Kingdom. _JASIS,_ published in the United States, has roots starting in 1938, began as _American Documentation_ in 1950 and changed its name in 1970.

Like many others before it, this article is an example of bibliometric exploration of important journals in librarianship and information science (e.g., [Saracevic& Perk,](#saracevic) 1973; [Olsgaard & Olsgaard](#olsgaard), 1980; [Cline](#cline), 1982; [Carter & Kascus](#carter), 1991; [Stephenson](#stephenson) , 1993; [Smiraglia & Leazer](#smiraglia), 1995; [Terry](#terry), 1996; [Al-Ghamdi et al](#al-ghamdi), 1998; [Bates](#bates), 1999; or [Cano](#cano),1999).  Our analysis differs from these because we explore more than one journal. With the exception of _JASIS_, we also examine all issues and numbers for the subject journals, and we do the same for _JASIS_ through the 1990s.

We recognize that there is a new medium for publishing scholarly journals. That medium, of course, is the World Wide Web. The number of e-journals are proliferating in the information sciences as well as in other professional and academic arenas. Aside from the publication and delivery vehicle, are e-journals a breed apart from their p-journal and h-journal counterparts?  We suggest that the differences we find for the e-journals, p-journals, and h-journal we consider derive from sources other than the publication medium. These include publication frequency, publication size measured by number of articles, number of issues per year, corporate author nationality, number of authors, author gender, and other factors. E-journals may have certain limited advantages over the p-journal counterparts, and these include cost of publication and tolerance for longer offerings. E-journal editors face the same challenges to build readership and contribution bases, quality control, sponsorship, and the many other factors that enter into journal publication. E-journals may face some resistance from potential authors in that not all academic disciplines as yet accept them as "peer reviewed journals" that have the same merit to promote scientific findings nor do e-journals offer the same assurance of long-term availability that their paper siblings do.

Are e-journals different from p-journals or h-journals? We explore several variables taken either directly from the journals themselves, or when necessary by asking authors or editors. We find differences among the five journals we analyze, but we also find that most differences can be explained by variables other than their "publication status" or medium. Or to paraphrase badly: "a journal is a journal is a journal".

## Methods

To document changes in authorship, citation patterns, funding and funding sources, and related bibliometric phenomena, the spring 2000 Elements of Research course class at the School of Library and Information Studies at the University of Oklahoma collected data from each number of each volume of _CM, IR_, _JIC_, and _Libres_. Data for _JASIS_ from the 1990s was adapted from a similar project conducted by the fall 1999 Elements of Research class (see [Koehler et all](#koehler), 2000).

The names of all authors were collected in the published order together with each author's specific and general affiliation. For example, were we to record authorship for this article, we would record each of the authors and identify the School of Library and Information Studies as the specific corporate author and the University of Oklahoma as the general author. From the affiliation data, we compiled data for corporate nationality. The University of Oklahoma is an American institution, and is scored as "US". Sheffield University is considered for this same purpose as "UK". A "nationality" variable was created for each of the corporate authors. We were successful in attributing corporate nationality in all but one case.

We also developed a gender variable for each author. In most cases, author gender was surmised from each author's name. Where we could not determine gender from names published in articles because of the use of initials, names common to both genders, or because of unfamiliarity with the name, we contacted journal editors. Failing that, we contacted the authors themselves. As is [shown below](#journal-author-gender), we were largely successful in identifying author gender, and our "failure" rates ranged from zero to 4.5%, with an overall "failure" rate of 3.9%.

In addition, for each article we logged the full article title (including "stop" words), the journal name, number, date, position of the article in each issue, and the editor's name By collecting the full article title, we are able to replicate research that suggests that the greater the number of authors the longer the title ([Kuch](#kuch), 1978; [Yitzhaki,](#yitzhaki)1994)although our findings point to a very weak association, if any for the journals we analyzed (Pearson's r = .060, p<0.1).

We collected citation data from each article. These include the number of articles, books, proceedings, government documents, media reports (radio, television, newspapers, newsmagazines, etc.), personal communications, and Internet material cited. In addition, we counted the number of self citations to the work of any of the authors. Finally, we collected funding data by type of funding agency. These included "not-funded," "government agency," "foundation," "university," and "other."

Data were collected to individual spreadsheet (Excel) templates. Each data set was checked by the lead author (and professor) to determine not only data accuracy but the exercise grade. Where the data error rate was low for a ten- percent random sample of each set, corrections were made as necessary. Where there was a large error rate, the entire set was rejected and a new collection made. Once quality control was accomplished, each of the data sets was merged into a single spreadsheet. Further quality control was accomplished by ordering authors and corporate authors alphabetically and through a series of counts. The spreadsheet was imported into a statistical package (SPSS) for further analysis.

## Findings

### Journal Characteristics

The journals examined for this study exhibit very different publication behaviour and characteristics. These are outlined in Table 1\. With the exception of _JASIS_, which has published continuously since 1950, none of the other four journals existed at the beginning of the 1990s, or for that matter prior to the birth of the WWW in 1991\. The size and number of issues per volume and the number of articles has varied both as an inter journal and as an intra-journal variable. _CyberMetrics_ has published the fewest articles and among the newer ones _IR_ has published the most. The size and frequency of each issue is in large part a function of acceptable submissions to each journal.

<a id="table-1"></a>

<table><caption>

Table 1\. Journal characteristics in the 1990s</caption>

<tbody>

<tr>

<td></td>

<td>Began  
Publication</td>

<td>No. of Vols.</td>

<td>No. of  
Articles</td>

<td>Max No. of  
Issues/Vol.</td>

<td>Avg.  
Articles/Vol</td>

</tr>

<tr>

<td>

_CM_</td>

<td>1997</td>

<td>3</td>

<td>3</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>

_IR_</td>

<td>1995</td>

<td>5</td>

<td>86</td>

<td>4</td>

<td>17.2</td>

</tr>

<tr>

<td>

_JIC_</td>

<td>1997</td>

<td>2</td>

<td>30</td>

<td>4</td>

<td>15.0</td>

</tr>

<tr>

<td>

_Libres_</td>

<td>1993 -  
peer review</td>

<td>7 (3 to 9)</td>

<td>42</td>

<td>6</td>

<td>6.0</td>

</tr>

<tr>

<td>

_JASIS_</td>

<td>1950 -  
data from 1990+</td>

<td>10</td>

<td>588</td>

<td>14</td>

<td>58.8</td>

</tr>

</tbody>

</table>

### Article Characteristics

Article characteristics cover a wide variety of variables. The majority of articles published in these five journals have had but one author, although the trend is toward a growing multi-authorship. There has been an insufficient number of issues published by the four new journals to demonstrate any trends, but [Koehler et al.](#koehler) have shown that the number of authors on average per _JASIS_ article has risen from about 1.2 in 1950 to about 1.8 in 1999\. As is shown in Table 2, the typical article carries between one and three authors, although to date there have been as many as nine.

Multiple authorship, it has been suggested, is a sign of a mature discipline publishing complex articles addressing complex issues. It is also suggested that funded research is a sign of complex science since it demonstrates not only external interest in the research but also again of the complexity that research.

<a id="assessing"></a>Assessing the importance of multiple authorship is problematic. As [Harsanyi](#harsanyi) (1993) has shown, different disciplines interpret the order of authorship differently. Some list co-authors alphabetically. Some list co-authors by the order of contribution to the article.  If [Terry](#terry) (1996: 379) is correct, there are no established norms in librarianship and the information sciences for citation order. Because it is difficult to establish a rule, we consider not only first authors but all authors in our discussion of [gender](#gender) and [nationality](#nationality) below.

<a id="table-2"></a>

<table><caption>

Table 2\. Percentage distribution of number of authors per article</caption>

<tbody>

<tr>

<td> </td>

<td colspan="9">Number of Authors</td>

</tr>

<tr>

<td> </td>

<td>One</td>

<td>Two</td>

<td>Three</td>

<td>Four</td>

<td>Five</td>

<td> Six </td>

<td>Seven</td>

<td>Eight</td>

<td>Nine</td>

</tr>

<tr>

<td>

_CM_</td>

<td>100.0</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

_IR_</td>

<td>46.5</td>

<td>22.1</td>

<td>15.1</td>

<td>3.5</td>

<td>11.6</td>

<td> </td>

<td> </td>

<td> </td>

<td>1.2</td>

</tr>

<tr>

<td>

_JIC_</td>

<td>63.3</td>

<td>16.7</td>

<td>20.0</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

_Libres_</td>

<td>76.2</td>

<td>14.3</td>

<td>7.1</td>

<td>2.4</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

_JASIS_</td>

<td>51.5</td>

<td>28.9</td>

<td>11.7</td>

<td>5.2</td>

<td>1.5</td>

<td>0.5</td>

<td>0.8</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Sample</td>

<td>52.7</td>

<td>27.1</td>

<td>12.0</td>

<td>4.7</td>

<td>2.3</td>

<td>0.4</td>

<td>0.8</td>

<td> </td>

<td>&gt; 0.1</td>

</tr>

</tbody>

</table>

Articles also differ by the mix of citations they carry. If the number of citations per article is an indicator of article or journal complexity, _JASIS_ and _IR_ rank "highest", while _Libres_ is "lowest". We can conclude that the number of citations vary significantly from one journal to another, and while that may be an indicator of complexity, may also be one merely of style.

There is wide variation in the material cited by authors in each of the five journals. The five most commonly cited materials are listed in Table 3 by the mean number of citations during the 1990s as well as by their percent contribution. There are others, thus no row totals 100 percent. These data suggest the sources that are important to any given field or sub field. Books and articles are in the first rank. Proceedings, government documents, and other sources are less important. It is interesting to note the wide variation in citations to the Internet. This is more fully considered in the next [section](#web).

The last column of Table 3., labeled "Avg Selfcites" is separate from the others and reports the number of citations of any kind to works by any of the authors of the work analyzed. Self citation is not uncommon, but its frequency varies from journal to journal. We suggest that self citation is more common in newly developing fields, for example here Internet cataloging, because the literature for the new field is relatively under developed, and often the author is among the few writing in the field. Authors in new areas of inquiry may be "forced" to cite themselves in the absence of anyone else's work to cite.

Of these five journals, only _JIC_ can be considered to address totally new concerns, and even it is the application of a very long standing discipline (cataloging) to a new medium (the Internet). _CyberMetrics_  also brings an established discipline (bibliometrics) to the Internet, but its "n" is too small for generalization. _Libres_, _IR_, and _JASIS_ publish a wide range of information science disciplines, some established and some new. Journals publishing the wider literature tend to have lower self citation rates, as demonstrated by the values for _Libres_ and _IR_. These conclusions are perhaps moderated somewhat by the _JASIS_ 11% self citation statistic. Further research is needed to confirm this, but it may be true that because _JASIS_ authors have, as a whole, published more, they have a larger reservoir of their own material to self cite. This self citation percent statistic and those for _Libres_ and _IR_ are significantly smaller than that for _JIC._

<a id="table-3"></a>

<table><caption>

Table 3\. Selected mean citation characteristics per article 1990/1999 (in percent)</caption>

<tbody>

<tr>

<td> </td>

<td>Avg. Total  
Citns</td>

<td>Avg. Book  
Citns</td>

<td>Avg. Article  
Citns</td>

<td>Avg. Proc.  
Citns</td>

<td>Avg. Gov.  
Doc. Citns</td>

<td>Avg. Internet  
Citns</td>

<td>

_Avg. Self-citns_</td>

</tr>

<tr>

<td>

_CM_</td>

<td>13.3</td>

<td>1.0 (7.5)</td>

<td>5.0 (37.6)</td>

<td>1.7 (12.8)</td>

<td>0.0</td>

<td>5.0 (37.6)</td>

<td>

_1.3 (9.8)_</td>

</tr>

<tr>

<td>

_IR_</td>

<td>19.7</td>

<td>7.2 (36.5)</td>

<td>8.9 (45.2)</td>

<td>1.3 (6.6)</td>

<td>0.9 (4.6)</td>

<td>2.7 (13.7)</td>

<td>

_1.2 (6.1)_</td>

</tr>

<tr>

<td>

_JIC_</td>

<td>12.4</td>

<td>4.2 (33.9)</td>

<td>4.2 (33.9)</td>

<td>0.0</td>

<td>1.0 (8.1)</td>

<td>7.2 (58.1)</td>

<td>

_3.2 (25.8)_</td>

</tr>

<tr>

<td>

_Libres_</td>

<td>9.9</td>

<td>2.6 (26.3)</td>

<td>6.5 (65.7)</td>

<td>1.0 (10.1)</td>

<td>0.2 (2.0)</td>

<td>1.1 (11.1)</td>

<td>

_0.5 (5.1)_</td>

</tr>

<tr>

<td>

_JASIS_</td>

<td>29.9</td>

<td>9.0 (30.1)</td>

<td>15.9 (53.2)</td>

<td>0.9 (3.0)</td>

<td>0.3 (1.0)</td>

<td>1.2 (4.0)</td>

<td>

_3.3 (11.0)_</td>

</tr>

</tbody>

</table>

#### <a id="web"></a>Web as legitimate resource

Should we consider material found on the World Wide Web a resource appropriate for citation and validation of scientific work? The Web and in fact the Internet as a whole are very recent. The creation of the Web dates only to 1991\. It is therefore possible to track the inclusion of Web based materials into bibliographies and reference sections of journals we analyze. First we suggest that in order for Web materials to be legitimated as reference resources, they must be used as such in major journals. In effect, the inclusion of Web citations constitutes a precedent setting action. We also hypothesize that journals addressing Web related issues will necessarily cite Web material, the subject of their analysis, at a greater rate than more general interest journals. And finally Web based electronic journals will be more likely to include Web references than their more "traditional" counterparts. We use the proportion of citations to Internet materials reported by _JASIS_ authors as the baseline for comparison. In 1990, _JASIS_ carried no citations to the Internet, but by the end of the decade, the proportion of web citations to all citations rose to just under 8%. We suggest that this represents the beginning of an acceptance of citations to the Web as legitimate. This is important for this analysis because it also tends to legitimate the source of some of those citations: the e-journal.

![Figure 1](../p88fig1.gif)

Figure 1 graphs the annual percent of Internet citations to all citations for each of the five journals since 1990\. There were no Web citations in any of these five journals, although there were rare citations to user groups, gophers, and mail lists in _JASIS_ in the late 1980s and early 1990s. Citations to the Internet began to occur in 1994, and varied by year and journal. Note for example, the large number of _Libres_ citations in 1995.  This spike is due in large part to a single article addressing mail lists and user groups, with many citations to those Internet resources. Both _CyberMetrics_ and the _Journal of Internet Cataloging_ show greater than average Web citations, we suggest because of their subject matter. _IR_ mimics the _JASIS_ baseline more than the other three journals because _IR_ is a general purpose information science journal just as _JASIS_ is.

### Funding and Published Research

One hallmark of important or complex research is its funding status. This is a difficult area, because research takes many forms and some problems require more or less time, effort, equipment, and so on than do others. Perhaps we can borrow from physics, and distinguish between the experimentalists and the theorists. Experimentalists necessarily require complex equipment, labor, and time – all costly components of research. Theorists may or may not require costly inputs to achieve their results. We are perhaps not defining the hallmarks of important and unimportant or simple or complex science with a discussion of funding, but rather of big science and little science as meant by [Price](#price) (1963). It is debatable whether information science has achieved the status of big science, although it is certain that aspects of the discipline do require costly and complex approaches. Perhaps these are our experimentalists while the others are our theorists.

In any event, much of the research reported in the information sciences, particularly in the five journals analyzed for this article is unfunded research. These data are shown in Table 4\. _IR_ and _JASIS_ publish more articles as a percent of all articles supported by reported internal or external funds. As [Koehler, et al](#koehler) show, there has been a slow increase in funded research reported in JASIS over its fifty years.

At least part of the reason that less than half of the research reported in the five journals in the 1990s is unfunded is the nature of the research. This paper, for example, is "unfunded" research. Data collection, analysis, and other labor were provided by students and a faculty member as part of their normal "unfunded" responsibilities. The journals were accessed "free" because they are available on-line, are part of the academic library collection, or are part of the professor's private professional collection. The hardware and software used for analysis were either provided as matter of course by the university or owned by researchers. Given an adequate labor pool, this research could also be accomplished in a relatively short time period. In the final analysis, all research is funded. It is just that some research is more funded than others.

<a id="table-4"></a>

<table><caption>

Table 4\. Funded research by journal article, 1990s</caption>

<tbody>

<tr>

<td> </td>

<td>N</td>

<td>Percent Funded</td>

</tr>

<tr>

<td>

_CM_</td>

<td>3</td>

<td>0</td>

</tr>

<tr>

<td>

_IR_</td>

<td>86</td>

<td>34.9</td>

</tr>

<tr>

<td>

_JIC_</td>

<td>30</td>

<td>6.7</td>

</tr>

<tr>

<td>

_Libres_</td>

<td>42</td>

<td>2.4</td>

</tr>

<tr>

<td>

_JASIS_</td>

<td>588</td>

<td>22.1</td>

</tr>

</tbody>

</table>

We have suggested that _IR_ and _JASIS_ publish from and to a more general information science audience than do CyberMetrics, _JIC_, or _Libres_. If it is true that information science consists of both "big" and "little" components, it is also none too surprising to find that the journals serving the whole audience also publish "big" and "little" results. We suggest that the uneven funding distribution for these journals is in part explained by their target audiences and article sources.

### Author Characteristics

The majority of authors are responsible for a single article in the five subject journals over the study period. There were 1611 authors (including multiple counts for multiple articles) for 916 articles. Because of authors writing multiple papers, there were 1182 discrete individuals producing those articles. The number of articles per issue ranged from one to nine.

Four authors were responsible for ten or more articles each. They are included in the 216 authors who published two or more articles in one or more of the journals. Of these, 16 published in two or more of the five journals, and two have published in three or more. Because _JASIS_ was published over the entire period of study and because it is published in more issues each than the other four journals combined, there were far more multiple publications in _JASIS_ by individual authors than for the other journals.

<a id="table-5"></a>

<table><caption>

Table 5\. Number of all authors publishing more than a total of one article and the number of articles published</caption>

<tbody>

<tr>

<td>Articles Published  
per Author</td>

<td>

_CM_</td>

<td>

_IR_</td>

<td>

_JIC_</td>

<td>

_Libres_</td>

<td>

_JASIS_</td>

<td>Total</td>

</tr>

<tr>

<td>1+</td>

<td>2</td>

<td>18</td>

<td>8</td>

<td>7</td>

<td>199</td>

<td>216</td>

</tr>

<tr>

<td>2+</td>

<td>1</td>

<td>14</td>

<td>5</td>

<td>2</td>

<td>183</td>

<td>210</td>

</tr>

<tr>

<td>3+</td>

<td>0</td>

<td>5</td>

<td>3</td>

<td>2</td>

<td>79</td>

<td>93</td>

</tr>

<tr>

<td>4+</td>

<td>0</td>

<td>3</td>

<td>3</td>

<td>1</td>

<td>42</td>

<td>51</td>

</tr>

<tr>

<td>5+</td>

<td>0</td>

<td>1</td>

<td>3</td>

<td>1</td>

<td>24</td>

<td>30</td>

</tr>

</tbody>

</table>

#### <a id="gender"></a>Gender

Gender data can be interpreted in several ways. First, we find that of the 1611 authors for the 916 articles analyzed, 64% were male, 32% female, and 4% unknown. Note that because some individuals have authored more than one article in the set, they are counted multiple times in these totals.

If the _JASIS_ gender distribution represents a baseline for comparison, Table 6 indicates that the "typical distribution" is one third women and two thirds men as first and all authors. [Koehler et al](#koehler) theorize that these data approach but under represent the number of women in the disciplines publishing in _JASIS_.  The four journals under analysis for this paper, _CyberMetrics, Information Research, Journal of Internet Cataloging,_ and _Libres,_ each have a greater percent of both first and all female authors than _JASIS_. We believe that there are three possible explanations for this. First, there are more women in the disciplines attracted to publishing in the four journals than there are in information science in general; second,  more women are interested in the subject matter these journals represent than in information science in general; and third, women submit articles to the newer journals because they have a greater expectation of publication success in those journals. As is shown in Tables [8](#table-8) and [9](#table-9), below, most of the authors have academic bases and of those, most are from schools of library and information science or are library staff. Both fields have larger women representation than most of the other fields represented.

<table><caption>

Table 6.<a id="journal-author-gender"></a>Percentage journal author gender distribution in the 1990s</caption>

<tbody>

<tr>

<td> </td>

<td colspan="4">First Authors</td>

<td colspan="4">All Authors</td>

</tr>

<tr>

<td>Journal</td>

<td>Female</td>

<td>Male</td>

<td>Unknown</td>

<td>N</td>

<td>Female</td>

<td>Male</td>

<td>Unk</td>

<td>N</td>

</tr>

<tr>

<td>

_CM_</td>

<td>33.3</td>

<td>66.7</td>

<td> </td>

<td>3</td>

<td>33.3</td>

<td>66.7</td>

<td> </td>

<td>3</td>

</tr>

<tr>

<td>

_IR_</td>

<td>48.5</td>

<td>51.2</td>

<td> </td>

<td>86</td>

<td>41.3</td>

<td>58.7</td>

<td> </td>

<td>154</td>

</tr>

<tr>

<td>

_JIC_</td>

<td>53.3</td>

<td>46.7</td>

<td> </td>

<td>30</td>

<td>46.8</td>

<td>53.2</td>

<td> </td>

<td>47</td>

</tr>

<tr>

<td>

_Libres_</td>

<td>47.5</td>

<td>50</td>

<td>2.5</td>

<td>40</td>

<td>43.6</td>

<td>54.5</td>

<td>1.8</td>

<td>55</td>

</tr>

<tr>

<td>

_JASIS_</td>

<td>30.0</td>

<td>65.5</td>

<td>4.5</td>

<td>754</td>

<td>30.1</td>

<td>65.7</td>

<td>4.2</td>

<td>1347</td>

</tr>

</tbody>

</table>

Figure 2 charts the variation in gender by presenting the percent of all "known" female authors for each journal since 1990 or commencement of publication. "Known" authors are those whose gender could be identified either from name clues or by requesting that information from peers, editors, or the authors themselves. We include all authors rather than just first authors because it is difficult to [prioritize authorship](#assessing) order in the information science discipline and because secondary authorship is one method by which more junior participants can begin to be recognized by a field. If women are beginning to participate at greater and greater rates, they are likely to begin to do so as junior team members. As individuals become more senior, so will their authorship standing.

![Figure 2](../p88fig2.gif)

Figure 2 indicates a fairly level _JASIS_  female publication rate for the 1990s, with variation around 30%. It should be noted that [Koehler et al](#koehler) (2000) document a slow but perhaps inevitable increase in female author participation from 1950 through 1999\. Data for _JIC_, _Libres_, and _IR_ are less constant than for _JASIS_ and tend to be somewhat higher than for _JASIS_. _CyberMetrics_ is not included in the figure because it has published but three articles, two by the same author (a man) and one by a woman. The annual fluctuations are due we believe to the relatively low "n" for each of the journals as compared to _JASIS_, and therefore to the vicissitudes of selection, submission, and editorial decision making.

#### <a id="nationality"></a>Nationality and globalization

Transnationalism is defined in the international relations literature as the social, economic, political communication across national boundaries among actors at least one of which is not an agent of a government or of a state empowered to enter into intergovernmental or interstate agreements ([Nye & Keohane](#nye), 1972). Science is said to be borderless. Academic journals report scientific findings. Are those journals as borderless as the science they report or do their author distributions reflect parochialism? [Koehler et al](#koehler) (2000) demonstrate that _JASIS_ has become more transnational in its author pool overtime. In its early years, the pages of _JASIS_ were almost completely dominated by US-based authors. _JASIS_ now draws its authors from throughout the world, although North American based authors still predominate.

We suggest that over time information science journals will become more global in their author distribution. There are however, several factors that tend to counter that trend. The first is language of publication. The journals we explore are all published in English. It is well recognized that English is the language of science. In order to reach a broad audience, it is necessary to publish in that language. However, authors whose first language is not English or who wish to reach audiences who do not read English may well publish in languages other than English. While it is true that most academic scientists are driven by "publish or perish" considerations, it may be that the locus of the publication is also important. We are struck, for example, by the _Journal of Internet Cataloging_'s volume two, first issue. In it are published papers from a conference in Mexico in English. More interesting is the fact that all but one contributor is from the United States. Can we infer that Mexican authors prefer for professional reasons to publish elsewhere?

The country of publication may be important in establishing the author pool, at least initially. We show, for example, that _Information Research_, a journal published in the United Kingdom at Sheffield University, drew most of its authors early on from the United Kingdom and more specifically from Sheffield University. However, as _IR_ matured, its author pool broadened significantly. Contrast this experience with _CyberMetrics_, a journal published in Spain but in the English language. _Libres_, published at an uncertain but English speaking location (Australia, UK, or US) also tends to draw the majority of its authors from anglophone countries. Perhaps because it is the oldest of the e-journals, its author pool is broader and includes more non-anglophone places.

The _Journal of Internet Cataloging_ is a p-journal. To date, its author base is almost entirely drawn from the United States. The sole exception is the Mexican author already referred to. Note also that between its inception in 1997 and 1999, CyberMetrics has published but three articles. _JIC_ and _CyberMetrics_ differ from the other journals in that these are journals publishing in areas of narrow interest, while _IR_, _JASIS_, and _Libres_ are information science publications with a broader mandate.

We suggest therefore that the age, publication location, and focus of the journal affect the author pool from which it may draw. That, in turn, affects publication frequency,  number of articles, and perhaps the quality of articles the journal may publish. It may also be that the medium of publication -- paper, electronic, or both -- will influence that author pool as well.

<table><caption>

Table 7\. Distribution by journal by region of all corporate authors in the 1990s, in percent for each journal</caption>

<tbody>

<tr>

<td> </td>

<td>

_CM_</td>

<td>

_IR_</td>

<td>

_JIC_</td>

<td>

_Libres_</td>

<td>

_JASIS_</td>

</tr>

<tr>

<td>N</td>

<td>

_3_</td>

<td>

_154_</td>

<td>

_47_</td>

<td>

_56_</td>

<td>

_1327_</td>

</tr>

<tr>

<td>

**Africa**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.5</td>

</tr>

<tr>

<td>

**America**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>North</td>

<td> </td>

<td>10.4</td>

<td>97.8</td>

<td>67.9</td>

<td>75.7</td>

</tr>

<tr>

<td>Latin</td>

<td> </td>

<td> </td>

<td>2.2</td>

<td> </td>

<td>0.3</td>

</tr>

<tr>

<td>

**Asia**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Market</td>

<td> </td>

<td>2.6</td>

<td> </td>

<td>7.1</td>

<td>1.1</td>

</tr>

<tr>

<td>Socialist</td>

<td> </td>

<td> </td>

<td> </td>

<td>1.8</td>

<td>1.2</td>

</tr>

<tr>

<td>

**Europe**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>West</td>

<td>66.7</td>

<td>13.0</td>

<td> </td>

<td>11.3</td>

<td>11.8</td>

</tr>

<tr>

<td>East</td>

<td> </td>

<td>1.3</td>

<td> </td>

<td>5.4</td>

<td>0.6</td>

</tr>

<tr>

<td>CIS</td>

<td> </td>

<td>1.3</td>

<td> </td>

<td> </td>

<td>0.5</td>

</tr>

<tr>

<td>UK</td>

<td> </td>

<td>68.2</td>

<td> </td>

<td>1.1</td>

<td>5.0</td>

</tr>

<tr>

<td>

**Oceania**</td>

<td> </td>

<td>2.6</td>

<td> </td>

<td>7.1</td>

<td>2.4</td>

</tr>

<tr>

<td>

**Middle East**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Arab</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.4</td>

</tr>

<tr>

<td>Israel</td>

<td>33.3</td>

<td>0.6</td>

<td> </td>

<td>5.4</td>

<td>0.5</td>

</tr>

</tbody>

</table>

![Figure 3](../p88fig3.gif)

Figure 3 "consolidates" the data from Table 7 for three of the journals: _IR_, _JASIS_, and _Libres_ into regions (NAm for North America, WEur for Western Europe, UK for the United Kingdom, and Oth for all others). The other two journals were not included because of low "n" (_CyberMetrics_) and overwhelming regionalism (_JIC_). The regionality for each of the journals is demonstrated.  As is shown in Table 7, _Libres_ has greater author participation from other regions, including authors from those areas not represented by the other journals. Its author base from Australia and New Zealand may be due in part to its wider editorial and publication base.

![Figure 4](../p88fig4.gif)

Figure 4 plots the corporate author distribution in the 1990s for _JASIS_ and Figure 5 provides similar data for _IR_. The _JASIS_ plot is fairly constant across the decade, although again it should be pointed out that over its fifty year life, _JASIS_ has become more transnational in character ([Koehler et al](#koehler), 2000).  _IR_, on the other hand, went from an e-journal with an author base entirely drawn from the United Kingdom to one with representation from multi-regions. The _IR_ plot represents, we believe, an interesting and apparently successful strategy by its editor to ensure an adequate article base at the onset of publication. As the journal has become more successful and recognized, it has diversified its author base. UK based authors were represented at a rate greater than others, although by 1999 that rate declined and can be expected to continue to do so as _IR_ continues to diversify its author base.

![Figure 5](../p88fig5.gif)

Perhaps one explanation for the overwhelming North American representation in the _JIC_ article mix is the type of corporate seat of its authors. As is shown in Table 8, the other four journals draw much of their articles from academe while _JIC_ attracts more from the corporate and government sectors. Moreover, where the other journals tend to attract articles from academic teaching and research departments, _JIC_ has a larger base in libraries. Further research is needed to substantiate reasons, but perhaps _JIC_ draws more authors from a single region because of the nature of its author and reader networks. It is certain that the reason for the regionalism is not the journal's subject interest - Internet cataloging. Great interest in the subject has been shown throughout the world.

#### Discipline

The five journals we analyze are information science journals. Yet, as we have argued, they represent different constituencies within the larger field. Table 8 presents data for the corporate home of the journal authors. All draw a majority of their authors from academe, but two (_JIC_ and _JASIS_) attract authors from other arenas as well. _JASIS_ has historically had strong representation from non-academic authors. It was not until the 1970s that a majority of _JASIS_ authors came from academe ([Koehler et al.](#koehler), Table 13). It is also perhaps noteworthy that of the five journals, only _JASIS_ and _JIC_ are published solely in North America. It is possible that the size of the market affects the author pool. It is also important to note that _JIC_ has its roots not in academic departments but in libraries and the corporate sector. Many of its contributors are drawn from OCLC, a not for profit organization with a strong interest in Internet cataloging.

<a id="table-8"></a>

<table><caption>

Table 8\. Corporate Affiliation First and Second Authors, 1990-99 in percent</caption>

<tbody>

<tr>

<td> </td>

<td>Academic</td>

<td>Corporate</td>

<td>Government</td>

<td>Other</td>

</tr>

<tr>

<td>

_CyberMetrics_</td>

<td>100.0</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

_IR_</td>

<td>95.0</td>

<td>1.7</td>

<td>0.8</td>

<td>2.5</td>

</tr>

<tr>

<td>

_JIC_</td>

<td>65.0</td>

<td>30.0</td>

<td>5.0</td>

<td> </td>

</tr>

<tr>

<td>

_Libres_</td>

<td>95.7</td>

<td> </td>

<td> </td>

<td>4.3</td>

</tr>

<tr>

<td>

_JASIS_</td>

<td>85.2</td>

<td>6.1</td>

<td>2.4</td>

<td>5.3</td>

</tr>

</tbody>

</table>

Table 9 shows the distribution of authors by department, subdivisions of the entities displayed in Table 8.  This includes not only academic but corporate and government entities as well. _JIC_, as suggested above, draws much of its author base from libraries, as does _Libres_. _JASIS_ has the greatest dispersion among departments of the five journals.

_IR_ has the largest library and information science representation. This is, in part, an artifact of its founding – all Sheffield, almost all Department of Information Studies authors. Its author base is more distributed in more recent issues.

<a name="table-9"></a>

<table><caption>Table 9 Departmental affiliation of first and second authors, 1990-99 in percent</caption>

<tbody>

<tr>

<td> </td>

<td>Lib. & Info. Studies</td>

<td>Library</td>

<td>Sci-Tech</td>

<td>Soc. Sci./Humanities</td>

<td>Other</td>

</tr>

<tr>

<td>

_CyberMetrics_</td>

<td>33.3</td>

<td> </td>

<td>66.6</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

_IR_</td>

<td>74.3</td>

<td>3.8</td>

<td>8.6</td>

<td>1.0</td>

<td>12.3</td>

</tr>

<tr>

<td>

_JIC_</td>

<td>13.3</td>

<td>60.0</td>

<td>14.3</td>

<td> </td>

<td>12.4</td>

</tr>

<tr>

<td>

_Libres_</td>

<td>20.7</td>

<td>55.2</td>

<td>10.3</td>

<td>3.4</td>

<td>10.4</td>

</tr>

<tr>

<td>

_JASIS_ (first authors only)</td>

<td>43.6</td>

<td>5.8</td>

<td>32.5</td>

<td>5.2</td>

<td>18.7</td>

</tr>

</tbody>

</table>

The data presented in Tables 8 and 9 reinforce the conclusion that each of the five journals represents a somewhat different component of the information science community. It is not surprising therefore that different journals have different element mixes and serve different author pools and readers.

## Conclusions

The journals explored for this article differ from one another in some very important ways. First, they have very different publication records. _JASIS_ has been a mainstay since 1950, and in the 1990s the number of issues and [](#)articles published continued to increase to average 59 articles per year. _IR_ is the next most "productive", with 17 per year. With the exception of _CyberMetrics_, all journals reflect the trend toward [multi-authorship](#), where between 25% and 50% of the articles have more than one author.

There is variability among journals for citation counts. Individual _JASIS_ articles tend to carry more citations than do the other journals. This is, in part, a function of article length. The pattern of citations among citation types does not vary greatly, except for citations to Web based materials. We believe these differences are in large part because of the target subject for the journals. For example, the _Journal of Internet Cataloging_ and _CyberMetrics_ contain the greatest percentage of Web citations. Both of these journals address the Web and necessarily must cite it.  _JASIS_, on the other hand, has among the fewest. _JASIS_ is a more traditional journal. We suspect its authors tend to follow more traditional citation patterns. It must be noted, however, that the percent of citations to the Web has increased over time, from zero in 1990 to almost 8% for _JASIS._ In 1999, Web citations comprised an even greater proportion of all citations, as is shown in Figure 1\. This reflects a greater legitimization of Web resources for purposes of citation. The _JASIS_ values probably reflect a conservative adoption strategy, while the other journals either address the subject or their authors have a less traditional approach to citations.

We believe that both _JASIS_ and now perhaps _IR_ are perceived by their author cohorts as archival or journals of record, while the others are perceived as publishers of works in progress. This conclusion is supported in part by the data presented in [Table 4](#). Articles published in _JASIS_ and _IR_ are the result of funded research at a rate far greater than the other journals. This suggests that some funded research publication represents findings at a more mature stage of development. Data presented in [Table 5](#) may also carry a similar implication. Articles with multiple authors are sometimes perceived as "more scientific" or at least reflect more complex constructs than those with single authors. _JASIS_, _IR_, and _JIC_ tend to carry mutiple-author pieces at rate greater than _CyberMetrics_ or _Libres._

There are author gender differences among the five journals. The four journals under analysis for this paper, _CyberMetrics, Information Research, Journal of Internet Cataloging,_ and _Libres,_ each have a greater percentage of both first and all female authors than _JASIS_. We believe that there are three possible explanations for this. First, there are more women in the disciplines attracted to publishing in the four journals than there are in information science in general; second,  more women are interested in the subject matter these journals represent than in information science in general; and third, women submit articles to the newer journals because they have a greater expectation of publication success in those journals. As is shown in Tables [8](#) and [9](#) most of the authors have academic bases and of those, most are from school of library and information science or are library staff. Both fields have larger women representation than most of the other fields represented.

Given that science, and by inference information science, is a borderless and transnational activity, journals should reflect that global character in their author distributions. The five journals analyzed manifest some parochialism. More than 90% of _JIC_ authors and about three quarters of _JASIS_ authors in 1990s are from North America, while almost 70% of _IR_ authors are from the United Kingdom.  _[IR](#)_ has become less parochial overtime,  while the [_JASIS_ distribution](#) has been more constant over the 1990s. This we believe marks _IR_ as a maturing journal. That said, and with the exception of _Libres_, the overwhelming author source for these journals in the 1990s has been North America and Western Europe.

_IR_ may well be a good model for the successful development of a journal. Its early numbers were dominated by authors from a single university in a single country. It has since expanded its author base; and if funded research is an indicator, it has begun publishing more "mature" findings. _CyberMetrics_ and _JIC_ are struggling and may or may not achieve journal maturity if their frequency and publication schedules and author national distributions can be considered as indicators.

Different journals are of interest to different constituencies. As is shown in Tables [8](#table-8) and [9](#table-9), all of the journals attract most of their authors from the academic community. There is, however, variation among the departmental sources of authors. The _Journal of Internet Cataloging_ and _Libres_ attract most of their authors from the librarian community. Discounting _CyberMetrics_ because of its small author pool_,_ only _JASIS_ attracts a significant pool of authors from the science-technology community.  _IR_, _JIC_, as well as _JASIS_ rely on the library and information science departments for the greatest proportion of their author constituencies. In the early 1980s, [Machlup & Mansfield](#machlup) (1983) described the information sciences as some 40 disciplines. Our analysis suggests that because of the author corporate and disciplinary distributions we have shown for each of the journals, _JASIS_ is a more general purpose publication, while the others have a more targeted author pool and perhaps therefore more targeted disciplinary and reader pools.

In sum, we have examined several characteristics of five journals. Three of those journals are e-journals, one is an h-journal, and one is a p-journal. All but one were started in the 1990s. Two have fairly narrowly defined constituencies, three publish to the larger information sciences communities. We find that newer journals exhibit "growing pains," that the more general journals tend to achieve a degree of stability more quickly, and these general journals also tend to grow their author bases rapidly. All of the newer journals attracted a greater percent of women authors. We conclude that the phenomena we document are attributable to "traditional" journal growth factors. The journal medium - electronic, print, or both - appears to have minor impact on journal publication patterns. There are two exceptions. E-journal are less concerned with space limitations. E-journals are also "easier" to distribute.

A final closing cautionary word is in order. This study addresses five journals among many in a broad and disparate field. Interpretation of these results should be tempered by these limits. Most of the journals examined are very new. Some are very subject focused. Additional research is required within and outside the field before generalizing further.

## Acknowledgments

The authors wish to acknowledge and thank the anonymous referees for their suggestions and recommendations for improvement of this article.

## References

*   <a id="al-ghamdi"></a>Al-Ghamdi, A., Al-Harbi, M., Beacom, N.A.B., Dedolph, J., Deignan, M., Elftmann, C., Finley, N., LoCicero, L., Middlecamp, J., O'Regan, C., Pluskota, F., Ritter, A.A., Russell, S., Sabat, I., Schneider, J., Schoeberl, M., Tragash, P., and Withers, B. (1998). "Authorship in JASIS: A quantitative analysis." _Katherine Sharp Review_ **6**: [http://edfu.lis.uiuc.edu/6/al_ghamdi.pdf.](http://edfu.lis.uiuc.edu/6/al_ghamdi.pdf.)
*   <a id="bates"></a>Bates, M. (1999). "A tour of information science through the pages of JASIS." _Journal of the American Society for Information Science,_ **50**: 975-993.
*   <a id="buckland"></a>Buckland, M. (1999). "The landscape of information science: The American Society for Information Science at 62." _Journal of the American Society for Information Science,_ **50**: 970-975.
*   <a id="cano"></a>Cano, V. (1999). "Bibliometric overview of library and information science research in Spain." _Journal of the American Society for Information Science,_ **50**: 675-680.
*   <a id="carter"></a>Carter, R.C. and Kascus, M.A. (1991). "Cataloging & Classification Quarterly, 1980-1990: Content, change, and trends." _Cataloging & Classification Quarterly,_ **12**: 69-79.
*   <a id="cline"></a>Cline, G.S. (1982). College & Research Libraries: Its first forty years. _College & Research Libraries,_ **43**_:_ 209-232.
*   <a id="harsanyi"></a>Harsanyi, M.A. (1993). Multiple authors, multiple problems -- bibliometrics and the study of scholarly collaboration: A literature review. _Library and Information Science Research,_ **15**: 325-354.
*   <a id="jarvelin-90"></a>Jårvelin, K and Vakkari, P. (1990). "Content-analysis of research articles in library and information-science." _Library & Information Science Research,_ **12**: 395-421.
*   <a id="jarvelin-93"></a>Jårvelin, K. and Vakkari, P. (1993). The evolution of library and information-science 1965-1985: A content-analysis of journal articles. _Information Processing & Management,_ **29**: 129-144.
*   <a id="koehler"></a>Koehler, W.C.,  Anderson, A.D., Dowdy, B.A., Fields, D.E., Golden, M.,  Hall, D.,  Johnson, A.C., Kipp, C., Ortega, L.L., Ripley, E.B., Roddy, R.L., Shaffer, K.B., Shelburn, S. and Wasteneys, C.D.,  (2000)  "A Bibliometric Exploration of the Demographics of Journal Articles: Fifty Years of American Documentation and the Journal of the American Society for Information Science" Under review at _CyberMetrics_. Available on line: [http://www.ou.edu/cas/slis/courses/Methods/jbib/](http://www.ou.edu/cas/slis/courses/Methods/jbib/)
*   <a id="kuch"></a>Kuch, T.D.C. (1978)."Relation of title length to the number of authors in journal titles." _Journal of the American Society for Information Science,_ **29**: 200-202.
*   <a id="machlup"></a>Machlup, F. and Mansfield, U. (1983). _The Study of Information: Interdisciplinary Messages_. NY: John Wiley & Sons.
*   <a id="nye"></a>Nye, J.S. and Keohane, R.O. (1972). "Introduction", _in:_ _Transnational Relations and World Politics_, edited by R.O. Keohane and J.S. Nye. Cambridge, MA: Harvard University Press
*   <a id="olsgaard"></a>Olsgaard, J. and Olsgaard, J. (1980). "Authorship in five library periodicals." _College & Research Libraries,_ **41:** 49-54.
*   <a id="price"></a>Price, D. J. de Solla. (1963). _Little Science, Big Science._ NY: Columbia University Press.
*   <a id="saracevic"></a>Saracevic, T. and Perk, L. (1973). "Ascertaining activities in a subject area through bibliometric analysis." _Journal of the American Society for Information Science,_ **24**_:_ 120-134.
*   <a id="smiraglia"></a>Smiraglia R.P and Leazer, G.H. (1995). "Reflecting the maturation of a profession: Thirty-five years of Library Resources & Technical Services." _Library Resources & Technical Services,_ **38**: 27-46.
*   <a id="stephenson"></a>Stephenson, M.S. (1993). The Canadian Library Journal, 1981-91: An analysis -- Le Canadian Library Journal, 1981-91: Analyse. _CJILS/RCSIB,_ **18**: 1-18.
*   <a id="terry"></a>Terry, J.L. (1996). "Authorship in College & Research Libraries revisited: Gender, institutional affiliation, collaboration." _College & Research Libraries,_ **58**: 377-383.
*   <a id="yitzhaki"></a>Yitzhaki, M. (1994). "Relation of title length of journal articles to number of authors." _Scientometrics,_ **30**: 321-332.