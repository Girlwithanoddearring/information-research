#### Information Research, Vol. 7 No. 1, October 2001,

# Environmental scan on women's health information resources in Ontario, Canada

#### [Christine Marton](mailto:marton@fis.utoronto.ca )  
Faculty of Information Studies  
University of Toronto  

Toronto, Canada

#### **Abstract**

> This paper describes the development of an environmental scanning system by a group of academics and health librarians for the purpose of conducting a consumer scan on women's health information resources on behalf of Ontario Women's Health Council, Ontario Ministry of Health and Long-Term Care. The focus of this paper is on the use of database technology in information acquisition for environmental scanning.

## Introduction

Ontario is one of the largest and most prosperous provinces in Canada with a diverse population, in terms of language, ethnoracial identity and regional differences. Health care in Canada is a provincial jurisdiction, in contrast to the United States and United Kingdom. The Ontario Ministry of Health and Long-Term Care is responsible for the funding and maintenance of all health care institutions in Ontario. Since the mid-1990s, the health care system in Ontario has been subjected to many changes. The provincial government has undertaken massive restructuring of the health care system, which has resulted in the reorganization and merger of existing health care institutions, from hospitals to home care agencies, often against their will and without extensive consultation ([Sinclair & Nickologff, 1998](#ref22); [Sinclair, Rochon and Kilbertus, 1998](#ref23); [Burke & Greenglass, 2000](#ref2); [Michalski, Ceighton & Jackson, 1999](#ref16); [Goyder, 1999](#ref10)).

Women represent approximately fifty percent of the population of Ontario. The Ontario Women's Health Council (OWHC) was established in December 1998 by the Ontario Minister of Health and Long-Term Care as an advisory council to the Ministry. Its mandate is to improve the health of all women in Ontario in all stages of their lives. One component of this mandate is to support women's informed decision-making.

The provision of health information to women is essential to informed decision making about healthy lifestyle choices and the appropriate use of health care services. Health information is produced in many formats (print, electronic, Internet-based), by many groups (government health and social services ministries, community groups, women's groups, pharmaceutical companies, and book publishers, to name but a few), and covers many health topics (biomedical conditions and social issues). The OWHC must be informed of the types of health information available to women in Ontario so it can address gaps in information resources through the provision of funding for resources on health topics that are under-represented, in formats that are accessible to women who represent many different languages, cultures, religions and regions. However, the ability of the OWHC to conduct an audit of women's health information resources is made difficult by the many changes occurring in the health care sector, in particular, the merger of health care institutions in Ontario. One example is the merger of Sunnybrook Health Sciences Centre, Women's College Hospital and Orthopedic and Arthritic Hospital to form Sunnybrook and Women's College Health Sciences Centre. When institutions merge and staff at the formerly independent institutions leave, the corporate memory of their published information resources is impaired or lost altogether. Environmental scanning, and the use of formalized scanning systems in environmental scanning, is an appropriate strategy for organizations to employ to keep apprised of events and trends in the external environment. ([Choo & Auster, 1993](#ref3))

In the spring of 1999, the OWHC issued a request for proposals (RFP) for a consumer scan on women's health issues.

The request for proposals asked that three specific services be undertaken:

1.  Conduct a consumer education scan to determine what health information exists in the public domain on women's health issues, particularly those resources that have a health promotion focus (prevention and education).
2.  Identify gaps in women's health information resources from the consumer education scan and through dialogue with women's health organization representatives, in the form of focus groups.
3.  Make recommendations, in the form of a report, addressing the gaps and identifying the optimal media and methods of dissemination of women's health information to the greatest number of women.

## Methodology

The project was a collaborative endeavour between four institutions: the Faculty of Information Studies, University of Toronto; The University Health Network, Fudger Library; the Consumer Health Information Service, and Ryerson Polytechnic University School of Nursing. Each organization contributed to the project their unique knowledge and perspective on consumer health information resources. The involvement of both academics and practitioners in librarianship and nursing created an intellectually stimulating and productive scanning team.

The Consumer Education Scan on Women's Health Issues was conducted in two parts:

1.  development of a database of health information resources and
2.  focus group discussions.

A review of the literature on provision of health information to the public and theories of health information-seeking behaviour comprises a third component, not originally identified in the Request for Proposals (RFP).

## Developing an environmental scanning system

While survey methodology (questionnaires and interviews) is the most widely used method of environmental scanning, the use of database technology has been discussed since the mid-1980s ([Danzinger, 1986](#ref6); [Morrison, 1988](#ref17)). However, these studies did not examine environmental scanning in the health care sector.

There are many journal articles on the use of health databases indexed in MEDLINE, including the use of databases in hospital information systems and government population health registries ([Saunders, Mathers, Parry & Stevens, 2001](#ref20); [Dean, Vernon, Cook, Nechodom, Reading, & Suruda, 2001](#ref7); [Hawkins, Young, Hubert & Hallock, 2001](#ref13); [Sheikh & Hurwitz, 2001](#ref21)). Yet, a MEDLINE search on "environmental scanning" retrieved very few studies, none of which discussed the use of database technology in environmental scanning. This is surprising because environmental scanning is widely employed in the Canadian health care sector. Two recent examples are the Report on an Environmental Scan of Mental Health Services in First Nations Communities ([Elias, Greyeyes, & Assembly of First Nations, 1999](#ref9)) and the National Plan of Action for Nutrition ([Health Canada, 1998](#ref14)). It would seem environmental scans are published primarily as government reports and consequently do not appear in the academic literature.

In this project, the use of database technology was influenced by an article published in the Journal of the Canadian Medical Association by Arminže Kazanjian, Associate Director of the Centre for Health Services and Policy Research; Associate Professor in the Department of Health Care and Epidemiology, University of British Columbia, and Faculty Associate with the BC Centre of Excellence for Women's Health, BC Children and Women's Hospital and Health Centre. Kazanjian ([1998, p. 344](#ref15)) advocated the study of existing health databases and outlined the characteristics of various types of data and type of analysis relevant to women's health research. Although she was referring to databases containing administrative data and survey data, and was primarily interested in how to link databases to evaluate social and health policy interventions and gender-based analysis of health determinants, it occurred to the Principal Investigator that the use of database technology would be useful for conducting the consumer scan on women's health information resources.

To develop a bibliographic database of women's health information resources, it was necessary first to collect existing print and electronic information resources and provide accurate descriptions of these resources. Microsoft Access 97 for PC was used as the database software because of the ready availability of Microsoft Office on the Faculty of Information Studies computer network.

Analysis of health information resources is facilitated by incorporating the descriptors into the database fields. The descriptors, or attributes, of information resources are as follows:

*   Information source characteristics  
    * Accessibility/availability (cost, time and effort to obtain the resource, readability)  
    * Quality (authoritative, reliability, relevance to user)  

*   Health topic coverage  
    * Reproductive versus non-reproductive health topics  
    * Biomedical versus social determinants  

*   Resource format  
    * Print, oral communication, electronic, Internet  

*   Representation of Canadian society according to  
    * Gender  
    * Language  
    * Ethnoracial identity  
    * Geographic region  

Although the database was created primarily as a data collection and analysis tool, it was also the intent of the project managers to make the database available to all possible interested parties - the public, policy makers, researchers, and clinicians. To achieve this goal, a Web interface was developed for the project database, similar in look and function to a public library online catalog (OPAC). The Web interface is hosted on the [Ontario Women's Health Council website](http://www.womenshealthcouncil.com/scripts/index_.asp). [Note: click on the link ":Search for Health Information"] This searchable database will be of great value to anyone interested in health information resources pertaining to women's health, both in terms of determining what materials are in the public domain and how to obtain them. A public launch of the database was held at the Toronto Reference Library on August 24, 2001.

### Data Collection

The scope of the information resources captured in the project database varied in relation to the method used to collect the resources. To obtain <u>physical</u> information resources (print, videotape, audiotape, CD-ROM), several postal mailings were conducted in spring 2000 (February-May). Letters of request for copies of health information resources were sent to government health and women's ministries/departments (federal and provincial); health-related non-governmental organizations (such as the Heart and Stroke Foundation); women's cultural groups; native groups; environmental groups; professional associations (e.g. Canadian health librarians; Canadian nursing, medical, midwifery and hospital associations), and pharmaceutical companies. Over 1300 agencies, institutions, groups, associations and corporations were contacted. As well, a considerable number of print resources were collected in person by visiting the booths at the annual Women's Health Matters Forum and Expo and various health clinics, such as the Bay Centre for Birth Control.

For _Internet-based_ information resources, such as websites, newsgroups and listservs, data entry staff were given training on the use of Web search engines to locate Internet-based resources developed in Canada and other English-speaking countries. While bilingual resources were included, time did not permit the development of French descriptions for many of these resources. The provision of French descriptions of bilingual resources was addressed in March 2001 with the development of a separate 400-record French-language database.

The disadvantage of this approach to data collection, at least for print and electronic media, is the reliance on obtaining these materials from the organizations that produced them. While some organizations provided a comprehensive collection of their materials, others sent but a few materials, mainly pamphlets, while others chose not respond. Some organizations informed us that the short timeframe for this project did not permit them to spend an adequate amount of time gathering their materials and mailing them to us. Another issue we encountered was that many organizations did not assign an individual or department the responsibility of archiving and distributing their resources. Consequently, some organizations, including large teaching hospitals, were not aware of the consumer health materials they had produced over the past five years. Ideally, the public relations department and/or library of each health institution and agency should function as a repository of all materials developed in-house.

### Database Scope

Several dimensions of scope were identified at the start of the project, in consultation with representatives from the Ontario Women's Health Council. They are date of publication; publication format; intended audience (according to language, geographic location), and health topic. Specifically, we attempted to identify what health information resources pertaining to women's health had been published in all formats (print, electronic, Internet, oral communication) over the past five years (1995-2000) in Ontario, and to a lesser extent in Canada and internationally, in English and French, with a focus on health promotion.

Twenty-four health topic databases were developed which have been merged into one comprehensive database. The health topics examined are as follows:

*   birth control and abortion
*   blood disorders
*   bone (and connective tissue) health
*   cancers affecting women
*   digestion
*   disabilities (sensory disabilities, physical disabilities, neurological impairment)
*   environment and health
*   exercise
*   gynecological health
*   healthy sexuality
*   heart and circulatory (cardiovascular) health
*   hormones (menstruation, menopause, thyroid)
*   immune system, allergies and autoimmune disorders
*   medication and addictions
*   mental health
*   nutrition
*   pregnancy
*   respiratory health
*   sexual orientation
*   sexually transmitted diseases/infections and HIV/AIDS
*   urinary tract disorders
*   violence against women

In terms of health topics covered, the scope of this project adheres to the seven content areas identified by Chesney and Ozer ([1995](#ref4)). They are reproductive health; diseases that are more common in women than men; leading causes of death in women; gender influences in health risk; societal influences in health risk; violence (domestic, physical, sexual); and women and health care policy. This list is similar to the list of Canadian women's health issues identified in the report from the 1990 Conference of Deputy Ministers of Health, Federal/Provincial/Territorial Working Group on Women's Health. This list includes reproductive health; mental health; cancers occurring in women; occupational and environmental health, and nutrition and active living. It is also similar to the list of core themes in women's health research identified by the Working Group on the CIHR, Gender and Women's Health Research ([2000, pp. 11-13](#ref24)). The report, CIHR 2000: Sex, Gender and Women's Health ([Greaves et al., 2000](#ref11)) identifies three research foci - cardiovascular disease, osteoporosis, and violence. Although the scope seems limited, the approach is unique in identifying gender aspects of conditions commonly seen solely as biomedical (cardiovascular disease and osteoporosis) and making the links to other health topics, for example, the relationship between eating disorders, depression and osteoporosis.

The number of health topics covered in this project is more extensive than originally identified in the RFP proposal. Originally, the health topics identified by the project managers for study were the following: cardiovascular health; bone health; mental health; addictions; cancers affecting women; reproductive health (birth control and abortion; sexually transmitted diseases, pregnancy); autoimmune disorders, and violence against women. Despite the expanded coverage, there are some omissions. Health topics that were not covered are skin care and skin disorders; eye care; genetics; occupational health; poverty, and homelessness. In terms of format, electronic media, such as CD-ROMS, television programs, videotapes, audiotapes, and 1-800 telephone services are under-represented. In terms of language, languages other than English are under-represented. In terms of geographic region, northern Ontario agencies and institutions are under-represented.

### Database Structure

A database was constructed for each health topic using Microsoft 97 for Windows software. Each database has the same structure, as follows:

*   a table, in which the data is stored;
*   a form, in which the data was entered;
*   a report, used by staff and management to view the records and check for errors;
*   a set of queries to check for inconsistencies in labelling fields, across all records; and
*   a set of queries to generate descriptive statistics for data analysis.

The selection of database fields was guided by Dublin Core Metadata Set, version 1.0/.1 ([Dublin Core Metadata Initiative, 1999](#ref8)), with the assistance of Professor Wendy Duff at the Faculty of Information Studies. The Dublin Core has fifteen elements: Title, Date, Author or Creator, Other Contributor, Publisher, Rights Management, Subject and Keywords, Description, Coverage, Format, Resource Type, and Resource Identifier, and three optional elements, source, relation, and language. Twenty-eight fields were derived from the metadata set, as listed below:

_Basic bibliographic fields:_

1.  Title
2.  Date
3.  Creator
4.  Contributor
5.  Publisher
6.  HowToOrder
7.  Origin
8.  Rights
9.  Description
10.  Cataloguing Note
11.  Date of Data Entry

_Health keyword fields:_

1.  Body Area (System)
2.  Body Area (Organ, Bone)
3.  Health Condition (Typology)
4.  Health Condition (Common Name)
5.  Health Determinant
6.  Health Action 1
7.  Health Action 2

_Demographic fields:_

1.  Age Group
2.  Geographic Region
3.  Ethnoracial Identity
4.  Audience

_Attributes of the resource:_

1.  Format
2.  Resource Type
3.  Resource Dimensions
4.  Resource Identifier
5.  Relation
6.  Language

Most of the database fields are text fields, which are indexed and therefore searchable. Those fields requiring a substantial amount of space for text input, for example, the Description field, were designated memo fields. The date field is a date/time type and has a specific format: YYYY-MM-DD. Some fields (Title, Creator, Resource Identifier, Description and Cataloguing Note) have two fields: one for English and the other for French, to accommodate bilingual and French-only resources. To make data entry efficient and prevent errors in classifying health keywords and demographic variables, these fields contained pull-down lists. For Internet resources, the resource identifier field was standardized using the URI Generic Syntax developed by Berners-Lee et al. ([1998](#ref1)).

For health keyword fields, specifically the Body Area (System) and Body Area (Organ, Bone) fields, a decision was made to adopt the 1996 Cumulative Index of Nursing and Allied Health Literature ([CINAHL, 1996](#ref5)) subject headings, which is a more flexible classification system than Medical Subject Headings (MeSH). For the health determinant and health action fields, we consulted the Canadian Health Network Thesaurus; the description of health determinants in Population Health Promotion: An Integrated Model of Population Health and Health Promotion ([Hamilton and Bhatti, 1996](#ref12)), and the Subject Focus pull-down list on the Canadian Women's Health Network Database of Women's Health Resources. The Age Group field follows the categories established for MeSH ([National Library of Medicine, 2000](#ref18)).

A partial screen capture of the MS Access database data entry form is presented below:

<figure>

![Figure 1](../p116fig1.gif)

<figcaption>

**Figure 1: MS Access data entry form**</figcaption>

</figure>

### Web Database Search Interface

Two sets of considerations governed the development of a Web search interface for the Microsoft Access 97 health topic databases: the physical dimensions of the Women's Health Council website and the public's lack of training and experience searching Web databases. Several Web search interfaces were examined to gain a sense of what features to include that would enhance usability. They included both general and health search engines, such as,

*   MEDLINE IGM (http://igm.nih.gov/) and PubMed (http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=PubMed);
*   UTCAT: Author/Title/Subject/Call Number Search  
    (http://www.library.utoronto.ca/resources/utcat.html);
*   Yahoo Canada (http://www.yahoo.ca); and
*   AltaVista Canada (http://www.altavista.ca).

Women's health search engines were also examined, including the following,

*   National Women's Health Information Center (http://www.4woman.org/search);
*   Women's Health Matters (http://www.womenshealthmatters.ca), and
*   Canadian Women's Health Network Database of Women's Health Resources (http://server1.minisisinc.com/cwhnlog.htm).

The simple and effective design of the UTCAT search interface and the topic-specific approach of both the Canadian Women's Health Network and the Women's Health Matters search interfaces were considered when developing the Web search interface for this project.

The search engine interface developed for the information resources database offers the searcher several options: to search by health topic, or by resource Title, Author, Organization, or some combination of these fields. There is some degree of redundancy in this interface. For example, the health topic is often identified in the Title of the resource. As well, the Author and Organization are identical if the resource was produced by a group, instead of by an individual. The search query can be restricted by one or more parameters - date of publication; language; format type, and intended audience (age group).

A screen capture of the Web search interface is presented below:

<figure>

![Figure 2](../p116fig2.gif)

<figcaption>

**Figure 2: Web search interface**</figcaption>

</figure>

Web search interface fields utilize Microsoft Access database fields as follows:

<figure>

![Table 1](../p116tab1.gif)

<figcaption>

**Table 1: Web search interface query fields and corresponding MS Access database fields**</figcaption>

</figure>

Database fields are organized in a tabular structure, using coloured backgrounds to break up the monotony of text and organize the text effectively. The most salient fields are presented in the top two panels with the lower panel containing the publisher and how-to-order information.

A partial screen capture of the search result interface, reduced in size, is presented below:

<figure>

![Figure 3](../p116fig3.gif)

<figcaption>

**Figure 3: Search result interface**</figcaption>

</figure>

### Staff Recruitment and Training

The selection of a scanning team is an important component of environmental scanning ([Newsome & McInerney,1990](#ref19)). The skill mix required for conducting this type of scan guides the recruitment of scanning staff. The ideal candidate must possess a sound knowledge of health sciences, as well as an understanding of cataloguing and classification, and experience with online searching, in particular, searching for Internet health sites. Staff hired included four graduate students at the Faculty of Information Studies, two undergraduate nursing students, a graduate student in medical illustration, a graduate student from Community Health, and an experienced health librarian. All possessed some knowledge of a specific health topic, ranging from Lupus and cancer to violence against women, as well as the ability to conduct basic Web searches.

A two-hour training session was held on Friday, February 11, 2000\. The training session covered searching for health information in print and on the Internet, evaluating quality health information resources, and use of MS Access database software and Windows NT on Faculty of Information Studies computers. All staff received a comprehensive print guide on these topics. Furthermore, several guides to database structure and searching for Internet health information resources, a copy of the Health Promotion document, and a copy of CINAHL subject headings were placed in a resource box in the Faculty of Information Studies library, the Inforum. Furthermore, guides were developed on how to catalog each type of resource - print, websites, listservs, and newsgroups.

All staff members signed a confidentiality agreement and were assigned a computer lab card and FIS computer account. Health topics were allocated to individual staff members on the basis of their knowledge of that health topic. Staff were given copies of a one-page Evaluation of Resources sheet with Likert-type scales for evaluating information resources according to different attributes of information quality and accessibility.

The process of evaluating and cataloguing information resources began in February and concluded in September 2000\. Because letters of request for obtaining copies of health information resources were in the process of being mailed during the months of February and March, staff members were instructed to evaluate and catalog Internet resources first, which were located using Web search engines. Once print and electronic resources were received in the mail, they could be sorted according to health topic and distributed to database staff. From June-October 2000, database-checking staff were hired to review the records of each health topic database for missing or incorrectly completed fields. As well, an experienced programmer was hired to create the scripts that would link the Microsoft Access database to its Web interface. This was accomplished using Active Server Pages, a Microsoft technology. The design of the Ontario Women's Health Council website, [http://www.womenshealthcouncil.ca/](http://www.womenshealthcouncil.ca/) was replicated on a test site on the Faculty of Information Studies web server, so that the placement of database fields within the parameters of this website could be visualized.

### Database analysis

There are approximately three thousand records in the comprehensive English-language database; one record represents one information resource. The approximate number of records in each health topic database is provided in Table 2\. The number of records indicates, approximately, the amount of coverage a health topic receives in the public domain. There is some degree of overlap between health topics, which presented problems in determining how to catalog a resource according to health condition. Some examples of resources with multiple topics include, lesbians with cancer (originally catalogued in the sexual orientation database instead of the cancer database), nutrition and osteoporosis (catalogued in both nutrition and bone databases); and leukemia (catalogued in the blood database but not in the cancer database). Consequently, when the individual health topic databases were merged to produce the final database, it was not uncommon to find duplicate records.

<figure>

![Table 2](../p116tab2.gif)

<figcaption>

**Table 2: Number of records per health topic**</figcaption>

</figure>

The analysis of health topic databases focused on several attributes of the resources:

*   Information source characteristics  
    * Quality (authoritativeness)
*   Health topic coverage  
    * Reproductive versus non-reproductive health topics  
    * Biomedical versus social determinants
*   Resource format  
    * Print, electronic, Internet
*   Representation of Canadian society according to  
    * Gender  
    * Language  
    * Ethnoracial identity  
    * Geographic region

These attributes are obviously not exclusive, there is some degree of overlap. For example, accessibility is related, in part but not exclusively, to representation of Canadian society. A resource published exclusively in English illustrates this point. It is accessible to Canadians who read the English language, but not to those Canadians who do not read English. Non-English-speaking Canadians are concentrated in certain regions across the country, for example, rural Quebec and northern Ontario. Similarly, although a resource published in British Columbia in the English language would be accessible to Ontarians in terms of language, it may not be relevant (an attribute of quality) to Ontarians because of differences in the provision of health care services between these two provinces.

The following set of questions were addressed using database queries.

#### Resource format

1.  How many resources are provided in each format (print, electronic, Internet)?
2.  How many resources are provided in multiple formats?
3.  How many resources are provided in novel formats? (e.g. bookmark, sticker)  
    [Format and Resource Type fields]

#### Regional representation

1.  How many resources were published in Ontario?
2.  How many resources were published in Canada (outside of Ontario)?
3.  How many resources were published outside of Canada?  
    [Origin field]

.

#### Gender representation

1.  How many resources are specific to women (use a gender lens)?  
    [Title and Description fields, also health-topic specific]

#### Language representation

1.  How many resources are in English? Bilingual (English-French), French-only, languages other than English, multiple languages?  
    [Language field]

#### Accessibility: education/literacy

1.  Who is the intended audience? Consumer? Researcher? Practitioner? All groups?  
    [Audience field]

#### Ethnoracial identity representation

1.  How many resources were produced for all Canadians?
2.  How many resources focus on a specific ethnoracial population?  
    [Ethnoracial Identity field]

#### Quality

1.  Who is the predominant Canadian author of information resources on a health topic?  
    (alternatively phrased, who in Canada publishes the most material on a health topic?)

#### Health topic coverage (within a health topic database)

1.  Which health topics are covered the most, the least?  
    [Body Area and Health Condition fields]
2.  Is coverage strictly biomedical in focus or does it include social determinants (and which ones)?  
    [Health Determinant and Health Action fields]

## Findings

Several findings were obtained from conducting simple count queries in the project database. They are categorized according to attributes of information resources, as follows.

### Format

For the majority of health topic databases, there is a preponderance of print and Internet health information resources, with considerably fewer resources in electronic media (video tape, audio tape, CD-ROM), oral communication (conferences, symposia), and multiple formats. Print is by far the predominant medium for reproductive health and sexuality topics. This is understandable given that people accessing these resources are likely to do so in the context of visiting a reproductive health or sexual health clinic. Health care clinics such as the Bay Centre for Birth Control, Planned Parenthood, and various public health departments offer excellent print publications, primarily pamphlets and fact sheets, on these topics. However, for topics where advocacy and activism and programs and services are important health actions, namely, sexual orientation and physical disability, more resources are available on the Internet than in print. It is plausible that for grassroots organizations representing disenfranchised and hard-to-reach populations, Internet websites represent a viable means of communicating information because of the anonymity of accessing Internet-based health information in the privacy of the home or at public library terminals.

### Regional representation

Overall, the majority of resources captured in the database were produced in Ontario. This is particularly true for reproductive health, healthy sexuality, and medication and addiction - health areas that are strongly service-oriented. It is considerably less true for biomedical topics, disability topics and sexual orientation, where international resources, predominantly American health websites, outnumber resources produced in Canada.

### Language Representation

For all health topics, approximately 75% or more resources are English-only, while approximately 10-20% are bilingual (English-French), and 1% or less are written in languages other than English or French.

### Ethnoracial Identity

For all health topics, approximately 80% or more of resources are generic - they are intended for all Canadians. A small percentage of resources are specifically for native/aboriginal Canadians. They are predominantly produced by native/aboriginal groups and government agencies. Although resources exist in other languages, e.g. Chinese, Vietnamese, Spanish, Italian, Portuguese, their number is small. Multilingual resources are produced predominantly by urban public health departments (e.g. Toronto Public Health) and ethnic community organizations.

### Intended Audience

Overall, most health information resources, irrespective of health topic, are aimed at the public (consumer). A fair number of resources, usually websites, are aimed at both consumers and practitioners and/or researchers. A small number of resources, usually authored by and for professional associations, offer content solely for practitioners and/or researchers.

### Gender representation

Gender representation, that is, how well the content of an information resource addresses gendered aspects of health and illness, varies considerably across health topics, and even within a health topic. For some topics, in particular, reproductive health and healthy sexuality, most of the resources are written implicitly for a female audience. For biomedical health topics, most resources do not address gender. There is an assumption that diagnosis and treatment are the same for both females and males. Within a biomedical health topic, coverage of specific health topics is predominantly for a female audience if and only if that health condition is experienced more by women than men, e.g. urinary incontinence. An interesting observation is that a considerable number of materials on addiction (alcohol, tobacco) are aimed at the pregnant woman. Those resources that address addiction in women in general tend to be produced by service agencies that have a predominantly female clientele.

### Authorship

Overall, those who produce the most health information resources on any given health topic tend to be health-related non-governmental organizations and advocacy groups, e.g. Canadian Hearing Society. Government (federal, provincial, municipal) health ministries, departments, and agencies also produce a considerable number of health information resources, predominantly on biomedical health topics. The federal government, specifically Health Canada, is the leading publisher of bilingual health information resources. Hospitals produce health information resources but they are not leaders in this field, with the exception of Sunnybrook and Women's College Health Sciences Centre, Women's College Campus (formerly Women's College Hospital). Patient education and service information resources from hospitals and other health care institutions are particularly lacking, despite the fact that each hospital in Ontario was invited to participate in this project. Interestingly, the most comprehensive set of patient education materials came not from Ontario but from the Queen Elizabeth II Health Science Centre in Halifax, Nova Scotia.

### Health Determinants

_Quality of life_ is one of the highest ranked health determinants, in particular, for disabilities, medication and addiction, biomedical topics, and sexuality topics. For healthy sexuality, _personal health care practices_ was the highest ranked health determinant, indicating a desire on the part of health care agencies to improve or regulate personal health care practices. _Social support_ ranked highly for sexual identity, disability topics, and, interesting to note, breastfeeding. For resources on infertility, the highest ranked health determinant is coping skills.

### Health Actions

For biomedical topics, health education and health promotion are the predominant health actions. Resources that focus on describing health conditions, their diagnosis and treatment, were identified as having a health education focus, while resources that offer suggestions for improving lifestyle choices were identified as having a health promotion focus. A considerable number of resources on biomedical topics offer both health education and health promotion. For medication and addiction, sexual orientation, healthy sexuality and disability topics, the highest ranked health action is services and programs. Advocacy and activism is an important health action for birth control (particularly abortion) and disabilities. Research appears to be an important health action for environment and health, indicating a lack of answers on the effects of environmental contamination on human health.

## Identification of Gaps

Comparing the results from each of the three research methods: database analysis, focus group analysis, and literature reviews, permits the identification of gaps in the provision of health information resources to women in Ontario. Gaps are categorized according to attributes of health information resources, as follows.

### Language

Between eighty to ninety percent of resources are English-only, with a small number of bilingual resources produced predominantly by Canadian government agencies and departments. Those materials that are available in other languages are too frequently just translations of the English version.

### Health topic area

The majority of information resources are biomedical in focus with a health promotion element focusing on lifestyle changes. There is little if any coverage in the following areas: social determinants of health; health services availability and costs; alternative/complementary therapies, and the linkages between environmental degradation and health. Several health topics are poorly covered; gynecological cancers; hysterectomy and fibroids; mental health issues; nervous system diseases; relationship and parenting issues; aboriginal health; lesbian health; and surgical procedures. Health issues that pertain disproportionately to women have the largest number of health information resources in the public domain - pregnancy, breast cancer, and violence against women.

### Population

The multicultural and ethnoracial identity of Canadian women is not adequately addressed. Not only are there few if any resources for them in their language, but what material exists may be culturally insensitive in concept. There appears to be no distinction made between the health information needs of rural versus urban women, despite the fact that rural women experience a much higher degree of isolation and dependence than do urban women.

### Resource type

Print, especially in pamphlet form, remains as popular a format as ever but much of the material for women adheres to a limited biomedical model that continues to be focused on reproductive health and women's traditional role as caregiver. "Popular" conditions such as breast cancer and chronic fatigue syndrome are the focus of much of what is produced to the exclusion of other, equally if not more important, conditions.

Electronic media such as video and audiotapes, CD-ROMs and television shows are sometimes difficult for consumers to learn about and are not promoted as heavily as mass-marketed health books. They are also more expensive to purchase than print pamphlets and booklets.

Use of the Internet to find health information is growing in popularity. The issue of quality becomes paramount as consumers are faced with a plethora of resources from a wide range of organizations and individuals. Training is required in order for women to effectively search for relevant health websites, otherwise, the likelihood is great that they will stumble upon a commercial, non-Canadian health website that contains a considerable amount of misinformation. The recent development of the Canadian Health Network (CHN) is a positive step. The CHN serves as a meta-directory for high-quality, predominantly Canadian health websites on a range of topics. The Ontario Women's Health Network website also performs a vital role in informing women in Ontario about health issues, as does its federal counterpart, the Canadian Women's Health Network. The database developed as part of this project will also serve to point women in Ontario to useful health information resources produced in ALL formats.

There is an access issue to be considered with electronic media such as videotapes, CD-ROMs and the Internet. The purchase and maintenance of required equipment and the cost associated with Internet access and computer hardware and software upgrades is not trivial. These costs affect women disproportionately because of their lower income with respect to men. This must be considered when organizations make decisions with respect to using the Internet, particularly, the Web as their main communication and information dissemination medium. Currently, fifty percent of Canadians have Internet access, which means fifty percent do not. It is essential that information dissemination includes both print and electronic media formats.

### Literacy

Much of the material in print and in electronic formats is easy to read and understand for those who have some understanding of health sciences, which many citizens may have obtained from high school biology and physical education health classes. However, there is a paucity of materials for those with a low level of literacy and virtually no materials available for those who are mentally disabled and are most vulnerable in their interactions with health and social service agencies. The production of large-print, low-literacy friendly materials is much needed.

## Recommendations

A list of recommendations to the Ontario Women's Health Council was developed, based on identification of gaps in information resources pertaining to women's health. These recommendations are listed in the Executive Summary of the report. A more extensive list of recommendations is also presented in the Recommendations section of the report. The Council will meet in September 2001 to discuss implementation of the recommendations. For the sake of conciseness, only the shorter list of recommendations is listed here, as follows:

1.  Health information must be culturally appropriate and connected to the larger social and political contexts governing women's lives. This requires funding for the development of health information resources that address demographic variables such as gender; age; sexual orientation; ethnoracial identity; language (bilingual and multilingual); and regional differences in health issues and the provision of health services.
2.  Research is required to determine how to effectively disseminate health information to women. Women should be involved in this research partnership. Survey methodology, such as Web-based and print questionnaires, interviews with women from diverse backgrounds, and focus groups that recruit women from the public at large, is an effective means of gathering research data from a large and heterogeneous group of women.
3.  Training programs are required for women to become comfortable in searching for health information resources and to learn to evaluate that information. Health and public librarians can play a valuable role. Funding is required for the development and delivery of training programs that would ideally be offered in public settings, such as libraries, schools and community centres.
4.  The medium of transmitting or sharing information must be appropriate and sensitive to various learning needs. Learning needs can be health topic-specific and format specific. Some people benefit most from reading print materials or accessing text information from the Web. Others prefer interactive media such as CD-ROMs. Still others prefer learning through interaction with others in a group setting.
5.  Canadian-developed resources must be better marketed and disseminated. This applies particularly to websites where "Canadian-only sites" are difficult to locate, despite the effort of the Canadian Health Network. The Ontario Ministry of Health and Long-Term Care and Ontario Women's Health Council can provide listings of high quality Canadian sources. These listings would be available in print format, as a brochure, and in Internet format, as a list of hypertext links to Canadian health information websites.
6.  Funding should be directed specifically towards the development of information resources on the following health topics: social determinants of health; heart disease; gynecological cancers; hysterectomy and uterine fibroids; mental health issues, such as stress and role strain; exercise; neurological disorders; relationship and parenting issues; aboriginal health; lesbian/bisexual/transgendered health issues; treatment options, including specific surgical procedures and what to expect, and alternative/complementary therapies. Information is also required on how to "access" the health care system.
7.  Specific sensitivity must be given to the language and content of materials developed for women with disabilities and other challenges. Avoiding medical terminology and complex wording is recommended.
8.  Ongoing funding to maintain the database developed for this project is required to
    1.  add new information resources
    2.  capture electronic media resources (audio and video tape, television, radio)
    3.  capture multilingual resources
    4.  capture resources produced by women's cultural groups and native groups
    5.  provide French-language descriptions of bilingual resources
    6.  develop a rating system for quality assessment
    7.  develop a rating system for gender coverage
    8.  conduct further research on the database

## Conclusions

The use of database technology to conduct a consumer scan proved to be both challenging and rewarding. Difficulties were encountered in the following areas: recruiting staff with the appropriate skill mix of health knowledge, online searching skills, and database expertise; developing a suitable vocabulary for classifying resources according to health topic; obtaining copies of existing women's health information resources from the many groups, organizations and health care institutions, both small and large, that produce these resources; updating database records for Internet resources, which can change design, content or location (URL) frequently; maintaining communication with assigned contact people at the OWHC, an organization with high staff turnover, and meeting project deadlines. The successful completion of two products for the consumer scan - a bibliographic database of women's health information resources, and a report that relied on database queries to generate quantitative data that revealed gaps in resources, testify that information acquisition using database technology is a viable methodology for environmental scanning. Furthermore, the database is an enduring product of the scan. In this project, it underwent further development to serve another valuable purpose; an online bibliographic database accessible to the public through a Web search interface.

## Acknowledgements

This publication acknowledges that the Ontario Ministry of Health and Long-Term Care is a source of financial support. Any conclusions are those of the author and no official endorsement by the Ministry is intended or should be inferred. Funding was provided by the Ontario Women's Health Council.

Special thanks to Dr. Donna Stewart, Lillian Love Chair in Women's Health at the University of Toronto and Director of the Women's Health Program at the University Health Network, for her advice on women's health issues; to Sheryl Mitchell, Director, Women's Health Matters Partnerships, Sunnybrook and Women's College Health Sciences Centre, for her assistance contacting women's health organizations; to Homa Asayesh, Head Librarian, Women's Health Resource Centre, for her assistance acquiring resources, and to Joyce Byrne, Communications Strategist, Centre for Research in Women's Health, for her evaluation of the Web interface.  
I would also like to extend my gratitude to AbdulraufÊGehani, Network Administrator, and Denyse Rodrigues, Web Content Coordinator, at the Faculty of Information Studies, for their technical support.

## References

*   <a id="ref1"></a>Berners-Lee, T., Fielding, R., & Masinter, L. (1998, August) _Uniform Resource Identifiers (URI): Generic Syntax._ Network Working Group. Request for Comments: 2396 MIT/LCS. Updates: 1808, 1738\. Category: Standards Track.  
    [http://www.ietf.org/rfc/rfc2396.txt](http://www.ietf.org/rfc/rfc2396.txt) [Accessed 3 September 2000]
*   <a id="ref2"></a>Burke, R. J., & Greenglass, E. R. (2000) Hospital restructuring and downsizing in Canada: are less experienced nurses at risk? _Psychological Reports, 87_(3): 1013-1021.
*   <a id="ref3"></a>Choo, C. W., & Auster, E. (1993) Environmental Scanning: Acquisition and Use of Information by Mangers. _Annual Review of Information Science and Technology, 28,_ pp. 279-314.
*   <a id="ref4"></a>Chesney, M.A., & Ozer, E.M. (1995) Women and health in search of a paradigm. _Women's Health Research on Gender, Behavior, Policy, 1,_ 3-26.
*   <a id="ref5"></a>_CINAHL 1996 subject heading list: alphabetical list, tree structures, permuted list._ (1996). Glendale, CA: CINAHL Information Systems.
*   <a id="ref6"></a>Danzinger, P. N. (1986). Competitive intelligence. Building internal databases for environmental scanning. In: _Online '86 Conference Proceedings_Weston, CT.: Online, Inc.
*   <a id="ref7"></a>Dean, J. M., Vernon, D.D., Cook L., Nechodom, P., Reading, J., & Suruda, A. (2001) _Annals of Emergency Medicine, 37_(6), 616-626.
*   <a id="ref8"></a>Dublin Core Metadata Initiative (1999, July 2). _Dublin Core Metadata Element Set, Version 1.1: Reference Description._ [http://purl.oclc.org/dc/documents/rec-dces-19990702.htm](http://purl.oclc.org/dc/documents/rec-dces-19990702.htm) [Accessed 20 February 2000]
*   <a id="ref9"></a>Elias, J. W., Greyeyes, D. J., & Assembly of First Nations. (1999). _Report on an environmental scan of mental health services in First Nations communities in Canada for the Assembly of First Nations._ Prince Albert, SK :Peter Ballantyne Child & Family Services Inc.; Saskatoon, SK : Elias & Associates Consulting Inc.
*   <a id="ref10"></a>Goyder, J. (1999) Petitions, public opinions and hospital restructuring in Kitchener-Waterloo. _Healthcare Management Forum, 12_(1), 14-26.
*   <a id="ref11"></a>Greaves, L., Hankivsky, O., Amaratunga, C., Ballem, P., Chow, D., De Konick, M., Grant, K., Lippman, A., Maclean, H., Maher, J., Mesing, K., & Vissandjee, B. (1999). _CIHR 2000: Sex, Gender and Women's Health_.Vancouver, B.C.: British Columbia Centre of Excellence for Women's Health.
*   <a id="ref12"></a>Hamilton, N., & Bhatti, T. (1996, February). _Population Health Promotion: An Integrated Model of Population Health and Health Promotion._ Health Canada. Health Promotion Development Division.
[http://www.hc-sc.gc.ca/hppb/healthpromotiondevelopment/pube/php/php.htm](http://www.hc-sc.gc.ca/hppb/healthpromotiondevelopment/pube/php/php.htm)

*   <a id="ref13"></a>Hawkins, H. H., Young, S. K., Hubert, K. C., & Hallock, P. (2001) Conceptual database modeling for understanding and developing information management applications. _Radiographics, 21_(3), 781-787.
*   <a id="ref14"></a>Health Canada (1998). [_National Plan of Action for Nutrition_](http://www.hc-sc.gc.ca/datahpsb/npu/) . Ottawa: Health Canada. http://www.hc-sc.gc.ca/datahpsb/npu/ (16 Aug. 2001)
*   <a id="ref15"></a>Kazanjian, A. (1998). Understanding women's health through data development and data linkage: implications for research and policy. _Journal of the Canadian Medical Association, 159_(4), 342-345.
*   <a id="ref16"></a>Michalski, J. H., Creighton, E., & Jackson, L. (1999). The impact of hospital restructuring on social work services: a case study of a large, university-affiliated hospital in Canada. _Social work in health care, 30_(2), 1-26.
*   <a id="ref17"></a>Morrison, J. L. (1986). _Establishing an environmental scanning system to augment college and university planning._ Paper presented at the Annual Meeting of the Society for College and University Planning. San Diego, CA.
*   <a id="ref18"></a>National Library of Medicine. (2000). _National Library of Medicine - Medical Subject Headings: 2000 MeSH: MeSH Descriptor Data_.  
    [http://www.nlm.nih.gov/cgi/mesh/2K/MB_cgi?term=Age+Groups&field=entry](http://www.nlm.nih.gov/cgi/mesh/2K/MB_cgi?term=Age+Groups&field=entry) [Accessed 3 September, 2000]
*   <a id="ref19"></a>Newsome, J., & McInerney, C. (1990). Environmental scanning and the information manager. _Special Libraries, 81_(4), 285-293.
*   <a id="ref20"></a>Saunders, P., Mathers, J., Parry J., & Stevens, A. (2001). Identifying ‘non-medical’ datasets to monitor community health and well-being. _Journal of Public Health Medicine, 23_(2), 103-108.
*   <a id="ref21"></a>Sheikh, A., & Hurwitz, B. (2001). Setting up a database of medical error in general practice: conceptual and methodological considerations. _British Journal of General Practice, 51_(462), 57-60.
*   <a id="ref22"></a>Sinclair, D. G., & Nickoloff, B. J. (1998). The road to change and transition: resizing, reinvestment and restructuring in Ontario's health system. _Hospital Quarterly, 2_ (1), 61.
*   <a id="ref23"></a>Sinclair, D. G., Rochon, M., & Kilbertus, P. (1998). A status report on hospital restructuring in Ontario. _Hospital Quarterly, 2_(1), 57-60.
*   <a id="ref24"></a>Working Group on the CIHR, Gender and Women's Health Research. (2000, January). _A Women's Health Research Institute in the Canadian Institutes of Health Research_.