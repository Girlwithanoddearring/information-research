# Abstracts in Spanish/Resúmenes en Español

## Introduction

With this issue, we begin an experiment in making _Information Research_ even more international in scope. The Spanish language is one of the largest language groups in the world, and yet contributions to the journal from the Spanish-speaking countries are few. With the co-operation of colleagues at the Universidad de Murcia in Spain, we shall publish translations of the authors' abstracts in Spanish and, beginning with volume 8, we hope to produce a more extensive Spanish language edition, with original contributions in Spanish. Feedback on this idea would be welcome, either to the [Editor in Chief](mailto:t.d.wilson@shef.ac.uk), or to our [Spanish collaborators](mailto:jovi@um.es).

## Introducción

Con este número, iniciamos un experimento para hacer de _Information Research_ una revista de alcance más internacional. El español es uno de los grandes idiomas del mundo, y todavía son pocas las contribuciones a la revista de los países hispanohablantes. Con la cooperación de los colegas de la Universidad de Murcia en España,  publicaremos las traducciones en español de los resúmenes de las contribuciones de los autores en el volumen 7.  A partir del volumen 8,  esperamos producir una edición más extensa en español, con contribuciones originales en español. Cualquier sugerencia sobre esta idea sería bien recibida, tanto por el [Director](mailto:t.d.wilson@shef.ac.uk), como por los [colaboradores españoles](mailto:jovi@um.es).

### [Abstracts in Spanish/Resúmenes en Español](spanish2.html)

#### Traducciones realizadas por [José Vicente Rodríguez](mailto:jovi@um.es) y [Pedro Díaz](mailto:diazor@um.es), Universidad de Murcia, España.