#### Information Research, Vol. 7 No. 1, October 2001,

# Information as a tool for management decision making: a case study of Singapore

#### [Shrianjani Marie (Gina) de Alwis](mailto:ginaalwis@sim.edu.sg)  
Singapore Institute of Management  
[Susan Ellen Higgins](mailto:ashiggins@ntu.edu.sg)  
Division of Information Studies  
Nanyang Technological University  
Singapore

#### **Abstract**

> The main objective of this study was to develop an understanding of how Singapore's managers behave as information users and determine if their behavioural patterns are similar to their counterparts in other countries (as disclosed in the literature) or if it differs, in what ways. A total of 369 questionnaires were mailed to individual members of Singapore's Institute of Management. Only twenty members responded. The main focus of the survey was the relative uses of the different types of information sources. The survey also touched briefly on the relative importance of domains, and the correlation between hierarchical and functional levels. Results indicated that the types of information considered very important for decision making included Competitor Trends followed by Regional Economic Trends. Types of information considered important included Business news followed by Political, Social, and Supplier trends, Regulatory information, Use of Information Technology, Demographic Trends and New Management methods. Sources given a very high preference rating were Personal Contact for Competitor Trends and the use of Government Publications for obtaining regulatory information. Respondents also preferred use of Government Publications for Local Economic information and the use of Newspapers for Political Trends and Business News. Internal computer printouts were used for forecasting information and company performance. Subordinate managers were referred to for information on the use of technology, Forecasting, and Company Performance. Because the Company Library provided access to newspapers (very high usage) and business news, information about Political Trends, International and Local Economic Information and Competitor Trends were associated with it. However, the Company Library was perceived as a storage facility rather than a dynamic information resource. Local libraries were also used for Regional and International Economic information. Radio and television were used to obtain regional and Local Economic Information in Singapore, but were rated low in accessibility. Very high preference was given to personal contacts as a source of information. Managers in Singapore did not exploit all types of information sources available to them, mainly due to lack of awareness, lack of information skills and lack of accessibility to world news channels.

## Introduction

The end of the 20th century saw economic boundaries between countries crumble, as businesses became more complex, global, and knowledge driven. Managers needed to ensure that their company's continuously innovated and improved in order to achieve and maintain a sustainable competitive edge. In fact Porter ([1985](#por85)) highlights that it is this competitive ability which is considered to be at the core of the success or failure of a firm. Managers realised that if their companies are to survive in this dynamic and uncertain environment, they have to make decisions concerning new business opportunities, products, customers, suppliers, markets, and technical developments very quickly. They also have to be aware of what "cultural" factors gave other firms the competitive edge. The situation in the metropolitan city of Singapore in Southeast Asia was no different. The Singapore Business Trends Report, 1996 indicates that the country needs to move the mainstay of the economy to high value-added activities in order to capitalize on the emergence of the knowledge-based economy and create a scientific base from which to develop new industries. The report further suggests that to effectively compete in the 21st century, organisations in Singapore need to build a capacity for learning in employees.

## Objectives of the study

This study aims to provide an insight into the behavioural patterns of Singapore's managers as information users and determine if these patterns are similar to that of their counterparts in other countries, as given in the literature, or if it differs, in what ways. A knowledge of what information is needed on a regular basis for decision making purposes, how it is sought and used, and the preference for sources would provide an indication not only of the information-seeking behavioural patterns of managers but also provide an insight into why pertinent information may not be utilised. Furthermore a better understanding of the manager's information seeking behaviour, needs and perceptions would also help business information providers enhance existing products and services as well as develop new products and services. Ready accessibility to information and knowledge and strategic use of information and knowledge would allow Singapore's managers to be better decision-makers and lead their organisations to achieve that much sought-after competitive edge.

## Literature Survey

Daft and Lengel ([1984](#daf84)) note that the design of organisations, in fact the very act of organising, reflects the use of different methods of handling information and the use of teams, task forces or vertical information systems all reflect information processing needs within organisations. An organisation skilled at creating, acquiring, organising, and sharing knowledge is able to adapt its goals and behaviour to reflect new knowledge. Choo ([1996](#cho96)) refers to such an organisation as an information-savvy "intelligent learning organisation". The critical success factor for successful management is the strategic use of information and a positive correlation has been found between management success and effective information needs assessment, gathering and use ([Goodman, 1993](#goo93)). While relevant and timely information allows managers to make accurate decisions, irrelevant information makes decision making difficult, adds to confusion, and affects the performance of the company. Therefore it is crucial that managers are aware of what information they require, how to acquire it and how to maximize the use of it in order to survive and prosper in today's information-intensive environment. Managers need to use information not only for decision making and making sense of changes and developments in their external environment but also to generate new knowledge which can be applied to design new products and services, enhance existing offerings and improve organisational processes ([Choo, 1996](#cho96)). On the other hand it is also suggested that managers do not characteristically solve problems but only apply rules and copy solutions from others ([March, 1991](#mar91)) . In either case the need is access to information. Therefore information can be identified as the critical resource for decision making and management considered an information-intensive activity which requires a close relationship between decision making and information use.

## Managers, managerial activities and decision making

Cyert _et al._, ([1956](#cye56)) imply that management is a series of decision making processes and assert that decision making is at the heart of executive activity in business. But decisions need to be made fast, especially in the current context where the most precious and least manageable commodity available to managers is time . Hales ([1986](#hal86)) identifies research studies conducted over a period of thirty years which form the major source of evidence of what managers do and show that only the seminal work of Mintzberg ([1973](#mint73)) includes 'informational' as one of the managerial roles. The informational role incorporates monitoring, filtering and disseminating information as common, if not a universal part of managerial work. Mintzberg also observes that a manager's unique access to information and his special status and authority places him at the central point in the system which makes significant and strategic organisational decisions.

But Diffenbach ([1983](#dif83)) cautions that what managers want and what they need may not be the same. Drucker ([1995](#dru95)) concurs when he states that most managers still need to learn how to use data and take responsibility for information. He highlights that few managers know how to ask _'What information do I need to do my job ? When do I need it? In what form ? and from whom should I be getting it ?'_ Still fewer ask '_What new tasks should I abandon ? Which tasks should I do differently ?' Practically no one asks 'What information do I owe? To Whom? When? In What form?'_

## Characteristics of the Asian and Singaporean manager

Min ([1995](#min95)) suggests that the world's most inspiring management experiments are taking place today in the East Asian and Southeast Asian regions and identifies four major existing management systems, namely the Japanese, mainland Chinese, overseas Chinese, and Korean, all of which are considered to be heavily influenced by the Confucian tradition. As far as managerial control is concerned all four management systems are believed to have a strong vertical information-sharing and control system, but the Japanese management system is considered the only exception to relatively poor horizontal communication. In terms of leadership styles and decision-making, the degree of authoritarianism is observed to be generally high, though Japanese managers seem to be more interested in collecting information and seeking opinions from their subordinates as evidenced in the _nemawashi_ and _ringi_ processes. _Nemawashi_ is an important Japanese business concept that literally refers to dealing with the roots of trees or the preliminary and informal sounding out of employees' ideas about a proposed course of action or project. It also implies the activities that take place below ground-level and describes the nature of sounding out in which contacted persons remain anonymous and feel free to talk about their ideas. _Ringi_, as opposed to _nemawashi_, is a commonly used formal procedure of management by group consensus. Min also shows that the most predominant authoritarian style of leadership and intuitive decision making is found amongst the leaders of the Chinese family businesses (CFBs) and therefore the participation of professionals in decision making is considered the lowest in this group. Mendoza ([1991](#men91)) perceives the Asian manager as someone who has limited his areas of involvement to those that are linked to his own community and segment of society where he feels a sense of belonging and therefore needs to develop a much wider horizon in his own mind.

Ditzig and You ([1988](#dit88)) suggest that 61.3% of the Singaporean managers are Traditionalists and the Visionaries a distant second. The majority Traditionalists follow an authoritarian leadership style and exert tight control as far as possible, are logical, analytical, decisive, tough-minded, well-organised and does planning well in advance. The forte of these managers is maintenance of stability. They are skilful at setting up and holding routines, rules, schedules, regulations and hierarchy and have great respect for policies, contracts, and standard operating procedures. Some of the possible weaknesses of the Traditionalist managers are considered to be impatience with delays, hastiness in decision making in the interest of efficiency, and relative slowness and even reluctance in responding to the changing needs of the organization. The authors highlight that these characteristics tend to hinder the effective communication within the organizations and even prompted the former National Productivity Board (NPB) (now known as the Productivity and Standards Board - PSB) to host a "Lets Talk" campaign targeted at the corporations.

## How managers acquire and use information

In order to understand the information behaviours of managers, it is first necessary to have an understanding of the contexts in which managers seek and use information. Significant work done in this area include Taylor ([1986](#tay86)), Katzer and Fletcher ([1992](#kat92)), and Choo and Auster ([1993](#cho93)) who identify and analyse previous research work covering the information environment of managers and their information requirements.

Literature on the information behaviours of managers was found to be plentiful in the subject areas of Business Management, Organisational Behaviour, Psychology, and Information Technology (IT) and only very lately in the field of Information Science. In fact Auster and Choo ([1993](#aus93)) highlight the dearth of literature in the field of Information Science devoted to managers and the way they acquire and use information in their work. Literature covering information gathering activities surveyed for the study show that much is written on environmental scanning and Competitor Intelligence (CI), and also in the field of Information Technology how Management Information Systems (MIS) are being effectively used to meet these needs.

Two researchers who undertook a major study and have written prolifically on the information behaviour of managers are Auster and Choo ([1992](#aus92), [1993](#aus93), [1994a,](#aus94a) [1994b](#aus94b), and Choo ([1994](#cho94), [1995](#cho95), [1996](#cho94)). The study found a substantial correlation between the amount of scanning executives carry out and their level of perceived environmental uncertainty and suggests that the turbulence of the external environment, the strategic role of scanning and the information-use contexts of managers all combine to explain why information quality is more important than source accessibility when managers scan the environment.

Prior to the launch of the Reuters Business Information, Reuters commissioned a survey of a representative sample of business managers in order to investigate the usage, flow and politics of information in and around Britain's businesses. The survey report entitled _Information in organizations_ ([1994](#inf94)) suggests that Britains' managers need to first learn how to manage the politics of information. The report highlights that by making information a key organisational "currency", a generation of managers may have been created who value information highly but are protective of it to the point of withholding it from the others and as an organisational "currency" too valuable for many managers to give away. The report further states that the giving and withholding of information is inextricably linked with organisational politics and as a result the knowledge-based organisation where free flow of information is considered to contribute to the general good is still largely a fantasy.

_The Library 2000 Report_ ([1994](#lib94)) highlights the results of a survey carried out by the Library 2000 Subcommittee on Business Information Services and confirms that currently there are a number of specialised libraries with excellent collections on a broad spectrum of subject areas, but unfortunately except for a few government libraries such as the Trade Development Board and the Singapore Institute of Standards and Industrial Research (SISIR) libraries, access to most of these special libraries is restricted to its own staff and clientele. Therefore Singapore's business community does not have ready access to information services apart from those offered by commercial information providers and some local government agencies to assist in their decision making process. The report further highlights that this lack of comprehensive information services was also confirmed at a Regionalisation Forum organised by the Singapore Economic Development Board in 1993 and by the Committee to Promote Enterprise Overseas. The survey also confirms that while 91% of the respondents indicated that they would use a business library, they also expressed willingness to pay a subscription fee for information service, although on the lower end of the pricing range proposed in the questionnaire.

Chalmers ([1995](#cha95)) observes that lately there has been a growing recognition that the ability of businesses in New Zealand to compete in a global environment depends on the availability of comprehensive and easily accessible information resources. Reporting on the findings of a survey undertaken by the National Library of New Zealand, Chalmers concludes that business people do not always know what information they need, what is available and how to use it, but as the need for information begins to be recognised issues about its access, use and management emerge.

The literature survey provides insights into writings available todate and suggests that managers work in information intensive environments and need to acquire superior information about the environment which would enable them to develop a strategic information advantage for the company. But the literature also suggests that managers are invariably required to make decisions with or without full information about a situation. Unfortunately many of the studies also highlight the fact that managers are not aware of what their own information needs are or how these could be met and the information professionals are not sufficiently aware of managers' information needs so adequate support could be provided.

The survey shows that a majority of the research studies originate in the United States with a few originating in the United Kingdom and Canada. The number of studies traced to the Asia-Pacific region including Singapore is almost negligible.

This article, based on the findings of a survey, is an attempt to provide an insight into the role of information in Singapore's business environment and how Singapore's managers behave as information users.

## Methodology

### Survey method

The different methods of data collection such as personal interviews, telephone interviews and self-administered questionnaires were considered and personal interviews were ruled out due to manpower and cost constraints and telephone interviews due to the nature of the questionnaire and the group surveyed - managers whose busy schedules may not allow for a telephone interview lasting approximately half-hour. Therefore the survey approach using self-administered questionnaires was selected for the study. It was recognised that one drawback of this approach was that returns would not be very high and therefore the generalisation of the findings of the study may not be representative of the total population. Though it was noted that a low response rate would undermine the validity and reliability of the study it was nevertheless decided to go ahead with the use of the self-administered questionnaire due to constraints mentioned above. Furthermore due to the paucity of previous research in the Singapore context it was felt that even a small-scale survey would be better to obtain data that could be used as indicative information.

#### Sample selection

The target population was selected from the membership base of the Singapore Institute of Management (SIM) as it was considered a good sample frame for the survey. Sample selection was based on Membership type, Membership grade, Industry code and Functional code. As suggested by Patton ([1990](#pat90)) purposeful sampling was selected as it would allow for the selection of information-rich cases for study in depth. Patton identifies information-rich cases as those from which one can learn a great deal about issues of central importance to the purpose of the research.

The SIM Individual Membership base was identified for the sample frame as it offered a larger pool to select from and allowed retrieval of membership information via Industry Code and Functional code, ie., nature of work. It was decided to limit the sample to one category of SIM Members, namely the Ordinary Membership grade as they would be at least 32 years of age, possess a minimum of a Diploma qualification and have at least six to eight years experience in a senior management position, all characteristics which would ensure certain managerial and decision making experience of the sample that would enable them to relate to the questionnaire better.

The selection of Industry and Functional code was considered crucial to the screening process as it would allow for the selection of respondents opined to belong to industries or job categories that require larger amounts of quality information. Table 1 lists the groups from the listing of Industry and Functional Codes identified for the sample based on assumption that they are information-rich categories :

<table><caption>  

**Table 1: Groups selected**  
</caption>

<tbody>

<tr>

<th>Category</th>

<th>Number</th>

</tr>

<tr>

<td>Management Consultants</td>

<td>45</td>

</tr>

<tr>

<td>Research & Development</td>

<td>8</td>

</tr>

<tr>

<td>Product Design & Development</td>

<td>4</td>

</tr>

<tr>

<td>Marketing</td>

<td>131</td>

</tr>

<tr>

<td>Marketing Research</td>

<td>4</td>

</tr>

<tr>

<td>Marketing/Sales Planning/Operations</td>

<td>39</td>

</tr>

<tr>

<td>Public Relations</td>

<td>8</td>

</tr>

<tr>

<td>Corporate Planning</td>

<td>12</td>

</tr>

<tr>

<td>Human Resource Management/Development  
(Personnel/Training)</td>

<td>98</td>

</tr>

<tr>

<td>Human Resource Development (Training)</td>

<td>14</td>

</tr>

<tr>

<td>Management Development</td>

<td>6</td>

</tr>

<tr>

<td>TOTAL</td>

<td>369</td>

</tr>

</tbody>

</table>

#### Survey questionnaire design

The development of the questionnaire followed criteria given by Fowler ([1993](#fow93)) and the preliminary set of questions went through many drafts before it was put into a form for self-administration. The questionnaire was distributed to a sample of five respondents for pretesting. Based on feedback received the questionnaire was further modified. The survey instrument comprised of three parts. Part I focused on Information Resources and comprised of seven questions, and included a combination of both open and closed questions. Respondents were also required to list Services/Products according to preference in this section. Examples of titles were provided in order to ensure there was no confusion. Part II was on Use of Information and included four closed questions which required ranking. The elements listed under types and sources of information and preference for the sources of information were adapted from previous studies to suit local conditions. Examples illustrating how to complete the questions were provided for clarity. Part III of the questionnaire was for obtaining information on the respondents to capture survey demographics.

The questionnaire was accompanied by a cover letter which described the objectives of the survey, assured the respondents of confidentiality of the information provided and requested for returns to be forwarded by a deadline. A self-addressed envelope was provided for the return of completed questionnaires. Follow-up included two reminder letters to non-respondents. The survey was conducted during the period April to June 1996\.

## Findings

A total of 20 members responded to the survey, a response rate of 5.2% which was not surprising and was consistent with previous results as stated in the literature.

### Survey demographics

Eleven of the respondents belonged to the age group of 35 - 44 while 16 of the respondents were males and eight of the respondents had a minimum of a Bachelor's degree. Industries represented included six from the manufacturing sector and five Management Consultants while the job function showed five were from the marketing sector. The level of authority represented included five from Top management, seven from Senior management and four from Middle management. Only statistics reflecting the highest in each area are presented in the study. The size of the company represented by the respondents included six from companies with over 1000 employees, four from companies with 201-500 employees and three from companies with 501-1000 employees.

### Use of information resources

A total of 11 respondents affirmed that their companies maintained libraries while three indicated that their companies intended operating a library in the near future. Respondents who mentioned that their companies did not intend operating a library in the near future cited reasons such as : individual departments maintained own specialised collections, company is new and therefore unable to afford high overheads, overheads too high, need to assess value to company, and manuals prepared by parent company are sufficient.

#### Preference for local libraries

Respondents preference for local libraries is shown in Table 2\. It is seen that ten of the respondents indicated that the Singapore Institute of Management Library as the library of first choice while the national university of Singapore Library was the first preference of five of the respondents.

<table><caption>  

**Table 2: Local libraries used frequently to meet respondents' information needs (N=20)**  
</caption>

<tbody>

<tr>

<th>Name of library</th>

<th>1<sup>st</sup> preference</th>

<th>2<sup>nd</sup> preference</th>

<th>3<sup>rd</sup> preference</th>

</tr>

<tr>

<td>Singapore Institute of Management</td>

<td>10</td>

<td>3</td>

<td>2</td>

</tr>

<tr>

<td>National University of Singapore</td>

<td>5</td>

<td>4</td>

<td>3</td>

</tr>

<tr>

<td>National Productivity Board(formerly)</td>

<td>1</td>

<td>3</td>

<td>1</td>

</tr>

<tr>

<td>Nanyang Technological University</td>

<td>1</td>

<td>–</td>

<td>1</td>

</tr>

<tr>

<td>National Computer Board</td>

<td>1</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Singapore Tourism Promotion Board</td>

<td>1</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Company HQ Information Centre</td>

<td>1</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Singapore Polytechnic</td>

<td>–</td>

<td>1</td>

<td>–</td>

</tr>

<tr>

<td>National Library</td>

<td>–</td>

<td>3</td>

<td>3</td>

</tr>

<tr>

<td>SISIR</td>

<td>–</td>

<td>1</td>

<td>–</td>

</tr>

<tr>

<td>Pacific Asia Travel Association</td>

<td>–</td>

<td>1</td>

<td>–</td>

</tr>

<tr>

<td>Ministry of Trade and Industry</td>

<td>–</td>

<td>–</td>

<td>1</td>

</tr>

<tr>

<td>Institute of South East Asian Studies</td>

<td>–</td>

<td>–</td>

<td>1</td>

</tr>

<tr>

<td>Singapore Institute of Human Resource Management</td>

<td>–</td>

<td>–</td>

<td>1</td>

</tr>

<tr>

<td>Company Library</td>

<td>–</td>

<td>–</td>

<td>1</td>

</tr>

</tbody>

</table>

The question on use of foreign libraries, suppliers and services drew zero responses and no names were cited.

Two respondents affirmed that their company libraries maintained CD-ROM subscriptions while preference for CD-ROM titles to obtain information drew zero response. Four responded positively to use of CD-ROMs available in other libraries and the the titles mentioned were _ABI/Inform_ and _Datapro_. Reasons given for not using CD-ROMs ranged from high cost to PCs not having CD drives, no current need being perceived, not containing the specialised information required, parent company handles research, decision made by parent company, lack of staff and need to identify how useful it would be. If given a choice the titles that would be the preferred purchases included _Dun's Asia/Pacific Key Business Enterprise_, _Singapore Trade Connection_, _ABI/Inform_ and _Extel Financial Information Service_.

Subscriptions to on-line services was confirmed by just one respondent while a zero response was drawn to preference for on-line services to obtain information. Two respondents confirmed use of on-line services available in other libraries while 15 indicated they do not do so. Reasons cited for not using on-line services included high cost, decision made by parent company, facilities not available, need to identify how useful it would be, not familiar with the services but would like to explore, does not have the specialised information required by the parent company, not required for the nature of work carried out, no equipment, not required frequently and do not see the need for such services currently. If given a choice, the preferred on-line services were Reuter's _Business Briefing_, Singapore Press Holdings's _Newslink_, and _Singapore Network Services_.

A total of 11 responded that they had access to Internet while nine indicated in the negative. Five of the 11 indicated they had personal accounts while another five accessed via company account and one had access to both. One respondent stated that although the company had its own global internet account employees had restricted access. Usage of Internet ranged from e-mail surfing for general interest, ftp and retrieving specific information from web sites. List of favourite internet sources drew zero responses. A total of five responded that the company intended providing access to Internet and the time frame given for subscribing ranged from three months to one year. Reasons for not getting access to Internet included cost, not sure how it would help the company's South East Asian business and the need to determine needs first.

#### Use of information

<table><caption>  

**Table 3: Types of information managers used on a regular basis for decision making (N=20)**  
(Scores above 8 are in bold face type)  
</caption>

<tbody>

<tr>

<td> </td>

<th>Very Important</th>

<th>Important</th>

<th>Unimportant</th>

</tr>

<tr>

<td>Political Trends</td>

<td>6</td>

<td>

**10**</td>

<td>1</td>

</tr>

<tr>

<td>Economic Trends</td>

<td>

**10**</td>

<td>7</td>

<td>1</td>

</tr>

<tr>

<td>- local</td>

<td>

**10**</td>

<td>6</td>

<td>–</td>

</tr>

<tr>

<td>- regional</td>

<td>

**11**</td>

<td>7</td>

<td>–</td>

</tr>

<tr>

<td>- world</td>

<td>

**10**</td>

<td>7</td>

<td>1</td>

</tr>

<tr>

<td>Social Trends</td>

<td>5</td>

<td>

**10**</td>

<td>3</td>

</tr>

<tr>

<td>Consumer Trends</td>

<td>3</td>

<td>5</td>

<td>

**8**</td>

</tr>

<tr>

<td>Supplier Trends</td>

<td>4</td>

<td>

**10**</td>

<td>3</td>

</tr>

<tr>

<td>Competitor trends</td>

<td>

**12**</td>

<td>5</td>

<td>2</td>

</tr>

<tr>

<td>Regulatory Information</td>

<td>6</td>

<td>

**10**</td>

<td>1</td>

</tr>

<tr>

<td>Use of IT</td>

<td>7</td>

<td>

**10**</td>

<td>–</td>

</tr>

<tr>

<td>Forecasts</td>

<td>

**8**</td>

<td>

**8**</td>

<td>1</td>

</tr>

<tr>

<td>Industry/Market</td>

<td>

**10**</td>

<td>7</td>

<td>1</td>

</tr>

<tr>

<td>Stock Market Trends</td>

<td>2</td>

<td>6</td>

<td>

**8**</td>

</tr>

<tr>

<td>Business News</td>

<td>6</td>

<td>

**11**</td>

<td>–</td>

</tr>

<tr>

<td>Market Research Info.</td>

<td>4</td>

<td>

**8**</td>

<td>3</td>

</tr>

<tr>

<td>Company Performance</td>

<td>

**9**</td>

<td>7</td>

<td>2</td>

</tr>

<tr>

<td>Demographic Trends</td>

<td>5</td>

<td>

**10**</td>

<td>1</td>

</tr>

<tr>

<td>- local</td>

<td>3</td>

<td>

**9**</td>

<td>3</td>

</tr>

<tr>

<td>- regional</td>

<td>2</td>

<td>

**10**</td>

<td>3</td>

</tr>

<tr>

<td>New Management Methods</td>

<td>

**8**</td>

<td>

**10**</td>

<td>–</td>

</tr>

<tr>

<td>Best Practices</td>

<td>1</td>

<td>–</td>

<td>–</td>

</tr>

</tbody>

</table>

Table 3 shows that Competitor Trends topped the "very important" category of information required for decision making followed very closely by Regional Economic Trends. Local and World Economic Trends, Industry/Market Trends and Company Trends and New Management Methods followed closely behind. Of the "unimportant" categories Consumer and Stock Market Trends were the top scorers.

#### Preference for sources of information for obtaining difference types of information

Table 4 is very large and is presented as [a pop-up window](javascript:openNewWindow('p114tab4.html')). Remember to close the window. The table reveals the following:

*   Sources given a _Very High Preference Rating_ by more than five persons

*   Personal contact for information on Competitors
*   Trends and Government Publications for obtaining Regulatory information. (6x)
*   Government publications for local Economic information and Newspapers for Political trends and Business news. (5x)

*   Sources given a _High Preference_ rating by more than five persons

*   Newspapers for Regional Economic information (7x)
*   Other libraries for regional and international Economic information , Social, Supplier, Competitor and Industry/Market trends. (6x)
*   Newspapers to track Consumer trends and Business news and Internal computer printouts for Company performance. (5x)

*   Sources given a _Medium Preference_ rating by more than five persons

*   Colleagues for Use of IT and to track Supplier trends and use of Radio/TV to track Political trends.

#### Preference for sources by types of information

<table><caption>  

**Table 5: A summary of preference for sources by types of information**  
(Only sources with a rating of 5(x) and above are reflected (N=20))  
</caption>

<tbody>

<tr>

<th>Type of information</th>

<th>Sources which received _Very High Preference_ rating</th>

<th>Sources which received _High Preference_ rating</th>

</tr>

<tr>

<td>Political Trends</td>

<td>Newspapers (5x)</td>

<td>–</td>

</tr>

<tr>

<td>Economic Trends</td>

<td>Government publications (5X)</td>

<td>Newspapers (7x)</td>

</tr>

<tr>

<td>Social Trends</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Consumer Trends</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Supplier Trends</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Competitor Trends</td>

<td>Personal contact (6x)  
Subordinate managers (5x)</td>

<td>–</td>

</tr>

<tr>

<td>Regulatory Information</td>

<td>Government publications (6x)</td>

<td>–</td>

</tr>

<tr>

<td>Use of IT</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Forecasts</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Industry/Market</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Stock Market</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Business News</td>

<td>– Newspapers (5x)</td>

<td>–</td>

</tr>

<tr>

<td>Company Performance</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>Demographic</td>

<td>–</td>

<td>–</td>

</tr>

<tr>

<td>New Management Methods</td>

<td>–</td>

<td>–</td>

</tr>

</tbody>

</table>

Table 5 reveals the following:

*   Sources given a "_Very High Preference_" rating by more than five persons (5x) (N=20)

*   Personal contact for Competitor Trends and Government publications for Regulatory Information (6x).
*   Newspapers for Political Trends and Business News, Government Publications for Economic Trends, and Subordinate Managers for Competitor Trends.

*   Sources given a _"High Preference"_ rating by more than five persons (5x) (N=20)

*   Newspapers for Economic Trends (7x)

#### Preference for sources of information and reasons for preference

<table><caption>  

**Table 6: Preference for sources or information and reasons for preferences**  
(N=20)  (Sources with a rating of 5 (x) and above are highlighted)  
</caption>

<tbody>

<tr>

<th>Sources</th>

<th colspan="5">Ratings</th>

<th>Prior  
experience</th>

<th>Easy  
access</th>

<th>Recommended  
/referred</th>

<th>Unfamiliar</th>

<th>Not easily  
accessible</th>

<th>Expensive</th>

<th>Time  
consuming</th>

<th>Not  
available</th>

<th>Not up-to-date</th>

</tr>

<tr>

<td> </td>

<td>5</td>

<td>4</td>

<td>3</td>

<td>2</td>

<td>1</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**Personal contacts**</td>

<td>

**7**</td>

<td>4</td>

<td>2</td>

<td>-</td>

<td>2</td>

<td>

**10**</td>

<td>

**8**</td>

<td>3</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>

**Subordinate managers**</td>

<td>-</td>

<td>

**7**</td>

<td>

**6**</td>

<td>-</td>

<td>1</td>

<td>

**7**</td>

<td>

**9**</td>

<td>2</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>1</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>

**Colleagues**</td>

<td>1</td>

<td>

**7**</td>

<td>4</td>

<td>2</td>

<td>-</td>

<td>

**8**</td>

<td>

**7**</td>

<td>3</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>

**Company library**</td>

<td>4</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>1</td>

<td>1</td>

<td>

**7**</td>

<td>3</td>

<td>-</td>

<td>1</td>

<td>-</td>

<td>2</td>

<td>2</td>

<td>3</td>

</tr>

<tr>

<td>

**Other libraries**</td>

<td>1</td>

<td>

**5**</td>

<td>

**5**</td>

<td>2</td>

<td>1</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>-</td>

<td>4</td>

<td>2</td>

<td>3</td>

</tr>

<tr>

<td>

**Govt. publications**</td>

<td>2</td>

<td>2</td>

<td>

**5**</td>

<td>

**5**</td>

<td>1</td>

<td>3</td>

<td>4</td>

<td>3</td>

<td>1</td>

<td>2</td>

<td>-</td>

<td>2</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>

**Radio and TV**</td>

<td>1</td>

<td>3</td>

<td>4</td>

<td>3</td>

<td>2</td>

<td>2</td>

<td>

**7**</td>

<td>4</td>

<td>-</td>

<td></td>

<td>-</td>

<td>1</td>

<td>1</td>

<td>-</td>

</tr>

<tr>

<td>

**Newspapers**</td>

<td>4</td>

<td>2</td>

<td>

**5**</td>

<td>3</td>

<td>2</td>

<td>4</td>

<td>

**9**</td>

<td>5</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>2</td>

<td>1</td>

<td>-</td>

</tr>

<tr>

<td>

**CD-ROM services**</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>3</td>

<td>1</td>

<td>1</td>

<td>-</td>

<td>

**6**</td>

<td>4</td>

<td>3</td>

<td>-</td>

<td>4</td>

<td>-</td>

</tr>

<tr>

<td>

**On-line bibliographic databases**</td>

<td>-</td>

<td>-</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>-</td>

<td>2</td>

<td>-</td>

<td>

**6**</td>

<td>2</td>

<td>2</td>

<td>1</td>

<td>4</td>

<td>-</td>

</tr>

<tr>

<td>Other on-line databases</td>

<td>1</td>

<td>-</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>-</td>

<td>-</td>

<td>1</td>

<td>

**6**</td>

<td>2</td>

<td>2</td>

<td>1</td>

<td>

**5**</td>

<td>-</td>

</tr>

<tr>

<td>

**Internet**</td>

<td>1</td>

<td>3</td>

<td>2</td>

<td>

**5**</td>

<td>3</td>

<td>2</td>

<td>2</td>

<td>-</td>

<td>4</td>

<td>1</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>-</td>

</tr>

<tr>

<td>

**Internal computer printouts (Sales/ Financial/Management reports)**</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>1</td>

<td>1</td>

<td>4</td>

<td>

**5**</td>

<td>2</td>

<td>-</td>

<td>-</td>

<td>1</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td colspan="15">Legend: 5-Very high preference; 4-High preference; 3-Moderate preference; 2-Low preference; 1-No preference</td>

</tr>

</tbody>

</table>

Table 6 shows following results for sources rated by 5 or more respondents and with a preference of 5 and above:

*   Personal contacts was given "_Very High Preference_" (7x) and the reasons for the preference was Prior Experience (10x) and Easy Access (8x).
*   Subordinate Managers were rated _"High Preference"_ (7x) and _"Moderate Preference"_ (6x).
*   Reasons for preference included Prior Experience (7x) and Easy Access (9x).
*   Colleagues were given _"High Preference"_ (7x) and reasons for preference were Prior Experience (8x) and Easy Access (7x).
*   Other Libraries received a _"Moderate preference"_ (5x) with no common preference for rating given.
*   Government Publications received a "Moderate" or "Low" rating each (5x) with no common rating for the non-preference.
*   Newspapers received "Moderate Preference" (5x).

<table><caption>  

**Table 7: Use of information**  
(Only those with ratings of 5 and above are noted (N=20))  
</caption>

<tbody>

<tr>

<td> </td>

<th colspan="10">Most important -------> Not important</th>

</tr>

<tr>

<th>USE</th>

<th>10</th>

<th>9</th>

<th>8</th>

<th>7</th>

<th>6</th>

<th>5</th>

<th>4</th>

<th>3</th>

<th>2</th>

<th>1</th>

</tr>

<tr>

<th>Prepare Presentation</th>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<th>Prepare Report</th>

<td>-</td>

<td>-</td>

<td>8</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<th>Prepare Strategic Plan</th>

<td>6</td>

<td>6</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<th>Prepare Executive Summary</th>

<td>-</td>

<td>5</td>

<td>6</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<th>Pass on to a Colleague</th>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>5</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<th>Pass on to a Superior</th>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>7</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<th>Pass on to a Client</th>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>6</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<th>Personal Use</th>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

</tbody>

</table>

Table 7 shows that the most important task for which information was used was for the preparation of the Company's Strategtic Plans. While six respondents rated the importance as 10/10, six respondents rated its importance a 9/10\.

Preparation of Executive Summaries is rated the next most important task for which information is used with five rating the importance as 9/10 and six rating the importance as 8/10\.

Eight respondents, the highest, indicated that information was used for Preparation of Reports and rated the importance of the task a 8/10\.

Use of information to Pass on to a Superior was given a rating of 6/10 by seven respondents, _Pass on to a Client 5/10 by six respondents and Pass on to a Colleague a 3/10 by five respondents.

Use of information for Personal Purposes id not receive a rating above five.

## Discussion of survey results and limitations of study

The low response rate of 20 was not surprising as shown in the literature. In fact [Chalmers](#cha95) (1995) observes that a number of similar studies conducted previously had achieved low or very low response rates. [White](#whi86) (1986) in discussing the difficulties encountered in her own study suggests that based on reactions and response to the postal questionnaire, it must be questioned whether this type of information use survey has not outlived its usefulness and response rates will always be low and completely unrepresentative. In considering the best approach to data collection for their study, [Roberts and Clifford](#rob86) (1986) suggest that they accepted the fact that the business world was too diffuse and uncertain a concept to be handled satisfactorily by mailed questionnaires.

As the response rate was low and it was hard to judge whether the results were representative of the group surveyed it was decided that a more careful reliability check needed to be carried out and therefore the findings were compared and contrasted with previous findings of a similar nature very meticulously as suggested in _[Qualitative research and information management](#gla92)_ (1992). The results of the survey did not show any major surprises and were more or less consistent with results of similar studies done previously in Canada, USA, and New Zealand. It was postulated that significant differences may have been evident with a qualitative survey. However the depth of the information retrieved was considered arguably indicative of the wider information needs and information seeking behaviour of the Singaporean managers.

### Availability of company library or information centre

As seen in Table 2, although more than half the respondents indicated that their companies maintained libraries, many of these were confined to small collections of documents housed in small rooms with no professional supervision. The reasons given for not setting up own library such as high overheads and the need to assess value to company. [Keegan's](#kee74) (1974) observation that even an extensive library forms only a small part of a company's information system could be seen as a reason why companies do not give high priority to setting up their own libraries. [Chalmers](#cha95) (1995) observes that "Except for the largest company, the companies in the study did not have effective in-house systems for the management of published information." Furthermore [Lester and Waters's](#les89) (1989) observation that "planners often find traditional sources, such as libraries, tedious, time-consuming and frustrating to use [and] they find it difficult to establish what is available, where…". [Choo's](#cho94) (1994) suggestion that "the library is perceived as a place where publications like trade journals… are simply stored after the staff have finished reading them… the final resting place for used publications" could be indicative of the perceptions that have coloured policy-maker' decisions against allowing the establishment and expansion of libraries.

In the local context, the _[Library 2000 Report](#lib94)_ (1994) highlights the results of a survey carried out by the Library 2000 SubCommittee on Business Information Services which confirms "the need for more comprehensive information services to satisfy the information needs of businesses in their decision making process."

### Libraries and Information Suppliers both foreign/local frequently used by the respondents

#### Local libraries used frequently by the respondents

As shown in Table 2 the preferred Libraries mentioned by the respondents cover more or less the majority of the key business and management libraries located in Singapore. Therefore, it can be concluded that users who require information are fully aware of where the relevant resources could be found. This is consistent with Chalmers (1995) who observes that some respondents made use of libraries outside their company either on a regular basis or as a source of information to help them deal with a particular situation. It must be highlighted that although half of the respondents named the Singapore Institute of Management library as the library of first choice, this result may have been due to the fact that the survey was conducted by the SIM library.

#### Foreign libraries and information suppliers used frequently by respondents

The very low to zero responses to this question did not come as a surprise considering the high costs involved.

#### Use of CD-ROM and on-line services

The low response to these services did not come as any surprise either, considering the findings of similar studies conducted previously. The main reason for the low usage in Singapore and the region could be attributed to high costs. Results definitely showed that respondents were not only aware of the usefulness of CD products and on-line Services as sources of information but were also aware of the diversity of titles available for retrieval of different types of information.

### Use of Internet

Singapore's attempts to ride the information superhighway and the intense competition among the internet providers to grab a slice of the local market were reflected in the survey results as it showed that over half the respondents accessed the internet through both personal and corporate accounts. But it must be noted that the results reflected the use of Internet basically for e-mail purposes, while surfing for general and specific usage was low.

### Types of information used regularly for decision making

As shown in Table 3 types of information considered very important for decision making purposes by ten or more respondents included Competitor Trends followed by Economic and Industry/Market information. Types of information considered important by ten or more respondents included Business News followed by Political, Social, and Supplier Trends, Regulatory Information, Use of Information Technology, Demographic Trends and New Management Methods.

One result of the local survey which was not generally consistent with findings in the literature was the importance of information about Suppliers. [Prescott and Bhardwaj](#pre95) (1995) highlight that considering the focus many companies place on quality it was surprising that this category had been given so little emphasis. As ISO 9000 and quality are high on the agenda of Singapore companies it was not surprising that Singapore's managers considered Supplier Trends important. The importance of Suppliers as a source of information was corroborated by [Auster and Choo](#aus93) (1993).

While the high use of Regulatory Information was consistent with the reviewed literature one finding not consistent was the importance given to Economic, Political and Social Trends.

Due to the entrepreneurial skills inherent in the Chinese culture and the spill over effect from Singapore Government's _[Strategic Economic Plan](#sin91)_ (1991) which envisages for Singapore a "Vision to become a first league developed country" it is perhaps not surprising that Economic, Political and Social Trends are given high importance. It can be concluded that Singapore's managers gave top priority to information on Competitor Trends because Singapore's business tends not to be restricted within the country but driven regionally to take advantage of a fast developing region as well as at a global level.

### Preference for sources of information for the different types of information

As seen in Table 4 Personal sources such as Personal Contacts, Subordinate Managers and Colleagues were given a _"Very High Preference"_ to obtain information on Political Trends, Competitor Trends, Use of IT, Forecasts and Company Performance. A _"High Preference"_ rating was given to personal sources to obtain information on Industry/Market, Supplier and Competitor Trends and Company Performance.

Printed sources such as Government Publications, Newspapers, Internal Reports and materials available in the Company Library as well as Other Libraries were given _"Very High Preference"_ to obtain information on Political, Economic, Social and Competitor Trends, Regulatory Information, Business News and Forecasts. A _"High Preference"_ was indicated for printed sources such as Newspapers, Government Publications and Internal Computer Printouts to obtain information on Political, Economic, Social, Consumer, Suppliers, Demographic and Competitor Trends, Industry/Market Trends, Regulatory Information, Forecasts, Business News and Company Performance.

In the final analysis it was seen that managers used different sources of information to fulfil their different information needs.

### Preference for sources of information and reasons for preference

A summary of the results based on information in Table 6 is given below and includes only sources indicated as _"Very High Preference"_ and _"High Preference"_ by five or more respondents.

<table><caption>

**Table 8: Preference for sources of information (N = 20)**</caption>

<tbody>

<tr>

<th>Source</th>

<th>Very High</th>

<th>High</th>

<th>Prior Experience</th>

<th>Easy Access</th>

<th>Recommended/  
Referred</th>

</tr>

<tr>

<td>Personal Contacts</td>

<td>7</td>

<td>-</td>

<td>10</td>

<td>8</td>

<td>-</td>

</tr>

<tr>

<td>Subordinate Managers</td>

<td>-</td>

<td>7</td>

<td>7</td>

<td>9</td>

<td>-</td>

</tr>

<tr>

<td>Colleagues</td>

<td>-</td>

<td>7</td>

<td>8</td>

<td>7</td>

<td>-</td>

</tr>

<tr>

<td>Company Library</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>7</td>

<td>-</td>

</tr>

<tr>

<td>Radio/TV</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>7</td>

<td>-</td>

</tr>

<tr>

<td>Newspapers</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>9</td>

<td>5</td>

</tr>

<tr>

<td>Internal Computer Printouts</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>5</td>

<td>-</td>

</tr>

</tbody>

</table>

The survey results revealed that _"Very High"_ preference was given to personal Contacts as a source of information and _"High"_ preference was given to use of Subordinate Managers and Colleagues within the organisation. The literature suggests that the important factor is knowing whom to consult rather then being loaded with lots of information. It is interesting to note that the [Reuters](#reu94) (1994) study reveals a contradictory finding that although word of mouth is an important source the word of mouth/meetings/personal contact are considered the least accurate or reliable source of information.

The survey conducted locally also revealed that the "_High"_ use of Personal sources by Singaporean managers is attributed mainly to Prior Experience and Easy Access and because it was Recommended or Referred to.

The survey results also showed that Newspapers were placed second as a source of _"Very High"_ preference. The survey also suggested that Newspapers were voted as a popular source due to its Easy Access and because it had been Recommended or Referred to. A range of newspapers, both local and foreign, reasonably priced and easily accessible in the local market and the popularity of newspapers amongst Singaporean managers as a source of information could be identified as reasons for its popularity.

While the usage of the Company Library received a middle ranking under "Very High" usage in the survey the usage of the broadcast media such as Radio/TV received a high rating which could be attributed to the fact that Singaporeans have a wide range of choices to select from the broadcast media.

Internal Computer Printouts received a middle ranking under both "Very High" preference and "High" preference mainly due to its ease of access.

Observations worth noting here include one made by [Ghoshal and Kim](#gho86) (1986) who suggest that there is a complex set of interactions between information and its source that influences the way information is perceived and acted upon by managers. The same piece of information is seen differently when received from a favourite and trusted subordinate than when it is received from the manager of the Intelligence Section. According to [Miller](#mil94) (1994) users base their selection of information on the basis of the effort required to gain access to the source. Accessibility not only determined the overall frequency of the use of the source, but also the choice of being selected as the first source.

### Preference for using the information

As seen in Table 7, the three most important types of usage fell under the umbrella of "use of information for personal purposes" and collection of information to "pass on to other persons" was not considered as important.  Further research need to be carried out in this area to determine if Singapore's managers are protective of information to the point of withholding it from others and are similar in behaviour to Britains' managers as reported in _[Information in organisations](#inf94)_ (1994). These findings would also be a reflection of the extent to which Singapore's companies could be considered knowledge-based where free-flow of information exists.

## Conclusion

Although the response rate to the survey was low, the results of the survey did provide information which could be considered representative and was more or less consistent with previous findings.  It can be concluded that Singapore's managers use a range of sources, print and personal as well as sources which are internal and external to the organisation to obtain information. The preference for sources or source selection and use are influenced by the user's perception or attitudes to the accessibility of sources.  But it is doubtful if the combination of sources used by Singapore's managers provide a balanced input of the required information with appropriate "richness". Managers did not exploit all types of information sources available to them locally, mainly due to a lack of awareness of their availability and usefulness and also due to a lack of skills in the use of such sources. [Drucker's](#dru95) (1995)  claim that "few executives yet know how to ask 'What information do I need to do my job?  When do I need it? In what form? And from whom should I be getting it?'" could also be applied to the Singapore context.

### The future

The 21st century has been hailed as the "Asia-Pacific century" by many economists and futurists. Therefore, managing businesses in the 21st century in the Asia-Pacific region will undoubtedly be an enormous challenge to the Asian managers. In order to maintain a competitive edge these managers would need to be visionaries who are innovative and creative. Companies therefore need to be up to date with information on market trends, benchmark data, and productivity indicators to enable them assess their business performance and stay competitive. Therefore Singapore's information professionals have a role to play – a more proactive role using innovative approaches to support managers with relevant, timely and value-added information. The National Library Board of Singapore and the Singapore Institute of Management Library as the main players in the market will need to take an aggressive stance to meet these needs.

## References

*   <a id="aus92"></a>Auster, Ethel and Choo, Chun Wei (1992) "Environmental scanning: preliminary findings of a survey of CEO information seeking behaviour in two Canadian industries", in _Proceedings of the 55th Annual Meeting of the American Society for Information Science_ edited by D. Shaw. (pp. 48-54) Medford, NJ: Learned Information.  

*   <a id="aus93"></a>Auster, Ethel and Choo, Chun Wei (1993) "Environmental scanning by CEOs in two Canadian industries." _Journal of the American Society of Information Science_, **44**(4), 194-203.  

*   <a id="aus94a"></a>Auster, Ethel and Choo, Chun Wei (1994a) "CEOs, information and decision making: scanning the environment for strategic advantage." _Library Trends_, **43**(2), 206-225\.  

*   <a id="aus94b"></a>Auster, Ethel and Choo, Chun Wei (1994b) "How senior managers acquire and use information in environmental scanning." _Information Processing & Management_, **30**(5), 607-618.  

*   <a id="cha95"></a>Chalmers, Anna (1995) "Finding out: the use of business information by managers in New Zealand" _Business Information Review_, **12**(1), 43-56.  

*   <a id="cho94"></a>Choo, Chun Wei (1994) "Perception and use of information sources by chief executives in environmental scanning" _Library & Information Science Research_, **16**(1), 23-40.  

*   <a id="cho95"></a>Choo, Chun Wei (1995) _Information management for the intelligent organization: the art of scanning the environment._ New Jersey: Information Today.  

*   <a id="cho96"></a>Choo, Chun Wei (1996) "The knowing organization: how organzations use information to construct meaning, create knowledge and make decision"  _International Journal of Information Management_, **16**(5), 23-40\.  

*   <a id="cho93"></a>Choo, Chun Wei and Auster, Ethel (1993) "Environmental scanning: acquisition and use of information by managers". Annual Review of Information Science and Technology (ARIST) **28**, 279-314\.  

*   <a id="cye56"></a>Cyert, R.M., Simon, H.A. and Trow, D.B. (1956) "Observations of a business decision." _The Journal of Business_, **29**(4), 237-248\.  

*   <a id="daf84"></a>Daft, Richard L. and Lengel, Robert H.. (1984) "Information richness: a new approach to managerial behavior and organization design" _Research in Organzational Behavior_, **6**, 191-233.  

*   <a id="dif83"></a>Diffenbach, John (1983) "Corporate environmental analysis in large U.S. corporations." _Long Range Planning_, **16**(3), 107-116.  

*   <a id="dit88"></a>Ditzig, Hermann and You Poh Seng (1988) "In search of the Singapore managerial style." _Singapore Management Review_ **10**(2), 35-51.  

*   <a id="dru95"></a>Drucker, Peter (1995) _Managing in a time of great change_. Oxford: Butterworth Heinemann.  

*   <a id="fow93"></a>Fowler, Floyd J. (1993) _Survey research methods_. 2nd ed. Newbury Park: Sage Publications.  

*   <a id="goo93"></a>Goodman, Susan K. (1993) "Information needs for management decision making." _Records management Quarterly_ October, 21-22\.  

*   <a id="gho86"></a>Ghoshal, Sumantra and Kim, Seok Ki (1986) "Building effective intelligence systems for competitive advantage" _Sloan Management Review_ Fall, 49-58.  

*   <a id="gla92"></a>Glazier, Jack D. & Powell, Ronald R., eds. (1992) _Qualitative research and information management_. Colorado: Libraries Unlimited. 238p.  

*   <a id="hal86"></a>Hales, Colin P. (1986) "What do managers do? A critical review of the evidence." _Journal of Management Studies_. **23**(1), 88-115\.  

*   <a id="inf94"></a>"Information in organisations: new research from Reuters Business Information" (1994) _Business Information Review_, **11**(2), 48-52.  

*   <a id="kat92"></a>Katzer, Jeffrey and Fletcher, Patricia T. (1992) "The information environment of managers", Annual Review of Information Science and Technology (ARIST) **27**, 227-263\.  

*   <a id="kee74"></a>Keegan, Warren J.(1974) "Multinational scanning: a study of the information sources utilized by headquarters executives in multinational companies." _Administrative Science Quarterly_, **19**(3), 411-421\.  

*   <a id="les89"></a>Lester, Ray and Waters, Judith (1989) _Environmental scanning and business strategy_. London: British Library Board. (Library and Information Research Report 75).  

*   <a id="lib94"></a>Library 2000 Review Committee (1994) Library 2000 - investing in a learning nation: report of the... Committee. Singapore: SNP Publishers.  

*   <a id="mar91"></a>March, James G. (1991) "How decisions happen in organization." _Human-Computer Interaction_ **6**(2), 95-117.  

*   <a id="men91"></a>Mendoza, Gaby (1991) _Management: the Asian way_. Manila: Asian Institute of Management.  

*   <a id="mil94"></a>Miller, Jarry P. (1994) "The relationship between organizational culture and environmental scanning: a case study" _Library Trends_ **43**(2), 170-205.  

*   <a id="min95"></a>Min Chen (1995) _Asian management systems: Chinese, Japanese and Korean styles of business_. London: Routledge.  

*   <a id="mint73"></a>Mintzberg, Henry (1973) _The nature of managerial work_. New York: HarperCollins.  

*   <a id="pat90"></a>Patton, Michael Quinn (1990) _Qualitative evaluation and research methods_. 2nd ed. Newbury Park, Calif.: Sage.  

*   <a id="por85"></a>Porter, Michael E. (1985) _Competitive advantage_. New York: Free Press.  

*   <a id="pre95"></a>Prescott, John E. and Bhardwaj, Gaurab (1995) "Competitive intelligence practices: a survey" _Competitive Intelligence Review_, **6**(2), 4-14.  

*   <a id="rob86"></a>Roberts,  N. and Clifford, B. (1986) "Regional variations in the demand and supply of business information: a study of manufacturing firms" _International Journal of Information Management_, **6**, 171-183.  

*   <a id="sin96"></a>Singapore Business Trends Survey, 1996 (1996) Singapore: Singapore Institute of Management and Brunel: Henley Management College.  

*   <a id="sin91"></a>Singapore. _Ministry of Trade and Industry_. _The Economic Planning Committee_ (1991) _The Strategic Economic Plan: toward a developed nation_. Singapore: Singapore National Printers.  

*   <a id="tay86"></a>Taylor, Robert S. (1986) _Value-added processes in information systems_. Norwood, NJ: Ablex Publishing.  

*   <a id="whi86"></a>White, Brenda (1986) _Representative organisations as sources of information: use by firms of trade associations, chambers of commerce and the Confederation of British Industry: final report for the period July 1985 to February 1986._ Edinburgh: Brenda White Associates. (British Library R & D Reports, No.5903).