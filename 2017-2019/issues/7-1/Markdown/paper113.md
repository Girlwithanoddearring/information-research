#### Information Research, Vol. 7 No. 1, October 2001,

# Scanning and vicarious learning from adverse events in health care

#### [Anu MacIntosh-Murray](email:anu.macintosh@utoronto.ca)  
Faculty of Information Studies  
University of Toronto  
Toronto, Ontario  
Canada

#### **Abstract**

> Studies have shown that serious adverse clinical events occur in approximately 3%-10% of acute care hospital admissions, and one third of these adverse events result in permanent disability or death. These findings have led to calls for national medical error reporting systems and for greater organizational learning by hospitals. But do hospitals and hospital personnel pay enough attention to such risk information that they might learn from each other's failures or adverse events? This paper gives an overview of the importance of scanning and vicarious learning from adverse events. In it I propose that health care organizations' attention and information focus, organizational affinity, and absorptive capacity may each influence scanning and vicarious learning outcomes. Implications for future research are discussed.

## Introduction

> _"Given the increasing pace of accumulation of medical knowledge, the only surety is that today's knowledge is obsolete tomorrow. Instead of being masters only of memorization, physicians must become masters of acquiring necessary information from many sources in a timely enough fashion to make correct clinical decisions. The amount of knowledge necessary to practice high-quality health care is just too large and changing too rapidly to be carried around in any person's brain."_  
> ([Becher & Chassin](#becher), 2001, p. 74)

Although [Becher and Chassin](#becher) (2001) refer specifically to physicians in the above passage, their blunt, but astute, assessment applies to all those who are responsible for the provision and management of health care. In order to improve quality and minimise error, health care organizations must become proficient in acquiring necessary information from many sources in a timely fashion in a rapidly changing environment. Effective scanning may be one way to accomplish this.

What is meant by "scanning" can vary depending on the type of information, the reasons for which it is sought, and the frequency and scope of the information gathering (see [Choo](#choo), 1998: 74-75). For example, Choo describes environmental scanning as the "acquisition and use of information about events, trends, and relationships in an organization's external environment, the knowledge of which would assist management in planning the organization's future course of action" (1998: 72). This includes information activities related to identifying opportunities and threats in a for-profit business environment. Active environmental scanning is associated with performance differences in business organizations ([Aguilar](#aguilar), 1967; [Choo](#choo), 1998; [Yasai-Ardekani](#yasai-ardekani), 1996) and hospitals ([Subramanian](#subramanian), 1994; [Thomas _et al._,](#thomas) , 1993).

A somewhat more focused form of scanning may be usefully extended to health care organizations and their need to anticipate potential patient safety issues. Health care risk management is intended to operate in a pre-emptive mode, identifying risks to safety and hazards and implementing preventive action before injuries are incurred ([Harpster & Veatch](#harpster), 1990). One goal is to avoid adverse events, defined as unintended injuries caused by medical management rather than the underlying condition of the patient. Examples include hospital-acquired infections, medication errors, and surgery on the wrong side or limb. An adverse event attributable to error is a "preventable adverse event" ([Kohn _et al._, 1999: 24](#kohn)). Scanning could play an important role in the process of learning from adverse events and risk management by sensing and identifying adverse event information as part of an organizational "early-warning system". Scanning in this context could be defined as the acquisition and use of information about adverse events, patient safety risks, hazards, and trends from an organization's external environment, the knowledge of which would assist health care providers and managers in taking preventive action where necessary. For this to be successful, however, we need to understand what facilitates or impedes the recognition, evaluation, and transfer of such information and knowledge. The aim of this paper is to begin to lay the theoretical ground work for questions and tentative hypotheses which may guide future research. First I will set the context and discuss the theoretical and practical importance of the issue. This is followed by a review of relevant research, which leads to a framework for consideration of scanning for and learning from adverse event information. I will then continue by describing three constructs based on organizational characteristics and practices which I suggest may influence scanning and vicarious learning from adverse events. These constructs form the basis of three tentative hypotheses.

## The context of the issue

In several large-scale studies researchers have estimated that approximately 3% to 10% of inpatient admissions result in some form of medically-related injury, half of which were preventable ([Brennan](#brennan) _et al._, 1991; [Leape _et al._, 1991](#leape); [Thomas _et al._, 2000](#thomasej); [Vincent _et al._, 2001](#vincent)). Recent high-profile publications have decried this situation as unacceptable and have prompted understandably urgent calls for immediate solutions ([Cook _et al._, 1998](#cook); [National Patient Safety Foundation, 1998](#national); [Leape & Berwick, 2000](#leape-berwick)). One suggested intervention is the development of adverse event reporting systems. These would allow open sharing of the incidence and causal factors to promote inter-organizational learning and prevention ([Kohn, Corrigan, & Donaldson](#kohn), 1999). In the United States, the Agency for Healthcare Research and Quality (AHRQ) recently issued a research agenda for medical errors and patient safety. It includes two very pertinent questions: "How can useful information be provided effectively to those who can act (e.g., consumers, providers and provider organizations, purchasers, states, and oversight organizations)?" and "How can we encourage the adoption and use of safety information?" ([Meyer _et al._, 2001: xvi](#meyer)).

From a pragmatic vantage point, there are major incentives to avoid severe clinical adverse events: multiple and potentially very high costs, including death or disability for the patient, emotional and financial distress for the family, consequences for the caregivers involved (regulatory or disciplinary; reputational; stress; possible job loss), damage to staff morale, litigation costs for the hospital, damage to organizational reputation, and erosion of institutional legitimacy. Given these potentially high costs, one could expect to find evidence that hospitals optimize opportunities to learn how to avoid such occurrences. This would include thorough investigation of and learning from internal mishaps, as well as scanning the environment for possible information about risks and events experienced elsewhere. Scanning and vicarious learning could be much more efficient and intuitively appealing approaches as the hospital may be able to derive the lessons learned yet avoid the adverse event costs incurred by the other hospitals.

Although we do not yet have national error reporting systems in Canadian health care [(1)](#1) such as the ones supported by the aviation industry, there are other potential sources of information in the health care environment. The sources naturally vary in formality, detail, and accuracy. At one end of the spectrum are inquest reports and recommendations, malpractice litigation findings, and medical equipment hazard alerts which are produced by judicial bodies and monitoring agencies. Clinical and professional journals provide another source. The other end is quite informal and includes incidents described in stories spread by word of mouth between health care providers. Media reports, including newspaper, radio and television accounts, may be perceived as falling somewhere in between. Understanding how hospitals currently use various sources to learn from others' experiences with adverse clinical events and exploring what organizational characteristics facilitate efficient and effective transfer of learning is of practical relevance to health care managers in the face of increasing demands for improvement and accountability.

The topic of vicarious learning by hospitals from the mishaps of others is of theoretical interest as well. [Sitkin](#sitkin) (1992) proposes that an organization's own failures provide important lessons and experience necessary for adaptation. Although he doesn't elaborate further, Sitkin also suggests that failure may be helpful to other organizations vicariously, that is, they may benefit from observing the outcomes of others' failures. [Huber](#huber) (1991) describes the processes through which organizations acquire information or knowledge, including searching, scanning, and vicarious learning, "about which relatively little has been learned beyond the fact that [it] occurs" (p. 107). There is little theory and empirical research directly on this topic in the health services research literature, so it is not clear whether hospitals systematically scan for adverse event information and knowledge, what factors may impede or facilitate transfer, or whether in fact hospitals do learn from others' failures. As there is little theory or empirical studies that are directly on the topic of hospitals' vicarious learning from adverse clinical events, I have used organizational and population-level learning theory ([Argote, 1999](#argote); [Hedberg, 1981](#hedberg); [Miner & Haunschild, 1995](#miner); [Miner _et al._, 1999](#minerkim); [Sitkin, 1992](#sitkin)) as the theoretical foundation. Concepts from the literature on organizational attention and information orientation ([Choo, 1998](#choo); [Ocasio, 1997](#ocasio); [Subramanian _et al._, 1994](#subramanian); [Thomas _et al._, 1993](#thomas)), as well as inter-organizational knowledge transfer and innovation ([Argote & Ingram, 2000](#argote-ingram); [Argote _et al._, 2000](#argote-ingram-levine); [Darr & Kurtzberg, 2000](#darr); [Greve & Taylor, 2000](#greve)), are also incorporated into the theory base of this paper.

## The effect of organizational experiences on others

Although there is a substantial literature on organizational learning, surprisingly little of this has been about learning from others' failures. [Miner _et al._](#miner-kim) (1999) point out that even fewer studies have looked at organizational failure as an independent variable. They proceeded to study the effects of failure events (organizational deaths, very poor financial performance, or acquisition by another firm) on the types and mix of routines in populations of firms. They describe four general categories of organization-level reaction: imitating routines of more successful organizations; avoiding the failed routine or organization; developing a new routine; and reinforcing a failing routine. They also identified possible population-level responses such as creation of a new industry entity and routine, or industry-level co-operation or regulation. Miner _et al._'s dependent variable is "change in routines," not the performance outcomes for the organizations. Their model does not predict whether the changes will result in positive or negative effects, although they state, "at its broadest level, our work argues that patterns of prior organizational failures will substantially influence the survival and prosperity of remaining organizations" (p. 212). As another example, [Ingram and Baum](#ingram) (1997) found that cumulative industry operating experience and competitive experience (failures) affect the failure rates of US hotel chains.

The observations from [Miner _et al._,](#miner-kim) (1999) and [Ingram and Baum](#ingram) (1997) suggest that failures, which perhaps could include significant adverse events involving a patient deaths and inquests, may have an effect on other hospitals by enabling the transfer of some form of knowledge. Although Miner _et al._, (1999) and Ingram and Baum (1997) studied aspects of learning from others, they did not focus on how organizations pay attention to the experience of others in the first place. Paying attention to cues from the environment is a precursor to experiencing effects from others and an information-based approach to the process could provide some guidance.

## A framework for considering scanning for adverse event information

Huber provides an information-based view of organizational learning and identifies four component processes: knowledge acquisition, information distribution, information interpretation, and organizational memory ([Huber, 1991](#huber)). He describes the sub-processes through which organizations acquire information or knowledge, including vicarious learning ("acquiring second-hand experience"), searching, and scanning, (p. 96). Focused search implies that a specific problem is driving the pursuit of information. Once a "good enough" answer is found the search may stop, which [March and Simon](#march-simon) (1993) called "satisficing". The nature of scanning implies a more generalised and persistent information need, to keep abreast of ongoing changes or new ideas in a field or environment. Adapting [Choo's](#choo) (1998: 86) conceptual framework for environmental scanning, we can modify the basic components to represent a framework for considering scanning for adverse event information (Figure 1).

<figure>

![Figure 1](../p113fig1.jpg)

<figcaption>

**Figure 1.** A conceptual framework for adverse event information scanning</figcaption>

</figure>

Choo's framework includes information needs, seeking, and use as part of scanning behaviour. Information needs for health care risk management are potentially very broad, ranging from risks related to diagnostic and therapeutic procedures and hospital-acquired infections to equipment hazards and drug interactions. Information about adverse events related to these risks and hazards experienced in other organizations likely could be useful for vicarious learning.

With regard to information seeking, it would be interesting to know who scans, what sources, and how frequently, in order to know how to facilitate the endeavour. Many staff may play an informal boundary-spanning role, acting as conduits from external sources of risk and adverse event information. Health care professionals have become highly specialised over time, which may mean that discipline-specific knowledge and experience are required to scan effectively. Specialisation may also create barriers and compartmentalisation of information and knowledge ([West](#west), 2000). Given the increasing workload of front-line care providers, they may have little time available for general scanning, and by sheer necessity their information seeking may be restricted to highly focused, problem-driven searching.

In environmental scanning, information is used for strategic planning and decision making. In the case of adverse event information, the intended application is risk management and improvement activities. The risk information has to be evaluated to gauge its relevance and applicability within the particular organization. The process may vary markedly in rigour and formality, depending on who makes this judgement and how (i.e., based on what criteria). Many health care organizations have designated risk managers and/or risk management departments and committees. However, given the diversity of specialities and the sheer volume of potential adverse event information, it is unlikely that these entities alone could manage the floodgates. Assuming that some risk information passes the relevance test, the organization still has to muster the resources, ability, and will to do something about the identified risk or hazard, in the face of many other competing demands.

A descriptive study of these scanning behaviours related to adverse event information in hospitals would be a useful starting point. In addition, there are three constructs or sets of organizational characteristics and practices that may be quite relevant based on organizational theory and information studies research: a) organizational attention and information focus, b) organizational affinity, and c) absorptive capacity of the organization. These constructs may influence whether and how effectively health care organizations scan for adverse event information and learn vicariously.

### A. Organizational attention and information focus

Although it may seem rather obvious, the effectiveness of scanning and vicarious learning depend on organizational alertness to events and changes. If health care providers and managers are not paying attention to the external environment, useful information about risks and hazards may go unnoticed and the potential opportunity for learning may be lost.

Researchers highlight the importance of information processes linking the organization to its external environment ([Choo](#choo), 1998; [Hedberg](#hedberg), 1981; [MacDonald](#macdonald), 1995; [Stinchcombe](#stinchcombe), 1990). [Moorman](#moorman) (1995), for example, demonstrates the effect of organizational information processes on new product development. She emphasizes "the importance of having an external orientation that motivates the need for information and an internal orientation that fosters effective transmission and utilization" (1995, p. 328). She describes information acquisition as attention or awareness that has direction and intensity. Where and how an organization directs its gaze may be dictated by the types of issues it takes for granted as being important. [Corner _et al._](#corner) (1994) define attention as a focus on information and they state that an organization's attentional capacity is limited by its routines for gathering information or scanning. Likewise, [Rulke _et al._](#rulke) (2000) study particular learning channels for acquiring external information. They describe relational learning channels based on personal contacts and relationships, and non-relational learning channels, such as knowledge dissemination through journals, for example.

[Ocasio](#ocasio) (1997) develops these ideas further with his attention-based theory of the firm. He suggests that an organization's response to cues from its environment is influenced by the organization's attention structures and procedural and communication channels. His theory is based on three main principles. First, the focus of attention is selective and depends on what issues preoccupy organizational members at the time. As [Turner and Pidgeon](#turner) (1997) aptly note, "a way of seeing is always also a way of not seeing" (p. 49), so there is always the potential that important information may be overlooked or not recognised. Second, attention is also situated because the organizational and environmental contexts in which the organizational members find themselves shape their focus of attention and how they respond. Third, the structural distribution of attention suggests that organizational members are placed in particular contexts based on the way issues and responsibilities are allocated within the organization. "Each local activity within the firm involves a set of procedures and communications, and these procedures and communications focus the attention of decision-makers on a selected set of issues and answers" ([Ocasio](#ocasio), 1997: 191). Consequently, a health care organization's attention structures, procedures, and communication channels all may influence 1) who scans, 2) what types of risks, hazards, and events are noticed, 3) how frequently scanning takes place, and 4) how the information is processed.

Attention could also be influenced by the customary ways in which information is generally valued and handled in an organization. [Marchand _et al._](#marchand) (2000) describe this focus as information orientation, the composite of a company's capabilities to effectively manage and use information. Information orientation is comprised of three categories of practices: information technology, information management, and information behaviours and values ([Marchand _et al._, 2000](#marchand)). The latter category includes (p. 71):

*   Integrity: the absence of manipulation of information for personal gains;
*   Formality: the degree of use of and trust in formal information sources;
*   Control and sharing: the degree of exchange and disclosure of information;
*   Proactiveness: the degree to which members actively seek out information about changes in the environment; and
*   Transparency: the degree to which there is enough trust to be open about errors and failures.

These studies together suggest that there are differences in organizations' focus on information, that is, their propensity to gather information, the nature and effectiveness of their information routines and attentional mechanisms, and their degree of information use. The differences in this inclination may affect their awareness of and response to external adverse event information, which leads to the following hypothesis:

**HYPOTHESIS 1:** Health care organizations with a stronger external information focus may show greater organizational learning in response to other organizations' adverse events than health care organizations with a weaker external information focus.

### B. Organizational Affinity

Studies of knowledge transfer and innovation in and between firms provide insights into another construct that may influence the effect of external adverse event information on other health care organizations, which can be labelled as affinity [(2)](#2) between organizations. [Argote and Ingram](#argote-ingram) (2000) summarize several such studies, commenting that information is transferred more readily between organizations in network, alliance, or franchise relationships. For example, [Darr_et al._](#darr) (1995) found that knowledge or experience (as reflected in the unit cost of pizza production) transferred to other stores within same-owned chains, but not to other-owned franchises. [Darr and Kurtzberg](#darr-kurtzberg) (2000) describe how store managers rely on strategic similarity to chose knowledge transfer partners because of a better match with problems experienced, rather than store proximity or customer similarity. Networks have been found to promote sharing and adaptation among private colleges ([Kraatz](#kraatz), 1998) and biotechnology firms ([Liebeskind _et al._, 1996](#liebeskind); [Powell _et al._, 1996](#powell)). [Greve and Taylor](#greve) (2000) found that geographic proximity of firms to an innovating firm increased the likelihood that they would make changes as well. Consequently, affinity, such as similarity in organizational characteristics, proximity, and connections through networks or alliances, may affect scanning for and transfer of adverse event information.

Affinity by itself, however, is unlikely to be sufficient to enable organizational learning on its own; it is more likely that it provides hospitals that have a stronger information focus greater awareness and access to necessary information.

**HYPOTHESIS 2:** The relationship between information focus and organizational learning will be stronger for health care organizations that have a higher affinity with one experiencing an adverse event than will health care organizations with a lower affinity.

### C. Absorptive capacity

[Cohen and Levinthal](#cohen) (1990) coined the term "absorptive capacity" to describe an organization's ability to exploit external knowledge in innovation processes. They assert that this ability to recognize the value of new information, assimilate and apply it depends on the level of the firm's prior related knowledge. The same may be true of a health care organization's ability to scan effectively for risk or hazard information. Two types of prior related knowledge or experience may be necessary for a hospital to successfully use such information. First, the organization must develop self-knowledge ([Rulke _et al._, 2000](#rulke)), that is, familiarity with its own strengths and weaknesses, and capabilities. This allows the organizational members to know what types of information to be alert to when scanning. This also requires safety imagination, the creative ability to anticipate where and how processes may break down ([Turner & Pidgeon, 1997](#turner); [Westrum, 1992](#westrum)).

Second, the health care organization's ability and skills in improvement may influence whether risk or hazard information is successfully used to change practice. Knowledge of quality improvement methods ([Langley _et al._, 1996](#langley)) and skill in their application to clinical improvement ([Berwick & Nolan, 1998](#berwick-nolan)) could facilitate learning from adverse events experienced by others and risks identified through scanning. Improvement knowledge includes understanding health care delivery as a system ([Nolan, 1998](#nolan)); developing and testing changes ([Berwick, 1998](#berwick)); knowing what and how to measure ([Nelson _et al._, 1996](#nelson-mohr); [Nelson _et al._, 1998](#nelson-splaine)); and the role of interdisciplinary co-operation ([Clemmer _et al._, 1998](#clemmer)). Following such methods may increase the probability that the right changes will be made to address patient safety risks, and that the changes will actually achieve intended outcomes.

This variant of absorptive capacity, which is comprised of the organization's self-knowledge of its strengths and weaknesses, and its improvement capabilities, may act as another factor which differentiates organizations' scanning and vicarious learning:

**HYPOTHESIS 3:** Health care organizations with a greater absorptive capacity will show greater organizational learning in response to other organizations' adverse events than health care organizations with less-developed absorptive capacity.

## Summary

The intent of this paper was to extend the concepts of scanning and vicarious learning from failure to the realm of clinical adverse events and hospitals. Vicarious learning is "second-hand" learning derived from the experience of other individuals or organizations. Scanning in this context is defined as the acquisition and use of information about adverse events, patient safety risks, hazards, and trends from a health care organization's external environment, the knowledge of which would assist health care providers and managers in taking preventive action where necessary. Scanning for such information and making improvements before patients are injured could be an efficient and effective way for health care organizations to learn vicariously and avoid incurring the costs of adverse events themselves. This is easier said than done, however, and there are gaps in our knowledge of how to facilitate the process.

We need to understand more about information behaviours related to scanning for and use of adverse event information. As depicted in Figure 1, this includes information needs, seeking, and use and the multiple factors which may influence scanning and vicarious learning. Three constructs in particular are suggested for consideration. Organizational attention and information focus may affect how alert a health care organization is to risk and hazard information. This construct refers to an organization's propensity to gather information, the nature and effectiveness of its information routines and attentional mechanisms, and degree of information use. Second, organizational affinity, such as similarity in organizational characteristics, proximity, and connections through networks and alliances, may promote greater awareness of and access to information. Finally, absorptive capacity is comprised of the organization's self-knowledge of its strengths and weaknesses, combined with its improvement skills and capabilities. Absorptive capacity may help health care providers and managers know what kinds of information to scan for, and may increase the likelihood of successful implementation of improvements.

These preliminary hypotheses offer a starting point for increasing our understanding of the effect of attentional mechanisms and information focus, as well as the influence of organizational affinity and absorptive capacity, on the acquisition and transfer of adverse event and risk information. They could pave the way for further exploration of the mechanisms and processes by which adverse event information can be effectively used by hospitals. This would fall squarely within the AHRQ's research agenda for medical errors and patient safety ([Meyer_et al._, 2001](#meyer)).

## Endnotes

<a id="1">(1)</a> The Institute for Safe Medication Practices Canada (ISMPC) is implementing a voluntary reporting program specifically for medication-related errors and adverse events, modelled after a system originated in the US.

<a id="2">(2)</a> "Affinity" means a form of relationship, structural resemblance, or similarity of character, according to _The Concise Oxford Dictionary of Current English_ (7th ed.), (1982), Oxford: Oxford University Press.

## References

*   <a id="aguilar">Aguilar</a>, F. J. (1967). _Scanning the Business Environment._ New York, NY: Macmillan Co.  

*   <a id="argote">Argote</a>, L. (1999). _Organizational Learning: Creating, Retaining and Transferring Knowledge._ Norwell, MA: Kluwer Academic Publishers.  

*   <a id="argote-ingram">Argote</a>, L. and Ingram, P. (2000) "Knowledge transfer: a basis for competitive advantage in firms." _Organizational Behavior and Human Decision Processes,_ **82**(1), 150-169.  

*   <a id="argote-ingram-levine">Argote</a>, L., Ingram, P., Levine, J. M., and Moreland, R. L. (2000) "Knowledge transfer in organizations: learning from the experience of others." _Organizational Behavior and Human Decision Processes,_ **82**(1), 1-8.  

*   <a id="becher">Becher</a>, E.C., and Chassin, M.R. (2001) "Improving quality, minimizing error: making it happen." _Health Affairs,_ **20**(3), 68-81.  

*   <a id="berwick">Berwick</a>, D.M. (1998) "Developing and testing changes in delivery of care." _Annals of Internal Medicine,_ **128**, 651-656.  

*   <a id="berwick-nolan">Berwick</a>, D.M. and Nolan, T.W. (1998) "Physicians as leaders in improving health care: a new series in the Annals of Internal Medicine." _Annals of Internal Medicine,_ **128**, 289-292.  

*   <a id="brennan">Brennan</a>, T. A., Leape, L. L., Laird, N. M., Hebert, L., Localio, A. R., Lawthers, A. G., _et al._, (1991) "Incidence of adverse events and negligence in hospitalized patients: results of the Harvard Medical Practice Study I." _New England Journal of Medicine,_ **324**, 370-376.  

*   <a id="choo">Choo</a>, C. W. (1998). _Information Management for the Intelligent Organization: the Art of Scanning the Environment._ Medford, NJ: Information Today.  

*   <a id="clemmer">Clemmer</a>, T.P., Spuhler, V.J., Berwick, D.M., and Nolan, T.W. (1998) "Co-operation: the foundation of improvement." _Annals of Internal Medicine,_ **128**, 1004-1009.  

*   <a id="cohen">Cohen</a>, W.M., and Levinthal, D.A. (1990) "Absorptive capacity: a new perspective on learning and innovation."_Administrative Science Quarterly,_ **35**, 128-152.  

*   <a id="cook">Cook</a>, R. I., Woods, D. D., and Miller, C. (1998). _Tale of Two Stories: Contrasting Views of Patient Safety._ Workshop on Assembling the Scientific Basis for Progress on Patient Safety. Chicago, IL: National Patient Safety Foundation.  

*   <a id="corner">Corner</a>, P. D., Kinicki, A. J., and Keats, B. W. (1994) "Integrating organizational and individual information processing perspectives on choice." _Organization Science,_ **5**(3), 294-308.  

*   <a id="darr">Darr</a>, E.D., Argote, L., and Epple, D. (1995) "The acquisition, transfer, and depreciation of knowledge in service organizations: Productivity in franchises." _Management Science,_ **41**(11), 1750-1762.  

*   <a id="darr-kurtzberg">Darr</a>, E. D. and Kurtzberg, T. R. (2000) "An investigation of partner similarity dimensions on knowledge transfer."_Organizational Behavior and Human Decision Processes,_ **82**(1), 28-44.  

*   <a id="greve">Greve</a>, H. R. and Taylor, A. (2000) "Innovations as catalysts for organizational change: shifts in organizational cognition and search." _Administrative Science Quarterly,_ **45**(1), 54-80.  

*   <a id="harpster">Harpster</a>, L.M., and Veatch, M.S. (eds.) (1990). _Risk Management Handbook for Health Care Facilities._ Chicago: American Hospital Publishing Inc.  

*   <a id="hedberg">Hedberg</a>, B. (1981) "How organizations learn and unlearn." _in: Handbook of Organizational Design, Vol. 1,_ edited by P. C. Nystrom and W. H. Starbuck. (pp. 3-27) Oxford: Oxford University Press.  

*   <a id="huber">Huber</a>, G. P. (1991) "Organizational learning: the contributing processes and the literatures." _Organization Science,_ **2**(1), 88-115.  

*   <a id="ingram">Ingram</a>, P. and Baum, J. A. C. (1997) "Opportunity and constraint: organizations' learning from the operating and competitive experience of industries." _Strategic Management Journal,_ **18**, 75-98.  

*   <a id="kraatz">Kraatz</a>, M.S. (1998) "Learning by association? Interorganizational networks and adaptation to environmental change." _Academy of Management Journal,_ **41**(6), 621-643.  

*   <a id="kohn">Kohn</a>, L. T., Corrigan, J. M., and Donaldson, M. S. (1999). _To Err Is Human: Building a Safer Health System._ Washington, DC: Committee on Quality of Health Care in America, Institute of Medicine.  

*   <a id="langley">Langley</a>, G.J., Nolan, K.M., Nolan, T.W., Norman, C.L., and Provost, L.P. (1996). _The Improvement Guide: A Practical Approach to Enhancing Organizational Performance._ San Francisco, CA: Jossey-Bass Publishers.  

*   <a id="leape">Leape</a>, L. L., Brennan, T. A., Laird, N., Lawthers, A. G., Localio, A. R., and Barnes, B. A. (1991) "The nature of adverse events in hospitalized patients. Results of the Harvard Medical Practice Study II." _New England Journal of Medicine,_ **324**, 377-384.  

*   <a id="leape-berwick">Leape</a>, L. L. and Berwick, D. M. (2000) "Safe health care: are we up to it?" _British Medical Journal,_ **320**(7237), 725-726.  

*   <a id="liebeskind"></a>Liebeskind, J. P., Oliver, A. L., Zucker, L., and Brewer, M. (1996) "Social networks, learning, and flexibility: sourcing scientific knowledge in new biotechnology firms." _Organization Science,_ **7**(4), 428-443.  

*   <a id="macdonald"></a>MacDonald, S. (1995) "Learning to change: an information perspective on learning in the organization." _Organization Science,_ **6**(5), 557-568.  

*   <a id="march"></a>March, J.G. (1999) "The evolution of evolution", _in: The Pursuit of Organizational Intelligence._ (pp. 100-113) Oxford: Blackwell Publishers Ltd.  

*   <a id="march-simon"></a>March, J.G., and Simon, H.A. (1993). _Organizations._2nd. Edition. Oxford: Blackwell.  

*   <a id="marchand"></a>Marchand, D. A., Kettinger, W. J., and Rollins, J. D. (2000) "Information orientation: people, technology and the bottom line." _Sloan Management Review,_ **41**(4), 69-80.  

*   <a id="meyer"></a>Meyer, G., Foster, N., Christup, S., and Eisenberg, J. (2001) "Setting a research agenda for medical errors and patient safety." _Health Services Research,_ **36**(1), Part 1, x-xx.  

*   <a id="miner"></a>Miner, A. S. and Haunschild, P. R. (1995) "Population-level learning." _Research in Organizational Behavior,_ **17**, 115-166.  

*   <a id="miner-kim"></a>Miner, A. S., Kim, J. Y., Holzinger, I. W., and Haunschild, P. (1999) "Fruits of failure: organizational failure and population-level learning." _Advances in Strategic Management,_ **16**, 187-220.  

*   <a id="moorman"></a>Moorman, C. (1995) "Organizational market-information processes: cultural antecedents and new product outcomes." _Journal of Marketing Research,_ **32**(3), 318-335.  

*   <a id="national"></a>National Patient Safety Foundation. (1998). _Enhancing Patient Safety and Reducing Errors in Health Care._. Rancho Mirage, CA: Annenberg Center for Health Sciences at Eisenhower  

*   <a id="nelson-mohr"></a>Nelson, E.C., Mohr, J.J., Batalden, P.B., and Plume, S.K. (1996) "Improving health care, part 1: the clinical value compass." _Joint Commission Journal on Quality Improvement,_ **22** (4), 243-258.  

*   <a id="nelson-splaine"></a>Nelson, E.C., Splaine, M.E., Batalden, P.B., and Plume, S.K. (1998) "Building measurement and data collection into medical practice." _Annals of Internal Medicine,_ **128**, 460-466.  

*   <a id="nolan"></a>Nolan, T.W. (1998) "Understanding medical systems." _Annals of Internal Medicine,_ **128**, 293-298.  

*   <a id="ocasio"></a>Ocasio, W. (1997) "Towards an attention-based view of the firm." _Strategic Management Journal,_ **18**, 187-206.  

*   <a id="powell"></a>Powell, W. W., Koput, K. W., and Smith-Doerr, L. (1996) "Interorganizational collaboration and the locus of innovation: networks of learning in biotechnology." _Administrative Science Quarterly,_ **41**(1), 116-145.  

*   <a id="rulke"></a>Rulke, D.L., Zaheer, S., and Anderson, M.H. (2000) "Sources of managers' knowledge of organizational capabilities." _Organizational Behavior and Human Decision Processes,_ **82**(1), 134-149.  

*   <a id="sitkin">Sitkin</a>, S. B. (1992) "Learning through failure: the strategy of small losses", _in: Organizational Learning,_ edited by M. D. Cohen and L. S. Sproull. (pp. 541-577) Thousand Oaks, CA: Sage.  

*   <a id="stinchcombe">Stinchcombe</a>, A. L. (1990). _Information and Organizations._ Berkeley, CA: University of California Press.  

*   <a id="subramanian">Subramanian</a>, R., Kumar, K., and Yauger, C. (1994) "The scanning of task environments in hospitals: an empirical study." _Journal of Applied Business Research,_ **10**(4), 104-115.  

*   <a id="thomasej">Thomas</a>, E. J., Studdert, D. M., Burstin, H. R., Orav, E. J., Zeena, T., Williams, E. J., Howard, K. M., Weiler, P. C., and Brennan, T. A. (2000) "Incidence and types of adverse events and negligent care in Utah and Colorado." _Medical Care,_ **38**(3), 261-271.  

*   <a id="thomas">Thomas</a>, J.B., Clark, S.M., and Gioia, D.A. (1993) "Strategic sensemaking and organizational performance: linkages among scanning, interpretation, action, and outcomes." _Academy of Management Journal,_ **36**(2), 239-270.  

*   <a id="turner">Turner</a>, B. A. and Pidgeon, N. F. (1997). _Man-Made Disasters._ 2nd. Edition. Oxford: Butterworth-Heinemann.  

*   <a id="vincent">Vincent</a>, C., Neale, G., and Woloshynowych, M. (2001) "Adverse events in British hospitals: preliminary retrospective record review." _British Medical Journal,_ **322**, 517-518.  

*   <a id="west">West</a>, E. (2000) "Organisational sources of safety and danger: sociological contributions to the study of adverse events." _Quality in Health Care,_ **9**, 120-126.  

*   <a id="westrum">Westrum</a>, R. (1992) "Cultures with requisite imagination", _in: Verification and Validation of Complex Systems: Human Factors Issues,_ edited by J. A. Wise, V. D. Hopkin, and P. Stager. (pp. 401-416) Berlin: Springer-Verlag.  

*   <a id="yasai-ardekani">Yasai-Ardekani</a>, M. and Nystrom, P. C. (1996) "Designs for environmental scanning systems: tests of a contingency theory." _Management Science,_ **42**(2), 187-204.