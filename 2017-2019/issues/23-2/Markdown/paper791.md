<header>

#### vol. 23 no. 2, June, 2018

</header>

<article>

# Active participants and lurkers in online discussion groups: an exploratory analysis of focus group interviews and observation

## [Tali Gazit, Jenny Bronstein, Yair Amichai-Hamburger, Noa Aharony, Judit Bar-Ilan](#author) and [Oren Perez](#author).

> **Introduction**. While online discussion groups have become powerful tools to enhance open democratic discussions, the literature shows that only a marginal percentage of individuals are active participants. The majority of users read the content but do not participate (lurkers). The aim of this research was to better understand the psychological and environmental factors impacting the online participation of active participants and lurkers.  
> **Method**. Four focus groups were conducted in which participants were asked to describe their participation behavior in online public discussions.  
> **Analysis**. The data were analysed using thematic analysis that facilitates identifying, analysing, and reporting patterns or themes within data.  
> **Results**. Regarding psychological factors, findings show that active participants seemed to be more extroverted and open than lurkers, they felt they had better control over the online environment, a higher ability to influence, higher self-efficacy and a greater need for gratification. Regarding environmental factors, both active participants and lurkers reported they need a place to express themselves, need the content to be emotionally triggered and relevant, and need a familiar environment.  
> **Conclusions**. Findings demonstrated that both personality and environmental factors play in the adoption of the role of lurker or active participant in online communities.

<section>

## Introduction

Modern communication channels, and especially the Internet, are considered nowadays to be a vital part of the daily lives of millions of people around the world ([Amichai-Hamburger, 2008](#ami08)). Online discussion groups, as an important aspect of online communications, are Internet spaces that allow people with similar interests to congregate and to discuss common problems and issues and to offer information and support on a variety of topics ([Himelboim, Gleave and Smith, 2009](#him09)).

Despite the need for user participation in online groups, research demonstrates that only a marginal percentage of individuals contribute to online discussions while the majority of online community users play a passive role ([Han, Hou, Kim and Gustafson, 2014](#han14); [Nonnecke and Preece, 2000](#nonpre00)). According to the ninety-nine-one rule, ninety percent of users do not actively participate in online discussions, while nine percent of users contribute to some degree, and only one percent of users account for almost all of the online action ([van Mierlo, 2014](#van14)).

The literature distinguishes between _lurkers_ and _creators_ ([Lai and Chen, 2014](#lai14); [Preece, Nonnecke and Andrews, 2004](#pre04)). Creators are active members in online discussions who are generally regarded as more constructive members and considered essential for sustaining the online community as a dynamic social group ([Cullen and Morse, 2011](#cul11)). Lurkers, on the other hand, are defined by their That is, lurkers read social media data, but do not directly contribute to it ([Muller, 2012](#mul12)) and prefer passive attention over active participation ([Rafaeli, Ravid, and Soroka, 2004](#raf04)).

Understanding the factors behind participation and lurking in online communities is central to understanding the socialisation element in online social behaviour, especially since lurkers have opinions, ideas, and information that can be of value to the online and offline community ([Edelmann, 2013](#ede13)). Too many lurkers may lead to a low posting rate and lack of valuable content, which is a problem that many online communities are faced with ([Sun, Rau and Ma, 2014](#sun14)).

The level of participation, as an aspect of online behaviour, is also important because prior research has found that online participation enhances social well-being ([van Uden-Kraan, Drossaert, Taal, Seydel and van de Laar, 2008](#van08)), has a positive influence on social self-esteem, and reduces levels of stress and depression ([Herrero, Meneses, Valiente and Rodríguez, 2004](#her04)). Although the phenomenon of lurking has been examined before, it has been a challenge recruiting lurkers as subjects, as it is difficult to identify them online ([Muller, 2012](#mul12)) and once they are identified, researchers have used surveys [(Hung, Chai, and Chou, 2015](#hun15)) or interviews ([Lee, Chen, and Jiang, 2006](#lee06); [Nonnecke and Preece, 2000](#nonpre00)) to examine their online behaviour. This study used focus groups as a research methodology that allowed us to hear both active participants' and lurkers' voices in a group context. According to Gibbs ([1997](#gib97)), focus groups are widely used in academic research to examine attitudes, feelings, experiences and reactions in a way that would not be possible with one to one interviews, observations or questionnaires. The environment of focus groups allows participants to react and build upon responses from other members ([Klein, Tellefsen and Herskovitz, 2007](#kle07)). Exploring focus groups as an innovative methodology in the study of online participation enabled us to better understand the psychological and environmental factors behind both participation and lurking in online discussion groups.

</section>

<section>

## Literature review

The literature specifies a variety of factors that determine the users' level of participation. For example, fifteen indicators were associated with the popularity of online question and answer discussion forums, divided into three components: personal, environmental, and behavioural ([Cheng, Liu and Shieh, 2012](#che12)). In an earlier study, four types of reasons for lurking were identified: environmental influence, personal preference, individual-group relationship and security considerations ([Sun _et al._, 2014](#sun14)). Preece, Nonnecke and Andrews ([2004](#pre04)) observed five main reasons for lurking: (1) not needing to post; (2) needing to find out more about the group before participating; (3) thinking that they were being helpful by not posting; (4) not being able to make the software work (i.e., poor usability); (5) not liking the group dynamics or the community was a poor fit for them. In a recent article Amichai-Hamburger _et al._, ([2016](#ami16)) proposed a model which divides the factors behind lurking into three categories: individual differences (need gratification levels, personality dispositions, time available and self-efficacy), social-group processes (socialisation, type of community, social loafing, responses to delurking and quality of the response) and technological setting (technical design flaws and the privacy and safety of the group). In light of this model, the current study divides the factors affecting online participation into two main interests: psychological factors and environmental ones.

</section>

<section>

### Psychological factors

Nonnecke’s ([2000](#non00)) study claimed that there are individuals who are predisposed to lurk and individuals who are predisposed to post. Research has shown that when users of social media sites are not anonymous they tend to behave in a similar way to how they would behave in face-to-face interactions ([Amichai-Hamburger and Hayat, 2013](#ami13)). Prior studies have found a relationship between personality traits and participation behaviour ([Aharony, 2013](#aha13); [Bronstein _et al._, 2016](#bro16)). For example, individuals who are higher on the openness personality trait participate more in online groups ([Nussbaum, Hartley, Sinatra, Reynolds and Bendixen, 2004](#nus04)), and extroverted people participate more than introverts in social media sites ([Wang, Jackson, Zhang and Su, 2012](#wan12)). However, other studies suggested that introvert individuals would be expected to display higher levels of online participation than their regular, more personal interactions, especially when they interact in an anonymous environment in an attempt to meet their social and intimacy needs ([Amichai-Hamburger _et al._, 2016](#ami16)). The model of Amichai-Hamburger _et al._([2016](#ami16)) suggests four individual factors for lurking: need gratification levels, personality dispositions, time available and self-efficacy. The current study relates to this model, based on participants’ reports in the focus groups.

To summarise, it seems that people have a tendency to express their personalities online, and that the extroversion - introversion theory appears to demonstrate this tendency with extroverts being more dominant in identified environments and introverts adopting a more extrovert style in anonymous environments ([Amichai-Hamburger, 2017](#ami17); [Amichai-Hamburger and Hayat, 2013](#ami13); [Amichai-Hamburger and Schneider, 2014](#ami14)).

Furthermore, Cullen and Morse ([2011](#cul11)) suggested that different personality traits are related to different motivations to become active members of an online community. Thus, we may conclude that one cannot focus solely on the personality aspect and must also take into account the environment.

</section>

<section>

## Environmental factors

When relating to the model of Amichai-Hamburger _et al._ model ([2016](#ami16)), the social-group processes and technological setting categories are considered as environmental factors to participation. These are especially interesting, since group managers can take action in order to enhance participation. Muller ([2012](#mul12)) found that the behaviour of participants changed according to the group they participated in. In one group they could be active, while in another they can become lurkers. Rafaeli _et al._ ([2004](#raf04)) found that to become more active, one needs to feel very comfortable with the group. In a survey that distinguished between active participants and lurkers, different motivations for participation were found. Active participants were influenced by enjoyment of helping others, while lurkers were influenced by the perceived ease of use and compatibility ([Hung _et al._, 2015](#hun15)).

Other studies have found that the dynamics in online groups, such as information overload, can impact online participation ([Haythornthwaite, 2009](#hay09)) causing users to read less and thus acquiring less social capital, and having less in common with other users ([Rafaeli _et al._, 2004](#raf04)). Other motivations for participation in online discussion can be the level of group intimacy ([Phua, 2014](#phu14)), the relevance of the content for users, speed of the response rate ([Cheng, Liu and Shieh, 2012](#che12)) and the quality of information given in these discussions ([Lee, Chen and Jiang, 2006](#lee06)). To summarise, while it is clear that personality _per se_ does not predict actual behaviour, an individual may possess personality traits that will give them a tendency to behave in a certain way. This, however will not be expressed if the context for this course of action is not perceived as appropriate. In light of the above, the current study will examine both psychological and environmental factors that influence lurkers' and active participants’ behaviour in online communities and will use focus groups in order to understand the elements that affect online participation in discussion groups.

</section>

<section>

## Method

This study examined the following exploratory research questions:

> 1\. What are the psychological factors behind participating or lurking in online discussion groups?  
> 2\. What are the environmental factors behind participating or lurking in online discussion groups?

</section>

<section>

### Populations and sampling

To answer the research questions, we conducted a qualitative study involving four focus group discussions with small groups of Internet users. Two of the groups consisted of lurkers and two consisted of active participants. A total of twenty-three participants took part in the discussions.

</section>

<section>

#### The two lurkers' groups

The difficulty of sampling lurkers is well-known (e.g., [Muller, 2012](#mul12); [van Uden-Kraan _et al._, 2008](#van08)), since they are seldom heard on the Internet. The sampling of this population was consequently conducted in two steps: first, an advertisement calling for participation in a research study was published on the Facebook page of the Bar-Ilan University student union. Approximately 500 students interested in participating responded to the advertisement. An e-mail message was then sent to all 500 students asking them to answer two brief questions about how often they read online discussion group content and how active they were in these groups. Two hundred and forty students responded to the message, of whom fifty-nine fitted the criteria for lurkers, reporting that, while they read discussion group content, they rarely participated or did not participate at all. Twelve of these students were randomly selected and invited to participate in each of the two focus groups.

</section>

<section>

#### The two active participants' groups

Active participants were invited to participate in the study through a number of advertisements published on several Websites and Facebook pages that have active discussions about Israeli social issues ([Open Government](http://oGovernment.org/), [URU](https://www.facebook.com/URUisrael); and [The Situation Room](https://www.facebook.com/j14live?fref=ts) ). Those who answered by e-mail or Facebook messenger were asked to send their telephone number and a short telephone interview was conducted about their participation in the groups. Twelve participants who reported being very active in Internet discussion groups were randomly selected and invited to participate in one of the two focus groups, while one of them did not attend the meeting. The distribution of respondents is shown in Table 1.

<table><caption>Table 1: Participants in the four focus groups</caption>

<tbody>

<tr>

<th>Groups</th>

<th>Participation level</th>

<th>Male</th>

<th>Female</th>

</tr>

<tr>

<td>1</td>

<td>6 lurkers</td>

<td>4</td>

<td>2</td>

</tr>

<tr>

<td>2</td>

<td>6 lurkers</td>

<td>3</td>

<td>3</td>

</tr>

<tr>

<td>3</td>

<td>6 active participants</td>

<td>5</td>

<td>1</td>

</tr>

<tr>

<td>4</td>

<td>5 active participants</td>

<td>3</td>

<td>2</td>

</tr>

<tr>

<td>Total</td>

<td>23</td>

<td>15</td>

<td>8</td>

</tr>

</tbody>

</table>

</section>

<section>

### Method of data collection

Focus groups were chosen as the method of data collection. This technique is a form of group interview that capitalises on communication between research participants to generate data ([Morgan, 1996](#mor96)). Prior research ([Secker, Wimbush, Watson and Milburn, 1995](#sec95)) has established that a focus group is a small group of four to twelve people that meets with a trained facilitator for a short period of time to discuss a selected topic or topics in a non-threatening environment. Although group interviews are often used simply as a quick and convenient way to collect data from several people simultaneously, focus groups use group interaction as part of the method of data collection. Participants in the groups are encouraged to interact with each other by asking questions, exchanging anecdotes, and commenting on each other's experiences and points of view. Group discussion is particularly appropriate when the interviewer has a series of open-ended questions and wishes to encourage research participants to explore issues. The disadvantage of such group dynamics is that the articulation of group norms may silence individual voices of dissent ([Kitzinger, 1995](#kit95)). This technique has been used to investigate various subjects such as information needs of nurses ([Schweikhard, 2016](#sch16)), continuing education of medical students ([Swanberg, Engwall and Mi, 2015](#swa15)), breast cancer detection ([Duke, Gordon-Sosby, Reynolds and Gram, 1994](#duk94)), adolescent sexuality ([Barker and Rich, 1992](#bar92)) and using social networking sites for educational activities ([Hamid, Waycott, Kurnia and Chang, 2015](#ham15)).

The focus groups were held in conference rooms at Israeli academic institutions. The sessions lasted for approximately one hour and were moderated by a trained groups instructor. Participants were asked to describe their participation behaviour in online public discussions. The session continued with several additional guiding questions, allowing free discussion to continue when the content was interesting or relevant. Students received remuneration for participating in the study.

</section>

<section>

### Data analysis

Data collected during the focus groups were analysed in two stages. The first stage consisted of a thematic analysis that facilitates identifying, analysing, and reporting patterns or themes within data. A theme is a pattern found in the information that describes and organises the possible observations ([Boyatzis, 1998](#boy98)). Data from the focus groups were first transcribed in full. The transcriptions were then read several times and analysed to search for data pertaining to environmental and psychological factors. After the two major themes were identified the classification process continued until all content was categorised. Groups of related categories were arranged by themes using an inductive approach in which the categories are derived from the data moving from the specific to the general. Specific instances were observed and then combined into a larger whole or general statement ([Elo and Kyngäs, 2008](#elo08)).

</section>

<section>

## Findings

Observation of the four group discussions and analysis of the transcriptions revealed that in order to understand the behaviour and motivations of active participants we must relate to personality dimensions, environmental characteristics, and the interaction between the two. Figure 1 presents the categories and sub-categories of the determinants for participation and lurking in online discussions.

<figure>

![Figure 1: Categories of qualitative content analysis conducted on groups of active participants and lurkers in Internet discussion groups ](../p791fig1.png)

<figcaption>Figure 1: Categories of qualitative content analysis conducted on groups of active participants and lurkers in Internet discussion groups</figcaption>

</figure>

Personality dimensions refer to openness to experience and extroversion as well as various psychological characteristics of perception of the world, including sense of control, the ability to influence, self-efficacy, and the need for gratification. Environmental dimensions refer to four characteristics of the discussion groups that appeared in participants' discussions: a place to express and innovate, content that triggers emotion, relevancy, and familiarity. The meetings were analysed and categorised according to these concepts while relating to the four focus groups of active participants and lurkers.

</section>

<section>

### Atmosphere of the groups

Before quoting the actual statements of the participants, non-verbal impressions are also important when relating to focus group findings ([Kitzinger, 1995](#kit95)). The atmosphere at the two lurkers' meetings was very calm, relatively quiet, each of the participants talked in turn and it was only seldom that we witnessed a participant who expressed an urgent need to talk. The body language of the lurkers expressed tension, many nods of agreement with the other speakers, and it was evident that they were not used to such discussions. Since all of the participants have described themselves as readers, who usually do not participate in online discussions, we must take into account that even in real life their participation in the focus group may have been somewhat forced and unnatural to them. Indeed, in most cases participants spoke only when the moderator approached them with a question or when an orderly round was held.

On the other hand, the atmosphere was quite different in the active participants' groups. The two focus groups of active participants were characterised by great alertness from the participants and a sincere desire to share, express and without fear of disagreements between the participants. Even if at the beginning of the discussion there was an orderly round in which each answered the moderator's questions in turn, the participants did not refrain from expressing themselves freely without necessarily being addressed directly. In a short time, the discussion became spontaneous without the need for round-ups or explicit appeals to the participants. This characterisation is probably compatible with the fact that these people are very active, both in the virtual world and in the outside world.

The findings presented below represent the participants' self-perceptions and their reflections about their online behaviour.

</section>

<section>

### Personality

</section>

<section>

#### Personality traits

The personality traits considered were taken from the big five personality scale ([McCrae and John, 1992](#mcc92)), according to the description of the concepts extroversion and openness to experience.

**Extroversion/introversion**: The big five personality scale defines introverted people who are at the bottom of the extroverted scale as shy, quiet, and cautious ([McCrae and John, 1992](#mcc92)). The quiet atmosphere that prevailed among lurkers therefore fits their descriptions of themselves. Content analysis of the groups of lurkers revealed that the lurkers in the focus groups defined themselves, among other things, as people who are not open, who like to listen, or who prefer not to draw attention to themselves.

> I'm introverted. I like listening to others more than I like talking.

Extroverts, on the other hand, extend their energies outward. They are involved in society, active, assertive, and adventurous ([McCrae and John, 1992](#mcc92)). These personality traits are evident among the active participants in the focus groups. From the standpoint of atmosphere these people are talkative, active, and assertive - traits that are characteristic of extroverts.

> I like to be a leader. I'm involved and active.

**Openness to experience:** The _big five personality scale_ defines open-minded people as being creative, willing to explore new intellectual directions, and having a broad range of interests. People who have a low level of open-mindedness have limited areas of interest, and have little imagination or love of art (McCrae and John, 1992). Content analysis of the groups of lurkers shows that they describe themselves as being more closed.

> I'm a person who has difficulty changing my opinion.

On the other hand, the active participants in the focus groups described themselves as having traits that are characteristic of open-mindedness, such as searching for intellectual challenges on the Internet and being members of a large number of interest groups.

> I like being exposed to different things, exploring them, and learning something new every time.

In summary, we observed relatively introverted personality traits among lurkers during the focus groups sessions as well as in their descriptions of themselves as people who seldom take centre stage. Their behaviour in the group as well as their descriptions of themselves portray them as relatively shy, not open, and difficult to persuade. Active participants, on the other hand, display open, extrovert personality traits. These people seemed to be curious, willing to listen, and to change their opinion if persuaded, but are also quite ready to express their opinion, to change, and to lead. The extroverted personalities of the active participants in the focus groups were also expressed in the dynamics that took place in the groups: extensive chatting, enthusiastic talk, and a great deal of willingness to expose themselves and to express their opinions. These findings can be associated with a previous study that showed a positive relationship between the traits extroversion and openness and the participation level in online discussion groups ([Bronstein _et al._, 2016](#bro16)).

**Perception of the world:** Most participants in the lurkers groups revealed that they perceive the world as an unsafe place fraught with danger. This perception creates the need to be careful in order to feel safe. They consequently proceed through the world with caution and suspicion. Like the real world offline, the Intern_et al._ so constitutes an unfamiliar place. They do not know who is on the other side of the discussion, who is reading, or what information about them is being exposed, and therefore they feel they must proceed with caution.

> I didn't want to leave my fingerprints because anything that is published is permanent.  
>   
> I feel strange conducting a conversation with people when I don't know who they are. They are often impersonating someone else.

As the lurkers in the focus groups noted, they need a familiar and safe environment to express themselves. After they become familiar with the environment there is a chance they will allow themselves to express their opinions. However, such an environment is familiar to them only in the real, non-virtual world:

> If there's an article that especially interests me I'll share it with a friend and talk to him about it, but not on the Internet.  
>   
> When friends sit together everyone expresses their opinion to the others and makes them think and delve deeper. On the Internet people might read and simply move on.

These statements reveal that lurkers may want to respond, but because they perceive that the Internet is an unsafe place, they refrain from doing so. There is a better chance that they will respond when the environment is familiar and friendly, or when they are sufficiently certain that what they have to say is more accurate than what was published on the Internet and they want to correct it:

> I only respond when I really know. Sometimes there are things that I know are correct but they are not presented correctly in the discussion. For example, spreading incorrect information about a representative of a particular political party will make me enter the discussion.  
>   
> When something is presented incorrectly, I might respond.

The active participants in the focus groups felt that it is possible to control the online world regardless of whether or not it is a safe place.

> I don't censor myself, but I also won't embarrass myself. If a discussion crosses the red line, I'll delete what I wrote.

**Feeling in control:** The lurkers in the focus groups described themselves as using the Internet to get information and they admitted that they often read Israeli news Websites such as [Ynet](https://www.ynet.co.il/home/0,7340,L-8,00.html) or [Mako](https://www.mako.co.il). For example:

> I read news on Mako a lot. It's important to me to keep informed, but I don't make any other use of it.

This action appears to increase their feelings of control, since in this case information helps to relieve anxiety. The fact that they need a great deal of information points to the need for control and for the relief of anxiety:

> I'm a frequent user. Ynet is always open in my browser. I keep up to date in what is happening every hour. I'm drawn to politics. I'm not calm if I'm not up-to-date.

It appears that the lurkers in the focus groups use the Internet as a source of information that gives them a clearer picture of the world, making reality appear more familiar and secure. This could simply be keeping updated for its own sake, but it may also have a psychological function of re-establishing control and eliminating anxiety.

> I'm active in places where people identify themselves by their real name, where you can delve deeply into things, examine what people think, and contribute. I want to be able to understand that the other side doesn't simply toss things out by copying and pasting but really reads it.  
>   
> I have to feel comfortable in order to be active. I need to feel whether the environment is comfortable or threatening.

**Belief in the ability to influence:** Most of the lurkers in the focus groups believed that activity on the Internet has no impact upon the discussion itself and certainly not on life outside the Internet. This causes them to refrain from action and makes them believe that participating in this environment is an unnecessary waste of time.

> My participation won't change anything. The Internet offers many possibilities of responding, but it's all pointless.

The lurkers in the focus groups felt that the only way they could influence or control the discussion is through face-to-face interaction:

> If something interests me I'll talk about it with friends face to face. There is no real impact on the Internet.  
>   
> I'm more interested in participating and expressing my opinion in real life. There is less influence on the Internet than in real life. Face-to-face conversations are more significant.

Even the lurkers in the focus groups did not completely deny the impact of the Internet when it is combined with face-to-face activity:

> The Social Protest movement would not have brought about the results that it did if it had occurred only on the Internet. The Internet contributed to it, but without the real-life protests nothing would have happened.

On the other hand, the active participants in the focus groups expressed the need for their actions to have an impact on their environment.

> I write in order to bring about change.

At the same time, it is important to them to feel that they are not writing in a vacuum, and that there is really someone listening on the other side and that there is a possibility of change.

> It has to have impact. And if I feel that there is no chance that there will be some sort of impact, I may not enter [the discussion]. Why should I say what I feel?  
>   
> Nobody cares if I respond on Ynet. I'm not looking for "Wow, read me!" I prefer more distinct things such as recommending a book that I read. I prefer audiences who know me, where I have the ability to influence people. I prefer to do it there.

Statements made by the active participants reveal that they attribute a great deal of importance to their impact on the Web, but that the experiences they have accumulated until now are also significant.

**Self-efficacy:** People with high self-efficacy are confident that they will be able to deal successfully with tasks. High self-efficacy increases the tendency to invest effort in executing tasks. Low self-efficacy, on the other hand, decreases the motivation to execute the task and arouses a sense of failure ([Bandura, 1982](#ban82)). A number of active participants shared similar stories in which they posted something out of a need to influence others, and received positive feedback from other participants.

> People told me that they voted for a particular candidate because of what I wrote.  
>   
> I respond when there is a chance I will reach someone. The last time I responded on talkback in "HaAretz" news Website, it made a change. The headline was very biased, and they changed it. At least there should be change.

In other words, these people understood that their statements did not simply vanish into virtual space, but created an interaction or had an impact that made them feel that they had a presence in the community. This in turn brought about feelings of self-efficacy - increasing their tendency to invest effort in the activity. Active participants' sense of self-efficacy increased as a result of proof of change, together with the knowledge that there are people that are reading and paying attention to their opinions. For example:

> The thing that matters most to me is not change, but seeing that there is some sort of interaction. Change is a complex thing. I don't pretend that I'll succeed in changing anything visible – not even slightly. But I do hope to see some interaction. I want to see that someone has read what I wrote, that someone responded, that someone asked a question that I can answer.  
>   
> The response of the other side is important. Will they do something about what was written? I think that's an extremely significant catalyst that determines whether or not to enter [the discussion].

The lurkers in the focus groups, as opposed to the active participants, showed a much lower sense of self-efficacy regarding activity on the Web.

> Nothing will happen even with those who respond.  
>   
> People who write are optimistic and believe that it's possible to arouse interest on the Internet. I'm not like them.

The results of the perception of self-efficacy are a critical factor in determining a person's activity on virtual groups. When people perceive a given task as not being compatible with their abilities as they perceive them, they are likely to stop investing efforts in the task. If people perceive a task as one suited to their abilities, they will go out of their way to increase their efforts despite difficulties and obstacles ([Bandura, 1982](#ban82)). There was consequently a lack of expression of self-efficacy among the lurkers, while there was a significant amount of expression among the active participants.

**The need for gratification:** The degree to which users feel the need to write and communicate with others on the Internet is often a reflection of their deep social and emotional needs ([Amichai-Hamburger _et al._, 2016](#ami16)). According to the psychologist Heinz Kohut, the need for feelings of gratification and value from others stems from parent-child relations ([Siegel, 1996](#sie96)). Some of the active participants in the focus groups expressed their desire for publicity and gratification.

> It's important to me not to be anonymous. I want people to know who I am and who is speaking.  
>   
> When I write I'm not the type to simply formulate a short slogan that will receive lots of likes and lots of people will see and recommend. I write posts that are longer than one page. I'm usually drawn to it and take the trouble to keep up with it, to see what others added…I even look the next day to see if anyone added anything.

The need for gratification seemed to be one of the motivating forces of the active participants in the focus groups. The question is not whether the responses to the post were positive or negative. A positive response may boost feelings of self-efficacy and encourage continued activity, but even when the response is not a positive one, the need for gratification and publicity can be met. This is a sufficient reason to continue writing.

> Putting it up and seeing everyone's responses. The best thing is to see a response, whether it's good or not. The type of response doesn't bother me.

This need for gratification was not evident among the lurkers in the focus groups. They are satisfied with communication with those close to them and it appears that they do not need outside recognition. Their limited activity on the Web, which primarily consists of quietly reading, may reflect the fact that they have little need for gratification. As passive participants, they observe what is going on and are more than a little critical of the active participants. They read what others write, but believe that they are writing because of a desire to stand out, do not have a real life, or are bored. The criticism and need to separate themselves from the active participants is evident in their tone of speech as well as the content:

> Those who write can't keep things to themselves. They have to release their feelings immediately. They aren't used to turning to friends or relatives – which is what I think is the better thing to do.  
>   
> It's a matter of character. If a person likes to arouse provocation, they will respond.

On the other hand, being satisfied with themselves and those around them may also lead to living in a bubble as expressed in their level of activity in the real world that is discussed in the following category.

**Activity outside the Internet:** When asked directly about social activities outside the Internet, most of the lurkers in the focus groups spoke about contact with close friends or family members, but only one talked about social involvement in a broader framework such as volunteering or community service.

> I volunteer on a friendship hotline where I offer people legal advice.  
>   
> I feel the focal points of people's difficulties and meet people who have been badly affected by certain laws. It supports my agenda and enables me to implement real changes.

Other participants did not talk about any type of social activities such as joining a political party, or participating in any committees.

Findings from the content analysis revealed a pronounced difference in the participation in offline activities between active participants and lurkers. When asked directly about their activities outside the Internet most of the active participants spoke about extensive activities which they take part in outside the Internet, such as participating in the social protest movement, volunteering with refugee children, and registering with political parties.

> I'm very active in anything regarding sharing the burden. I'm active in forums, and participate in marches, rallies, booths, and signing petitions. I have been active in this issue for five years. (Translator’s note: ‘sharing the burden’ refers to legislation calling for all Israeli citizens, including the religious, to share the burden of defence.)  
>   
> I am active in several issues. I deal with personal and political education. I counsel refugees from Eritrea within the framework of an urban kibbutz.

It appears, therefore, that most active participants on the Internet are also active offline. Indeed, it was recently found that there is a high positive correlation between social activity outside the Internet and similar activity within it ([Bronstein _et al._, 2016](#bro16)), which support these findings.

</section>

<section>

### Environment

Environmental factors that encourage or discourage active participation exist alongside differences in personality between lurkers and active participants. People who manage Internet discussions have no control over the personalities of group members or their perception of the world, but they have control over the environment. Comprehensive understanding of environments that encourage activity, while differentiating between various populations, can promote rich discussions with large numbers of participants.

**A place for expression and innovation:** As passive readers, the lurkers in the focus groups expressed the feeling that there is nothing that has not been said already and they have nothing to contribute to the group. For example:

> Everything has already been said. There is a wide range of responses and someone else has already said what I think.  
>   
> To tell the truth, I rely on others to respond.

The last statement raises the question: what if everyone was like you? What if everyone relied on others to say what is important? Evidently the lurkers in the focus groups did not see things that way. They experience the Internet as a place that is flooded with opinions and do not feel that their opinion is significant. This attitude is referred to in social psychology as social loafing. Social loafing implies that individuals contribute less effort to attaining an objective when they understand that they are part of a group as opposed to when they are working alone ([Karau and Williams, 1993](#kar93), p. 681). When lurkers assume that others in the group will post, they absolve themselves of the responsibility of being active ([Amichai-Hamburger _et al._, 2016](#ami16)).

It is, however, important to note that most of the lurkers in the focus groups explained that they are most familiar with discussion groups of the talkback variety on news Websites. Exposure to this type of discussion constitutes in itself an explanation for their lack of participation.

> There were times when I wanted to offer my opinion, but then I read the superficial responses [on talk back] and I didn't want to be a part of it.

If it were possible to expose them to a more protected discussion format such as forums or closed Facebook groups, they would feel more confident to express their opinion and not remain passive.

The active participants in the focus groups, on the other hand, do not hesitate in expressing their opinions and are not deterred by arguments or confrontations.

> If I feel I have enough to contribute to devote my time to it, I'll post.

However, the active participants also stated that they stop and assess things before they express themselves – something that is possible in a virtual environment but not in real life.

> I'll wait [to respond] until I’m sure that I understand what's happening.  
>   
> I look at every place I reach, assess, read, and often if someone there incites me to respond, I won't respond if I don't feel it's a safe place for me.

It is likely that monitoring responses and a less crowded atmosphere are needed to encourage lurkers to participate. On the other hand, a diluted discussion would result in less participation of active participants. There is therefore a need to find a balance in discussions that will allow active participants to continue their activities, but will also leave lurkers a place for their opinions.

**Content that triggers emotion:** The lurkers in the focus groups gave the impression that they were pleasant, quiet people who did not interrupt each other and were not quick to contribute their opinions to the group. It is evident that this type of behaviour also occurs on the Internet. Despite this, it was interesting to hear what it would take for them to become active users. Some of the lurkers explained that in order for that to happen there would need to be content that is particularly inciting, irritating, inaccurate, or anger-provoking.

> I'm only active when something makes me irritated or angry.

It appears that there is no difference between the lurkers and the active participants in the focus groups in this regard:

> The thing that motivates me to respond is whether the topic creates enough emotional/personal involvement for me to respond.  
>   
> It depends on how much the topic speaks to me or irritates or angers me. Then I will share or respond. If I have nothing to add, I'll share.

The need for content that triggers emotion (either negative or positive) is therefore an important factor in weighing whether or not to act. The topic must invoke anger, irritation, speak to people's hearts, or simply excite them.

**The relevance of the topic to group members:** Content and relevancy of the topic to their lives is important for both active participants and lurkers. Unlike lurkers who do not respond even if the topic is important to them, active participants testify that if the topic is important to them, it will urge them to respond.

> The first thing I do is decide whether the topic interests me enough and if it is relevant to me.  
>   
> If it's interesting I'll read it thoroughly and not just skim. I've restrained myself somewhat in the last few months because discussions can develop that last for hours. If the discussion is focused, I'll respond.

The active participants in the focus groups actively look for topics that interest them and are relevant to their lives. Whenever they note a topic of interest, they are willing to invest their time and energy to write, express themselves, and enter a discussion. The lurkers in the focus groups seemed to be less active, but might make more effort to find places that are relevant to them.

I don't always hurry to express my opinion. Even in social discussions outside the Internet people don't always want to listen. If there are things that pertain to me I'll express myself, but I'm in no hurry.  

When I first planned to hike on the Israel Path I searched for forums with recommendations and tips. I had questions, but I didn't feel comfortable posing them to strangers on a forum.

When the topic is close to their hearts both lurkers and active participants might participate, but lurkers may decide that the existing material is sufficient, while the active participants may create it. As explained by one of the lurkers, if the environment were warmer, less strange, and more familiar she would have built up the courage to post.

**A familiar environment:** As mentioned above, the lurkers in the focus groups sometimes want to ask a question or express their opinion but don't dare to do so:

> I really wanted to express my opinion, but I didn't.

Many participants explained that they will express their opinion to friends and family, and even argue, but not on the Internet where they don't know who is on the other side. For example:

> I find it strange to conduct a conversation with people I don't know. Sometimes they are impersonating someone else.

Participants' remarks reveal that an environment that makes lurkers feel safe will encourage them to be more active than one that is perceived as new or alienating. Some lurkers may feel safer in their natural face-to-face environment where they have more experience with social interactions. Such an environment offers visual, vocal, and non-verbal dimensions that give them a better sense of control as opposed to the virtual Internet.

The active participants in the focus groups, on the other hand, dare to operate with an unknown audience or in discussion groups in which they do not know the other members. However, some of the active participants expressed a desire for a warm environment in order to express themselves.

> I think that in the end it has to fulfill some need. I need to feel warm and pleasant and for my friends to be there to embrace me.  
>   
> I have to feel OK. This site is not fiction. It's trustworthy. It's pleasant to be there. There are all kinds of Websites that you feel are truthful.

It appears that a warm and supportive atmosphere encourages activity among both lurkers and active participants. Lurkers have an additional advantage if they are acquainted with other participants offline and view them as friends.

</section>

<section>

### The complexity of anonymity

Participation in online discussion groups can provide a sense of security when cloaked in anonymity. The discussions in the two lurkers' focus groups were divided into two categories: talkbacks that are characterised by posters remaining anonymous in which they read but do not participate, and the participation in Facebook groups where everyone is represented by their personal profile and full name. These two environments constitute an interesting opportunity for assessing the level of lurkers' participation from the aspect of both the platform and the level of anonymity.

> People are influenced a great deal from what others say about them. Anonymity helps a lot to express, particularly in groups with stigmas where people are not like everyone.  
>   
> Anonymity helps people writing answers to be free and truthful. On the other hand, readers will relate to them less seriously.

These two statements reflect the complexity of anonymity. On one hand, it is easier to express oneself in an anonymous environment. On the other hand, it is harder to relate seriously to a person who does not write using their real name. Consequently, in order to understand the significance of anonymity for lurkers, the structure of the group and the status of the person within it must be taken into consideration. Active participants also expressed a variety of opinions that differed according to their personalities and experiences. One participant feels her appearance is linked to a large amount of conflicts:

> I won't censor myself while writing anonymously or with my real name. A person needs to be who they are. But I might not post certain things because I don't like to get into arguments like I used to. When I identify myself it can make things complicated for me personally or socially.

The status of the participant in the group as a member or an administrator can also have an impact:

> As someone who has been a forum administrator, I know how easy it is to identify people, and there's no difference. I don't respond differently when I am anonymous than when I'm identified. I always stand behind what I write or say. There is no need for masks. But on the other hand, as a forum administrator, I censor more.

Online participation can have an impact on the user's daily life:

> There's a big difference between anonymity and identity. I expressed my opinions very clearly on a forum for equality against the religious using my full name. But I received a phone call inviting me to come to a meeting about a business proposal in the religious sector. I immediately deleted all of my responses on Facebook before the meeting. It's nice to write your opinion, but sometimes your opinions can harm your livelihood!

Hence, the active participants in the focus groups do not hesitate to occasionally express themselves under their real identity, but they are sometimes reminded that when they are identified, or when they are working in certain jobs, they must censor themselves and are not completely free to speak their minds.

</section>

<section>

## Discussion and conclusion

This study examined the role that psychological and environmental factors play in participation in online discussion groups. Findings from the content analysis of the focus groups showed that both factors were found as important for understanding the level of participation for the lurkers as well as for the active participants in the focus groups. There are a number of psychological factors that can impact the level of participation. Personality traits such as extroversion and openness to experience were the ones that stood out as the subjects described themselves psychologically. This finding echoes earlier studies ([Amichai-Hamburger and Hayat, 2013](#ami13); [Bronstein _et al._, 2016](#bro16); [Nussbaum _et al._, 2004](#nus04)) that showed that the more extroverted and open a person is, the higher their level of participation in online discussion. Self-efficacy was also revealed as one of the factors because lurkers can be motivated to become more active if they are ensured that their actions will be influential with respect to both the group attentiveness as well as in the world outside the Internet. Self-efficacy predicted thirty-four percent of the variance in computer usage among participants ([Gangadharbatla, 2008](#gan08)). This study validated that belief in one's computer skills significantly predicts online participation. The belief in making a change or saying something meaningful in a discussion is more complex, but still can empower lurkers. This finding echoes the study of Hung _et al._ ([2015](#hun15)), which showed that knowledge self-efficacy enhanced the perceived behavioural control of knowledge sharing among participants.

Environmental factors were found to be important for both active participants and lurkers in the focus groups. Participants in the lurkers focus groups expressed the need for a safe, familiar, and protected environment in order to become active in the discussions. These observations revealed that a feeling of greater security and protection - both internally and within the external environment - motivate them to become more involved. Supporting this observation, Amichai-Hamburger _et al._. ([2016](#ami16)) claimed that online communities that provide participants with safety should therefore ensure that there can be no harmful psychological effects to the participation online communities. Participants also stated the need for certain supervision in the group's activity; that is, someone should manage the discussion so things do not get out of control or deteriorate into superficial areas and ensure that feedback remains positive and friendly. Supporting this perception, Nonnecke ([2000](#non00)) points out that privacy and safety are frequently not guaranteed in online communities, and it seems that many who would have been active participants in virtual communities find that the uncontrolled access and persistent messages are an obstruction of their privacy and security. In addition, care should be taken to avoid a flood of opinions so lurkers will not feel that they have nothing to say. Information overload can impact online participation ([Haythornthwaite, 2009](#hay09)) causing users to read less and thus acquiring less social capital, and having less in common with other users ([Rafaeli _et al._, 2004](#raf04)).

One interesting point that contributes to this area of research is the role that content plays in the level of participation. Participants from both groups reported that their reaction to the type of content posted in the discussion, especially if they found this content annoying, emotionally charged, or directly relevant to their lives, can be a trigger to participating. No prior research was found echoing this assertion.

The role that anonymity plays in online participation is a complex one and depends much upon the type of group, its dynamics, and upon the social status that the lurker occupies within it ([Amichai-Hamburger _et al._, 2016](#ami16)). Online discussion groups that facilitate anonymity might allow users to feel less exposed, but at the same time might create some feelings of insecurity as to the identity of other members. A number of recent studies found that in platforms like WhatsApp, in which the users were identified, the participation was much higher than in more anonymous platforms like news talkbacks. In either case the key is how much control lurkers have over activity in the group, and how secure they feel as part of the group ([Aharony and Gazit, 2016](#aha16); [Bronstein _et al._, 2016](#bro16)).

This study contributes to the subject of online participation by focusing on two important sets of factors, psychological and environmental characteristics that might impact the level of participation. The use of focus groups as a methodology tool allowed access to different types of data that present a deeper understanding of the issue based on the users' individual experiences. The study's methodology represents its main limitation. Since this is a qualitative and explorative study based on a small number of participants, its findings cannot be generalised to all Internet users. However, we believe that the opportunity for an exchange of opinions in a thought-provoking atmosphere at the focus groups provided a unique and interesting opportunity for the collection of data not always possible in quantitative surveys or face-to-face interviews.

</section>

<section>

## Acknowledgements

This research was supported by the Ministry of Science, Technology & Space of Israel.

## <a id="author"></a>About the authors

**Tali Gazit** is a researcher and a lecturer in the Department of Information Science, Bar-Ilan University (Israel) and a member in the Research Center for Internet Psychology (CIP), Interdisciplinary Center (IDC), Israel. She holds a PhD in Internet Psychology from the Department of Information Science, Bar-Ilan University. Her research interests are psychological and environmental factors of participation and lurking in online discussions, well-being and Internet, social networking groups and information literacy. Dr. Gazit can be contacted at: [tal.gazit@biu.ac.il](mailto:tal.gazit@biu.ac.il)  
**Jenny Bronstein** is a senior lecturer at the Information Science department at Bar-Ilan University. Her research interests are in information behavior of migrants and refugees, education and professional development, self presentation and self disclosure on different social platforms and has published in refereed information science journals. She teaches courses in information retrieval techniques, information behavior, academic libraries and business information. She holds a PhD and a M.S. in Information Science from Bar-Ilan University. She can be contacted at: [jenny.bronstein@biu.ac.il](mailto:jenny.bronstein@biu.ac.il)  
**Yair Amichai-Hamburger** (PhD, Oxford University) is a Professor of Psychology and Communication, and Director of the Research Center for Internet Psychology (CIP) at the Interdisciplinary Centre, IDC, Herzlia. He has written widely on the impact of the Internet on wellbeing. He can be contacted at [yairah@idc.ac.il](mailto:yairah@idc.ac.il)  
**Noa Aharony** received her Ph.D. in 2003 from the School of Education at Bar- Ilan University (Israel). Her research interests are the Internet, technological innovations, Web 2.0, and information literacy. Prof. Aharony is the head of the School of Information Science at Bar Ilan University. She can be contacted at: [Noa.Aharony@biu.ac.il](mailto:noa.aharony@biu.ac.il).  
**Judit Bar-Ilan** is professor at the Department of Information Science of Bar-Ilan University in Israel. She received her PhD in computer science from the Hebrew University of Jerusalem and started her research in information science in the mid-1990s at the School of Library, Archive and Information Studies of the Hebrew University of Jerusalem. Her areas of interest include: informetrics, information retrieval, Internet research, information behavior and usability. Since 2015 she has been academic head of MALMAD, the Israeli Inter-University Center for Digital Information Services. She is the winner of the 2017 De Solla Price Award. Her detailed CV and list of publications [can be found here](http://is.biu.ac.il/en/judit). Contact her at [Judit.Bar-Ilan@biu.ac.il](mailto:judit.bar-ilan@biu.ac.il)  
**Oren Perez** has an LLB from Tel Aviv University and LL.M. (1997), Ph.D. (2001) from London School of Economics and Political Science. He also has a BA in Philosophy, University of London, 2015\. He is currently Dean of Bar-Ilan University, Faculty of Law and can be contacted at: [oren.perez@biu.ac.il](mailto:oren.perez@biu.ac.il).

</section>

<section>

## References

<ul> 

<li id="aha13">Aharony, N. (2013), Factors affecting the adoption of Facebook by information professionals. <em>Proceedings of the American Society for Information Science and Technology, 50</em>(1), 1-10.</li>

<li id="aha16">Aharony, N. &amp; Gazit, T. (2016). The importance of the WhatsApp family group: an exploratory analysis. <em>ASLIB Journal of Information Management, 68</em>(2), 174-192.</li>

<li id="ami08">Amichai-Hamburger, Y. (2008). Internet empowerment. <em>Computers in Human Behavior, 24</em>(5), 1773-1775.</li>

<li id="ami17">Amichai-Hamburger, Y. (2017). <em>Internet psychology: the basics.</em> New York, NY: Routledge</li>

<li id="ami16">Amichai-Hamburger, Y., Gazit, T., Bar-Ilan, J., Perez, O., Aharony, N., Bronstein, J. &amp; Dyne, T.S. (2016). Psychological factors behind the lack of participation. <em>Computers in Human Behavior, 55</em>(Part A), 268-277. </li>

<li id="ami13">Amichai-Hamburger, Y. &amp; Hayat, Z. (2013). Personality and the Internet. In Y. Amichai-Hamburger (Ed.), <em>The social net: understanding our online behavior</em> (2nd ed), (pp. 1-20). New York, NY: Oxford University Press.</li>

<li id="ami14">Amichai-Hamburger, Y. &amp; Schneider, B. H. (2014). Loneliness and Internet use. In Robert J. Coplan and Julie C. Bowker, (Eds.). <em>The handbook of solitude: psychological perspectives on social isolation, social withdrawal, and being alone.</em> (pp. 317-334). Chichester, UK: Wiley Blackwell</li>

<li id="ban82">Bandura, A. (1982). Self-efficacy mechanism in human agency. <em>American Psychologist, 37</em>(2), 122–147.</li>

<li id="bar92">Barker, G.K. &amp; Rich, S. (1992). Influences on adolescent sexuality in Nigeria and Kenya: findings from recent focus-group discussions. <em>Studies on Family Planning, 23</em>(3), 199-210.</li>

<li id="boy98">Boyatzis, R.E. (1998). <em>Transforming qualitative information: thematic analysis and code development.</em> Thousand Oaks, CA: Sage Publications.</li>

<li id="bro16">Bronstein, J., Gazit, T., Perez, O., Bar-Ilan, J., Aharony, N. &amp; Amichai-Hamburger, Y. (2016). An examination of the factors contributing to participation in online social platforms. <em>ASLIB Journal of Information Management, 68</em>(6), 793-818.</li>

<li id="che12">Cheng, S.S., Liu, E.Z.F. &amp; Shieh, R.S. (2012). Identifying the indicators attracting users to online question and answer discussion forums. <em>Social Behavior and Personality, 40</em>(2), 283-292.</li>

<li id="cul11">Cullen, R. &amp; Morse, S. (2011). Who's contributing? Do personality traits influence the level and type of participation in online communities? In <em>Proceedings of the 44th Hawaii International Conference on System Science</em> (pp. 1-11).  Washington, DC: IEEE.</li>

<li id="duk94">Duke, S.S., Gordon-Sosby, K., Reynolds, K.D. &amp; Gram, I.T. (1994). A study of breast cancer detection practices and beliefs in black women attending public health clinics. <em>Health Education Research, 9</em>(3), 331-342.</li>

<li id="ede13">Edelmann, N. (2013). Reviewing the definitions of &quot;lurkers&quot; and some implications for online research. <em>Cyberpsychology, Behavior, and Social Networking, 16</em>(9), 645-649. 

</li><li id="elo08">Elo, S. &amp; Kyngäs, H. (2008). The qualitative content analysis process. <em>Journal of Advanced Nursing, 62</em>(1), 107-115.</li>

<li id="gan08">Gangadharbatla, H. (2008). Facebook me: collective self-esteem, need to belong, and Internet self-efficacy as predictors of the iGeneration’s attitudes toward social networking sites. <em>Journal of Interactive Advertising, 8</em>(2), 5-15.</li>

<li id="gib97">Gibbs, A. (1997). <a href="http://www.webcitation.org/6oO7aYint">Focus groups.</a> <em>Social Research Update</em>, No. 19. Retrieved from  http://sru.soc.surrey.ac.uk/SRU19.html. (Archived by WebCite® at  http://www.webcitation.org/6oO7aYint)</li>

<li id="ham15">Hamid, S., Waycott, J., Kurnia, S. &amp; Chang, S. (2015). Understanding students' perceptions of the benefits of online social networking use for teaching and learning. <em>The Internet and Higher Education, 26</em>(1), 1-9.</li>

<li id="han14">Han, J.Y., Hou, J., Kim, E. &amp; Gustafson, D.H. (2014). Lurking as an active participation process: a longitudinal investigation of engagement with an online cancer support group. <em>Health Communication, 29</em>(9), 911-923.</li>

<li id="hay09">Haythornthwaite C. (2009). <em><a href="http://www.webcitation.org/6oO7oKi4M">Online knowledge crowds and communities.</a> </em>Paper presented at the International Conference on Knowledge Communities, Center for Basque Studies, University of Nevada, Reno, Retrieved from https://bit.ly/2Gwfmp4 (Archived by WebCite® at http://www.webcitation.org/6oO7oKi4M)</li> 

<li id="her04">Herrero, J., Meneses, J., Valiente, L. &amp; Rodríguez, F. (2004). Online social participation. <em>Psicothema, 16</em>(3), 456-460.</li>

<li id="him09">Himelboim, I., Gleave, E. &amp; Smith, M. (2009). Discussion catalysts in online political discussions: content importers and conversation starters. <em>Journal of Computer-Mediated Communication, 14</em>(4), 771-789.</li>

<li id="hun15">Hung, S.Y., Lai, H.M. &amp; Chou, Y.C. (2015). Knowledge-sharing intention in professional virtual communities: a comparison between posters and lurkers. <em>Journal of the Association for Information Science and Technology, 66</em>(12), 2494-2510.</li>

<li id="kar93">Karau, S.J. &amp; Williams, K.D. (1993). Social loafing: a meta-analytic review and theoretical integration. <em>Journal of Personality and Social Psychology, 65</em>(4), 681-706.</li>

<li id="kit95">Kitzinger, J. (1995). Introducing focus groups (qualitative research, part 5). <em>British Medical Journal, 311</em>(7000), 299-302. </li>

<li id="kle07">Klein, E.E., Tellefsen, T. &amp; Herskovitz, P.J. (2007). The use of group support systems in focus group: information technology meets qualitative research. <em>Computers in Human Behavior, 23</em>(5), 2113-2132.</li>

<li id="lai14">Lai, H.-M. &amp; Chen T.T. (2014). Knowledge sharing in interest online communities: a comparison of posters and lurkers. <em>Computers in Human Behavior, 35</em>, 295-306.</li>

<li id="lee06">Lee, Y.W., Chen, F.C. &amp; Jiang, H.M. (2006). Lurking as participation: a community perspective on lurkers' identity and negotiability. In <em>ICLS’06 Proceedings of the 7th International Conference on Learning Sciences</em> (pp. 404-410). Bloomington, IN: International Society of the Learning Sciences. </li> 

<li id="mcc92">McCrae, R.R. &amp; John, O.P. (1992). An introduction to the five-factor model and its applications. <em>Journal of Personality, 60</em>(2), 175-215.</li>

<li id="mor96">Morgan, D.L. (1996). Focus groups. <em>Annual Review of Sociology, 22</em>,129-152.</li>

<li id="mul12">Muller, M. (2012). Lurking as personal trait or situational disposition? Lurking and contributing in enterprise social media. In <em>Proceedings of the ACM 2012 conference on Computer Supported Cooperative Work, Seattle, Washington, USA — February 11 - 15, 2012</em> (pp. 253-256).  New York, NY: ACM Press.</li>

<li id="non00">Nonnecke, R.B. (2000). <em><a href="http://www.webcitation.org/6oO80tisz">Lurking in e-mail-based discussion lists</a></em>. Unpublished doctoral dissertation, South Bank University, London, UK. Retrieved from http://www.cis.uoguelph.ca/~nonnecke/research/blairsthesis.pdf (Archived by WebCite® at: http://www.webcitation.org/6oO80tisz)

</li><li id="nonpre00">Nonnecke, R. B. &amp; Preece, J. (2000). Lurker demographics: counting the silent. In <em>Proceedings of the SIGCHI2000 Conference on Human Factors in Computing Systems. The Hague, Netherlands</em> (pp. 73-80), New York, NY: ACM Press.</li>

<li id="nus04">Nussbaum, E.M., Hartley, K., Sinatra, G.M., Reynolds, R.E. &amp; Bendixen, L.D. (2004). Personality interactions and scaffolding in on-line discussions. <em>Journal of Educational Computing Research, 30</em>(1-2), 113-136.</li> 

<li id="phu14">Phua, J. (2014). Quitting smoking using health issue-specific social networking sites (SNSs): What influences participation, social identification, and smoking cessation self-efficacy? <em>Journal of Smoking Cessation, 9</em>(1), 39-51.</li>

<li id="pre04">Preece, J., Nonnecke, B. &amp; Andrews, D. (2004). The top five reasons for lurking: improving community experiences for everyone. <em>Computers in Human Behavior, 20</em>(2), 201-223.</li>

<li id="raf04">Rafaeli, S., Ravid, R. &amp; Soroka, V., (2004). <a href="http://www.webcitation.org/6oO8F3R1O">De-lurking in virtual communities: a social communication network approach to measuring the effects of social and cultural capital.</a> In <em>Proceedings of the 37th Annual Hawaii International Conference on System Sciences, (HICSS 04)</em> (10 pages). Los Alamitos, CA: IEEE. Retrieved from http://www.unhas.ac.id/~rhiza/arsip/jarkomsos/lurkers.pdf (Archived by WebCite® at http://www.webcitation.org/6oO8F3R1O)</li>

<li id="sch16">Schweikhard, A.J. (2016). An information needs assessment of school nurses in a metropolitan county. <em>Medical Reference Services Quarterly, 35</em>(1), 27-41.</li>

<li id="sec95">Secker, J., Wimbush, E., Watson, J. &amp; Milburn, K. (1995). Qualitative methods in health promotion research: some criteria for quality. <em>Health Education Journal, 54</em>(1), 74-87.</li>

<li id="sie96">Siegel, A.M. (1996). <em>Heinz Kohut and the psychology of the self</em>. London: Routledge.</li>

<li id="sun14">Sun, N., Rau, P.P.L. &amp; Ma, L. (2014). Understanding lurkers in online communities:a literature review. <em>Computers in Human Behavior, 38</em>, 110-117.</li>

<li id="swa15">Swanberg, S. M., Engwall, K. &amp; Mi, M. (2015). Continuing education for medical students: a library model. <em>Journal of the Medical Library Association</em>, 103(4), 203-207</li>

<li id="van14">van Mierlo, T. (2014). The 1% rule in four digital health social networks: an observational study. <em>Journal of Medical Internet Research, 16</em>(2), e33. </li>

<li id="van08">van Uden-Kraan, C.F., Drossaert, C.H.C., Taal, E., Seydel E.R., &amp; van de Laar, M.A. (2008). Self-reported differences in empowerment between lurkers and posters in online patient support groups.  <em>Journal of Medical Internet Research, 10</em>(2), e18. </li>

<li id="wan12">Wang, J.L., Jackson, L.A., Zhang D.J. &amp; Su, Z.Q. (2012). The relationships among the Big Five Personality factors, self-esteem, narcissism, and sensation-seeking to Chinese university students’ uses of social networking sites (SNSs). <em>Computers in Human Behavior, 28</em>(6), 2313-2319. </li>

<li id="wan11">Wang, Q., Woo, H.L., Quek, C.L., Yang, Y. &amp; Liu, M. (2011). Using the Facebook group as a learning management system: an exploratory study. <em>British Journal of Educational Technology, 43</em>(3), 428-438.</li>

<li id="yeo06">Yeow, A., Johnson, S. &amp; Faraj, S. (2006). Lurking: legitimate or illegitimate peripheral participation? In <em>Proceedings of the 27th International Conference on Information Systems (ICIS 2006)</em> (p. 967–982). Atlanta, GA: Association for Information Systems. </li>

</ul>

</section>

</article>