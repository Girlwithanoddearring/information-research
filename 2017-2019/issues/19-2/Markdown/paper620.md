<header>

#### vol. 19 no. 2, June, 2014

</header>

<article>

# The acquisition of e-books in the libraries of the Swedish higher education institutions

#### [Elena Maceviciute, Martin Borg, Ramune Kuzminiene and Katie Konrad](#author)  
University of Borås, SE-501 90 Borås, Sweden

#### Abstract

> **Introduction.** Our aim is to compare the advantages and problems of e-book acquisition identified in research literature to those experienced by two Swedish university libraries.  
> **Method.** A literature review was used to identify the main issues related to acquisition of e-books by academic libraries. The data for comparison were collected through case studies in two Swedish universities. Document analysis, interviews and personal experience were used for data collection.  
> **Results.** The main drivers of e-book acquisition by Swedish academic libraries are the perceived needs of the users. E-books are regarded as potentially useful for solving some of the problems of library service. A number of challenges and problems identified by the participants in the case studies coincide with those that were derived from the literature review.  
> **Conclusions.** The problems of e-book acquisition in academic libraries seem to be common to the economically strong Western countries. University librarians see certain advantages of e-books for their users and libraries. Publishers and academic librarians expect that e-books would not lose the advantages that printed books offered to them. Hence, publishers restrict the usage of e-books to ensure revenues as if from selling individual copies. Librarians try to regain the same level of control over e-book collections as for printed materials.

<section>

## Introduction

The number of articles and books on e-books is growing rapidly, but the literature stays fragmented and scattered over several disciplines. One of the areas that already can benefit from consolidation of research data is academic librarianship. Academic libraries acquire e-books and manage a variety of e-book collections from a number of distributors, aggregators and publishers using their rich experience of negotiations from the earlier phase of working with e-journals and databases of full-text articles as well as other digital resources. The supply of academic e-book resources (mainly, scholarly and scientific monographs, treatises, research reports, e-textbooks, reference books and some other non-fiction e-books used as study literature) in the English language is already quite prolific on the international market and has great significance for scholarly communication. Even the production of e-monographs in smaller local languages is emerging in many countries (e.g., Sweden, Poland, etc.) ([Bernhardsson, _et al._, 2013](#ber13); [Kulczycki, 2012](#kul12)) and in different academic publishing contexts (university presses, non-profit foundations, commercial publishing). Such texts also draw attention to the role of academic libraries in their production, distribution, and preservation. As academic libraries were the main buyers of scholarly monographs that rarely appeared as entirely commercial products, the stress on library budgets from the acquisition of other electronic resources (e-journals, databases, e-textbooks) disrupts the existing business model of monograph production severely ([Svensson and Eriksson, 2013](#sve13)), so new ways for their distribution are sought (see [www.knowledgeunlatched.org](http://www.knowledgeunlatched.org) or [www.oapen.org](http://www.oapen.org)). As research is mainly focused on academic libraries in English speaking countries or English language publications, the question arises whether the same is true for academic libraries in countries with less widely-used languages.

The aim of the paper is to compare the problems identified in research literature to those experienced by two Swedish university libraries. This should help to establish, 1) if the general problems identified in research literature in other countries are present in a Swedish university library; 2) what are the needs expressed by Swedish libraries for acquiring and providing access to e-books and, 3) what are the problems they experience in relation to this.

The results of the literature review and of two case studes will be used in the design of an extended survey of Swedish academic libraries and their practice of acquiring and managing e-books in the research project on e-books funded by the Swedish Research Council (Vetenskapsrådet). The team of authors is related to this country through working or studying in one of its universities, and in the case of the principal author, through participation in the funded research project. The topic is limited to the issues of acquisition of e-books and managing e-book collections. It does not include e-book usage or user perspectives on e-books in academic libraries or investigations of e-book providers for academic libraries.

## Methodology

Winston's model ([1998](#win98)) of technological innovation in media has offered a useful framework for this investigation. Elsewhere ([Bergström, Höglund, Maceviciute, Nilson and Wilson, 2014](#ber14)) we have explained the use of Winston's model for exploring the diffusion of e-books as follows:

> Winston suggests that, before an applicable invention is employed, a number of prototype forms exist. These arise, in the technological field, out of ideas derived from science. For the invention to be adopted a ‘supervening social necessity' is needed: he cites the case of the single track railway necessitating a means of communication beyond line-of-sight, which encouraged the adoption of the telegraph. The widespread diffusion of an innovation requires that attempts to suppress the ‘radical potential' of an invention must be overcome: we can think here of the attempts by the music industry to suppress by various means, including digital rights management, the invention of the digital music file. Finally, some inventions may be shown to be redundant over time and other, subsidiary inventions may spin off from the invention under study ([Bergström, _et al._, 2014](#ber14)).

The model includes four stages of innovation diffusion: ideation, prototyping, invention, and diffusion (production). The stage of invention transforms some of the existing prototypes into inventions under certain supervening social necessities (i.e., a complex of technological and social forces working directly on the process of innovation). The final stage relates to an innovation moving into the market under circumstances of suppression of its radical potential ([Winston, 1998, p. 7-10](#win98)).

The model offers the possibility of exploring the tension between the radical potential of the e-book conflicting with the established investment in the existing technologies and adherence to established business models. The underlying social necessities have affected the development of technologies underpinning e-reading devices and spread of e-books in a variety of formats and in different spheres. The underlying social necessities for e-books could be traced through the ways and conditions of e-books' usage in these different spheres. Exploration of the activities of different actors related to the exploitation of e-books can also reveal the means of the supression of the radical potential of e-books. At present, looking at the spread of an e-book in the world and the attention it attracts to the book sector, we already may be sure that an e-book, as it exists at present, has outgrown the stages of a prototype, an invention, and entered the stage of production.

The following methods were used in the two equally important parts of this article: literature review and two case studies.

A literature search was conducted on the ISI Web of Knowledge and Scopus databases, and was augmented by findings in other sources (e.g., using Swedish library catalogues and scholar.google.se). The review of relevant literature focuses on both the global and the Swedish perspectives on the impact of the e-book on academic libraries. More research is available on a global scale, therefore, more examples and investigations come from this English language literature. The intent is not to de-emphasize the Swedish case but a lack of material prevents an equally deep analysis. The publications used in the review were also reviewed in two Master's theses on e-books in academic libraries ([Konrad, 2013](#kon13); [Kuzminiene, 2014](#kuz14)). Selection for the review was performed looking for the newest literature first, but also for repetitive topics indicating the commonality of a described feature or problem. The main focus of this review is to understand contemporary trends of acquisition of e-books and e-book collection development from the academic library's perspective. It focuses on publications appearing within the past seven to eight years that signify the change in the use and spread of e-books, though some older ones also may be used. The structure of the literature review reflects the identified topics starting with the widest issues and gradually introducing narrower and narrower issues.

Case study data were collected from the library documents, through library events focused on e-books, and on interviews with e-book acquisition staff. It also includes direct experience of one of the authors who works in the library with e-book collection management and licence negotiations. The data were collected during the last months of 2013 and the start of 2014\. Case 2 study was performed by Katie Konrad in 2012 and 2013\. The data analysis was made and the results are presented following the topics identified in the research literature.

## Previous research and literature review

### The e-book as a concept and as a digital object

Scholars have used many different ways to define an e-book. Armstrong and Lonsdale ([2005](#arm05)) claim that initially there was some confusion resulting from associating an e-book itself with an e-book reader. At present most definitions emphasize a book-like structure and the possibility of reading a text on the screen of a digital device ([Armstrong, 2014](#arm14)). Armstrong has earlier declared that an e-book is:

> Any content that is recognisably ‘book-like', regardless of size, origin or composition, but excluding serial publications, made available electronically for reference or reading on any device (handheld or desk-bound) that includes a screen ([Armstrong, 2008, p. 199](#arm14)).

An e-book is becoming more than just a PDF version of the printed book, as digital texts can change and augment the nature of book-like objects in an electronic context ([Nelson, 2008](#nel08)). Vassileiou and Rowley suggest a two-part definition: the first part relates to the reasonably stable nature of e-books (persistent characteristics) and the second part focuses on the changing new technologies through which they are delivered and read:

> (1) An e-book is a digital object with textual and/or other content, which arises as a result of integrating the familiar concept of a book with features that can be provided in an electronic environment. (2) E-books, typically have in-use features such as search and cross reference functions, hypertext links, bookmarks, annotations, highlights, multimedia objects and interactive tools.” ([Vassiliou and Rowley, 2008, p. 362](#vas08)).

As with print books, there are also not only many genres of e-books, but many types of them already available and multiplying further as the technology develops. E-books range from scanned copies of print books to database-type structures ([Minci&cacute;-Obradovi&cacute;, 2011](#min11)). According to Tedd ([2005](#ted05)) in academic libraries the general types of e-book likely to be acquired include textbooks, multimedia books, reference books and directories as well as digitised versions of out-of-print books. Similarly, Armstrong and Lonsdale ([2005](#arm05)) in their study of UK education libraries provide a nearly identical list for type of e-books in academic libraries, which the individual disciplines felt were most likely to be in demand, but they add scholarly monographs, grey literature and free e-books. As one can easily see, these lists lack coherence and include items defined by genre (scholarly monographs, textbooks, reference), by access mode (free e-books, grey literature, out-of-print books) or by technological characteristics (multimedia, databases). The problem of definition is discussed by Velagi&cacute; ([2014](#vel14)) in another paper in this issue.

The issue of academic e-books may be addressed in relation to the issue of the e-book as a concept; however, here it will be treated in a pragmatic way. We define an academic e-book as any book-like digital object (meeting the terms of Vassileiou and Rowley definition) acquired by academic libraries.

#### E-book formats and platforms

Contrary to the situation of e-journal articles, which are predominantly in the PDF format, there is no standard file format for e-books. According to Walters ([2013a](#wal13a)) some sources have even up to 27 and at least 16 different e-book formats listed as being currently in use.

The PDF file format seems to be dominating collections of academic libraries ([Vasileiou, Hartley and Rowley, 2012a](#vas12a); [Walters, 2013a](#wal13a)). But most of the formats are proprietary and store data in the ways designed by a particular company making it easily accessible only when using software and hardware of that company. The specification of data encoding format is not released or published but restricted by digital rights management (DRM). That means that by acquiring an e-book in proprietary format the customer is locked into a certain platform or with a specific e-book reader and vice versa. Thus, it is only natural that the academic libraries are reluctant to invest in multiple technologies for each e-book. However if content is available only on one specific platform, academic libraries might be obliged to purchase it anyway '_and soon find themselves in the situation of having more platforms than they would have wished_' ([Thompson and Sharp, 2009, p. 138](#tho09)). One has to bear in mind that PDF formats are also often protected by digital rights management.

There is no doubt that the differences in formats are causing a range of various problems not only for e-books users but also for librarians. Nelson ([2008](#nel08)) talks about reduced adoption rates by students and academic staff as well as about learning and usage barriers that the variety of e-book file formats create. Similarly Walters claims that, '_the multiplicity of e-book file formats poses serious difficulties for both cross-platform compatibility and long term access_' ([2013b, p. 203](#wal13b)). Emerging new formats and multiple publisher platforms are also affecting the workflow of libraries, as they have to adjust or create new procedures for handling e-books and staff must handle increasing complexity ([Thompson and Sharp, 2009](#tho09); [Morris and Sibert, 2010](#mor10)). Old formats can, and many have already, become incompatible with existing e-readers or, even worse, would be left abandoned by e-book suppliers as they merge or go out of business and cause major problems for access to and preservation of acquired resources ([Walters, 2013a](#wal13a)).

### Collection development and e-books in academic libraries

According to Tedd ([2005](#ted05)), the development of electronic versions of printed books can be seen as part of the whole e-publishing phenomenon that began in the 1960s. However, most researchers date the first appearance of e-books in academic libraries only to the 1990s ([Armstrong and Lonsdale, 2005](#arm05); [Minci&cacute;-Obradovi&cacute;, 2011](#min11); [Schell, 2010](#sch10)). The years 2007–2009 marked a transition for the higher education e-book market with major growth in both digital textbooks and digital library collections ([Nelson, 2008](#nel08)). E-books have entered the mainstream of book acquisition for major university libraries, especially, when the variety and number of e-book reading devices increased the demand for library e-books from users and for the provision of e-book content by publishers ([Beisler and Kurt, 2012](#bei12)).

There is a lot of research on various aspects of e-books and their collections in academic libraries in recent years. Blummer and Kenton ([2012](#blu12)) provide a review of literature on best practices of managing e-books in academic libraries since 2005\. Other researchers were interested in the usage of e-book collections; different studies analyse the scholarly use of e-books by different users ([Martin and Quan-Haase, 2013](#mar13); [Ahmad and Brogan, 2012](#ahm12); [Cassidy, Martinez and Shen, 2012](#cas12)); and there are studies looking into users' perceptions of e-books as well as the ways e-books are used. This review shows that the experience of academic libraries, in procuring e-books and managing them, have attracted the attention of researchers. Even some of the user and usage studies touch on the multitude of issues that affect e-book acquisition (e.g., [Sheperd and Arteaga, 2014](#she14)). However, there is still very little deeper analysis and research done on collections management and e-book acquisition issues in the academic libraries. Vasileiou, Rowley and Hartley ([2012c](#vas12c)) believe that librarians face challenges and issues with e-book management, collection development and budgeting throughout the whole e-book life-cycle in an academic library, i.e., to the point of cancellation or re-ordering of e-books .

Incorporating a new medium into the library can be a challenging task, which requires the development and implementation of specific policies and well-grounded plans ([Stamison, 2011](#sta11)). Blummer and Kenton ([2012](#blu12)) agree that acquisition plans and strategies enable successful acquisition as well as integration of e-books in library collections. Ideally, collection management issues surrounding the selection, acquisition and promotion of e-books have to be addressed within the collection development policies of an institution. However, a new medium creates different uncertainties that prevent libraries from documenting the new rules, especially, in the area where changes can be quite rapid. The recent study investigating UK academic libraries' experiences and perceptions of e-book management shows that e-book-related policies exist and are included in general collection development policies of the libraries, however separate policies of e-book collection management are still non-existent and not even planned in the surveyed libraries ([Vasileiou _et al._, 2012c](#vas12c)).

#### Barriers to the access and use of academic e-books in academic libraries

Looking at the present book market, it seems that there is no shortage of e-books in any area. Most of the big scholarly and science, technology and medicine publishers list e-books in their catalogues and on their Websites, but trying to find the newest titles as e-books in most of them is quite difficult. Actually, the low availability of scholarly e-books or no availability at all is one of the main issues for librarians building e-collections. E-books have become a regular feature for publishers and vendors of popular titles, and though the amount of scholarly and scientific e-content is growing, the academic market is still supplied rather poorly ([Walters, 2013b](#wal13b)). There are numerous reasons for this explained by other authors, e.g., Dewan ([2012](#dew12)) who also points out that among others this problem becomes one of the biggest barriers to the development of e-book collections in college and research libraries. However, slow growth of the number of academic e-books is only a part of the problem.

##### Embargoes

In many cases there are delays in the release of academic e-books in order to protect print sales because publishers try to protect printed, usually hardcover, production sales, which is the primary revenue driver ([Meehan, 2010](#mee10)). This is observed in the market of the major textbooks where publishers are not hurrying to invest in the development of electronic study materials as noted by [Bennett and Landoni in 2005](#ben05). Hodges, Preston and Hamilton explain how this business model works:

> the longer the hardcover edition is the sole source of content, the more money the publisher makes; after the hardcover sales peak, a paperback edition often has its run; at some point the publisher releases the e-book, first for sale, and later by permission to include the e-book in leased collections” ([Hodges _et al._, 2010, p. 198](#hod10)).

According to some authors, this business model anihilates a major advantage of the e-book in reducing the time from manuscript acceptance to formal public distribution ([Walters, 2012](#wal12)).

The embargo period can vary from three to eighteen months ([Hodges _et al._, 2010](#hod10); [Walters, 2013b](#wal13b)). This causes a major dilemma for acquisition librarians, since not many academic libraries can afford to buy the same content twice. Thus, they have to make a decision on whether to delay the purchase of the title until the e-book is released or buy a paper book instead. In many cases, libraries can afford to wait for an e-book, but when a high profile item is assigned as a reading material or course literature "such a delay is unacceptable" ([Medeiros, 2011, p. 161](#hod10)). Furthermore, if a paperback edition of the book is chosen over an e-book very often, that could put the building of the e-books collection in jeopardy, especially for libraries that favour providing e-book version of a title. In such libraries this could also mean leaving their users without preferred format of the book.

##### Digital rights management (DRM)

Digital rights management is '_access control technology used by copyright holders to limit the use of digital content_' ([Stamison 2011, p. 10](#sta11)). Digital rights management might allow or deny the right to transfer content from one device to another, it also sets the maximum values for printing, copying, sharing and downloading for offline reading. Sometimes digital rights management restrictions are actually contrary to the licence terms and, '_in practice this means, that DRM gives publishers the initial ability to limit use however they choose_' ([Walters 2013b, p. 5](#wal13b)). Not surprisingly, these limits frustrate and even infuriate library users. Joe Wikert, a publisher and an author of the _Publishing 2020_ blog, also shares a very critical view on digital rights management. He argues that it does not eliminate piracy, but because of many restrictions not only makes the e-book less attractive than paper books, but also implies a lack of trust ([Wikert, 2012](#wik12)). Various digital rights management systems are used to protect the copyright of the owner. In exercising this protection, digital rights management is limiting the potential of an e-book not only reducing its possibilities but sometimes making even more limited than a printed book. It also prevents the users of e-book of exercising their rights and especially the fair use principle that allows copying the parts of the text for individual and educational use without infringing the economic rights of the copyright owners. The increasing unpopularity of the digital rights management has lead to the development and use of alternative, less noticeable, protective means, such as watermarking (system tracking a personalized copy ([Srivastava and Sharma, 2012](#sri12)) of e-books.

##### Limitations regarding interlibrary loan

Interlibrary loan, within one country and internationally, is one of the ways to enhance the usage of resources developed by libraries while managing their collections over time . E-books have a lot of potential to ease the process of interlibrary loan. By using an e-book for this traditional function, libraries could save a lot of time and money and also avoid situations where library users would be left without a book for a period of time when it is on loan to another library. Despite these benefits, there are currently many barriers for sharing e-book items that are still under copyright protection. Some e-book licences impose an outright ban on interlibrary loans, while others allow sharing of chapters of books, but also with certain limits and restrictions ([Radnor and Shrauger, 2012](#rad12); [Walters, 2013b](#wal13b)). Additional restrictions include use of a specific platform or a proprietary format that limits e-books use to specific devices. '_As in many other instances, the limitations imposed by vendors negate the advantages that e-books might otherwise provide_' ([Walters, 2013b, p. 6](#wal13b)).

Some investigations show that the complexities regarding communication and limitations involved in sharing e-books are so troublesome that librarians, though they realise and wish to exploit the existing potential, refrain from using e-books in interlibrary loan transactions ([Fredriksen, Cummings, Cummings and Carrroll, 2011](#fre11)). Radnor and Shrauger share the same opinion by illustrating a common situation in libraries that have to deal with e-book requests by taking into account the issues of licencing, platform, additional workflows and time, etc.: '_These barriers to the ILL of ebooks go far beyond the realm of ILL and cause many ILL practitioners simply to respond that ebooks are not available through ILL or to deflect all ILL requests for ebooks automatically_' ([Radnor and Shrauger, 2012, p. 156](#rad12)).

### Acquisition of e-books in academic libraries

Walters ([2013b](#wal13b)) claims that challenges associated with academic e-books are much less known compared to their real and perceived advantages. E-book acquisition in academic libraries is affected by many factors, including access limitations, formats and platforms discussed earlier. Some others, such as, content, e-book features, platforms, access models, arise from the e-books themselves and their suppliers. Others reflect the needs and preferences of the users and the institutions. Demands of certain disciplines or the state of distance education can heavily affect the decisions regarding e-book acquisition ([Blummer and Kenton, 2012](#blu12)). Similar conclusions were drawn by Vasileiou and others ([2012b](#vas12b)) in their research report examining the perceptions and predictions of academic librarians regarding the future role and development of e-books. A very interesting point to note is that according to them, it is a two-way process where the parties interested in e-book acquisitions not only directly influence decisions of academic libraries about e-book collection development but in turn are affected by those decisions. The main sources of e-book acquisition in academic libraries are: 1) publishers who supply e-books directly to libraries, 2) vendors offering contents from different publishers and providing access to e-books through publishers' websites, and 3) aggregators supplying content from different publishers on their own platforms ([Devenney and Chowcat, 2013](#dev13)).

There are many important considerations and criteria for selecting and acquiring e-books in academic libraries. Barriers to access and limitations in use have to be considered among them. But, based on the findings of recent studies, Vasileiou _et al._ ([2012a](#vas12a)) suggest that the most important selection criteria in academic libraries are the cost of e-books, high usage, and demand by the library users, licences, business models, platforms, interfaces, subject coverage, and match to reading lists for studies. However, academic librarians have also to consider the library's budget and the collection development policies as well as many restrictions that various licences bring in. Some of these issues are analysed in more detail in the following paragraphs.

#### Budget and e-book pricing

The difference between considering the price of a printed book and an e-book lies in considering for the latter not only a single purchase cost, but also the continued funding of annual access fees ([Bennett and Landoni, 2005](#ben05)). In addition, there are different ways of acquiring e-books that must be compared in terms of price and cost. Thus, there is no doubt that the budget plays a major role in building the academic library e-books collection. It can be a very challenging task for libraries to decide how to allocate the financial resources if the new collection has to be funded from the existing library budget and how to proceed with its updating. Furthermore, Tedd and Carin ([2012](#ted12)) discovered that librarians with limited budgets are very cautious about acquiring e-books when they do not know that they will be used. In addition, Burnette argues that the budget is even more than just numbers:

> When applied to e-resources, the objective of the budget is more than setting a spending threshold or percentage. E-resource budgeting is an agreement on the financial support allocated for digital collections within a fiscal year. It is also a philosophical agreement to use the staff and information technology required to provide access. The budget is the starting point of a plan for successful access to and sound stewardship of digital collections ([Burnette, 2008, p. 4](#bur08)).

Recent years have seen a significant decrease in overall library budget and a diminishing ratio for books, but also an increase in budgetary allocations for electronic resources, with many libraries assigning more than half their budgets to e-content, though largest part of it goes to e-journals ([Blummer and Kenton, 2012](#blu12); [Minci&cacute;-Obradovi&cacute;, 2011](#min11); [Vasileiou _et al._, 2012c](#vas12c)). Schell suggests that the academic libraries starting an e-book collection should have at least 10% of their total collection budget working up to 25% as users demand grows ([Schell, 2010](#sch10)). Even though budgetary spending on e-books is increasing, academic libraries do not seem to have a separate budget for e-books acquisition but acquire individual e-book titles or collections from the part of the budget allocated to different subject collections or genreal e-resources ([Vasileiou _et al._, 2012c](#vas12c)).

Various studies have shown the importance of sensible charging and price regime for e-books for librarians when making decisions to acquire e-books and explained the reluctance of academic libraries to invest in them ([Blummer and Kenton, 2012](#blu12); [Mulvihill, 2011](#mul11); [Vasileiou _et al._, 2012a](#vas12a); [Vasileiou _et al._, 2012b](#vas12b)). Lippincott _et al._ ([2012](#lip12)) stress the importance of sustainable, affordable pricing models for e-books for academic libraries that librarians have to develop from available opportunities. The authors suggest that in order to achieve that,

> they must critically examine costs for single institution purchases and sharing between institutions and consider how existing options – like multipliers over list price for a consortial purchase, multi-user and single-user costs, tiered pricing, and approval plan discounts – can evolve to the mutual benefit of librarians, publishers, and vendors” ([Lippincott _et al._, 2012, p. 6](#lip12)).

Even though many believe that digital textbooks are the solution to textbook affordability and accessibility on college campuses, digital editions can be as costly and can also affect the accessibility for individuals with various disabilities, which in return would create some legal and educational challenges for institutions ([Nelson and Hains, 2010](#nel10)). According to recent studies, e-books often cost more than their print counterparts. When academic books are purchased individually their price can be even 50% higher than their print equivalents ([Walters, 2012](#wal12)). They also require additional costs involved for providing online hosting content, server and network maintenance fees, reformatting for multiple platforms of file formats, as well as providing customer service and technical support ([Vasileiou _et al._, 2012a](#vas12a); [Walters, 2013b](#wal13b)). Walters ([2013b](#wal13b)) argues that, even if e-books were less expensive to produce, it would not necessarily reduce the prices paid by libraries.

#### Business models

A number of business models are available to academic libraries in terms of acquiring e-books for their collections. These models vary from vendor to vendor. This variety would suggest that academic libraries are well cared for by e-book suppliers, but, according to Vassileiou _et al._ '_[t]here is a clear indication in the literature that the variety of e-book business models provided by vendors is an issue for librarians who find them complicated_' ([Vasileiou _et al._, 2012a, p. 23](#vas12a)). Emerging new purchasing models might even make the situation worse, even though publishers are trying to be helpful in offering options suitable for different libraries. According to Minci&cacute;-Obradovi&cacute; ([2011](#min11)) the choice of a model primarily depends on the content needs of the institutions. We will try to introduce here an overview of business models that are in use by most of the libraries and highlight their advantages and shortcomings.

##### Subscription and renting

The subscription model requires payment of a subscription (usually annual) for access to a collection or subject areas of a collection offered by a vendor or an aggregator. It may also involve a pay-per-view payment in addition to a small initial access fee allowing access to the whole collection. This alternative is also called a rental model allowing for short-term access to individual titles and is sometimes used as an alternative to interlibrary lending.

The subscription model is similar to the offers of e-journals and can be negotiated through library consortia, especially, if both services are offered by the same actor. Subscription is one of the cheapest ways to get access to large collections of academic e-books. However, it is not always the most attractive model as librarians have no possibility to decide what should be available in the package and it may include materials that do not meet the needs of a higher education institution. In addition '_many vendors and publishers reserve the right to add or withdraw titles from the collection during the subscription period_ ([Walters 2013a, p. 196](#wal13a)).

##### Patron driven acquisiton (PDA) and evidence-based selection (EBS)

Patron driven acquisiton seems to be one of the most discussed purchasing models in recent literature. It is also included in the article listing 2012 top ten trends in academic libraries, which identifies this model as "an inevitable trend for libraries under pressure to prove that their expenditures are in line with their value" ([Anonymous, 2012, p. 314](#ano12)). This model has several modifications, but all of them employ the same access to a certain number of books, which are bought after the first use or loaned to the library for a certain sum. Criteria are set at the start of a project. Most international suppliers offer some type of the model, which requires a library to select a set of titles matching certain criteria (e.g. subject, publisher, price) from a vendor or aggregator's collection. Bibliographic records for selected titles are loaded into the catalogue where they can be discovered by users. The first use of a title results in a short time lease and triggers the payment of a small sum agreed between library and e-book provider (e.g., a certain percentage of the e-book price). The second (or third) use of the same title will trigger a purchase from the library into the ownership. The library receives notification after the e-book was accessed for the first time and may withdraw it from the collection. New titles can be added to the corpus periodically ([Medeiros, 2011, p. 160](#med11)). This model enables a library to acquire e-books requested by their users. Studies show higher circulation rates of the items acquired through this model than those acquired by library decisions (e.g. [Tyler, Falci, Melvin, Epp and Kreps, 2013](#tyl13)), i.e. e-books are used more by other library users as well.

On the other hand, Fischer, Wright, Clatanoff, Barton and Shreeves rise a valid question regarding budgeting and patron driven acquisition about responsible budgeting for selection decisions made by an unidentified subset of a high number of potential users ([Fischer _et al._, 2012, p. 490](#fis12)). One of the main concerns academic librarians have regarding this model is a fear of building an unbalanced and lower in quality collections ([Bucknell, 2012](#buc12); [Fischer _et al._, 2012](#fis12); [Walters, 2012](#wal12)).

Evidence-based selection is one of the newer purchasing models and, compared to patron driven acquisition, is still very little discussed in the literature. However, Bucknell provides a detailed explanation of how this model works:

> Under this model, the library pays a relatively modest up-front fee in order to be able to access a collection for a year. Towards the end of that year, the library can evaluate its usage reports to decide which titles or collections to retain permanent access to, with a total value up to the fee already paid. At that point access to the non-retained titles is lost unless the library and vendor agree another year of EBS ([Bucknel, 2012, p. 58](#buc12)).

The author argues that this model is beneficial for both libraries and vendors. For the libraries this model reduces the risk of purchasing a package which turns out to be little used and allows the library to evaluate its purchasing strategy. While vendors might be attracted to it because it gives a minimum guaranteed level of income and may also lead to larger purchase that a library would not have been prepared to make without evidence of use.

##### Buying of individual titles and collections for perpetual ownership

Vassileiou _et al._ ([2012a](#vas12a)) report that the most popular business model among acqusition librarians is purchase with perpetual access. The perpetual ownership model enables a library to buy an individual title or collections of e-books directly from a publisher by paying up front or over a couple of years. Having a perpetual access to those e-books still requires payment of a maintenance fee for ongoing hosting of purchased material on a publisher's platform ([Morris and Sibert, 2010](#mor10); [Vasileiou _et al._, 2012a](#vas12a)).

Academic libraries have usually an option either to acquire e-books as individual titles or e-book packages. In both cases, permanent ownership would mean the removal of most digital rights management restrictions on the use of purchased items. However, sometimes, a credit system is attached to purchased items allowing to loan the book a certain number of times over a certain period, such as a year. When the credits are consumed a library may buy another copy or wait for the new period when the credits are restored to the already purchased copy.

One of the downsides of acquiring e-books as packages is that academic librarians can rarely choose which titles are included in the packages. On the positive side, e-book packages are offered for much more attractive price than individual titles and can also save time in selection, acquisition and processing ([Ashcroft, 2011](#ash11)).

Acquiring e-books as individual titles is very time consuming, as the acqusition librarian has to find out whether a title is available as an e-book, on which platform(s), and how much it costs if acquired through different models. According to Vasileiou _et al._ '_title by title selection facilitates better use of funds and provides maximum control over the acquisition decisions by preventing acquisition of lower-quality or out-of-scope material_' ([Vasileiou _et al._, 2012a, p. 25](#vas12a)). Mulvihill states that '_most librarians want e-books to behave like print books, which are comparatively simple to read and buy_' and will offer the same advantages for collection development ([Mulvihill, 2011, p. 13](#mul11)). That statement explains the wish of librarians to acquire e-books in permanent ownership.

Thus, none of the existing business models for e-book acquisition is ideal for academic libraries and, in any case, the power over the use and management of the books remains with the publisher. In every case, even when the books are purchased for permanent ownership, the threat remains of losing access to e-books because the collection is abandoned by its holder (a publisher, a vendor or an aggregator) for various reasons, or because of changing conditions of access. There are no permanent digital preservation solutions built into any of the publishers' platforms, which forms additional problem in handling these resources by libraries.

#### E-book licensing

One of the main factors that changes the way librarians work with e-books as opposed to print books is that e-books are controlled not solely by a copyright law, but require application of a contractual law through licencing ([Müller, 2012](#mul12)). However these licences are a huge burden for academic libraries. The problem is well summarized by the Commission of European Communities:

> Libraries and universities underline the complexity and fragmentation of the current system of licensing agreements with publishers. A typical European university is required to sign a hundred or more licenses governing the use of digital research material supplied by various publishers. Examining what each of these individual licenses permit with respect to e.g. access, printing, storage and copyright is a cumbersome process (as cited in [Müller, 2012, p. 153](#mul12)).

Since most of the e-books in academic libraries are leased and not purchased it is a major adjustment for libraries to realise that they no longer own the collection, and even more, depending on the licence, the libraries might still have to pay recurring payments for access to a set of titles that does not improve over time ([Walters, 2013b](#wal13b)).

The publishers and aggregators usually have a variety of licensing models, which control how users may access the e-books. Moreover, Walters ([2012](#wal12)) argues that most e-book vendors have adopted licensing models that prevent users from taking advantages of the benefits that e-book technology might otherwise provide. Thus, there are many considerations that academic libraries have to take into account with respect to licensing e-books. The need for librarians to become familiar with licensing issues and terminology and be able to identify limitations and restrictions inherent in licence agreements were also emphasised by Vassileiou _et al._ ([2012a](#vas12a)). Negotiating the license terms through the consortia might be an option for academic libraries. The recent study suggests that the experienced team can achieve better deals, reduced effort in negotiating separately with each publisher, as well as discounts on platform hosting fees ([Vasileiou _et al._, 2012c](#vas12c)). Moreover, sharing books across library consortia reduce the burden of training local staff with new skill sets and also reduce the work involved in cataloguing since all these things are done centrally by the leading facility in consortium ([Schell, 2010](#sch10)).

Definition of an authorized user, interlibrary loan restrictions, fair use application, use for scholarly sharing, IP authentication issues, incorporation of e-book content into virtual learning environments are just few of many other issues to consider ([Stamison, 2011](#sta11); [Vasileiou _et al._, 2012a](#vas12a)).

Since libraries acquire e-books from a number of different suppliers that means they have to cope with different licensing models and with different terms. Tedd ([2005](#ted05)) stresses the importance of making users aware of these differences as they will be the ones feeling their impact. Thompson and Sharp ([2009](#tho09)) conducted a session with library staff, which revealed that very few librarians felt that their library was achieving that successfully.

Briefly summarizing the first part of this paper, it is possible to argue that, though the main principles of acquisition have not changed with the advent of e-books, the conditions of their purchase, maintenance, access and use for libraries and library users have become rather complicated, though advantages seem to be more attractive and outweigh the complications.

## E-books in Swedish university libraries

Quite a lot is written about e-books in Swedish libraries and a passionate public debate rages on the issue of e-books availability and business models for access. Most of it relates to public libraries and the mass consumer market. The issues of academic libraries are not considered very often or extensively. The differences of the academic, professional and teaching book (and e-book) market is only fleetingly mentioned in the official reports by the Swedish Government ([Sweden. _Ministry of Culture_, 2012, p. 228](#swe12)) and the Swedish Parliament ([Sweden. _Parliament_, 2013/14:RFR3, p. 14](#swe13)). Partly it is explained by Svedjedal ([2012](#sve12)) in his report to the Swedish Publishers Association, where a short chapter (p. 34-36) explains the other world of academic publishing and university libraries, pointing out the dominance of international providers of e-resources in university libraries and the realities of e-publishing in the scholarly communication field (e-journals and e-monographs). In 2011 the National Library of Sweden produced a report mapping the problems related to the introduction of e-books into the library activities in the country ([Kungliga Biblioteket, 2011](#kun11)). The report stated that there was no problem with the acquisition and provision of e-books in academic libraries: by then most university libraries in Sweden had already accepted the policy that, if there is a choice between a printed book and an e-book, they will buy an e-book (especially books used in teaching) ([Gustaf, 2010](#gus10)). It seems that the main research into e-book issues in Swedish academic libraries is conducted by students writing their graduation theses in the departments of library and information science around the country. Some librarians are also conducting small investigations in their libraries. Byström ([2012](#bys12)) has compared statistics of different e-book packages from different suppliers in the Library of Uppsala University and arrived to the conclusion that these are misleading and incomparable. According to her the lack of comparable statistics does not allow acquisition librarians to make sound decisions in the access renewal process, makes it impossible to justify the purchases or to evaluate the cost-benefit of use ([Byström, 2012, p. 220](#bys12)).

One of the articles about situation in Swedish academic libraries discusses the infrastructure for the management of e-books, namely, the role of link resolver SFX purchased by a consortium of Swedish academic libraries (BIBSAM) in 2004 and its use in union and library catalogues ([Söderback, 2011](#sod11)). The article also mentions that the consortium buys e-journals for Swedish libraries, but leaves handling of e-books to the local libraries. Statistics for 2009 show that 5,598,777 e-books were down-loaded from universities or university colleges; the author also notes that '_discussions with libraries suggest that e-books take up no more than 6% of money spent on acquisitions. From this perspective, e-books are highly used and seem to be a good investment for academic libraries_' ([Söderback, 2011, p. 38](#sod11)).

Despite this, e-books are so far significantly outnumbered by the collections of printed books in Swedish university libraries. But if we look at newly bought books in 2012 we see that the situation looks entirely different throughout the country (see Fig. 1) and in separate universities: 70% (overall) to 88% (in some of the universities) of all new acquisitions consist of e-books ([Maceviciute and Borg, 2013, p. 16](#mac13)).

<figure>

![E-books in academic library collections and acquisition in 2012](../p620fig1.png)

<figcaption>

Figure 1: E-books in academic library collections and acquisition in 2012 (Source: [Borg, 2013](#bor13). Figure derived from data in Kungliga Biblioteket ([2012](#kun12)))</figcaption>

</figure>

The trend also coincides with the investments that Swedish academic libraries have made recently according to the accepted policy.

We will illustrate the issues of e-books acquisitions in Swedish academic libraries by presenting two short case studies that, in principle, should be different as one represents a medium-sized university with a smaller number of students and staff and a relatively simple management structure, while the other relates to an old big research university with many employees of different types, large numbers of students, and complex structures of governance and work of the academic units and of its library.

### Two case studies: a middle-sized regional and a big research university

#### Case study 1

The regional university in case study 1 (hereafter, U1) made an especially big, recent investment to acquire e-books. U1 Library is operating under the increasing stress of diminishing budgets. In 2013 the budget was severely cut in comparison with earlier years. The library's goals are to support the main goals of the university in increasing external funding for research, supporting students' learning, aiding relations with professional partners through provision of information resources, consultancy, and training.

At present, U1 Library's collection of e-books is as big as the stock of printed materials (108.456 vs 114,313), but acquisition of e-books is far larger than that of printed books. Use and loans of e-books is also on the way to exceed the loans of printed books. With regard to the acquisition policy approved by the U1 Library the preference in acquisition is given to an e-book if there is one. Eighty percent of the whole media budget goes to electronic material: e-books, electronic journals, and databases.

The U1 Library has decided to be open to most of the e-book acquisition business models during this first phase of development of e-book collections. It expects to be able to choose one or several most appropriate models.

#### Case study 2

A research university in case study 2 (hereafter, U2) has a long history and is one of the top-ranked Swedish research universities with strong international reputation. Its structural and managerial complexity is reflected in the complexity of U2 Library that consists of several large departments, including over 20 organizational units, most of which also function as rather autonomous libraries.

The libraries' collection contain millions of physical and digital items. The library has access to approximately 400,000 e-books, which is only a small part of its overall collection of printed books and also a small part of its digital resources which is dominated by full-text journals and bibliographic databases.

This library experiences problems of communicating with hugely dispersed university staff, especially researchers, who prefer using library resources remotely: but maintains more contact with university students in local library departments and units (branch libraries).

<table><caption>Table 1: Summaries of case studies</caption>

<tbody>

<tr>

<th>U1 Library</th>

<th>U2 Library</th>

</tr>

<tr>

<th colspan="2">Acquisition policy</th>

</tr>

<tr>

<td>

E-books are included in the general policy and made a priority in acquisition to meet students' expectations to get access to digital resources.  
The e-books are commercially acquired.  
Other types of digital documents (digitized older books by the library or other agents) are not regarded as e-books.</td>

<td>

E-books are not mentioned anywhere in the overall policy, but digital resources should be given priority in acquisition when this decision can be argued well.  
E-books are characterized mainly as purchased digital objects used similarly as printed books and with the same purpose.  
There is a clear distinction between e-books and other digital materials.</td>

</tr>

<tr>

<th colspan="2">Acquisition sources</th>

</tr>

<tr>

<td>

There are fifteen main sources for e-book acquisition:

1.  aggregators: Ebrary, EBSCO, Dawsonera, Safari Tech Books online etc.
2.  publishers: Chandos, Emerald, Elsevier, Oxford Publishing Online, Royal Society of Chemistry, OECD iLibrary, SpringerLink, Wiley, Woodhead Publishing Online;
3.  vendors: Gale, Elib for Swedish language books.
4.  Providers of free e-books: Alex Catalogue, Project Gutenberg, Project Runeberg.
5.  Ten subject encyclopedias online.

</td>

<td>

There are thirty-eight main sources for e-book acquisition:

1.  aggregators: DawsonEra, Ebrary, Ebsco, Knovel, OECD, Safari.
2.  publishers: ASTM, Cambridge Online, Emerald, Elsevier, Grypho Editions, IEEE, IMF, McGraw Hill, MedicinesComplete, Morgan and Claypool, Oxford Online, Sage, Spie, Springer, Thieme, World Bank.
3.  vendors: Early European books, EECO, EEBO, Gale, PsycBooks.

</td>

</tr>

<tr>

<th colspan="2">Collection size and structure</th>

</tr>

<tr>

<td>

Printed materials: 114,313  
E-journals: 36,900  
E-books: 108,456  
Databases (commercial): 86  
Inter-library loans to the Library: 1,249</td>

<td>

Printed materials: 'several millions'  
E-journals: no data  
E-books: 400,000  
Databases: more than 500 (including bibliographic, factual, full text, encyclopedic, e-books, etc.)</td>

</tr>

<tr>

<th colspan="2">Business models</th>

</tr>

<tr>

<td>

**_Subscription_**: Ebrary Academic complete with over 80,000 titles and Safari Tech Books online (Current file) with almost 7,000 titles.  
_Advantages_:  
relatively cheap, access to a large number of items. Most of the books can be downloaded for off-line reading on e-readers or tablet computers.  
_Shortcomings_:  
DRM systems impose limits on the printing and copying of the material due to copyright issues, and on loan times. Limitations exist on how many days a reader can have a book.  
Books can disappear from the collections without notification.</td>

<td>

**_Subscription_**: U2 Library maintains a significant number of subscriptions to packages. They are subscribed to by the central service of the Library.  
_Advantages_:  
a large number of high quality academic books available to the users.  
Many packages offered by different sellers are suitable for different disciplines and researchers.  
_Shortcomings_:  
Each package comes with different rules and conditions of use that must be communicated to users.</td>

</tr>

<tr>

<td>

**_PDA or EBS_**: Library gives a fixed sum of money to the provider Ebrary and selects a number of e-book collections. The system is configured to produce a short-time loan. When the book is used for the second time, it is purchased by the Library. When a book is used for the first time, the Library gets a message and can decide if that book should stay in the PDA collection.  
_Advantages_:  
For PDA, first use is cheap (10-15% of the licensing price) and while the usage was 2% of the Ebrary collection (3000 books) all seemed well.  
The books are bought purposefully and for existing need.  
For EBS provided by Elsevier there is no need to make a decision about buying and the selection is possible after assessing the usage data.  
_Shortcomings_:  
The first use is for seven days only.  
When the use increases, expenditure increases and it is impossible to plan the budget. Requires constant monitoring of situation with access to e-books in the collection. In 2014 funding for PDA was consumed in one month.  
EBS does not have this disadvantage.</td>

<td>

**_PDA or EBS_**: Librarians take into account the suggestions and requests from the staff and students and try to acquire a requested book, but do not consider buying an e-book as it will be too expensive. They try to find the requested title in bought packages.  
PDA tried by the U2 Library in 2012 with the three first uses paid as loans and, in the case of the fourth, the Library buys the e-book.  
_Advantages_:  
Bought after the actual use by patrons.  
Good way to buy more books and save resources in manual buying.  
_Shortcomings_:  
Difficult to keep within the framework of funding.  
Unpredictable in quality and relevance of titles offered and selected.  
Selection can be skewed if one user is particularly active.</td>

</tr>

<tr>

<td>

**_Buying Pick-and-Choose model_**: Buying from aggregators Ebrary, DawsonEra and Ebsco.  
_Advantages_:  
Library owns the book.  
Easy to satisfy purchase requests.  
Unlimited number of users is the best option.  
Dawsonera introduced credits for how many times a year a book can be used (from 125 to 400).  
Books available for download and reading off-line.  
_Shortcomings_:  
Limitations on number of copies and printouts, number of simultaneous readers (from one to unlimited).  
Items for unlimited number of users cost 1.5 times more than a printed book.  
Credit system is limiting and can change for popular items.</td>

<td>

**_Buying from vendors_** is the responsibility of departmental librarians. EBL and DawsonEra dominate the list of providers of e-books.  
_Advantages_:  
Buying from a limited number of vendors keeps down the number of e-book platforms in use.  
_Shortcomings_:  
The vendor sets the rules of access and usage.  
A librarian has no opportunity to shop around for the best price.  
Impossible to buy Swedish books, even if they exist, from international vendors.  
E-books often can be much more expensive than printed versions. The price matters in making a final decision on which to buy.  
Newest titles are not always available as e-books.</td>

</tr>

<tr>

<td>

**_Buying e-books from publishers._** Elsevier, Springer, Wiley-Blackwell, Woodhead.  
_Advantages_:  
Little limitations and restrictions on use.  
Discounts for several collections can be negotiated.  
_Shortcomings_:  
More expensive than printed books and subscriptions to e-books.  
Buying individual titles is very time consuming for librarians.</td>

<td>

**_Buying from publishers_** is a rare occasion in the U2 Library.  
_Advantages_:  
Library can get 'best DRM' in terms of least restrictions to use, choose the most liberal publisher (e.g., Springer).  
_Shortcomings_:  
Different publishers have different platforms and buying from many will increase complexity for librarians and readers.</td>

</tr>

<tr>

<td>

**_Books in Swedish_** available only from Elib service owned by four biggest publishers. Each loan costs the Library 20 SEK (˜2.25€).  
_Advantages_:  
Has some non-fiction books in Swedish.  
Helps to promote leisure reading to students and staff.  
_Shortcomings_:  
Difficult to plan the costs.  
Amount of non-fiction is very limited.  
Books can be taken out of the collection, but it is anounced beforehand.</td>

<td>

**_Books in Swedish_** not available as e-books. Rough estimate of the respondents in the study was that 98%–100% of e-book collection is not only in foreign but in the English language with some inclusions of French of Norwegian e-books. Even Swedish printed books in some of the U2 Library units and departments can be almost non-existent, depending on the nature of served disciplines. Other disciplines (e.g., nursing) experience severe lack of Swedeish textbooks in e-book formats. Swedish books can be purchased from Dawson or Adlibris when necessary.</td>

</tr>

<tr>

<th colspan="2">Budget and buying process</th>

</tr>

<tr>

<td>

**_Budgets._** Library budget is approved by the University Library. The head of the Library decides how finances will be used, including acquisition and media management.  
**_Negotiations_** are carried by a librarian specialising in e-books with the representatives of vendors who usually arrive to the university library after a public tender is announced.  
**_Licences and contracts_** are signed by the head of the library or the Rector in cases of very large payments. Licences are archived in the university Registry.</td>

<td>

**_Budget_** for library acquisitions is seen as exceptionally high and university members have difficulties in understanding the costs of the Library resources. Rector allocates Library budget within the U2 budget.  
**_Negotiations_** carried out by a special teams for big packages. Unit librarians put in suggestions, mainly, asking to look for the 'best DRM'. Most of the digital resources are acquired through BIBSAM consortium.</td>

</tr>

<tr>

<th colspan="2">Consortium</th>

</tr>

<tr>

<td>The consortium (BIBSAM) cannot sign contracts with aggregators, but gets a good price from some publishers who are selling through Consortium (e.g., Emerald, Royal Society of Chemistry), especially when collections are bought or negotiations include other products.</td>

<td>U2 Library subscribes to most of the digital resources purchased by the BIBSAM Consortium, including the e-book packages.</td>

</tr>

<tr>

<th colspan="2">Platforms and formats</th>

</tr>

<tr>

<td>

Approximately ten platforms are in use by a library.  
Main formats are Epub and PDF.  
Different vendors allow users to read books using different systems: AdobeReader, Bluefire, etc.</td>

<td>Each collection has a different platform and interface. Even with introduction of a simple search function over the catalogues, the users are confused by different conditions imposed by providers.</td>

</tr>

<tr>

<th colspan="2">General advantages and benefits of e-books</th>

</tr>

<tr>

<td>

_For readers_  
Many readers can use the same text simultaneously.  
Accessible from anywhere and any time for many at the same time.  
Readers can have many books without carrying a load, read them anywhere.  
_For libraries_  
Saving time in processing, cataloguing and providing access phase.  
E-books do not occupy physical space.  
No need to send parcels of books from one library to another.</td>

<td>

_For readers_ A large amount of academic books that are quite new. High level academic books.  
Can take the books anywhere they wish.  
_For libraries_  
E-books do not occupy physical space and leave place for quite reading rooms.</td>

</tr>

<tr>

<th colspan="2">General barriers and problems of e-books</th>

</tr>

<tr>

<td>

_For readers_  
All DRM limitations.  
Multiple interfaces (approximately ten) of multiple platforms have to be learned.  
Different reading software for e-books of different vendors required.  
_For libraries_  
It is always a publisher who decides the conditions of use. Library has no power over it and in certain cases has to impose its own limitations to access.  
Finding existing e-books, comparing availability, prices, conditions, making acquisition decisions of a selected title takes much time.  
Even in case of a bought and owned book, it is accessible only through a publisher's platform (though limitations are removed).  
Publishers do not allow libraries to use e-books for interlibrary loans, though sending chapters is allowed in many cases.  
DRM and other systems limit possibilities of use.  
Librarians have to learn all the platforms in use by a library.</td>

<td>

_For readers_  
Different platforms for each collection detract from e-book use.  
DRM limitations.  
Users are not used to using e-books and will buy a print book of their own if the Library acquires only an e-book version.  
Small screens of reading devices and mobile phones make it difficult to read e-books.  
Use is restricted to the members of U2.  
_For libraries_  
Librarians assess the multiple platforms as means of branding and marketing publishers.  
Suppliers of e-books have full power on setting the restrictions of use (number of multiple users, downloading, printing), Library has no influence over them.  
Users have limited awareness of the e-book collections and the conditions of their use.  
Impossible to assess if the e-books are used and how much because of usage statistics.  
Changing conditions of use for acquired titles, especially the ones used as reference titles.  
Impossibility to use e-books for interlibrary loans.</td>

</tr>

<tr>

<th colspan="2">Access, bibliographic and usage control</th>

</tr>

<tr>

<td>Using a knowledge base, links resolver, and discovery tool it is easy to choose the platform containing the necessary e-book, find the most effective work-flows and record the data on the item to catalogue by one click.</td>

<td>The titles from purchased or subscribed to packages reach catalogue through a link resolver. The metadata on those items is very poor, though improving slightly over the years. Individual titles are catalogued by purchasing librarians.</td>

</tr>

</tbody>

</table>

## Conclusion

Both Swedish university libraries, regardless of their size and other characteristics face the same challenges in e-book acquisition that are described in international research literature. It is very likely that other problems of e-book management, preservation and usage also would be similar all over the world. On the other hand, e-book use in higher education institutions in Sweden is becoming more popular, the demand for them is growing and e-books bring certain advantages both to libraries and their users. The advantages for libraries and their users seem mainly to include the unlimited access to multiple readers from any place and at any time, save the space in storage, make texts easily transportable by users and from one library to another.

Practically all suppliers mentioned in the documentation of both libraries provide books in English. As it is the main language used in scholarly communication and spreading to higher education reading materials, the needs of university library users are readily satisfied with regard to the content. This situation is not without problems, however. The providers of the e-books set the conditions of their use and the librarians have lost most of their freedom of managing them in comparison with printed books. There are significant barriers to cataloguing e-books and marketing them efficiently related to multiple platforms and formats as each supplier uses ones own platform. The discovery tools have to be specifically fitted to be able to access e-book collections. The Library has to train a number of librarians as experts in separate e-book platforms as each provider has its own platform and often use proprietory formats. Librarians have to organize workshops and training for the users of e-books for the same reason. However, multiple platforms may be seen as means of branding and marketing of publishers, therefore, promotion of the e-books and trainings can loose their effectiveness.

Swedish language books for academia are not yet available in e-book formats, except for some titles that can be acquired through Elib or Adlibris. The respondents also mentioned one international supplier as holding some Swedish titles. The problem is the most acute in the disciplines that use Swedish textbooks extensively (e.g. nursing). Swedish publishers are not willing to provide their digital versions fearing that library loans of e-textbooks might affect their revenues. Librarians feel clear need in having more e-textbooks in Swedish language in some disciplines.

It also seems that both groups of actors involved expect e-books to function as printed books and they express their dissatisfaction to each other in these terms.

Publishers try to suppress the radical potential of e-books ([Winston, 1998](#win98)) by placing digital rights management and other restrictive systems on them, annihilating the basic advantages of e-books or at least pricing them significantly higher than a printed book. For this group, an e-book is a threat to a traditional revenue model through selling physical copies of the same title to users. An e-book in principle abolishes the concept of one or many copies of the same title and may be seen as a single source for multiple digital replicas that can be produced and distributed without much effort or cost.

Librarians expect to have possibilities to exploit and manage e-books as they used to do with physical books, selecting particular items, fostering digital collections and growing them over time to meet and reflect changing demands of the communities they serve. The loss of control over these essential functions makes them feel powerless and incapable of conducting an essential part of the library mission. This helplessness prevents libraries from exploring new possibilities and untapped potential of academic e-books. It seems that an e-book as an innovation is not fitting organically into traditional market structures, but has to be forced into them to function as a market product or service. No one of the participants has mentioned high VAT on e-books. This is not important for Swedish academic libraries, which are the state agencies. However, the VAT issue is worth exploring further. It seems that with the higher VAT, e-books are on the governmental level officially reduced from a cultural and knowledge good to a commercial product by taxing it at the same rate as any mass consumption item and stripping them of other privileges enjoyed by the authors and produces of printed books. This reduction has a detrimental impact on the radical potential of e-books and stalls the spread of e-books in society.

## Acknowledgements

The authors acknowledge the support of Swedish Research Counsil, which finances the project _The impact of the e-book on 'small language' culture: media technology and the digitalsociety_ and of two university libraries and their staff who generously supported the case studies by their time and enthusiasm. We also thank the reviewers and our copy editor for their valuable input to the text.

## <a id="author"></a>About the authors

**Elena Maceviciute** is a Professor in the Swedish School of Librarianship and Information Science, University of Borås. She is also Professor in the Faculty of Communication, Vilnius University, Lithuania. Her research relates to information use in organization, digital libraries and resources, and, currently, the role of e-books modern society. She can be contacted at: [elena.maceviciute@gmail.com](mailto:elena.maceviciute@gmail.com)  
**Martin Borg** is a librarian at the Library and Learning Resources of the University of Borås. He has rich experience in working with digital resources and has conducted a number of projects related to library work for his library and Swedish Library Association. They were presented in professional conferences and library press. He can be contacted at: [martin.borg@hb.se](mailto:martin.borg@hb.se)  
**Ramune Kuzminiene** has just graduated with a Masters degree in Digital Library and Information Science from the University of Borås. She is working at the Dublin City University Library as an assistant librarian. She can be contacted at [ramune.kuzminiene@gmail.com](mailto:ramune.kuzminiene@gmail.com)  
**Katie Konrad** has graduated the Master's Programme in Digital Library and Information Sciences at the University of Borås. She is educated as a political scientist and digital services librarian and has experience working in a number of Swedish and American libraries and educational institutions. She can be contacted at: [konrad.katie@gmail.com](mailto:konrad.katie@gmail.com)

</section>

<section>

## References

<ul>
<li id="ahm12">Ahman, P. &amp;Brogan, M. (2012). Scholarly use of e-books in a virtual academic environment: a case study. <em>Australian Academic &amp; Research Libraries 43</em>(3), 189-213.
</li>
<li id="ano12">Anonymous. (2012). 2012 top ten trends in academic libraries: a review of the trends and issues affecting academic libraries in higher education. <em>College &amp; Research Libraries News, 73</em>(6), 311-320.
</li>
<li id="arm08">Armstrong, C. (2008). Books in a virtual world: the evolution of the e-book and its lexicon. <em>Journal of Librarianship and Information Science 40</em>(3), 193-206.
</li>
<li id="arm14">Armstrong, C. &amp; Lonsdale, R. (2014). <a href="http://www.webcitation.org/6PIPYQjg6">E-books basics.</a> London: UKeiG: UK einformation Group. Retrieved from http://www.ukeig.org.uk/factsheet/e-book-basics. (Archived by WebCite® at http://www.webcitation.org/6PIPYQjg6)
</li>
<li id="arm05">Armstrong, C. &amp; Lonsdale, R. (2005). Challenges in managing e-books collections in UK academic libraries. <em>Library Collections, Aquisitions and Technical Services, 29</em>(1), 33-50.
</li>
<li id="ash11">Ashcroft, L. (2011). Ebooks in libraries: an overview of the current situation. <em>Library Management, 32</em>(6/7), 398-407.
</li>
<li id="bei12">Beisler, A. &amp; Kurt, L. (2012). E-book workflow from inquiry to access: facing challenges to implementing e-book access at the University of Nevada, Reno. <em>Collaborative Librarianship, 4</em>(3), 96-116.
</li>
<li id="ben05">Bennett, L. &amp; Landoni, M. (2005). E-books in academic libraries. <em>The Electronic Library, 23</em>(1), 9-16.
</li>
<li id="ber14">Bergström A., Höglund, L., Maceviciute, E., Nilson, K. &amp;Wilson, T.D. (2014). The case of the e-book in a 'small language' culture: media technology and the digital society. <em>Knygotyra, 62</em>. (in press)
</li>
<li id="ber13">Bernhardsson, K., Eriksson, J., Henning, K., Lindelöw, C.H., Lawrence, D., Neidenmark, T., … Svensson, A. (2013). <a href="http://www.webcitation.org/6PSnexIKt"><em>Towards quality-controlled open access monographs in Sweden. An investigation funded by National Library of Sweden, Swedish Research Council and Riksbankens Jubileumsfond. Final report.</em></a> Stockholm: National Library of Sweden. Retrieved from http://bit.ly/Tp00u0 (Archived by WebCite® at http://www.webcitation.org/6PSnexIKt)
</li>
<li id="blu12">Blummer, B. &amp; Kenton, J. (2012). Best practices for integrating e-books in academic libraries: a literature review from 2005 to present. <em>Collection Management, 37</em>(2), 65- 97.
</li>
<li id="bor13">Borg, M. (2013). <em>E-books in Sweden.</em> Borås, Sweden: University of Borås. (PowerPoint presentation).
</li>
<li id="buc12">Bucknell, T. (2012). Buying by the bucketful: a comparative study of e-book acquisition strategies. <em>Insight: the UKSG journal, 25</em>(1), 51-60.
</li>
<li id="bur08">Burnette, E. S. (2008). Budgeting and acquisitions. In M. D. D. Collins &amp; P. L. Carr (Eds.), <em>Managing the transition from print to electronic journals and resources</em> (pp. 3-28). Oxford: Routledge.
</li>
<li id="bys12">Byström, K. (2012). <a href="http://www.webcitation.org/6PKBMVcqH">Everything that's wrong with e-book statistics: a comparison of e-book packages</a>. In Bernhard, B.R., Hinds, L.H. and Strauch K.P. (Eds), <em>Accentuate the positive: Charleston Conference Proceedings 2012</em> (pp. 216-220), West Lafayete, IN: Purdue University Press. Retrieved from http://docs.lib.purdue.edu/cgi/viewcontent.cgi?article=1362&amp;context=charleston (Archived by WebCite® at http://www.webcitation.org/6PKBMVcqH)
</li>
<li id="cas12">Cassidy, E.D., Martinez M. &amp; Shen, L. (2012). Not in love or not in the know? Graduate student and faculty use (and non-use) of e-books. <em>The Journal of Academic Librarianship 38</em>(6), 326–332.
</li>
<li id="dev13">Devenney, A. &amp; Chowcat, I. (2013). <a href="http://www.webcitation.org/6PIkFMWTK"><em>Research underpinning the SCONUL and JISC e-books co-design project.</em></a> Retrieved from http://bit.ly/1nYx1rX (Archived by WebCite® at http://www.webcitation.org/6PIkFMWTK)
</li>
<li id="dew12">Dewan, P. (2012). Are books becoming extinct in academic libraries? <em>New Library World, 113</em>(1/2), 27–37.
</li>
<li id="fis12">Fischer, K.S., Wright, M., Clatanoff, K., Barton, H. &amp;Shreeves, E. (2012). Give 'em what they want: a one-year study of unmediated patron-driven acquisition of e-books. <em>College &amp; Research Libraries, 73</em>(5), 469-492.
</li>
<li id="fre11">Fredriksen, L., Cummings, J., Cummings, L. &amp; Carroll, D. (2011). Ebooks and interlibrary loan? Licenced to fill. <em>Journal of Interlibrary Loan, Document Delivery &amp; Electronic Reserve 21</em>(3), 117-131.
</li>
<li id="gus10">Gustaf, H. (2010). <em>E-böcker på högskole- och universitetsbibliotek. En studie kring det digitala förvärvet: kandidatsuppsats i biblioteks- och informationsvetenskap.</em> [E-books in college and university libraries: a study of digital acquisition]. Unpublished Bachelor's dissertation, University of Borås, Borås, Sweden.
</li>
<li id="hod10">Hodges, D., Preston, C. &amp;Hamilton, M. (2010). Resolving the challenge of e-books. <em>Collection Management, 35</em>(3), 196-200.
</li>
<li id="kon13">Konrad K. (2013). <em>Old habits in a new world? E-book management techniques at an academic library</em>. Unpublished Master dissertation, University of Borås, Borås, Sweden.
</li>
<li id="kul12">Kulczycki, E. (2012, September 10). <a href="http://www.webcitation.org/6PSoU6ijE">eBook jako monografia naukowa – czyli kiedy moge dostac punkty?</a> [the eBook as a scientific monograph - that is, when do I get points?] [Web log <em>Warztat badacza</em> post]. Retrieved from http://ekulczycki.pl/warsztat_badacza/ebook-jako-monografia-naukowa-czyli-kiedy-moge-dostac-punkty/ (Archived by WebCite® at http://www.webcitation.org/6PSoU6ijE)
</li>
<li id="kun11">Kungliga Biblioteket. (2011). <em>När kommer boomen? En kartläggningav e-boken i Sverige ur ettbiblioteksperspektiv.</em> [When will the boom happen? A survey of the e-book in Sweden from a library perspective]. Stockholm: SvenskBiblioteksföreningen.
</li>
<li id="kun12">Kungliga Biblioteket. (2012). <em>Forskningsbibliotek 2002-2012 tabell NHS1-NHS8.</em> [Research libraries 2002-2012, tables NHS1-NHS8]. Stockholm: Kungliga Biblioteken.
</li>
<li id="kuz14">Kuzminiene (2014). <em>E-books in Irish universities' libraries: changes and challenges in collection development and acquisitions</em> Unpublished Master's dissertation, University of Borås, Borås, Sweden.
</li>
<li id="lip12">Lippincott, S. K., Brooks, S., Harvey, A., Ruttenberg, J., Swindler, L. &amp;Vickery, J. (2012). Librarian, publisher, and vendor perspectives on consortial e-book purchasing: the experience of the TRLN beyond print summit. <em>Serials Review, 38</em>(1), 3–11.
</li>
<li id="mac13">Maceviciute, E. &amp; Borg, M. (2013). The current situation of e-books in academic and public libraries in Sweden. <em>Libellarium, 6</em>(1-2), 13-28.
</li>
<li id="mar13">Martin, K. &amp; Quan-Haase, A. (2013). Are e-books replacing print books? tradition, serendipity, and opportunity in the adoption and use of e-books for historical research and teaching. <em>Journal of the American Society for Information Science and Technology, 64</em>(5), 1016–1028.
</li>
<li id="med11">Medeiros, N. (2011). Shaping a collection one electronic book at a time: patron-driven acquisition in academic libraries. <em>OCLC Systems &amp; Services: International digital library perspectives, 27</em>(3), 160-162.
</li>
<li id="mee10">Meehan, D. (2010). Strictly online? E-books in the academic world. <em>Panlibus Magazine, 17</em>, 8-9.
</li>
<li id="min11">Minci&amp;cacute;-Obradovi&amp;cacute; K. (2011). <em>E-books in academic libraries</em>. Oxford: Chandos.
</li>
<li id="mor10">Morris, C. &amp;Sibert, L. (2010). Acquiring e-books. In S. Polanka (Ed.), <em>No shelf required : E-books in libraries</em> (pp. 95-124). Chicago, IL: American Library Association Editions.
</li>
<li id="mul11">Mulvihill, A. (2011). Librarians face e-book acquisition obstacles. <em>Information Today, 28</em>(5), 13.
</li>
<li id="mul12">Müller, H. (2012). Legal aspects of e-books and interlibrary loan. <em>Interlending &amp; Document Supply, 40</em>(3), 150-155.
</li>
<li id="nel08">Nelson, M. R. (2008). <a href="http://www.webcitation.org/6PIPqEhmV">E-books in higher education: nearing the end of the era of hype?</a> <em>EDUCAUSE Review, 43</em>(2), 40–56. Retrieved from http://www.educause.edu/ero/article/e-books-higher-education-nearing-end-era-hype (Archived by WebCite® at http://www.webcitation.org/6PIPqEhmV)
</li>
<li id="nel10">Nelson, M.R. &amp;Hains, E. (2010). <a href="http://www.webcitation.org/6PTGlPzIB">E-books in higher education: are we there yet?</a> <em>ECAR Research Bulletin, 2</em>, 1-13. Retrieved from https://net.educause.edu/ir/library/pdf/ERB1002.pdf (Archived by WebCite® at http://www.webcitation.org/6PTGlPzIB)
</li>
<li id="rad12">Radnor, M.C. &amp;Shrauger, K.J. (2012). E-book resource sharing models: borrow, buy, or rent. <em>Journal of Interlibrary Loan, Document Delivery, 22</em>(3-4), 155-161.
</li>
<li id="sch10">Schell, L. (2010). The academic library e-book. In S. Polanka (Ed.), <em>No shelf required: e-books in libraries</em> (pp. 75-94). Chicago, IL: American Library Association Editions.
</li>
<li id="she14">Shepherd, J. &amp; Arteaga R. (2014). Social work students and e-books: a survey of use and perception. <em>Behavioural and Social Sciences Librarian, 33</em>(1), 15-28.
</li>
<li id="sod11">Söderback, A. (2011). Infrastructure first! E-books and academic libraries in Sweden. <em>Serials, 24</em>(1), 38-42.
</li>
<li id="sri12">Srivastava, R. &amp; Sharma, B. (2012). <a href="http://www.webcitation.org/6PSuX0JYS">Digital rights management.</a> In Publicon 2012. <em>Digital Publishing. September 3-4, 2012. FICCI, Federation House. Selected Papers.</em> (pp. 46-57). New Delhi: FICCI, Zubaan Books. from http://bit.ly/1jJPuo2 (Archived by WebCite® at http://www.webcitation.org/6PSuX0JYS)
</li>
<li id="sta11">Stamison, C. M. (2011). Developing a sound e-book strategy. <em>Information Outlook, 15</em>(5), 10-12.
</li>
<li id="sve12">Svedjedal J. (2012). <em>Biblioteken och bokmarknaden – från folkskolan till e-böcker</em> [Libraries and the book market – from elementary school to e-books]. Stockholm: Svenska Förläggareföreningen.
</li>
<li id="sve13">Svensson, A. &amp; Eriksson J. (2013). <a href="http://www.webcitation.org/6PGlBgu2P">Monographs and open access.</a> <em>Sciencominfo (Nordic-Baltic Forum for Scientific Communication), 9</em>(1). Retrieved from http://journals.lub.lu.se/ojs/index.php/sciecominfo/article/view/6126. (Archived by WebCite® at http://www.webcitation.org/6PGlBgu2P)
</li>
<li id="swe12">Sweden. <em>Ministry of Culture</em>. (2012). <em><a href="http://www.regeringen.se/content/1/c6/20/02/57/65903c80.pdf">Läsandets kultur. Slutbetänkande av litteraturutredningen</a></em> [Reading culture: final report of the literature survey]. Stockholm: Ministry of Culture. Retrieved from http://www.regeringen.se/content/1/c6/20/02/57/65903c80.pdf [Unable to archive.] (SOU 2012:65).
</li>
<li id="swe13">Sweden. <em>Parliament</em>. (2013). <em><a href="http://www.webcitation.org/6PWJT0Ocf">En bok är en bok är en bok? - en fördjupningsstudie av e-böckerna i dag</a></em> [A book is a book is a book? In-depth study of e-books today]. Stockholm: Riksdagstrykeriet. (Rapport från riksdagen 2013/14:RFR3.) Retrieved from http://data.riksdagen.se/fil/F358F77F-BAD8-4B71-8F86-B9D0140A80F0 (Archived by WebCite® at http://www.webcitation.org/6PWJT0Ocf)
</li>
<li id="ted05">Tedd, L. A. (2005). E-books in academic libraries: an international overview. <em>New Review of Academic Librarianship, 11</em>(1), 57-79.
</li>
<li id="ted12">Tedd, L. A. &amp;Carin, W. (2012). Selection and acquisition of e-books in Irish institutes of technology libraries. <em>Aslib Proceedings, 64</em>(3), 274- 288.
</li>
<li id="tho09">Thompson, S. &amp; Sharp, S. (2009). E-books in academic libraries: lessons learned and new challenges. <em>Serials, 22</em>(2), 136-140.
</li>
<li id="tyl13">Tyler, D., Falci, C., Melvin, J., Epp, M. &amp; Kreps, A. (2013). Patron-driven acquisition and circulation at an academic library: interaction effects and circulation performance of print books acquired via librarians' orders, approval plans, and patrons' interlibrary loan requests. <em>Collection management. 38</em>(1), 3-32.
</li>
<li id="vas11">Vasileiou, M. &amp;Rowley, J. (2011). Marketing and promotion of e-books in academic libraries. <em>Journal of Documentation, 67</em>(4), 624-643.
</li>
<li id="vas12a">Vasileiou, M., Hartley, R. &amp;Rowley, J. (2012a). Choosing e-books: a perspective from academic libraries. <em>Online Information Review, 36</em>(1), 21-39.
</li>
<li id="vas12b">Vasileiou, M., Rowley, J. &amp;Hartley, R. (2012b). Perspectives on the future of e-books in libraries in universities. <em>Journal of Librarianship and Information Science, 44</em>(4), 217- 226.
</li>
<li id="vas12c">Vasileiou, M., Rowley, J. &amp;Hartley, R. (2012c). The e-book management framework: The management of e-books in academic libraries and its challenges. <em>Library and Information Science Research, 34</em>(4), 282-291.
</li>
<li id="vas08">Vassiliou, M. &amp; Rowley, J. (2008). Progressing the definition of "e-book". <em>Library Hi Tech, 26</em>(3), 355 – 368.
</li>
<li id="vel14">Velagi&amp;cacute;, Z. (2014). <a href="http://www.webcitation.org/6PWSfF85P">The discourse on printed and electronic books: analogies, oppositions, and perspectives</a>. <em>Information Research, 19</em>(2), paper 619. Retrieved from http://informationr.net/ir/19-2/paper619.html (Archived by WebCite® at http://www.webcitation.org/6PWSfF85P)
</li>
<li id="wal12">Walters, W. H. (2012). Patron-driven acquisition and the educational mission of the academic library. <em>Library Resources &amp; Technical Services, 56</em>(3), 199-213.
</li>
<li id="wal13a">Walters, W.H. (2013a). <a href="http://www.webcitation.org/6PIUH5IFf">E-books in academic libraries: challenges for acquisition and collection management.</a> <em>Portal: Libraries and the Academy, 13</em>(2), 187-211. Retrieved from http://bit.ly/1qt7GYe (Archived by WebCite® at http://www.webcitation.org/6PIUH5IFf)
</li>
<li id="wal13b">Walters, W. H. (2013b). E-books in academic libraries: challenges for sharing and use. <em>Journal of Librarianship and Information Science,</em> (In press).
</li>
<li id="wik12">Wikert, J. (2012, February 9). <a href="http://www.webcitation.org/6PSskVLrw"><em>It's time for a unified e-book format and the end of DRM: proprietary e-book formats and rights restrictions are holding customers back</em></a> [Web log post]. Retrieved from http://toc.oreilly.com/2012/02/unified-ebook-format- end-drm.html (Archived by WebCite® at http://www.webcitation.org/6PSskVLrw)
</li>
<li id="win98">Winston, B. (1998). <em>Media technology and society: a history from the telegraph to the Internet</em>. London: Routledge.
</li>
</ul>

</section>

</article>