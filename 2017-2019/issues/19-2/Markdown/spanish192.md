#### vol. 19 no. 2, June 2014

* * *

# Resúmenes en Español Abstracts in Spanish

<section>

## Número temático sobre los libros electrónicos

#### [El fenómeno e-libro: una tecnología disruptiva](”paper612.html)  
T.D. Wilson

> **Introducción**. El e-libro y la tecnología asociada a él, han emergido como una tecnología disruptiva en los últimos diez años. El objetivo de este artículo es analizar algunas de las consecuencias de este desarrollo, basado en el trabajo sobre e-libros en un proyecto de investigación en Suecia.  
> **Argumento**. Para explicar el impacto del fenómeno e-libro se utiliza la teoría de la innovación tecnológica de Winston, con especial referencia a la "necesidad social sobrevenida”, la combinación de factores que convierte una innovación en un producto comercializable. Como resultado de la tecnología se ven afectados todos los aspectos de la producción, distribución y uso de libros.  
> **Variación sectorial y cultural**. El e-libro está teniendo diferentes efectos en diferentes sectores y en diferentes partes del mundo: rápido desarrollo en los EE.UU., más lento en Francia y Japón; rápido desarrollo en las bibliotecas universitarias, desarrollo más lento en las bibliotecas públicas, de país a país. Estas diferencias sugieren que para explicar las divergencias puede ser ineludible una necesidad social sobrevenida.  
> **Conclusión**. Hay una mucha exageración sobre el impacto del e-libro, principalmente por la influencia de los EE.UU. El desarrollo en otros países se está produciendo de manera más lenta y diferente y en los países con un idioma poco extendido como Suecia, el ritmo de desarrollo, excepto en las bibliotecas universitarias, es probable que sea lento.

#### [El discurso sobre los libros impresos y electrónicos: analogías, oposiciones y perspectivas](”paper619.html)  
Zoran Velagić

> **Introducción**. El punto de partida de este trabajo es la doble analogía (analogía de contenidos, analogía de medio) entre los libros impresos y electrónicos, con el objetivo de llamar la atención sobre la percepción habitual de sus capacidades y relaciones, para proporcionar un análisis más detallado de los **Resultados** y la sostenibilidad de tales analogías y en última instancia para indicar los inconvenientes involucrados.  
> **Método**. Se emplea el análisis contextual de los contenidos de los temas clave; en la articulación de las conclusiones, se utilizan enfoques analíticos y sintéticos.  
> **Resultados**. Las definiciones de libro electrónico no son consensuales o sostenibles, reflejando la fase actual de desarrollo del fenómeno. Se pone el énfasis en los cambios y se ignora la continuidad, y no se ve que la posibilidad de analogías derive de un largo desarrollo histórico. Mientras que la analogía de contenidos es sostenible, ya que implica la reproducción de los mismos discursos en diferentes medios de comunicación, la analogía de medio no, porque las capacidades interactivas permitidas por el medio impreso y el medio electrónico son diferentes.  
> **Conclusiones**. El discurso sobre el libro electrónico tiene que ser expandido mediante entendimientos extraídos de áreas afines como la historia del libro, estudios de publicación y, en general, por los de los puntos de vista extremadamente útiles empleados en el enfoque histórico-cultural, porque todos los medios en funcionamiento en un período determinado coexisten y se afectan mutuamente.

#### [Una encuesta nacional sobre los primeros lectores de e-libros en Suecia](”paper621.html)  
Annika Bergström y Lars Höglund

> **Introducción**. Se cree que la lectura de literatura es una piedra angular de la democracia y la buena ciudadanía. Con una disminución de la lectura de libros y de un mercado cada vez mayor de libros electrónicos, es importante seguir la difusión de la lectura de estos libros electrónicos.  
> **Método**. Los datos fueron recogidos en una encuesta realizada en 2012 a gran escala por correo sobre la población sueca de 16 a 85 años. Análisis. Se realizó un análisis bi-variado para revelar grupos de usuarios con respecto a los datos demográficos, hábitos de lectura de libros y frecuencia de lectura de textos digitales distintos de los libros electrónicos. Se realizó un análisis multivariante para revelar el poder explicativo del modelo.  
> **Resultados**. El pequeño número de lectores de libros electrónicos que se encuentra en el estudio indica que estamos al comienzo del proceso de difusión. Un número limitado de variables demográficas y otras explican una parte significativa de la variación en la lectura de libros electrónicos. La demografía de los lectores de libros electrónicos se diferencia tanto para los lectores de libros tradicionales como para los lectores de los diarios digitales y blogs.  
> **Conclusión**. La penetración de los dispositivos de lectura en Suecia es alta, lo que indica la posibilidad de que la lectura de libros electrónicos se expanda y llegue más allá del grupo de los primeros usuarios. Pero el desarrollo es también una cuestión de títulos disponibles en suecos, sobre todo en las bibliotecas públicas, el principal canal de distribución a los usuarios en la actualidad.

#### [La adquisición de libros electrónicos en las bibliotecas de las instituciones de educación superior en Suecia](paper620.html)  
Elena Maceviciute, Martin Borg, Ramune Kuzminiene y Katie Konrad

> **Introducción**. Nuestro objetivo es comparar las ventajas y problemas de la adquisición de libros electrónicos identificados en la bibliografía de investigación experimentados por dos bibliotecas universitarias suecas.  
> **Método**. Se utilizó una revisión de la bibliografía para identificar los principales problemas relacionados con la adquisición de libros electrónicos en las bibliotecas académicas. Los datos para la comparación fueron recolectados a través de estudios de casos en dos universidades suecas. Se utilizaron análisis de documentos, entrevistas y la experiencia personal para la recolección de datos.  
> **Resultados**. Los principales impulsores de la adquisición de libros electrónicos de las bibliotecas académicas suecas son las necesidades percibidas de los usuarios. Los libros electrónicos son considerados como potencialmente útiles para resolver algunos de los problemas de servicio de la biblioteca. Una serie de retos y problemas identificados por los participantes en los estudios de caso coinciden con los que se derivan de la revisión de la bibliografía.  
> **Conclusiones**. Los problemas de la adquisición de libros electrónicos en las bibliotecas académicas parecen ser comunes a los países occidentales fuertes económicamente. Los bibliotecarios universitarios ven ciertas ventajas en los libros electrónicos para sus usuarios y las bibliotecas. Los editores y los bibliotecarios académicos esperan que los libros electrónicos no pierdan las ventajas que los libros impresos les ofrecen. Sin embargo, los editores restringen el uso de los libros electrónicos para garantizar los ingresos como si vendieran copias individuales. Los bibliotecarios tratan de recuperar el mismo nivel de control sobre las colecciones de libros electrónicos como para los materiales impresos.

#### [No todos en la misma página: e-libro, adopción y tecnología de exploración por las personas mayores](paper622.html)  
Anabel Quan-Haase, Kim Martin y Kathleen Schreurs 

> **Introducción**. Este trabajo tiene como objetivo comprender la adopción de los libros electrónicos y lectores electrónicos por parte de personas de sesenta años o más. Esto incluye una investigación sobre donde los adultos mayores se encuentran en las etapas de adopción de libros electrónicos.  
> **Método**. Los datos fueron recolectados a través de entrevistas semi-estructuradas en una ciudad de tamaño medio en el suroeste de Ontario, Canadá.  
> **Análisis.** Las entrevistas fueron transcritas y codificadas utilizando teoría fundamentada. Se utilizó un modelo del proceso de innovación-decisión de Rogers para informar el proceso de análisis de datos.  
> **Resultados**. Los resultados muestran tres factores clave que afectan la adopción: anhelo por la materialidad, la confianza en la tecnología, y la tecnología de la exploración. Mientras que las personas mayores están interesados en e-libros y e-lectores, ven muchas ventajas en su uso y sienten curiosidad por conocer cómo funcionan, la mayoría percibe a esta tecnología como fundamentalmente apropiada para las generaciones más jóvenes.  
> **Conclusión**. Los resultados tienen implicaciones para nuestra comprensión de la difusión de las innovaciones en la población de alto nivel y el desarrollo de servicios dirigidos a ellos. Los libros electrónicos y lectores electrónicos son tecnologías que podrían resultar beneficiosas, ayudando con los problemas relacionados tanto con la portabilidad y conveniencia. Sin embargo, los libros electrónicos no permiten el intercambio de libros a que esta población está acostumbrada, y muchos de ellos todavía muestran reticencia en la adopción plena de esta herramienta en sus prácticas de lectura.

#### [Validación del esquema visitante y residente en un escenario de e-libro](paper623.html)  
Hazel C. Engelsmann, Elke Greifeneder, Nikoline D. Lauridsen y Anja G. Nielsen

> **Introducción**. Mediante la aplicación del marco visitante y residente en el uso del e-libro, el artículo explora si los conceptos de residente y visitante pueden ayudar a explicar el uso del e-libro, y puede ayudar a obtener una mejor comprensión de las motivaciones de los usuarios de utilización de e-libros.  
> **Método**. Se realizaron un cuestionario y entrevistas semi-estructuradas con los usuarios de la Biblioteca de la Facultad de Ciencias Sociales, de la Universidad de Copenhague.  
> **Análisis**. Los datos empíricos se trataron sobre la base de un esquema modificado de visitante y residente de libros electrónicos.  
> **Conclusión**. Los resultados mostraron que el marco se puede aplicar a un contexto de e-libro y que ayudó a investigar de otra forma el comportamiento de los usuarios y a comprender mejor algunas de las dificultades que los usuarios encuentran cuando interactúan con los libros electrónicos. No está claro cuánto se puede utilizar el esquema para predecir el comportamiento del usuario en relación a los e-libros y puede, en última instancia, pronosticar tendencias de uso.

#### [Libros de texto digitales: etapas de las preocupaciones de los bibliotecarios escolares en la implementación inicial](paper625.html)  
Ji Hei Kang y Nancy Everhart

> **Introducción**. Un mandato legislativo para la adopción de los libros de texto digitales en Florida ofrece la oportunidad de identificar las preocupaciones que tienen los bibliotecarios escolares con respecto a la implementación de los libros de texto digitales.  
> **Método**. Se utilizó el cuestionario de etapas de preocupación para identificar las etapas en la preocupación de los bibliotecarios escolares de Florida acerca de su posible papel en esta implementación.  
> **Análisis**. Se utilizó estadística descriptiva para representar la muestra y la distribución de las respuestas a cada pregunta. Las preguntas se dividen en siete etapas distintas, con diferencias claves analizadas, utilizando las etapas de perfiles de preocupación y el análisis de varianza.  
> **Resultados**. Los resultadosr indican que los participantes se ajustan a un perfil no usuario, que tiene mayor despreocupación (Etapa 0), preocupaciones informativas (etapa 1) y preocupaciones personales (Etapa 2). También se encontró que los horarios restrictivos y el gran volumen de trabajo de los bibliotecarios escolares les impiden estudiar nuevas innovaciones, en particular los libros de texto digitales. La intensificación de las preocupaciones personales (Etapa 2) indica la posibilidad de resistencia a los libros de texto digitales.  
> **Conclusiones**. Estos hallazgos podrían ofrecer a los bibliotecarios, como educadores, la oportunidad de establecer un mayor interés en los libros de texto digitales y en discutir el impacto esos libros de texto en la enseñanza y el aprendizaje. En términos más generales, los hallazgos pueden proporcionar algunas implicaciones y recomendaciones para ayudar a los responsables de las políticas educativas y los administradores de la escuela a identificar cuestiones relacionadas con la ampliación de los libros de texto digitales.

#### [El obstáculo del lector: una revisión sistemática de las barreras de adopción, acceso y uso en los estudios de usuarios de libros electrónicos](paper624.html)  
Adam Girard

> **Introducción**. Esta revisión de las barreras para el uso del e-libro identifica sistemáticamente los obstáculos en experiencias de lectura atractivas. Mediante el uso de un marco analítico, se describen los usuarios estudiados, el escenario de estudio y los métodos utilizados en trabajos anteriores, con el fin de identificar áreas prometedoras para la investigación futura.  
> **Método**. El método utilizado es una revisión sistemática de la literatura que recoge los datos de las bases de datos de bibliotecas centrales. Se identifican los criterios explícitos de inclusión y exclusión para garantizar que la revisión está libre de sesgo.  
> **Análisis**. Un marco analítico basado en investigaciones previas en el ámbito de los estudios de usuarios y el concepto de barrera identifica las tendencias comunes en relación a quién está en estudio, en qué contexto, y los métodos utilizados. Además, se identifican las barreras físicas, cognitivas y socio-culturales de la investigación anterior.  
> **Resultados**. Los estudios de los usuarios sobre los libros electrónicos y lectores electrónicos comúnmente se enfocan en los estudiantes en el ámbito universitario y a las barreras físicas (software y hardware) a las que se enfrentan durante la lectura electrónica. Muchos estudios se centran en la adopción en lugar de su uso, y rara vez se centran en la lectura de ficción.  
> **Conclusiones**. Los resultados proporcionan una idea clara del carácter de los estudios de usuarios de libros electrónicos. Aunque los estudios de usuarios sobre los libros electrónicos anteriores son particularmente útiles para las decisiones políticas sobre la colección de libros electrónicos en las bibliotecas, proporcionan una comprensión limitada sobre cómo la gente lee realmente los libros electrónicos. Se identificaron varios aspectos de la experiencia de lectura de libros electrónicos, que han sido poco explorados hasta la fecha. Futuras áreas para una investigación fructífera incluyen la incorporación de los procesos cognitivos y factores sociales y culturales que intervienen en el uso del e-libro. La incorporación de estos factores puede ser particularmente valiosa fuera de los ambientes universitarios, de estudiantes y de biblioteca donde se llevan a cabo normalmente los estudios de usuarios.

## Artículos normales de la revista

#### [Impacto de los criterios de evaluación sobre el comportamiento en la publicación: el caso de la investigación en comunicación en España](paper613.html)  
Pere Masip

> **Introducción**. En este trabajo se describe la evolución de la producción española en el ámbito de la investigación en comunicación en los últimos diecisiete años. Se analiza si la consolidación de los sistemas existentes de evaluación de la actividad científica ha quedado reflejado en un aumento de la producción de autores españoles en revistas indizadas por el Social Sciences Citation Index.  
> **Método.** Se ha seleccionado un enfoque bibliométrico a la materia objeto. Hemos analizado indicadores tales como la productividad institucional e individual, los modelos de publicación y la dinámica de la cooperación (intra e inter-institucional, nacional e internacional).  
> **Análisis.** Este método ha sido aplicado a treinta y cuatro revistas incluidas en la categoría de comunicación del Social Sciences Citation Index. Para garantizar la coherencia en los datos recopilados sólo han sido seleccionadas revistas que han permanecido en esta base de datos durante los diecisiete años que abarca el estudio, de 1994 a 2010.  
> **Resultados**. Los resultados revelan que la producción de los investigadores españoles en comunicación ha aumentado significativamente durante cinco años, des de cuarenta-ocho artículos en 1994-2005, a ochenta y dos en el período 2006-2010.

#### [Creación de Información y código ideológico del managerialismo en el registro del trabajo.](paper614.html)  
Pamela J. McKenzie, Elisabeth Davies y Sherilyn Williams

> **Introducción**. En este trabajo se considera que las prácticas de creación de información en la gestión de información personal mediante el estudio del registro del en la vida cotidiana, por ejemplo, la creación de listas y calendarios.  
> **Método**. Entrevistamos a diez participantes de dos provincias canadienses sobre cómo realizaban el registro y observamos y fotografiamos los espacios físicos y los documentos que creaban y utilizaban. Nuestro conjunto de datos consta de catorce horas de entrevistas, 330 fotografías y 500 páginas de transcripciones de entrevistas.  
> **Análisis.** Se utilizó la técnica cualitativa de comparación constante dentro de un marco abductivo de análisis de relación y discurso para estudiar a) cómo el trabajo doméstico de mantenimiento de registros engancha con las necesidades de las organizaciones, como escuelas y lugares de trabajo, y b) cómo hablar de mantener un registro se relaciona con las manifestaciones de los propios participantes como buenos trabajadores, padres, ciudadanos, etc.  
> **Resultados.** La labor de mantener un registro funciona en términos del concepto de código ideológico de Dorothy Smith. Un imperativo empresarial impregna esta labor, incluso en los contextos domésticos, y los participantes hicieron uso de los géneros y convenciones del lugar de trabajo.  
> **Conclusiones.** Incluso en los hogares, el trabajo de mantener un registro está integrado dentro de contextos organizacionales. El managerialismo se produce y se reproduce en el código ideológico que da forma a la creación de la información de los participantes y su conversación al respecto.

#### [Experiencias de información en línea de adultos jóvenes en la búsqueda de noticias](paper615.html)  
M. Asim Qayyum y Kirsty Williamson

> **Introducción**. Basado en la premisa de que las noticias de todo tipo es una forma de información, el propósito de este estudio fue comprender el uso de las fuentes de noticias por parte de adultos jóvenes en entornos de medios en línea de ritmo rápido y dinámico.  
> **Método**. Se utilizó un marco cualitativo (interpretativo) y un enfoque ampliamente etnográfico. Catorce estudiantes realizaron seis tareas en línea (aquí informamos de cinco) mientras describían sus pensamientos y acciones. Se registraron todas las interacciones en línea y se les hicieron preguntas de entrevista inmediatamente después de cada tarea con el fin de obtener una mayor comprensión.  
> **Análisis**. Se analizaron los protocolos verbales concurrentes y retrospectivos de las tareas para desarrollar temas y categorías.  
> **Resultados**. La mayoría de los participantes prefirieron buscar noticias locales a través de los medios impresos tradicionales pero utilizaron y confiaron confortablemente en los medios de comunicación en línea (excepto blogs) para las noticias nacionales e internacionales. La mayoría de los participantes eran más propensos a utilizar una búsqueda en Google para encontrar información de la vida cotidiana que comprobar un periódico, ya sea en forma impresa o en línea, lo que confirma la prominencia de motores de búsqueda en el mundo online.  
> **Conclusiones**. Los resultados tienen implicaciones para la búsqueda y provisión de información en un sentido amplio de los proveedores de noticias que luchan con las necesidades de información y las preferencias de redes sociales de los lectores en estos entornos en constante evolución.

#### [Suministro e intercambio de información entre proveedores de servicios y refugiados en asentamientos](paper616.html)  
M. Asim Qayyum, Kim M. Thompson, Mary Anne Kennan y Annemaree Lloyd

> **Introducción**. El propósito de este estudio es comprender el suministro e intercambio de información entre proveedores de servicios y refugiados en asentamientos, mientras que los refugiados transitan a nuevas condiciones de vida. Se investigan los esfuerzos de los proveedores de servicios para en tender si se habilita la participación comunitaria, si se reduce la exclusión social, y si las barreras al acceso y uso de la información se minimizan.  
> **Método**. Se utilizó un enfoque cualitativo para explorar en profundidad las prácticas de información de las agencias de servicios que atienden y proveen para el reasentamiento de refugiados en Australia regional. Se realizaron entrevistas semiestructuradas-presenciales y grupos de discusión con los refugiados y los proveedores de servicios de las organizaciones comunitarias y el sector público.  
> **Análisis**. Las entrevistas y narraciones de grupos focales fueron re-analizadas temáticamente con un enfoque en el papel de los proveedores de servicios.  
> **Resultados**. Los refugiados encuentran el contexto de información complejo y de difícil navegación y sufren de sobrecarga de información durante el asentamiento. Esta complejidad produce barreras de información, lo que limita la adquisición de información y por lo tanto la participación. Los proveedores de servicios trabajan fuerte para ayudar a los refugiados, pero una mayor coordinación entre sí y con las entidades comerciales ayudaría a reducir esta complejidad y la sobrecarga y permitir el suministro de información más personalizada.  
> **Conclusiones**. Se recomiendan iniciativas financiadas por el Gobierno sobre la base de estos hallazgos para fortalecer el intercambio de información y la coordinación entre los proveedores de servicios y los refugiados.

#### [La teoría del actor-red en la investigación de sistemas de información](paper617.html)  
Patricia M. Alexander y Emile Silvis

> **Introducción**. El uso de la teoría del actor-red es cada vez más común entre los investigadores de sistemas de información. Este estudio sostiene que la utilidad de la teoría del actor-red como herramienta conceptual para la investigación de los sistemas de información se puede aumentar mediante la expresión de la teoría del actor-red en un formato gráfico. Para este fin, fue diseñada una sintaxis gráfica sobre la base de una conceptualización integral de la teoría del actor-red.  
> **Método**. Se utilizó la investigación en ciencias del diseño para producir un artefacto (la sintaxis gráfica) que se cree que puede ser de uso práctico (relevante), innovador y basado en un conjunto riguroso de los conocimientos.  
> **Análisis**. La sintaxis gráfica se ilustra en este documento sólo por medio de un ejemplo ficticio con el fin de mantener la atención en la sintaxis y los conceptos representados por la sintaxis.  
> **Resultados**. Las fortalezas y debilidades de la sintaxis están relacionadas con los de la teoría del actor-red en sí. La fuerza principal es la inclusión de actores heterogéneos en el análisis, y la principal debilidad se refiere a los límites imprecisos de la teoría. Esta debilidad está tratada por la capacidad de la sintaxis para identificar los focos de investigación primaria.  
> **Conclusiones**. Se propone que la teoría del actor-red se puede representar con éxito por una sintaxis gráfica y que puede aumentar la utilidad de la teoría del actor-red como una herramienta conceptual para la investigación de sistemas de información.

#### [Clarificando la colaboración en situaciones de atención médica de emergencia: colaboración paramédico - médico y tecnología de telepresencia 3D](paper618.html)  
Diane H. Sonnenwald, Hanna Maurin Söderholm, Gregory F. Welch, Bruce A. Cairns, James E. Manning y Henry Fuchs

> **Introducción**. Este artículo se centra en las perspectivas paramédicas con respecto a la colaboración actual paramédico - médico y sus perspectivas en relación con el potencial de la tecnología de telepresencia 3D en el futuro.  
> **Método**. Se realizaron entrevistas a cuarenta paramédicos.  
> **Análisis**. Los datos de las entrevistas se analizaron mediante codificación abierta y axial. Se llegó a un acuerdo de 0,82 utilizando la kappa de Cohen de medida de concordancia entre codificadores. Después de completar la codificación de los temas y las relaciones entre códigos se sintetizaron utilizando memorándums de temas.  
> **Resultados**. Los paramédicos expresaron su preocupación por la falta de respeto y confianza hacia ellos exhibida por otros profesionales médicos. Hablaron de cómo pintan la imagen para los médicos y la importancia para el médico de confiar en el paramédico. Informaron además de cómo la tecnología de telepresencia 3D haría su trabajo visible de forma que anteriormente no era posible. También informaron de que la tecnología requeriría capacitación adicional, cambios en los modelos financieros existentes utilizados en la atención de emergencia, y un mayor acceso a los médicos.  
> **Conclusiones**. La enseñanza de habilidades y estrategias de colaboración a médicos y paramédicos podría beneficiar su colaboración actual, y aumentar su disposición a utilizar eficazmente las tecnologías de colaboración en el futuro.

#### [Traducciones realizadas por](paper.html) [José Vicente Rodríguez](mailto:jovi@um.es) y [Pedro Díaz](mailto:diazor@um.es), Universidad de Murcia, España.

##### Last updated 10 June, 2014

</section>