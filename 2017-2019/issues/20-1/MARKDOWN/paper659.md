#### vol. 20 no. 1, March, 2015

# Citizens and social media in times of natural disaster: exploring information experience

#### [Christine Yates](#author) and [Helen Partridge](#author)  
Information Systems School, Queensland University of Technology, Brisbane, Australia

#### Abstract

> **Introduction**. Social media are becoming a vital source of information in disaster or emergency situations. While a growing number of studies have explored the use of social media in natural disasters by emergency staff, military personnel, medical and other professionals, very few studies have investigated the use of social media by members of the public. The purpose of this paper is to explore citizens’ information experiences in social media during times of natural disaster.  
> **Method**. A qualitative research approach was applied. Data was collected via in-depth interviews. Twenty-five people who used social media during a natural disaster in Australia participated in the study.  
> **Analysis**. Audio recordings of interviews and interview transcripts provided the empirical material for data analysis. Data was analysed using structural and focussed coding methods.  
> **Results**. Eight key themes depicting various aspects of participants’ information experience during a natural disaster were uncovered by the study: connected; wellbeing; coping; help; brokerage; journalism; supplementary and characteristics.  
> **Conclusion** . This study contributes insights into social media’s potential for developing community disaster resilience and promotes discussion about the value of civic participation in social media when such circumstances occur. These findings also contribute to our understanding of information experiences as a new informational research object.

## Introduction

Research investigating social media in natural disasters is an emerging area of enquiry, and one that has attracted attention from an array of disciplines within recent years such as media studies, emergency management, crisis information management and computer science ([Fraustino, Liu and Jin, 2012](#fra12); [Bruns, Burgess, Crawford and Shaw, 2012](#bru12b)). Within this area, it is acknowledged that greater research attention has been afforded to the use of social media in disaster or emergency contexts by emergency services staff, military personnel, and medical and various other professionals as opposed to members of the public ([Shklovski, Palen and Sutton, 2008](#shk08)). Recognising this paucity in research attention, this study examines citizens’ use of social media within the context of natural disaster events through exploration of an emerging concept within the information research field, namely, information experience ([Bruce, Davis, Hughes, Partridge and Stoodley, 2014](#bru14)). The study investigates the following question: What are citizens’ information experiences in social media during times of natural disaster?

This research draws upon two events that occurred in Queensland, Australia. The first of these was a series of floods (referred to as the 2010-2011 Queensland floods) that affected over seventy towns and 200,000 people in much of central and southern Queensland during December 2010 and throughout January 2011\. The second event was a severe tropical cyclone (Tropical Cyclone Yasi), which made landfall in northern Queensland on 3 February 2011 and is cited as one of the most powerful cyclones to have affected Queensland in recorded history ([Queensland Government, 2011](#que11); [Australian Bureau of Meteorology, 2011](#aus11)).

The types of applications and specific affordances of social media are not a focus of this study which is narrower than social media use in natural disasters. Instead it examines the information experience aspect of social media use during this type of event.

The study uncovered eight key themes depicting various aspects of participants’ information experience during a natural disaster: connected; wellbeing; coping; help; brokerage; journalism; supplementary and characteristics. These findings share several similarities with the results of earlier research that have in some way examined citizen use of social media in natural disasters, together with a number of new insights. The study provides further illustration of how social media are becoming a vital source of information in disaster related situations for many citizens. In addition, it contributes additional insights into social media’s potential for promoting and developing community disaster resilience, and promotes discussion in relation to the value of civic participation in social media spaces when such circumstances occur.

## Literature review

This review examines literature that has investigated citizen or public use of social media in disaster related contexts. In particular, it concentrates on research concerning citizen use of social media in natural disasters as opposed to human-generated emergency events. The review will focus on outlining an array of themes that have been examined in this broad research space, rather than providing a detailed discussion of individual studies.

The study by Shklovski _et al._ ([2008](#shk08)) provides an early illustration of research that examined public use of social media during a natural disaster event. This research investigated people’s information seeking practices using information and communication technology during the Southern California wildfires in October 2007\. In defining such technologies the study included the use mobile phone devices, Short Message Service (SMS) texting, the Internet, and Web-based social networking sites.

A number of studies have examined the various ways in which citizens may participate in social media during disaster events (e.g. [Nagar, Seth and Joshi, 2012](#nag12); [Hughes and Palen, 2009](#hug09)). Research within this trajectory has revealed that public use of social media during disasters may be apportioned into three distinct segments including influential social media creators, social media followers, and social media inactives ([Fraustino _et al_., 2012](#fra12), p. 21). Likewise research by Bruns and Stieglitz ([2012](#bru12a)) and Cheong and Cheong ([2011](#che11)) also identified that different groups of actors appeared in social media, and performed essential roles with respect to the provision and communication of disaster-related information.

Research attention regarding public or popular use of social media in disasters has examined the kinds of content contained in messages that citizens communicate. The studies by Acar and Muraki ([2011](#aca11)) and Cho, Jung and Park ([2013](#cho13)) both examined the types of messages expressed by Twitter users during the Great East Japan Earthquake on 11 March 2011\. Similarly, research by Sinnappan, Farrell and Stewart ([2010](#sin10)) provides a further illustration of this line of interest in their investigation of the kinds of Twitter messages (i.e. tweets) posted during the Black Saturday bushfires that occurred in Victoria, Australia during February 2009.

Several studies have likewise explored citizens’ reasons for using social media in times of natural disaster. In a survey of university students, Jung ([2012](#jun12)) drew upon media system dependency theory to investigate citizens’ motivations for using different types of social media in the aftermath of the 2011 earthquake in Japan. Taylor, Wells, Howell and Raphael ([2012](#tay12)) also conducted a survey to examine people’s motives for participating in social media and what they were doing in social media applications in regards to several natural disasters in Australia and New Zealand that took place between January and March 2011\. As a further example, research by Bird, Ling and Haynes ([2012](#bir12)) examined people’s reasons for looking at community Facebook pages during the 2010/11 floods in Queensland and Victoria, Australia.

Propagation and re-use of information is a further topic that has captured research attention. The study by Starbird, Palen, Hughes and Vieweg ([2010](#sta10)) analysed the content of public microblogging behaviour in Twitter during the 2009 Red River Valley flood threat in the United States and Canada. This research examined the extent of new information about the flood threat that was injected into the Twitterverse and by whom, along with the degree to which information was re-used through analysing data concerning usage of the re-tweet function. In the same way, Bruns and Steiglitz ([2012](#bru12a)) reviewed Twitter activity patterns to examine people’s use of various inbuilt communicative tools including original tweets, retweets, @replies and URLs across a range of events including natural disasters. The study by Murthy and Longwell ([2013](#mur13)) likewise investigated the re-use of information by people in Twitter during the 2010 Pakistan floods. Here, emphasis was placed on exploring tweets and retweets of URLs from traditional information sources as opposed to social media to investigate users’ information source preference.

Some examination has also occurred in relation to the co-production of information in social media between members of the public and emergency management services during disaster events. The study by NGIS Australia ([2009](#ngi09)) evaluated and trialled the use of social media as a communication vehicle for enhancing information sharing between citizens in affected communities and emergency management agencies, and more specifically social media’s value as a source of location-enabled information. Sinnappan, _et al._ ([2010](#sin10)) examined citizen communication in Twitter during the Black Saturday bushfires in Australia, and revealed the value it provided as a source of information for emergency management services and information that reflected ground-level conditions. More recently, research by Chatfield, Scholl and Brajawidagda ([2013](#cha13)), investigated citizens’ involvement in co-producing time-critical public information. This study examined Twitter use during the 2012 Northern Sumatran earthquake in Indonesia and the role of citizenry in re-tweeting government generated information pertaining to Tsunami early warnings.

Sreenivasan, Lee and Goh ([2011](#sre11)) and Qu, Huang, Zhang and Zhang ([2011](#qu11)) both studied citizens’ use of information from online social media spaces in disaster events. The study by Sreenivasan, _et al._ drew upon Taylor’s information use environment model to analyse citizens’ microblog postings in the 2010 Icelandic volcano eruption. Qu, _et al._ undertook a series of studies that investigated how Chinese citizens made use of information on the microblogging site Sina-Weibo for disaster response and recovery, and examined the role that online communities may play in disaster response. Here several disaster contexts that occurred in China were included in these investigations including the 2008 Sichuan earthquake, the 2010 Yushu earthquake and severe flooding in 2010.

Finally, research into citizens’ use of social media in natural disaster has placed emphasis on certain aspects such as how using online spaces made them feel ([Taylor, Wells, Howell and Raphael, 2012](#tay12)), and how citizens’ discovered disaster-related community Facebook pages, as well as their perceptions of information quality in this social media space ([Bird, _et al._, 2012](#bir12)).

To date research into citizen or public use of social media in disaster situations has examined a diverse range of foci. However, no prior studies to this point have been undertaken that have explored citizens’ information experience as its research object. This study fills that gap.

## Conceptualising information experience

As previously stated, the intent of this study was to investigate citizens’ information experience in social media during times of natural disaster. However, before proceeding further it is necessary to provide some explanation of how, for this study, we delineate the idea of information experience as a research object.

Information experience may be understood as a ‘_complex, multi-dimensional engagement with information’ in real-world contexts'_ ([Bruce, _et al._, 2014](#bru14), p. 4). Our view of information experience as a research object draws upon and is influenced by phenomenology, which seeks to investigate the unique meanings that comprise people’s lived experience of a particular phenomenon. Central to phenomenology is its interest in what is described as the life-world, which refers to ‘_the world of immediate experience_’, or ‘_the existent world as we find ourselves in it_’ ([Adams and van Manen, 2008](#ada08), p. 617). In attending to the idea of the life-world, interest is placed on understanding the inter-subjective world of human experience, which comprises people’s thoughts and actions, along with the social manifestation of these ([Schwandt, 2007](#sch07)). Thus, this study’s interest and intent was to examine people’s lived experience of social media as an informational life-world during times of natural disaster.

We consider that information experience may be viewed as a discrete object of research, and one that exists alongside other research objects such as information behaviour, information seeking and use, information practice and information literacy within the overarching information research field ([Partridge and Yates, 2014](#par14)). Furthermore, in investigating information experience we consider that this expression incorporates several elements. First, taking a broad perspective, the idea of information experience may include capturing a holistic view of a particular informational life-world. In addition, it may also be imagined as a set of information experiences, which taken together constitute people’s information experience. Finally, information experience may also be thought of as occasions in which people are conscious of making use of or engaging with information, that is, people’s experiences of information ([Bruce, _et al._, 2014](#bru14)).

## Research approach

Applying a qualitative research approach the project used in-depth interviews to gather data about citizens’ information experience in social media during times of natural disaster. In-depth interviews seek to understand the world from the participants’ perspective, to reveal the meaning of these experiences, and to explore the participants’ lived world ([Kvale and Brinkmann, 2009](#kva09)). Kvale ([2007](#kva07), p. 7) describes interviews as ‘_a conversation that has structure and a purpose determined by one party – the interviewer_’. Through this conversation the interviewer has a ‘_unique opportunity to uncover rich and complex information_’ ([Cavana, Delahaye and Sekaran, 2001](#cav01), p. 138), and the participants can express their story using their own words. In-depth interviews were identified as the most appropriate approach for the study because of their suitability in obtaining data about people’s views, opinions, ideas and experiences ([Arskey and Knight, 1999](#ars99)).

### Participants

The research sample consisted of twenty-five (25) people that were in some way affected by either of the two natural disaster events. The sample comprised 18 females and 7 males who were aged between 18 and 59 years. The majority of the study’s participants were residents from locations that were directly impacted by one of these natural disasters. However the study’s sample also included a couple of participants that were residents from outside the affected areas. In these instances, these participants identified themselves as people who had also made use of social media during a natural disaster because of having family or friends that were directly impacted by either of these events.

A combination of convenience and purposive sampling were selected as the most effective approach for participant recruitment. Consequently, participants were recruited for the study in a number of ways. First, the researchers drew upon personal and professional networks by posting recruitment messages through their own accounts in Facebook and Twitter. Second, a number of recruitment messages for the study were posted on government or community managed pages in Facebook (e.g. Townsville Disaster Information, Cyclone Yasi Update). Third, a number of participants were recruited as a result of media releases posted on the Queensland University of Technology website and a series of radio interviews broadcast in Far North Queensland. Finally, several participants in the study were recruited as a result of messages they had seen reposted or retweeted about the study by others in Facebook or Twitter.

### Data collection and analysis

Data was collected through semi-structured interviews. Kvale and Brinkmann ([2009](#kva09), p. 3) define the semi-structured interview as a ‘_planned and flexible interview with the purpose of obtaining descriptions of the life world of the interviewee in order to interpret the meaning of the described phenomena_’. Interviews were conducted face-to-face or by Skype and were audio recorded. The duration of interviews ranged from 28 to 69 minutes. One member of the research team was exclusively responsible for undertaking data collection.

In developing the data collection instrument one of the challenges encountered was determining the best way in which to orient participants to what the interview was about. It was recognised that the term information experience was something that participants would potentially not be familiar with. Instead, the interview guide used language that would encourage participants to reveal their experiences of using social media as an information world and was focussed on particular themes ([Kvale and Brinkmann, 2009](#kva09)). Illustrations of the kinds of questions asked during interviews included: Can you tell me about your experiences of using social media during the flood/cyclone?; Why did you use social media?; What did you do with information you received or found on social media?; and What did you find helpful or beneficial about using social media during the flood/cyclone?. Within this study, understanding of the term ‘_social media_’ was left to the interpretation of each participant, together with the decision of which particular social media applications that they wanted to discuss. In addition, follow-up and probing questions were used to further explore points as they emerged during each interview. The interview scripting was not strictly predetermined, and as such, the specific questions used and the sequence in which they were posed varied across all interviews.

The data analysis process was an iterative one and constantly grounded in the interview data. Data analysis focussed on exploring the concept of information experience as a research object and, therefore, sought to uncover different aspects of citizens’ lived experience of social media as an information life-world during a natural disaster event. The researchers spent time listening to the audio recordings and reviewing the interview transcripts, with the aim of identifying the emerging themes and also to determine the similarities and differences and potential connections among keywords, phrases and concepts within and among each interview. Data was analysed using first and second cycle coding methods. In the first cycle, structural coding was used as a way to allocate basic labels to the data that would provide a topic inventory. In the second cycle, focussed coding was used to categorise the data according to thematic or conceptual similarity, and to eventually develop the most prominent or significant categories from the data ([Saldaña, 2013](#sal13)).

The researchers collectively analysed a portion of the transcripts to develop a common list of codes to attribute to the data. This helped to ensure that there was consistency in each researcher’s understanding of the object of study, and in the way that data analysis of the remaining interview transcripts was approached. The remaining transcripts were then evenly allocated for analysis between the two researchers. Each researcher undertook analysis of their respective portion of transcripts and then exchanged these with one another for review. Following the advice of Saldaña ([2013](#sal13)), the research team developed and maintained a codebook during data analysis, which contained a list of all the codes that had been created, together with its descriptive meaning. As new codes emerged during the process of analysis these were added as necessary following intensive discussion with the respective research team member.

## Findings

The study’s findings revealed eight key themes that depicted various aspects of participants’ information experience in social media during a natural disaster. The eight themes uncovered during data analysis were as follows: connected; wellbeing; coping; help; brokerage; journalism; supplementary and characteristics. A description of each of the eight themes follows, together with illustrative quotes that were derived from research interviews. Where quotations are shown, the corresponding participant number is provided at the end of each statement.

### Connected

This theme represents an aspect of participants’ information experience that referred to the sense of connectedness that was engendered through the sharing, exchange and communication of information between citizens in social media.

The continual flow of content that transpired through social media helped to create and heighten feelings of togetherness or closeness with other citizens on an individual level, as well as to the broader community or city in which the natural disaster had occurred.

> I remember at the time thinking I don’t feel alone… you know I can sit here [on Facebook] and I’m collectively going through this experience with all these other people. (P15)

In consequence several participants spoke about how this sense of connectivity ‘_made you feel not so isolated_’ (P14), or they ‘_didn’t feel like I was alone_’ (P24). Instead, it created a virtual environment where ‘_this whole sense of the world’s there with you_’ (P10) prevailed.

Particular functionalities inherent to social media likewise were perceived as evidence that amplified participant’s sense of connection and provided signals that other people were out there as the incident unfolded. This included features such as icons on Facebook chat, which indicated that people were online, people liking or sharing or commenting on posts in Facebook, and people using the retweet function in Twitter. The following quotation illustrates this idea:

> We’ve always used the radio [during a cyclone]… but it is isolating… whereas if you’ve got the social media, you’ve got that instant feedback. If you put something up there, somebody 'likes it'. (P14)

In this theme, what was considered to be information comprised details that facilitated the experience of human connectedness as opposed to factual detail about the disaster itself. For example, one participant described how information was ‘just basically hearing what your friends were doing’ as well as narratives where they were ‘_sharing our experiences with the world_’ (P13). Similarly one participant described how sharing certain kinds of details during Cyclone Yasi had intensified the sense of connection to others. This comment was made in regards to conversations that had occurred in Facebook after they posted the following kinds of messages as a status update:

> Has anyone else run out of colourings for the kids to do? Has anybody else run out of conversation with a partner? Please somebody other than my family speak to me! (P18)

Finally, some participants also spoke about the idea of connection, by reflecting on a previous natural disaster they had experienced. Their comments reveal the difference and impact that information transpiring through social media had made:

> That was really important, the fact that I could talk to people, because traditionally in a cyclone, you lock the doors, you just sit there and you wait till it’s finished and you hope you come out the other end. This was a much more interactive experience. (P13)

> …just having that access to information, being up to date and aware of what was going on. I felt much more a part of the Cairns community this time. Last time [Cyclone Larry] it felt like very individual, this was just happening to me… Whereas this time [Cyclone Yasi] I felt much more aware of Cairns in general, and how everyone was pulling together. (P11)

### Wellbeing

The theme of wellbeing portrays a facet of participants’ information experience that referred to communicating information about their own personal wellbeing through social media, or using information available in social media to monitor the wellbeing of others. Participants described creating and sharing information through social media during the natural disaster for the purpose of providing ‘_updates_’ (P15), to people located outside of the area affected by the natural disaster, that expressed what they were ‘_dealing with_’ (P17) and ‘_letting them know that_ [they] _were alright_’ (P11).

The following two comments depict the act of generating and imparting information through social media in order to communicate personal wellbeing:

> I was able to sit on Facebook and say 'Okay everybody, this is the deal… this is how we’re going, we’re all safe, we’re downstairs, and the power’s still on as you can tell cause I’m still on the Internet'. (P21)

> We were putting up updates saying 'everything’s okay, we’re bunkered down'. We took some photos. We had a setup of like an airbed in this little room that had few windows, we’d brought our car in and put it sort of in the gap in the only possible place where a window could be smashed and a tree come in, the car was between us and that space. So we put up some photos [of that] letting our friends know that we’re pretty prepared, it’s all good. (P15)

Likewise several participants mentioned drawing upon information shared through social media as a way of monitoring or tracking other people’s welfare, and which diminished the necessity for direct conversations with one another:

> I did have a couple of other friends in Townsville who were on [Facebook]... and I asked them where they were and whether they’d gone to somebody else’s house or stayed at their house... and that was quite nice, to be able to just see where everyone was. (P21)

> It was a good way of following friends and family who were using it... a way of keeping track of them without actually bugging them... you knew how they were doing without having to disturb them… So it was useful in that sort of sense, that you could get a sense of what was going on without having to actually harass people. (P6)

Information shared through social media to report on personal wellbeing was frequently highly contextualised, as opposed to simple statements such as I’m okay. Instead this kind of information provided a more nuanced and meaningful understanding of how people were faring at a particular point in time.

The following participant’s comment provides an illustration of contextualised information to communicate wellbeing in making preparations for the arrival of Cyclone Yasi:

> Oh look, we’ve got two generators, 40 litres of fuel, got the x-box, the big TV, a carton of beer and a bag of ice. Me and [name] and the missus are here and we’ll be fine… it’s all good, we’re locked up in the shed and you know it’s a sturdy old place. (P4)

As a further example, this next comment depicts contextualised information that was shared once the cyclone has passed:

> I put up a photo of our street, just to show some people who were freaking out still… our street was fine compared to the rest of Townsville. Like, no one in our street lost a roof, no one in our street lost trees really. We lost one palm tree, and the whirly bird flew off my neighbour’s house. But overall, compared to the rest of Townsville, we were pretty good. (P21)

### Coping

Coping portrays an affective aspect of participants’ information experience that related to particular emotions which information in social media generated for people that assisted them to manage or better handle the natural disaster event.

Many participants described how information communicated through social media helped to engender feelings of comfort or reassurance:

> It was actually really reassuring to watch the information that was coming through on Twitter and knowing that I was doing the right thing by staying at home and keeping off the roads… because there was that impulse to go out there and see what was going on… or go out and try and help people and the messages that kept coming through saying to keep off the roads if it wasn’t essential for you to travel. (P3)

In addition, participants also revealed how the opportunity to share information in social media, in terms of expressing their own feelings or concerns, promoted emotions that were helpful to coping with the occurrence of a natural disaster. This next participant’s statement illustrates this point by making reference to how they used Facebook during Cyclone Yasi:

> To get things off my chest... basically just to get it out there that I’m worried, these are my concerns. And to have other people put that sort of thing up [on Facebook]… I just found that once I put it out there to say you know “I’m worried”, I felt a hell of a lot better after that. (P18)

A final element identified in this theme also concerned how information transpiring through social media became a source of entertainment for several participants as it ‘gave you something to do’ (P18) or something that ‘_kept [them] busy_’ (P9). In this way, the distraction it provided from the situation likewise assisted in helping people to cope as the following quote reveals:

> In the early hours of the morning, one of my friends who was local started to just put up music clips, you know, anything to do with wind... so that was kinda cool. So in the middle of it all you think 'Oh I haven’t heard that song for a while', and so you take three minutes out of the middle of the cyclone to listen to the song... it really filled the time. (P13)

### Help

Help describes a part of participants’ information experience that referred to disclosing or imparting details through social media for the purpose of offering or requesting assistance or advice to the broader community, along with using information communicated through social media platforms as a way of trawling for opportunities or ways to assist. Participants reported on how they divulged information via social media in order to request help from other citizens:

> The house next door to us was completed neglected… it had all of this rubbish in the backyard, like shopping trolleys and old swings, things that would have been potential missiles [in a cyclone]. I put this up on the [Townsville Disaster Information] Facebook page saying “I don’t know what to do about this” and people were posting back “Contact Council”. (P10)

Likewise the capacity to impart information through social media to offer help was also mentioned by many participants:

> There was a lot of outputting of 'How can we just help?'… and 'What can I do to help you?'. (P23)

> There was lots of people just saying lots of general, you know 'I’ve got room if there’s anyone that needs somewhere to stay' or 'I’ve got a spare fridge' or 'I’ve got a generator'… people helping each other. (P15)

Some participants also spoke of how details communicated through social media provided a source of inspiration for ideas on ways in which they could help during a time of natural disaster:

> There was something that started up on Twitter [during the floods] called Baked Relief, there was a hashtag that happened. So as a result of that I was able to get ideas for ways that I could contribute by sending food. (P3)

> I’m a bit of a supporter of the RSPCA… so I was looking for updates on if there is anything I could do to help. Did they need temporary accommodation for some of their animals or so on? Do I need to drive down there and offer to mind a couple of dogs or something? (P3)

One noteworthy aspect connected to this theme was that several participants commented on how the disclosure of details through social media helped to heighten their sense of ‘community spirit’ (P15) or engender a ‘community feeling’ (P10). In this way, it appeared to be evident that for some participants, the capacity to impart information via social medial enabled and facilitated their relationship with and connection to the broader community, and in consequence could more effectively draw upon this resource in a time of need.

In addition it was also evident within this theme that in disclosing details via social media, conversations were taking place between strangers, rather than purely between individuals that were already known to the participants (e.g. friends on Facebook). This point was observable through participants’ use of the term people when describing this aspect of their information experiences. The idea of conversations taking place among strangers within a community is demonstrated in the following quotes below:

> There were people putting up 'Oh McDonalds at the Lakes is open and they’ve got power points that you can use' and ]If your phone battery is running low, go to the Willow Shopping Centre and plug it into the cleaners’ outlets'. (P14)

> The whole of the town basically ran out of masking tape, because everyone was buying it by the bucketload to tape the windows up. So if you found a newsagent tucked away behind a bread shop and it had ten rolls of tape you posted it so people would know where to get tape from. (P11)

### Brokerage

This theme depicts an aspect of participants’ information experience in social media that was associated with taking on or playing the role of an information broker during a time of natural disaster. By taking on or playing this role participants recognised that they became a ‘_conduit_’ (P1, P10) or ‘_gatekeeper_’ (P13) of information for others, and as such were ‘_providing an information service_’ (P1).

The role of an information broker involved picking up ‘_little nuggets_’ or ‘_pieces of unique, useful information_’ that could be passed or shared on to ‘_the right ears – people who needed to know that information_’ and ‘_that would benefit people knowing_’ (P3). In the same way, the role included distilling or filtering what was regarded as important information, or as one participant stated ‘_news as opposed to... just people’s feelings [about the impending natural disaster]_’ (P9). As further illustration, the following comment reveals what the role of playing an information broker entailed:

> I think that’s really what I was trying to do during [Cyclone] Yasi you know, pull out the relevant information out of the noise. (P9)

The motivation to play the role of an information broker was ostensibly altruistic in that participants considered that they were ‘_doing their bit_’ (P1), to ‘_help the people in my life, help them do what they needed to do and to get through it_’ (P9). Participants recognised that a natural disaster signified a time of need, and as such the impetus for playing the role of an information broker was to pass on or share information in order to help or assist others. This idea is conveyed in the following comment:

> A friend of mine… he was actively searching for tips and he’d come [on to Facebook] saying 'all right I’ve just done some research, and one of the biggest failings of modern houses is roller doors in cyclones… get your wheelie bins pushed up against the roller doors, inside, and fill them with water... will stop the door rolling up'. (P13)

Similarly, the impetus to take up the role of an information broker was also prompted by instances where participants felt that certain information had missed being shared around:

> there was a meeting point for flood assistance that was announced on the radio, but I wasn’t seeing that information announced anywhere else – so I was able to share that on Twitter and that way it could get passed around. (P3)

In brokering information participants spoke of doing this in either a targeted or generic way, that is, to either a narrow or broad audience. As one participant noted:

> If the Queensland Police or the Cyclone Yasi update or the Council posted something [on Facebook] that I thought would benefit people, then I would repost it, and I would tag my friends, like my Cairns friends. I’d make sure that they were tagged in it so that they got a notification that I’d sent them something, rather than just posting it and hoping that they saw it. (P11)

Whereas other participants commented about passing on information in a more universal sense:

> For clean up stuff [after Cyclone Yasi]… you’d click ‘share’ [in Facebook] so it’d go up onto your wall so that everybody else would be able to see it (P14).

As an information broker, participants shared information both within and external to social media platforms. In several instances, participants reported harnessing communication platforms external to social media (e.g. text messages, face-to-face conversation) in order to broker information to others. This practice was based on the participant’s understandings of others access to information via social media channels, along with recognised disruption to communication channels as a result of the natural disaster:

> I was texting everybody. So you know, everybody’s racing around doing stuff... even to be in radio earshot I think was hard for people at various stages. So I was watching QPS Media [on Twitter] and anytime that I felt there was anything relevant, as opposed to the standard stuff. You know there’s no point telling them there’s a bloody big cyclone coming, everybody knows that. (P9)

### Journalism

Journalism denotes an aspect of participants’ information experience in social media that referred to performing the role of a citizen reporter during a time of natural disaster. In carrying out this role, participants recognised that social media platforms provided a means by which they could broadcast information without the need of a media agency in the form of personal commentary, photos or stories and to share these with the world:

> So we were driving around, taking photos and, you know, sort of posting them on Facebook straight away so that other people who were still in their homes, or people overseas, or my family interstate, you know they were seeing the damage straight up. 'This happened an hour ago' or 'This happened two hours ago' and 'Here are the photos so that you can see what it looks like here now'. I put a whole album on Facebook, called Cyclone Yasi, with all photos of trees fallen over, and power lines fallen over. (P11)

> The Toowoomba [inland tsunami] was very powerful actually... seeing those photographs and the videos that people put up on Facebook... somebody standing at their back door with their iPhone and they’d uploaded this to YouTube or they put it on Facebook or whatever... the fact that some teenage kids could stand at their back door with their iPhone and just shoot really, really graphic images of utter destruction. (P1)

Some participants discussed how the ability to act as an independent newscaster gave citizens greater opportunities to provide unique personalised accounts of their individual circumstance:

> With Twitter and Facebook I felt that the information was being provided by individuals and it was more personal, that they were providing it because they wanted to show people 'Hey look what’s happened to me, look what’s happened to my backyard!'. (P3)

Likewise, citizens’ capacity to broadcast information in the form of personal stories also enabled people to exercise greater control over the nature and way in which information was presented. The statement below reflects this idea:

> Whereas with Facebook, you’re getting someone’s personal account, with the real emotions in or out, depending on what they wanna do, but you’re getting more of the facts, instead of the doctored propaganda machine. (P4)

However for some participants, the capacity to take on the role of a citizen reporter generated questions about the ethics and protocols associated with sharing information via social media. Specifically this referred to questions about the ownership of certain information along with the entitlement to share it. This was especially apparent when information was deemed to be highly personal in nature and involved imagery in either photographic or audio-visual formats:

> So kinda taking advantage of adversity. It can be a bit cringe-worthy when professional photographers do it for media, but I think when you’ve got a lot of other people who go around and take shots and then load them up on the internet, and people who’ve just lost their house and everything in it and they really don’t want somebody they don’t know just whizzing by with an iPhone and snapping it... it’s not being respectful of people in a really bad point of their life. (P23)

Similarly this next participant’s statement provides an illustration of their rationale about decisions made concerning the types of photographs they considered were appropriate to post on Facebook:

> The photos that I posted were taken by people who pretty much live in the area, and knew the area, and it may have been photos of some of their stuff that’s been affected. Or there were photos of places that me and my family had grown up with in Bulimba... they weren’t photos of someone’s desolate house who I didn’t know. So it was much less personal, much less personal. I can’t think of any photos that I stole to be honest and put up that were personal. (P4)

In contrast, another participant spoke of criticism they received after posting photos on Facebook in the aftermath of Cyclone Yasi. Their comments reveal an alternative perspective on the citizen reporter’s entitlement to share information and the underlying motivations for doing this:

> I was the one that posted them on my Facebook page... I got a little bit of backlash by some of the people in Townsville saying that I was a parasite, a parasite going around glorifying in other people’s tragedy... I didn’t see it that way, I saw it totally different, I saw it that I was showing people that this is what has happened around our area. (P14)

### Supplementary

This theme depicts how information in social media was experienced as a supplementary information world to other information media during a time of natural disaster. It refers to participant’s use of multiple channels to obtain information, whereby information in social media formed an added component within a potential information landscape. This idea is reflected in the following participant’s statement regarding their use of several channels during Tropical Cyclone Yasi:

Many participants commented on the value of social media as an additional avenue through which information could be obtained, and more specifically how social media as an information world compared to other media networks. In one instance a participant spoke about ‘using Twitter and Facebook to fill in the gaps for information that we couldn’t get through the radio or through any other means’ (P1), while another made mention of how social media provided a way through which information not reported as part of major television news broadcasts could be attained:

> I found it was actually the only way to get any information about what was actually happening in Roma, because the Brisbane [TV] channels aren’t really interested in reporting on what’s going on there. (P5)

As a further example, this next statement likewise supports this idea:

> You know like using Facebook, BOM [Bureau of Meteorology] and the [Queensland] Police site you got a better picture than what you were getting off the TV, farbetter. Most of the TV stuff, what you got on the TV was stuff from above. While the stuff we were getting [from social media] was more on the ground. (P7)

In addition, participants also spoke of perceived differences between information in social media and mass media such as television or newspapers during a time of natural disaster. Specifically many participants remarked at how information broadcast through mass media on such occasions was heavily infused with drama and irrelevant propaganda:

> The Channel 9 news stood at the top of the Kangaroo Point cliffs [in Brisbane] all week and we had people being interviewed who weren’t affected, didn’t know anybody who was affected, but were there being interviewed on TV... and it had absolutely nothing to do with anything, and so that wasn’t useful. The Queensland Police Service weren’t putting that kind of garbage out through social media. (P2)

Similarly, this next participant’s statement also notes the dramatized nature of information in mass media compared to social media during Cyclone Yasi:

> I think it was more that with the news bulletins on TV and radio, they were quite sensational, and it was difficult to distil exactly what was going on from them compared with the Facebook site [Townsville Disaster Information], because it was linked in to the Bureau of Meteorology and the Queensland Police Service. (P10)

Finally, participant’s comments also revealed how social media as an information world facilitated access to information from organisations, which historically had not been looked upon as media agencies in their own right. In this way, participants observed an emergence of information that was emanating from a multitude of new media agencies. The two interview excerpts below provide evidence of this point:

> I would trust that the Queensland Police Service is going to say the right stuff. You know, I had faith in them as an [law enforcement] agency, and I have faith in them now, as a media agency. (P1)

> I still follow the Queensland Police Service... it’s probably one of the first times when I really started following an organisation [on social media] and really seeing it as my major way of connecting with them. (P2)

### Characteristics

The theme of characteristics depicts particular qualities that participants ascribed to information in social media during times of natural disaster. In other words, this theme reveals various characteristics that participants perceived as being unique about information as an object as experienced in social media:

> I think my experience of the floods was very different because I used social media. If I hadn’t had that source of information, you know, specific and detailed information and the constant updates and being able to sit there on my computer and watch as things, you know, information filtered in minute by minute... I think my experience of the event was made different because of social media. (P3)

Information in social media was described as something that was generated in ‘real-time’ (P13). This point is illustrated by the following comment where the participant compares Facebook to the radio as an information source:

> Facebook was a bit different because you were able to, like, actively keep up to date on that minute, not the delay that the radio has. (P21)

Likewise, the idea of real time information is also evident in the next participant’s quote when they mention using the online chat function within Facebook during Cyclone Yasi:

> Sitting there talking to people online and watching them saying “my carport’s just disappeared, the neighbour’s roof is gone”, all those kinda things. (P13)

In addition, participants described information in social media as something that was instantaneous in nature. Here, many participants compared social media to other conventional information channels, which they considered to be ‘passive’ due to the fact that they ‘only get updated every now and then’ (P9). In contrast, information in social media was an object that could be constantly accessed in an immediate manner as the following quotation shows:

> It was like you just had to press refresh and you could get information… you don’t have to worry about, you know, like waiting for the newspaper to come out... or anything like that, it’s just there in your hand on your phone. (P24)

Convenient was a further trait that participants associated with information in social media, which referred to the ease of accessibility through which information could be obtained in social media, as well as how it could be used as ‘one place to go’ (P16). This next participant’s statement illustrates the notion of convenience that was ascribed to information in social media:

> I found that I didn’t really need to listen to the radio or anything like that because all the information was just there and it was really handy because all the links and the phone numbers were posted [on various Facebook pages]. So it was like… it just made it all really convenient. (P11)

Many participants also made reference to the highly localised nature of information that could be obtained through social media during a time of natural disaster. Here, social media was observed as an information channel through which it was possible to access precise details, or in other words ‘little bits’ (P14) of information that were relevant to a specific geographic location:

> people were just tweeting and commenting on what was happening in their local area… so I could listen to the news, or I could watch Twitter which was giving me information about things at a really local kind of level. (P1)

As a further example, this next statement also reveals the localised nature of information that could be obtained through social media. In this instance, the participant makes reference to details concerning the capacity of evacuation shelters for residents in the lead up to Cyclone Yasi:

> Social media also posted, things like 'Only 300 spots left at Stockland Shopping Centre' or 'Only 50 more people allowed into Trinity Bay School'. So you knew where you stood. Like if you were heading to Trinity Bay School and you could see that they were only letting 50 more people in, you might turn around and go to a different one, instead of waiting in line for ages and then being turned away. (P11)

A final quality that participants associated with information in social media was as an object that could be controlled. Several participants mentioned how social media provided them with the capacity to ‘_really target… [to be] able to control the kind of information and get what you need_’ (P15), or in other words, to ‘_pick and choose what you want to hear or see_’ (P2). This same point is similarly reflected in the statement below where the participant is discussing how they chose to follow certain Twitter accounts that were providing information about the 2011 Queensland floods:

> I had a look at what people were tweeting and started following a few more people when it became clear that they were providing useful information. (P3)

However, while many participants spoke about this aspect of control about information in social media, this was coupled with an understanding that there were limitations on the degree to which this could be achieved. Here, participants recognised that the inherent workings of social media technology also meant that irrelevant content would appear in their _Facebook News Feed_ or _Twitter Timelines_ as the following statement shows:

> Of course there’s not total control. I was still getting everyone’s status update about you know, they went to some party and got drunk and didn’t get home till 3 o’clock in the morning or whatever. But yeah, I could still get back what I needed as well. (P25)

The next quotation provides a further illustration of this point where reference is made to a particular example of irrelevant content that appeared in a participant’s Facebook newsfeed in the aftermath of Cyclone Yasi:

> But at the same time you’ve got someone else who doesn’t know what’s going on, or doesn’t care what’s going on, posting you know, a kitten rings doorbell video… so life goes on quite normally and life [for me] had been fundamentally altered... it’s all jumbled up. (P13)

In these instances, it appeared that the infiltration of irrelevant content was an accepted reality, and that the benefits afforded by social media as an information channel outweighed its shortcomings for the majority of participants.

## Discussion

Several of the findings in the present study concur with the results of previous research that has been undertaken in this field. Starting with Characteristics, this theme is consistent with other studies that identified convenience, timely and highly localised as unique qualities of information in social media ([Fraustino, _et al._, 2012](#fra12); [Bird, _et al_., 2012](#bir12); [Shklovski, _et al._, 2008](#shk08)). Likewise, the affective aspect of people’s information experiences as depicted in the Coping theme accords with research that has also revealed citizens use of social media during disaster situations for obtaining emotional support ([Qu, _et al._, 2011](#qu11)) and to make them feel less worried (Taylor, Wells, Howell and Raphael, 2012). Similarities with the results of existing research were also found in the themes of Connected and Help (([Fraustino, _et al._, 2012](#fra12); ([Taylor, _et al._, 2012](#tay12); ([Qu, _et al._, 2011](#qu11); ([Hughes and Palen, 2008](#hug08)). Here, previous research showed how information in social media helped maintain a sense of community and a feeling of being connected to others, together with assisting citizens to self-mobilise and become actively involved in a disaster situation.

Next, the themes of Supplementary and Wellbeing likewise concur with prior research findings in this area. In regards to the Supplementary theme, these findings correspond with differences that citizens’ observed between information in social media compared to mass media (e.g. television) during disaster situations. Here, this referred to comments regarding the highly sensationalised nature of information broadcast through mass media channels, compared to social media ([Shklovski, _et al._, 2008](#shk08)). Likewise, communicating information about personal safety to family and friends using social media as portrayed in the Wellbeing theme is supported by previous research findings ([Fraustino, _et al._, 2012](#fra12)). In this instance, the often highly contextualised nature of information shared to communicate personal wellbeing was a similar observation of earlier research by Sreenivasan, Lee and Goh ([2011](#sre11)).

In contrast, there were several aspects of difference within the study’s findings that have not been previously described in earlier research. First, turning to Coping, this theme revealed how information in social media came to be a source of entertainment, which provided a positive source of distraction, and assisted people to cope with a natural disaster event. Second, a particular quality ascribed to information in social media was a further point of difference uncovered in the Characteristics theme. Here, this related to the capacity to control or target as a unique feature of information as an object experienced in social media. Third, a further interesting aspect was revealed in the Supplementary theme. Specifically this concerned how social media was perceived as facilitating citizens’ access to information from organisations that traditionally were not considered to be media agencies. In this way social media provided citizens with admission to an additional information world, and by extension to new and discrete sources of information.

The themes of Brokerage and Journalism, which depict the different roles that participants played or took on as part of their information experience in social media were significant findings which had received limited attention in existing literature. Although the idea of information brokers is identified in earlier research by Sutton, Palen and Shklovski ([2008](#sut08)), these current findings provide further illumination of citizens’ performing this kind of role. They provide additional insights into aspects such as motivations for taking on the role of information broker, what this role may involve, and how it may be enacted. In the same way, the current study also provided further insights into taking on the role of citizen reporter within the context of social media as detailed through the Journalism theme. This included explicating certain affordances that were enabled through this role, together with exposing questions about moral codes and etiquettes that performing this role may generate.

In addition, while collectively the eight themes represent the lived experience of citizens’ information experience in social media, inter-relationships between some of the themes were also identified. First, the themes of Characteristics and Supplementary are both concerned with how citizens experienced information as an object within social media spaces. This referred to the particular characteristics of information in social media, and social media as an additional information world. Second, the themes of Connected, Wellbeing and Coping refer to what citizens’ information experiences in social media enabled them to realise on an individual level during a natural disaster. Finally, the themes of Brokerage, Help and Journalism each appear to depict an aspect of citizens’ information experiences that referred to fostering relationships and connections with the broader community.

These findings of this study provide further support for assertions about the potential psychological benefits that citizens may derive from social media use during natural disaster situations ([Rive, Thomas, Hare and Nankivell, 2012](#riv12)). Similar to other studies, they affirm how using social media in a disaster can provide a source of psychological first aid for citizens through promoting sensations of security and connectedness, as well as fostering individual and group efficacy ([Taylor, _et al._, 2012](#tay12)). Indeed discussing the importance of social interaction for citizens to effectively manage the effects of disasters, some authors consider that citizens’ information-sharing activities in social media serve a dual function. First, by supplying needed information to other citizens. Second, by providing a means through which individuals can engage in the psychologically beneficial practice of collectively conversing about traumatic events ([Sutton, _et al._, 2008](#sut08)).

Furthermore, this study helps us to understand the potential of social media use in terms of promoting and developing community disaster resilience. For example, Dufty ([2012](#duf12)) discusses how social media can help build community disaster resilience through establishing social capital that can be used to grow or enhance social networks, provide support during the course of a disaster, and to facilitate post-event learning. Likewise, Taylor, _et al._, ([2012](#tay12)), and Horbury and Hughes ([2010](#hor10)) also affirm how the use of social media in disaster events supports capacities that aid and bolster community resilience. In particular, they identify information and communication as a key adaptive capacity that reinforces community resilience, and how social media facilitates this through developing shared meaning and understanding of an intense experience by way of collective narrative.

Finally, the current findings add to a growing body of literature that is acknowledging the value of civic participation in social media during times of natural disaster. Here, this refers to the recognition of citizens as an important source of information in disaster situations, as well as the roles that individuals and communities can play with respect to information provision and dissemination ([Comrie, 2011](#com11); [Bird, _et al._, 2012](#bir12); [Bruns, 2011](#bru11)). In the same way, the capacity to obtain information that is derived from citizens’ local knowledge has also been identified as a benefit of citizen engagement in social media, together with the roles that citizens may play in facilitating disaster response ([Sutton, Hansard and Hewett, 2011](#sut11); [Qu, _et al._ 2011](#qu11)).

## Conclusion

This paper has presented the findings of research that investigated how citizens use social media in a disaster situation. Using a qualitative approach, this study identified a set of eight themes that depict various aspects of the participant’s information experience during a natural disaster. While the study’s findings share some similarities with the results of earlier research examining the use of social media in natural disasters, they also offer a number of unique insights. The outcomes of this study illustrate how social media are becoming a vital source of information in disaster situations. The study also highlights social media’s potential for promoting and developing community disaster resilience. The study outlined here is one of an emerging area of enquiry exploring information experience as a research object ([Bruce, _et al._, 2014](#bru14)). This study has revealed that people’s information experiences, at least within the context of social media and natural disasters, are rich, complex and dynamic.

Information experience is a construct worthy of further investigation. This study has shown that researching information experience provides deep insights into the ways in which people relate to their information worlds. More research exploring information experience within different contexts and setting, and that draw upon different methodologies, is needed to help develop our theoretical understanding of this important and emerging construct.

## Acknowledgements

The authors acknowledge, with thanks, the auDA Foundation for providing research funding to undertake this study. The authors are also deeply grateful to the research participants for their willingness to take part in the study and for sharing their experiences.

## About the authors

**Christine Yates** is a Liaison Officer with Youngcare, an organization that works to help provide choice in care and housing for young Australians with high care needs. From 2009 – 2015 she worked as a Research Fellow and Project Manager in the Information Systems School at the Queensland University of Technology (QUT) on a number of research projects that focused on the way people experience information and/or technology to learn either as students, as professionals or as citizens in their everyday life. Christine’s research interests focus on exploring people’s experiences with information in various contexts of everyday life and with lifelong learning. She can be contacted at [cl1.yates@gmail.com](mailto:cl1.yates@gmail.com)  
**Helen Partridge** is a Professor and Pro Vice Chancellor (Scholarly Information and Learning Services) at the University of Southern Queensland. She is also as Adjunct Professor in the Information Systems School at the Queensland University of Technology. Helen’s research focuses on the interplay between information, learning and technology. She has been a visiting fellow at the Oxford Internet Institute, University of Oxford (2011) and the Berkman Center for Internet and Society, Harvard University (2014). She can be contacted at [Helen.Partridge@usq.edu.au](mailto:Helen.Partridge@usq.edu.au)

#### References

*   Acar, A. & Muraki, Y. (2011). Twitter for crisis communication: lessons learned from Japan’s tsunami disaster. _International Journal of Web Based Communities, 7_(3), 392-402.
*   Adams, C. & van Manen, M. (2008). Phenomenology. In L. Given (Ed.), _The Sage encyclopaedia of qualitative research methods_ (pp. 615-620). Los Angeles, CA: Sage Publications.
*   Arskey, H. & Knight, P. (1999). _[Interviewing for social scientists](http://www.webcitation.org/6RRWYi1N0))_. London: Sage Publications.
*   Australian Bureau of Meteorology. (2011). [_Severe tropical cyclone Yasi: 30 January – 3 February 2011_](http://www.webcitation.org/6RRWYi1N0)). Retrieved from http://www.bom.gov.au/cyclone/history/yasi.shtml (Archived by WebCite® at http://www.webcitation.org/6RRWYi1N0)
*   Bird, D., Ling, M. & Haynes, K. (2012). Flooding Facebook - the use of social media during the Queensland and Victorian floods. _The Australian Journal of Emergency Management, 27_(1), 27-33.
*   Bruce, C., Davis, K., Hughes, H., Partridge, H. & Stoodley, I. (Eds.). (2014). _Information experience: approaches to theory and practice_. Bingley, UK: Emerald.
*   Bruns, A. (2011). Towards distributed citizen participation: lessons from WikiLeaks and the Queensland floods. In P. Parycek, M.J. Kripp & N. Edelmann (Eds.), _CeDEM11: Proceedings of the International Conference for E-Democracy and Open Government_ (pp. 25-52). Krems, Austria: Danube-University Krems.
*   Bruns, A. & Stieglitz, S. (2012). Quantitative approaches to comparing communication patterns on Twitter. _Journal of Technology in Human Services, 30_(3-4), 160-185.
*   Bruns, A., Burgess, J., Crawford, K. & Shaw, F. (2012). _#qldfloods and @QPSMedia: crisis communication on Twitter in the 2011 South East Queensland floods._ Brisbane, QLD: ARC Centre of Excellence for Creative Industries and Innovation, Queensland University of Technology.
*   Cavana, R., Delahaye, B. & Sekaran, U. (2001). _Applied business research: qualitative and quantitative methods_. Brisbane, QLD: Wiley.
*   Chatfield, A.T., Scholl, H.J. & Brajawidagda, U. (2013). Tsunami early warnings via Twitter in government: net-savvy citizens' co-production of time-critical public information services. _Government Information Quarterly, 30_(4), 377-386.
*   Cheong, F. & Cheong, C. (2011). [Social media data mining: a social network analysis of tweets during the 2010-2011 Australian floods.](http://www.webcitation.org/6RRaj3ZPX)) In _Proceedings of the Pacific Asia Conference on Information Systems_. Retrieved from http://aisel.aisnet.org/pacis2011/46 (Archived by WebCite® at http://www.webcitation.org/6RRaj3ZPX).
*   Cho, S.E. Jung, K. & Park, H.W. (2013). Social media use during Japan's 2011 earthquake: how Twitter transforms the locus of crisis communication. _Media International Australia, Incorporating Culture & Policy,_ No. 149, 28-40.
*   Comrie, N. (2011). _[Review of the 2010-11 flood warnings & response](http://www.webcitation.org/6RRasGMjk)_. Retrieved from http://www.floodsreview.vic.gov.au/about-the-review/final-report.html (Archived by WebCite® at http://www.webcitation.org/6RRasGMjk).
*   Dufty, N. (2012). Using social media to build community disaster resilience. _Australian Journal of Emergency Management, 27_(1), 40-45.
*   Fraustino, J.D., Liu, B. & Jin, Y. (2012). _[Social media use during disasters: a review of the knowledge base and gaps.](http://www.webcitation.org/6RRazWNst)_ Retrieved from http://www.start.umd.edu/sites/default/files/files/publications/START_SocialMediaUseduringDisasters_LitReview.pdf (Archived by WebCite® at http://www.webcitation.org/6RRazWNst).
*   Horbury, A. & Hughes, P. (2010). Popular knowledge and performances of the self in distributed networks: social media after Black Saturday. _Media International Australia, Incorporating Culture & Policy_, No. 137, 144-153.
*   Hughes, A.L. & Palen, L. (2009). Twitter adoption and use in mass convergence and emergency events. _International Journal of Emergency Management, 6_(3), 248-260.
*   Hughes, A.L, Palen, L., Sutton, J., Liu, S.B. & Vieweg, S. (2008). Site-seeing in disaster: an examination of on-line social convergence. In F. Fiedrich & B. Van de Walle (Eds.), _Proceedings of the 5th International Conference on Information Systems for Crisis Response and Management ISCRAM 2008_, (pp. 324-333). Washington, DC: ISCRAM.
*   Jung, J-Y. (2012). [Social media use and goals after the Great East Japan earthquake](http://www.webcitation.org/6RRbhxdh5). _First Monday, 17_(8–6). Retrieved at http://firstmonday.org/ojs/index.php/fm/article/view/4071/3285 (Archived by WebCite® at http://www.webcitation.org/6RRbhxdh5).
*   Kvale, S. (2007). Doing interviews. In U. Flick (Ed.), _The Sage qualitative research kit_, (pp. 1-157). London: Sage Publications.
*   Kvale, S. & Brinkmann, S. (2009). InterViews: learning the craft of qualitative research interviewing. Los Angles: CA: Sage Publications.
*   Murthy, D. & Longwell, S.A. (2013). Twitter and disasters: the uses of Twitter during the 2010 Pakistan floods. _Information, Communication & Society, 16_(6), 837-855.
*   Nagar, S., Seth, A. & Joshi, A. (2012). Characterization of social media response to natural disasters. In _Proceedings of the 21st International Conference on World Wide Web,_ (pp. 671-674). New York, NY: ACM.
*   NGIS Australia. (2009). _[Social media helping emergency management](http://www.webcitation.org/6RRc5HZOy)_. Retrieved from http://gov2.net.au/projects/project-14/(Archived by WebCite® at http://www.webcitation.org/6RRc5HZOy).
*   Partridge, H. & Yates, C. (2014). Research information experience: object and domain. In C. Bruce, K. Davis, H. Partridge & I. Stoodley (Eds.), _Information experience: approaches to theory and practice,_ (pp. 19-31). Bingley, UK: Emerald.
*   Qu, Y., Huang, C., Zhang, P. & Zhang, J. (2011). _[Harnessing social media in response to major disasters](http://www.webcitation.org/6RRcLJreF)_. Paper presented at the ACM Conference on Computer Supported Cooperative Work (CSCW ’11), 19-23 March, Hangzhou, China. Retrieved from http://www.cs.cmu.edu/~yangwan1/cscw2011/(Archived by WebCite® at http://www.webcitation.org/6RRcLJreF).
*   Queensland Government. (2011). _[Queensland Government response to the Floods Commission of Inquiry interim report](http://www.webcitation.org/6RRd1eSgf)_. Retrieved from http://www.floodcommission.qld.gov.au/publications/interim-report/ (Archived by WebCite® at http://www.webcitation.org/6RRd1eSgf).
*   Rive, G., Thomas, J., Hare, J. & Nankivell, K. (2012). _[Social media in an emergency: developing a best practice guide literature review.](http://www.webcitation.org/6RRdZupsJ)_ Retrieved from http://www.getprepared.org.nz/sites/default/files/uploads/Social%20media%20in%20an%20emergency%20-%20a%20best%20practice%20guide%202012.pdf. (Archived by WebCite® at http://www.webcitation.org/6RRdZupsJ).
*   Saldaña, J. (2013). _The coding manual for qualitative researchers_. London: Sage Publications.
*   Schwandt, T. (2007). Lifeworld. In T. Schwandt (Ed.), _The Sage dictionary of qualitative inquiry_ (3rd ed.), (pp. 179-180). Thousand Oaks, CA: Sage Publications.
*   Shklovski, I., Palen, L. & Sutton, J. (2008). Finding community through information and communication technology during disaster events. In _Proceedings of the 2008 ACM conference on Computer Supported Cooperative Work (CSCW ’08)_, (pp. 127-136). New York, NY: ACM.
*   Sinnappan, S., Farrell, C. & Stewart, E. (2010). _[Priceless tweets! A study on Twitter messages posted during crisis: Black Saturday](http://www.webcitation.org/6RRdTz4eC)_. Paper presented at the 21st Australasian Conference on Information Systems (ACIS), 1-3 December, Brisbane. Retrieved from http://aisel.aisnet.org/acis2010/39 (Archived by WebCite® at http://www.webcitation.org/6RRdTz4eC).
*   Sreenivasan, N., Lee, C.S. & Goh, D.H. (2011). Tweet me home: exploring information use on Twitter in crisis situations. In _Proceedings of the 4th International Conference on Online Communities and Social Computing (OCSC ’11)_, (pp. 120-129). Berlin: Springer-Verlag.
*   Starbird, K., Palen, L., Hughes, A.L. & Vieweg, S. (2010). Chatter on the red: what hazards threat reveals about the social life of microblogged information. In _Proceedings of the 2010 ACM Conference on Computer Supported Cooperative Work (CSCW ’10)_, (pp. 241-250). New York, NY: ACM.
*   Sutton, J., Hansard, B. & Hewett, P. (2011). _[Changing channels: communicating tsunami warning information in Hawaii](http://www.webcitation.org/6RRes6okJ)_. Paper presented at the 3rd International Joint Topical Meeting on Emergency Preparedness and Response, Robotics, and Remote Systems, 9 August, Knoxville, Tennessee. Retrieved from http://www.jeannettesutton.com/Papers_and_presentations.html (Archived by WebCite® at http://www.webcitation.org/6RRes6okJ).
*   Sutton, J., Palen, L. & Shklovski, I. (2008). Backchannels on the front lines: emergent uses of social media in the 2007 Southern California wildfires. In F. Fiedrich & B. Van de Walle (Eds.), _Proceedings of the 5th International Conference on Information Systems for Crisis Response and Management ISCRAM 2008_, (pp. 624-632). Washington, DC: ISCRAM.
*   Taylor, M., Wells, G., Howell, G. & Raphael, B. (2012). The role of social media as psychological first aid as a support to community resilience building. _The Australian Journal of Emergency Management, 27_(1), 20-26.