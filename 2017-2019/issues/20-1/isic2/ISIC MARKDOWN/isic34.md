<header>

#### vol. 20 no. 1, March, 2015

</header>

<article>

# Intelligence, academic self-concept, and information literacy: the role of adequate perceptions of academic ability in the acquisition of knowledge about information searching

#### [Tom Rosman](#author) and [Anne-Kathrin Mayer](#author)  
ZPID, Leibniz Institute for Psychology Information and Documentation, Trier, Germany  
[Günter Krampen](#author)  
University of Trier and ZPID, Leibniz Institute for Psychology Information and Documentation, Trier, Germany

#### Abstract

> **Introduction.** The present paper argues that adequate self-perceptions of academic ability are essential for students' realization of their intellectual potential, thereby fostering learning of complex skills, e.g., information-seeking skills. Thus, academic self-concept should moderate the relationship between intelligence and information literacy: a positive relationship between intelligence and information literacy is only expected for students with a high academic self-concept. It is expected that this moderator effect is mediated by students' effort: Whenever students recognise their actual deficits or strengths, they will invest more effort than if they are over- or under-confident.  
> **Method.** Data were gathered in a quantitative field study with 137 psychology freshmen from the University of Trier, Germany. Measures included a standardised information literacy test, Raven's Advanced Progressive Matrices test for fluid intelligence as well as standardised measures for students' academic self-concept and work avoidance tendencies.  
> **Analysis.** . Data was analysed through multiple regression analysis and tests for mediated moderation.  
> **Results.** With regard to the hypothesised interaction effect, it was confirmed that a positive relation between intelligence and information literacy solely exists for students with a high academic self-concept. A high academic self-concept may even be detrimental for information literacy when paired with a low intelligence. These effects were partially mediated by students' tendency for work avoidance.  
> **Conclusions.** Our findings corroborate that adequate self-perceptions of academic abilities are a basic requirement for information-seeking skills. Hence, we emphasise a need for ability-tailored information literacy training paired with performance feedback to foster realistic self-perceptions.

<section>

## Introduction

Due to an exponentially growing body of scholarly information, information literacy - often defined as the ability to successfully conduct academic information searches - is more and more considered a '_basic skills set of the 21st century_' ([Eisenberg, 2008](#Eis08), p. 39). Much effort has been put in conceptualizing and defining the construct (e.g., [Lloyd, 2005](#Llo05); [Campbell, 2008](#Cam08); [Mackey and Jacobson, 2011](#Mac11)), and researchers lively discuss its consequences for student achievement ([Bruce, 2004](#Bru04); [Detlor, Julien, Willson, Serenko and Lavallee, 2011](#Det11)) as well as didactic guidelines on information literacy instruction ([Andretta, 2005](#And05); [Limberg and Sundin, 2006](#Lim06); [Streatfield, Allen and Wilson, 2010](#Str10)).

As most approaches on information literacy (e.g., [ACRL, 2000](#Acr00)) underline its processual nature, researchers increasingly investigate procedural knowledge about information searching ([Ivanitskaya, O'Boyle and Casey, 2006](#Iva06); [Rosman, Mayer and Krampen, under review](#Ros0)). In contrast to declarative, more factual knowledge, procedural knowledge relates to knowledge about how to successfully conduct information searches (e.g., knowledge about how to use certain bibliographic databases). As research in the field of cognitive psychology demonstrates, procedural knowledge is probably best acquired through learning by doing ([Nokes and Ohlsson, 2005](#Nok05)). Educational psychology adds to this field an analysis of individual characteristics like intelligence or personality dispositions, susceptible to influence knowledge acquisition ([Jonassen and Grabowski, 1993](#Jon93); [Sohn, Doane and Garrison, 2006](#Soh06)).

However, little to no research has been conducted on interindividual differences in the acquisition of procedural knowledge about information searches. We thus argue that research on the didactics of information literacy training should be complemented by research on how individual factors influence information-seeking knowledge. Results of this research may in turn help researchers and practitioners to improve their training designs or allocate students to customised training modules.

Regarding the types of individual characteristics to be considered, Markus, Cross and Wurf ([1990](#Mar90)) argued that human performance is a function of both objective skills (e.g., intelligence) and subjective beliefs (e.g., academic self-concept). Therefore, we plead for an integrated approach that considers both objective and subjective determinants of knowledge acquisition about information searching. The present paper discusses two such variables: intelligence, considered as an objective skill, and academic self-concept, its subjective counterpart.

#### Intelligence and information literacy

Intelligence, or general cognitive ability, is considered the most important predictor of academic success ([Jensen, 1998](#Jen98); [Kuncel, Hezlett and Ones, 2001](#Kun01); [Mayer, 2011](#May11)). Some argue that information searching requires - at least to some extent - the same set of skills that is measured by common intelligence tests, such as analytical ([Lenox and Walker, 1993](#Len93)) and problem-solving skills ([Andretta, 2005](#And05); [Brand-Gruwel, Wopereis and Vermetten, 2005](#Bra05)). This assumption gains support when taking a closer look at intelligence theory. Fluid intelligence, as measured by Raven's Advanced Progressive Matrices ([Raven, Raven and Court, 1998](#Rav98)), refers to the '_… ability to decompose problems into manageable segments and iterate through them, the differential ability to manage the hierarchy of goals and subgoals generated by this problem decomposition, and the differential ability to form higher level abstractions_' ([Carpenter, Just and Shell, 1990](#Car90), p. 429). This decomposition of a problem into smaller subproblems and the resulting necessity of organizing and managing a multitude of subgoals (the so-called goal management) supposedly plays a central role in information searching: the initial problem (e.g., '_I want to find scholarly literature on personality disorders in children_'.) has to be decomposed into manageable segments (e.g., determining the extent of information need, selecting relevant databases, elaborating which functions of the databases are of use, etc.) prior to and throughout the actual search. Therefore, fluid intelligence should have some influence on a nonlinear and iterative process like information literacy ([Rosenfeld, Salazar-Riera and Vieira, 2002](#Ros02)).

### The role of the academic self-concept

Many authors have argued that academic self-perceptions play a central role in academic achievement by enabling students to develop their potential ([Marsh and Seaton, 2013](#Mar13)). Students' academic self-concept is deemed to influence learning and academic performance through motivational processes like increased effort and persistence ([Marsh, 1993](#Mar93); [Bandura, 1997](#Ban97)) as well as academic choice behaviour ([Guay and Vallerand, 1996](#Gua96); [Guay, Marsh and Boivin, 2003](#Gua03)). Students with a high academic self-concept should therefore invest more effort in their learning, persevere in the face of difficulties, and act out of pleasure and choice. Especially this acting out of pleasure and choice is hereby hypothesised to have positive effects '_on depth of processing, retention, integration, generalization of knowledge, and thereby academic achievement_' ([Guay and Vallerand, 1996](#Gua96), p. 214).

A shortcoming of this research is that it solely focuses subjective beliefs and attitudes, neglecting individual abilities as another important predictor of academic success. Markus _et al_. ([1990](#Mar90)) therefore elaborate their theorizing by hypothesising that '_competence in a domain requires both some ability in the domain and a self-schema for this ability_' and that '_felt competence is an essential aspect of actual competence_' (p. 206). Even though individual ability is seen as a basic requirement for performance, commitment and achievement motivation can only emerge in case of a favourable view of oneself and one's abilities (e.g., a high academic self-concept or self-efficacy). If, however, students underestimate their possibilities, they will be less optimistic, devote less effort to their actions, and give up more easily in the face of difficulties ([Markus _et al_., 1990](#Mar90)). Hence, the under-estimation of abilities (underconfidence) may well hinder individuals to make use of their potential: high ability students should only perform better than their lower talented peers if their academic self-concept matches their ability (see also [Feldhusen and Hoover, 1986](#Fel86)). Findings that bright but low performing students (so-called underachievers) have low self-concepts can be brought forward as empirical support for the postulated negative effects of underconfidence ([Van Boxtel and Mönks, 1992](#Van92); [Reis and McCoach, 2000](#Rei00)).

However, this literature is somewhat one-sided, as most authors focus on high-ability students and the detrimental effects of underconfidence. A high subjective ability is seen as beneficial in all possible circumstances, as it is ascribed to promote effort, persistence, and attention ([Markus , 1990](#Mar90)). This theorizing is challenged by a growing body of literature on the detrimental effects of ability over-estimation (overconfidence). Freund and Kasten ([2012](#Fre12)) give the compelling example of someone having a car accident because of the over-estimation of his driving abilities. With respect to academic domains, it has been shown that the over-estimation of academic ability has negative effects on students' grades ([Zakay and Glicksohn, 1992](#Zak92)), learning and retention ([Dunlosky and Rawson, 2012](#Dun12)), or IT competence ([Moores and Chang, 2009](#Moo09)). These negative effects might be due to a reduction of effort as it has been shown that over-confident students prematurely terminate studying ([Dunlosky and Rawson, 2012](#Dun12)) and become less academically engaged in general ([Robins and Beer, 2001](#Rob01)). Effort calculation theory, which states that with increasing subjective ability, humans will assume less effort to be necessary for goal attainment ([Heider, 1958](#Hei58)), provides support for this assumption.

To sum up, there is an apparent contradiction between the theories of Markus _et al_. ([1990](#Mar90)) who state that high subjective ability always promotes effort, and Heider ([1958](#Hei58)) who argues that high subjective ability always reduces effort. In combination with the presented empirical findings, we hypothesised the following: Whenever subjects are able to _realistically_ estimate their ability, the resulting effort will be higher than when they over- or underestimate their ability. As learning (or, more generally, performance) requires both effort and ability ([Heider, 1958](#Hei58)), the most pronounced increase of knowledge is to be expected for subjects who have a high ability and devote effort to realise their potential. Reduced effort, on the other hand, should impair knowledge development.

We thus argue that an adequate estimation of one's ability might promote knowledge development through an increase in effort. On the other hand, over- or underconfidence might impair knowledge development due to a reduction of effort. One can therefore conclude that accuracy of self-evaluations plays a major role in learning and performance ([Freund and Kasten, 2012](#Fre12)). Correspondingly, Plucker, Robinson, Greenspon, Feldhusen, McCoach and Subotnik ([2004](#Plu04)) argue: '_One needs a realistic view of one's abilities in order to capitalise on personal strengths and compensate for weaknesses_' (p. 269). Hence, most pronounced knowledge development should occur when there is match between self-perceived and actual ability: A high academic self-concept enables intelligent students to make use of their potential and thus facilitates knowledge development. These effects might at least partially be due to greater effort expenditure in contrast to high ability students who underestimate their ability. On the other hand, for less intelligent students, an adequately low academic self-concept indicates that they recognise their deficits, which in turn enables them to develop at least some knowledge through compensatory effort. In contrast, less intelligent students who overestimate their potential might reduce their effort as they don't recognise their deficits and presume that they can succeed with little effort.

With regard to information literacy, knowledge development of over-confident students might be weakened by a reduction of effort through terminating all searches prematurely assuming that all relevant literature has been discovered (less learning by doing), or by simply renouncing to attend to information literacy training. On the other hand, under-confident students cannot make use of their intellectual potential because of the detrimental effects of this underconfidence on their motivation and effort. For example, they might think that no matter how much effort they invest, they won't be able to develop adequate information-seeking skills, or they might just avoid information searching at all. Students with realistic self-views, on the other hand, live up to their potential because of the resulting increased effort.

In sum, this theorizing enables to derive a moderator effect of the academic self-concept on the relationship between cognitive ability (i.e., intelligence) and information literacy: on the one hand, the positive effects of high cognitive ability have full effect if paired with a (realistically) high academic self-concept ([Markus _et al_., 1990](#Mar90)), whereas the negative effects of a low cognitive ability are even intensified by an overly high academic self-concept (due to the detrimental effects of overconfidence). Intelligence and information literacy should thus be positively related for students with a high academic self-concept. On the other hand, the negative effects of a lower intelligence may partially be buffered by a realistically low self-concept (and the resulting increased effort), whereas the positive effects of a higher intelligence are dampened by a low self-concept because of the under-estimation of one's ability ([Markus _et al_., 1990](#Mar90)). Thus, for students with a low academic self-concept, the relationship between intelligence and information literacy should be small to non-significant. A fan-shaped or even crossed interaction would result. Leclerc, Larivée, Archambault, and Janosz ([2010](#Lec10)) were able to confirm the existence of such a moderator effect with regard to math performance. With respect to our theorizing, we assume that the effect may be reproduced with regard to information literacy. Moreover, we expect that student's effort is responsible for (mediates) this moderator effect (see Figure 1).

<figure class="centre">![Figure 1: Conceptual diagram of the moderated mediation effect of Hypothesis 2](isic34fig1.jpg)

<figcaption>Figure 1: Conceptual diagram of the moderated mediation effect of Hypothesis 2</figcaption>

</figure>

Based on our theorising, we suggest the following hypotheses:

_Hypothesis 1_: Academic self-concept moderates the relationship between intelligence and information literacy: the correlation between intelligence and information literacy is higher for students with a high academic self-concept.  
_Hypothesis 2_: The negative effects of ability over- and under-estimation on effort and motivation are responsible for this moderator effect. Thus, in a mediated moderation model, effort mediates the moderating effects of the academic self-concept on the relationship between intelligence and information literacy (see Figure 1).

## Method

### Participants and procedure

Participants were 137 psychology freshmen from University of Trier taking part in a longitudinal study on knowledge development during the first two years of their studies. Mean age was _M_ = 20.43 (_SD =_ 2.53) and ranged from 18 to 31 years. Sex was distributed unevenly with 82 % females and 18 % males, which is nevertheless typical for German psychology students ([Schneller and Schneider, 2005](#Sch05); [Wentura, Ziegler, Scheuer, Bölte, Rammsayer and Salewski, 2013](#Wen13)). Participants were recruited by means of flyers and a personalised email inviting them to take part in a longitudinal study on knowledge development. Data collection was divided into two parts. After registration for the study, subjects received a link to an online module comprising several self-report inventories, including a measure of academic self-concept. This module took approximately 30 minutes and had to be completed before attending a laboratory session in a computer lab at the University of Trier. Data from this laboratory session was collected in groups ranging from 2 to 16 participants and took approximately two hours. Among other measures, participants completed an intelligence test and an information literacy test. Participants received a compensation of ? 25 for their participation.

### Measures

_Information literacy_ was measured using the PIKE-P test (Procedural Information-seeking Knowledge Test - version for students of psychology: [Rosman _et al_., under review](#Ros0); see also [Rosman and Birke, in press](#Ros0a)). The 22-item test focuses procedural knowledge about various aspects of information-seeking (e.g., generation of search keywords, selection of adequate sources, utilisation of limiters and Boolean operators, etc.) by drawing on a situational judgment format. Items begin with a description of a problem situation requiring an information search (e.g., '_You need the following book: "Richard S. Lazarus - Stress, Appraisal, and Coping". How do you proceed?_). Subsequently, four response alternatives (e.g., '_I look up the ISBN-Number of the book and enter it into the library catalog_'; '_I try to locate and download a .pdf of the book using a web search engine_'; etc.) are to be rated on a 5-point Likert-Scale (not useful at all to very useful) with respect to their appropriateness of solving the problem. Subjects' scores are obtained through a standardised scoring key which is based on an expert ranking of the appropriateness of the response alternatives. Scale validity was demonstrated in a validation study ([Rosman , under review](#Ros0a)), in which high correlations (r > .60; p < .001) between the scale and scores achieved in actual information search tasks (see [Leichner, Peter, Mayer and Krampen, in press](#Lei)) were found.

_Fluid intelligence_ was measured by Raven's Advanced Progressive Matrices ([Raven _et al_., 1998](#Rav98)). Each test item consists of a visual pattern with a missing piece, and subjects are invited to complete this pattern by choosing the correct piece from a set of eight alternatives. The test consists of 32 items of increasing difficulty. With reference to the work by Hamel and Schmittmann ([2006](#Ham06)), a time limit of 20 minutes was imposed on the test. Scores were obtained by summing up the number of correctly solved items.

_Academic self-concept_ was measured with the absolute academic self-concept scale from Dickhäuser, Schöne, Spinath and Stiensmeier-Pelster ([2002](#Dic02)), which assesses students' academic self-concept without providing a reference norm (e.g., social or individual reference). Its five items begin by a statement that has to be completed on a 7-point Likert-Scale (e.g., '_My academic ability is . low [1] - high [7]_'; '_In my opinion I am . not intelligent [1] - very intelligent [7]_'). Subjects' scores were obtained by calculating the arithmetic mean of the five items.

_Effort_ was measured with the work avoidance scale of a German self-report measure (Skalen zur Erfassung der Lern-und Leistungsmotivation; SELLMO: [Spinath, Stiensmeier-Pelster, Schöne and Dickhäuser, 2002](#Spi02)). The scale consists of 8 items (e.g, '_In my studies it is important for me to do as little work as possible_') that are to be rated on a 5-point Likert-Scale (totally disagree [1] to totally agree [5]). By referring to '_the goal to invest as little effort as possible_' ([Steinmayr and Spinath, 2009](#Ste09), p. 81), the scale constitutes a reverse coded (high scores = low effort) indicator for study effort. With regard to our mediation hypothesis, this reverse coding does not threaten interpretability. Subjects' scores were again obtained by calculating the arithmetic mean of all items.

## Results

The following section is organized according to our hypotheses: In the first part, we will report the results of the test of the moderation hypothesis. The second part describes tests for mediated moderation. Table 1 shows descriptive statistics and intercorrelations of all study variables.

<table class="center"><caption>  
Table 1: Means (_M_), standard deviations (_SD_), internal consistencies (values on the diagonal), and intercorrelations of study variables  
</caption>

<tbody>

<tr>

<th>Construct</th>

<th>M</th>

<th>SD</th>

<th>1</th>

<th>2</th>

<th>3</th>

<th>4</th>

</tr>

<tr>

<td>1 Information literacy</td>

<td style="text-align:center">47.05</td>

<td style="text-align:center">7.23</td>

<td style="text-align:center">(0.50)</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>2 Fluid intelligence</td>

<td style="text-align:center">21.01</td>

<td style="text-align:center">3.72</td>

<td style="text-align:center">.011</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>3 Academic self-concept</td>

<td style="text-align:center">4.70</td>

<td style="text-align:center">0.83</td>

<td style="text-align:center">-0.02</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">(0.84)</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>4 Work avoidance</td>

<td style="text-align:center">1.89</td>

<td style="text-align:center">0.59</td>

<td style="text-align:center">-0.30**</td>

<td style="text-align:center">0.12</td>

<td style="text-align:center">-0.15*</td>

<td style="text-align:center">(0.87)</td>

</tr>

<tr>

<td colspan="7">_N_ = 137; * _p_ < 0.05; ** _p_ < 0.01.</td>

</tr>

</tbody>

</table>

### Hypothesis 1

Hypothesis 1 was tested by adding an interaction term into a multiple regression predicting information literacy from z-standardised intelligence and academic self-concept scores ([Aiken and West, 1991](#Aik91)). As depicted in Table 2, the interaction term was statistically significant. An inspection of the corresponding regression plot (Figure 2) revealed that the moderator effect was in the hypothesised direction: a positive relationship between intelligence and information literacy was found for subjects with a high (+1 _SD_) academic self-concept, whereas the relationship between intelligence and information literacy is close to zero for subjects with a low (-1 _SD_) academic self-concept. The interaction was crossed (and not just fan-shaped; see Figure 2), so that information literacy for subjects with a low intelligence and a high academic self-concept is even inferior to the respective scores of subjects with a low intelligence and a (realistically) low academic self-concept. As suggested by Aiken and West ([1991](#Aik91)), the graphic interpretation was verified by a simple slope test: for subjects with a high academic self-concept, the slope of the relationship between intelligence and information literacy was significant (_B_ = 2.38; _t_ = 2.72; _p_ < 0.01). For subjects with a low academic self-concept, the respective relationship was - as hypothesised - not significant. Hypothesis 1 is therefore fully supported.

<table class="center"><caption>  
Table 2: Multiple regression predicting information literacy from fluid intelligence (independent variable), academic self-concept (moderator), and their interaction term</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="3">Information Literacy</th>

</tr>

<tr>

<th>_ß_</th>

<th>R<sup>2</sup></th>

<th>_?R_<sup>2</sup></th>

</tr>

<tr>

<td>Block 1</td>

<td></td>

<td style="text-align:center">0.01</td>

<td></td>

</tr>

<tr>

<td>Fluid intelligence (z-standardised)</td>

<td style="text-align:center">0.11</td>

<td rowspan="2" colspan="2"> </td>

</tr>

<tr>

<td>Academic self-concept (z-standardised)</td>

<td style="text-align:center">-0.03</td>

</tr>

<tr>

<td>Block 2</td>

<td></td>

<td style="text-align:center">0.06**</td>

<td style="text-align:center">0.05**</td>

</tr>

<tr>

<td>Fluid intelligence (z-standardised)</td>

<td style="text-align:center">0.16*</td>

<td rowspan="3" colspan="2"> </td>

</tr>

<tr>

<td>Academic self-concept (z-standardised)</td>

<td style="text-align:center">0.03</td>

</tr>

<tr>

<td>APMxASC (interaction term)</td>

<td style="text-align:center">0.22**</td>

</tr>

<tr>

<td colspan="4">_N_ = 137; Method = Enter; _ß_ = standardised regression weight; _R_<sup>2</sup> = total variance explained; ?R<sup>2</sup> = change in _R_<sup>2</sup> from block 1 to block 2; * _p_ < 0.05; ** _p_ < 0.01.)</td>

</tr>

</tbody>

</table>

<figure class="centre">![Figure 2: Moderator effect of academic self-concept (ASC) on the relationship between fluid intelligence (Raven's APM) and information literacy (PIKE-P)](isic34fig2.jpg)

<figcaption>Figure 2: Moderator effect of academic self-concept (ASC) on the relationship between fluid intelligence (Raven's APM) and information literacy (PIKE-P)</figcaption>

</figure>

### Hypothesis 2

To test for mediated moderation, we adopted the procedure by Muller, Judd and Yzerbyt ([2005](#Mul05)), which relies on multiple regression. Their first criterion for mediated moderation is that the interaction of the independent variable (intelligence) and the moderator (academic self-concept) significantly predicts the dependent variable (information literacy). As shown in Table 2, this criterion was met (see also results of the test of hypothesis 1). The second criterion requires that the interaction between independent variable and moderator significantly predicts the mediator (work avoidance). Table 3 shows that this criterion is fulfilled. The third criterion requires that the mediator significantly predicts the dependent variable while controlling for both the interactions between (1) the moderator and the independent variable and (2) the moderator and the mediator. As shown in Table 4, this criterion was also met. As a fourth criterion, it is required that the _ß_-weight of the interaction term between independent variable and moderator is reduced when the interaction term of mediator and moderator as well as the mediator are included in the regression predicting the dependent variable (compare Tables 2 and 4). Even though it stayed significant, the effect of this interaction term on the dependent variable decreased from _ß_ = .22 (p < .01) to _ß_ = 0.18 (p < 0.05), which indicates partial mediated moderation. As Muller _et al_. ([2005](#Mul05)) do not provide any technique to test this decrease for significance, additional analyses were conducted by means of the PROCESS macro (model 4; manually calculated interaction term; 10.000 bootstrap samples; 95 % confidence intervals) provided by Hayes ([2013](#Hay13)). The procedure confirmed the existence of mediated moderation. As bootstrap confidence intervals of the mediated effect did not include 0 (a<sub>3</sub>b = .11; CI = 0.02 to 0.22), it can be concluded that the hypothesised mediator effect is indeed statistically significant. Hypothesis 2 is supported.

<table class="center"><caption>  
Table 3: Multiple regression predicting work avoidance (mediator) from intelligence (independent variable), academic self-concept (moderator), and their interaction term</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="3">Work avoidance</th>

</tr>

<tr>

<th>_ß_</th>

<th>_R<sup>2</sup>_</th>

<th>_?R<sup>2</sup>_</th>

</tr>

<tr>

<td>Block 1</td>

<td></td>

<td style="text-align:center">0.04*</td>

<td></td>

</tr>

<tr>

<td>Fluid intelligence (z-standardised)</td>

<td style="text-align:center">0.13</td>

<td rowspan="2" colspan="2"> </td>

</tr>

<tr>

<td>Academic self-concept (z-standardised)</td>

<td style="text-align:center">-0.16*</td>

</tr>

<tr>

<td>Block 2</td>

<td></td>

<td style="text-align:center">.07*</td>

<td style="text-align:center">0.03*</td>

</tr>

<tr>

<td>Fluid intelligence (z-standardised)</td>

<td style="text-align:center">0.09</td>

<td rowspan="3" colspan="2"> </td>

</tr>

<tr>

<td>Academic self-concept (z-standardised)</td>

<td style="text-align:center">-0.21**</td>

</tr>

<tr>

<td>APMxASC (interaction term)</td>

<td style="text-align:center">-0.20*</td>

</tr>

<tr>

<td colspan="4">_N_ = 137; Method: Enter; _ß_ = standardised regression weight; R<sup>2</sup> = total variance explained; ?R<sup>2</sup> = change in _R<sup>2</sup>_ from block 1 to block 2; * _p_ < 0.05; ** _p_ < 0.01.</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table 4: Multiple regression predicting information literacy from fluid intelligence (independent variable), academic self-concept (moderator), work avoidance (mediator), and both interaction terms</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="2">Information literacy</th>

</tr>

<tr>

<th>_ß_</th>

<th>_R<sup>2</sup>_</th>

</tr>

<tr>

<td>Block 1</td>

<td></td>

<td style="text-align:center">0.15**</td>

</tr>

<tr>

<td>Fluid intelligence (z-standardised)</td>

<td style="text-align:center">0.18*</td>

<td rowspan="5"> </td>

</tr>

<tr>

<td>Academic self-concept (z-standardised)</td>

<td style="text-align:center">-0.03</td>

</tr>

<tr>

<td>Work avoidance (z-standardised)</td>

<td style="text-align:center">-0.32**</td>

</tr>

<tr>

<td>APMxASC</td>

<td style="text-align:center">-0.18*</td>

</tr>

<tr>

<td>WAxASC</td>

<td style="text-align:center">-0.05</td>

</tr>

<tr>

<td colspan="3">_N_ = 137; Method: Enter; APMxASC = interaction term of fluid intelligence and academic self-concept; WAxASC = interaction term of work avoidance and academic self-concept; _ß_ = standardised regression weight; _R<sup>2</sup>_ = total variance explained; * _p_ < 0.05; ** _p_ < 0.01.</td>

</tr>

</tbody>

</table>

## Discussion

The purpose of the present study was to investigate whether an adequate perception of one's academic ability is essential in information-seeking knowledge development. Consistent with our theorizing, a high academic self-concept should be beneficial for students with high cognitive ability, whereas it may even have detrimental effects on lower ability students. A low academic self-concept, on the other hand, should have detrimental effects on the performance of high ability students but be beneficial for students with lower ability. This reasoning implies a moderator effect of the academic self-concept on the relationship between intelligence and information literacy (Hypothesis 1), which was supported empirically. We attributed this moderator effect (1) on the positive effects of an adequate ability self-perception on effort and motivation, and (2) on the negative effects of under- and overconfidence on effort and motivation (Hypothesis 2). A mediated-moderation analysis confirmed that effort is indeed one of the mechanisms responsible for the moderator effect postulated in Hypothesis 1, thus confirming Hypothesis 2.

### Hypothesis 1: Moderator effect of the academic self-concept

The significant moderator effect in hypothesis 1 implies that, for subjects with a high academic self-concept, intelligence and information literacy are positively related. In the case of a low academic self-concept, however, this relationship becomes non-significant. Our results show that the findings of Leclerc _et al_. ([2010](#Lec10)) can indeed be generalised to the domain of information literacy. The crossed nature of the interaction (see Figure 2) indicates that a high self-concept paired with low intelligence (overconfidence) impairs performance even stronger than an adequately low self-concept together with a low intelligence. Likewise, information literacy of high ability students with a high academic self-concept is higher than the respective levels of high ability but under-confident students. The findings thus confirm that realistic academic self-perceptions are desirable.

### Hypothesis 2: Mediator effect of effort on the moderator effect of the academic self-concept

Effort partially mediating the moderator effect of hypothesis 1 indicates that effort is - at least to a certain extent - responsible for this moderator effect. As long as their self-concept matches their ability, students devote increased effort to their studying, which in turn promotes knowledge development. Over- and under-confident students, however, are likely to reduce their effort, as they don't recognise their weaknesses or their potential and thus fail to act accordingly. Hence, our findings show that the common assumption of a high academic self-concept (or, more generally, high self-confidence) _always_ being beneficial (e.g., [Marsh and Seaton, 2013](#Mar13)) is short-sighted. In fact, our data suggests that overconfidence (overly high academic self-concept) leads to a decrease in both effort and performance relative to students who assess their ability in a more realistic way.

### Limitations and implications for further research

Even though we took great care in collecting, analysing and interpreting our data, some potential limitations to our efforts should not be withheld. First, Cronbach's Alpha of the PIKE-P test was very low in the present study. As the test has proven to be reliable with two different samples of psychology students (see [Rosman _et al_., under review](#Ros)), this might be caused by the reduced variability in our data as our sample only comprised freshmen. With respect to a low reliability usually deflating (and therefore masking) relationships between variables ([Carmines and Zeller, 1979](#Car79); [Schmitt, 1996](#Sch96)), we nevertheless do not expect this issue to severely bias our findings.

A more serious limitation, however, concerns our sample, as we solely investigated psychology freshmen from one single university. The uneven sex distribution - although typical for German psychology students - aggravates this limitation and impairs generalizability. Additionally, one can argue that freshmen are not well-suited to investigate the development of information-seeking knowledge, as they presumably do not have much experience in information searching yet. However, with a certain amount of variance and no floor effects in our data, it is plausible to assume that most students had already acquired basic information-seeking knowledge prior to and in the beginning of their studies (e.g., by performing scholarly information searches during their last high school years).

A third potential limitation is that the PIKE-P test used to investigate information literacy solely focuses procedural knowledge about information searching as such, leaving aside evaluation and utilisation of the discovered information. Thus, our study does not account for the whole breadth of the information literacy construct as defined by the ACRL ([2000](#Arc00)).

Finally, our mediated moderation analysis showed that effort is only partly responsible for the moderator effect. Even though partial mediation is much more common than full mediation ([Hayes, 2013](#Hay13)), it could prove fruitful to investigate other variables that might account for the moderator effect of the academic self-concept. For example, Markus _et al_. ([1990](#Mar90)) argued that under-confident students are more hesitating and set less challenging goals. A mediated moderation analysis including these variables could shed more light on the mechanisms of the positive effects of an adequate self-assessment. Furthermore, research should concentrate on replicating our findings in different domains and with different information literacy measures. As our theoretical reasoning about the beneficial effects of an accurate self-perception can easily be expanded to other domains than information literacy, replicating our findings with regard to knowledge development in other disciplines (e.g., physics, math or languages) or study achievement in general could also prove fruitful. Additionally, longitudinal studies investigating the impact of self-perception adequacy on knowledge development are desirable.

### Practical implications

Several conclusions can be drawn from the present investigation. First, the findings explain that an accurate ability self-perception is preferable over excessively high self-perceptions: a high academic self-concept is only beneficial if these self-perceptions are realistically founded. Efforts to identify unrealistic self-perceptions should therefore be made. Additionally, as a high academic self-concept enables high ability students to make use of their potential, these students should be subject to interventions that foster their self-perceptions. An approach that directly enhances academic self-concept was developed by Craven, Marsh and Debus ([1991](#Cra91)) and focuses on performance feedback through effective praising strategies (e.g., credible praising with respect to specific accomplishments and attributing these accomplishments to effort and ability; [Brophy, 1981](#Bro81)). A second, indirect approach of fostering students' academic self-concepts consists in the training of academic skills (e.g., learning strategies, problem solving, or information skills). In accordance with the so-called reciprocal effects model, the resulting academic success in turn positively reflects on the academic self-concept ([Guay _et al_., 2003](#Gua03)).

With regard to low-ability students who overestimate their ability, the situation is more precarious. Pajares and Kranzler ([1995](#Paj95)) strongly discourage efforts to directly lower such students' self-perceptions (for example through negative feedback). As such methods might lead to anxiety and disengagement, we fully agree with their point. As the present paper points out that over-confident students likely do not invest enough effort in academic matters, communicating information searching as being a rather difficult task (which surely is the case) might however make sense. Consistent with effort calculation theory (see above; [Heider, 1958](#Hei58)), an increase in subjective task difficulty will - all other things being equal - lead to increased effort and thus better learning.

In sum, when developing information literacy instruction programmes, we have to keep in mind that what is good for some students (e.g., praise for under-confident students) might be bad for others (e.g., over-confident students). Practitioners should therefore focus on the assessment of objective as well as subjective individual abilities and give individually tailored support and performance feedback. To teach procedural knowledge about a complex skill set like information literacy, it is of crucial importance that students _actively_ participate in the training, for example by conducting their own searches, and receive feedback about their performance to be able to develop realistic academic self-perceptions (see also [Rosman, Mayer and Krampen, 2014](#Ros14)). Under such conditions, the benefits of _learning by doing_ have full effect. To conclude, we plead for a stronger integration of individualised information literacy instruction into college and university courses, so that high ability students can reach excellence and less fortunate students are not unnecessarily impeded in their skill development.

## Acknowledgements

Research was funded by the German Joint Initiative for Research and Innovation with a grant acquired in the Leibniz Competition 2013.

## About the authors

**Tom Rosman** is Research Associate and PhD student at the Leibniz Institute for Psychology Information (ZPID), Trier, Germany. He received his diploma in Psychology from the University of Trier. His research focuses on information behaviour, epistemological beliefs, and knowledge development. He can be contacted at: [rosman@zpid.de](mailto:rosman@zpid.de)  
**Anne-Kathrin Mayer** is head of the Research Department at the Leibniz Institute for Psychology Information (ZPID), Trier, Germany. She received both her diploma in Psychology and her PhD from the University of Trier. Her research focuses on information behaviour, psychological assessment, and intergenerational relationships. She can be contacted at: [mayer@zpid.de](mailto:mayer@zpid.de)  
**Günter Krampen** is Professor for Clinical Psychology, Psychotherapy, and Scientometrics at the University of Trier, Germany, and director of the Leibniz Institute for Psychology Information (ZPID) in Trier. He received his diploma in Psychology from the University of Trier and his PhD from the University of Erlangen-Nürnberg, Germany. His research focuses on clinical psychology, psychological assessment, scientometrics, and history of psychology. He can be contacted at: [krampen@zpid.de](mailto:krampen@zpid.de)

</section>

#### References

*   ACRL (2000). _Information literacy standards for higher education_. Retrieved from http://www.ala.org/acrl/files/standards/standards.pdf on 30 March 2014 (Archived by WebCite® at http://www.webcitation.org/6Rluliej1).
*   Aiken, L.S. & West, S.G. (1991). _Multiple regression: Testing and interpreting interactions_. Newbury Park, CA: Sage.
*   Andretta, S. (2005). _Information literacy: A practitioner's guide_. Oxford: Chandos Publishing.
*   Bandura, A. (1997). _Self-efficacy: The exercise of control_. New York, NY: Freeman.
*   Brand-Gruwel, S., Wopereis, I. & Vermetten, Y. (2005). Information problem solving by experts and novices: Analysis of a complex cognitive skill. _Computers in Human Behavior, 21_(3), 487-508.
*   Brophy, J. (1981). Teacher praise: A functional analysis. _Review of Educational Research, 51_(1), 5-32.
*   Bruce, C. (2004) Information literacy as a catalyst for educational change: A background paper. In Patrick A. Danaher (Ed.), _Proceedings 'Lifelong Learning: Whose responsibility and what is your contribution?' of the 3rd International Lifelong Learning Conference_ (pp. 8-19). Yeppoon, Australia: Central Queensland University Press.
*   Campbell, S. (2008). Defining Information Literacy in the 21st Century. In J. Lau (Ed.), _Information literacy: International perspectives_ (pp. 17-26). Munich, Germany: K. G. Saur Verlag.
*   Carmines, E.G. & Zeller, R.A. (1979). _Reliability and validity assessment_. Thousand Oaks, CA: Sage.
*   Carpenter, P.A., Just, M.A. & Shell, P. (1990). What one intelligence test measures: A theoretical account of the processing in the Raven Progressive Matrices Test. _Psychological Review, 97_(3), 404-431.
*   Craven, R.G., Marsh, H.W. & Debus, R.L. (1991). Effects of internally focused feedback and attributional feedback on enhancement of academic self-concept. _Journal of Educational Psychology, 83_(1), 17-27.
*   Detlor, B., Julien, H., Willson, R., Serenko, A. & Lavallee, M. (2011). Learning outcomes of information literacy instruction at business schools. _Journal of the American Society for Information Science and Technology, 62_(3), 572-585.
*   Dickhäuser, O., Schöne, C., Spinath, B. & Stiensmeier-Pelster, J. (2002). Die Skalen zum akademischen Selbstkonzept: Konstruktion und Überprüfung eines neuen Instrumentes [Construction and evaluation of a new tool assessing the academic self-concept]. _Zeitschrift für Differentielle und Diagnostische Psychologie, 23_(4), 393-405.
*   Dunlosky, J. & Rawson, K.A. (2012). Overconfidence produces underachievement: Inaccurate self evaluations undermine students' learning and retention. _Learning and Instruction, 22_(4), 271-280.
*   Eisenberg, M.B. (2008). Information literacy: Essential skills for the information age. _DESIDOC Journal of Library & Information Technology, 28_(2), 39-47.
*   Feldhusen, J.F. & Hoover, S.M. (1986). A conception of giftedness: Intelligence, self concept and motivation. _Roeper Review, 8_(3), 140-143.
*   Freund, P.A. & Kasten, N. (2012). How smart do you think you are? A meta-analysis on the validity of self-estimates of cognitive ability. _Psychological Bulletin, 138_(2), 296-321.
*   Guay, F., Marsh, H.W. & Boivin, M. (2003). Academic self-concept and academic achievement: Developmental perspectives on their causal ordering. _Journal of Educational Psychology, 95_(1), 124-136.
*   Guay, F. & Vallerand, R.J. (1996). Social context, student's motivation, and academic achievement: Toward a process model. _Social Psychology of Education, 1_(3), 211-233.
*   Hamel, R. & Schmittmann, V.D. (2006). The 20-minute version as a predictor of the Raven Advanced Progressive Matrices Test. _Educational and Psychological Measurement, 66_(6), 1039-1046.
*   Hayes, A.F. (2013). _Introduction to mediation, moderation, and conditional process analysis: A regression-based approach_. New York, NY: Guilford Press.
*   Heider, F. (1958). _The psychology of interpersonal relations_. Hoboken, NJ: Wiley.
*   Ivanitskaya, L., O'Boyle, I. & Casey, A.M. (2006). Health information literacy and competencies of information age students: Results from the interactive online Research Readiness Self-Assessment (RRSA). _Journal of Medical Internet Research, 8_(2), paper e6\. Retrieved from http://www.jmir.org/2006/2/e6/ (Archived by WebCite® at http://www.webcitation.org/6RluvZBlx).
*   Jensen, A.R. (1998). _The g factor: The science of mental ability_. Westport, CT: Praeger.
*   Jonassen, D.H. & Grabowski, B.L. (1993). _Handbook of individual differences, learning, and instruction_. Hillsdale, NJ: Erlbaum.
*   Kuncel, N.R., Hezlett, S.A. & Ones, D.S. (2001). A comprehensive meta-analysis of the predictive validity of the graduate record examinations: Implications for graduate student selection and performance. _Psychological Bulletin, 127_(1), 162-181.
*   Leclerc, M., Larivée, S., Archambault, I. & Janosz, M. (2010). Le sentiment de compétence, modérateur du lien entre le QI et le rendement scolaire en mathématiques [The sense of competency, moderator of the link between IQ and academic achievement in mathematics]. _Canadian Journal of Education, 33_(1), 31-56.
*   Leichner, N., Peter, J., Mayer, A.-K. & Krampen, G. (in press). Assessing information literacy using information search tasks. Journal of Information Literacy.
*   Lenox, M.F. & Walker, M.L. (1993). Information literacy in the educational process. _The Educational Forum, 57_(3), 312-324.
*   Limberg, L. & Sundin, O. (2006). Teaching information seeking: Relating information literacy education to theories of information behaviour. Information Research, 12(1), paper 280\. Retrieved from http://www.informationr.net/ir/12-1/paper280.html (Archived by WebCite® at http://www.webcitation.org/6Rlv3FdrA).
*   Lloyd, A. (2005). Information literacy: Different contexts, different concepts, different truths? _Journal of Librarianship and Information Science, 37_(2), 82-88.
*   Mackey, T.P. & Jacobson, T.E. (2011). Reframing information literacy as a metaliteracy. _College & Research Libraries, 72_(1), 62-78.
*   Markus, H., Cross, S.E. & Wurf, E. (1990). The role of the self-system in competence. In R.J. Sternberg & J. Kolligian, Jr. (Eds.), _Competence considered_ (pp. 205-226). New Haven, CT: Yale University Press.
*   Marsh, H.W. (1993). Academic self-concept: Theory, measurement, and research. In J. M. Suls (Ed.), _The self in social perspective. Psychological perspectives on the self (Vol. 4)_ (pp. 59-98). Hillsdale, UK: Lawrence Erlbaum Associates.
*   Marsh, H.W. & Seaton, M. (2013). Academic self-concept. In J. Hattie & E.M. Anderman (Eds.), _International guide to student achievement_ (pp. 62-63). New York, NJ: Routledge.
*   Mayer, R. E. (2011). Intelligence and achievement. In R.J. Sternberg & S.B. Kaufman (Eds.), _The Cambridge handbook of intelligence_ (pp. 738-747). New York, NJ: Cambridge University Press.
*   Moores, T.T. & Chang, J.C.J. (2009). Self-efficacy, overconfidence, and the negative effect on subsequent performance: A field study. _Information & Management, 46_(2), 69-76.
*   Muller, D., Judd, C.M. & Yzerbyt, V.Y. (2005). When moderation is mediated and mediation is moderated. _Journal of Personality and Social Psychology, 89_(6), 852-863.
*   Nokes, T.J. & Ohlsson, S. (2005). Comparing multiple paths to mastery: _What is learned? Cognitive Science, 29_(5), 769-796.
*   Pajares, F. & Kranzler, J. (1995). Self-efficacy beliefs and general mental ability in mathematical problem-solving. _Contemporary Educational Psychology, 20_(4), 426-443.
*   Plucker, J.A., Robinson, N.M., Greenspon, T.S., Feldhusen, J.F., McCoach, D.B. & Subotnik, R.F. (2004). It's not how the pond makes you feel, but rather how high you can jump. _American Psychologist, 59_(4), 268-269.
*   Raven, J., Raven, J.C. & Court, J.H. (1998). _Raven manual section 4: Advanced Progressive Matrices_. Oxford: Oxford Psychologists Press.
*   Reis, S.M. & McCoach, D.B. (2000). The underachievement of gifted students: What do we know and where do we go? _Gifted Child Quarterly, 44_(3), 152-170.
*   Robins, R.W. & Beer, J.S. (2001). Positive illusions about the self: Short-term benefits and long-term costs. _Journal of Personality and Social Psychology, 80_(2), 340-352.
*   Rosenfeld, P., Salazar-Riera, N. & Vieira, D. (2002). Piloting an information literacy program for staff nurses: Lessons learned. _Computers Informatics Nursing, 20_(6), 236-241.
*   Rosman, T., Mayer, A.-K. & Krampen, G. (2014). Combining self-assessments and achievement tests in information literacy assessment: Empirical results and recommendations for practice. _Assessment & Evaluation in Higher Education_. Advance online publication.
*   Rosman, T., Mayer, A.-K. & Krampen, G. (under review). _Measuring psychology students' information-seeking skills in a situational judgment test format: Construction and validation of the PIKE-P Test_.
*   Rosman, T. & Birke, P. (in press). Fachspezifische Erfassung von Recherchekompetenz durch prozedurale Wissenstests: Psychologie vs. Informatik [Discipline-specific assessment of information-seeking ability with procedural knowledge tests: Psychology vs. computer science]. In Mayer, A.-K. (Ed.), _Informationskompetenz im Hochschulkontext - Interdisziplinäre Forschungsperspektiven_. Lengerich, Germany: Pabst Science Publishers.
*   Schmitt, N. (1996). Uses and abuses of coefficient alpha. _Psychological Assessment, 8_(4), 350-353.
*   Schneller, K. & Schneider, W. (2005). Bundesweite Befragung der Absolventinnen und Absolventen des Jahres 2003 im Studiengang Psychologie [National survey among psychology graduates in 2003]. _Psychologische Rundschau, 56_(2), 159-170.
*   Sohn, Y.W., Doane, S.M. & Garrison, T. (2006). The impact of individual differences and learning context on strategic skill acquisition and transfer. _Learning and Individual Differences, 16_(1), 13-30.
*   Spinath, B., Stiensmeier-Pelster, J., Schöne, C. & Dickhäuser, O. (2002). Die Skalen zur Erfassung von Lern- und Leistungsmotivation (SELLMO) [Measurement scales for learning and performance motivation]. Göttingen, Germany: Hogrefe.
*   Steinmayr, R. & Spinath, B. (2009). The importance of motivation as a predictor of school achievement. _Learning and Individual Differences, 19_(1), 80-90.
*   Streatfield, D., Allen, D. & Wilson, T. (2010). Information literacy training for postgraduate and postdoctoral researchers: A national survey and its implications. _Libri, 60_(3), 230-240.
*   Van Boxtel, H.W. & Mönks, F.J. (1992). General, social, and academic self-concepts of gifted adolescents. _Journal of Youth and Adolescence, 21_(2), 169-186.
*   Wentura, D., Ziegler, M., Scheuer, A., Bölte, J., Rammsayer, T. & Salewski, C. (2013). Bundesweite Befragung der Absolventinnen und Absolventen des Jahres 2011 im Studiengang BSc Psychologie [German nationwide survey of bachelor's degree graduates in Psychology in 2011]. _Psychologische Rundschau, 64_(2), 103-112.
*   Zakay, D. & Glicksohn, J. (1992). Overconfidence in a multiple-choice test and its relationship to achievement. _Psychological Record, 42_(4), 519-524.

</article>