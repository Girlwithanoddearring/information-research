<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
<link rel="stylesheet" href="style.css">
    <title>
      Ecological modelling of individual and contextual influences: a person-in-environment framework for hypothetico-deductive information behaviour research
    </title>
    <meta charset="utf-8" />
    <meta name="dcterms.title" content="Ecological modelling of individual and contextual influences: a person-in-environment framework for hypothetico-deductive information behaviour research" />
    <meta name="author" content="Sei-Ching, Joanna Sin" />
    <meta name="dcterms.subject" content="" />
    <meta name="description" content="This paper discusses the person-in-environment framework, which proposes the inclusion of environmental factors, alongside personal factors, as the explanatory factors of individual-level information behaviour and outcome. The paper first introduces the principles and schematic formulas of the person-in-environment framework. It then presents the findings of an empirical verification study. A multi-way ANOVA test was conducted to verify the the person-in-environment framework framework. The main and interaction effects of eight individual and information environment variables on individual academic performance were tested. Four main effects (baseline academic grade, outcome expectation, home computer resources, and public library usage) and two interaction effects (home computer resources x public library usage; public library usage x neighbourhood public library resource level) were significant. The person-in-environment framework can help identify significant environmental interaction effects that would have been missed in studies that included only personal factors." />
    <meta name="keywords" content="information behaviour, information environment, research methods" />
    <meta name="robots" content="all" />
    <meta name="dcterms.publisher" content="Professor T.D. Wilson" />
    <meta name="dcterms.type" content="text" />
    <meta name="dcterms.identifier" content="ISSN-1368-1613" />
    <meta name="dcterms.identifier" content="http://InformationR.net/ir/20-1/isic2/isicsp12.html" />
    <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/20-1/infres201.html" />
    <meta name="dcterms.format" content="text/html" />
    <meta name="dc.language" content="en" />
    <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />
    <meta name="dcterms.issued" content="2015-03-15" />
    <meta name="geo.placename" content="global" />
</head>
<body>
<header>
<h4 id="vol-20-no-1-march-2015">vol. 20 no. 1, March, 2015</h4>
</header>
<article>
<h1 id="ecological-modelling-of-individual-and-contextual-influences-a-person-in-environment-framework-for-hypothetico-deductive-information-behaviour-research">Ecological modelling of individual and contextual influences: a person-in-environment framework for hypothetico-deductive information behaviour research</h1>
<h4 id="sei-ching-joanna-sin"><a href="#author">Sei-Ching Joanna Sin</a></h4>
<p>Division of Information Studies, Wee Kim Wee School of Communication and Information, Nanyang Technological University. 31 Nanyang Link, Singapore 637718, Singapore</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> This paper discusses the person-in-environment framework, which proposes the inclusion of environmental factors, alongside personal factors, as the explanatory factors of individual-level information behaviour and outcome.<br>
<strong>Method.</strong> The paper first introduces the principles and schematic formulas of the person-in-environment framework. It then presents the findings of an empirical verification study.<br>
<strong>Analysis.</strong> A multi-way ANOVA test was conducted to verify the person-in-environment framework. The main and interaction effects of eight individual and information environment variables on individual academic performance were tested.<br>
<strong>Results.</strong> Four main effects (baseline academic grade, outcome expectation, home computer resources, and public library usage) and two interaction effects (home computer resources x public library usage; public library usage x neighbourhood public library resource level) were significant.<br>
<strong>Conclusions.</strong> The the person-in-environment framework framework can help identify significant environmental interaction effects that would have been missed in studies that included only personal factors.</p>
</blockquote>
<section>
<h2 id="introduction">Introduction</h2>
<p>The importance of capturing both personal and contextual influences is firmly recognised in information behaviour research. However, structural-environmental factors are not well explored (<a href="#cou07">Courtright, 2007</a>; <a href="#fid12">Fidel, 2012</a>; <a href="#tal99">Talja, Keso, and Pietilainen, 1999</a>; <a href="#vak88">Vakkari, 1997</a>). While holistic conceptual frameworks have been proposed, they are more successfully applied in inductive or qualitative studies rather than in hypothetico-deductive research. Few individual-level information behaviour studies have used a deductive approach to test variables at different units of observation simultaneously (i.e., variables about an individual and variables about that individual's life environment). This research gap hinders a comprehensive understanding of how factors and mechanisms at different levels affect individual information behaviour and the potential points of intervention. This paper aims to briefly introduce and test the person-in-environment framework, which is a conceptual and methodological framework for testing the relationships between individual variables, structural environmental influences and individual information behaviour.</p>
<h2 id="person-in-environment-conceptual-framework">&gt;Person-in-environment: conceptual framework</h2>
<p>The framework includes four core classes of constructs: person, environment, information behaviour, and outcome. The person-in-environment framework holds that there is reciprocity among personal and environmental factors (axiom 1), and that all personal and environmental factors suggest potentiality, not absolute deterministic influence (axiom 2).The relationships between the four classes of constructs are schematised in a heuristic formula (Figure 1) in which outcome (O) is the joint function (<em>f</em> ) of the reciprocal relationships among person (P), environment (E) and information behaviour (IB), plus the influence from random variables (? ) that affect this outcome by chance.</p>
<figure class="centre">![Figure 1: Schematic formula for <em>outcome</em> (Formula 1)](isicsp12fig1.jpg)
<figcaption>Figure 1: Schematic formula for _outcome_ (Formula 1)</figcaption>
</figure>
<p>Formula 1 is built in part from Lewin's field theory formula, <em>Be = F</em> [_P,_<em>E</em> ] (<a href="#lew39">Lewin, 1939</a>), Bronfenbrenner's (<a href="#bro05">2005</a>) ecological systems theory formula, D = f(PE), and Bandura's (<a href="#ban78">1978</a>) idea of reciprocal causations among person, environment and behaviour. The person-in-environment framework framework is different from the aforementioned formulas in that it includes random variables (?), which underscores the framework's non-deterministic nature. The time dimension is explicated in the framework by unpacking heuristic Formula 1 into two subparts and adding time notations (Figure 2). Formula 1.1 represents: information behaviour at time 1 is the joint function of personal factors at time 0 and environmental factors at time 0, plus random factors that incidentally influence the individual's information behaviour. Formula 1.2 is interpreted as follows: outcome at time 2 is the joint function of the interaction among personal factors, environmental factors and information behaviour, all at time 1, plus random factors that incidentally influence the outcome.</p>
<figure class="centre">![Figure 2: Time-notated schematic formulas](isicsp12fig2.jpg)
<figcaption>Figure 2: Time-notated schematic formulas (Formulas 1.1 and 1.2)</figcaption>
</figure>
<p>The person-in-environment framework proposes six research design principles: (1) The individual is the focus of research. The recommended unit of analysis is at the individual level; (2) environmental factors should be included as explanatory variables; (3) the information environment is a crucial category of explanatory variables; (4) interactionism (not determinism) among personal and environmental factors; (5) the choice of unit of observation for environmental factors is flexible; and (6) inclusion of both emic and etic measures. Further discussion can be found in Sin (<a href="#sin09">2009</a>, <a href="#sin11">2011</a>).</p>
<h2 id="empirical-verification-of-the-person-in-environment-framework">Empirical verification of the person-in-environment framework</h2>
<p>The study tested the main and interaction effects of eight variables on the academic performance of U.S. high school students. The eight variables are: baseline academic performance, socioeconomic status, self-efficacy, student's outcome expectation, home computer resources, resource levels of neighbourhood public libraries, accessibility of neighbourhood public libraries, public library usage. Personal variables were drawn from Bandura's social cognitive theory (<a href="#ban01">Bandura, 2001</a>; <a href="#len94">Lent, Brown and Hackett, 1994</a>). Environmental variables focused on an individual's home and neighbourhood library environment that were found to be salient in previous studies (<a href="#hem06">Hemmeter, 2006</a>; <a href="#sin12">Sin, 2012</a>; <a href="#vak88">Vakkari, 1988</a>). In terms of research method, the study used secondary analysis. Data sources included the education longitudinal study, which included a nationally representative sample of more than 13,000 high school sophomores in 2002. The same respondents were surveyed again in 2004 and 2006 (<a href="#ing07">Ingels <em>et al</em>., 2007</a>). Environmental variables were drawn from U.S. census data and the public libraries survey (<a href="#kro06">Kroe <em>et al</em>., 2006</a>). A multi-way ANOVA was used.</p>
<h2 id="findings">Findings</h2>
<p>Four main effects and two interaction effects were found to be significant. The variables together explained 40.8% of the variance. The significant main effects are: baseline academic grade [ <em>F</em>(3, 10497) = 317.44, <em>p</em>=0.000 ], outcome expectation [ <em>F</em>(1, 10497)=5.11, <em>p</em>=0.024 ], home computer resources [ <em>F</em>(1, 10497) = 9.10, <em>p</em>=0.003 ] and public library usage [ <em>F</em>(1, 10497) = 4.07, <em>p</em>=0.044 ]. All four showed a positive relationship with the outcome variable.</p>
<figure class="centre">![Figure 3: Interaction plots](isicsp12fig3.jpg)
<figcaption>Figure 3: Interaction plots</figcaption>
</figure>
<p>The left panel of Figure 3 shows the significant interaction effect between home computer resources and public library usage [ <em>F</em>(1, 10497) = 4.23, <em>p</em>=0.040 ]. Overall, frequent public library users have better grades than infrequent public library users. The interaction effects show that positive influence of public library usage on academic performance is found among students who do not have computers at home, but not among those with home computers.</p>
<p>The right panel of Figure 3 shows the significant interaction effect between a student's public library usage and the resource level of his/her neighbourhood public library [ <em>F</em>(1, 10497) = 7.88, <em>p</em>=0.005 ]. One may posit that infrequent library users living in a neighbourhood with fewer library resources would have the lowest grades among the groups. The findings show that this was not the case. <em>Ceteris paribus</em>, those with lower grades were actually infrequent library users living in neighbourhoods with good library resources. This may be due in part to comparative disadvantage; infrequent users not only miss out on quality resources from the public library, they also are likely to compete with peers in their neighbourhood who frequently use the neighbourhood library and can capitalise on the higher-quality resources there. In contrast, among students living in a neighbourhood with fewer library resources, frequent library users did not gain as big a grade differential over infrequent users. It is hypothesised that the lower level of resources available in the libraries may have curtailed some of the benefits that the students gain from using these libraries.</p>
<h2 id="conclusion">Conclusion</h2>
<p>The empirical testing using the person-in-environment framework found significant main and interaction effects among environmental factors such as home computer and public library resource levels. These significant effects would have been missed in studies that included only personal factors. Given the diversity in personal and environmental factors, contextual research would benefit from a multitude of studies informed by different philosophical stances, conceptual lenses and research methods. Concerted efforts in testing environmental variables as explanatory factors of individual information behaviour and outcome should yield useful results and new insights.</p>
<h2 id="about-the-author">About the author</h2>
<p><strong>Sei-Ching Joanna Sin</strong> is an assistant professor at Nanyang Technological University, Singapore. She received her Bachelor of Social Science degree from The Chinese University of Hong Kong and her Master's and PhD degree in Library and Information Studies from the University of Wisconsin-Madison. She can be contacted at: <a href="mailto:joanna.sin@ntu.edu.sg">joanna.sin@ntu.edu.sg</a></p>
</section>
<h4 id="references">References</h4>
<ul>
<li>Bandura, A. (1978). The self system in reciprocal determinism. <em>American Psychologist, 33</em>(4), 344-358.</li>
<li>Bandura, A. (2001). Social cognitive theory: An agentic perspective. <em>Annual Review of Psychology, 52</em>, 1-26.</li>
<li>Bronfenbrenner, U. (2005). Ecological systems theory. In <em>Making human beings human: Bioecological perspectives on human development</em> (pp. 106-173). Thousand Oaks, CA: Sage Publications.</li>
<li>Courtright, C. (2007). Context in information behavior research. <em>Annual Review of Information Science and Technology, 41</em>, 273-306.</li>
<li>Fidel, R. (2012). <em>Human information interaction: an ecological approach to information behavior</em>. Cambridge, MA: MIT Press.</li>
<li>Hemmeter, J. A. (2006). Household use of public libraries and large bookstores. <em>Library &amp; Information Science Research, 28</em>(4), 595-616.</li>
<li>Ingels, S. J., Pratt, D. J., Wilson, D., Burns, L. J., Currivan, D., Rogers, J. E. &amp; Hubbard-Bednasz, S. (2007). <a href="http://www.webcitation.org/6RzUe8yUu"><em>Education longitudinal study of 2002: Base-year to second follow-up data file documentation</em></a>. Retrieved from http://nces.ed.gov/pubs2008/2008347.pdf (Archived by WebCite� at http://www.webcitation.org/6RzUe8yUu)</li>
<li>Kroe, P. E., O'Shea, P., Craig, T., Freeman, M., Hardesty, L., McLaughlin, J. F. &amp; Ramsey, C. J. (2006). <em><a href="http://www.webcitation.org/6Rp7MrFTR">Data file, public use: Public libraries survey: fiscal year 2004</a>.</em> Retrieved from http://nces.ed.gov/pubs2006/2006347.pdf (Archived by WebCite� at http://www.webcitation.org/6Rp7MrFTR)</li>
<li>Lent, R. W., Brown, S. D. &amp; Hackett, G. (1994).Toward a unifying social cognitive theory of career and academic interest, choice, and performance. <em>Journal of Vocational Behavior, 45</em>(1), 79-122.</li>
<li>Lewin, K. (1939). Field theory and experiment in social psychology: Concepts and methods. <em>American Journal of Sociology, 44</em>(6), 868-896.</li>
<li>Sin, S.-C. J. (2009). <em>Structural and individual influences on information behavior: a national study of adolescents' use of public libraries.</em> Unpublished doctoral dissertation, University of Wisconsin-Madison, Madison, WI, USA</li>
<li>Sin, S.-C. J. (2011). Modelling individual-level information behaviour and outcomes: a person-in-environment (PIE) framework for agency-structure integration. In A. Spink &amp; J. Heinstrom (Eds.), <em>New directions in information behaviour</em> (pp. 181-209). Bingley, UK: Emerald.</li>
<li>Sin, S.-C. J. (2012). Modeling the impact of individual's characteristics and library service levels on high school student's public library usage: a national analysis. <em>Library &amp; Information Science Research, 34</em>(3), 228-237.</li>
<li>Talja, S., Keso, H. &amp; Pietilainen, T. (1999).The production of 'context' in information seeking research: a metatheoretical view. <em>Information Processing &amp; Management, 35</em>(6), 751-763.</li>
<li>Vakkari, P. (1988). Library supply as an incentive to borrowing: a contextual analytic approach. <em>Svensk Biblioteksforskning, 3-4</em>, 24-41.</li>
</ul>
</article>
</body>
</html>
