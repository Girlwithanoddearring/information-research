#### Vol. 6 No. 4, July 2001

# Intranets in French firms: evolutions and revolutions

#### [Emmanuelle Vaast](mailto:vaast@poly.polytechnique.fr)  
Centre de recherche en Gestion (CRG),  
Ecole Polytechnique  
Paris, France

#### Abstract

> (Progressive) evolutions and (radical) revolutions characterise the temporal paths of intranets in French firms. Based on the results of 12 case studies, this research has adopted a dynamic perspective. It has focused on the implementation and subsequent evolution of intranets, the ways they are shaped by and impact diverse aspects of organisational life, including strategic management and daily tasks. It shows that gradual and radical changes are not mutually exclusive but complementary. On the one hand, intranets evolve incrementally, by co-construction of their technical and organisational dimensions. On the other hand, they experience substantial changes aimed at interpreting them in new ways. This dual pace is noticeable in the changes intranets provide on critical organisational dimensions, such as information management and human resources policies. Finally, this dynamic perspective underscores how new balances in traditional managerial tensions may be reached as a result of these radical and progressive co-evolutions between intranets and organisations.

## Introduction

Intranets today seem to merge countless discourses, technical tools, projects, realisations and illusions. Such profusion hides the great difficulty in grasping what intranets really are and how they are applied and used in firms.

Frequently defined as corporate internal networks using web standards (e.g., Hills, 1997), intranets in fact rely on varied and changing technologies. Moreover, in diverse organisations, the intranet is presented as a new way to manage information, as an example of the technological dynamism of the firm, or an essential instrument for collaboration and work.

This research has studied the evolution of intranets in twelve French firms in order to resolve the paradox that intranets are simultaneously omnipresent in managerial concerns and yet little known. It has confirmed the great diversity of intranets and the representations that are associated with them. Of the three main aspects that characterize them, the first two depend highly on the third. First, intranets are generally deployed across the whole organisation. Their study requires multiple levels of analysis. Second, uses of intranets may vary significantly depending on the characteristics of diverse departments, services and work groups in the firm. These uses range from mere official communication to sophisticated groupware and renewed ways of working. Third, intranets evolve with time, rendering their static observation insufficient. They must be studied in a dynamic perspective. In particular, they follow both gradual and radical transformations. They may also foster organisational changes, either progressive or revolutionary ones. This article focuses on these temporal dimensions of the relationships between intranets and firms.

The distinction between radical and progressive evolutions is standard in the literature on the management of innovation (David, 1985). These two kinds of changes have most often been considered as separate or even mutually exclusive (Tyre & Orlikowski, 1994, Robey & Sahay, 1996). However, these two paces have been observed and appear complementary in all cases studied here. The evolution of intranets oscillates between (gradual) evolutions and revolutions. Furthermore, a new issue has surfaced in the much debated field of the relationships between information technology (IT) and organisations (Rowe & Struck, 1995). It deals with the temporal dimensions of the interdependencies between technology and firms (Barley, 1986). ITs not only evolve with time, but they also influence diverse organisational rhythms that concern information diffusion, the ways diverse groups work and how they interact (Lee, 1999). A dynamic perspective has been applied to underscore the evolution of the intranet in diverse organisations and the changing processes arising from the evolutionary mutual influence between organisational and technological dimensions. The main argument of this paper is that firm evolutions follow progressive paths as well as radical changes and that intranets are an illustration and part and parcel of these evolutions.

The first two parts of this article specify the conceptual framework and methodology of this research. Next, the results of the case studies are presented. The presentation focuses on the implementation, the progressively increasing complexity and revolutions of intranets according to multiple interdependencies between organisational and technological dimensions. Afterwards, these analyses are discussed. The alterations in Intranets offer various 'occasions for organizing' and lead to a new balance involving three fundamental dualities: the local/central levels of the organisation, action/communication and deliberate/emergent changes. Limitations and future research directions are finally briefly suggested.

## Review of the literature

### The immaturity of intranets and the burgeoning literature

The recent literature on intranets is not very satisfying. The 'hyping' of intranets has generated a wealth of managerial articles, whereas the newness of intranets explains why very few research studies have been published to date. Most available articles link intranets to diverse managerial trends, but do not question the nature of these assumed relations and their potential consequences. For instance, intranets supposedly improve project management (Mead, 1997). They are also frequently seen as the basis of 'virtual organising'(Venkatraman, 1998). Additionally, intranets are considered instruments of organisational learning due to their ability to share and manage knowledge (Harvey _et al._, 1998). The lack of depth in this literature is due to the recency of intranets and the time consuming process of conducting and publishing thorough studies.

Research designs particularly reflect this newness. Except for a few original and rigorous studies (Kim, 1998), published articles have thus far been based on two main static types of empirical investigations. First, many studies present intranet applications in specific firms. Those examples are frequently closer to anecdotes than to in-depth case studies (Lovatt, 1997). Second, surveys are conducted (e.g., Wilder, 1999), with little precision or efforts to systematise the data. More important, these two kinds of empirical investigations are static. They consider the intranet as a given subject of investigation and do not study its evolutions and co-evolutions in firms.

The study of temporal aspects of intranets is therefore still rather exploratory. This exploration nevertheless benefits from the very rich and diverse literature on the relationships between IT and organisation. This field, in particular, has highlighted the radical and gradual alterations of IT in firms.

### The emergent perspective on the relationships between IT and organisation, and the question of time

Theoretical debate has prevailed in the field of the relationships between IT and organisations since Leavitt and Whisler's (1958) precursory article. The objective of this section is not to make an exhaustive review of the literature, but, rather, to present the conceptual framework adopted here; that is, the 'emergent' perspective in Markus and Robey's (1988) typology. Markus and Robey have distinguished theories on ITs and organisational change according to their 'causal structure': their hypotheses on the nature and direction of causal influence are shown in Table 1.

<table align="center" bgcolor="#FDFFBB" border="" cellspacing="1" cellpadding="4"><caption align="bottom">**Table 1: Markus and Robey's (1988) typology**</caption>

<tbody>

<tr>

<th width="335" valign="TOP">Causal agency:</th>

<th width="139" valign="TOP">Nature of causality</th>

<th>Level of analysis</th>

</tr>

<tr>

<td width="335" valign="TOP">**Technological imperative**  
External, technological forces, lead to changes. Technology as an exogeneous force.  
Example: Leavitt and Whisler (1958).</td>

<td width="139" valign="TOP">Variance theory.</td>

<td width="139" valign="TOP">Macro</td>

</tr>

<tr>

<td width="335" valign="TOP">**Organisational imperative**  
IT as a dependent variable of organisational imperatives, of information processing needs.  
Example: media richness theory.</td>

<td width="139" valign="TOP">Variance theory</td>

<td width="139" valign="TOP">Micro</td>

</tr>

<tr>

<td width="335" valign="TOP">**Emergent perspective**  
Uses and consequences of IT emerge unpredictably from interactions between organisation and technology.  
Example: structuration theory.</td>

<td width="139" valign="TOP">Processual theory</td>

<td width="139" valign="TOP">Multi-level</td>

</tr>

</tbody>

</table>

Research from the emergent perspective presents the effects of the introduction of ITs in firms as the semi-unpredictable result of interactions between technological and organisational variables. In particular, structuration theory (Orlikowski & Robey, 1991), inspired by Giddens' work (1984), rests on the notion of 'duality of structure'. As a consequence, organisational phenomena are explained by human action and by institutional properties. IT is _being structured_ (it is a social product of subjective human action in particular contexts) and _structures_ (it constitutes a new set of objective rules and resources and, therefore, contributes to changes in contexts). More generally, 'emergent' research accounts for the contradictory consequences of IT in organisations (Robey & Boundreau, 1999, Ciborra, 2000). It considers technology as an element of social processes that is influenced by material contexts and has _a priori_ unspecified consequences for organisations (Poole & DeSanctis, 1990). This unpredictability does not imply that organisational impacts of ITs are totally incomprehensible and surprising. Rather, it is necessary to adopt a suitable methodology in order to understand and try to predict them as much as possible. Thus the emergent perspective research design must be longitudinal in order to investigate its temporal dimension (Poole & DeSanctis, 1994).

Regarding this temporal dimension, two paces of evolution have been distinguished: radical and gradual. These changes accompany the mutual influence of IT and organisation. Given the 'interpretive flexibility' of IT (Orlikowski, 1992), choices made during its implementation phase are crucial and have consequences throughout the technology life cycle in the organisation. This period of radical choices corresponds to 'windows of opportunity' (Tyre & Orlikowski, 1994) that structure technology in the firm and stimulate managerial changes. Other evolutions are gradual and come from regular use of IT, and from the emerging co-evolution between particular capabilities of the technology and an evolving organisational context (Contractor & Eisenberg, 1990).

This research has focused on these two - radical and gradual - paces of evolution and has adopted a dynamic research design.

## Methodology

The exploratory character of this research and its methodological requirements (longitudinal and multi-level investigations) have led to conducting case studies of intranets. Case study is appropriate to the study of phenomena which are hardly separated from their context, to exploratory investigations, and to the application of existing conceptual frameworks to empirical situations (Yin, 1989). Moreover, this method is highly flexible and can be fruitfully applied to the study of IT in organisations (Cavaye, 1996).

Twelve case studies were conducted from December 1999 to May 2000\. They consisted of observation and retrospective reconstruction of firms that have implemented one or more intranets. Cases have been selected to obtain a representative sample (analytically, but not statistically, cf. Yin, 1989) of firms using intranets in France. Diverse aspects characterising these organisations have been distinguished (table 2).

<table align="center" bgcolor="#FDFFBB" border="" cellspacing="2" cellpadding="4" width="614"><caption align="bottom">**Table 2: Some characteristics of the studied organisations**</caption>

<tbody>

<tr>

<th width="7%" valign="TOP">Case</th>

<th width="28%" valign="TOP">Kind of firm</th>

<th width="29%" valign="TOP">Sector</th>

<th width="12%" valign="TOP">Date of creation</th>

<th width="24%" valign="TOP">Turnover - employees</th>

</tr>

<tr>

<td width="7%" valign="TOP">A </td>

<td width="28%" valign="TOP">International group</td>

<td width="29%" valign="TOP">Services  
Insurance</td>

<td width="12%" valign="TOP">1820  
1998</td>

<td width="24%" valign="TOP">$15.4 Million  
28 000 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">B </td>

<td width="28%" valign="TOP">Small holding of 4 firms.</td>

<td width="29%" valign="TOP">Industry  
Mechanical engineering subcontracting</td>

<td width="12%" valign="TOP">1937  
1994</td>

<td width="24%" valign="TOP">$43.1 M  
400 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">C </td>

<td width="28%" valign="TOP">International group</td>

<td width="29%" valign="TOP">Services, media.</td>

<td width="12%" valign="TOP">1826  
1994</td>

<td width="24%" valign="TOP">$12.1 Billion  
30 000 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">D </td>

<td width="28%" valign="TOP">International group</td>

<td width="29%" valign="TOP">Industry  
Chemistry</td>

<td width="12%" valign="TOP"> </td>

<td width="24%" valign="TOP">$15.0 B  
65 000 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">E </td>

<td width="28%" valign="TOP">Occupational association</td>

<td width="29%" valign="TOP">Services.</td>

<td width="12%" valign="TOP">1820</td>

<td width="24%" valign="TOP">unavailable  
87 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">F </td>

<td width="28%" valign="TOP">International group</td>

<td width="29%" valign="TOP">Industry  
Construction.</td>

<td width="12%" valign="TOP">1881  
1992</td>

<td width="24%" valign="TOP">$2.3 B  
56,000 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">G </td>

<td width="28%" valign="TOP">International group</td>

<td width="29%" valign="TOP">Services  
Bank</td>

<td width="12%" valign="TOP">1863</td>

<td width="24%" valign="TOP">Unavailable  
51 000 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">H </td>

<td width="28%" valign="TOP">Association.</td>

<td width="29%" valign="TOP">Services  
Certification</td>

<td width="12%" valign="TOP">1988</td>

<td width="24%" valign="TOP">Unavailable  
280 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">I </td>

<td width="28%" valign="TOP">Semi-public organisation</td>

<td width="29%" valign="TOP">Services  
Housing</td>

<td width="12%" valign="TOP"> </td>

<td width="24%" valign="TOP">$25.9 M  
280 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">J </td>

<td width="28%" valign="TOP">International group</td>

<td width="29%" valign="TOP">Industry  
Electricity and mechanics</td>

<td width="12%" valign="TOP">1886</td>

<td width="24%" valign="TOP">$8.6 B  
61 000 employees</td>

</tr>

<tr>

<td width="7%" valign="TOP">K </td>

<td width="28%" valign="TOP">State-owned company</td>

<td width="29%" valign="TOP">Services  
Railroad.</td>

<td width="12%" valign="TOP">1936</td>

<td width="24%" valign="TOP">$17.8 B  
170 000 employees.</td>

</tr>

<tr>

<td width="7%" valign="TOP">L </td>

<td width="28%" valign="TOP">Ministry</td>

<td width="29%" valign="TOP">Services  
Health.</td>

<td width="12%" valign="TOP"> </td>

<td width="24%" valign="TOP">Non relevant  
15 000 employees.</td>

</tr>

</tbody>

</table>

The sample of cases was developed from a preliminary documentary study and from ongoing dialogue with IT professionals, managers and end users of intranets in diverse organisations. Because of the lack of available cross-sectional data on intranets in French firms, it is difficult to assert the total lack of bias in the sample of cases. However, according to the press, conferences, and professional meetings on the subject, it seems that the sample is fairly representative of the main types of French organisations with intranets.

### Data collection and analysis

Diverse types of data have been collected and analysed.

<table align="center" bgcolor="#FDFFBB" border="" cellspacing="1" cellpadding="4" width="614"><caption align="bottom">**Table 3: Collected data**</caption>

<tbody>

<tr>

<td width="25%" valign="TOP">**Semi-structured interviews**</td>

<td width="75%" valign="TOP">From 5 to 15 interviews for each case.  
With firm CEOs, managers of intranets, from diverse departments.  
With end-users of the intranets.</td>

</tr>

<tr>

<td width="25%" valign="TOP">**Statistical data**</td>

<td width="75%" valign="TOP">Of available pages and of consultations.</td>

</tr>

<tr>

<td width="25%" valign="TOP">**Surfing the intranet**</td>

<td width="75%" valign="TOP">Observation, at a given moment, of the content and structure of the intranet.</td>

</tr>

<tr>

<td width="25%" valign="TOP">**Diverse documentation**</td>

<td width="75%" valign="TOP">Reports on meetings.  
Official communication concerning the intranet.</td>

</tr>

<tr>

<td width="25%" valign="TOP">**Archives**</td>

<td width="75%" valign="TOP">Archives on the implementation and evolution of projects.</td>

</tr>

<tr>

<td width="25%" valign="TOP">**Screen savings**</td>

<td width="75%" valign="TOP">To get 'snapshots'of a given state of the intranets (to make cross cases and temporal comparisons).</td>

</tr>

</tbody>

</table>

To make sense of these mainly qualitative data (Miles & Huberman, 1984), the first stage of data reduction has consisted of writing of monographs for each firm. Then, analyses have been performed for each case and then across cases.

The dynamic dimension of this research has been ensured by the retrospective reconstruction of the intranet evolution from the original idea that led to its implementation. Actors' discourses and archives have been triangulated in order to limit more or less intentional foresights.

The adoption of multiple levels of analysis has been performed by interviewing people at diverse levels of the firms (CEOs, webmasters, different users, etc.). These qualitative data have been confronted by harder, statistical data concerning, in particular, the evolution of available pages and the use of the intranet. The main results of these case studies are detailed below.

## Case studies results

Case studies have brought to the forefront the succession of progressive and radical evolutions characterising intranets in French organisations. These evolutions come from dynamics linked to intranets and their capabilities, to changing organisational contexts and to the co-construction of organisational and technological variables. They are noticeable during the implementation of the intranet as well as its future evolutions.

### Intranet implementation: between sudden enthusiasms and specific progressive realisations

<table align="center" bgcolor="#FDFFBB" border="" cellspacing="2" cellpadding="4" width="613"><caption align="bottom">**Table 4: Intranet implementation**</caption>

<tbody>

<tr>

<th width="16%" valign="TOP">Cases</th>

<th width="24%" valign="TOP">Strategic - organisational context</th>

<th width="8%" valign="TOP">Project Start</th>

<th width="29%" valign="TOP">Initial objective of the intranet</th>

<th width="14%" valign="TOP">Project team*</th>

<th width="9%" valign="TOP">Intranet  
deployed</th>

</tr>

<tr>

<td width="16%" valign="TOP">A: international insurance group.</td>

<td width="24%" valign="TOP">Privatisation of the group in 1995  
Numerous mergers in the sector</td>

<td width="8%" valign="TOP">1992 - 1993.</td>

<td width="29%" valign="TOP">Making the most of technological opportunities</td>

<td width="14%" valign="TOP">CompD then also ComD and HRD</td>

<td width="9%" valign="TOP">End of 1996.</td>

</tr>

<tr>

<td width="16%" valign="TOP">B: small industry holding</td>

<td width="24%" valign="TOP">Increasing dependence on principals</td>

<td width="8%" valign="TOP">1995 -1996</td>

<td width="29%" valign="TOP">Overcoming geographical distance among firms</td>

<td width="14%" valign="TOP">HO  
CompD, ComD and also HRD</td>

<td width="9%" valign="TOP">Mid - 1997</td>

</tr>

<tr>

<td width="16%" valign="TOP">C: International media group</td>

<td width="24%" valign="TOP">Numerous mergers in the sector</td>

<td width="8%" valign="TOP">End of 1995</td>

<td width="29%" valign="TOP">Creating a common identity in the group</td>

<td width="14%" valign="TOP">ComD and CompC</td>

<td width="9%" valign="TOP">End of 1997.</td>

</tr>

<tr>

<td width="16%" valign="TOP">D: international chemistry group</td>

<td width="24%" valign="TOP">Numerous mergers in the sector</td>

<td width="8%" valign="TOP">1996 - 1997.</td>

<td width="29%" valign="TOP">Improving information management throughout the group</td>

<td width="14%" valign="TOP">ComD and CompD</td>

<td width="9%" valign="TOP">End of 1997.</td>

</tr>

<tr>

<td width="16%" valign="TOP">E: occupational association</td>

<td width="24%" valign="TOP">Fraught relations with institutions.</td>

<td width="8%" valign="TOP">1995.</td>

<td width="29%" valign="TOP">Improving exchanges among organisations.</td>

<td width="14%" valign="TOP">CompD</td>

<td width="9%" valign="TOP">End of 1997.</td>

</tr>

<tr>

<td width="16%" valign="TOP">F: international construction group.</td>

<td width="24%" valign="TOP">Mergers in the sector.  
Merger of the group in 1992.</td>

<td width="8%" valign="TOP">01/1999.</td>

<td width="29%" valign="TOP">Bringing together scattered employees in the group.</td>

<td width="14%" valign="TOP">HO, ComD, CompC</td>

<td width="9%" valign="TOP">Mid  
2000</td>

</tr>

<tr>

<td width="16%" valign="TOP">G: banking international group.</td>

<td width="24%" valign="TOP">Growth then crisis of the group.  
Mergers in the sector</td>

<td width="8%" valign="TOP">1997</td>

<td width="29%" valign="TOP">Improving the relationships in project teams</td>

<td width="14%" valign="TOP">Project teams</td>

<td width="9%" valign="TOP">Mid 1997.</td>

</tr>

<tr>

<td width="16%" valign="TOP">H: association of certification</td>

<td width="24%" valign="TOP">General growth of the sector</td>

<td width="8%" valign="TOP">1995</td>

<td width="29%" valign="TOP">Sharing electronic information</td>

<td width="14%" valign="TOP">HO + CompD.</td>

<td width="9%" valign="TOP">Mid 1997.</td>

</tr>

<tr>

<td width="16%" valign="TOP">I: Semi-public organisation</td>

<td width="24%" valign="TOP">Modernisation and improvement of services</td>

<td width="8%" valign="TOP">1995</td>

<td width="29%" valign="TOP">Facilitating information access and fostering communications.</td>

<td width="14%" valign="TOP">CompD</td>

<td width="9%" valign="TOP">01/1997</td>

</tr>

<tr>

<td width="16%" valign="TOP">J: International industrial group</td>

<td width="24%" valign="TOP">Matrix structure since the beginning of 1990s.</td>

<td width="8%" valign="TOP">1996</td>

<td width="29%" valign="TOP">Reaching the objective of modernisation of the group.</td>

<td width="14%" valign="TOP">HO  
CompD</td>

<td width="9%" valign="TOP">End of 1996</td>

</tr>

<tr>

<td width="16%" valign="TOP">K: State-owned railroad compagny</td>

<td width="24%" valign="TOP">Reorganisation of the relationships between central services and regions</td>

<td width="8%" valign="TOP">1995 - 1996</td>

<td width="29%" valign="TOP">Funnelling regional intranet initiatives</td>

<td width="14%" valign="TOP">CompD + ComD</td>

<td width="9%" valign="TOP">02/1998.</td>

</tr>

<tr>

<td width="16%" valign="TOP">L: ministry</td>

<td width="24%" valign="TOP">Modernisation of public service.</td>

<td width="8%" valign="TOP">02/1997.</td>

<td width="29%" valign="TOP">Improving ministry use of ICT.</td>

<td width="14%" valign="TOP">CompD  
ComD</td>

<td width="9%" valign="TOP">02/1998</td>

</tr>

<tr>

<td colspan="6">* CompD - Computer Department; ComD - Communication Department; HRD - Human Resources Department; HO - Head Office</td>

</tr>

</tbody>

</table>

The analysis of intranet implementation in the sample of cases has revealed the need to go beyond the mere vision of intranets as simple artefacts with stable and specific characteristics implemented to respond to precise organisational needs. Rather, since 1995, their introduction in France has depended on institutional factors. The specific development of an intranet in a firm also takes place in idiosyncratic contexts. The implementation of an intranet, therefore, is characterised by the articulation of slogans and visions of intranets, by technological instruments, and by the action of managers and teams. This institutional, technological and organisational phenomenon corresponds more to the combination of diverse processes than to the mere introduction of a given technology.

#### Intranets as a sudden institutional phenomenom: the 1995 turning point

In almost all firms of the sample, reflections linked to the intranet have started in 1995 - 1996\. This period marks the massive beginning of intranets in French firms. A simple explanation of this 1995 turning point, based on the sudden appearance of a technology, is not satisfactory. Before 1995, web standards inside firms, already used in the United States, were also available in France (cf. the precursory reflections of case A). Moreover, web technologies have constantly been evolving. Then, no technological cause explains the turning point of 1995\. Nonetheless, before 1995, in France, the intranet was almost unknown but in 1995 and 1996 the intranet has become 'unavoidable'. This sudden and wide appearance of the intranet in managerial concerns corresponds to a mimetic phenomenon. This sudden emergence had already been underscored for previous IT and consists of the emergence of a 'hype' by the convergence of discourses, investments and concrete realisations. Intra- and inter-sector institutional processes are fuelled by consulting firms, technical providers and effective investments and implementations. They lead to the development of an 'organizing vision' (Swanson & Ramiller, 1997). According to Swanson & Ramiller, innovation diffusion processes come from normative institutional pressures that contribute to the construction of a collective image of the new technology. This perspective emphasises the institutional development of organisational concerns. The organizing vision of the intranet emerged in 1995\. It neither dictates precise implementations nor specific applications. Rather, it consists of general assertions such as, 'The intranet is the new technology, the future of all information systems'. These attractive expressions can be applied to multifarious situations, and open the way to very diverse implementations and evolutions.

#### Specific implementations

**_The organisational origin of the intranet project_**

Various departments may initiate the intranet (mainly head offices and the computing, communication and human resources departments). This range reflects the existing power struggles and is typical of large-scale intra-organisational projects (Lundin & Midler, 1998).

This variety may however seem surprising when it comes to ICT projects that, at first glance appear essentially technical. The surprise may be even further increased by the observation of many 'multi-departments'projects (general offices + human resources + computing + communication departments). Furthermore, dominant discourses underscore the universality of web standards and the consequent intranet supposed ease of implementation. However, observations have revealed that the introduction of an intranet generally lasts at least one year. This quite long period is partially explained by the initial lack of knowledge of the firm. The organizing vision proposes powerful slogans but no operational advice. More importantly, it takes time to implement an intranet because of its a priori indetermination. The intranet is nothing as long as one considers only its minimal technological definition of web standards adapted to the internal network of a firm. This feature of 'interpretive flexibility' is all the more noticeable for intranets because they cannot simply be considered as computing projects. Indeed, the intranet is widely deployed in the organisation. Of course, it involves substantial hardware and software investments. However, what distinguishes the intranet from previous computer and ITs projects (such as e-mail or groupware) is its communicative and visual identity dimension. Similar to internet sites, the intranet conveys a particular image of the firm. Communication and computer aspects of intranets projects are inextricably linked. This sheds light on the time necessary to implement intranets, as well as on the frequency of multi-departments projects.

**_The intranet team_**

The composition and functioning of the team in charge of the implementation of the intranet influence its effective realisation and, most of all, its perception by other members of the organisation. This has an impact on its future uses and evolutions.

Initial choices are crucial and structuring because of the interpretive flexibility of the intranet. At first glance, this notion of flexibility leads to the hypothesis that intranet implementations have to be specific and highly differentiated. On the contrary, however, the initial contents of intranets seem rather standard. Specific aspects of intranets are, initially, less numerous than generalised contents. They are composed of official information concerning the firm, its businesses, members, etc. At the time of its introduction, the intranet is typically an instrument of official information management.

This observation of standardised initial contents of intranets may be complemented and refined by other findings. According to the case studies, there is no correlation between the date of implementation of the intranet and the initial richness of its contents (defined here as the quantity and range of applications and information). However, an important correlation has been observed between the date of implementation and the richness of its content at the moment of the data collection. The older the intranet, the more numerous, varied and idiosyncratic are its contents.

Taken together, these observations suggest the importance of subsequent evolutions of intranets. The interpretive flexibility endures beyond the implementation period which leads the intranet on a not totally predictable evolution trajectory. A renewed conception of the influence of the intranet team may consequently be drawn. It is less significant for the initial content than it is for the subsequent evolutions and learning processes it may foster. Its initial role is to provide the intranet with visibility and legitimacy. The project team embodies the future intranet and, in this way, influences its evolutions.

For instance, in the sample of organisations, two much different cases have benefited from a particularly influential project team. The CEO has run the intranet team of case B (a small firm of mechanical engineering sub-contracting). The project has quickly gained a substantial legitimacy, being presented as an instrument of the modernisation of the whole firm. The subsequent evolutions of the intranet have consequently been oriented toward a greater technological sophistication. In contrast, the project team for case A (international insurance group) has been much more informal. Its influence has taken more than one year to be noticeable and to foster local evolutions of the intranet. However, this informal character has given the intranet an image of relative freedom and flexibility in comparison with the traditional organisational functioning, generally seen as bureaucratic. It has favoured original appropriations of the intranet.

Therefore, the implementation period of an intranet is characterised by the articulation of institutional and other, more specific phenomena. It is composed of sudden and more gradual phases. The subsequent evolutions of an intranet are even more marked by these radical and progressive changes.

### What the intranet becomes, between gradual and radical changes

The evolution of intranets in the case studies have been both gradual and radical. Intranets change incrementally by co-construction of technical and organisational dimensions. They also experience 'revolutions', substantial changes aimed at interpreting them in new ways and at creating new processes. The distinction between incremental / radical evolutions is traditional in the literature on the management of innovation. Incremental evolution is gradual, by successive additions. The path is progressively drawn. Conversely, radical evolution is characterised by an explicit and official change of version of the intranet. It is marked by substantial changes in the general structure of the intranet, in the content of its applications, and in the way it is managed.

#### Incremental evolutions: the gradual construction of the intranet

<table align="center" bgcolor="#FDFFBB" border="" cellspacing="2" cellpadding="4" width="637"><caption align="bottom">**Table 5: Incremental evolution of intranets**</caption>

<tbody>

<tr>

<td width="13%" valign="TOP">Cases</td>

<th width="9%" valign="TOP">Connected*</th>

<th width="7%" valign="TOP">Period</th>

<th width="28%" valign="TOP">Organisational and strategic context</th>

<th width="42%" valign="TOP">Decision level of evolutions.  
New tools, sites, rubrics (or headings).</th>

</tr>

<tr>

<td width="13%" valign="TOP">A: international insurance group.</td>

<td width="9%" valign="TOP">1,000  
13,000  
(28,500)</td>

<td width="7%" valign="TOP">End of 1996 - End of 1999</td>

<td width="28%" valign="TOP">Merger in 1998 with another insurance group. The intranet has become the official communication medium.</td>

<td width="42%" valign="TOP">Flexible and coordinated development at the corporate level.  
Search engines, directories. New sites linked to the general intranet, are created according to the diverse firms and departments of the group.</td>

</tr>

<tr>

<td width="13%" valign="TOP">B: small industry holding</td>

<td width="9%" valign="TOP">50  
120  
(400)</td>

<td width="7%" valign="TOP">1997 - 2000</td>

<td width="28%" valign="TOP">Increasing competition, intranet seen as a showcase of a firm at the leading edge of progress.</td>

<td width="42%" valign="TOP">New developments are decided by the CEO.  
Implementation of logistical applications and intelligent agents.</td>

</tr>

<tr>

<td width="13%" valign="TOP">C: International media group</td>

<td width="9%" valign="TOP">8,000  
8,000  
(30,000)</td>

<td width="7%" valign="TOP">End of 1997 - Mid-1999</td>

<td width="28%" valign="TOP">Numerous mergers in the sector. Increased specialisation of the group in the sector of new media. Intranet as a showcase.</td>

<td width="42%" valign="TOP">Sites developed according to the firms and departments of the group.  
Group level rubrics decided by ComD</td>

</tr>

<tr>

<td width="13%" valign="TOP">D: International chemistry group</td>

<td width="9%" valign="TOP">Unavail.  
45,000  
(65,000)</td>

<td width="7%" valign="TOP">End of 1997 - End of 1999</td>

<td width="28%" valign="TOP">Numerous mergers in the sector. Transfers and acquisitions involving the group. Intranet as a reflection of the entire group.</td>

<td width="42%" valign="TOP">Development of linked sites, according to decisions of the diverse firms. Corporate rubrics decided by ComD.</td>

</tr>

<tr>

<td width="13%" valign="TOP">E: occupational association</td>

<td width="9%" valign="TOP">540  
5,400  
(9,500)</td>

<td width="7%" valign="TOP">End of 1997 - Mid 2000</td>

<td width="28%" valign="TOP">No major change of the strategic context.  
Intranet as a way to modernise the image of the occupation.</td>

<td width="42%" valign="TOP">Development of new rubrics comes from local initiatives and decisions by CompD.</td>

</tr>

<tr>

<td width="13%" valign="TOP">F: international construction group.</td>

<td width="9%" valign="TOP">Unavailable</td>

<td width="78%" valign="TOP" colspan="3">Group intranet not yet officially launched during data collection.</td>

</tr>

<tr>

<td width="13%" valign="TOP">G: banking international group.</td>

<td width="9%" valign="TOP">2,000  
Unavailable</td>

<td width="7%" valign="TOP">End of 1997 - Mid 2 000</td>

<td width="28%" valign="TOP">Privatisation of the group in May 1999\. Strategic reconstruction of the group. Intranet is not used as a major tool of this reconstruction.</td>

<td width="42%" valign="TOP">Development of 35 sites that are not linked together. Sites developed according to the structure of the group and diverse occupations. All employees do not have access to all sites.</td>

</tr>

<tr>

<td width="13%" valign="TOP">H: association of certification</td>

<td width="9%" valign="TOP">280  
280  
(280)</td>

<td width="7%" valign="TOP">Mid 1997 - Mid 2000</td>

<td width="28%" valign="TOP">No major organisational or strategic change.  
Intranet as the official communication medium.</td>

<td width="42%" valign="TOP">Development of tools making the intranet use and information search easier.  
Development of new rubrics according to diverse lines of business and not according to the association structure.</td>

</tr>

<tr>

<td width="13%" valign="TOP">I: Semi-public organisation</td>

<td width="9%" valign="TOP">90  
280  
(280)</td>

<td width="7%" valign="TOP">02/1997 - Mid 2000</td>

<td width="28%" valign="TOP">No major organisational or strategic change.  
Intranet as the official communication medium.</td>

<td width="42%" valign="TOP">New tools making the use of the intranet easier (search engine, etc.)  
Development of new rubrics according to diverse departments.</td>

</tr>

<tr>

<td width="13%" valign="TOP">J: International industrial group</td>

<td width="9%" valign="TOP">Unavail.  
33,000  
(61,000)</td>

<td width="7%" valign="TOP">End of 1996 - 01/2000</td>

<td width="28%" valign="TOP">No major organisational or strategic change.  
Intranet as the official communication medium.</td>

<td width="42%" valign="TOP">Development of rubrics of the group intranet, according to diverse departments. Development of firms intranets, coordination at the group level.</td>

</tr>

<tr>

<td width="13%" valign="TOP">K: State-owned railroad company</td>

<td width="9%" valign="TOP">25,000  
40,000  
(170,000)</td>

<td width="7%" valign="TOP">02/1998 - Mid 2000</td>

<td width="28%" valign="TOP">No major evolution of the organisational and strategic context.</td>

<td width="42%" valign="TOP">New rubrics decided according to regional or functional departments. Substantial centralisation by CompD and ComD.</td>

</tr>

<tr>

<td width="13%" valign="TOP">L: ministry</td>

<td width="9%" valign="TOP">Unavail.  
3,000  
(15,000)</td>

<td width="7%" valign="TOP">01/1998 - 05/2000</td>

<td width="28%" valign="TOP">No major evolution of the context</td>

<td width="42%" valign="TOP">Development of rubrics according to the diverse departments of the ministry and a few occupational communities.  
Decisions concerning the whole intranet are taken by the Ministry's department of the modernisation.</td>

</tr>

<tr>

<td colspan="5">*First Figure - Connected employees when the intranet was first made available; Second figure - Connected employees at the beginning of 2000; Third figure - total number of employees in the organisation.</td>

</tr>

</tbody>

</table>

The progressive construction of intranet sites and rubrics reflects the particular features of each organisation. In general, intranet sites and main rubrics develop along the lines of its main departments, some of its services and a few particular teams (mainly project teams - case C - and R&D ones - case D). Sometimes, sites develop beyond the official structure, according to business lines (case H) or occupational groups (case L). The impetus of these rubrics may come from the project team of the intranet, or may be directly handled by the concerned department, with or without ratification by the project team. Frequently, the development of intranet rubrics mixes initiatives, incentives and approvals at diverse organisational levels. Two opposite cases are documented in the sample. In one case (small firm, case B), the head of the intranet (the CEO) has determined all new development and orientation concerning the intranet. In another case, (international banking group, case G), thirty-five intranet sites have spontaneously developed with no coordination and no links among them.

#### A progressive sophistication of intranets, as a result of the co-evolution between technological tools and contents

The progressive sophistication of intranets (Scheepers & Damsgaard, 1997) comes from the gradual development of rubrics and applications, as well as from their progressive enrichment of information. It reflects the co-evolution between the technological base of the intranet and its content. Some of these evolutions are visible to the end user of the intranet, such as the creation of a new rubric for a specific occupational group. Others are transparent to the user but may influence subsequent evolutions. For instance, dynamic databases sometimes replace static ones. These progressive evolutions have some effects on the rubrics, on the possibilities of updating, multi-authoring, diverse levels of confidentiality, and so on.

Technical changes may not be fully incorporated because of the great diversity and increasing amount of intranet contents. Intranets are supposed to become easier and easier to use, thanks to new tools (such as search engines, help rubrics, site plan, etc.) aimed at improving user-friendliness. However, the profusion of its contents limits the effectiveness of these tools and may favour a feeling of confusion and powerlessness of a user confronted with information overload. The awareness of this overload is a frequent cause of radical change of the intranet.

#### Radical evolutions in intranets: providing new interpretation of intranets

<table align="center" bgcolor="#FDFFBB" border="" cellspacing="2" cellpadding="4" width="609"><caption align="bottom">**Table 6: Radical changes in intranets**</caption>

<tbody>

<tr>

<th width="14%" valign="TOP">Case</th>

<th width="10%" valign="TOP">Date 1</th>

<th width="9%" valign="TOP">Date 2</th>

<th width="9%" valign="TOP">Date 3</th>

<th width="20%" valign="TOP">Strategic - organisational context</th>

<th width="39%" valign="TOP">Objectives of the new version</th>

</tr>

<tr>

<td width="14%" valign="TOP">A: international insurance group.</td>

<td width="10%" valign="TOP">End of 1996.</td>

<td width="9%" valign="TOP">11/1999</td>

<td width="9%" valign="TOP">05/2000</td>

<td width="20%" valign="TOP">Since the beginning of 1999, as a consequence of the merger, reorganisation of the group.</td>

<td width="39%" valign="TOP">Ensuring a better consistence between the intranet structure and the new corporate structure.  
Formalising the evolutions of the intranet.  
Responding to the increasing overload of the intranet.</td>

</tr>

<tr>

<td width="14%" valign="TOP">B: small industry holding</td>

<td width="10%" valign="TOP">Mid - 1997</td>

<td width="9%" valign="TOP">End of 1997</td>

<td width="9%" valign="TOP">Mid 1998.</td>

<td width="20%" valign="TOP">No major strategic context evolution.</td>

<td width="39%" valign="TOP">Increasing opening of the intranet on the internet.  
Improving the diffusion of all kinds of information to diverse employees.  
Intranet as a showcase of the firm.</td>

</tr>

<tr>

<td width="14%" valign="TOP">C: International media group</td>

<td width="10%" valign="TOP">End of 1997</td>

<td width="9%" valign="TOP">03/1999</td>

<td width="8%" valign="TOP">01/2000</td>

<td width="20%" valign="TOP">No major change: continuing increased focalisation on new media.</td>

<td width="39%" valign="TOP">Fostering the collaborative communication and work at the corporate level thanks to the creation of virtual communities among employees of diverse firms of the group.</td>

</tr>

<tr>

<td width="14%" valign="TOP">D: International chemistry group</td>

<td width="10%" valign="TOP">End of 1997</td>

<td width="9%" valign="TOP">09/1999</td>

<td width="8%" valign="TOP">12/1999</td>

<td width="20%" valign="TOP">Merger with another chemistry group in December 1999.</td>

<td width="39%" valign="TOP">The new intranet as a tool of the merger between the two groups.  
Favouring the development of a common identity in the new group.</td>

</tr>

</tbody>

</table>

Only four of the twelve firms studied have experienced radical changes in the management and content of their intranets. These changes of version have not systematically appeared in the organisations that have implemented their intranets very early. Nevertheless, for three of the four concerned organisations, at least two years have passed from the initial implementation to the new version.

Radical evolutions of the intranet frequently correspond to major strategic and/or organisational changes. In particular, mergers open the way for profound reorganisations of the intranet (cases C and D). However, this revolution may take place at diverse moments, either at the same time as the organisational change or later.

As for case D, the date of the effective merger has coincided with the new version of the intranet. This simultaneity expressed the new head offices' will to make the intranet a showcase and an instrument of the merger. Case B has been much different. The group had experienced a merger in 1998, but the radical change of the intranet - leading to a new official version - was only decided in December 1999 and became effective in May 2000\. The objective of the new version was to ensure a better correspondence between the structures of the intranet and of the corporation. The mismatch between the moment of the organisational change and the new version of the intranet has arisen from the different dynamics of the intranet and the official organisation. The intranet dynamic in this group has mainly come from a small informal and very active team that has promoted the intranet throughout the organisation. The highly informal and decentralised dynamic of the intranet has been much different from the much more formal and hierarchical traditional corporate organisation. The new version of the intranet may therefore be interpreted as the aim of the general management to appropriate the intranet and to ensure the coincidence between its evolution and the whole organisation.

Radical changes may be considered as the will to give the intranet a new interpretive flexibility. These breaks are consistent with the perspective of 'windows of opportunity' (Tyre & Orlikowski, 1994) of ICTs. The installation of a new version opens a new 'window of opportunity' and gives the intranet a new trajectory. The opening of a window of opportunity is two-fold. First, it comes from the availability of new tools and information that change the possible uses of the intranet. Second, it is the result of a new asserted direction of the intranet.

This dual dimension of the window of opportunity is particularly exemplified by case C. The new version of the intranet was decided two years after its implementation. Initially the objective had been to promote a common identity among the employees. The intranet provided relevant information in the whole corporation. The objective of the second version of the intranet was to deepen the response of the same main goal of promotion of a common identity. In addition to common information a new application has therefore been implemented. It is aimed at fostering communication and knowledge sharing among employees of different firms that do not necessarily know each other. These 'micro virtual communities' offer their members shared agendas, directories, forums and people who gather virtually according to shared interests.

## Discussion

This article offers one of the first explorations of intranets in French firms, disentangles some of the complex inter-relationships between IT and organisations, and therefore helps to distinguish appreciable evolutions from the 'vapourweb'. More important, this research deals with the diverse temporal mechanisms of IT in firms. It presents empirical illustration of the interpretive flexibility of IT. The two temporal conceptions of organisational changes - (gradual) evolutions and revolutions - find empirical support and are observed according to different organisational and strategic contexts, the technology life cycle, the actions of a few influential individuals, and so on. Radical as well as progressive evolutions are part of the duality of structure of the technology. This dual rate of impacts of IT represents a new perspective on the contradictory consequences of IT in organisations (Robey & Boudreau, 1999), one that deals with the temporal effects of IT, and that is complementary to the results on the size and structure of the firm, on the relationships between departments, etc. These temporal sequences of radical and gradual changes open the way for intranets as 'occasions for organizing'and also to an evolutionary balance concerning three major organisational tensions.

### Intranets as 'occasions' for organizing

Beyond the general structure and evolution of the intranet, it is possible to pay closer attention to its content, applications, information, and possible uses. These contents appear as 'occasions for structuring' (Barley, 1986). The intranet offers occasions for the evolution of multiple internal processes and of the general organisational context. It may or may not lead to organisational changes. Organisational variables are crucial, for they influence the bringing into play of intranet potentialities. Conversely, and simultaneously, what the intranet technically permits influences the organisational context: old problems may be posed in new terms. The evolution of information management and recent trends in human resources illustrate this idea.

First, the intranet is, in all case studies, mainly seen as a tool for the management of diverse kinds of information. The intranet information management reveals the functioning of the organisation. Furthermore, the implementation and evolution of an intranet is frequently the cause of a general reflection on the official communication and information management. Conceptions of information management in organisations (Feldman & March, 1981) and of information and informing sciences (Kling, 1999, Sawyer & Rosenbaum, 2000), are relevant in this context. In the case of the smallest and/or youngest firms of the sample, the intranet has been the occasion for a first official and explicit reflection on information management. In case I, the intranet implementation has been accompanied by the creation of a new on-line newspaper that has proved to be a driving force to its use. With regard to the biggest and/or oldest firms of the sample, the intranet has been used to answer traditional questions of information management in new ways. Intranet 'capabilities' (Culnan & Markus, 1992) give way to the availability of enormous amounts of information. Progressive accumulation leads to the problem of overload and requires a renewed reflection on the intranet organisation as well as on information management. Moreover, the intranet also permits the restriction of information diffusion, thanks to levels of access. What one can and must know becomes a crucial matter for intranet evolutions.

Second, the intranet accompanies diverse organisational trends and may further their dynamic. This is particularly noticeable in the human resources field. The rubric 'stock quotes' of the firm is very often the most regularly consulted. Such a rubric arises from the development of employee shareholding. Furthermore, the 'available jobs' published on the intranets are also much consulted. The intranet is frequently seen as a way to change human resources in the firm (Strauss _et al._, 1998). Some human resources managers (case J) believe the intranet may help employees take an active part in their career. Such assertions are not new, but they are given concrete expression on the intranet. The intranet does not revolutionise human resources management, but may foster significant changes. This encouragement is both direct and indirect. Directly, the intranet offers tools, such as available jobs, training courses catalogues, on-line training, etc., that give employees the opportunity to carry out a reflection on their careers and to take action to change them. Indirectly, these HR rubrics are a way to express the importance of these trends and to contribute to a change in attitude and behaviour from employees and managers. However, the consequences of intranet communication may be limited by the organisational climate and pre-existing discourses. Intranet messages may clash with other organisational discourses.

### New balance in three dualities

These occasions for organizing show the fundamental indetermination of intranets. Not that technological variables have no structuring effects, but these variables are varied and gradually or radically enter a unique organisational context. Because of their fundamental indetermination, intranets contribute to multiple organisational processes which oscillate between dualities. These dual dimensions of intranet changes are consistent with, first, the emergent perspective on the relationships between technology and organisation which has shed light on the contradictory effects of ICTs in firms. Second, these dualities corroborate the present empirical investigations which have emphasised the most diverse aspects and evolutions of intranets. Three dualities have been underscored: the local/central levels, action/communication and deliberate/emergent changes.

_Local - central levels_

This duality corresponds to the diverse organisational levels involved in the gradual and radical evolutions of the intranet. By its very structure the intranet makes these different levels and the articulations among them visible. Some intranet evolutions may come from the central level of the firm and then be locally adapted. Others may originate locally and afterwards be incorporated throughout the organisation. Moreover, the study of intranets brings to the foreground the relative character of the local - central dimension, according to the level of observation. At the corporate level, what is 'central' concerns the whole group whereas what is "local" refers to its diverse forms. The 'local' levels involve diverse departments or even teams and individuals.

_Action - communication_

This duality of the intranet as a tool for official information and communication and as a component of the business and daily activities of the firm fits the diverse processes in which it may be involved. Two distinct logics seem to characterise intranets. The latter is first of all a way to sophistically manage information in the organisation. Second, rubrics, which concern employees'tasks directly, emerge. The introduction of levels of access frequently parts these two logics of action and communication. Nevertheless, more thoroughly, the action and the communication dimensions are linked and influence each other. For instance, action may lead to new communication: information concerning precise objectives and their carrying out may be published. Communication may also trigger new actions, such as increased dialog between the R&D and the production departments or among diverse project teams.

_Deliberate - emergent changes_

This duality is usual in the emergent perspective on the relationships between ICTs and organisation. The organisational consequences of the introduction of a new technology are neither totally predictable nor absolutely unmanageable. 'Opportunistic' changes (i.e., initially emergent changes which are then used by different actors in the firm and become intentional) are frequently observed (Orlikowski, 1996). Because of its great flexibility (leading to and requiring emergent changes) and its general development in the firm (demanding deliberate management), the intranet particularly experiences this duality. Deliberate/radical changes and emergent/progressive evolutions should not be equated. Admittedly, revolutions of the intranet imply a strong managerial will to promote them and progressive changes are frequently emergent. However, all radical changes of intranets may not be deliberate at first and one can influence gradual evolutions of the network.

### Limits and future research

The main limitation of this research comes from its exploratory nature. It is therefore highly dependent on the quality of the sample of cases. Other case studies should be conducted to corroborate the main findings and deepen the understandings of the articulations between gradual and radical evolutions.

Moreover, this article has presented the first insights concerning the temporal dimensions of intranets in French firms. However, the national specificity of investigations has not been assessed. International comparison would be fruitful. In particular, the evolution of intranets in French and American firms could be compared, given that US organisations have been developing them for a longer time and have started to use them more widely in their daily activity.

Furthermore, the studied organisations have been selected in order to be representative of the variety of intranet implementations in French firms, but this variety is also time dependent. Consequently, until approximately 1999, the intranet was used very little in daily work life. It was not possible, then, to get valid insights on the link between intranets and the way diverse groups of employees achieve their work and on the possible changes of rhythms of the organisational life (from polychronicity to monochronicity or vice versa: Lee, 1999). It seems nevertheless that, as time goes by and as old computer applications are abandoned, new intranet applications develop, which are increasingly used by workers as part of their jobs. This is particularly noticeable in the banking and insurance sectors. It would be particularly interesting to study intranet evolutions and revolutions when employees have to use it to accomplish their tasks. Temporal dimensions of these new interactions between the intranet and the firm could be observed. For instance, it is possible that new technological capabilities lead to progressive changes as well as radical evolutions in the way employees achieve their work, or in the relationships between two departments. Evolutions and revolutions with intranets in the realm of work are not over and require further examination (Barley & Kunda, 2001).

## Bibliography

*   Barley, S. R. (1986) "Technology as an occasion for structuring: evidence from observation of CT Scanners and the social order of radiology departments". _Administrative Science Quarterly_, **31**, 78-108.
*   Barley, S. R. & Kunda, G. (2001) "Bringing work back in." _Organization Science_, **12** (1) 76-95.
*   Benghozi, P. J. (1999) "Technologies de l'information et organisation: de la tentation de la flexibilité à la centralisation". Paper presented at 2ème colloque "Usages et services des télécommunications", Bordeaux.
*   Benghozi, P. J. & Cohendet. P. (1998) "L'organisation de la production et de la décision face aux TIC". In Rallet, A., editor, Technologies de l'information, organisation et performances Economiques: Commissariat general au Plan.
*   Cavaye, A.L.M. (1996) "Case study research : a multi-faceted research approach for IS". _Information Systems Journal_(6): 227-42.
*   Ciborra, C.U., editor. (2000) _From control to drift: the dynamics of corporate information infrastrucutres_. Oxford: Oxford University Press.
*   Contractor, N. S. & Eisenberg, E. M. (1990) "Communication networks and new media in organizations" .In Fulk, J. & Steinfield, C. W. editors, _Organizations and information technology_. Newbury Park, CA: Sage. pp. 143-171
*   Culnan, M. J. & M. L. Markus. (1992) "Information technologies".In Jablin, F. M., Roberts, K. H. & Porter, L.W. editors, _Handbook of organizational communication - an interdisciplinary perspective_. Newbury Park, CA: Sage.
*   David, P. (1985) "Clio and the economics of QWERTY". _American Economic Review_, **75** (2).
*   Feldman, M. S. & J.G. March. (1981) "Information in organization as signal and symbol". _Administrative Science Quarterly_, **26**, 171-86.
*   Giddens, A. (1984) _The constitution of society: outline of the theory of structuration_. Cambridge: Polity press.
*   Harvey, M., Palmer, J. & Speier, C. (1998) "Implementing intra-organizational learning: a phased-model approach supported by intranet technology." _European Management Journal_, **16** (3), 341-54.
*   Hills, M. (1997) _Intranet business strategies_: New York, NY: Wiley.
*   Kim, J. (1998) "Hierarchical structure of intranet functions and their relative importance : using the analytic hierarchy process for virtual organizations". _Decision Support Systems_, **23**, 59-74.
*   Kling, R. (1999) "[What is social informatics and why does it matter?](http://www.dlib.org:80/dlib/january99/kling/01kling.html)" _D-Lib magazine_: at http://www.dlib.org:80/dlib/january99/kling/01kling.html. (Accessed 12th July 2001)
*   Leavitt, H.J. &Whisler, T.L. (1958) "Management in the 1980s". _Harvard Business Review_, **36**, 41-48.
*   Lee, H. (1999) "Time and information technology: monochronicity, polychronicity and temporal symmetry". _European Journal of Information Systems_, **8**, 16-26.
*   Lovatt, M. (1997) "Herding cats: a case study on the development of internet and intranet strategies within an engineering organization". Paper presented at SIGCPR'97\. New York, NY: Association of Computing Machinery. pp.104-109
*   Lundin, R. A. & Midler, C. (1998) "Projects as arenas for renewal and learning processes" Dordrecht: Kluwer Academic Publishers.
*   Markus, M. L. & Robey, D. (1988) "Information technology and organizational change: causal structure in theory and research". _Management Science_, **34** (5): 583-98.
*   Mead, S. P. (1997) "Project-specific intranets for construction teams". _Project Management Journal_, **28** (3), 44-51.
*   Miles, M. B. & Huberman, A. M. (1984) _Qualitative data analysis - a source book of new methods_. Beverly Hills, CA: Sage.
*   Orlikowski, W. J. (1996) "Evolving with Notes: organizational change around groupware technology".In Ciborra, C. U., editor, _Groupware and teamwork: invisible aid or technical hindrance?_ New York, NY: Wiley.
*   Orlikowski, W. J. & Robey, D. (1991) IT and the structuring of organizations. _Informations Systems Research_, **2** (2), 143-69.
*   Poole, M.S. & DeSanctis, G. (1994) "Capturing the complexity in advanced technology use : adaptive structuration theory". _Organization Science_, **5** (2): 121-47.
*   Poole, M.S. & G. DeSanctis. (1990) "Understanding the use of group decision support systems : the theory of adaptive structuration" .In Fulk, J. & C. W. Steinfield, C. W. editors. _Organizations and information technology_. Newbury Park, CA: Sage. pp. 173-193
*   Robey, D. & Boudreau, M-C. (1999) "Accounting for the contradictory consequences of IT: theoretical directions and methodological implications". _Informations Systems Research_, **10** (2), 167-185.
*   Robey, D. & Sahay, S. (1996) "Transforming work through IT: a comparative case study of Geographic Information Systems in County government". _Information Systems Research_, **7** (1): 93-110.
*   Rowe, F. & Struck, D. (1995) "L'interaction télécommunications-structure des organisations: perspectives, théories et méthodes". _Economie et Sociétés, série Sciences de gestion_, **21** (5), 51-83.
*   Scheepers, R. & Damsgaard, J. (1997) "Using internet technology within the organization: a structurational analysis of intranets". Paper presented at Group 97 Phoenix Arizona, USA. New York, NY: Association for Computing Machinery. pp. 9-18
*   Strauss, S. G & et al. (1998) "Human resource management practices in the networked organization: impacts of electronic communication systems". _Journal of organizational behavior_, **5** 127-154.
*   Swanson, E. B. & N. C Ramiller. (1997) "The organizing vision in information systems innovation". _Organization Science_, **8** (5), 458-174.
*   Tyre, M. J. & Orlikowski, W. J. (1994) "Windows of opportunity: temporal patterns of technological adaptation in organizations". _Organization Science_, **5** (1), 98-118.
*   Venkatraman, N. (1998) "Real strategies for virtual organizing". _Sloan Management Review_, **40** (1): 33-48.
*   Weick, K. E. (1990) "Technology as equivoque: sensemaking in new technologies".In Goodman, P. S. &Sproull, L. S. editors, _Technology and organizations_. San Francisco, CA: Josey Bass. pp. 1-44
*   Wilder, C. (1999). "e-business work starts". _Information week_, January: 53-54.
*   Yin, R.K. (1989). _Case study research_. London: Sage