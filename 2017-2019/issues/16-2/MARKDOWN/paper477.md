#### vol. 16 no. 2, June, 2011

# Factors affecting the information behaviour of managers in the Kuwaiti civil service: a relational model

#### [Helaiel Almutairi](#author)  
Public Administration Department, College of Administrative Sciences, Kuwait University, Kuwait

#### Abstract

> **Aim.** The purpose of the study is to understand the impact of personal and professional factors on Kuwaiti public managers' information behaviour.  
> **Method.** Data were gathered using a questionnaire sent to a total of 400 staff in eighteen ministries. A response rate of 80% was obtained. Data were collected on six person-related variables: age, sex, education, management level, job experience and information system use, and three information behaviour dimensions: information characteristics, information types, and information sources.  
> **Analysis**. The statistical analysis package, SPSS, was used to carry multivariate analysis of variance, analysis of variance, and multiple comparisons with the Scheffe test.  
> **Results.** The study's findings indicated that age, education, and information system use are the only contextual variables that make a difference in the three information dimensions. The three independent variables were further subjected to a more detailed analysis to understand which of the subgroups' means differ significantly from the others and what direction the differences (positive/negative) take.  
> **Conclusion**. The general finding is that there are subgroup differences in terms of the impact on information behaviour of age, educational level and information system use.

## Introduction

_'Information behavior is a term whose time has come'._ ([Case 2002](#cas02): 76)

The importance of information behaviour springs from the importance of the first part of this concept, that is, information. Organizations need information because they are open social systems that must process information to accomplish tasks and connect effectively with the external environment ([Katz and Kahn 1966](#kat66); [Lawrence and Lorsch 1967](#law67); [Thompson 1967](#tho67)).

As members of an organization, managers also depend on information to carry out their operations and make rational decisions, potentially achieving success as a result. Several researchers have found empirical evidence for the prominent role of information in a manager's work (e.g., [Mintzberg 1973](#min73), [1991](#min91); [Auster and Choo 1993](#aus93); [Ramdeen and Fried 2003](#ram03); [Almutairi 2011](#alm11)). Mintzberg argued, '_It is the informational roles that tie all managerial work together_' ([Mintzberg 1973](#min73): 71-2). He found that 40% of managers' time was spent on the transmission of information and 70% of their incoming mail was purely informational as opposed to presenting requests for action ([Mintzberg 1991](#min91)). In the same vein, Auster and Choo ([1993](#aus93)) stated that managers function as an information-processing system in which information is received, its flow is directed, and action is taken based on this information.

At both levels, organizational and managerial, the importance of information springs from the necessity of dealing with _uncertainty,_ which results from viewing an organization as an open social system. Thompson argued that uncertainty is the '_fundamental problem of complex organization and coping with uncertainty is the essence of administrative process_' ([Thompson 1967: 159](#tho67)). He identified two major sources of uncertainty: technology and environment.

Today, these two sources of uncertainty are becoming more complex and continually changing. Thus, the degree of uncertainty is becoming very high. To be able to survive and thrive in such degree of uncertainly, organizations need to increase their information-processing capacity. Several researchers have argued that the organization has limited information-processing capacity (e.g., [Simon 1960](#sim60); [Swanson 2003](#swa03)).

In order to increase this capacity, organizations need, for instance, to increase managers' information management skills (collecting, analysing, and transmitting). In other words, they need to enhance managers' information behaviour. Further, it is essential to know what factors impact on managerial information behaviour because these may hinder or enhance this type of behaviour. Lack of the understanding of these factors leaves managers blind in the area of how to control their information behaviour, which is essential in the emerging environment that is built around information.

In 1968, [Paisley](#pai68) studied factors affecting the information behaviour of scientists and concluded that the progress of this line of research seems not fast enough to keep up with the speed of information technologies and information society changes. In 1993, [Auster and Choo](#aus93) argued that there is a dearth of knowledge on how managers acquire and use information in their work. More recently, in 2007, [Losch and Lambert](#los07) argued that the conceptualisation and nature of information behaviour are still not clear.

Consequently, as cited by Case ([2002](#cas02)), information behaviour is a concept that researchers still need to focus on because it is one of the core factors that distinguishes between success and failure in the new organizational world. The purpose of this study is to contribute to understanding and enhancing managers' information behaviour through advancing our knowledge of the factors affecting this behaviour. More specifically, this study investigates these factors in the context of Kuwaiti public managers.

## Literature review

### Treatment of personal factors in the literature

In his review of information behaviour research, Case ([2007](#cas07)) noted that human information behaviour is more complex and interpersonal than it was previously assumed. He added that one of the themes in current information behaviour research is to include context, which can be studied by including demographic factors. This complexity of information behaviour is a mirror of the complexity of the environment we live in nowadays, and that requires adding more variables to our research to explain this behaviour.

Several theories and models that attempt to explain human behaviour have recognised this complexity and the need to use additional variables that were not included previously (e.g., theory of planned behaviour, social cognitive theory). For example, social cognitive theory ([Bandura 2001](#ban01)) attempts to explain how the interaction of cognitive and social factors shapes behaviour, and one of its propositions is that individuals live in a social system that consists of many factors, such as sex and education. Each social system has its own messages as to whether to take and not to take certain actions; in making their behavioural choices, individuals are motivated to engage in any behaviour that is valued by their social system.

In the same vein, in his theory of planned behaviour, Ajzen ([1991](#ajz91)) recognised the importance of contextual variables on human behaviour, which was clear through giving importance to the antecedents of the main concepts in the theory. In this regard, beliefs relevant to behaviour were considered by Ajzen as the basic level of explanation for behaviour and the prevailing determinants of a person's intentions and actions. Three kinds of beliefs were argued to influence behaviour: behavioural beliefs (which influence attitudes), normative beliefs (which influence subjective norms), and control beliefs (which influence perceptions of behavioural control).

Each one of these three beliefs carries in itself an impact on actual behaviour, which can be direct or indirect, through masking the impact of other variables such as past experiences. Ajzen ([1991](#ajz91)) stated, for instance, that the control belief deals with the existence or absence of resources and opportunities. The author added,

> These control beliefs may be based in part on past experience with the behavior, but they will usually also be influenced by second-hand information about the behavior, by the experiences of acquaintances and friends, and by other factors that increase or reduce the perceived difficulty of performing the behavior in question' ([Ajzen 1991: 196](#ajz91)).

Consequently, personal variables, such as age, sex, and education, and work-related variables, such as experience, task, and structure can play a vital role in shaping behaviour.

Similar to these general human behaviour models (social cognitive theory and theory of planned behaviour), a number of more specific information behaviour models. Wilson ([1981](#wil81), [1999](#wil99)) developed a set of models explaining information behaviour and representing a generic conceptual framework of information behaviour. Wilson argued that the need for information is what triggers information behaviour, which, in turn, consists of three interrelated activities: seeking information, exchanging information, and using information. These three activities are repeated until the information need is satisfied.

Wilson ([1999](#wil99)) argued that information needs are not primary needs; rather, they are secondary ones, stemming from basic needs. Basic needs can be defined as physiological, cognitive, or affective. The context of these needs could be the person himself or herself, his or her role in work or life, and the environments (political, economic, technological, etc.) within which the work and life take place. Also, Wilson argued, barriers to information search will spring from the same set of contexts. Thus, he stated that his model, _'also embodies, implicitly, a set of hypotheses about information behaviour that are testable: for example, the proposition that information needs in different work roles will be different, or that personal traits may inhibit or assist information seeking_' ([Wilson 1999: 253](#wil99)).

In the same vein, other theories and models of information behaviour have also drawn attention to the impact of specific contextual variables. For example, the theory of information activities in work tasks ([Bystrom 2006](#bys06)) focused on the role of task complexity in the use of information types and sources. Bystrom argued that when task complexity increases, people tend to use more information types and are less knowledgeable about the type of information they need to obtain. Furthermore, this theory proposed that when a task is perceived to be complex, people tend to rely on experts as a source of information.

Another information behaviour theory that also recognised the role of context is the theory of information use environments ([Taylor 1991](#tay91)). This theory proposed that a user's environment or situation plays a major role in defining the information needed and its purposes; thus, contextual variables such as nature of a problem (structured or unstructured), previous knowledge of the problem, and clear work assumptions are factors impacting the use of information types and require different information types ([Palmquist 2006](#pal06)). As a result, this theory attempted to link certain types of context and information behaviour for engineers, legislators, and physicians, grounding this linkage in the argument that each group has different assumptions of what makes information useful and valuable ([Palmquist 2006](#pal06)).

The theory of work task information-seeking and retrieval processes ([Hansen 2006](#han06)) also presented an argument similar to that of the theory of information use environments. Herein, however, the view of context was much broader. According to this theory, to understand work task information, there is a need to view this behaviour as embedded in a broader task context that consists of work task context, situational and individual context, and organizational and social context. According to [Hansen (2006](#han06)), the information behaviour should be investigated in terms of how these different context levels interact with each other and how this interaction affects information behaviour. 

Consequently, like human behaviour theories, information behaviour theories raised the issue of the importance of context in forming information behaviour. To add to this issue, Mick _et al._ ([1980](#mic80)) stated that information travels through complex paths and information behaviour is the product of a complex interaction of several factors such as personal factors, environment, and task. Furthermore, it is rare that the same information behaviour is expressed by two individuals; therefore, information behaviour improvement intervention should focus on specific situations and variables that can be controlled, such as factors within organizational boundaries.

### Personal factors in empirical literature

Empirical investigation has also linked several personal variables and various information behaviour dimensions. The sex of the respondent, for example, is argued to be a variable that makes a difference in information behaviour ([Palsdottir 2003](#pal03); [Urquhart and Yeoman 2009](#urq09)) and it was found that women are more active with health information than men (e.g., [Kenkel 1990](#ken90); [Rutten _et al._ 2006](#rut06); [Lorence and Park 2007](#lor07)). Hupfer and Detlor ([2006](#hup06)) used men's and women's traits and argued that women are greater information seekers than men because of the traits associated with women and, more specifically, the other-orientation trait associated with women as opposed to self-orientation. In the same vein, Steinerova and Susol ([2007](#ste07)) studied information behaviour in libraries in Slovakia and found that men prefer individual work and independent information seeking. Women, on the other hand, tend to use librarians' help, catalogues and reference works more often than men do; women are more patient in information seeking, while men use faster retrieval tools.

Barnard [(1991)](#bar91) tested whether certain demographic variables influence the choice of media by new managers who handle personnel problems. These demographic variables included experiences in current organization, age, and sex. The researcher found that new managers in the service sector use business mail more frequently than their counterparts in other types of organizations; managers appointed from outside the organizations make more use of superiors and scheduled meetings, while those from inside the organizations make more use of informal channels of subordinates and business mail as sources of information. Moreover, younger managers (under the age of 40) use social activities as information sources more often and they find subordinates and peers more useful than older managers do; male managers also use subordinates as information sources more frequently than female managers do. In explaining the results, the researcher argued that the use of peers by managers appointed from inside the organization is logical because they have had experience in dealing with them and thus feel comfortable in using them as information sources, and young managers' use of subordinates as information sources may be a reflection of a trend towards an open management style.

Thus, previous studies provide clear empirical evidence that personal and professional characteristics do make a difference in information behaviour. Also, several other empirical studies either confirmed the previous studies' findings ([Larner 2006](#lar06); [Pors 2008](#por08)) or found support for other variables such as time ([Williams and Coles 2007](#wil07)), lack of awareness of the availability of services ([Ramlogan and Tedd 2006](#ram06)), and knowing what a person knows, valuing what a person knows and being able to have access to that person's thinking ([Borgatti and Cross 2003](#bor03)).

However, looking at these empirical studies we notice that there are still several research opportunities in the quest to understand managers' information behaviour. First, most of these studies were applied in public organizational settings which include libraries ([Pors 2008](#por08)) and private sector organizations ([Auster and Choo 1993](#aus93)) or dealt with persons in specific positions such as teachers and head teachers ([Williams and Coles 2007](#wil07)) and undergraduate university students ([Ramlogan and Tedd 2006](#ram06)). Secondly, most of these studies were applied in Western countries such as the UK ([Williams and Coles 2007](#wil07)), Canada ([Auster and Choo, 1993](#aus93)), Slovakia ([Steinerova and Susol 2007](#ste07)), Poland ([Niedzwiedzka 2003](#nie03)) and the USA ([Barnard 1991](#bar91)). Finally, most of these studies focused on only parts of the information behaviour, which included information sources ([Barnard 1991](#bar91)), frequency of use ([Larner 2006](#lar06); [Pors 2008](#por08)), and information usage and choice of information source ([Auster and Choo 1993](#aus93); [Niedzwiedzka 2003](#nie03); [Williams and Coles 2007](#wil07)).

The present study aims to develop a relational model that includes several personal and information behaviour variables. Based on the preceding literature review and lack of examination of Kuwaiti public managers' information behaviour, the present study attempts to answer the following general question:

_Do Kuwaiti public managers' personal and professional factors create differences in their information behaviour?_

First this study is a response to the call by several researchers (e.g., [Mick _et al._ 1980](#mic80); [Borgatti and Cross 2003](#bor03); [Case 2007](#cas07); [Steinervoa and Susol 2007](#ste07)) to view information behaviour and factors impacting upon it as a complex phenomenon that involves several types of factors and relationships (see Figure 1). Such a view would lead to knowing which relationship, or part thereof, could enhance or hinder informational activities. This is one of this model's purposes. Second, this study attempts to validate the propositions and empirical findings of the previously cited studies by investigating information behaviour of non-Western managers and, more specifically, managers in Kuwaiti government ministries, thereby enhancing the generalisation of those findings.

<div align="center">![Figure 1: The study model](p477fig1.jpg)</div>

<div align="center">  
**Figure 1: The study model.**</div>

## Research methods

### Population and sampling

The population of this study comprises managers who work in the eighteen Kuwaiti ministries. Because of time and cost constraints, a sample was taken from staff in all eighteen ministries from all managerial levels using a computer-based, random sampling method. In this way, eight ministries (Education, Foreign Affairs, Interior, Communication, Health, Energy, Welfare, and Defence) and 400 managers were selected.

The composition of the sample was as follows: 26% of the respondents were female while 74% were male; 71% were 40 years old or less; and 58% had an education level higher than high school. Half of the respondents (50%) had served in the Kuwaiti government sector for between 1 and 10 years and 49% had served more than 10 years. Regarding the respondents' managerial level, 25% were low level managers, 29% were middle managers and 47% were top managers. 73% of the respondents indicted that their usage of information system in tasks related to their work to be good and excellent, 18% reported moderate use, while 9% indicated between no use to weak use.

A questionnaire was used to obtain the data for this study (see the [Appendix](#app)). The questionnaire was divided into two sections, one consisted of questions measuring the three information dimensions, while the other was designed to measure personal and professional variables. In total, 400 questionnaires were distributed and 321 were returned: a response rate of 80%. Of the returned questionnaires, 94 were eliminated because the participants either provided two answers for the same question or left most of the questions unanswered, leaving 227 usable questionnaires (55%).

### Measurement

#### The three information behaviour dimensions.

Reviewing empirical studies that dealt with information behaviour dimensions (e.g., [O'Reilly 1982](#ore82); [Ashill and Jobber 2001](#ash01); [Breen 2005](#bre05); [Koksal 2008](#kok08); [Wright and Evans 2009](#wri09)), one clearly notices the fragmentation of these dimensions along a wide group of studies. The present study attempts to collect most dimensions in one model. Thus, information behaviour, in this study, was first grouped into three general categories: the perceived importance of information characteristics, the types of information used, and information sources used. These categories have been used individually by other researchers (e.g., [O'Reilly 1982](#ore82); [Breen 2005](#bre05)). This grouping of information behaviour dimensions is consistent with the practice of other researchers (e.g., [Trumbach 2006](#tru06); [Dearman _et al._ 2008](#dea08); [Church and Smyth 2009](#chu09)). Following is a detailed discussion of each category. 

The information characteristics dimension was measured using ten information characteristics: subjectivity, timeliness, specificity, clarity, comparability, trustworthiness, unbiased, usefulness, flexibility, and truthfulness. Responses ranged from 1 (_no importance_) to 5 (_very high importance_). These characteristics have been adopted from other studies in the literature (e.g., [Ashill and Jobber 2001](#ash01); [Wright and Evans 2009](#wri09)).

The information types dimension was measured using four general information types: general information about the organization, information about the employees, information about the customers, and information about organizational plans and procedures. A five-point Likert-type scale, ranging from 1 (_very high disagreement_) to 5 (_very high agreement_), was used. All information types used in the present study have been previously tested by other researchers (e.g., [O'Reilly 1982](#ore82); [Koksal 2008](#kok08)). 

The information source dimension was measured using twenty-eight information sources. These information sources were divided into three groups: traditional, electronic, and personal. Yes/No answers were employed to indicate the usage of the information sources. Several researchers (e.g., [O'Reilly 1982](#ore82); [Breen 2005](#bre05); [Koksal 2008](#kok08)) have used these information sources.

**The six personal and professional variables**

The personal variables were age, sex, and education, while the professional ones were management level, job experience, and information system use. All of these variables were measured using nominal or ordinal measures. Sex was indicated using nominal measures, while the other independent variables (age, education, management level, job experiences) were measured using ordinal measures. These six independent variables represent characteristics (professional or personal) and are not a measure of a dimension or an attitude. Thus, there are no reliability and validity measures for these variables.

All measures were subjected to a multi-step process to check for validity and reliability. First, the questionnaire was submitted to a panel of four professors at Kuwait University to check whether the items in the questionnaire measured what they should and whether they suited the Kuwaiti business environment. No major changes were suggested by the panel. All suggestions were related to the format of the questionnaire, and all these suggestions were incorporated into the final version of the questionnaire. Secondly, a pilot study was conducted to check whether the items in the questionnaire were understandable and clear. The study covered sixty employees from three Kuwaiti ministries. The findings of the pilot study indicated that all questions and concepts were clear and understandable.

Cronbach's alpha (an internal consistency technique) was used to assess the reliability of each scaled measure, which included the three information behaviour dimensions (information characteristics, information types, and information sources). A cut-off of 0.70 is considered the minimum acceptable level of reliability ([Nunnally 1967](#nun67)) and is adopted in this study. Cronbach's alpha coefficients for the three measures were as follows: 0.84 for information characteristics, 0.88 for information types, and 0.90 for information sources. Thus, this measure exceeded the cut-off accepted in this study.

## Data analysis and results

Data analysis was conducted using multivariate analysis of variance (MANOVA), analysis of variance (ANOVA), and multiple comparisons with the Scheffe test. The logic behind the use of these statistical methods was that each provides one type of output that is different from the outputs of the other statistical methods in terms of broadness and/or scope of the output. The outputs that these statistical methods provide range from a more general output (MANOVA) to a more specific output (the Scheffe test). At the same time, these outputs complement each other, thereby drawing a complete picture of the relationships in the study's model.

### MANOVA analysis

In this analysis, the independent variables are the three personal and three professional factors, while the dependent variables are the three information behaviour dimensions (information characteristics, information types, information sources). First, the overall differences between the six independent variables and the three dependent variables were assessed using multivariate analysis of variance. Table 1 presents the results of the MANOVA tests, which indicated that the differences are statistically significant in only age, education, and information system use. Consequently, these three independent variables are focused on in subsequent analyses.

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
Table 1: Multivariate tests (Pillai's Trace)  
</caption>

<tbody>

<tr>

<th valign="middle">Effect</th>

<th valign="middle">Value</th>

<th valign="middle">F</th>

<th valign="middle">Hypothesis df</th>

<th valign="middle">Error df</th>

<th valign="middle">Sig.</th>

<th valign="middle">Partial Eta Squared</th>

</tr>

<tr bgcolor="#E8FBA4">

<td>Age</td>

<td align="center">0.226</td>

<td align="center">1.472</td>

<td align="center">34</td>

<td align="center">392</td>

<td align="center">0.046*</td>

<td align="center">0.113</td>

</tr>

<tr>

<td>Sex</td>

<td align="center">0.109</td>

<td align="center">1.404</td>

<td align="center">17</td>

<td align="center">195</td>

<td align="center">0.137</td>

<td align="center">0.109</td>

</tr>

<tr bgcolor="#E8FBA4">

<td>Education</td>

<td align="center">0.265</td>

<td align="center">1.761</td>

<td align="center">34</td>

<td align="center">392</td>

<td align="center">0.006*</td>

<td align="center">0.132</td>

</tr>

<tr>

<td>Management level</td>

<td align="center">0.097</td>

<td align="center">0.588</td>

<td align="center">34</td>

<td align="center">392</td>

<td align="center">0.970</td>

<td align="center">0.048</td>

</tr>

<tr>

<td>Job experience</td>

<td align="center">0.189</td>

<td align="center">0.781</td>

<td align="center">51</td>

<td align="center">591</td>

<td align="center">0.864</td>

<td align="center">0.063</td>

</tr>

<tr bgcolor="#E8FBA4">

<td>Information system use</td>

<td align="center">0.436</td>

<td align="center">1.425</td>

<td align="center">68</td>

<td align="center">792</td>

<td align="center">0.017*</td>

<td align="center">0.109</td>

</tr>

<tr>

<td colspan="7">* Sig. at =0.05;</td>

</tr>

</tbody>

</table>

Analysis of variance (ANOVA) was used to test for differences among the six independent variables. Table 2 shows the results: only statistically significant variables are reported in this table. The independent variable of age was found to be statistically different in one type of information (employee information), one information source (personal sources), and one information characteristic (comparability). The independent variable of education was found to be statistically different in three information characteristics (subjectivity, clarity, unbiased). Finally, the independent variable of information system use was found to be statistically different in all three information types (employee information, customer information, and general organizational information), two information sources (traditional and personal), and ten information characteristics (subjectivity, timeliness, specificity, clarity, comparability, trustworthiness, unbiased, usefulness, flexibility, and truthfulness). Therefore, these independent variables were subjected to a more detailed analysis.

<table width="85%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Tests of between-subjects effects**  
</caption>

<tbody>

<tr>

<th valign="middle">Source</th>

<th valign="middle">Dependent variable</th>

<th valign="middle">Type III sum  
of squares</th>

<th valign="middle">df</th>

<th valign="middle">Mean square</th>

<th valign="middle">F</th>

<th valign="middle">Sig.</th>

<th valign="middle">Partial eta squared</th>

</tr>

<tr>

<th rowspan="4" valign="middle">Age</th>

</tr>

<tr>

<td>Info. type: employee information</td>

<td align="center">8.090</td>

<td align="center">2</td>

<td align="center">4.045</td>

<td align="center">9.283</td>

<td align="center">0.000**</td>

<td align="center">0.081</td>

</tr>

<tr>

<td>Source: personal</td>

<td align="center">18.816</td>

<td align="center">2</td>

<td align="center">9.408</td>

<td align="center">3.959</td>

<td align="center">.021*</td>

<td align="center">.036</td>

</tr>

<tr>

<td>Characteristic: comparability</td>

<td align="center">5.175</td>

<td align="center">2</td>

<td align="center">2.587</td>

<td align="center">3.036</td>

<td align="center">0.050*</td>

<td align="center">0.028</td>

</tr>

<tr>

<th rowspan="4" valign="middle">Education</th>

</tr>

<tr>

<td>Characteristic: subjectivity</td>

<td align="center">9.538</td>

<td align="center">2</td>

<td align="center">4.769</td>

<td align="center">7.955</td>

<td align="center">0.000**</td>

<td align="center">0.070</td>

</tr>

<tr>

<td>Characteristic: clarity</td>

<td align="center">4.423</td>

<td align="center">2</td>

<td align="center">2.211</td>

<td align="center">3.873</td>

<td align="center">0.023*</td>

<td align="center">0.035</td>

</tr>

<tr>

<td>Characteristic: unbiased</td>

<td align="center">11.119</td>

<td align="center">2</td>

<td align="center">5.560</td>

<td align="center">5.509</td>

<td align="center">0.005**</td>

<td align="center">0.050</td>

</tr>

<tr>

<th rowspan="15" valign="middle">Information  
system use</th>

</tr>

<tr>

<td>Info. type: employee information</td>

<td align="center">6.788</td>

<td align="center">4</td>

<td align="center">1.697</td>

<td align="center">3.894</td>

<td align="center">0.004**</td>

<td align="center">0.069</td>

</tr>

<tr>

<td>Info. type: customer information</td>

<td align="center">10.045</td>

<td align="center">4</td>

<td align="center">2.511</td>

<td align="center">4.635</td>

<td align="center">0.001**</td>

<td align="center">0.081</td>

</tr>

<tr>

<td>Info. type: organizational plans</td>

<td align="center">7.327</td>

<td align="center">4</td>

<td align="center">1.832</td>

<td align="center">4.610</td>

<td align="center">0.001**</td>

<td align="center">0.080</td>

</tr>

<tr>

<td>Source: traditional</td>

<td align="center">28.944</td>

<td align="center">4</td>

<td align="center">7.236</td>

<td align="center">3.248</td>

<td align="center">0.013*</td>

<td align="center">0.058</td>

</tr>

<tr>

<td>Source: personal</td>

<td align="center">24.564</td>

<td align="center">4</td>

<td align="center">6.141</td>

<td align="center">2.585</td>

<td align="center">0.038*</td>

<td align="center">0.047</td>

</tr>

<tr>

<td>Characteristic: subjectivity</td>

<td align="center">6.819</td>

<td align="center">4</td>

<td align="center">1.705</td>

<td align="center">2.843</td>

<td align="center">0.025*</td>

<td align="center">0.051</td>

</tr>

<tr>

<td>Characteristic: timeliness</td>

<td align="center">7.226</td>

<td align="center">4</td>

<td align="center">1.806</td>

<td align="center">2.600</td>

<td align="center">0.037*</td>

<td align="center">0.047</td>

</tr>

<tr>

<td>Characteristic: specificity</td>

<td align="center">12.033</td>

<td align="center">4</td>

<td align="center">3.008</td>

<td align="center">4.494</td>

<td align="center">0.002**</td>

<td align="center">0.078</td>

</tr>

<tr>

<td>Characteristic: clarity</td>

<td align="center">6.178</td>

<td align="center">4</td>

<td align="center">1.545</td>

<td align="center">2.680</td>

<td align="center">0.033*</td>

<td align="center">0.048</td>

</tr>

<tr>

<td>Characteristic: comparability</td>

<td align="center">10.773</td>

<td align="center">4</td>

<td align="center">2.693</td>

<td align="center">3.160</td>

<td align="center">0.015</td>

<td align="center">0.057</td>

</tr>

<tr>

<td>Characteristic: trustworthiness</td>

<td align="center">11.924</td>

<td align="center">4</td>

<td align="center">2.981</td>

<td align="center">3.997</td>

<td align="center">0.004**</td>

<td align="center">0.149</td>

</tr>

<tr>

<td>Characteristic: usefulness</td>

<td align="center">20.964</td>

<td align="center">4</td>

<td align="center">5.241</td>

<td align="center">9.229</td>

<td align="center">0.000**</td>

<td align="center">0.149</td>

</tr>

<tr>

<td>Characteristic: flexibility</td>

<td align="center">23.438</td>

<td align="center">4</td>

<td align="center">5.859</td>

<td align="center">6.247</td>

<td align="center">0.000**</td>

<td align="center">0.106</td>

</tr>

<tr>

<td>Characteristic: truthfulness</td>

<td align="center">10.606</td>

<td align="center">4</td>

<td align="center">2.652</td>

<td align="center">3.239</td>

<td align="center">0.013*</td>

<td align="center">0.058</td>

</tr>

<tr>

<td colspan="8">* Sig. at =0.05;      ** Sig. at =0.00</td>

</tr>

</tbody>

</table>

The Scheffe test was used to determine which of the subgroups' means differ significantly from the others and the direction (positive or negative) of the differences. Tables 3 and 4 present the findings from this test for age and education. The table related to information system use is not included because of its large size. Several important conclusions can be drawn from the Scheffe test for the three independent variables.

Regarding age groups:

*   Managers who tended to use information related to employees were characterised by being 30 years old or older.
*   Managers who preferred _comparable_ information tended to be 40 years old or younger.

<table width="65%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Multiple comparisons with the Scheffe test at age group.**  
</caption>

<tbody>

<tr>

<th valign="middle">Dependent variable</th>

<th valign="middle">(I) age</th>

<th valign="middle">(J) age</th>

<th valign="middle">Mean difference (I-J)</th>

<th valign="middle">Sig.</th>

</tr>

<tr>

<th rowspan="10" valign="middle">information about  
employees</th>

</tr>

<tr>

<th rowspan="3" valign="middle"><30</th>

</tr>

<tr>

<td align="center">30-40</td>

<td align="center">-0.3637 *</td>

<td align="center">0.003</td>

</tr>

<tr>

<td align="center">41+</td>

<td align="center">-0.3647 *</td>

<td align="center">0.006</td>

</tr>

<tr>

<th rowspan="3" valign="middle">30-40</th>

</tr>

<tr>

<td align="center"><30</td>

<td align="center">0.3637*</td>

<td align="center">0.003</td>

</tr>

<tr>

<td align="center">41+</td>

<td align="center">-0.0010</td>

<td align="center">1.000</td>

</tr>

<tr>

<th rowspan="3" valign="middle">41+</th>

</tr>

<tr>

<td align="center"><30</td>

<td align="center">0.3647*</td>

<td align="center">0.006</td>

</tr>

<tr>

<td align="center">30-40</td>

<td align="center">0.0010</td>

<td align="center">1.000</td>

</tr>

<tr>

<th rowspan="10" valign="middle">personal</th>

</tr>

<tr>

<th rowspan="3" valign="middle"><30</th>

</tr>

<tr>

<td align="center">30-40</td>

<td align="center">-0.2724</td>

<td align="center">0.539</td>

</tr>

<tr>

<td align="center">41+</td>

<td align="center">-0.4324</td>

<td align="center">0.263</td>

</tr>

<tr>

<th rowspan="3" valign="middle">30-40</th>

</tr>

<tr>

<td align="center"><30</td>

<td align="center">0.2724</td>

<td align="center">0.539</td>

</tr>

<tr>

<td align="center">41+</td>

<td align="center">-0.1600</td>

<td align="center">0.815</td>

</tr>

<tr>

<th rowspan="3" valign="middle">41+</th>

</tr>

<tr>

<td align="center"><30</td>

<td align="center">0.4324</td>

<td align="center">0.263</td>

</tr>

<tr>

<td align="center">30-40</td>

<td align="center">0.1600</td>

<td align="center">0.815</td>

</tr>

<tr>

<th rowspan="10" valign="middle">comparability</th>

</tr>

<tr>

<th rowspan="3" valign="middle"><30</th>

</tr>

<tr>

<td align="center">30-40</td>

<td align="center">0.09</td>

<td align="center">0.824</td>

</tr>

<tr>

<td align="center">41+</td>

<td align="center">0.49*</td>

<td align="center">0.009</td>

</tr>

<tr>

<th rowspan="3" valign="middle">30-40</th>

</tr>

<tr>

<td align="center"><30</td>

<td align="center">-0.09</td>

<td align="center">0.824</td>

</tr>

<tr>

<td align="center">41+</td>

<td align="center">0.40*</td>

<td align="center">0.032</td>

</tr>

<tr>

<th rowspan="3" valign="middle">41+</th>

</tr>

<tr>

<td align="center"><30</td>

<td align="center">-0.49 *</td>

<td align="center">0.009</td>

</tr>

<tr>

<td align="center">30-40</td>

<td align="center">-0.40 *</td>

<td align="center">0.032</td>

</tr>

<tr>

<td colspan="8">* Sig. at <0.05;  
 The mean difference is that between persons of age I and person of age J.</td>

</tr>

</tbody>

</table>

Regarding education level:

*   Managers who preferred the _subjectivity_ information characteristic tended to have a bachelor's degree.
*   Managers who preferred the _clarity_ information characteristic tended to have a bachelor's degree.
*   Managers who preferred the _unbiased_ information characteristic tended to have a bachelor's degree.

<table width="75%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Multiple comparisons with the Scheffe test at education level.**  
</caption>

<tbody>

<tr>

<th valign="middle">Dependent variable</th>

<th valign="middle">(I) education</th>

<th valign="middle">(J) education</th>

<th valign="middle">Mean difference (I-J)</th>

<th valign="middle">Sig.</th>

</tr>

<tr>

<th rowspan="10" valign="middle">subjective</th>

</tr>

<tr>

<th rowspan="3" valign="middle">Secondary education</th>

</tr>

<tr>

<td align="center">Bachelor's degree</td>

<td align="center">-0.38*</td>

<td align="center">0.002</td>

</tr>

<tr>

<td align="center">Higher university degree</td>

<td align="center">-0.396</td>

<td align="center">0.251</td>

</tr>

<tr>

<th rowspan="3" valign="middle">Bachelor's degree</th>

</tr>

<tr>

<td align="center">Secondary education</td>

<td align="center">0.38*</td>

<td align="center">0.002</td>

</tr>

<tr>

<td align="center">Higher university degree</td>

<td align="center">-0.017</td>

<td align="center">0.997</td>

</tr>

<tr>

<th rowspan="3" valign="middle">Higher university degree</th>

</tr>

<tr>

<td align="center">Secondary education</td>

<td align="center">0.396</td>

<td align="center">0.251</td>

</tr>

<tr>

<td align="center">Bachelor's degree</td>

<td align="center">0.017</td>

<td align="center">0.997</td>

</tr>

<tr>

<th rowspan="10" valign="middle">clarity</th>

</tr>

<tr>

<th rowspan="3" valign="middle">Secondary education</th>

</tr>

<tr>

<td align="center">Bachelor's degree</td>

<td align="center">-0.206</td>

<td align="center">0.145</td>

</tr>

<tr>

<td align="center">Higher university degree</td>

<td align="center">0.419</td>

<td align="center">0.199</td>

</tr>

<tr>

<th rowspan="3" valign="middle">Bachelor's degree</th>

</tr>

<tr>

<td align="center">Secondary education</td>

<td align="center">0.206</td>

<td align="center">0.145</td>

</tr>

<tr>

<td align="center">Higher university degree</td>

<td align="center">0.62*</td>

<td align="center">0.026</td>

</tr>

<tr>

<th rowspan="3" valign="middle">Higher university degree</th>

</tr>

<tr>

<td align="center">Secondary education</td>

<td align="center">-0.419</td>

<td align="center">0.199</td>

</tr>

<tr>

<td align="center">Bachelor's degree</td>

<td align="center">-0.62*</td>

<td align="center">0.026</td>

</tr>

<tr>

<th rowspan="10" valign="middle">unbiased</th>

</tr>

<tr>

<th rowspan="3" valign="middle">Secondary education</th>

</tr>

<tr>

<td align="center">Bachelor's degree</td>

<td align="center">-0.41*</td>

<td align="center">0.013</td>

</tr>

<tr>

<td align="center">Higher university degree</td>

<td align="center">-0.294</td>

<td align="center">0.635</td>

</tr>

<tr>

<th rowspan="3" valign="middle">Bachelor's degree</th>

</tr>

<tr>

<td align="center">Secondary education</td>

<td align="center">0.41*</td>

<td align="center">0.013</td>

</tr>

<tr>

<td align="center">Higher university degree</td>

<td align="center">0.117</td>

<td align="center">0.929</td>

</tr>

<tr>

<th rowspan="3" valign="middle">Higher university degree</th>

</tr>

<tr>

<td align="center">Secondary education</td>

<td align="center">0.294</td>

<td align="center">0.635</td>

</tr>

<tr>

<td align="center">Bachelor's degree</td>

<td align="center">-0.117</td>

<td align="center">0.929</td>

</tr>

<tr>

<td colspan="8">* Sig. at =0.05</td>

</tr>

</tbody>

</table>

Regarding information system use:

*   Managers who tended to use _information related to customers_ were characterised as being good or excellent users of information systems.
*   Managers who tended to use _information related to organization_ were characterised as being excellent users of information system.
*   Managers who tended to use _traditional information sources_ were characterised by good information system usage.
*   Managers who preferred the _subjectivity_ information characteristic tended to be excellent users of information systems.
*   Managers who preferred the _specificity_ information characteristic tended to be excellent users of information systems.
*   Managers who preferred the _comparability_ information characteristic tended to be excellent users of information system.
*   Managers who preferred the _trustworthiness_ information characteristic tended to be in different levels of information system usage (e.g., good, excellent).
*   Managers who preferred the _usefulness_ information characteristic were characterised by none, weak, or excellent information system usage.
*   Managers who preferred the _flexibility_ information characteristic were characterised by good or excellent information system use.
*   Managers who preferred the _truthfulness_ information characteristic were characterised by being excellent users of information systems.

## Discussion

The purpose of the study was to understand the impact of personal and professional characterists on the information behaviour of managers in Kuwaiti government ministries. Three personal (age, sex, education) and three professional (management level, job experience, information system use) variables were used, while information behaviour was measured using three information dimensions (information characteristics, information types and information sources). Figure 2 presents the variables found significant in this study.

<div align="center">![Figure 2: The study model after data analysis](p477fig2.jpg)</div>

<div align="center">  
**Figure 2: The study model after data analysis**</div>

The study's findings indicated that age, education, and information system use are the only variables of those selected that make a difference in the three information dimensions. Specifically, age was found to make a difference in one type of information (employees information), one information source (personal sources), and one information characteristic (comparable). Education was found to make a difference in three information characteristics (subjectivity, clarity, unbiased). Finally, information system use was found to make a difference in all three information types (employees information, customer information, general organizational information), two information sources (traditional, personal), and ten information characteristics (subjectivity, timeliness, specificity, clarity, comparability, trustworthiness, unbiased, usefulness, flexibility, truthfulness). In further subjecting the three independent variables to a more detailed analysis to understand which of the subgroups' means differ significantly from the others and what the direction of the differences (positive or negative) is, the general finding was that subgroup differences do exist. These differences have been detailed in the data analysis section above.

On the empirical level, the study's findings are consistent with the line of empirical research that has linked information behaviour and a wide range of personal and professional characteristics, which include time ([Ikoja-Odongo 2001](#iko01); [Williams and Coles 2007](#wil07); [Dearman _et al._ 2008](#dea08)), export performance measures ([Koksal 2008](#kok08)), environmental uncertainty ([Ashill and Jobber 2009](#ash09)), social interactions ([Church and Smyth 2009](#chu09)), special organizational circumstances such as turnaround and restructuring ([Bonnici and Fredenberger 1995](#bon95)), organizational change ([Ballantyne 1993](#bal93)), lack of awareness of the availability of services ([Ramlogan and Tedd 2006](#ram06)), knowing what a person knows or valuing what a person knows or being able to access that person's thinking ([Borgatti and Cross 2003](#bor03)), and sex ([Palsdottir 2003](#pal03); [Urquhart and Yeoman 2009](#urq09)). Consequently, empirical studies that have been applied in a wide range of countries and contexts, including Kuwaiti government ministries, give clear evidence that these variables play a major role in information behaviour.

More specifically, the study's findings are consistent with studies that found that education (e.g., [O'Reilly 1982](#ore82)) and age (e.g., [Barnard 1991](#bar91)) are the differentiating factors in information behaviour. In the context of this study, information related to measuring the performance of employees is most likely to be used by managers who are 30 years and older. A possible explanation for this result is that managers in this age category are most likely not in the entry stage, are in their mid-career, and occupy the middle level in the organizational hierarchy. Thus, the span of control for these managers is relatively large; they have the responsibility of evaluating the performance of a large number of employees and are required to report these evaluations to a high management level. Therefore, these managers are extensive consumers and producers of information related to other employees. Also, in this study, managers who are 40 years and older were found to prefer the _comparability_ information characteristic. This result may be a reflection of the mature mentality of these managers in terms of their skills to rationally make decisions that are based on careful comparative analysis of available information. Goodman ([1993](#goo93)) has argued that experienced managers have the ability to critically evaluate information, and managers who are 40 years and older are expected to have such experience.

The same explanation could also be extended to managers who have a bachelor's degree and prefer information characteristics such as unbiased, clarity, and subjectivity. Here, however, the maturity is due to education and not to age. Furthermore, information system use, as a professional characteristic, was found to make differences in most of information dimensions. Information systems have certainly departed from being a secondary instrument located at low hierarchical level within the organization and have become an important strategic business tool, essential for achieving business goals: thus, information system responsibilities are located at the highest level in the hierarchy. Information system practices and effects have thereby become institutionalised. Therefore, the impact of information system use on all organizational aspects is logical and expected. Several researchers have provided empirical proof for information system impact on aspects such as organizational productivity (see [Almutairi 2007](#alm07)). In the present study, empirical investigation has provided evidence that this impact is extended to the information dimensions.

On the other hand, this study's findings are inconsistent with some of the available studies that investigated information behaviour on one side, and personal and professional characteristics on the other (e.g., [Kenkel 1990](#ken90); [Goodman 1993](#goo93); [Palsdottir 2003](#pal03); [Rutten _et al._ 2006](#rut06); [Lorence and Park 2007](#lor07); [Urquhart and Yeoman 2009](#urq09)). A possible explanation for this inconsistency is the context where this study was conducted, that is, Kuwaiti ministries. Several researchers have argued that a difference in context may lead to differences in results (e.g., [Hofstede 1984](#hof84), [1991](#hof91); [Straub _et al._ 1997](#str97); [Almutairi 2008](#alm08); [Rose and Straub 1998](#ros98)). Thus, context could be the reason for this inconsistency with other studies.

On the theoretical level, the present study's findings (as with other studies) support the propositions of human behaviour and information behaviour models that argue that personal and professional factors influence general human behaviour (e.g., the theory of planned behaviour, social cognitive theory) and, more specifically, information behaviour, e.g., Wilson's ([1999](#wil99)) model of information behaviour, theory of information activities in work tasks, the information use environments, theory of work task-information-seeking and retrieval processes).

The present study contributes to these models by enhancing the validity of the propositions that age and education play a role in information behaviour and by extending this validity to the Kuwaiti government ministries. Further, this study provides empirical evidence that information system use, whose impact was rarely investigated in available studies, also plays a role in information system behaviour.

In general, the existence of the impact of the above factors on information behaviour indicates that information behaviour is the product and a combination of many factors. Maceviciute ([2006](#mac06)), in her discussion of the origin of information needs research in Russia, categorised these factors into two types: objective and subjective. The former includes factors such as society goals, tasks of various economic sectors, and directions of science and technology; the latter (subjective) includes people's psychological features, knowledge, and values. Both types, according to Maceviciute, form a dialectic unity.

## Discussion and conclusion

This study has several implications for research and practice. We will start with the implications for practice.

First, today managers are the true assets and vital tools for competitive advantage, because they have the responsibility to deal with the high degree of uncertainty that characterises the current and future environment. Thus, organizations need to recognise that creating effective and efficient managers does not come by chance; rather, a very careful planning is needed, which should focus on the key issues and take into account all important variables. Managers' information behaviour is an example of such issues.

Secondly, this study has provided evidence that age, education, and information system use are among the factors that affect managers' information behaviour. Consequently, organizations interested in enhancing information use by their managers may need to look at the three variables in terms of matching the nature of information (types, characteristics, sources) with the type of managers (age category, education level, information system use level).

Thirdly, information plays an essential role in any decision-making process; moreover, failure of decision-making is sometimes due to issues such as lack of use of information in the decision-making process. To overcome these problems, managers interested in enhancing their decision-making skills may need to consider the fact that available types, characteristics and sources of information may not suit their style. These managers may need to restructure their information system to include information (in terms of types, characteristics and sources) that suits their users' style, thereby enhancing the usage of this information in decision-making.

Fourthly, information system designers need to pay attention to the role of personal variables such as age, education, and information system use in forming managers' information behaviour. Thus, these designers should not follow the principle of one size fits all; rather, they should build flexibility into their systems that allows for individualism in information use in terms of choosing the right information types, characteristics, and sources for each manager, thus leading to the full utilisation of information system benefits.

The study's findings also have several implications for research. First, through investigating the impact of six personal and professional variables, the study has contributed to enhancing the cumulative knowledge regarding the variables that imipact upon different information dimensions. However, there is still room for enhancing this knowledge further through investigating variables such as national culture and other contextual issues. Future researchers are urged to investigate the impact of such variables.

Secondly, the study investigated direct relationships between personal and professional variables and various information behaviour dimensions. There is, however, a high probability that there are internal interactions among the contextual variables on one hand and among the information behaviour dimensions on the other, as well as among all these internal interactions. Analysing these types of interactions would increase our knowledge of the indirect impact of variables on each other, on the information dimensions, and within the information dimensions. Future researchers are highly advised to investigate these types of interactions.

Thirdly, the study's model consists of two levels: personal and professional variables (independent variables) and information dimensions (dependent variables). Future researchers could extend this model to include a third level, which could be called final organizational impacts; these can include organizational and employees' productivity. The third level is located after the second one (information behaviour dimensions), and it measures the direct impact of the second level and indirect and direct impacts of the first level. Fourth and finally, to enhance the generalisation of this study's findings, future researchers are called to replicate this study in different contexts such as other countries, cultures, and types of organizations.

Finally, in a world that is predominantly characterised by a high degree of uncertainty and speedy decision-making, managers represent one of the true organizational assets. Furthermore, information behaviour of these managers is becoming a crucial element in organizational success because of the rich informational future environment. Future researchers are called to continue working on this issue due to its importance. In their work, they should be aware of two things. First, the differences in patterns of information behaviour are masking the impact of other variables, such as demographic and organizational variables, on this behaviour. Second, researchers should build relational models because these models will contribute to enriching our knowledge of the factors that enhance or hinder the efficiency and effectiveness of managers' information behaviour.

## <a name="author" id="author">About the author</a>

Helaiel Almutairi is an Associate Professor of Public Administration and Information Systems at Kuwait University. He holds a BSc from Kuwait University, an MPM from Carnegie Mellon University, and an MSIS and PhD from Pennsylvania State University. His current research interests include end-user computing, impacts of information systems, information systems and leadership, and e-government. He can be contacted at: [hma115@excite.com](mailto:hma115@excite.com)

#### References

*   Ajzen, I. (1991). The theory of planned behavior. _Organizational Behavior and Human Decision Processes,_ **50**(2), 179-211.
*   Almutairi, H. (2007). Information system and productivity in Kuwaiti public organizations: looking inside the black box. _International Journal of Public Administration_, **30**(11), 1263-1290.
*   Almutairi, H. (2008). Information system usage and national culture: evidence from the public service sector. _International Journal of Society Systems Science_, **1**(2), 151-175.
*   Almutairi, H. (2011). Sources and information use by Kuwaiti public managers: an empirical study. _International Journal of Management_ (forthcoming).
*   Ashill, N. J. & Jobber, D. (2001). Defining the information needs of senior marketing executives: an exploratory study. _Qualitative Market Study_, **4**(1), 52-61.
*   Ashill, N. & Jobber, D. (2009). Measuring state, effect, and response uncertainty: theoretical construct development and empirical validation. _Journal of Management_, **36**(5), 1278-1308.
*   Auster, E. & Choo, C. (1993). Environmental scanning by CEOs in two Canadian industries. _Journal of the American Society for Information Science,_ **44**(4), 194-203. 
*   Ballantyne, P. G. (1993). Managing scientific information in agricultural research. _Public Administration and Development_, **13**(3), 271-280.
*   Bandura, A. (2001). Social cognitive theory of mass communication. _Media Psychology,_ **3**(3), 266-299.
*   Barnard, J. (1991). The information environments of new managers. _Journal of Business Communication,_ **28**(4), 312-325.
*   Bonnici, J. & Fredenberger, W. (1995). Information priorities in turnaround situations. _Journal of Applied Business Research,_ **11**(3), 94-100.
*   Borgatti, S. P. & Cross, R. (2003). A relational view of information seeking and learning in social network. _Management Science,_ **49**(4), 432-445.
*   Breen, H. (2005). Assessing the information needs of Australian gaming managers. _UNLV Gaming Research & Review Journal_, **9**(2), 29-43.
*   Bystrom, K. (2006). Information activities in work tasks. In K. E. Fisher, S. Erdelez, & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 174-178). Medford, NJ: Information Today.
*   Case, D.O. (2002). _Looking for information: a survey of research on information seeking, needs, and behavior._ San Diego, CA: Academic Press.
*   Case, D.O. (2007). _Looking for information: a survey of research on information seeking, needs, and behavior_ (2nd ed.). London: Academic Press.
*   Church, K. & Smyth, B. (2009). Understanding the intent behind mobile information needs. In _IUI 09: Proceedings of the 14th International Conference on Intelligent User Interfaces: February 8-11, 2009, Sanibel Island, Florida, USA_ (pp. 247-256). New York: ACM Press.
*   Dearman, D., Kellar, M. & Trunog, K. (2008). An examination of daily information needs and sharing opportunities. In _CSCW 08:_ _Proceedings of the ACM 2008 Conference on Computer-Supported Cooperative Work: San Diego, CA, USA, November 8-12, 2008_ (pp. 679-688). New York: ACM Press.
*   Goodman, S. (1993). Information needs for management decision making. _ARAM Records Management Quarterly,_ **27**(4), 12-23.
*   Hansen, P. (2006). Work task'information-seeking and retrieval processes. In K. E. Fisher, S. Erdelez, & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 392-6). Medfort, NJ: Information Today.
*   Hofstede, G. (1984). _Culture's consequence_ (Abridged ed.) Beverly Hills, CA: Sage.
*   Hofstede, G. (1991). _Cultures and organizations: software for the mind._ London: McGraw-Hill.
*   Hupfer, M.E. & Detlor, B. (2006). Gender and Web information seeking: a self-concepts orientation model. _Journal of the American Society for Information Science and Technology,_ **57**(8), 1105-1115.
*   Ikoja-Odongo, R. (2001). A study of the information needs and uses of the informal sector in Uganda: preliminary findings. _Library and Information Science Research,_ **11**(1), 1-12.
*   Katz, D. & Kahn, R. L. (1966). _The social psychology of organizations._ New York, NY: John Wiley.
*   Kenkel, D. (1990). Consumer health information and the demands of medical care. _The Review of Economics and Statistics,_ **62**(4), 587-595.
*   Koksal, M. H. (2008). How expert marketing research affects company export performance. _Marketing Intelligence & Planning,_ **26**(4), 416-430.
*   Larner, A. J. (2006). Searching the Internet for medical information: frequency over time and by age and gender in outpatient population in the UK. _Journal of Telemedicine and Telecare,_ **12**(4), 339-355.
*   Lawrence, P. R. & Lorsch, J. W. (1967). _Organization and environment._ Boston, MA: Harvard University.
*   Lorence, D., & Park, H. (2007). Gender and online heath information: a partitioned technology assessment. _Heath Information and Libraries Journal,_ **24**(3), 204-209.
*   Losch, A. & Lambert, S. (2007). Information behavior in e-reverse auctions. _Journal of Enterprise Information Management,_ **20**(4), 447-464.
*   Maceviciute, E. (2006). [Information needs research in Russia and Lithonia: 1965-2003.](http://www.webcitation.org/5yHjeB3E4) _Information Research,_ **11**(3), paper 256\. Retrieved 13 May, 2012 from http://informationr.net/ir/11-3/paper256.html. (Archived by WebCite athttp://www.webcitation.org/5yHjeB3E4)
*   Mick, C., Lindsey, G. N. & Callahan, D. (1980). Toward usable user studies. _Journal of the American Society for Information Science_, **31**(5), 347-56.
*   Mintzberg, H. (1973). _The nature of managerial work._ New York, NY: Harper & Row.
*   Mintzberg, H. (1991). The manager's job: folklore and fact. In H. Mintzberg & J. Quinn, _The strategy process concepts, context, cases_ (2nd ed., pp. 21-31). Englewood Cliffs, NJ: Prentice-Hall.
*   Niedzwiedzka, B. M. (2003). Barriers to evidence-based decision making among Polish healthcare managers. _Health Services Management Research,_ **16**(2), 106-115.
*   Nunnally, J. (1967). _Psychometric theory._ New York, NY: McGraw-Hill.
*   O'Reilly, C. A. (1982). Variations in decision makers' use of information sources: the impact of quality and accessibility of information. _Academy of Management Journal,_ **25**(4), 756-771.
*   Paisley, W. J. (1968). Information needs and users. _Annual review of information science and technology_, **3**, 1-30.
*   Palmquist, R.A. (2006). Taylor's information use environments. In K. E. Fisher, S. Erdelez, & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 354-357). Medford, NJ: Information Today.
*   Palsdottir, A. (2003). Icelandic citizens' everyday life health information behavior. _Health Informatics Journal_, **9**(4), 225-40.
*   Pors, N.O. (2008). Traditional use pattern An analysis of high school students' use of libraries and information resources. _New Library World,_ **109**(9-10), 431-443.
*   Ramdeen, C. & Fried, B. (2003). The roles of casino controllers. _Gaming Research and Review Journal,_ **7**(2), 25-41.
*   Ramlogan, R. & Tedd, L.A. (2006). Use and non-use of electronic information sources by undergraduates at the University of West Indies. _Online Information Review,_ **30**(1), 24-42.
*   Rose, G. & Straub, D. W. (1998). Predicting general IT use: applying TAM to the Arabic world. _Journal of Global Information Management,_ **6**(3), 39-46.
*   Rutten, L. J. Squiers, L., & Hesse, B. (2006). Cancer-related information seeking: hints from the 2003 Heath Information National Trends Survey (HINTS). _Journal of Health Communication,_ **11**(1), 147-56.
*   Simon, H. (1960). _The new science of management decision._ New York, NY: Harper.
*   Steinerova, J. & Susol, J. (2007). [Users' information behavior: a gender perspective](http://www.webcitation.org/5yHjrFX95). _Information Research,_ **12**(3), paper 320\. Retrieved 13 May, 2011 from http://informationr.net/ir/12-3/paper320.html (Archived by WebCite at http://www.webcitation.org/5yHjrFX95).
*   Straub, D.W., Keil, M. & Brennan, W. (1997). Testing the technology acceptance model across cultures: a three country study. _Information and Management,_ **33**(1), 1-11.
*   Swanson, L. (2003). An information-processing model of maintenance management, _International Journal of Production Economics_, **83**(1), 45-64
*   Taylor, R. S. (1991). Information use environments. _Progress in communication sciences_, **10**, 217-255.
*   Thompson, J. D. (1967). _Organizations in action._ New York, NY: McGraw-Hill.
*   Trumbach, C. (2006). Addressing the information needs of technology managers: making derived information usable. _Technology Analysis & Strategic Management_, **18**(2), 221-243.
*   Urquhart, C., & Yeoman, A. (2009). Information behavior of women: theoretical perspectives on gender. _Journal of Documentation,_ **66**(1), 113-139.
*   Williams, D. & Coles, L. (2007). Evidence-based practice in teaching: an information perspective. _Journal of Documentation_, **63**(6), 812-35.
*   Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation,_ **37**(1), 3-15.
*   Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation,_ **55**(3), 249-70.
*   Wright, W. & Evans, C. (2009). The how-to series, 15: How to evaluate your information needs. _Manager: the British Journal of Administrative Management_, **66,** 14-15.

# Appendix: Items used in the questionnaire

## Section one: Personal and professional information.

*   age: under 30; 30 to 40; 41 or older
*   sex: male, female
*   education level: secondary education; university level; higher degree
*   managerial level: low, middle or high
*   level of information system use in work: weak, moderate, good, excellent
*   job experience: 5 years or less, 6-10 years, 11-15 years, 16 years or more

## Section two: information characteristics.

Please indicate how important the following information characteristics are to you:

<table align="center">

<tbody>

<tr>

<td>Subjectivity  
Timeliness  
Specificity  
Clarity  
Comparability</td>

<td width="25%"> </td>

<td>Trustworthiness  
Unbiased  
Usefulness  
Flexibility  
Truthfulness</td>

</tr>

</tbody>

</table>

## Section three: information types.

The following statements indicate the types of information you may need. Please indicate the degree of your agreement with each statement.

1.  I use information I receive periodically about the work accomplishments of employees who under my supervision.
2.  I use information about the attendance of employees who under my supervision.
3.  I use information about the work obstacles related to the work of employees who under my supervision.
4.  I use information I receive periodically about the work procedures and requirements.
5.  I use information about work plans that have been made.
6.  I use follow up information about the implemented work plans.
7.  I use evaluation information about the implemented work plans.
8.  I use information that helps in making work plans.
9.  I use information about the degree of customers' satisfaction.
10.  I use information that shows the needs of customers.
11.  I use information that shows the problems of customers.
12.  I use information about organizational accomplishments.
13.  I use information about organizational problems.
14.  I use information about the causes of organizational problems.
15.  I use information that helps in researching solutions for organizational problems.

## Section three: information sources.

The following are several sources of information. Please indicate whether or not you use these sources.

<table align="center">

<tbody>

<tr>

<td>current formal documents;  
archived formal documents;  
annual books and letters [Note: these are  
      formal organizational documents];  
reports and journals;  
formal publications;  
statistical publications;  
standard and specification publications;  
text and university publications;  
encyclopedia;  
booklet and periodical publications;  
guidebooks and atlases;  
audio sources;  
visual sources;  
audio-visual sources;  
</td>

<td width="20%"></td>

<td>CD-DVD;  
hard disk;  
comprehensive general sources;  
specialized sources;  
detail specialized sources;  
internal databases;  
external databases;  
regional network;  
international network;  
personal experience;  
family and friends;  
colleges inside the work unit;  
colleges outside the work unit;  
colleges outside the organizations.</td>

</tr>

</tbody>

</table>