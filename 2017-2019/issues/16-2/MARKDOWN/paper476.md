#### vol. 16 no. 2, June, 2011

# The search queries that took Australian Internet users to Wikipedia

#### [Vivienne Waller](mailto:vwaller@swin.edu.au)  
Institute for Social Research, Swinburne University of Technology, Mail H53, PO Box 218, Hawthorn, Victoria 3122, Australia

#### Abstract

> **Introduction.** This exploratory study analyses the content of the search queries that led Australian Internet users from a search engine to a Wikipedia entry.  
> **Method.** The study used transaction logs from Hitwise that matched search queries with data on the lifestyle of the searcher. A total sample of 1760 search terms, stratified by search term frequency and lifestyle, was drawn.  
> **Analysis.** Each search term was coded to indicate the subject of the query and weighted according to its position in the long tail distribution. Quantitative analysis was carried out using the statistical package SPSS.  
> **Results.** The results of the study suggest that Wikipedia is used more for lighter topics than for those of a more academic or serious nature. Significant differences among the various lifestyle segments were observed in the use of Wikipedia for queries on popular culture, cultural practice and science.  
> **Conclusions.** The analysis provides some analytical purchase on the complex nature of information search and the difficulties inherent in assuming a valid distinction between information search and entertainment. It is suggested that the term _leisure search_ be used to identify information search that is in itself a leisure activity and not a search for particular information.

## Introduction

Launched in 2001, _Wikipedia_ describes itself as The Free Encyclopedia. Articles are written by an ongoing collaborative process and anyone can contribute to almost any article. As of April 2011, _Wikipedia_ contained more than 3.6 million articles in English and more than 100,000 articles in each of 36 languages. (Source: _Wikipedia_ home page at [www.Wikipedia.org](http://www.Wikipedia.org), accessed 22 April. 2010). According to Spinellis and Louridas ([](#spi08)2008) _Wikipedia_'s rate of growth is sustainable, meaning that _Wikipedia_ is viable in the longer term as an online encyclopaedia.

## Comparing _Wikipedia_ with traditional encylopaedias

Much research has compared _Wikipedia_ with traditional encyclopaedias in terms of the quality of articles. There has also been a small amount of research on the coverage of topics where it has been found that _Wikipedia_ has generally good coverage of the sciences, excluding medicine. The coverage of social science topics, especially law, appears to be less extensive ([Halavais and Lackaff 2008](#hal08)). As may be expected, given the instant nature of publishing on _Wikipedia_, it also has greater coverage of more recent topics than do traditional encylopedias ([Royal and Kapila 2009](#roy09)), especially popular music and recent fiction ([Halavais and Lackaff 2008](#hal08)).

One of the basic tenets of _Wikipedia_ is that all articles should present a neutral point of view. In theory, this, and the fact that anybody can edit a _Wikipedia_ entry, should allow for alternative points of view to find expression. Hansen _et al._ idealise the _Wikipedian_ model as '_approximat(ing) the conditions for a Habermasian rational discourse_'' ([2009: 53](#han09)), unshackling knowledge from traditional power relations. It appears, however, that the reality of _Wikipedia_'s entries is not so emancipatory. An empirical study by Elvebakk ([2008](#elv08)) comparing _Wikipedia_ entries on twentieth century philosophers with entries from online encyclopaedias written by academics concluded that _Wikipedia_ does not generally present an alternative point of view to dominant Western thinking and values.

There is no consensus on the quality of articles in _Wikipedia_. Some commentators have made deductions about the quality of articles on the basis of the nature of the _Wikipedian_ model of collaborative authorship. For example, the fact that anybody can contribute to an article and no one has overall editorial responsibility has led some to doubt the accuracy of articles ([Gorman 2007](#gor07); [Keen 2007](#kee07)) or their coherence ([Duguid 2006](#dug06)). Others draw the opposite conclusion, arguing that it is the hierarchy of user levels combined with the system of user-created policies and guidelines that ensures the quality of _Wikipedia_ articles ([Lipczynska 2005](#lip05); [McGrady 2009](#mcg09)).

Empirical analyses of the content of actual _Wikipedia_ articles have also yielded conflicting results. A study in Nature ([Giles 2005](#gil05); [Giles 2006](#gil06)) found that a sample of _Wikipedia_'s science articles were as accurate as related entries in the _Encyclopaedia Britanica_. Chesney ([2006](#che06)) arranged for 258 academics to each read two _Wikipedia_ articles, one within and one outside the academic's area of expertise. Chesney found that the accuracy of information in the _Wikipedia_ articles was high but that a given article was more likely to be rated as credible by experts than by non-experts. Ehmann _et al._ ([2008](#ehm08)) found that the quality of articles varied across disciplines.

Some commentators have suggested that the quality of _Wikipedia_ articles could be improved by involving academics and librarians as active contributors to _Wikipedia_ ([Badke 2008](#bad08); [Pressley and McCallum 2008](#pre08)) while Lally ([2007](#lal07)) suggests that _Wikipedia_ should be used to extend digital collections.

## Need for research on how _Wikipedia_ is actually used

The relevance of research on the quality and coverage of _Wikipedia_ and of debates concerning the role of academics and librarians hinges on how _Wikipedia_ is actually being used. This is an area in which there is a paucity of research even though the popularity of _Wikipedia_ is well established. In America, just over one in three adults use _Wikipedia_, with almost one in ten (8%) Internet users using _Wikipedia_ on any day ([Rainie and Tancer 2007](#rai07)). In Australia, in April 2009, _Wikipedia_ was the twelfth most visited site on the Internet according to unpublished data from Hitwise Australia. In America, the likelihood of use increases with income and decreases with age, with those who have a university degree most likely to use _Wikipedia_ ([Rainie and Tancer 2007](#rai07)). In a US study by Lim ([2009](#lim09)), one third of university students reported that they use _Wikipedia_ for academic purposes, generally for finding background information.

While it is established that _Wikipedia_ is well used, there is less research on what it is used for, with most existing research based on surveys or interviews (for example, [Luyt _et al._ 2008](#luy08); [Lim 2009](#lim09)). However, for a variety of reasons (such as poor recall, or wanting to impress the interviewer) how people say they use _Wikipedia_ may not accurately reflect what they actually do.

In addition, most empirical analyses of _Wikipedia_ articles or their use focus on academic content or context. For example, Lim ([2009](#lim09)), Eisenberg ([2009](#eis09)) and Luyt _et al._ ([2008](#luy08)) all surveyed students. The debate in _Nature_ ([Giles 2005](#gil05); [Giles 2006](#gil06)) and the popular press about whether _Encyclopaedia Britanica_ or _Wikipedia_ is more accurate (e.g. [_Wikipedia_ study fatally flawed 2006](#wik06)) has focussed on scientific articles. The studies by Ehmann _et al._ ([2008](#ehm08)), Chesney ([2006](#che06)) and Elvebakk ([2008](#elv08)) have all focussed on articles on traditional academic subjects. An analysis of the 100 most visited _Wikipedia_ pages, however, found that more than half of these pages were related to sexuality and entertainment, particularly media celebrities and TV shows ([Spoerri 2007](#spo07)). This finding points to the need to analyse more closely how a range of people, rather than just students, are actually using _Wikipedia_. Hence, this study addresses the following research questions:

*   What type of information is actually being accessed on _Wikipedia_?
*   Does use of _Wikipedia_ vary across different segments of the online population?

This paper is based on an analysis of the content of the search queries that led Internet users from a search engine to a _Wikipedia_ entry. It is located at the intersection of research into _Wikipedia_ use and research into Web search. It follows in the tradition of the transaction log analysis ([Jansen 2006](#jan06)), an established method in Web search studies. However, it differs from existing transaction log analyses (for example, [Spink _et al_. 2001](#spi01), [Jansen and Spink 2006](#janspi06), [Park 2009](#par09)) in two important respects. Firstly, the sample has been selected and weighted to take account of the distribution of search queries. Secondly, data on the lifestyle of the searcher is attached to each query. While there have been previous studies that relate Web search to individual characteristics (for example, [Ford _et al_. 2008](#for08)), such studies been limited in sample size as they rely on interviews. In addition, the searches have been conducted in a controlled, and hence artificial, environment. The analysis reported in this paper is therefore distinctive because data on the lifestyle of the searcher have been attached to transaction logs.

Web search studies have been informed by models of information-seeking behaviour that assume that an information search is conducted in response to an information need ([Spink and Jansen 2004](#spi04), [Knight and Spink 2008](#kni08)). However, Broder ([2002](#bro02)) and Rose and Levinson ([2004](#ros04)) have each demonstrated that a search engine user may not necessarily be searching for information. Broder ([2002](#bro02)) introduced a taxonomy of Web search that added navigational searches and transactional searches to informational search. Navigational searches are those searches undertaken in order to reach a particular website that the user has in mind, while transactional searches are those searches undertaken in order to be able to complete a transaction, such as banking, shopping, or downloading files. Rose and Levinson ([2004](#ros04)) preferred to replace Broder's category of transactional search with the broader category of resource search, where the user's goal is to access an online resource other than information.

Before undertaking this study, it was assumed that the goal of users accessing _Wikipedia_ was to locate information on a particular topic. Because _Wikipedia_ is described as an encyclopaedia, this seemed a reasonable assumption. However, as discussed in the conclusion, the findings indicate that searches that lead to _Wikipedia_ may not be searches for information.

It has been noted that, in order to improve search engine design, it is necessary to understand a user's intention or goal when conducting a Web search ([Jansen _et al._ 2008](#jan08), [Rose and Levinson 2004](#ros04)). Similarly, it could be argued that understanding more about who is accessing what type of information on _Wikipedia_ has implications for the design of _Wikipedia_ pages and their accessibility through search engines.

For a user's search to appear in the sample, all of the following four conditions shown in Figure 1 needed to have been satisfied.

<div align="center">![Figure 1: Arriving at Wikipedia via a search engine](p476fig1.png)</div>

<div align="center">  
**Figure 1: Arriving at _Wikipedia_ through a search engine**</div>

The distribution of subjects looked up in _Wikipedia_ is not an indication of subjects looked up on the Internet because the extent to which each of these four conditions is satisfied may vary across subjects. For some subjects, the user may be more likely to go directly to a Web page, or the relevant _Wikipedia_ entries may be less likely to appear high in the results or the user may be less likely to choose _Wikipedia_ from the search results. So, for example, while studies of Internet search queries found that more than 10% of queries related to computers or the Internet ([Jansen and Spink 2006](#jan06)), only 5% of queries that led to _Wikipedia_ were about computers or the Internet.

The following discussion summarises existing research in the United States, United Kingdom and Australia relevant to the likelihood that each of these conditions is satisfied.

### Condition 1: Use of Internet to find information

It is evident that the Internet has become a major source of information, particularly because of its convenience ([De Rosa _et al._ 2005](#der05); [Horrigan 2006](#hor06)). A study taken by the Pew Internet and American Life Project in 2007 ([Wells 2008](#wel08)) found that almost 60% of respondents would consult the Internet when they had problems they needed to address. In Britain, two-thirds of people turn to the Internet first when looking for information relating to a professional, school or personal project ([Dutton _et al._ 2009](#dut09)). In Australia, where approximately 80% of the population aged over 15 use the Internet ([Ewing _et al._ 2008](#ewi08)), just over two-thirds (68.5%) of surveyed Internet users described the Internet as an 'important' or 'very important' source of information ([Ewing _et al._ 2008](#ewi08)).

There is not a great deal of data on what types of information the Internet is used for. The British Oxford Internet Survey asks people whether they use the Internet for getting information about local events, news, health, sports, jobs, humorous content or making travel plans. Two-thirds of Internet users in Britain ([Dutton and Helsper 2007](#dut07); [Dutton _et al._ 2009](#dut09)), and approximately 80% of Internet users in the US ([Fox and Jones 2009](#fox09)) look for health information online. In 2006, in the US, more than half (54%) had used the Internet to find out news or information about science ([Horrigan 2006](#hor06)).

### Condition 2: Use of a search engine

Little is known about the likelihood that a user uses a search engine rather than directly to a Web page for a particular subject. However, there is evidence of high overall use of search engines. In the US, PEW found that in 2008 almost half of all Internet users used search engines on a typical day ([Fallows 2008](#fal08)). In Britain, almost two thirds (64%) of Internet users mainly use search engines when they are looking for information online ([Dutton _et al._ 2009](#dut09)). In Australia, one in eight Website visits in the four weeks ending April 2009 was to a search engine (Source: Unpublished data from Hitwise Australia).

### Condition 3: _Wikipedia_ entry appears in the search results

The requirement that the relevant _Wikipedia_ entry should appear in the search results can be restated as the requirement that the relevant _Wikipedia_ entry should appear _high_ in the search results. This is because more than two-thirds (68%) of users do not go past the first page of results and 92% do not go past the first three pages of results ([iProspect 2008](#ipr08)). It has been shown, however, that _Wikipedia_ pages are often very highly ranked in search engine results, partly because of the dense structure of links in _Wikipedia_ articles ([Rainie and Tancer 2007](#rai07); [Spoerri 2007](#spo07); [Tann and Sanderson 2009](#tan09)).

### Condition 4: User clicks on link to _Wikipedia_

Little is known about when a user will click on a link to _Wikipedia_ and when they choose a different type of site. However, research by Tann and Sanderson ([2009](#tan09)) suggests that a search engine is sometimes used as a shortcut to the relevant _Wikipedia_ entry. In their study of university students, they found that more students expected to end up using _Wikipedia_ than went straight to _Wikipedia_. Other research provides evidence to suggest that the likelihood of trusting _Wikipedia_ as a credible source depends on the user's knowledge of the topic. Lim ([2009](#lim09)) found that users who are new to a topic are likely to underestimate the quality of the relevant _Wikipedia_ article. This may lead to reluctance to use _Wikipedia_ for more _important_ topics.

The situation is quite different regarding variation in _Wikipedia_ use across lifestyle groups. For a particular subject, this variation may reflect differences in the propensity to use the Internet for information, to use a search engine rather than go directly to a Web page or to choose _Wikipedia_ from the search results. Condition 3, whether or not a link to _Wikipedia_ appears in the search results is, of course, independent of lifestyle group.

## Methods

_Wikipedia_ use can be measured in terms of the number of visits to particular pages or in the context of people's information searches. The number of visits to particular pages is of interest but this measure may be artificially inflated by the repeat visits of zealous contributors.

In the context of information searches, the most common way that users arrive at _Wikipedia_ is through clicking on search engine results. In April 2009, two thirds (66%) of visitors (in Australia) to _Wikipedia_ came directly from a search engine (Source: Unpublished data from Hitwise Australia) and almost all (93%) of these came from Google. This paper analyses the search queries that took people to _Wikipedia_ in the four weeks ending 25 April 2009, using data provided by web analytics company, Hitwise Australia.

Hitwise measures Internet use by collecting data directly from Internet service providers. It collects the log files of proxy cache servers covering more than one third of Australian Internet subscriptions, including homes, businesses, schools, universities, and libraries.

The advantage of using transaction logs for investigating the sort of the information people access on _Wikipedia_ is that transaction logs enable the study of a large sample of users, are unobtrusive, and do not affect user behaviour. Because transaction logs record what people actually do, they overcome the limitations associated with relying on what people say they do. The main limitation for a study like this is that the topic in which the user was interested can only be imputed by the researcher on the basis of the search query. A more certain method would be to observe users conducting searches and interview them at this time. In addition, the data used in this study does not provide information on those cases where _Wikipedia_ appeared near the top of the search results but was not visited or where Internet users went directly to a Web page for information rather than by a search engine.

This study is quite different from Spoerri's ([2007](#spo07)) study of the 100 most visited _Wikipedia_ pages in that it includes the long tail in its analysis. The term _long tail_ was brought into popular usage by Chris Anderson ([2006](#and06)). It is used to refer to a Zipf distribution whereby a few items account for a sizable proportion of the total and an enormous number of items (the long tail) each contribute a tiny proportion to the rest. For example, the top one hundred search terms that brought people to _Wikipedia_ in April 2009 accounted for just 4% of visits through search engines; hundreds of thousands of search terms accounted for the other 96% of visits with most only accounting for one or two visits each. Analysing the long tail is laborious as it requires manual coding of large numbers of search terms. It is important here to make a distinction between search terms and search queries. A _search query_ is an instance of a user typing a query into a search engine. A _search term_ is what is typed into the search engine. It is estimated that more than 600,000 search queries in the Hitwise sample took a user to _Wikipedia_ and that at least 400,000 of these queries used search terms that appeared only once. The sample analysed in this study was selected from an extract of 50,000 different search terms that took a user to _Wikipedia_ in April 2009\. The distribution of the number of queries associated with each search term was mapped and divided into seven groups based on known and estimated search term frequency, as shown in Figure 2.

<div align="center">![p476fig2: Long tail distribution of search queries to Wikipedia](p476fig2.png)</div>

<div align="center">  
**Figure 2: Long tail distribution of search queries to _Wikipedia_**</div>

As well as including the long tail in its analysis, another unique feature of this study is that it matches search query data with data on the lifestyle of the searcher. It does this by taking advantage of the Hitwise proprietary method to match Internet use with Mosaic lifestyle profiles. Mosaic Australia has divided the Australian population into eleven lifestyle groups on the basis of a range of lifestyle information including income, education, employment, housing, consumer habits and preferences, entertainment, media use, and attitudes to government. This information is sourced from more than 200 variables derived from a range of data sources. The eleven lifestyle groups are listed in Table 1 alongside the relevant _tagline_ which summaries the salient features. (See [http://www.mosaicaustralia.com.au/](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.mosaicaustralia.com.au%2F&date=2011-04-24) for more information on the method and the lifestyle profiles).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Lifestyle groups and their tagline**</caption>

<tbody>

<tr>

<th width="42%" valign="middle">Lifestyle Group</th>

<th width="58%">Tagline</th>

</tr>

<tr>

<td>A Privileged Prosperity</td>

<td>The most affluent families in the most desirable locations</td>

</tr>

<tr>

<td>B Academic Achievers</td>

<td>Wealthy areas of educated professional households</td>

</tr>

<tr>

<td>C Young Ambition</td>

<td>Educated and high-earning young singles and sharers in the inner suburbs</td>

</tr>

<tr>

<td>D Pushing the Boundaries</td>

<td>Young families living in recent developments on the fringes of major cities</td>

</tr>

<tr>

<td>E Family Challenge</td>

<td>Mixed family forms with stretched budgets in outer suburbs</td>

</tr>

<tr>

<td>F Metro Multiculture</td>

<td>Medium to high density areas with much cultural diversity</td>

</tr>

<tr>

<td>G Learners and Earners</td>

<td>Students and professionals living in high density, lower cost suburbs</td>

</tr>

<tr>

<td>H Provincial Optimism</td>

<td>Anglo-Australian blue-collar families in provincial settlements</td>

</tr>

<tr>

<td>I Farming Stock</td>

<td>Rural landowners and workers in agricultural heartlands</td>

</tr>

<tr>

<td>J Suburban Subsistence</td>

<td>Low income, low-spending households in major regional and outer metro</td>

</tr>

<tr>

<td>K Community Disconnect</td>

<td>Older blue-collar workers and retirees in country and coastal locations</td>

</tr>

</tbody>

</table>

These lifestyle groups have been derived for market research rather than academic research. However, this does not detract from the usefulness of the categories for a study like this which can be understood as an analysis of the consumption of the information product _Wikipedia_. For each lifestyle group, a random sample of 160 search terms, stratified by search query frequency, was selected without replacement. In this way, a total sample of 1,760 search terms was drawn. The sample was divided equally across the lifestyle groups to ensure the same precision for each lifestyle group.

The analysis weighted queries by search term frequency. To avoid the dominance of those search terms that were used most frequently, search queries were also weighted according to their position in the long tail distribution. For example, queries in the sample that used search terms that appeared more than 500 times were weighted to contribute a total of 1.7% of visits in the sample (see Figure 2). Reporting on the total frequencies for each subject required an additional weighting taking into account the representation of each lifestyle group in the online Australian population.

Each of the search terms in the final sample was examined and coded to indicate the subject of the search term. Each search term was looked up in Google to enable inspection of the relevant _Wikipedia_ entry in the search results. Those terms which related to an Australian subject were also separately identified.

The starting point for the coding in this study was that developed in an analysis of the subject of library catalogue queries ([Waller 2009](#wal09)). The Library of Congress classification was used by Halavais and Lackaff ([2008](#hal08)) in their analysis of the topical coverage of _Wikipedia_. However, as Park ([2009](#par09)) explains, traditional classification systems such as this are not well suited for classifying Web queries. The categories introduced by _Wikipedia_ in 2004 were used by Holloway _et al_. ([2007](#hol07)) in their analysis of the topical coverage of _Wikipedia_. However, these categories were not suitable for this research either as they are not mutually exclusive and not sufficiently fine-grained. For example the top level category Culture and the Arts includes topics as diverse as sport, tourism, music, philosophy, TV, and food. ( [Portal: contents/categories n.d.](#por)).

Using subject codes similar to those of Waller ([2009](#wal09)) will allow the subject of the library catalogue queries to be compared with the subject of _Wikipedia_ queries and the subject of search engine queries in a subsequent analysis. Drawing from the grounded theory technique of open coding ([Strauss and Corbin 1998](#str98)) the coding scheme in Waller ([2009](#wal09)) was generated from close examination of the data and the creation of codes that most closely described the content of the search queries. For this study of _Wikipedia_ use, care was taken not to force the search terms into categories that did not reflect the substance of the term and additional codes were created as required. For example, the code _pornography_ was not needed for the analysis of library catalogue queries, but was an appropriate code for some of the _Wikipedia_ queries. The resulting fifty-two codes closely described the content of the search queries and these were amalgamated into the following twelve broad subject groupings.

*   Popular culture (popular music, TV show, actor, movie, video game, celebrity)
*   Cultural practice (sport, religion, cultural practice not elsewhere classified)
*   Computing/Web
*   Health
*   History
*   Science
*   Place/building
*   Contemporary issues
*   Book/author
*   High culture
*   Other
*   Unknown

In most cases the classification was straightforward and it was possible to code 98% of queries with a primary code. Where the meaning of the term was unknown or ambiguous, the code _unknown_ was assigned. The presentation of the findings includes descriptions of each category to enable application of the coding scheme to other data.

There are a number of caveats. The coding inevitably involves some subjective decisions about the appropriate category in which to place a particular Website or term. Although the contribution of each Website or search term to the whole is small in the long tail, there is a degree of imprecision to the classifications. In interpreting the data, the focus should be on the overall pattern, rather than on the precise size of each category. In recognition of the imprecision, the percentage size of the categories is reported to the nearest integer.

Although the Internet service providers that supply data to Hitwise are a representative cross-section of such providers, there may be some sample bias, the direction of which is impossible to detect.

The segmentation of the Australian population is according to household rather than individual user. Some household types only include particular age groups within households, for example, retirees, or people in their twenties and thirties. Within households with families, it is not possible to distinguish the children's use of _Wikipedia_ from that of the adults.

It should also be borne in mind that Hitwise measures _visits_ not _visitors_ and so the data set may include a number of queries from the one individual. This is, however, unlikely, given the large size of the set of queries from which the sample was drawn.

## Findings

### Who were the visitors?

<div align="center">![Figure 3 - lifestyle profile of visitors to Wikipedia, compared with their representation in the Australian population](p476fig3.png)</div>

<div align="center">  
**Figure 3 : Lifestyle profile of visitors to _Wikipedia_, compared with their representation in the Australian population. (Data source: Hitwise)**</div>

Figure 3 compares the lifestyle profile of visitors to _Wikipedia_ with the lifestyle profile of Australian households and the online Australian population in the research data. It can be seen that lifestyle group G (Students and professionals living in high density lower cost suburbs) are the smallest group, accounting for 4.8% of the Australian population and 4.3% of the online Australian population. Lifestyle groups E (medium to high density areas with much cultural diversity) and K (Mixed family forms with stretched budgets in outer suburbs) are the largest. These each account for 14.3% and 14.0% respectively of the online population. Comparing the representation of any particular lifestyle group in the Australian online population with their representation in visits to _Wikipedia_, it can be seen that the distribution of visits to _Wikipedia_ approximates the actual distribution of the online population: the largest difference is less than two percentage points. However, the data indicates that amongst visitors to _Wikipedia_ there was a slight over-representation of people who were better-off and had higher educational attainment and a slight under-representation of people who were socially or economically disadvantaged and who lived in rural or suburban fringe areas.

### What were they searching for?

<div align="center">![Figure 4: Distribution of topics that took search engine users to Wikipedia](p476fig4.png)</div>

<div align="center">**Figure 4: Distribution of topics that took search engine users to _Wikipedia_**</div>

Search queries that were obviously intended to lead to the _Wikipedia_ site (_Wikipedia_, _wiki_, _Wikipedia encyclopaedia_, _site:en.Wikipedia.org_, and _www.Wikipedia.org_) were regarded as navigational queries ([Broder 2002](#bro02)). Less than 2% of all queries were navigational. All other queries about a particular topic were regarded as informational queries, even if they also contained the word _wiki_.

A distinction was made in the coding between High Culture and Popular Culture. Drawing from Gans ([1999](#gan99)), the code High Culture was used for queries relating to fine art and classical music while Popular Culture was used for queries relating to popular music, TV shows, actors, movies, video games or celebrities. While less than 2% of queries that took users to _Wikipedia_ related to high culture, more than one third (36%) related to popular culture. As Figure 4 shows, popular culture queries most commonly related to popular music. Almost all of the queries tagged as popular culture were for names: the names of TV shows, actors, musicians, songs, movies or video games.

Only 15% of queries related to Cultural Practice. As can be seen from Figure 3, 5% were queries about sport (for example, _david beckham_, _figure skating_), 1% were about religion (for example, _buddhism_, _luke the evangelist_), and 9% were about cultural practices not elsewhere classified. This last group comprised queries about aspects of everyday life, including product names, foods, holidays and ceremonies (for example, _toyota corolla_, _jager bomb_, _green tea ritual_, _near death experiences_, _anzac day_) as well as words or expressions for which it seemed that the searcher was seeking a definition or some background. Examples of this latter group include the words _oxymoron_ and _dessert_. At the time of writing, the relevant _Wikipedia_ page is the first Google search result for either of these two queries.

Queries about Science included queries about the biological sciences (for example, _European honey bee_, _spectacled eider,_ _biggest crocodile in the world_) the physical sciences (for example, _chlorine dioxide_, _black holes_, _transitional metals_) and mathematics (for example, _fibonacci sequence_, _pi_, _radial basis functions_). Only 7% of queries were tagged as science.

Queries tagged as Health included queries relating to mental health (for example, _psychosis symptoms_, _depression_), sexual health (for example, _herpes_, _urethritis_), and psychology (for example, _five stages of grief_, _nlp_, _resilience_), drugs (for example, _heroin,_ _temazepam overdose_) as well as other health queries (for example, _rotavirus vaccine_, _peppermint tea benefits_. Only 7% of queries related to health.

There were 6% of queries tagged as History. These were queries about historical events or periods (for example, _mccarthyism_, _reasons for australia's colonisation_), historical figures (for example, _ned kelly,_ _st Catherine of siena_) and artefacts (for example _ancient Egyptian tomb_). One third of these queries related to military history (for example, _vietnam war_, _battle of vimy ridge_).

Contemporary issues, Computing and the Web and Book/author each accounted for 5% of queries. Contemporary issues was used for queries about current affairs (for example, _fannie mae and freddie mac collapse_, _description of israeli and palestinian conflict_), the environment (for example, _effects of co2 emission_, _world oil usage per year_), organisations (for example, _dyno nobel portal_), the law (for example, _testimony_, _define:liquidated damages_), political figures (for example, _nelson mandela_), and political or sociological concepts such as _diaspora_ and _pragmatics_.

Computing or the Web included queries on computing software and hardware (for example, _what is a cvbs input_), gadgets (for example, _qkphone 911_ and _sony walkman mp3 flash player e-series)_, Websites such as _youtube_, _google_ and _last fm_, and Web tools (for example, _what is irc or irc bots_).

Book/Author queries were the names of works of fiction, comics, or writers, (for example, _dave eggers_, _taming of the shrew_, _bleach manga_) as well as other book-related queries (for example, _poetry prizes_).

The final 2% of queries tagged as Other are those topics too small to be reported on separately. These were all either queries about pornography, explicit sex, genealogy or business.

### What was each lifestyle group looking for?

There were some distinct differences in what different segments of the online population were looking for on _Wikipedia_ as Table 2 shows. The differences are significant (p<0.001) for queries relating to Popular culture, Cultural practice and Science.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Comparison of search topics for the various lifestyle segments**</caption>

<tbody>

<tr>

<th rowspan="2">Subject</th>

<th colspan="12" valign="middle">Lifestyle segment</th>

</tr>

<tr>

<th>A%</th>

<th>B%</th>

<th>C%</th>

<th>D%</th>

<th>E%</th>

<th>F%</th>

<th>G%</th>

<th>H%</th>

<th>I%</th>

<th>J%</th>

<th>K%</th>

<th>Total</th>

</tr>

<tr bgcolor="#C4F856">

<td>Popular culture</td>

<td align="center">42.4</td>

<td align="center">37.7</td>

<td align="center">42.9</td>

<td align="center">36.8</td>

<td align="center">28.7</td>

<td align="center">54.7</td>

<td align="center">51.8</td>

<td align="center">29.2</td>

<td align="center">29.8</td>

<td align="center">31.8</td>

<td align="center">23.7</td>

<td align="center">35.9</td>

</tr>

<tr bgcolor="#C4F856">

<td>Cultural practice</td>

<td align="center">12.5</td>

<td align="center">17.4</td>

<td align="center">14.1</td>

<td align="center">17.7</td>

<td align="center">16.5</td>

<td align="center">5.4</td>

<td align="center">10.0</td>

<td align="center">18.8</td>

<td align="center">12.3</td>

<td align="center">21.8</td>

<td align="center">20.3</td>

<td align="center">15.4</td>

</tr>

<tr>

<td>Health</td>

<td align="center">7.1</td>

<td align="center">3.4</td>

<td align="center">7.0</td>

<td align="center">5.7</td>

<td align="center">9.0</td>

<td align="center">7.6</td>

<td align="center">5.8</td>

<td align="center">11.2</td>

<td align="center">7.6</td>

<td align="center">7.0</td>

<td align="center">5.5</td>

<td align="center">7.2</td>

</tr>

<tr bgcolor="#C4F856">

<td>Science</td>

<td align="center">3.1</td>

<td align="center">13.0</td>

<td align="center">6.6</td>

<td align="center">6.4</td>

<td align="center">13.7</td>

<td align="center">1.9</td>

<td align="center">2.6</td>

<td align="center">6.4</td>

<td align="center">7.0</td>

<td align="center">7.2</td>

<td align="center">7.5</td>

<td align="center">7.1</td>

</tr>

<tr>

<td>History</td>

<td align="center">6.6</td>

<td align="center">3.2</td>

<td align="center">3.7</td>

<td align="center">8.3</td>

<td align="center">6.2</td>

<td align="center">9.2</td>

<td align="center">6.7</td>

<td align="center">4.7</td>

<td align="center">6.8</td>

<td align="center">3.4</td>

<td align="center">5.7</td>

<td align="center">5.9</td>

</tr>

<tr>

<td>Computing/Web</td>

<td align="center">6.3</td>

<td align="center">4.1</td>

<td align="center">2.1</td>

<td align="center">6.6</td>

<td align="center">2.6</td>

<td align="center">6.5</td>

<td align="center">6.2</td>

<td align="center">3.3</td>

<td align="center">5.8</td>

<td align="center">8.2</td>

<td align="center">6.1</td>

<td align="center">5.4</td>

</tr>

<tr>

<td>Place/building</td>

<td align="center">6.0</td>

<td align="center">5.6</td>

<td align="center">5.3</td>

<td align="center">1.1</td>

<td align="center">6.2</td>

<td align="center">2.7</td>

<td align="center">4.9</td>

<td align="center">5.5</td>

<td align="center">9.1</td>

<td align="center">5.0</td>

<td align="center">6.4</td>

<td align="center">5.4</td>

</tr>

<tr>

<td>Contemporary issues</td>

<td align="center">3.2</td>

<td align="center">6.2</td>

<td align="center">5.5</td>

<td align="center">4.7</td>

<td align="center">3.8</td>

<td align="center">2.9</td>

<td align="center">3.9</td>

<td align="center">8.2</td>

<td align="center">3.8</td>

<td align="center">4.5</td>

<td align="center">10.9</td>

<td align="center">5.3</td>

</tr>

<tr>

<td>Book/author</td>

<td align="center">7.2</td>

<td align="center">4.8</td>

<td align="center">4.1</td>

<td align="center">4.4</td>

<td align="center">9.0</td>

<td align="center">3.2</td>

<td align="center">3.8</td>

<td align="center">4.1</td>

<td align="center">4.9</td>

<td align="center">6.7</td>

<td align="center">1.1</td>

<td align="center">5.0</td>

</tr>

<tr>

<td>_Wikipedia_</td>

<td align="center">3.0</td>

<td align="center">1.6</td>

<td align="center">1.7</td>

<td align="center">2.2</td>

<td align="center">1.7</td>

<td align="center">4.7</td>

<td align="center">2.4</td>

<td align="center">2.3</td>

<td align="center">1.7</td>

<td align="center">1.6</td>

<td align="center">1.4</td>

<td align="center">2.2</td>

</tr>

<tr>

<td>High culture</td>

<td align="center">1.5</td>

<td align="center">1.2</td>

<td align="center">3.0</td>

<td align="center">-</td>

<td align="center">2.1</td>

<td align="center">0.6</td>

<td align="center">0.7</td>

<td align="center">0.6</td>

<td align="center">4.2</td>

<td align="center">0.6</td>

<td align="center">4.2</td>

<td align="center">1.8</td>

</tr>

<tr>

<td>Other</td>

<td align="center">0.6</td>

<td align="center">1.1</td>

<td align="center">-</td>

<td align="center">3.0</td>

<td align="center">0.6</td>

<td align="center">0.6</td>

<td align="center">1.3</td>

<td align="center">3.7</td>

<td align="center">2.5</td>

<td align="center">1.6</td>

<td align="center">4.1</td>

<td align="center">1.8</td>

</tr>

<tr>

<td>Unknown</td>

<td align="center">0.7</td>

<td align="center">0.6</td>

<td align="center">3.9</td>

<td align="center">3.1</td>

<td align="center">-</td>

<td align="center">-</td>

<td align="center">-</td>

<td align="center">2.0</td>

<td align="center">4.4</td>

<td align="center">0.6</td>

<td align="center">3.1</td>

<td align="center">1.6</td>

</tr>

<tr>

<td>Total</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

<td align="center">100.0</td>

</tr>

</tbody>

</table>

Those most likely to go to _Wikipedia_ for popular culture information were those from group F, _medium to high density areas with much cultural diversity_ and those from group G, _students and professionals living in high density, lower cost suburbs_. Those least likely to go to _Wikipedia_ for pop culture queries were those from group K _older blue-collar workers and retirees in country and coastal locations_. The presence or absence of children or young people does not entirely explain the differences across the various lifestyle segments. For example lifestyle segment E (_mixed family forms with stretched budgets in the outer suburbs_) had a relatively small proportion of visits related to popular culture.

In general, those with higher income and living in high-density or wealthy areas were more likely to arrive at _Wikipedia_ through pop culture queries than predominantly lower income Anglo-Australians living in the suburban fringe or country towns.

Those from older and lower income households in outer metropolitan, regional and country locations were the most likely to arrive at _Wikipedia_ via cultural practice queries. These did not tend to be queries about sport but were more general queries about aspects of cultural practice including definitions or background on words or expressions. Those living in high-density areas with high cultural diversity were the least likely to arrive at _Wikipedia_ through cultural practice queries.

Those most likely to arrive at _Wikipedia_ through science queries were those from group B, _wealthy areas of educated professional households_ and those from group E, _mixed family forms with stretched budgets in outer suburbs_.

<div align="center">![Figure 5: Proportion of queries about Australian topics by subject](p476fig5.png)</div>

<div align="center">  
**Figure 5: Proportion of queries about Australian topics by subject**</div>

Only 7.3% of queries were about Australian topics. There was no significant difference in this across the lifestyle groups but, as would be expected, the proportion of Australian queries varied across topic areas. As shown in Figure 5, almost one in five (19%) queries about places or buildings were about Australian places or buildings and 13% of queries about history were about Australian history. One in six (16%) queries about cultural practice related to Australia. These were mainly queries about sport and Anzac Day (an Australian public holiday that occurred during the sampling period).

## Discussion

### Use of _Wikipedia_

As in the United States, those in Australia who have higher educational attainment and a higher income are slightly more likely to use _Wikipedia_.

When the coverage of articles is compared with the subject of articles being accessed, it is clear that certain subject areas are being more intensively accessed than others. According to Halavais and Lackaff 's ([2008](#hal08)) study of the coverage of _Wikipedia_ articles, the topics with the greatest number of articles on _Wikipedia_ were general history and science, (each with approximately 13% of all articles) followed by geography, social science and literatures. Together, these five subjects account for 57% of articles. The analysis presented here suggests a very different pattern of use, whereby those five subjects accounted for 28% of visits. While articles about music, including popular music, account for 7% of _Wikipedia_ articles, queries about popular music accounted for double that proportion (14%) of visits.

The research by Lim ([2009](#lim09)) and by Ehmann _et al._ ([2008](#ehm08)) provides clues as to why this may be so. Lim's research suggests that the average user may be wary of the credibility of _Wikipedia_ for complex topics. Moreover, the research by Ehmann _et al._ ([2008](#ehm08)) suggests that most of the articles relating to traditional academic subjects, such as sociology, medicine, biology and philosophy, are difficult to read. In contrast to traditional academic subjects, information about popular culture and cultural practices is not conceptually difficult. Some researchers go so far as to classify queries lodged about favourite music and TV as entertainment rather than information-seeking ([Ewing _et al._ 2008](#ewi08); [Luyt _et al_. 2008](#luy08)). Together queries about popular culture and cultural practice accounted for just over half (51%) of all queries.

Previous research has found that, in terms of the content of the most popular search terms, Australia has similarities with the United States, Canada, Korea and the Netherlands ([Segev and Ahtiv 2010](#seg10)). Further research is needed to gauge whether this similarity in content of the most popular search terms extends to a similarity in the type of subjects being accessed in _Wikipedia_ via search engine queries.

### The importance of including the long tail

This study shows that looking at only the most common search terms or the most popular _Wikipedia_ pages can be misleading. It demonstrates the importance of analysing the long tail of search terms or visited pages when analysing the type of information accessed on _Wikipedia_. As already noted, in April 2009 the 100 most common search terms that took people to _Wikipedia_ accounted for only 4% of all visits to _Wikipedia_ from search engine results. There is no _a priori_ reason to assume that the distribution of topics in the most common 100 search terms is the same as the distribution of topics along the long tail. There were some similarities to Spoerri's ([2007](#spo07)) analysis of the top 100 pages, but also important differences. The proportion of queries relating to entertainment in the top 100 and along the long tail appears to be quite similar (Spoerri's code of Entertainment matched the code of Popular Culture grouped together with Fiction and Comics). Spoerri found that 43% of the 100 most visited _Wikipedia_ pages were related to entertainment. In this study, the equivalent proportion of search queries (queries relating to popular culture plus queries relating to fiction and comics) along the long tail was 39%. However, the findings on queries relating to sexuality were quite different. While pages providing information on sexuality accounted for 10% of the most visited pages ([Spoerri 2007](#spo07)), taking the long tail into account, queries on sexuality accounted for less than 1% of search queries that took people to _Wikipedia_.

### Who uses _Wikipedia_ and for what?

_Wikipedia_'s appeal appears to be fairly even across different segments of the Australian population. However, as in the United States, it seems that those in Australia with more income are more likely to use _Wikipedia_ and older people with lower income are less likely to use it. It is not clear from the data why this is so.

Significant differences across the lifestyle segments were observed in the proportion of queries about popular culture, cultural practice and science. It is possible that the observed differences between lifestyle groups in subjects of queries are to do with differences between lifestyle groups in any one of the conditions illustrated in Figure 1\. In other words, it is difficult to know whether these differences are a reflection of different interests, a different propensity to use the Internet or a search engine to research these topics, or different levels of appreciation for the _Wikipedia_ content on these topics.

Those most likely to arrive at _Wikipedia_ through pop culture queries were those from _medium to high density areas with much cultural diversity_ and _students and professionals living in high density, lower cost suburbs_. _Older blue-collar workers and retirees in country and coastal locations_ were much less likely to have queries relating to pop culture and much more likely to have queries relating to cultural practice. Although the explanation could be that this group is less likely to use the Internet for pop culture information, or they go directly to other Internet pages for pop culture information, this is unlikely. It is more likely that this group have less interest than other groups in pop culture. This explanation is borne out by the fact that according to Mosaic, this group _identifies strongly with all things Australian_ ([http://www.mosaicaustralia.com.au/](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.mosaicaustralia.com.au%2F&date=2011-04-24)). Although the proportion of specifically Australian queries was not significantly different for this group, the vast majority of the pop culture queries to _Wikipedia_ (and arguably, the vast majority of pop culture) relate to specifically American products.

A US study ([Horrigan 2006](#hor06)) found that people who turn to the Internet most for science information are more likely to be young and well educated. This study found that those most likely to arrive at _Wikipedia_ via science queries included the well-educated (those from group B _wealthy areas of educated professional households_) as well as those with lower educational attainment (those from group E _mixed family forms with stretched budgets in outer suburbs_). It is not at all clear from the data why this is so.

## Conclusions

This study contributes to studies on _Wikipedia_ use and on the field of Web search.

It informs debates concerning the quality and coverage of _Wikipedia_ and the possible role of academics and librarians by providing evidence of what parts of _Wikipedia_ are actually being accessed. The existing controversy about the accuracy of articles in _Wikipedia_ has focused on articles about serious academic subjects. Concerns about the accuracy of the information contained in _Wikipedia_ may be partially allayed by an understanding of the type of information that people are mainly accessing. The current study suggests that it is the entries on popular culture that are being accessed most frequently. Half of the visits to _Wikipedia_ from search engines are to find out information on popular culture or cultural practices. Nevertheless, it should also be borne in mind that even though the percentage of _Wikipedia_ queries on serious subjects may be small, the numbers of people visiting _Wikipedia_ are high. Indications are that amongst those Internet service providers sampled by Hitwise Australia, more than 1,000 people are using _Wikipedia_ every day to search for information on contemporary issues and even more are searching for health information.

It appeared that a small proportion (approximately 2%) of visits were to find the meaning of words. It will be interesting to see whether this proportion increases over time and whether _Wikipedia_ becomes significant as a dictionary as well as an encyclopaedia.

Significant differences among the various lifestyle segments were observed in the use of _Wikipedia_ for queries on popular culture, cultural practice and science. This analysis of transaction logs does not provide enough information to explain these differences. Finer-grained research on where people go for particular types of information should enable a clearer understanding on the propensity of particular types of people to use the Internet for information on particular topics and to turn to _Wikipedia_ for this information. It is also important that future research analyse search queries that are actually typed in to _Wikipedia_ to see if there is any difference in the spread of topics accessed. Given that search engines account for two thirds of visits, it is unlikely that the main findings of this study would change.

The analysis provides some analytical purchase on the complex nature of information search and the difficulties inherent in assuming that there is always a meaningful distinction between information search and entertainment or leisure. While the common presumption is that _Wikipedia_ is a source of information, the high proportion of popular culture queries suggests that for many, _Wikipedia_ could be considered a source of leisure. This highlights the need for a term to distinguish between information search where finding the information is the goal and information search that is an end in itself. I suggest the term _leisure search_ to indicate when the searching for information is itself a leisure activity. This is very different from _infotainment_, which is the imparting of information through the medium of entertainment. With infotainment, what is important is that the information is imparted to the viewer or listener. With leisure search, the searching is itself a form of leisure and the question _Did you find what you were looking for?_ does not make sense or is not relevant. The phenomenon of leisure search has existed before the Internet; for example, looking up particular terms in an encyclopaedia as a way of passing the time. However, whereas leisure search would have been quite an uncommon activity in the past, the ease of looking up anything on the Internet has facilitated the expansion of leisure search to become a very common activity. It is possibly similar in nature to surfing the web as a leisure activity.

Hence, this study contributes to the field of Web search user studies, indicating that use of a Web search engine may not be always be to achieve one of the goals identified by previous research. Rather than having a particular informational, navigational or transactional goal in mind, a search engine user may be engaging in a leisure activity. Whether an information search is leisure search or a search for information cannot be precisely determined by the subject. For one person, idly looking up science information is a leisure pursuit while for another, looking up a popular musician may be a search for a particular piece of information. It is reasonable to assume, however, that for most people a search related to a traditional academic subject will be an information-seeking activity while a query about popular culture is more likely to be to be a leisure activity than an information-seeking one. More research is needed to shed light on this issue. In the meantime, the implication for Web search user studies is that it is important to be specific about topics when asking people about, or reporting on, information search. In terms of improvement of search engine design, the implication is that it is important for search engine designers to understand more about the phenomenon of leisure search. This phenomenon could also have implications for the design of _Wikipedia_ pages relating to popular culture and their accessibility via search engines.

## Acknowledgements

This research was funded by Australian Research Council grant LP077215\. The author would like to thank Hitwise for their generous support, in particular, Sandra Hanchard, Ben Carroll and Bill Tancer as well as the State Library of Victoria and Dr Denny Meyer. Thanks also go to the anonymous reviewers for their helpful comments.

## About the author

Dr. Vivienne Waller is a Research Fellow at the Institute for Social Research at Swinburne University of Technology, Australia. She received Bachelor's degrees in both Science and the Arts from the University of Melbourne, Australia and her PhD from the Australian National University, Australia.  She can be contacted at: [vwaller@swin.edu.au](mailto:vwaller@swin.edu.au).

#### References

*   Anderson, C. (2006). _The long tail: why the future of business is selling less of more_. New York: Hyperion.
*   Badke, W. (2008). [What to do with Wikipedia](http://www.webcitation.org/5wPFQHnru). _Online_, **32**(2). Retrieved 2 February 2010 from http://www.infotoday.com/online/mar08/Badke.shtml Accessed: 2011-02-10\. (Archived by WebCite at http://www.webcitation.org/5wPFQHnru)
*   Broder, A. (2002). A taxonomy of Web search. _SIGIR Forum_ **36**(2), 3-10.
*   Chesney, T. (2006). [An empirical examination of Wikipedia's credibility](http://www.webcitation.org/5wPW5JDvK). _First Monday,_ **11**(11). Retrieved 15 October 2009 from http://www.uic.edu/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1413/1331 (Archived by WebCite at http://www.webcitation.org/5wPW5JDvK)
*   Duguid, P. (2006). [Limits of self-organization: peer production and "laws of quality".](http://www.webcitation.org/5wPWzoFZR) _First Monday,_ **11**(10). Retrieved 7 February 2010 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1405/1323 (Archived by WebCite at http://www.webcitation.org/5wPWzoFZR)
*   Dutton, W. H. & Helsper, E. J. (2007). _The Internet in Britain: 2007_. Oxford: Oxford Internet Institute, University of Oxford.
*   Dutton, W. H., Helsper, E. J. & Gerber, M. M. (2009). _The Internet in Britain: 2009._ Oxford:Oxford Internet Institute, University of Oxford.
*   Ehmann, K., Large, A. & Beheshti, J. (2008). [Collaboration in context: comparing article evolution among subject disciplines in Wikipedia](http://www.webcitation.org/5wPJpdV71). _First Monday,_**13**(10), Retrieved 15 October 2009 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/2217/2034 (Archived by WebCite? at http://www.webcitation.org/5wPJpdV71)
*   Eisenberg, M. B. (2009). [How today's college students use Wikipedia for course-related research.](http://www.webcitation.org/5wPJvJS9r) _First Monday,_ **15**(3). Retrieved 2 February 2010 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/2830/2476 (Archived by WebCite at http://www.webcitation.org/5wPJvJS9r)
*   Elvebakk, B. (2008). [Philosophy democratized? A comparision between Wikipedia and two other Web-based philosophy resources](http://www.webcitation.org/5wPKuYlkl). _First Monday,_ **13**(2). Retrieved 7 September 2009 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/2091/1938 (Archived by WebCite at http://www.webcitation.org/5wPKuYlkl)
*   Ewing, S., Thomas, J. & Schiessl, J. (2008). _CCi Digital Futures report: the Internet in Australia_. Melbourne: ARC Centre of Excellence for Creative Industries and Innovation, Institute for Social Research, Swinburne University of Technology.
*   Fallows, D. (2008). _Search engine users._ Washington, DC: Pew Internet and American Life Project.
*   Ford, N., Eaglestone, B., Madden, A., & Whittle, M. (2008). Web searching by the 'general public': an individual differences perspective. _Journal of Documentation_, **65**(4), 632-667.
*   Fox, S. & Jones, S. (2009). _The social life of health information_. Washington, D.C: Pew Internet and American Life Project.
*   Gans, H. (1999). _High culture and popular culture_. New York, NY: Basic Books.
*   Giles, J. (2005). Internet encyclopaedias go head to head. _Nature,_ **438**(15), 900-901.
*   Giles, J. (2006). Wikipedia rival calls in the experts. _Nature,_ **443**(5), 493.
*   Gorman, G. E. (2007). A tale of information ethics and encyclopaedias; or, is Wikipedia just another internet scam? _Online Information Review,_ **31**(3), 273-276.
*   Halavais, A, & Lackaff, D. (2008). An analysis of topical coverage of Wikipedia. _Journal of Computer-Mediated Communication,_ **13**, 429-440.
*   Hansen, S., Berentea, N. & Lyytinen, K. (2009). Wikipedia, critical social theory, and the possibility of rational discourse. _The Information Society,_ **25**(1), 38-59.
*   Holloway, T., Bozicevic, M. & Borner, K. (2007). Analysing and visualizing the semantic coverage of Wikipedia and its authors. _Complexity,_ **12**(3), 30-40.
*   Horrigan, J. B. (2006). _The Internet as a resource for news and information about science_. Washington, D.C: Pew Internet and American Life Project.
*   iProspect (2008). _[iProspect blended search results study](http://www.webcitation.org/5wPKzShvw)._ Retrieved 9 March 2010, from http://www.iprospect.com/about/researchstudy_2008_blendedsearchresults.htm (Archived by WebCite at http://www.webcitation.org/5wPKzShvw)
*   Jansen, B. & Spink, A. (2006). How are we searching the World Wide Web? A comparison of nine search engine transaction logs. _Information Processing & Management,_ **42**(1), 248-263.
*   Jansen, B. J. (2006). Search log analysis: what it is, what's been done, how to do it. _Library & Information Science Research,_ **28**_,_ 407-432.
*   Jansen, B. J., Booth, D.L & Spink, A. (2008). Determining the informational, navigational and transactional intent of Web queries. _Information Processing and Management,_ **44**, 1251-1266.
*   Keen, A. (2007). _The cult of the amateur: how today's Internet is killing our culture_. London: Nicholas Brealey.
*   Knight, S. A. & Spink, A. (2008). Toward a Web search information behavior model. In A. Spink and M. Zimmer, (Eds.), _Web search: multidisciplinary perspectives_ (pp. 209-234). Berlin: Springer-Verlag.
*   Lally, A. M. & Dunford, C. E. (2007). [Using Wikipedia to extend digital collections](http://www.webcitation.org/5wPL3f4Zv). _D-Lib Magazine,_ **13**(5/6). Retrieved 14 November 2009 from http://www.dlib.org/dlib/may07/lally/05lally.html (Archived by WebCite at http://www.webcitation.org/5wPL3f4Zv)
*   Lim, S. (2009). How and why do college students use Wikipedia? _Journal of the American Society for Information Science and Technology,_ **60**(11), 2189-2202.
*   Lipczynska, S. (2005). Power to the people: the case for Wikipedia. _Reference Reviews,_ **19**(2), 6-7.
*   Luyt, B., Zainal, C. Z. B. C., Mayo, O. V. P. & Yun, T. S. (2008). [Young people's perceptions and usage of Wikipedia](http://www.webcitation.org/5wPX9drJQ). _Information Research,_ **13**(4). Available at http://informationr.net/ir/13-4/paper377.html (Archived by WebCite at http://www.webcitation.org/5wPX9drJQ)
*   McGrady, R. (2009). [Gaming against the greater good](http://www.webcitation.org/5wPLERBPP). _First Monday,_ **14**(2). Retrieved 14 November 2009 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/2215/2091 (Archived by WebCite at http://www.webcitation.org/5wPLERBPP)
*   De Rosa, C., Cantrell, J., Cellentani, D., Hawk, J., Jenkins, L. & Wilson, A. (2005). _[Perceptions of libraries and information resources: a report to the OCLC membership](http://www.webcitation.org/5wPLIm5v8)_.Dublin, OH: OCLC. Available at http://www.oclc.org/reports/2005perceptions.htm (Archived by WebCite at http://www.webcitation.org/5wPLIm5v8)
*   Park, S. (2009). Analysis of characteristics and trends of Web queries submitted to NAVER, a major Korean search engine. _Library and Information Science Research,_ **31**, 126-133.
*   Portal: contents/categories. (n.d.). In _[_Wikipedia_](http://www.webcitation.org/5wPLN7bKo)._ Retrieved 3 March 2010 from http://en._Wikipedia_.org/wiki/Portal:Contents/Categorical_index (Archived by WebCite at http://www.webcitation.org/5wPLN7bKo)
*   Pressley, L. & McCallum, C. J. (2008). [Putting the library in Wikipedia](http://www.webcitation.org/5wPLRUOJa). _Online,_ **32**(5). Available at http://www.infotoday.com/online/sep08/Pressley_McCallum.shtml (Archived by WebCite at http://www.webcitation.org/5wPLRUOJa)
*   Rainie, L. & Tancer, B. (2007). _[_Wikipedia_ users](http://www.webcitation.org/5wPLVhEbT)_. Washington, DC: Pew Internet and American Life Project. Retrieved 3 July 2009 from http://www.pewinternet.org/Reports/2007/_Wikipedia_-users.aspx (Archived by WebCite at http://www.webcitation.org/5wPLVhEbT)
*   Rose, D. E. & Levinson, D. (2004). Understanding user goals in Web search. In _WWW 2004: proceedings of the thirteenth international World Wide Web conference: May 17-22_ (pp. 13-19). New York: Association for Computing Machinery.
*   Royal, C. & Kapila, D. (2009). What's on Wikipedia, and what's not ...?: assessing completeness of information. _Social Science Computer Review,_ **27**(1), 138-148.
*   Segev, E. & Ahituv, N. (2010). Popular searches in Google and Yahoo!: a "digital divide" in information uses? _The Information Society,_ **26**(1), 17-37.
*   Spinellis, D. & Louridas, P. (2008). The collaborative organization of knowledge. _Communications of the ACM,_ **51**(8), 68-73.
*   Spink, A. & Jansen, B. J. (2004). _Web search: public searching of the Web_. Dordrecht, Netherlands: Kluwer Academic Publisher.
*   Spink, A., Wolfram, D., Jansen, B.J. and Saracevic, T. (2001). Searching the Web: the public and their queries. _Journal of the American Society for Information Science_ **52**(3), 226-234.
*   Spoerri, A. (2007). [What is popular on Wikipedia and why?](http://www.webcitation.org/5wPLa6rKe) _First Monday,_ **12**(4 ). Retrieved 3 July 2009 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1765/1645 (Archived by WebCite? at http://www.webcitation.org/5wPLa6rKe)
*   Strauss, A. & Corbin, J. (1998). _Basics of qualitative research: grounded theory procedures and techniques_. Thousand Oaks, CA: Sage Publications.
*   Tann, C. & Sanderson, M. (2009). Are Web-based informational queries changing? _Journal of the American Society for Information Science and Technology,_ **60**(6), 1290-1293.
*   Waller, V. (2009). What do the public search for on the catalogue of the State Library of Victoria? _Australian Academic and Research Libraries,_ **40**(4), 266-285.
*   Wells, A. (2008). _[A portrait of early Internet adopters: why people first went online - and why they stayed](http://www.webcitation.org/5wPM5usMj)._ Washington, DC: Pew Internet and American Life Project. Retrieved 5 March 2010 from http://www.pewinternet.org/~/media//Files/Reports/2008/PIP_Early_Adopters.pdf.pdf (Archived by WebCite at http://www.webcitation.org/5wPM5usMj)
*   [_Wikipedia_ study fatally flawed](http://www.webcitation.org/5wPLjYS7o) (2006, March 24). _BBC News._ Retrieved 27 January 2011 from http://news.bbc.co.uk/2/hi/technology/4840340.stm (Archived by WebCite at http://www.webcitation.org/5wPLjYS7o)