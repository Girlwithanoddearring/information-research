#### Vol. 11 No. 4, July 2006



# Beyond information seeking: towards a general model of information behaviour.

#### [Natalya Godbold](mailto:nat@godbold.name)  
Information & Knowledge Management Programme  
University of Technology, Sydney, Australia

#### Abstract

> **Introduction.** The aim of the paper is to propose new models of information behaviour that extend the concept beyond simply information seeking to consider other modes of behaviour. The models chiefly explored are those of Wilson and Dervin.  
> **Argument** A shortcoming of some models of information behaviour is that they present a sequence of stages where it is evident that actual behaviour is not always sequential. In addition, information behaviour models tend to confine themselves to depictions of information seeking.  
> **Development.** A model of 'multi-directionality' is explored, to overcome the notion of sequential stages. Inspired by authors such as Chatman, Krikelas, and Savolainen, modes of information behaviour such as creating, destroying and avoiding information are included.  
> **Conclusion.** New models of information behaviour are presented that replace the notion of 'barriers' with the concept of 'gap', as a means of integrating the views of Wilson and Dervin. The proposed models incorporate the notion of multi-directionality and identify ways in which an individual may navigate 'gap' using modes of information behaviour beyond information seeking.



## Introduction

Information science has produced numerous models of information seeking. In [1994](#wil94), Wilson called for 'an integrative model of information need, information seeking behaviour and information use'. He said it should be a 'person centred model, based largely on Dervin's Sense-Making approach', and in 1996 he presented the beginnings of such a model ([Wilson 1997](#wil97)). An attempt will be made here to extend several of Wilson's models, in particular to more fully incorporate Dervin's theory of _sense-making_, her portrait of the human and her description of the conceptual _gap_ between an individual's inner reality and the external reality with which they find themselves confronted.

Rather than simply concentrating on information seeking however, a general model of information behaviour is presented, which includes information seeking as well as other forms of information behaviour. A range of alternative modes of information behaviour, identified by theorists over the last ten years, will be brought together in the process. A diagram is developed which expresses the multi-directional progression of the individual through the model instead of restricting them to a linear progression from one defined stage of information behaviour to another.

## Existing models of information behaviour

The models reviewed in this section are by no means an exhaustive survey of the field, nor are they discussed at any depth. They are presented to provide a theoretical and historical background, as they are the main models which inspired the proposed new model.

Brookes defined information as that which affects the world view or knowledge of the person receiving it. His 'fundamental equation' (below) states:

<div align="center">**K[S] + ΔI = K[S + ΔS]**</div>

> ...which states in its very general way that the knowledge structure K[S] is changed to the new modified structure K[S+ΔS] by the information ΔI, the ΔS indicating the effect of the modification. ([Brookes 1980](#bro80): 131)

Traditionally, information theorists have paid most attention to the processes undertaken by people when they approach a system (a library, a database) for information. Ellis [(1989)](#ell89) for instance, identified a list of characteristic actions within information seeking behaviour:  

*   starting
*   browsing / chaining / monitoring
*   differentiating
*   extracting
*   verifying
*   ending

## Four models by Wilson

Figure 1 shows Wilson's [1981](#wil81) model, as modified in [1999](#wil99). He used Ellis' list as charcteristics of information seeking behaviour, which he placed within the context of an information need arising out of a situation (of the person's environment, social roles and individual characteristics). That same context presents barriers which must be overcome before information seeking takes place.

<div align="center">![figure1](p269fig1.jpg)</div>

<div align="center">  
**Figure 1: Wilson's 1981 model of information-seeking behaviour**</div>

Wilson later [(1999)](#wil99) described this 1981 model as a 'macro model or a model of the gross information-seeking behaviour', suggesting that it implies hypotheses about information context without making them explicit, and that it does not indicate the processes whereby a person is affected by context, nor how context then affects his or her perception of barriers to information seeking.

By 1996 Wilson had expanded this model as shown in Figure 2 (reported in [Wilson, 1997](#wil97)). Aspects of the 1981 model have been developed, in particular the possible kinds of barriers which must be surmounted by the information seeker, and the possible forms his or her information seeking may take.

<div align="center">![figure2](p269fig2.gif)</div>

<div align="center">  
**Figure 2: Wilson's 1996 model of information behaviour**</div>

With the next model (Figure 3) presented in [1999](#wil99), Wilson pointed out that _information search behaviour_ is a subset of _information seeking behaviour_ and that _information seeking behaviour_ is in turn only a subset of all possible _information behaviour_. As such, the existence of modes of information behaviour, other than information seeking, is implied.

<div align="center">![figure3](p269fig3.jpg)</div>

<div align="center">  
**Figure 3: Wilson's 1999 nested model of information behaviour**</div>

Also in 1999, Wilson combined the work of Ellis ([1989](#ell89)) and Kuhlthau [(1991)](#kuh91), each of whom had suggested phases or stages which tend to occur within information seeking. In Figure 4, Wilson pointed to similarities between the two while emphasizing that the movement of the seeker between characteristic seeking behaviour can occur in varying sequence.

<div align="center">![figure4](p269fig4.jpg)</div>

<div align="center">  
**Figure 4: A stage process version of Ellis's behavioural framework and comparison with Kulthau's stage process model - by Wilson 1999**</div>

## Dervin and _sense-making_

Dervin ([1999](#der99)) provided a portrait of the protagonist which not only made the physical, social and psychological contexts explicit, but also placed those contexts inside time and pointed to the importance of the individual's plans, dreams, fantasies and illusions. She described reality as evolving and perceived differently by each human and attributed individuals with a kind of momentum (or inertia) in the form of a constant revision of the sense they can make of the world. 'The human condition [is] a struggle through an incomplete reality... Humans make sense individually and collectively as they move: from order to disorder, from disorder to order.' (Dervin [2000](#der00): 40-41). She refered to this process as _sense-making_.

In Dervin's description of _sense-making_, when one encounters a gap between one's understanding of the world and one's experience of the world, then the _sense-making_ momentum is stopped. Her model for this situation appeared in [1992](#der92). The [2003](#der03) version is reproduced in Figure 5.

<div align="center">![figure5](p269fig5.jpg)</div>

<div align="center">  
**Figure 5: Dervin's _sense-making_ metaphor (1992)**</div>

_Sense-making_ is here depicted using a bridge-building metaphor, creating a way across 'a gappy reality' which allows the individual's conceptual journey to resume. As such, Dervin's gap is both a barrier to _sense-making_ and a prompt to action; for example, to undertake information seeking.

## Shortfalls of the models

### Multi-directionality in information behaviour

In the diagrams for most of the models discussed so far, a sequential progression of information behaviour is depicted: the individual experiences an information need, goes out to seek information, finds it and thus solves the need. The simplicity of such a depiction is usually pointed out by authors. Wilson ([1999](#wil99): 267) for example, remarks 'Feedback loops must exist within all models, since progression towards a goal is hardly ever unproblematic'. Dervin stated 'it is not intended to suggest that all situation-facing is linear or purposive' ([Dervin 1992](#der92): 70). Observation confirms that information seekers do not necessarily follow ideal, optimized routes ([Solomon 1997c](#sol97c); [Pettigrew 1999](#pet99)). The order of information seeking tasks may be reversed or convoluted, and includes dead-ends, changes of direction, iteration, abandonment and beginning again. It therefore seems timely to develop a diagram which explicitly shows the myriad paths that can be taken through the different modes of information behaviour, from confusion to revelation and back again, until such time as the information excursion is deemed to be over.

### Alternative strategies for the gap

#### First, analyse the gap

#### When the gap seems big

Individuals may not search for information if they perceive that the gap is too big. The gap may appear too big when undertaking information behaviour involves risk, as suggested by Chatman ([2000](#cha00)) and Wilson ([1997](#wil97)):

*   emotional risk, (fear of bad news, fear of failure, fear of increased uncertainty);
*   physical risk (for instance if the individual is in a domestic violence situation) and
*   political/social risk (endangering your status or acceptability in your community).

Other situations in which the gap will appear to big are when the person feels they do not have time or cannot see how to proceed. They may not be able to see how to search usefully, they may not understand the problem enough or they may be unable to imagine the possibility of a solution. This can manifest as a form of fatalism as described by Chatman ([1991](#cha91)) in her study of janitors.

#### When the gap seems small

Individuals do not have to search for information if they can say that there is 'no' gap. This may mean:

*   that they know there is a gap but feel they can deal with it, such as discussed by stress/coping theory ([Wilson 1997](#wil97): 41); or
*   people tell them there is a gap but they can justify to themselves that there is no gap; or
*   no one in their community has noticed a gap.

But more generally

*   When the relative importance of the gap can be minimized.

Consideration of the gap is a kind of recurring cost-benefit analysis before and during any information behaviour. Is the gap too big (too difficult)? Is the gap small (not important enough given the current life situation)? When the gap is too big or small enough to ignore, the individual may abandon or not even start information seeking behaviour.

#### A wider view of information behaviour

In Wilson's models, the main information behaviour depicted is information seeking, although another 1981 model (Figure 1) does depict _information transfer, exchange_ and _use_. A number of models presented in recent years as models of information behaviour (such as [Niedzwiedzka 2003](#nie03); and [Pharo 2004](#pha04)) are also mainly concerned with information seeking. Exceptions include Fisher, whose 'Information grounds and information behaviour' model ([2005](#fis05): 187) includes _information giving_ and _information use_ and Erdelez's model of information behaviour, which includes what she calls _other forms of information behaviour_ ([2005](#erd05): 180).

Wilson used the term _information behaviour_ to describe 'those activities a person may engage in when identifying his or her own needs for information, searching for such information in any way and using or transferring that information.' ([Wilson 1999](#wil99): 249) but subsequently described information behaviour as 'the totality of human behaviour in relation to sources and channels of information, including both active and passive information seeking and information use.' ([Wilson 2000](#wil99): 49)

In the light of this wider definition, a multitude of alternative strategies proposed by the literature can be seen as information behaviour. Below is a list of identified common examples:

People encounter information by actively gathering information towards a goal, but also by:

*   routine information gathering ([Krikelas 1983](#kri83); [Savolainen 1995](#sav95); [Williamson 1998](#wil98)) and
*   picking up information by chance in the course of other activities. ([Krikelas 1983](#kri83); [Savolainen 1995](#sav95); [Williamson 1998](#wil98); Erdelez [2005](#erd05)

People respond to information by seeking more information, but also by:

*   sharing or spreading information, creating documents, telling other people. ([Krikelas 1983](#kri83); [Wilson 1994](#wil94); [Haythornthwaite 1996](#hay96); [Williamson 1998](#will98); [Pettigrew 1999](#pet99); [Rioux 2005](#rio05))
*   _taking mental note_ (non specific goal or goal not imagined yet) ([Krikelas 1983](#kri83))
*   avoiding or ignoring information ([Chatman 1996](#cha96), [2000](#cha00); [Wilson 1997](#wil97); [Solomon 2005](#sol05))
*   disputing or disbelieving information ([Chatman 1999](#cha99))
*   hiding and / or destroying information ([Chatman 1996](#cha96), [2000](#cha00))

In Dervin's 1992 diagram, the bridge across the gap is constructed using _strategies_. It is explicitly proposed here that all the different kinds of information behaviour identified above can be _sense-making_ strategies, that is, strategies undertaken by the individual to make sense, or to maintain sense, in an unruly world.

To bridge the gap, people can _seek information_. This allows them to change their internal reality, until it more closely fits what we are calling 'external reality'.

They can also close the gap by changing 'external reality' until it more closely fits their views. For instance, a person may set out to change what their community considers to be 'true' or 'acceptable': such a course may be taken by a researcher developing a new theory or by a person who stands accused of something. In this situation, _spreading_ or _disputing_ information may be effective information behaviour. Alternatively, the person may believe that most of his or her community is not aware of a situation which s/he has just discovered or realised. In this situation, the gap exists for the person, but will not be as much of a problem until the information spreads into the broader community. A person discovering that their child is gay may feel themselves to be in such a situation. The best information behaviour option may appear to be to _hide_ or _destroy_ the information. Or, a person may realize that the information he or she has encountered may be the tip of an iceberg. They may have discovered a genetic health risk in their family and not want to be diagnosed with the condition themselves. The gap is currently still negotiable, but a danger may be felt that more information, for instance a medical check up, would enlarge the gap and in a manner beyond his or her power to navigate. A person may take steps, therefore. to _avoid_ receiving more such information.

Or an individual may not cross the gap at all. Wilson ([1997](#wil97)) notes 'we are all happy, some of the time, not to make sense of the situation'. Happy or not, one cannot always successfully negotiate gap. Having first read Dervin's ideas some months after the unexpected death of my mother, I was immediately struck by the problem of an individual having a gap with no means to span it. Furthermore, that gap of mine remains: I still feel a need for information about death in general and about my mother's death in particular, information that I believe I will never obtain. It is plain that in some sense, some gaps may never go away. Yet life goes on and I am not still shivering by the gap. Instead, there seems to be a human ability to juggle many gaps simultaneously. Some problems may be left to one side for a time while the individual focuses on other issues. A version of this situation is that of the janitors observed by Chatman, who lived in a world 'in which they perceived that it was not possible for them to solve their problems by themselves' ([2000](#cha00): 5)

We now have a fuller collection of modes of information behaviour with which to populate Wilson's nested model of 1999 (Figure 3 above) of information behaviour. The analysis thus far suggests there may be three useful strategies to cope with gap and information behaviour could roughly be categorised within them as shown:

1.  build a bridge
    *   seek information
    *   create information
2.  make the gap smaller
    *   spread or dispute information
    *   destroy information
3.  ignore the gap (take a different path)
    *   avoid or disbelieve information
    *   take mental note

It is acknowledged that the categorisation of behaviour shown here is arbitrary to some extent (and the list is not exhaustive). Depending on individual motivation, a person may create information to build a bridge or to make the gap smaller. Avoiding information may be seen as a way of ignoring the gap or as a way of keeping it small.

## Proposed new models

### Extending Wilson's 1981 model of information seeking behaviour

The preceding discussion gives rise to some obvious preliminary extensions to Wilson's 1981 model (Figure 1 above). In Figure 6, I introduce the concepts of Dervin's gap and human-in-context and extend the possibilities of information behaviour beyond information seeking only.

<div align="center">![figure6](p269fig6.jpg)</div>

<div align="center">  
**Figure 6: Extension of Wilson's 1981 model of information-seeking behaviour**</div>

This new model shows the person who will soon exhibit information behaviour, once again enclosed in the context of his or her situation. Here, however, the person's individual characteristics incorporate Dervin's construction of the actor as 'A body-mind-heart-spirit living in a time-space... anchored in material conditions' ([Dervin 1999](#der99): 730). As Dervin's human is always in context, context probably no longer needs to be considered separately, but is retained in the diagram as a reminder of its importance. Wilson's proposed barriers to information seeking, with their implied connection to the context of the person in his or her situation, have been replaced by Dervin's gap.

The person stands at the gap, contemplating the break in his or her momentum and the need for _sense-making_. On the other side of the gap, the possibilities of information behaviour have been expanded to include the full range of options identified by other thinkers. Information seeking, should it be undertaken, is now only one of six possible courses of action.

This model however, is still linear, so further diagrammatic development is required.

### Extending Wilson's 1999 model of Ellis/Kulthau's information seeking behaviour

Figure 7 is an extension of Wilson's 1999 model (Figure 4) of information seeking in which are included the concepts of Dervin's gap and multi-directionality of information behaviour. In Figure 7 we see that at every stage of the information process, individuals have the opportunity to stop and re-analyse the situation and if they wish, abandon their search or change into a different mode.

Information seeking behaviour is conceived to start at the _initiation_ phase and finish at the _ending_ phase, but apart from that, individuals may go to any one of the modes described here, including returning to a new start, in the course of their search. At each step, the gap is faced anew and the individual's assessment of it will affect his or her next move.

<div align="center">![figure7](p269fig7.jpg)</div>

<div align="center">  
**Figure 7: Extension of Wilson's 1999 model of the link between those of Ellis and Kulthau**</div>

However this analysis is still restricted to information seeking, which is only one of the many possible modes of information behaviour. A final extended model of information behaviour is required, one which incorporates the gap, multi-directionality and the proposed range of human information behaviour.

### Navigating the gap: extending Wilson's 1999 nested model of information behaviour

The model of information behaviour presented below is intended as an extension of Wilson's nested model of information behaviour (Figure 3 above), but also incorporates his earlier models of information seeking behaviour (Figure 1 and 2 above), Dervin's model of information seeking (Figure 5 above) and Brookes' fundamental equation.

Once again we consider the person who may soon exhibit information behaviour, halted at the gap. In this model, the individual begins with a knowledge state K and is subjected to Brookes' fundamental equation - restated here in simplified form:

<div align="center">K + I = K'</div>

<div align="center">![figure8](p269fig8.jpg)</div>

<div align="center">  
**Figure 8: Navigating gap**</div>

The person's knowledge structure K is changed to the new modified structure K' by passing through the __information behaviour wheel__ (I). As K' is actually a variation on K, this progression is better imagined as a spiral.

<div align="center">![figure9](p269fig9.jpg)</div>

<div align="center">**Figure 9: Spiral motion associated with gap navigation**</div>

The name __information behaviour wheel__ was chosen because a person can pass through any sequence of the modes of information behaviour depicted on it, any number of times.

The three strategies for navigating the gap (build a bridge, close the gap, take a different path) are presented here. Within each strategy, suggested relevant information behaviour appears. There is no reason why an individual has to remain within one strategy. It should be noted again that the modes of information behaviour depicted are unlikely to be an exhaustive list.

The specifics of Wilson's _activating mechanism_ (from Figure 2 above) have begun to be fleshed out in this model. The barriers to information seeking from Wilson's 1981 model (Figure 1 above) are here depicted by the gap; similarly the gap here contains the _intervening variables_, _stress/coping theory_, _risk/reward theory_ and _social learning_ presented in Wilson's 1996 model (Figure 2). These elements are valuable and are not being discarded. This model focuses on the depiction of information behaviour and gap navigation, so the elements relevant to a closer view are not depicted. Similarly, the details of information seeking behaviour displayed in other diagrams have been packed into the _information seeking_ mode here.

Throughout the _information behaviour wheel_, the gap is repeatedly encountered and measured. Information strategies undertaken may have made the gap appear larger or smaller. The person remains in this phase of navigation until the point where they have 'built a bridge', or decide that the gap is now either too big to navigate or small enough to ignore. At that point they leave the _information behaviour wheel_ with knowledge state K'.

Regardless of the path an individual takes to in response to gap, even if they merely take note or avoid it, he or she will be changed by the experience. In particular, the memory of how s/he reacted to the gap and the outcome of the situation remain with the individual as the basis of a possible new pattern of behaviour.

## Applications and implications

### Multi-directionality

As has been noted, the diagram at Figure 8 makes explicit the mulit-directional progression of an individual through the gap.

### Focus on the gap

In previous models, the gap was referred to implicitly, or only partially explored. The diagrams discussed here make the gap explicit and direct attention towards it. The most obvious research area to suggest here is the gap itself and particularly how people navigate the gap, possibly using the interviewing techniques developed by Dervin for understanding the user.

### How is the individual changed?

K', the individual's changed knowledge state after navigating the gap, draws attention to the changed individual. How are people's knowledge states affected by information? How does a person's prior experience with information affect his or her next interaction with it, when s/he next contemplates the gap? The model points to the complex relationship between knowledge and information: one does not just add information to change knowledge in some predictable way. Rather, outcomes are dependant on the reaction of the individual and sense making strategies used (paths taken) ([Todd 1999](#tod99)).

### Information behaviour does not need the gap

The gap prompts information behaviour, but information behaviour, in particular information seeking, spreading and creation, also occurs without experience of a gap. As frequently noted (for instance by [Krikelas 1983](#kri83); [Savolainen 1995](#sav95); [Rioux 2005](#rio05)), at times people take pleasure in information behaviour for its own sake, in knowing or discovering things, in telling others and so on. For others, the primary motivation is to do nothing about the gap they experience, wherever possible (see Case's ([2005](#cas05)) discussion of Zipf's Principle of Least Effort). In his discussion of 'non-purposeful information behaviour', Olsson ([2005](#ols05)) remarks, 'rather than conceiving of information behaviour as being driven by the desire to satisfy discrete information needs, any information interaction or encounter should be seen as one chapter in an individual's ongoing engagement with and construction of, their life-world.'

### More than one gap at a time

As noted previously, there appears to be a human ability to juggle many gaps simultaneously. Problems may be left to one side while the individual focuses on other issues. Negotiation of one gap may give rise to discovery of other gaps, which must then be navigated or ignored. Foster ([2005](#fos05)), discussing 'non-linear information seeking behaviour', referred to 'concurrent, continuous, cumulative and looped cycles' It would be useful to extend the model to depict this multiplicity of intertwining paths.

### Ongoing gap

A theoretical point which emerges from the preceding discussion is that the process of navigating the gap does not necessarily make the gap go away. Apart from metaphorical bridge building, the other tactics presented were that the individual moves on as soon as the gap appears small enough, or else merely takes a different path in order to evade the gap. That the gap remains may be a symptom of the ongoing _struggle through an incomplete reality_ described by Dervin ([2000](#der00): 40-41). Note that gap, being a subjective experience, is not the same as the permanent difference between human understanding and external reality. That difference is there all the time. The gap only appears when we perceive it.

## Suggested research directions

### Exploring typical paths

Researchers of information systems such as libraries and computer-based information retrieval systems have long been aware of the need to keep people from dropping out of information searches. The proposed model draws attention to possible relationships between different modes of information behaviour. When will an individual take one kind of path through information behaviour to information seeking and away from it and why? How do other preferred or habitual modes of information behaviour affect a person's likelihood of successfully finding (and dealing with) information? Are there common habitual patterns or strategies of information behaviour?

Bearing in mind Dervin's ([1989](#der89)) assessment of the inadequacies of demographic, psychological and other traditional categories to predict individual information needs and considering that people's experiences of information behaviour are an internal and highly subjective process, it would be reasonable to take an interpretivist approach to the problem. Kuhlthau ([1991](#kuh91)) combined quantitative measures with logs, journals, concept maps and in-depth, open-ended styles of interviewing to identify stages of emotional response to information seeking phases, as experienced by high school students. Solomon ([1997a](#sol97a); [1997b](#sol97b); [1997c](#sol97c)) suggested relationships between personality, context and _sense-making_ styles, which included behaviour such as information gathering and sharing.

In a [1980](#der80) paper, Dervin categorised patients speaking to health practitioners according to the patient's own perceived _sense-making_ momentum and found that their information seeking and use was predicted by the patients' descriptions of their situations. It would be useful to extend this kind of enquiry beyond information seeking, this time including in the analysis a general investigation of how people navigate the gap. Are there styles of navigating the gap that relate to the way people perceive their situation? Can satisfaction levels for each course of action be measured? Which paths work best for protagonists, under what _sense-making_ conditions?

In fact, investigation of recent trends in information behaviour theory reveal a whole range of other ways in which pre-existing research could be extended beyond information seeking. Is there a link between the way professions see themselves ([Hjørland 2005](#hjo05); [Sundin & Hedman](#sun05) [2005](#sun05); [Talja 2005](#tal05)) and patterns of information behaviour? Are behavioural patterns linked to self efficacy? Savolainen ([2005](#sav05): 146) remarked that mastery of life 'describes the tendency to adopt a certain information seeking strategy'; does it also describe a general tendency toward strategies of information behaviour? Perhaps information behaviour patterns are linked more directly to the context of the actor at the time of the behaviour; for instance to information intents ([Todd 2005](#tod05)), affective load ([Nahl 2005](#nah05)), motivational factors ([Watters & Duffy 2005](#wat05)), or the task in which the actor or actors are engaged ([Hansen 2005](#han05); [Wildemuth & Hughes 2005](#wil05))? Would the problem be investigated best by using discursive information research ([Tuominen _et al._ 2005](#tuo05)) and described by using social positioning theory ([Given 2005](#giv05)), or viewed in some other way?

## Conclusion

In a recent paper, Wilson ([2005](#wil05): 32-33) again reflects on the importance of developing a 'general model of information seeking behaviour' (and lack of progress in this endeavour), to be used as a basis for 'thinking about the problems of human information behaviour'. Inspired by the usefulness of ideas from many areas of information behaviour research, I propose three models which attempt to depict the variety of information practices which have emerged in the past decade. In particular, the proposed models are based on Wilson's models of information behaviour and Dervin's 1999 model of _sense-making_. They depict the multi-directional process that a progression through Dervin's gap can entail and include an expanded list of modes of information behaviour, beyond information seeking alone. I hope that these new models present some of the main themes in modern information science in a way that is clear and useful and, as such, may serve as a step towards a general model of information behaviour.

## Acknowledgements

The author wishes to thank Dr Michael Olsson for his critical insight, encouragement and inspiration, as well as the anonymous reviewers and the Editor for their valuable suggestions.

## References

*   <a id="bak05" name="bak05"></a>Baker, L. M. (2005). Monitoring and blunting. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 239-241). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="bro80" name="bro80"></a>Brookes, B. (1980). The foundations of information science. Part1: Philosophical aspects. _Journal of Information Science_, **2**, 125-133\.
*   <a id="cas05" name="cas05"></a>Case, D. O. (2005). Principle of least effort. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_. Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="cha91" name="cha91"></a>Chatman, E. A. (1991). Life in a small world: Applicability of gratification theory to information seeking behavior. _Journal of the American Society for Information Science_, **42**(6), 438-449\.
*   <a id="cha96" name="cha96"></a>Chatman, E. A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science_, **47**(3), 193-206\.
*   <a id="cha99" name="cha99"></a>Chatman, E. A. (1999). A theory of life in the round. _Journal of the American Society for Information Science_, **50**(3), 207-217\.
*   <a id="cha00" name="cha00"></a>Chatman, E. A. (2000). Framing social life in theory and research. _New Review of Information Behaviour Research_, 3-17\.
*   <a id="der80" name="der80"></a>Dervin, B. (1980). The human side of information: an exploration in a health communication context. _Communication Yearbook_, **4**, 591-608\.
*   <a id="der89" name="der89"></a>Dervin, B. (1989). Users as research inventions: how research categories perpetuate inequities. _Journal of Communication_, **39**(3), 216-232\.
*   <a id="der92" name="der92"></a>Dervin, B. (1992). From the mind's eye of the user: the _sense-making_ qualitative-quantitative methodology. In J. D. Glazier & R. R. Powell (Eds.), _Qualitative research in information management_. Englewood, CO: Libraries Unlimited Inc.
*   <a id="der99" name="der99"></a>Dervin, B. (1999). On studying information seeking methodologically: the implications of connecting metatheory to method. _Information Processing & Management_, **35**(6), 727-750\.
*   <a id="der00" name="der00"></a>Dervin, B. (2000). Chaos, order and _sense-making_: A proposed theory for information design. In R. Jacobson (Ed.), _Information design_ (pp. 35-57).
*   <a id="der03" name="der03"></a>Dervin, B. (2003). _Sense-making methodology reader: selected writings of Brenda Dervin_ Cresskill, NJ: Hampton Press.
*   <a id="ell89" name="ell89"></a>Ellis, D. (1989). A behavioural approach to information retrieval design. _Journal of Documentation_, **45**(3), 171-212\.
*   <a id="erd05" name="erd05"></a>Erdelez, S. (2005). Information encountering. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 179-184). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="fis05" name="fis05"></a>Fisher, K.E. (2005). Information grounds. In K.E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 185-190). Medford, NJ: Information Today, Inc. (ASIST Monograph Series).
*   <a id="fos05" name="fos05"></a>Foster, A. (2005). [A non-linear model of information seeking behaviour](http://informationr.net/ir/10-2/paper222.html). Retrieved 20th March 2006, from http://informationr.net/ir/10-2/paper222.html
*   <a id="giv05" name="giv05"></a>Given, L. M. (2005). Social positioning. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 334-338). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="han05" name="han05"></a>Hansen, P. (2005). Work task information-seeking and retrieval processes. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 392-396). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="hay96" name="hay96"></a>Haythornthwaite, C. (1996). Social network analysis: An approach and technique for the study of information exchange. _Library & Information Science Research_, **18**, 323-342\.
*   <a id="hjo05" name="hjo05"></a>Hjorland, B. (2005). The socio-cognitive theory of users situated in specific contexts and domains. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 339-343). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="kri83" name="kri83"></a>Krikelas, J. (1983). Information seeking behaviour: Patterns and concepts. _Drexel Library Quarterly_, **19**(2), 5-20\.
*   <a id="kuh91" name="kuh91"></a>Kuhlthau, C. C. (1991). Inside the search process: Information seeking from the user's perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371\.
*   <a id="nah05" name="nah05"></a>Nahl, D. (2005). Affective load. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 39-43). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="nie03" name="nie03"></a>Niedzwiedzka, B. (2003). [A proposed general model of information behaviour](http://informationr.net/ir/9-1/paper164.html). _Information Research_, **9**(1), paper 164\. Retrieved 20th March 2006, from http://informationr.net/ir/9-1/paper164.html
*   <a id="ols05" name="ols05"></a>Olsson, M. (2005). [Meaning and authority: the social construction of an 'author' among information behaviour researchers](http://informationr.net/ir/10-2/paper219.html). _Information Research_, **10**(2), paper 219\. Retrieved 20th March 2006, from http://informationr.net/ir/10-2/paper219.html
*   <a id="pet99" name="pet99"></a>Pettigrew, K. (1999). Waiting for chiropody: Contextual results from an ethnographic study of the information behaviour among attendees at community clinics. _Information Processing & Management_, **35**(6), 801-817\.
*   <a id="pha04" name="pha04"></a>Pharo, N. (2004). [A new model of information behaviour based on the search situation transition schema](http://informationr.net/ir/10-1/paper203.html). _Information Research_, **10**(1), paper 203\. Retrieved 20th March 2006, from http://informationr.net/ir/10-1/paper203.html
*   <a id="rio05" name="rio05"></a>Rioux, K. (2005). Information acquiring-and-sharing. In K.E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 169-173). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="sav95" name="sav95"></a>Savolainen, R. (1995). Everday life information seeking: Approaching information seeking in the context of 'way of life'. _Library & Information Science Research_, **17**, 259-294\.
*   <a id="sav05" name="sav05"></a>Savolainen, R. (2005). Everyday life information seeking. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 143-148). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="sol97a" name="sol97a"></a>Solomon, P. (1997a). Discovering information behavior in sense making, I. Time and timing. _Journal of the American Society for Information Science_, **48**(12), 1079-1108\.
*   <a id="sol97b" name="sol97b"></a>Solomon, P. (1997b). Discovering information behavior in sense making, II. The social. _Journal of the American Society for Information Science_, **48**(12), 1109-1126\.
*   <a id="sol97c" name="sol97c"></a>Solomon, P. (1997c). Discovering information behavior in sense making, III. The person. _Journal of the American Society for Information Science_, **48**(12), 1127-1138\.
*   <a id="sol05" name="sol05"></a>Solomon, P. (2005). Rounding and dissonant grounds. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 308-312). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="sun05" name="sun05"></a>Sundin, O., & Hedman, J. (2005). Professions and occupational identities. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 293-297). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="tal05" name="tal05"></a>Talja, S. (2005). The domain analytic approach to scholar's information practices. In K.E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 123-127). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="tod99" name="tod99"></a>Todd, R.J. (1999). Adolescents and drugs: responsive information services in schools and libraries. _Orana_, **35**(3), 32-44\.
*   <a id="tod05" name="tod05"></a>Todd, R. J. (2005). Information intents. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 198-203). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="tuo05" name="tuo05"></a>Tuominen, K., Talja, S., & Savolainen, R. (2005). The social constructionist viewpoint on information practices. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 328-333). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="wat05" name="wat05"></a>Watters, C., & Duffy, J. (2005). Motivational factors for interface design. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 242-246). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="wild05" name="wil05"></a>Wildemuth, B. M., & Hughes, A. (2005). Perspectives on the tasks in which information behaviors are embedded. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 275-279). Medford, N.J: Information Today, Inc. (ASIST Monograph Series).
*   <a id="will98" name="will98"></a>Williamson, K. (1998). Discovered by chance: the role of incidental information acquisition in an ecological model of information use. _Library & Information Science Research_, **20**(1), 23-40\.
*   <a id="wil81" name="wil81"></a>Wilson, T.D. (1981). [On user studies and information needs.](http://informationr.net/tdw/publ/papers/1981infoneeds.html) _Journal of Documentation_, **37**(1), 3-15\. Retrieved 10 June, 2006 from http://informationr.net/tdw/publ/papers/1981infoneeds.html
*   <a id="wil94" name="wil94"></a>Wilson, T.D. (1994). [Information needs and uses: fifty years of progress?](http://informationr.net/tdw/publ/papers/1994JDocRev.html) In B. Vickery (Ed.), _Fifty years of information progress: A Journal of Documentation review_ (pp. 15-51). London: Aslib. Retrieved 10 June, 2006 from http://informationr.net/tdw/publ/papers/1994JDocRev.html
*   <a id="wil97" name="wil97"></a>Wilson, T.D. (1997). Information behaviour: an inter-diciplinary persepective. In: P. Vakkari, R. Savolainen & B. Dervin (Eds.). _Information seeking in context. Proceedings of an international conference on research in information needs, seeking and use in different contexts 14-16 August, 1996, Tampere, Finland._ (pp. 39-50) London: Taylor Graham.
*   <a id="wil99" name="wil99"></a>Wilson, T.D. (1999). [Models in information behaviour research.](http://informationr.net/tdw/publ/papers/1999JDoc.html) _Journal of Documentation_, **55**(3), 249-270\. Retrieved 10 June, 2006 from http://informationr.net/tdw/publ/papers/1999JDoc.html
*   <a id="wil00" name="wil00"></a>Wilson, T.D. (2000). [Human information behavior](http://informationr.net/tdw/publ/papers/2000HIB.pdf). _Informing Science_, **3**(1), 49-55\. Retrieved 10 June, 2006 from http://informationr.net/tdw/publ/papers/2000HIB.pdf
*   <a id="wil05" name="wil05"></a>Wilson, T.D. (2005). Evolution in information behavior modeling: Wilson's model. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_. (pp. 31-36). Medford, N.J: Information Today, Inc.

