#### Vol. 11 No. 4, July 2006



# From information to knowledge: charting and measuring changes in students' knowledge of a curriculum topic

#### [Ross J. Todd](mailto:)  
Center for International Scholarship in School Libraries  
School of Communication, Information and Library Studies  
Rutgers, The State University of New Jersey  
4 Huntington Street, New Brunswick  
New Jersey 08901, USA



#### Abstract

> **Introduction.** This research sought to investigate how school students build on their existing knowledge of a curriculum topic and transform found information into personal knowledge, and how their knowledge of this topic changes.  
> **Method.** The qualitative study involved 574 students from Grades 6 to 12 in ten New Jersey schools. The context for data collection was an instructional programme framed by Kuhlthau's _information search process_.  
> **Analysis.** Data were collected through surveys at the initiation, midpoint and conclusion of the instructional programme. The instruments sought to measure changes in knowledge, specifically in relation to substance of knowledge, structure of knowledge, amount of knowledge, estimate of extent of knowledge, and label of knowledge.  
> **Results.** It was possible to operationalise knowledge change in terms of substance, amount and structure of knowledge, and user-centreed perceptions of knowledge growth. Additive and integrative approaches to knowledge development were identified.  
> **Conclusion.** Students came to know more about their topics, and perceived that they knew more as they progressed through the task. However, students seemed more oriented to gathering facts and knowing a set of facts, and accumulating these in an additive manner, rather than building complex, integrated and abstract knowledge representations.



## Introduction

The research reported here was part of a large research and development project that sought to design and test instruments for tracking how students build knowledge of a topic through the school library, ultimately to be used by school teams to accumulate evidence of how school libraries make an impact on learning. In the broader context of information seeking and use, this project sought to understand more about how students use found information as a result of information seeking: specifically, how they build on existing knowledge and transform found information into personal knowledge, and how their knowledge of a topic changes. Methodologically, this project sought to explore approaches to the elicitation, representation and measurement of new knowledge and the interactions of cognitions, behaviors and feelings in this constructive process.

The project's focus was shaped by contextual factors in schools, including increasing emphasis on standards-based education, accountability, school improvement, and calls for school librarians to demonstrate links between school libraries and gains in knowledge and skills. While there is evidence that school libraries make an impact on student achievement in standardized test scores (Lance 2001) and help students in information seeking and using ([Todd & Kuhlthau 2005](#tod05)), the project sought to build a more comprehensive picture of how student learning through using found information might be characterized and measured.

The research reported here is about information use: the transformation and integration of found information into existing knowledge, and the creation of new knowledge. Wilson defines information use as the physical and mental acts employed by humans to incorporate found information into their knowledge base or knowledge structure ([Wilson 2000](#wil)). Vakkari ([1997](#vak)) and Spink & Cole ([2005](#spi)) acknowledge that information use research is the least developed in information behaviour research. School students spend considerable time seeking information sources to complete research tasks, and this context gives rise to many questions about what happens as a result of information seeking: How do they use the found information? Do they build new knowledge about a topic? If so, what does this knowledge look like? How does their existing knowledge change through information seeking and use? How can this knowledge change be measured?

Three concepts are central to this research: learning, knowledge representation and knowledge change. Learning is perceived as a process of personal and social construction where people are actively involved in making sense of information they interact with, rather than passively receiving it ([Kuhthau 2004](#kuh)). This cumulative and developmental process involves the whole person in thinking, acting, reflecting, discovering ideas, making connections, and transforming prior knowledge, skills, attitudes, and values into new knowledge ([Dewey 1933](#dew)). In this research, learning is operationalised as students' development of knowledge about a topic. This requires an understanding of how students use found information as a result of information seeking to transform existing knowledge and build new topical knowledge.

The research arena of representing knowledge and measuring how it changes draws on many disciplines, each articulating various philosophical and epistemological assumptions about what knowledge is, how it is structured, elicited and represented ([Todd 1999a](#tod99a)). Several long-held assumptions have guided this research: subjective, private knowing is structured, and can be adequately elicited and represented ([Markman 2002](#mar)); past experience and prior knowledge form the basis for constructing new knowledge ([Dewey 1933](#dew); [Kelly 1963](#kel)); knowledge may be depicted as the overall structure of concepts linked together by relationships ([Ausubel _et al._ 1978](#aus) ); previous knowledge forms a filter in the selection and rejection of new information and knowledge structures may or may not change as a result ([Brookes 1980](#bro)); conceptual change develops gradually by modifying, refining and transforming previous knowledge structures ( [Dole & Sinatra 1998](#dol); [Todd 1999b](#tod99b)).

This literature also identifies common approaches to knowledge elicitation and acquisition, that is, writing down a person's knowledge in a formal, useable way. These include written and verbal protocols, (for example, interviews, questionnaires), familiar task methods, think-alouds, and concept mapping ( [Cooke 1999](#coo); [Polk & Seifert 2002](#pol)). Though not without challenge (see, for example [Talja 1997](#tal)), these measures assume a strong relation between cognition and discourse, whereby written and verbal accounts function as a lens through which knowledge can be observed and measured.

In schools, the measurement of students' knowledge typically centres on verbal and written protocols such as essays, exams, and presentations. Rather than being measured in terms of initial existing knowledge and how it has changed through information seeking and information use, the amount and nature of topical knowledge is typically determined by subject experts (teachers) who match elicited knowledge to some target, expert conception and then assign a score. Amount of knowledge, as a prescribed word count, is often part of this measurement.

Building on Kuhlthau's research ([2004](#kuh)) on the _information search process_, considerable research in information science has examined the relationships between dimensions of a topic, search process and knowledge structures. In reviewing this research, Pennanen & Vakkari ( [2003](#pen): 761) conclude that people's conceptual construct of a topic becomes more structured and complex when their level of knowledge of a topic or task increases. However, mapping the actual knowledge output as students progress through the stages of information searching has been given limited attention. Pennanen & Vakkari ([2003](#pen)) identify the need to develop more accurate techniques for representing and measuring conceptual structures and how they change.

Educational research has given some emphasis to differentiating novices' and experts' knowledge. It has found that novices' and experts' knowledge varies in terms of number of links, nature of clusters of statements, structural coherence and centrality. In particular, this research has found that, as knowledge becomes more detailed and comprehensive, knowledge structures show more local and global coherence, with mechanisms of knowledge construction such as addition, deletion, discrimination and generalization being evident ( [Dole & Sinatra 1998](#dol); [Chi 1992](#chi); and [Gobbo & Chi 1986](#gob) ). Important work has also been undertaken by Limberg ([1999](#lim99), [2005](#lim05)) in particular, who posits that variations in the quality of learning outcomes corresponds to variations in information seeking and use. Limberg found that students experience information seeking and use in three ways: fact-finding, balancing information in order to make correct choices, and scrutinizing and analysing information.

In summary, little research has focused on establishing existing knowledge and measuring how it changes through using information found from information sources. Accordingly, this research seeks to understand how school students build knowledge using found information in the context of an inquiry unit framed by Kuhlthau's _information search process_. It draws on established methods in the knowledge elicitation and representation field, and seeks to bring together in one study diverse approaches to measuring and characterizing changes in knowledge.

## Specific research questions

In order to identify changes in knowledge, the following specific research questions were investigated:

> 1.  **Substance of Knowledge:** What changes in content of relational statements are evident in students' knowledge descriptions as they progress through the stages of a guided inquiry project?
> 2.  **Amount of Knowledge:** What changes in amount of relational statements are evident in students' knowledge descriptions as they progress through the stages of a guided inquiry project?
> 3.  **Structure of Knowledge:** What changes in structure of relational statements are evident in students' knowledge descriptions as they progress through the stages of a guided inquiry project?
> 4.  **Title of Knowledge:** How do the students' titles given to their topics reflect their knowledge descriptions as they progress through the stages of a guided inquiry project?
> 5.  **Extent of Knowledge:** How do students perceive their knowledge changes as they progress through the stages of a guided inquiry project?

## Sample

The study involved ten school librarians working with seventeen classroom teachers from ten diverse public schools in New Jersey. Data were collected from 574 students in Grades 6 to 12 who were undertaking a library-based guided inquiry project. There was one credentialed school librarian in each school and the school librarians partnered with teachers in designing and delivering instruction. The school teams were non-randomly chosen through a public nominations process and selected by the project's expert panel. The teams were trained in implementation protocols, including the nature of inquiry-based projects, Kuhlthau's _information search process_ and applying the data collection instruments. Data were collected during March-April 2004\. Table 1 provides a summary of grade, student, classroom teacher involvement, and curriculum topics:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Sample characteristics and inquiry project focus**</caption>

<tbody>

<tr>

<th>School</th>

<th>Grade</th>

<th>No. of Students</th>

<th>No. of Teachers</th>

<th>Project Description</th>

</tr>

<tr>

<td align="center">1</td>

<td align="center">6</td>

<td align="center">47</td>

<td align="center">2</td>

<td>Social Studies: Ancient civilizations</td>

</tr>

<tr>

<td align="center">2</td>

<td align="center">8</td>

<td align="center">72</td>

<td align="center">1</td>

<td>Social Studies - Middle Ages & Renaissance</td>

</tr>

<tr>

<td align="center">3</td>

<td align="center">9-10</td>

<td align="center">19</td>

<td align="center">1</td>

<td>Biology - 8 characteristics of life</td>

</tr>

<tr>

<td align="center">4</td>

<td align="center">8</td>

<td align="center">63</td>

<td align="center">2</td>

<td>Heritage project</td>

</tr>

<tr>

<td align="center">5</td>

<td align="center">9-12</td>

<td align="center">79</td>

<td align="center">3</td>

<td>3 projects: Research & Debate - current issues in society, Social studies - UN; English - 'American Dream' 1920s</td>

</tr>

<tr>

<td align="center">6</td>

<td align="center">8</td>

<td align="center">64</td>

<td align="center">3</td>

<td align="center">Three projects: ESL - heritage project; English - multicultural literature, self choice; Biology</td>

</tr>

<tr>

<td align="center">7</td>

<td align="center">10</td>

<td align="center">22</td>

<td align="center">2</td>

<td>An accomplished person</td>

</tr>

<tr>

<td align="center">8</td>

<td align="center">10</td>

<td align="center">74</td>

<td align="center">1</td>

<td>Chemistry - A chemical component</td>

</tr>

<tr>

<td align="center">9</td>

<td align="center">8</td>

<td align="center">64</td>

<td align="center">3</td>

<td>Four projects: Humanities - Westward expansion; Sciences - related to Westward expansion; English - poetry; Language - national anthem</td>

</tr>

<tr>

<td align="center">10</td>

<td align="center">7</td>

<td align="center">67</td>

<td align="center">3</td>

<td>Humanities - traditions of another country</td>

</tr>

</tbody>

</table>

The instructional context centred on guided inquiry, that is, planned, closely supervised, and targeted interventions that enable students to seek and use diverse, often conflicting, sources of information. Kuhlthau's _information search process_ ([2004](#kuh)) framed the instructional design. Students were given opportunity to explore topics and build background knowledge, formulate a specific topic focus, select relevant sources and gather pertinent ideas from them, discover new ideas, and present their new knowledge. Approaches to information seeking were not prescribed, and students were free and encouraged to seek and engage with a diverse range of sources. Source logs recorded by students showed considerable diversity of format, text type, content and relevance to task.

## Method

Data were collected through three survey instruments. These were initially developed, pilot-tested and refined with forty-three mixed-ability Grade 9 students at an independent school in New Jersey. For the purposes of measuring changes in knowledge, the data collection instruments were used at three stages of the inquiry units in each school:

1.  Writing Task 1 (WT1) (at initiation of inquiry unit)
2.  Writing Task 2 (WT2) (at formulation stage of inquiry unit)
3.  Writing Task 3 (WT3) (at conclusion of curriculum unit)

Writing Tasks 1 and 2 consisted of the following questions:

1.  Write the title that best describes your research project at this time.
2.  Take some time to think about your research topic. Now write down what you know about this topic.
3.  What interests you about this topic?
4.  How much do you know about this topic? Check (√) one box that best matches how much you know: Nothing, Not much, Some, Quite a bit and A great deal.
5.  Write down what you think is easy about researching your topic.
6.  Write down what you think is difficult about researching your topic.
7.  Write down how you are feeling now about your project. Check (√) only the boxes that apply to you: Confident, Disappointed, Relieved, Frustrated, Confused, Optimistic, Uncertain, Satisfied, Anxious or Other.

Additional questions at Writing Task 3 were:

1.  What did you learn in doing this research project? (This might be about your topic, or new things you can do, or learn about yourself)
2.  How did the school librarian help you?
3.  How did the teacher help you?

## Operationalizations of knowledge, and findings

### Substance and amount of knowledge.

The substance (content) of knowledge was measured through examining the nature of the students' relational statements about their topics at each of the three stages of data collection. Students were required to do this from memory, without referenece to any sources or notes. The output of this free writing protocol was regarded as a working representation of their knowledge. The number of statements was also counted. A classification of statements developed by Graesser & Clark ([1985](#gra)) was employed. This classification describes content regardless of discipline and statement accuracy. It provided a framework for comparing subsequent representations and being able to establish any changes to them. The coding framework derived from Graesser & Clark is shown below with examples:

**1\. Statements that focus on facts:** These were:  
**_Properties:_** statements describing characteristics _The colour of Valentine's day is red; Michelangelo was one of the greatest artists._  
**_Manner:_** statements describing processes, styles, actions _People drive aggressively in USA; It explodes when sparked by heat._  
**_Set membership:_** statements about class inclusion: _Michelangelo created works such as statue of David, Cistine [sic] Chapel and the famous Pieta._

**2\. Statements that focus on explanations and results:** These were:  
**_Reason:_** statements of explanations of how and why: _The wall was constructed to block invaders; This was because of the air pollution._  
**_Outcome:_** statements providing end result: _(People eat too much) As a result, people got very sick._  
**_Causality:_** statements showing some event causally leads to another: _Acid rain causes many fish to die._

**3\. Synthesis:** statements showing predictive relations, inference, implied meaning or statements presenting personal position, viewpoint or value judgment. These were statements that went beyond explaining and stating of outcomes, rather, they took statements to another level, including formulating conclusion, expressing opinions, positions, implications, reflections and evaluations, and value judgments. _That's not right; I do not believe that; I now realize that it is not as easy as you think living in a castle._

Table 2 shows the total number of statements by type recorded at each stage of data collection.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Total number of statements by type**</caption>

<tbody>

<tr>

<td> </td>

<th colspan="3">Number of statements</th>

</tr>

<tr>

<th>Statement Type</th>

<th>WT1 N</th>

<th>WT2 N</th>

<th>WT3 N</th>

</tr>

<tr>

<th>Facts</th>

<th colspan="3"> </th>

</tr>

<tr>

<td>Properties</td>

<td align="center">1194</td>

<td align="center">1820</td>

<td align="center">2160</td>

</tr>

<tr>

<td>Manner</td>

<td align="center">589</td>

<td align="center">1190</td>

<td align="center">1514</td>

</tr>

<tr>

<td>Set membership</td>

<td align="center">143</td>

<td align="center">218</td>

<td align="center">343</td>

</tr>

<tr>

<th>Explanation and result</th>

<td colspan="3"> </td>

</tr>

<tr>

<td>Reason</td>

<td align="center">152</td>

<td align="center">163</td>

<td align="center">193</td>

</tr>

<tr>

<td>Outcome</td>

<td align="center">35</td>

<td align="center">58</td>

<td align="center">89</td>

</tr>

<tr>

<td>Causality or consequence</td>

<td align="center">48</td>

<td align="center">105</td>

<td align="center">116</td>

</tr>

<tr>

<th>Synthesis</th>

<td align="center">29</td>

<td align="center">70</td>

<td align="center">82</td>

</tr>

</tbody>

</table>

The number of statements recorded at each stage showed that the students' knowledge of their topics progressively increased. There were significant differences between the number of property statements between:

*   WT1 and WT2 _t_ (220) = 6.29, _p_ = 0.0000;
*   WT2 and WT 3 _t_ (222) = 4.26, _p_ <= 0.00); and
*   WT1 and WT3 _t_ (220) = 9.27, _p_ = 0.000).

There was a significant difference between the number of manner statements that the students recorded between:

*   WT1 and WT2 _t_ (106) = 4.69, _p_ = 0.00),
*   WT2 and WT3 _t_ (106) = 2.28, _p_ <= 0.03), and
*   WT1 & WT3 _t_ (142) = 5.32, _p_ = 0.000.

There were no significant differences related to other statement types.

Table 3 shows the considerable variation in the number of statements recoded by each student at each data collection stage:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Mean, standard deviation and range of statements**</caption>

<tbody>

<tr>

<th>Statements</th>

<th>WT1</th>

<th>WT2</th>

<th>WT3</th>

</tr>

<tr>

<th>Facts</th>

<th colspan="3">Mean (Standard deviation) and Range</th>

</tr>

<tr>

<td>Properties</td>

<td align="center">3.87 (3.12)  
0-19</td>

<td align="center">5.74 (4.57)  
1-31</td>

<td align="center">7.38 (5.85)  
1-33</td>

</tr>

<tr>

<td>Manner</td>

<td align="center">3.40 (2.66)  
1-18</td>

<td align="center">4.73 (4.12)  
</td>

<td align="center">6.62 (5.45)  
1-30</td>

</tr>

<tr>

<td>Set membership</td>

<td align="center">2.76 (1.86)  
1-9</td>

<td align="center">3.15 (2.28)  
1-11</td>

<td align="center">3.49 (2.61)  
1-13</td>

</tr>

<tr>

<th>Explanation and result</th>

<td colspan="3"> </td>

</tr>

<tr>

<td>Reason</td>

<td align="center">2.17 (1.32)  
1-8</td>

<td align="center">1.70 (1.11)  
1-7</td>

<td align="center">2.25 (1.65)  
1-9</td>

</tr>

<tr>

<td>Outcome</td>

<td align="center">1.79 (1.23)  
1-5</td>

<td align="center">1.80 (1.54)  
1-9</td>

<td align="center">1.71 (0.93)  
1-4</td>

</tr>

<tr>

<td>Causality or consequence</td>

<td align="center">2.14 (1.36)  
1-6</td>

<td align="center">2.13 (1.88)  
1-11</td>

<td align="center">1.79 (1.23)  
1-7</td>

</tr>

<tr>

<th>Synthesis</th>

<td align="center">2.00 (1.71)  
1-7</td>

<td align="center">1.68 (2.16)  
1-13</td>

<td align="center">1.27 (0.52)  
1-3</td>

</tr>

</tbody>

</table>

The data showed that the students represented their knowledge of the topic predominantly by factual property and manner statements, and that they used increasingly more factual statements to represent their knowledge throughout the three stages. Overall the number of reason and outcome statements was lower than property and manner statements, and did not substantially increase as students progressed, while the cause or consequence and synthesis statements show a decreasing though non-significant trend. Students appeared to be oriented to gathering facts and knowing a set of facts throughout their inquiry. However, our analysis also showed that some students developed a more abstract level of knowledge through the stages and expressed it in a more condensed way at WT3\. For some students, this reflected their ability to describe their knowledge in fewer statements, and in a more abstract, synthesized way.

Two distinctive patterns in the changes of knowledge were identified. The first pattern was an _additive approach_ to knowledge construction. Knowledge development seemed to be characterized by the progressive addition of facts, and it remained on a descriptive level throughout. The addition of new facts was typically a listing of property statements of a generic, superficial kind. As the students built knowledge, they continued to add property and manner statements, and to a lesser extent, set membership statements. Students typically found more facts at each stage of the research process, and added these to their stockpile of facts, even though these added facts were sorted, organized and grouped to some extent into thematic units by WT3\.

An example is shown in the following student's statements about William Shakespeare:

> WT1: _He is very famous for his plays_  
> WT2: _He married Anne Hathaway. They had 3 children. He wrote 37 plays and 152 sonets._  
> WT3: _He was born on April 23, 1564 in Warichshire, Stratford-upon-Avon, Britain Married at age 18\. He married Anne Hathaway. Had three children; Judith, Hammet and Susana. He was the first boy in the family, had 3 sister and 1 brother, Joan, Margaret, Gilbert sibling. He wrote 37 plays and 152 sonets._

A second pattern, an _integrative approach_ to knowledge construction, also was identified. Here, some students did not see the task as one of just gathering facts at each stage, rather they manipulated these facts in a number of ways: building explanations, synthesizing facts into more abstract groupings and consequently reducing the number of statements in their representations, organizing facts in more coherent ways; reflecting on facts to build positional, predictive conclusion statements. At WT1, their representations were similar to the additive model, with sets of facts statements. However, at WT2, their representations included more facts, as well as explanations. At WT3 they appeared to interpret the found information to establish personal conclusions and reflect on these. In addition, some appeared to subsume sets of facts into more abstract groups. This is illustrated by the following student's statements about gay marriage:

> WT1: _I know that the major in San Fransisco was performing gay marriages._  
> WT2: _I know that right now only a few states are allowing homosexuals to get married. I know Massachusetts was the first state to... 4000 marriages were carried out in San Francisco in February... President Bush strongly against..._ (long reply)  
> WT3: _Homosexuals are being discriminated, being denied their rights._

### Structure of knowledge

We undertook a qualitative analysis of the representations of knowledge to identify if there were any structural changes to students' knowledge. This focused on the nature of the thematic organization and integration of statements into a meaningful structure, if any, and the links between these, based on coherence and structural centrality as identified by Chi (1992). From this analysis, we developed the following typology of structure of knowledge:

> **Ideas are discrete and unrelated.** Students recorded random facts, without any clear sequence and conceptual relatedness. The linkages between these facts were not immediately apparent.  
> **Some interrelationships.** Students showed partial organization of ideas, with some evidence of grouping of like or related ideas together.  
> **Contiguous ideas are associated; set of ideas may be somewhat continuous.** There was evidence of a sequence of connections between statements, some conceptual flow and direction that gave a sense of structure.  
> **Overall, ideas are interrelated and continuous.** There was a clear grouping of ideas, often around some specified theme, although groups were often discreet and unrelated.  
> **Ideas are integrated and unified; there is structural centrality.** This was similar to the above, but with some overall notion of unification.

We identified a change towards more structured replies from WT1 to WT3, but this was not consistent. Typically the change was from unstructured and random listing of facts (properties, manner and set member lists) towards some level of structural centrality and conceptual coherence, where endpoint representations showed organization of facts into thematic groupings, with a smaller number of students linking the thematic groupings into larger more coherent and more conceptual units.

For some students, particularly those showing an additive model, WT1 showed more structure than WT2\. In other words, initial knowledge representation had a higher level of structure than those at WT2\. This is consistent with the pervasive viewpoint that people's existing knowledge is structured. However, as students built background knowledge, the gathering of facts rather than sequencing and organizing these facts seemed to take over, as evident in WT2 representations being less structured. By WT3, some organization of statements into conceptually coherent and linked groups had taken place. We also found a higher level of structure at WT1 for those students who indicated from the start that they knew something about their topic.

### Labelling (title) of knowledge

In asking students to state the title of their topic at each of the three stages, we assumed that there was a relationship between title and substance of knowledge: as content changed, title would also change. We also assumed that a general title maintained during the stages showed that students were unable to form a focus, while a change to a specific title indicated ability to formulate a topic focus and build a more specific knowledge structure. Our pilot study had provided some evidence of this. Our initial examination identified three kinds of titles, which formed the basis for tracing the changes:

> 1\. **General title:** A title that describes the project on a general, overall level. For example: _Ancient Rome; pollution._  
> 2\. **Specific title:** The title brings forward a specific aspect of the project. For example: _Ancient Roman Government; air pollution; westward expansion_.  
> 3\. **The title is expressed in a creative, or artistic way:** For example: _PCP, not just a drug or PCP, deadly, yet not gone; Is your hair really getting clean?_

The following patterns were identified as we charted the titles from WT1 to WT3\. Counts are shown in Table 4:

> 1. **The title remains the same** (and typically general) **throughout**. For example: _Ancient Greece → Ancient Greece → Ancient Greece._  
> 2. **Hourglass phenomenon**, → where a title is general in WT1 and WT3, but specific in WT2\. For example: _Health in Ancient Greece → medicine in ancient Greece → Health in Ancient Greece_.  
> 3. **Increasing specificity**: For example _Ancient Greece → Fashion in Ancient Greece → Daily clothing of Ancient Greece._  
> 4. **General to specific to artistic/creative**: For example: _perfume and cologne → chemistry behind the scent → chemistry stinks, the chemistry behind perfumes and other scents._  
> 5. **Associative changes**: (small numbers) _Roman military → Roman fashion → Food in Rome_  
> 6. **General to general to artistic**: (small numbers) _The culture of the English people → The culture of the English people → The mother of America but still so different._

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Distribution of title development**</caption>

<tbody>

<tr>

<th>Title Development</th>

<th>Number of Students</th>

<th>Percentage</th>

</tr>

<tr>

<td>Stays same throughout the writing tasks</td>

<td align="center">243</td>

<td align="center">60</td>

</tr>

<tr>

<td>More specific throughout the writing tasks</td>

<td align="center">81</td>

<td align="center">20</td>

</tr>

<tr>

<td>Hourglass phenomenon.</td>

<td align="center">20</td>

<td align="center">5</td>

</tr>

<tr>

<td>The title is expressed in a creative, artistic way</td>

<td align="center">28</td>

<td align="center">7</td>

</tr>

<tr>

<td>Associative</td>

<td align="center">17</td>

<td align="center">4</td>

</tr>

<tr>

<td>General to General to Artistic</td>

<td align="center">15</td>

<td align="center">4</td>

</tr>

<tr>

<td>Total students</td>

<td align="center">404</td>

<td align="center">100</td>

</tr>

</tbody>

</table>

Table 4 shows that 404 students provided a complete sequence of titles at the three stages, and 60% of these titles remained the same, even though their representations became more specific as they progressed. 20% of students used a more specific title over the stages. 7% of students used artistic titles in WT3\. The use of general titles throughout seems to indicate that students replied to the research question by stating their 'official' project title instead of expressing their personal conceptualization of it. This was particularly evident in one school, where students had researched a variety of individual and specific topics, yet used the more generic curriculum theme to label their topic. While some students had lengthy titles, 90% of titles were very short, and students may have also perceived that a title is too short a mechanism for them to elaborate any particular aspect of the topic.

The titles were qualitatively analyzed and compared to the additive and integrative patterns. This revealed no clear patterns, although some students whose titles seemed to develop to a more specific expression (or an hourglass expression) over time had synthesized replies in WT3\. We noted also that students with an artistic expression at WT3 claimed that, 'I know a lot about the topic', and seemed to use this knowledge to generate creative titles. Overall, title was not a strong or clear measure of students' change of knowledge, and we acknowledge that this may have been an artifact of the research design, and worthy of further exploration.

### Estimate of knowledge

We asked the students to estimate how much they knew about their topic at each stage through a five-point scale, and these estimates are shown in Table 5.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Personal estimate of knowledge**</caption>

<tbody>

<tr>

<th>How much do you know about this topic?</th>

<th>WT 1  
n</th>

<th>WT 2  
n</th>

<th>WT 3  
n</th>

</tr>

<tr>

<td>Nothing</td>

<td align="center">35 (10 %)</td>

<td align="center">6 (2%)</td>

<td align="center">1 (0.3 %)</td>

</tr>

<tr>

<td>Not Much</td>

<td align="center">142 (40%)</td>

<td align="center">14 (4 %)</td>

<td align="center">6 (2 %)</td>

</tr>

<tr>

<td>Some</td>

<td align="center">125 (35 %)</td>

<td align="center">111 (33 %)</td>

<td align="center">29 (9 %)</td>

</tr>

<tr>

<td>Quite a bit</td>

<td align="center">41 (12 %)</td>

<td align="center">156 (47 %)</td>

<td align="center">148 (48 %)</td>

</tr>

<tr>

<td>A great deal</td>

<td align="center">10 (3 %)</td>

<td align="center">47 (14 %)</td>

<td align="center">126 (41 %)</td>

</tr>

<tr>

<td>Knowledge increase (mean)</td>

<td align="center">1.26 (0.91)</td>

<td align="center">2.6 (0.83)</td>

<td align="center">3.2 (0.76)></td>

</tr>

</tbody>

</table>

There was a significant difference between the students' estimates of their knowledge between the stages. The students' perception of their knowledge seemed to particularly increase between the WT1 and WT2 (t = 21, p < 0.000). The qualitative analysis of the statements also showed that the students believed that their knowledge of their topic grew as they progressed through their inquiry. The majority of students expressed this in terms of knowing 'heaps' or 'lots' more.

## Conclusions and discussion

Did the students' knowledge change when they undertook this library-based inquiry unit? Students came to know more about their topics, as shown in the number of statements they recalled. They also perceived that they knew more as they progressed through the task, as identified in the estimate of knowledge measure. The students seemed however to be oriented to gathering facts and knowing a set of facts throughout the search process, and for many students, the process of knowledge construction was one of finding facts and adding facts to their knowledge base. This finding of an additive approach of knowledge change supports Limberg's _information seeking as fact-finding_. For many students in this study, the substance of their knowledge focused on an accumulation of factual property and manner statements, and they used increasingly more factual statements to represent their knowledge throughout the three stages.

The study also identified a group of students who appeared to engage more analytically, conceptually and reflectively in information use, and who showed an integrative approach to knowledge construction. These students first gathered facts to form an information foundation for manipulating and transforming them in more synthesized and abstract ways and developing knowledge that showed higher levels of coherence and centrality. This phenomenon appears to equate with Chi's finding ([1992](#chi)) of the development of expert knowledge structures which show more local and global coherence and abstraction. It also reflects Limberg's _information seeking as scrutinizing and analysing_, where students engaged in higher-order information processing and discovering relations between ideas.

Both additive and integrative approaches of knowledge change and the character of the knowledge outcome raise some critical research questions. What constitutes deep knowledge and deep understanding of a topic? Is the development of additive and integrative approaches different, yet comparable, patterns of knowledge development, or is it that the students who remain at the level of additive have stagnated in their learning process? Given the focus on standards based education, which knowledge pattern represents a higher level of intellectual quality? These are questions that continue to challenge the school education arena.

While this research did not track nor examine the specific dimensions of the learning environments in each of the schools, the students' comments point to the influence of contextual elements shaping the knowledge building process and its outcome. These include the nature of the task; scope of choice within the curriculum area; the nature of instructional interventions provided to guide students in information seeking and use; the instructional emphasis given to, and balance of, information gathering and knowledge building activities; a focus on the affective and motivational aspects of knowledge construction, as well as the cognitive dimensions. These resonate with Limberg (2005) notion of the school's discursive practice shaping library research beyond 'transport' to 'transformation' of text. Exploring these variations and how these facilitated the incorporation of different statement types is a fruitful line of further research.

The findings also point to the importance of Kuhlthau's Focus-Formulation stage of the _information search process_, and the need for carefully chosen instructional interventions to enable students to develop knowledge in rich and complex ways. At the heart of this instructional intervention is what Gore_et al._ ([2002](#gor)) call productive pedagogy, which focuses on the development of higher order thinking, depth of knowledge, depth of understanding, ability to engage in substantive conversation, ability to recognize knowledge as problematic, and reading literacy grounded in language, grammar, and technical vocabulary. This calls for a fundamental shift in instruction, moving beyond helping students find information to helping them engage with it in critical ways to build a deep level of knowledge as opposed to a superficial one.

## Acknowledgements

This project acknowledges the financial support of the Institute for Museum and Library Services, and the contributions of Dr Carol Kuhlthau, Dr Jannica Heinström (Research Associate with CISSL), and Nora Bird (Doctoral Student at Rutgers University).

## References

*   <a id="aus" name="aus"></a>Ausubel, D., Novak, J., & Hanesian, H. (1978). _Educational psychology: a cognitive view._ (2nd Ed.). New York, NY: Holt, Rinehart & Winston.
*   <a id="bro" name="bro"></a>Brookes, B. (1980). The foundations of information science. Part 1\. Philosophical aspects. _Journal of Information Science_, **2**(3/4), 125-133.
*   <a id="chi" name="chi"></a>Chi, M. (1992). Conceptual change within and across ontological categories: Examples from learning and discovery in science. In R. Giere (Ed.), _Cognitive models of science: minnesota studies in the philosophy of science_, (pp. 129-186). Minneapolis, MN: University of Minnesota Press
*   <a id="coo" name="coo"></a>Cooke, N. J. (1999). Knowledge elicitation. In. F.T. Durso, (Ed.), _Handbook of applied cognition_ (pp. 479-509). Chichester, UK: Wiley.
*   <a id="dew" name="dew"></a>Dewey, J. (1933). _How we think. a restatement of the relation of reflective thinking to the educative process_ (Rev. ed.), Boston, MA: D. C. Heath.
*   <a id="dol" name="dol"></a>Dole, J., & Sinatra, G. (1998). Reconceptualizing change in the cognitive construction of knowledge. _Educational Psychologist_, **33**(2/3), 109-128.
*   <a id="gob" name="gob"></a>Gobbo, C. & Chi, M. (1986). How knowledge is structured and used by expert and novice children. _Cognitive Development_, **1**(3), 221-237.
*   <a id="gor02" name="gor02"></a>Gore, J., Griffiths, T., & Ladwig, J. (2002). _[Productive pedagogy as a framework for teacher education: towards better teaching](http://www.aare.edu.au/01pap/gor01501.htm) _. Callaghan, NSW: University of Newcastle, Faculty of Education. Retrieved 4 June, 2006 from http://www.aare.edu.au/01pap/gor01501.htm
*   <a id="gor92" name="gor92"></a>Gordon, S. (1992). Implications of cognitive theory for knowledge acquisition. In: R. Hoffman (ed.) _The psychology of expertise: cognitive research and experimental AI_ (pp 99-120). New York, NY: Springer-Verlag.
*   <a id="gra" name="gra"></a>Graesser, A. & Clark, L. (1985). _Structures and procedures of implicit knowledge_. Norwood, NJ: Ablex.
*   <a id="kel" name="kel"></a>Kelly, G.A. (1963). _A theory of personality: the psychology of personal constructs_. New York, NY: W.W. Norton.
*   <a id="kuh" name="kuh"></a>Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services_. (2nd ed.). Westport, CT: Libraries Unlimited.
*   <a id="lan" name="lan"></a>Lance, K.C. (2001). _Proof of the power: recent research on the impact of school library media programmes on the academic achievement of U.S. public school students_. ERIC Digest. EDO-IR-2001-05 October 2001, Syracuse, N.Y.: ERIC Clearinghouse on Information & Technology.
*   <a id="lim99" name="lim99"></a>Limberg, L. (1999). [Experiencing information seeking and learning: a study of the interaction between two phenomenon.](http://informationr.net/ir/5-1/paper68.html) _Information Research_, **5**(1). Retrieved 4 June, 2006 from http://informationr.net/ir/5-1/paper68.html
*   <a id="lim05" name="lim05"></a>Limberg, L. (2005). _Patterns of variation in information seeking and learning_. Paper presented at the International Research Symposium, centre for International Scholarship at Rutgers University, New York, April 29, 2005.
*   <a id="mar" name="mar"></a>Markman, A. (2002) Knowledge representation. In: H. Pashler & D. Medin, (Eds.) _Stevens' handbook of experimental psychology_ (pp 165-208). 3rd Ed. Volume 2: memory and cognitive processes. New York, NY: John Wiley & Sons.
*   <a id="pen" name="pen"></a>Pennanen, M. & Vakkari, P. (2003). Students' conceptual structure, search process, and outcome while preparing a research proposal: a longitudinal case study. _Journal of the American Society for Information Science_, **54**(8), 759-770\.
*   <a id="pol" name="pol"></a>Polk, T. A. & Seifert, C. M. (2002). _Cognitive modeling_. Cambridge, MA: The MIT Press.
*   <a id="spi" name="spi"></a>Spink, A. & Cole, C. (2006). Human information behavior: integrating diverse approaches and information use. _Journal of the American Society for Information Science and Technology_, **57**(1), 25-35.
*   <a id="tal" name="tal"></a>Talja, S (1997). _Constituting 'information' and 'user' as research objects. A theory of knowledge formations as an alternative to the information man theory_ In: Vakkari, P., Savolainen, R. & Dervin, B. (eds.) _Information Seeking in Context_. (pp. 67-80). London: Taylor Graham.
*   <a id="tod99a" name="tod99a"></a>Todd, R. (1999a). Back to our beginnings: information utilization, Bertram Brookes and the Fundamental Equation of Information Science. _Information Processing & Management_, **35**(6), 851-870.
*   <a id="tod99b" name="tod99b"></a>Todd, R. (1999b). Utilization of heroin information by adolescent girls in Australia: a cognitive analysis. _Journal of the American Society for Information Science_, **50**(1), 10-23\.
*   <a id="tod05" name="tod05"></a>Todd, R. & Kuhlthau, C. (2005). Student learning through Ohio school libraries, Part 1: How effective school libraries help students. _School Libraries Worldwide_, **11**(1), 63-88.
*   <a id="vak" name="vak"></a>Vakkari, P. (1997). _Information seeking in context: a challenging metatheory_. In P. Vakkari, R. Savolainen & B. Dervin. (Eds). Information Seeking in Context. (pp.81-96). London: Taylor-Graham.
*   <a id="wil" name="wil"></a>Wilson, T.D. (2000). Human information behavior. _Informing Science_, **3**(2), 49-56.

