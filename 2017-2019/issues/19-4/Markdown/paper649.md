<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# Reaching health service managers with research

#### [Jacqueline MacDonald](#authors)  
Annapolis Valley Health, South Shore Health, South West Health, Nova Scotia, Canada  
#### [Peter A. Bath](#authors)  
Centre for Health Information Management Research (CHIMR), Information School, University of Sheffield, Sheffield, UK  
#### [Andrew Booth](#authors)  
School of Health and Related Research (ScHARR) University of Sheffield, Sheffield, UK

#### Abstract

> **Introduction.** The aim of this paper is to identify and characterize the routes by which research may reach health service managers to influence their critical decisions.  
> **Method.** This research used two series of qualitative interviews, documentary analysis (a calendar study) and a card-sorting exercise to explore the workplace information practices of thirty-six health service managers.  
> **Analysis.** Both interview studies used the cross-case analysis. The second interview study also used within-case analysis in the form of information transaction mapping. Information transactions, calendar study and card-sorting exercise data were reported quantitatively.  
> **Results.** This exploratory study found that these health service managers overcame short time lines, unclear processes and simultaneous conflicting priorities by bringing together groups accustomed to sharing information orally to inform their critical decisions. Each decision was informed by different categories and types of information. Group decisions allowed information from multiple sources, including research, to be integrated until there was _just enough_ for a comfortable decision. Research reached these managers through one or more of at least eight different routes.  
> **Conclusions.** Professional standards, conferences, structurally positioned gatekeepers (knowledgeable co-workers in positions related to the critical decision subject) and other information sources that appraise and filter research then blend it with local context are preferred over research papers that must be searched, read, appraised and then integrated with other information types. Eight routes by which research reaches health services managers are compared with seven elements in a research-to-policy conceptual framework.

<section>

## Introduction

This paper presents findings from a three-part research project conducted between 2005 and 2009 in a publicly funded District Health Authority in Nova Scotia, Canada. The research developed in response to a district committee's information need; the committee, working to establish population health as a routine consideration in decision-making, wanted to know what sources managers use to inform decisions, their decision-making processes and the optimal point in decision-making at which it would a be best to introduce population-health considerations ([MacDonald, Bath and Booth, 2008a](#MacDonaldBB2008a); [MacDonald, Bath and Booth, 2008b](#MacDonaldBB2008b); [MacDonald, Bath and Booth, 2011](#MacDonaldBB2011)).

The first and third parts of this research were exploratory interview studies that examined the information that health-service managers used to support critical decisions unrelated to individual patient care, such as decisions involved in developing practice guidelines, complying with patient safety standards and planning chronic-disease prevention strategies. When interview analysis suggested that participants did not typically look for primary research to inform their critical decisions, the information they did use was examined to determine content and source.

## Research on health service managers

Health services have been described as the most complex organizations to manage ([Glouberman and Mintzberg, 2001](#Glouberman2001)) and health service managers have been seen as low users of health research ([Burns, 2005](#Burns2005)). Much of the literature on health service managers has focused on their use of research specifically rather than their use of information generally ([Gray and Ison, 2009](#Gray2009); [Innvaer, Vist, Trommald and Oxman, 2002](#Innvaer2002); [Innvaer, 2009](#Innvaer2009); [Lavis, Huw, Oxman, Golden-Biddle and Ferlie, 2005](#Lavis2005)) or on why they do not use information as they should ([Kadane, 2005](#Kadane2005); [Willis, Mitton, Gordon and Best, 2012](#Willis2012)).

Although only eleven studies were identified that explored types of information that health-service managers use to inform their decisions, their decision processes, or how they acquired the information they used, these share overlapping results and conclusions.

A U.K. study conducted between 2000 and 2003 ([Weatherly, Drummond and Smith, 2002](#Weatherly2002)) used a mailed questionnaire (N=68), semi-structured telephone interviews (N=10) and documentary analysis (N=25) to explore the use of evidence to inform local health improvement policies. Most participants were medically trained and involved in public health policy development. The researchers concluded that a mix of internal (experiential) and external (empirical) evidence was used to inform policy. Preferred sources of empirical research were government reports and National Institute for Clinical Excellence (NICE) guidelines over primary research papers. Barriers to use of primary research included lack of time, lack of resources to gather and appraise available relevant research and difficulties synthesizing information to inform local policies. Seven ways to increase research-based content in local health policies were suggested. These included strengthening and disseminating the evidence base of national guidance, setting quantifiable targets for health improvement and the role of evidence in priority and target setting, educating policy developers in evidence based practice and defining and adequately resourcing the local role in obtaining evidence to inform policy.

Seven workplace studies of health service managers' workplace information and decision- making behaviour identified similar challenges. Five of these were of workplace information access and use, each conducted in a different country and with a slightly different focus ([Head, 1996](#Head1996); [Kovner and Rundall, 2006](#Kovner2006); [Mbananga and Sekokotla, 2002](#Mbananga2002); [Moahi, 2000](#Moahi2000); [Nied&zacute;wiedzka, 2003](#Nieds2003)). Despite differences in the wealth of the country (G8 or not), degree of computerization (desktop access to databases and the internet, or not), single hospital or multi-site health service, health service funding (whether public or private), health service managers were similar in that they did not describe looking for research papers to support their decisions. The remaining two workplace studies observed that much of health service managers' work time was spent in meetings ([Moss, 2000](#Moss2000); [Arman, Dellve, Wikström and Törnström, 2009](#Arman2009); [Tengelin, Arman, Wikström and Dellve, 2011](#Tengelin2011)) where they often shared information to support decisions.

A mixed-methods study of 116 Australian health administrators' workplace decision-making practices ([Baghbanian, Hughes, Kebriaei and Khavarpour, 2012](#Baghbanian2012)) used interviews and surveys to explore resource allocation decision situations. The authors concluded that policy makers were "enlightened by" research that reached them indirectly. Managers more frequently made decisions by involving others with knowledge of the situation than by following formal procedures and reading primary research or systematic reviews. Their decisions were characterized by ambiguity and complexity, short deadlines, incomplete information and significant unknowns.

Two studies of U.K. National Health Service managers had comparable findings. [Clarke _et al._](#Clarke2013) (2013) used print questionnaires administered in face-to-face meetings or a web form to ask NHS managers, Band 7 or higher (N=345) to rate the importance of different information sources in a recent decision. [Edwards _et al._](#Edwards2013) (2013) used case study interviews (n=54) and surveys (n=2092) in a two phase study that examined the information behaviour of health service managers in decision-making. Managers were overwhelmed with too much information of various types and quality, yet often could not find the information they needed. They tended to use personal experience and "seeing what works" over academic or formal sources.

In considering that decision makers who use social science research to inform policy do not always search specifically for it, or receive it in its pure form, [Weiss (1979)](#Weiss1979) identified seven models (Table 1) from the literature on social science research use in policy development. These models have also been considered with respect to use of health research ([Black, 2001](#Black2001); [Bowen and Zwi, 2005](#Bowen2005); [Brehaut and Juzwishin, 2005](#Brehaut2005); [Dobrow, Goel and Upshur, 2004](#Dobrow2004);[Sheldon, 2005](#Sheldon2005); [Williams and Bryan, 2007](#Williams2007)).

<table><caption>

Table 1: Seven research-to-policy models ([Weis, 1979)](#Weiss1979)</caption>

<tbody>

<tr>

<th>Model</th>

<th>Brief description</th>

</tr>

<tr>

<td>Knowledge-driven model</td>

<td>A linear model that assumes the following sequence of events: Basic research --> applied research --> development --> application. There are more examples of this linear model in the sciences than there are in the social sciences where research is unlikely to affect policy following this sequence.</td>

</tr>

<tr>

<td>Problem-solving model</td>

<td>A linear model that assumes a knowledge gap in a pending decision is bridged through direct application of empirical evidence and conclusions from relevant researchDecision makers must have shared goals and must know about existing relevant research to inform the decision, or be willing to commission research.</td>

</tr>

<tr>

<td>Political model</td>

<td>A linear model for individual decision makers who search specifically for research to supports their own existing firm position. All decision makers must have access to research to support individual positions and make sure results are interpreted correctly and taken in their intended context.</td>

</tr>

<tr>

<td>Tactical model</td>

<td>Decision makers do not search for or consider research but use the fact that research related to an issue has been or is being done as an excuse, response or delaying tactic, regardless of the results or their relevance to the decision.</td>

</tr>

<tr>

<td>Enlightenment model</td>

<td>A non linear model where rather than a single study or series of related studies informing policy, research diffuses circuitously through different channels (journals, media, colleagues) to form a backdrop of ideas to inform policy.This is the most likely route by which social sciences research reaches policy.</td>

</tr>

<tr>

<td>Interactive model</td>

<td>A non linear model where there is an interactive or collaborative search for knowledge where those involved in a policy decision actively seek information from a variety of sources, then pool their knowledge, talents, beliefs to make sense of and inform the problem with research, politics, experience, opinion and other intelligence. A body of convergent knowledge may influence policy more quickly than research can be identified or conducted.</td>

</tr>

<tr>

<td>Research as intellectual enterprise</td>

<td>A non linear model where research is a dependant variable, along with philosophy, journalism, history, law and criticism that influences and is influenced by policy and responds to the fads and fancies of the period. Research may focus to take advantage of funding, then in response to research results, ideas and consequently funding for research might expand.</td>

</tr>

</tbody>

</table>

The remaining literature on health service managers has tended to focus on their use of research specifically rather than their use of information generally [(Gray and Ison, 2009](#Gray2009); [Innvaer, Vist, Trommald and Oxman, 2002](#Innvaer2002); [Innvaer, 2009;](#Innvaer2009) [Lavis, Huw, Oxman, Golden-Biddle and Ferlie, 2005)](#Lavis2005) or on why they do not use it as they should [(Kadane, 2005](#Kadane2005); [Willis, Mitton, Gordon and Best, 2012)](#Willis2012).

This paper examines the extent to which health service managers acquired research to inform their critical decisions and identifies the routes by which research reached them. This will be of interest to researchers and research funders concerned with translating research for implementation in practice as well as to those designing information services for health service managers.

## Method

### Research design

The original aims of this research were to investigate information behaviour among health service managers and to identify whether any information seeking models represented their behaviour. These reflected an assumption that health service managers' dominant information behaviour was information seeking. However, results of the first interview study failed to provide an in-depth understanding of participants' information behaviour but suggested that information sharing was more important to participants than information seeking. At this stage and at each step in the research that followed, as the complexity of participants' information behaviour became more obvious, it became necessary to stop and refocus, first try to take a broad overview of results and then try to drill down in one or more areas to find out exactly what was happening. Consequently, changes to research design were ongoing over the course of this study. The research design/redesign process is explained in Appendix A.

This study began with three main research questions:

> 1\. What are the information needs of health service managers and how do they use information?  
> 2\. What are the information behaviours of health service managers?  
> 3\. What are the barriers and challenges to health service managers in obtaining the information they need?

This paper explores an additional question that arose from the first interview study.

> 4\. If these health service managers informed decisions with research, how did they acquire it?

### Participants

Thirty six health service managers were interviewed in two exploratory critical incident interview studies. Data were also gathered through documentary analysis and a card-sorting exercise (Author's PhD thesis, 2011). Ethics approval was obtained through the District's Health Research Ethics Committee.

Study participants were chosen based on their portfolio, whether Acute Care, Community Health, Operations, or _Administration_ (a convenience label used in this study to represent Senior Executive, Human Resources, Finance and the Corporate Office staff), and organization chart level, whether Senior Executive, Directors, Managers, or "Junior Leaders" (a second convenience label used to represent leaders charged with responsibility for a programme or service but without staff or budgets).

### Interviews

Interview study participants were asked to describe an important recent decision they made to address a situation that they had not experienced before. Nineteen semi-structured interviews were initially conducted in 2005-2006 (called the first interview study), an exploration of the information sharing behaviour of health service managers as they informed critical decisions made over varying time periods. A further seventeen participants were then interviewed in 2008 (called the second interview study), an in-depth exploration of group information sharing behaviour to inform critical decisions made in a single meeting. The majority of participants in both interview studies were hybrid managers, clinicians who after some years of experience in their professions became managers (Head, 1996). No participant was interviewed in both studies.

Both interview studies used a multiple case study approach ([Yin, 2002](#Yin2002)) with critical incident questions ([Flanagan, 1954](#Flanagan1954)) and general exploratory questions about information behaviour. All interviews were audio recorded and transcribed verbatim, then indexed categorically using ATLAS.ti™ software. A demographic questionnaire was used to gather participant characteristics.

### Calendar study

Documentary analysis, in the form of a study of an electronic meeting room calendar ([MacDonald, Bath and Booth, 2014](#MacDonaldBB2014)), was conducted to quantify the extent to which health service managers scheduled large group information sharing opportunities. The results were used to focus the direction of the second interview study.

### Card-sorting exercise

The second interview study participants completed a card-sorting exercise ([Rugg and McGeorge, 2005](#Rugg2005); [Arthur and Nazroo, 2003](#Arthur2003)) designed to examine the extent to which perceptions on the value of different types of information varied by portfolio, the number of health services career years and their position level. Participants were asked to rate sixty types of information and knowledge (Figure 3, Appendix B) as "need to know", "nice to know" or "not essential" with respect to their critical incident. These types were either mentioned by first interview study participants (N=53) or not mentioned, but frequently provided to managers by their library service (N=7).

### Analysis

Framework analysis, a matrix-based content analysis technique developed by the U.K. National Centre for Social Research was used for qualitative between-case analysis in the First Interview study. Intended for applied social policy research, the NatCen FrameWork builds on cross-case synthesis to create a five step systematic approach to qualitative data analysis that is grounded in the original data of the research study ([Ritchie, _et al._, 2003](#Ritchie2003)).

To identify the information used to inform group decisions and the information behaviour associated with each piece of information, the second interview study used information transaction mapping, an approach to quantitative within-case analysis developed for this study. Thematic frameworks were developed from the literature or developed during data analysis and reporting. Decisions were examined within a conceptual framework developed from research related to decision complexity. The framework included decision levels ([Heller, Drenth, Koopman and Rus, 1988](#Heller1988)), decision modes ([Choo and Johnston, 2003](#Choo2003); [Simon, 1960](#Simon1960); [Cohen, March and Olsen, 1972](#Cohen1972); [Mintzberg _et al._,1976](#Mintzberg1976); [Allison and Zelikow, 1999](#Allison1999); [Lipshitz and Strauss, 1997](#Lipshitz1997)), decision types ([Canadian Health Services Research Foundation, 1998](#Canadian1998)), decision structure ([Simon, 1960](#Simon1960)) and decision situations ([Mintzberg, Raisinghani and Theoret, 1976](#Mintzberg1976)). Information that participants said influenced their decisions was indexed by location of origin, whether internal or external, by form, whether oral or recorded, and by category, whether explicit, tacit or cultural ([Choo, 2006](#Choo2006); [Polanyi, 1966](#Polanyi1966)), and then within categories, by subcategories and types of information and knowledge. Bookings for the year 2007 for the District's twenty large meeting rooms were extracted from the District's Outlook Calendar and examined for activity, location, day of week, time of day, meeting duration, meeting recurrence and group membership, whether drawn from one or more departments, programmes or portfolios. The calendar study did not address unscheduled meetings or scheduled meetings held in managers' offices, departmental meeting rooms, by phone or by videoconferencing ([MacDonald, Bath and Booth, 2014](#MacDonaldBB2014)). Card-sorting exercise data were reported as simple counts, with comparisons made between the information that participants from the second interview study said they actually did use and ratings assigned in the card-sorting exercise.

## Results

### Participant characteristics

All thirty-six participants had been educated beyond the secondary level, seventeen had post-graduate degrees and twelve had undergraduate degrees. Over half of the participants had worked in health services for over twenty-five years. Six participants were under forty-five years old while thirty were forty-five or over. The mean participant age was fifty years old and the mean length of healthcare careers was fourteen years. The breakdown of participants according to portfolio and managerial level is shown in Table 2.

<table><caption>Table 2: Study participants by portfolio and managerial level</caption>

<tbody>

<tr>

<th>Variable</th>

<th>Category</th>

<th>First interview  
study</th>

<th>Second interview  
study</th>

<th>Total</th>

</tr>

<tr>

<td rowspan="7">Organizational level</td>

<td>Senior executive</td>

<td>2</td>

<td>2</td>

<td>4</td>

</tr>

<tr>

<td>Director</td>

<td>7</td>

<td>3</td>

<td>10</td>

</tr>

<tr>

<td>Manager</td>

<td>5</td>

<td>4</td>

<td>9</td>

</tr>

<tr>

<td>Junior leader</td>

<td>3</td>

<td>8</td>

<td>11</td>

</tr>

<tr>

<td>Board chair</td>

<td>1</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Board member</td>

<td>1</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Total</td>

<td>19</td>

<td>17</td>

<td>36</td>

</tr>

<tr>

<td rowspan="6">Portfolio</td>

<td>Acute care</td>

<td>4</td>

<td>3</td>

<td>7</td>

</tr>

<tr>

<td>Administration</td>

<td>2</td>

<td>4</td>

<td>6</td>

</tr>

<tr>

<td>Community health</td>

<td>5</td>

<td>8</td>

<td>13</td>

</tr>

<tr>

<td>Operations</td>

<td>6</td>

<td>2</td>

<td>8</td>

</tr>

<tr>

<td>Board</td>

<td>2</td>

<td> </td>

<td>2</td>

</tr>

<tr>

<td>Total</td>

<td>19</td>

<td>17</td>

<td>36</td>

</tr>

</tbody>

</table>

Participants described work environments in which they faced multiple conflicting priorities, imposed short deadlines, unclear processes and group decisions informed by information shared orally. Their mode of decision-making conformed most closely to naturalistic decision-making ([Lipshitz and Strauss, 1997](#Lipshitz1997)).

### Overview of critical decision situations and participants' roles

Participants chose to discuss critical incidents that involved limited resources. Limited financial resources and limited time were symptoms of all incidents discussed and most involved some aspect of human resource use. Limited physical resources concerned eight participants whose critical incidents involved a disruptive and unhealthy workplace odor, a flood, renovation of a corridor serving a busy clinic, shortage of meeting workspace for an annual community event, office space for administrative staff, off site shared office space and requirements to address a fire code violation in an acute care area. Eight participants had information resource related critical incidents. Of these three related to information systems and five to information flow problems within and between work groups. Three incidents involved health policy development, one administrative and two responding to public policy. Six participants described incidents with human resource issues as a primary focus. Of these, five described reorganizing their programmes, departments and services to make better use of limited staff and integrate best practices.

Problem situations were more common than crises (Table 3), with participants typically taking the opportunity to progress beyond simply resolving the immediate issue. Administrative policy decisions ([Canadian Health Services Research Foundation, 1998](#Canadian1998)) were dominant.

<table><caption>Table 3: Critical incident decision characteristics</caption>

<tbody>

<tr>

<th>Decision</th>

<th>First interview  
study</th>

<th>Second interview  
study</th>

<th>Total</th>

</tr>

<tr>

<td>Individual</td>

<td>3</td>

<td>0</td>

<td>3</td>

</tr>

<tr>

<td>Group</td>

<td>16</td>

<td>17</td>

<td>33</td>

</tr>

<tr>

<td>Total</td>

<td>19</td>

<td>17</td>

<td>33</td>

</tr>

<tr>

<th colspan="4">Decision situation</th>

</tr>

<tr>

<td>Crises</td>

<td>3</td>

<td>4</td>

<td>7</td>

</tr>

<tr>

<td>Problem</td>

<td>4</td>

<td>5</td>

<td>9</td>

</tr>

<tr>

<td>Opportunity</td>

<td>0</td>

<td>5</td>

<td>5</td>

</tr>

<tr>

<td>Problem-opportunity</td>

<td>12</td>

<td>3</td>

<td>15</td>

</tr>

<tr>

<td>Total</td>

<td>19</td>

<td>17</td>

<td>36</td>

</tr>

<tr>

<th colspan="4">Decision level</th>

</tr>

<tr>

<td>Strategic</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Tactical</td>

<td>2</td>

<td>3</td>

<td>5</td>

</tr>

<tr>

<td>Operational</td>

<td>8</td>

<td>13</td>

<td>21</td>

</tr>

<tr>

<td>Operational-tactical-strategic</td>

<td>5</td>

<td>1</td>

<td>6</td>

</tr>

<tr>

<td>Operational-tactical</td>

<td>2</td>

<td>0</td>

<td>2</td>

</tr>

<tr>

<td>Tactical-strategic</td>

<td>2</td>

<td>0</td>

<td>2</td>

</tr>

<tr>

<td>Total</td>

<td>19</td>

<td>17</td>

<td>36</td>

</tr>

</tbody>

</table>

How participants acted with respect to their critical incident situation was easily classified by one or more managers' roles as described by [Mintzberg (1973](#Mintzberg1973)), both as individuals in the first interview study ([MacDonald, Bath and Booth, 2008](#MacDonaldBB2008a)) and as members of a group in the second interview study. As shown in Table 4, their activities were heterogeneous, spread throughout Mintzberg's three groups of managerial activities, with only the role of figurehead not represented in the critical incident situations described. The dominant category in both studies was decisional; improver/changer was the most common managerial activity.

<table><caption>Table 4: Participants' roles in critical incident activities classified by Mintzberg's managerial roles  
(Note: more than one role is possible)</caption>

<tbody>

<tr>

<th>Activities</th>

<th>Mintzberg's managerial role</th>

<th>First interview study</th>

<th>Second interview study</th>

<th>Total</th>

</tr>

<tr>

<th rowspan="5">Decisional activities</th>

<td>Resource allocator</td>

<td>7</td>

<td>4</td>

<td>11</td>

</tr>

<tr>

<td>Negotiator</td>

<td>2</td>

<td>6</td>

<td>8</td>

</tr>

<tr>

<td>Problem handler</td>

<td>0</td>

<td>7</td>

<td>7</td>

</tr>

<tr>

<td>Improver/changer</td>

<td>5</td>

<td>9</td>

<td>14</td>

</tr>

<tr>

<td>Total decisional</td>

<td>14</td>

<td>26</td>

<td>40</td>

</tr>

<tr>

<th rowspan="4">Informational activities</th>

<td>Monitor</td>

<td>9</td>

<td>1</td>

<td>10</td>

</tr>

<tr>

<td>Disseminator</td>

<td>0</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>Spokesman</td>

<td>2</td>

<td>0</td>

<td>2</td>

</tr>

<tr>

<td>Total informational</td>

<td>11</td>

<td>2</td>

<td>13</td>

</tr>

<tr>

<th rowspan="4">Interpersonal activities</th>

<td>Figurehead</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Leader</td>

<td>8</td>

<td>0</td>

<td>8</td>

</tr>

<tr>

<td>Liaison</td>

<td>1</td>

<td>6</td>

<td>7</td>

</tr>

<tr>

<td>Total interpersonal</td>

<td>9</td>

<td>6</td>

<td>14</td>

</tr>

</tbody>

</table>

### Role of meetings

The calendar study examined bookings of twenty large meeting rooms for one year ([MacDonald, Bath and Booth, 2014](#MacDonaldBB2014)). The three most frequently occurring reasons for the bookings (N= 7,349, 100%) were meetings (N=5975, 81.28%), education (N=1,174, 15.97%), and self-help sessions (N=172, 2.34%) such as Alcoholics Anonymous. Activities that started and ended in the morning occurred most frequently (N=2,806, 38.13%), with afternoon-only activities next most common (N=2,226, 30.25%).

The District's managers participated in eleven of the nineteen different activities identified. Meetings with co-workers (N=3,084) were the most frequently reported of these activities. Numbers of meetings peaked in January and October (Figure 1) with a monthly mean of 257\. The highest number of meetings scheduled for managers in these large meeting rooms on a single day in 2007 was twenty-eight; the mean number of meetings per day was twelve. Congruent with other studies of health service managers and meetings ([Arman, 2009](#Arman2009); [Moss, 2000](#Moss2000)) over half of second interview study participants said they spent two thirds or more of their work time in scheduled meetings, met monthly with at least eleven named groups and spent as much time in informal group meetings. Most participants said that meetings contributed significantly to their workload.

<figure>

![Figure1: The conceptual framework](../p649fig1.jpg)

<figcaption>Figure 1: Employee-only meetings (N=3,084) involving managers (N=61) by month in 20 large meeting rooms, 2007</figcaption>

</figure>

### Use of research

First interview study participants mentioned fifty-three different types of information and knowledge to inform their decisions, most shared orally with them (Figure 3, Appendix B). Second interview study participants mentioned many of these same types together with an additional thirty-five types of information and knowledge (Table 6, Appendix B) The role of research in critical incident decisions was initially not as clear as had been expected, principally because participants were not asked directly how research information influenced their critical incident decision. It was only after data were examined specifically to identify when in the decision process different types of information were used that it became evident that no participant had explicitly mentioned seeking health research or using it to inform their critical decision. Some participants commented on the lack of published information relevant to rural Nova Scotia health services, expressing a preference for information from Nova Scotia or Canadian health service organizations.

Such a finding was not congruent with records for literature search and document delivery services kept by the District's Library Service. In contrast these records showed that these managers did specifically ask for research information to inform at least some of their decisions ([triDHA L&KMS Reports, 2000-2010](#triDHA)). Furthermore it contrasted with responses to exploratory questions about how participants acquired research information and to findings from the card-sorting exercise. With only one exception, second interview study participants identified research evidence as "need to know" in the card-sorting exercise.

Re-examination of the types of information and knowledge (N=88) that had influenced critical decisions and responses to general exploratory questions about information behaviour suggested that managers had indeed used research. Almost all first interview study participants said that they either tried to keep up with the literature or searched specifically for information when they needed it. Many reported that they tried to do both.

> I do look for it when I need it but I also keep up. I subscribe to [journals in my field] or different things related to quality. (Junior Leader, Acute Care)

In a multidisciplinary analysis of information gatekeeping, Lu (2007) described information-savvy individuals who occupied strategic positions and facilitated access to information for others, observing that people more knowledgeable than others were usually the ones that group members tended to interact with most. This description matched that of Junior Leaders and hybrid managers who described keeping up to date in subjects that related both to their previous and current positions. These "structurally positioned gatekeepers" monitored new research information in their areas, searched purposefully for research when colleagues needed information from them that they did not already have and filtered information for value, relevance and credibility to meet each situation's context, often sharing it orally.

> ...at any given time there are probably fifty different things that I am looking for and that I am gathering information for. (Junior Leader, Acute Care)

However, participants said they did not always have time to read either the literature searches they requested or the papers provided to them.

> I have in the past [asked for] a literature search... and I get this whole list of papers back and I don't have time to review it... I probably have two or three sitting in a folder on my desk... (Director, Administration).  
>   
> Can I get through those thirty papers? Oh my god I can't do that... (Director, Acute Care).

Eight routes by which research reached these managers were identified.

1\. Generally, first they consulted explicit information that had already integrated research. Information types consulted first included professional standards, legislation, policies and practice guidelines.

> ... we will look at what the regulatory bodies say first... (Director, Operations)  
>   
> ... we always look back at the standards of practice. (Junior Leader, Acute Care)

2\. They consulted structurally positioned gatekeepers who monitor, search for, synthesize and filter research.

> So in this whole process, we had the staff involved in the department, we involved the health and safety committee, we brought in the occupational health nurse and I guess she doubles as infection control so we had both sides there. (Director, Operations)  
>   
> I was asked to help someone with a position description -and the position was to include responsibilities in an area that the person did not have much experience with. It included community development, the use of a population health approach in decision-making vs. a traditional facility management role. So I was asked to think of some of the skills that would be important in this newly defined role. (Manager, Community Health)

3\. They searched for information themselves or asked a librarian or staff member to search for them.

> Now I am not sure what is out there myself, but she would have gone through that and done the literature review to look and see what is there... (Director, Community Health)  
>   
> I will go to Embase, CINAHL, PubMed... I try to look for information before I ask somebody else. (Manager, Operations)

4\. They formed working groups to address situations not solely within an individual participant's mandate, identified information gaps and, then divided the work of searching needed to bridge them.

> ...we have done literature searches and scoured (Senior Executive, Administration)  
>   
> We had the present clinical manager, the Medical Director and [our] policy and planning committee... also the facilitators, who are the staff reps in terms of each of the teams. So the discussions would have happened there. (Director, Community Health)

5\. They contacted counterparts in other organizations to ask about the information and processes they used in similar situations.

> So basically what we have learned we have gotten through discussions with other people who have done similar... work... information from other people as to what they used for sources would be a big part of it. (Director, Community Health)  
>   
> They were supposed to look to best practice, current practices, current structures... in the province and they were to do interviews with people in the province and go to the literature, whatever avenues they had, talk to current stakeholders across the province and they were to... compile an inventory of what was happening now and make recommendations around the various ways that you could actually address the issue, and pros and cons for each one, and provide that to us as a discussion document for us as steering committees to go out and seek input from our own respective teams. (Senior Executive, Administration)

6\. They sent someone to gather information from conferences and workshops on a particular subject, when information needs happened to coincide with such events.

> We sent people to the conference in Quebec on [critical incident subject] and said bring back whatever you can - talk to whoever you can about how [critical incident subject] is working in a regional system - what are some of the experiences. (Director, Community Health)  
>   
> It was actually a presentation at a workshop in a conference that I went to that was most useful.(Director, Administration)

7\. They asked academic contacts, provincial government representatives and external consultants to consider their situations and provide advice.

> If I come up with a question that I don't know the answer to, the first thing I am going to do is access a specialist in the field... I might do a little bit of a search but if I can get an answer in like two seconds that is where I go first...they say to me you know "Check New England Journal of Medicine - I am not quite sure but I know something came out on it" - or "We never ever do this in practice, don't be fooled". (Manager, Operations)  
>   
> So we went a step further and brought in the Department of Labour and sat down for discussions - hear what we did, we need your help, give us some direction. And they sent up one of their industrial hygienists who worked with [the problem] off and on. Actually, we had the Department of Labour in for three days I think, three or four at least, just to share information. (Director, Operations)

8\. When seeking approval for decisions, they would outline one-page Situation-Background-Analysis/Alternative-Recommendation (SBARs) ([Haig, Sutton and Whittington, 2008](#Haig2006)) briefing notes, sometimes supported by key relevant research.

> Let's not ask each other on the face of it to accept something... [rather let's] say this is the issue, it is not what I think it is because this is what I am seeing, this is the evidence, both what I have seen and what has happened and what the literature can add... so for instance when somebody is charged with doing some work makes a recommendation to the Executive it comes as an SBAR so the work is all done and enough background is there for us to be able to make a decision. (Senior Executive, Administration)

These results suggest that while these health service managers did not search purposely for written research information to inform every decision, information from research did influence their decisions, reaching them through one or more of at least eight routes. These findings are discussed in the next section.

## Discussion

Discussion begins generally with the main research questions then focuses on whether and how health service managers acquire research, the focus of this paper.

**What are the information needs of health service managers and how do they use information?**

These health service managers faced multiple simultaneous decision situations generally prompted by one or more resource limitations, whether financial, physical (space, equipment), information or human. Their critical incidents were characterized by limited time resources in the form of imposed, short deadlines and little time to search for and use primary research information. The information needs of all thirty-six health service managers required blending information from multiple sources with local context to address each situation.

**What is the information behaviour of health service managers?**

These health service managers relied first on their co-workers and colleagues as information sources. The first action was often to physically or virtually (by phone or email) bring people with relevant knowledge together to share information. They tended to explore alternatives that might work rather than identify, gather information on, compare and evaluate a number of alternatives. When these health service managers faced a decision they appeared more likely to immediately consider health research already integrated into professional standards and other external sources of explicit, rule-based information than to search for unblended research in the form of journal papers or separate tools designed to translate research ([Lavis, Permanand, Oxman, Lewin and Fretheim 2009](#Lavis2009)). Participants described spending much of their work time sharing and using information to inform decisions. Those who described themselves as occupying roles characteristic of structurally positioned gatekeepers ([Lu, 2007](#Lu2007))maintained a general knowledge of the organization and also monitored the research literature to keep up-to-date in their own subject areas. They also searched specifically for new research when asked by co-workers to bring their unique perspectives to situations requiring a decision. Internal information, including policies and Situation-Background-Analysis/Assessment-Recommendation (SBAR) briefing notes, that blend tacit knowledge with research, provided an additional route for research to reach these health service managers.

**What are the barriers and challenges to health service managers in obtaining the information they need?**

Although some participants stated that their information seeking and appraisal skills were less adequate than ideal ([MacDonald, 2011](#MacDonaldJ.2011)) time constraints appeared to be the most common challenge faced by the health service managers interviewed for this study. However, it is not clear from the results of research discussed in this paper whether the information behaviour of these managers would be different had they had greater resources. Most participants appeared to satisfice, that is balance administrative efficiency with a good enough alternative ([MacDonald, Bath and Booth, 2011](#MacDonaldBB2011)). There appeared for most to be an information saturation point beyond which additional information would not have made a difference.

It has been suggested that although the academic researchers who contribute to evidence-based management work are well-trained and conduct good research, the subjects they choose to investigate tends not to be of interest to managers, or their research serves only to identify problems already familiar to managers without suggesting solutions ([Davies, 2006](#Davies2006)).

**If these health service managers informed decisions with research, how did they acquire it?**

The first observation noted for this paper is that these health service managers did consider research but bypassed the need to read numerous publications of no relevance to their immediate need by asking colleagues they trusted to keep up with research, appraise it for relevance, value and credibility, and synthesise it for the immediate context. When asking academics, government officials or external consultants for advice, they received current research knowledge synthesized and filtered for their immediate context. Research that was easier to digest and apply also reached these managers through conference presentations that blended research with practice and through co-workers and counterparts in other organizations who had already used research to inform their own decisions.

This preference for research blended with context relates to web video conferencing as an alternative to travel. Health service workers register for relevant webinars and other online workshops on behalf of the organization, book a meeting room and equipment, and invite others to come, hear and ask questions about the subject, thereby accessing and sharing the most recent research. Webinars are a cost effective way to reach health services decision makers more often used by vendors than researchers.

A second observation is that health services managers may be showing an increased interest in blended secondary sources and a corresponding decreasing interest in library subscriptions to traditional journals. Sources for clinicians, for example, [PEN Practice-Based Evidence in Nutrition](http://www.pennutrition.com/index.aspx) from Dietitians of Canada, provide '_patient/practice oriented evidence that matters_/ at a reasonable cost. Other blended sources, for example, [Infotech](http://www.infotech.com) for information systems managers, are sources of both written decision oriented evidence that matters and just-in-time expert consulting services.

A third observation relates to parallels between the seven research-to-policy routes developed by [Weiss (1979](#Weiss1979)) and the eight routes by which research reached these health service managers identified in this study. These are compared in Table 5.

<table><caption>

Table 5: Comparison of the eight routes by which research reaches health service managers identified in the present study with the ([Weis, 1979)](#Weiss1979) research-to-policy conceptual framework.</caption>

<tbody>

<tr>

<th>Weiss model</th>

<th>Research route to health service managers</th>

</tr>

<tr>

<td>

**Knowledge-driven Model**</td>

<td>Not described.</td>

</tr>

<tr>

<td>

**Problem-solving Model**</td>

<td>

1\. Generally, first they consulted explicit information that had already integrated research. Information types consulted first included professional standards, legislation, policies and practice guidelines.  
3\. They searched for information themselves or asked a librarian or staff member to search for them.</td>

</tr>

<tr>

<td>

**Political model**</td>

<td>

8\. When seeking approval for decisions, they would outline one page _situation-background-analysis/alternative-recommendation_ (SBAR) ([Haig, _et al_., 2008](#Haig2006)) briefing notes sometimes supported by key relevant research.</td>

</tr>

<tr>

<td>

**Tactical model**</td>

<td>Not described.</td>

</tr>

<tr>

<td>

**Enlightenment model**</td>

<td>

2\. They consulted structurally positioned gatekeepers who monitor, search for, synthesize and filter research.</td>

</tr>

<tr>

<td>

**Interactive model**</td>

<td>

4\. They formed working groups to address situations not solely within an individual participant's mandate, identified information gaps and, then, divided the searching needed to bridge them.  
5\. They contacted counterparts in other organizations to ask about the information and processes they used in similar situations.  
6\. They sent someone to gather information from conferences and workshops on a particular subject, when information needs happened to coincide with such events.  
7\. They asked academic contacts, provincial government representatives and external consultants to consider their situations and provide advice.</td>

</tr>

<tr>

<td>

**Research as intellectual enterprise**</td>

<td>Not described.</td>

</tr>

</tbody>

</table>

## Conclusion

An important finding of this research is that health service managers do indeed use research information to inform their decisions. Each decision is informed with multiple pieces of different categories and types of information that must be integrated and balanced. Typically research reaches them through more indirect routes, in the form of standards or after being filtered by colleagues or by research summaries. Health services managers thus find research blended with experience and expertise, already appraised for credibility and filtered for value and relevance, to be more easily applied within their intense work environments.

Their preferences for research blended with experience or expertise, filtered to suit the context may encourage researchers and research funders concerned that their findings are not being applied to practice ([Burns, 2005](#Burns2005); [Canadian Institutes of Health Research, 2004](#Canadian2004)) to target preferred sources such as professional standards to ensure they maintain congruence with current research evidence.

Further study is needed to identify structurally positioned gatekeepers within health service organizations, understand their roles in filtering and sharing research and how best to support them. The popularity of tools such as _PEN Nutrition_ suggest additional models for translating research and blending it with experience.

## Acknowledgements

This research was completed as part of requirements for a PhD, Centre for Health Information Management Research, University of Sheffield ([MacDonald, 2011](#MacDonaldJ.2011)).

## <a id="authors"></a>About the authors

**Jacqueline MacDonald** is a full time health service manager working for three publicly funded district health authorities in Nova Scotia Canada. She is also an adjunct professor at Dalhousie University in the School of Information Management, Faculty of Management and in Medical Informatics, Faculty of Medicine. She received her Bachelor of Science degree in Biology from Mount Saint Vincent University, Halifax Nova Scotia, her Master of Library Science degree from Dalhousie University and her Ph.D. from University of Sheffield Information School, U.K. She can be contacted at [jmacdonald@ssdha.nshealth.ca](mailto:jmacdonald@ssdha.nshealth.ca).  
**Peter Bath** Bath is Professor of Health Informatics at the University of Sheffield Information School. He is Director of the Centre for Health Information Management Research (CHIMR) and Programme Co-ordinator of the MSc in Health Informatics program. He received his BSc (Technical) from University of Wales, MSc in Information Studies from University of Sheffield and PhD in Information Science (Sheffield). He can be contacted at [p.a.bath@sheffield.ac.uk](mailto:p.a.bath@sheffield.ac.uk).  
**Andrew Booth** is Reader in Evidence Based Information Practice and Director of Information at the University of Sheffield School of Health and Related Research (ScHARR). He received his BA (Hons) from University of Reading, Diploma in Librarianship from the College of Librarianship, Aberystwyth, Wales, MSc in Economics, University College of Wales and PhD in Qualitative Evidence Synthesis (Sheffield). He can be contacted at [A.Booth@sheffield.ac.uk](mailto:A.Booth@sheffield.ac.uk).

</section>

<section>

## References

<ul>
<li id="Allison1999">Allison, G. T. &amp; Zelikow, P. (1999). <em>Essence of decision: explaining the Cuban missile crisis.</em> New York, NY: Addison-Wesley.
</li>
<li id="Arman2009">Arman, R., Dellve, L., Wikström, E. &amp; Törnström, L. (2009). What health care managers do: applying Mintzberg's structured observation method. <em>Journal of Nursing Management, 17</em>(5), 718-729.
</li>
<li id="Arthur2003">Arthur, S. &amp; Nazroo, J. (2003). Designing fieldwork strategies and materials. In J. Ritchie &amp; J. Lewis, (Eds.). <em>Qualitative research practice: a guide for social science students and researchers</em>, (pp. 109-137). London: Sage Publications.
</li>
<li id="Baghbanian2012">Baghbanian, A., Hughes, I., Kebriaei, A. &amp; Khavarpour, F.A. (2012). Adaptive decision-making: how Australian healthcare managers decide. <em>Australian Health Review, 36</em> (1), 49-56.
</li>
<li id="Baker2004">Baker, G. R., Ginsburg, L. &amp; Langley, A. (2004). An organizational science perspective on information, knowledge evidence and organizational decision making. In L. Lemieux-Charles, &amp; F. Champagne (Eds.), <em>Using knowledge and evidence in health care: multidisciplinary perspectives</em>, (pp. 86-114). Toronto, Canada: University of Toronto Press.
</li>
<li id="Black2001">Black, N. (2001). Evidence based policy: proceed with care. <em>The BMJ, 323</em>(7307), 275-279.
</li>
<li id="Bowen2005">Bowen S. &amp; Zwi, A. B. (2005). Pathways to "evidence-informed" policy and practice: a framework for action. <em>PLoS Medicine, 2</em>(7), e166.
</li>
<li id="Brehaut2005">Brehaut, J. D. &amp; Juzwishin, D. (2005). <a href="http://www.webcitation.org/6UIqCmEgf">Bridging the gap: the use of research evidence in policy development.</a> Edmonton, Canada: Alberta Heritage Foundation for Medical Research, Health Technology Assessment Unit. ( HTA Initiative #18.) Retrieved from http://www.ihe.ca/documents/HTA-FR18.pdf (Archived by WebCite® at http://www.webcitation.org/6UIqCmEgf).
</li>
<li id="Burns2005">Burns, A. (2005). Recognizing the needs and roles of the key actors. <em>Journal of Health Services Research &amp; Policy, 10</em> (Suppl. 1), 53-54.
</li>
<li id="Canadian1998">Canadian Health Services Research Foundation. (1998). <em><a href="http://www.webcitation.org/6UJ8Prze3">Health services research and evidence-based decision-making</a></em>. Ottawa: Canadian Health Services Research Foundation. Retrieved from http://www.cfhi-fcass.ca/migrated/pdf/mythbusters/EBDM_e.pdf. (Archived by WebCite® at http://www.webcitation.org/6UJ8Prze3)
</li>
<li id="Canadian2004">Canadian Institutes for Health Research/ (2004). <a href="http://www.webcitation.org/6UIqNFulI">Innovation in action: knowledge translation strategy 2004-2009.</a> Ottawa: Canadian Institutes for Health Research Retrieved from http://www.cihr-irsc.gc.ca/e/26574.html (Archived by WebCite® at http://www.webcitation.org/6UIqNFulI).
</li>
<li id="Choo2003">Choo, C. W. &amp; Johnston, R. (2003). <a href="http://www.webcitation.org/6UIqVywNP">Innovation in the knowing organization: a case study of an e-commerce initiative.</a> <em>Sprouts: Working Papers on Information Systems, 3</em>, article 4. Retrieved from http://sprouts.aisnet.org/144/2/rev-2003-10.pdf (Archived by WebCite® at http://www.webcitation.org/6UIqVywNP).
</li>
<li id="Choo2006">Choo, C.W. (2006). <em>The knowing organization: how organizations use information to construct meaning, create knowledge and make decisions.</em> 2nd ed. New York, NY: Oxford University Press.
</li>
<li id="Clarke2013">Clarke, A., Taylor-Phillips, A., Swan, J., Gkeredakis, E., Mills, P., Powell, J. &amp; Grove, A. (2013). Evidence-based commissioning in the English NHS: who uses which sources of evidence? A survey 2010/2011. <em>BMJ Open, 3</em>, paper e002714. Retrieved from http://bmjopen.bmj.com/content/3/5/e002714.full.pdf. (Archived by WebCite® at http://www.webcitation.org/6UJ5Lx6Fk)
</li>
<li id="Cohen1972">Cohen, M. D., March, J. G. &amp; Olsen, J. P. (1972). A garbage can model of organizational choice. <em>Administrative Science Quarterly, 17</em>(1), 1-25.
</li>
<li id="Davies2006">Davies H. (2006). Improving the relevance of management research: evidence-based management: design science or both? <em>Business Leadership Review, 3</em>(3), 1-6.
</li>
<li id="Dobbins2007">Dobbins, M., Rosenbaum, P., Plews, N., Law, M. &amp; Fysh, A. (2007). <a href="http://www.webcitation.org/6UKPpsBs3">Information transfer: what do decision makers want and need from researchers?</a> <em>Implementation Science, 2</em>, paper 20. Retrieved from http://www.implementationscience.com/content/2/1/20 (Archived by WebCite® at http://www.webcitation.org/6UKPpsBs3)
</li>
<li id="Dobrow2004">Dobrow, M., Goel, V. &amp; Upshur, R. (2004). Evidence-based health policy: Context and utilisation. <em>Social Science &amp; Medicine, 58</em>(1), 207-217.
</li>
<li id="Edwards2013">Edwards, C., Fox, R., Gillard, S., Gourlay, S., Guven, P., Jackson, C. &amp; Drennan, V. (2013). <a href="http://www.webcitation.org/6UIqjNgK4">Explaining health managers' information seeking behaviour and use. Final report.</a> Southampton, UK: National Institute for Health Research. Retrieved from http://www.nets.nihr.ac.uk/__data/assets/pdf_file/0003/85062/FR-08-1808-243.pdf (Archived by WebCite® at http://www.webcitation.org/6UIqjNgK4).
</li>
<li id="Flanagan1954">Flanagan, J.C. (1954). The critical incident technique. <em>Psychological Bulletin. 51</em>(4), 327-359.
</li>
<li id="Glouberman2001">Glouberman, S. &amp; Mintzberg, H. (2001). Managing the care of health and the cure of disease-Part II: Integration, <em>Health Care Management Review, 26</em>(1), 70-84.
</li>
<li id="Gray2009">Gray, J.A.M., &amp; Ison, E. (2009). <em>Evidence-based healthcare and public health: how to make decisions about health services and public health</em>. Edinburgh, New York, NY: Churchill Livingstone Elsevier.
</li>
<li id="Haig2006">Haig, K.M., Sutton, S. &amp; Whittington, J. (2006). SBAR: a shared mental model for improving communication between clinicians. <em>Joint Commission Journal on Quality and Patient Safety 32</em>(3), 167-75.
</li>
<li id="Head1996">Head, A.L. (1996). <em>An examination of the implications for NHS information providers of staff transferring from functional to managerial roles.</em>Unpublished MSc. thesis, University College of Wales, Aberystwyth, U.K.
</li>
<li id="Heller1988">Heller, F.P., Drenth, P., Koopman, P. &amp; Rus, V. (1988). <em>Decisions in Organizations: a three county comparative study.</em> London: Sage Publications.
</li>
<li id="Innvaer2002">Innvaer, S., Vist, G., Trommald, M. &amp; Oxman, A. (2002). Health policy-makers' perceptions of their use of evidence: A systematic review. <em>Journal of Health Services Research &amp; Policy, 7</em>(4), 239-244.
</li>
<li id="Innvaer2009">Innvaer, S. (2009). <a href="http://www.webcitation.org/6UKQAFFlP">The use of evidence in public governmental reports on health policy: an analysis of 17 Norwegian official reports (NOU).</a> <em>BMC Health Services Research, 9</em>, paper 177. Retrieved from http://www.biomedcentral.com/1472-6963/9/177 (Archived by WebCite® at http://www.webcitation.org/6UKQAFFlP)
</li>
<li id="Kadane2005">Kadane, J. B. (2005). Bayesian methods for health-related decision making. <em>Statistics in Medicine, 24</em>(4), 563-567.
</li>
<li id="Kovner2006">Kovner, A. R. &amp; Rundall, T. G. (2006). Evidence-based management reconsidered. <em>Frontiers of Health Services Management, 22</em>(3), 3-22.
</li>
<li id="Lavis2005">Lavis, J. N., Davies, H. T. O., Oxman, A., Denis, J-L., Golden-Biddle, K. &amp; Ferlie, E. (2005). Towards systematic reviews that inform health care management and policy-making. <em>Journal of Health Services Research &amp; Policy, 10</em>(Suppl. 2), 35-48.
</li>
<li id="Lavis2009">Lavis, J.N., Permanand, G., Oxman, A.D., Lewin, S. &amp; Fretheim, A. (2009). Support tools for evidence-informed health policymaking (STP): preparing and using policy briefs to support evidence-informed policymaking. <em>Health Research Policy and Systems, 7</em> (Suppl.1), S13.
</li>
<li id="Lipshitz1997">Lipshitz, R. &amp; Strauss, O. (1997). Coping with uncertainty: a naturalistic decision-making analysis, <em>Organizational Behavior and Human Decision Processes, 69</em>(2), 149-163.
</li>
<li id="Lu2007">Lu, Y. (2007). The human in human information acquisition: understanding gatekeeping and proposing new directions in scholarship. <em>Library &amp; Information Science Research, 29</em>(1), 103-122.
</li>
<li id="MacDonaldJ.2011">MacDonald, J. (2011). <a href="http://www.webcitation.org/6UIqpKkBJ"><em>The information sharing behaviour of health service managers: a three-part study</em>.</a> Unpublished doctoral dissertation, University of Sheffield, Sheffield, U.K. Retrieved from http://etheses.whiterose.ac.uk/4394/ (Archived by WebCite® at http://www.webcitation.org/6UIqpKkBJ).
</li>
<li id="MacDonaldBB2008a">MacDonald, J., Bath, P.A. &amp; Booth, A. (2008a). Healthcare service managers: what information do they need and use? <em>Evidence Based Library and Information Practice, 3</em>(3), 18-38.
</li>
<li id="MacDonaldBB2008b">MacDonald, J., Bath, P.A. &amp; Booth, A. (2008b). Healthcare managers' decision making: findings of a small scale exploratory study. <em>Health Informatics Journal, 14</em>(4), 247-58
</li>
<li id="MacDonaldBB2011">MacDonald, J., Bath, P.A. &amp; Booth, A. (2011). Information overload and information poverty: challenges for health service managers? <em>Journal of Documentation, 67</em>(2), 238-263.
</li>
<li id="MacDonaldBB2014">MacDonald, J., Bath, P.A. &amp; Booth, A. (2014). An exploration of opportunities for health service managers to share information orally: a method of documentary analysis using an electronic room-booking calendar. <em>International Journal of Health Information Management Research, 2</em>(1), 31-43.
</li>
<li id="Mbananga2002">Mbananga, N. &amp; Sekokotla, D. (2002) <a href="http://www.webcitation.org/6UIqtvWzO">The utilisation of health management information in Mpumalanga Province.</a> Durham, South Africa: Health Systems Trust. Retrieved from http://www.hst.org.za/uploads/files/mpuinfo.pdf (Archived by WebCite® at http://www.webcitation.org/6UIqtvWzO).
</li>
<li id="Mintzberg1973">Mintzberg, H. (1973). <em>The nature of managerial work.</em> New York, NY: Harper and Rowe.
</li>
<li id="Mintzberg1976">Mintzberg, H., Raisinghani, D. &amp; Theoret, A. (1976). The structure of "unstructured" decision processes. <em>Administrative Science Quarterly, 21</em>(2), 246-275.
</li>
<li id="Mitton2004">Mitton, C. &amp; Patten, S.J. (2004). Evidence-based priority-setting: what do the decision-makers think? <em>Health Services Research and Policy, 9</em>(3), 146-152.
</li>
<li id="Moahi2000">Moahi, K.H. (2000). <em>A study of the information behavior of health care planners, managers and administrators in Botswana and implications for the design of a national health information system.</em> Unpublished doctoral dissertation, University of Pittsburgh, Pittsburgh, Pennsylvania, USA.
</li>
<li id="Moss2000">Moss, L.J. (2000). <a href="http://www.webcitation.org/6UIqzACke">Perceptions of meeting effectiveness in the Capital Health Region.</a> Unpublished masster's thesis, Royal Roads University, Victoria, BC, Canada. Retrieved from http://www.collectionscanada.gc.ca/obj/s4/f2/dsk1/tape2/PQDD_0021/MQ49208.pdf (Archived by WebCite® at http://www.webcitation.org/6UIqzACke).
</li>
<li id="Nieds2003">Nied&amp;zacute;wiedzka, B. (2003). <a href="http://www.webcitation.org/6CH7RXJih">A proposed general model of information behaviour</a>. <em>Information Research, 9</em>(1) paper 164. Retrieved from http://InformationR.net/ir/9-1/paper164.html (Archived by WebCite® http://www.webcitation.org/6CH7RXJih)
</li>
<li id="Nutley2007">Nutley, S.M.; Walter, I. &amp; Davies, H.T.O. (2007). <em>Using evidence: how research can inform public services.</em> Bristol, UK: Policy Press.
</li>
<li id="Polanyi1966">Polanyi, M. (1966). <em>The tacit dimension.</em> New York, NY: Doubleday.
</li>
<li id="Ritchie2003">Ritchie, J. &amp; Spencer, L. (2003). Carrying out qualitative analysis. In J. Ritchie &amp; J. Lewis (Eds.). <em>Qualitative Research Practice: A Guide for Social Science Students and Researchers</em>, (pp. 219-262). London: Sage Publications.
</li>
<li id="Rugg2005">Rugg, F. &amp; McGeorge, P. (2005). The sorting techniques: a tutorial paper on card sorts, picture sorts and item sorts. <em>Expert Systems, 22</em>(3), 94-107.
</li>
<li id="Sheldon2005">Sheldon, T. A. (2005). Making evidence synthesis more useful for management and policy-making. <em>Journal of Health Services Research and Policy, 10</em>(Suppl. 1), 1-5.
</li>
<li id="Simon1960">Simon, H.A. (1960) <em>The new science of management decision.</em> New York, NY: Harper and Row.
</li>
<li id="Stepanovich1999">Stepanovich, P. &amp; Uhrig, J.D. (1999). Strategic decision-making in high velocity environments: implications for health care. <em>Journal of Healthcare Management, 44</em>(3), 197-204.
</li>
<li id="Tengelin2011">Tengelin, E., Arman, R., Wikström, E. &amp; Dellve, L. (2011). Regulating time commitments in healthcare organizations: managers' boundary approaches at work and in life. <em>Journal of Health Organization and Management, 25</em>(5), 578-599.
</li>
<li id="triDHA">TriDHA Health. <em>Library and Knowledge Management Services</em>. (2000-2010). <em>Quarterly reports to the Director of Information Services</em>. Unpublished internal reports.
</li>
<li id="Ward2009">Ward, V., House, A. &amp; Hamer, S. (2009). Developing a framework for transferring knowledge into action: a thematic analysis of the literature. <em>Journal of Health Services Research and Policy, 14</em>(3), 156-64.
</li>
<li id="Weiss1979">Weiss, C.H. (1979). The many meanings of research utilization. <em>Public Administration Review, 39</em>(5), 426-431.
</li>
<li id="Weatherly2002">Weatherly, H., Drummond, M. &amp; Smith, D. (2002). Using evidence in the development of local health policies. Some evidence from the United Kingdom. <em>International Journal of Technology Assessment in Health Care, 18</em>(4), 771-81.
</li>
<li id="Williams2007">Williams, I. &amp; Bryan, S. (2007). Understanding the limited impact of economic evaluation in health care resource allocation: a conceptual framework. <em>Health Policy, 80</em>(1), 135-143.
</li>
<li id="Willis2012">Willis, C.D., Mitton, C., Gordon, J. &amp; Best A. (2012). System tools for system change. <em>BMJ Quality &amp; Safety, 21</em>(3), 250-262
</li>
<li id="Yin2002">Yin, R.K. (2002). <em>Case study research: design and methods.</em> 2nd ed. Newbury Park, CA: Sage Publications.
</li>
</ul>

</section>

</article>

<section>

* * *

## Appendix A - An overview of the research design process

<figure>

![Figure 2 Diagrammatic overview of research design or redesign process](../p649fig2.jpg)

<figcaption>Figure 2 Diagrammatic overview of research design or redesign process</figcaption>

</figure>

The aims of this research were to investigate information behaviours among health service managers and to identify whether any information seeking models represented their behaviour. These research aims reflected an assumption that health service managers' dominant information behaviour was information seeking, perhaps due at least in part because prominent information behaviour models tend to represent information seeking. Had the interviews chosen as the means of gathering data for Part 1 of this research (Point A in Figure 2) resulted in a deep understanding of participants' information behaviour, Part 2 of the research would have been a questionnaire to determine whether this behaviour could be generalized to a larger group of health service managers.

The first interview study results suggested that information sharing was more important to participants than information seeking and failed to provide an in-depth understanding of participants' information behaviour to be followed up by questionnaire. At this stage and at each step in the research that followed, the complexity of participants' information behaviour became more obvious. It became necessary to stop and refocus, first, to try to take a broad overview of results and then try to drill down in one or more areas to find out exactly what was happening. Consequently, changes to research design were ongoing over the course of this study. These are represented by decision diamonds in Figure 2.

Data gathering changes involved replacing the questionnaire study with an information sharing study (Point C in Figure 2) that would include a calendar study, a second series of interviews, a card-sorting exercise and a series of meeting observations. After Calendar data were analysed, a decision was made to report results separately (Point D in Figure 2) with study results used to refocus the revised information sharing study (Point E in Figure 2). Then, because they produced data on information and information behaviour that was too rich for a smaller follow-up study, eliminating meeting observations from the information sharing study (Point F in Figure 3?1) and relabelling that study as the second interview study.

Data analysis changes included replacing inadequate coding systems, re-analyzing data and replacing simpler for more complex approaches and _vice versa_, as follows:

*   The coding system developed to analyze first interview study results for information and information seeking behaviour was replaced with a new indexing framework developed for knowledge.
*   After first interview study data were analyzed, a second pass was made through the interviews to see whether comments suggested a relationship between the satisficing and inappropriate information quantity (Point B in Figure 2). The process raised questions for the second interview study.
*   Social network analysis mapping proved not to be a productive way to analyze card-sorting exercise data so results were presented as simple counts.
*   Indexing second interview study data did not produce very different results from the first interview study so a new approach was developed to map interview transactions to allow quantitative within and between case analysis.

Although the iterative nature involved in designing this was not the planned approach, moving between study results and the research literature at each step was more effective in accomplishing research aims than the original study design could have been. The final design, a three part study, with each part arising from and building on the previous part, brings new clarity to what is known about the information behaviour of health service managers.

## Appendix B - Types of information and knowledge

<figure>

![Figure 3](../p649fig3.jpg)

<figcaption>Figure 3 Types of information and knowledge mentioned by first interview study participants that served as components for the Card Sorting Exercise (N=60). There were cards for each of 26 types of explicit information (purple), 24 types of cultural knowledge (yellow) and 10 types of tacit knowledge (orange). Seven types of explicit information frequently provided by library services and not mentioned by first interview study participants were included (green).</figcaption>

</figure>

<table><caption>  
Table 6: Types of information and knowledge used (N=35) to inform critical incident decisions in the second interview study</caption>

<tbody>

<tr>

<th>Explicit</th>

</tr>

<tr>

<td>advertisements</td>

</tr>

<tr>

<td>best practices</td>

</tr>

<tr>

<td>content in popular magazines and web sites</td>

</tr>

<tr>

<td>corporate report created</td>

</tr>

<tr>

<td>corporate report used</td>

</tr>

<tr>

<td>course content</td>

</tr>

<tr>

<td>decision making framework</td>

</tr>

<tr>

<td>demonstration of technical know how</td>

</tr>

<tr>

<td>environmental scan</td>

</tr>

<tr>

<td>individual performance evaluation</td>

</tr>

<tr>

<td>meeting agenda</td>

</tr>

<tr>

<td>meeting documents</td>

</tr>

<tr>

<td>meeting records</td>

</tr>

<tr>

<td>news media</td>

</tr>

<tr>

<td>operational plan</td>

</tr>

<tr>

<td>opinion surveys</td>

</tr>

<tr>

<td>organizational performance evaluation</td>

</tr>

<tr>

<td>other plans</td>

</tr>

<tr>

<td>practical examples</td>

</tr>

<tr>

<td>presentation slides</td>

</tr>

<tr>

<td>quality plan</td>

</tr>

<tr>

<td>staffing guidelines</td>

</tr>

<tr>

<td>template for information sharing</td>

</tr>

<tr>

<td>tools to measure performance</td>

</tr>

<tr>

<th>Tacit</th>

</tr>

<tr>

<td>demonstration of how something works</td>

</tr>

<tr>

<td>historical summary</td>

</tr>

<tr>

<td>understanding goals and objectives</td>

</tr>

<tr>

<td>understanding of best practices</td>

</tr>

<tr>

<td>understanding of teams and partnerships</td>

</tr>

<tr>

<td>understanding other departments</td>

</tr>

<tr>

<th>Cultural</th>

</tr>

<tr>

<td>cultural competency</td>

</tr>

<tr>

<td>report on status of current situation</td>

</tr>

<tr>

<td>rumours</td>

</tr>

<tr>

<td>stories of similar situations</td>

</tr>

<tr>

<td>system stressors and pressure points</td>

</tr>

</tbody>

</table>

</section>