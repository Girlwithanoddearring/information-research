<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# A bibliometric study of scholarly articles published by library and information science authors about open access

#### [Jennifer Grandbois](#author) and [Jamshid Beheshti](#author)  
School of Information Studies, McGill University, Montreal, Canada

#### Abstract

> **Introduction.** This study aims to gain a greater understanding of the development of open access practices amongst library and information science authors, since their role is integral to the success of the broader open access movement.  
> **Method.** Data were collected from scholarly articles about open access by library and information science authors from 2003 until 2011 found in the Library and Information Science Abstracts database.  
> **Analysis.** A bibliometric approach is taken for the information gathered from 203 articles. Excel and SPSS were used to derive descriptive statistics and correlations.  
> **Results.** Overall an open access rate of 60% was found, which was lower than expected considering 94% of these articles appeared to endorse open access.  
> **Conclusions.** Although these results show a higher open access rate than previous studies, and a linear growth of open access publications over the years, there is still a large gap between theory and practice which needs to be addressed.

<section>

## Introduction

Advancements of information and communication technologies over the past two decades have changed significantly the way that information is disseminated and accessed, impacting scholarly communications. Printed journals have become less popular in academic libraries, both due to space limitations and patrons' demands, being replaced with digital publications which can be accessed from many countries across the world. In addition, recent budget cuts in most libraries, as well as the increasing cost of scholarly journals, the prices of which '_have risen faster than the average inflation rate_' ([Drott, 2006](#dro06), p.82), have compelled many institutions to contemplate reducing their expensive subscriptions to scholarly publications.

Within this context of limited funds, librarians must find a way to provide the services and materials their patrons' desire in the most cost effective manner. Most information-seekers in this digital age expect free and easily accessible electronic materials ([Baich, 2012](#bai12); [Sidorko and Yang, 2009](#sid09)), to the extent that '_in some situations information seekers will readily sacrifice content for convenience_' ([Connaway, Dickey and Radford, 2011](#con11), p. 27). Connaway _et al._'s study found convenience to be '_the primary criteria used for making choices_' ([2011, p. 27](#con11)), where convenience includes the format of the resource (print or digital), the quality of the content and how much time accessing and using the resource requires. As such, one solution to providing resources that patrons desire without excessive costs to the library involves open access resources, which are freely available online.

Although there are other definitions of open access, in this paper we use the definition presented by the Budapest Open Access Initiative (BOAI). Authors deserve '_control over the integrity of their work and the right to be properly acknowledged and cited,_' but this literature should otherwise be freely and easily available on the Internet for anyone to

> read, download, copy, distribute, print, search, or link to the full texts of these articles, crawl them for indexing, pass them as data to software, or use them for any other lawful purpose' ([BOAI, 2012](#bud12), para. 8).

Accordingly the only barrier to accessing peer-reviewed academic literature should be that of having access to the Internet itself ([BOAI, 2012](#bud12)).

There are two methods for authors to make their academic literature available in open access venues: publish their articles directly into an open access journal (gold open access) or self-archive their preprint, postprint or PDF, depending on the copyright regulations of the journal in which they are publishing their work, into an open access repository or on their personal Website (green open access) ([BOAI, 2012](#bud12), para.7). Thus to some extent the choice of how an article is made available is in the hands of the authors themselves, although they may be limited by a lack of appropriate journals to publish in or the funds to publish directly in open access journals. To counter these barriers some institutions have open access policies and resources for authors and some research funding agencies now require a digital copy of their funded authors' manuscripts to be submitted into an open access repository upon acceptance to a journal ([Zuccala, 2009](#zuc09), p. 35-36).

The benefits of open access extend beyond the money saved by libraries. Antelman's study found that '_across a variety of disciplines, open-access articles have a greater research impact than articles that are not freely available_' ([2004](#ant04), p. 379), which is confirmed by other studies that regarded the open access citation advantage ([Davis, 2009](#dav09); [Norris, Oppenheim and Rowland, 2008](#nor08); [Xia and Nakanishi, 2012](#xin12)). Similar research analysed impact factor trends of open access journals over several years showing an increased trajectory of open access publishing and demonstrating that open access journals are comparable to more traditional journals ([Gumpenberger, Ovalle-Perandones and Gorriaz, 2013](#gum13); [Mukherjee, 2007](#muk07); [Mukherjee, 2009a](#muk09); [Mukherjee, 2009b](#mub09); [Xia, 2012](#xia12); [Yuan and Hua, 2010](#yua10)). De Bellis lists several additional benefits, such as faster publication and dissemination, easier access for researchers and fulfilling the ethical human '_right to know_' ([2009](#deb09), p. 299). Evidently there are many reasons to make academic articles available through open access.

Since library and information science authors should be well aware of this problem, it stands to reason that they would want to support open access publishing in every way possible, including adopting this method of publication for their own academic works. Despite this, previous studies have only shown between 27% ([Way, 2010](#way10), p. 306) and 51% ([Xia, Wilhoite and Myers, 2011](#xia11), p. 796) of publications by library and information science authors available through open access options. However, these studies did not look specifically at articles about open access which would likely have a higher percentage of open access availability, since these authors are more aware of this issue, especially if their articles endorse this method of publication.

This study investigates this issue by gathering information about articles about open access written by library and information science authors, the journals they were published in, and their authors, from 2003 to 2011, to determine:

*   What proportion of these articles endorse open access as well as what proportion is available as open access resources (directly or through self-archiving)?
*   What are the characteristics of these articles, journals and authors?
*   What correlations are there between article, journal and author attributes, and the availability of their publications?
*   What are the open access publication longitudinal trends?

## Literature review

Although many studies about open access have been published over the last decade, this literature review targets those which are relevant to the present research and groups them by their main foci for analysis.

### Discipline and subject analysis

Bibliometric studies about open access within the library and information science context presuppose differences between various disciplines and attempts to understand open access within this specific context ([Kousha and Thelwall, 2006](#kou06); [Mukherjee, 2007](#muk07); [Mukherjee, 2009a](#muk09); [Mukherjee, 2009b](#mub09); [Way, 2010](#way10); [Xia _et al._, 2011](#xia11); [Xia, 2012](#xia12); [Yuan and Hua, 2010](#yua10)). Some of these studies were more focused on article and journal analysis, which will be examined in more detail below. Only two of these studies consider the overall rate of article open access availability, which is one of the main focuses of this research ([Way, 2010](#way10); [Xia _et al._, 2011](#xia11)). To date there is only one study about open access as the subject of sampled articles. Although this study does gather data for multiple years, it is not limited to the library and information science discipline and it focuses on citation analysis, which is not directly relevant ([Duzyol, Taskin and Tonta, 2010](#duz10)).

Way sampled articles from the top twenty library and information science journals (including open access journals) and found only 27% of articles available in open access options for 2007 ([2010](#way10), p. 304-306). Way also emphasized the discrepancy between the high percentage of journals that allow self-archiving and the minimal amount of authors who actually archive their works ([2010](#way10), p. 306-307). This presents a fundamental problem because '_if professionals in [library and information science] are unwilling to archive their works in repositories, it should not be surprising that repositories face difficulties in recruiting content_' ([Way, 2010](#way10), p. 306-307). Considering that '_a large number of institutional repositories have been initiated and operated by academic libraries_' ([Xia _et al._, 2011](#xia11), p. 799) and that librarian positions are being created specifically for scholarly communications, publishing and digital repositories at university libraries, it is clear that if library and information science authors are not following open access practices themselves there may be a limit to how far the open access movement will advance.

Xia _et al._ were more concerned with library and information science author self-archiving practices, so open access journals were excluded from analysis, and despite this they reported a more promising 51% open access availability rate for 2006 ([Xia _et al._, 2011](#xia11), p. 794-796). They were also interested in correlations between author status and other variables, finding publication patterns between librarian and faculty authors to be similar in terms of the percentage of open access publications. Being the first study to compare author status with open access availability in this discipline, Xia _et al._ were surprised with this finding since '_librarians in general are more familiar with [open access] practice, and they understand better than teaching faculty the importance of free and quick information sharing_' ([Xia _et al._, 2011](#xia11), p. 799). Evidently, understanding the open access publication patterns of library and information science authors is integral to the progression of the open access movement; however, since each of these studies only gathered data for a one year period at a time and there is a discrepancy between their methodologies, it is difficult to estimate the overall trends. Therefore the present research fills a gap by collecting data on library and information science articles about open access for multiple years to investigate the trends in the rate of open access availability.

### Article analysis

A few studies focused on the attributes of the articles themselves from a multidisciplinary approach ([Antelman, 2004](#ant04)) or within the library and information science field ([Kousha and Thelwall, 2006](#kou06)). Aside from citation analysis to determine the impact of open access articles across four disciplines Antelman also tabulated the proportion of preprints and postprints for each selected discipline. The results showed that open access articles are cited between 45% (philosophy) and 91% (mathematics) more often than paid access articles, and also that self-archiving patterns do differ depending on the discipline ([Antelman, 2004](#ant04), p. 376-378). This research contributes to a better understanding of the benefits of open access and demonstrates that the open access culture of each discipline must be studied directly in order to be understood.

Kousha and Thelwall ([2006](#kou06)) were interested in the '_motivations for URL citations to open access library and information science articles_'. They found that the majority of citations to these open access articles were for formal scholarly purposes (43%) and another 18% were for informal scholarly purposes such as course readings and bibliographies ([Kousha and Thelwall, p. 2006](#kou06), p. 510), indicating that availability of open access contributes to scholarly communications. Thus article analysis is another integral component to understanding open access publications and the present research gathered information on the self-archived formats of articles (preprint, postprint, PDF) amongst other attributes, but within the library and information science discipline.

### Journal analysis

The impact factor and growth of open access journals have been analysed to determine if these journals are comparable to more traditional methods of academic publications. Whether from the multidisciplinary perspective ([Gumpenberger _et al._, 2013](#gum13)) or from the library and information science viewpoint ([Mukherjee, 2007](#muk07); [Mukherjee, 2009a](#muk09); [Mukherjee, 2009b](#mub09); [Xia, 2012](#xia12); [Yuan and Hua, 2010](#yua10)) these studies have shown that open access journals are a viable method of publication, that they are comparable to paid access journals, including in the sense that some journals have more impact than others, and that they have had a steady growth over five to ten year periods of data analysis. This means that the gold open access route can be effective, but since it takes time for these journals to establish themselves and they tend to be newer than most paid access or hybrid journals, it will take time before there are sufficient open access journal options within each discipline.

As mentioned in the introduction, the alternative open access route is through self-archiving, thus when an open access journal is not available authors can choose a traditional journal that allows them to archive their works in an open access format. Laakso found that 81% of the sampled articles were published in journals that allow some form of self-archiving ([Laakso, 2014](#laa14), p. 491), making it feasible to avoid publishing articles where self-archiving is not permitted. However, this assumes that authors are aware of green open access methods and publisher policies, which is not always the case ([Kim, 2010](#kim10), p. 1917).

What has not received much attention prior to this study but is integral to open access is hybrid journals. Many journals that began as paid-access only journals now either have an option for authors to pay for their articles to be available via open access, or in some cases newer issues will be paid access but older issues will be available in an open access archive. Thus the present research addresses this phenomenon by gathering information about each journal included in the study.

### Author and country analysis

Since the choice of whether or not an article is made available through an open access option is primarily the author's prerogative, aside from when their affiliation mandate or funding agency policy requires self-archiving of the article, studies that focus on author analysis are also integral to the open access movement. In addition to article availability, Xia _et al._ ([2011](#xia11), p. 800) also compared first author status with other article attributes and found that faculty were more likely to '_publish longer articles, have more references, and collaborate more often than librarians_'. The differences in the publication patterns of different authors indicates that studies and services should take these differences into consideration in order to be more accurate and beneficial. The research at hand is also concerned with the publication patterns of library and information science authors, however, with a more narrow focus on articles about open access and including articles that were directly published into open access journals.

Country analysis is grouped with author analysis since it is based on the country of the authors who published articles, rather than the country of the journal or publishing body. There are few studies that focus on country analysis and so far there does not appear to be any within the library and information science context. More broadly though, Sotudeh and Horri ([2009](#sot09), p. 21) determined that '_a relatively broad range of countries participate in [open access] movement whether by universally sharing and maintaining their [open access journals], or by submitting their scientific outputs_'. This is promising since international support is required for the open access movement to succeed and studies such as this one shed light on which countries are flourishing in terms of open access practices as opposed to countries which may need more information and support.

### Summary

The above studies lay an integral foundation for the current study; however, no research conducted to date has taken into consideration all aspects under investigation within this context. Considering a substantial difference (25%) in the findings of the two studies that tabulated the proportion of library and information science publications available as open access resources ([Way, 2010](#way10); [Xia _et al._, 2011](#xia11)) and given that each of these studies only collected data for single year periods, there is a clear need for further investigation. More information is needed to determine whether or not the open access movement is gaining momentum and the level of support required to promote open access. This study aims to fill some gaps in the research and gain a more comprehensive understanding of the longitudinal trends in publications by library and information science authors about open access.

## Method

This quantitative study gathered multiple points of data concerning scholarly articles about open access written by library and information science authors in order to better understand their open access publication patterns. Similar studies selected top-ranked library and information science journals from directories in order to perform their searches within this discipline ([Way, 2010](#way10); [Xia _et al._, 2011](#xia11)). However, for the purposes of the current study, searching a subject database was deemed more appropriate, which is the method utilized by Duzyol _et al._ ([2010](#duz10)). _Library and Information Science Abstracts_, was used as the primary database in the field, since it indexes open access, paid access and hybrid journals. This search was conducted between January and March 2014.

### Data collection

The search in _Library and Information Science Abstracts_ was conducted by requiring that _open access_ was found in the title of the record of English-language articles that were considered peer reviewed published in scholarly journals within the specified year range. Articles found by this method that fit the search criteria were then searched in Google Scholar by full title in quotation marks to determine if paid access articles were available in an open access format ([Mukherjee, 2009a](#muk09); [Norris _et al._, 2008](#nor08); [Way, 2010](#way10); [Xia _et al._, 2011](#xia11)), as well as which self-archive format was used (preprint, postprint, PDF). Journal titles from these articles were searched in the SHERPA/RoMEO database or on the Websites of the journals themselves, if not available in this database, to determine information about journal types and self-archiving policies ([Laakso, 2014](#laa14)). Table 1 indicates the type of data gathered from each article that fit the criteria as well as how it was found.

<table><caption>Table 1: Data collection and categorization</caption>

<tbody>

<tr>

<th>Information type</th>

<th>Specifics</th>

<th>Notes</th>

</tr>

<tr>

<td>Bibliographic information</td>

<td>Title, author, year of publication, journal, number of pages, number of references</td>

<td>Most information was included in the bibliographic record of each article. Unique references were counted manually when not included. Article title and author name were used to retrieve further information.</td>

</tr>

<tr>

<td>Article type</td>

<td>Research, case study, review, other</td>

<td>Article was reviewed when type of publication was not specified. Research included a method section, case studies were self-identified, reviews regarded other pertinent literature and research, other included editorials and conceptual papers.</td>

</tr>

<tr>

<td>Article availability</td>

<td>Open access, paid access, both</td>

<td>Determined by searching Google Scholar, followed by Google if not found, without university proxy.</td>

</tr>

<tr>

<td>Article self-archive format</td>

<td>Preprint, postprint, PDF, not applicable</td>

<td>Determined by reviewing the archived article file when found. Not applicable was automatically applied to directly open access articles.</td>

</tr>

<tr>

<td>Endorse OA</td>

<td>No, yes, uncertain</td>

<td>Determined by reviewing the article. Uncertain meant that both benefits and disadvantages of open access were listed with a neutral tone.</td>

</tr>

<tr>

<td>Journal type</td>

<td>Open access, paid access, hybrid</td>

<td>Determined by searching for each journal in the SHERPA/RoMEO database or conducting a Web search for journals not found in this database.</td>

</tr>

<tr>

<td>Journal self-archive policy</td>

<td>Preprint, postprint, PDF, unknown</td>

<td>Same method as journal type.</td>

</tr>

<tr>

<td>Author information</td>

<td>Number of authors, status, country</td>

<td>A search of the Web was conducted if author status or country was not listed within the paper itself.</td>

</tr>

</tbody>

</table>

### Criteria and categorization

Since this research is focused on the publication patterns of library and information science authors, any paper that did not have at least one author that fit this criterion was discarded. While conducting a search of the Web about the author it was determined whether they were an academic, librarian or other professional with a Master's degree in Library and Information Science or an equivalent degree. Academics included professors, lecturers and researchers, whereas librarians and other professionals included individuals with the librarian title even if they had faculty status within their organization, or professionals working within a related industry that held a library or information science degree. In some cases when researching the author it was discovered that the individual was both an academic and a librarian so a new category was created for this group (both).

Time was another limiting factor. Originally it was intended to gather information about articles published between 2001, the first year the Budapest Open access Initiative conference took place, and 2011\. However, there were no articles fitting the search criteria published in 2002 or earlier, so only relevant articles from 2003 until 2011 were included in this study. The cut-off of 2011 is important for validity, since authors may have an embargo of up to twenty-four months between when their article is published in a paid access or hybrid journal and when they can self-archive their article ([Laakso, 2014](#laa14), p. 482). As such, including more recent articles might skew the results.

Book and article reviews, as well as articles or communiqués of less than two pages, were not included in this study, since the interest was in original and substantive publications about open access. In total, 203 articles out of a possible 327 publications were deemed to fit the criteria for the present research.

### Data analysis

Excel and SPSS were used to derive descriptive statistics, determine if any correlations were present in the gathered data and produce trend analysis. Initially the interest was to investigate the correlations between article availability and all other variables, however, no significant correlation was found for this relationship.

## Results

### Endorsement and availability

Of the 203 articles gathered for this study, 190 (94%) appeared to endorse open access, with only one article that obviously did not endorse it, and 12 (6%) articles were ambivalent. Combining two methods of open access publishing, 122 (60%) of the articles were made available via open access. More specifically, 81 (40%) articles were paid access only, 74 (36%) were directly published in an open access journal and 48 (24%) of the originally paid access articles were self-archived (Figure 1).

<figure>

![Figure 1: Overall article availability](../p648fig1.png)

<figcaption>Figure 1: Overall article availability</figcaption>

</figure>

When regarding article availability per publication year, aside from 2007, direct publication in open access journals has always been below that of strictly paid-access articles (Table 2). However, when self-archived articles are included in the total open access article count, only 2004 and 2005 have more paid-access articles. The three highest years for combined open access are 2009 (27 articles or 13%), 2007 (22 articles or 11%) and 2011 (20 articles or 10%), whereas for the percentage of open access availability the highest years are 2003 (100%), 2007 (85%), 2008 and 2009 (60% each).

<table><caption>Table 2: Article availability per year  
</caption>

<tbody>

<tr>

<th rowspan="2">Year of  
publication</th>

<th colspan="5">Availablility</th>

<th rowspan="2">Total</th>

</tr>

<tr>

<th>Open  
access</th>

<th>Self-  
archived</th>

<th>Open  
access  
total</th>

<th>% open  
access</th>

<th>Paid  
access</th>

</tr>

<tr>

<td>2003</td>

<td>0</td>

<td>2</td>

<td>2</td>

<td>100%</td>

<td>0</td>

<td>2</td>

</tr>

<tr>

<td>2004</td>

<td>2</td>

<td>3</td>

<td>5</td>

<td>45%</td>

<td>6</td>

<td>11</td>

</tr>

<tr>

<td>2005</td>

<td>7</td>

<td>2</td>

<td>9</td>

<td>47%</td>

<td>10</td>

<td>19</td>

</tr>

<tr>

<td>2006</td>

<td>6</td>

<td>4</td>

<td>10</td>

<td>53%</td>

<td>9</td>

<td>19</td>

</tr>

<tr>

<td>2007</td>

<td>12</td>

<td>10</td>

<td>22</td>

<td>85%</td>

<td>4</td>

<td>26</td>

</tr>

<tr>

<td>2008</td>

<td>7</td>

<td>5</td>

<td>12</td>

<td>60%</td>

<td>8</td>

<td>20</td>

</tr>

<tr>

<td>2009</td>

<td>16</td>

<td>11</td>

<td>27</td>

<td>60%</td>

<td>18</td>

<td>45</td>

</tr>

<tr>

<td>2010</td>

<td>11</td>

<td>3</td>

<td>14</td>

<td>52%</td>

<td>13</td>

<td>27</td>

</tr>

<tr>

<td>2011</td>

<td>13</td>

<td>8</td>

<td>21</td>

<td>62%</td>

<td>13</td>

<td>34</td>

</tr>

<tr>

<td>Total</td>

<td>74</td>

<td>48</td>

<td>122</td>

<td>60%</td>

<td>81</td>

<td>203</td>

</tr>

</tbody>

</table>

The goodness-of-fit tests for cumulative distributions show linear trends for paid access, open access and self-archived articles (Figure 2). As Table 3 indicates, the coefficient of determination (R<sup>2</sup>) for all the distributions is highest for the linear equation, suggesting a steady and relatively slow pace of growth for publications on the topic of open access. Among these distributions, the self-archive category has the slowest growth rate, while the paid access category has the highest growth rate. However, when comparing the cumulative open access total, including self-archived articles, with that of paid access articles, the linear growth is comparable until 2006, after which open access takes the lead.

<figure>

![Figure 2: Cumulative distributions](../p648fig2.png)

<figcaption>Figure 2: Cumulative distributions</figcaption>

</figure>

<table><caption>Table 3: R<sup>2</sup> values for cumulative distributions  
</caption>

<tbody>

<tr>

<th></th>

<th colspan="2">

Linear  
_y = a x + b_</th>

<th>

Power  
_y = a x<sup>b</sup>_</th>

<th>

Exponential  
_y = a e<sup>bx</sup>_</th>

</tr>

<tr>

<th>Paid access*</th>

<td>0.9702</td>

<td>y=10.595 x - 21230</td>

<td>0.9236</td>

<td>0.9234</td>

</tr>

<tr>

<th>Open access*</th>

<td>0.9836</td>

<td>y=10.429 x - 20901</td>

<td>0.8813</td>

<td>0.8810</td>

</tr>

<tr>

<th>Self-archived</th>

<td>0.9705</td>

<td>y=6 x - 12020</td>

<td>0.9395</td>

<td>0.9393</td>

</tr>

<tr>

<td colspan="5">* For the purposes of calculation, eight data points were used</td>

</tr>

</tbody>

</table>

### Articles

As shown in Table 2, there has been an increase in publications about open access over the years and the three most recent years were the most productive, but the number of publications fluctuates depending on the year. Overall, 2009 was the most productive with 45 (22%) articles, followed by 2011 with 34 (17%) and 2010 with 27 (13%).

Figure 3 displays the percentages of articles by type. The majority of articles published about open access were original research, especially if the categories for case studies (39 or 19%) and research (90 or 44%) are combined (129 or 64%). Reviews were the second most common type (55 or 27%), with other types of articles being the least common (19 or 9%).

<figure>

![Figure 3: Overall article types](../p648fig3.png)

<figcaption>Figure 3: Overall article types</figcaption>

</figure>

Only forty-eight of the 129 initially paid access articles were archived (37% self-archive rate): of these, the majority were archived as postprints (25 or 52%), with similar amounts of preprints (12 or 25%) and PDFs (11 or 23%). It was also found that eight articles (17%) were archived in formats not expressly allowed by their publisher's self-archive policy, although at least one of these did receive permission to do so.

### Journals

Of the 203 articles, fifty-one (25%) were published in open access journals, two (1%) in strictly paid access journals and 150 (74%) in hybrid journals. Most articles published in hybrid journals were nonetheless strictly paid access (80 or 53%), forty-seven (31%) were self-archived and twenty-three (15%) were open access available from the journal itself. Of these twenty-three directly open access articles in hybrid journals, thirteen involved the authors paying for their article's open access availability, seven were archived by the journal and the policy for the remaining three could not be determined.

From the data gathered, there were a total of sixty-four unique journals of which sixteen (25%) were completely open access, two (3%) were strictly paid-access and forty-six (72%) were hybrid journals. All open access journals allowed authors to retain the copyright to their articles, whereas one of the paid access journals (_School Libraries Worldwide_) allowed joint copyright, and the copyright for the remaining journals was transferred to the publisher upon acceptance. The majority of hybrid journals (forty) allowed authors to choose to pay for open access, three journals allowed open access to archived issues (_American Archivist, Law Library Journal, Learned Publishing_), two journals have open access articles but the policy for the _Journal of Scholarly Publishing_ is unclear whereas the _Malaysian Journal of Library and Information Science_ requires the author to transfer their copyright to the journal for publication, and the _South African Journal of Library and Information Science_ became open access in 2009 but not all materials from before that year are freely available.

Aside from three journals (5%) whose self-archive policy could not be determined, all journals in this study allowed some form of archiving. The majority of journals allowed the postprint of the article to be archived (40 or 63%), followed by the PDF (20 or 31%), and just one journal allowed only the preprint to be archived (_Journal of Scholarly Publishing_). Of the twenty journals that allowed the final copy of the article to be self-archived (PDF), sixteen were open access journals, three were hybrid journals (_American Archivist, Libri, South African Journal of Library and Information Science_), and one was a paid access journal (_School Libraries Worldwide_).

### Authors

The publications gathered for this study had between one and five authors per article. There is a negative correlation between the total number of authors per article and the frequency of articles. The majority of articles had only one author (117 or 58%), followed by two authors (63 or 31%), three authors (18 or 9%), four authors (4 or 2%), and only one article was published with five authors.

The status of each author who published an article was combined so that if all authors were academics the author status was designated _academic_, and if all authors were professionals the author status was coded as _professionals_. However, if there was at least one academic and one professional, or if one author had been categorized as both, the author status was classified as _both_. Based on this classification, the majority of combined article author statuses were _professionals_ (105 or 52%), followed by _academics_ (56 or 28%), and the remainder were _both_ (42 or 21%).

Although there was no statistically significant correlation between combined author status and article availability, the results show that professionals published the largest number of articles about open access in open access journals (45 or 22%), followed by academics (16 or 8%) and articles published by both types of authors (13 or 6%) as shown in Figure 4.

<figure>

![Figure 4: Author status by article availability](../p648fig4.png)

<figcaption>Figure 4: Author status by article availability</figcaption>

</figure>

As shown in Table 4, of the 48 self-archived articles, professionals archived 21 articles (44%), academics archived 14 articles (29%) and both author types combined archived 13 articles (27%). A difference was found in the percentage of self-archived preprints, of which professionals accounted for 75%.

<table><caption>Table 4: Author status by article self-archive format  
</caption>

<tbody>

<tr>

<th rowspan="2">Self-archived  
format</th>

<th colspan="3">Status</th>

<th rowspan="2">Total</th>

</tr>

<tr>

<th>Academic</th>

<th>Professional</th>

<th>Both</th>

</tr>

<tr>

<td>Preprint</td>

<td>2</td>

<td>9</td>

<td>1</td>

<td>12</td>

</tr>

<tr>

<td>Postprint</td>

<td>8</td>

<td>9</td>

<td>8</td>

<td>25</td>

</tr>

<tr>

<td>PDF</td>

<td>4</td>

<td>3</td>

<td>4</td>

<td>11</td>

</tr>

<tr>

<td>Total</td>

<td>14</td>

<td>21</td>

<td>13</td>

<td>48</td>

</tr>

</tbody>

</table>

When self-archived articles are included in the total open access count (Figure 5), although all author statuses published more open access articles than paid access articles, professionals have the highest open access contribution (33%), followed by academics (15%) and both author types combined (13%). However for the open access advantage of each status, professionals are again in the lead (63%), closely followed by both (62%) and academics (54%).

<figure>

![Figure 5: Author status by total open access vs. paid access](../p648fig5.png)

<figcaption>Figure 5: Author status by total open access vs. paid access</figcaption>

</figure>

Some significant relationships were found between combined author status and article attributes. Author status compared with article type showed a highly significant correlation (?2 = 42.323, df = 6, p <0.000). As shown in Figure 6, professionals published the vast majority of case studies (31 or 79%) and reviews (40 or 73%), whereas academics published the majority of research articles (39 or 42%). Other article types had an even distribution.

<figure>

![Figure 6: Correlation between author status and article type](../p648fig6.png)

<figcaption>Figure 6: Correlation between author status and article type</figcaption>

</figure>

Using one way analysis of variance (ANOVA), significant results were found for combined author status compared to the length of articles (F = 5.827, df = 2, 200, p <0.003) and number of unique references (F = 7.132, df = 2, 200, p <0.001). As demonstrated in Figure 7 the mean number of pages for academics is 13.68 pages versus the mean number of pages for professionals at 10.10 pages. For articles written by both authors the mean is at 11.57 pages. Figure 8 shows a similar pattern with the mean number of unique references for academics being 30.32 versus professionals at 18.70, and the mean for articles written by both types of authors is 24.26.

<figure>

![Figure 7: Mean number of pages per author status](../p648fig7.png)

<figcaption>Figure 7: Mean number of pages per author status</figcaption>

</figure>

<figure>

![Figure 8: Mean unique number of references per author status](../p648fig8.png)

<figcaption>Figure 8: Mean unique number of references per author status</figcaption>

</figure>

A relatively significant relationship (at p<0.1) between combined author status and the number of authors per article was found (?2 = 8.384, df = 4, p = 0.078). Professionals published the majority of articles written by one author (66 or 56%) and two authors (32 or 51%), whereas articles published by three or more authors were more likely to be written by both authors (9 or 39%), as shown in Figure 9.

<figure>

![Figure 9: Correlation between author status and author count per article](../p648fig9.png)

<figcaption>Figure 9: Correlation between author status and author count per article</figcaption>

</figure>

As with author status, the summary of author country per article was determined by classifying an article that has multiple authors from the same country as that country, or authors from more than one country per article under multiple countries. Based on this classification the top six countries for publications about open access are the USA (91 or 45%), the UK (19 or 9%), India (17 or 8%), Canada (10 or 5%), Iran (8 or 4%) and Germany (5 or 3%).

Although the USA is in the lead in terms of overall publications about open access, it does not have the highest rate of making articles available as an open access resource. The USA published forty-three of a total of ninety-one articles in open access venues (47%), compared broadly to other countries which published seventy-eight of a total of 112 articles as open access resources (70%).

Figure 10 demonstrates the frequency of open access versus paid access articles in individual countries aside from the USA. Countries with a 100% open access publishing rate are China, Denmark, France, Germany, Greece, Iran, Japan, Netherlands, Nigeria, Norway, Peru, Philippines, South Africa and Spain. Countries with a 50% or higher open access publishing rate in descending order are India (88%), Canada (70%), Australia (67%), Korea, Malaysia and Slovenia (50%).

<figure>

![Figure 10: Combined author country per article by availability, excluding USA](../p648fig10.png)

<figcaption>Figure 10: Combined author country per article by availability, excluding USA</figcaption>

</figure>

## Discussion

### Endorsement and availability

Overall the findings of this study reported a higher open access percentage (60%) than previous studies which focused on publications by library and information science authors. This means a 33% increase over Way's findings ([2010](#way10)) and a 9% increase over those of Xia _et al._ ([2011](#xia11)). It might be argued that some of this difference is due to the inclusion of other article types since Xia _et al._ ([2011](#xia11)) only included research papers, but if only research articles are considered in the present study, the open access rate is even higher (66%), whereas non-research articles are reported at 56% (Table 5).

<table><caption>Table 5: Article type by availability</caption>

<tbody>

<tr>

<th rowspan="2">Type of  
publication</th>

<th colspan="3">Availability</th>

<th rowspan="2">% open access</th>

</tr>

<tr>

<th>Open access</th>

<th>Paid access</th>

<th>Total</th>

</tr>

<tr>

<td>Research</td>

<td>59</td>

<td>31</td>

<td>90</td>

<td>66%</td>

</tr>

<tr>

<td>Case study</td>

<td>23</td>

<td>16</td>

<td>39</td>

<td>59%</td>

</tr>

<tr>

<td>Review</td>

<td>27</td>

<td>28</td>

<td>55</td>

<td>49%</td>

</tr>

<tr>

<td>Other</td>

<td>13</td>

<td>6</td>

<td>19</td>

<td>68%</td>

</tr>

<tr>

<td>Total</td>

<td>122</td>

<td>81</td>

<td>203</td>

<td>60%</td>

</tr>

</tbody>

</table>

Another way to consider this is based on the years that data were collected, since Xia _et al._ ([2011](#xia11)) collected data for 2006 ([p. 794](#xia11)) and Way ([2010](#way10), p. 304) collected data for 2007\. In this case the current study has very similar findings to that of Xia _et al._ ([2011](#xia11)), 53% compared to their 51%, whereas the difference between this study and Way's ([2010](#way10)) is increased, 85% compared to 27%, despite Way having 14 of the 20 journals examined in common with the present study. This discrepancy is likely in part due to the present study's focus on articles about open access, but is also affected by Way's methodology, in which data was collected within a year of the original article publication ([2010](#way10), p. 304). As mentioned above, this might not allow sufficient time for authors to be able to self-archive their works due to journal embargo periods, which can be up to two years. The current study allowed a little over two years between the latest article publication year and data collection, and received similar results to Xia _et al._ ([2011](#xia11)) which waited a little over three years.

Nonetheless, a higher open access rate was expected for the present research since the articles included in this study were specifically about open access, 94% appeared to endorse open access within the article that they published and 98% of the articles were published in journals that allowed the author to self-archive. Since statistically significant relationships between availability and other variables were not determined in this study, further research needs to be conducted in order to understand why library and information science authors choose to publish their articles into open access or paid access journals and whether or not to self-archive their articles.

It was expected that the rate of open access publications would increase steadily over the years after the Budapest Open Access Initiative began, due to raised awareness and greater establishment of the benefits of open access. The cumulative distribution for availability shows a steady increase of combined open access through linear growth. Whether in the future the rate of growth of publications on open access remains linear, becomes exponential or diminishes remains to be seen.

### Articles

Although there is some fluctuation, overall the number of articles about open access is increasing; however, there was a spike in 2009 that was not matched by any other year in this study (Table 2). Due to the delay between when an event occurs and when articles would be published about it, the increase in articles is perhaps related to the first Open Access Day '_held on October 14, 2008_' and the related announcements '_beginning on August 4, 2008_' ([Curran, 2009](#cur09), p. 34). The increased awareness brought the open access movement to the forefront briefly, but then the rate of publications reverted to the steady increase seen in most other years.

The self-archive rate (37%) was lower than expected since 98% of these articles were published in journals that allowed the authors to archive their works. This indicates that other factors affect the author's choice to archive articles. Kim ([2010](#kim10), p. 1909) argues that '_concerns about copyright, extra time and effort, technical ability, and age_' are all factors that negatively affect author self-archive rates and that one solution to this problem is providing the right services.

For the articles that were archived, the library and information science field appears to have a preference for postprints (52%) compared to preprints (25%) and PDFs (23%). Antelman ([2004](#ant04), p. 378) compared the proportion of preprints and postprints in four disciplines, which ranged from predominantly preprints in mathematics (close to 90%) to predominantly postprints in engineering (close to 80%). Evidently the pattern of archiving articles varies greatly depending on the discipline. Kim ([2010](#kim10), )p. 1919 found this to be true from the qualitative perspective as well, stating that '_a self-archiving culture significantly affects the extent that professors self-archive their research_'. However, since Kim only surveyed professors in broad subject areas, the closest category being social science, it would be ideal to conduct a survey on library and information science authors specifically with consideration for the differences between author status.

Another interesting finding involves authors archiving their works in a format that was not generally allowed by their publisher (17% of self-archived articles). Permission can sometimes be granted for this practice by the publisher and it was clear that at least one of those eight articles did have such permission. Nonetheless, this points to one of the issues found in Kim's ([2010](#kim10), p. 1917) study, that the '_majority of interviewees expressed concerns and uncertainty about copyright issues involved in self-archiving_'. Thus not being aware of what rights authors have, not being sure what format is allowed based on what journal they published in and not being aware where to find this information is a problem that needs to be addressed when creating services that promote open access publication generally and self-archiving specifically.

### Journals

As mentioned in the results section, the majority of unique journals in this study are hybrids that have both open access and paid access elements to them (72%), followed by strictly open access journals (25%) and a minority of strictly paid-access journals (3%). It is very interesting that traditional journals are becoming a rarity, which indicates a change in culture even though only 9% of articles published in hybrid journals include the author paying for open access to their article. This low rate is likely because '_for each accepted article the author is charged a fee of from $300 to over $3,000 depending on the journal_' ([Drott, 2006](#dro06), p. 94). Even if it is also argued that '_almost every open access journal has a policy of permitting authors to request that fees be waived_' ([Drott, 2006](#dro06), p. 95), it is unclear whether this is also true of hybrid journals, which is another issue that needs to be studied further.

Like traditional journals, not all open access journals have the same impact ([Mukherjee, 2009b](#mub09)), thus authors should research the journals they choose to publish in. Although publishing directly into open access journals is ideal in the sense that authors retain their copyright, if a high quality open access journal is not available then subscription journals may be the better choice. When that is the case authors should at least ensure enough rights are retained to archive their articles. This is certainly feasible in library and information science since 98% of these articles were published in journals that allow self-archiving, with the remaining 2% having policies that could not be determined. This is 17% higher than the level shown in Laakso's ([2014](#laa14)) results from a multidisciplinary approach. Some of this discrepancy might be due to the smaller sample size and the inclusion of open access journals in the current study; however, when open access journals are excluded, 97% of the remaining articles could still be self-archived.

### Authors

Many comparisons can be made between the findings of this study and that of Xia _et al._ ([2011](#xia11)) in terms of author status correlations. This study confirms that there is no statistically significant correlation between article availability and author status, and that academics/faculty are significantly more likely to publish longer articles and have more references per article. In terms of author status compared with the total number of authors per article, academics/faculty were only somewhat more likely to collaborate (41% of articles had more than one author) than professionals/librarians (37%).

However, some differences between these studies should be noted. In the study by Xia _et al._ ([2011](#xia11)) study the open access percentages for professionals/librarians (52%) were much closer to that of academics/faculty (51%) than they were in the present study, in which professionals or librarians were found to have 63% and academics and faculty had 54%. Also, the total publications of faculty made up 69% ([Xia _et al._, 2011](#xia11), p. 796), whereas in this study the majority of articles were written by professionals (52%). This could be because Xia _et al._ ([2011](#xia11)) only considered the status of the first author rather than combined author status and because they excluded all non-research papers from their study, since faculty were found to be 68% more likely to publish research than other article types in this study.

Considering this study focused on articles about open access, it is less surprising that there were no differences found between author status and article availability, since all authors in this study are aware of the open access movement to some extent. The findings of other correlations with author status are fairly reasonable since academics are often expected to conduct more theoretical or conceptual research as part of their employment, and resources are allotted for this, which can account for longer articles, more references per article and being more likely to publish research articles.

Another interesting finding is that although the USA accounted for the vast majority of articles about open access (45%), it did not have the highest rate of open access availability at 47%, compared to every other country combined at 70%. Author surveys should be conducted to understand this gap between theory and practice in the USA, and, if resources allow, surveys of stratified samples from countries with different open access rates.

## Limitations

One of the limitations of this study is the accuracy of author status classification. Xia _et al._ ([2011](#xia11)) suggested that this classification could be inaccurate when not included within the paper or bibliographic record since a Web search usually reflects the author's current status, rather than that at the time of publication. In some cases, a more in-depth Web search may increase accuracy; however, limited information may be available about an author's previous employment.

Another constraint of the study may be the limited sample size. Data could be collected on all articles written by library and information science authors to increase the sample size. It may be also feasible to conduct a similar study across multiple sources and disciplines for comparative analysis and more generalizable results.

## Conclusion

This study served to develop a greater understanding of the characteristics of scholarly publications about open access by library and information science authors over multiple years. Additionally, by comparing the methods of this study with previous studies it was confirmed that at least two years are required between when an article is published and when data should be collected to reflect an accurate rate of open access availability.

Since the vast majority of these articles were published in journals that allow author self-archiving (98%), the low rate of self-archiving (37%) is indicative of a problem that requires further research in order to properly promote and support the practice. This study found a clear gap between theory (94% of articles endorsed open access in writing) and practice (60% of articles were published in an open access venue), which could be better understood by conducting a qualitative study that determines the motivations and barriers in authors' choices in terms of whether or not they publish directly into open access journals and whether or not they self-archive their articles. As Björk ([2004, Conclusions, para. 1](#bjo04)) articulates:

> Trying to get researchers to support the move towards open access, which most agree would be good for the advancement of science in principle, is like trying to get people to behave in a more ecological way. While most people recognise the need to save energy and recycle waste it takes much more than just awareness to get them to change their habits on a large scale. It takes a combination of measures of many different kinds.

Since the financial circumstances of libraries are unlikely to improve, a viable solution needs to be implemented consistently in order to achieve positive results. This solution will involve continuous iterations of research to determine the state of open access practice, what barriers are preventing further growth, and assessment of the benefits and limitations of current promotions and services. Considering the majority of publishers allow some form of author archiving, it is very important to address the low rate of self-archiving through further research and the creation of services for authors. The steady linear growth of publications on open access in library and information science is encouraging and it is hoped that with more research and more publications there will be a growing awareness among academics and professionals about the issues and challenges of open access.

## Acknowledgements

We would like to thank the editors and reviewers of _Information Research_ as well as all the proponents of the open access movement.

## About the authors

**Jennifer Grandbois** completed her Bachelor of Arts (Honours English Literature) at Concordia University followed by her Master of Library and Information Studies (Librarianship Stream) at McGill University. Her research interests include assessment of library services and resources, information literacy and information-seeking behaviour, as well as scholarly communications and the open access movement. She can be contacted at: [jennifer_grandbois@hotmail.ca](mailto:jennifer_grandbois@hotmail.ca).  
**Jamshid Beheshti** has taught at the School of Information Studies at McGill University for more than twenty five years. He has been the principal investigator and co-investigator on more than a dozen Social Sciences and Humanities Research Council of Canada grants, and has published widely in many international journals, including _JASIS&T, Information Processing & Management_, and _Education for Information_. He has recently co-edited two books, _The information behavior of a new generation: children and teens in the 21st Century_ (with Andrew Large, 2013), and _New directions in children's and adolescents' information behavior research_ (with Dania Bilal, 2014).

</section>

<section>

## References

*   Antelman, K. (2004). [Do open-access articles have a greater research impact?](http://www.webcitation.org/6Sbz60lJL) _College & Research Libraries, 65_(5), 372-382\. Retrieved from http://crl.acrl.org/content/65/5/372 (Archived by WebCite® at http://www.webcitation.org/6Sbz60lJL)
*   Baich, T. (2012). [Opening interlibrary loan to open access.](http://www.webcitation.org/6SbzGinO5) _Interlending & Document Supply, 40_(1), 55-60\. Retrieved from https://scholarworks.iupui.edu/bitstream/handle/1805/2733/Baich_Opening_post-print.pdf?sequence=1 (Archived by WebCite® at http://www.webcitation.org/6SbzGinO5)
*   Björk, B-C. (2004). [Open access to scientific publications - an analysis of the barriers to change.](http://www.webcitation.org/6SbzPqsE1) _Information Research, 9_(2), paper 170\. Retrieved from http://InformationR.net/ir/9-2/paper170.html (Archived by WebCite® at http://www.webcitation.org/6SbzPqsE1)
*   Budapest Open Access Initiative. (2012). [_Ten years on from the Budapest Open Access Initiative: setting the default to open._](http://www.webcitation.org/6Sbzagv01) Retrieved from http://www.budapestopenaccessinitiative.org/boai-10-recommendations (Archived by WebCite® at http://www.webcitation.org/6Sbzagv01)
*   Connaway, L. S., Dickey, T.J. & Radford, M. L. (2011). [If it is too inconvenient, I'm not going after it: convenience as critical factor in information-seeking behaviors.](http://www.webcitation.org/6SbzjAU2l) _Library and Information Science Research, 33_(3), 179-190\. Retrieved from http://www.oclc.org/content/dam/research/publications/library/2011/connaway-lisr.pdf (Archived by WebCite® at http://www.webcitation.org/6SbzjAU2l)
*   Curran, M. (2009). Open access day 2008: movement building in a Web 2.0 world. _The Serials Librarian, 57_(1), 34-39.
*   Davis, P. M. (2009). [Author-choice open-access publishing in the biological and medical literature: a citation analysis.](http://www.webcitation.org/6SbzuBxPX) _Journal of the American Society for Information Science and Technology, 60_(1), 3-8\. Retrieved from http://www.ecommons.cornell.edu/bitstream/1813/11647/1/Davis%20%28JASIST%202009%29.pdf (Archived by WebCite® at http://www.webcitation.org/6SbzuBxPX)
*   De Bellis, N. (2009). [Bibliometrics and citation analysis: from the science citation index to cybermetrics.](http://www.webcitation.org/6Sc04xWfH) Lanham, MD: Scarecrow Press, Inc. Retrieved from elisa.ugm.ac.id/user/archive/download/92386/f2878618868fbea525d0a79a8672fa46 (Archived by WebCite® at http://www.webcitation.org/6Sc04xWfH)
*   Drott, M. C. (2006). Open access. _Annual Review of Information Science and Technology, 40,_ 79-109.
*   Duzyol, G., Taskin, Z. & Tonta, Y. (2010). [_Mapping the intellectual structure of the open access field through co-citation analysis._](http://www.webcitation.org/6Sc16EM2V) Paper presented at IFLA Satellite Pre-conference: Open Access to Science Information Trends, Models and Strategies for Libraries, Crete, Greece. Retrieved from http://eprints.rclis.org/14910/ (Archived by WebCite® at http://www.webcitation.org/6Sc16EM2V)
*   Gumpenberger, C., Ovalle-Perandones, M. A. & Gorriaiz, J. (2013). [On the impact of gold open access journals.](http://www.webcitation.org/6Sc260Vtn) _Scientometrics, 96_(1), 221-238\. Retrieved from https://uscholar.univie.ac.at/get/o:246061.pdf (Archived by WebCite® at http://www.webcitation.org/6Sc260Vtn)
*   Kim, J. (2010). Faculty self-archiving: motivations and barriers. _Journal of the American Society for Information Science and Technology, 61_(9), 1909-1922.
*   Kousha, K. & Thelwall, M. (2006). [Motivations for URL citations to open access library and information science articles.](http://www.webcitation.org/6Sc2CZFRu) _Scientometrics, 68_(3), 501-507\. Retrieved from http://www.scit.wlv.ac.uk/~cm1993/papers/MotivationsURLcitationsOA.pdf (Archived by WebCite® at http://www.webcitation.org/6Sc2CZFRu)
*   Laakso, M. (2014). [Green open access policies of scholarly journal publishers: a study of what, when, and where self-archiving is allowed.](http://www.webcitation.org/6Sc2NpbvA) _Scientometrics, 99_(2), 475-494\. Retrieved from http://www.openaccesspublishing.org/green1/Lakso2014-GreenOAPoliciesAcceptedVersion.pdf (Archived by WebCite® at http://www.webcitation.org/6Sc2NpbvA)
*   Mukherjee, B. (2007). [Evaluating e-contents beyond impact factor - a pilot study selected open access journals in library and information science.](http://www.webcitation.org/6ScA5gtu4) _The Journal of Electronic Publishing, 10_(2). Retrieved from http://quod.lib.umich.edu/j/jep/3336451.0010.208?view=text;rgn=main (Archived by WebCite® at http://www.webcitation.org/6ScA5gtu4)
*   Mukherjee, B. (2009a). Do open-access journals in library and information science have any scholarly impact? A bibliometric study of selected open-access journals using Google Scholar. _Journal of the American Society for Information Science and Technology, 60_(3), 581-594.
*   Mukherjee, B. (2009b). Scholarly research in LIS open access electronic journals: a bibliometric study. _Scientometrics, 80_(1), 167-194.
*   Norris, M., Oppenheim, C. & Rowland, F. (2008). The citation advantage of open-access articles. _Journal of the American Society for Information Science and Technology, 59_(12), 1963-1972.
*   Sidorko, P. E. & Yang, T. T. (2009). Refocusing for the future: meeting user expectations in a digital age. _Library Management, 30_(1), 6-24.
*   Sotudeh, H. & Horri, A. (2009). Countries positioning in open access journals system: an investigation of citation distribution patterns. _Scientometrics, 81_(1), 7-31.
*   Way, D. (2010). [The open access availability of library and information science literature.](http://www.webcitation.org/6ScAF1c4f) _College & Research Libraries, 71_(4), 302-309\. Retrieved from http://crl.acrl.org/content/71/4/302 (Archived by WebCite® at http://www.webcitation.org/6ScAF1c4f)
*   Xia, J., Wilhoite, S. K. & Myers, R. L. (2011). [A “librarian-LIS faculty” divide in open access practice.](http://www.webcitation.org/6ScARg4Fr) _Journal of Documentation, 67_(5), 791-805\. Retrieved from ejournals.library.ualberta.ca/index.php/EBLIP/article/downloadSuppFile/16575/2263 (Archived by WebCite® at http://www.webcitation.org/6ScARg4Fr)
*   Xia, J. (2012). [Positioning open access journals in a LIS journal ranking.](http://www.webcitation.org/6ScAZClwG) _College & Research Libraies, 73_(2), 134-145\. Retrieved from http://crl.acrl.org/content/73/2/134.full.pdf+html (Archived by WebCite® at http://www.webcitation.org/6ScAZClwG)
*   Xia, J. & Nakanishi (2012). Self-selection and the citation advantage of open access articles. _Online Information Review, 36_(1), 40-51.
*   Yuan, S. & Hua, W. (2010). Scholarly impact measurements of LIS open access journals: based on citations and links. _The Electronic Library, 29_(5), 682-697.
*   Zuccala, A. (2009). [The lay person and open access.](http://www.webcitation.org/6ScAfwtev) _Annual Review of Information Science and Technology, 43_, 1-62\. Retrieved from http://individual.utoronto.ca/azuccala_web/AZuccala%282009%29_ARIST.pdf (Archived by WebCite® at http://www.webcitation.org/6ScAfwtev)

</section>

</article>