<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# The case for a risk-assessed approach to organizational records management in the oil and gas sector

#### [Laura Muir](#author)  
Department of Information Management, Aberdeen Business School, Robert Gordon University, Aberdeen, UK

#### [Fionnuala Cousins](#author)  
Information Systems and Global Solutions, Lockheed Martin, Aberdeen, UK

#### [Audrey Laing](#author)  
Department of Communication, Media and Marketing, Aberdeen Business School, Robert Gordon University, Aberdeen, UK

#### Abstract

> **Introduction.** Processes and practices for information management service delivery, including retention, retrieval and disposal of documents and records, are not easily quantified for benchmarking and performance improvement.  
> **Method.** A mainly qualitative approach was applied to evaluate records management services in oil and gas sector organizations. A case study, with data gathered from eleven organizations by questionnaire and interview, explored records management challenges facing the industry.  
> **Analysis.** Quantitative and qualitative analyses were carried out on the data. Given the small sample and the mainly qualitative approach, analysis of questionnaire responses was mainly descriptive. Content analysis was applied to the interview transcript data to identify emerging themes and issues.  
> **Results.** The challenges faced by organizations are human and cultural and are specifically related to unwillingness to accept ownership responsibility and fear of consequences of records disposal.  
> **Conclusions.** We argue that those responsible for information and records management must make a strong business case for investment in development of their people and services. This would avoid the consequences of failing to comply with 'legal holds' or e-discovery demands, thereby improving retrieval performance, business decision making and, ultimately, business performance. A risk-assessed approach to scheduling records retention and disposal is recommended.

<section>

## Introduction

Best ([2010, p. 61](#bes10)) defined information management as: '_The economic, efficient and effective coordination of the production, control, storage, retrieval and dissemination of information from external and internal sources, in order to improve the performance of the organization_' We also argue that, unless there is economic, efficient and effective coordination of the destruction of information, the problems of managing information in modern business organizations will continue to increase.

Information management benchmarking is a rare activity, partly attributed to its relative immaturity as a professional discipline and also because its activities are often difficult to measure as they involve decision-making and processes that vary widely between organizations (even within the same sector, such as oil and gas). In a typical organizational information technology or information systems department, performance is measured and quantitative data can be gathered and benchmarked internally (to measure performance improvement) or externally (to measure performance relative to other comparable services). Information management activities, such as records management service processes, are not typically measured. Measurement of performance in this area of the business is subjective and is most likely to be evaluated in generic user satisfaction surveys rather than any form of industry-wide benchmarking. These _opinions_ can be used to make improvements to the service but they do not provide weighty evidence for investing in the service or demonstrate a return on any investment made. Linking service delivery and organizational benefits and demonstrating improved performance using benchmarking can go some way towards making the business case for information management.

We present the findings from our information management benchmarking research, conducted in 2010, 2011 and 2012, for a small group of oil and gas sector organizations which included a study of their records management activities. Evidence gathered from information professionals representing the companies participating in the benchmarking study demonstrates the need to conquer the _records mountain_. Information/records management _solutions_ are offered by software vendors to address the problems of document and records management but our research shows that the problems are human and cultural rather than technological.

This paper explores the human barriers to economic, efficient and effective records management in a case study of oil and gas sector companies. It makes the business case for defining organizational records and implementing procedures for managing records which include risk assessment for retention and disposal with confidence.

## Literature review

The plethora of information available to business organizations makes information professionals' job a challenging one since they must manage information across a range of business units and formats. The growth of electronic data has greatly increased this information management challenge ([Becker and Rauber, 2011](#bec11); [No end in sight..., 2011](#imj11); [Micu, Dedeker, Lewis, Moran, Netzer, Plummer, _et al_., 2011](#mic11); [Tallon, 2010](#tal10)). The discipline of information management is a complex one, stemming from the fact that it is a '_multi-disciplinary, multi-faceted, integrative change management activity that has, as its subjects, fickle humans and messy information_' ([Flett, 2011, p. 92](#fle11)).

The importance of the development and maintenance of appropriate information management services must be supported by senior managers if they are to be valued and successful ([Tallon, 2010](#tal10)). Hase and Galt ([2011](#has11)) argue that this also applies specifically to systems for records management. Improved records management must start at the top, but full engagement at every level of the workforce is imperative for a system to succeed. Daum ([2007, p. 43](#dau07)) also points out the need for clear records management policy '_developed at the highest level of the organization_' and communication with staff to ensure accountability and engagement. User engagement is an acknowledged problem in records management, particularly when encouraging users to adopt best practice ([Bailey and Vidyarthi, 2010](#bai10)). Users may not see the benefits of adhering to a system which does not seem to have been designed with their needs in mind (ibid.) and is regarded as time-consuming and burdensome. According to Borglund and Öberg ([2008, p. 12](#bor08)), '_new methods and techniques must be found to minimise uncertainty that follows from the difficulty of defining use and users._'

Kooper, Maes and Roos Lindgreen ([2011, p. 195](#koo11)) believe that '_organizations with an instituted information governance process are more effective at seeking, collecting, processing and applying information and are getting more value from their and others' information sources_'. They state that many organizations lack an '_all-encompassing information governance policy_' and that those systems that are in place are often incomplete or ineffective ([2011, p. 196](#koo11)). Duranti ([2010, p. 81](#dur10)) concurs, stating,

> it is necessary that the international community of records professionals develop strategies, procedures and standards capable of meeting the challenge presented by the creation and maintenance of reliable records and the preservation of authentic records.

Bailey ([2011](#bai11)) found that '_investment in improvements to records management can realise significant and sustained financial ROI_ [Return on Investment]', although he was careful to point out that there are specific areas which provide more scope for such return than others. Similarly, Beck, Dionne, Koti, Loriss, McLain and Veal ([2010](#bec10)) contend that '_RIM [records and information management] is one of the most underestimated and effective tools at a company's immediate disposal._' While records management is often an unrecognised, even invisible, cost to an organization, the benefits are manifold ([Saffady, 2011](#saf11)), including reduced storage volume, reduced operating costs and increased retrieval effectiveness. Additionally, poor digital records management can leave many organizations vulnerable to law-breaking, albeit inadvertently ([Cumming and Findlay, 2010](#cum10); [Ingram, 2004](#ing04)) and so benefits may be realised in areas of compliance. This is particularly important in highly regulated industry sectors such as oil and gas. However, these benefits are not being effectively communicated to senior executives and this is attributed to lack of awareness and quantifiable evidence.

Best ([2010, p.67](#bes10)) identified the problems inherent in managing information over a long time period. In this situation, there may be challenges related to the use of a number of different systems for information management. These issues are relevant to many oil and gas companies subject to acquisitions and divestment and the resulting need to make different systems work as harmoniously as possible. He posed the question: '_even if information can be retained, linked and accessed, how can policies for destruction be devised which will effectively counter the tendency to retain matters indefinitely?_'

## Methods

In 2010 a knowledge transfer partnership supported by the Technology Strategy Board, was formed between Robert Gordon University and Amor Group, an organization providing business technology solutions, professional and managed services across a range of industries. A knowledge transfer partnership is created between industry and academia to foster knowledge exchange. It involves the employment of a knowledge transfer partnership associate to work on a project of strategic importance, utilising skills and knowledge from the university that are not usually available to the business partner. Part of the knowledge transfer partnership involved the establishment of an Information Management Energy Forum. This group brings together information management experts across the oil and gas industry. Information Management Energy Forum meetings established that there was a lack of performance measurement and identification of issues and best practice procedures across information management processes within the oil and gas industry and that benchmarking would be beneficial for service quality improvement. This presented a challenge: to develop what is typically a quantitative tool for capturing qualitative data.

The research methods were designed and developed in close consultation with Information Management Energy Forum members. The members attended an initial consultation meeting where information and ideas about the benchmarking of information management were exchanged and it was agreed that the focus would be on collaborative and internal performance benchmarking. Following a second consultation meeting, the Information Management Energy Forum agreed the scope of the research enquiry and the data collection methods.

For the first round of data gathering in 2010, interviews were conducted. Information Management Energy Forum members and non-member oil and gas companies were invited to participate in the interviews and six information management professionals agreed to participate. Interviews were conducted with the information manager (or equivalent) in five oil companies and one engineering service provider. The interview transcripts were coded and scoring scales were devised for each of the following six key areas of information management:

1.  Information management roles, responsibilities and relationships
2.  User satisfaction, expectations and service level agreement
3.  Information management skill, competency and behaviour model
4.  Information management systems and platforms
5.  Information management activities
6.  Retention and retrieval

These areas of information management were refined for the 2011 benchmarking study, based on the experiences of participants and researchers in the 2010 study and on feedback from the forum members about the main areas of interest for the future. The information management service areas for the 2011 benchmarking study were:

*   Customer service
*   Information management staff development
*   Information management control and demand
*   Retention, retrieval and destruction

These categories were refined further for the 2012 study as follows:

1.  Service measurement
2.  Staff development
3.  Service management
4.  Records management

<figure>

![Figure1: General benchmarking scoring system for information management service maturity](../p647fig1.jpg)

<figcaption>Figure 1: General benchmarking scoring system for information management service maturity</figcaption>

</figure>

Performance rating scores were assigned on a scale of 0 to 4 to represent the level of maturity of activity in each of the benchmarked information management performance areas from unspecified to optimal (Figure 1).

The scores represent performance in different aspects of the information management areas included in the benchmarking study: 0 (unspecified or no activity); 1 (developing - no activity, but is planned for implementation in the next 12 months); 2 (variable - activity is evident but limited or constrained); 3 (formalised - a formal scheme or process exists but impact is limited or not assessed or acted upon) and 4 (optimal - activity is fully implemented and integrated and impact is evident across the business).

Figure 1 illustrates the general benchmarking criteria and rating scores applied following analysis of the data. In the 2010 study, scores were awarded based on analysis of interview data by the researchers. These scores were awarded by the researchers based on responses to questions which were specifically designed to determine the maturity of the various service areas (A to F) across the information management function.

The second benchmarking study, conducted in 2011, involved the design of an online questionnaire for data collection. This was largely informed by the rich data which emerged from the interviews in the previous study. These data, along with further consultation with Information Management Energy Forum members, were used to create a questionnaire designed to probe information management processes in a more focused way. The questionnaire was completed by senior information management professionals in six energy companies. Four of the information management professionals represented companies which had been part of the interviews in 2010\. The dynamic nature of the oil and gas industry means it becomes meaningless to strive for consistent company participation. Indeed, one of the companies participating in both the 2010 interviews and 2011 benchmarking study had, in the meantime, acquired another energy organization. Subsequently, interviews were carried out with the six information management professionals who completed the questionnaire. These interviews focused on probing any ambiguity or lack of clarity in the questionnaire data and exploring reasons and actions taken. Data obtained from the questionnaire and interviews were scored in the information management service areas (1 to 4) in the 2011 study.

The third annual benchmarking study, conducted in 2012, applied the same approach as the 2011 study (using an online questionnaire and interviews) to gather data from participating oil and gas companies in four information management service performance areas (i. to iv. above). Representatives from ten companies completed the questionnaire and nine of these were interviewed. An additional company representative partially completed the questionnaire.

The scope of the research, while limited to a small number of companies, was in-depth, iterative and highly consultative, producing a rich set of data that enabled the research to explore the how and why questions about information management services in oil and gas sector companies.

The value of the research presented in this paper is principally an insight into organizational records management challenges rather than any academic interest in comparing performance across the industry.

## Results

The case study results presented in this paper are drawn from our benchmarking data collection, from interviews conducted in 2010 and from questionnaires and follow-up interviews in 2011 and 2012.

This paper focuses on the results and analysis of the records management activities in the benchmarking study, specifically retention, retrieval and destruction. Participating companies are referred to by alphabetical letter (_Company A_, _Company B_, etc.) so that their identity is not revealed or implied by association. It should also be noted that these attributions are not consistent with those in the benchmarking reports provided to the participating organizations.

To provide some context for these results, given that the organizational details are confidential, the information management departments in the participating companies provide a considerable range of services to a wide range of departments within their organizations. Appendix 1 shows the spread of the service delivered by participants in the 2012 study.

### Maturity of retention scheduling

Only one company was judged to have retention schedules designed and implemented to any real degree of maturity (Company G, a participant in the 2011 and 2012 study). The general approach tended towards saving all records across all formats. For example, Company B (2011) noted:

> Everyone is so busy. Thinking about what needs to be retained is not on the radar. They don't really care. Human nature is that if it's broken we'll find out about it and if it's not we won't.

In the 2012 study, respondents were asked to 'Choose the statement that best describes the maturity of your retention scheduling.' The results are given in Figure 2.

<figure>

![Figure 2: Retention scheduling in the participating organizations in the 2012 study](../p647fig2.jpg)

<figcaption>Figure 2: Retention scheduling in the participating organizations in the 2012 study</figcaption>

</figure>

Only two of the participants in the 2012 study reported full implementation and compliance. However, one of the respondents (Company G, 2012) qualified this in the interview:

> Our retention scheduling applies to a subset of information that we call records .. information that is of corporate importance or that has some legal or regulatory requirement to retain it for a certain period of time, no shorter than or no longer than, or it is of corporate historic significance or in some other way assists the business.

The three respondents who reported limited implementation of retention schedules gave the following explanations in the interviews, which illustrate the major cultural and organizational challenges they face. Company B stated that their problem is with implementation: '_I think we're quite mature; we have detailed schedules for every business unit. What we haven't done is implement them very well_' ( 2012). Similarly, Company A (2012) reported:

> We do have a schedule. Recently I went to departments to engage and turn into a format easy for them to use. Very difficult to get any buy-in, interest is limited at best. I did get it updated and published and had user training for the main people dealing with it, but as archiving comes down [to us] things are still not right, people are not using the schedule properly. We tried to implement it in that we would not accept anything that was incorrect but it is a real pain point for them. Boxes [of records] sit around, people say they don't have time or they push it back with permanent retention. Sometimes department managers will overrule and insist we archive stuff.

Company D (2012) avoids the problems reported by Company A:

> We don't destroy anything. I put a moratorium in here a long time ago because we don't have a handle on what we've got, is the best way to describe it. So it's less risk orientated to keep it than it is to destroy it.

The organization that did not have a retention schedule (and no plans to initiate one in the next twelve months) has the same problem as Company D: 'In many places I'm just trying to find out what we've got' (Company I, 2012).

For those that were planning to develop schedules in 2013, one respondent (Company F, 2012) openly stated that 'whether it actually happens is another matter' and explained that this would be influenced by the global business. The other respondent in this category (Company C, 2012) was asked what the motivation for the planned implementation was and elaborated: 'Storage costs. Ease of access to information that we do need to keep, it not being cluttered up with lots of other stuff. Basic compliance and [an] efficiency improving measure.'

### Effectiveness of the retention schedule

Regarding the overall effectiveness of retention, retrieval and destruction, the researchers asked two questions in the 2011 study:

1.  Please describe how you measure the effectiveness of policies and their implementation for the retention and disposal of records, and
2.  Is your retention, retrieval and destruction service defined such that you can measure its effect on the user?

For one company, these questions could not be answered as they have no policies on records management and generally retain everything by default. Two of the five companies who could answer the first question referred to their policy review cycle as the means of measuring effectiveness. However, this does not assure effectiveness because it is a theoretical exercise until implementation data are gathered. Three companies referred to gathering data on what is actually done but one of them did not have the tools in place to do this yet. Of the other two, only one (Company C, 2011) was in a strong enough position to meaningfully compare volumes scheduled for destruction with actual destruction volumes.

In response to the second question, the answers were, in most cases, speculative rather than definitive, at times to the point that there was clearly no definition of what the retention, retrieval and destruction process actually is. This possibly reflects a central issue in the problems the industry faces with information overload. When asked for examples of impact on the user, all the examples provided were scenarios in which something that had been destroyed became necessary again. Only one company specifically acknowledged that its business has yet to make the connection between their insistence on keeping everything and how hard it is to find what they need within the enormous collections of the organization.

Eight of the nine interview participants in the 2012 study who had a retention schedule were asked what is defined therein and the responses are illustrated in Figure 3.

<figure>

![Figure 3: The scope of participants' retention schedules 2012](../p647fig3.jpg)

<figcaption>Figure 3: The scope of participants' retention schedules (2012)</figcaption>

</figure>

Company A (2012) did not define a record, the stakeholders, the period after which the record would be destroyed, the record format or the process for contesting disposal. They illustrated the challenges faced in defining elements in the retention schedule:

> Even if we did define the process, that wouldn't change much. Until you have management support, trying to do things from the bottom up can be so challenging. Something like archiving is of limited interest when seen in the light of production value. Their own budgets do not suffer, IS [Information Services] pays.

This last point is of interest as it could be argued that, until the business units bear the costs of archiving their own documents, they will not see the value of disposal. Company G referenced the processes for disposal in their Business Management System (BMS) rather than in their retention schedule.

Cost of document and record storage has been raised as an issue for the organizations concerned, although these costs had not yet reached a critical stage. Although the companies acknowledged that destruction saved storage costs, this cost was not usually highlighted by organizations as a priority and so it was usually avoided by retaining everything. According to Company B (2011): 'I don't destroy anything. Destruction just saves storage costs which isn't on the radar yet. Not so expensive that [we] yet want to risk destroying something. [It] might become an issue in a couple of years'.

Company C (2011) also commented:

> Nobody [is] really focusing on the cost of retaining everything. They do witness the effect because [they] have to rummage through so much more stuff to find what [they] want, [they] can't see [the] wood for [the] trees, [but they] don't make that logical connection.

### Disposal of records

In the 2010 interviews, participants were asked, 'What is the level of adoption of retention schedules?' Company D answered: 'Awful! Compliance is lip service. Disposal is a draconian process'.

According to Company F (2011), 'In [the] UK, we don't dispose of anything. Keeping everything is not a policy; rather it is lack of a policy otherwise'.

The companies were found to be particularly inactive in electronic records management and confusion emerged with regard to hard copy documents as opposed to electronic data, with some organizations happier to dispose of hard copy, but less sure of company procedures, or legal requirements regarding retention or destruction of electronic documents.

Figure 4 illustrates the responses to the question, 'Who approves the destruction of records?' in the 2012 study (where more than one answer could be given).

<figure>

![Figure 4: Responsibility for approving destruction 2012](../p647fig4.jpg)

<figcaption>Figure 4: Responsibility for approving destruction (2012)</figcaption>

</figure>

In Company G (2012), approval for destruction is signed-off by the legal department when they sign off on the retention schedule. They have no intention of changing this policy but are frequently challenged in different countries where there are local ways of working and where they are reluctant to follow organization wide policy. For the other three organizations that do have approval processes, they have multiple channels for approval which can create bottlenecks or prevent the process being completed.

Respondents in the 2012 study were asked about measurement and reporting of destruction volumes (Figure 5).

<figure>

![Figure 5: Measurement and reporting of destruction volumes](../p647fig5.jpg)

<figcaption>Figure 5: Measurement and reporting of destruction volumes</figcaption>

</figure>

Only two of the organizations in the 2012 study reported that they measure and report volumes. However, one of these (Company B) stated that 'about 5% of what is eligible [for disposal] is destroyed. The business doesn't really care. They aren't very engaged in this at all.'

The other organization in this category, Company G, explained that:

> When electronic records are disposed [of] it reduces the volume in the document store but not sufficiently that it would impact on the baseline cost so there's not really anything to report. For physical records, centrally, as an act of disposing we would not report that to budget holders. It' the custodians of the physical records who tend to be the accountable party within each business for records management, they need to report to us that they've done their disposals and provide evidence.

The representative of Company G also expanded on the cost factor:

> If they [the custodians] then want to talk to the budget holders and say we saved you [x amount] per box per year then that's fine but it's not in the central records management remit. There is ongoing reporting to budget holders for retention costs, for disposition costs but it doesn't happen consistently across [the organization]. I don't think they care one iota.

This participant went on to say that responsibility for disposal should 'be very important, particularly from an e-discovery perspective because we want to manage our risks.' This final point from the representative of Company G is of particular interest: risk was a theme which emerged from the study but was not something that was specifically assessed in the records management process.

### Awareness of records management issues across the business

The participants were asked about the levels of awareness of records management issues in their company. Figure 6 illustrates company awareness in relation to statements about these issues.

<figure>

![Figure 6: Records management awareness across the business 2012](../p647fig6.jpg)

<figcaption>Figure 6: Records management awareness across the business (2012)</figcaption>

</figure>

The interview responses from the 2012 study provide further insight:

> You can see an ambivalence (sic) about the whole thing. I think they're aware of the driver being the total cost of retention, also if we don't choose to destroy them over time it will become more difficult to find things (Company B).

Company G attributed their organization's indifference to records management to a lack of employee education, stating 'There's not a high level of awareness'. In contrast, Company J described an annual 'housekeeping' day for their organization which provided an opportunity to increase awareness and provide training to employees:

> We have a specific day when we ask people to look through what they're keeping. If it's company records we give them a description of what a company record is and say to them you have to make a call as to what this is but if you don't know come and see us, or this isn't a company record so throw it out. We are going to change that once the retention schedules are properly linked to the DMS and part of the induction process will not just be for the DMS but will tell them more about retention and company records ... people have to be aware of what is a company record and what isn't (Company J).

### Ownership

Closely linked to the problems inherent in responsibility for retention and destruction, is the issue of ownership. According to Company D (2011):

> Information retrieval problems are down to fragmented systems, lack of standardised approach for cataloguing information, inconsistent metadata models, lack of Enterprise Search capability, and historically the organization fundamentally not having an understanding of information lifecycles, the mindset of _it's mine, I need it, once I don't need it anymore I'm just going to walk away_. Huge lack of ownership of info[rmation] is something we're really struggling with at the moment.

Company B (2011) noted: 'The retention schedule is issued to all divisional managers and includes their responsibility. They define which documents to retain; I just tell them the retention period.' This seems to reflect an attitude of offloading ownership by the representative of company B. Company G expressed the problem of ownership thus: 'The philosophy is, the 'business' are the data owners, IM [information management] is the custodian'

Company D (2010) commented that 'Currently the philosophy is that each individual in the business is responsible for their own documentation and data.' However, the representative from company D went on to say, '[There is a view] that engineers are good at engineering, not filing and IM [information management]. There are lots of people that think like that.'

Therefore, while notionally supporting individual responsibility, the representative of Company D also offered a view which appears to excuse a responsible attitude, company-wide, towards records management because people may not be good at it.

## Discussion

Overall, the research identified a range of records management challenges and issues. These challenges were exacerbated by the acquisition and complex divestment histories of companies and assets. Each company holds documents and data created under the policies of various currently defunct, assimilated or divested companies, leading to significant records management and information management challenges. As a result, information management professionals must conform to different retention and retrieval orthodoxies according to the provenance of any given document. Furthermore, new data may be retained in different formats from older data/documents, presenting a further issue of uniformity across storage media. Some of the greatest problems facing information management professionals emerged during the interview questions probing retention and retrieval procedures. Information management professionals were often at a loss to supply information about volumes of data or documents in off-site storage.

### Assessing the value of records

One of the challenges which emerged from the research was the difficulty facing document controllers and information managers in assessing which records or documents were important. Kooper _et al._ ([2011, p. 197](#koo11)) noted that giving meaning to information is, by its very nature, subjective 'since objectivism cannot deal with the human sense making'. In the context of digital preservation, Becker and Rauber ([2011, p. 1010](#bec11)) note that 'the decision maker has to achieve multiple competing objectives such as _minimize costs, ensure authenticity,_ and _provide online access_ while considering the contextual constraints of legislation, technology, and budgets'. They go on to make the point that organizational systems must be in place which allow objective decision making to take place, rather than placing this decision-making burden upon subjective human judgement.

An elementary lack of clarity emerged during the course of the interviews regarding the definition of a _record_. Company F noted that '[t]o define a schedule you must define records. We don't have a clear distinction between information and records'.

Standard ISO 15489-1:2001 supplies the following definition:

> information created, received, and maintained as **evidence and information** by an organization or person, in pursuance of legal obligations or in the transaction of business.

This definition allows records to be both _evidence and information_. Evidence is straightforward enough (any information item where retention and/or disposal is subject to a legal obligation is thus a record); information is less so. We surmise that evidence and information actually means that records are either evidence in pursuance of legal obligations, or information valuable in the transaction of business. This is aligned with the statement by Jones, Taylor, Stephens and Wallace ([2008, p. 31](#jon08)) that 'an organization's goal should be to retain _only_ those records needed to conduct business, to comply with the law and to reasonably preserve archival documentation' (original emphasis).

The ISO definition covers information creation, reception and maintenance. This implies that information that will at some future point qualify as evidence or have long-term or recognised potential future value is, by definition, a record from the very beginning. Indeed, Micu _et al._ ([2011, p. 216](#mic11)) note that 'in the new world, knowledge will exist before the business question is formed'. Both evidence and records should, therefore, be protected from alteration. Furthermore, another challenge emerges when we realise that information creation is often by committee, requiring multiple drafts, edits and comments. For this reason, items in the process of being created are not wholly protected from alteration. It is only after document lockdown that further alterations will initiate the creation of a new document rather than edits to the existing document. After lockdown, they may immediately be considered to be records or they may be considered to be documents in active use, to become records when they are no longer in active use.

To accommodate different companies' implementation of data management, document control, document management and records management, we suggest the ISO definition be revised to: 'information approved at point of release, maintained as evidence in pursuance of legal obligations by an organization or person, and/or as having current, long-term or recognised future potential value in the transaction of business'. This also clarifies the ambiguity around the original _evidence and information_ statement. This revision excludes the activities of document control in new and draft documents by requiring that information has been approved and released. It allows for document management by including current value. Should a consensus be reached that what might be called document management is not part of records management, the reference to _current_ value could be removed. In defining records it also communicates that anything that has no legal weight or no real or potential value can be disposed of.

### Signatures and ownership

In theory, there is no reason why a hardcopy signature should turn what might be a document into a record. Nevertheless, the need for a signature was raised and seems important for some companies, not for others. Blindly categorising all signed documents as records risks undermining the purpose of records management to protect and aid business activity. Treating all documents that require signatures as records does not have sufficient business justification. Nevertheless, a physical signature can have weight, depending on the organizational culture. As records management should be primarily about supporting current and future business activity, if the business wants to make all _wet signature_ documents records, despite understanding the associated storage and maintenance costs, then that is their business decision to make. As long as a retention schedule applies and is implemented, the impact is on storage costs only. Nevertheless, there is still an argument that one should move to change attitudes as the business is archiving for cultural rather than business reasons. Company F (2011) stated that they had made some progress in moving away from requiring 'wet signatures' in one document subset. They made the case that it is inefficient as well as counter-intuitive to have to print _born digital_ documents just so they can be signed and then scanned back into the system.

There were other key issues emerging from the interviews which demonstrated a consensus across all of the companies. The business or organization is generally considered to be the data owner (rather than the user, or the information management professional) and they are responsible for the signing off for disposal or retention. Information management professionals assume the role of information gatekeepers, rather than information owners. This makes sense, bearing in mind the legal implications associated with document ownership, especially when problems arise.

### The challenges of electronic records management

Despite the growth of digital records, hard copy documents are increasingly becoming a problematic cost consideration for information management professionals. Similarly, the 'near exponential growth in the volume of data that is being collected and held by firms' ([Tallon, 2010, p.121](#tal10)) brings its own information management challenges. Data already in storage must be environmentally maintained, may need to be transferred to cheaper storage media and may increase or decrease in intrinsic value ([Tallon, 2010](#tal10)).

Company G (2011) answered the question 'What is the procedure for retention of electronic documents?' with '[n]o policy as format is irrelevant, content determines retention. Not media driven'. This is commendable. However, this is the same company that does not have the resources to implement destruction as planned in the retention schedule. This statement then can be treated as a theoretical intent at best. The challenge is that companies simply do not know how to apply disposal scheduling to both electronic and hardcopy records and the business is largely unaware of the inherent financial, legal and reputational risks. Indeed a recent industry report on the benchmarking of records management found that 'electronic records remain a universal Achilles heel' ([Iron Mountain, 2010, p. 13](#irm10)).

Any effective retention programme must acknowledge that there will always be a range of formats to be managed for information management professionals. Nevertheless, there is a lack of technical and legal clarity concerning the points at which information is both officially and completely destroyed. This is before considering the problems of multiple copies of electronic documents outside the document management system (DMS), on shared and personal drives, on external media and on email servers. This is a huge challenge for information managers, which the researchers argue can only be fully addressed through a change of culture. The solution is training and educating the business and end users to recognise corporate ownership of records and appreciate the risk of financial and legal penalties that is introduced by retaining multiple _unofficial_ copies across formats. This recommendation of training is supported by other industry research: '_consistent, enterprise-wide staff training is an essential element of a best-practices based records management program(me)_' ([Iron Mountain, 2010, p. 10](#irm10)). This must be regular and ongoing to ensure all new and existing staff are trained and retrained.

Several of the organizations studied indicated that they had plans to improve records management or information management procedures _in the next 12 months_. There is an element of doubt over this kind of indicative planning; without any evidence of an improvement in practice, future plans can be judged to be meaningless. The problematic element of future intent is also indicated in an industry report ([Iron Mountain, 2010, p. 4](#irm10)), which refers to immature companies which '_claim to be committed to records management "next year"'_.

### Disposal processes

With regard to the disposal processes, this was an element of information management with which the companies struggled. The research found that most of the companies practised near total retention: Company A undertook no destruction, attributed to lack of resources, and Company B retained everything by default. Company F also practised near total retention. Company G was the only company to report on a current destruction project, but this applied only to hard copy documents (not electronic data) since they were unsure how to address this. One of the key findings emerging from the research project was that the implementation of a retention and destruction schedule that applies to both hard and soft copy is extremely challenging. The two biggest improvement opportunities in defining an efficient destruction process are to require business approval of the retention schedule only and to incorporate the destruction process into normal information management activity.

Weise ([2007, p. 19](#wei07)) makes an important point when discussing records disposal: '_business records should be destroyed in accordance with approved records retention schedules and in the **normal course of business**_'. The normal course of business seems to be where several of the participant companies stumbled. In any business there will be documents and records spread along the general lifespan from creation to destruction and thus disposal will be due at regular intervals. Nevertheless, only one company recognised this in its processes. Where destruction is treated as a sporadic and weighty task, it is not in the normal course of business. One of the consensus points emerging from the research is that when a destruction date approaches, there is a tendency for the information management department to ask the business if the records can be destroyed. This kind of _double checking_ is redundant. Company G, judged to have the most developed scheme, reported that the destruction process is '_managed by the_ [information manager equivalent], _destructions are approved by department heads, project managers and directors_'. This is healthier but again the business is required to approve at point of destruction. Even as the most developed in terms of records management, this company is not active in electronic retention or disposal. Regarding the challenge of electronic retention and retrieval, organizations must aspire to a culture change.

Ingram ([2004](#ing04)) gives several examples of companies failing to adhere to their own records management policies (often related to the destruction of electronic data) and subsequently facing litigation and large fines. In one example ([Saffady, 2011](#saf11)), Murphy Oil USA Inc. had failed to recycle backup tapes of email communication according to their own forty-five-day destruction policy. Legal argument over the discoverability of email messages (which should have been destroyed) subsequently involved significant time and money. This could have been avoided if the company had adhered to its own records management policy.

### Implementing workable destruction processes

The research found that many of the information management departments were inclined to retain all records across all data formats, avoiding an active retention or destruction decision. This is considered to be a mark of immaturity in records management for a number of reasons. First, total retention reduces visibility of what the business actually holds because the vast majority of items are neither retrieved by users, nor given any concerted attention by the organization's own information managers. Second, the impact of destruction is not just saving on storage costs: reducing the volumes held also affects search effectiveness and efficiency. Third, format is not just a challenge in destruction but also in obsolescence and usage scenarios. Therefore, there is no point in keeping data and information that is no longer accessible using current tools.

This research suggests that a further challenge exists: that of _implementing_ destruction. The organization concerned may not accept the opportunity cost of addressing the existing information build-up. The necessary compromise is to design and implement achievable and realistic records management policies henceforth. There are two basic requirements: retention schedules and destruction processes. Two of the companies studied had retention schedules but with overly difficult or entirely absent destruction implementation. They found it impossible to motivate the company to action and the effect was almost complete retention of all information. Clearly then, if only one of the two requirements is being practised, records management is not being addressed.

<table><caption>

Table 1: Record retention purposes (from [Hoke, 2011](#hok11))</caption>

<tbody>

<tr>

<th>Purpose</th>

<th>Description</th>

</tr>

<tr>

<td>Operational</td>

<td>For how long is the record necessary for organizational function?</td>

</tr>

<tr>

<td>Legal</td>

<td>For how long can legal bodies command presentation of the record?</td>

</tr>

<tr>

<td>Regulatory</td>

<td>For how long can regulatory bodies command presentation of the record as evidence of compliance?</td>

</tr>

<tr>

<td>Survival</td>

<td>What records are vital to organizational survival throughout the organizational lifetime and are not normal operational records?</td>

</tr>

<tr>

<td>Historical</td>

<td>What records have historical value and should be kept for posterity?</td>

</tr>

</tbody>

</table>

The findings from this research support Hoke's ([2011](#hok11)) discussion of Generally Accepted Recordkeeping Principles® (GARP®) which suggests that there are five purposes for which a record may be retained (Table 1). All documents should be considered in the light of these five points and the longest period applicable is the retention period. Companies should define this in the retention schedule developed with the business and obtain sign-off on the schedules from the relevant business areas. Then, as new documents are created, by definition they have a type and a retention schedule. This must then follow through to the destruction process. If an organization has approved a destruction schedule when it was devised or last reviewed, destruction following completion of the retention period has already been approved. There should be a procedure in place to establish a means by which upcoming destructions can be contested. In this way, the path of least resistance for the business is to allow destructions to generally proceed as planned.

We propose that a sixth purpose be added to Hoke's list, reputational: What is the probability and impact of negative consequences for the reputation of the business as a result of not retaining this record? A supplementary question would be: what action must be taken to minimise or eliminate that risk (such as retention beyond the legal or regulatory minimum period)? This must be a well informed decision based on a detailed risk assessment and take into consideration the need to comply with legal requests for information.

An example is in the case of an organizational dispute where exchange of information in electronic format is required during the pre-trial phase. Evidence may be obtained from the opposing party by means of discovery devices (e-discovery), including requests for documents. Legal holds are used by an organization to preserve all forms of relevant information when litigation is anticipated.

It could be argued that records risk assessment is embedded in the other GARP® purposes and that the previously given example is adequately covered by the 'legal' purpose. However, given the potential consequences of non-compliance for the reputation of an individual organization and the industry as a whole, a 'reputational' purpose may make this activity more prominent in a business sense.

It must be emphasised that our proposal for a reputational purpose is not a mechanism for destroying any potentially incriminating evidence. Instead it offers confidence that there is a valid risk-assessed reason for retention beyond the legal or regulatory requirements and avoids a 'just in case' total retention safety-net approach to records management.

## Conclusions and recommendations

Our empirical research has found many inconsistencies across information management in the oil and gas industry. The two greatest barriers to designing and implementing a complete records management programme in oil and gas are the multiple formats in use and a history of lack of business support for comprehensive effort. There is significant scope for improvement, especially in the field of records management.

Retention, retrieval and destruction are processes which are challenging for any business, especially as they are invisible processes, and often not prioritised by organizations. These processes have been further complicated by the tendency to acquire and divest companies in the oil and gas industry, leaving information management professionals with the extra challenge of integrating a range of formats and platforms. The cost of addressing a massive backlog of documents with nowhere to go, no clear value or owner and no guidelines around them is discouraging; rarely can any enterprise anticipate the extent of an eventual information overload. It would seem then that a compromise would be not to unrealistically require the business to sort through everything that has already accumulated, but to practise records management from now on. This is a much reduced burden to the business and will minimise the additional strain of incoming information and records by managing them properly. If the definitions and schedules are backward compatible, the organization then has the tools to approach the accumulated mass should business priorities move in favour of this. An ongoing programme of education and training should be undertaken for information management professionals and users to encourage accountability and ownership, as well as pointing out potential legal risks and financial penalties which may ensue if multiple copies of documents are retained.

DeSilva and Vednere ([2008](#des08)) recommend that the information manager, the relevant department manager and the legal department collaborate to develop and sign off retention schedules. This is to ensure that a balance is achieved in sorting valuable content from worthless content, maintaining legal compliance and devising a practical system to manage this. Involving these three roles protects from legal non-compliance, avoids expensive reacquisition of destroyed information, and ensures workable management processes. It is recommended that sign-off is obtained on the retention schedule, thus automating as much as possible the implementation of the retention schedule. Requiring additional sign-off at the time of destruction is redundant. The department managers should still, however, be informed of upcoming disposals, giving them an opportunity to choose to opt out, for which there should be a documented resolution procedure. In this way, the path of least resistance for the business is to proceed with destruction as planned but without undermining the business's ownership of that information.

While external standards are perhaps rather advanced to be of immediate use, the research suggests that they should be consulted. As stated earlier, rarely can the extent of any information overload be anticipated before it comes about. If companies such as those in this study consider standards such as BS 10008 ([BSI 2008](#bsi08)) and ISO 15489 ([BSI 2001](#bsi01)) now, they can implement robust records management that is more likely to withstand future pressures.

We also argue that retention schedules must be applied and implemented. So that they are given the respect they deserve, the schedules must be designed in such a way that they address Hoke's five records retention purposes ([Hoke, 2011](#hok11)) with one addition: reputational. Unless record retention and disposal is risk assessed and this is clearly quantified in the schedule, the reputational value of these assets will not be realised and acted upon. We argue that this additional 'reputational' purpose for retention is designed to provide '_records management with confidence_'. Further work is required to find an efficient and effective approach to reputational risk assessment of records so that it adds value rather than burden to organizations.

## Acknowledgements

The authors wish to acknowledge the support and participation of the members of the Information Management Energy Forum in this research.

## <a id="author"></a>About the authors

**Dr Laura Muir** is a Senior Lecturer in the Department of Information Management of Aberdeen Business School and a member of the Institute for Management, Governance and Society (IMaGeS) Research at Robert Gordon University, UK. She received her Masters and PhD degrees from Robert Gordon University, UK. Her research interests include information accessibility and usability and organizational information management. She can be reached at [l.muir@rgu.ac.uk](mailto:l.muir@rgu.ac.uk)  
**Fionnuala Cousins** is an Information Management Business Analyst at Lockheed Martin UK and an Associate Lecturer in the Department of Information Management of Aberdeen Business School at Robert Gordon University, UK. She received her Masters degree in Information and Library Studies from The University of Strathclyde, UK. She can be reached at [fionnuala.cousins@civil.lmco.com](mailto:fionnuala.cousins@civil.lmco.com)  
**Dr Audrey Laing** is a Lecturer in the Department of Communication, Media and Marketing and a member of the Research Institute for Management, Governance and Society (IMaGeS). She received her PhD in Bookselling Culture and Consumer Behaviour from Robert Gordon University. Her research interests include digital marketing, restorative servicescapes and human interaction with technology. She can be reached at [a.laing@rgu.ac.uk](mailto:a.laing@rgu.ac.uk)

</section>

<section>

## References

<ul>
<li id="als03">Alshawi, S., Irani, Z. &amp; Baldwin, L. (2003). Benchmarking: information and communication technologies. <em>Benchmarking: an International Journal, 10</em>(4), 312-324.
</li>
<li id="ana08">Anand, G. &amp; Kodali, R. (2008). Benchmarking the benchmarking models. <em>Benchmarking: an International Journal, 15</em>(3), 257-291.
</li>
<li id="bai11">Bailey, S. (2011). Measuring the impact of records management: data and discussion from the UK higher education sector. <em>Records Management Journal, 21</em>(1), 46-68.
</li>
<li id="bai10">Bailey, S. &amp; Vidyarthi, J. (2010). Human-computer interaction: the missing piece of the records management puzzle? <em>Records Management Journal, 20</em>(3), 279-290.
</li>
<li id="bec10">Beck, V., Dionne, M., Koti, I., Loriss, T., McLain, W. &amp; Veal, S. (2010). Making the case for merging document control and records management. <em>Information Management, 44</em>(6) 24-27.
</li>
<li id="bec11">Becker, C. &amp; Rauber, A. (2011). Decision criteria in digital preservation: what to measure and how. <em>Journal of the American Society for Information Science and Technology, 62</em>(6), 1009-1028.
</li>
<li id="bes10">Best, D.P. (2010). The future of information management. <em>Records Management Journal, 20</em>(1), 61-71.
</li>
<li id="bor08">Borglund, E.A.M. &amp; Öberg, L-M. (2008). <a href="http://www.webcitation.org/6T02Lgios">How are records used in organizations?</a> <em>Information Research, 13</em>(2) paper 341. Retrieved from http://www.informationr.net/ir/13-2/paper341.html (Archived by WebCite® at http://www.webcitation.org/6T02Lgios)
</li>
<li id="bsi08">Britisih Standards Institution. (2008). BS 10008:2008 <em>Evidential weight and legal admissibility of electronic information.</em> London: Britisih Standards Institution..
</li>
<li id="bsi01">Britisih Standards Institution. (2001). BS ISO 15489-1:2001 <em>Information and documentation – Records management.</em> London: Britisih Standards Institution.
</li>
<li id="cum10">Cumming, K. &amp; Findlay, C. (2010). Digital recordkeeping: are we at a tipping point? <em>Records Management Journal, 20</em>(3), 265-278.
</li>
<li id="dau07">Daum, P. (2007). Evolving the records management culture: from ad hoc to adherence. <em>Information Management Journal, 41</em>(3), 42-49.
</li>
<li id="des08">DeSilva, N. &amp; Vednere, G. (2008). The foundations for sound records management. <em>AIIM E-DOC Magazine.</em> May/June, 2008, 26-31.
</li>
<li id="dur10">Duranti, L. (2010). Concepts and principles for the management of electronic records, or records management theory is archival diplomatics. <em>Records Management Journal, 20</em>(1), 78-95.
</li>
<li id="fle11">Flett, A. (2011). Information management possible: why is information management so difficult? <em>Business Information Review, 28</em>(2), 92-100.
</li>
<li id="has11">Hase, S. &amp; Galt, J. (2011). Records management myopia: a case study. <em>Records Management Journal, 21</em>(1), 36-45.
</li>
<li id="hok11">Hoke, G.E.J. (2011, January-February). <a href="http://www.webcitation.org/6T01w40Hr">Shoring up information governance with GARP®.</a> <em>Information Management</em>. Retrieved from http://content.arma.org/IMM/January-February2011/IMM0111shoringupinformationgovernance.aspx (Archived by WebCite® at http://www.webcitation.org/6T01w40Hr)
</li>
<li id="ing04">Ingram, T. (2004). Help your clients deal with document retention and deletion. <em>Journal of Internet Law, 7</em>(10), 3-12.
</li>
<li id="irm10">Iron Mountain. (2010). <em>Best practices for records management: Iron Mountain compliance benchmark report.</em> Boston, MA: Iron Mountain.
</li>
<li id="jon08">Jones, T.M., Taylor, M.D., Stephens, D.O. &amp; Wallace, R.C. (2008). Going global: mapping an international records retention strategy. <em>The Information Management Journal, 42</em>(3), 30-36.
</li>
<li id="koo11">Kooper, M.N., Maes, R. &amp; Roos Lindgreen, E.E.O. (2011). On the governance of information: introducing a new concept of governance to support the management of information. <em>International Journal of Information Management, 31,</em>(3), 195-200.
</li>
<li id="ktp12">Knowledge Transfer Partnerships. (2012). <a href="http://www.webcitation.org/6T01Na6Rx"><em>What is a Knowledge Transfer Partnership?</em></a> Retrieved from http://www.ktponline.org.uk/faqs/ (Archived by WebCite® at http://www.webcitation.org/6T01Na6Rx)
</li>
<li id="mic11">Micu, A. C., Dedeker, K., Lewis, I., Moran, R, Netzer, O., Plummer, J. &amp; Rubinson, J. (2011). Guest editorial: the shape of marketing research in 2021. <em>Journal of Advertising Research, 51</em>(1), 213-221.
</li>
<li id="mor11">Moriarty, J.P. (2011). A theory of benchmarking. <em>Benchmarking: an International Journal, 18</em>(4), 588-612.
</li>
<li id="imj11">No end in sight to the digital data deluge. (2011). <em>Information Management Journal, 45</em>(5), 20.
</li>
<li id="saf11">Saffady, W. (2011). Making the business case for records management. <em>Information Management, 45</em>(1), 38-41.
</li>
<li id="tal10">Tallon, P.P. (2010). Understanding the dynamics of information management costs. <em>Communications of the ACM, 53</em>(5), 121-125.
</li>
<li id="wei07">Weise, C. (2007). Eliminate records clutter: getting rid of records and content isn't a bad thing. <em>AIIM – E-DOC, 21</em>(3), 19-23.
</li>
<li id="whi85">White, M. (1985). Intelligence management. In B. Cronin (Ed.), <em>Information management: from strategies to action</em> (pp. 19-35). London: Aslib.
</li>
</ul>

</section>

</article>

<section>

* * *

## Appendix 1

<figure>

![Services provided by participating information management departments across their organization 2012](../p647appendix.jpg)

<figcaption>Services provided by participating information management departments across their organization (2012)</figcaption>

</figure>

</section>