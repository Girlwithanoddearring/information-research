<header>

#### vol. 19 no. 4, December, 2014

Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# Information behaviour illustrated

#### Jenna Hartel  
Faculty of Information, University of Toronto, 140 St. George Street, Toronto, Ontario M5S 3G6, Canada

#### Abstract

> **Introduction.** Building upon an ongoing, arts-informed, visual research project about the nature of information, an existing set of original drawings of information were examined for insights into information behaviour. The study explored the questions: What can drawings of information suggest about information behaviour? What role can images of this kind play in information behaviour research?  
> **Method.** The draw-and-write technique was used. Research participants were asked to draw a response to the question _What is information?_ on one side of a 4" by 4" piece of paper and to complete the phrase _Information is…_ in words, on the back side. 293 drawings, coined _iSquares_, were collected.  
> **Analysis.** Analysis occurred in multiple stages and focused on the drawings. First, an iSquare of relevance to information behaviour was operationalized as one that contained a whole or partial human being. 126 iSquares qualified and were recast as _information behaviour squares_, or _ibSquares_. Then, thematic analysis was performed on the ibSquares in the light of the aforementioned research questions and the major concepts in information behaviour.  
> **Results.** Drawings of information frequently illustrate information behaviour. The most common visual themes were: the hands, the brain, a person thinking, an individual in a context, a twosome in information exchange and an information-rich social world. The themes embody central tenets of information behaviour.  
> **Conclusions.** TA limitation is the difficulty of discerning specific types of information activity in the drawings. Otherwise, the ibSquares provide an accessible, fresh perspective on information behaviour research and pedagogy, and expand the visual vocabulary of information behaviour beyond models.

## Introduction

For an ongoing, arts-informed, visual research project about the nature of information, 293 original drawings of information have been collected from graduate students at a North American iSchool. The goals of the project are to better understand how people visualize information; whether patterns exist across different populations; and how these images relate to conceptions of information made of words. Each illustration has been made in black pen on a compact, 4" by 4" piece of white paper, coined an _iSquare_. To date, the images have been analyzed to discover how information is rendered as a drawing ([Hartel, 2014a](#Har14a); [2013a](#Har13a), [2013b](#Har13b), [2012](#Har12)). It has been established that people construe information as pictures, link diagrams, grouping diagrams and symbols, among other types of graphic representations ([Engelhardt, 2002](#Eng02)) suggesting a complex multi-dimensionality. Parallel to the research, the pedagogical benefits of drawing information in the classroom have been realized ([Hartel, 2014b](#Har14b); [2014c](#Har14c)). The iSquare project has provided an accessible and inspiring alternative and complement to conceptions of information generated through the conventional, philosophical analytic approach that relies upon words alone.

Gazing across this collection of illustrations, attendees to the 2014 Information Seeking in Context conference would be delighted to see many human beings _dealing with information_, the phrase Savolainen ([2008](#Sav08)) uses as a catch-all for information behaviour, information activity and information practice. (For simplicity, the term information behaviour will be used, broadly construed, in this paper.) The fusion of information and information behaviour on the ibSquares also reflects Buckland's ([1991](#Buc91)) insight that information can be both noun and verb; as a noun it is _information-as-thing_ and as a verb (enacted by people) it is _information-as-process_ or _information processing_.

To explore the intriguing confluence of information and information behaviour in the drawings, the study at hand analyzes the iSquares for what they may reveal about information behaviour. The Information Seeking in Context conference seems the ideal venue to entertain a fresh perspective on information behaviour as generated through arts-informed, visual research. The intent is to answer these questions: _What can drawings of information suggest about information behaviour? What role can images of this kind play in information behaviour research?_

### Theoretical framework: arts-informed visual research

As an example of arts-informed methodology ([Cole and Knowles, 2008](#Col08)), this research project combined the systematic and rigorous features of qualitative methodologies with the artistic and imaginative qualities of the arts. Picasso captured the synergy of the crossroads when he said, _'I never made a painting as a work of art, it's all research'_. Arts-informed methodology sanctions unconventional modes of inquiry as research, such as poetry, literary prose, playwriting, visual arts, dance and music.

The study is also an instance of visual methods ([Prosser and Loxley, 2008](#Pro08)) that rely upon images as evidence, rather than words or numbers alone. Weber ([2008](#Web08)) states three compelling reasons to tap the visual realm: Images can be used to capture the ineffable, the hard-to-put into words; images can be used to communicate more holistically, incorporating multiple layers and evoking stories or questions; and, images can be more accessible than most forms of academic discourse. The field of information science and its information behaviour specialty can only benefit from more nuanced, holistic, layered and accessible ways of knowing through images.

Both arts-informed methodology and visual approaches have been making their way into information research and the information behaviour community. The 2013 annual meeting of the American Society for Information Science and Technology featured a panel, "Exploring the Complexities of Information Practices through Arts-Based Research" ([Given, O'Brien, Absar and Greyson, 2013](#Giv13)). Visual approaches have been championed by several information behaviour scholars ([Sonnenwald, 2005](#Son05); [Copeland and Agosto, 2012](#Cop12)). These recent efforts to study information behaviour creatively continue the mission of pioneering ethnographer Elfreda Chatman ([1996](#Cha96)), who sought richer, deeper understandings of information behaviour.

### Research method

Data were gathered by applying the draw-and-write technique ([Pridmore and Bendelow, 1995](#Pri95)). In the draw-and-write technique a researcher asks participants to draw something, followed by an interview, focus group or written questionnaire. Given the involvement of human subjects, the Office of Research Ethics at the University of Toronto approved the research design. Respondents were students pursuing a Master of Information or Master of Museum Studies degree, and aged from 22 to 58 years (most being between 24-32 years).

Two researchers visited the classroom for 15 minutes to oversee the data collection. For a consistent outcome, participants were given a black pen and a 4" by 4" square of white paper, the iSquare. They were then asked spontaneously to answer the question _'What is information?'_ in the form of a drawing, and on the back side of the paper to complete the phrase _'Information is…'_. The 293 iSquares, gathered from several classes and collectively referred to as the iSquare corpus, can be seen in their entirety at the project website ([www.iSquares.info](http://www.iSquares.info)).

Though the draw-and-write technique generates visual _and_ textual data in tandem, the drawings on the front sides of the iSquares were deemed of greatest interest for the Information Seeking in Context conference, since few studies of information behaviour employ visual data. For this paper, the written responses to the phrase _'Information is…'_ on the back sides of the iSquares were placed aside, a methodological decision discussed further in Hartel ([2014a](#Har14a)).

<figure>

![A student at the Faculty of Information, University of Toronto, participates in the study](../isic11fig1.jpg)

<figcaption>Figure 1: A student at the Faculty of Information, University of Toronto, participates in the study.</figcaption>

</figure>

### From iSquares to _ibSquares_

From the corpus of 293 iSquares, a preliminary step in the analysis process was to create a distinct sub-set of the data in which the drawings of information could be associated with information behaviour. Put differently, this meant re-casting iSquares as _'information behaviour squares'_ or _'ibSquares'_ due to their explicit features of interest to information behaviour scholars. To bring this special sub-set into sharper relief, it helps to first survey those iSquares that unequivocally come across as illustrations of information, not information behaviour.

#### Print artifacts

<figure>

![iSquares in which information is rendered as print artifacts](../isic11fig2.jpg)

<figcaption>Figure 2: iSquares in which information is rendered as print artifacts.</figcaption>

</figure>

Within the corpus there were many renderings of print artifacts (Figure 2), such as books and documents. They were usually expressed as singular items on the center of the page, but appeared as collections in bookcases, too. These visions of information reflect Buckland's ([1991](#Buc91)) information-as-thing, the longstanding material epicenter of information science, and also Dervin's notion ([1983b](#Der83b)) of information as objective, like a brick that can be put in a bucket. Methodologically speaking, students may have chosen print artifacts, in part, because they were relatively easy to draw.

#### Information technologies

<figure>

![iSquares in which information is rendered as information technologies.](../isic11fig3.jpg)

<figcaption>Figure 3: iSquares in which information is rendered as information technologies.</figcaption>

</figure>

Numerous iSquares were devoted to information technologies (Figure 3). Most featured computer hardware, though a few displayed more intangible aspects of information systems, such as the Internet and file storage, which are harder to visualize. Information systems were central in the early history of information behaviour research when a systems perspective was dominant. Dervin and Nilan's landmark _ARIS&T_ chapter ([1986](#Der86)) was a call to action to shift attention off information systems and onto the user; hence these drawings of information technologies were not included in our special sub-set.

#### Abstractions and patterns

<figure>

![iSquares in which information is rendered as abstractions and patterns](../isic11fig4.jpg)

<figcaption>Figure 4: iSquares in which information is rendered as abstractions and patterns.</figcaption>

</figure>

Many iSquares displayed abstractions and patterns (Figure 4). These markings brought to mind scientific and mathematical conceptions of information such as information theory, cybernetics, and Bates' preferred definition of information as, _'the pattern of organization of matter and energy'_ ([Bates, 2005b](#Bat05b)). The beautiful drawings capture information at its most transcendental, scientific and monotonous, placing them beyond the human, practical, and everyday concerns of the information behaviour enterprise; hence these were not deemed ibSquares.

#### Symbols

<figure>

![iSquares in which information is rendered as symbols.](../isic11fig5.jpg)

<figcaption>Figure 5: iSquares in which information is rendered as symbols.</figcaption>

</figure>

The corpus also included a handful of symbols, like question marks or the infinity sign. These drawings were perhaps the most economical response from participants, for a few pen strokes generated powerful, though ambiguous, ideas. Surprisingly, no obvious symbol for information appears across the data set, suggesting information science lacks a succinct image at its epicenter. Like the patterned iSquares shown in Figure 4, these images were hard to relate precisely to the domain of information behaviour.

#### Nature

<figure>

![iSquares in which information is rendered as nature.](../isic11fig6.jpg)

<figcaption>Figure 6: iSquares in which information is rendered as nature.</figcaption>

</figure>

Some participants in the study invoked nature as a trope for information; several iSquares of this kind took the form of landscapes, such as a rough sea or serene mountain range. All capture the power, vastness and mystery of information but did not have immediate relevance to information behaviour.

Against these visions of information as print artifacts, technologies, abstractions, patterns, symbols and nature an _ibSquare_ was operationalized as _one that contains a human being or a visual synecdoche in which a part of a human being represents the whole_. The presence of a person enabled the re-casting of the drawing and its associated idea from information to information behaviour. It seems almost too obvious to state that people are a qualifying and central marker of information behaviour scholarship and should characterize its visual vocabulary. The eminence of human beings is central to Bates' definition of information behaviour as _'the currently preferred term used to describe the many ways in which human beings interact with information, in particular, the ways in which people seek and utilize information'_ ([emphasis added] [Bates, 2010, p. 2381](#Bat10)). To sum up, in contrast with iSquares, ibSquares were _anthropocentric_, meaning, that they _'consider human beings the most significant entity of the universe'_ and _'interpret or regard the world in terms of human values and experiences'_ (["Anthropocentric"](http://www.merriam-webster.com/dictionary/), 2014). Of the 293 iSquares, 126 (43%) contained human beings or a body part and were therefore ibSquares.

### Thematic analysis

Next, _thematic analysis_ ([Braun and Clarke, 2006](#Bra06)) was performed upon the 126 ibSquares. Thematic analysis is a flexible qualitative method for identifying, analyzing and reporting patterns, called themes, within data. In thematic analysis epistemological assumptions must be made explicit at the outset, as follows. This study applied a _deductive_ form of _theoretical_ thematic analysis that viewed the ibSquares in light of major theories, concepts and trends of information behaviour research. The theoretical approach stands in contrast to _inductive_ thematic analysis that aims to solicit indigenous themes from the data. Further, the drawings were seen in a realist or essentialist manner that embraced their surface reality, rather than their socially-constructed, latent or metaphorical meanings. From this deductive and realist epistemological stance, the analysis process entailed: 1.) Familiarizing oneself with the data, 2.) Generating initial codes, 3.) Searching for themes, 4.) Reviewing themes, 5.) Defining and naming themes, and 6.) Producing the report.

### Results

Despite their common humanity, the 126 ibSquares were actually quite diverse. The human beings engaged in information behaviour were represented as whole bodies or, more often, in synecdoche as hands, brains, faces, heads, or busts. The drawings ranged from simple smiley faces or stick figures to artful and expressive portraits. People appeared solo, in dyads, or as groups. The actors were on the sidelines or stood front and center. The figure(s) occupied a stark canvas or inhabited a cluttered information environment. Upon applying the thematic analysis process, themes were identified. The themes and their associations with information behaviour are surveyed next.

#### The hand(s)

<figure>

![Appearances of the hand(s) in the ibSquares.](../isic11fig7.jpg)

<figcaption>Figure 7: Appearances of the hand(s) in the ibSquares.</figcaption>

</figure>

Several of the drawings represented a person through hand(s). This may be expected since, _'The hand has played a part in the creative life of every known society, and it has come to be symbolic or representative of the whole person in art, in drama, and in the dance'_ ([Alpenfels, 1955, p. 4](#Alp55)). For example, in one ibSquare a hand with a paintbrush is poised to create a work of art and in another the hands carry a wide-open book of multimedia information resources. In all cases the hand(s) animate the ibSquare with vitality and suggest engagement with the material world. From the annals of information behaviour, this reflects the role of _action_ in Kuhlthau's information search process ([1988](#Kuh88)), such as taking a book off a library shelf. These ibSquares also channel Buckland's _information processing_, that is, the handling of information that is tangible ([1991](#Buc91)). More recently, Hektor ([2001](#Hek01)) has used the phrase _information activity_ instead of information behaviour, to emphasize the physical manipulation of information as done with the hand(s). Alpenfels continues, _'The hands are the organs of the body which, except for the face, have been used most often in the various art forms to express human feeling'_ ([1955, p. 14](#Alp55)). While emotions did not appear to run particularly deep in these ibSquares, the hand(s) effectively channel ability and agency on a material plane.

#### The brain

<figure>

![Appearances of the brain in the ibSquares.](../isic11fig8.jpg)

<figcaption>Figure 8: Appearances of the brain in the ibSquares.</figcaption>

</figure>

Another recurring motif was the brain. Uniformly, these entailed a roundish, lumpy cerebral cortex with symmetrical hemispheres and distinct, variegated folds. The brain is the physical structure considered to be the site of the mind, consciousness and thought, which are likely the associations to information sought by the creators of these drawings. The preponderance of brains affirms information science's affiliation with cognitive science, cognitive psychology and neuroscience. They are reminders, too, that information behaviour research treats the brain as a black box; we do not pursue anatomical and physiological explanations for information phenomena. Yet these ibSquares point to a nascent frontier in our specialty. Emergent techniques of brain imaging might someday afford a new perspective on information behaviour. Imagine, for a moment, seeing colorful, real-time images of the brain as a research subject searches for and uses information, revealing relationships and patterns not detected before.

#### A person thinking

<figure>

![Appearances of a person thinking in the ibSquares](../isic11fig9.jpg)

<figcaption>Figure 9: Appearances of a person thinking in the ibSquares.</figcaption>

</figure>

Numerous ibSquares harbored a person thinking. Two graphic sub-objects dominated the simple compositions: a head and its thought bubble. The latter is a convention used in comic books and cartoons to display an actor's subjective thoughts. What is on the mind of the figures varied: information, an idea! and ? to name a few contemplations.

These ibSquares of a person thinking reflect the cognitive viewpoint in information science and information behaviour. Anchored in Brookes' ([1980](#Bro80)) fundamental equation of information science [K (S) +ΔI = K (S +ΔS)] in which information produces a change in a knowledge structure, the cognitive perspective centers information phenomena in the mind. The same idea is captured in Buckland's _information-as-process_ ([1991](#Buc91)), which is _'When someone is informed, what they know has changed'_ (p. 351). In the cognitive tradition, Belkin ([1980](#Bel80)) generated important subjective concepts such as the anomalous state of knowledge (ASK) as the driver for information seeking. An example of the cognitive approach to information behaviour research is Todd's ([1999](#Tod99)) study of how girls' understanding changes as they process drug information. (If Todd were to turn his research on girls into a comic book, it might resemble this batch of ibSquares.)

Like the brains in Figure 8, the actors appear alone on the page, devoid of sociality and context. Nothing exists in the surroundings to influence what is happening inside the head. This chronic individualism and mentalism is the crux of the critique of the cognitive perspective that emerged in the 1990s ([Frohmann, 1992](#Fro92)) and that continues today ([Day, 2011](#Day11)).

Unlike the more sterile brain ibSquares (Figure 8) the portraits in Figure 9 convey feeling and personality, with wide-open eyes and sometimes a charming smile. Whether happy, ambivalent, or sad, they confirm Kuhlthau's ([1988](#Kuh88)) insight that affect is an important element of the information experience. That the majority of mouths were smiling also validates recent inquiry into information behaviours that are positive ([Kari and Hartel, 2007](#Kar07)), pleasurable ([Fulton, 2009](#Ful09)) and happy ([Tinto, 2013](#Tin13)).

An individual in a context

<figure>

![Appearances of an individual in a context in the ibSquares.](../isic11fig10.jpg)

<figcaption>Figure 10: Appearances of an individual in a context in the ibSquares.</figcaption>

</figure>

All the drawings in the ibSquare series of Figure 10 showed an individual in a context. The person stands front and center surrounded by his or her own thoughts and ideas, print information artifacts, media, information technologies, people, nature, culture and language. The compositions are dynamic, with lines and arrows connecting graphic sub-objects to suggest a process, influence or transformation.

On account of the assemblage of things surrounding each figure, these ibSquares reflect the theme of the Information Seeking in Context conference, _context_. Context is a disputed term with many interpretations ([Dervin, 1996](#Der96)). Here, context comes across as _objectified_, an approach that identifies the entities that impact information behaviour in any situation ([Talja, Keso and Pietiläinen, 1999](#Tal99)). These ibSquares do not capture context in its most fulsome expression, which might include epistemological, social, cultural, historical and temporal elements. Indeed, the context shown here is quite generic. A less felicitous but more precise banner for this set would be _'An Individual in an Objectified Generic Context'_.

By visualizing a person in a situation, these ibSquares expose the matter of space, an important contextual factor in information behaviour research. Savolainen ([2006](#Sav06)) asserts that spatial factors in studies of information seeking have been addressed in an objective, realist-pragmatic and perspectivist manner. The ibSquares that place the actor on the ground or in a room reflect the _objectifying approach_ which entails an actual geography and its barriers or enablers to information access. Those that treat space as a subjective realm in the actor's mind are the _perspectivist approach_. The latter, perspectivist view is a hallmark of Sonnenwald's _information horizon_ ([2005](#Son05)) and these ibSquares resemble the diagrams produced during the information horizon interview process.

#### A twosome in information exchange

<figure>

![ Appearances of a twosome in information exchange in the ibSquares.](../isic11fig11.jpg)

<figcaption>Figure 11: Appearances of a twosome in information exchange in the ibSquares.</figcaption>

</figure>

Another common picture manifestation was a twosome engaged in information exchange. On these cartoony ibSquares, partners face each other in conversation via speech bubbles or through messages that manifest as arrows or lines. Ironically, their mouths are opened wide for speaking though most figures lacked ears for listening. The substance of the conversations is not apparent, except one which captures a reference interview. A man says, _'Hey...do you know where I can get info on...'_ and a woman (perhaps a librarian in a neat skirt and sensible shoes), responds, _'I can tell you where to find it'_.

These ibSquares depict an information activity that Hektor ([2001](#Hek01)) calls _information exchange_. It entails the bi-directional acts of dressing (expressing information) and unfolding (experiencing information) in an ongoing reciprocal process. Reciprocity is underscored in the drawings by evenly distributed thought bubbles or an arrow with two heads. Another apt term for what appears is _information sharing_ and the vigor of the figures suggests they may be _super-sharers_ ([Talja, 2002](#Tal02)). These ibSquares are not placed under the banner of _communication_, which implies mass communication, a topic not usually broached in information behaviour research.

A different interpretation is that the twosome is experiencing an _information ground_ ([Pettigrew, 1999](#Pet99)), that is, an environment where people come together for one reason but end up socializing and sharing information spontaneously. Information grounds bring attention to locations, such as waiting rooms, hair salons and other unexpected places where they occur. However, no spatial or geographic clues exist on the drawings, so these glimpses of information grounds are incomplete.

ibSquares that show people chatting highlight the role of language and reflect a discursive perspective on information behaviour, as well. Discursive approaches view information phenomena as embedded in cultural, social, or organizational practices that reside in discourses ([Talja and McKenzie, 2007](#Tal07)). Champions of discursive approaches have argued that conversations, not information, should be the central concept in information science ([Tuominen, Talja and Savolainen, 2003](#Tuo03)); the ibSquares in Figure 11 advertise that very idea.

#### An information-rich social world

<figure>

![Appearances of an information-rich social world in the ibSquares.](../isic11fig12.jpg)

<figcaption>Figure 12: Appearances of an information-rich social world in the ibSquares.</figcaption>

</figure>

A final visual motif within the ibSquare collection showed an information-rich social world ([Strauss, 1978](#Str78)). People were numerous and appear as tiny faces or stick figures, resembling a colony of insects. In different ways the actors are placed in relation to information phenomena such as information technology, information resources and information institutions. Arrows and lines connect the graphic sub-objects, suggesting an iterative flow between the entities.

Different social groupings appeared in these ibSquares. Some bore the markers of a town or city, with a library and school at the center. One took the form of a hierarchical organizational chart, suggesting a workplace. A few were quite abstract and may have referred to the entirety of humanity. Only one ibSquare in this set illustrated an identifiable social world, a theatre troupe, which is also a powerful metaphor for social life ([Goffman, 1956](#Gof56)).

The ibSquares in Figure 12 construe information behaviour as embedded in community, reflecting a long-standing social approach to information behaviour that takes a group as the unit of analysis. The first textbook of library and information science established the relevance of sociality in a chapter entitled _'The Sociological Problem'_ ([Butler, 1933](#But33)). Early studies of scientific communication by Menzel ([1959](#Men59)) focused on scientific disciplines and there have since been research on marginalized populations ([Chatman, 1996](#Cha96)), youth ([Agosto and Hughes-Hassell, 2006](#Ago06)), the elderly ([Williamson, 1997](#Wil97)) and hobbies ([Hartel, 2003](#Har03); [2010](#Har10)), among sundry collectives. The metatheories that guide this line of research are known as _collectivism_ ([Talja, Tuominen and Savolainen, 2005](#Tal05)), _domain analysis_ ([Hjørland and Albrechtsen, 1995](#Hjø)), or _socio-cognitivism_ ([Jacob and Shaw, 1998](#Jac98)). In a current climate that favors more precise terminology, information behaviour within communities is referred to as _information practice_ ([Savolainen, 2008](#Sav08); [McKenzie, 2003](#Mck03)), and it is suggested in these ibSquares.

## Discussion

To review, of 293 original drawings of information gathered using the draw-and-write technique, 126 (43%) contain a whole or partial human being and can be re-cast as expressions of information behaviour, or _ibSquares_. By applying thematic analysis to the ibSquares, recurring visual motifs were identified as: the hand(s), the brain, a person thinking, an individual in a context, a twosome in information exchange and an information-rich social world. The themes can be linked to tenets of information behaviour. In light of the results, the discussion that follows revisits the research questions of the study: What can drawings of information suggest about information behaviour? What role can images of this kind play in information behaviour research?

The outcome of this project suggests the importance and omnipresence of information behaviour in information science today. It is remarkable that almost half of a data set in which participants envisioned information can likewise be cast as information behaviour. Perhaps information behaviour is _the_ unifying concept that lies at the epicenter of the broader field, more so than information itself. The confluence of information and information behaviour on the ibSquares is an invitation, of sorts, for information behaviour scholars to be more active in the theoretical debates concerning information. The oeuvres of Marcia J. Bates, Brenda Dervin, and Sanna Talja serve as inspiration and evidence that scholars who bridge information theory and information behaviour can make especially comprehensive and powerful contributions.

By focusing attention on the visual vocabulary of information behaviour research, the ibSquares set the stage for a critical visual analysis that _'takes images seriously...and thinks about the social conditions and effects of visual objects'_ ([Rose, 2007, p. 15](#Ros07)). To explain, for the past three decades findings in information behaviour have taken the form of models, that is, _'a tentative ideational structure used as a testing device'_ ([Bates, 2005a, p. 2](#Bat05a)). Our specialty is indelibly marked by the serpentine berrypicking path ([Bates, 1989](#Bat89)) and the smiling Sense-maker who leaps over a gap on the time-space continuum ([Dervin, 1983a](#Der83a)), among other visualizations. These models have been surveyed for their contribution to the understanding of information phenomena ([Wilson, 1999](#Wil99); [Järvelin and Wilson, 2003](#Jar03)) but they have not been problematized as representations themselves. This paper has not focused upon existing models such as berrypicking and Sense-making; rather, it has presented a different set of images in order to raise awareness of the visual discourse that permeates the information behaviour domain.

As was discovered with the iSquares ([Hartel, 2014b, 2014c](#Har14b)), the ibSquares have pedagogical applications, too. They can be useful as courses on information behaviour are becoming popular and required in some curriculums. When shown to students, these drawings of information behaviour can introduce an abstract topic, accommodate diverse learning styles and intelligences, complement the scholarly literature and seed lively classroom discussions. In this paper ibSquares have illustrated information activity, the cognitive viewpoint, context, information exchange, information ground, and many other fundamental lessons are possible. The most vigorous debate today — _information behaviour_ versus _information practice_ — can be engaged afresh through the drawings in Figures 9/10 and Figures 11/12, which are highly suggestive of each concept, respectively. In short, through the ibSquares we experience what Weber ([2008](#Web08)) claims as the benefits of visual research, that is, _'Images can be used to capture the ineffable, the hard-to-put into words…and images can be more accessible than most forms of academic discourse'_.

One limitation of the ibSquares and the visual medium must be noted. In all the drawings people are _'dealing with information'_ ([Savolainen, 2008](#Sav08)) and it is possible to infer informationactivity ([Hektor, 2001](#Hek01)) or information behaviour versus information practice ([Savolainen, 2008](#Sav08); [McKenzie, 2003](#Mck03)) based upon the number of figures present. Otherwise, it is difficult to say _with more precision_ just what is being done with information in each drawing. Using words, Hektor ([2001](#Hek01)) has characterized eight information activities of search and retrieve, browse, monitor, unfold, exchange, dress, instruct, and publish. In most ibSquares a viewer cannot distinguish among these distinct information activities. Unfortunately, there may never be a visual classification system for information behaviour at its most granular. Scholars of information behaviour can only envy other realms of human experience, such as Olympic sports, that can be displayed in neat, distinctive pictograms, as in Figure 13.

<figure>

![A sample of the pictograms for various sports of the summer Olympics.](../isic11fig13.jpg)

<figcaption>Figure 13: A sample of the pictograms for various sports of the summer Olympics. Unfortunately, specific information activities are not so easy to represent.</figcaption>

</figure>

## Conclusion

This study is one of the first applications of arts-informed, visual methods in information behaviour research and hopefully will not be the last. Many other research designs and questions can build upon the iSquare and ibSquares work. For example, whereas the data set at hand originally targeted "information", the draw-and-write technique could explore information behaviour directly by asking people to draw themselves "dealing with information" ([Savolainen, 2008](#Sav08)) and then commenting in words upon the matter. Alternatively, new insights could be gleaned about information behaviour in different contexts through comparative draw-and-write studies among different populations, following the tradition of socially-oriented information behaviour scholarship.

Arts-informed, visual methods also aim to stimulate the imagination, especially via unconventional formats that invite participation. Therefore, an exhibition of the 126 ibSquares accompanies this paper to enliven the venue of 2014 Information Seeking in Context conference. The sprawling, eclectic array of ibSquares should make a memorable impression. The installation is interactive; adjacent to the display is a table where people can draw, write, and talk about information phenomena.

<figure>

![The author's rendering of the ibSquare exhibition at the 2014 Information Seeking in Context conference](../isic11fig14.jpg)

<figcaption>Figure 14: The author's rendering of the ibSquare exhibition at the 2014 Information Seeking in Context conference.</figcaption>

</figure>

## Acknowledgements

Thanks to all students in my Winter 2014 course, INF2332: Information Behaviour, at the Faculty of Information, University of Toronto. Our learning experience together inspired this paper. Thanks also to: research assistants Karen Pollock and Rebecca Noone; proofreader Stephanie Power; HTML expert Ashleigh Burnet; and the anonymous reviewers who offered ideas to improve the manuscript.

<section>

## References

<ul> 
<li id="Ago06">Agosto, D. E. & Hughes-Hassell, S. (2006). Toward a model of the everyday life information needs of urban teenagers: Part 1, theoretical model, <em>Journal of the American Society for Information Science and Technology, 57</em>(10), 1394&ndash;1403.</li>
<li id="Alp55">Alpenfels E. (1955). The anthropology and social significance of the human hand, <em>Artificial Limbs, 2</em>(2), 4&ndash;21.</li>   
<li id="Ant14"><a href="http://www.webcitation.org/6RppmMh8z">Anthropocentric</a>. (2014). In <em>Merriam Webster</em>. Retrieved from http://www.merriam-webster.com/dictionary/(Archived by WebCite&reg; at http://www.webcitation.org/6RppmMh8z)</li>
<li id="Bat89">Bates, M. J. (1989). The design of browsing and berrypicking techniques for the online search interface. <em>Online Review, 13</em>(5), 407&ndash;424.</li>
<li id="Bat05a">Bates, M. J. (2005a).  An Introduction to metatheories, theories, and models.  In K. E. Fisher, S. Erdelez, and L. McKechnie (Eds.), <em>Theories of information behaviour</em> (pp. 1&ndash;24). Medford, NJ: Information Today.</li>
<li id="Bat05b">Bates, M. J. (2005b). <a href="http://www.webcitation.org/6Rppv4Tq9">Information and knowledge: an evolutionary framework for information science</a>. <em>Information Research, 10</em>(4) paper 239 [Available at http://InformationR.net/ir/10-4/paper239.html] (Archived by WebCite&reg; at http://www.webcitation.org/6Rppv4Tq9)</li> 
<li id="Bat10">Bates, M. J. (2010). Information. In M. J. Bates and M. N. Maack (Eds.), <em>Encyclopedia of library and information science</em> (3rd ed.) (pp. 2347&ndash;2360). New York: Taylor and Frances.</li>
<li id="Bel80">Belkin, N. J. (1980). Anomalous states of knowledge as a basis for information retrieval. <em>Canadian Journal of Information Science, 5</em>, 133-143.</li>
<li id="Bra06">Braun, V. & Clarke, V. (2006). Using thematic analysis in psychology. <em>Qualitative Research in Psychology, 3</em>(2), 77&ndash;101.</li>
<li id="Bro80">Brookes, B. C. (1980).  The foundations of information science. Part I. Philosophical aspects. <em>Journal of Information Science>, 2</em>(3&ndash;4), 125&ndash;133.</li>
<li id="Buc91">Buckland, M. K. (1991). Information as thing. <em>Journal of the American Society for Information Science, 42</em>(5), 351&ndash;360.</li>
<li id="But33">Butler, L. P. (1933). <em>An introduction to library science</em>. Chicago: University of Chicago Press.</li>
<li id="Cha96">Chatman, E. (1996). The impoverished life-world of outsiders. <em>Journal of the American Society for Information Science, 47</em>(3), 193&ndash;206.</li>
<li id="Col08">Cole, A. L. & Knowles, J. G.  (2008). Arts-informed research. In J. G. Knowles and A. L. Cole (Eds.), <em>Handbook of the arts in qualitative research: perspectives, methodologies, examples, and issues</em> (pp. 55&ndash;70). Thousand Oaks, CA: Sage Publications.</li>
<li id="Cop12">Copeland, A. J. & Agosto, D. E. (2012). Diagrams and relational maps: the use of graphic elicitation techniques with interviewing for data collection, analysis, and display. <em>International Journal of Qualitative Research Methods, 11</em>(5), 513&ndash;533.</li>
<li id="Day11">Day, R. E. (2011). Death of the user: reconceptualizing subjects, objects, and their relations. <em>Journal of the American Society for Information Science and Technology, 62</em>(1), 78&ndash;88, 2011</li>
<li id="Der86">Dervin, B. & Nilan, M. (1986). Information needs and uses. In M. E. Williams (Ed.), <em>Annual review of information science and technology</em>, Vol. 21 (pp. 3&ndash;33). White Plains, NY: Knowledge Industry Publications.</li>
<li id="Der83a">Dervin, B. (1983a). <em>An overview of sense-making research: concepts, methods and results</em>. Paper presented at International Communication Association Annual Meeting. Dallas, Texas.</li>
<li id="Der83b">Dervin, B. (1983b) Information as a user construct: the relevance of perceived information needs to synthesis and interpretation. In Ward, S.A., Reed, L. J. (Eds.), <em>Knowledge structure and use: implications for synthesis and interpretation</em> (pp. 153&ndash;184). Philadelphia: Temple University Press.</li>
<li id="Der96">Dervin, B. (1996). Given a context by any other name: methodological tools for taming the unruly beast. In P. Vakkari, R. Savolainen, and B. Dervin (Eds.), <em>Information seeking in context</em> (pp. 13&ndash;38). London: Taylor Graham.</li> 
<li id="Eng02">Engelhardt, Y. (2002). <em>The language of graphics: a framework for the analysis of syntax and meaning in maps, charts and diagrams </em>. (Unpublished doctoral dissertation). Institute for Logic, Language and Computation, University of Amsterdam, The Netherlands.</li>
<li id="Fro92">Frohmann, B. (1992). The power of images: a discourse analysis of the Cognitive viewpoint.  <em>Journal of Documentation, 48</em>(4), 365&ndash;386.</li>
<li id="Ful09">Fulton, C. (2009). The pleasure principle: the power of positive affect in information seeking. <em>Aslib Proceedings, 61</em>(3), 245&ndash;261. </li>
<li id="Giv13">Given, L., O'Brien, H., Absar, R. & Greyson, D. (2013). Exploring the complexities of information practices through arts-based research. <em>Proceedings of the Annual Meeting of the American Society for Information Science and Technology, 50</em>(1), 1-4.</li>
<li id="Gof56">Goffman, E. (1956). <em>The presentation of self in everyday life</em>. New York: Doubleday.</li>
<li id="Har26">Hartel, J. & Thomson, L. (2011). Visual approaches and photography for the study of immediate information space. <em>Journal of the American Society for Information Science & Technology, 62</em>(11), 2214&ndash;2224. </li>
<li id="Har03">Hartel, J. (2003). The serious leisure frontier in library and information science: hobby domains. <em>Knowledge Organization, 30</em>(3/4), 228&ndash;238.</li>
<li id="Har10">Hartel, J. (2010). Hobby and leisure information and its user. In M. J. Bates, and M. N. Maack (Eds.), <em>Encyclopedia of Library and Information Science</em> (3rd ed.) (pp. 3263&ndash;3361). New York: Taylor and Francis.</li>
<li id="Har12">Hartel, J. (2012). A visual response to the perennial question: What is information? <em>Proceedings of the 2012 iConference</em>.</li>
<li id="Har13a">Hartel, J. (2013a). New views of information. Alternative event at the Conceptions of Library and Information Science (8) Conference, Copenhagen, Denmark.</li>
<li id="Har13b">Hartel, J. (2013b). The concept formerly known as information. <em>Proceedings of the Annual Meeting of the American Society for Information Science and Technology, 50</em>(1), 1-4.</li>
<li id="Har14a">Hartel, J. (2014a). An Arts-informed study of information using the draw-and-write technique. <em>Journal of the American Society for Information Science and Technology, 65</em>(7), 1349&ndash;1367.</li>
<li id="Har14b">Hartel, J. (2014b). Drawing information in the classroom. <em>Journal of Education for Library and Information Science Education, 55</em>(1), 83&ndash;85.</li>
<li id="Har14c">Hartel, J. (2014c). <a href="http://www.webcitation.org/6RppzMly0">iSquares: a new approach to research and teaching in information science</a> [Webinar]. In ASIS&T Webinar Series, retrieved from http://www.asis.org/Conferences/webinars/Webinar-2-7-2014-purchase.html (Archived by WebCite&reg; at http://www.webcitation.org/6RppzMly0)</li> 
<li id="Hek01">Hektor, A. (2001). <em>What's the use? Internet and information behaviour in everyday life</em>. Linkoping: Linkoping University. </li>
<li id="Hjo">Hjørland, B. & Albrechtsen, H. (1995). Toward a new horizon in information science: domain analysis. <em>Journal of the American Society for Information Science, 46</em>(6), 400&ndash;425.</li>
<li id="Jac98">Jacob, E, K. & Shaw, D. (1998). Sociocognitive perspectives on representation. <em>Annual review of information science and technology</em>, 33, 131&ndash;185.</li>
<li id="Jar03">Järvelin, K. & Wilson, T.D. (2003) <a href="http://www.webcitation.org/6Rpq21uVu">On conceptual models for information seeking and retrieval research</a>. <em>Information Research, 9</em>(1) paper 163 [Available at http://InformationR.net/ir/9-1/paper163.html] (Archived by WebCite&reg; at http://www.webcitation.org/6Rpq21uVu) </li>
<li id="Kar07">Kari, J. & Hartel, J. (2007). Information and higher things in life: addressing the pleasurable and the profound in Information Science. <em>Journal of the American Society for Information Science and Technology, 58</em>(8), 1131&ndash;1147.</li> 
<li id="Kuh88">Kuhlthau, C. C. (1988). Developing a model of the library search process: investigation of cognitive and affective aspects. <em>Reference Quarterly, 28</em>(2), 232&ndash;242.</li>
<li id="Mck03">McKenzie, P. J. (2003). A model of information practices in accounts of everyday-life information seeking. <em>Journal of Documentation, 59</em>(1), 19&ndash;40.</li>
<li id="Men59">Menzel, H. (1959). Planned and unplanned scientific communication (pp. 199-243). <em>Proceedings of the international conference on scientific information</em>, Washington, D.C.:  National Academy of Sciences, National Research Council.</li> 
<li id="Pet99">Pettigrew, K. E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behaviour among attendees at community clinics. <em>Information Processing Management, 3</em>(6), 801&ndash;817.</li>
<li id="Pri95">Pridmore, P. & Bendelow, G. (1995). Health images: exploring children's beliefs using the draw-and-write technique. <em>Health Education Journal, 54</em>(4), 473&ndash;488.</li>
<li id="Pro08">Prosser, J. & Loxley, A. (2008).  <a href="http://www.webcitation.org/6Rpq5JOBl"><em>Introducing visual methods</em></a>. Retrieved March 1, 2014 from  http://eprints.ncrm.ac.uk/420/1/MethodsReviewPaperNCRM-010.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6Rpq5JOBl)</li>
<li id="Ros07">Rose, G. (2007). <em>Visual methodologies: an introduction to interpreting visual materials</em> (2nd ed.). London: Sage.</li>
<li id="Sav06">Savolainen, R. (2006). <a href="http://www.webcitation.org/6Rpq7puX1">Spatial factors as contextual qualifiers of information seeking</a>.<em>Information Research, 11</em>(4) paper261 [Available at http://InformationR.net/ir/11-4/paper261.html] (Archived by WebCite&reg; at http://www.webcitation.org/6Rpq7puX1)</li> 
<li id="Sav08">Savolainen, R.  (2008). <em>Everyday information practices: a social phenomenological perspective</em>. Lanham, Maryland: Scarecrow Press.</li>
<li id="Son05">Sonnenwald, D.  (2005). Information horizons. In K. Fisher, S. Erdelez, and L. McKechnie (Eds.), <em>Theories of information behaviour a researcher's guide</em> (pp. 191&ndash;197). Medford, NJ: Information Today.</li> 
<li id="Str78">Strauss, A. (1978). A social world perspective. <em>Studies in Symbolic Interaction, 1</em>(1), 119-128.</li>
<li id="Tal07">Talja, S. & McKenzie, P. J. (2007). Editors' introduction. Special issue on discursive approaches to information seeking in context. <em>Library Quarterly, 77</em>(2), 97&ndash;108.</li>
<li id="Tal02">Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. <em>New review of information behaviour research, 3</em>, 143-160. </li>
<li id="Tal99">Talja, S., Keso, H. & Peitil&auml;inen, T. (1999). The production of 'context' in information seeking research: a metatheoretical view. <em>Information Processing and Management, 35</em>(6), 751&ndash;763.</li> 
<li id="Tal05">Talja, S., Tuominen, K. & Savolainen, R. (2005). "ISMS" in information science. <em>Journal of Documentation, 61</em>(1), 79&ndash;101. </li>
<li id="Tin13">Tinto, F. (2013). <em>Study into individuals' information sharing behaviour of 'happy information' </em>. University of Strathclyde, Glasgow, UK.</li>
<li id="Tod99">Todd, R. (1999). Utilization of heroin information by adolescent girls in Australia: a cognitive analysis. <em>Journal of the American Society for Information Science, 50</em>(1), 10&ndash;23.</li>
<li id="Tuo03">Tuominen, K., Talja, S. & Savolainen, R. (2003). Multiperspective digital libraries: the implications of constructionism for the development of digital libraries. <em>Journal of the American Society for Information Science and Technology, 54</em>(6), 561&ndash;569.</li>
<li id="Web08">Weber, S. (2008). Using visual images in research. In J. G. Knowles and A. L. Cole (Eds.), <em>Handbook of the arts in qualitative research: perspectives, methodologies, examples, and issues </em> (pp. 41&ndash;54). London: Sage Press.</li>
<li id="Wil97">Williamson, K. (1997). The information needs and information-seeking behaviour of older adults: an Australian study. In P. Vakkari, R. Savolainen and B. Dervin (Eds.), <em>Information seeking in context: Proceedings of an international conference on research in information needs, seeking and use in different contexts</em> (pp. 337&ndash;350). London, UK: Taylor Graham.</li>
<li id="Wil99">Wilson, T. D. (1999) Models in information behaviour research. <em>Journal of Documentation, 55</em>(3), 249&ndash;270.
</li>
</ul>
</section>

</article>