<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# Contextualising information practices and personal information management in mobile work

#### Leslie Thomson and Mohammad Jarrahi,  
School of Information and Library Science, University of North Carolina, Chapel Hill, NC 27599, USA

#### Abstract

> **Introduction.** People living in rural areas may face specific barriers to finding useful health-related information, and their use of information may differ from their urban counterparts.  
> **Method.** This random-digit dial telephone survey of 253 people (75% female) living in a rural, medically under-serviced area of Ontario, Canada, follows-up a previous interview study to examine with a larger sample issues related to searching for and using health information.  
> **Analysis.** Descriptive statistics were used to describe the sample and the distribution of responses to each question. Sex differences on key questions were analysed using the Chi-squared test.  
> **Results.** Respondents were as likely to find information on the Internet as from doctors, although several reported that they had no access to these resources. Many of those surveyed used the information they found to look after themselves or someone else, to decide whether to seek assistance from a professional health care provider, and/or to make treatment decisions. Echoing results from other studies, a significant proportion of women reported that they did not discuss with a doctor the information they found.  
> **Conclusions.** These findings have implications for Canadian government health policy, particularly the use of e-health strategies.

Traditional, centralised offices and workspaces have provided a common setting for information practices research ([Courtright, 2007](#Cou07); [Harris and Dewdney, 1994](#Har94)). One specific line of information practices inquiry concerns personal information management, individuals' daily activities of '_acquiring, organising, maintaining, retrieving, using, and controlling the distribution of information items_' ([Jones and Teevan, 2007](#Jon07)), both physical and digital, that they encounter. As personal information management is equally applicable to professional and non-professional instances, it might be considered an apt lens for studying mobile work; individuals who perform mobile work navigate various boundaries-spatial, temporal, social, informational, and otherwise (e.g., [Kakihara and Sorenson, 2001](#Kak01); [Urry, 2007](#Urr07))-so to access, use, and store their professional content successfully, and also plan and corral artefacts in order to enable their work in and across various settings ([Perry, O'Hara, Sellen, Harper, and Brown, 2001](#Per01)) and configurations ([Olson and Olson, 2014](#Ols14); [Sorensen, 2011](#Sor11)). (Here, the term mobile work is used to denote the professional work tasks of individuals who are required or enabled to transit more often than episodically between disparate locations-offices, homes, and coffee shops, for example.) Further, mobile work not only depends upon informational considerations like these, but is often itself inherently '_informational_' and knowledge-based ([Davis, 2002](#Dav02)).

Yet, despite this seeming congruence between the aims of information practices scholarship and the realities of mobile work, there is little discussion of the intricacies of information practices and personal information management activities in the context of mobile work. This short paper briefly reviews the largely socio-technological scope of mobile work research thus far, points to three possible dimensions of a more '_informationally holistic_' view of mobile work-accounting for various interactions beyond the technological-and describes a project now underway that is exploring mobile workers' information practices and their management activities. What shapes do mobile workers' information practices and activities take?

To date, information-related discussions of mobile work have been approached mainly from technological angles, often in the subfields of human-computer interaction and computer-supported collaborative work. Recently, Ciolfi and Pinatti de Carvalho ([2014](#Cio14)) and Erickson, Jarrahi, Thomson, and Sawyer ([2014](#Eri14)) have stated that studies of mobile work ought to account for work practices, mobility issues, technological concerns, and work-life boundaries as interrelated parts of mobile workers' day-to-day realities. Even inclusive of these four elements, however, research will still risk an incomplete, potentially abstracted, picture of mobile workers' experiences, presuming one is interested in gleaning more holistically informational insights of mobile work, beyond the technological. Three potential (equally interrelated) avenues by which information practices approaches could illuminate the growing body of mobile work research are outlined below; these suggestions represent only three possible dimensions of many within this particular context and should be altered and extended as research in this vein progresses.

First, as existing mobile work research and theorization remains relatively nascent ([Pica and Kakihara, 2003](#Pic03)), individual workers are not usually considered beyond their immediate work situations. Yet no one enters the professional realm an empty vessel, without the same sorts of '_personal information infrastructure_' that Marchionini ([1995](#Mar95)) notes affect information seeking; mental models, past experiences, and specific abilities are brought to and continually shaped through information encounters. Information practices and management preferences and tendencies manifest as personality traits ([Massey, TenBrook, Tatum, and Whittaker, 2014](#Mas14)) , '_styles_' ([Berlin, Jeffries, O'Day, Paepcke, and Wharton, 1993](#Ber93)), and dynamics ([Heath, Knoblauch, and Luff, 2000](#Hea00)) that come to bear in social and collaborative situations, common features of mobile work. Taken up by information researchers as group information management issues, these ideas may serve as a springboard for further investigation of the virtual settings of mobile work, or may even find application to considerations of the physical settings of mobile work.

Second, mobile work research has yet to probe the information activities and strategies of individual mobile workers. Instead, information-related discussions occur at higher levels, with a technological purview, not often delving into the roles of non-digital artefacts over the long-term or across different settings and situations (independent, collaborative, and collocated work, for example). Similarly, individual mobile workers' '_meta-level_' ([Jones, 2008](#Jon08)) activities, such as information organisation, storage, and discarding, across devices and formats, remain to be examined.

Lastly, mobile workers encounter and use information across multiple physical locations, and while some existing mobile work research accounts for setting (e.g., [Brown and O'Hara, 2003](#Bro03); [Liegl, 2014](#Lie14)), such discussions are not often detailed in their attention to sociospatial contexts, so key to information practices investigations (see [Mervyn and Allen, 2012](#Mer12), for a review of such literature). Liegl ([2014](#Lie14)) found that settings influence work processes like creativity, and Spinuzzi ([2012](#Spi12)) showed that different individuals perceive of the same setting differently, raising questions of whether and how individuals' information practices and activities are nurtured or confined, implicitly or explicitly, depending on the '_psychological and social ecology_' ([Marchionini, 1995](#Mar95)) that different locations engender. A mobile worker interacts with settings in ways that extend beyond logistics of resources and technologies, and '_informationally holistic_' studies could account for this.

This short paper has identified three potential crossroads between information practices scholarship and the context of mobile work, which are currently being investigated, altered, and refined in an exploratory pilot project that involves approximately 35-50 mobile workers in the United States. As mobile work itself and mobile work research are relatively new phenomena, this pilot study is gathering data from individuals across both diverse professions (consultancy, design, and academia, for example) and locations. At present, semi-structured interviews are being conducted that centre on-among other issues-mobile workers' professional needs; their salient information tools and artefacts; their information formats, upkeep, organisation, and storage; as well as their workspaces and their information activities therein. Interviews are expected to conclude in June, and a subset of this data (from 15-20 interviews in one state) will be analyzed thereafter, affording findings about the information practices and activities of mobile workers at a more encompassing yet finer level of detail than seems to currently exist. A formalised study, including interviews, observation, and a diary component with a targeted mobile worker population, will follow. Given that a small percentage, if any, of the present workforce consists of true '_digital natives_,' such an informationally inclusive investigation of mobile work seems appropriate.

<section>

## References
 
<ul> 

<li id="Ber93">Berlin, L.M., Jeffries, R., O'Day, V.L., Paepcke, A., and Wharton, C. (1993). Where did you put it? Issues in the design and use of a group memory.<em> Proceedings of the 1993 conference on Computer Human Interaction (CHI '93).</em> New York: ACM Press, 23-30.</li>

<li id="Bro03">Brown, B., and O'Hara, K. (2003). <em>Place as a practical concern of mobile workers. Environment and Planning A, 35</em>, 1565-1587.</li>

<li id="Cio14">Ciolfi, L., and Pinatti de Carvalho, A.F. (2014). Work practices, nomadicity and the mediational role of technology. <em>Computer Supported Collaborative Work Journal (CSCW), 23</em>(2).</li>

<li id="Cou07">Courtright, C. (2007). Context in information behavior research. <em>Annual Review of Information Science and Technology</em>(ARIST), 41, 273-306.</li> 

<li id="Dav02">Davis, G. B. (2002). Anytime/anyplace computing and the future of knowledge work. <em>Communications of the ACM, 45</em>(12), 67-73.</li>

<li id="Eri14">Erickson, I., Jarrahi, M., Thomson, L., and Sawyer, S. (2014). More than nomads: Mobility, knowledge work and infrastructure. Paper presented at the European Group for Organizational Studies colloquium, Rotterdam, The Netherlands.</li> 

<li id="Har94">Harris, R., and Dewdney, P. (1994). <em>Barriers to information: How formal help systems fail battered women.</em> Westport, CT: Greenwood Press. </li>

<li id="Hea00">Heath, C., Knoblauch, H., and Luff, P. (2000). Technology and social interaction: The emergence of 'workplace studies.' <em>British Journal of Sociology, 51</em>(2), 299-320.</li>

<li id="Jon08">Jones, W. (2008). <em>Keeping found things found: The study and practice of personal information management.</em> San Francisco, CA: Morgan Kaufmann.</li> 

<li id="Jon07">Jones, W., and Teevan, J. (Eds.) (2007). <em>Personal information management.</em> Seattle, WA: University of Washington Press.</li>

<li id="Kak01">Kakihara, M., and Sorenson, C. (2001). Expanding the 'mobility' concept. <em>SIGGROUP Bulletin, 22</em>(3), 33-37.</li>

<li id="Lie14">Liegl, M. (2014). Nomadicity and the care of place-On the aesthetic and affective organization of space in freelance creative work. <em>Computer Supported Collaborative Work Journal (CSCW), 23</em>(2).</li> 

<li id="Mar95">Marchionini, G. (1995). <em>Information seeking in electronic environments</em>. Cambridge: Cambridge University Press.</li>

<li id="Mas14">Massey, C., TenBrook, S., Tatum, C., and Whittaker, S. (2014). PIM and personality: What do our personal file systems say about us? <em>Proceedings of the 2014 conference on Computer Human Interaction (CHI '14).</em> New York: ACM Press.</li>

<li id="Mer12">Mervyn, K., and Allen, D. K. (2012). Sociospatial context and information behaviour: Social exclusion and the influence of mobile information technology. <em>Journal of the American Society for Information Science and Technology, 63</em>(6), 1125-1141.</li>

<li id="Ols14">Olson, J.S., and Olson, G.M. (2014). <em>How to make distance work work. ACM Interactions, 21</em>(2), 28-35.</li>

<li id="Per01">Perry, M., O'Hara, K., Sellen, A., Harper, R., and Brown, B.A.T. (2001). Dealing with mobility: understanding access anytime, anywhere. <em>ACM Transactions on Computer-Human Interaction,4</em>(8), 1-25.</li> 

<li id="Pic03">Pica, D., and Kakihara, M. (2003). The duality of mobility: Understanding fluid organizations and stable interaction. <em>European Conference on Information Systems (ECIS)</em>, 1555-1570.</li>

<li id="Sor11">Sorensen, C. (2011). <em>Enterprise mobility: Tiny technology with global impact on work (Technology, work, and globalization)</em> (1st ed.). London: Palgrave Macmillan.</li> 

<li id="Spi12">Spinuzzi, C. (2012). Working alone together: Coworking as emergent collaborative activity. <em>Journal of Business and Technical Communication, 26</em>(4), 399-441.</li> 

<li id="Urr07">Urry, J. (2007). <em>Mobilities.</em> Cambridge: Polity Press.</li>

</ul>

</section>

</article>