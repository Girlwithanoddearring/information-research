<header>

#### vol. 19 no. 4, December, 2014

</header>

# Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

<article>

<section>

> Thanks to Corin Nanton, of [MEETinLEEDS](http://www.meetinleeds.co.uk), the conference, meeting and events facilities of the University of Leeds, for the conversion of the papers to html. The papers were peer-reviewed for the Conference but have not been through the journal's copy-editing and final proof-reading and, in general, may not conform to the journal's style requirements and standards. The remaining papers will be published with Volume 20, No. 1, March, 2015

## Full papers

##### Fatmah Almehmadi, Mark Hepworth and Sally Maynard, [A framework for understanding information sharing: an exploration of the information sharing experiences of female academics in Saudi Arabia](isic01.html)  

##### Anna Suorsa and Maija-Leena Huotari, [Knowledge creation in interactive events. A pilot study in the Joy of Reading programme.](isic02.html)  

##### Brigit van Loggem, ['Nobody reads the documentation': true or not?](isic03.html)  

##### Helena Lee and Natalie Pang, [Responding to the haze: information cues and incivility in the online small world](isic04.html)  

##### Ching Seng Yap, Boon Liat Cheng and Kum Lung Choe, [Web 2.0 as a tool for market intelligence acquisition in the Malaysian hotel industry](isic05.html)  

##### Zinaida Manzuch and Elena Maceviciute, [Library user studies for strategic planning](isic06.html)  

##### Cecilia Gärdén, Helena Francke, Anna Hampson Lundh and Louise Limberg, [A matter of facts? Linguistic tools in the context of information seeking and use in schools](isic07.html)  

##### Gunilla Widén, Jela Steinerová and Peter Voisey, [Conceptual modelling of workplace information practices: a literature review](isic08.html)  

##### Ina Fourie and Heidi Julien, [Ending the dance: a research agenda for affect and emotion in studies of information behaviour](isic09.html)  

##### Jyoti L Mishra, [Factors affecting group decision making: an insight on information practices by investigating decision making process among Tactical Commanders.](isic10.html)  

##### Jenna Hartel, [Information behaviour illustrated](isic11.html)  

##### J. Tuomas Harviainen and Reijo Savolainen, [Information as capability for action and capital in synthetic worlds](isic12.html)  

##### Elke Greifeneder, [Trends in information behaviour research.](isic13.html)  

##### Natalie Pang, [Crisis-based information seeking: monitoring versus blunting in the information seeking behaviour of working students during the Southeast Asian Haze Crisis](isic14.html)  

##### Ariadne Chloe Furnival and Nelson Sebastian Silva-Jerez, [The general public's access and use of health information: a case study from Brazil](isic15.html)  

##### Jenny Bronstein, [Is this OCD?: Exploring conditions of information poverty in online support groups dealing with obsessive compulsive disorder](isic16.html)  

##### Marzena Świgoń, [Personal knowledge and information management behaviour in the light of the comparative studies among Polish and German students.](isic17.html)  

##### Lilach Manheim, [Information non-seeking behaviour](isic18.html)

## Short papers

##### Manca Noc and Maja Zumer, [Eliciting mental models of music resources: a research agenda](isicsp1.html)  

##### Melanie Benson and Andrew Cox, [Visual and creative methods and quality in information behaviour research](isicsp2.html)  

##### Geoff Walton and Jamie Cleland, [Information literacy in higher education - empowerment or reproduction? A discourse analysis approach](isicsp3.html)  

##### Leslie Thomson and Mohammad Jarrahi, [Contextualising information practices and personal information management in mobile work](isicsp4.html)  

##### Graeme Baxter and Rita Marcella, [The 2014 Scottish independence referendum: a study of voters' online information behaviour.](isicsp5.html)  

##### Martha Sabelli, [Health care information for youth in vulnerability contexts: designing a website with an interdisciplinary and participatory approach](isicsp6.html)  

##### Emma Dunkerley, David Allen, Alan Pearman, Stan Karanasios and Jeremy Crump, [The influence of social media on information sharing and decision making in policing: research in progress.](isicsp7.html)  

##### Chandramohan Ramaswami, Murugiah Murugathasan, Puvandren Narayanasamy, and Christopher S.G. Khoo, [A survey of information sharing on Facebook](isicsp8.html)

</section>

</article>