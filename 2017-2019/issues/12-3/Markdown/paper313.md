#### Vol. 12 No. 3, April 2007

* * *

# From activity to learning: using cultural historical activity theory to model school library programmes and practices

#### [Eric M. Meyers](mailto:meyerse@u.washington.edu)  
The Information School, 
The University of Washington  
Box 352840, 
Suite 370 Mary Gates Hall, 
Seattle, WA 98195-2840

#### Abstract

> **Introduction:** changes in educational policy and practice demand that we examine school library programmes from a new perspective. As a model that takes a developmental view of minds in context, Cultural Historical Activity Theory is particularly well suited to the study of school libraries and the learning that occurs therein. This paper focuses on the activity theoretic concepts of ‘contradictions’ and ‘expansive learning’ as they relate to the development of best practices.  
> **Method:** Developmental Work Research was applied as a guiding methodology in an intervention study of six high school libraries in Washington State, USA. Library activity is illustrated from multiple perspectives using a triangulated, qualitative approach.  
> **Analysis:** contradictions and tensions in the general school library activity system are identified, and an intervention was designed to facilitiate the development of expansive instruments. A case example illustrates a second level of analysis and specific points of intervention.  
> **Results:** analysis reveals that the tensions and contradictions provide opportunities for expansive learning on the part of the teacher-Librarians. The research team can use practitioners' zones of proximal development to guide the alignment of library programme goals and practices.  
> **Conclusions:** some limitations and future promise of the framework and its application are discussed. The proposed activity theory toolkit suggest a new way of exploring the practices of teacher-librarians, incorporating research evidence, professional expertise and reflective decision making.

## Introduction

Teacher-librarians work at the intersection of education and information science, attempting to balance the skills, roles and responsibilities of both teaching and librarianship. Adding to this tension, the context in which teacher-librarians work is rapidly changing because of new educational philosophies and practices, as well as the explosion of information made available through the Web ([Lonsdale 2003](#lon03)). A growing body of research indicates that information and communication technologies, information literacy education and systems of accountability for learning outcomes have created new challenges for teacher-librarians around the world ([Moore 2005](#moo05); see also, the [International Association for School Librarianship](http://www.iasl-slo.org/) Website.

The ability to adjust individual practice in the midst of reform is an essential skill for education professionals. Teacher-librarians work in an environment that will inevitably undergo periods of organizational and pedagogical transformation. Most reform efforts in the United States focus on classroom teachers, marginalizing or eliminating “specialists” and programmes that do not articulate their direct contributions to student achievement ([Hartzell 2001](#har01)). Furthermore, teacher-librarians are often physically isolated from colleagues who share similar job descriptions, limiting their ability to collaboratively diagnose tensions, respond to new priorities and models of governance and design new practice models.

One of the challenges that continues to plague practitioners and researchers is the application of theory to the practice of librarians in the field. Teacher-librarians need additional tools that will permit them to assess their own practice by identifying areas of strength and diagnosing areas of weakness and trouble, taking into account the wider school context. In a recent issue of _School Libraries Worldwide_ focused on evidence-based practice, Todd ([2006](#tod06)) called for an integrated approach to the research of school librarianship, moving beyond advocacy to the development of effective, reflective library programmes. This paper proposes an activity-theoretical approach for examining school library practices, which integrates research evidence, professional expertise and reflective decision making.

The Small High School Libraries Project involves a group of six geographically dispersed teacher-librarians in the greater Seattle, Washington (USA) area whose schools are in the midst of structural and pedagogical reform. The goals of the project included: developing an understanding of the issues faced by teacher-librarians during the reform process; assisting the teacher-librarians in aligning their practice with the information needs of a changing school; and identifying best practices for supporting positive adaptation during this process.

Early in the project, the research team looked for tools with which to analyse a growing body of qualitative data to develop an understanding of the teacher-librarians’ professional world from multiple viewpoints. Developmental Work Research emerged as a viable approach. To the team’s knowledge, this framework has not been applied to school librarianship. It has, however, found numerous applications in health care, education and business practice ([Engeström 1991](#eng91); [Engeström _et al._ 2002](#eng02); [Miettinen, 1999](#mie99)). In Developmental Work Research, practices are analysed as socially distributed collective activities which evolve over time through tension, contradiction and innovation. New models of practice emerge from the research participants’ experience, similar to the action research models of organizational learning ([Argyris & Schon 1996](#arg96)). Solutions are never adopted as prescriptive, but are adapted to individual contexts with the guidance of the researchers and, in this case, the peer group, which evolved as a community of practice.

This paper begins by describing the background of the project and the research methods used for data collection. A brief introduction to activity theory and the concept of expansive learning is followed by an analysis of the teacher-librarian’s activity system and its contradictions. The instruments designed from these contradictions to improve practice are described, as well as a case example of the specific application of the framework in one of our study contexts. After mapping the future direction of the research, the paper concludes by suggesting the development of additional tools to enhance the theory-practice relationship in school library research.

### Background: the [Small High School Libraries Project](http://www.Webcitation.org/5NhhGipYm)

The Small High School Libraries Project is a three-year investigation of secondary school libraries in the midst of a specific structural and curricular reform effort. Through this reform, comprehensive high schools (grades 9-12) of 800-2000 students are being subdivided into autonomous academies or “small schools” of 400 students or fewer. Small schools purportedly provide an improved learning environment that results in increased academic achievement, lowered dropout rates and improved parent, teacher and student satisfaction with schooling. The small schools approach has the support of the United States Congress, the Department of Education and the Bill and Melinda Gates Foundation. Changing the size and nature of high schools also requires a change in the library services of those schools.

The teaching and learning approaches championed by small schools include: teachers personalizing instruction to facilitate student inquiry and to meet the needs of individual students; flexible curriculum focused on independent research; standards-based learning with intensive support to help students meet standards; and student demonstration of learning through projects, exhibitions and performance-based assessments. Teaching and learning in a small school requires that the library and librarian provide a rich infrastructure of information skills instruction and services, reading and literacy advocacy, information and technology services and resources management.

To date, little attention has been given to the needs of small schools in terms of library and information services, systems and resources, the role of teacher-librarians, or how to deploy library and information infrastructure effectively and efficiently. The underlying assumption is that effective library and information services are essential for the successful education of adolescents in small schools.

## Analytical and empirical method

### Methodology: Activity Theory and Developmental Work Research

Activity theory originated within the cultural-historical tradition of Soviet psychology and its development is credited largely to Alexei N. Leont’ev ([1979](#leo79) [1981](#leo81)). It is concerned with the process of mediation: how practical activity shapes and is shaped by cognitive functioning. A growing international community has developed around this theory and excellent analyses of its intellectual origins are available in the work of several scholarly domains, including education ([Engeström 1999](#eng99)), cultural psychology ([Ratner 2006](#rat06)), human-computer interaction ([Kaptelinin & Nardi 2006](#kap06); [Kuutti 1996](#kuu96)) and information science ([Wilson 2006](#wil06)). Within library and information science the theory is gaining significant attention, having been applied to subject representation ([Hjørland 1997](#hjo97)), digital library evaluation ([Spasser 2002](#spa02)) and information seeking ([Wilson 2006](#wil06)). The particular interpretation of activity theory used in this paper, Cultural Historical Activity Theory, is grounded in the work of Yrjö Engeström and his Developmental Work Research programme at the University of Helsinki. The following paragraphs describe three key concepts of this research approach: the activity system, contradictions and expansive learning.

### The socially distributed activity system

Cultural-historical activity theory addresses human activities as they relate to artefacts, shared practices and institutions, thus it goes beyond individual knowledge and decision making to take a developmental view of minds in context. As people work, play, think and solve problems together they demonstrate an accumulated set of habits and values. Learning is not an isolated act; rather it is situated in time and space and influenced by the surrounding actors, resources and behavioural constraints. One should also recognize that agents in the learning process, through their activities, influence the contexts in which such learning takes place. Cultural-historical activity theory, then, as a dynamic model, is particularly appropriate for the study of educational practice.

In Developmental Work Research, which draws on the mediational theories of Leont’ev ([1978](#leo78)) and Vygotsky ([1978](#vyg78)), the socially distributed activity system is the fundamental unit of analysis. Activity becomes the least meaningful context for understanding individual actions, but at a higher level it can be used to describe and evaluate systemic interactions and relationships. In this approach, the activity system is composed of '_the individual practitioner, the colleagues and co-workers of the workplace community, the conceptual and practical tools and the shared objects as a unified dynamic whole_' ([Engeström, 1991](#eng91): 267). A model of this system appears in Figure 1\. The systemic view of activity posits that each element is related to all other elements dialectically; that is, they are distinct yet interdependent ([Ratner 2002](#rat02); [2006](#rat06)). The activity system, composed of the actor (subject) and his mediating tools and environment (rules, community, division of labour), is defined by the object, or objective, of the activity. The object-oriented nature of activity is what gives it direction, as well as what distinguishes it from other activities. The object should correspond to the motive that drives the system and the interrelationship of its elements.

<figure>

![Figure 1: model of the socially distributed activity system.](../p313fig1.gif)

<figcaption>

**Figure 1: model of the socially distributed activity system.**</figcaption>

</figure>

The system has embedded in it different viewpoints, historically conditioned roles and rules of behaviour and artefacts that constrain and afford activity. It is useful for making sense of everyday practice as well as disturbances and changes in the work environment. Tensions may exist within and among the different elements of the system. These tensions and how they are resolved (or aggravated) provide continuous transformations, the development of new practices.

Much of the power of activity theory as an explanatory framework rests in the concept of _contradictions_ ([Engeström 1999](#eng99)). Contradictions arise when new ways of thinking or doing come in conflict with traditional or currently accepted ways of thinking and doing and may occur within each of the elements, between elements, or among activities, resulting in tensions within the system. Exaggerated contradictions, or those which reach crisis proportions, may result in a breakdown of the activity system itself. Very often, in the course of everyday activities, tensions or breakdowns in activity will be repaired or negotiated, but not all tensions or contradictions are obvious to the actors engaged in a given activity. Furthermore, actors may not share consistent motivations or conceptions, despite their participation in the same activity ([Blackler 1995](#bla95)).

### The expansive learning cycle

Engeström ([1987](#eng87) [1991](#eng91)) describes the practice, which he calls _expansive learning_, of using contradictions to serve as a _springboard_ for changing activity systems. Rather than seeing contradictions as adverse consequences, they provide a potential driving force for innovation and improvement of practices and services. This is not naïve, unscientific action research, but a model of intervention based in a careful analysis of practical activity, with an historical analysis illuminating the origins of these conditioned practices.

Activity systems do not reside in a vacuum; rather, they are constantly influenced by the conditions in which they reside, including other related activity systems and the persons who engage in the activities themselves. The system is continually striving for balance while encountering tension and contradiction, hence it is a learning cycle not unlike Vygotsky’s ([1978](#vyg78)) Zone of Proximal Development (ZPD). The 'more capable peer', in the case of the Small High School Libraries project, is the research team itself, which is seeking to stimulate innovation and change in each of the six study schools. By working closely with the teacher-librarians, both as individuals and as a community of practice, we are engaged in the design of contextually-situated best practices that may be applied and adapted to other schools facing similar change.

Figure 2 illustrates the expansive learning cycle employed by the research team. The team, in the first fourteen months,was engaged deeply in the first two steps in the cycle, during which time interviews and observations, what we have called the _ethnography of trouble_, have helped us identify a number of contradictions and tensions. Combined with historical analysis of the reform process, both locally and on a broader plane, these empirical studies have helped us develop instruments to influence library activity, specifically targeting the aspects of reform present in each school while supporting the existing roles and practices of the librarians that appear to work well. The librarians involved in the study have been instrumental in helping the team design and modify the proposed models of practice (Step 3). Implementation of these new models is occurring in all six sites, with analysis of the effects to follow in the coming year.

<figure>

![Figure 2: Expansive learning cycle](../p313fig2.gif)

<figcaption>

**Figure 2: Expansive learning cycle (adapted from [Engeström 1991](#eng91))**</figcaption>

</figure>

The nature of this model of research is inherently interventionist. The project team, however, does not impose its new models on the research context; rather it stimulates implementation of new practices. These _given_ practices will be adapted by the activity system as they come into conflict with existing practice, creating a new and critically, socially constructed practice. Finally, the research team will engage in the _ethnography of transformation_ (Step 5) in which it will describe the new practices that emerge from the research and suggest ways in which these practices might be adapted by other library programmes.

### Method of empirical investigation

A triangulated, qualitative approach was used to develop a comprehensive perspective of the work life of librarians, the libraries they worked in and their place in the school community ([Patton 2002](#pat02): 306). The data presented herein are drawn from interviews with administrators, librarians and classroom teachers, corroborated by over 100 hours spent observing the same librarians and classroom teachers as well as individual students and full classes engaged in activities within their high school libraries. The data were collected over a twenty-four month period, overlapping two academic years. The field sites are six school libraries in the greater Seattle, Washington area (USA), representing a diverse sample of student and community demographics.

In-depth interviews with each teacher-librarian once a year (a total of eighteen interviews), administrators (eighteen interviews) and classroom teachers (fifty-seven interviews) were necessary to understand the different perspectives on library activity. We balanced these perspectives with extended observations (two to three hours each, forty-two in total), performed at different times of day and different days of the week to permit study of a wide range of both student and adult activities. Collaborative interviews were designed to get the perspective of both classroom teachers and teacher-librarians on the same library activity. These occurred within two days of an observation and included questions concerning: their use of library resources; collaboration activities with the teacher-librarian; and their perception of the role of the teacher-librarian and of the library in the school's curriculum. In addition to these formally scheduled data collection activities, the research team regularly communicated with teacher-librarians by e-mail and in person.

The data collected through these methods were analysed iteratively by the research team, combined with an historical analysis of school reform and the individual research sites. The historical investigation included a review of the local context and of the broader field of school libraries and educational reform. Case studies of each school were constructed, along with axial coding of themes among different data sources to represent trends across all six schools.

## Modelling activity: empirical investigations of school reform

The cultural-historical activity theory framework can be used to make sense of both everyday and ongoing situations, disturbances and innovations in library practice. But beyond understanding the why and how of existing school library practice is the question of how we change and improve the work practitioners do in serving the needs of students and teachers. In the case of schools engaged in reform practice, that question becomes: How do school libraries adapt to evolving educational policies and practices at the school, district or even national level? How will school libraries, as dynamic agents of students' learning, respond to changing times?

Learning in the school library is not simply a matter of individual students acquiring knowledge of specific domains, but is, rather, the development of specific cultural practices that rely on the rules, roles and tools present in the learning context. The nature of _knowing_ in the socio-cultural sense is collective, situated and tentative ([Cole & Engeström 1993](#col93)). While all activity-theoretic approaches concur with this epistemological stance, there are two ways in which cultural-historical activity theory has been applied to learning domains ([Blackler 1995](#bla95)). These approaches are different, but not incompatible with the central tenets of Leont’ev's theoretical concepts ([Zinchenko 1995](#zin95)). The first of these focuses on how individuals develop common understanding of their practice through participation in shared activities (e.g., [Hakkarainen 1999](#hak99); [Lee 2003](#lee03)). The second focuses on how group activity is conceived and is subsequently conditioned by the social, material and intellectual resources to which they have access (e.g., [Bellamy 1996](#bel96); [Engeström _et al._ 2002](#eng02); [Miettinen 1999](#mie99)). The empirical investigation detailed here draws on this latter interpretation of the theory.

### General model of teacher-librarian activity

The situated nature of activity systems indicates that they are rooted in historically developed and conditioned practices. The position of the teacher-librarian within the school and the role of the library in learning is also a historical construction with inherent contradictions. While the literature of school librarianship abounds with idealized role suggestions, such as collaborator, instructional consultant, change agent, to name a few, it is the rare teacher-librarian who is been capable of translating these conceptual buzzwords into action. The _Information Power_ guidelines and subsequent revision, _Information Power: Building Partnerships for Learning_ ([AASL/AECT 1998](#aas98)), lay out the numerous and sometimes conflicting responsibilities of the school library professional. Even when teacher-lirarians are able to negotiate among these different roles, teachers, students and administrators may have very different perceptions ([Hartzell 2002](#har02); [McCracken 2001](#mcc01); [Meyers _et al._ 2006](#mey06); [Nakamura 2000](#nak00); [Newell 2004](#new04)). Thus, there exist contradictions between theory and practice in the work of teacher-librarians.

Early in the analysis of qualitative data, there emerged contradictions between how teacher-librarians described their practice and their observed behaviour. Further, a number of tensions were apparent, some as a result of school re-organization, others that had been present, latently, for years before the reform effort. The research team identified common themes; each of which was present to a different extent in the six participating sites:

*   Teacher-librarians often had a difficult time articulating how the library contributed to student learning or aligned with the broader teaching mission of the school. Many of their priorities and motivations were based in affective needs or responses to immediate problems or “fighting fires.” Librarians were neither thinking nor acting strategically. None had short-term or long-term goals for his/her programmes or facilities.
*   Teacher-librarians felt isolated from other librarians and misunderstood by teachers and administrators. They were not using their position in the school to enhance pedagogy or promote the mission of the library. Most did not display the attributes of influential practitioners or leaders.
*   Teachers and administrators had a poor grasp of the roles teacher-librarians played in the school. They focused predominantly on the provision of resources rather than learning outcomes. Mis-communication between the teacher-librarian and classroom teachers appeared to occur regularly.
*   The degree to which teacher-librarians felt they were in charge of the library programme dictated the extent to which they felt influential within the building. Teacher-librarians often felt victimized and powerless to change their situation, even when they recognized the need for new practices.

Drawing on these themes and the emerging portraits of practice, the research team formulated a general model of library activity based on the activity system in Figure 1\. This model with its contradictions (Figure 3) is a synthesis of the activities apparent in all six research sites. Using this general model as a base, the research team would construct models of individual practice for each site, focusing on the challenges most salient in each context.

<figure>

![Figure 3: Library activity system](../p313fig3.jpg)

<figcaption>

**Figure 3: Library activity system and its contradictions.**</figcaption>

</figure>

An important secondary contradiction, which was either latent or salient in each of the library programmes, was located between the object and the tools of the teacher-librarian's activity system. Much of the conversation around library practice was focused on the tools of research, so much so that it appeared to be the motivation behind much of the work. They also appeared to struggle with concepts of identity, a contradiction between subject and division of labour. Rather than focusing on student learning, teacher-librarians were serving as gatekeepers to collections of resources. Both of these contradictions contributed toward ambiguity of the object: was the motivation behind librarianship student learning and subsequent achievement, or the mechanics of finding information?

### The intervention: collabouratively designing instruments of expansion

Conversations with the teacher-librarians during initial data collection (Spring 2005) led the research team to think that a collaborative support structure might assist them in successfully adapting to their changing environment. The conceptual work of Etienne Wenger, specifically his discussion of designing a _learning architecture_ to support a community of practice (Wenger 1998) also served as a guide at this stage. We planned and arranged a formal meeting of the six participating teacher-librarians, four members of the research team and an expert in school reform from a nearby school district. This successful initial meeting has since evolved into a series of six day-long workshops (at approximately three month intervals) which we call Research Conversations.

At subsequent workshops, armed with a deeper understanding of the school contexts and existing practices, the research team set about defining learning goals with the teacher-librarians. Through the course of interviews and these formal meetings, the following set of goals emerged as priorities for our practitioners:

*   **Self knowledge:** teacher-librarians gaining a better understanding of their own practice and identity.
*   **Contextual knowledge:** teacher-librarians acquiring a deeper sense of their contexts and the key stakeholders in their programmes.
*   **Inspiring practice:** giving teacher-librarians the opportunity to see different interpretations of the library mission through sharing, discussion and presentations.
*   **Aligning practice:** adjusting the mission of the teacher-librarian and library to align with the needs of the school community.
*   **Communicating mission and identity:** projecting the new mission and implementing complementary practices in the school community.

The Research Conversations have become an opportunity for the teacher-librarians to test ideas about practice, as well as observe different interpretations of their work. It serves as a venue for _externalization_ of disturbances in an atmosphere of open discussion. The research team guides a portion of the agenda to ensure consistency, but much of the work accomplished is driven by the challenges faced by the teacher-librarians in context.

The means to achieving the goals jointly constructed through these meetings has taken the form of a series of expansive instruments, created by the research team and presented to the teacher-librarians at the Research Conversations. In some cases, the instruments are reflective activities performed as individuals, but some are research activities designed to promote evidence-based practice. In all cases, the instruments are intended to present practice as problematical, suggesting areas of improvement or re-alignment. Not all of the teacher-librarians have engaged fully with all the instruments. The research team guides their use of the instruments, but the extent to which they change the attitudes and behaviour of the teacher-librarians is individual. The Zone of Proximal Development, that is the difference between the real and potential state of their practice, is different for each practitioner and the depth of _internalization_ varies by the extent to which these instruments enable them to regard their work as problematical.

Table 1 presents a summary of the research to date, aligning the five steps of the expansive learning cycle with the research methods employed and the instruments of expansion designed to facilitate new professional practice.

<table><caption>

**Table 1:** **Aligning the expansive methodology with research methods and instruments.**</caption>

<tbody>

<tr>

<th>Expansive methodology</th>

<th>Research methods</th>

<th>Instruments of expansion</th>

</tr>

<tr>

<td>

1\. Describing the current activity and disturbances</td>

<td>Observations, interviews, document analysis</td>

<td>Reflective interviews. Support circle</td>

</tr>

<tr>

<td>

2\. Analysing and modelling contradictions</td>

<td>Iterative analysis. General and specific models of activity. Research Conversations</td>

<td>“What if?” scenarios. Time management survey</td>

</tr>

<tr>

<td>

3\. Constructing new models of activity</td>

<td>Research Conversations</td>

<td>Student surveys. Advisory boards</td>

</tr>

<tr>

<td>

4\. Implementing new patterns of activity</td>

<td>Site visits to work with individual practitioners e-mail, listserv, Website</td>

<td>Ten-week reports</td>

</tr>

<tr>

<td>

5\. Evaluating new models and patterns of activity</td>

<td>Observations, interviews, document analysis</td>

<td> </td>

</tr>

</tbody>

</table>

While it is outside the scope of this paper to give a thorough accounting of these instruments, their design and implementation, a brief summary provides the key concepts and perceived benefits of each.

**Reflective interviews:** teacher-librarians were engaged in semi-structured interviews which asked them to reflect on their programme priorities and daily activities. They enjoyed these conversations about their practice and most interviews lasted well beyond the intended protocol length of thirty to forty-five minutes.

**Support circles:** during the interview, teacher-librarians completed a circle diagram based on Hartzell’s ([1994](#har94)) Power/Dependency Mapping with an interview protocol based in the Perceived Social Network Inventory, drawing on the concept of “reciprocality of support” ([Oritt _et al._ 1985](#ori85)). The protocol asked teacher-librarians to identify who they support and who in turn supports their work, creating a visual network structure (for an example, see Figure 4). These diagrams were revisited each year and updated.

<figure>

![Figure 4: Sample Support Circle document](../p313fig4.jpg)

<figcaption>

**Figure 4: Sample Support Circle document. Arrows represent support direction. H indicates a “high” or strong support relationship**</figcaption>

</figure>

**“What if?” scenarios:** during the third research conversation, teacher-librarians were asked to imagine what would happen if the library and librarian were to be removed and, specifically, what the impact would be on five different teaching and learning practices. Responses were shared among the group and discussed. This activity asked them critically to assess the impact of their programmes.

**Time management survey:** teacher-librarians were asked to track their daily activities in fifteen-minute intervals for an entire day once a week for five weeks (in total, five days of data from each participant). The findings were shared at the fifth Research Conversation. One teacher-librarian created a form for sharing perceived and real time data with administrators. This data was used to analyse perceived role selections compared and contrasted with activity logs.

**Student surveys:** each school was provided with a Personal Response System (PRS), which consists of software and hardware for gathering real-time survey data from classes of students. With the help of the teacher-librarians, the research team constructed a survey instrument to be administered with the device to all ninth- and twelfth-grade students. Questions focused on student information behaviour and perceptions of library and information services. These surveys provided an engaging point of interaction between teacher-librarians and students and provided a foundation for evidence-based practice.

**Advisory boards:** teacher-librarians were encouraged to form advisory boards for their library programmes, consisting of teacher leaders and administrators. Advisory boards would provide localized feedback on services and help align the library mission with school goals. The research team provided guidance and support for the formation of these boards.

**Ten-week reports:** teacher-librarians were encouraged to provide regular reports to faculty and staff concerning library activities, including circulation statistics and materials acquisitions. These reports would serve as a means of communicating the library mission and reinforcing its identity as a contributor to student learning. The researchers provided a template and encouragement; teacher-librarians shared their reports with each other to provide inspiration.

It should be noted that not all of these instruments have been successful or universally applied. For example, only two teacher-librarians have formed advisory boards. On the other hand, all the teacher-librarians utilized the student surveys and reflective activities. A thread which the research team has tried to pull through all of these instruments is the concept of student learning as the object of library activity, focusing efforts on strategic actions that contribute to the educational mission of the school. Some of these instruments may be _uncomfortable_ for individual teacher-librarians; furthermore, the benefits of changing practice may not be visible in the short term. Working at the edge of the practitioner's comfort and competence is consistent with the principles of the Zone of Proximal Development.

Feedback on the Research Conversations, however, has been overwhelmingly positive. The research team has expanded the format of these workshops to facilitate greater ownership of the agenda and presentation content by the teacher-librarians themselves. Three of the six have chosen to work together on professional development outside the auspices of the research project. This would indicate the potential for replicability or sustainability of this model beyond the duration of the grant funding.

The paper thus far has presented a more general view of the challenges facing teacher-librarians it the reform context, as well as how the research team has helped foster opportunities for expansion with these practitioners. In the following section, a case example of one of the six participating high schools is presented, which illustrates exaggerated contradictions that have put the fate of the library programme in jeopardy and indicates which instruments of expansion we hope will alter the fate of this practitioner.

### Case example: Redwood High School

Ms. Reed (all place names and identities in the case example are pseudonymous) has been the librarian at Redwood High School for 15 years and has witnessed significant changes in teaching and learning since the school joined the [Coalition of Essential Schools](http://www.essentialschools.org/). Redwood is gradually converting grade levels into academy groups, which are using team teaching methods, integrated curriculum and a block schedule to facilitate a more personalized learning environment. As this process has progressed, she has noticed changes in the way the library is used, as well as teacher attitudes toward the research process.

#### Ethnography of trouble

In nearly twenty hours of observation in the Redwood library, the researchers recorded only two class visits. During the remaining time the library was used by a small number of drop-in students or by no one at all. Ms. Reed linked this change in the frequency of library use to the formation of the academy structure in the school:

> It started a few years ago when the small schools programme was really getting under way and the tenth-grade academy was just starting. There was a large drop in the number of teachers bringing in their classes to work on reports here [the library] and dig through the references, the books, go through it all, talk about citations, the whole little quick reference thing.

One of the pedagogical techniques being used by the teachers during this transition was the development of _focus folders_, photocopied articles that were pre-selected by the teachers to frame classroom discussion. This technique had become embedded in the practices of the school and its curriculum. Ms. Reed further explained: '_The whole curriculum is geared towards the teachers doing the units, getting together and doing the research for them [the students] and then passing out the articles to the students._' The emphasis by the classroom teachers had moved from the discovery of information through the research process to the analysis and synthesis of information from previously vetted sources. This was confirmed by our interviews with classroom teachers, one of whom remarked, '_We don't have a lot of visits to the library._' This teacher's discussion of the collabourative activities with the librarian illustrates this transformation:

> Another way that we have worked together in the past is when we did an integrative project for tenth grade; this is a couple of years ago now, on globalization. We needed some background readings on different sorts of subjects and the teacher-lirarian did the research for us and put together packets so that was really helpful in terms of the project that we did.

The classroom teachers interviewed at Redwood High (n=12) remarked that Ms. Reed is an excellent researcher, capable of finding the most obscure information on request. One teacher went so far as to say ‘_her talents are wasted at the high school level_’ and that she would be better suited to a post-secondary reference library position. Her own strength as a researcher has prompted Ms. Reed to see research as the _object_ of student learning, rather than the means or _instrument_ of the learning process, disconnecting her from the motivations of the faculty. One veteran teacher remarked that the school seems to have moved forward pedagogically during the reform process while ‘_the library has been left behind_’.

Ms. Reed has also chosen to assume the added role in the school of technology coordinator. In this capacity, she administers and diagnoses problems with the computer and communication systems in the school, including teacher e-mail. Because these tasks often require her to leave the library, she discourages the teachers from sending down large groups of students to use the space and resources when she cannot be present to help them. The move to a different mode of research and library usage has been reinforced by the librarian assuming this new role. The lack of library usage is distressing to Ms. Reed, but she does not know how to alter the practice. ‘_They used to have to come four or five times a semester but now the teachers just hand them the articles, which I have a hard time with, but that is what they do._’ Furthermore, the school principal confided that she is disappointed with the current level of library use and suggested that the librarian exacerbates this trend. In her analysis, the librarian ‘_drives people to rely on online materials’._ She commented further that, ‘ _It is a huge space and not well used. We do not have the foot traffic we need to have in that space. I have a problem with that_’. She is beginning to look at plans for utilizing the library space with other faculty, but not the teacher-librarian.

#### Analysis of contradictions

At Redwood High School we have significantly different views of the research process, particularly what constitutes research and how students should be interacting with information. Ms. Reed enjoys the _messy_ aspects of research and feels that students gain a great deal from the search for reliable information. '_Students may find something you don't like, but that's good; it's an important part of the research process'._ The teachers, on the other hand, view research as a means of analysing and synthesizing data. While they rely on background information for their investigations, they see searching as a process that impedes or delays other processes. As a result, the library is sorely underutilized and its future as an institution in the school is in danger. Role adjustments have aggravated this contradiction nearly to the point of break down.

<figure>

![Figure 5: Lack of common object](../p313fig5.gif)

<figcaption>

**Figure 5: Lack of common object creates competing activity systems.**</figcaption>

</figure>

The library activity system and the teacher activity system do not have a common objective; that is, they have contradictory perceptions of the student outcome. This is what Hakkarainen (1999) calls the '_common object problem'._ (Figure 5). The teachers have rejected the library activity system and it does not contribute to student learning. The double bind of under-utilization and associated role change have reinforced the opposing activity system, making change less likely to affect a return to the teacher-librarian's construction of the object: research as the process of tool selection, search and relevance evaluation. This is an example of a tertiary contradiction: the teachers consider their approach to pedagogy to be more advanced, making the earlier model of information seeking espoused by the teacher-librarian obsolete.

#### Opportunities for Expansive Learning

When such a significant difference in meaning occurs, intervention will be necessary to move the systems closer together. Change on the part of the library programme may not be sufficient to influence the teacher's activity system. For the system to return to balance and a common object to take hold in practice, the teachers and teacher-librarian will need to develop a new way of conceiving research by viewing these processes as complementary, rather than contradictory. Communication will be central to eliminating the contradictory positions and returning the system to balance.

The project team has focused individual work with Ms. Reed on two of the instruments of expansion, specifically the student survey and on improving communication practices with the faculty through ten-week reports. The student survey presented an opportunity for Ms. Reed to create connections with students and faculty, as well as to create a boundary object, the student responses, which would form the foundation for collaborative information literacy instruction. By gathering and presenting evidence of students' information behaviour to the faculty, Ms. Reed could encourage conversation about issues that affected classroom work, such as the students' use of convenient rather than authoritative sources. By providing the Personal Response System hardware to the Redwood Library, we hoped to encourage evidence-based practice and facilitate formative assessment techniques, both of which are recognized as pedagogically progressive. Ten-week reports would further serve to open a line of communication with the faculty and maintain the library's visibility.

Thus far we have seen some encouraging progress at Redwood High. The ninth-grade world history teachers are working with Ms. Reed, who is facilitating training on the school's online resource collections. There has been a modest increase in the number of students using the library space, although it is still under-utilised. The student survey is complete and the results are due to be shared with the teaching community in the coming months. In the process of assisting with these instruments, new areas of improvement have emerged. The research team has already discussed possible interventions that will improve the relationship with students, namely making the physical library space more welcoming.

## Discussion: using cultural-historical activity theory to enact new library practices

Cultural-historical activity theory is above all a model of change, one that holds the potential to drive innovation in practice. Identifying contradictions is the diagnostic facet. To learn and grow, these diagnoses must be acted upon in context to form new practices and ways of looking at the world. The empirical case presented above illustrates the beginning of this process of designing and implementing new practice to improve student learning through the school library. We intend to document the changes in these programmes as the practices take root and evolve; however, the time frame for educational change is often measured in years rather than months or days. One of the limitations of Developmental Work Research is its reliance on practitioners recognizing the need to incorporate new work practices. This is also a key strength of the approach, the adoption of new practice is mutually constructed and collegial and the solution is the one which produces the greatest, longest-lasting benefit.

The research team has already seen evidence of changing attitudes, if not behaviour, among the teacher-librarians in the study. During our regular contact with these practitioners they offer their insights and feedback to the project team, both by e-mail and in person. All have commented, either formally or informally, that their involvement in the research has made them more thoughtful, intentional or reflective about their practice. The following insight came from one of our teacher-librarians in the course of an interview in Autumn 2006:

> Something that I have thought a lot about since the involvement with the grant: it seems to me that back in the really, really, really, old days that the primary relationship that the librarian had was with the things. Like you know, you had to make sure that the things were right, that the books were on the shelf, that the card catalogue was in order, that kind of stuff. And then it seemed that the primary relationship was with the patrons, helping the patrons on an individual basis. But now I'm starting to see the key relationship is between the librarian and the teacher. And the more I think about it the more it makes sense to me, because the primary relationship in the school is between the students and their teacher in the classroom. And to think [in the past] that the class could come in for an hour and that I would be able to supersede that relationship, even though my role is teacher-librarian... I think that working with the grant has helped me with that evolution of thought.

This teacher-librarian would appear to be grappling with the strategic nature of her work, specifically which activities and relationships within the school permit her to have the greatest impact on learning. She has come to understand that the limited time she has to perform direct instruction to students is ineffective without the integrated support of classroom educators. She illustrates an evolution in conceiving of library work, from the system-centred to the user-centred, then to the higher plane of working to coordinate her activities with the learning outcomes of her fellow educators. Conceptually, she is realigning the object of her practice.

### Implications for future study

This paper does not suggest that the solutions designed in these reform contexts are generalisable to all school library programmes. Rather, what we hope to reproduce is the method by which the solutions were designed through analysis and peer collabouration. The next step in our project will be the design of an activity theory toolkit for use by teacher-librarians, disseminated through workshops and facilitated by the community of practice model of professional development ([Wenger 1998](#wen98)). Our research team has found that the development of communities of practice, bringing teacher-librarians together virtually and physically, to work on designing new ways of conceiving and delivering information services in their schools, contributes significantly to changing the conversation on effective library programmes.

An activity theory toolkit is not a new concept; rather, it draws on the pioneering work of Michael Cole at the Labouratory of Comparative Human Cognition ([Cole 1996](#col96); [Brown & Cole 2002](#bro02)). In designing his Fifth Dimension programme, Cole and his research team use cultural-historical activity theory as a guiding principle of their investigations and intervention programmes targeting at-risk youth. Extending this toolkit concept to library practitioners would empower them to examine and improve their own practices through a rigorous, science-based, action research agenda situated in the socio-cultural perspective. Two potential benefits to such a toolkit appear obvious: it offers the opportunity to establish a _lingua franca_ for researchers and practitioners in library-based learning; and it should elevate the conversation on library practices to incorporate the macro-cultural factors of the broader learning enterprise.

The activity-theoretic approach opens new possibilities for the study of library programmes, youth information behaviour and student learning as situated activity that is rooted in cultural practice. Beyond descriptive research, cultural-historical activity theory can be used to model and design new practices to respond to changing educational needs and constraints. The integration of multiple dimensions of library processes in the concept of an activity system moves away from abstraction and isolation and moves toward the prospect of directing change to facilitate more productive interactions and greater achievement.

## Acknowledgements

The research presented in this paper was sponsored in part by the Institute of Museum and Library Services grant # LG-02-04-0012-04

The author wishes to thank the members of the Small High School Libraries project team at the University of Washington Information School (Michael B. Eisenberg, Matthew Saxton, Lisa Nathan, Bryce Nelson and Doug Eriksen) for their support in the collection of the empirical data used in this paper.

## References

*   <a id="aas98"></a>American Association of School Librarians & Association for Educational Communications and Technology (AASL & AECT). (1998). Information power: building partnerships for learning. Chicago, IL : American Library Association.
*   <a id="arg96"></a>Argyris, C. & Schön, D.A. (1996). _Organizational learning II: theory, method and practice._ Reading, MA: Addison-Wesley.
*   <a id="bel96"></a>Bellamy, R.K. (1996). Designing educational technology: computer-mediated change. In B. Nardi, (Ed.). _Context and consciousness._ (pp. 123-146). Cambridge, MA: The MIT Press.
*   <a id="bla95"></a>Blackler, F. (1995). Knowledge, knowledge work and organizations: an overview and interpretation. _Organization Studies_ **16**(6), 1021-1046.
*   <a id="bro02"></a>Brown, K. & Cole, M. (2002). Cultural historical activity theory and the expansion of opportunities for learning after school. In G. Wells & G. Claxton, (Eds.) _Learning for life in the 21st Century_, (pp. 225-238). Malden, MA: Blackwell.
*   <a id="col96"></a>Cole, M. (1996). _Cultural psychology: a once and future discipline_. Cambridge, MA: Belknap/Harvard University Press.
*   <a id="col93"></a>Cole, M. & Engeström, Y. (1993). A cultural historical approach to distributed cognitions. In G. Salomon, (Ed.). _Distributed cognitions: psychological and educational considerations._ (pp. 1-46). NY: Cambridge University Press.
*   <a id="eng87"></a>Engeström, Y. (1987). _[Learning by expanding: an activity-theoretical approach to developmental research.](http://www.webcitation.org/5NhyCTdGW)_ Helsinki: Orienta-Konsultit Oy. Retrieved 23 March 2006 from http://lchc.ucsd.edu/MCA/Paper/Engestrom/expanding/toc.htm
*   <a id="eng91"></a>Engeström, Y. (1991). Developmental work research: reconstructing expertise through expansive learning. In M. Nurminen & G. Weir, (Eds.). _Human jobs and computer interfaces_ (pp. 265-290). New York, NY: North-Holland.
*   <a id="eng99"></a>Engeström, Y. (1999). Activity theory and individual and social transformation. In Y. Engeström, R. Miettinen & R. Punamäki (Eds.), _Perspectives on activity theory_ (pp.19-38). New York, NY: Cambridge University Press.
*   <a id="eng02"></a>Engeström, Y., Engeström, R. & Suntio, A. (2002). Can a school community learn to master its own future? An activity-theoretical study of expansive learning among middle school teachers. In G. Wells & G. Claxton (Eds.), _Learning for life in the 21st Century_ (pp. 211-224). Malden, MA: Blackwell.
*   <a id="hak99"></a>Hakkarainen, P. (1999). Play and motivation. In Y. Engeström, R. Miettinen & R. Punamäki (Eds.), _Perspectives on activity theory_ 231-249\. NY: Cambridge University Press.
*   <a id="har94"></a>Hartzell, G. (1994). _Building influence for the school librarian._ Worthington, OH: Linworth Publishing.
*   <a id="har01"></a>Hartzell, G. (2001). [The implications of selected school reform approaches for school library media services.](http://www.webcitation.org/5NhyT1qaZ) _School Library Media Research_, **4**. Retrieved 12 October 2004 from: http://www.ala.org/ala/aasl/aaslpubsandjournals/slmrb/slmrcontents/volume42001/hartzell.htm
*   <a id="har02"></a>Hartzell, G. (2002). The principal's perceptions of school libraries and teacher-librarians. _School Libraries Worldwide_ **8**(1), 92-110.
*   <a id="hjo97"></a>Hjørland, B. (1997). _Information seeking and subject representation: an activity-theoretic approach to information science_. Westport, CT: Greenwood Press.
*   <a id="kap06"></a>Kaptelinin, V. & Nardi, B.A. (2006). _Acting with technology: activity theory and interaction design_ . Cambridge, MA: The MIT Press.
*   <a id="kuu96"></a>Kuutti, K. (1996). Activity theory as a potential framework for human-computer interaction research. In B. Nardi, (Ed.). _Context and consciousness._ (pp. 17-44). Cambridge, MA: The MIT Press.
*   <a id="lee03"></a>Lee, C. D. (2003). Cultural modelling: cultural-historical activity theory as a lens for understanding instructional discourse based on African-American English discourse patterns. In A. Kozulin, B. Gindis, V. Ageyev & S. Miller (Eds.). _Vygotsky’s educational theory in cultural context_ (pp. 393-410). NY: Cambridge University Press.
*   <a id="leo78"></a>Leont'ev, A. M. (1978). _Activity, consciousness and personality_. Englewood Cliffs, NJ: Prentice-Hall.
*   <a id="leo81"></a>Leont’ev, A. M. (1981). The problem of activity in psychology. In J. Wertsch, (trans. and ed.) _The concept of activity in Soviet psychology._ (pp. 37-70). Armonk, NY: M.E. Sharpe.
*   <a id="lon03"></a>Lonsdale, M. (2003). _[The impact of school libraries on student achievement: a review of research.](http://www.webcitation.org/5NhymJQep)_ Camberwell, Victoria: Australian Council for Educational Research. Retrieved 11 January 2007 from: http://www.acer.edu.au/research/documents/schoollibraries.pdf
*   <a id="mcc01"></a>McCracken, A. (2001). [School library media specialists' perceptions of the practice and importance of the roles described in Information Power.](http://www.webcitation.org/5Nhz0E3FO) _School Library Media Research_ **4**. Retrieved 29 March, 2007 from: http://www.ala.org/ala/aasl/aaslpubsandjournals/slmrb/slmrcontents/volume42001/mccracken.htm
*   <a id="mey06"></a>Meyers, E.M., Nathan, L.P. and Saxton, M.L. (2006). [Barriers to information seeking in school libraries: conflicts in perceptions and practice.](http://informationr.net/ir/12-2/paper295.html) _Information Research_ **12**(2), paper 295\. Retrieved 10 February 2007 from http://InformationR.net/ir/12-2/paper295.html
*   <a id="mie99"></a>Miettinen, R. (1999). Transcending traditional school learning: teachers' work and networks of learning. In Y. Engeström, R. Miettinen & R. Punamäki (Eds.), _Perspectives on activity theory_ (pp. 325-344). NY: Cambridge University Press.
*   <a id="moo05"></a>Moore, P. (2005). An analysis of information literacy education worldwide. _School Libraries Worldwide_ **11**(2), 1-23\. [The 'White Paper' upon which this article is based [is available online.](http://www.webcitation.org/5NhzMegqv)]
*   <a id="nak00"></a>Nakamura, Y. (2000). Teachers' perceptions of school libraries: comparisons from Tokyo and Honolulu. _School Libraries Worldwide_ **6** (1), 66-87.
*   <a id="new04"></a>Newell, T.S. (2004). [Thinking beyond the disjunctive opposition of information literacy assessment in theory and practice](http://www.webcitation.org/5NhzSKcfm). _School Library Media Research_ **7**. Retrieved 15 August 2006 from: http://www.ala.org/ala/aasl/aaslpubsandjournals/slmrb/slmrcontents/volume72004/beyond.htm
*   <a id="ori85"></a>Oritt, E.J., Paul, S.C. & Behrman, J.A. (1985). Perceived social network inventory. _Journal of Community Psychology_ **13**(5), 565-581.
*   <a id="pat02"></a>Patton, M.Q. (2002). _Qualitative research and evaluation methods_. 3rd Edition. Thousand Oaks, CA: Sage.
*   <a id="rat02"></a>Ratner, K. (2002). _Cultural psychology: theory and method_. New York, NY: Kluwer Academic.
*   <a id="rat06"></a>Ratner, K. (2006). _Cultural psychology: a perspective on psychological functioning and social reform_. Mahwah, NJ: Lawrence Erlbaum Associates.
*   <a id="spa99"></a>Spasser, M.A. (1999) Informing information science: the case for activity theory. _Journal of the American Society for Information Society_ **50**(12), 1136-1138
*   <a id="spa02"></a>Spasser, M.A. (2002). Realist activity theory for digital library evaluation: conceptual framework and case study. _Computer Supported Cooperative Work_ **11**(1/2), 81-110.
*   <a id="tod06"></a>Todd, R. (2006). School libraries and evidence-based practice: an integrated approach to evidence. _School Libraries Worldwide_ **12**(1), 31-37.
*   <a id="vyg78"></a>Vygotsky, L. S. (1978). _Mind in society: the development of higher psychological processes_. Cambridge, MA: Harvard University Press.
*   <a id="wen98"></a>Wenger, E. (1998). _Communities of practice: learning, meaning and identity_. New York, NY: Cambridge University Press.
*   <a id="wil06"></a>Wilson, T.D. (2006). [A re-examination of information seeking behaviour in the context of activity theory.](http://InformationR.net/ir/11-4/paper260.html) _Information Research_ **11**(4), paper 260\. Retrieved 15 August, 2006 from http://InformationR.net/ir/11-4/paper260.html
*   <a id="zin95"></a>Zinchenko, V. P. (1995). Cultural-historical psychology and the psychological theory of activity: retrospect and prospect. In J. Wertsch , P. del Rio & A. Alvarez (Eds.), _Sociocultural studies of mind_ (pp.37-55). New York, NY: Cambridge University Press.