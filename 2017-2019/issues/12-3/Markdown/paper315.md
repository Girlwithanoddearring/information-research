#### Vol. 12 No. 3, April 2007

* * *

# Uma revisão dos algoritmos de radicalização em língua portuguesa

#### [Angel Freddy Godoy Viera](mailto:godoy@cin.ufsc.br) e [Johnny Virgil](mailto:johnnyvirgilbr@gmail.com)  
Programa de Pós-graduação em Ciência da Informação  
Universidade Federal de Santa Catarina  
Florianópolis, Brasil

#### Resumo

> **Introdução**. A recuperação da informação teve a sua importância aumentada com a geração e o uso costumeiro de arquivos digitais, principalmente no âmbito da Ciência da Informação. Uma das diversas estratégias de tratamento dos textos para a indexação do seu conteúdo por sistemas de recuperação da informação é a denominada radicalização, que consiste na redução de palavras congêneres em uma representação única. A radicalização depende da estrutura morfológica da língua que se propõe a tratar.  
> **Objetivo**. Este artigo pretende definir o conceito de radicalização, apresentar os algoritmos disponíveis para a língua portuguesa e tecer comentários sobre vários pontos relativos ao uso dessa espécie de algoritmos.  
> **Metodologia**. Realizou-se uma revisão de toda a literatura pertinente à radicalização em língua portuguesa de conhecimento dos autores.  
> **Resultados**. Até o momento, há apenas três algoritmos divulgados que tratam da radicalização em língua portuguesa: os de Porter, Orengo e Gonzalez.  
> **Conclusão**. Atualmente, a perfeição no processo de radicalização em língua portuguesa não pode ser obtida com sucesso absoluto, em razão do pouco estudo que é está sendo desenvolvido na área.

#### [Abstract in English](#abstract)

## Introdução

No conjunto das diversas áreas que envolvem a Ciência da Informação, a recuperação inteligente da informação ocupa um espaço não totalmente compreendido por setores mais voltados a aspectos relacionados com o estudo de usuários. No entanto, o uso cada vez mais corriqueiro de computadores e a onda crescente de avanços em processamento textual têm gerado, na prática, uma infinidade de arquivos digitais. Esses arquivos tanto podem representar relatórios simples quanto artigos científicos, monografias, notícias de jornal, livros, enciclopédias, dicionários, páginas Web, entre outros tipos de documentos. Esse acúmulo de documentos acabou criando a necessidade de encontrar soluções para melhorar a recuperação da informação neles contida.

A aplicação de mecanismos de recuperação inteligente da informação no universo empresarial, principalmente no que se refere aos mecanismos de busca na Internet, enfatizou a importância comercial e estratégica do acesso rápido e confiável às fontes de informação. Empresas como Google (http://www.google.com), AltaVista (http://www.altavista.com) e Yahoo! (http://www.yahoo.com) tornaram-se referência no que tange à divulgação, disponibilização, organização e recuperação de informações em um contexto de acesso público e gratuito pela comunidade mundial de internautas.

A possibilidade de varrer o conteúdo textual (letras, palavras, parágrafos) dos documentos digitais gerou diversas abordagens distintas no âmbito da recuperação da informação. Uma delas será tratada neste artigo. No caso, estudar-se-á uma das técnicas de pré-processamento do texto, etapa realizada sobre os documentos digitais antes de sua indexação. Essa técnica é a _radicalização_, ou _stemming_ (em língua inglesa), ou ainda _determinação do radical_, dentre outras denominações, que visa a reduzir variações de uma mesma raiz vocabular com a finalidade de recuperar palavras correlatas.

Infelizmente, a radicalização é um processo que implica algoritmos diferentes de acordo com a língua em questão. Na opinião de Figuerola, Gómez Díaz e López de San Román ([2000: 1](#fig00)): "...there are notable differences between different languages in the way of forming derivatives and inflected forms, so that the application of specific techniques can produce unequal results according to the language of the documents and queries". ([Nota 1](#footnote1))

Porter ([1980](#por80)) desenvolveu um algoritmo extremamente eficiente para a língua inglesa, que tem sido utilizado extensivamente, nas palavras do seu autor: '_the Algorithm has been widely used, quoted, and adapted over the past 20 years_' ([Porter 2005](#por05)) ([Nota 2](#footnote2)). No entanto, as características da língua inglesa são distintas, por exemplo, das da língua portuguesa. Isso obriga afirmar que a radicalização em língua portuguesa deve possuir um algoritmo que esteja de acordo com a sua morfologia.

Até o momento, diversos algoritmos de radicalização foram criados para as mais diferentes línguas. Esses algoritmos são apresentados no Quadro 1.

<table><caption>

**Quadro 1: Algoritmos de radicalização para várias línguas**  
Fontes: Adaptado de Porter ([1980](#por80), [2005](#por05)), Paice ([1990](#pai90)), Popovic e Willett ([1990](#pop90)), Krovetz ([1993](#kro93)), Kraaij e Pohlmann ([1994](#kra94)), Ekmekçioglu _et al._ ([1995](#ekm95)), Schinke _et al._ ([1996](#sch96)), Figuerola _et al._ ([2001](#fig01)), Carlberger _et al._ ([2001](#car01)), Alemayehu e Willett ([2002](#ale02)), Chaves ([2003](#cha03)) e Nakov ([2003](#nak03)).</caption>

<tbody>

<tr>

<th>Língua</th>

<th>Algoritmo</th>

<th>Autoria</th>

</tr>

<tr>

<td rowspan="5">Inglês</td>

<td>Porter</td>

<td>Porter</td>

</tr>

<tr>

<td>KStem</td>

<td>Krovetz</td>

</tr>

<tr>

<td>Paice/Husk</td>

<td>Paice e Husk</td>

</tr>

<tr>

<td>Porter 2</td>

<td>Porter</td>

</tr>

<tr>

<td>Dawson</td>

<td>Dawson</td>

</tr>

<tr>

<td rowspan="3">Português</td>

<td>Porter - Português</td>

<td>Porter</td>

</tr>

<tr>

<td>Orengo</td>

<td>Orengo</td>

</tr>

<tr>

<td>Pegastemming</td>

<td>Gonzalez</td>

</tr>

<tr>

<td rowspan="2">Alemão</td>

<td>Porter - Alemão</td>

<td>Porter</td>

</tr>

<tr>

<td>Porter - Alemão - Variação</td>

<td>Porter</td>

</tr>

<tr>

<td>Amárico (etíope)</td>

<td>Alemayehu-Willett</td>

<td>Alemayehu e Willett</td>

</tr>

<tr>

<td>Búlgaro</td>

<td>BulStem</td>

<td>Nakov</td>

</tr>

<tr>

<td>Dinamarquês</td>

<td>Porter - Dinamarquês</td>

<td>Porter</td>

</tr>

<tr>

<td>Esloveno</td>

<td>Popovic-Willett</td>

<td>Popovic e Willett</td>

</tr>

<tr>

<td>Espanhol</td>

<td>Porter - Espanhol</td>

<td>Porter</td>

</tr>

<tr>

<td>Finlandês</td>

<td>Porter - Finlandês</td>

<td>Porter</td>

</tr>

<tr>

<td>Francês</td>

<td>Porter - Francês</td>

<td>Porter</td>

</tr>

<tr>

<td rowspan="2">Holandês</td>

<td>Porter - Holandês</td>

<td>Porter</td>

</tr>

<tr>

<td>Kraaij-Pohlmann</td>

<td>Kraaij e Pohlmann</td>

</tr>

<tr>

<td>Italiano</td>

<td>Porter - Italiano</td>

<td>Porter</td>

</tr>

<tr>

<td>Latim</td>

<td>

Schinke _et al._</td>

<td>

Schinke _et al._</td>

</tr>

<tr>

<td rowspan="2">Norueguês</td>

<td>Porter - Norueguês</td>

<td>Porter</td>

</tr>

<tr>

<td>

Carlberger _et al._</td>

<td>

Carlberger _et al._</td>

</tr>

<tr>

<td>Russo</td>

<td>Porter - Russo</td>

<td>Porter</td>

</tr>

<tr>

<td>Sueco</td>

<td>Porter - Sueco</td>

<td>Porter</td>

</tr>

<tr>

<td>Turco</td>

<td>

Ekmekçioglu _et al._</td>

<td>

Ekmekçioglu _et al._</td>

</tr>

</tbody>

</table>

Apesar de a língua portuguesa possuir mais de 220 milhões de falantes no mundo (["Temática Barsa" 2005: 46](#tem05)), estando entre as dez mais faladas, e possuir uma baixa representação em termos de recursos ling?ísticos em relação a outras línguas européias como o francês e o espanhol ([Orengo 2004: 63](#ore04)), pouco se fez em termos de construir um algoritmo que radicalização que acrescentasse vantagens à versão portuguesa do algoritmo de Porter, de uso público ([Orengo 2004: 48](#ore04)).

O objetivo deste artigo, por conseguinte, é apresentar alguns dos algoritmos de radicalização para a língua portuguesa. Na primeira parte, a radicalização será definida, para que haja clareza sobre a sua significação e abrangência; na segunda parte, apresentam-se os algoritmos disponíveis para a língua portuguesa; na última parte, tecem-se algumas considerações sobre os problemas e lacunas existentes na área.

## Radicalização

O vocábulo _radical_ pode ser definido como o "elemento que serve de base às palavras de uma mesma família etimológica [...], e que é tomado como ponto de partida para o estudo da estrutura dos vocábulos numa língua, como _luc-_, _luz-_, em _elucidar_, _translúcido_, _luzir_" (["Radical" 1999: 1697](#rad99)).

O Dicionário Houaiss da Língua Portuguesa apresenta o seguinte significado para radical: 'parte da estrutura de uma palavra que contém seu significado básico e recebe os sufixos flexionais, como, p.ex., _livro/livros_; pode ou não ter afixos derivacionais: _livreiro/livreiros_ [É simples, se constituído de um único morfema: _feliz_; e complexo, se constituído por mais de um morfema: _infeliz_ (_in-_ + _feliz_), _livreiro_ (_livr-_ + _-eir-_ + _-o_).] ([Radical 2001: 2374](#rad01)).

A palavra _radicalização_ não remete a nenhum desses significados (["Radicalização" 1999: 1697](#raz99); ["Radicalização" 2001: 2374](#raz01)). Assim, depreende-se que _radicalização_ apresenta um significado novo que ainda não foi incorporado ao vernáculo da língua portuguesa.

De maneira semelhante, a palavra equivalente em língua inglesa, _stemming_, também carece de um significado apropriado à nova idéia que procura introduzir, como atesta a sua ausência como entrada no Oxford Advanced Learner's Dictionary ([1995](#oxf95)), no The Newbury House Dictionary of American English ([1999](#the99)) e no Merriam-Webster Online Dictionary ([2005](#mer05)). Convém salientar que a palavra _stem_ seria traduzida para o português como _radical_.

Dessa forma, o conceito de _radical_ norteia a definição de _radicalização_.

Uma outra possibilidade de nomenclatura para _radicalização_ poderia ser _normalização morfológica_, apesar de aparecer dividida originalmente em dois tipos: _stemming_ e _normalização can?nica_ ([Gonzalez _et al._ 2003: 2](#gon03)). O termo _conflação_ também aparece na literatura, com o sentido de fusão e combinação de variantes morfológicas, segundo Chaves ([2003: 2](#cha03)), apoiando-se em Frakes e Baeza-Yates ([Nota 3](#footnote3)).

Além das interpretações dadas por dicionários, convém levar em consideração o que gramáticos e ling?istas dizem a respeito.

Sacconi ([1991: 78](#sac91)) define radical desta maneira: "**_Radical_**, **_lexema_** ou **_semantema_** é o elemento portador de significado, comum a um grupo de palavras da mesma família. Assim, na família de palavras **_terra_**, **_terrinha_**, **_terriola_**, **_térreo_**, **_terráqueo_**, **_terreno_**, **_terreiro_**, **_terroso_**, existe um elemento comum: **_terr-_**, que é o radical."

Um pouco mais adiante, Sacconi ([1991: 79](#sac91)), em contraposição à _semantema_, define _morfema_: "dá-se o nome de **_morfema_** ao elemento ling?ístico que, isolado, não possui nenhum valor, servindo apenas para relacionar semantemas na oração, para definir a categoria gramatical (gênero, número e pessoa), etc."

Câmara Júnior ([1980: 94](#cam80)) trata _semantemas_ e _morfemas_ como _formas mínimas_, "elementos últimos de significação" ([Câmara Júnior 1980: 91](#cam80)), assim denominados de acordo com a designação proposta por Vendryes ([1958: 133](#ven58)): "Por _semantemas_ deben entenderse los elementos ling?ísticos que expresan las ideas de las representaciones [...], y por _morfemas_, los que expresan las relaciones entre las ideas [...]. Los morfemas, por lo tanto, expresan las relaciones que el espíritu establece entre los semantemas." ([Nota 4](#footnote4))

Em suma, "o vocábulo ...é uma combinação de formas mínimas na base de um semantema e um morfema" ([Câmara Júnior 1980: 93](#cam80)).

Contudo, _semantemas_ e _morfemas_ nada mais são que, lato sensu, morfemas ([Câmara Júnior 1980: 91-92](#cam80)). Bloomfield ([1969: 161](#blo69)) parece ter o mesmo ponto de vista: "A linguistic form which bears no partial phonetic-semantic resemblance to any other form is a _simple_ form or _morpheme_. Thus, _bird_, _play_, _dance_, _cran-_, _-y_, _-ing_ are morphemes." ([Nota 5](#footnote5))

No âmbito da recuperação da informação, a radicalização é vista com algumas variâncias.

Allan e Kumaran ([2003: 1](#all03)) indicam uma afinidade com o entendimento ling?ístico, quando dizem que "stemming is the process of collapsing words into their morphological root" ([Nota 6](#footnote6)).

Com pouca divergência, Xu e Croft ([1998: 1](#xuj98)) afirmam que "stemming is used in many information retrieval (IR) systems to reduce variant words to common roots" ([Nota 7](#footnote7)).

De modo semelhante, para Orengo ([2004: 48](#ore04)), a radicalização, ou _stemming_, "is the process of conflating the variant forms of a word into a common representation, the stem" ([Nota 8](#footnote8)).

Baeza-Yates e Ribeiro-Neto ([1999: 165](#bae99)) definem radicalização, ou _stemming_, como a remoção dos afixos (prefixos e sufixos) das palavras, a qual ocorre durante o pré-processamento do texto.

Para Russell e Norvig ([2004: 817](#rus04)), a radicalização (os autores se valem do termo traduzido como _determinação do radical_) é algo tão simples quanto "reduzir 'poltronas' à forma de radical 'poltrona'", ou seja, tão simples quanto eliminar o morfema gramatical de plural. Essa eliminação resulta, para a língua inglesa, em um aumento de 2% na recuperação, apesar de que os efeitos podem ser melhores com outras línguas, como o alemão, o finlandês, o turco, o inuit e o yupik ([Russell & Norvig 2004: 817](#rus04)).

Porter ([1980](#por80)) indica que a sua preocupação foi remover sufixos com base em determinados critérios, sem se preocupar diretamente com os aspectos ling?ísticos: "In any suffix stripping program for IR work, two points must be borne in mind. Firstly, the suffixes are being removed simply to improve IR performance, and not as a linguistic exercise. This means that it would not be at all obvious under what circumstances a suffix should be removed, even if we could exactly determine the suffixes of a word by automatic means." ([Nota 9](#footnote9))

Logo, pode-se sintetizar o conceito de radicalização para a recuperação da informação da seguinte maneira: radicalização é o processo de reduzir variações de uma mesma palavra a uma representação única, objetivando aumentar o nível de recuperação de documentos. Em teoria, essa representação tem a intenção de isolar o semantema das palavras dos seus morfemas, assim como na ling?ística. Contudo, não existe obrigatoriedade nesse sentido, uma vez que as representações podem ser simplificações não preocupadas com a perfeição, mas, sim, com oferecer benefícios de recuperação sem onerar o sistema e impactar na rapidez de processamento, seja no momento da indexação, seja no momento da consulta.

Na continuação, apresentam-se os algoritmos de radicalização para a língua portuguesa e, na próxima seção, apresentam-se os problemas desses algoritmos de radicalização.

## Algoritmos de radicalização para a língua portuguesa

Infelizmente, poucos são os algoritmos de radicalização disponíveis para a língua portuguesa. A proposta mais conhecida para a língua portuguesa é a de Orengo ([2004](#ore04)). Segundo Silva ([2004: 8](#sil04)), "para a língua portuguesa, dispomos dos algoritmos de Porter e Viviane Orengo", ou seja, existe uma versão portuguesa desenvolvida pelo próprio Porter ([2005](#por05)), seguindo as mesmas regras daquele desenvolvido para a língua inglesa. Chaves ([2003: 3](#cha03)), além de mencionar o algoritmo de Orengo, apresenta uma outra possibilidade chamada _Pegastemming_, criada por Marco Antonio Insaurriaga Gonzalez.

Russell e Norvig ([2004: 817](#rus04)) ainda dividem o processo de radicalização em dois tipos de algoritmos: os baseados em regras, que realizam operações de redução geralmente de acordo com as terminações das palavras, e os baseados em dicionário, que realizam uma checagem prévia em uma lista para evitar que todas ou determinadas palavras com características específicas sofram uma redução irregular.

### O algoritmo de Porter

O algoritmo original de Porter ([1980](#por80)) está baseado em regras ou critérios: "Assuming that one is not making use of a stem dictionary, and that the purpose of the task is to improve IR performance, the suffix stripping program will usually be given an explicit list of suffixes, and, with each suffix, the criterion under which it may be removed from a word to leave a valid stem. This is the approach adopted here." ([Nota 10](#footnote10))

O algoritmo de Porter ([2005](#por05)) adaptado à língua portuguesa também está baseado em regras. Cinco são os passos realizados por ele (preliminarmente, é necessário tratar as vogais nasalizadas _ã_ e _õ_):  
- remoção dos sufixos;  
- remoção dos sufixos verbais, se o primeiro passo não realizou nenhuma alteração;  
- remoção do sufixo _i_, se precedido de _c_;  
- remoção dos sufixos residuais _os_, _a_, _i_, _o_, _á_, _í_, _ó_;  
- remoção dos sufixos _e_, _é_, _ê_ e tratamento da cedilha.

Ao final, devem-se voltar as vogais nasalizadas à sua forma original.

### O algoritmo de Orengo

O algoritmo de Orengo ([2004](#ore04)) está baseado em regras para a remoção de sufixos. No total, apresenta 199 delas. Contudo, as regras apresentam exceções, que são tratadas com base em um pequeno dicionário de 32 mil termos, permitindo uma redução em termos de _overstemming_ (quando parte do radical é removida pelo algoritmo) de 5% ([Orengo 2004: 49 e 51](#ore04)). Pode-se dizer, então, que o algoritmo é um misto das duas alternativas apresentadas por Russell e Norvig.

O algoritmo de Orengo ([2004: 50-53](#ore04)) apresenta oito etapas, que são apresentadas no Quadro 2.

<table><caption>

**Quadro 2: Descrição das etapas do removedor de sufixos**  
Fonte: Adaptado de Orengo ([2004: 50-53](#ore04))</caption>

<tbody>

<tr>

<th>Etapa</th>

<th>Descrição</th>

</tr>

<tr>

<td>

1\. Redução do plural</td>

<td>

Remove-se o final _-s_ indicativo de plural de palavras que não se constituem em exceções à regra, realizando modificações, quando necessário.</td>

</tr>

<tr>

<td>

2\. Redução do feminino</td>

<td>

Remove-se o final _-a_ de palavras femininas com base nos sufixos mais comuns.</td>

</tr>

<tr>

<td>

3\. Redução adverbial</td>

<td>

Remove-se o final _-mente_ de palavras que não se constituem em exceção.</td>

</tr>

<tr>

<td>

4\. Redução do aumentativo/diminutivo</td>

<td>Removem-se os indicadores de aumentativo e diminutivo mais comuns.</td>

</tr>

<tr>

<td>

5\. Redução nominal</td>

<td>Removem-se 61 sufixos possíveis para substantivos e adjetivos.</td>

</tr>

<tr>

<td>

6\. Redução verbal</td>

<td>Reduzem-se as formas verbais aos seus radicais.</td>

</tr>

<tr>

<td>

7\. Remoção de vogais</td>

<td>

Removem-se as vogais _a_, _e_ e _o_ das palavras que não foram tratadas pelos dois passos anteriores.</td>

</tr>

<tr>

<td>

8\. Remoção de acentos</td>

<td>Removem-se os sinais diacríticos das palavras.</td>

</tr>

</tbody>

</table>

Essas etapas ocorrem em uma seq?ência estabelecida previamente.

Note-se que algumas etapas são executadas apenas se determinada característica é encontrada. Isso ocorre no caso na redução do plural e na redução do feminino. Em outras duas situações, determinados procedimentos deixam de ser executados se uma etapa anterior foi realizada. É o caso da redução verbal, que só ocorre se a redução nominal não ocorreu. É, também, o caso da remoção de vogais, que ocorre somente quando a redução nominal e a redução verbal não foram feitas.

A Figura 1 apresenta o fluxograma do algoritmo de Orengo.

<figure><a id="figure1"></a>

![Figura 1](../p315fig1.jpg)

<figcaption>

**Figura 1: Seq?ência de passos para o removedor de sufixos** ([Nota 11](#footnote11)) Fonte: Orengo ([2005](#ore05)).</figcaption>

</figure>

### O algoritmo de Gonzalez

Chaves ([2003: 3](#cha03)) apresenta o algoritmo _Pegastemming_ de autoria de Gonzalez, mas não fornece muitas informações pertinentes ao seu funcionamento.

Esse algoritmo não realiza nenhum tratamento para artigos, conjunções e preposições ([Chaves 2003: 4](#cha03)). Tal comportamento pode ser facilmente comprovado por uma visita ao sítio onde o programa de radicalização reside (http://www.inf.pucrs.br/~gonzalez/ri/pesqdiss/analise.htm) e pela visualização dos resultados obtidos a partir de documentos previamente disponibilizados.

Em geral, esse algoritmo se preocupou em implementar a remoção de sufixos comuns.

A literatura não apresenta nenhuma outra menção ao algoritmo de Gonzalez, não existindo nenhuma descrição pública disponível do seu funcionamento interno.

## Problemas e lacunas dos algoritmos de radicalização em língua portuguesa

Russell e Norvig ([2004: 817](#rus04)) já mencionam a possibilidade de que um algoritmo de radicalização possa prejudicar, em vez de melhorar, a recuperação das informação.

Orengo ([2004: 49](#ore04)) simplifica os problemas existentes com a radicalização propondo duas categorias:  
a) os problemas de _overstemming_, ou seja, quando se remove parte do radical, o que resulta na associação de palavras não relacionadas;  
b) os problemas de _understemming_, ou seja, quando um sufixo não é removido, o que remete a uma situação em que não houve a radicalização.

De maneira específica, a língua portuguesa apresenta diversas razões para complicar o processo de radicalização. Orengo ([2004: 53-55](#ore04)), principalmente no que se refere à morfologia, cita:  
a) o número de exceções, dado o aparecimento comum de sufixos como terminações de palavras (o algoritmo de Porter não se preocupa com esse aspecto);  
b) o número de homógrafos, que podem pertencer a diferentes classes gramaticais, interferindo na categorização dos sufixos;  
c) a irregularidade na conjugação dos verbos;  
d) mudanças no radical morfológico, como no caso de _emissão_ e _emitir_;  
e) o uso de nomes próprios, que não deveriam ser radicalizados.

O algoritmo de Orengo apresenta uma sensível melhora em relação à versão aportuguesada do algoritmo de Porter ([Orengo 2004: 56](#ore04)).

O algoritmo de Porter reduz o tamanho do vocabulário em 44%, enquanto o algoritmo de Orengo reduz o tamanho em até 51%. Além disso, o algoritmo de Orengo gera menos erros de _understemming_ e de _overstemming_ ([Orengo 2004: 60](#ore04)).

Chaves ([2003: 5](#cha03)), em análise comparativa dos algoritmos de Gonzalez e Orengo, aferiu que o algoritmo de Orengo se comporta com melhor eficácia, gerando menos erros de _overstemming_ e _understemming_ no geral.

Assim, pode-se afirmar que, atualmente, o algoritmo proposto por Orengo é o mais adequado para o uso em língua portuguesa.

Apesar do funcionamento apropriado do algoritmo de Orengo e do avanço que ele representa, diversas considerações devem ser feitas.

A primeira delas envolve a própria lista de exceções. A partir do momento em que determinados termos são selecionados conscientemente por um especialista no funcionamento do algoritmo e se lhes imputa o caráter de serem exceções, parece evidente que se cria um vocabulário de pequenas proporções, cuja aparência esconde uma anterioridade objetiva, proposital. Assim, apesar de serem invocadas em situações específicas conforme o sufixo sendo removido, as exceções nada mais são do que o esforço de evitar que determinadas palavras previamente conhecidas conduzam a um problema de _overstemming_ ou _understemming_. Da mesma forma, os problemas do algoritmo ficam mascarados pelos desvios sancionados pelas exceções. Por fim, a lista de exceções cita, muitas vezes, palavras pouco comuns ([Orengo 2004: 136-142](#ore04)), preciosismo esse que pode influenciar a performance do sistema negativamente.

A segunda consideração trata de se encarar o radical morfológico, ou seja, o semantema, como o produto que deva ser gerado pelo algoritmo de radicalização. Essa idéia aparece implícita no trabalho de Orengo ([2004](#ore04)), contudo Porter ([1980](#por80)) parece não ver essa questão pelo mesmo prisma. Por isso, parece ser mais interessante para um algoritmo de radicalização que a forma verbal da primeira da pessoa do singular do presente do indicativo _mato_ seja reduzida a _mat-_, enquanto o substantivo _mato_ permaneça _mato_. Dessa, evitar-se-ia que _matem_, _matarás_, _mataria_ se confundissem com _mato_, _matos_ na recuperação dos documentos. Nesse sentido, a normalização ling?ística parece ser uma boa hipótese a ser estudada ([Arampatzis _et al._ 2000: 20](#ara00)). Todavia, cumpre salientar que os algoritmos de radicalização podem ser mais eficientes se não se preocuparem em produzirem única e exclusivamente semantemas.

A terceira consideração envolve o que se espera em nível de correção ortográfica. É de se notar que os algoritmos de radicalização partem do princípio de que o texto que processam se encontra de acordo com as regras de ortografia da língua. O algoritmo de Orengo, em vários momentos ([Orengo 2004: 136-142](#ore04)), requer, inclusive, que a acentuação tenha sido corretamente aplicada, assim como o de Porter ([2005](#por05)). Partindo-se da premissa de que os textos processados pelo algoritmo de radicalização são provenientes de fontes que priorizam a correção ortográfica, chega-se a um segundo problema, que é a especificação da consulta pelo usuário do sistema de recuperação da informação, que pode ou não ter respeito às convenções ling?ísticas. Obviamente, se se consideram acentos como relevantes no processamento do texto, no momento em que o usuário erroneamente informar _lençois_ ao invés de _lençóis_, o sistema de recuperação executará o mesmo algoritmo utilizado com o texto sobre a palavra-chave e retornará _lençois_, quando, na verdade, deveria trazer _lençol_ ([Nota 12](#footnote12)). Isso se torna uma característica pouco adequada em situações onde não existe controle sobre a entrada de dados feita pelo usuário.

O quarto ponto envolve a incapacidade de um algoritmo de radicalização tratar todos os casos e situações possíveis. Assim, por mais que o algoritmo de Orengo ([2004: 137](#ore04)) tenha buscado prever problemas por meio do tamanho mínimo do radical e de exceções, advérbios como _somente_ e _vãmente_ continuam sendo radicalizados de forma incorreta.

Em muitos casos, parece evidente que existe a necessidade de um reconhecimento morfológico-semântico das palavras, ou seja, seria satisfatório saber qual é a classe gramatical das palavras durante a aplicação do algoritmo de radicalização. Com esse tipo de informação, determinados passos de um algoritmo poderiam ser desprezados em função das características intrínsecas das palavras. Para a língua portuguesa, parece lógico que um advérbio não deva ser testado em relação a sufixos de plural; da mesma forma, sufixos verbais não deveriam ser testados com substantivos. Certamente, tal informação é difícil de depreender, uma vez que toma em consideração aspectos diversos, como termos adjacentes, pontuação e o próprio contexto. Em parte, essa é a proposta de Gonzalez _et al._ ([2003: 8](#gon03)), quando realiza o etiquetamento do texto de entrada com as classes morfológicas dos itens lexicais.

Outro ponto importante na avaliação da eficácia de um algoritmo de radicalização se refere à capacidade de tratar a multiplicidade de significados. Palavras como _casa_ (substantivo no singular) e _casa_ (verbo na 3a. pessoa do singular do indicativo presente) apresentam significados completamente distintos, mas ambas gerariam, caso a redução fosse adotada, o mesmo radical _cas-_. Nessa situação, se o algoritmo não objetivasse a redução das palavras em número de letras mas focasse exclusivamente o tratamento das representações únicas, poder-se-ia ter o radical do substantivo _casa_ como _cas-_ e o radical do verbo _casa_ como _casar_.

A última consideração trata de textos que se valem com freq?ência de termos estrangeiros, ou que são compostos por porções em mais de uma língua. Como os algoritmos de radicalização devem respeitar a morfologia de uma língua em específico, quando há termos de diferentes origens que não podem ser isolados ou corretamente identificados, o sistema de recuperação da informação tende a criar aberrações devido ao desconhecimento sobre a inserção de elementos de outro idioma no texto, até então supostamente escrito em uma única língua.

Todas as considerações acima resumem a problemática imposta por algoritmos de radicalização em língua portuguesa.

## Considerações finais

A recuperação da informação, em razão do aumento no número de documentos digitais e da necessidade inerente de pesquisar no seu conteúdo, tem-se mostrado cada vez mais importante na área da Ciência da Informação.

Uma das abordagens existentes no contexto da recuperação da informação é a chamada _radicalização_. A radicalização nada mais é do que a redução de variações de uma palavra em uma única representação. Apesar de existir a tendência de se associar essas representações ao semantema das palavras, não existe obrigatoriedade de correlação nesse sentido.

O algoritmo mais lembrado para a língua inglesa é o de Porter ([1980](#por80)), que teve adaptações para outros idiomas. Contudo, a remoção de sufixos depende da morfologia da língua a que se refere, obrigando, sempre, a existência de um algoritmo adaptado às especificidades. Para a língua portuguesa, o algoritmo mais moderno e mais completo é o proposto por Orengo ([2004](#ore04)).

Em geral, os algoritmos de radicalização apresentam alguns problemas e lacunas: a existência de uma lista de exceções mascara as falhas nas regras dos algoritmos; a melhor forma de representar um radical nem sempre é o seu semantema; a premissa de que o texto deve estar ortograficamente correto aumenta o risco de radicalizações incorretas, principalmente no momento da recuperação; a incapacidade de tratar todas as exceções por meio de regras; a necessidade deduzida de um reconhecimento semântico anterior à radicalização; a multiplicidade de significados das palavras, que geram radicais com sentidos distintos; a existência de termos estrangeiros nos textos.

Em suma, a discussão sobre a radicalização para a língua portuguesa se abre para quatro frentes: a primeira delas leva a crer que a complexidade da língua portuguesa inviabiliza uma radicalização satisfatória dos termos; a segunda incrementa a lista de exceções à medida que elas vão surgindo na visão do modelador do sistema de recuperação; a terceira utiliza uma estrutura de dicionário que visa a criar uma lista de variações com seus respectivos radicais; a quarta se vale do reconhecimento morfológico das palavras. Dessas quatro alternativas não estão excluídas as abordagens mistas. De qualquer maneira, a perfeição não é obtida por esta ou aquela forma, atualmente, mas tão-somente pela seleção humana baseada no contexto fornecido pelo texto.

Por fim, os algoritmos de radicalização em língua portuguesa, atualmente, não se prestam à perfeição, constituindo-se num vasto campo de pesquisa sobre recuperação inteligente da informação na área da Ciência da Informação.

## Notas

<a id="footnote1"></a>1\. Do inglês: "...há notáveis diferenças entre línguas diferentes na maneira de gerar formas derivadas e flexionadas, tal que a aplicação de técnicas específicas pode produzir resultados díspares de acordo com a língua dos documentos e das consultas." ([Figuerola, Gómez Díaz & López de San Román 2000: 1](#fig00))

<a id="footnote2"></a>2\. Do inglês: "o Algoritmo tem sido usado, citado e adaptado extensivamente ao longo dos últimos 20 anos" ([Porter 2005](#por05)).

<a id="footnote3"></a>3\. Frakes, W.B., & Baeza-Yates, R. (1992). _Information retrieval: data structures algorithms_. Upper Saddle River, NJ: Prentice Hall.

<a id="footnote4"></a>4\. Do espanhol: "Por _semantemas_ devem ser entendidos os elementos ling?ísticos que expressam as idéas das representações [...], e por _morfemas_, os que expressam as relações entre as idéias [...]. Os morfemas, portanto, expressam as relações que o espírito estabelece entre os semantemas." ([Vendryes 1958: 133](#ven58))

<a id="footnote5"></a>5\. Do inglês (os exemplos não foram traduzidos porque são características morfológicas da língua inglesa): "Uma forma ling?ística que não fornece uma semelhança fonético-ling?ística parcial com qualquer outra forma é uma forma _simples_ ou _morfema_. Assim, _bird_, _play_, _dance_, _cran-_, _-y_, _-ing_ são morfemas." ([Bloomfield 1969: 161](#blo69))

<a id="footnote6"></a>6\. Do inglês: "radicalização é o processo de reduzir as palavras ao seu radical morfológico" ([Allan & Kumaran 2003: 1](#all03)).

<a id="footnote7"></a>7\. Do inglês: "a radicalização é usada em muitos sistemas de recuperação da informação (RI) para reduzir palavras variantes a radicais comuns" ([Xu & Croft 1998: 1](#xuj98)).

<a id="footnote8"></a>8\. Do inglês: "é o processo de mesclar as formas variantes de uma palavra em uma representação comum, o radical" ([Orengo 2004: 48](#ore04)).

<a id="footnote9"></a>9\. Do inglês: "Em qualquer programa de remoção de sufixos para o trabalho da recuperação da informação, dois pontos devem ser tidos em mente. Primeiramente, os sufixos estão sendo removidos simplesmente para melhorar a performance da recuperação da informação, e não como um exercício ling?ístico. Isso significa que não é de todo óbvio saber sob que circunstâncias um sufixo deve ser removido, mesmo se nós pudermos determinar de maneira exata os sufixos de uma palavra por meios automáticos." ([Porter 1980](#por80))

<a id="footnote10"></a>10\. Do inglês: "Tendo-se a premissa de que não se está fazendo uso de um dicionário de radicais, e que o propósito da tarefa é melhorar a performance da recuperação da informação, será dada ao programa de remoção de sufixos uma lista explícita de sufixos, e, junto a cada sufixo, o critério que diz como ele pode ser removido da palavra para gerar um radical válido. Essa é a abordagem adotada aqui." ([Porter 1980](#por80))

<a id="footnote11"></a>11\. Do inglês, da esquerda para a direita, de cima para baixo, não seguindo obrigatoriamente o fluxo: "Início", "A palavra termina em 's'?", "Não", "Sim", "Redução do plural", "A palavra termina em 'a'?", "Não", "Sim", "Redução do feminino", "Redução do aumentativo", "Redução adverbial", "Redução nominal", "Sufixo removido", "Não", "Redução verbal", "Sim", "Sufixo removido", "Não", "Remove vogal", "Remove acentos", "Sim", "Fim".

<a id="footnote12"></a>12\. Exemplo dado com base no algoritmo de Orengo ([2004](#ore04)).

## References

*   <a id="ale02"></a>Alemayehu, N. & Willett, P. (2002). Stemming of Amharic words for information retrieval. _Literary and Linguistic Computing_, **17**(1), 1-17.
*   <a id="all03"></a>Allan, J. & Kumaran, G. (2003). [_Details on stemming in the language modeling framework_](http://www.webcitation.org/5NnpiKUq9). Amherst, MS: University of Massachusetts, Center for Intelligent Information Retrieval Recuperado em 1 apr. 2007 do http://ciir.cs.umass.edu/pubfiles/ir-284.pdf
*   <a id="ara00"></a>Arampatzis, A., van der Weide, T.P., van Bommel, P. & Koster, C.H.A. (2000). [Linguistically motivated information retrieval](http://www.webcitation.org/5Nnq3h6QN). In _Encyclopedia of Library and Information Science_. New York, NY: Marcel Dekker. Recuperado em 1 apr. 2007 do http://tinyurl.com/2uhwmt
*   <a id="bae99"></a>Baeza-Yates, R. & Ribeiro-Neto, B. (1999). _Modern information retrieval_. New York, NY: Addison Wesley.
*   <a id="blo69"></a>Bloomfield, L. (1969). _Language_. London: George Allen & Unwin.
*   <a id="cam80"></a>Câmara Júnior, J.M. (1980). _Princípios de ling?ística geral: como introdução aos estudos superiores da língua portuguesa_. (6a ed.). Rio de Janeiro, Brasil: Padrão.
*   <a id="car01"></a>Carlberger, J., Dalianis, H., Hassel, M. & Knutsson, O. (2001). [Improving precision in information retrieval for Swedish using stemming](http://www.webcitation.org/5NnvSj5BV). In _Proceedings of the NoDaLiDa '01: 13th Nordic conference on computational linguistics_, 2001, Uppsala. Uppsala, Suécia: Uppsala University. Recuperado em 1 apr. 2007 do http://www.nada.kth.se/~xmartin/papers/Stemming_NODALIDA01.pdf
*   <a id="cha03"></a>Chaves, M.S. (2003). [_Um estudo e apreciação sobre algoritmos de stemming para a língua portuguesa_](http://www.webcitation.org/5Nscr1EN4). Artigo apresentado nas IX Jornadas Iberoamericanas de Informática, Cartagena de Indias, Col?mbia. Recuperado em 1 apr. 2007 do http://xldb.di.fc.ul.pt/~mchaves/pg_portugues/public/stemming.pdf
*   <a id="ekm95"></a>Ekmekçioglu, F.Ç., Lynch, M.F. & Willett, P. (1995). Development and evaluation of conflation techniques for the implementation of a document retrieval system for Turkish text databases. _The New Review of Document and Text Management_, **1**, 131-146.
*   <a id="fig00"></a>Figuerola, C.G., Gómez Díaz, R. & López de San Román, E. (2000, dezembro). [Stemming and n-grams in Spanish: an evaluation of their impact on information retrieval](http://www.webcitation.org/5NnvUS51R). _Journal of Information Science_, **26**(6), 461-467\. Recuperado em 1 apr. 2007 do http://reina.usal.es/pub/figuerola2000stemming.pdf
*   <a id="fig03"></a>Figuerola, C.G., Gómez, R., Rodríguez, A.F.Z. & Berrocal, J.L.A. (2001). [Stemming in Spanish: a first approach to its impact on information retrieval](http://www.webcitation.org/5NnvVf6HV). In C. Peters (Ed.), _Results of the cross-language system evaluation campaing CLEF 2001_. Darmstadt, Alemanha: IEI-CNR. Recuperado em 1 apr. 2007 do http://www.ercim.org/publication/ws-proceedings/CLEF2/figuerola.pdf
*   <a id="gon03"></a>Gonzalez, M., Toscani, D., Rosa, L., Dorneles, R. & Lima, V.L.S. de. (2003). [Normalização de itens lexicais baseada em sufixos](http://www.webcitation.org/5NnvX7yyW). _Workshop em tecnologia da informação e da linguagem humana_, _1_. Recuperado em 1 apr. 2007 do http://www.nilc.icmc.usp.br/til2003/oral/gonzalez_21.pdf
*   <a id="kra94"></a>Kraaij, W. & Pohlmann, R. (1994). [Porter's stemming algorithm for Dutch](http://www.webcitation.org/5NnvYRNtk). In L.G.M. Noordman & W.A.M. Vroomen, _Informatiewetenschap 1994: Wetenschappelijke bijdragen aan de derde STINFON Conferentie_. [S.l.: s. n.]. Recuperado em 1 apr. 2007 do http://tinyurl.com/3b7yc2
*   <a id="kro93"></a>Krovetz, R. (1993). [Viewing morphology as an inference process](http://www.webcitation.org/5Nnva2Lm2). In _Proceedings of ACM-SIGIR93_, 1993, [s.l.]. [S.l.: s.n.]. Recuperado em 1 apr. 2007 do http://ciir.cs.umass.edu/pubfiles/ir-35.pdf
*   <a id="mer05"></a>[_Merriam-Webster online dictionary_](http://www.m-w.com). (2005). Recuperado em 1 apr. 2007 do http://www.m-w.com
*   <a id="nak03"></a>Nakov, P. (2003). [Design and evaluation of inflectional stemmer for Bulgarian](http://www.webcitation.org/5NnvbjCOm). In _Proceedings of Workshop on Balkan Language Resources and Tools_, 2003, Thessaloniki. Thessaloniki, Grécia: [s.n.]. Recuperado em 1 apr. 2007 do http://www.cs.berkeley.edu/~nakov/selected_papers_list/nakov_BLRT_BulStem.pdf
*   <a id="ore04"></a>Orengo, V.M. (2004). _Assessing relevance using automatically translated documents for cross-language information retrieval_. Tese de Doutorado, School of Computing Science, Middlesex University, London.
*   <a id="ore05"></a>Orengo, V.M. (2005). [_RSLP stemmer (Removedor de sufixos da lingua Portuguesa)_](http://www.webcitation.org/5NnvdIzOb). Recuperado em 1 apr. 2007 do http://www.inf.ufrgs.br/~vmorengo/rslp/index.htm
*   <a id="oxf95"></a>_Oxford advanced learner's dictionary_. (1995, 5a ed.). Oxford: Oxford University Press.
*   <a id="pai90"></a>Paice, C.D. (1990). [Another stemmer](http://www.webcitation.org/5Nnvetcz6). _SIGIR Forum_, **24**(3), 56-61\. Recuperado em 1 apr. 2007 do http://www.comp.lancs.ac.uk/computing/research/stemming/paice/article.htm
*   <a id="pop90"></a>Popovic, M. & Willett, P. (1990). Processing of documents and queries in a Slovene language free text retrieval system. _Literary and Linguistic Computing_, **5**(2), 182-190.
*   <a id="por80"></a>Porter, M. (1980). [An algorithm for suffix stripping](http://www.webcitation.org/5NnvgB3zG). _Program_, **14**(3), 130-137\. Recuperado em 1 apr. 2007 do http://www.tartarus.org/~martin/PorterStemmer/def.txt
*   <a id="por05"></a>Porter, M. (2005). [_Snowball_](http://snowball.tartarus.org/index.php). Recuperado em 1 apr. 2007 do http://snowball.tartarus.org/index.php
*   <a id="rad99"></a>Radical. (1999). In _Novo Aurélio Século XXI: o dicionário da língua portuguesa_. Rio de Janeiro, Brasil: Nova Fronteira.
*   <a id="rad01"></a>Radical. (2001). In _Dicionário Houaiss da língua portuguesa_. Rio de Janeiro, Brasil: Objetiva.
*   <a id="raz99"></a>Radicalização. (1999). In _Novo Aurélio Século XXI: o dicionário da língua portuguesa_. Rio de Janeiro, Brasil: Nova Fronteira.
*   <a id="raz01"></a>Radicalização. (2001). In _Dicionário Houaiss da língua portuguesa_. Rio de Janeiro, Brasil: Objetiva.
*   <a id="rus04"></a>Russell, S. & Norvig, P. (2004). _Inteligência artificial_. (2a ed.). Rio de Janeiro, Brasil: Elsevier.
*   <a id="sac91"></a>Sacconi, L.A. (1991). _Nossa gramática: teoria_. São Paulo, Brasil: Atual.
*   <a id="sch96"></a>Schinke, R., Greengrass, M., Robertson, A.M. & Willett, P. (1996). A stemming algorithm for Latin text databases. _Journal of Documentation_, **52**(2), 172-187.
*   <a id="sil04"></a>Silva, C.F. (2004). _Uso de informações ling?ísticas na etapa de pré-processamento em mineração de textos_. Dissertação de Mestrado, Ciências Exatas e Tecnológicas, Universidade do Vale do Rio dos Sinos, São Leopoldo, Brasil.
*   <a id="tem05"></a>_Temática Barsa_ (Vol. 8). (2005). Rio de Janeiro, Brasil: Barsa Planeta, 2005.
*   <a id="the99"></a>_The Newbury House dictionary of American English_. (1999). Boston, MA: Heinle & Heinle.
*   <a id="ven58"></a>Vendryes, J. (1958). _El lenguaje: introducción ling?ística a la historia_. México, D.F., México: UTEHA.
*   <a id="xuj98"></a>Xu, J. & Croft, W.B. (1998). [Corpus-based stemming using co-occurrence of word variants](http://www.webcitation.org/5NnviKSZU). _ACM Transactions on Information Systems_, **16**(1), 61-68\. Recuperado em 1 apr. 2007 do http://ciir.cs.umass.edu/pubfiles/ir-95.pdf http://ciir.cs.umass.edu/pubfiles/ir-95.pdf

#### <a id="abstract"></a>Abstract in English

> **Introduction**.The significance of information retrieval has been increased as a result of the increasing frequency with which digital files are created and used. One of the many strategies for processing texts for the indexing of their contents in information retrieval systems is called stemming, which consists in the reduction of similar words to the same unvarying representation. Stemming relies on the morphological structure of the language with which it is intended to work.  
> **Aim**.This paper aims at defining the concept of stemming, presenting the algorithms available for Portuguese and commenting on several topics related to the use of this type of algorithms.  
> **Method**. Literature review on stemming for Portuguese known to the authors.  
> **Results**. To this day, there are only three algorithms published which deal with stemming in Portuguese: Porter's, Orengo's and Gonzalez's.  
> **Conclusion**. Today, improvement in terms of stemming for the Portuguese language cannot be achieved successfully, since there is little research being done in this area.