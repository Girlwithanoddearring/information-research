#### Vol. 12 No. 3, April 2007

* * *

# Activity systems, information sharing and the development of organizational knowledge in two Finnish firms: an exploratory study using Activity Theory

#### [Gunilla Widén-Wulff](mailto:gunilla.widen-wulff@abo.fi)  
Information Studies, 
Åbo Akademi University, 
Tavastgatan 13, 
20500 Åbo, Finland

#### [Elisabeth Davenport](mailto:e.davenport@napier.ac.uk)  
School of Computing,
Napier University,
10 Colinton Road,
Edinburgh EH10 5DT, Scotland

#### Abstract

> **Introduction.** In this paper, we discuss the link between information sharing and organizational knowledge production in two very different organizations; a company that handles insurance claims and a small, entrepreneurial hi-tech company. We suggest that this link has not been adequately addressed by studies of information behaviour, though a number of recent papers have proposed that human information behaviour research should appropriate methods from workplace studies and computer-supported cooperative work to provide a richer account of organizational information and knowledge work.  
> **Method.** Two case studies of sharing practices in Finnish firms were carried out.  
> **Analysis.** The version of activity theory that has been developed by Engeström and other Finnish researchers was used to analyse the data. This has provided highly specific accounts of information sharing as a constituent of the varied processes that contribute to the development of organizational knowledge.  
> **Results.** The overall analysis has allowed us to explain how and why organizational information sharing happens in terms that go beyond the cognitive and descriptive accounts of our earlier studies.  
> **Conclusions.** Information behaviour is a repertoire of actions and operations and judgements about timing and ethics that are brought into play across work cycles and routines. From this perspective, the duality of organizational knowledge becomes clear: it is both individual and collective judgements about how to behave, and the incremental outcome of these judgements, embedded in decisions that support the objects of activity systems.

## Background and rationale

The need for a more holistic view of the information science field has been a concern lately, taking the social and communicative context more visibly into account when discussing the understanding of information ([Bates 2005](#bates2005)) and in developing areas like information seeking and retrieval ([Ingwersen and Järvelin 2005](#ingwersen2005)). This is an important discussion bringing the different lines of interest in the Library and Information Science domain towards a broader theoretical understanding of information behaviour in different contexts ([Spink and Cole 2006](#spink2006)).

Information behaviour research has mainly been involved with information seeking in context (ISIC), and information encountering, which constitute a distinct line of work ([Wilson and Allen 1998](#wilson1998), [Fisher __et al._._, 2005](#fisher2005)). This line of work may be differentiated in a number of ways from the LIS field's original focus on information organization and information retrieval. Information behaviour research derives many of its theories from observation of practice, rather than formal hypothesis testing; and, in acknowledging the messiness of the world, it admits a range of accounts of information activity such as first person testimony ('sense-making', critical incident technique, etc.) and third person narratives about individuals (usually ethnographic observation). Some of these accounts have achieved canonical status, including Kuhlthau's ([2004](#kuhlthau2004)) accounts of high school and undergraduate students involved in when writing course assignments; the accounts by Chatman and Pendleton ([1995](#chatman1995)) of impoverished everyday living; Dervin's ([1992](#dervin1992)) accounts of sense-making and information as therapy; and the accounts by Vakkari ([2003](#vakkari2003)) and Byström and Hansen ([2002](#bystrom2002)) of task-based information seeking.

Given the emphasis on cognitive approaches to both information seeking and information retrieval, there have been few studies of organizational information behaviour though a number of library and information science researchers have appropriated methods from cognate domains (such as social studies of technology, social informatics, workplace studies and computer-supported co-operative work) to explore phenomena other than individual information behaviour. For example, Kling and his colleagues ([Elliott and Kling 1997](#elliott1997), [Kling and McKim 2000](#kling2000)) have explored organizational information work in a number of domains (law, academia, finance, etc.) using a multi-level social informatics approach ([Kling and Scacchi 1982](#kling1982)). Bowker and Star ([1999](#bowker1999)) have explored classification work in terms of organizational politics and draw on actor network theory (e.g [Latour 1987](#Latour1987)) in their accounts. Poltrock _et al._ have explained complex and extensive corporate information procedures using techniques from cognitive engineering ([Poltrock _et al._ 2003](#poltrock2003)), and Bartlett and Toms ([2005](#bartlett2005)) have explored the activities of bio-informatics specialists by combining this approach with human information behaviour reserach protocols. A further approach to understanding digital libraries in terms of activities and Activity Theory is offered by Spasser ([2002](#spasser2002)).

Activity theory is an approach to understanding learning that presents the individual and social dimensions of the process as inseparably coupled. Subjects and objects mutually co-define each other in a process of continual transformation, mediated by artefacts, rules and roles that evolve through shifting social groupings, or communities. Engeström ([1999](#engestrom1999)) describes organizational learning (a proxy for our 'the development of organizational knowledge') in terms of an overall activity system, made up of many different activity systems that can be observed at different levels of work - a meeting may be an articulation of an activity system, as may the work of a project team, or an organizational department (and see [Kuutti 1996](#kuutti1996)). An explicit link between activity theory and the human information behaviour domain has recently been made by Wilson ([2006](#wilson2006)), who suggests that activity theory can provide a much needed stimulus to human information behaviour research in a number of ways. It offers a systematic n-dimensional analytic framework that can guide both observation and interpretation. It embeds studies in a wider organizational framework that allows the intersection of behaviour and processes to be observed and assessed over time and across a range of organizational activities. It sharpens our definition of information behaviour by applying this framework to observable activities. We suggest that a further claim can be made for activity theory as a means to study information behaviour: it can throw light on the ways in which information behaviour contributes to the development of organizational knowledge. In the text that follows, we present an analysis using activity theory of two case studies of information sharing behaviour in Finnish companies.

## Aims and methods

Our aims in the paper are twofold:

1.  Using activity theory, to articulate ways in which the information behaviour of individuals and groups intersects with organizational processes and contributes to the development of organizational knowledge.
2.  To evaluate Activity Theory as an analytic framework for studying information behaviour in organizations.

## Methods and material

Our two case studies are of quite different kinds of groups. One presents routine-based work, whereas the other case presents specialist activities in an expert organization. The sectors involved are knowledge intensive: the financial and biotechnology industries. We focus on sharing as an example of information behaviour.

The data for the study were collected between December 2003-May 2004 in a project that explored information sharing practices in business organizations ([Widén-Wulff and Ginman 2004](#widen-wulff2004); [Widén-Wulff and Davenport 2005](#widen-wulff2005); [Widén-Wulff 2007](#widen-wulff2007a)). The project involved semi-structured interviews with the following main themes;

*   information seeking;
*   information sharing;
*   tasks undertaken in the workplace;
*   collaboration;
*   construction of organizational memory.

Two protocols were used for interviews. The first was based on a number of earlier studies of information sharing behaviour and social capital formation, on the expectation that a link between information behaviour and organizational learning might be established in the intersection of these topic domains. Social capital studies were included because they look at the conditions within a group or organization that promote sharing and exchange and organizational learning. Common measures for studying the constituents of social capital are membership in informal and formal associations and networks and the trust, norms, values that facilitate exchange ([Davenport and Snyder 2005](#davenport2005); [Krishna and Schrader 2002](#krishna2002); [Schuller 2001](#schuller2001); [Woolcock and Narayan 2000](#woolcock2000)). The measures for the actual information interactions were based on the classification scheme developed by Cool and Belkin ([2002](#cool2002)). The classification scheme has five major facets, by combination of which multiple interactions of people with information can be described. The method was developed to identify, describe and classify a range of information seeking strategies in a group of knowledge-intensive workers (for further discussion on how the classification scheme was used see Huvila and Widén-Wulff ([2006](#huvila2006))). A separate interview protocol covered cultural aspects of collaboration (such as group identity, norms following, and cooperation). This was based on studies by Tyler and Blader ([2000](#tyler2000); [2001](#tyler2001)), who have analysed people's cooperative behaviour in groups.

The analysis of the interviews has been undertaken in different phases that focus on different dimensions of sharing. For example, one analysis explored the theme of individual motivation to share ([Widén-Wulff 2007](#widen-wulff2007a)). Organizational information and knowledge production were not focal areas of that analysis. A further study explored timing as an important factor in sharing ([Widén-Wulff and Davenport 2005](#widen-wulff2005)). The second study inevitably involved sociality and identified distinctive 'information rhythms', which shaped the development of organizational knowledge in the two case study firms. But the findings were somewhat non-specific and we attempted to design a more probing instrument to analyse how processes, activities and timing intersect to produce knowledge.

In our first attempt to address this issue, we evolved a 'dual axis' coding schema. High level codes were used to describe organizational activities and behaviour across each of the organizations ('lateral' codes), and more specific codes described aspects of individual practice ('vertical' codes). The former allowed the coders to derive a number of broad units of the analysis such as types of information sources used, and patterns of personal contacts. From the latter, a number of 'information profiles' were developed that covered roles, responsibilities, and reporting status. Though this approach provided material for constructing a number of descriptive typologies, it did not help us understand the relationships between specific instances of sharing behaviour, activities and knowledge production within and across different levels of organizational activity. Consequently, we turned to activity theory in our current attempt at analysis. We are confident that the data collected with the original interview protocol (itself derived from a mix of studies) are comprehensive and pertinent and can support an activity theory analysis. The analysis is indicative - a sample specifically designed for an activity theory project would have covered a larger number of intersecting communities over a longer period of time (see [Virkkunen and Kuutti 2000](#virkkunen2000)). However, there are precedents ([Nardi 1996](#nardi1996); [Wilson 2006](#wilson2006)) for indicative studies using activity theory to analyse data that were collected without an activity theory analysis in mind.

## Case One - activity analysis of an insurance claims handling unit

Case One presents the work of the claims applications unit in a Finnish insurance business group offering insurance and financial services to small- and medium-sized companies and private persons. The company was established in the beginning of the 20th century and has been operating as a group of different insurance services since 2001\. The company employs about 440 persons and has a nationwide service network with more than thirty offices. The claims applications unit is situated in the life insurance branch of the company and has about thirty employees. Ten persons from this unit were interviewed.

We have focused on three different subjects in our account of activity systems in the insurance company: general claims handlers, claims committee specialists, and claims administration personnel. Each of these is described in terms of its own activity model, though the three groups intersect in the overall outcome of the unit's activity, i.e., the establishment of a reliable and legitimate knowledge base for the company. We draw heavily in our analysis on the terminology and examples in Kuutti ([1996](#kuutti1996)), and Virkkunen and Kuutti ([2000](#virkkunen2000)). The first offers a walkthrough of a putative scenario (software engineering); the second offers a detailed longitudinal study of the work of a Health and Safety Inspectorate.

Kuutti states that an activity is a form of 'doing directed to an object' ([1996: 27](#kuutti1996)) and that artefacts (broadly defined) are always present to mediate between actors and objects. Objects are heterogeneous and are shared by participants in an activity (the pertinent community) and the motivation for an activity is the transformation of an object into an outcome. The relationship between subject and community is mediated by rules, and that between object and community by the division of labour ([1996: 27-29](#kuutti1996)). Sometimes one activity may contribute to more than one object, and this may result in distortions and contradictions.

The objects of activities are transformed into outcomes over time, and shorter term processes - actions - must be taken into account. Activities consist of chains of actions whose significance will vary when the same action features in different activities. Actions in turn consist of operations, or 'work defined habitual routines used as answers to conditions faced during the performing of an action' ([1996: 31](#kuutti1996)). Very familiar actions can 'collapse' into operations, though when conditions change an operation can unfold back into an action. Wilson ([2006: 11](#wilson2006)) suggests that these fine-grained distinctions are pertinent to information behaviour research. The constituents of activities are fluid; as conditions change, their status in an activity hierarchy may change. This fluidity allows the activity model to account for development. Figure 1 presents a version of Engestrom's ([1999](#engestrom1999)) activity theory template.

<figure>

![Figure 1](../p310fig1.jpg)

<figcaption>

**Figure 1: Engestrom's activity theory diagram ([Engestrom, 1987](#eng87): 78)**</figcaption>

</figure>

In our first activity analysis ('Activity 1A' - note that we refer throughout the text that follows to the activities that are analysed by the case numbered in sequence, followed by an activity alphabetised in sequence), the subject is general claims handlers, whose object is to validate claims. This activity is mediated by artefacts: the claims unit database, the expertise of colleagues in the group. The motivation of subjects in Activity 1A is to become knowledgeable, and the outcome is reliable claims processing.

The object - to validate claims - is shared with other members in the general claims handling group (the community) and is achieved by means of a division of labour that is both formal and informal, and that emerges from pragmatic judgements about who can answer what, in terms of physical proximity and expertise. Table 1 (below) illustrates this.

<table><caption>

**Table 1: Distribution of labour in Activity 1A**</caption>

<tbody>

<tr>

<th>Person</th>

<th>Tasks</th>

<th>Years in the org.</th>

<th>Division of labour criteria</th>

</tr>

<tr>

<td>A</td>

<td>Claims handler</td>

<td>0-1</td>

<td>

1\. Physical 2\. Reliability 3\. Expertise 4\. Personal</td>

</tr>

<tr>

<td>F</td>

<td>Claims handler</td>

<td>2-5</td>

<td>

1\. Physical 2\. Expertise</td>

</tr>

<tr>

<td>G</td>

<td>Claims handler</td>

<td>0-1</td>

<td>

1\. Physical 2\. Expertise</td>

</tr>

<tr>

<td>H</td>

<td>Claims handler</td>

<td>6-10</td>

<td>

1\. Physical 2\. Expertise</td>

</tr>

<tr>

<td>J</td>

<td>Claims handler</td>

<td>11-15</td>

<td>

1\. Expertise</td>

</tr>

</tbody>

</table>

The subject is related to the community through a set of rules that are also formal (such as explicit instructions for checking and filing claims) and informal (tacit rules about whom to consult and when). These rules and roles constitute the practice of the community.

It is at the level of actions that we can gain specific understanding about different information behaviours, and thus address our first objective of articulating the role of information sharing in knowledge production. Typical actions in the general claims-handling activity are keyword searching, asking questions of colleagues, responding to queries and volunteering information. Many of the questions and answers involve internal process knowledge ('who to' and 'how to' questions), with more knowledgeable and experienced colleagues in a given area helping the less knowledgeable and experienced:

> There are about ten persons to collaborate or share information with every day. It is sometimes complicated to make claims handling decisions but it is good that you usually get information from other persons in the unit rather than seek for all the information yourself. (Claims handler F)

In many instances, information sharing could not be attributed to a disposition or cognitive style, yet it is in this particular context that work gets done: information is given in response to the stimulus for a request. Routine requests are handled by querying formal sources:

> We use specific sources and they are reliable. We don't search so broadly that you have to assess reliability. (Claims handler H)

A typology of the content that is shared at different points in Activity1A is presented below in Table 2.

<table><caption>

**Table 2: Shared content in Activity 1A**</caption>

<tbody>

<tr>

<th>Internal mediation (claims database)</th>

<th>External mediation (Internet)</th>

</tr>

<tr>

<td>Rules, clauses, principles, best practice</td>

<td>Formal information, Medical information, Customer information</td>

</tr>

<tr>

<td>

**Normative interaction (meetings)**</td>

<td>

**Community**</td>

</tr>

<tr>

<td>Cases and best practice, General information</td>

<td>Validation of information, Access to information for special work tasks, Best practice, Access to information about other sources</td>

</tr>

</tbody>

</table>

Such routine or operationalised actions continue until exceptional or novel claims cause uncertainty and controversy, or until shifts in medical knowledge and financial regulations de-routinise activity. These types of disruption are described as 'contradictions' by Engeström ([1999](#engestrom1999)), who classifies them as first order (within an activity system), second order (across two activity systems) and so on. Contradictions stimulate organizational learning: as participants work through their differences, understanding evolves, and new knowledge is produced, both locally - members of the group change the way they do things - and globally - negotiation over contradictions results in shifts in company wide definitions of what a claim is. (We illustrate this with an example of a 'difficult case' in a later section.)

Subjects in Activity 1B are claims procedure specialists, whose object is to establish what counts as a legitimate claim. This is undertaken through actions such as consulting with company experts including medical, legal and financial specialists whose views are taken into account in setting criteria for validating claims. These people work outside the claims handling department, monitoring and evaluating web resources to establish which sites are reliable, notifying other colleagues in the department of these and informing those colleagues of shifts in the criteria by which claims may be validated:

> You have to be careful what you seek for and from where. E.g. about research results I usually check from several sources. (Claims procedure E)

These actions are closely tied to the division of labour in the group. The authority of claims procedure specialists is higher than that of general claims handlers where judgements about the reliability of sources are concerned, and they are often proactive, placing sites on recommended lists for example (in itself a form of operationalisation). Contradictions may arise within Activity1B where subjects disagree about the criteria for a legitimate claim, or where judgements about legitimacy are contested across Activity1A and Activity1B. These are discussed in plenary meetings once a week:

> _"Collective knowledge is well defined in the weekly meetings. You discuss a problem case and someone in the group can refer to an earlier experience. And the information is shared to everyone in the meeting. From several persons experience a collective knowledge is structured, this way it functions very well." (Claims procedure E)_

These meetings are sites where Activities 1A and 1B overlap. Organizational knowledge emerges as subjects work their way through contradictions within and across activity systems in a process that appears to corroborate Engeström's view of expansive learning.

The weekly meetings are organized by the Head of Claims Administration (the subject of Activity1C, whose object is to control the quality of the unit's knowledge base). She is the ultimate authority on what is preserved as examples of legitimate claims. This is achieved through actions such as organizing weekly meetings where disputes are resolved, ensuring that minutes are kept, arbitrating in disputes, identifying best practice and deciding what will be filed in the database as legitimate claims. The community of Activity1C is members of other activity systems whose knowledge is incomplete,perhaps because they are in a state of contradiction or because they are novices:

> _"There are always good discussions about the conditions and principles. From these you learn a lot". (Claims handler G)_

The rules that moderate the community in Activity1C are not always predetermined, as it is in this activity system that rules, roles and artefacts are formed and legitimised. A prime mediator in this process is the internal unit database which assembles examples that have been discussed and endorsed by the claims collective of what is legitimate and what counts as best practice. The internal database is a Microsoft Word document where claims handling rules and insurance clauses are filed. The various reimbursement principles, along with the descriptions and agreements from the claims handling meetings are saved here, constituting the collected _best practices_. The document is updated regularly after the claims handling meetings and information is retrieved by keyword search. The database stabilises the knowledge of the unit, until a change in legislation, or a difficult case de-stabilises current criteria and leads to the revision of existing knowledge.

Overlap arises in cases where participation in community is shared across activities, where rules are applied in common, or through intersecting mediations such as meetings (as we note above) or common access to the claims database. A 'difficult case', for example, might involve decisions about re-insuring a client, where, for example, there is new information about the customer's health conditions. Claims procedure specialists assist claims handlers with advice in such cases, mostly with investigations on the customers' medical conditions. This entails reading different documents, collaborating with doctors in the company, and evaluating decisions to see if they comply with the insurance clauses and conditions. The trajectory of such a case can be traced through a series of decisions about who to consult, judgements about the reliability of sources (from inside and outside the unit), discussions about the exemplary status of cases, and the eventual embedding of a case in the unit's knowledge base. Though many of these decisions are made by individuals (whose judgement is legitimised by their role within an activity system), the process of translation across roles and activities leads to an organizational or collective outcome. One claims procedure specialist observes:

> It is better that as many as possible know how to handle the claims. It is a big advantage if you dare to ask for advice; you see that in the meetings. Everyone has bad and good days, and you can get really difficult cases. And you have to ask for advice. And usually you verify your own thoughts - I have decided to do it like this, do you agree? Or are there other solutions? This is important.(Claims procedure I)

Our discussion of the activity system(s) of the claims handling unit is far from complete. Because of the limitations of data collection (ten interviews), we have not been able to represent the multiple activity systems that would present the work of the claims unit in relation to the larger organization (for example, the activity that links claims administration personnel to medical specialists). But the analysis does articulate the work of this small sample as part of a larger organizational knowledge dynamic, by allowing us to systematically link actions - such as seeking, sharing, validation, evaluation - to formal and informal processes. In the case of the claims unit, the knowledge that is produced is both procedural ('How do we handle this?') and declarative ('This is what counts as a legitimate claim'). By emphasising mobility, fluidity, development and learning, activity theory overcomes the limitations of more static task analysis, and provides a vocabulary to describe evolving knowledge production in terms of specific information behaviour.

Our brief summary of the information habits and habitat of the claims firm reveals a complicated, but not a complex environment, where there are few surprises, because anomalies can be handled within a structure of interconnected responsibilities and routines. At many points in the claims handling process, individual knowledge is brought into play. Individual judgement underpins the selection of sources for decisions about cases, and informs the answers given to less experienced and less knowledgeable colleagues. But individual judgements cannot be separated from collective knowledge work - they are called into question by the group and debated, and they are made in the context of prior experience in the group. In this environment, knowledge is largely defined retrospectively, in terms of what has been agreed, and is modified in responsive mode.

## Case Two: Activity analysis of a biotechnology firm

The focus of Case Two is a small biotechnology company that has fourteen employees divided into six functions of the company (Top management, board, financial, training, marketing, sales department, research and development (R&D) and communications). Working practice in Case Two is, on the surface, highly fragmented. The company consists of a number of domain experts, who work together on projects in teams of different sizes and composition and, at any given time, experts may be working on a number of concurrent projects. In addition, there are four infrastructural or intermediary roles: R&D support, Administration, Corporate Communications and Training. The data analysed were gathered in interviews with these intermediaries, who can account for the activities of the domain experts with whom they interact. Each of the experts draws on the knowledge of other persons affiliated to a number of institutions inside and outside Finland. In addition, there is a global training and customer network. Tables 3 and 4 (below) convey some of the richness of the communities that experts inhabit.

<table><caption>

**Table 3: External contacts in Activities 2A 2B 2C**</caption>

<tbody>

<tr>

<td>

**Work task**</td>

<td>

**Source**</td>

<td>

**Purpose**</td>

</tr>

<tr>

<td>

**Researcher**</td>

<td>Dentists / experts</td>

<td>Valuation of new products, prediction</td>

</tr>

<tr>

<td>

**Communication**</td>

<td>Information about research</td>

<td>Make visible to a greater audience</td>

</tr>

<tr>

<td></td>

<td>Information about distributors needs</td>

<td>To support their trade</td>

</tr>

<tr>

<td>

**Administration**</td>

<td>Distributors</td>

<td>Strategies</td>

</tr>

<tr>

<td></td>

<td>Official organizations (tax, insurance, customs)</td>

<td>General administration, compliance</td>

</tr>

</tbody>

</table>

As the company produces leading edge dental technology products, the environment is volatile and decisions about product development must take account of emerging trends and market niches. A number of researchers have addressed issues that arise in such turbulent project environments, focusing on knowledge transfer (or non-transfer) across projects , and development (or non-development) of social capital ([Newell & Huang 2005](#Newell2005)) within project teams. Few researchers, however, have explored the interplay of institutional knowledge and project knowledge and it is only recently that research attention has foucused on knowledge infrastructures ([Hine 2006](#Hine2006)). Activity Theory provides a framework for such exploration by providing a means to model activity systems with a common template but from different points of view (the different contexts of the team, the project, the organizational line function, etc.). Activity Theory thus reveals intersection points where resources, interpretations, expertises overlap, and shows where decisions must be made about boundaries, about who does what and about what criteria to apply: these are the stuff of organizational knowledge.

The overall activity system in Case Two has, as subject, a group of affiliated subject experts or specialist knowledge workers, whose object is to continuously embed their knowledge in innovative products for dentistry. The overall outcome of the system may be seen as the transformation of practice in the dental domain. As we have shown, knowledge practice in Case One is largely retrospective, and concerned with compliance and validation work - 'difficult cases' and adapting to changes in rules and regulations are exceptional activities. In contrast, information and knowledge practices in Case Two are largely prospective - choices must be made about what products to develop, what markets to target, who to assemble in project teams. Reliability is more nuanced in this environment than in Case One. The administrator who was interviewed observed:

> Information can be facts and information can be verifying facts...it is interesting and important to know how this information is collected or produced in order to judge its reliability. Information can be a "wild guess", this is ok, but must be presented as such in order to be able to question it on that level. Having this context, information or knowledge can then be judged. (Administrator)

The subject/object relationship is mediated by varied artefacts - reports, articles, records, computer files, a server, a database (though as we indicate below this is a problem area), meetings, e-mail and conversations. Table 4 (below) summarises these.

<table><caption>

**Table 4: Shared content in activity system 2**</caption>

<tbody>

<tr>

<td>

**External mediation**</td>

<td>

**Internal mediation**</td>

</tr>

<tr>

<td>Market related contacts, Advice from external experts (dentists, doctors, lawyers), Market reports, Databases (Medline), Research articles</td>

<td>Own research & reports, Company handbook, Internal database (IS), Customer management & marketing software, Web pages</td>

</tr>

<tr>

<td>

**Normative interactions**</td>

<td>

**Community**</td>

</tr>

<tr>

<td>Meetings (working froups, units, board, projects), Training programme for new staff</td>

<td>Personal networks, Open office landscape, Coffee - lunch breaks, 'Corridor discussions'</td>

</tr>

</tbody>

</table>

The organizational structure is minimal in this firm:

> We have no units, only a person responsible for his or her area. But then we gather teams for specific purposes, which are responsible for a part of the communication or the continuing communication of a matter. (Researcher)

What binds the community together is motivation:

> We strongly believe in our product...a unique thing. (Trainer)

> You need to feel that you are part of the thing that this company is about. (Communications expert)

The community produces output through the division of labour among the fourteen employees. These employees comply with rules (some are in-house rules and some are personal judgements) about whom to take information from and to whom to give it, acting in a timely fashion, only providing information that has been verified. For example:

> You must be very concentrated, but at the same time you need to have the whole process internalised so that you are able to stop and think who might need this information. Even if I'm in a hurry, I must respect the duty to share information to others. (Training)

In Case Two, each of the constituents of the activity model is addressed in a parsimonious way. The organization is a lean machine and is compared by one employee to a relay race, with the information or knowledge 'baton' passed from player to player to realise the final goal. As we note above, our analysis of Case Two draws on interviews with four employees whose responsibilities intersect with those of projects and teams, but whose activity sustains the organization as a whole. Their work remits, like those of the other employees in the company, comprises multiple activity systems. We present activity systems for three of these employees (administration, R&D, corporate communications) and, in addition, draw on the insights of the fourth (training).

Research and development work (Activity 2A) is heterogeneous, involving conversing in the corridor, arranging meetings, contributing to team projects, identifying appropriate sources, retrieval, extraction of items from relevant literature in different technical and commercial domains, documenting internal innovations, organizing evaluation of company products, contacting experts from opinions and judgements, environmental scanning and reporting. Though this subject can be described in terms of several sub-systems, we focus here on support for product development. Activity 2A is mediated by a variety of artefacts; documents, databases, expert reports and so on, but these are brought into play by many different persons: the Researcher himself, outside library experts and external domain experts, etc. These actors are representative of the division of labour that characterises this R&D community. The overall outcome of Activity 2A, that is, intermediate knowledge products that are used to support decisions about product development and production processes, is moderated by the Researcher himself, applying rules (many of which are valid in other activity systems) about what is pertinent and what is reliable. Intelligence work for product R&D is also undertaken by domain experts who may rely on their own judgement and whose team-based activity systems overlap with Activity 2A. Rules are hot spots in such overlaps and negotiations about reliability criteria may be taken to meetings to be resolved. Although there is a handbook,

> [The rules] don't cover a logical sequence... On the other hand, this is not a negative thing, written rules on everything are soon out of date... and rules vary in different contexts. (Administrator)

Both of these activity systems, R&D and that of domain experts, feed into strategic decisions at Board level and intersect there with the activities of the administration. The expert employees who are on the Board have an opportunity to act as advocates for particular lines of development. Decisions at this level are competitive, and the same mediating artefacts (strategic plans, marketing summaries) will be appropriated in different ways by different champions. The perceived reliability on the intelligence that supports a particular case is important in such situations:

> The products must be based on research facts - there is no room for mistakes. (Communications expert)

Activity 2B is Corporate Communication, where the object is to inform insiders and outsiders of the firm's achievements. The intended outcome is to sustain and improve the visibility of the firm's products. The object is shared with insiders and outsiders who participate through a number of different roles, and who constitute a corporate communications community within the activity system. This community draws on the personal networks of the subject. Activities 2A and 2B intersect as they share artefacts (A's output of intermediate knowledge products are part of B's mediating artefacts). But, as we see below, they have contradictory views on the firm's sharing culture. In our attempt to understand the links between information sharing and organizational knowledge, we focus here on internal visibility. This is achieved through a number of mediations - campaigns, meetings, documents, reports, e-mails, and a customer database, which, however, was dysfunctional at the time of the fieldwork. Much of the mediation emerges from informal, oral communication. The communications specialist has a very positive view of openness in the community:

> You are really close to the different phases of the process, production, and results. You can see where things come from, and where they go.

All of her colleagues, in her view, have a duty to share:

> I must tell my expertise, knowledge to my colleague and he/she must tell me. Together we have worked out the surfaces of knowledge - who knows what.

Some of her colleagues, whose activity systems intersect with Activity 2B, do not share this view. The Researcher (Activity2A), for example, observed that information flows are far from streamlined:

> There is too much nonsense information, and lack of information between the functions at the same time.

Activity 2B involves standard information actions: retrieving, extracting, verifying, synthesising, for example, and information sharing is tightly coupled with the creation of associated information products (such as reports and campaign materials). Subject 2B's actions also cover standard corporate communications such as maintaining the company Website, producing a customer newsletter and organizing formal meetings. Much of the information sharing in this activity is informal: _The open office landscape means that everything is public information._ and happens in corridor discussions and informal meetings. These informal interactions are an important means of resolving contradictions, such as how to interpret documents produced by other domain specialists:

> A challenge in this kind of organization is to combine information from different sources all the time; be organized and think of who should know about this... Information means different things depending on experience. The analysis of information is different, all the time you must remember to question: What does it mean? (Communications specialist)

To some extent, the rules for resolving such issues are shared with Activity 2A (for example, 'treat as reliable if the material comes from the primary expert; validate if it is mediated by another'). But there is a set of issues relating to time management that is raised by the corporate communication specialist's almost evangelical commitment to reciprocity. Attention is a very scarce resource in this economy. The corporate communications specialist's injunction to bear in mind _From whom should I receive information? To who(m) should I send information?_ must always be compromised by the demands of other activity systems, as the Researcher observes:

> There is constant interacting and it is important that everyone participate enough in information sharing. But not too much.

As with Activity 2A, attempts are made to solve contradictions at meetings. The Administrator (Activity 2C) describes an attempt to improve matters with a particular information event:

> It is not as effective as it could be, people tend to go there and listen, but not to discuss. And we have actually not allocated very much... limited... time to it.

Activity 2C is administration, and its object is to maintain infrastructure to support the firm's strategy. The outcome is internal continuity that contributes to the long-term future of the firm. Like the other intermediary activity systems presented in the paper, Activity 2C consists of subsystems with different objects (infrastructure is both material and institutional, for example), and the activity system overlaps at a number of points (rules, mediation, community) with other internal activity systems. The community is potaentially everybody employed by the firm, though at any given point in time, the composition of the group will depend upon circumstances. The administrator, who is the primary actor in System 2C is the longest serving employee in the firm, and commands considerable respect.

We focus here on the institutional activity system. The object is to ensure that due process is observed in company activities through fiscal and regulatory compliance, open decision-making and reliable documentation. As due process involves both formal (external) regulation, and (internal) normative practice, different sets of rules are brought into play: the Administrator herself can exploit a wide-ranging network of contacts in the insurance and legal industries and due process may be formative as well as responsive. The Administrator manages a four-monthly planning cycle by means of which the firm's disparate projects are gathered into a coherent work flow. The board meetings function as the company knowledge base, where decisions are made on the basis of reports from all of the different functions. As _Half of our staff are members of the board_, these meetings are important points of intersection between systems. Each of the six domain experts on the Board represents a number of activity systems.

The relationship between subject and object is mediated by a range of artefacts - strategy documents, verbal messages, minutes of meetings, reports, insurance documents, tax compliance documents, verbal messages. One artefact - the customer management system - was a major area of contradiction. At the time of the fieldwork, much of the firm's internal planning information was held in shared files on a common server. The customer management system was meant to integrate information on customers, distributors, markets, research, competitors and so on, but, in the words of the Administrator:

> The system has become the garbage can of information. This is because there is not enough structure, common language and mutual understanding of its value. And this is then a good excuse not to use it. This is a shame.

Given the issues of reliability, and constraints on time budgets revealed in our analysis so far, this (all too common) account of lack of engagement is not surprising.

A number of actions are associated with Activity 2C: gathering background information, reporting to the Board, sales prediction and market analyses, office management and logistics and general information service functions. Because of the pivotal role of the administrator, timing and reliability are pressing concerns:

> I get mad if I get wrong information and especially if I, based on that information, complain to another party. This makes you then very careful, and you become also suspicious towards the organization! (Administrator)

Our analysis of the activity systems of the three intermediaries presents only a small part of the complex of activity in Case Two. Even so, we have shown how overlapping and intersecting these systems are and how the outcomes of activities will tend to be indeterminate. The differences between Cases One and Two are deceptive - in each organization, information from different sources undergoes a series of transformations that carry the incremental judgements of different actors until it is stabilised as knowledge in an approved forum: for example, the meetings chaired by the Head of Claims Administration in Case One, and the Board in Case Two, where we can see most clearly how expert knowledge, translated through activity systems, is embedded in decisions about innovative products.

## Conclusions

This paper has addressed the relationship between information behaviour and the production of organizational knowledge, taking sharing as an example of behaviour. Our brief review of current human information behaviour research suggested that organizational issues have been neglected in information behaviour research, though a number of recent studies have appropriated methods from cognate fields in an attempt to broaden the scope of inquiry. Following Wilson ([2006](#wilson2006)), we have presented an analysis of information behaviour in two very different Finnish companies that applies activity theory to a dataset collected for an earlier project. We stated that we had two aims in undertaking the analysis: to articulate ways in which individual and group information behaviour intersect with organizational processes and contribute to the development of organizational knowledge and to evaluate activity theory as an analytic framework for studying information behaviour in organizations. We have fulfilled our first aim in the case study analyses that are presented above, and focus on the second aim of evaluating activity theory in the text that follows.

Activity theory has expanded our understanding of information behaviour in two major ways. First, it has forced us to clarify our terminology. Many human information behaviour studies are confounded by indeterminate terminology; a point made recently by Bartlett and Toms ([2005](#bartlett2005)). By tying a term like 'information sharing' to a range of activities and actions whose salience varies across a number of organizational processes we are forced into specific usage. We have been able to distinguish nuances in behaviour by framing actions, and the coupling of actor and artefact, in a process model. A discussion of the nature of organizations as information processing mechanisms (and of the merits of that view), or of the validity of terms like knowledge management, or of the stability and instability of knowledge (e.g., [Capurro 1997](#capurro1997)) is outside the modest scope of this paper. Within the purview of the two case studies, information and knowledge appear to work as a duality. In its simplest version (much of the sharing behaviour in Case One, for example) the duality works like this: you ask for information on a topic, I tell you what I know. My 'knowledge' (which is your 'information') may or may not become part of your knowledge, as you construct your own version of the topic. As activity theory indicates, the interaction may be mediated in many different ways: through a database, in a meeting (as in the case of the collective judgements on difficult cases in Case One) and may be synchronous or asynchronous. In some cases, knowledge may be proffered without the specific stimulus of a request - as in the case of the expert reports and newsletters that are produced in Case Two, where their status as information is questioned by recipients who do not share that expertise. Respondents are aware that knowledge is inherently contextualised, and that they must always make a judgement about reliability when material comes from others.

Secondly, as noted by Wilson, activity theory forces us to expand the horizons within which we observe and explore behaviour. Actions and operations are traced across different organizational processes, and sequences of inputs and outputs are made visible in ways that cannot be understood when research is based on more limited accounts of task-based work. Information behaviour is a repertoire of actions and operations and judgements about timing and ethics brought into play across work cycles and routines. From this perspective, the duality of organizational knowledge becomes clear: it is both individual and collective judgements about how to behave (when to give information, when to withhold it, when to accept something as reliable and so on) and the incremental outcome of these judgements, embedded in decisions that support the objects of activity systems.

## Acknowledgements

We wish to thank the anonymous reviewers whose comments have greatly improved the paper.

## References

*   <a id="bartlett2005"></a>Bartlett, J. & Toms, E. (2005). Developing a protocol for bioinformatics analysis: an integrated information behaviour and task analysis approach. _Journal of the American Society for Information Science and Technology_, **56**(5), 469-482
*   <a id="bates2005"></a>Bates, M.J. (2005). Information and knowledge: an evolutionary framework for information science_Information Research_, **10**(4) Retrieved 22 February 2007 from http://InformationR.net/ir/10-4/paper239.html
*   <a id="bowker1999"></a>Bowker, G. &Star, L. (1999). _Sorting things out._ Cambridge MA: MIT Press.
*   <a id="bystrom2002"></a>Byström, K. & Hansen, P. (2002). Work tasks as units for analysis in information. In H. Bruce, R. Fidel, P. Ingwersen & P. Vakkari (Eds.)_Emerging Frameworks and Methods. Proceedings of the Fourth International Conference on Concepts of Library and Information Science (CoLIS4)_ (pp. 239-251). Greenwood Village: Libraries Unlimited.
*   <a id="capurro1997"></a>Capurro, R. (1997). _Stable knowledge?_ Paper presented at the workshop: Knowledge for the Future - Wissen für die Zukunft, Brandenburgische Technische Universitüt Cottbus, Zentrum für Technik und Gesellschaft, March 19 - 21 1997\. Retrieved 10 February 2007 from http://www.capurro.de/cottbus.htm
*   <a id="chatman1995"></a>Chatman, E. &Pendleton, V. (1995). Knowledge gap, information-seeking and the poor. _Reference Librarian_, (49/50), 135-145
*   <a id="cool2002"></a>Cool, C. &Belkin, N.J. (2002). A classificationn of interactions with information. In H. Bruce, R. Fidel, P. Ingwersen & P. Vakkari (Eds.)_Emerging Frameworks and Methods. Proceedings of the Fourth International Conference on Concepts of Library and Information Science (CoLIS4)_ (pp. 1-15). Greenwood Village: Libraries Unlimited.
*   <a id="davenport2005"></a>Davenport E. & Snyder, H. (2005). Managing social capital. _Annual Review of Information Science and Technology_. **39**, 517-550.
*   <a id="dervin1992"></a>Dervin, B. (1992). From the mind's eye of the user: the sense-making qualitative-quantitative methodology. In J.D. Glazier &R.R. Powell (Eds.), _Qualitative research in information management (pp. 61-84)._ Englewood: Libraries Unlimited.
*   <a id="elliott1997"></a>Elliott, M. &Kling, R. (1997). Organizational usability of digital libraries: case study of legal research in civil and criminal courts. _Journal of the American Society for Information Science_, **48** (11) 1023-1035
*   <a id="eng87"></a>Engeström, Y. (1987). _Learning by expanding: an activity-theoretical approach to developmental research._ Helsinki: Orienta-Konsultit.
*   <a id="engestrom1999"></a>Engeström, Y. (1999). Activity theory and individual and social transformation. In. Y. Engeström, R. Miettinen &R-L. Punamäki (Eds.), _Perspectives on activity theory (pp. 19-38)._ Cambridge: Cambridge U.P.
*   <a id="fisher2005"></a>Fisher, K., Erdelez, S. &McKechinie, L. (Eds.) (2005). _Theories of information behavior._ Medford NJ: Information Today.
*   <a id="hine2006"></a>Hine, C. (2006). _New infrastructures for knowledge production: understanding science._ Hershey PA: Information Science Publishing.
*   <a id="huvila2006"></a>Huvila, I. &Widén-Wulff, G. (2006). Perspectives to the classification of information interactions: the Cool and Belkin faceted classification scheme under scrunity. _Proceedings of the First International Conference on Information Interaction in Context 18-20 October, Copenhagen (pp. 144-152)._Copenhagen: Association for computing Machinery (ACM).
*   <a id="ingwersen2005"></a>Ingwersen, P. &Järvelin, K. (2005). _The turn: integration of information seeking and retrieval in context._ Dordrecht, the Netherlands: Springer.
*   <a id="kling1982"></a>Kling, R. &Scacchi, W. (1982). The web of computing: computer technology as social organization. _Advances in Computers,_ **21** 1-90.
*   <a id="kling2000"></a>Kling, R. &McKim, G. (2000). Not just a matter of time: field differences and the shaping of electronic media in supporting scientific communication. _Journal of the American Society for Information Science,_ **51**(14), 1306-1320.
*   <a id="krishna2002"></a>Krishna, A. &Schrader, E. (2002). The social capital assessment tool. In C. Grootaert &T. v. Bastelaer (Eds.), _Understanding and measuring social capital_ (pp. 17-44). Washington, DC: International Bank for Reconstruction and Developments.
*   <a id="kuhlthau2004"></a>Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services._ Westport, CT: Libraries Unlimited.
*   <a id="kuutti1996"></a>Kuutti, K. (1996). Activity theory as a potential framework for human-computer interaction research. In B. Nardi (Ed.), _Context and consciousness: activity theory and human-computer interaction_. (pp. 17-44). Cambridge MA: MIT Press.
*   <a id="latour1987"></a>Latour, B. (1987). _Science in action: how to follow scientists and engineers through society._ Milton Keynes: Open University Press.
*   <a id="nardi1996"></a>Nardi, B. (1996). Some reflections on the application of activity theory. In B. Nardi (Ed.), _Context and consciousness: activity theory and human-computer interaction_ (pp. 235-246). Cambridge MA: MIT Press.
*   <a id="newell2005"></a>Newell, S. & Huang, J. (2005). Knowledge integration processes and dynamics within the context of cross-function projects. In P. Love, P.S.W. Fong &Z. Irani (Eds.), _Management of knowledge in projects_ (pp. 19-40). Oxford: Butterworth-Heinemann.
*   <a id="poltrock2003"></a>Poltrock, S., Grudin, J., Dumais, S., Fidel, R., Bruce, H. & Pejtersen, A.M. (2003). Information seeking and sharing in design teams. _Proceedings of the 2003 International ACM SIGGROUP conference on supporting group work_ (pp. 239-247). New York, NY: ACM Press.
*   <a id="schuller2001"></a>Schuller, T. (2001). The complementary roles of human and social capital. _Isuma_, **2**(1). Retrieved 22 February 2007 from http://www.isuma.net/v02n01/schuller/schuller_e.shtml
*   <a id="spasser2001"></a>Spasser, M.A. (2002). Realist activity theory for digital library evaluation: conceptual framework and case study. _Computer Supported Cooperative Work_ **11**(1/2), 81-110\. Also available at http://www.ics.uci.edu/~redmiles/activity/final-issue/Spasser/Spasser.doc
*   <a id="spink2006"></a>Spink, A. &Cole, C. (2006). _New directions in human information behaviour._ Dordrecht: Kluwer.
*   <a id="tyler2000"></a>Tyler, T.R. &Blader, S.L. (2000). _Cooperation in groups: procedural justice, social identity, and behavioral engagement._ Philadelphia: Psychology Press.
*   <a id="tyler2001"></a>Tyler, T.R. &Blader, S.L. (2001). Identity and co-operative behavior in groups. _Group Processing &Intergroup Relations,_ **4**(3) 207-226.
*   <a id="vakkari2003"></a>Vakkari, P. (2003). Task based information searching. _Annual Review of Information Science and Technology,_ **37**, 413-464.
*   <a id="virkkunen2000"></a>Virkkunen, J. &Kuutti, K. (2000). Understanding organizational learning by focusing on activity systems. _Accounting Management and Information Technologies,_**10** 291-319.
*   <a id="widen-wulff2007a"></a>Widén-Wulff, G. (2007). Motives for sharing: social networks as information sources. _Advances in Library Administration and Organization,_ **25**, (forthcoming).
*   <a id="widen-wulff2005"></a>Widén-Wulff, G. & Davenport, E. (2005). Information sharing and timing: findings in two Finnish organizations. In F. Crestani &I. Ruthven (Eds.), _Information Context: Nature, Impact, and Role: 5th International Conference on Conceptions of Library and Information Sciences, CoLIS 2005, Glasgow, UK, June 4-8 2005_ (pp. 32-46). Berlin: Springer-Verlag.
*   <a id="widen-wulff2004"></a>Widén-Wulff, G. &Ginman, M. (2004). Explaining knowledge sharing in organizations through the dimensions of social capital. _Journal of Information Science,_ **30**(5), 448-458.
*   <a id="wilson2006"></a>Wilson, T.D. (2006). A re-examination of information seeking behaviour in the context of activity theory. _Information Research_, **11**(4) paper 260 Retrieved 20 March, è00ø from http://InformationR.net/ir/11-4/paper260.html
*   <a id="wilson1998"></a>Wilson, T.D. &Allen, D. (Eds.) (1998). _Exploring the contexts of information behaviour. Proceedings of the Second International Conference on Research in Information Needs, Seeking and Use in Different Contexts_. London: Taylor Graham.
*   <a id="woolcock2000"></a>Woolcock, M. &Narayan, D. (2000). Social capital: implications for development theory, research, and policy. _World Bank Research Observer,_ **15**(2) 225-249.