#### Vol. 12 No. 3, April 2007

* * *

# Activity Theory in information systems research and practice: theoretical underpinnings for an information systems development model

#### [Ánja Mursu](mailto:anja.mursu@uku.fi), [Irmeli Luukkonen,](mailto:irmeli.luukkonen@uku.fi) [Marika Toivanen ](mailto:marika.toivanen@cs.uku.fi) and [Mikko Korpela](mailto:mikko.korpela@uku.fi)  
University of Kuopio  
Department of Computer Science and  
Information Technology Centre  
Kuopio, Finland

#### Abstract

> **Introduction.** The purpose of information systems is to facilitate work activities: here we consider how Activity Theory can be applied in information systems development. Method. The requirements for an analytical model for emancipatory, work-oriented information systems research and practice are specified. Previous research work in Activity Theory is then elaborated to satisfy these requirements in parallel with practical applications.  
> **Elaboration**. Activity Analysis and Development (ActAD) which sees work activity as a systemic entity, its constituent elements and networks of activities, is introduced and the information systems viewpoint is applied throughout the elaboration. Information systems development is modelled as an activity, applying the ActAD framework.  
> **Results**. Our contribution is a participatory development model called the Activity Driven ISD Model, aimed at developing work and information systems in parallel, in three phases: understanding the present state of an activity; describing the goal state of the activity; and planning for the transformation to the goal state.  
> **Conclusion**. The ActAD framework and the Activity Driven ISD Model meet six of the seven criteria set. The seventh requirement will be satisfied after the finalisation of the model. The experiences in our pilot studies concerning its applicability to the practitioners have been encouraging.

## Introduction

This paper deals with socio-technical information systems that consist of information, a technology, system, communication, an organization and people. The purpose of information systems is to facilitate and help work activities; an information system is a human activity system. We define an information system as the use of information technology (manual or computer-based) in a collective work activity, either as a means of work or of co-ordination and communication. Accordingly, we define information systems development as the process by which some collective work activity is facilitated by new information-technological means through analysis, design, implementation, introduction and sustained support.

Philosophically we regard information systems development as a critical process ([Orlikowski and Baroudi 1991](#orl91)), as an emancipatory and developmental process. By that we mean a process where people have a say in their work activities and information systems at the same time. An improved information system should mean that workers can do their work better. Information systems development projects are risky due to factors such as high complexity, high mutability, high situation-dependency and high risk of value conflicts (e.g. [Bødker _et al._ 2004](#bod04)). Thus we are looking for methods that are work-oriented and applicable to participatory information systems development. At the same time the methods should be operational and theoretically sound ([Korpela _et al._ 2004](#kor04)). Theoretically sound methods explain the subject matter better and are well-grounded, thus making it possible to change the method into a more operational form. We started to study the possibilities of Activity Theory as an optimal approach for creating such a method.

Activity Theory has been applied in Information Systems for more than a decade, mainly by applying Engeström's ([1987](#eng87), [1999](#eng99)) Developmental Work Research (DWR) model. Activity Theory was first presented as an information systems research approach at the IFIP WG 8.2 conference in Copenhagen in 1991\. Kuutti ([1991](#kuu91)) suggested that the object of analysis in information system should be _activity system_ instead of _information system_. Similarly, Bødker ([1991](#bod91)) discussed the potential of Activity Theory as an analytical framework in understanding computer-based artefacts as instruments for work activities and materials for systems design. During the decade and a half since the Copenhagen conference, Activity Theory has been applied to research in the fields of Human-Computer Interaction, Computer-Supported Cooperative Work, Information Systems and Information Science in a few studies (e.g., [Bardram 2000](#bar00); [Bertelsen & Bødker 2000](#ber00); [Bødker 1997](#bod97); [Mwanza 2001](#mwa01); [Redmiles 2002](#red02); [Vrazalic & Gould 2001](#vra01); [Wilson 2006](#wil06)).

Activity Theory in general and Engeström’s Developmental Work Research in particular emphasize that activity is a collective phenomenon, involving several actors. The studies reviewed above, however, tend to reduce activity to a set of actions by an individual – the doctor’s activity, the patient’s activity, etc. The analytical model does not guide the researcher clearly enough in studying the individual and collective aspects of work activities within the same framework. This leads to a relatively weak support to designers of cooperative systems ([Korpela _et al._ 2004](#kor04)).

There are some activity-based methods for information systems development (see the review paper by [Quek and Shah 2004](#que04)). However, these methods are not yet practicable enough to be used by practitioners without Activity Theory knowledge; they are limited to a narrow scope of information systems development, or focusing on a narrow scope of practice. In addition, there is a lack of practical guidelines for practitioners on how to apply or operationalize a method for practical use (e.g., Activity Oriented Design Method, [Mwanza 2002](#mwa02)).

The strengths of Activity Theory are based on its long historical roots. It is a philosophical and cross-disciplinary framework for studying different forms of human practices as development processes, with both individual and social levels interlinked at the same time ([Kuutti 1996](#kuu96)). The strengths of Activity Theory are the interaction in a social context and the idea of dynamics and development of a work activity. However, although Activity Theory considers context to be important, it is quite restricted concerning the broader socio-economic context.

We consider Activity Theory to be a promising approach to work-oriented and participatory information systems development. But instead of creating a method or methodology, which means a quite systematic way of conducting certain processes, we collected usable methods under the activity-philosophical approach by creating an activity-philosophical model. By applying this model one can create one's own situation bound methodology for information systems development.

The need for an analytical model for work-oriented information system research and practice comes from the requirement that people doing their everyday tasks and duties should have an opportunity to make an impact on the prospective information systems. We suggest that such a model should meet the following requirements ([Korpela _et al._ 2004](#kor04)):

*   The starting point must be work activity as a systemic entity.
*   Technology, including computer-based technology, must be seen as a tool to facilitate work, embedded in the work system.
*   Both collective and individual aspects of work need to be taken into account.
*   Work systems need to be studied in their organizational context
*   The analytical model must be based on a sound theoretical basis
*   The analytical model must be applicable to both descriptive studies and practical development.
*   The analytical model must be applicable to both technological development by software and IS professionals and the development of work practice itself by the workers.

Such a model has been under development and in use for several years now at the University of Kuopio in three research projects: PlugIT ([www.plugit.fi](http://www.plugit.fi/)), INDEHELA ([www.uku.fi/indehela](http://www.uku.fi/indehela)) and ZipIT ([www.uku.fi/zipit](http://www.uku.fi/zipit)). The model, or the _Activity Driven Information Systems Development Model_ as it is called at the moment, has its basis in Activity Theory and exploits the framework called Activity Analysis and Development (ActAD) (Korpela _et al._ 2000 2004). The ActAD framework has been applied and elaborated in several cases in the research projects and it has evolved into the Activity-Driven Information Systems Development Model (hereafter referred to as the Activity-Driven Model)during the latest ZipIT research project. The objective of the ZipIT project is to narrow the distance between work improvement and information systems development and the model will contribute to that.

The purpose of this paper is to describe the theoretical basis of the Activity Driven information systems development Model and to show that Activity Theory is very functional in information systems research and practice. The paper is composed of the following parts. First, an introduction and definition of the ActAD framework and its elements is provided. The next section introduces our aims to cover broader socio-economic contexts of the environment into the model by identifying integrative levels of analysis. Next, we describe how information systems development should be considered from the work-oriented viewpoint. After that we provide some examples of applying ActAD in previous projects. Then, we briefly introduce the objectives and basic ideas of the Activity Driven information systems development Model. Finally, in discussion, we relate the Activity-Driven Model to the presented requirements.

## Activity Analysis and Development (ActAD)

In this section we present the ActAD framework by providing an overview and the theoretical construction of the elements (adapted from Korpela _et al._ 2004). We first illustrate our concepts with an example from intensive care in a hospital. In treating a seriously ill baby, several healthcare professionals need to engage in a care _process_ and take action within it (Figure 1). The ill health of the baby is the _object_ of the activity in this case and the improvement of her health condition is the intended _outcome_, which is the purpose (motive) of the activity ([Korpela _et al._ 2004](#kor04)).

<figure>

![Figure 1](../p311fig1.jpg)

<figcaption>

**Figure 1: The object and intended outcome of an activity in neonatal intensive care case.**</figcaption>

</figure>

Looking closer at the process of the activity, in Figure 2 we have two of the _actors_ (nurses) required in the care process engaged in _actions_, making use of different _means of work_ (both physical and mental). In order to achieve the intended outcome, it is important that the individual actions are coordinated towards the shared goal. Various _means of coordination and communication_ are used to that end; in this case a vital signs monitor helps the nurses to coordinate their actions. ([Korpela _et al._ 2004](#kor04))

<figure>

![Figure 2](../p311fig2.jpg)

<figcaption>

**Figure 2: Two actors performing actions that are part of the activity, with two types of means.**</figcaption>

</figure>

When the health condition of the baby has improved sufficiently, the intensive care activity is completed and the responsibility for her further care is handed over to an ordinary ward (Figure 3). The outcome of the intensive care activity is transformed into the object of the ward activity. A third type of means, _means of networking_, is required to mediate the relation between the activities. In this case, patient documents are such a means ([Korpela _et al._ 2004](#kor04)).

<figure>

![Figure 3](../p311fig3.jpg)

<figcaption>

**Figure 3: Intensive care activity becoming a ward activity.**</figcaption>

</figure>

The concepts introduced in the intensive care case in an informal manner are now presented as an abstract framework (Figure 4). This framework provides a check list for analysing a work activity and its elements are presented below. We start with work activity as a whole, which is the focus of the framework.

<figure>

![Figure 4](../p311fig4.jpg)

<figcaption>

**Figure 4: The ActAD framework: the structure and relations of a work activity as a systemic entity ([Mursu _et al._ 2003](#mur03); adapted from [Korpela _et al._ 2000](#kor00)).**</figcaption>

</figure>

### Work activity

The basic unit of analysis in Activity Theory is an activity which ties individual actions to context. Actions without context are meaningless. Leont'ev ([1978](#leo78)) elaborated on Vygotsky’s concept of ‘human activity mediated by tools and signs’. He created a three-level hierarchical structure where collective activity is the holistic basic unit of analysis (Table 1).

<table><caption>

**Table 1: Leont'ev’s three-level model ([Engeström _et al._ 1990,](#eng90) p. 140)**</caption>

<tbody>

<tr>

<th>Unit</th>

<th>Directing factor</th>

<th>Subject</th>

</tr>

<tr>

<td>Activity</td>

<td>Object / motive</td>

<td>Collective</td>

</tr>

<tr>

<td>Action</td>

<td>Goal</td>

<td>Individual or group</td>

</tr>

<tr>

<td>Operation</td>

<td>Conditions</td>

<td>Non-conscious</td>

</tr>

</tbody>

</table>

Activity is a collective phenomenon with a shared object and motive. The activity is divided into actions through the division of work. Action is conducted by an individual or group using some kind of tool. The tool can be physical or intellectual or abstract, such as knowledge or skills. Usually the action is more limited in time and it is directed by a goal, which is more concrete than a motive. However, to have a reasonable action, the actor should consider her action in relation to the motivated activity. Operation is a non-conscious form of doing work, created by routine actions. The difference between activity, action or operation is not clear-cut or stable.

The application of Activity Theory in information systems research and practice often focuses on individual activity (or action) rather than collective activity (see the Introduction). This is quite understandable since the interest is usually in human computer interaction, user interfaces, or computer supported cooperative work. Thus the focus of analysis has been on the information system (or rather information technology), not on the work activity system. There are certainly some exceptions (e.g., [Helle 2000](#hel00), [Kuutti 1994](#kuu94), [Hyysalo and Lehenkari 2001](#hyy01)), which have applied Engeström's Developmental Work Research to study work activities. The activity driven approach and model in this article emphasizes the collective nature of work activities, as in multi-professional teams in the health care sector. This collective nature should be represented in the systems development process as a participative method.

In the proposed Activity Driven information systems development Model the interest is not just in any activity, but in _work_ activity. In work activity, people have a motive and a shared object. Usually the motive in work activity comes from organizational objectives, or from strategic or political definitions. The motive can also be based on national or global concerns.

An activity has a dynamic nature; it changes and develops during its life cycle, although there are some stable tasks and processes as well. In the information systems development process the focus should be on tasks and processes that are about to change because of the new system, on the situations in which information is used and decisions are made. Cultural mediation, i.e. the relationship between different elements in an activity, is situation bound; each activity has developed in different cultural processes.

The most important features of an activity, besides the motive and the object, are mediation, focus, its heterogeneous and historic nature and inner contradictions (which can be innovations as well) ([Engeström 1999](#eng99)). Activity systems are characterized by inner contradictions; they are not stable and harmonious systems. It is characteristic of work in information systems that the collective activity is not necessarily obvious, since people can work in different places at different times, even without knowing each other and in different organizations. However, the issue is whether the work involves a collective group and an information system, or an individual and an information tool.

### Elements of work activity

Looking at the elements of the work activity, the framework starts from the elements of a _mediated action by an individual_ (Figure 4, broken line); the subject or actor, the object of the action, the instruments or means (both mental and physical) needed for the action, as well as the goal ([Vygotsky 1978](#vyg78)), all take place within a work process.

#### Actors, subjects

Usually we describe an actor as a member of a working group, carrying out specific tasks individually or in a team, using different kinds of facilities and skills, independently or in cooperation. What is important is that the actor is part of an activity, doing her work and focusing on a shared goal, based on the motive of an activity.

In practice it almost always requires several actions by several individuals to produce any useful service or product (Figure 4, lower half). In Activity Theory, such a set of mediated actions on a shared object by a number of actors, directed by a (more or less consciously) jointly desired outcome, is called an _activity_ ([Leont'ev 1978](#leo78)).

In the Activity-Driven Model we can observe an actor as an individual in her work, or analyse a certain task or function as an exemplar of professional action. It is important to note that individual human actions can only be understood through the collective activity of which they are a part. Instead of groups of uncoordinated actions, work, in practice, consists of systemic activities subordinating the actions in a purposeful way. For example in the health care sector, it would be restrictive to analyse only the doctor's work or the nurse's work in patient treatment. In addition to these, attention should be paid to multi-professional cooperative work and how this is organized and managed.

#### Means of work

Actors work on their objects using specific means of work, i.e. different kinds of tools, facilities, artefacts and also mental skills, knowledge and so forth. In all mediated work there is a need for information about how to use the means. It is important to consider where actors get the information needed in the work process, how they use that information and where they record or save new information. There are also many kinds of information, e.g., formal information and tacit information, as well as professional skills.

In the Activity Driven ISD Model the main focus is on the information management means, not on every piece of work, such as using a thermometer in a nursing situation. However, the location of different tools could be worth analysing in some cases, for example creating a physical model of the work place or environment ([e.g., Beyer and Holtzblatt 1998](#bey98)).

#### Object

An object is the target of the actors' actions. The object is part of the shared goal and actors should recognize the object of the work. The first experiences of the object are based on external features, which are transformed into deeper knowledge of the object as the work process continues. In the work process the actor (or collective actor) and the object are in interaction, mediated by the means of work.

#### Work process

The work process includes an object of the activity, a transformation towards an outcome and the outcome. The object is where the actors aim to contribute, to create a transformation process to achieve an intended outcome. It is not simply changing something into something else, but more related to the motive of an activity - it is purposeful.

In work processes or production lines we can see different levels depending on whether we are interested in activity networks (processes and their relations, see Network of activities below), an individual work process in its context, or the actions of different people in an individual work process. The elaborateness of analysis and descriptions is based on that level.

The primary focus in the ActAD framework is on work processes by professionals within an activity, the information flows and information management. It is crucial to create a collective understanding among information system designers and actors about how they are working, what kind of tasks are being carried out, what information tools are used and how, who are the other actors involved and so on. We take a group of professionals and analyse their relation to the information system in use, thus the interaction is between group and information system. We can view this level as the actors' perspective in work processes. An actor has a goal, tools, colleagues and rules when s/he is working on the goal and transforms it into the intended outcome.

#### Means of coordination and communication

In addition to the instruments or means of the individual actions, other kinds of mediating instruments – ‘social infrastructure’ – are also needed within an activity, as emphasized by Engeström ([1987](#eng87)). The actions need to be oriented by means of coordination and communication (Figure 4, upper half). Among the means of coordination and communication we can include the division of work and rules and also physical artefacts such as multi-user computer systems.

### Mode of operation and contradictions

According to Engeström ([1987](#eng87)), work activity as a real-world phenomenon is _systemic_ by nature. That is, there must be a relative fit between the elements of a work activity, a _mode of operation_ (Figure 4, large oval). When an activity evolves over time, it moves from one relative fit to another according to zones of proximal development, from one mode to another, in historical phases. _Contradictions_, imbalances within and between various elements and the mode, are the force driving the activity to change.

The relation between the elements creates the mode of operation. If the elements are not in balance, because of a new information tool somewhere inside the activity, for example, it causes contradictions and creates changes in other elements to reach a balance again. The mode of operation is a relatively stable mode; it has been developed during different phases in its history. It has several stable structures and culture, professional identity and cultural mediation. It creates a community for the information system. Within the mode of operation the activities should be developed based on the _zone of proximal development_: "the distance between the present everyday actions of the individuals and the historically new form of the societal activity that can be collectively generated as a solution to the inner contradictions embedded in the everyday actions" ([Engeström & Engeström 1986: 2](#eng86)). In information systems development this is not the case in many places. New systems are put into place with no one knowing even the present state of work activity. However, the development should be dependent on organizational culture and values.

Engeström ([1987](#eng87)) divides contradictions into four levels: primary inner contradictions within each constituent component of the central activity, secondary contradictions between the constituents of the central activity, tertiary contradictions between the object and motive of the dominant form of the central activity and the object and motive of a culturally more advanced form of the central activity and quaternary contradictions between the central activity and its neighbouring activities.

On the primary inner level, contradictions cause frictions, for example, because of contradictory objectives, such as an effective work process and data security. On the secondary level, the elements develop differently in relation to each others. There are external pressures and requirements directed to the elements, or there will be new external factors such as rules or tools. On the tertiary level, contradictions emerge when work processes are reorganized and the old mode of operation is rebelling against the new mode of operation. On the quarternary level, the mode of operation is not in balance with its environment or activity network.

### Network of activities

Finally, activities do not stand alone. The elements of one activity are produced by other activities and the outcome of one activity is usually needed in one or more other activities (Figure 4, smaller ovals; [Engeström 1987](#eng87)). Mediation is also needed between the activities and this is achieved by _means of networking_ ([Korpela _et al._ 2000](#kor00)).

The uppermost level of activities and the network of activities provide an understanding of the chain of activities, where the product or the outcome of the service chain is produced. Usually we need a network of activities, including different supporting activities and producing activities, to finalize the outcome. We can view this level as the clients' perspective, because the goal is to provide an outcome to the next activity and finally there is a final outcome, e.g., a recovered patient or a car.

Sometimes it is very useful to climb even higher, to see the landscape of the social construction of different service providers, for example in health care. We have to consider the legislation, national programs and policies, competitors and so forth. Sometimes it is useful to see the global picture as well, to identify the international factors involved. Even if these uppermost levels are not described in detail, they usually have to be considered in development solutions. For example, in health care there are many international standardization programmes in progress that need to be considered in computer-based systems.

Networks of activities constitute the “metabolism of use-values” in society; i.e., activities are the “organs” that produce, exchange and consume use-values in the “body” of society. The networks are split by organizational boundaries and accompanied by other kinds of societal relations, e.g., financial ones dealing with exchange-values. Social theories other than Activity Theory should be applied to the organizational, financial and wider societal contexts of activities ([Korpela _et al._ 2001](#kor01)).

#### Means of networking

The relations between activities usually need some means for networking and data sharing, to manage the information transferred. This networking can take different forms. For example in the health care sector the information transformation between special health care and a health centre can flow electronically, on paper, by phone, with a patient and so on. To create a whole picture of an activity network, it is crucial to describe the means of networking.

### Information systems in work activity

According to the framework, information and communication technology can be used as a means in three different ways: as a means of work in individual actions, as a means of coordination and communication for collaboration and cooperation between actions in an activity and as a means of networking between activities. However, the scopes overlap; for example one instrument can be both a means of work and a means of coordination, e.g., an electronic patient record. From the viewpoint of supporting work activities by information and communication technology, all three types of means are needed.

In agreement with Kuutti ([1991](#kuu91)), we maintain that the systemic unit that information systems researchers and practitioners should consider first is (work) activity in all its aspects and dynamics. It is not very important whether the information management facilities and processes within an activity or a network form an information _system_ – the point is whether the use of information and communication technology facilitates the objectives of the activity.

However, information and communication technology is becoming increasingly widespread and it is one of the most important types of means in and between many information-laden activities. The need for new information-technological means is one of the most common sources for change in work and information systems projects are one of the most common forms of change ([Korpela _et al._ 2002](#kor02)).

## Integrative levels of activity

In the Activity Driven ISD Model we follow Walsham's ([2000](#wal00)) suggestion that information systems researchers should cover all the levels of analysis from the individual to the societal in their research agenda. Korpela _et al._ ([2001](#kor01)) present a model with four levels of analysis - individual, group/activity, organizational and societal - with two viewpoints - intra and inter - on each level (Figure 5). This integrative framework illustrates the idea of applying Activity Analysis and Development on different levels with different aspects, using different theories and methods.

<figure>

![Figure 5](../p311fig5.jpg)

<figcaption>

**Figure 5: The 2x4 integrative levels of analysis ([Korpela _et al._ 2001](#kor01))**</figcaption>

</figure>

The lowest level of an individual naturally represents an actor working on her task in an organization. In the Activity Driven ISD Model we do not consider the psychological aspect, except when it comes to cognitive psychology in human computer interaction. Thus at the individual level we consider information tools, actions and operations, as well as human-information tool interaction. At this level the focus on information tools goes into the details, for example, data items. At this level we can utilize modelling tools such as Use Cases, User Interface layouts, work flow diagrams and so forth.

The group/activity level represents work activities as discussed above. At this level we are interested in work processes and information flows, coordination and communication, activities and network of activities, usually inside an organization. We consider work processes from the actors' perspective and data are usually examined in entities. At this level we can utilize the elements of an activity as described in the ActAD framework.

At the organizational level, an organization is described as a group of activities that are controlled and coordinated by a shared management activity. Here we consider the network of activities and organizational boundaries and networking between activities or organizations. The viewpoint can be described as a client process perspective where the different organizations provide services to clients and share and deliver information.

The societal level considers the landscape of an activity or an organization within one society (nation) or even globally. Sometimes it is useful to consider for example national strategies, policies and legislation, or international strategies, from a global viewpoint.

Thus the focus of interest is slightly different at each level, but to understand the real functionality of a certain level, we usually need to examine the other levels as well. All the different levels interact with each other in organizations and have an influence on each other. For example, at the activity level there can be restrictions and rules that have an effect on the work, but are invisible and can be understood only by examining the organizational level. The work flow diagrams do not show the reasons for different ways of functionality. Also, there can be some contradictions at the activity level in work processes, for example in information accessibility, which are invisible to the managerial level, but can be seen in poor effectiveness, for example. It maybe that these problems can only be solved at the organizational level, for example by improving information delivery between two organizations. The basic idea is that every solution made should be traceable across the organization at different levels and we can zoom in or out in our analysis.

## Information systems development as a temporary activity

Since information systems should be developed in their organizational context, it is important to specify information systems development as an activity itself. Firstly, we define information system as 'the use of information technology (manual or computer-based) in a collective work activity, either as means of work or as means of co-ordination and communication' and accordingly information systems development as 'the process by which some collective work activity is facilitated by new information-technological means through analysis, design, implementation, introduction and sustained support' ([Korpela _et al._ 2000: 137](#kor00)).

We see information systems development as a temporary activity at the border of two organizations or units; there are software producing and technical solutions providing participants and participants who are going to use the information system being created (Figure 6). The information systems development process is essential to both of them, so it combines the two aspects of the participants. The participants from the technical organization are experts in providing technical solutions and means and the participants from the user organization are experts in their work activities and purposes. The technical participants should understand the work done in the user organization in order to be able to evaluate the appropriateness of different technical solutions to a given situation and the user organization should be able to evaluate the impact of new technology on its work activities. In the information systems development process it is important to understand both the technical and work aspects and this is possible only in a participative way. The Activity Analysis and Development framework can provide a tool for modelling the situation in the work place so that all participants in the process understand the whole picture. Usually the best starting point is to model the current situation.

<figure>

![Figure 6](../p311fig6.jpg)

<figcaption>

**Figure 6: The composition of IS development as an activity ([Korpela _et al._ 2002](#kor02))**</figcaption>

</figure>

The object of the information systems development activity is a problem in the users' work processes - a need for better facilities. The clients' activity determines what kind of facilities the user will need. The information systems development activity gets its actors, rules, means etc. from both organizations and activities - the users' and the IS developers' ordinary activities.

However, if we want to assess the appropriateness of information and communication technology for an information system of a work activity in a user organization, we have to consider different aspects of information system in use (Figure 7). First, we have to identify the central activity, the client's activity and related activities. With the help of the ActAD framework and the integrated levels of analysis, we can analyse the different elements of the central activity and the activity network and evaluate the appropriateness (affordability, cost-effectiveness, suitability) and usability of information and communication technology as means within a use activity (Figure 7: A).

Secondly, related to appropriateness, it is crucial to analyse the real demand or need for information and communication technology and identify the problem in the users' work process and the possible impacts of information technology on work activity (Figure 7: B). How much will the service provided by this activity be improved by information and communication technology use? Where does the real demand for modern technology come from and what is the value derived from the technology?

Thirdly, it is important to analyse the capacity to sustain the information system in use, to identify the supporting activities (Figure 7: C). How ready is the organization for new technology in terms of technical, managerial, intellectual and cultural support, or training? Also, we have to analyse the impact of new technology on the activities that are related to our central activity.

Once we have all these aspects included in the information systems development process, the sustainability of information and communication technology in organizational information system will be better (Figure 7: D). There might be a need for changes in different aspects to provide better services. However, the traditional scope of information systems development is more limited; there is a need to include the essential elements of work development and human computer interaction as part of systems development, depending on the case.

<figure>

![Figure 7](../p311fig7.jpg)

<figcaption>

**Figure 7: Sustainability factors of IS in use (modified from [Mursu _et al._ 2004](#mur04))**</figcaption>

</figure>

The appropriate introduction of information and communication technology means information systems development that empowers the user community and the preliminary aim of the Activity Driven ISD Model is the participation of all the stakeholders (cf. [Bødker _et al._ 2004](#bod04)), as shown in Figure 7, whenever it is reasonable. The ActAD framework and sustainability factors provide a modelling tool for participants to achieve a shared understanding of the activity and its goals. All in all, the theoretical models we have presented in this paper describe the needs of information systems development within an activity by considering the context (network of activities and community), the moment (ICT as means at a given phase of the use activity) and the process (information systems development).

## Experiences of applying the ActAD framework in IS research

The ActAD framework has been applied in several research projects to test its suitability for information systems research. For example, the framework was used as an analytical tool in descriptive research on information systems development practices and problems in Nigeria ([Mursu _et al._ 2002](#mur02)) and on analysis of a hospital software development project in an academic environment in Nigeria ([Soriyan 2004](#sor04)) in a joint Finnish-Nigerian project. Within the project, the framework was tested and further elaborated.

ActAD has been used in several small cases as a lens for rapid analysis of work, services or the activity chain, for example in Nigeria by nurses and a general practitioner in a local health centre ([Korpela _et al._ 2000](#kor00)) and in some information systems courses in Finland and South Africa. The ActAD framework has also been applied to explorative analysis by two teams in a large software integration research and development project in Finland. First, the framework was used for a rapid participatory assessment for integration needs ([Häkkinen 2003](#hak03)) and, secondly, the framework was used as an analytical tool for gathering, structuring and describing information needs in previously unexplored inter- and extra-organizational activities ([Toivanen _et al._ 2004](#toi04)).

The experiences were promising. The use of the framework gave a lot of information needed for analysing and developing work practices and technological solutions in them. The experiences encouraged us to continue the work, since there was still a need for a practical model for work development and information systems development, a model that can be applied by both systems developers and practitioners in user organization.

## Activity-Driven Model

We have developed the Activity Driven Information Systems Development (ISD) Model in the ZipIT research project. The main goal of the ZipIT project is to narrow the gap between work development and information systems development and we focus on the health care sector in this project. The objective is to develop a model that can be used when a health care organization needs to improve its information systems. The model is based on Activity Theory and the ActAD framework and participatory design. It is basically a collection of different methods and tools, directed at different levels of activity in different development phases. It draws from the best practices of requirements engineering, system design, user interface design and work process development. Based on the theoretical considerations presented in this paper, the Activity-Driven Model has been developed and tested in several cases in the ZipIT project.

The theoretical basis of the Activity-Driven Model lies on the ActAD framework (Figure 4), including elements of work activity, mode of operation, network of activities; the integrative levels of activity (Figure 5); participatory methods because of the nature of information systems development as a temporary activity at the border of two organizations (Figure 6); and the sustainability framework (Figure 7).

The Activity-Driven Model rests on the Activity Analysis and Development (ActAD) framework from top to bottom. The framework has been applied as a tool for descriptive and explorative analysis in information systems research. The framework considers three levels of activity: the individual level, group and activity level and organizational level (cf. Figure 4). The framework is useful for analysing different elements of an activity; it serves as a checklist and it provides a notation for modelling the work and information systems. The ActAD framework works best when analysing the group and activity level of work activities. It directs users to analyse actors, means of works, means of coordination and communication, work processes and information flows. In addition, we have applied modelling methods based, for example, on user-centred design studies (e.g., scenarios).

As indicated in Figure 5, Activity Theory does not help in analysing at individual or organizational levels, nor the societal level. The ActAD framework helps in identifying the elements on the individual and organizational levels, but we have to apply other theories or approaches to conduct a complete analysis. For example, we have applied Human Computer Interaction and usability studies and software engineering methods to analyse an individual level (e.g., work flow and use case diagrams). For organizational level analysis we have applied theories such as Business Process Reengineering and business modelling methods from software engineering. Societal level analysis requires some social- or environment-oriented studies to cover properly that aspect of information systems development. This kind of social study is going on in one other research project ([Tiihonen _et al._ 2006](#tii06)) and the results will be considered in this information systems development model. However, the ActAD framework helps in creating country-level landscape models of activities in, for example, the health care sector.

The emphasis in the Activity-Driven Model is on participatory methods, as shown in Figures 6 and 7\. The development of information systems changes work processes, so these two aspects (information systems and work system) must be considered in participation with developers and user organization. Developing work processes and developing information systems is a two-way process and they cannot be separated from each other. Although work today is very information intensive, the development of work and of information system is done separately, which cannot create usable solutions. The unsuccessful implementations in the health care sector, for example, are the result mainly of inapplicable technical solutions when we consider work processes, not of the technology itself ([Whittaker _et al._ 1999](#whi99)). The sustainability framework in Figure 7 completes the ActAD framework by emphasizing the different aspects of work activity; appropriate technology for user activity, demand for technology in light of provided services and capacity building for sustained use of technology. According to these aspects, participation means not only the final users of the information system (cf. participatory method), but also the management level of the user organization, the decision makers, people who provide supporting services in the user organization and the final beneficiaries of the new information system, the clients. In that way the transparency of development activities and decisions goes through the organization and the motivation of each participant to commit to the change process is easier to obtain.

<figure>

![Figure 8](../p311fig8.jpg)

<figcaption>

**Figure 8: Dimensions, levels and phases of the Activity-Driven Model**</figcaption>

</figure>

Figure 8 presents the two-dimensional model of the Activity-Driven Model, including the dimensions of time (phases) and description (levels). The levels suggest how detailed the analysis and descriptions of the activity are and the phases suggest the milestones of the information systems development model. The purpose of the model is to show that we have to focus on one level at a time and also have to travel between the levels in each phase, depending on the situation. Travelling between phases is also probably necessary. Each intersection of a level and a phase should have an objective, tools and methods to achieve that objective and an outcome that is connected to the other parts of the picture. All the descriptions and decisions should be based on shared understanding among different stakeholders and professional levels.

In the Activity-Driven Model, as emphasized in the ActAD framework and the integrative levels of activity (Figure 5), we have to analyse activity at different _levels_ and integrate the different levels with each other: the individual level, the group and activity level, the organizational level, the societal level and the global level. At the individual level (Figure 8, Level 3) we study individual actions and the information tools that are used by an individual: that is, human-information tool interaction (cf. Human Computer Interaction). At the group or activity level (Figure 8 Level 2) we study work activities and processes as well as information systems and flows: we take the collective actor's perspective in work processes: that is, group-information system interaction (cf. Computer-Supported Co-operative Work). At the organizational level (Figure 8, Level 1) we study networks of activities, organizational boundaries and information landscape and we take the perspective of the client process; for example, the patient treatment chain. In the Activity-Driven Model we consider the relevant societal and global level issues at the first level. At the societal level we must depict the landscape of the service providers in question; we have to consider legislation, national strategies and so forth. Then there is the global level, which consists of international strategies and policies.

On the other hand, we have different _phases_ or goals in work development and information systems development. In the first phase (Figure 8, Phase 1) the present state of an activity must be analysed and the development points or problems in the activity discovered. Since participation, or co-designing, is a basic principle of our model, useful methods for eliciting data include interviews, observation and guided tours of the work place and for validating data workshops and focus group sessions. When we understand the present state of an activity, we can go to the second phase (Figure 8, Phase 2) where the goal state of an activity is described and stated, based on the objectives and restrictions. Good methods for sketching the goal state are brainstorming, workshops and user interface modelling. The ActAD framework provides the notation for collaboration.

The Activity-Driven Model gives most support for phases 1 and 2, defining the present state and the goal state of an activity. After these phases there must be a place for validation, verification and decision making on different options (Figure 8) to continue to the next phase of planning change. What decisions are made, how the development activities are prioritized and the nature of the forthcoming changes are organization-specific. In phase 3 (Figure 8) we have two descriptions of the activity. i.e., of the present situation now and of the near-future situation. Based on these descriptions and decisions it is possible to make a plan for the transformation from the present state to the goal state. Communication and interaction between designers and practitioners is the key for a successful outcome of this phase. The model helps in making concrete plans, because we have already determined, for example, our target, the restrictions and the people involved. Taking into account the zone of proximal development, the model guides us in planning realistic milestones and development activities.

Information systems development processes are related to high-level organizational visions and strategies. The overall objective of a development process is defined in the starting point. With more defined objectives of the development process it is possible to decide what levels of the Activity-Driven Model are needed and through what phases it is necessary to go.

This model is still introductory and, in the pilot cases, in the ZipIT project the model has been applied and has evolved in different ways and with different emphases. In this project, the model has been applied in eight pilot cases in the health care sector. In most cases (six), the starting point was a situation in which a health care organization had made a decision to introduce a software application. The purpose was to determine the impact of the software on the work, to see how the current activity would change and how to do it. In one case the selection of a software application was an objective. One pilot case was purely a feasibility study, where possible development points were defined. A software company was involved in four pilot cases. We found in practical cases that there is no one correct way to carry out the process: it depends very much on the objectives, resources and the current situation in the organization in the light of information system acquisition process. In six pilot cases the main focus was on the activity level, which usually is the logical starting point for the process of improving information system and work activity.

## Discussion: relating the Activity-Driven Model to theoretical underpinnings

The purpose of the ZipIT project is to “zip up” the distance between information systems development and work development. We strive to capture and describe an activity driven model the makes it possible to consider both work improvement and information systems development at the same time and to use the same holistic descriptions of work activity in both development tasks.

Work activity can be seen as a sum of its elements. In the Activity Driven Model we utilize the elements of activity described in the ActAD framework and the integrative levels of analysis as checklists and guide to understand work activity. The focus is on the work activity as a whole, not just on an information system or a piece of software. Information systems are studied in the context of the organization and its work flows.

The different levels are used to capture different views of the work activity; individual, group, organizational or societal. The highest level serves as a map on which lower level descriptions are traced and reflected. Zooming in and out between the levels is useful and necessary; for example, to find out how changes in individual work process affect the organizational level.

The mode of activity indicates the relation of the elements of the activity at the moment. In other words, a work activity can be seen as a result of its development over time. We utilize the idea of the zone of proximal development: it is possible to plan future activity only through understanding the present activity. Our model includes three phases or descriptions of work activity: the present state, the goal state and the plan for changes. The the starting point is to describe the work activity as it is today and to identify possible developmental points. The second step is to set the goals for the future activity and to describe restrictions. The third step is to set realistic goals and sub-goals, taking into account restrictions and sustainability factors and planning the concrete design solutions for new information and communication technologies (if such is needed) and the actions needed to change the work activity from the present state to the goal state.

We wish to emphasize the importance of choosing proper design tools for modelling work activity by practitioners or designers or by both in participation. It is important to create a rich and holistic picture of the work activity and describe the reasons for and restrictions on real work practices. Quite often these issues are discussed to only limited extent and with restricted viewpoints, among the workers and other people on different organizational levels, but if these issues are not described in any shared document that is communally accepted and understood, the requirements are easily biased and unpractical.

Since information systems development is a temporary activity between system designers and users, we emphasize participatory design methods and interactivity. We utilize observation and workshops, for example. and semi-structured, thematic and focus group interviews. Whenever possible, the information gathering should take place in the actual work context, not necessarily during the work flows, but certainly in the work place. Thus, we follow the general principles of Participatory Design (e.g., [Bødker _et al._ 2004](#bod04)) by applying qualitative methods and involving users and other stakeholders. Besides management and the final users of the information system, we also want to involve support-providing partners and people from other activities who are related to the information system in use. In addition, sometimes it is also useful to involve the clients of the activity, people or organizations who are using the services

It is important to involve different professionals from the work activity under study. Every professional describes and operates work from the standpoint of his or her own work flows and we can obtain a holistic picture of the work activity by combining the different views. It is useful to arrange multi-professional workshops and focus group discussions. Discussion between the participants brings out knowledge of the work practices, which may not come out otherwise. We attempt to facilitate communication among different professionals, for example by using rich pictures applied from Soft Systems Methodology ([Checkland & Scholes 2005](#che05)) and scenarios ([Alexander & Maiden 2004](#ale04)). The Activity-Driven Model provides methods and an ideology to create those rich pictures so that the multi-professional group will achieve a shared understanding.

We define information systems development as a process of analysis, design, implementation, introduction and sustained support phases. We can emphasize the Activity-Driven Model to be applicable in the analysis phase. However, the descriptions of the present state and the goal state, the outcomes of the first two phases of the model and the plans for change are all very useful in the design, implementation, introduction and sustaining activities. The different analytical levels provide extensive viewing of activity from different perspectives; for example, for designing a proper information technology solution in relation to the work activity and the nearby systems, for designing and implementing the work development at the same time, for identifying proper participants in the different phases of development, for designing and indicating people for training and for revealing the needs for supporting activities.

Overall, in this paper, we seek to provide answers to the requirements addressed in the introduction. The ActAD framework offers solutions to the first three requirements; it gives a method to describe a work activity as a systemic entity, ICT is seen as a tool to facilitate work, embedded in the work system and both collective and individual aspects of work are taken into account. In the Activity Driven ISD Model information system and work system are studied in their organizational context, thus meeting the fourth requirement. The fifth requirement emphasising that the analytical framework must be based on a sound theoretical basis should be satisfied with this paper. Our purpose is to create a foundation for the development method based on Activity Theory, ActAD framework, integrative levels of analysis, empowering information systems development and user participation. The application of the ActAD framework and lately also the Activity Driven ISD Model in several cases has proved the sixth requirement of its applicability to both descriptive studies and practical development. The seventh requirement, i.e. the applicability of the model both to technological development by software and information system professionals and to work practice development by the workers, will be realized after the finalization of the model. We have had encouraging experiences in our pilot cases of its applicability by practioners. But the final applicability can be tested after the model is completed and put into practical use.

## Conclusion

This paper collects some of the theoretical work that has been done over the years in studying the appropriateness of Activity Theory to information systems research and practice. The emphasis of this paper is on the work that is relevant to a work-oriented development model that aims at combining the two worlds of work development and information systems development. The development model, the Activity-Driven Model, has its theoretical basis in Activity Theory, the Activity Analysis and Development framework, information systems development and user participation. The model is a construction of different levels of analysis in different development phases and a collection of different methods and tools. It utilizes the best practices of requirements engineering, system design, user interface design and work process development.

The theoretical discussion and the experiences in different cases in the research process have persuaded us to create a holistic model that covers the different elements and levels of work activity and work processes. Our objective is to develop a model that is practicable and operational to both work developers and information systems developers and this paper provides the theoretical ground for such a model.

## Acknowledgements

This paper is based on research funded by the Finnish Agency for Technology and Innovation Tekes through the ZipIT project (grants no. 40426/04, 40354/05, 40252/06 and 790/04, 644/05, 644/06 www.centek.fi/zipit), the Finnish Work Environment Fund through the ActAD-HIS project (grant no. 104151) and the Academy of Finland through the INDEHELA-Methods and INDEHELA projects (grants no. 39187 201397 and 104776, www.uku.fi/indehela).

## References

*   <a id="ale04"></a>Alexander I.F. and Maiden N, (2004), _Scenarios, stories, usecases through the systems development life-cycle_, England: John Wiley & Sons, Ltd.
*   <a id="bar00"></a>Bardram, J. (2000), Temporal coordination: of time and collaborative activities at a surgical department, _Computer Supported Cooperative Work_, **9**(2) 157-187.
*   <a id="ber00"></a>Bertelsen, O.W. and Bødker, S. (2000), Introduction: information technology in human activity, _Scandinavian Journal of Information Systems_, **12**(1), 3-14.
*   <a id="bey98"></a>Beyer H. and Holtzblatt K. (1998), _Contextual design: defining customer-centered systems_, Morgan Kaufmann Publishers 
*   <a id="bod91"></a>Bødker, S. (1991). Activity theory as a challenge to systems design, in H-E. Nissen, H. K. Klein and R. Hirscheim (eds.), _Information systems research: contemporary approaches and emergent traditions_, (pp. 551-564), Amsterdam: Elsevier.
*   <a id="bod97"></a>Bødker, S. (1997), Computers in mediated human activity, _Mind, Culture and Activity_, **4**(3) 149-158.
*   <a id="bod04"></a>Bødker K., Kensing F. and Simonsen J. (2004), _Participatory IT design: designing for business and workplace realities_, Massachusetts: The MIT Press 
*   <a id="che05"></a>Checkland, P. and Scholes J. (2005), _Soft Systems Methodology in action_, (pp. A16-A19), England: John Wiley & Sons, Ltd.
*   <a id="eng87"></a>Engeström, Y. (1987). _Learning by expanding_. Helsinki: Orienta-konsultit.
*   <a id="eng99"></a>Engeström, Y. (1999). Activity theory and individual and social transformation, in Y. Engeström, R. Miettinen and R. Punamäki (eds.), _Perspectives on Activity Theory_, (pp. 19-38). Cambridge, UK: Cambridge University Press.
*   <a id="eng86"></a>Engeström Y. and Engeström R. (1986), Developmental Work Research: the approach and an application in cleaning Work, _Nordisk Pedagogik_ 1 2-15.
*   <a id="eng90"></a>Engeström Y., Brown K., Engeström R. and Koistinen K. (1990), Organizational forgetting: an activity-theoretical perspective. In David Middleton and Derek Edwards, (Eds.). _Collective Remembering_, (pp. 139-168). London: Sage Publications.
*   <a id="hel00"></a>Helle M. (2000), Disturbances and contradictions as tools for understanding work in the newsroom, _Scandinavian Journal of Information Systems,_ 12, 81-114. 
*   <a id="hak03"></a>Häkkinen, H. (2003), Rapid method for integration requirements assessment – case maternity care, in S. Laukkanen and S. Sarpola (eds.), _Electronic proceedings of the 26th information systems research seminar in Scandinavia_, Helsinki: Helsinki School of Economics.
*   <a id="hyy01"></a>Hyysalo S. and Lehenkari J. (2001), An activity-theoretical method for studying dynamics of user-participation in IS design, In Anders, Mørch and Opdahl (eds.), _Electronic proceedings of 24th information systems research seminar in Scandinavia_, Norway 
*   <a id="kor00"></a>Korpela, M., Soriyan, H.A. and Olufokunbi, K.C. (2000), Activity analysis as a method for information systems development: General introduction and experiments from Nigeria and Finland, _Scandinavian Journal of Information Systems_, **12**(1) 191-210.
*   <a id="kor01"></a>Korpela, M., Mursu, A. and Soriyan, H.A. (2001), Two times four integrative levels of analysis: a framework, in N.L. Russo, B. Fitzgerald and J.I. DeGross (eds.), _Realigning research and practice in information systems development: the social and organizational perspective_, (pp. 367-377), Boston, MA: Kluwer Academic.
*   <a id="kor02"></a>Korpela, M., Mursu, A., Soriyan, H.A. and Olufokunbi, K.C. (2002), Information systems development as an activity, _Computer Supported Cooperative Work_, **11**(1-2) 111-128.
*   <a id="kor04"></a>Korpela M, Mursu A, Soriyan A, Eerola A, Häkkinen H & Toivanen M (2004), I.S. research and development by activity analysis and development - dead horse or the next wave? In Kaplan B., Truex III D., Wastell D., Wood-Harper A.T. and DeGross J.I. (eds.), _Information systems research – relevant theory and informed practice_, (pp. 453-471), Boston: Kluwer Academic Publishers.
*   <a id="kuu91"></a>Kuutti, K. (1991). Activity theory and its applications to information systems research and development, in H-E. Nissen, H. K. Klein and R. Hirscheim (eds.), _Information systems research: contemporary approaches and emergent traditions_, (pp. 529-549). Amsterdam: Elsevier.
*   <a id="kuu94"></a>Kuutti K. (1994), _Information systems, cooperative work and active subjects: the activity-theoretical perspective_. Research papers. Series A 23\. Oulu University Printing Centre 
*   <a id="kuu96"></a>Kuutti K. (1996). Activity theory as a potential framework for human-computer interaction research, in Nardi (ed.), _Context and consciousness: activity theory and human-Computer interaction_, (pp. 17-44), Cambridge: MIT Press.
*   Leont'ev A.N. (1978), _Activity, Consciousness and Personality_, Engelwood Cliffs, NJ: Prentice Hall.
*   <a id="leo78"></a>Leont'ev, A.N. (1978). _Activity, consciousness and personality._ Englewood Cliffs, NJ: Prentice Hall.
*   <a id="mur02"></a>Mursu Á. (2002). _Information systems development in developing countries - risk management and sustainability analysis in Nigerian software companies._ Jyväskylä, Finland: University of Jyväskyl&auml. (Doctoral dissertation, Jyv?skyl? studies in Computing 21).
*   <a id="mur04"></a>Mursu Á., Korpela M., Soriyan A. (2004), Generic framework for sustainability analysis of Information Systems, In Romano N.C. Jr (ed.), _Proceedings of tenth americas conference on information systems_, New York, August 6-8, Conference website, http://aisel.isworld.org/proceedings/amcis/2004/home.asp.
*   <a id="mur03"></a>Mursu, Á., Soriyan, A. and Korpela, M. (2003), ICT for development: sustainable systems for local needs, in M. Korpela, R. Montealegre and A. Poulymenakou (eds.), _Organizational information systems in the context of globalization. IFIP TC8 & TC9 / WG8.2 & WG9.4 working conference on information systems perspectives and challenges in the context of globalization. In progress research papers [CD-ROM]_, Athens: Athens University of Economics and Business 199-210.
*   <a id="mwa01"></a>Mwanza, D. (2001), Where theory meets practice: a case for an activity theory based methodology to guide computer system design, in M. Hirose (ed.), _Proceedings of INTERACT’2001_, Oxford, UK: IOS Press.
*   <a id="mwa02"></a>Mwanza D. (2002), _Towards an activity-oriented design method for HCI research and practice_, PhD Thesis, United Kingdom: Open Unviersity.  
*   <a id="orl91"></a>Orlikowski W.J., Baroudi J. (1991), Studying infromation technology in organizations: research approaches and assumptions, _Information Systems Research_, **2**(1) 1-28. 
*   <a id="que04"></a>Quek A. and Shah H. (2004), A comparative study of activity-based methods for information Systems development, Proceedings of the 6th international conference on enterprise information systems, ICEIS, Portugal, 221-232.
*   <a id="red02"></a>Redmiles, D. (2002), Introduction, _Computer Supported Cooperative Work_ 11(Special issue on Activity Theory).
*   <a id="sor04"></a>Soriyan, H.A. (2004), _A conceptual framework for information systems development methodology for educational and industrial sectors in Nigeria_, PhD thesis, Obafemi Awolowo University, Ile-Ife, Nigeria.
*   <a id="tii06"></a>Tiihonen T., Korpela M., Mursu A. (2006), Creating a framework to recognize context-originated factors in IS in organizations, In Berleur J., Nurminen M., Impagliazzo J. (eds.), _Social informatics: an information society for all?,_ proceedings of the seventh conference on human choice and computers, HCC7, (367-379). Boston: Springer. 
*   <a id="toi04"></a>Toivanen M, Häkkinen H, Eerola A, Korpela M and Mursu A. (2004) Gathering, structuring and describing information needs in home care: a method for requirements exploration in a ”gray area”, In: Fieschi M, Coiera E, Li Y-C, eds. _MEDINFO 2004: building high performance health care organizations_. Amsterdam: IOS Press 2004\. pp. 1398-1402.
*   <a id="vra01"></a>Vrazalic, L. and Gould, E. (2001), Towards an activity-based usability evaluation methodology, in Anders, Mørch and Opdahl (eds.), _Electronic proceedings of 24th information systems research seminar in Scandinavia (IRIS24)_.
*   <a id="vyg78"></a>Vygotsky L.S. (1978), _Mind in society: the development of higher psychological processes_, Compiled from several sources and edited by Cole M., John-Steiner V. and Scribner S., Harvard MA: Harvard University Press.
*   <a id="wal00"></a>Walsham G. (2000), Globalization and IT: agenda for research, in Baskerville, Stage and DeGross (eds.), _Organizational and social perspectives on information technology_, (pp. 195-210), Boston: Kluwer Academic Publishers.
*   <a id="wil06"></a>Wilson, T.D. (2006). A re-examination of information seeking behaviour in the context of activity theory. _Information Research_, **11**(4), paper 260 [Available at http://InformationR.net/ir/11-4/paper260.html]
*   <a id="whi99"></a>Whittaker B. (1999), What went wrong? Unsuccessful information technology projects. _Information Management & Computer Security_ **7**(1) 23-29.