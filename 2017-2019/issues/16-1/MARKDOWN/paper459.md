#### vol. 16 no. 1, March 2011

# Comportamiento humano de la informacion comercial: teleformacion en Espana

#### [M. Rosario Fernandez Falero](#authors) y [Diego F. Peral Pacheco](#authors)

Universidad de Extremadura  
Facultad de Biblioteconomia y Documentacion  
Plazuela de Ibn Marwan s/n  
06071 Badajoz  
Spain

#### Resumen

> **Introduccion.** El comercio electronico es uno de los casos mas claros de diseno de sistema de informacion centrado en las necesidades de los usuarios, teniendo siempre en cuenta a que sector de la poblacion se dirige el producto en venta y las necesidades del mismo.  
> **Material y metodo.** Se buscan las Web espanolas a estudiar, partiendo de sitios conocidos, realizando busquedas en Internet y en directorios especializados y se evaluan mediante un cuestionario elaborado por el grupo de investigacion, que realiza el analisis siguiendo el ciclo de compra de Rian Van der Merwe y James Bekker, que divide el proceso de compra en cuatro fases: Reconocimiento de necesidades, (information behaviour), reunir informacion sobre el producto a comprar (information seeking behaviour), evaluar esta informacion (information searching behaviour) y realizar la compra (information use behaviour).  
> **Resultados y Analisis.** Se indican los aspectos mejorables y destacables de las empresas de venta de teleformacion en Espana, tales como que la informacion esto muy estructurada mediante menos para acceder facilmente o que apenas un 14% presentan informacion de otros alumnos, que han cursado en esa Web. En definitiva los resultados muestran las respuestas de los sitios Web de venta de teleformacion a las necesidades de informacion de los alumnos.  
> **Conclusiones.** Una vez analizados los resultados se puede concluir, que las empresas de teleformacion si disean sus Web sites, valorando en mayor o menor medida, las necesidades de sus alumnos.

## Introduccion

Este trabajo consiste en el analisis de sitios Web de venta de cursos en linea. Para ello se parte de que en el comercio electronico se diseran Web sites ([Wilson](#autor11) 2000) para dar al usuario la informacion que necesita (human information behaviour), ya que la informacion contenida en los sitios de teleformacion intenta captar la atencion (publicidad) del usuario de la Red para que vaya al sitio y localice el curso deseado, - information seeking, subgrupo de information behaviour, que indica el proposito de buscar informacion con una finalidad concreta ([Spink y Cole 2001](#autor6)) - y sobre todo lo compre. Information use behaviour, cuando se incorpora la informacion al conocimiento de un individuo.

En el analisis de la informacion ([Wilson 1981](#autor12)) hay que tener en cuenta las necesidades del comprador, ya que las necesidades personales son la raiz de la motivacion hacia la localizacion de informacion, information seeking behaviour; por ello, un metodo de analisis debe en primer lugar establecer las necesidades y caracteristicas del grupo de personas al que va dirigido. En este trabajo se aplica el metodo elaborado por el grupo de investigacion ([Fernandez _et al._ 2006](#autor1)), resultado del estudio bibliografico de trabajos sobre analisis de sites comerciales centrados en el comprador (B2C).

El comercio electronico surge gracias a las posibilidades que brindan las nuevas tecnologias informaticas que permiten el acceso a la informacion en cualquier momento ([Sheng 2006](#autor5)) y desde cualquier sitio (ubiquitious-computing); han permitido que surjan distintos tipos de comercio, entre ellos el comercio en linea (e-commerce), un tipo de comercio ubicuo o u-commerce ([Watson _et al._ 2002)](#autor10): uso de redes ubicuas de comunicacion para realizar transacciones entre una firma y sus clientes para proporcionar un nivel de valor, mas alle del comercio tradicionale, y que se caracteriza porque el canal utilizado para realizar la transaccion comercial es la red de comunicacion Internet.

Los objetivos de este trabajo son:  
Analizar el comportamiento de la informacion en los sitios de venta de teleformacion.  
Determinar que tipo de comportamiento de la informacion participa en el comercio electronico de cursos.  
Aspectos destacables y mejorables del mercado en linea de teleformacion.

## Material y metodo

En primer lugar se recuperan las Web comerciales: partiendo de direcciones conocidas en otros trabajos ([Fernandez _et al._ 2009](#autor2)) realizando una busqueda de sitios dedicados a la teleformacion que ofrecen sus servicios por Internet, y a traves de portales especializados:

*   AEFOL [http://www.aefol.com/](http://www.aefol.com/) (se recuperan 79 una vez eliminados los repetidos)
*   ANCED, Asociacion Nacional de Centros de e-Learning y Distancia [http://www.anced.es/](http://www.anced.es/)  (se recuperan 16 sin repetir)

Los criterios de seleccion de los sitios a estudiar son: que sean empresas espanolas, que sean de formacion de pago, o bien centros privados subvencionadas por el Estado y que las Web s estan actualizadas y en funcionamiento.  
Se decide que quede fuera de este estudio, por ser un campo muy amplio, la formacion on line ofrecida por el Ministerio de Educacion en su pagina Web [(Ministerio 2011)](#autor4) , asi como las Universidades (campus virtuales), pero se analizan plataformas e-learning con programas acreditados por universidades y que aportan su titulacion, por ej la Escuela de Negocios Telefonica Learning Services.

La metodologia empleada para el analisis se establecia por el grupo de investigacion mediante una revision bibliografica de trabajos de evaluacion de Web sites basados en las necesidades de los usuarios, mediante la realizacion de un formulario/cuestionario con las variables de estudio, aplicandose a las Web estudiadas y tabulando los resultados mediante una hoja de calculo (Excel).

El perfil de los posibles clientes segun un informe elaborado por el equipo de Red.es ([Urea _et al._ 2010](#autor9)) sobre las caracteristicas sociodemograficas de los compradores espanoles, indica que respecto al sexo practicamente es igual la proporcion, apenas un 1,1% mas de hombres que mujeres y la edad se encuentra mayoritariamente entre los 25 y los 49 anos, especialmente en la franja de 35 a 49 anos. En relacion con la situacion laboral destaca, en primer lugar, el sector de la poblacion activa y con trabajo, seguida de los estudiantes, personas dedicadas a tareas domesticas y de los parados pero con edad laboral, jubilados etc. En relacion al nivel cultural de la poblacion, son mayoria los que tienen estudios secundarios, seguidos de superiores y finalmente primarios. La relacion entre el tamao del municipio y el acceso a Internet es directamente proporcional: a mayor numero de habitantes, mayor numero de usuarios de la Red, salvo en las poblaciones entre 50 a 100 mil habitantes que presentan los datos m?s bajos.

Se analizan 100 sitios de teleformacion escogidos al azar de los 183 recuperados que se adjuntan en el directorio. Si bien todos ellos incluyen formacion en linea, no todas las empresas se dedican exclusivamente a la formacion; en esos casos silo se han analizado las paginas de formacion online.

El formulario se realiza siguiendo el ciclo de compra de Van der Merwe ([Van der Merwe y Bekker 2003](#autor3)), que divide el proceso de compra en cuatro fases que se corresponden con las definiciones sobre el comportamiento humano de la informacion (HIB) que aporta Wilson en su trabajo titulado Comportamiento Humano de la Informacion ([Wilson 2000](#autor11)):

*   Reconocimiento de necesidades ? Visita a los sitios ? Comportamiento de la informacion (Information behaviour)
*   Reunir informacion ? Navegar ? Tratar de encontrar informacion (Information seeking behaviour)
*   Evaluar la informacion ? Contenidos ? Busqueda de informacion (Information searching behaviour)
*   Hacer la compra ? Fiabilidad ? Utilizar la informacion (Information use behaviour).

Primera fase - Reconocimiento de necesidades: pongamos como ejemplo que una persona navegando en Internet recibe publicidad sobre cursos, por ejemplo, de inglis, y reconoce la necesidad de estudiar este idioma,  accediendo por primera vez a una Web de teleformacion como posible sitio donde realizar el curso. Estudiaremos en este caso la informacion que hace que el usuario reconozca su necesidad de realizar un curso en dicha Web . En definitiva se analizan aspectos que hacen atractivo el sitio y que enganchan al usuario y por tanto se considera information behaviour ([Wilson 2000](#autor11)), ya que se trata del comportamiento humano en relacion a fuentes y canales de informacion, incluidas tanto la informacion activa como la pasiva, sin intencion de actuar sobre la informacion dada. Esta fase se inicia antes de entrar en los sitios, pero en este trabajo se va a tratar desde que se entra en el site, cuando el potencial comprador recibe informacion que le hará pensar que esta tienda va a satisfacer sus necesidades. Los parametros a analizar en esta fase son aquellos referidos a la primera impresion del usuario, el aspecto visual, lo atractivo que es el sitio:

*   COLORES. Uso de colores asociados al producto en venta. A veces es dificil determinar el color de la página, en estos casos se selecciona el color del menu, pues es el que se repite en todo el sitio
*   CONTENIDO MULTIMEDIA. Inclusion de fotos, dibujos, animaciones o videos asociados al producto. No hay que olvidar que el sitio debe ser agradable al comprador y que lo asocie con el producto a la primera mirada, dado que con un simple clic del ratón el cliente se va y se pierde la venta.
*   LENGUAJE claro directo y conciso, sin palabras tecnicas, con estructuras sencillas.
*   MUSICA/VOZ EN OFF. La musica establece diferencias con otras paginas y la voz en off ademos de transmitir la informacion que el empresario quiere, permite la accesibilidad para personas con discapacidad visual.

Segunda fase - Reunir informacion sobre el producto a comprar, Information seeking behaviour ([Wilson 2000](#autor11)). Es la busqueda consciente de informacion como consecuencia de una necesidad. En el proceso de busqueda se interacta con sistemas de informacion, en este caso computadoras que acceden a la Red permitiendo navegar por el sitio: en este trabajo se analizar si se permite llegar a la informacion lo mas directa y rapidamente posible (por ej. si hay menus, etc), en definitiva se valoraran aspectos de usabilidad del sitio.

*   TIEMPO DE DESCARGA de la pagina (entre 8-10 segundos, nunca mayor de 10 segundos). Para localizar la informacion con facilidad y lo mas rapidamente posible el sitio debe estar muy bien organizado, por ello se estudian aquellos aspectos relacionados con el resultado del proceso de analisis y clasificacion del material de la pagina Web en estudio en categorias definidas ([Spink y Cole 2001](#autor7)).
*   INFORMACION ESTRUCTURADA. La informacion aparece convenientemente estructurada mediante un indice, contiene algon tipo de mapa-Web .
*   ESTRUCTURA DE NAVEGACIoN. Uso de menus de navegacion, menu vertical, horizontal, etc. y otras estructuras.
*   ACCESO RAPIDO al producto. Se necesitan menos de tres clics para acceder a cualquier informacion, sobre todo sobre el producto a comprar.
*   FUNCIONAMIENTO. No hay enlaces rotos y las aplicaciones funcionan correctamente
*   PERSONALIZACION. Posibilita la personalizacion (cuando la Web permite escoger el tamao de la letra, color, musica, etc).
*   OTROS IDIOMAS. Facilita la consulta en varios idiomas (desde la portada) posibilitando la navegacion en diferentes lenguas; en realidad es otra posibilidad de personalizacion del sitio pero dado que en Internet, donde no hay fronteras ni distancias, la unica barrera es el idioma, si se quieren aprovechar sus posibilidades, el sitio debe ofertar la pagina en varias lenguas y los iconos de cambio de idioma de la Web deben aparecer en la primera pagina, o mejor dicho, en la primera pantalla del site.
*   BUSCADOR. Incluye un motor de busqueda dentro de la propia Web .

Tercera fase - Analizar el contenido del sitio, Information Searching Behaviour ([Wilson 2000](#autor11)). Hay una interaccion con la informacion, con todas las posibilidades del sistema: manualmente navegando por el sitio para encontrar la informacion, y a nivel intelectual buscando informacion específica sobre el producto y la empresa; en esta fase el comprador busca conscientemente informacion sobre el curso que desea recibir y si le convence el producto, buscará informacion sobre la empresa, fundamentalmente para ver si es un centro especializado en formacion, valorando la situacion de la empresa en el sector. Por eso, la informacion que se estudia es aquella que genera confianza en el alumno para que asi escoja este centro para realizar su curso.

Contenidos comercialies. Informacion sobre cursos en venta, para elegir el curso que se adapte a las necesidades.

*   PRESENTACION CURSOS. Contiene datos de presentacion del curso. Tipo de curso, nivel, etc.
*   DESCRIPCION CURSOS. Incluye descripcion del curso: contenido y temario, metodologia, evaluacion, docentes y certificado del curso.
*   CATALOGO DE CURSOS. Existencia de un catalogo de cursos on line.
*   PRECIO DE LOS CURSOS. En este caso de los cursos ofertados.
*   ENLACES EXTERNOS. Proporciona links con sitios que incluyen informacion relacionada.
*   INFORMACION RELACIONADA. Inclusion de otro tipo de informacion que pudiera ser útil para el alumno de formacion on line.

Contenidos corporativos. Informacion sobre la empresa para que el usuario conozca quien le vende y generar seguridad y confianza en el sitio Web.

*   INFORMACION DE LA EMPRESA. Se incluye informacion clara sobre la empresa (historia, mision, objetivos, etc).
*   IMAGEN CORPORATIVA. Inclusion en la plataforma de teleformacion del logotipo de la empresa, marca o identificativo
*   INFORMACION ADICIONAL sobre la empresa o el sector, no sobre el producto.
*   MAIL DE CONTACTO. Direccion de correo electronico. Supone un valor añadido la imagen de la empresa, ya que es la posibilidad que brinda la Red para poder establecer una conversacion entre vendedor y comprador a través de Internet.
*   MAPA DE LOCALIZACION. Morada fisica, pueden incluir mapa de ubicacion.

Cuarta fase - Realizar la compra, Information use behaviour ([Wilson 2000](#autor11)): Consiste en el acto fisico y mental de incorporar la informacion al conocimiento. Mentalmente se compara la nueva informacion con la antigua y se decide que, donde y como comprar. Los parametros estudiados son aquellos que generan fiabilidad y ayudan al futuro alumno a decidirse por compra el curso.

*   CERTIFICACION. Los productos aparecen identificados con la certificacion correspondiente.
*   VENTA DIRECTA. La adquisicion de los productos se hace a través de la venta directa cuando se pueden realizar todas las operaciones en la Red (eleccion, matriculacion, pago, disponibilidad de materiales y realizacion del curso).
*   FORMULARIO DE PETICION de reserva, o matriculacion.
*   FORMULARIO DE COMPRA. Incluye formulario de peticion de compra. Este tipo de Sitios solicitan rellenar formularios de compra, reserva y matriculacion; aparentemente puede suponer rechazo por parte del cliente, pero en realidad es uno de los puntos claves en la venta de estos productos, sobre todo porque al dar informacion personal a un site, hace que el comprador fidelice sus compras, ya que al ceder sus datos ha demostrado su confianza y seguridad en este sitio y en futuras ocasiones vuelve a los Web ya visitados.
*   MENSAJE DE SEGURIDAD sobre datos personales, y/o bancarios. Cuando se hace referencia a la proteccion de datos y a la seguridad del sitio.
*   INFORMACION DE OTROS USUARIOS. Cuando el site brinda la oportunidad de mostrar las opiniones, (valoraciones, recomendaciones) que tienen sus alumnos de sus curso

## Resultados

El censo total de Web recuperadas es de 183, las cuales se obtienen de la siguiente manera:

*   40 de direcciones conocidas de otros trabajos,
*   48 recuperadas en la Red mediante el buscador Google
*   95 de portales especializados: 79 de AEFOL y 16 de ANCED

Los resultados se encuentran en la tabla 1 donde se muestran las relaciones porcentuales entre paginas y parametros analizados.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 1: Relacion entre parametros y porcentajes en que se encuentran.**</caption>

<tbody>

<tr>

<th colspan="3">Primera fase. Reconocimiento de las necesidades. Information behaviour</th>

</tr>

<tr>

<td rowspan="2">COLORES</td>

<td align="center">Azul</td>

<td>Otros colores</td>

</tr>

<tr>

<td align="center">52%</td>

<td align="center">48%</td>

</tr>

<tr>

<td> </td>

<td align="center">SI</td>

<td align="center">NO</td>

</tr>

<tr>

<td>CONTENIDOS MULTIMEDIA</td>

<td align="center">99%</td>

<td align="center">1%</td>

</tr>

<tr>

<td>LENGUAJE</td>

<td align="center">97%</td>

<td align="center">3%</td>

</tr>

<tr>

<td>MUSICA/VOZ EN OFF</td>

<td align="center">21%</td>

<td align="center">79%</td>

</tr>

<tr>

<th colspan="3">Segunda fase. Reunir informacion. Information seeking behaviour</th>

</tr>

<tr>

<td>TIEMPO DE DESCARGA</td>

<td align="center">97%</td>

<td align="center">3%</td>

</tr>

<tr>

<td>INFORMACION ESTRUCTURADA</td>

<td align="center">100%</td>

<td align="center">0%</td>

</tr>

<tr>

<td rowspan="2">ESTRUCTURA DE NAVEGACION.</td>

<td align="center">Solo horizontal o solo vertical</td>

<td>Combinación de menús</td>

</tr>

<tr>

<td align="center">50%</td>

<td align="center">50%</td>

</tr>

<tr>

<td>ACCESSO RAPIDO</td>

<td align="center">80%</td>

<td align="center">20%</td>

</tr>

<tr>

<td>FUNCIONAMIENTO</td>

<td align="center">78%</td>

<td align="center">22%</td>

</tr>

<tr>

<td>PERSONALIZACION</td>

<td align="center">30%</td>

<td>70%</td>

</tr>

<tr>

<td>OTROS IDIOMAS</td>

<td align="center">29%</td>

<td align="center">71%</td>

</tr>

<tr>

<td>BUSCADOR</td>

<td align="center">39%</td>

<td align="center">61%</td>

</tr>

<tr>

<th colspan="3">Tercera fase. Anlisis de contenido. Information searching behaviour: Contenidos comcerciales</th>

</tr>

<tr>

<td>PRESENTACION DE CURSOS</td>

<td align="center">96%</td>

<td align="center">4%</td>

</tr>

<tr>

<td>DESCRIPCION DE LOS CURSOS</td>

<td align="center">85%</td>

<td align="center">15%</td>

</tr>

<tr>

<td>CATALOGO DE CURSOS</td>

<td align="center">92%</td>

<td align="center">8%</td>

</tr>

<tr>

<td>PRECIO DE LOS CURSOS</td>

<td align="center">41%</td>

<td align="center">59%</td>

</tr>

<tr>

<td>ENLACES EXTERNOS</td>

<td align="center">48%</td>

<td align="center">52%</td>

</tr>

<tr>

<td>INFORMACION RELACIONADA</td>

<td align="center">45%</td>

<td align="center">55%</td>

</tr>

<tr>

<th colspan="3">Contenidos corporativos</th>

</tr>

<tr>

<td>INFORMACION DE LA EMPRESA</td>

<td align="center">96%</td>

<td align="center">4%</td>

</tr>

<tr>

<td>IMAGEN CORPORATIVA</td>

<td align="center">97%</td>

<td align="center">3%</td>

</tr>

<tr>

<td>INFORMACION ADICIIONAL</td>

<td align="center">79%</td>

<td align="center">21%</td>

</tr>

<tr>

<td>CORREO ELECTRONICO</td>

<td align="center">81%</td>

<td align="center">19%</td>

</tr>

<tr>

<td>MAPA DE LOCALIZACION</td>

<td align="center">57%</td>

<td align="center">43%</td>

</tr>

<tr>

<th colspan="3">Cuarta fase. Hacer la compra. Information use behaviour</th>

</tr>

<tr>

<td>CERTIFICACION</td>

<td align="center">64%</td>

<td align="center">36%</td>

</tr>

<tr>

<td>VENTA DIRECTA</td>

<td align="center">29%</td>

<td align="center">71%</td>

</tr>

<tr>

<td>FORMULARIO DE PETICION</td>

<td align="center">32%</td>

<td align="center">68%</td>

</tr>

<tr>

<td>FORMULARIO DE COMPRA</td>

<td align="center">34%</td>

<td align="center">66%</td>

</tr>

<tr>

<td>MENSAJE DE SEGURIDAD</td>

<td align="center">19%</td>

<td align="center">81%</td>

</tr>

<tr>

<td>INFORMACION DE OTROS USUARIOS</td>

<td align="center">14%</td>

<td align="center">86%</td>

</tr>

</tbody>

</table>

Al estudiar los resultados se ve el Comportamiento de la informacion ([Spink y Currier 2006](#autor8)), entendiendo esta como el comportamiento humano en relacion a las fuentes y canales de informacion. Internet es una fuente de informacion tanto activa, con la que se puede interactuar (comprar), como pasiva, mucha informacion que no llega a utilizarse (publicidad). La metodologia empleada muestra los resultados obtenidos en lo sitios de venta de teleformacion siguiendo las fases establecidas para el comportamiento de la informacion.

En la primera fase se analizan aspectos que llevan al comprador a reconocer el site como el que va a responder a sus necesidades de formacion, para lo cual se analizan aspectos del comportamiento de la informacion en la Web evaluando la primera impresion que produce el sitio: los aspectos visuales, como son los colores y los contenidos multimedia, auditivos; musica o voz en off. Es de sobra conocido el efecto de la musica en los compradores - las grandes superficies comerciales la usan adecuadamente-, al igual que la voz, llevando a los compradores al producto en venta. Finalmente, dado que se trata de una pagina con informacion, es imprescindible que el lenguaje sea claro y preciso para que sea entendido por cualquier persona.  
En la tabla 1, en el caso del color, bajo el epigrafe otros colores se incluyen: gris 16%, rojo 9%, verde 8%, naranja 6%, negro 3%, amarillo y blanco 2% y morado y marron 1%, pero el que domina es el azul en diferentes tonalidades.

Las plataformas de teleformacion espanolas dan mucha importancia a la imagen, con muchas fotos, dibujos, animaciones, asi como al lenguaje, claro y directo, pero no acaban de ver la importancia del sonido para captar alumnos en la Red.

La segunda fase de localizacion de informacion en sitios de formacion on line, es el proceso de inicio de busqueda de ciertos contenidos que dare paso a la tercera fase de busqueda especifica de informacion. En este caso se evaluan los aspectos que facilitan la localizacion de informacion, clave en el proceso de compra, ya que permite al comprador pasar a la tercera fase al tomar la decision de iniciar el proceso de bsqueda de informacion con intencion de realizar el curso en este sitio.

Los resultados muestran que se han cuidado mucho los aspectos tecnicos de acceso a la informacion tales como el tiempo de carga o el funcionamiento. Pero parece que aspectos como la presencia del buscador o la personalizacion no son considerados como se debiera; sobre todo la opción de navegar en varios idiomas por el site es un tipo de personalizacion que se analiza separadamente del resto dada la importancia que tiene, ya que una de las posibilidades que brinda Internet es el acceso a todos los paises; por tanto la posibilidad de varios idiomas aumenta el numero de posibles alumnos. De entre todos los sitios que permiten la personalizacion llama la atencion que niac Formacion es la unica que, permitiendo cambios en el tipo de letra, etc., no permite la posibilidad de elegir idioma.  

El comercio de formacion ha entendido claramente la necesidad de organizacion de los contenidos ya que el 100% presenta la informacion estructura, concretamente en menus: horizontal el 35% , vertical el 15% y la otra mitad presenta menus combinados, el 49% horizontal y vertical simultáneamente, y solo una pagina, Piquer Ensenanza y Formacion, con menus vertical y en forma de arbol (en concreto en su pagina de inicio presenta un menu arborescente, dado que este tipo de estructura es muy usada en la ensenanza, nada mas entrar, con un simple golpe de vista sitia al usuario en un centro docente).

En la tercera fase, cuando el usuario inicia el proceso de busqueda de informacion mas detallado y cuyo resultado es clave para realizar el siguiente paso a la fase en la que se produce la compra -por tanto la informacion evaluada es la que se refiere especificamente al producto y a la empresa-, se analiza el comportamiento de la informacion que se presenta en el sitio para generar confianza y seguridad.

Los contenidos comerciales en general estan muy bien indicados, pero menos de la mitad de las Web no indican el precio del producto, ni informacion mas detallada del curso, por ej: programa, bibliografia, etc y tampoco incluyen enlaces a otros sitios con informacion relacionada.

Los contenidos corporativos son mas valorados en general que los del producto; esta claro que en este caso la empresa tiene que mostrar muy buena imagen para captar alumnos para sus aulas virtuales. Sin embargo, el mapa de localizacion es el aspecto menos cuidado, bien porque sean empresas virtuales o porque el empresario no lo considere. A los usuarios siempre les atraen mas los centros fisicos que acceden a la Red frente a las empresas virtuales al 100 %.

En la ultima fase, cuando, como indican Spink ([Spink y Cole 2006](#autor6)), se reúne y se usa la informacion recuperada -es decir se realiza la compra- de los parametros estudiados, el mas usado (64%) es la certificacion, bien sea a traves de empresas certificadoras, o avalada por universidades o por la administracion publica. El resto de caracteristicas estudiadas la presentan menos de la mitad de las Web estudiadas, pero sobre todo llama la atencion que menos del 20% incluyan mensajes de seguridad e informacion aportada por otros usuarios, aspectos muy importantes que generan fiabilidad en el comprador y pueden ser muy importantes a la hora de tomar la decision de matricularse.

Finalmente lo que mas sorprende es la venta directa (cuando se puede matricular, adquirir los materiales del curso y pagar por la Red) con un 29% de resultados positivos; es cuanto menos llamativo que se use Internet para dar clases, pero no para pagar el curso.

## Conclusiones

El comercio electronico de la formacion en linea responde muy bien a las necesidades de los futuros alumnos, pero se han puesto de manifiesto aspectos claramente mejorables como son: musica y voz en off, la personalizacion especialmente la opcion de otros idiomas-  la presencia de un buscador en el sitio, incluir el precio de los cursos, o el mapa de localizacion de los centros. En definitiva, las tres primeras fases del proceso de compra valoran notablemente los comportamientos informacionalmente humanos,  cuidando mucho la imagen de sus sitios, la localizacion y organizacion de la informacion. Sin embargo la ultima fase, cuando se realiza la compra, es la peor cuidada. El uso de la informacion, compra del curso, es el aspecto informacional que hay que mejorar claramente en la venta de curso en linea, con la presencia de mensajes de seguridad, etc, que generan que el comprador considere el sitio seguro y fiable.

La teleformacion espanola se caracteriza por cuidar mucho aspectos informacionales referidos al comportamiento de la informacion que el usuario recibe de manera pasiva, es decir a primera vista.

Queda demostrado que el comercio electronico de teleformacion centra el diseno de sus sitios Web en el comportamiento humano de la informacion, tratando de captar alumnos ofreciendo la informacion que necesitan en el momento en que lo necesitan.

Los tipos de comportamiento de la informacion que participan en la venta en linea son:  (Information behaviour) al entrar en el sitio, (information seeking), al iniciar el proceso de localizacion de informacion, la busqueda de informacion, (information searching), al analizar los contenidos de los sitios localizados para encontrar el curso deseado y la garantia de la empresa y finalmente el uso de la informacion, (information use), para comprar el curso. 

## Agradecimientos

Este trabajo ha contado con la ayuda financiera del proyecto SEJ 003, recibida de la Junta de Extremadura y el Fondo Europeo de Desarrollo Regional. Queremos agradecer los comentarios de Ana Castillo y de los editores de la revista.

## Autores

M. Rosario Fernandez Falero es profesora del departamento de Informacion y Comunicacion de la Facultad de Biblioteconomia y Documentacion de la Universidad de Extremadura (Espana), es Licenciada en Ciencias Quimicas, especialidad Industriales y Doctora por la Universidad de Extremadura. Direccion de contacto [rferfal@alcazaba.unex.es](mailto:rferfal@alcazaba.unex.es)  
<a name="author0" id="author0"></a>Diego F. Peral Pacheco es profesor del Departamento de Informacion y Comunicacion de la Facultad de Medicina de la Universidad de Extremadura (Espana), es Licenciado en Antropologia y en Medicina y Cirugia y Doctor por la Universidad de Extremadura. Direccion de contacto [diego@unex.es](mailto:diego@unex.es)

#### Referencias

*   Espana _Ministerio de Educacion_. (n.d.). _[Formacion a distancia](http://www.webcitation.org/5x6xNZprq)_/ Retrieved 19 January 2011 from http://www.educacion.es/educacion/que-estudiar-y-donde/aprendizaje-largo-vida/formacion-a-distancia.html (Archived by WebCite? at http://www.webcitation.org/5x6xNZprq)
*   Espana. _Ministerio de Industria, Turismo y Comercio_ (2010). _[Estudio sobre Comercio Electronico B2C 2010](http://www.webcitation.org/5x6x4e8U5)_. Retrieved 29 January 2011 from http://www.red.es/media/registrados/2010-11/1288789343549.pdf?aceptacion=a1408615af86d805ac71da620cbf5257 (Archived by WebCite? at http://www.webcitation.org/5x6x4e8U5)
*   Fernandez Falero, M.R., Hurtado Guapo, M.A. & Peral Pacheco, D. (2006). [Metodologia de investigacion documental aplicada a la informacion economica presente en Internet: caso practico, Extremadura, Espana.](http://www.Web%20citation.org/5wJqpRqhL) _Hologramatica_ **1**(5), 3-19\. Retrieved 19 April 2010 from http://www.cienciared.com.ar/ra/usr/3/194/n5_v1_pp3_19.pdf (Archived by Web Cite? at http://www.Web citation.org/5wJqpRqhL)
*   Fernandez Falero M.R., Melindez Gonzalez-Haba, G. & Peral Pacheco, D. (2009). [Usabilidad de sitios Web comerciales dedicados a la teleformacion.](http://www.Web%20citation.org/5wJqehd7N) _Revista de Estudios Extrememos_, **65**(1) 517-533\. Retrieved 19 April, 2010 from http://www.dip-badajoz.es/publicaciones/reex/index.php?cont=1_2009 (Archived by WebCite? at http://www.Web citation.org/5wJqehd7N)
*   Sheng, H. (2006).  _U-commerce: a tale of two studies_. Unpublished doctoral dissertation, University of Nebraska, Lincoln, Nebraska, USA.
*   Spink, A. & Cole, C. (2006). Human information behaviour: integrating diverse approaches and information uses. _Journal of the American Society for Information Science and Technology_, **57(**1) 25-35.
*   Spink, A. & Cole, C. (2001). Introduction to the special issue: everyday life information seeking research. _Library and Information Science Research_, **23**(4), 301-4.
*   Spink, A. & Currier, J. (2006). Towards an evolutionary perspective for human information behaviour: an exploratory study. _Journal of Documentation_, **62**(2), 171- 193.
*   Van der Merwe, R. & Bekker, J.A. (2003). Framework and methodology for evaluating e-commerce Web sites. _Internet Research: Electronic networking application and policy_, **13**(5), 330-341.
*   Watson, R.T., Pitt, L.F., Berthon, P. & Zinkhan, G.M. (2002). U-commerce expanding the universe of marketing. _Journal of the Academy of Marketing Science,_ **30**(4), 329-43.
*   Wilson, T.D. (2000). Human information behaviour. _Informing Science_, **3**(2), 49-55.
*   Wilson, TD (1981). On users studies and information needs. _Journal of Documentation_, **37**(1), 3-15.