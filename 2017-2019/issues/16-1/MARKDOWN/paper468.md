#### vol. 16 no. 1, March 2011

# In search of facilitating citizens' problem solving: public libraries' collaborative development of services with related organizations

[Nozomi Ikeya](#authors)  
Palo Alto Research Center, 333 Coyote Hill Road, Palo Alto, CA 94304, USA  
[Shunsaku Tamura](#authors)  
Keio University, 2-15-45 Mita, Minato-ku, Tokyo, Japan  
[Makiko Miwa](#authors)  
The Open University of Japan, 2-11 Wakaba, Mihama-ku, Chiba-shi, Japan  
[Mika Koshizuka](#authors)  
Gakushuin Women's College, 3-20-1 Toyama, Shinjuku-ku, Tokyo, Japan  
[Seiichi Saito](#authors)  
Chiba Keizai University, 4-3-30 Todorokicho, Inage-ku, Chiba-shi, Japan  
[Yumiko Kasai](#authors)  
Tamagawa University, 6-1-1 Tamagawagakuen, Machida-shi, Japan



#### Abstract

> **Introduction**. The paper attempts to understand _value constellations_ in organising and using the business information service that was recently developed by various stakeholders with libraries who were in pursuit of supporting people's problem solving in Japanese public libraries.  
> **Method**. In-depth interviews were conducted not only with users and librarians, but also with members from specialised organizations who participated in organising the service.  
> **Analysis**. Tape-recorded interviews were transcribed and analysed to identify how the new set of services were designed and implemented in collaboration with organizations. Because the analysis was aimed to identify values recognised by those involved in designing, managing and using the services, an ethnomethodological approach was adopted as the approach is appropriate for understanding practical reasoning embedded in activities.  
> **Results**. Two approaches (i.e. value constellations) in designing the service were identified, one is reference service oriented approach, where the reference service is located at the centre of the business information service and its transformation is carried out; and the other is specialised service, programme-oriented approach, where the service is organized with more reliance on outside specialists. Different emphases on various values were also identified in using the business information service.  
> **Conclusions**. Close examination of value constellations as part of organizing and using the service seem to be an appropriate way to systematically understand and inform the actual services in relation to the use of such services.

## Introduction

Some public libraries in Japan have recently begun providing services that facilitate both information seeking and use by setting their service goal as supporting business. One unique feature of this service is that libraries have collaborated with institutions such as local government departments of business and industry and chambers of commerce and industry.

As a way of characterizing this service, this paper examines how libraries together with other institutions have managed to design and implement such services by paying attention to the logic actually used in organizing and using the service. By doing so, this paper attempts to understand such services as organized activities carried out by various interest groups, i.e., librarians, members of various institutions, and citizens. This approach is derived from two views. One is the idea of service as activity, presented by Normann and Ramirez ([1994](#nor94)). They argue that values in service are produced by actual activities accomplished by stakeholders. Another is an ethnomethodological interest in practical actions, i.e., how activities are organized so that people can recognise certain phenomenon as such ([Garfinkel 1967](#gh67)). Combining these two ideas, an attempt can be made to understand social phenomena as something actually organized by various group activities. This enables us to understand the new service not just as a product of such activities, but as something actually designed and implemented by different groups. In other words, this helps us to understand the service at the practical level and allows us to reflect upon its characteristics, its further development, and its application in other libraries. The advantage of taking such an approach is that it enables us to examine the practice of information seeking and use in relation to a service that enables and supports such practice. This allows us to consider the service from various perspectives at the level of each group's practice, which can inform each other.

## Background

In the previous paper ([Tamura _et al._ 2008](#tse08)) we developed a value model based primarily on the work of Normann and Ramirez ([1994](#nr94)) who argued that components of a system become flexibly combined with each other and form a _value constellation_ in which components (i.e., different groups) work together to create _joint value_, which is manifest in _offerings_, i.e., products or services that contain value. This is contrasted with a model Taylor ([1986](#tay86)) proposed, where he supposed that the processing of information in an information system could be seen as a value-added process, that is, a process which incrementally adds values through the information processing. According to the model, a system forms a value chain that extends beyond the system to its users, and it is somewhat fixed. On the other hand, from the value constellation perspective, '_value is co-produced by actors who interface with each other_'. The actors '_allocate the tasks involved in value creation among themselves and to others, in time and space, explicitly or implicitly_' ([Normann and Ramirez 1994](#nr94): 54). This may explain a situation better where various groups are involved in creating a service.

In fact, their theory of value constellation finds its application in information behaviour research in a series of studies which explored 'strategic information management and recognised the dynamics of networked social and intellectual capital where information sharing occurs among trusted insiders'. ([Schultz-Jones, 2009](#sb09); see also ).

The model of value constellation seemed appropriate to explain value involved in business information service we were studying. We initially developed a model where value is co-produced as a result of interactions among librarians, between librarians and users, and among users in their community. As research progressed, it became increasingly clear that some libraries had attempted to reconfigure the allocation of roles, work, and values when they tried to design a business information service through collaboration with other organizations. Thus, it made more sense to take another actor into consideration: organizations specialising in the business area which were collaborating with the library. This seemed to allow us to understand the dynamics of designing a service to facilitate people's information behaviour, i.e., the _value constellation_ of the service. While the value constellation model provides a way of explaining how the innovation of service takes place through stakeholder collaboration in creating a new value, how this value constellation is actually put into practice in actual cases remains to be further investigated. More specifically, as the co-production of values is emphasised for example, it is worthwhile to examine how each stakeholder came into the value constellation. For this interest, the ethnomethodological approach Garfinkel initiated is taken up to analyse various activities including information behaviour found to be meaningful, since the approach is concerned with understanding how activities are practically organized, i.e., how activities are carried out and what logics are being used. Thus, this approach is appropriate for examining the variety of practical reasoning held by each stakeholder organization in their activities, i.e., in designing, implementing, and using the service.

The previous paper focused on users' perception of usefulness of this service by delineating their information search processes, and developed a users' value model by using Miwa's _information behaviour grammar_ model ([2007](#mm07)) as a framework. However, the paper did not pay much attention to how each service was organized that enabled their service use as part of their information seeking and use. This paper will deal with a specific service being newly developed as an instance of services that take people's information search process into account.

## Methods

For the reasons discussed above, it made sense to us to conduct interviews not only with librarians and users, but also with various organisation personnel with whom the libraries collaborated in organizing the new services. Previously, we reported our research based on the analysis of interviews with librarians and users around the business information service at four different libraries in Japan ([Tamura _et al._ 2007](#tam07), [2008](#tam08); [Ikeya _et al._ 2008](#ike08)). The second round of interviews has so far been conducted at two of the four libraries (libraries A & B in Table 1 below). This time interviews were conducted not only with users and librarians, but also with members from other institutions who were usually part of the committees the libraries organised when they prepared and launched the new services to support business. Those we interviewed were typically staff members of local government departments of business and industry, as well as departments of agriculture, business consultants related to these local governmental offices, and industry and chambers of commerce and industry.

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Interviews**</caption>

<tbody>

<tr>

<th align="center"> </th>

<th align="center">Library A</th>

<th align="center">Library B</th>

<th align="center">Total</th>

</tr>

<tr>

<td>Users</td>

<td align="center">5</td>

<td align="center">5</td>

<td align="center">10</td>

</tr>

<tr>

<td>Librarians</td>

<td align="center">6</td>

<td align="center">3</td>

<td align="center">9</td>

</tr>

<tr>

<td>Members of other organizations</td>

<td align="center">10</td>

<td align="center">8</td>

<td align="center">18</td>

</tr>

</tbody>

</table>

In our previous paper, a typology was developed to categorise libraries that provide the business information service according to the kind of services provided. The two libraries at which interviews were conducted can be categorised as being in the consultation service group, that is characterised as providing a consultation service provided by business specialists. Thus, analysis in this paper will be focused primarily on libraries that are characterised as part of the consultation service group. We will discuss other categories in a later section.

In-depth interviews were conducted with users of this service, librarians who were involved in either designing and managing the service and members from other institutions that came to participate in the service. In each kind of interview, we tried to explore and obtain information using the following questions.

*   Interviews with users:
    1.  What was the last use made of the public library business information service concerned?
    2.  What was the goal of the use?
    3.  What was the process leading to the generation of the goal?
    4.  What use was made of other sources to meet the goal?
    5.  Can you give a detailed description of the use of the business information service for the goal? and
    6.  How would you evaluate the service?
*   Interviews with librarians:
    1.  What kind of services is currently provided in business information service in Japanese public libraries? It is necessary to get the overall knowledge of the field concerned, i. e., characteristics of the libraries offering the service, varieties of services provided, and so on.
    2.  What kind of users do librarians expect, and have in reality? Users and uses, both in expectation and in reality, are together determining factors in providing the service.
    3.  What kind of uses or values do librarians think users get from the service? Based upon the anticipated valuations by users, librarians organize their service, and, conversely, the organization of service will attract certain types of users.
*   Interviews with members from specialized institutions:
    1.  How did the organisation come to have collaboration with the library?
    2.  What was the collaboration about?
    3.  How did the organisation and members deal with the collaboration?
    4.  What does s/he think about the collaboration?
    5.  How does s/he normally use the library if s/he does either as part of work or private?

Tape-recorded interviews were transcribed and analysed to identify how the new set of services were designed and implemented in collaboration with different organizations. Because the analysis aimed to identify values embedded and experienced by the respondents in designing, implementing, managing and using the services, an ethnomethodological approach was adopted in the analysis (see also [Ikeya and Tamura 2010](ike10)). More specifically, the focus was put on identifying the practical reasoning or understanding commonly held by different kinds of stakeholders in engaging with the new service in various forms.

## Designing a service for facilitating problem solving

### organizing a service based on the problem-solving framework

The business information service is new to Japanese public libraries. Stimulated by the services of American public libraries, this service was initiated in the late 1990s. In 2000, a group of librarians, journalists and academics formed an association promoting the service called the Bijinesu Shien Toshokan Suishin Kyogikai [The Business Library Association] ([Saito 2009](#ss09)). The goal established for this service is _supporting business_, and its literal translation is _business support service_.

The service is defined by the Association in the following manner:

> The business support library is a library with an additional function that is designed to support businesses including starting up businesses, both by using accumulated information in the library and various digital information available on the Internet and databases, and by training libraries to manage the information ([Takeuchi 2005](#tt05): 39)

The definition specifies that this service will be provided by librarians who are trained to search and manage information resources the library makes available to public.

The objective of this service, defined as 'supporting business', implies a radical transformation in Japanese public libraries in the following two senses. Firstly, it implies adopting the view for organizing a service in which public libraries should support human problem solving activities. This view has often contrasted with the view that the book circulation service should be the core of the public library services?a debate with which this paper is not concerned. Secondly, the service implies an openness to the ways in which the service is designed. It is important to note that the word 'information service' is not included in the literal translation of the service name. The terminology and the goal do not narrowly define the library role or means for providing the service. It does not define, for example, that the library's role is to provide information. Although how seriously this phrase was taken when it was created and introduced to stakeholders is something that needs further investigation, it is still true that different organizations, independent from the library, were brought into play some major roles in creating the service with the library, and the diversity of service has been noteworthy. In this sense, it is fair to say that business support is an example of _open innovation_ in Japanese public libraries.

Furthermore, it is interesting to note that the service movement itself was initiated by a mixture of people who were not necessarily direct stakeholders in the public library: journalists, academics and government officials. This is somewhat distinctive, as library services have typically been viewed and actually designed solely by librarians themselves. However, this does not imply that librarians were forced to provide such a service by external forces. In fact, a few librarians shared the general view with individuals promoting the service that there is a necessity to reorganize services so that library users can see the public library not just as a place to find good books to read for their pleasure, but as a place to search for information to solve problems they face in life. In fact, some librarians were aware that the number of male users in their fifties to seventies had been on the rise, and some reference questions clearly had to do with solving work related problems ([Oyamashiritsu Toshokan 2009](#ot09); [Tokoyoda 2003](#tr03)).

Additionally, public library budgets had been decreasing in most parts of Japan due to local government economic difficulties. Increasingly, librarians recognised the need for libraries to directly contribute to solutions to problems that people in the local community face in their everyday lives ([Kobayashi 2008](#kt08)). They decided one way to reorganize library services so that they could be of help to people in everyday life would be to create services supporting local area businesses, and thus enable them to contribute to the local economy ([Takeuchi 2005](#tt05)).

In addition to information resources and librarian information skills, the general image that a public library is a place that is open to all citizens is also considered valuable in forming this service ([Tokoyoda 2003: 168](#tr03)). While this value has been taken for granted by librarians, it has been _rediscovered_ by various business information service stakeholders as a distinctive public library value. Among those we interviewed, notably people from different specialised organizations acknowledged that one attractive and important public library value is the perspective of providing services to all citizens who have the need.

Thus, the business information service is specifically designed to support problem solving in business or work-related contexts by utilising library information resources, librarians' research and information management skills, and by taking advantage of the library as a place open to all citizens. organizing a library service based on the problem-solving model is clearly distinctive in the context of Japanese public libraries where strong emphasis has long been put on the book circulation service. It is also generally agreed among advocates of this service that the mission of public libraries is to deliver this kind of support by facilitating information seeking and use ([Tamura 2008: 57](#ts08)). The issues of how to design and provide actual services still seem to remain; they are up to each library to decide. This is one of the reasons why our project has been closely examining how each library has been organizing their service.

In our previous research three library typologies were presented ([Tamura _et al._ 2007](#ts07)). _The minimal service group_ includes libraries providing a special business-related collection and identifying it as such by setting off a couple of shelves or designating a corner with shelves for it. _The reference service group_ includes libraries that provide reference service as well as a special collection for supporting business related problem solving. _The consultation service group_ includes libraries that provide consultation on business-related topics in conjunction with specialist organizations in those areas, such as the local governmental department of business and industry and the chamber of commerce and industry. Research has identified that various programmes are offered in the library, often in conjunction with other specialist organizations. Most seminars and lectures on specific topics are a way of providing information that would facilitate problem solving in a business context.

Thus, there are a variety of service menus for business information services, and each is organized to facilitate information seeking and its use in support of problem solving. Our research has identified that not only are there different ways in which service menu items are actually organized to deliver as a service, but there are different assumptions about how the value of facilitating problem solving should be delivered to people. In the following, different value constellation types will describe how values and stakeholders are organized to create each constellation that is also recognised as producing certain values by different stakeholders.

### Designing the actual service for dealing with people's problem solving

We have so far identified two different patterns of organizing the service at libraries that fall into either _the reference service group_ or _the consultation service group_. The distinction is not so much as whether a library provides a specific service menu such as a reference service or a consultation service, but rather, the difference is how a specific value is selected and is organized with particular resources to create the service value. Thus, the difference here is whether or not the reference service is located at the core of the new service.

#### The reference service-oriented approach

This approach is characterised by its concern with locating the reference service at the centre when the business information service is organized. An underlying assumption here is that in the context of book circulation service framework always having been dominant, reference service has not been properly directed towards citizens ([Tokoyoda 2003](#tr03); [Kobayashi 2008](#kt08)). That is to say, while the problem-solving framework has been presupposed in the provision of reference service, it has never been clearly indicated to the public, and thus, the fact that people can go to the library to obtain information and be assisted by a librarian in the process of problem solving has not exactly become integrated within common sense. At the same time, in terms of staff allocation and an emphasis on values, it is acknowledged that not all libraries have organized themselves to be ready to provide a service supporting citizens' problem-solving, ([Kobayashi 2008](#kt08)).

In taking this approach, the first thing libraries need to do is to transform the reference service so that it can deal with people's problem-solving not only in a business context, but also in a more general context. This itself implies a big step for most public libraries in Japan as exiting reference service policy simply calls for librarians to be concerned with locating the right information resource and leaving the user responsible for finding the relevant information. Under the existing policy a librarian consequently has no concern with the user's purpose in using any particular information, i.e., what kind of problem solving (i.e., purpose for information use) the user is engaged in should not matter to a librarian. Thus, a reference service transformation also involves training librarians so that they can provide an appropriate service to the user primarily by furnishing relevant information from among their information resources; but this function may go beyond that, and it may involve communicating with a suitable specialist in the relevant area so that the user can directly confer with that person. Reorganization of staffing for the reference service becomes another issue to be considered in making sure a skilled librarian will be ready to serve users. In addition to the above, selecting appropriate library information resources that provide pertinent information to users in the business related problem-solving context is another essential part of the transformation of reference service.

Taking this _reference service oriented approach_ does not require a library to remain closed to other possibilities in organizing its business information service. In fact, some libraries collaborate with various business specialist organizations both formally and informally on regular basis. One library, for example, not only holds a regular meeting to exchange information with a neighbouring organization that supports people who start up businesses, but they also refer their users to each other's service desk so that individuals can receive service appropriate to their needs. The library also collaborates with the organization whenever they hold seminars and other events by advertising in the library as well as by displaying books related to the event topic. Thus, the library, together with the specialist organization tries to facilitate information seeking and use for problem solving in a business context to deliver value to the users.

Another library also collaborates with other specialised organizations both formally and informally. The library initially hosted a regular committee meeting to discuss how to collaborate for organizing business support. While this formal committee was dissolved, the librarians are now part of informal gatherings where people from specialised organizations in business meet. Thus, collaboration may take place on a daily basis: a librarian at the reference service desk may call a member of the department of the local governmental business and industry to ask for advice in handling a user inquiry. The librarian may obtain the name of a person in another organization with whom the user should speak. The library may also circulate lists of reference books for potential purchase to solicit specific organization suggestions regarding such purchase decisions. In this way, by providing feedback as to what books are good for the library to have in terms of communal information resource in the local community, members of specialised organizations offer some input in building the library collection.

Further, specialist organizations often find the library an attractive place for holding different service programmes. The chamber of commerce and industry, for example, holds regular consultation service for people who want to start businesses. The library helps the organization administer these services not only by making appointments, but also by preparing related books for people coming for advice. This is one way of facilitating user information seeking and use. The library also helps various specialised organizations organize seminars and other events by preparing related books in the conference room so that event participants are able to see and borrow them. Librarians also try to acquire a short time slot whenever they can to inform participants of library services, i.e., in solving their problems they can search for information, and ask for librarian assistance when needed. By doing so, librarians hope to inform lecturers as well as audiences about how they can use the library. They even try to do the same thing for business related seminars outside the library.

This particular library also treats local government staff as its _users_, holding seminars to facilitate problem solving in policy making, and includes this programme in their business information service. From the stance that the business information service can be provided by properly addressing and exercising the reference service in collaboration with other specialised organizations this development probably comes naturally. This library did not create another new category of service for local government staff as other libraries have. In such manner, libraries adopting this _reference service oriented approach_ attempt to take every opportunity to inform people of their service capabilities so that they become their users.

#### The specialised service programme oriented approach

Another way of organizing business information service apart from the _reference service approach_ discussed above is characterised by a library relying more on outside specialist teams or organizations in providing a business information service. This normally involves organizing a new service menu in which specialist groups normally play a major part in service implementation. This is the very reason it is called the _specialised service programme oriented approach_.

This is not to say that this approach ignores existing library services. Libraries that select this approach surely appropriately enhance their information resources so that people with business-related problems are able to use them. Further, libraries encourage their reference librarians to develop skills and become equipped to help such users. For example, librarians take training courses operated by the Business Library Association. At the same time it is fair to say that significant emphasis is not put on making changes to the existing reference service.

More efforts are made on creating new specialised service menu items, such as consultation services, seminars and other related programmes. Through these efforts libraries are urged to collaborate with specialist organizations such as the local governmental department of business and industry, and the chamber of commerce and industry. While some libraries outsource such services to these outside organizations, others take a role in coordinating the service programmes and let outside organizations administer the service programmes. To what extent libraries participate in administering the programmes varies according to their resource constraints. On the whole, these service programmes tend to be administered by a specialised department or team more or less independently from the rest of the library services.

In this way, the difference between the two approaches in organizing business information service for dealing with people's problem solving may to a certain extent be a matter of relativity, dependant upon how much emphasis is put either on the reference service or the new specialised service programmes. Potential consequences resulting from these two different approaches will be discussed later.

## Using a business information service

As reported elsewhere, we identified three effects of public library business information service use: 1) getting relevant information, 2) making connections with relevant people and organizations, and 3) getting affective support ([Tamura _et al._ 2008](#tse08)). Business information service users we interviewed seemed to benefit from all three effects, as we reported earlier, and this was confirmed again in the second set of interviews conducted this time.

### Values recognised by users of a library adopting the _reference service oriented approach_

When we review the interviews we reported earlier ([Tamura _et al._ 2008](#tse08)), we see how users of libraries adopting different approaches recognise values differently. Users of a library implementing the _reference service oriented approach_ acknowledged in interviews the effect of relevant information as something derived from two values they recognise: library information resources and librarian expertise. It is not to be forgotten that we interviewed some users of the reference service, so this result may be completely natural. However, at the same time, we should note that often the local government officers mentioned the value of librarian skills and library information resources as users, for both professional and private use. Another effect, that of making connections with relevant people and organizations, was mentioned by users of this type of library in relation to the values they recognised in librarian expertise in finding not only the relevant information but also locating relevant persons the user might have otherwise found extremely difficult to track down and actually meet. In terms of the effect of getting affective support, some users of the library taking the _reference service oriented approach_ mentioned receiving support from librarians, by users getting back to them during the process of solving problems, e.g., in preparing for starting up a new business.

### Values recognised by users of a library adopting the _specialised service programme-oriented approach_

Users of the library adopting the _specialised service programme oriented approach_ primarily acknowledged the latter two effects: that of making connections with people and getting affective support from people. However, the effect of information provided by the library was not normally mentioned alongside these two effects. When we asked specifically about information they gained through the use of the business information service, it tended to be information that they obtained from lectures or through an exchange of information among attendees rather than information obtained through searching library information resources. The implication here may be that the attendees of such business seminars held at the library did not necessarily become the users of other services in the library, e.g. the reference service. If this is correct, then the value they recognised is that of the library as a place or forum where they could acquire these effects.

Considering what makes it possible for people to experience the effects of getting affective support and making connections with people in the first place is probably meaningful. People, including previously non-library users, would likely visit the library to participate in seminars because they find the public library as a place open to all citizens. One user told us:

> When it comes to business support, it has been usually associated with places such as the chamber of commerce and industry, hasn't it? So I felt more comfortable when I came to learn that it (the business seminar) would be held at the library not at the chamber of commerce and industry. This is what other people have also said. If it is held at the chamber of commerce and industry, it makes people feel like that you are told where you should borrow money, or you are asked to reassure them that you are starting a business. (omission) If it is held at the library, it feels like you are still allowed to join while you may be just thinking that you may want to start up business at some point in future (UO).

It is important to note that some seminar attendees actually started their businesses. That is to say, seminars coordinated by the library, while taught by business specialists, certainly facilitated participant problem solving, not only by providing information in the form of a series of seminars but also by providing a forum where they could exchange information, and by offering psychological support and connections with other people during the process of individual problem solving.

## Collaborating with a library to provide a service

Interviews conducted with members of specialist organizations that have been collaborating with libraries to deliver a business information service show that they generally highly value their collaboration. What follows is a list of values recognised by those organizations collaborating with a library in delivering a business information service.

### The public library as a place open to all citizens

Regardless of the different approaches libraries take, every specialised organization member we interviewed valued the public library as a place anybody can visit at any time with or without a specific purpose. They regarded this as a significant advantage because although they always wanted to encourage and help people who have problems in their business or start up a new business, they knew that people who are not familiar with these specialised organizations would not easily approach them. People have a much lower psychological barrier regarding the public library when compared to specialised organizations. Actual experience has taught them that holding events with a library can attract far more people, or a different kind of people from those drawn to their organizations.

> What is often said about general economic organizations such as us is that people do not find it very easy to visit us. They normally feel that they won't be able to get advice unless they have specific purposes. They do not find it comfortable visiting us with just rough initial ideas. So we ourselves need to go outside our office. Here is where libraries come in. If they can collaborate with us, that can turn out to be supporting business or starting up business (RO7).

That public libraries are open during weekends is seen to be another advantage for attracting more people.

### The public library as a communal information resource

Members of organizations collaborating with public libraries implementing the _reference service oriented approach_ regard the public library as a communal information resource not only for citizens, but also for local government staff members like themselves. They encourage people they advise to visit the library to find information. They themselves are often serious library users and they volunteer to provide some suggestions as to what books the library should have on a specific topic area.

> I often intentionally tell business persons to visit the library themselves, saying 'if you borrow books and read them with your own eyes, then you will find more good things'. Then they come back and tell me that 'Yes, there are many good things there, aren't there?'. If they go there, then they are likely to borrow other books. Even the advancement of technology starts from the soft level to the more difficult level. It does not go to the difficult level at the beginning. So you need some information at the soft level, and that is where the library can take charge, while what researchers at the industry and technology centre or professors at the university would really need should not be what the library should run. They need to have it with them. However, some information at the middle level should be kept in the public library, and the rest should be owned by the library (RT3).

That they value their library as a communal information resource means that they recognise the value of library information resources in the context of their own work.

### The public library as an information searching capability

Librarian information searching capabilities are also valued by specialised organization members collaborating with the library adopting the _reference service oriented approach_. Comprehensive and systematic search is recognised as a specialised librarian capability, and specialised organization members seem to rely on librarians for this reason when they need to do a thorough search in their own work.

> When someone from a company comes to us, we may need to conduct a thorough investigation of a specific industry including how the business is organized, how the particular field is related to other industries. We also need to conduct marketing research as well. That is another area we owe so much to the library. There usually exist statistical data, articles written on the industry, and I may need to request a specific journal issue. So I go to the library desk and make all the requests, and the librarian deals with them nicely. In fact, they now often suggest some different information that may be of interest to me. I feel that they are becoming more alert with information. (RT5)

This remark seems to demonstrate that librarian expertise is recognised as a value by specialised organization members.

Thus, the _public library as a place open to all citizens_ value is recognised by collaborators with libraries adopting either approach. However, according to interviews we conducted so far, the two other values, that of the _public library as communal information resource_ and the _public library as information searching capability_ are typically recognised by those who collaborate with the library that takes the _reference service oriented approach_.

## Conclusion

We have so far described two different ways in which public library business information service is organized in Japan. One is the _specialised service oriented approach_, and the other is the _reference service oriented approach_. For these two approaches, our analysis was first made on the logic utilised when the goal of the service was defined by a group of librarians, academics and journalists comprising the Business Library Association. Further analysis was made on the logic the service uses, and also on that of the specialist organizations collaborating with libraries in delivering the service, by focusing on values recognised by these organizations. Thus, these two approaches can be regarded as value constellations that are organized differently. They aim to achieve the same goal, that of delivering the value of facilitating problem solving in a business context. However when analysed at the level of service design, use and implementation, we are able to see how differently these value constellations are organized. Each organization has goals that may be different or overlap with other stakeholder goals. Naturally, they may have different values to deliver to citizens, and the assumptions they have about delivering the values may also differ. When various organizations try to collaboratively deliver the same value, how the collaboration actually comes to be organized may vary. It is the value constellation dynamics that we think are worth investigating.

As was examined earlier, two different approaches we identified for organizing a business information service seem to involve different sets of values recognised by the organizations involved. On the one hand, the service implemented by a library taking the _reference service oriented approach_ seems to be recognised by others as involving the values of library information resources, librarian expertise and the library as a place open to all citizens. On the other hand, among these three values, the service implemented by a library taking the _specialised service, programme-oriented approach_ seems to be recognised by others as most involving the value of the library as a place open to all citizens. While it is still too early to make any conclusive statements, we can still consider how these recognised value sets are related to the goal initially set up for the business information service, i.e., supporting solving business-related problems by using library information resources and by fostering librarians. Some reconsideration may be necessary when the value of library as a place is the most recognised among the three values, as recognized values would affect people's organization of information seeking activities.

The service itself and our study, in many ways, seem to validate the new role of intermediaries i.e., a more interactive, collaborative role in the information search process in the workplace, suggested by Kuhlthau ([1996](#kc96)). In fact, the fact that this service has been successful seem to suggest that studies and models of information seeking and use can inform and contribute to design of new information services. This paper has shown that studying the organization of a service that supports and enables the users' information seeking and use itself can be illuminating, and even more so when it is examined in relation to the users' practice in the context of a particular service. The kind of analysis undertaken in this paper can be taken as a mode connecting studies on information seeking with those on library service evaluation.

## About the authors

Nozomi Ikeya is a Senior Research Scientist in Ethnography Services Area, Computing Science Laboratory at Palo Alto Research Center. She received her M.A. in Library and Information Science from Keio University, and her Ph.D in Sociology from the University of Manchester. Her interest is in ?knowledge in action? (i.e. practical management of knowledge), and conducts ethnographic and ethnomethodological studies of work in various organizational settings. She can be contacted at [nozomi.ikeya[at]parc.com](mailto:nozomi.ikeya@parc.com)

Shunsaku Tamura earned a BA in cultural anthropology from the University of Tokyo, a MA in library and information science from Keio University. He is currently a professor at Keio University, and specialized in information-seeking behaviour and reference and user services in public libraries. He can be contacted at [tamaran@slis.keio.ac.jp](mailto:tamaran@slis.keio.ac.jp)

Makiko Miwa earned a BA in history from Japan Women?fs University, an MSL from the University of Pittsburgh, and Ph D. from the Syracuse University. She is currently a professor at the Open University of Japan, teaching digital literacy course as well as database and information management course. She is also a professor at the Graduate University of Advanced Studies, teaching information behaviour course and research method course. Her research interests include information search behaviour, science communication, and information literacy. She can be reached at [miwamaki@nime.ac.jp](mailto:miwamaki@ouj.ac.jp)

Mika Koshizuka earned a BA in 1989 from Keio University, an MSL from Keio University. She is currently a professor at Gakushuin Women?fs College and teaching librarian training courses, information seeking behavior, and media study. Seiichi Saito earned a BA and in law and librarian certificate in from Aoyama Galkuin University, employed by the City of Tachikawa as a librarian, and joined the Chiba Keizai College as an associate professor. He is teaching librarian training courses. Contact her at [mika.koshizuka@gakushuin.ac.jp](mailto:mika.koshizuka@gakushuin.ac.jp)

Seiichi Saito earned a BA and in law and librarian certificate in from Aoyama Galkuin University, employed by the City of Tachikawa as a librarian, and joined the Chiba Keizai College as an associate professor. He teaches librarian training courses. You can contact him at [sei01-s@jcom.home.ne.jp](mailto:sei01-s@jcom.home.ne.jp)

Yumiko Kasai earned a BA in Japanese literature from Meiji University, and a Master's degree and Ph.D. in interdisciplinary information studies from The University of Tokyo. She is currently an associate professor at Tamagawa University. Her research interests include information search behaviour, information literacy, educational technology, learning theories and school library. She can be reached at [ykasai@edu.tamagawa.ac.jp](mailto:ykasai@edu.tamagawa.ac.jp)

#### References

*   Garfinkel, H. (1967). _Studies in ethnomethodology._ Englewood Cliffs, NJ: Prentice-Hall.
*   Huotari, M.-L. & Wilson, T.D. (2001). [Determining organizational information needs: the critical success factors approach](http://www.webcitation.org/5x4Pnr1bV). _Information Research_, **6**(3), paper 108\. Retrieved 10 April, 2011 from http://InformationR.net/ir/6-3/paper108.html (Archived by WebCite? at http://www.webcitation.org/5x4Pnr1bV)
*   Huotari, M.-L. & Chatman, E. (2001). Using everyday life information seeking to explain organizational behavior. _Library and Information Science Research_, **23**(4), 351-366.
*   Huotari, M.-L. & Iivonen, M. (2005). Knowledge processes: a strategic foundation for the partnership between the university and its library. _Library Management_, **26**(6/7), 324-335.
*   Ikeya, N. & Tamura, S. (2010) _Phenomena of value co-creation in service._ Paper presented at the 2nd International Service Innovation Design Conference (ISIDC 2010), Future Hakodate University, October 18-20, Hakodate, Japan.
*   Ikeya, N., Tamura, S., Miwa, M., Koshizuka, M., Saito, S. & Saito, Y. (2008). [Bijinesu shien sabisu no sekkei to unei](http://www.webcitation.org/5wrnrL2cf) [Designing and implementing business information service]. Paper presented at the Spring Conference of Japan Society of Library and Information Science, University of Tokyo, Japan. Retrieved 1 March, 2011 from http://www.slis.keio.ac.jp/~tamaran/research/bizlib/1_3.pdf (Archived by WebCite? at http://www.webcitation.org/5wrnrL2cf)
*   Kuhlthau, C.C. (1996). [The concept of a zone of intervention for identifying the role of intermediaries in the information search process.](http://www.webcitation.org/5wzgqbpK8) _Proceedings of the 59th Annual Meeting of the American Society for Information Science_, **30**, 91-94\. Retrieved 1 March, 2011 from http://www.asis.org/annual-96/ElectronicProceedings/kuhlthau.html (Archived by WebCite? at http://www.webcitation.org/5wzgqbpK8)
*   Kobayashi, T. (2008). Shigoto ya seikatsu ni yakudatsuto ninchi sareru toshokan ni narutameni [Towards libraries being recognised as useful at work and in daily life]. In N. Ogushi (Ed.), _Mondai Kaiketsugata Sabisu no Sozo to Tenkai_ [Problem solving oriented service: its creation and implementation]. (pp. 21-52). Tokyo: Seikyusha.
*   Miwa, M. (2007). [Verification of information behavioral grammar: role of searchers.](http://www.webcitation.org/5wzhQO7qE) Proceedings of the 70th ASIS&T Annual Meeting, Milwaukee, 10-2007, 44, paper 74\. Retrieved 1 March, 2011 from http://www.asis.org./Conferences/AM07/ (Archived by WebCite? at http://www.webcitation.org/5wzhQO7qE)
*   Normann, R. & Ramirez, R. (1994). _Designing interactive strategy: from value chain to value constellation_. Chichester, UK: John Wiley.
*   Oyamashiritsu Toshokan. (2009). 'Sai charenji' wo oensuru toshokan [Libraries supporting 'trying again']. _Shakai Kyoiu_, **2**, 20-23.
*   Saito, S., Miwa, M., Tamura, S., Kasai, Y., Ikeya, N. & Koshizuka, M. (2009). [The role of the business training program in the business support services provided by public libraries.](http://www.webcitation.org/5wzlUTm4T) In _Proceedings of the Asia-Pacific Conference on Library & Information Education & Practice, 2009, March 7-9, Tsukuba, Japan_. Retrieved 1 March, 2011 from http://a-liep.kc.tsukuba.ac.jp/proceedings/Papers/a64.pdf (Archived by WebCite? at http://www.webcitation.org/5wzlUTm4T)
*   Schultz-Jones, B. (2009). Examining information behaviour through social networks: an interdisciplinary review. _Journal of Documentation_, **65**(4), .592-631.
*   Takeuchi, T. (2005). [Sogyo oyobi chusho kigyo no bijinesu wo shien suru kokyo toshokan](http://www.webcitation.org/5wzlUTm4T) [Public libraries supporting business start-ups and small businesses]. _Kokumin Seikatsu Kinyu Koko Chosa Geppo_, **531**, pp.38-41\. Retrieved 1 March, 2011 from http://www.sangyo-npo.jp/chosyoronbunsyuzai/kokumin-cyosageppo200507.pdf (Archived by WebCite? at http://www.webcitation.org/5wzgqbpK8http://www.webcitation.org/5wzmiCRLo)
*   Tamura, S., Miwa, M., Saito, Y., Koshizuka, M., Kasai, Y., Matsubayashi, M. & Ikeya, N. (2007). [Information sharing between different groups: a qualitative study of information service to business in Japanese public libraries.](http://www.webcitation.org/5wzn8SE6A) _Information Research_, **12** (2) paper 306\. Retrieved 1 March, 2011 from http://InformationR.net/ir/12-2/paper306.html (Archived by WebCite? at http://www.webcitation.org/5wzn8SE6A)
*   Tamura, S. (2008). Bijinesu shien sabisu [Business information service]. In S. Tamura & T. Ogawa (Eds.), _Kokyo Toshokan no Ronten Seiri_ [Issues on Public Libraries]. (pp. 35-58) Tokyo: Keisoshobo.
*   Tamura, S., Miwa, M., Koshizuka, M., Ikeya, N., Saito, S., Kasai, Y., Saito, Y. & Awamura, N. (2008). [Satisfaction and the perception of usefulness among users of business information service in Japan.](http://www.webcitation.org/5wznwm2iu) _Information Research_, **13** (4) paper 366\. Retrieved 1 March, 2011 from http://InformationR.net/ir/13-4/paper366.html (Archived by WebCite? at http://www.webcitation.org/5wznwm2iu)
*   Taylor, R. S. (1986). _Value-added processes in information systems_. Norwood, NJ: Ablex.
*   Tokoyoda, R (2003). Kokyo toshokan ni okeru bijinesu shien sabisu no genjo [The current status of business information service in public libraries]. In _Urayasu Toshokan ni Dekiru Koto_. [What Urayasu Public Library Can Do]. (pp. 160-172). Tokyo: Keisoshobo.