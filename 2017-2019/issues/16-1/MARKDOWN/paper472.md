#### vol. 16 no. 1, March 2011

# Everyday hassles and related information behaviour among youth: a case study in Taiwan

#### [Ya-Ling Lu](mailto:yalinglu@rci.rutgers.edu)  
Department of Library and Information Science, School of Communication and Information, Rutgers University, 4 Huntington Street, New Brunswick, NJ 08901

#### Abstract

> **Introduction.** This reported study is a part of a larger project. The research objective considered in the present paper is information behaviour and everyday hassles among youth in Taiwan.  
> **Method.** The sample consisted of 133 children, including sixty-five girls and sixty-eight boys, in fifth- and sixth-grade classrooms in a public elementary school in an urban community in Taiwan. This study employed a semi-structured journal adopted from Sorensen's coping research. The journal contained eight questions.  
> **Analysis.** The aim of this study was description and conceptualization. Both qualitative and quantitative analyses were used to explain and describe the phenomenon. Daily problems were coded and analysed based on established coping research in psychology. In analysing data regarding children's information behaviour and coping, the method of open coding was used because typologies of children's information behaviour and coping were unavailable.  
> **Findings.** Children in this study reported fifteen categories of everyday hassles and exhibited four different types of related information behaviour.  
> **Conclusion.** These findings have implications for children's services.

## Introduction

This study is a part of a larger project. The research objective considered in the present paper is information behaviour and everyday 'hassles' [an American colloquialism signifying problems of varying difficulty] among youth in Taiwan. It is based on a case-study carried out in a local public elementary school. The research objective has been chosen for one main reason: children's information behaviour regarding their everyday hassles are seldom studied within library and information science. Virtually no basic research has examined how (or if) young people use or seek information to cope with their problems and challenges and what role information plays in children's daily lives when they encounter various problems.

In order to address the aforementioned issues, the following research questions guided the investigation.

*   What everyday hassles do children in Taiwan encounter? How do children cope with these hassles?
*   How do these children interact (or not interact) with information in this everyday, coping context? What information behaviour do these children exhibit in coping with their everyday hassles?

The above questions were chosen for exploration for two major reasons. First, although information behaviour can be culturally situated, the experiences of children in one culture can still help provide the building blocks for extended future studies that analyse, explain, and compare similar experiences among children in other cultures. Secondly, examining children's information behaviour to cope with daily-life hassles sheds light on the understanding of children's daily-life coping and relevant information services for them.

## Definitions and related works

As there has been relatively little writing and effort devoted directly to understanding the information behaviour and everyday problems of children, not to mention, specifically, children in Taiwan, this literature review depends largely upon studies and concepts that are implicitly relevant. These studies provide the background to link together children's coping, daily hassles, and information behaviour.

### Information behaviour

Wilson defines human information behaviour as,

> the totality of human behaviour in relation to sources and channels of information, including both active and passive information seeking and information use. Thus, it includes face-to-face communication with others, as well as the passive reception of information as in, for example, watching TV advertisements, without any intention to act on the information given. ([Wilson 2000](#wilson2000): 49)

In other words, human information behaviour looks at how, or if, one person interacts or does not interact with information in different situations. Information seeking, information use, information blunting, and other information-related behaviour all fall within this concept.

### Everyday hassles

Everyday hassles are '_irritating, frustrating, distressing demands that in some degree characterize everyday transactions_' ([Kanner _et al._ 1981](#Kanner1981): 9). These studies focus on problems and difficulties confronting the child that '_do not pose a serious threat to him or her_' ([Seiffge-Krenke 1995](#Seiffge-Krenke1995): 12). It has also been found that everyday hassles are better predictors of children's health ([Kanner _et al._ 1981](#Kanner1981)). Kanner _et al._ (1981) studied hassles and uplifts in 9- and 11-year-old children, and developed the _children's hassles scale_, consisting of twenty-five items covering the areas of family, school, friends, and play in children's lives. Two other studies have developed instruments to tap children's hassles: the Early Adolescent School Role Strain Inventory ([Fenzel 1989](#Fenzel1989)) and the Adolescent Hassles Inventory ([Bobo _et al._ 1986](#Bobo1986)), both highlighting school, peer, and parents as critical components in children's everyday hassles. School-related stressors were most frequently reported, followed by interpersonal stressors such as conflicts with parents, siblings, and peers ([Anda _et al._ 2000](#Anda2000); [Donaldson _et al._ 2000](#Donaldson2000)).

### Information seeking as coping

In coping literature, information seeking is defined as '_behaviour that involves obtaining information about the stressor_' ([Ryan-Wenger 1992](#Ryan-Wenger1992): 261). It has been studied mostly as a response '_to the stresses of disease and disability_', and so has a strong medical or clinical application. Coping scholars see information-seeking an adaptive process to 'learn more about a stressful situation or condition, including its course, causes, consequences, and meaning as well as strategies for intervention and remediation' ([Skinner _et al._ 2003](#Skinner2003): 243), and the methods involved include reading, observation, and asking others ([Skinner and Zimmer-Gembeck 2007](#Skinner2007)). In today's world the internet is also used both for information seeking and entertainment in response to stress. For instance, Leung ([2007](#Leung2007)) found that the use of the Internet can provide children with mood management.

Mixed results have been found regarding the influence of information seeking as a coping strategy for children in clinical settings. For example, studies have shown that either information-seeking or providing information-based interventions can yield a broad range of benefits to pediatric patients ([Peterson 1989](#Peterson1989)), to children in managing stress related to surgery ([LaMontagne _et al._ 1997](#LaMontagne1997)), and to children who undergo psychological adjustment to medical procedures ([Lazebnik _et al._ 1994](#Lazebnik1994)). On the other hand, information avoidance is also found to be a good strategy for some children in dealing with surgery-related stress ([Hubert _et al._ 1988](#Hubert1988)).

### Everyday life information seeking

Savolainen's ([1995](#Savolainen1995): 266-267) model of 'everyday life information seeking' refers to

> the acquisition of various informational (both cognitive and expressive) elements which people employ to orient themselves in daily life or to solve problems not directly connected with the performance of occupational tasks'. The three major elements to highlight in this short quote are: the acquisition of information (or information seeking), non-occupational problems, and problem solving. The three elements point to the goal of ELIS?'the mastery of life ([Sovalainen 1995](#Savolainen1995): 254).

or, the activities to take care of life. Savolainen sees information seeking as an integral component of mastery of life because it '[facilitates] problem solving' and thus helps keep things in order. Therefore, the major questions pursued in everyday life information research are: How do people seek information to keep their daily-life activities in order?

In the past decade, in its research in the area of children and adolescents, everyday life information seeking has particularly focused on their information-seeking and needs, such as adolescent girls' daily life information needs ([Poston-Anderson and Edwards 1993](#Poston-Anderson1993)), adolescent girls' health information use (Todd [1999](#Todd1999)), children's daily life information needs ([Walter 1994](#Walter1994)), teenagers' career decision making ([Julien 1999](#Julien1999)), and urban teenagers' daily life information needs ([Agosto and Hughes-Hassell 2006](#Agosto2006)). The broader and more comprehensive concept, information behaviour, has seldom been examined among youth. Information behaviour studies examining adult groups, such as Chatman's ([1996](#Chatman1996)) theory of non-users in information poverty and Pettigrew's ([1999](#Pettigrew1999)) concept of _information ground_ where people simultaneously exchange information in a social setting, have not yet been extended to the study of children's information behaviour.

## Method

The main objective of this study is to explore children's personal experiences and their interaction with information in coping with their everyday hassles. Therefore, a qualitative method was used to collect data. Both qualitative and quantitative methods were employed to analyse data in order to obtain a richer understanding of this phenomenon.

### Survey instruments and subjects

The sensitive subject of this study does not easily lend itself to interviews. Journal keeping make it easier to ensure confidentiality and privacy and provide a safer environment for disclosure and exploration of sensitive issues related to coping. In order to elicit children's reports of everyday hassles and their coping-related information behaviour, this study employed a semi-structured journal adopted from Sorensen's ([1993](#Sorensen1993): 83) coping research. The journal contained eight questions. The first four questions were taken from Sorensen's journal items, which were to elicit the participant's daily-life worries and how he or she coped with them. The first four questions also helped set up the context for the current study by asking the participant to describe the thing that upset him that day and what he did about it. The investigator of the present study designed and added questions five to eight in order to examine how children interact or do not interact with information. The survey question protocol was tested in a pilot study and revised accordingly. Journal questions and responses were translated and back-translated between English and Chinese, the official language in Taiwan, by a native Chinese-speaking translator. Content accuracy was verified by another bilingual (Chinese/English) scholar.

Participants included 133 children, 65 girls and 68 boys, in fifth- and sixth-grade classrooms in a public elementary school in an urban community in Taiwan. Participating children kept the journal in Chinese for five days during regular school days.

## Data analysis

Both qualitative and quantitative analyses were employed to interpret data. In this study, responses about children's daily hassles were coded and analysed based on Sorensen's ([1993](#Sorensen1993): 87-89) coping conceptualization. In studying children's daily-life stress and coping, Sorensen identified sixteen categories of daily-life stressors. The daily-life stressors of children included school, home chores, interruption, organizational demands, environmental discomfort, health care visits, disappointment, physical symptoms, cognitive-emotional discomforts, personal responsibility, friends, siblings, parental demands, family concerns, school teacher, and family members. After coding children's daily life hassles, frequencies of categories were recorded to examine major trends and to enhance the descriptions.

Another method, open coding ([Strauss and Corbin 1990](#Strauss1990)), was employed to analyse responses about children's information seeking and coping because typologies about them were unavailable. During open coding '_the data are broken down into discrete parts, closely examined, compared for similarities and differences, and questions are asked about the phenomena as reflected in the data_' ([Strauss and Corbin 1990](#Strauss1990): 62). To test the reliability of the coding categories, two other scholars coded 50 randomly selected responses, respectively, based on the investigator's coding scheme that provided both definitions and examples. The intercoder reliability was 0.87 and 0.92 respectively, according to Miles and Huberman's ([1994](#Miles1994)) reliability formula. Thus, the reliability of coding process was adequate.

## Findings and discussion

The total number of daily journal responses from children was 532\. Of these responses, 257 were from girls, and 268 were from boys.

### Children's everyday hassles

Sorensen's hassle taxonomy was able to capture most of the daily-life worries children reported. There were minor modifications of the taxonomy in order to better represent the data of this study. Two categories, _self image_ and _animal/pet_, were added to exhaust data coding. Three categories in Sorensen's taxonomy were not noted among children's responses in this study. They were _health care visits_, _parental demands_ and _family members_. Descriptions of the categories follow in Table 1\. Categories of daily-life worries listing occurrence are shown in Table 2.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Children's everyday hassles**  
</caption>

<tbody>

<tr>

<th align="center">Everyday hassles</th>

<th align="center">Definition</th>

<th align="center">Examples</th>

</tr>

<tr>

<td align="center">School</td>

<td align="center">Included stressors of 'the school atmosphere in general, as well as homework, tests, school projects, special assignments, and particular school subjects' (Sorensen 1993): 87-88).</td>

<td align="center">'math class', 'English class', 'may fail in many courses', 'exam performance', etc.</td>

</tr>

<tr>

<td align="center">Home chores</td>

<td align="center">Tasks children had to help at home</td>

<td align="center">'house cleaning', 'clean up the room', etc.</td>

</tr>

<tr>

<td align="center">Interruption</td>

<td align="center">Represented the 'annoying, unpredictable interruption of plans or ongoing activities' ( Sorensen 1993: 88)</td>

<td align="center">'I wanted to watch TV, but had to practice piano'.</td>

</tr>

<tr>

<td align="center">Organizational demands</td>

<td align="center">Included 'time or task demands of scouting, sports, dancing lessons, etc'. (Sorensen 1993: 88) In this study, demands from extra-curricular activities outside of school such as piano lessons were placed under this category.</td>

<td align="center">'piano lesson', 'jogging', 'swimming lesson', etc.</td>

</tr>

<tr>

<td align="center">Environmental discomfort</td>

<td align="center">Referred to 'annoyances such as weather conditions or particular foods' (Sorensen 1993: 88).</td>

<td align="center">'it's raining', 'it is so hot today', why do I have to stand under the sun for the whole ceremony?'</td>

</tr>

<tr>

<td align="center">Disappointment</td>

<td align="center">Included 'apparent internal threats to the child's plans, desires, and hopes, and also disappointments in personal performance'.</td>

<td align="center">'I didn't get into a group with my good friend', 'I'm bad at playing computer games', I did a lousy job in art craft', 'my brother's year book is bigger than mine!', etc.</td>

</tr>

<tr>

<td align="center">Physical symptoms</td>

<td align="center">Included 'injuries, illness, or other physical complaints such as fatigue or hunger' (Sorensen 1993: 88).</td>

<td align="center">'My knees hurt', 'I got a cold', 'I need more sleep', etc.</td>

</tr>

<tr>

<td align="center">Self image</td>

<td align="center">Referred to physical appearance as a source of children's worries</td>

<td align="center">'my height/weight', 'what to wear to the party', 'I have to put on brace!', etc.</td>

</tr>

<tr>

<td align="center">Cognitive-emotional discomforts</td>

<td align="center">'Cognitive-emotional discomforts' indicated specific fears, feelings of lack of control, a sense of injustice, and unidentifiable unrest or boredom (Sorensen 1993: 88-89).</td>

<td align="center">'I have to fly alone back home', 'I'm worried about my future', 'I hate to do the same things every day', etc.</td>

</tr>

<tr>

<td align="center">Personal responsibility</td>

<td align="center">Focused on 'the child's sense of disappointment in self' (Sorensen 1993: 89). It is different from the 'Disappointment' category in that this category takes 'responsibility' to the degree of 'blame' or even 'guilt'.</td>

<td align="center">'I'm so dumb that I forgot to bring my swimming gears', 'I don't know why I write so slowly', I lost my exam sheet!', etc.</td>

</tr>

<tr>

<td align="center">Friends</td>

<td align="center">Contained descriptions of the 'bothersome behaviour of friends, as well as concerns in the relationship' (Sorensen 1993: 89).</td>

<td align="center">'friendship', 'My friend betrayed me', 'my friend spoke sill behind me', etc.</td>

</tr>

<tr>

<td align="center">Family concerns</td>

<td align="center">Focused largely on family relationships and illness of family members.</td>

<td align="center">'dad's illness', 'dad fought with mom', 'my brother's school performance', etc.</td>

</tr>

<tr>

<td align="center">School teacher</td>

<td align="center">The relationship with the teacher</td>

<td align="center">'One of my teachers is retiring', 'the teacher got mad', etc.</td>

</tr>

<tr>

<td align="center">Animal</td>

<td align="center">Indicated that the source of worries was from animals or pets.</td>

<td align="center">'My dog'.</td>

</tr>

</tbody>

</table>

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Children's everyday hassles: by type**  
<span style="font-size: small;">*Total not equal to 100.00 due to rounding error.</span></caption>

<tbody>

<tr>

<th>Hassle</th>

<th>Total (%)</th>

</tr>

<tr>

<td align="center">School</td>

<td align="center">45.3</td>

</tr>

<tr>

<td align="center">Home chores</td>

<td align="center">0.13</td>

</tr>

<tr>

<td align="center">Interruption</td>

<td align="center">0.25</td>

</tr>

<tr>

<td align="center">Organizational demands</td>

<td align="center">1</td>

</tr>

<tr>

<td align="center">Environmental discomfort</td>

<td align="center">3.76</td>

</tr>

<tr>

<td align="center">Disappointment</td>

<td align="center">22.71</td>

</tr>

<tr>

<td align="center">Physical symptoms</td>

<td align="center">3.51</td>

</tr>

<tr>

<td align="center">Self image</td>

<td align="center">0.5</td>

</tr>

<tr>

<td align="center">Cognitive-emotional discomforts</td>

<td align="center">8.16</td>

</tr>

<tr>

<td align="center">Personal responsibility</td>

<td align="center">5.27</td>

</tr>

<tr>

<td align="center">Friends</td>

<td align="center">6.52</td>

</tr>

<tr>

<td align="center">Siblings</td>

<td align="center">1.13</td>

</tr>

<tr>

<td align="center">Family concerns</td>

<td align="center">1.26</td>

</tr>

<tr>

<td align="center">School teacher</td>

<td align="center">0.38</td>

</tr>

<tr>

<td align="center">Animal</td>

<td align="center">0.13</td>

</tr>

<tr>

<td align="center">Total*</td>

<td align="center">100.01</td>

</tr>

</tbody>

</table>

Interestingly, school-related hassles were also most frequently reported (45.3%) among these children. These children worried about their academic performance in general, their test scores in specific courses such as math and English (which is a foreign language in Taiwan), the homework assignments, and the variety or talent show in school. It seemed to indicate that school is a global stressor that most, if not all, children experience.

Unlike their counterparts in Anglophonic cultures, who viewed interpersonal issues as one of the top daily hassles, children in this study seemed to be more comfortable with interpersonal issues among friends and siblings. Only 6.52% of the respondents reported having troubles with friends, and sibling concerns were even lower (1.13%). In contrast, what troubled them more was disappointment of some plans or hopes, as well as disappointment in self performance. In this study, more than 22% of children expressed different types of disappointment. Some examples included '_I didn't get into the same group with my best friends in the fieldtrip'_, '_we lost a ball game today'_, '_I didn't have enough money to buy gifts for my brother'_, '_I did a lousy job in my art craft'_, etc. It's not clear why children in this study had a higher level of disappointment. Perhaps they were more anxious about or had higher expectation towards certain things, or perhaps it is culturally situated.

### Information-seeking and children's everyday hassles

Twenty eight responses reported information-seeking to cope with daily life during the survey period. Among these cases, three looked for information from text materials such as the internet and the newspaper; the remainder sought for instrumental advice or information from interpersonal sources. (Table 3)

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Cases of information seeking**  
</caption>

<tbody>

<tr>

<th>What upset me today was.</th>

<th>so I.</th>

<th>Total</th>

</tr>

<tr>

<td>how to find a summer job</td>

<td>went online to look for some information.</td>

<td align="center">1</td>

</tr>

<tr>

<td>where to go in summer</td>

<td>looked for some websites.</td>

<td align="center">1</td>

</tr>

<tr>

<td>today's weather</td>

<td>looked it up in the newspaper.</td>

<td align="center">1</td>

</tr>

<tr>

<td>what to dress in the party</td>

<td>talked to my friend and got some ideas.</td>

<td align="center">1</td>

</tr>

<tr>

<td>myself</td>

<td>I asked for advice from others.</td>

<td align="center">1</td>

</tr>

<tr>

<td>romantic feelings</td>

<td>looked for advice from friends.</td>

<td align="center">1</td>

</tr>

<tr>

<td>what to do in the party</td>

<td>kept asking my friends for advice.</td>

<td align="center">2</td>

</tr>

<tr>

<td>homework assignment(s)</td>

<td>asked family members/friends for advice.</td>

<td align="center">20</td>

</tr>

</tbody>

</table>

It was not surprising to find that most of the cases (20 out of 28) have to do with homework assignments, which were a major hassle to the participating children. Interestingly, all such cases referred to interpersonal advice seeking, instead of factual type of information seeking. This may be because of the types of homework assignments they have, or because they shared particular learning styles.

## Information behaviour in the everyday context

The low occurrence of active information seeking in this study seemed to indicate that information played a very limited role when children dealt with their everyday hassles. However, the analysis of children's rationales behind their actions (i.e., '_When I feel upset, I would... because..._') unveiled a rich body of various information behaviour. Four major information behaviour emerged during the course of analysis.

1.  _Goal-driven information seeking._ This type of information-seeking is oriented to achieve a certain goal and includes four sub-types.
    *   First, _Information seeking for problem solving._ In some cases, children would actively seek particular information or advice from books or from the Internet to solve problems. It resembled Savolenein's everyday life information seeking in that children sought various types of information to solve their daily-life problems and to keep their daily-life activities in order. For instance, a girl in this study wrote,

        > [_I would seek information when I feel upset because] I would look for specific books to read because after reading I usually have different thoughts... Last time when my best friend stopped talking to me, I found a story that had a similar situation, which offered many different solutions. That's why I like going to the library when I feel bad. Reading gives me different thoughts, which help me think about what to do to solve the problem._

    *   Secondly, _Information seeking for escape._ Some children seek and read a variety of materials in order to escape from or to forget worries. The goal of such information seeking was simply not to face or think about the problems at all. In other words, they were seeking information about a totally different subject in order to avoid thinking about the problems that bothered them. One example was from a boy, who wrote '_I would look for particular comic books to make me laugh. Then I would forget my worries!_' In such cases, the goal of the information seeking was simply to not to face or think about the problems at all.
    *   Thirdly, _Information seeking for a transition._ Instead of a complete escape, children sometimes seek information, oftentimes through reading books, in order to temporarily disengage themselves from the current worries with an apparent goal to cognition restructuring. The strategy serves as a transition or a bridge to potential problem solving. Children in these cases reported using reading to calm them down so that they may face the problem later. A typical example is from a boy, who wrote '_I would find some books to read to calm me down. I need a break so that I can think later_'
    *   Finally, _Information seeking to change mood._ This type of information seeking is not necessarily related to problem solving, but information is sought and used to change mood or feel better. For instance, 'Reading [a specific book] makes me happy, and I feel relaxed and less upset after reading it'.
2.  _Emotion-driven information seeking._ The act of information seeking is motivated by emotions, especially negative ones or feelings of helplessness. No specific information-seeking goals are identified in these cases; children seek information because they feel troubled and helpless. As a boy described in his journal, '_I'm just upset... I don't know what else to do, so I'll try anything_'. However, it is unclear what the boy sought and what goals, if any, he would like to achieve through information seeking.
3.  _Efficiency-driven information seeking._ The act of information seeking is initiated by efficiency judgment. Children, in this case, seek information to cope with daily-life problems because '_it [information-seeking] is convenient, fast, and easy_'. Again, no specific goals or emotions are identified here.
4.  _Information avoidance._ In dealing with daily-life hassles, sometimes children do not want to seek and/or use information at all for a variety of reasons. Some major reasons include personal perceptions of information, social constraints, or personal preference over other coping methods. For children who perceive information as useless and unhelpful, their purposeful information avoidance is similar to Miller's ([1987](#Miller1987): 345) _information blunting_ in clinical treatment, where some cancer patients choose not to deal with threatening information. One example was from a girl who experienced a big earthquake:  

    > It [Information] is useless. It wouldn't erase my old memory... I experienced a big earthquake when I was little and almost got buried by the collapsing building. Would information keep the bad memory away from me? I don't think so.

    Information in these cases was perceived as being unhelpful, or even harmful, to the participants, and so these children chose to keep them away from relevant information. In other cases, children may not want to seek and use information to deal with their hassles because of their personal preference over other coping methods. For example, as a boy described, '_I would prefer running and wearing myself out. I don't want to think about the problem at all_'. Still, children's information avoidance could result from social constraints. They did not want to seek information because they did not have access to desirable information and they were not allowed to go online. Some examples include '_We don't have money to buy a computer_', '_I can't get to the library_', and '_My mom doesn't allow me to use the Internet_'.

These four types of information behaviour together point out that information plays different roles in this particular coping-with-daily-life context; it is more than problem-solving as was discussed in literature. Apparently, in coping with daily-life issues, information-seeking serves a much more complicated and sophisticated role than was understood. Problem-solving is not, and should not be, the only component in addressing information seeking and coping.

## Conclusion and implications

This study is unique in that it is one of the first to examine children's daily hassles and relevant information behaviour in this context, and in that it goes beyond the typical stress and coping investigation scheme to examine children's interaction with information in coping with their everyday hassles and the rationale behind their information-related strategies, which provide a more meaningful context for understanding their information needs in their daily lives.

Findings of this study revealed a wide range of children's goals in coping with their daily life, from confrontation (e.g., problem solving) to escape or denial. Because children aimed at different goals, the type of information they needed varied. When a child looked for information to solve his problem, information on that specific topic was what he needed. This is the typical problem-need equation seen in many library and information science studies. However, problem confrontation was not always the child's goal, and so information about that topic was not necessarily what the child needed. When a child wanted to calm down first and face the problem later, the information he needed could be anything that attracted his attention at that moment. When a child wanted to escape from the worry, the information he needed would be anything but that topic.

Children who seek information because it is seen as fast and convenient seem to belong to those who know what their needs or goals are. In contrast, children whose information-seeking is emotion-driven seemed to have very vague or even unspecified information needs. They might be aware of their problems, but did not seem to develop a focus yet for information seeking. Perhaps they had the _visceral_ or _conscious_ need described by Taylor ([1968](#Taylor1968)) or the _anomalous state of knowledge_ proposed by Belkin _et al._ ([1980](#Belkin1980)), and they were exploring their needs through information seeking. After, or during the course of information seeking, they may develop a focus or a goal.

These behaviour show that children's information needs in coping with daily hassles are not always subject- or topic-based. Subject- or topic-based information concepts seem to be helpful only to those who seek information for problem-solving. For children who seek information to escape or to find a transition and for children who seek information because they are too confused or bored, there are no specific subjects or topics that match their needs. In other words, the problems or issues that bothered or upset them may not always be the topics or needs of their information seeking. How to provide information to children who want to escape, who need a break, or who are feeling clueless needs to be explored.

Another issue emerged from the findings is the need of information behaviour theory for children. Current coping and information seeking theories, established from studies on adults, are unable to capture and explain the various kinds of information behaviour children exhibited in this study. Children's information behaviour goes beyond problem-focused as understood by psychology scholars but, unfortunately, results of this study point out that such information seeking is too narrow to understand children's information behaviour in this particular coping context. This study also showed that children do not always need to solve the problems in order to master their lives. Children in this study did seek information to keep their lives in order; although they sought information for various reasons, problem-solving was only one of them. This indicates that the boundaries of everyday life information seeking need extending, particularly for different developmental groups.

Lastly, there is a need to replicate the study with different populations. Some potential studies might include efforts to increase diversity and representativeness, including different cultural backgrounds and age-groups. Because the seeking and use of information can be culturally situated ([Hofstede 1980](#Hofstede1980)), children in Taiwan may exhibit different information behaviour from their Anglophonic counterparts. A cross-cultural comparison could assess how, and if, children with different cultural backgrounds choose alternative paths in coping with their everyday hassles. By conducting additional research and extending the findings of this study, and by helping children develop the information seeking skills that they should have, giving them the flexibility of non problem-focused information seeking, and understanding their need of information avoidance, information providers can do a great deal to assist children in their personal growth and in their well-being.

## Acknowledgements

This project is partially supported by SIGUSE Elfreda Chatman Research Proposal Award.

## About the author

Ya-Ling Lu received her Ph.D. from the department of Information Studies at University of California, Los Angeles. She is currently an assistant professor in the department of library and information science, Rutgers University. Her research interest mainly focuses on how children's materials are selected, provided, and used to meet children's diverse information needs in differing social and education institutions. She can be contacted at [yalinglu@rci.rutgers.edu](mailt:yalinglu@rci.rutgers.edu).

#### References

*   Agosto, D. E., & Hughes-Hassell, S. (2006). Towards a model of everyday life information needs of urban teenagers. Part 2: Empirical model. _Journal of the American Society for Information Science and Technology,_ **57**(11), 1418-1426.
*   Anda, D. de, Baroni, S., Boskin, L., Buchwald, L., Morgan, J., Ow, J., _et al._ (2000). Stress, stressors and coping strategies among high school students. _Children and Youth Services Review,_ **22**(6), 441-463.
*   Belkin, N.J., Oddy, R. N. & Brooks, H. M. (1980). Anomalous states of knowledge as a basis for information retrieval. _Canadian Journal of Information Science_, **38**(2), 133-143.
*   Bilal, D. (2005). Children's information seeking and the design of digital interfaces in the affective paradigm. _Library Trends,_ **54**, 197-208.
*   Bobo, J.K., Gilchrist, L.D., Elmer, J.F., Snow, W.H. & Schinke, S.P. (1986). Hassles, role strain, and peer relations in young adolescents. Journal of Early adolescence, **6**(4), 339-352.
*   Chatman, E. A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science,_ **47**(3), 193-206.
*   Compas, B. E., Malcarne, V. L. & Fondacaro, K. M. (1988). Coping with stressful events in older children and young adolescents. _Journal of Consulting and Clinical Psychology,_ **56**(3), 405-411.
*   Compas, B.E., Connor, J., Osowieeki, D. & Welsh, A. (1997). Effortful and involuntary responses to stress: implications for coping with chronic stress. In B. H. Gottlieb (Ed.), _Coping with chronic stress_ (pp. 105-30). New York, NY: Plenum Press.
*   Curry, S. L. & Russ, S. W. (1985). Identifying coping strategies in children. _Journal of Clinical Child Psychology_, **14**(1), 61-69.
*   Donaldson, D., Prinstein, M., Danovsky, M. & Spirito, A. (2000). Patterns of children's coping with life stress: implications for clinicians. _American Journal of Orthopsychiatry,_ **70**(3), 351-359.
*   Duncan M.K. & Sanger, M. (2004). Coping with the pediatric anogenital exam. _Journal of Child and Adolescent Psychiatric Nursing,_ **17**(3), 126-36.
*   Eisenberg, N., Fabes R. A. & Guthrie, I. K. (1997). Coping with stress: the roles of regulation and development. In S. A. Wolchik & I. N. Sandler (Eds.), _Handbook of children's coping: linking theory and intervention_ (pp. 41-70). New York, NY: Plenum Press.
*   Fenzel, L.M. (1989). Role strains in early adolescence: a model for investigating school transition stress. Journal of Early Adolescence, **9**(1), 13-33.
*   Gross, M. (1995). The imposed query. _RQ_, **35**(2), 126-43.
*   Hirsh, S.G. (1999). Children's relevance criteria and information seeking on electronic resources. _Journal of the American Society for Information Science,_ ,**50**(14), 1265-1283.
*   Hodgins, M.J. & Lander, J. (1997). Children's coping with venipuncture. _Journal of Pain and Symptom Management,_ (5)**13**, 274-285.
*   Hofstede, G. (1980). _Culture's consequences: international differences in work-related values._ Beverly Hills, CA: Sage Publications, Inc.
*   Hubert, N.C., Jay, S. M., Saltoun, M. & Hayes, M. (1988). Approach-avoidance and distress in children undergoing preparation for painful medical procedures. _Journal of Clinical Child Psychology,_ **17**(3), 194-202.
*   Julien, H. (1999). Barriers to adolescents' information seeking for career decision making. _Journal of the American Society for Information Science,_ **50**(1): 38-48.
*   Kafai, Y. & Bates, M. J. (1997). Internet web searching instruction in the elementary classroom: building a foundation for information literacy. _School Library Media Quarterly,_ **25**(2), 103-111.
*   Kanner, A. D., Coyne, J. C., Schaefer, D. & Lazarus, R. S. (1981). Comparison of two modes of stress measurement: daily hassles and uplifts versus major life events. _Journal of behavioural Medicine,_ **4**(1), 1-39.
*   Kuhlthau, C. C. (1988). Meeting the information needs of children and young adults: Basing library media programs of developmental states. _Journal of Youth Services in Libraries,_ **2**(1), 51-57.
*   LaMontagne, L.L., Johnson, J.E., Hepworth, J.T. & Johnson, B.D. (1997). Attention, coping, and activity in children undergoing orthopaedic surgery. _Research in Nursing & Health,_ **20**(6), 487-94.
*   Large, A. & Beheshti, J. (2000). The web as a classroom resource: reactions from the users. _Journal of the American Society for Information Science,_ **51**(12), 1069-1080.
*   Last, B. F., Stam, H., Onland-van Nieuwenhuizena, A-M. & Grootenhuis, M. A. (2007). Positive effects of a psycho-educational group intervention for children with a chronic disease: first results. _Patient Education and Counseling,_ **65**(1), 101-112.
*   Lazarus, R. S. (1991) Introduction: Stress and coping-some current issues and controversies. In A. Monat & R. S. Lazarus (Eds.), _Stress and coping: an anthology_ (pp. 1-16). New York, NY: Columbia University Press.
*   Lazarus, R.S. (1993). Coping theory and research: past, present, and future. _Psychosomatic Medicine_, **55**(3), 234-247.
*   Lazarus, R.S. & Folkman, S. (1984). _Stress, appraisal and coping_. New York, NY: Springer.
*   Lazebnik, R., Zimet, G.D., Ebert, J., Anglin, T.M., Williams, P., Bunch, D. L. _et al._ (1994). How children perceive the medical evaluation for suspected sexual abuse. _Child Abuse & Neglect,_ **18**(9), 739-745.
*   Leung, L. (2007). Stressful life events, motives for internet use, and social support among digital kids. _Cyberpsychology & behaviour,_ **10**(2), 204-214.
*   Lu, Y.-L. (2008). Coping assistance vs. readers' advisory: are they the same animal? _Children & Libraries: The Journal of the Association for Services to Children,_ **6**(1), 15-22.
*   Marchionini, G. (1989). Information-seeking strategies of novices using a full-text electronic encyclopedia. _Journal of the American Society for Information Science,_ **40**(1), 54-66.
*   Melamed, B.G., Yurcheson, R., Fleece, E.L., Hutcherson, S. & Hawes, R. (1978). Effects of film modeling on the reduction of anxiety-related behaviour in individuals varying in level of previous experience in the stress situation. _Journal of Consulting and Clinical Psychology,_ **46**(6), 1357-1367.
*   Miles, M.B. & Huberman, A.M. (1994). _Qualitative data analysis_ (2nd ed.). Thousand Oaks, CA: Sage Publications Inc.
*   Miller, S.M. (1987). Monitoring and blunting: validation of a questionnaire to assess styles of information seeking under threat. _Journal of Personality and Social Psychology_ **52**(2), 345-353.
*   Miller, S.M., and Green, M.L. (1985). Coping with stress and frustration: origins, nature and development. In M. Lewis and C. Saarni (Eds.) _The socialization of emotions_ (pp. 263-314). New York, NY: Plenum.
*   Perkins, H.V. (1974). _Human development and learning_. Belmont, CA: Wadsworth Publishing.
*   Peterson, L. (1989). Coping by children undergoing stressful medical procedures: some conceptual, methodological and therapeutic issues. _Journal of Consulting and Clinical Psychology_, **57**(3), 380-387.
*   Pettigrew, K. E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behaviour among attendees at community clinics. _Information Processing and Management_, **35**(5), 801-817.
*   Poston-Anderson, B. & Edwards, S. (1993). The role of information in helping adolescent girls with their life concerns. _School Library Media Quarterly,_ **22**(1), 1-6.
*   Roth, S. & Cohen, L.J. (1986). Approach, avoidance, and coping with stress. _American Psychologist,_ **41**(7), 813-819.
*   Ryan-Wenger, N. M. (1992). A taxonomy of children's coping strategies: a step toward theory development. _American Journal of Orthopsychiatry,_ **62**(2), 256-262.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of 'Way of life'. _library and information science Research_, **17**(3), 259-294.
*   Seiffge-Krenke, I. (1995). _Stress, coping, and relationships in adolescence._ Mahwah, NJ: Lawrence Erlbaum Associates, Inc.
*   Skinner, E., Edge, K., Altman, J. & Sherwood, H. (2003). Searching for the structure of coping: a review and critique of category systems for classifying ways of coping. _Psychological Bulletin_, **129**, 216-269.
*   Skinner, E. A. & Melanie J. Zimmer-Gembeck. (2007). The development of coping. _Annual Review of Psychology_, **58**, 119-144.
*   Solomon, P. (1993). Children's information retrieval behavior: a case analysis of an OPAC. _Journal of the American Society for Information Science,_ **44**(5), 245-264.
*   Sorensen, E.S. (1993). _Children's stress and coping: a family perspective._ New York, NY: The Guilford Press.
*   Strauss, A. & Corbin, J. (1990). _Basics of qualitative research: grounded theory procedures and techniques._ Newbury Park, CA: Sage Publications, Inc.
*   Taylor, R.S. (1968). Question negotiation and information seeking in libraries. _College & Research Libraries,_ (No. 29), 178-194.
*   Todd, R.J. (1999). Utilization of heroin information by adolescent girls in Australia: a cognitive analysis. _Journal of the American Society for Information Science,_ **50**(1), 10-23.
*   Walter, V.A. (1994). The information needs of children. _Advances in Librarianship,_ **18**, 111-129.
*   Wilson, T.D. (2000). Human information behaviour. _Informing Science,_ **3**(2), 49-55.