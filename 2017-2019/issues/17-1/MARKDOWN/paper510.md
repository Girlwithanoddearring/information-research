#### vol. 17 no. 1, March 2012

# Internet corporate reporting by listed firms in Hong Kong

#### [Pak-Lok Poon](#authors)  
School of Accounting and Finance, The Hong Kong Polytechnic University, Hung Hom, Hong Kong  
[Yuen Tak Yu](#authors)  
Department of Computer Science, City University of Hong Kong, Kowloon Tong, Hong Kong

#### Abstract

> **Introduction.** This paper studies the relationships between the degree of Internet corporate reporting and the characteristics of Hong Kong's listed firms which use this innovation.  
> **Method.** Applying our previously developed research framework, we performed an empirical study by adapting the research questions to the local context, identifying proxies for measuring the variables in the formulated hypotheses, and collecting two rounds of data over a three-year period for hypothesis testing.  
> **Analysis.** We analysed the data quantitatively, identified and visualised the patterns of data, and performed statistical tests to validate the formulated hypotheses.  
> **Results.** We found that the degree of a firm's Internet corporate reporting is positively associated with several of its characteristics, including the level of corporate governance and previous experience of adopting such a reporting practice. Moreover, the relationships between the degree of Internet corporate reporting and a firm's size, revenue and stock trading activity are very similar and exhibit a distinctive pattern.  
> **Conclusions.** Clear relationships exist between several characteristics of a firm and its degree of Internet corporate reporting. Our previously developed research framework can be adapted to study such a reporting practice in other regions or local contexts.

## Introduction

_Internet corporate reporting_ has emerged since the last decade ([Poon _et al._ 2003](#poo03); [Wong and Poon 2008](#won08); [Poon and Yu 2012](#poo12)). In this model, firms use the Web to disseminate up-to-date corporate information to a global audience ([Wong and Poon 2008](#won08)). Corporate information can be financial (such as share price) or non-financial (such as organisational profile). As financial information may influence investment decisions ([Holder-Webb _et al._ 2008](#hol08)), _Internet financial reporting_ is almost invariably an integral part of Internet corporate reporting ([Poon _et al._ 2003](#poo03)).

Hong Kong is a prominent financial centre in the Asia-Pacific region. In the past decade, there has been an influx of mainland China-incorporated companies being traded at the Hong Kong Exchanges and Clearing Limited (or the _Hong Kong Stock Exchange_) under the guise of the _H-share_. These offshore companies are called _H-share firms_ (for example, China Construction Bank Corporation). Other companies listed in the Hong Kong Stock Exchange are called _non-H-share firms_ (for example, Cathay Pacific Airways Limited). Some H-share firms have established their subsidiaries or representative offices in Hong Kong and have implemented corporate Websites for information dissemination, thus further contributing to the growth of Internet corporate reporting in Hong Kong.

We expect that many firms in Hong Kong have corporate Websites and, hence, are _Web-present_. Moreover, we postulate that many Web-present firms use Internet corporate/financial reporting to their benefit. To provide a clearer picture of the degree of Internet corporate reporting by firms in Hong Kong, we performed two rounds of studies within a three-year period. We define the _degree of Internet corporate reporting_ in terms of the _richness_ of the corporate information disclosed on the Websites. This was measured by the quantity of online corporate information categories. Our premise is that the _richness_ of corporate information disclosed on a firm's Website reflects the firm's commitment, effort, resources and level of strategic significance attached to Internet corporate reporting.

We performed two rounds of studies within a three-year period, analysing 200 firms in each round. The firms originated from two different groups of regions (mainland China and other locations) and showed a distinct difference in their level of corporate governance. Whereas Internet financial reporting has been studied in other regions, our work is unique in studying listed firms in Hong Kong and in considering the broader context of Internet corporate reporting. (Internet corporate reporting also covers Internet financial reporting in our analysis as long as financial information is relevant to the firm's characteristic under discussion.)

Our study shows that firms increasingly adopted Internet corporate reporting over the three-year study period, regardless of their origin. The degree of such reporting was generally high. In today's dynamic and competitive business environment, Internet corporate reporting is no longer a 'nice-to-have' option, but rather an essential and affordable one that any firm should implement for information dissemination.

This empirical study also shows how the research framework developed in Part 1 of this study ([Poon and Yu 2012](#poo12)) can be adapted to study the adoption of Internet corporate reporting in a local context.

## Research framework

Our research framework is grounded on the _diffusion of innovation theory_ ([Baskerville and Pries-Heje 2001](#bas01); [Carter _et al._ 2001](#car01); [Moore and Benbasat 1991](#moo91); [Rogers 2003](#rog03)), the _expectation-confirmation theory_ ([Bhattacherjee 2001](#bha01); [Chea and Luo 2008](#che08); [Lin _et al._ 2005](#lin05)), _corporate governance theories_ ([Claessens 2006](#cla06); [Cohen _et al._ 2004](#coh04)) and the _learning/experience curve theories_ ([Chang 1996](#cha96); [Henderson 1973](#hen73); [Lieberman 1987](#lie87)).

The diffusion of innovation theory states that individuals exhibit different degrees of willingness to adopt _innovations_, which can be an idea, practice or object that is perceived as new. The rate of adopting an innovation is influenced by several determinants, such as _relative advantage_ (the degree to which an innovation is perceived to be better than its precursor) and _trialability_ (the degree to which an innovation may be experimented with before adoption).

The expectation-confirmation theory is widely used to study customer satisfaction and post-purchase behaviour. It states that consumers have expectations of a product prior to purchase. If the product performs better than expected, customer satisfaction with the product increases. On the contrary, if the product performs less well than expected, customers are less likely to purchase the product in the future.

Corporate governance theories postulate a positive relationship between _corporate governance_ (a mechanism to ensure that a corporation conducts its business in accordance with corporate laws and regulations) and voluntary dissemination of company information. Thus, a firm with weak corporate governance is often less transparent in operations and, hence, is less inclined to voluntarily disclose company information to outsiders.

Finally, the learning/experience curve theories state that the more frequently a task is performed, the lower its cost. Therefore, firms should capitalise on the learning/experience effects to reduce operations costs and increase market share, profitability and market dominance.

Our research framework is generic and therefore applicable to a variety of contexts. It poses several research questions related to the adoption of Internet corporate reporting. The research questions were further refined into a set of hypotheses. Our first paper ([Poon and Yu 2012](#poo12)) has discussed these theories in detail and explained how they led to the research framework. In this paper, we adapted the framework to the local context for an empirical study of the degree of Internet corporate reporting in listed firms in Hong Kong. Modifications in our research questions are highlighted by words in italics, while the hypotheses are reproduced from our previous paper for application to the context of listed firms in Hong Kong.

### Research questions

*   **(RQ1)** How popular was Internet corporate reporting _among listed firms in Hong Kong_?
*   **(RQ2)** What were the degrees of Internet corporate reporting among the Web-present _listed firms in Hong Kong_?
*   **(RQ3)** Was the degree of Internet corporate reporting related to the following characteristics of _listed firms in Hong Kong_: (a) corporate governance, (b) system openness, (c) size, (d) revenue, (e) stock trading activity and (f) previous experience of adopting this reporting practice?

### Research hypotheses (in the context of listed firms in Hong Kong)

*   **_H1_** [degree of Internet corporate reporting]: The degree of Internet corporate reporting is generally high among Web-present firms.
*   **_H2a_** [corporate governance![arrow](rarr.png)Web presence]: _Firms with strong corporate governance are more likely to be Web-present._  
    **_H2b_** [corporate governance![arrow](rarr.png)degree of Internet corporate reporting]: _The level of corporate governance of a Web-present firm is positively associated with its degree of Internet corporate reporting._
*   **_H3a_** [system openness![arrow](rarr.png)Web presence]: _Firms with a higher level of system openness are more likely to be Web-present._  
    **_H3b_** [system openness![arrow](rarr.png)degree of Internet corporate reporting]: _The level of system openness of a Web-present firm is positively associated with its degree of Internet corporate reporting._
*   **_H4a_** [size![arrow](rarr.png)Web presence]: _Larger firms are more likely to be Web-present._  
    **_H4b_** [size![arrow](rarr.png)degree of Internet corporate reporting]: _The size of a Web-present firm is positively associated with its degree of Internet corporate reporting._
*   **_H5a_** [revenue![arrow](rarr.png)Web presence]: _Firms with higher revenues are more likely to be Web-present._  
    **_H5b_** [revenue![arrow](rarr.png)degree of Internet corporate reporting]: _The revenue of a Web-present firm is positively associated with its degree of Internet corporate reporting._
*   **_H6a_** [stock trading activity![arrow](rarr.png)Web presence]: _Firms with higher stock trading activities are more likely to be Web-present._  
    **_H6b_** [stock trading activity![arrow](rarr.png)degree of Internet corporate reporting]: _The level of stock trading activity of a Web-present firm is positively associated with its degree of Internet corporate reporting._
*   **_H7_** [experience of adoption![arrow](rarr.png)degree of Internet corporate reporting]: _Previous experience of adopting Internet corporate reporting of a Web-present firm is positively associated with the degree of such reporting._

Figure 1 summarises the research model that incorporates the major potential determinants affecting the degree of Internet corporate reporting.

<div align="center">![Figure 1\. Research model of degree of Internet corporate reporting](p510fig1.jpg)</div>

<div align="center">  
**Figure 1: Research model of degree of Internet corporate reporting ([Poon and Yu 2012](#poo12))**</div>

## Empirical setting

The empirical study tests the hypotheses formulated in the previous section. This section reports the data collection process and describes the development of the operational measures for the variables in the hypotheses.

### Data collection

We tested our hypotheses with data collected from all the firms listed in the Hong Kong Stock Exchange which constitute the Hang Seng Composite Index Series (or _the Index Series_). The Index Series is composed of the top 200 listed firms in terms of market capitalisation. These firms collectively account for over 80% of the total capitalisation of the stocks in Hong Kong. They thus form a significant and representative data set.

Data collection was performed during August-October 2004 and October-November 2007\. Comparing the two data sets, we found that seventy listed firms that appeared in the Index Series in the first round were replaced by a different set of seventy firms in the second round, as changes in these firms' financial performance had led to a change of the composition of the Index Series.

In each round, we first verified the existence of the firms' corporate Websites. We phoned the firm directly to confirm the absence of its Website if the latter could not be located.

### Measurement development

The following proxy variables are used to test our hypotheses.

_H1_ [degree of Internet corporate reporting]

Web presence can be directly determined by the existence (or absence) of a Website. No proxy was therefore needed here.

For each Web-present firm, we analysed its corporate Website in detail. We classified the online corporate information into two types, financial and non-financial, as shown in Table 1\. The table was developed from e-commerce literature, investor relations literature and online visits to corporate Websites.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Online corporate information categories.**</caption>

<tbody>

<tr>

<td colspan="2">**Scoring rule**: Count one for each of the following categories present on the corporate Website; zero otherwise.  
**Degree of Internet corporate reporting** (<var>CI</var>) = Total score of both online financial and non-financial categories.  
**Degree of Internet financial reporting** (<var>FI</var>) = Total score of online financial categories only.</td>

</tr>

<tr>

<td colspan="2">**Online non-financial categories**:</td>

</tr>

<tr>

<td>1\. Organisational profile</td>

<td>12\. Monitoring and control</td>

</tr>

<tr>

<td>2\. Organisational information</td>

<td>13\. Dividend announcement</td>

</tr>

<tr>

<td>3\. Organisational news</td>

<td>14\. Notice of annual general meeting</td>

</tr>

<tr>

<td>4\. Directors' information</td>

<td>15\. List of properties</td>

</tr>

<tr>

<td>5\. Employment information</td>

<td>16\. Business review</td>

</tr>

<tr>

<td>6\. Contact information</td>

<td>17\. Environment</td>

</tr>

<tr>

<td>7\. Product/service information</td>

<td>18\. Energy</td>

</tr>

<tr>

<td>8\. Chairman's statement</td>

<td>19\. Human resources and management</td>

</tr>

<tr>

<td>9\. Chief executive's review</td>

<td>20\. Products and customers</td>

</tr>

<tr>

<td>10\. Directors' report</td>

<td>21\. Community</td>

</tr>

<tr>

<td>11\. Audit committee</td>

<td></td>

</tr>

<tr>

<td colspan="2">**Online financial categories:**</td>

</tr>

<tr>

<td>22\. Profit and loss account</td>

<td>29\. Multiple currencies</td>

</tr>

<tr>

<td>23\. Balance sheet</td>

<td>30\. Segmental analysis</td>

</tr>

<tr>

<td>24\. Cash flow statement</td>

<td>31\. Share price</td>

</tr>

<tr>

<td>25\. Notes to accounts</td>

<td>32\. Share price analysis</td>

</tr>

<tr>

<td>26\. Financial ratios</td>

<td>33\. Analysis of shareholdings</td>

</tr>

<tr>

<td>27\. Financial summary</td>

<td>34\. Auditor's report</td>

</tr>

<tr>

<td>28\. Financial highlights</td>

<td>35\. Interim report</td>

</tr>

</tbody>

</table>

We measured the _degree of Internet corporate reporting_ by the amount of online corporate information categories presented on the Website. This is denoted by the variable <var>CI</var>. The value of <var>CI</var> gives an indication of the richness of corporate information disseminated through the firm's Website. Similarly, we use the amount of online financial information categories presented on the Website, denoted by the variable <var>FI</var>, as the measure of the _degree of Internet financial reporting_. Operationally, <var>CI</var> refers to the count of both financial and non-financial information categories found in the corporate Website, while <var>FI</var> refers to the count of financial information categories only.

_H2a_ [corporate governance![arrow](rarr.png)Web presence]  
_H2b_ [corporate governance![arrow](rarr.png)degree of Internet corporate reporting]

Cheung _et al._ ([2007](#che07)) argue that Hong Kong-based companies perform better than China-related companies in terms of corporate governance. This suggests that H-share and non-H-share firms are good indicators for corporations with (relatively) weak and strong corporate governance, respectively.

_H3a_ [>system openness![arrow](rarr.png)Web presence]  
_H3b_ [system openness![arrow](rarr.png)degree of Internet corporate reporting]

System openness measures the degree to which a firm is linked to its external environment. Non-executive (independent) directors generally play a pivotal role in corporate decision-making in listed firms. The independence of non-executive directors allows them to apply their experience and competencies with fresh, objective input. Some researchers ([Agrawal and Chadha 2005](#agr05); [Baysinger and Hoskisson 1990](#bay90)) argue that the appointment of non-executive directors reflects the effectiveness of a firm in receiving and responding to ‘external' forces in its environment. Thus, the number of non-executive directors in the Board of Directors (or the _Board_) is a good proxy for system openness.

The Hong Kong Stock Exchange requires every listed firm to appoint three or more non-executive directors to its Board, regardless of the Board's size. H-share firms must also fulfil the listing rules in mainland China, which require that at least one-third of their Boards must be non-executive directors. For example, if the size of a Board is 12, then the minimum number of non-executive directors is four, which more than fulfils the minimum requirement imposed by the Hong Kong Stock Exchange.

Owing to the above statutory requirements, a listed firm does not have complete discretion in deciding the number of non-executive directors. Hence, the _percentage_ of non-executive directors in a Board is not a good proxy for system openness. Instead, we divided all listed firms into two groups. We refer to firms that fulfil above the minimum statutory requirements for non-executive directors as _Group-A firms_, and other firms as _Group-B firms_. With respect to the appointment of non-executive directors, Group-A firms have adopted a more open and proactive management style, while Group-B firms have a relatively conservative and passive style. Therefore, Group-A firms and Group-B firms are used as proxies for corporations with a higher and lower level of system openness, respectively.

_H4a_ [size![arrow](rarr.png)Web presence]  
_H4b_ [size![arrow](rarr.png)degree of Internet corporate reporting]

_H5a_ [revenue![arrow](rarr.png)Web presence]  
_H5b_ [revenue![arrow](rarr.png)degree of Internet corporate reporting]

_H6a_ [stock trading activity![arrow](rarr.png)Web presence]  
_H6b_ [stock trading activity![arrow](rarr.png)degree of Internet corporate reporting]

Market capitalisation, net profit and stock turnover are often used as proxies for a firm's size, revenue and stock trading activity, respectively. We followed this practice when testing H4a, H4b, H5a, H5b, H6a and H6b. All the measurement information came directly from the Hong Kong Stock Exchange and, therefore, is reliable and free of bias.

_H7_ [experience of adoption![arrow](rarr.png)degree of Internet corporate reporting]

To test H7, we divided the firms into two groups: _less experienced_ and _more experienced_ adopters of Internet corporate reporting. For this purpose, we considered only the firms which were Web-present in both 2004 and 2007\. Suppose Firm-A is one such firm. Then Firm-A in 2004 was obviously less experienced as an adopter of Internet corporate reporting than the same firm in 2007\. By comparing the degrees of Internet corporate reporting of each firm in 2004 and 2007, we could test statistically whether H7 holds. For easy reference, we called these firms in 2004 _Group-1 firms_ (which were less experienced in adopting Internet corporate reporting) and the same firms in 2007 as _Group-2 firms_ (which were then more experienced). Even though the two groups consist of the same firms, they were actually at different _states_ (in terms of possessing different experiences of adopting Internet corporate reporting) in 2004 and 2007, respectively.

## Results and analysis

We obtained two data sets, which we refer to as the 2004 data set and 2007 data set. For notational convenience, given a hypothesis Hn, we denoted its corresponding null hypothesis by Hn<sup>o</sup>. For example, corresponding to H2a [corporate governance![arrow](rarr.png)Web presence], we denoted by H2a<sup>o</sup> the null hypothesis that '_there is no difference between the level of corporate governance of Web-present and non-Web-present firms_'.

For each hypothesis (except H1), we computed both its test statistic and p-value. Unless stated otherwise, we adopted 5% as the threshold of significance level to determine whether to reject a null hypothesis. When a result to reject a null hypothesis is statistically significant (that is, when p-value <0.05), we present the p-value with an asterisk (for example, ‘p-value = 0.021<sup>*</sup>').

### Web presence and degree of Internet corporate reporting

Table 2 shows that, in both data sets, a very high proportion of the firms were Web present. In 2004, 89.5% of the firms were Web present. This percentage increased to 96.5% in 2007\. The data sets confirm that the adoption of Internet corporate reporting has been very popular since 2004, and that this popularity has increased over time. This phenomenon is true of both H-share and non-H-share firms (see the results on corporate governance later). The proportion of firms with a Web presence is high irrespective of their origins.

<table width="85%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Web presence of firms.**</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="2">First round of study (2004)</th>

<th colspan="2">Second round of study (2007)</th>

</tr>

<tr>

<th>Number</th>

<th>Percentage</th>

<th>Number</th>

<th>Percentage</th>

</tr>

<tr>

<td>**Web-present firms**</td>

<td align="center">179</td>

<td align="center">89.5%</td>

<td align="center">193</td>

<td align="center">96.5%</td>

</tr>

<tr>

<td>**Non-Web-present firms**</td>

<td align="center">21</td>

<td align="center">10.5%</td>

<td align="center">7</td>

<td align="center">3.5%</td>

</tr>

<tr>

<td>**Total**</td>

<td align="center">200</td>

<td align="center">100%</td>

<td align="center">200</td>

<td align="center">100%</td>

</tr>

</tbody>

</table>

Table 3 shows statistics of the degrees of Internet corporate/financial reporting in Web-present firms. In 2004, the mean degree of Internet corporate reporting of Web-present firms was 26.9 out of a maximum of 35, and in 2007 the corresponding mean was 29.4 out of 35\. For the degree of Internet financial reporting of Web-present firms, the mean values were 10.9 and 12.1 in 2004 and 2007, respectively, out of a maximum of 14\. We observed a similar pattern regarding the respective median values. These results provide strong support for H1 [degree of Internet corporate reporting].

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Degrees of Internet corporate/financial reporting in Web-present firms.**</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="3">First round of study (2004)</th>

<th colspan="3">Second round of study (2007)</th>

</tr>

<tr>

<th>Mean  
(%age)</th>

<th>Median</th>

<th>Standard  
deviation</th>

<th>Mean  
(%age)</th>

<th>Median</th>

<th>Standard  
deviation</th>

</tr>

<tr>

<td>**Number of online corporate  
information categories, _CI_** (degree of Internet corporate reporting, max = 35)</td>

<td align="center">26.9  
(76.9%)</td>

<td align="center">28</td>

<td align="center">6.3</td>

<td align="center">29.4  
(84.0%)</td>

<td align="center">30</td>

<td align="center">2.6</td>

</tr>

<tr>

<td valign="top">**Number of online financial  
information categories, _FI_** (degree of Internet financial reporting, max = 14)</td>

<td align="center">10.9  
(77.9%)</td>

<td align="center">11</td>

<td align="center">3.2</td>

<td align="center">12.1  
(86.4%)</td>

<td align="center">13</td>

<td align="center">1.4</td>

</tr>

</tbody>

</table>

There are interesting changes in the degrees of Internet corporate/financial reporting in the two data sets. Table 3 shows that both the mean and median degrees of Internet corporate/financial reporting increased over the three-year period. At the same time, the standard deviations for the degrees of Internet corporate/financial reporting decreased by more than half (from 6.3 to 2.6 for Internet corporate reporting and from 3.2 to 1.4 for Internet financial reporting). In other words, the variation of the degrees of Internet corporate/financial reporting had been largely reduced.

The mean degrees of Internet corporate reporting in 2004 and 2007 were 76.9% and 84.0%, respectively, of the maximum, showing a growth of 7.1% within the three-year period. In comparison, the percentages for the degree of Internet financial reporting were 77.9% and 86.4%, respectively, showing a growth of 8.5%. The median values show a similar pattern. Thus, not only were the percentages in both 2004 and 2007 for the degree of Internet financial reporting consistently higher than the corresponding percentages for the degree of Internet corporate reporting, but the amount of growth of the degree of Internet financial reporting was also higher. This supports the common belief that financial information is generally of more interest than non-financial information, especially to investors. Holder-Webb _et al._ ([2008](#hol08)), for instance, report that 62% of retail investors prefer financial information for use in investment decisions.

### Corporate governance

We have used H-share and non-H-share firms as proxies for corporations with (relatively) weak and strong corporate governance, respectively. We analysed whether firms with weak corporate governance (H-share firms) and strong corporate governance (non-H-share firms) were statistically different in terms of Web presence (H2a) and, for Web-present firms, in terms of their degrees of Internet corporate reporting (H2b).

Table 4 shows that the percentages of H-share firms with Web presence (84.2% in 2004 and 93.9% in 2007) were consistently lower than the corresponding percentages of non-H-share firms (94.9% in 2004 and 100% in 2007). The chi-squared (?<sup>2</sup>) two-sample test statistics were 6.20 (p-value = 0.013<sup>*</sup>) in 2004 and 5.36 (p-value = 0.021<sup>*</sup>) in 2007, thereby rejecting the null hypothesis H2a<sup>o</sup>. Thus, the data sets provide strong support for H2a [corporate governance ? Web presence].

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Corporate governance versus Web presence.**</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="2">First round of study (2004)</th>

<th colspan="2">Second round of study (2007)</th>

</tr>

<tr>

<th>Number of firms</th>

<th>Number (percentage) of Web-present firms</th>

<th>Number of firms</th>

<th>Number (percentage) of Web-present firms</th>

</tr>

<tr>

<td>**H-share firms (weak corporate governance)**</td>

<td align="center">101</td>

<td align="center">85 (84.2%)</td>

<td align="center">115</td>

<td align="center">108 (93.9%)</td>

</tr>

<tr>

<td>**Non-H-share firms (strong corporate governance)**</td>

<td align="center">99</td>

<td align="center">94 (94.9%)</td>

<td align="center">85</td>

<td align="center">85 (100%)</td>

</tr>

<tr>

<td rowspan="2">**Chi-squared (?<sup>2</sup>) two-sample test statistic**</td>

<td colspan="2" align="center">6.20  
(p-value = 0.013<sup>*</sup>)</td>

<td colspan="2" align="center">5.36  
(p-value = 0.021<sup>*</sup>)</td>

</tr>

</tbody>

</table>

We then considered H2b [corporate governance![arrow](rarr.png)degree of Internet corporate reporting]. Table 5 shows the mean, median and standard deviation values of the degrees of Internet corporate reporting of H-share and non-H-share Web-present firms. In both 2004 and 2007, H-share Web-present firms had smaller mean and median (but larger standard deviation) values of <var>CI</var> than non-H-share Web-present firms. Moreover, the differences are significant based on the Mann-Whitney U test (p-value = 0.015<sup>*</sup> in 2004 and 0.001<sup>*</sup> in 2007). Thus, H2b<sup>o</sup> was rejected. In other words, both data sets support H2b. This result is in line with previous findings ([Claessens 2006](#cla06); [Cohen _et al._ 2004](#coh04)) which show that corporate governance is directly related to the disclosure of company information mainly through the traditional (non-Web-based) reporting channel.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Corporate governance versus degree of Internet corporate reporting.**</caption>

<tbody>

<tr>

<th rowspan="3"> </th>

<th colspan="3">First round of study (2004)</th>

<th colspan="3">Second round of study (2007)</th>

</tr>

<tr>

<th colspan="3">Number of online corporate information categories (<var>CI</var>)</th>

<th colspan="3">Number of online corporate information categories (<var>CI</var>)</th>

</tr>

<tr>

<th>Mean</th>

<th>Median</th>

<th>Standard  
deviation  
</th>

<th>Mean</th>

<th>Median</th>

<th>Standard  
deviation</th>

</tr>

<tr>

<td>**H-share Web-present firms  
(weak corporate governance)**</td>

<td align="center">26.0</td>

<td align="center">28</td>

<td align="center">6.8</td>

<td align="center">28.9</td>

<td align="center">29</td>

<td align="center">2.7</td>

</tr>

<tr>

<td>**Non-H-share Web-present firms  
(strong corporate governance)**</td>

<td align="center">27.8</td>

<td align="center">29</td>

<td align="center">5.7</td>

<td align="center">30.1</td>

<td align="center">30</td>

<td align="center">2.3</td>

</tr>

<tr>

<td rowspan="2" align="center">**Mann-Whitney U test statistic**</td>

<td colspan="3" align="center">3163  
(p-value = 0.015<sup>*</sup>)</td>

<td colspan="3" align="center">3370  
(p-value = 0.001<sup>*</sup>)</td>

</tr>

</tbody>

</table>

Corporate governance can be measured by many aspects of a firm, most of which are non-financial ([Bushman _et al._ 2004](#bus04)). For example, Governance Metrics International, a corporate governance research and ratings agency established in April 2000, uses a rating system which considers six categories of analysis in determining the rating of corporate governance of a firm: (1) board accountability, (2) financial disclosure and internal controls, (3) executive compensation, (4) shareholder rights, (5) ownership base and takeover provisions, (6) corporate behaviour and social responsibility. Most of these categories correspond to the non-financial information categories in Table 1\. In relation to corporate governance, therefore, there is little theoretical ground to consider the disclosure of financial information alone. As such, we did not further analyse Internet financial reporting with respect to H2b.

### System openness

We classified the 200 firms into two groups: firms that fulfilled above the minimum statutory requirements for non-executive directors (Group-A firms), and those that did not (Group-B firms). Group-A firms had a higher level of system openness than Group-B firms. We analysed whether Group-A firms and Group-B firms were statistically different in terms of Web presence (H3a) and, for Web-present firms, in terms of their degrees of Internet corporate reporting (H3b). Similar to corporate governance, while system openness is shown in the literature to bear relation to the disclosure of corporate (mainly non-financial) information, there is little theoretical ground to consider the disclosure of financial information alone. For example, Rashid and Lodh ([2008](#ras08)) report that introducing non-executive directors (our proxy of system openness) to a Board of Directors would result in a significant increase in corporate social reporting, which corresponds to the non-financial information categories such as ‘Environment', ‘Energy' and ‘Community' in Table 1\. Therefore, we did not include Internet financial reporting in the data analysis for H3b.

Table 6 shows that in 2004, 92.8% of Group-A firms were Web-present, compared to only 87.2% of Group-B firms. By 2007, however, the percentage of Web-present firms (95.3%) within Group-A was smaller than the percentage (97.9%) within Group-B. Thus, the results do not clearly show which group contained the higher proportion of Web-present firms. Moreover, when comparing the Web-present firms in Group-A and Group-B, the chi-squared (?<sup>2</sup>) two-sample test statistics were 1.62 (p-value = 0.204) and 0.99 (p-value = 0.320) for the 2004 and 2007 data sets, respectively. Neither difference is statistically significant. Thus, H3a<sup>o</sup> cannot be rejected in favour of H3a [system openness![arrow](rarr.png)Web presence].

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 6: System openness versus Web presence.**</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="2">First round of study (2004)</th>

<th colspan="2">Second round of study (2007)</th>

</tr>

<tr>

<th>Number of firms</th>

<th>Number (percentage) of Web-present firms</th>

<th>Number of firms</th>

<th>Number (percentage) of Web-present firms</th>

</tr>

<tr>

<td>**Group-A firms<sup>†</sup> (higher level of system openness)**</td>

<td align="center">83</td>

<td align="center">77 (92.8%)</td>

<td align="center">106</td>

<td align="center">101 (95.3%)</td>

</tr>

<tr>

<td>**Group-B firms<sup>†</sup> (lower level of system openness)**</td>

<td align="center">117</td>

<td align="center">102 (87.2%)</td>

<td align="center">94</td>

<td align="center">92 (97.9%)</td>

</tr>

<tr>

<td>**Chi-squared (?<sup>2</sup>) two-sample test statistic**</td>

<td colspan="2" align="center">1.62  
(p-value = 0.204)</td>

<td colspan="3" align="center">0.99  
(p-value = 0.320)</td>

</tr>

<tr>

<td colspan="5"><sup>†</sup> Group-A firms = firms that fulfilled _above_ the minimum statutory requirements for non-executive directors; Group-B firms = other firms</td>

</tr>

</tbody>

</table>

We then considered H3b [system openness![arrow](rarr.png)degree of Internet corporate reporting]. Table 7 shows the mean, median and standard deviation values of the degrees of Internet corporate reporting of Group-A and Group-B Web-present firms. In 2004, Group-A Web-present firms had a marginally lower mean but a somewhat higher median value of <var>CI</var> than those in Group-B. Moreover, the Mann-Whitney U test shows no significant difference between these two groups (p-value = 0.310). On the other hand, in 2007, both the mean and median values of <var>CI</var> of Group-A Web-present firms were higher than those in Group-B, and this difference is significant (p-value = 0.028<sup>*</sup>) according to the Mann-Whitney U test. Thus, the statistical evidence for H3b is mixed, and the null hypothesis H3b<sup>o</sup> cannot be clearly rejected.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 7\. System openness versus degree of Internet corporate reporting.**</caption>

<tbody>

<tr>

<th rowspan="3"> </th>

<th colspan="3">First round of study (2004)</th>

<th colspan="3">Second round of study (2007)</th>

</tr>

<tr>

<th colspan="3">Number of online corporate information categories (<var>CI</var>)</th>

<th colspan="3">Number of online corporate information categories (<var>CI</var>)</th>

</tr>

<tr>

<th>Mean</th>

<th>Median</th>

<th>Standard  
deviation</th>

<th>Mean</th>

<th>Median</th>

<th>Standard  
deviation</th>

</tr>

<tr>

<td>**Group-A Web-present firms (higher level of system openness)**</td>

<td align="center">26.9</td>

<td align="center">29</td>

<td align="center">7.0</td>

<td align="center">29.9</td>

<td align="center">30</td>

<td align="center">2.2</td>

</tr>

<tr>

<td>**Group-B Web-present firms (lower level of system openness)**</td>

<td align="center">27.0</td>

<td align="center">28</td>

<td align="center">5.7</td>

<td align="center">29.0</td>

<td align="center">29</td>

<td align="center">2.9</td>

</tr>

<tr>

<td align="center">**Mann-Whitney U test statistic**</td>

<td colspan="3" align="center">3198  
(p-value = 0.310)</td>

<td colspan="3" align="center">3803  
(p-value = 0.028<sup>*</sup>)</td>

</tr>

</tbody>

</table>

### Firm's size, revenue and stock trading activity

We first considered H4a [size![arrow](rarr.png)Web presence], H5a [revenue![arrow](rarr.png)Web presence] and H6a [stock trading activity![arrow](rarr.png)Web presence]. We measured a firm's size, revenue and stock trading activity by its market capitalisation, net profit and stock turnover (denoted by <var>MC</var>, <var>NP</var> and <var>ST</var>, respectively), and compared firms with Internet corporate/financial reporting to those without.

Table 8 shows the median values of <var>MC</var>, <var>NP</var> and <var>ST</var> of all the subject firms. (To avoid the table being clustered too densely with numbers, the mean and standard deviation values are not shown in Table 8). The 2004 and 2007 data sets are remarkably consistent: with no exception, the median values of <var>MC</var>, <var>NP</var> and <var>ST</var> of firms with Internet corporate/financial reporting are substantially larger than those without. More interestingly, according to the Mann-Whitney U test statistics, these differences are all statistically significant in 2004, but no longer so in 2007\. Thus, Table 8 supports H4a, H5a and H6a in 2004 but not in 2007.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 8\. Internet corporate/financial reporting versus firm's size, revenue and stock trading activity.**</caption>

<tbody>

<tr>

<th rowspan="3"> </th>

<th colspan="3">First round of study (2004)</th>

<th colspan="3">Second round of study (2007)</th>

</tr>

<tr>

<th colspan="3">Median value  
(in million Hong Kong dollars)</th>

<th colspan="3">Median value  
(in million Hong Kong dollars)</th>

</tr>

<tr>

<th>MC</th>

<th>NP</th>

<th>ST</th>

<th>MC</th>

<th>NP</th>

<th>ST</th>

</tr>

<tr>

<td>**Firms _with_ Internet corporate reporting**</td>

<td align="center">5,258</td>

<td align="center">424</td>

<td align="center">18</td>

<td align="center">28,358</td>

<td align="center">1,258</td>

<td align="center">71</td>

</tr>

<tr>

<td>**Firms _without_ Internet corporate reporting**</td>

<td align="center">1,694</td>

<td align="center">150</td>

<td align="center">10</td>

<td align="center">18,287</td>

<td align="center">553</td>

<td align="center">63</td>

</tr>

<tr>

<td>**Mann-Whitney U test statistic  
p-value**</td>

<td align="center">868  
<0.001<sup>*</sup></td>

<td align="center">1180  
= 0.005<sup>*</sup></td>

<td align="center">1350  
= 0.035<sup>*</sup></td>

<td align="center">582  
= 0.534</td>

<td align="center">445  
= 0.125</td>

<td align="center">672  
= 0.981</td>

</tr>

<tr>

<td>**Firms _with_ Internet financial reporting**</td>

<td align="center">5,628</td>

<td align="center">433</td>

<td align="center">20</td>

<td align="center">28,687</td>

<td align="center">1,265</td>

<td align="center">72</td>

</tr>

<tr>

<td>**Firms _without_ Internet financial reporting**</td>

<td align="center">1,986</td>

<td align="center">169</td>

<td align="center">9</td>

<td align="center">16,616</td>

<td align="center">479</td>

<td align="center">46</td>

</tr>

<tr>

<td>**Mann-Whitney U test statistic  
p-value**</td>

<td align="center">1195  
<0.001<sup>*</sup></td>

<td align="center">1636  
= 0.002<sup>*</sup></td>

<td align="center">1817  
= 0.012<sup>*</sup></td>

<td align="center">610  
= 0.325</td>

<td align="center">458  
= 0.053</td>

<td align="center">688  
= 0.615</td>

</tr>

</tbody>

</table>

Furthermore, our data shows that the degrees of Internet corporate financial reporting varied widely. For example, in 2004, out of the thirty-five online corporate information categories listed in Table 1, one corporate Website presented fewer than ten information categories, whilst another included more than thirty. In view of the wide range observed, we decided to perform a more fine-grained analysis of the relationship between the degrees of Internet corporate/financial reporting and the size, revenue and stock trading activity of the firm.

We plotted the scatter diagrams for each of the values of <var>CI</var> and <var>FI</var> of Web-present firms against each of their corresponding values of <var>MC</var>, <var>NP</var> and <var>ST</var> in 2004 and 2007, respectively. In total, there are 2 × 3 × 2 = 12 scatter diagrams. The patterns of data point distributions are amazingly similar across all the scatter diagrams. The consistency of the data point distribution patterns enhances the validity of our findings. Limited by the space of this paper, we include only the four scatter diagrams related to the variable <var>MC</var> in Figures 2 to 5.

<div align="center">![Figure 2\. Scatter diagram of CI against MC (for all Web-present firms in 2004)](p510fig2.gif)</div>

<div align="center">  
**Figure 2: Scatter diagram of <var>CI</var> against <var>MC</var> (for all Web-present firms in 2004)**</div>

<div align="center">![Figure 3: Scatter diagram of CI against MC (for all Web-present firms in 2007)](p510fig3.gif)</div>

<div align="center">  
**Figure 3\. Scatter diagram of <var>CI</var> against <var>MC</var> (for all Web-present firms in 2007)**</div>

<div align="center">![Figure 4: Scatter diagram of FI against MC (for all Web-present firms in 2004)](p510fig4.gif)</div>

<div align="center">  
**Figure 4\. Scatter diagram of <var>FI</var> against <var>MC</var> (for all Web-present firms in 2004)**</div>

<div align="center">![Figure 5: Scatter diagram of FI against MC (for all Web-present firms in 2007)](p510fig5.gif)</div>

<div align="center">  
**Figure 5\. Scatter diagram of <var>FI</var> against <var>MC</var> (for all Web-present firms in 2007)**</div>

Figures 2 and 3 show the scatter diagrams of <var>CI</var> against <var>MC</var> for all Web-present firms in 2004 and 2007, respectively. It is quite clear that the data points do not exhibit a visually linear relationship. Hence, the hypotheses H4b [size![arrow](rarr.png)degree of Internet corporate reporting], H5b [revenue![arrow](rarr.png)degree of Internet corporate reporting] and H6b [stock trading activity![arrow](rarr.png)degree of Internet corporate reporting] are rejected because of the lack of linear correlation between the variables in each hypothesis.

There is however a very interesting phenomenon here. Both Figures 2 and 3 show that most data points cluster in the top-left regions and very few appear in the bottom-right. To further investigate this distinctive distribution pattern and to refine and quantify our observations, we drew a horizontal line and a vertical line in each figure, corresponding to the mean values of <var>CI</var> and <var>MC</var>, respectively. The two lines divide each graph into four areas: area 1 (top-left), area 2 (top-right), area 3 (bottom-left) and area 4 (bottom-right). Points are separated in these four areas according to whether the firms represented by the points have above or below average values of <var>CI</var> and <var>MC</var>. The percentages of data points in these four areas are shown in Tables 9 and 10.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 9: Distribution of data points for <var>CI</var> against <var>MC</var> (for all Web-present firms in 2004).**</caption>

<tbody>

<tr>

<td rowspan="2"> </td>

<th colspan="3">Percentages of data points (see Figure 2)</th>

</tr>

<tr>

<th>Below-average <var>MC</var></th>

<th>Above-average <var>MC</var></th>

<th>Total</th>

</tr>

<tr>

<td>**Above-average <var>CI</var>**</td>

<td>(Area 1, top-left) 59.2%</td>

<td>(Area 2, top-right) 16.2%</td>

<td>(Areas 1 and 2, top) 75.4%</td>

</tr>

<tr>

<td>**Below-average <var>CI</var>**</td>

<td>(Area 3, bottom-left) 24.0%</td>

<td>(Area 4, bottom-right) 0.6%</td>

<td>(Areas 3 and 4, bottom) 24.6%</td>

</tr>

<tr>

<td>**Total**</td>

<td>(Areas 1 and 3, left) 83.2%</td>

<td>(Areas 2 and 4, right) 16.8%</td>

<td>(All areas) 100.0%</td>

</tr>

</tbody>

</table>

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 10\. Distribution of data points for <var>CI</var> against <var>MC</var> (for all Web-present firms in 2007).**</caption>

<tbody>

<tr>

<td rowspan="2"> </td>

<th colspan="3">Percentages of data points (see Figure3)</th>

</tr>

<tr>

<th>Below-average <var>MC</var></th>

<th>Above-average <var>MC</var></th>

<th>Total</th>

</tr>

<tr>

<td>**Above-average <var>CI</var>**</td>

<td>(Area 1, top-left) 49.7%</td>

<td>(Area2, top-right) 18.1%</td>

<td>(Areas1 and2, top) 67.8%</td>

</tr>

<tr>

<td>**Below-average <var>CI</var>**</td>

<td>(Area 3, bottom-left) 30.6%</td>

<td>(Area 4, bottom-right) 1.6%</td>

<td>(Areas3 and4, bottom) 32.2%</td>

</tr>

<tr>

<td>**Total**</td>

<td>(Areas 1 and 3, left) 80.3%</td>

<td>(Areas 2 and 4, right) 19.7%</td>

<td>(All areas) 100.0%</td>

</tr>

</tbody>

</table>

Since the patterns of data point distributions are very similar across all scatter diagrams, we use Figure 3 as an example to elaborate our observations, while also referring to the percentages of data points in each area as shown in Table 10.

1.  _The majority of Web-present firms exhibit an above-average degree of Internet corporate reporting._  

    Areas 1 and 2 together (corresponding to firms with above-average values of <var>CI</var>) already cover 67.8% of all the data points, whereas areas 3 and 4 together (corresponding to firms with below-average values of <var>CI</var>) cover 32.2% only. The first percentage is more than double the second, indicating that the large majority of Web-present firms have an above-average degree of Internet corporate reporting. That is, the amount of online corporate information disseminated by the subject Web-present firms was generally high.
2.  _Even smaller Web-present firms are likely to exhibit an above-average degree of Internet corporate reporting._  

    Consider the smaller Web-present firms (which have below-average values of <var>MC</var>), represented in Figure 3 by data points in areas 1 and 3\. Among these firms, 49.7% have above-average values of <var>CI</var> (area 1). This is greater than the proportion (30.6%) of firms with below-average values of <var>CI</var> (area 3). In other words, even the smaller Web-present firms are likely to exhibit an above-average degree of Internet corporate reporting. At the time of the study, Web technologies were already easily accessible, relatively inexpensive and very popular. As such, smaller Web-present firms could still afford, if they so wished, to exploit implementations of Internet corporate reporting to their benefits.
3.  _A large Web-present firm rarely discloses only a small amount of corporate information on its Website._  

    The larger Web-present firms, with above-average values of <var>MC</var>, are represented in Figure 3 by data points in areas 2 and 4\. A very small proportion (1.6%) of these firms has below-average values of <var>CI</var> (area 4). This shows that a large Web-present firm rarely exhibits a low degree of Internet corporate reporting.
4.  _A Web-present firm which exhibits an above-average degree of Internet corporate reporting may or may not be a large one. On the other hand, a Web-present firm which exhibits a below-average degree of Internet corporate reporting is almost certainly not a large one._  

    Firms with above-average values of <var>CI</var> are represented in Figure 3 by data points in areas 1 and 2\. These data points cover a broad spectrum of firm sizes in terms of <var>MC</var>. Thus, a firm which exhibits an above-average degree of Internet corporate reporting can be a small or large one. On the other hand, the percentage of data points in area 4 (1.6%) is very small. Thus, if a Web-present firm exhibits a below-average degree of Internet corporate reporting, it is almost certainly not a large one.

Figures 4 and 5 show the corresponding scatter diagrams of <var>FI</var> against <var>MC</var> in 2004 and 2007 respectively. The above observations also apply to Figures 2, 4 and 5, with appropriate adaptations, such as 2004 instead of 2007 for Figure 2, and <var>FI</var> in place of <var>CI</var> for Figures 4 and 5\. Indeed, these observations are also applicable to scatter diagrams (not shown in this paper) involving <var>NP</var> and <var>ST</var>, since all the data point distribution patterns are essentially the same.

### Experience of adoption

Finally, we considered the experience of adopting Internet corporate/financial reporting of firms. 122 Web-present firms appear in both the 2004 and 2007 data sets. According to our classification scheme, the 122 Web-present firms in the 2004 data set (Group-1 firms) were less experienced adopters of Internet corporate reporting than the same firms in 2007 (Group-2 firms). Table 11 shows the mean, median and standard deviation values of <var>CI</var> and <var>FI</var> for both groups of firms and the corresponding computed Wilcoxon signed-rank test statistics. Both the mean and median values of <var>CI</var> and <var>FI</var> for less experienced adopters of Internet corporate reporting are correspondingly smaller than those for more experienced adopters. These differences are highly significant (p-value <0.001<sup>*</sup> and = 0.002<sup>*</sup>, respectively). Thus, Table 11 provides strong statistical support for H7 [experience of adoption![arrow](rarr.png)degree of Internet corporate reporting].

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 11\. Experience of adoption versus degrees of Internet corporate/financial reporting.**</caption>

<tbody>

<tr>

<td rowspan="2"> </td>

<th colspan="3">Number of online corporate information categories (<var>CI</var>)</th>

<th colspan="3">Number of online financial information categories (<var>FI</var>)</th>

</tr>

<tr>

<th>Mean</th>

<th>Median</th>

<th>Standard  
deviation  
</th>

<th>Mean</th>

<th>Median</th>

<th>Standard  
deviation</th>

</tr>

<tr>

<td>**Group-1 firms (_less experienced_ adopters)**</td>

<td align="center">28.3</td>

<td align="center">29</td>

<td align="center">4.7</td>

<td align="center">11.6</td>

<td align="center">12</td>

<td align="center">2.3</td>

</tr>

<tr>

<td>**Group-2 firms (_more experienced_ adopters)**</td>

<td align="center">29.6</td>

<td align="center">30</td>

<td align="center">3.0</td>

<td align="center">12.1</td>

<td align="center">13</td>

<td align="center">1.5</td>

</tr>

<tr>

<td rowspan="2">**Wilcoxon signed-rank test statistic, W**</td>

<td align="center" colspan="3">2496  
(p-value <0.001<sup>*</sup>)</td>

<td align="center" colspan="3">605  
(p-value = 0.002<sup>*</sup>)</td>

</tr>

</tbody>

</table>

## Discussion, implications and limitations

### Discussion

The results for H1, H2a, H2b and H7 are not surprising, as these hypotheses were all motivated by the diffusion of innovation theory, expectation-confirmation theory, corporate governance theories and learning or experience curve theories, and furthermore statistically supported by the respective data. Thus, our discussion here focuses more on the other hypotheses.

Consider H3a [system openness![arrow](rarr.png)Web presence] and H3b [system openness![arrow](rarr.png)degree of Internet corporate reporting]. Both hypotheses fail to be supported by the data (see Tables 6 and 7). Apart from the possibility of unavoidable statistical Type I or Type II errors, one straightforward interpretation is that, contrary to the relevant theories, system openness actually does not have significant influence on whether a firm adopts Internet corporate reporting. On the other hand, it might also be because, in reality, whether or not a firm fulfils more than the minimum statutory requirements for non-executive directors is not a good enough proxy for its level of system openness. A plausible explanation is that, when a listed firm appoints an external person as a non-executive director, that person is already likely to be known by (and possibly has a good relationship with) other directors of the firm. In other words, non-executive directors may not be truly or totally _independent_ after all. It is thus not entirely surprising that hypothesis testing involving the number of non-executive directors does not yield meaningful or expected statistical results.

Next we considered H4a [size![arrow](rarr.png)Web presence], H4b [size![arrow](rarr.png)degree of Internet corporate reporting], H5a [revenue![arrow](rarr.png)Web presence], H5b [revenue![arrow](rarr.png)degree of Internet corporate reporting], H6a [stock trading activity![arrow](rarr.png)Web presence] and H6b [stock trading activity![arrow](rarr.png)degree of Internet corporate reporting]. We obtained mixed results for these hypotheses. Regarding Web presence (H4a, H5a and H6a), in both 2004 and 2007, with no exception, firms which adopt Internet corporate/financial reporting have larger median values of <var>MC</var>, <var>NP</var> and <var>ST</var> than those which do not (see Table 8). Mann-Whitney U tests show that all such differences were significant in 2004 but not in 2007\. In other words, H4a, H5a and H6a are supported by data in 2004 but not in 2007\. On one hand, the 2004 data clearly verify that a firm's size, revenue and stock trading activity did have a significant effect on the degree of Internet corporate reporting as proposed. On the other hand, the 2007 data reveal somewhat different results. This posteriori finding itself is significant in research value, since it suggests that the traditional theories reviewed in our earlier paper ([Poon and Yu 2012](#poo12)) may need to be reconsidered in light of the new observed trend when applied to the context of Internet corporate reporting. In this regard, our empirical results will help inspire renewed interests and advances in the respective theories.

Regarding the testing of H4b, H5b and H6b, we did not find a linear relationship between the values of <var>MC</var>/<var>NP</var>/<var>ST</var> and the degrees of Internet corporate/financial reporting. Instead, our analyses of the scatter diagrams (see Figures 2 to 5) revealed that the majority of Web-present firms, regardless of their size, revenue and stock trading activity, exhibited above-average degrees of Internet corporate and financial reporting.

### Implications

The results of H4a, H4b, H5a, H5b, H6a and H6b provide a clear insight into the state of Internet corporate reporting in 2004, when such reporting was less popular than in 2007\. Larger firms with higher revenues and stock trading activities are typically more innovative, have more financial resources and greater access to technical knowledge and expertise to adopt Internet corporate reporting. Thus, size, revenue and stock trading activity were some of the key indicators of whether a firm had implemented Internet corporate reporting (that is, was Web-present) in 2004\. By 2007, almost all Web-present firms (including the smaller firms) exhibited a high degree of Internet corporate reporting. As the popularity of Internet corporate reporting grew, the size, revenue and stock trading activity of a firm were no longer key indicators of adopting such a reporting practice. This change from 2004 to 2007, together with our positive test results of H1 [degree of Internet corporate reporting] and H7 [experience of adoption![arrow](rarr.png)degree of Internet corporate reporting], suggest that Internet corporate reporting has become not just a _nice-to-have_ option, but rather an essential and affordable corporate strategy that could be implemented across all firms.

### Limitations and future work

In principle, firms could have made changes to their Web content whilst our data were being collected. To reduce this potential threat of data validity, we have made our best effort to shorten the time taken for collecting the data from the large number of firms involved.

Strictly speaking, our measurement by counting the online corporate information categories in Table 1 addresses the _variety_ (that is, different types) rather than the _amount_ of online corporate information (though we use both terms interchangeably in the paper). Nevertheless, we focused on the variety of online corporate information categories for two reasons. First, investors are keen to obtain and analyse different types of corporate information before making their investment decisions. Secondly, there is no simple yet meaningful and practical measurement to quantify the amount of a particular type of corporate information. For example, it is not meaningful to measure the information by the number of words. To some extent, the variety of online corporate information categories reflects the _richness_ of the contents of the corporate Websites.

One may argue that, in addition to the six factors in Figure 1, other factors, such as a firm's transparency and management curriculum, may have an influence on the degree of Internet corporate reporting. Our six factors, however, are largely _external_, that is, the relevant information is publicly available and quantitatively measurable. By contrast, factors such as a firm's transparency and management curriculum, are mostly related to information that is _internal_ to a firm and, hence, is often hard to access externally or quantify meaningfully. A study of these internal factors should probably be done by collaborative and in-depth _qualitative_ analyses with the subject firms, which belongs to a different research paradigm, and has to be left to further work. Instead, this paper focused on the study of the _statistical relations_ between a firm's attributes or factors and the degree of its Internet corporate reporting.

As pointed out in the previous Subsection, _Implications_, following the growth of Internet corporate reporting, the size, revenue and stock trading activity of a firm were no longer key indicators of its adoption of such a reporting practice. Follow-up research could investigate whether the prevalence of Internet corporate reporting in 2007 had dwarfed the factors of the size, revenue and stock trading activity of a firm that were significant in 2004.

## Conclusion

This paper reports research on the common practice of adopting Internet corporate reporting. In particular, it investigates the relationship between certain characteristics of firms and the corporate information content of their Websites.

Our work focused on the richness of the content of the corporate Websites. We are aware, however, that a corporate Website should not only be rich in content, but also facilitate online visitors to locate the corporate information they require. To achieve this objective, online corporate information should be logically organised and incorporated into well-structured Web pages, and navigation of related online information should be made easy ([Poon and Lau 2006](#poo06)). Furthermore, the format of presentation should also be considered ([Poon _et al._ 2003](#poo03); [Wong and Poon 2008](#won08)). For example, instead of simply presenting financial data on the corporate Website, such data could be provided in the form of an electronic spreadsheet so that potential investors may download the data from the Website for detailed study and offline analysis. From an investor's point of view, financial data disseminated in a form that facilitates further processing would be much more useful. Further studies should take these considerations into account.

Finally, the empirical study reported in this paper shows how the research framework and its associated research questions/hypotheses developed in our earlier paper ([Poon and Yu 2012](#poo12)) can be adapted to study the adoption of Internet corporate reporting in a local context.

## Acknowledgements

We thank F.F. Chan and T.H. Ng for their assistance in compiling the data and performing the statistical tests for this paper. We also thank all those people who have helped review and edit this paper.

## About the authors

Pak-Lok Poon is an Associate Professor in the School of Accounting and Finance, The Hong Kong Polytechnic University, Hong Kong. He received his Master of Business (Information Technology) from Royal Melbourne Institute of Technology, Australia and his PhD in software engineering from The University of Melbourne, Australia. He can be contacted at: [afplpoon@inet.polyu.edu.hk](mailto:afplpoon@inet.polyu.edu.hk) .

Yuen Tak Yu is an Associate Professor in the Department of Computer Science, City University of Hong Kong, Hong Kong. He received his Bachelor's degree in Mathematics from The University of Hong Kong, Hong Kong and his PhD in software engineering from The University of Melbourne, Australia. He can be contacted at: [csytyu@cityu.edu.hk](mailto:csytyu@cityu.edu.hk).

#### References

*   Agrawal, A. & Chadha, S. (2005). Corporate governance and accounting scandals. _Journal of Law & Economics_, **48**(2), 371-406.
*   Baskerville, R.L. & Pries-Heje, J. (2001). A multiple-theory analysis of a diffusion of information technology case. _Information Systems Journal_, **11**(3), 181-212.
*   Baysinger, B.D. & Hoskisson, R.E. (1990). The composition of boards of directors and strategic control: effects on corporate strategy. _Academy of Management Review_, **15**(1), 72-87.
*   Bhattacherjee, A. (2001). Understanding information systems continuance: an expectation-confirmation model. _MIS Quarterly_, **25**(3), 351-370.
*   Bushman, R.M., Piotroski, J.D. & Smith, A.J. (2004). What determines corporate transparency? _Journal of Accounting Research_, **42**(2), 207-252.
*   Carter, F.J., Jr., Jambulingam, T., Gupta, V.K. & Melone, N. (2001). Technological innovations: a framework for communicating diffusion effects. _Information & Management_, **38**(5), 277-287.
*   Chang, T-L. (1996). Cultivating global experience curve advantage on technology and marketing capabilities. _International Marketing Review_, **13**(6), 22-42.
*   Chea, S. & Luo, M.M. (2008). Post-adoption behaviors of e-service customers: the interplay of cognition and emotion. _International Journal of Electronic Commerce_, **12**(3), 29-56.
*   Cheung, Y.L., Connelly, J.T., Limpaphayom, P. & Zhou, L. (2007). Do investors really value corporate governance? Evidence from the Hong Kong Market. _Journal of International Financial Management & Accounting_, **18**(2), 86-122.
*   Claessens, S. (2006). Corporate governance and development. _World Bank Research Observer_, **21**(1), 91-122.
*   Cohen, J., Krishnamoorthy, G. & Wright, A. (2004). The corporate governance mosaic and financial reporting quality. _Journal of Accounting Literature_, **23**(1), 87-152.
*   Henderson, B. (1973). _[The experience curve — reviewed. IV. The growth share matrix.](http://www.webcitation.org/5x0Af1eu4)_ Boston, MA: Boston Consulting Group. (Perspectives no. 135) Retrieved 7 March, 2011 from http://www.bcg.com/documents/file13904.pdf (Archived by WebCite® at http://www.webcitation.org/5x0Af1eu4)
*   Hoff, B. & Wood, D. (2008). _[The use of non-financial information: what do investors want?](http://www.webcitation.org/5x0B7jZb8)_ Chestnut Hill, MA: Boston College, Carrol School of Management. Retrieved 7 March, 2011 from http://www.finrafoundation.org/web/groups/foundation/@foundation/documents/foundation/p118412.pdf (Archived by WebCite® at http://www.webcitation.org/5x0B7jZb8)
*   Lieberman, M.B. (1987). The learning curve, diffusion, and competitive strategy. _Strategic Management Journal_, **8**(5), 441-452.
*   Lin, C.S., Wu, S. & Tsai, R.J. (2005). Integrating perceived playfulness into expectation-confirmation model for web portal context. _Information & Management_, **42**(5), 683-693.
*   Moore, G.C. & Benbasat, I. (1991). Development of an instrument to measure the perceptions of adopting an information technology innovation. _Information Systems Research_, **2**(3), 192-222.
*   Poon, P-L. & Lau, A.H.L. (2006). The PRESENT B2C implementation framework. _Communications of the ACM_, **49**(2), 96-103.
*   Poon, P-L., Li, D. & Yu, Y.T. (2003). Internet financial reporting. _Information Systems Control Journal_, **1**, 42-45.
*   Poon, P-L. & Yu, Y.T. (2012). [Degree of Internet corporate reporting: a research framework.](http://www.webcitation.org/666eEnJ7K) _Information Research_, 17(1), paper 509\. Retrieved 11 February, 2012 from http://informationr.net/ir/17-1/paper509.html (Archived by WebCite® at http://www.webcitation.org/666eEnJ7K).
*   Rashid, A. & Lodh, S.C. (2008). The influence of ownership structures and board practices on corporate social disclosures in Bangladesh. In M. Tsamenyi & S. Uddin (Eds.), _Research in accounting in emerging economics: corporate governance in less developed and emerging economics_ (pp. 211-237). Bradford, UK: Emerald.
*   Rogers, E.M. (2003). _Diffusion of innovations._ (5th ed.). New York, NY: Free Press.
*   Wong, A. & Poon, P-L. (2008). Control issues of using corporate Web sites for public disclosure. _Information Systems Control Journal_, **5**(1), 38-40.