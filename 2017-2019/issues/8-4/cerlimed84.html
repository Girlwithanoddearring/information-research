<!DOCTYPE html>
<html lang="en">

<head>
	<title>CERLIM issue editorial</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta name="keywords" content="Information Research, CERLIM">
	<meta name="description" content="Information Research. CERLIM">
	<meta name="rating" content="Mature">
	<meta name="VW96.objecttype" content="Document">
	<meta name="ROBOTS" content="ALL">
	<meta name="DC.Title" content="CERLIM issue Editorial">
	<meta name="DC.Creator" content="Peter Brophy">
	<meta name="DC.Subject" content="Information Research Editorial CERLIM">
	<meta name="DC.Description" content="Information Research CERLIM Editorial">
	<meta name="DC.Publisher" content="Professor T.D. Wilson">
	<meta name="DC.Coverage.PlaceName" content="Global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="information-research-vol-8-no-4-july-2003">Information Research, Vol. 8 No. 4, July 2003</h4>
	<h1 id="editorial">Editorial</h1>
	<h2 id="introduction">Introduction</h2>
	<p>This issue of <em>Information Research</em> contains papers which describe elements of the current and recent
		work of the <a href="http://www.cerlim.ac.uk/">Centre for Research in Library and Information Management
			(CERLIM)</a> which is based in the Department of Information &amp; Communications at Manchester Metropolitan
		University in the U.K. CERLIM has an unusual history. It was founded within the university library at the
		University of Central Lancashire in Preston, UK, as a focus for research and development activities which, for a
		number of reasons, had started to increase in number and significance. As University Librarian I was at that
		time primarily interested in exploring issues which were closely related to practice. Thus the very first
		project, a study funded by the then British Library Research &amp; Development Department, investigated the
		information-seeking and information-using practices of students following a specific mode of distance learning
		known in the UK as 'franchising'. Our interest stemmed from the observation that such students appeared to be
		struggling to access adequate resources to underpin their undergraduate studies. The project produced results
		which were both interesting and useful, leading to a new service called <a
			href="http://www.uclan.ac.uk/library/valnow/">VALNOW (the Virtual Academic Library of the North-West)</a>
		being designed and deployed, and also leading indirectly to CERLIM's first major research grant from the
		European Commission. Little did we realise at that time that the various EC Framework Programmes would form the
		largest source of research funding for CERLIM for the next ten years.</p>
	<p>CERLIM was also an active player in the UK's <a href="http://www.ukoln.ac.uk/services/elib/">Electronic Libraries
			Programme</a>, funded by the <a href="http://www.jisc.ac.uk/">Joint Information Systems Committee
			(JISC)</a>, and it was during this time that that opportunity arose to relocate from a University Library to
		an academic department. Although Preston and Manchester are no more than 40 miles apart, this was a very
		considerable culture change for us all and has certainly led to the research element of our R&amp;D gaining
		heightened prominence. Naturally, the focus of this research has changed over the years, but it has remained
		embedded in the exploration of real-world issues associated with the organisation, delivery and use of
		information in organisational and personal contexts. Thus CERLIM is active in research which explores, <em>inter
			alia</em>:</p>
	<ul>
		<li>organisational, social, cultural and technical issues in the development of library and information systems
			in networked, distributed spaces. There has been an increasing emphasis on strategic evaluation of
			national-level services and on the question of their impact.</li>
		<li>the user perspective on library and information services, an area which includes information literacy, user
			satisfaction and quality management (bearing in mind that quality is 'fitness for the purpose of the user')
			and which includes a specific interest in accessibility and visual impairment.</li>
		<li>information retrieval, again from a user rather than a systems perspective</li>
		<li>cultural heritage in networked environments, including an emphasis on the ability of ordinary citizens to
			become active contributors to cultural heritage creation.</li>
	</ul>
	<p>CERLIM has always worked in partnership with other organisations and of course European Commission work has often
		required these partnerships to be multinational. Our first non-UK partners were in Ireland and Greece, the
		latter providing us with a longstanding relationship with the University of the Aegean in Lesvos, where our
		biennial <a href="http://www.cerlim.ac.uk/conf/lww5/welcome.html"><em>Libraries without Walls</em>
			conference</a> is held.</p>
	<p>In addition to national and international collaborations, there is a very close working relationship between
		CERLIM staff and other staff of the Department of Information &amp; Communications. The Department's wide range
		of interests means that there are frequent opportunities to work together. This extends to research students who
		may find synergies with the work of CERLIM and are able to draw on a wide range of expertise. The papers in this
		issue cover all of these kinds of collaboration.</p>
	<h2 id="this-issue">This Issue</h2>
	<p>The first paper, which I have written as Director of CERLIM, illustrates the way in which research questions
		arise and start to be addressed: it describes a possible new sub-field of enquiry into the retrieval of
		multimedia objects. The origins of this interest actually came out of work on accessibility. We were exploring
		the different ways in which electronic information can be made accessible to people with visual impairments. One
		possibility was to use complex tagging of the elements of an object, including end-user defined fields, so that
		navigation did not have to be linear - the analogy with an audio tape which if untagged has to be listened to
		from start to finish is appropriate. If every sentence or even every spoken word could be tagged meaningfully, a
		blind person could immediately select the parts of interest. Technically. a number of groups were examining the
		use of multimedia synchronisation protocols for this purpose, most notably the <a
			href="http://www.daisy.org/">DAISY consortium</a>. The approach has indeed proved valuable and DAISY devices
		are now in widespread use. However, our thinking led us to wonder whether the underlying protocols and standards
		might not be exploited as a way to enhance the retrieval of objects embedded within multimedia presentations.
		Since the relevant standard defines where two objects are to be displayed simultaneously - say an image and an
		audio track - it might be possible to infer retrieval relevance of the second object from that of the first, and
		<em>vice versa</em>. The paper published here,  <em>Synchronised Object Retrieval: the enhancement of
			information retrieval performance in multimedia environments using synchronisation protocols</em>, reports
		on initial exploration of the potential of this approach - and suggests that this could indeed be a fruitful
		line of enquiry.</p>
	<p>In <em>Task dimensions of user evaluations of information retrieval systems</em> Frances Johnson, Jill Griffiths
		and Dick Hartley report on a project designed to explore the development of a framework for the evaluation of IR
		systems from a user perspective. The focus of the study was Internet search engines, but the authors postulated
		that users' evaluations will vary depending on both the user and the system features and that a multidimensional
		approach is therefore needed. Their preliminary findings support their hypothesis and they point to the need for
		larger-scale studies to explore this approach further.</p>
	<p>Jill Griffiths then contributes a paper derived from one of the constituent studies of CERLIM's 3-year <a
			href="http://www.cerlim.ac.uk/edner/welcome.html">Evaluation of the Distributed National Electronic Resource
			(EDNER)</a> project, funded by the UK's Joint Information Systems Committee (JISC) and designed to provide
		formative evaluation of the development of the DNER, which has now become known as the <a
			href="http://www.jisc.ac.uk/index.cfm?name=about_info_env">JISC Information Environment (IE)</a>. In this
		particular study students were given a variety of tasks which required the use of electronic services, including
		those provided by the JISC, and they rated these in accordance with 'quality attributes'. The results of the
		study shed light not just on JISC services but also on the ways in which this methodology might be modified for
		future use. A particularly interesting finding was the identification of much confusion in students' thinking
		about the concept of 'quality' in networked information environments.</p>
	<p>A second study from within the EDNER project is reported by Margaret Markland, who explored the ways in which
		university staff are using information resources within virtual learning environments (VLEs). This showed that
		staff are keen to embed resources in their online courses but are encountering many of the problems, such as
		persistent addressing and consistent resource description, that have become familiar to librarians and other
		information professionals. As yet there is little evidence of effective cooperation between the two groups.</p>
	<p>CERLIM's long-term interest in how users with visual impairments can operate effectively in electronic
		environments is represented by Jenny Craven's report on the <em>Non-visual access to the digital library</em>
		project. This work has provided evidence to support what was previously an hypothesis that such users are forced
		to interact with services for far longer than sighted users in order to generate useful results. It also
		revealed that the application of some fairly simple design principles - currently largely ignored - could make
		web sites far easier for visually-impaired people to access.</p>
	<p>The final paper in this issue is by one of our research students, Sirje Virkus, who is undertaking research into
		information literacy. In this paper she surveys the field within a European context and demonstrates that while
		the concept appears to be widely recognised there is no agreement on what it encompasses. In many countries
		information technology and basic IT skills are emphasised with little attention paid to an individual's ability
		to access and use information content. Sirje will be developing a model for the effective delivery of
		information literacy in open and distance learning during the next stage of her research.</p>
	<h2 id="conclusion">Conclusion</h2>
	<p>Taken together these papers provide a sense of the kinds of research which CERLIM undertakes. They are not, and
		could not have been, a comprehensive overview of the whole of our current work, let alone the history of our
		involvement in the field of library and information research. As CERLIM celebrates it's tenth anniversary we
		look forward to the next decade of challenging yet fascinating research. When we began in 1993 the World Wide
		Web was just starting to emerge into general use - who knows what innovation will be surfacing in 2013?</p>
	<h5 id="peter-brophy">Peter Brophy</h5>
	<p>Director of CERLIM and Professor of Information Management<br>
		Manchester Metropolitan University<br>
		July 2003</p>

</body>

</html>