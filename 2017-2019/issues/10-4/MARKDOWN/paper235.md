#### Vol. 10 No. 4, July 2005

# Information and knowledge management: dimensions and approaches

#### [Christian Schlögl](mailto:christian.schloegl@uni-graz.at)  
Institute of Information Science  
University of Graz  
Graz, Austria

#### Abstract

> **Introduction**. Though literature on information and knowledge management is vast, there is much confusion concerning the meaning of these terms. Hence, this article should give some orientation and work out the main aspects of information and knowledge management.  
> **Method**. An author co-citation analysis, which identified the main dimensions of information management, forms the basis of the study of literature. In it, the main aspects of information management are further refined. Furthermore, it will be investigated if the concept of knowledge management adds anything to information management, and if so what it is.  
> **Analysis**. Data for analysis were retrieved from Science Citation Index and Social Science Citation Index. Though the literature review is based on the quantitative results of the bibliometric analysis and has tried to consider the most prominent publications, some degree of subjectivism cannot be excluded.  
> **Results**. As a result of analysis, a distinction can be made between content and technology-oriented information management approaches. According to the literature review, technology-oriented information management includes data management, information technology management and strategic information technology management. The main emphasis of these approaches is the effective and efficient use of information technology. In contrast, content-oriented approaches focus on information and its use. They can be distinguished in records management, provision of external information, human-centered information management, and information resources management. The reading of the literature on knowledge management reveals, that this term is either used synonymously for information management or for the _management of work practices_ with the goal of improving the generation of new knowledge and the sharing of existing knowledge.  
> **Conclusions** This article identifies various aspects that are embraced by the terms _information management_ and _knowledge management_. Thus, it should contribute to more terminological clarity and finally improve communication both in science and in professional practice.


## Introduction

Information management has been around for more than two decades. Many authors date its beginning back to the Paperwork Reduction Act of 1980 in which U.S. federal agencies were forced to introduce information resource management. Regardless of its exact origins, there was a substantial growth in literature dealing with this topic at the beginning of the eighties. In the second half of the nineties, the term knowledge management became more popular.

Though literature on information and knowledge management is expansive, there is much confusion concerning the meaning of these terms. This is the starting point of this article the aim of which is to identify the main dimensions and major approaches of information and knowledge management. This should also give answers to the question, 'Does the concept of knowledge management add anything and, if so, what is it?'

## Mapping the literature of information management

In order to cope with the voluminous literature on information management, a bibliometric study was performed ([Schlogl 2003](#Schloegl_2003)). By means of an author co-citation analysis of data from Science Citation Index and Social Science Citation Index, the intellectual structure of information management was mapped (see Figure 1).

<div align="center">![fig1](p235fig1.jpg)</div>

<div align="center">Figure 1: Map of information management (source: [Schlögl 2003](#Schloegl_2003))</div>

In order to select the authors, all publications including either the phrase _information management_ or _information resource(s) management_ in the title were retrieved. The following DIALOG command was used: SELECT ((INFORMATION () MANAGEMENT (NOT W) SYSTEM?) OR (INFORMATION () RESOURCE? () MANAGEMENT)) /ART /TI. The authors who received most citations from these information management articles constituted the basis for co-citation analysis. (Problems which are associated with bibliometric studies in general and with a co-citation analysis covering multiple disciplines in particular are discussed extensively by Schloegl ([2000](#Schloegl_2000)).) For mapping the authors, a statistical method called multidimensional scaling was used. The calculations were done by means of the ALSCAL program which is included in SPSS.

Multidimensional scaling locates the authors in two or more dimensional spaces based on their similarity (number of co-citation counts). The arrangement of the authors is made as follows:

*   authors who are co-cited many times are grouped close together.
*   authors with many links to others tend to be in a central position, while authors with only a few links will be placed more peripherally.

Besides providing an information-rich display of the co-citation links, the second main use of multidimensional scaling is to identify the salient dimensions underlying their placement ([McCain 1990](#McCain)). On the vertical axis this is the extent to which the authors worked on the topic _information management_. On the top, there are scholars from management science and business administration. These authors did not focus their research on information management itself, but others used their work as a foundation, especially in the field of information systems. On the bottom, there are those authors whose work concentrated primarily on information management. These authors published the first books on this topic. They can be referred to as the _classical_ writers on information management.

The horizontal axis indicates the different subject dimensions. Authors on the left can be assigned to the information sciences. Their research interest lies in information and its use. On the right, there are those authors whose research topic is the efficient and effective use of information technology (IT). The related disciplines are information systems and business informatics.

Interestingly, no authors were placed in the centre of the map. This might suggest that information management is not an interdisciplinary but (only) a multidisciplinary topic, especially as there is no collaboration between researchers from information systems and those from information sciences. Ellis _et al._ ([1999](#Ellis)) arrive at a similar conclusion after having analysed the literature on information retrieval and user studies. They conclude that there is a 'lack of contact' between these two disciplines.

According to these interpretations, a distinction can be made between content-oriented and technology-oriented information management. In the following sections, various approaches will be discussed for each of these two dimensions of information management. Then the concept of knowledge management will be investigated, in particular its relationship to information management.

## Technology-oriented information management

The general purpose of information management is to make available the right information at the right time and at the right place. For technology-oriented information management, computer-based information systems are the primary means to this end. Information management from this angle stresses the importance of information technologies. This is justified by the high degree of IT integration, the complexity of its application, and its strong consequences for an organization.

Within technology-oriented information management, the following aspects can be distinguished:

*   data management,
*   IT management, and
*   strategic use of IT.

### Data management

Some authors equate information management with data management (e.g., [Hoven 1995](#Hoven_1995)). This is due to the high importance of data. Data is needed for and produced by nearly every activity and is an important input for almost all decisions on each level of an enterprise ([Levitin & Redman 1998](#Levitin)).

In a broader sense, data management can be defined as all organizational and technical tasks concerning the planning, storage, and provision of data, both for computer personnel and end-users ([Schulte 1987](#Schulte)). Its goal is to maximize the quality, usability, and value of the data resources in an enterprise ([Hoven 1995](#Hoven_1995)). In the literature, often two components are suggested for data management: data administration and database administration ([Gilllenson 1985](#Gillenson); [Henderson 1987](#Henderson); [Lytle 1986](#Lytle_1986)). Data administration primarily serves a planning and analysis function. It may be responsible for data planning, accountability, policy development, standards setting, and support. One of the major tasks includes the design of the data architecture of an organization. Database administration provides a framework for managing the data on an operational level. Its role may include performance monitoring, troubleshooting, security monitoring, physical database design, and data backup.

### IT management

There is a broader view of information management that data management is a part of or a precondition for information management ([English 1996](#English); [Heinrich 2002](#Heinrich); [Henderson 1987](#Henderson); [Schwarze 1988](#Schwarze)). The management of hardware, software, and IT personnel must be included as well. The emphasis here is on the technological aspects of electronic data processing.

One well-structured example for these approaches was suggested by Wollnik ([1988](#Wollnik)). His model distinguishes three levels (see Figure 2): the management of information use (upper level), the management of information systems (middle level), and the management of the information infrastructure (lower level).

<div align="center">![3-level model of information management](p235fig2.gif)</div>

<div>Figure 2: Three-level model of information management (source: [Wollnik 1988](#Wollnik))</div>

Information infrastructures provide means for all possible types of data processing open to different use (e.g., computers, networks). They constitute the basis for the information systems that, in contrast, support specific tasks of an organization. The information systems provide, in particular, the means for information use and exchange. The main role of information systems management is the management of the development and operation of an organization's information systems. Information management from this view can be defined as the planning, organization, and control of information use, information systems, and information infrastructure in an organization.

### Strategic use of information technology

The use of information technology as a strategic resource has attracted much attention especially in Anglo-American countries in the past. Many publications on this topic deal with the strategic relevance of information processing. These publications explore the question whether and to what extent information technology can contribute to the objectives of an organization ([Krueger & Pfeiffer 1988](#Krueger); [McFarlan 1984](#McFarlan); [Porter & Millar 1985](#Porter)). Some authors were concerned with models to support strategic applications ([Ives & Learmonth 1984](#Ives); [Porter & Millar 1985](#Porter); [Rockart & Crescenzi 1984](#Rockart); [Wiseman 1985](#Wiseman)). A comprehensive survey about examples for the competitive use of information technology is given by Mertens and Plattfaut ([1986](#Mertens_1986)) for instance. While the first publications were very "enthusiastic", there has been some disillusionment in latter years. Several authors are now concerned with the question of whether an IT-based competitive advantage can be maintained for a longer time ([Hatten & Hatten 1997](#Hatten); [Kettinger, _et al._ 1994](#Kettinger); [Mata, _et al._ 1995](#Mata); [Miron, _et al._ 1988](#Miron); [Ross, _et al._ 1996](#Ross)). Some authors doubt whether this is possible at all (e.g., [Sutherland 1991](#Sutherland)).

A strategic orientation should go along with an increased valuation of information processing in an organisation. This should be met by the creation of a new post at strategic management level: the chief information officer ([Synnott 1987b](#Synnott_1987b)). The main purpose behind this is to ensure that information processing is coordinated with corporate objectives.

Strategic aspects play an important role in nearly all concepts that are concerned with the management of information technology. For instance, the title of the book by Synnott ([1987a](#Synnott_1987a)) indicates the significance of strategic aspects in his approach. This applies to O'Brien and Morgan ([1991](#OBrien)) as well as to Lewis and Martin ([1989](#Lewis)) who view strategy as a fundamental characteristic of information [technology] management. Pietsch, _et al._ ([1998](#Pietsch)) have even called their approach _strategic information [technology] management_.

## Content-oriented information management

Authors in this area of information management usually have a background in library and information science, records management, or a closely related discipline. Contrary to technology-oriented information management, these approaches focus on information content.

Publications can be classified here as follows:

*   records management,
*   provision of external information,
*   human-centred information management, and
*   information resources management. 

### Records management

According to Savic ([1992](#Savic)) and Trauth ([1989](#Trauth)), records management is one of the oldest information management disciplines. This claim was renewed by the records management community when renaming the most important international journal in the field from _Records Management Quarterly_ to [_The Information Management Journal_](http://www.arma.org/imj/index.cfm). In a highly respected book in this field, records management is described as a discipline which is concerned with the management of document-based information systems ([Robek, _et al._ 1996](#Robek)).

The main goals of records management are:

*   to furnish accurate, timely, and complete information in order to enable efficient decision making processes;
*   to process recorded information as efficiently as possible;
*   to provide information and documents at the lowest cost;
*   to render the maximum utility to the users of documents;
*   to dispose of records which are no longer needed ([Robek, _et al._ 1996](#Robek)).

As can be seen, the main feature of records management is the management of the information life cycle. According to Robek, _et al._ ([1996](#Robek)), it consists of production, dissemination and use, storage and provision for current access, decisions on the retention/destruction, and archiving of documents.

Contrary to data management, the emphasis of records management lies in textual, document-based information (memos, reports, letters, etc.) ([Noot 1998](#Noot)) originally recorded on paper. Nevertheless recent developments in information technologies have caused significant changes in records management. Correspondingly, the notion of a document acquired a wider meaning. Records management deals less and less with documents recorded on paper. The management of electronic documents has become more central instead using support systems like intranets, electronic imaging systems, and workflow management systems.

### Provision of external information

Content-oriented information management has a much stronger focus on the provision of external information ([Huebner 1996](#Huebner)). In some publications only the provision of external information (from databases) ([Kind 1986](#Kind); [Meik 1997](#Meik)) or information and documentation ([Kroll 1990](#Kroll)) are related to information management. For other authors, the provision of external information is an important part of information management (e.g. [Gazdar 1989](#Gazdar); [Kmuche 1997](#Kmuche)).

Kuhlen and Finke ([1988](#Kuhlen)) claim that external information concerning changes in relevant segments of the environment is much more important for the success of an organization than the management of information technology. Choo ([1998](#Choo)) has similar arguments. According to him, the survival of a company depends on how well it processes information about its environment and, as a result, succeeds in adapting efficiently to environmental changes. This also shows the importance of external information for strategic planning ([Anthony 1988](#Anthony)).

### Human-centered information management

Humans play a much more important role in content-oriented management approaches ([Grudowski 1995](#Grudowski); [Taylor 1986](#Taylor); [Wang 1998](#Wang); [Wersig 1989](#Wersig); [Zijlker 1988](#Zijlker)). For instance, Wersig's approach to information management does not centre on information technology or formal theories but on how humans handle information in reality ([Grudowski 1995](#Grudowski)). While it is typical for technology-oriented information management approaches to model information systems in a very formal way (entity relationship diagrams, data flow diagrams, etc.), content-oriented concepts use much less formalistic methods (e.g., [Cronin & Davenport 1991](#Cronin); [Wersig 1989](#Wersig) ). This has also to do with the fact that the term information system has a much wider meaning, which is not only restricted to computers ([Bergeron 1996](#Bergeron)). For this reason, it is not the goal to thoroughly automate and completely formalize information processes ([Cronin & Davenport 1991](#Cronin)). According to Wersig ([1989](#Wersig)), order should only be established for circumstances where it is essential; allowing creative solutions with their inherent qualities to unfold from what may appear to be chaos.

Some approaches do not only consider information handling and/or information behaviour of individuals but of the organization as a whole. They also include an information culture that according to Davenport ([1997](#Davenport_1997)) results from the total information behaviour of the organization's staff. Schneider ([1990](#Schneider)) even calls the approach developed by her 'culture-conscious information management'. It takes into consideration that information management is embedded into a certain organization with a specific history and based on general assumptions that influence the behaviour of its staff.

### Information resources management

A series of authors mainly from the information sciences take a holistic perspective on information management integrating more or less all the aspects discussed above. Although there is not a well established term, these approaches are referred to as information resources management in the following. According to Bergeron ([1996](#Bergeron)), IRM is grounded in the following assumptions:

*   recognition of information as a resource;
*   an integrative management perspective;
*   management of the information life cycle;
*   a link with strategic planning.

One important feature of information resources management consists is that it is a framework that seeks to integrate different information professionals and functions under one umbrella ([Bergeron 1996](#Bergeron); [Levitan 1982](#Levitan_1982); [Lytle 1986](#Lytle_1986); [Miller 1988](#Miller); [Otremba 1987](#Otremba); [Vickers 1985](#Vickers); [Wiggins 1988](#Wiggins); [Wilson 2002](#Wilson02) ). However there are different points of view on which resources are to be managed ([Vogel 1992](#Vogel)). In a wide sense, information resources management can be defined as the management of those resources (human and physical) that are concerned with the systems support (development, enhancement, maintenance) and the servicing (processing, transformation, distribution, storage, and retrieval) of information ([Schneyman 1985](#Schneyman)). Marchand and Horton ([1986](#Marchand_1986)) distinguish between information resources and information assets. Information resources include information specialists; information technology; facilities like the library, the data processing department, and the information centre; and external information brokers. Information assets cover all the formal information holdings of an organization (data, documents, technical literature), know-how (rights on intellectual property, practical experience of staff), as well as knowledge about the environment (competitors; political, economic, and social environment). While the information assets concern information itself, the information resources are a means by which information can be gained. Lytle ([1988](#Lytle_1988)) has a somewhat narrower view. He differentiates between data, hardware, software, information systems and services, as well as personnel.

## Knowledge management

Since the mid-nineties the label _knowledge management_ has attracted much attention ([Disterer, _et al._ 2002](#Disterer); [Ponzi & Koenig 2002](#Ponzi_Koenig)) while information management has been used less. As with information management, there is no agreement on what constitutes knowledge management ([Corrall 1998](#Corrall); [Morrow 2001](#Morrow); [Wilson 2002](#Wilson)). According to Wilson ([2002](#Wilson)), knowledge management is either used as a synonym for information management or for the 'management of work practices' which are to improve the sharing of knowledge in an organization.

The first perspective includes both content and technology-oriented information management. In the case of IT driven approaches there is a stronger focus on unstructured data ([Davenport & Prusak 1998](#Davenport_Prusak); [Hoven 2001](#Hoven_2001)) and on different application areas of technology. They typically fall into the categories of knowledge databases and repositories (e.g., expertise of the after-sales staff), knowledge route maps and directories (e.g. yellow pages), and knowledge networks and communication tools (e.g., chat facilities, groupware) ([Corrall 1998](#Corrall)). Furthermore there is a shift in the underlying technology. Maurer ([2003](#Maurer)) distinguishes a knowledge management system from a conventional information system in that it has the following additional features: (i) it makes accessible private information that is of no use anymore to its creator but probably for others (e.g., former project documentation makes it possible to retrace decision processes), (ii) it learns from the users' use (e.g., many users who search x seek for y as well), (iii) it can initiate actions and provide information without any request by the user (e.g., if a user retrieves x, y is offered to him automatically), (iv) it can generate new information from existing (e.g., automatic classification).

One main shortcoming of the technologist's view is that it assumes that knowledge can be codified which is not usually the case ([Ponzi 2002](#Ponzi)). Though technology can play an important role, knowledge management is not about technology ([Al-Hawamdeh 2002](#Al-Hawamdeh)). This leads to the discussion of the innovative aspects of knowledge management. But, first, the meaning of the term _knowledge_ needs explaining. This article follows the definition by Wilson who defines knowledge as what a person knows. He states that, 'knowledge involves the mental processes of comprehension, understanding and learning that go on in the mind and only in the mind' ([Wilson 2002](#Wilson)). By contrast information is bound to a medium (which exists outside the mind). Before information can become knowledge, it must be incorporated into the knowledge structure of the particular human. This makes clear that knowledge cannot be managed. Rather, knowledge management is 'concerned with the management of work practices' ([Wilson 2002](#Wilson)) with the goal of improving the sharing of information (and, thereby, knowledge) in an organization. In line with this, knowledge management means the installation of purposeful management processes that enable to capture personal and contextual knowledge ([Broadbent 1998](#Broadbent)) and thus permanently make accessible the staff's creativity for an organization ([Scheid 2001](#Scheid)). One of the most crucial factors is the establishment of a knowledge-friendly culture ([Davenport, _et al._ 1998)](#Davenport_DeLong). Especially with regards to knowledge creation and transfer, flexible structures like personal networks or communities of practice (e.g., [Brown & Duguid 1998](#Brown)) are important. The high claims of knowledge management can only be realized, if at all, after a long learning process. Therefore, organizational learning (e.g., [Argyris 1991](#Argyris); [Garvin 1993](#Garvin); [Senge 1990](#Senge)) is an integral part of knowledge management.

As can be seen from what was mentioned above, knowledge management is predominantly an organizational sciences construct ([Ponzi 2002](#Ponzi)). However, knowledge management has also a strong link to business strategy ([Broadbent 1998](#Broadbent)), in that its proponents claim that it should enable an organization to cope effectively with rapid environmental change and to attain some form of competitive advantage ([Morrow 2001](#Morrow)).

Intellectual capital management is a closely related concept that is based on the view that in the information era the real market value of a company does not consist only of its physical and financial assets (book value) but more and more of its intangible assets. According to Corrall ([1998](#Corrall)) the choice between intellectual capital management and knowledge management depends on the emphasis given to measuring or managing knowledge assets. There were several models developed to measure intellectual capital (e.g. [Edvinsson & Malone 1997](#Edvinsson); [Kaplan & Norton 1992](#Kaplan); [Stewart 1997](#Stewart); [Sveiby 1997](#Sveiby)). Basically, these models classify the intangible _capital_ into the categories "customer capital" (e.g. customer relations, reputation, brands), "structural capital" (patents, business processes, culture, documented experience), and "human capital" (staff with their skills and experiences) ([Ponzi 2002](#Ponzi)). As with knowledge management, the capabilities of the staff, especially their knowledge and experiences (human capital), should be converted to structural [and customer] capital and thus avoid a loss of the corporate memory ([Corrall 1998](#Corrall)).

## Conclusions

This article has identified three major categories in the literature on information and knowledge management. They were referred to as technology-oriented information management, content-oriented information management, and knowledge management.

From a narrow point of view, data management is equated with information management. Even though information management is perceived as more comprehensive by nearly all authors, data management is one essential component of technology-oriented information management. IT management comprises of the planning, organization, and control of those tasks that are necessary for the provision and use of the information infrastructure as well as the (computer-based) information systems that are the basis for the information exchange in an organization. If possible the competitiveness of a company should be improved or at least its deterioration be prevented.

Content-oriented approaches are primarily concerned with the management of (codified) information. This relates both to the management of the information life cycle of mainly internally produced information and the provision of external information. Usually, they consider the user much more than technology-oriented approaches. Several authors who directed a particularly strong attention to humans were therefore attributed to their own sub-category. Strategic aspects are not dealt with as a central issue but are not of minor importance either. The emphasis is on external information and not on the competitive use of information technology. Information resources management has finally tried to integrate all the different aspects including the management of information technology creating a convergent view of information management.

If one does not consider its synonymous use for information management, knowledge management means the management of those work practices that aim at improving the generation of new and the sharing of existing knowledge. Some aspects of knowledge management were already partly dealt with by some authors of content-oriented approaches. However, the presented concepts were much less elaborated in this respect and mainly focused on behavioral aspects of information use. Knowledge management has a wider perspective working toward raising the creativity of the organization's staff. A few authors even developed some models for measuring the intellectual capital of an organization.

Table 1 summarizes the major aspects of information and knowledge management. It is mainly intended to serve as an orientation aid for the vast literature on this topic. Furthermore it should give a rough assessment of which disciplines (and professions) are engaged in which aspects of information and knowledge management. Accordingly, information systems and business informatics can be attributed to the management of information technology. Organizational and management sciences deal primarily with knowledge management (in a narrow sense). The management of (codified) information and the study of information use are the domains of records management and library and information science.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Information and knowledge management: objects, terms and related disciplines**</caption>

<tbody>

<tr>

<th rowspan="2" colspan="2">Objects</th>

<th rowspan="1" colspan="2">Terms</th>

<th rowspan="2" colspan="1">Disciplines</th>

</tr>

<tr>

<th>Narrower terms</th>

<th>Broader terms</th>

</tr>

<tr>

<td style="vertical-align: middle;" rowspan="3" colspan="1">information technology</td>

<td style="vertical-align: middle;">data (structure)</td>

<td style="vertical-align: top;">data management</td>

<td style="vertical-align: middle;" rowspan="3" colspan="1">IT management (technology-oriented information management)</td>

<td style="vertical-align: middle;" rowspan="3" colspan="1">information systems  

business informatics  
</td>

</tr>

<tr>

<td style="vertical-align: middle;">information system  
</td>

<td style="vertical-align: top;">information systems management  
</td>

</tr>

<tr>

<td style="vertical-align: middle;">information infrastructure  
</td>

<td style="vertical-align: top;">management of information infrastructure  
</td>

</tr>

<tr>

<td style="vertical-align: middle;" rowspan="2" colspan="1">(codified) information  
</td>

<td style="vertical-align: middle;">internal  
</td>

<td style="vertical-align: middle;">records management  
</td>

<td style="vertical-align: middle;" rowspan="2" colspan="1">(content-oriented) information management  
</td>

<td style="vertical-align: middle;" rowspan="2" colspan="1">records management  

library and information science</td>

</tr>

<tr>

<td style="vertical-align: middle;">external  
</td>

<td style="vertical-align: middle;">provision of external information  
</td>

</tr>

<tr>

<td style="vertical-align: middle;" rowspan="1" colspan="2">work practices that relate to knowledge generation and sharing  
</td>

<td style="vertical-align: top;"> </td>

<td style="vertical-align: middle;" rowspan="2" colspan="1">knowledge management  
</td>

<td style="vertical-align: middle;" rowspan="2" colspan="1">organizational sciences  

management sciences  
</td>

</tr>

<tr>

<td style="vertical-align: middle;" rowspan="1" colspan="2">intellectual assets  
</td>

<td style="vertical-align: top;">intellectual capital management  
</td>

</tr>

</tbody>

</table>

As already mentioned, the terms _information management_ and _knowledge management_ are used very inconsistently in theory and in practice. This is due to ignorance and _tactical_ reasons. The latter holds true for consultants and IT firms trying to market often _old_ services and products under a new label ([Wilson 2002](#Wilson)). However, this is also partly valid for science. In a scientometric analysis, Mertens ([1995](#Mertens_1995)) demonstrated how much business informatics suffers from emerging fashions that often go along with unnecessary changes of terms. For instance, this applies to issues like know-how databases and business intelligence which are investigated once again within the scope of knowledge management. Such scholarly behaviour could threaten a continuous accumulation of knowledge in the particular field in the long run. In this sense, the author hopes that this article will not only contribute to more terminological clarity but have further positive implications.

## Acknowledgement

The author would like to thank Mr. Norbert Berger from the University of Graz and Ms. Sheila French from Manchester Metropolitan University for proof-reading, Mr. Gunter Bauer for the technical assistance when creating this Web document, and the referees for their constructive comments.

## References

*   <a name="Al-Hawamdeh" id="Al-Hawamdeh"></a>Al-Hawamdeh, S. (2002). [Knowledge management: re-thinking information management and facing the challenge of managing tacit knowledge](http://InformationR.net/ir/8-1/paper143.html). _Information Research_, **8**(1). Retrieved 8 March, 2005 from http://InformationR.net/ir/8-1/paper143.html.
*   <a name="anthony1988" id="anthony1988"></a>Anthony, R. (1988). _The management control function_. Boston, MA: Harvard Business School Press.
*   <a name="Argyris" id="Argyris"></a>Argyris, C. (1991). Teaching smart people how to learn. _Harvard Business Review,_ **69**(3), 99-109.
*   <a name="Bergeron" id="Bergeron"></a>Bergeron, P. (1996). Information resources management. _Annual Review of Infor­mation Science and Technology_, **31**, 263-300.
*   <a name="Broadbent" id="Broadbent"></a>Broadbent, M. (1998, May). [The phenomenon of knowledge management: what does it mean to the information profession?](http://www.sla.org/pubs/serial/io/1998/may98/broadben.html) _Information Outlook_, 23-31\. Retrieved 20 May, 2005 from http://www.sla.org/pubs/serial/io/1998/may98/broadben.html
*   <a name="Brown" id="Brown"></a>Brown, J. & Duguid, P. (1998). Organizing knowledge. _California Management Review_, **40**(3), 90-111.
*   <a name="Choo" id="Choo"></a>Choo, C. W. (1998). _Information management for the intelligent organisation: the art of scanning the environment_. Medford, NJ: Information Today Inc.
*   <a name="Corrall" id="Corrall"></a>Corrall, S. (1998). [Knowledge management: are we in the knowledge management business?](http://www.ariadne.ac.uk/issue18/knowledge-mgt/) _Ariadne_, (18). Retrieved 8 March, 2005 from http://www.ariadne.ac.uk/issue18/knowledge-mgt/
*   <a name="Cronin" id="Cronin"></a>Cronin, B. & Davenport, E. (1991). _Elements of information management._ Metuchen, NJ: Scare­crow Press
*   <a name="Davenport_1997" id="Davenport_1997"></a>Davenport, T. (1997). _Information ecology: mastering the information and knowledge environment_. New York, NY: Oxford University Press.
*   <a name="Davenport_DeLong" id="Davenport_DeLong"></a>Davenport, T.H., De Long, D.W. & Beers, M. C. (1998). Successful knowledge management projects. _Sloan Management Review_, **39**(2), 43-57.
*   <a name="Davenport_Prusak" id="Davenport_Prusak"></a>Davenport, T. & Prusak, L. (1998). _Working knowledge: how organizations manage what they know._ Boston, MA: Harvard Business School Press.
*   <a name="Disterer" id="Disterer"></a>Disterer, G., Krystofiak, S. & Blitzer, M. (2002). Informationen zum Wissensmanagement im Inter­net. _Wirtschaftsinformatik_, **45**(5), 547-554.
*   <a name="Edvinsson" id="Edvinsson"></a>Edvinsson, L. & Malone, M. (1997). _>Intellectual capital: realizing your company’s true value by finding its hidden brainpower_. New York, NY: HarperBusiness.
*   <a name="Ellis" id="Ellis"></a>Ellis, D., Allen, D., & Wilson, T.D. (1999). Information science and information systems: conjunct subjects, disjunct disciplines. _Journal of the American Society for Information Science and Technology_, **50**(12), 1095-1107.
*   <a name="English" id="English"></a>English, L.P. (1996). Redefining information management. _Information Systems Management_, **13**(1), 65-67.
*   <a name="Garvin" id="Garvin"></a>Garvin, D. (1993). Building a learning organization. _Harvard Business Review_, **71**(4), 78-91.
*   <a name="Gazdar" id="Gazdar"></a>Gazdar, K. (1989). _Informationsmanagement fuer Fuehrungskraefte: Konkrete Perspektiven fuer Wirtschaft, Verwaltung und Politik_. Frankfurt: Frankfurter Allgemeine Zeitung.
*   <a name="Gillenson" id="Gillenson"></a>Gillenson, M. (1985). Trends in data administration. _MIS Quarterly_, **9**(4), 317-325.
*   <a name="Grudowski" id="Grudowski"></a>Grudowski, S. (1995). _Informationsmanagement und Unternehmenskultur_. Unpublished PhD dissertaton, Freie Universitaet Berlin.
*   <a name="Hatten" id="Hatten"></a>Hatten, M.L. & Hatten, K.J. (1997). Information systems strategy: long overdue - and still not here. _Long Range Planning_, **30**(2), 254-266.
*   <a name="Heinrich" id="Heinrich"></a>Heinrich, L.J. (2002). _Informationsmanagement._ (7th ed.). Munich: Oldenbourg.
*   <a name="Henderson" id="Henderson"></a>Henderson, M.M. (1987). The importance of data administration in information management. _Information Management Review_, **2**(4), 41-47.
*   <a name="Hoven_1995" id="Hoven_1995"></a>Hoven, J. van den, (1995). Information resources management: an enterprise-wide view of data. _Information Systems Management_, **12**(3), 69-72.
*   <a name="Hoven_2001" id="Hoven_2001"></a>Hoven, J. van den (2001). Information resource management: foundation for knowledge management. _Information Systems Management_, **18**(2), 80-83.
*   <a name="Huebner" id="Huebner"></a>Huebner, H. (1996). _Informationsmanagement und strategische Unternehmensfuehrung_. Munich: Oldenbourg.
*   <a name="Ives" id="Ives"></a>Ives, B. & Learmonth, G.P. (1984). The information system as a competitive weapon. _Communications of the ACM_, **27**(12), 1193-1201.
*   <a name="Kaplan" id="Kaplan"></a>Kaplan, R. & Norton, D. (1992). The balanced scorecard: measures that drive performance. _Harvard Business Review_, **70**(1), 71-79.
*   <a name="Kettinger" id="Kettinger"></a>Kettinger, W., Grover, V., Guha, S. & Segars, A.H. (1994). Strategic information systems revisited: a study in sustainability and performance, _MIS Quarterly_, **18**(1), 31-58.
*   <a name="Kind" id="Kind"></a>Kind, J. (1986). Besseres Informationsmanagement durch externe Datenbanken. _Office Management_, (5), 490-492.
*   <a name="Kmuche" id="Kmuche"></a>Kmuche, W. (1997). Informationsmanagement: chancenreiches und zukunftsrelevantes Taetigkeitsfeld mit neuen Anforderungen an die Ausbildung. _Nachrichten für Dokumentation_, **48**(3), 151-157.
*   <a name="Kroll" id="Kroll"></a>Kroll, H. (1990). _Informationsvermittlung in der Industrie_. Cologne: RKW-Verlag.
*   <a name="Krueger" id="Krueger"></a>Krueger, W. & Pfeiffer, P. (1988). Strategische Ausrichtung, organisatorische Gestaltung und Auswirkungen des Informationsmanagements. _Information Management_, (2), 6-15.
*   <a name="Kuhlen" id="Kuhlen"></a>Kuhlen, R. & Finke, W. (1988). Informationsressourcen-Management: Informations- und Technologiepotentiale professionell für die Organisation verwerten. _Zeitschrift für Führung und Organisation_, **57**(5), 314-323.
*   <a name="Levitan_1982" id="Levitan_1982"></a>Levitan, K.B. (1982). Information resource(s) management – information resources management. _Annual Review of Information Science and Technology_, **17**, 227-266.
*   <a name="Levitin" id="Levitin"></a>Levitin, A.V. & Redman, T.C. (1998). Data as a resource: properties, implications, and prescriptions. _Sloan Management Review_, **40**(1), 89-101.
*   <a name="Lewis" id="Lewis"></a>Lewis, D. & Martin, W. (1989). Information management: state of the art in the United Kingdom. _ASLIB Proceedings_, **41**(7-8), 225-250.
*   <a name="Lytle_1986" id="Lytle_1986"></a>Lytle, R.H. (1986). Information resource management. _Annual Review of Information Science and Technology_, **21**, 309-336.
*   <a name="Lytle_1988" id="Lytle_1988"></a>Lytle, R.H. (1988). Information resource management: a five-year perspective. _Information Management Review_, **3**(3), 9-16.
*   <a name="Marchand_1986" id="Marchand_1986"></a>Marchand, D.A. & Horton, F.W. (1986). _Infotrends: profiting from your information resources_. New York, NY: Wiley.
*   <a name="Mata" id="Mata"></a>Mata, F.J., Fuerst, W.L., & Barney, J. B. (1995). Information technology and sustained competitive advantage: a resource-based analysis. _MIS Quarterly_, **19**(4), 487-504.
*   <a name="Maurer" id="Maurer"></a>Maurer, H. (2003). Wissensmanagement: ein Schritt nach vorne oder nur ein neues Schlagwort? _Informatik Spektrum_, **26**(1), 26-33.
*   <a name="McCain" id="McCain"></a>McCain, K. (1990). Mapping authors in intellectual space: a technical overview. _Journal of the American Society for Information Science_, **41**(6), 433-443.
*   <a name="McFarlan" id="McFarlan"></a>McFarlan, F.W. (1984). Information technology changes the way you compete. _Harvard Business Review_, **61**(3), 98-103.
*   <a name="Meik" id="Meik"></a>Meik, F. (1997). Informationsmanagement reicht nicht. _Information Management_, (5), 42-47.
*   <a name="Mertens_1986" id="Mertens_1986"></a>Mertens, P. & Plattfaut, E. (1986). Informationstechnik als strategische Waffe. _Information Management_, (2), 6-17.
*   <a name="Mertens_1995" id="Mertens_1995"></a>Mertens, P. (1995). Wirtschaftsinformatik–von den Moden zum Trend. In Wolfgang König, (Ed.) _Wirtschaftsinformatik '95_, (pp. 25-64) Heidelberg: Physica.
*   <a name="Miller" id="Miller"></a>Miller, B. B. (1988). Managing information as a resource. In J. Rabin & E.M. Jackowski (Eds.), _Handbook of information resource management_, (pp. 3-33). New York, NY: Dekker.
*   <a name="Miron" id="Miron"></a>Miron, M., Cecil, J., Bradicich, K. & Hall, G. (1988). The myths and realities of competitive advantage. _Datamation_, **34**(19), 71-82.
*   <a name="Morrow" id="Morrow"></a>Morrow, N.M. (2001). Knowledge management: an introduction. _Annual Review of Information Science and Technology_, **35**, 381-422.
*   <a name="Nonaka" id="Nonaka"></a>Nonaka, I. & Takeuchi, H. (1995). _The knowledge-creating company: how Japanese companies create the dynamics of innovation_. New York, NY: Oxford University Press.
*   <a name="Noot" id="Noot"></a>Noot, T.J. (1998, October). Libraries, records management, data processing: an information handling field. _ARMA Records Management Quarterly_, 22-26.
*   <a name="Otremba" id="Otremba"></a>Otremba, G. (1987). Taetigkeitsfelder des Informationsmanagements. _Nachrichten für Dokumentation_, **38**(4), 201-203.
*   <a name="OBrien" id="OBrien"></a>O'Brien, J.A. & Morgan, J.N. (1991). A multidimensional model of information resource management. _Information Resources Management Journal_, **4**(2), 2-11.
*   <a name="Pietsch" id="Pietsch"></a>Pietsch, T., Martiny, L. & Klotz, M. (1998). _Strategisches Informationsmanagement_. (3rd ed.). Berlin: Erich Schmidt.
*   <a name="Ponzi" id="Ponzi"></a>Ponzi, L. (2002): _The evolution and intellectual development of knowledge management_. Unpublished PhD dissertation. Long Island University, Palmer School of Library and Information Science.
*   <a name="Ponzi_Koenig" id="Ponzi_Koenig"></a>Ponzi, L. & Koenig, M. (2002). [Knowledge management: another management fad?](http://InformationR.net/ir/8-1/paper145.html) _Information Research_, **8**(1). Retrieved 9 March, 2005 from http://InformationR.net/ir/8-1/paper145.html.
*   <a name="Porter" id="Porter"></a>Porter, M. & Millar, V.E. (1985). How information gives you competitive advantage. _Harvard Business Review_, **63**(4), 149-160.
*   <a name="Robek" id="Robek"></a>Robek, M.F., Brown, M.F. & Stephens, D.O. (1996). _Information and records management_. New York, NY: McGraw Hill.
*   <a name="Rockart" id="Rockart"></a>Rockart, J.F. & Crescenzi, A.D. (1984). Engaging top-management in information technology. _Sloan Management Review_, **25**(4), 3-16.
*   <a name="Ross" id="Ross"></a>Ross, J.W., Beath, C.M. & Goodhue, D.L. (1996). Develop long-term competitiveness through IT assets. _Sloan Management Review_, **38**(1), 31-42.
*   <a name="Savic" id="Savic"></a>Savic, D. (1992). Evolution of information resource management. _Journal of Librarianship and Information Science_, **24**(3), 127-138.
*   <a name="Scheer" id="Scheer"></a>Scheer, A-W. (1992). _Architecture of integrated information systems: foundations of enterprise modelling_. Berlin: Springer.
*   <a name="Scheid" id="Scheid"></a>Scheid, E.M. (2001). [_Wissensmanagement - mehr als eine moderne Philosophie!_](http://www.c-o-k.de/cp_artikel.htm?artikel_id=24) Berlin: Gora, Hecken & Partner. Retrieved 9 March, 2005 from http://www.c-o-k.de/cp_artikel.htm?artikel_id=24
*   <a name="Schloegl_2000" id="Schloegl_2000"></a>Schlögl, C. (2000). Informationskompetenz am Beispiel einer szientometrischen Untersuchung zum Informationsmanagement. In _Proceedings des Internationalen Symposiums für Informationswissenschaft_ (pp. 89-111) Konstanz: Universitaetsverlag Konstanz.
*   <a name="Schloegl_2003" id="Schloegl_2003"></a>Schlögl, C. (2003). Wissenschaftslandkarte Informationsmanagement. _Wirtschaftsinformatik_, **45**(1), 7-16.
*   <a name="Schneider" id="Schneider"></a>Schneider, U. (1990). _Kulturbewusstes Informationsmanagement_. Munich: Oldenbourg.
*   <a name="Schneyman" id="Schneyman"></a>Schneyman, A.H. (1985). Organizing information resources. _Information Management Review_ , **1**(1), 35-45.
*   <a name="Schulte" id="Schulte"></a>Schulte, U. (1987). Praktikable Ansatzpunkte zur Realisierung von Datenmanagement-Konzepten. _Information Management_, (4), 26-31.
*   <a name="Schwarze" id="Schwarze"></a>Schwarze, J. (1988). Zum Berufsbild des Informations-Managers. _Information Management_, (1), 48-53.
*   <a name="Sutherland" id="Sutherland"></a>Sutherland, E. (1991). Methodologies and models for information management. _ASLIB Proceedings_, **43**(2/3), 99-107.
*   <a name="Senge" id="Senge"></a>Senge, P. (1990). _The fifth discipline: the art & practice of the learning organization_. New York, NY: Doubleday.
*   <a name="Stewart" id="Stewart"></a>Stewart, T. (1997). _Intellectual capital: the new wealth of organizations_. New York, NY: Doubleday.
*   <a name="Sveiby" id="Sveiby"></a>Sveiby, K. (1997). _The new organizational wealth: managing and measuring knowledge-based assets_. San Francisco, CA: Berrett Koehler.
*   <a name="Synnott_1987a" id="Synnott_1987a"></a>Synnott, W.R. (1987a). _The information weapon: winning customers and markets with technology_. New York, NY: Wiley.
*   <a name="Synnott_1987b" id="Synnott_1987b"></a>Synnott, W.R. (1987b). The emerging chief information officer. _Information Management Review_, **3**(1), 21-35.
*   <a name="Taylor" id="Taylor"></a>Taylor, R.S. (1986). _Value-added processes in information systems_. Norwood, NJ: Ablex.
*   <a name="Trauth" id="Trauth"></a>Trauth, E.M. (1989). The evolution of information resource management. _Information and Management_, **16**(5), 257-268.
*   <a name="Vickers" id="Vickers"></a>Vickers, P. (1985). A holistic approach to the management of information. _ASLIB Proceedings_ , **37**(1), 19-30.
*   <a name="Vogel" id="Vogel"></a>Vogel, E. (1992). Informationsmanagement: Stand und Perspektiven des Managements von Informationsressourcen. In M. Buder, W. Rehfeld & T. Seeger (Eds.), Grundlagen der praktischen Information und Dokumentation (pp. 897-927). >Vol. 2\. (3rd ed.). Munich: Saur.
*   <a name="Wang" id="Wang"></a>Wang, R.Y. (1998). Manage your information as a product. _Sloan Management Review_, **39**(4), 95-106.
*   <a name="Wersig" id="Wersig"></a>Wersig, G. (1989). _Organisations-Kommunikation. Die Kunst, ein Chaos zu organisieren_ Baden-Baden: FBO-Fachverlag.
*   <a name="Wiggins" id="Wiggins"></a>Wiggins, R.E. (1988). A conceptual framework for information resources management. _International Journal of Information Management_, **8**(1), 5-11\.
*   <a name="Wilson02" id="Wilson02"></a>Wilson, T.D. (2002). Information management. In J. Feather and P. Sturges, (Eds.), _International encyclopedia of information and library science_, (2nd ed.) (pp. 263-278) London: Routledge, 2002
*   <a name="Wilson" id="Wilson"></a>Wilson, T.D. (2002). [The nonsense of 'knowledge management'](http://InformationR.net/ir/8-1/paper144.html). _Information Research_, **8**(1). Retrieved 9 March, 2005 from http://InformationR.net/ir/8-1/paper144.html.
*   <a name="Wiseman" id="Wiseman"></a>Wiseman, C. (1985). _Strategy and computers: information systems as competitive weapons_. Homewood, IL.: Dow Jones-Irwin.
*   <a name="Wollnik" id="Wollnik"></a>Wollnik, M. (1988). Ein Referenzmodell des Informationsmanagements. _Information Management_, (3), 34-43.
*   <a name="Zijlker" id="Zijlker"></a>Zijlker, A.W. (1988). Setting the scene for I.R.M. _Information and Management_, **15**(2), 79-84.
