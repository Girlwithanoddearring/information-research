#### Vol. 10 No. 4, July 2005

# Intention to seek information on cancer genetics

#### [James E. Andrews](mailto:jandrews@cas.usf.edu)  
University of South Florida, School of Library and Information Science  
Tampa, FL 33620-7800, USA

#### [J. David Johnson](mailto:jdj@pop.uky.edu), [Donald O. Case](mailto:dcase@uky.edu)  
College of Communications and Information Studies  
University of Kentucky, Lexington, KY 40506-0039, USA

#### [Suzie Allard](mailto:sallard@utk.edu)  
School of Information Sciences, University of Tennessee  
Knoxville TN 37996, USA

#### [Kimberly Kelly](mailto:kelly-29@medctr.osu.edu)  
Human Cancer Genetics Program, The Ohio State University  
Columbus, OH 43221, USA  




#### Abstract

> **Objective.** The public has a high interest in seeking personal genetic information, which holds implications for health information seeking research and health care policy. Rapid advances in cancer genetics research promise early detection, prevention and treatment, yet consumers may have greater difficulty finding and using the information they may need to make informed decisions regarding their personal health and the future of their families.  
> **Design.** A statewide telephone survey was conducted of non-institutionalized Kentucky residents 18 years of age or older to investigate factors associated with the intention to seek cancer genetics information, including the need for such information seeking help.  
> **Results.** The results show that intention to seek cancer genetics information, if testing were readily available, is moderately high (62.5% of those responding; n=835), and that status as a racial minority, the perception that cancer runs in one's family, and frequent worrying about cancer risk are statistically significant predictors of intent to seek genetics information.  
> **Conclusions.** We argue that an already complex health information environment will be even more difficult for individuals to navigate as genetic research becomes more ubiquitous in health care. An increase in demand for genetics information in various forms, as suggested by these results and those of other studies, implies that enduring intervention strategies are needed to help individuals acquire necessary health information literacy skills, with special attention given to racial minorities.

## Introduction

Basically, genetic testing refers to medical tests that help identify changes in chromosomes, genes, or proteins that may provide clues to one’s susceptibility to certain diseases, or whether they will pass on or inherit mutated genes (see the National Library of Medicine’s, Genetic Home Reference website: [http://ghr.nlm.nih.gov/](http://ghr.nlm.nih.gov/)). Often, genetic testing is preceded by a meeting with a genetic counsellor, who is specially qualified to examine a person’s familial experiences with potentially heritable diseases, and who can best explain the advantages and related risks that an individual and his or her family should consider before having a genetic test. Genetic testing in cancer has been particularly exciting. As former National Cancer Institute director Richard Klausner has noted: “We have learned that cancer is, at its core, the consequence of alterations in DNA – that cancer is a genetic disease. Genetic information has the potential to transform how we prevent, detect, and treat cancer” ([Klausner 1996](#Klausner): 3).

While there have been rapid advances in cancer genetics research that have led to promises of enhanced screening, prevention, and treatment in cancer care, these also pose a challenging information seeking problem ([Johnson, _et al._ 2005](#Johnson3); [Johnson _et al._ 2001](#Johnson2)). As genetic testing for cancer becomes more readily available, two issues are of critical importance: whether and how many people will actively seek personalized or general genetic information; and, whether or not they will need help in making informed decisions. The consumer movement in health ([Pifalo, _et al._ 1997](#Pifalo)) has led to an increased burden on individuals to make informed, complex decisions related to their own (and their family's) health care. Yet, in the case of cancer genetics, the general public may find it difficult to keep pace with this highly complex and continually growing body of knowledge. A high level of demand for genetic testing and counselling may be further hampered by a severe shortage of cancer genetic counsellors and frontline care-givers who may not be prepared to handle a large number of people seeking genetic information ([Burke & Emery 2002](#Burke); [Greendale & Pyeritz 2001](#Greendale)). Thus, factors associated with this special information seeking context need to be further explored in order to develop approaches for understanding, and interventions to improve, an individual's ability to navigate a complicated health information environment ([Johnson _et al._ 2001](#Johnson2)). In this paper, we examine issues related to attitudes and knowledge related to cancer genetics, in general, including the likelihood that the public would be interested in seeking information related to their personal predisposition to cancer and whether or not they will need help in seeking such information.

## Background

### Health information seeking

The focus of information seeking research has been to develop an understanding of how people receive and act upon information communicated to them from a variety of available information carriers. Information seeking in health contexts is particularly important as it is viewed as a key moderator between perceived threats of disease and the likelihood of engaging in actions that could lead to positive health-related outcomes ([Johnson 1997](#Johnson)). Previous research in this area has shown that information seeking and exposure to information can improve information gain can improve information gain, affective support, emotional adjustment, social adjustment ([Zemore & Shepel 1987](#Zemore)), attitude change, knowledge change, behaviour maintenance ([Anderson, Meissner, & Portnoy 1989](#Anderson)), a feeling of greater control over events, reduction of uncertainty ([Freimuth 1987](#Freimuth)), and compliance with medical advice ([Street 1990](#Street)). Information seeking also leads to earlier prognosis, which, at the very least, may allow an individual and his/her family more time to adjust to difficult circumstances ([Steen 1993](#Steen)). Generally, the scope and nature of the information on which to base judgments, the repertoire of alternative courses of action known to the searcher, and ultimately the action taken are affected by an individual's information seeking behaviour. Moreover, as is the case with other types of information seeking, individuals may be seeking control and mastery of life (as outlined in [Savolainen 1995](#savolainen)). 

A major implication of the consumer health movement has been that individuals are compelled to be more active participants and decision-makers in their own health care. To be successful at this, however, means negotiating a complex health information environment: one must have access to Internet sources, be familiar enough with them to successfully retrieve relevant information, and be able to make quality and credibility judgments about conflicting information from various sources. Unfortunately, many people lack the requisite health information literacy skills to make efficacious use of such information ([American Medical Association 1999](#AMA); [U.S. Dept. of Health and Human Services 2000](#USDoh)), and it is virtually impossible for even the most dedicated individual, or even health professional, to stay current on recent biomedical advances ([Gould & Pearce 1991](#Gould); [Kreps 1991](#Kreps)).

### Genetic information seeking

The special context of genetics information seeking has made health information seeking more challenging for individuals. This context is defined by the complexity of the health issues involved and the ever-evolving body of scientific knowledge ([Thomsen & Maat 1998](#Thomsen)), and is emerging as an especially interesting object of study in information seeking research (it has been labelled by the authors as “the perfect information seeking research problem” (see [Johnson _et al._ 2005](#Johnson3), for a more detailed review of this subject). As the advances in knowledge, technologies, and treatment continue, so does the amount and complexity of information available, which means more choices, rights, and responsibilities.

The benefits of seeking cancer genetics information (particularly genetic testing or counselling, which can be regarded as highly individualized and authoritative) relate to early detection, prevention, and individualized or pre-symptomatic treatment. People may be motivated to seek genetic testing by the desire to know if more screening tests are necessary ([Chaliki _et al._ 1995](#Chaliki); [Lerman, _et al._ 1996](#Lerman2); [Lerman, _et al._ 1995](#Lerman5)), to plan better for the future ([Lerman, _et al._ 1989](#Lerman4)), and to explore strategies for risk reduction ([Smith & Croyle 1995](#Smith)). Information seeking in this context suggests individuals may be looking for clues as to their actual risk, and how this will affect their lives in terms of more visits to the doctor, related costs, implications for their families, and so on. In best case scenarios, test results could motivate people to engage in more healthy behaviour ([Lerman _et al._ 1995](#Lerman5); [Lerman 1989](#Lerman4)), since so many cancers can be averted by making lifestyle changes and engaging in preventative actions. For instance, if a woman is aware that she is genetically predisposed to cancer, she may increase the frequency or types of screening tests, as well as make other health behaviour changes (stop smoking, change diet, etc.). Also, as research in pharmacogenetics continues to advance, drugs can be tailored to one’s genetic makeup ([Bell 1998](#bell); [Wortman 2001](#Wortman)). For instance, people with a certain genetic makeup may respond more positively to particular types of chemotherapy, and thus may suffer less extreme side effects.

It is not surprising, therefore, that high proportions of individuals express a desire for more health-related information. This appears true in regards to genetic information, as well, as evidenced in several public opinion polls and population-based surveys (e.g., [Andrykowski, _et al._ 1996](#Andrykowski2); [Andrykowski, _et al._ 1997](#Andrykowski); [Avins 2000](#Avins); [Ludman, _et al._ 1999](#Ludman); [Tambor, _et al._ 1997](#Tambor)). It should be noted, however, that there is simultaneous disapproval within the general population of certain aspects of genetic testing, such as eugenics concerns and privacy issues ([Jallinoja _et al._ 1998](#Jallinopa)). This is possibly a reflection of the manner in which these topics have been covered in the media.

Other studies have examined individuals' intentions to be tested or to receive results from genetic tests already taken among populations with greater interest in their personal cancer risks, such as those with a family history of particular cancers. Petersen _et al._ ([1999](#Petersen)) reported that 92% of first-degree relatives of cancer patients were _somewhat_ or _very likely_ to take a genetic test; Lerman _et al._ ([1996](#Lerman2)) found that 82% of first-degree relatives of colon cancer patients indicated they would _definitely_ or _probably_ want to be tested; and, Lerman _et al._ ([1989](#Lerman4)) found that 84% of women with breast and/or ovarian cancer indicated a probable or definite interest in testing. Other studies have shown similar results ([Bluman _et al._ 1999](#Bluman); [Chaliki _et al._ 1995](#Chaliki); [Smith & Croyle 1995](#Smith); [Struewing, _et al._ 1995](#Struewing)).

Yet, by and large, the general public has many misconceptions about, and appears largely unaware of, the basics of genetics ([Armstrong, _et al._ 2002](#Armstrong); [Lerman _et al._ 1999](#Lerman); [Welkenhuysen, _et al._ 2001](#Welkenhuysen)). For instance, it has been shown that many people vastly overestimate personal risk levels ([Diefenbach, _et al._ 2001](#Diefenbach); [Lee, _et al._ 2002](#Lee); [Pasacreta 2003](#Pasacreta)). In fact, the public perception of personal hereditary risk greatly exceeds the estimated 5-10% of cancers that may be genetically determined ([Kash, _et al._ 2000](#Kash); [Kinney, _et al._ 2000](#Kinney); [Stopfer 2000](#Stopfer); [Parker-Pope 2004](#Parker)). As but one example, Bluman _et al._ ([1999](#Bluman)), found 60% of women with breast cancer and/or ovarian cancer overestimated their risk.  Since, _Interest in undergoing testing is more strongly related to perceived risk than objective risk_ ([Marteau & Croyle 1998](#Marteau): 695), the demand for information related to genetics is likely to be great even among women who are not likely to have predisposing mutations.

Statements of intent to seek genetic testing in hypothetical situations do not always result in actual uptake of testing ([Pasacreta 2003](#Pasacreta)). For instance, Welkenhuysen _et al._ ([2001](#Welkenhuysen)) found that women with breast cancer relatives were much less interested in a predictive test than those without breast cancer relatives. Lerman, _et al._ ([1996](#Lerman2)) showed actual uptake of genetic testing within an at-risk population for hereditary breast-ovarian cancer was lower than anticipated from previous studies, with 43% of all study subjects requesting test results. A separate study showed only 43% of adults in extended Hereditary Nonpolyposis Colorectal Cancer (HNPCC) families chose to receive the results of tests they had taken ([Lerman, _et al._ 1999](#Lerman)). Other studies also have concluded that interest in testing appears inversely related to a family history of cancer ([Armstrong _et al._ 2002](#Armstrong); [Welkenhuysen _et al._ 2001](#Welkenhuysen)). In clinical settings, it has been reported that actual use of testing for abnormalities in the BRCA1 and BRCA2 genes (indicators of breast and ovarian cancer) was lower than anticipated from previous studies and hypothetical settings ([Lee, _et al._ 2002](#Lee)).

Of course, there are a number of reasonable scenarios wherein individuals might avoid this type of information, or health information in general. In fact, studies of cancer patients find that there is a hard core, albeit a minority, who do not want information under any circumstances ([Hinds, _et al._ 1995](#hinds)). Virtually all of us have had experiences with cancer either through friends and family or personally; it is a health concern that is very much tied to feelings of anxiety and fear which, in addition to other cognitive and emotional variables, are associated with information avoidance ([Case, _et al._ 2005](#Case05)). Although we traditionally think of information seeking during health crises as a rational course of action (e.g., to decrease anxiety and help inform choices) ([Case _et al._ 2005](#Case05)), there are cases where more information results in increased anxiety. Regarding genetic testing, for instance, the medical literature shows cases of increased anxiety ([Chaliki 1995](#Chaliki); [Lerman _et al._ 1995](#Lerman5)), depression ([Lerman _et al._ 1999](#Lerman), [Smith & Croyle 1995](#Smith)), and other negative psychosocial responses ([Marteau & Croyle 1998](#Marteau)). Given the possibility for such negative scenarios, it is understandable why people may choose _not_ to seek genetic testing, and may simply be content with their current situation ([Chaliki 1995](#Chaliki)). Alpert ([2003](#Alpert)) points out that the "right not to know" is a compelling issue in genetic testing when diseases are involved for which there are neither cures nor preventative measures, such as Huntington's Disease. Moreover, genetic information affects both the individual _and_ his or her familial network.  Thus, it is not only a matter of who is tested but with whom they share the results; some family members may not want to know the results of their relative's test. The issues connected with information avoidance will continue to complicate the genetic information seeking environment and will be necessary considerations in attempting to best reach people who may benefit from genetic counselling or testing.

Other factors unique to the genetics era also may preclude individuals from seeking testing or related genetic information. Many are concerned over losing their insurance or having increased rates, or other forms of discrimination ([Lerman _et al._ 1996](#Lerman2); [Lerman _et al._ 1989](#Lerman4); [Struewing _et al._ 1995](#Struewing); [Lerman _et al._ 1996](#Lerman3)). Also, although more accurate testing is becoming increasingly available, there is the understandable concern over test accuracy ([Lerman _et al._ 1996](#Lerman2); [Lerman _et al._ 1996](#Lerman3)) and how well an individual feels he or she can cope with uncertain results. Stuewing ([1995](#Struewing)) showed relatively high proportions (36%) of people who indicated that they would think a negative test was wrong. Even if tests are accurate, some worry about the potential stigma associated with having increased susceptibility to cancer ([Durfy, _et al._ 1999](#durfy)). Related to this is the non-trivial issue of what impact testing could have on one’s family. Emotional reactions to positive tests could cause resentment from family members, or otherwise have a negative impact on family networks ([Lerman _et al._ 1996](#Lerman2); [Bosompora _et al._2000](#Bosompora); [Smith & Croyle 1995](#Smith)). Other family issues may involve survivor guilt ([Rothstein 1997](#Rothstein); [Lerman _et al._ 1995](#Lerman5)), or cases where families ostracize members who do not share the same genetic destiny ([Marteau & Croyle 1998](#Marteau)). In addition, Lerman _et al._ ([1999](#Lerman)) revealed that about half of their interviewees indicated that negative test results would lead to the unintended effect of unhealthy behaviour, possibly due to either false reassurance or a misunderstanding of risk, and that they would decrease the frequency of screening tests and engage in fewer prevention-oriented activities. In other words, it appears that these first-degree relatives were not able to distinguish between general and genetic-oriented risk factors. Moreover, a negative test can result in inappropriate cancer management (lengthening screening intervals) which could result in delays in diagnosing cancer ([Stopfer 2000](#Stopfer)).

### Purpose of study

This project explores intention in a general population sample more broadly than the intention-related studies noted above. We conducted this project with the expectation that genetic testing for a number of cancers will be available, and perhaps more aggressively marketed, to the public in the future. We hypothesized that a majority of individuals will state that they would seek genetic testing if it were readily available, and that the following are positively related to this intent: self-described understanding of genetics, frequently worrying about cancer, the perception that cancer runs in one's family, and higher socio-economic status. Also, we expect that individuals who are likely to seek cancer-related genetic information would also express a desire for help with information seeking supporting decision making. A further description of the study and implications of the findings is described below.

## Methods

### Survey method

These data were derived from responses to questions that were part of a statewide telephone survey of non-institutionalized Kentucky residents 18 years of age or older. The survey was conducted from July 20th to August 26th, 2002, by the University of Kentucky Survey Research Center. The telephone survey employed the standard Waksberg random-digit dialing procedures ([Waksberg](#wak78) 1978) for contacting non-institutionalized adults, as well as the ACS Query Computer-Assisted Telephone Interviewing (CATI) system (See [WinQuery's Website](http://www.analyticalgroup.com/winquery.htm)). A total of 2,454 phone numbers was called and there were 125 ineligible respondents and 1,447 refusals or incomplete surveys, resulting in a total of 882 interviews completed (a response rate of 37.9%). The margin of error for the completed interview sample was approximately +/- 3.3% at the 95 percent confidence level.

The survey as a whole included a total of 156 questions submitted by a number of University of Kentucky researchers on a variety of topics. For this project, several questions were included that related to specific factors that might affect the intent to seek cancer genetics information and that were inspired by prior research on this subject.

### Survey questions

The main variable of intent was measured with a single question: _Would you have a genetic test to determine your risk for inherited cancer if it were readily available?_ Possible responses were: _definitely not,_ _probably not,_ _it depends,_ _probably yes,_ and, _definitely yes_). Several independent variables were measured using questions related to issues surrounding cancer genetics and that the researchers believed might be associated with respondents' stated intent to seek cancer genetic information (see [Table 1](#Table1)). These related to self-perception of risk, worry, perception that cancer runs in one's family, and understanding of genetics (the last was derived from Andrykowski _et al._ [1996](#Andrykowski2)). Also, individuals were asked whether or not they would need help finding information to help them with their decision (_If you were trying to decide whether to have a genetic test to determine your risk for inherited cancer, do you think you would want or need someone's help finding information about genetic testing?_; Yes or No). Basic socio-demographic information collected for the survey as a whole and used in this analysis included: sex, age, ethnicity, education, total household income, and community size.

All data analyses were done using SPSS v11.5\. A multinomial logistic regression was performed to determine which variables were significant predictors of the intention to seek cancer genetics information, as well as to identify predictors associated with the need for help in seeking cancer genetic information. Chi-squared analyses were used to explore anomalous findings. An alpha level of 0.05 was selected as the criterion for statistical significance for all analyses.

## Results

### Sample characteristics

Of the 882 respondents, 40% were male and 60% were female, with a mean age of 48\. The sample consisted of 92% white respondents, 4% African American and 3% _other_ racial or ethnic background. Most of the respondents stated that they live in a _small town_ (36%); 13% said they live in a rural, farming area; 15% in a rural, non-farming area; 13% in suburbs; and, 23% in a city. Twelve percent had less than a high school education, 32% percent had completed high school (or achieved the General Educational Development High School Equivalency Diploma (GED)), and the rest (51%) had at least some college; over 11% of the sample had at least some graduate school coursework. Most individuals reported a total annual household income ranging from $15,000 to $50,000 (34%), with 12% earning less than $15,000 per year and 28% earning more than $50,000\. Approximately 63% of respondents reported that they were currently married.

Respondent characteristics were compared with 2000 Kentucky census data (U.S. Census Bureau [2003](#USCensus)). The survey population appeared reasonably representative of the population of Kentucky in terms of proportion of minorities (7% in the study sample; 9% in census data), and percentage of those with high school diplomas or equivalent (35% in the study sample; 32% in the state), or post-high school education (50.9% in this sample; 42.5% in census data).

### Variables associated with intent to seek cancer genetics testing and their need for help

Consistent with previous studies, the results show a moderately high level of interest in cancer genetic testing. Of those respondents who answered the question (n=835), 62.5% said they probably or definitely would seek testing if it were readily available, 33% probably or definitely would not seek such information, and about 5% responded that it would depend on other factors.

Individuals' self-reported understanding of genetics varied. Most respondents reported either a _fair_ (38%) or _good_ (36%) understanding, while 11% stated their understanding was _poor_, and nearly 14% said it was _excellent._ The results of this measure are roughly the same as a previous population-based study conducted in Kentucky ([Andrykowski _et al._ 1996](#Andrykowski)). About 57% percent of respondents felt that cancer does not run in their family, and 43% felt that cancer does. Yet, only 13% of the individuals stated that they worry _often_ about getting cancer; 27% said they worry _sometimes_, 31% _rarely_, and 29% _never_.

Results of a multinomial logistic regression (model 1>χ<sup>2</sup> = 172; df = 64; p < .001) revealed that status as a racial minority (p < .002), the perception that cancer runs in one's family (p < .01), and more frequent worrying about getting cancer (p< .001) were found to be a statistically significant predictors of intent (See [Table 1](#Table1)).

<a name="table1"></a>  

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Intention to seek cancer genetic information as related to demographics, understanding of genetics, perception that cancer runs in one's family, and frequency of worry about cancer**</caption>

<tbody>

<tr>

<th rowspan="2" valign="middle">Variable</th>

<th width="20%" rowspan="2" valign="middle">(% valid responses)</th>

<th colspan="5">Intent to seek genetic information  
(% of respondents)</th>

</tr>

<tr>

<th width="10%">Def. No.</th>

<th width="10%">Prob. No.</th>

<th width="10%">Depends</th>

<th width="10%">Prob. Yes.</th>

<th width="10%">Def. Yes</th>

</tr>

<tr>

<td colspan="7" align="left">**Demographics—Sex**</td>

</tr>

<tr>

<td style="text-indent: 27px;">Male</td>

<td align="center">334 (40%)</td>

<td align="center">13</td>

<td align="center">21</td>

<td align="center">4</td>

<td align="center">33</td>

<td align="center">29</td>

</tr>

<tr>

<td style="text-indent: 27px;">Female</td>

<td align="center">501 (60%)</td>

<td align="center">10</td>

<td align="center">21</td>

<td align="center">5</td>

<td align="center">36</td>

<td align="center">28</td>

</tr>

<tr>

<td colspan="7" align="left">**Demographics—Race <sup>††</sup>**</td>

</tr>

<tr>

<td style="text-indent: 27px;">White</td>

<td align="center">720 (92%)</td>

<td align="center">12</td>

<td align="center">22</td>

<td align="center">5</td>

<td align="center">34</td>

<td align="center">27</td>

</tr>

<tr>

<td style="text-indent: 27px;">Non-white</td>

<td align="center">62 (8%)</td>

<td align="center">8</td>

<td align="center">10</td>

<td align="center">2</td>

<td align="center">31</td>

<td align="center">50</td>

</tr>

<tr>

<td colspan="7" align="left">**Community size**</td>

</tr>

<tr>

<td style="text-indent: 27px;">Rural area—farm</td>

<td align="center">110 (13%)</td>

<td align="center">13</td>

<td align="center">23</td>

<td align="center">9</td>

<td align="center">29</td>

<td align="center">26</td>

</tr>

<tr>

<td style="text-indent: 27px;">Rural area—non-farm</td>

<td align="center">124 (15%)</td>

<td align="center">11</td>

<td align="center">16</td>

<td align="center">4</td>

<td align="center">48</td>

<td align="center">22</td>

</tr>

<tr>

<td style="text-indent: 27px;">Small town</td>

<td align="center">299 (36%)</td>

<td align="center">11</td>

<td align="center">23</td>

<td align="center">3</td>

<td align="center">33</td>

<td align="center">30</td>

</tr>

<tr>

<td style="text-indent: 27px;">Suburb</td>

<td align="center">107 (13%)</td>

<td align="center">13</td>

<td align="center">23</td>

<td align="center">2</td>

<td align="center">36</td>

<td align="center">25</td>

</tr>

<tr>

<td style="text-indent: 27px;">City</td>

<td align="center">189 (29%)</td>

<td align="center">12</td>

<td align="center">19</td>

<td align="center">7</td>

<td align="center">30</td>

<td align="center">32</td>

</tr>

<tr>

<td colspan="7" align="left">**Education**</td>

</tr>

<tr>

<td style="text-indent: 27px;">≤ High school</td>

<td align="center">385 (49%)</td>

<td align="center">12</td>

<td align="center">22</td>

<td align="center">4</td>

<td align="center">34</td>

<td align="center">28</td>

</tr>

<tr>

<td style="text-indent: 27px;">> High school diploma</td>

<td align="center">401 (51%)</td>

<td align="center">12</td>

<td align="center">21</td>

<td align="center">5</td>

<td align="center">34</td>

<td align="center">28</td>

</tr>

<tr>

<td colspan="7" align="left">**Household Income:**</td>

</tr>

<tr>

<td style="text-indent: 27px;">< 15,000</td>

<td align="center">105 (16%)</td>

<td align="center">14</td>

<td align="center">15</td>

<td align="center">5</td>

<td align="center">35</td>

<td align="center">31</td>

</tr>

<tr>

<td style="text-indent: 27px;">15,000-50,000</td>

<td align="center">298 (46%)</td>

<td align="center">9</td>

<td align="center">19</td>

<td align="center">5</td>

<td align="center">37</td>

<td align="center">31</td>

</tr>

<tr>

<td style="text-indent: 27px;">> 50,000</td>

<td align="center">242 (38%)</td>

<td align="center">11</td>

<td align="center">24</td>

<td align="center">4</td>

<td align="center">33</td>

<td align="center">28</td>

</tr>

<tr>

<td colspan="7" align="left">**Understanding of genetics**</td>

</tr>

<tr>

<td style="text-indent: 27px;">Poor</td>

<td align="center">90 (11%)</td>

<td align="center">13</td>

<td align="center">26</td>

<td align="center">6</td>

<td align="center">31</td>

<td align="center">24</td>

</tr>

<tr>

<td style="text-indent: 27px;">Fair</td>

<td align="center">315 (38%)</td>

<td align="center">9</td>

<td align="center">21</td>

<td align="center">6</td>

<td align="center">38</td>

<td align="center">26</td>

</tr>

<tr>

<td style="text-indent: 27px;">Good</td>

<td align="center">299 (36%)</td>

<td align="center">14</td>

<td align="center">21</td>

<td align="center">3</td>

<td align="center">34</td>

<td align="center">28</td>

</tr>

<tr>

<td style="text-indent: 27px;">Excellent</td>

<td align="center">122 (15%)</td>

<td align="center">12</td>

<td align="center">18</td>

<td align="center">5</td>

<td align="center">27</td>

<td align="center">38</td>

</tr>

<tr>

<td colspan="7" align="left">**Cancer in family?** <sup>†</sup></td>

</tr>

<tr>

<td style="text-indent: 27px;">Yes</td>

<td align="center">357 (43%)</td>

<td align="center">11</td>

<td align="center">14</td>

<td align="center">5</td>

<td align="center">32</td>

<td align="center">37</td>

</tr>

<tr>

<td style="text-indent: 27px;">No</td>

<td align="center">470 (57%)</td>

<td align="center">11</td>

<td align="center">26</td>

<td align="center">5</td>

<td align="center">37</td>

<td align="center">21</td>

</tr>

<tr>

<td colspan="7" align="left">**Worry about cancer?** <sup>†††</sup></td>

</tr>

<tr>

<td style="text-indent: 27px;">Never</td>

<td align="center">239 (29%)</td>

<td align="center">21</td>

<td align="center">27</td>

<td align="center">4</td>

<td align="center">28</td>

<td align="center">20</td>

</tr>

<tr>

<td style="text-indent: 27px;">Rarely</td>

<td align="center">264 (32%)</td>

<td align="center">10</td>

<td align="center">25</td>

<td align="center">5</td>

<td align="center">41</td>

<td align="center">19</td>

</tr>

<tr>

<td style="text-indent: 27px;">Sometimes</td>

<td align="center">224 (27%)</td>

<td align="center">6</td>

<td align="center">17</td>

<td align="center">6</td>

<td align="center">37</td>

<td align="center">34</td>

</tr>

<tr>

<td style="text-indent: 27px;">Often</td>

<td align="center">105 (13%)</td>

<td align="center">4</td>

<td align="center">9</td>

<td align="center">2</td>

<td align="center">28</td>

<td align="center">58</td>

</tr>

<tr>

<td colspan="7" align="left">**Note: <sup>†</sup> p <0.01, <sup>††</sup> p <0.002, <sup>†††</sup> p <0.001**</td>

</tr>

</tbody>

</table>

A second logistic regression was performed to determine predicting factors associated with the need for help with seeking cancer genetics information. The results shown in [Table 2](#Table2) (model χ<sup>2</sup> = 83; df = 20; p < .001) reveal that education (p. < .008), household income (p < .01), intent to seek genetic testing if it were available (p < .001), and self-reported level of understanding of genetics (p < .006) are associated with the need for information seeking help. Of those whose responded _probably yes_ to the intent question, 67% stated they would need help, and of those responding _definitely yes,_ 80% said they would need help. For those who stated they would need help with information seeking, 36% said they would need _a lot_ of help, and 40% said they would like _some._

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Need help seeking cancer genetic information on socio-demographics, understanding of genetics, perception that cancer runs in one's family, frequency of worry about cancer, and intent to seek cancer genetic information**</caption>

<tbody>

<tr>

<th rowspan="2" valign="middle">Variable</th>

<th width="20%" rowspan="2" valign="middle">(% valid responses)</th>

<th colspan="2">Need help seeking genetic information  
(% of respondents)</th>

</tr>

<tr>

<th>Yes</th>

<th>No</th>

</tr>

<tr>

<td colspan="7" align="left">**Demographics—Sex**</td>

</tr>

<tr>

<td style="text-indent: 27px;">Male</td>

<td align="center">335 (40%)</td>

<td align="center">65</td>

<td align="center">35</td>

</tr>

<tr>

<td style="text-indent: 27px;">Female</td>

<td align="center">501 (60%)</td>

<td align="center">69</td>

<td align="center">31</td>

</tr>

<tr>

<td colspan="7" align="left">**Demographics—Race <sup>††</sup>**</td>

</tr>

<tr>

<td style="text-indent: 27px;">White</td>

<td align="center">728 (92%)</td>

<td align="center">68</td>

<td align="center">32</td>

</tr>

<tr>

<td style="text-indent: 27px;">Non-white</td>

<td align="center">64 (8%)</td>

<td align="center">67</td>

<td align="center">33</td>

</tr>

<tr>

<td colspan="7" align="left">**Community size**</td>

</tr>

<tr>

<td style="text-indent: 27px;">Rural area—farm</td>

<td align="center">111 (13%)</td>

<td align="center">69</td>

<td align="center">31</td>

</tr>

<tr>

<td style="text-indent: 27px;">Rural area—non-farm</td>

<td align="center">124 (15%)</td>

<td align="center">73</td>

<td align="center">27</td>

</tr>

<tr>

<td style="text-indent: 27px;">Small town</td>

<td align="center">304 (36%)</td>

<td align="center">67</td>

<td align="center">33</td>

</tr>

<tr>

<td style="text-indent: 27px;">Suburb</td>

<td align="center">109 (13%)</td>

<td align="center">70</td>

<td align="center">30</td>

</tr>

<tr>

<td style="text-indent: 27px;">City</td>

<td align="center">193 (23%)</td>

<td align="center">63</td>

<td align="center">37</td>

</tr>

<tr>

<td colspan="7" align="left">**Education** <sup>†</sup></td>

</tr>

<tr>

<td style="text-indent: 27px;">≤ High school</td>

<td align="center">389 (49%)</td>

<td align="center">75</td>

<td align="center">25</td>

</tr>

<tr>

<td style="text-indent: 27px;">> High school diploma</td>

<td align="center">407 (51%)</td>

<td align="center">61</td>

<td align="center">39</td>

</tr>

<tr>

<td colspan="7" align="left">**Household Income:** <sup>††</sup></td>

</tr>

<tr>

<td style="text-indent: 27px;">< 15,000</td>

<td align="center">105 (16%)</td>

<td align="center">71</td>

<td align="center">29</td>

</tr>

<tr>

<td style="text-indent: 27px;">15,000-50,000</td>

<td align="center">302 (47%)</td>

<td align="center">74</td>

<td align="center">26</td>

</tr>

<tr>

<td style="text-indent: 27px;">> 50,000</td>

<td align="center">243 (37%)</td>

<td align="center">58</td>

<td align="center">42</td>

</tr>

<tr>

<td colspan="7" align="left">**Understanding of genetics** <sup>†††</sup></td>

</tr>

<tr>

<td style="text-indent: 27px;">Poor</td>

<td align="center">91 (11%)</td>

<td align="center">75</td>

<td align="center">25</td>

</tr>

<tr>

<td style="text-indent: 27px;">Fair</td>

<td align="center">317 (38%)</td>

<td align="center">74</td>

<td align="center">26</td>

</tr>

<tr>

<td style="text-indent: 27px;">Good</td>

<td align="center">303 (36%)</td>

<td align="center">67</td>

<td align="center">33</td>

</tr>

<tr>

<td style="text-indent: 27px;">Excellent</td>

<td align="center">123 (15%)</td>

<td align="center">46</td>

<td align="center">54</td>

</tr>

<tr>

<td colspan="7" align="left">**Cancer in family?** <sup>†</sup></td>

</tr>

<tr>

<td style="text-indent: 27px;">Yes</td>

<td align="center">362 (43%)</td>

<td align="center">66</td>

<td align="center">34</td>

</tr>

<tr>

<td style="text-indent: 27px;">No</td>

<td align="center">476 (57%)</td>

<td align="center">68</td>

<td align="center">32</td>

</tr>

<tr>

<td colspan="7" align="left">**Worry about cancer?** <sup>†††</sup></td>

</tr>

<tr>

<td style="text-indent: 27px;">Never</td>

<td align="center">243 (29%)</td>

<td align="center">57</td>

<td align="center">43</td>

</tr>

<tr>

<td style="text-indent: 27px;">Rarely</td>

<td align="center">266 (32%)</td>

<td align="center">70</td>

<td align="center">31</td>

</tr>

<tr>

<td style="text-indent: 27px;">Sometimes</td>

<td align="center">228 (27%)</td>

<td align="center">70</td>

<td align="center">30</td>

</tr>

<tr>

<td style="text-indent: 27px;">Often</td>

<td align="center">106 (13%)</td>

<td align="center">81</td>

<td align="center">19</td>

</tr>

<tr>

<td colspan="7" align="left">**Genetic test?** <sup>††††</sup></td>

</tr>

<tr>

<td style="text-indent: 27px;">Definitely no</td>

<td align="center">94 (11%)</td>

<td align="center">46</td>

<td align="center">54</td>

</tr>

<tr>

<td style="text-indent: 27px;">Probably no</td>

<td align="center">177 (21%)</td>

<td align="center">61</td>

<td align="center">39</td>

</tr>

<tr>

<td style="text-indent: 27px;">Depends</td>

<td align="center">39 (5%)</td>

<td align="center">77</td>

<td align="center">23</td>

</tr>

<tr>

<td style="text-indent: 27px;">Probably yes</td>

<td align="center">287 (35%)</td>

<td align="center">67</td>

<td align="center">33</td>

</tr>

<tr>

<td style="text-indent: 27px;">Definitely yes</td>

<td align="center">234 (28%)</td>

<td align="center">80</td>

<td align="center">21</td>

</tr>

<tr>

<td colspan="7" align="left">**Note: <sup>†</sup> p <0.008, <sup>††</sup> p <0.01, <sup>†††</sup> p <0.006, <sup>††††</sup> p < 0.001**</td>

</tr>

</tbody>

</table>

## Discussion

The survey results reported here show a moderately high level of interest in genetic testing. Also, they indicate that worrying about one's cancer risk, the perceptions that cancer is heritable, as well as status as a racial minority, are significant predictors of intention to seek cancer genetic information. This differs from the Andrykowski _et al._ ([1996](#Andrykowski2)) study, conducted in the same state, which showed non-minority status and higher educational achievement as associated with greater interest in learning of a genetic predisposition to breast cancer. An integrative review of a number of other studies (focused solely on breast and ovarian cancer) noted that much more needs to be learned about predictive factors related to the likelihood will seek genetic testing ([Pasacreta 2003](#Pasacreta)), although greater perceived risk seemed one of the few variables consistently related with intent.

Of note in this study was that responses to the main variable of intention appear somewhat bimodal, with relatively few people choosing the middle, _it depends_ response. This suggests that people have fairly certain opinions regarding their likelihood of seeking genetics information. Also of interest was that self-reported understanding of genetics was not a significant predictor of intent. Yet, chi-squared tests showed that there is a statistically significant association between self-reported understanding of genetics and the perception that cancer runs in the family (>?<sup>2</sup> [df=3] = 16.7; p < .01), and also between self-reported understanding and worry (>?<sup>2</sup> [df=9] = 20.3; p < .05). Although self-reported understanding and actual understanding could be vastly different, this could seem paradoxical if one were to expect that those who claim to have a stronger understanding of genetics would show a higher level of intent.

In addition, those who are more convinced that cancer runs in their family seem more likely to seek cancer genetic information. However, several previously cited studies have shown that many individuals will ultimately avoid genetic information despite facing a very real threat of cancer. This implies that intention has a complicated relationship with other factors, which may ultimately affect one's information seeking actions. For instance, as Johnson ([1997](#Johnson)) explains, cancer-related information seekers may be classified as being at one of four _stages_ of involvement with cancer. These stages range from those furthest removed from cancer (those who may seek information simply out of curiosity or encounter it serendipitously), to those more directly confronting a diagnosis or increased susceptibility. Being at a particular stage means a person will face different information needs and emotional problems, any of which can impact his or her intention to seek information. Thus, an interesting finding in the literature is that while intent may be high in populations who are furthest from cancer, these levels decrease in those with more directed information needs. With regard to genetic testing, for instance, many people who initially say they would like to have a test (many of whom actually do get tested) eventually decide not to receive their test results, even though they were determined to be at a higher risk for certain cancers ([Armstrong _et al._ 2002](#Armstrong); [Lee _et al._ 2002](#Lee); [Lerman _et al._ 1999](#Lerman); [Lerman, _et al._ 1996](#Lerman2); [Welkenhuysen _et al._ 2001](#Welkenhuysen)). It may be that a health care industry, which is already under pressure, may find that those who need the information less (but who may be _hyper_ information seekers) will nevertheless seek genetic testing or other services (e.g., counselling), while those who may have a higher need for such information may avoid it, potentially exacerbating health problems. Marketing of genetic testing to the general population could further exacerbate the problem, adding stress to the health care system ([Bunn, _et al._ 2002](#Bunn)).

Other factors that could affect intent stem from the complex nature of genetic information and the overwhelming amount of health information available. Clearly, health information seeking is among the most popular activities of Internet information seeking ([Pew Internet and American Life Project 2003](#Pew)). The ostensible benefit is that Internet sources may lead to better understanding, and may encourage one to seek advice from a medical professional. A separate study based upon the same survey data used here supported the idea of a shift from the traditional _two-step flow_ view of information (where information was primarily obtained second-hand from friends and acquaintances) to a marked increase in use of the internet for health information seeking ([Case, _et al._ 2004](#Case)). The results showed that most individuals were likely to turn first to the Internet for information about cancer genetics, second to a public library, and third to a doctor, although, overall, doctors were more likely to be the first information source when second and third choices are considered ([Case _et al._ 2004](#Case)).

However, the current health care environment is such that there is the expectation that people must find, access, and make effacious use of complex health information in order to be active participants in making health decisions. This is a challenge for most people since many have less than optimal health information literacy skills. Moreover, health information literacy may be affected by a person's sense of efficacy, which can determine whether one will be a successful information seeker. This is especially true in the case of cancer genetics: lack of understanding or general misconceptions about genetics, including a person's subjective sense of risk, can affect whether or not one will seek cancer genetic information. Other antecedents, such as socio-demographic variables, also could affect successful information seeking actions ([Johnson 1997](#johnson)). Given that the use of the Internet for seeking genetic information may prove more difficult for many, more attention is needed to providing complex information in a form that may be most useful to consumers. The National Library of Medicine has begun to address this by its recent launching of the [Genetics Home Reference](http://ghr.nlm.nih.gov/ghr/page/Home) site (<a>Mitchell, _et al._ 2004</a> ), which offers a variety of genetics information from basic definitions to news for consumers and health care professionals . Moreover, sources such as the [National Cancer Institute](http://www.cancer.gov/), have many useful tools for people to learn more about genetics.

## Conclusions

People are facing new information seeking challenges because of the rapid advances in genetic mapping and genetic testing, which are anticipated to continually accelerate in the near future ([Collins & McKusick 2001](#Collins)). The role genetics will play in our personal health care promises to be great, and people will need to access quality and understand information to make better decisions regarding their own health and that of their families. As demand for individual counselling and testing may increase, however, the health care infrastructure may not be ready to respond. In addition, the fact that minority status was shown to be a significant predictor of intent amplifies the critical issues related to under-served populations and their special health information needs, particularly as the results showed that while minority status is a predictor of intent, it is not associated with the desire for help.

We need to understand better the average citizen's level of health information literacy and overall capacity to seek and comprehend such complex information, so that those most in need of genetic counselling or testing may obtain such information at times when it could be most useful. Interventions should involve the [Cancer Information Service](http://cis.nci.nih.gov/) (which already effectively communicates highly complex cancer information to the lay public), and should focus on providing not just information, but the skills people need to seek information on their own throughout their lives. Based on the results of this study, such interventions also must take into account the particular information seeking needs and barriers of minority populations.

## References

*   <a name="alpert" id="alpert"></a>Alpert, S. (2003). Protecting medical privacy: challenges in the age of genetic information. _Journal of Social Issues_, **59**(2), 301-322.
*   <a name="ama" id="ama"></a>American Medical Association, _Ad Hoc committee on Health Literacy for the Council on Scientific Affairs_ (1999). Health literacy: report of the Council on Scientific Affairs. _Journal of the American Medical Association_, **281**(6), 552-7\.
*   <a name="anderson" id="anderson"></a>Anderson, D.M., Meissner, H ..I. & Portnoy, B. (1989). Media use and the health information acquisition process: how callers learned about the NCI's Cancer Information Service. _Health Education Research_, **4**(3) 419-427.
*   <a name="andrykowski" id="andrykowski"></a>Andrykowski, M.A., Lightner, R., Studts, J.L. & Munn, R.K. (1997). Hereditary cancer risk notification and testing: how interested is the general population. _Journal of Clinical Oncology_, **15**(5), 2139-2148.
*   <a name="andrykowski2" id="andrykowski2"></a>Andrykowski, M.A., Munn, R.K. & Studts, J.L. (1996). Interest in learning of personal genetic risk for cancer: a general population survey. _Preventive Medicine_, **25**(5), 527- 536
*   <a name="armstrong" id="armstrong"></a>Armstrong, K., Weber, B., Ubel, P.A., Guerra, C. & Schwartz, S. (2002). Interest in BRCA1/2 testing in a primary care population. _Preventive Medicine_, **34**(6), 590-595.
*   <a name="avins" id="avins"></a>Avins, M. (2000, August 7). Genome map success: much yet to discover. _Los Angeles Times_, Part E, 1
*   <a name="bell" id="bell"></a>Bell, J. (1998). The new genetics in clinical practice. _British Medical Journal_, **316**(7131), 618-620.
*   <a name="bluman" id="bluman"></a>Bluman, L.G., Rimer, B.K., Berry, D.A., Borstelmann, N., Iglehart, J.D., Regan, K., Schildkraut, J. & Winer, E.P. (1999). Attitudes, knowledge, and risk perceptions of women with breast and/or ovarian cancer considering testing for BRCA1 and BRCA2\. _Journal of Clinical Oncology_, **17**(3), 1040-1046.
*   <a name="bosompora" id="bosompora"></a>Bosompora, K., Flynn, B.S., Shikaga, T., Rairikar, C.J., Worden, J.K. & Solomon, L.J. (2000). Likelihood of undergoing genetic testing for cancer risk: a population-based study. _Preventive Medicine_, **30**(2), 155-166.
*   <a name="bunn" id="bunn"></a>Bunn, J.Y., Bosompra, K., Ashikaga, T. Flynn, B.S. & Worden, J.K. (2002). Factors influencing intention to obtain a genetic test for colon cancer risk: a population-based study. _Preventive Medicine_, **34**(6), 567-577.
*   <a name="burke" id="burke"></a>Burke, W. & Emery, J. (2002). Genetics education for primary-care providers. _Nature Reviews: Genetics_, **3**(7), 561-566.
*   <a name="case" id="case"></a>Case, D.O., Johnson, J.D., Andrews, J.E., Allard, S.L. & Kelly, K.M. (2004). From two-step flow to the Internet: the changing array of sources for genetics information seeking. _Journal of the American Society for Information Science and Technology_, **55**(8), 660-669.
*   <a name="case05" id="case05"></a>Case, D.O., Andrews, J.E., Johnson, J.D. & Allard, S. (2005) Avoiding versus seeking: the relationship of information seeking to avoidance, blunting, coping, dissonance and related concepts. _Journal of the Medical Library Association_, **93**(3), 48-57.
*   <a name="chaliki" id="chaliki"></a>Chaliki, H., Loader, S., Levenkron, J.C., Logan-Young, W., Hall, W.J. & Rowley: T. (1995). Women's receptivity to testing for a genetic susceptibility to breast cancer. _American Journal of Public Health_, **85**(8), 1133-1135.
*   <a name="collins" id="collins"></a>Collins, F.S. & McKusick, V.A. (2001). Implications of the Human Genome Project for medical science. _Journal of American Medical Association_, **285**(5), 540-544.
*   <a name="diefenbach" id="diefenbach"></a>Diefenbach, M.A., Schnoll, R.A., Miller S.M. & Brower, L. (2000). Genetic testing for prostate cancer: willingness and predictors of interest. _Cancer Practice_, **8**(2), 82-86\.
*   <a name="durfy" id="durfy"></a>Durfy, S.J., Bowen, D.J., McTiernan, A., Sporleder, J. & Burke, W. (1999). Attitudes and interest in genetic testing for breast and ovarian cancer susceptibility in diverse groups of women in Western Washington. _Cancer Epidemiology, Biomarkers and Prevention_, **8**(2), 369-375.
*   <a name="freimuth" id="freimuth"></a>Freimuth, V.S. (1987). The diffusion of supportive information. In T. L. Albrecht & M. B. Adelman (Eds.), _Communicating social support_, (pp. 212-237). Newbury Park, CA: Sage.
*   <a name="gould" id="gould"></a>Gould, C.C. & Pearce, K. (1991). _Information needs in the sciences: an assessment._ Mountain View, CA: The Research Libraries Group.
*   <a name="greendale" id="greendale"></a>Greendale, K. & Pyeritz, R. E. (2001). Empowering primary care health professionals in medical genetics: How soon? How fast? How far? _American Journal of Medical Genetics_, **106**(3), 223-232.
*   <a name="hinds" id="hinds"></a>Hinds, C., Streater, A. & Mood, D. (1995) Functions and preferred methods of receiving information related to radiotherapy: perceptions of patients with cancer. _Cancer Nursing_, **18**(5), 374-84.
*   <a name="jallinopa" id="jallinopa"></a>Jallinoja, P., Hakonen, A., Aro, A.R., Niemela, P. , Hietala, M., Lonnqvist, J., Peltonen, L. & Aula P. (1998). Attitudes towards genetic testing: analysis of contradictions. _Social Science & Medicine_, **46**(10), 1367-1374.
*   <a name="johnson" id="johnson"></a>Johnson, J. D. (1997). _Cancer-related information seeking style='font-style:normal._ Cresskill, NJ : Hampton Press.
*   <a name="johnson2" id="johnson2"></a>Johnson, J. D., Andrews, J. E. & Allard, S. (2001). A model for understanding and affecting genetics information seeking. _Library and Information Science Research,_ **23**(4), 335-349.
*   <a name="johnson3" id="johnson3"></a>Johnson, J.D., Andrews, J.E., Case, D. O. & Allard, S. (2005). Genomics: the perfect information seeking research problem. _Journal of Health Communication_, **10**(3), 1-7.
*   <a name="kash" id="kash"></a>Kash, K.M., Ortega-Verdejo, K., Dabney, M.K., Holland, J.C., Miller, D.G. & Osborne, M.P. (2000). Psychosocial aspects of cancer genetics: women at high risk for breast and ovarian cancer. _Seminars in Surgical Oncology_, **18**(4), 333-338.
*   <a name="kinney" id="kinney"></a>Kinney ,A.Y., Choi, Y., DeVillis, B., Millikan, R., Kobetz, E. & Sandler, R.S. (2000). Attitudes toward genetic testing in patients with colorectal cancer. _Cancer Practice_, **8**(4), 178-186.
*   <a name="klausner" id="klausner"></a>Klausner, R. (1996). _The nation's investment in cancer research_ Bethesda, MD: National Cancer Insititute.
*   <a name="kreps" id="kreps"></a>Kreps, G.L. (1991). The pervasive role of information in health and health care: Implications for health communication policy. _Communication Yearbook_, **11**, 238-276.
*   <a name="lee" id="lee"></a>Lee, S., Bernhardt, B.A. & Helzlsouer, K.J. (2002). Utilization of BRCA1/2 genetic testing in the clinical setting. _Cancer_, **94**(6), 1876-85.
*   <a name="lerman" id="lerman"></a>Lerman, C., Hughes, C., Trock, B.J., Meyers, R.E., Main, D. & Bonney, A. (1999). Genetic testing in families with hereditary nonpolyposis colon cancer. _Journal of the American Medical Association_, **281**(17), 1618-1622.
*   <a name="lerman2" id="lerman2"></a>Lerman, C., Marshall, J., Audrain, J. & Gomez-Caminero, A. (1996). Genetic testing for colon cancer susceptibility: anticipated reactions of patients and challenges to providers. _International Journal of Cancer_, **69**(1), 58-61.
*   <a name="lerman3" id="lerman3"></a>Lerman, C., Narod, S., Shulman, K., Hughes, C., Gomez-Caminero, A., Bonney, G. _et al._ (1996). BRCA1 testing in families with hereditary breast-ovarian cancer: a prospective study of patient decision making and outcomes. _Journal of the American Medical Assocation_, **275**(24), 1885-1892.
*   <a name="lerman4" id="lerman4"></a>Lerman, C., Rimer, B. & Engstrom: F., (1989). Reducing avoidable cancer mortality through prevention and early detection regimens. _Cancer Research_, **49**(18), 4955-4962\.
*   <a name="lerman5" id="lerman5"></a>Lerman, C ., Seay, J., Balshem, A. & Audrain, J. (1995) Interest in genetic testing among first-degree relatives of breast cancer patients. _American Journal of Medical Genetics_, **57**(3), 385-92.
*   <a name="ludman" id="ludman"></a>Ludman, E., Curry S., Hoffman, E. & Taplin, S. (1999). Women's knowledge and attitudes about genetic testing for breast cancer susceptibility. _Effective Clinical Practice_, **2**(4), 15-36\.
*   <a name="marteau" id="marteau"></a>Marteau, T.M. & Croyle, R.T. (1998). The new genetics: psychological responses to genetic testing. _British Medical Journal_, **316**(7132), 693-696.
*   <a name="mitchell" id="mitchell"></a>Mitchell, J.A., Fun, J. and McCray, A.T. (2004). Design of Genetics Home Reference: a new NLM consumer health resource. _Journal of the American Medical Informatics Association_, **11**(6), 439-447.
*   <a name="parker" id="parker"></a>Parker-Pope, T. (2004, November 16). The hidden risks in your family tree. _The Wall Street Journal_, D.1.
*   <a name="pasacreta" id="pasacreta"></a>Pasacreta, J.V. (2003). Psychosocial issues associated with genetic testing for breast and ovarian cancer risk: an integrative review. _Cancer Investigation_ **21**(4), 588-623\.
*   <a name="pew" id="pew"></a>Pew Internet and American Life Project. (2003). [Internet health resources: health searches and email have becom more commonplace, but there is room for improvement in searches and overall Internet access.](http://www.pewinternet.org/pdfs/PIP_Health_Report_July_2003.pdf) Internet and American Life Project. Retrieved 6 December, 2004 from http://www.pewinternet.org/pdfs/PIP_Health_Report_July_2003.pdf.
*   <a name="petersen" id="petersen"></a>Petersen, G., Larkin, E., Codori, A., Wang, C., Booker, S., Bacon, J., Giardiello, F. & Boyd: (1999). Attitudes toward colon cancer gene testing: survey of relatives of colon cancer patients. _Cancer Epidemiology, Biomarkers & Prevention_, **8**(4), 337-344\.
*   <a name="pifalo" id="pifalo"></a>Pifalo, V., Hollander, S., Henderson, C. L., DeSalvo, P. & Gill, G.P. (1997). The impact of consumer health information provided by libraries: the Deleware experience. _Bulletin Medical Library Association_, **85**(1), 16-22\.
*   <a name="rothstein" id="rothstein"></a>Rothstein, M.A. (1997). Genetic secrets: a policy framework. In M. A. Rothstein (Ed.), _Genetic secrets: protecting privacy and confidentiality in the genetic era._ (pp. 451-495). New Haven, CT: Yale University Press.
*   <a name="savolainen" id="savolainen"></a>Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of "Way of Life". _Library and Information Science Research_, **17**(3), 259-294.
*   <a name="smith" id="smith"></a>Smith K.R. & Croyle, R.T. (1995). Attitudes toward genetic testing for colon cancer risk. _American Journal Public Health_, **85**(10), 1435-1438\.
*   <a name="steen" id="steen"></a>Steen, R.G. (1993). _A conspiracy of cells: the basic science of cancer._ New York, NY: Plenum Press.
*   <a name="stopfer" id="stopfer"></a>Stopfer, J. E. (2000). Genetic counselling and clinical cancer genetics services. _Seminars in Surgical Oncology_, **18**(4), 347-357.
*   <a name="street" id="street"></a>Street, R.L. (1990). Communication in medical consultations: a review essay. _Quarterly Journal of Speech_, **76**(3), 315-332\.
*   <a name="struewing" id="struewing"></a>Struewing, J.P., Lerman, C., Kase, R.G., Giambarresi, T.R. & Tucker, M.A. (1995). Anticipated uptake and impact of genetic testing in hereditary breast and ovarian cancer families. _Cancer Epidemiology, Biomarkers & Prevention_, **4**(2), 169-173.
*   <a name="tambor" id="tambor"></a>Tambor, E.S., Rimer, B.K. & Strigo, T.S. (1997). Genetic testing for breast cancer susceptibility: awareness and interest among women in the general population. _American Journal of Medical Genetics_, **68**(1), 43-49\.
*   <a name="thomsen" id="thomsen"></a>Thomsen, C.A. & Maat, J.T. (1998). Evaluating the cancer information service: a model for health communications. Part 1\. _Journal of Health Communication_, **3**(Suppl.), 1-14.
*   <a name="uscensus" id="uscensus"></a>U.S. Census Bureau. (n.d.) [Fact sheet: Kentucky](http://factfinder.census.gov/servlet/SAFFFacts?_event=Search&geo_id=&_geoContext=&_street=&_county=&_cityTown=&_state=04000US21&_zip=&_lang=en&_sse=on&pctxt=fph&pgsl=010). Washington, DC: U.S. Census Bureau. Retrieved 28 May, 2005 from http://factfinder.census.gov/servlet/SAFFFacts?_event=Search&geo_id=&_geo Context=&_street=&_county=&_cityTown=&_state=04000US21&_zip=&_lang=en&_sse=on&pctxt=fph&pgsl=010
*   <a name="usdoh" id="usdoh"></a>U.S. Dept. of Health and Human Services (2000). _Healthy people 2010_(2nd. ed.) (Vol. 1). Washington, DC: Government Printing Office.
*   <a name="wak78" id="wak78"></a>Waksberg, J. (1978). Sampling methods for random digit dialing. _Journal of the American Statistical Association_, **73**(361), 40-46.
*   <a name="welkenhuysen" id="welkenhuysen"></a>Welkenhuysen, M., Evers-Kiebooms, G., Decruyenaere M., Claes, E. & Edayer, L. (2001). A community based study on intentions regarding predictive testing for hereditary breast cancer. _Journal of Medical Genetics_, **38**(8), 540-547.
*   <a name="wortman" id="wortman"></a>Wortman, M. (2001, January/February). [Medicine gets personal](http://www.technologyreview.com/articles/01/01/wortman0101.asp). _Technology Review_, 72-78\. Retrieved 28 May, 2005 from http://www.technologyreview.com/articles/01/01/wortman0101.asp
*   <a name="zemore" id="zemore"></a>Zemore, R. & Shepel, L.F. (1987). Information seeking and adjustment to cancer. _Psychological Reports_, **60**(3), 874.

