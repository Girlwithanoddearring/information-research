#### Vol. 10 No. 4, July 2005



# Operational use of electronic records in police work

#### [Erik Borglund](mailto:erik.borglund@miun.se)  
Department of Information Technology and Media  
Mid-Sweden University, Härnösand, Sweden


#### Abstract

> **Introduction**. This research is about how police officers use electronic records in their operational work, reacting to calls from the dispatch centre.  
> **Method**. Three different qualitative studies, involving interviews and observations, were used to collect data. The studies were performed at a police county in the north of Sweden during 2003.  
> **Analysis**. The empirical data were analysed using a hermeneutical analysis technique without computer-based support. Iterative analysis was performed, where empirical data related to the use of electronic records were identified and grouped together into larger categories, which were named descriptively.  
> **Results**. There is a widespread use of information retrieved from electronic records. This has changed the possibilities for police officers to get reliable and authentic information for both tactical and legal decisions, which increases the officers' ability to make correct decisions within operational work. Information technology and information systems now handle many administrative tasks, and allow access to and searching of electronic information independently of the physical location of the officer. This opens up a possibility for mobile access to trustworthy information that supports police work.  
> **Conclusion**. Operational police work has changed in a positive direction by the use of electronic records, and it is possible to develop this change even further. Record management systems designed to support access independent of physical location of the police officer could open up new possibilities for working police officers.



## Introduction

Electronically created records have put archival science on test. How valid are old theories and methods? Some even regard electronic records as being different from traditional records, leading to a paradigm shift in archival science ([Cook 2001](#Cook); [Delmas 2001](#Delmas); [Gehrlich 2002](#Gehrlich); [Gilliland-Swetland 2000](#Gilliand-Swetland); [Lövblad 2000](#Lovblad2000); [2002](#Lovblad2002); [Menne-Haritz 2001](#Menne-Haritz)). Research about electronic records has mainly focused on how to define the problems occuring when records become electronic and limited efforts has been made to give possible solutions, although some guidelines exist ([Bearman 1994](#Bearman); [Duranti 2000](#Duranti2000); [2001](#Duranti2001)).

A record in this paper is defined as: _Recorded information in any form or medium, created or received and maintained, by an organization or person in the transaction of business or the conduct of affairs._ ([International Council on Archives 2000](#ICA): 11). A record could have the following quality criteria:

*   **Evidence:** a record can serve as evidence of transactions, processes, decisions and performed objectives ([Thomassen, 2001](#Thomassen)). A record is kept for a purpose to be a proof of what an organization, agency or administration has done, as evidence of the action ([Sprehe 2000](#Sprehe)).
*   **Authenticity:** according to Duranti ([2001](#Duranti2001)) and Andersson ([1996](#Andersson)) authenticity is needed if electronic records should be trustworthy and be able to be used within an organization over time.
*   **Accountability:** in public organizations accountability is necessary to be able to find a person or persons accountable for their actions. It is also necessary to be able to trace decisions made within an organization ([Bearman 1994](#Bearman); [Meijer 2001](#Meijer)).

Electronically created records have become a natural element within public administration, despite the existing and identified problems of managing such records. This paper has a different approach to that of other research papers within the archival community in that it focuses on the benefits of using electronic records and has this research question:

How are electronic records used within operational work, and how does the use of electronic records affect that work?

In this paper operational work is represented by two police officers working together on a shift in a patrol car, responding to calls from the dispatch centre. The police can be seen as a time-critical and knowledge-intensive organization ([Chen _et al._ 2002](#Chen)). The time-critical factor relates to the fact that police officers need to make decisions within seconds, and those decisions must be well-grounded on fact and knowledge. Information technology and computer-based information systems have become a support in operational police work, but, in this paper, _information systems_ is used to refer to both computer-based information systems and non-computer-based systems. The use of information technology has also changed information and record management, from paper-based to digital-based ([Chen et al., 2002](#Chen)).

This paper is based on three different studies carried out in the Swedish police. The police are a public administration, therefore it could be for the public good if the use of electronic records in operational work were better understood. Knowledge about the operational use of electronic records, could serve as a basis for the design of technical solutions, as well as for the development of new working methods for the police.

### Archives in Swedish public organizations

Sweden differs from Anglo-Saxon countries in its perspective on records and archives. Many countries separate records from archives, and record managers and archivists handle their management. In Sweden, archivists manage both records and archives, and there is no clear border between the two ([Ulfsparre 1998](#Ulfsparre)). This is mainly because of the Freedom of the Press Act, which declares the right of free access to public records ([Hörnfeldt 1998](#Hornfeldt)). The archives of Swedish authorities must be organized in a way that enables access to public records, as declared in the Public Records Act ([Hörnfeldt 1998](#Hornfeldt)).

### Previous research of electronic record within the police domain

Other research on the use of electronic records has been conducted in police domain. The main interest has been in the design information systems and how the use of information technology has changed the ability to gather information from many sources and present them as one unit. As the police can be seen as a time-critical and knowledge-intensive organisation ([Chen _et al._ 2002](#Chen)), information retrieval is of vital interest. Studies have been undertaken where new information systems were built, which gathered information from different sources, and visualized the information as one unit on the screen ([Brahan,_et al._ 1998](#Brahan); [Chen _et al._ 2002](#Chen); [Oatley & Ewart 2003](#Oatley); [Redmond & Baveja 2002](#Redmond)). Common to these research studies is that they use information from records to present a basis for decisions, both on a strategic level as well as on an operational level. In these studies a _record_ is interpreted as recorded information within different database systems. The aim of these research studies was to design information technology solutions, which could make police work more efficient.

How police officers search and retrieve information during pre-trial investigations was the topic of research carried out in Finland. The research identified a more complex information retrieval process than the researchers expected. Information systems were the main source of information. Some of those information systems also managed electronic records ([Kostiainen _et al._ 2003](#Kostiainen)).

## Method

The empirical material in this paper is based on three different studies, all with a common topic, the use of information. The studies were carried out in 2003 at a police county in the north of Sweden, and all three studies were based on a qualitative research approach. The topics of the studies were:

*   Communication and life-long learning in police practice.
*   The use of police intelligence in police practice, a hermeneutical approach.
*   Information management in operative police practice.

None of the selected studies had its main focus on electronic records. Despite this, the large collection of empirical data from these studies represents a great deal of operational police work and in that work, police officers use and create electronic records. Use of electronic records is an integral part of operational police work.

In total twenty-seven semi-structured interviews were carried out following a question guide made for each study. All interviews were recorded and transcribed afterwards. In addition to the interviews, participant observations were made when the author was working as a police officer to a total of approximately fifty hours within these three studies. Apart from these three studies two complementary interviews were held. The three studies were performed at a police department in the north of Sweden.

### Course of action

The phenomenon which the empirical analysis aimed to clarify and to understand in this research was the use of electronic records. The purpose was to understand how police officers used electronic records in their operational work and if the use in any way changed police work, that is, how police officers actually performed their work. A hermeneutical approach is usable when the researcher wishes to interpret a social situation where humans are involved ([Svensson & Starrin 1994](#Svensson)). In this work hermeneutics has been used as underlying analytical methodology following the guidelines presented in Svensson & Starrin ([1994](#Svensson)).

The empirical data were first read through to allow the researcher to create a first impression of the material. This first impression must be interpreted in relation to the purpose of this study, though the original empirical data had already been analysed for other purposes. After having read through every transcription of the interviews all identified use of electronic records were highlighted. Each separate description of use was then described shortly and written on adhesive notes, e.g. use for information collection, use in operational work, and planning. These descriptions were the first step towards a categorization. Secondly, the material was evaluated and similarities and differences in the material were found, and labelled. The material was analysed from the whole to its parts, as suggested by Svensson & Starrin ([1994](#Svensson)). The adhesive notes were grouped together on a whiteboard, and the groups created larger categories. The reading of the transcriptions and the categorization was performed iterative, to minimize the risk of misunderstanding of the empirical data. Based on the second step, the findings were categorized and an explanation and meaning was added to each category. In this work the categories found are those presented in the results section, and are there described by their own subheadings. The explanation and meaning were descriptive, that is, making the category understandable. The fourth and final step of the hermeneutical analysis was to analyse how categories were connected to each other. When the empirical material was briefly read through a need for complementary interviews was identified and these were also carried out. Two interviews were performed to complement the original empirical data. An IT archivist and superior police officers on administration level were interviewed.

### The research domain

The Swedish police are governed by the Department of Justice and is one element of the Swedish legal system. The Swedish police consists of approximately 16,300 police officers, who work in one of the twenty-one police authorities, which are identical in area to the twenty-one Swedish counties ([Pettersson _et ak,_ 2001](#Pettersson)).The police career is a popular choice, and only five percent of applicants actually reach the Swedish National Police Academy, and the two-year long police training programme. The pedagogical approach at National Police academy is case studies, which are intended to prepare the students for real police practice ([Pettersson _et al._ 2001](#Pettersson)).

Since early 1990 the Swedish police have changed their work to be more pro-active, which, in Sweden, is called 'problem oriented policing' or 'community policing'. Roughly two types of police practice can be identified, reactive police work and pro-active police work. Police officers, who are working reactively, respond to events. This often means responding to 911-calls (emergency calls) from the dispatch centre, a work practice where information is used to manage risk and situated action. In pro-active police work police officers try to prevent crimes, before something happens ([Lundin & Nuldén 2003](#Lundin)). The majority of Swedish police officers are categorized as community police officers, with a task to work pro-actively to prevent crime. The problem in _problem oriented policing_ is about how police officers should work. Based on statistical material, crime-related problems should be identified and the police officers should analyse the situation and plan their police work to solve the problem, and by doing so also prevent crimes. For example, if a street has a large incident of thefts from cars the police should work together with the insurance companies to inform the car owners of the risk and make proposals to the municipality to improve the number of streetlamps, etc. By being part of a crime preventative work, the police work pro-actively, with the aim of reducing the number of reported crimes ([Pettersson _et al._ 2001](#Pettersson)).

The empirical data from the three studies, which are the basis of this research paper, were collected in one police department, a police county in northern Sweden. The population is approximately 244,000 people and the area covered is 21,700 square kilometers (approximately the size of the state of New Jersey in the USA) ([Länstyrelsen, 2001](#Lansstyrelsen2005)). The county consists of five cities which all have local police stations. The three studies were mainly performed at one of these local police stations. The police station works mainly according to the problem-oriented mode. Beside the community policing the police station is obliged to keep a patrol car manned twenty-four hours a day, with responsibility for responding to emergency calls within a geographical area of the county. They are also obliged to investigate all crimes reported within their geographical area of responsibility.

## Result

This section presents the results from the three studies, which served as empirical sources for this work. Only the results that in some way deal with electronic records are presented.

### Electronic records within the Swedish police

There are three sorts of diaries in Swedish police, which provide examples of the different kinds of records:

*   A-diary, dealing only with administrative tasks. The administrative task could also be part of a police main business.
*   K-diary, every document concerning criminal investigations manages by this diary.
*   T-diary, a diary for police reports of traffic offences.

Information technology use aims to increase the efficiency of police work. The Swedish police have several computer-based information systems, and several of those information systems have become a necessary tool for police work. Some information systems manage records and their contents should be preserved long-term. When new information systems are developed the system is presented to the Swedish National Archive, which decides if the contents of the system are to be preserved. Long-term preservation means that the contents of the system are so valuable for the public that it must be preserved forever.

The police have a National Police computer network and every police station has PCs connected to this network. Those terminals are only connected to the police network and not to the Internet. The majority of the systems accessible on the network are national. All national police systems can be accessed from every network connected work terminal. The access is secured with a public key infrastructure (PKI), where each officer is given personal authority for access to systems necessary for his or her work. Each police officer has a personal smart card, with information needed to verify the officer's rights of access to the police information systems. Information from the smart card is read through a card reader and each terminal verifies the right of access. Every action taken by an officer within the police information systems is logged in separate log files, to ensure proof if the information systems have been misused. The information systems, in Table 1, handle electronic record which all must be preserved long-term.

<table width="456" border="1" cellspacing="0" cellpadding="3" style="background-color: #FDFFDD; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: smaller; font-style: normal; border: solid;" align="center"><caption align="bottom">  
**Table 1: Information systems the content of which should be preserved forever**</caption>

<tbody>

<tr>

<th>System</th>

<th>Task</th>

</tr>

<tr>

<td>DurTvÅ</td>

<td>Computerized system for managing criminal investigations</td>

</tr>

<tr>

<td>RAR</td>

<td>A computerized system for making police reports</td>

</tr>

<tr>

<td>VÄS</td>

<td>A national system for managing all legal weapons in Sweden</td>

</tr>

<tr>

<td>PASS</td>

<td>A system for managing passports</td>

</tr>

<tr>

<td>STORM</td>

<td>A command and control system at the dispatch centre, which manages all police activity</td>

</tr>

</tbody>

</table>

Besides those information systems the Swedish police also have an intranet (IntraPolis) and an e-mail system (GroupWise) for internal information sharing. Those two systems have contents that need to be preserved for varying time periods. The police have several information systems supporting different operational tasks. Those systems are not nationally regulated or controlled. At the police county in the studies for this paper, there was one system of interest handling police intelligence. Police officers had to apply for authority to access that system.

Police information systems are designed to make information available and to make police work more efficient and not to manage records. All information systems are designed with some database solution, enabling quite advanced search functionality.

Electronic records within the police information systems are not automatically public. Requests for records are always followed by an analysis of whether the record is protected by secrecy.

All the systems mentioned above were designed and developed to computerize tasks, which previously were handled manually. It was possible to receive or capture all of the information that these systems can deliver or present previously, when the tasks were managed manually. However, when the records were handled manually access for police officers was limited and it was not easy to search, find and get the information.

### Operational use of records

#### Information retrieval process

All police officers involved in these studies agreed on the importance of information in police practice. Police practice and police work is based on information, which is used to support decision-making both tactically and legally. Police officers retrieve information either from others or by their own active search and the gathering mostly relates to needs that have their origin in police work. The police officers commented that they never know when the information could be needed. They made the selection of the most relevant and necessary information themselves, based upon experience and actual needs. Several police officers mentioned the connection between information and knowledge and the need always to gather, or try to gather, as much information as possible. One officer noted: _Information as well as knowledge is not a burden, it is an easy load to carry._

One of the most important sources of information is the police information systems. The majority of police officers in these three studies ranked information systems as the major source for relevant information. If they did not find what they were looking for in those systems, only then did they use other sources.

Police officers use the information systems presented in Table 1 as source of information. The following excerpts from the field study explain how electronic record serves as source of information.

> Police officer, Hakan, starts his duty at 17.00 a Friday evening. After having equipped himself, he enters the report-writing room at the station, which is the natural meeting place for officers at the station. He logs in to one of the workstations and starts to read his e-mail. His mail could consist of some directives for personnel on duty this weekend from the local superior officers. After reading all relevant e-mail, he logs in to the police intelligence database and reads all analysed and unanalysed intelligence of interest. He finally uses the command and control system, STORM, to get a brief view of the situation in the county. Finally, he reads newly published post on the county's portal on IntraPolice. After he has logged out from the workstation he begins to read faxes, which have arrived, police reports in paper form and other information sources on paper. (Excerpt 1)

This excerpt shows how information systems support information gathering and information retrieval. Before these systems existed the police officers had to read everything in paper form. Some of the systems described above are bringing new access possibilities, e.g. the precursor to the command and control system STORM was paper-based and no police officers were able to read those paper sheets, if they were not at the dispatch centre.

> A police patrol has found a sports bag full of goods and they suspect that they are stolen goods. They bring the goods to the station where they search in RAR to find out if some of the goods can be connected to a police report. This time there were no positive hits, and the goods were documented as lost property. (Excerpt 2)

Search possibilities have increased since the computerized system for making police report were introduced. Before RAR was developed, officers could search for goods in a national goods database, but the database was out of date and rarely gave any hits. When police officers make a report and use RAR, the police officers are helped by the system to fill in fields, according to which the database can be searched.

> Two police officers were going to plan a two-month job with a specific aim: to arrest persons who sell narcotics and illicitly distilled alcohol. They started to read intelligence reports in the police intelligence systems. They found several intelligence reports pointing out a group of persons. The officers used RAR, STORM and DurTVÅ to gather more information about those persons. They gathered enough information to be able to present their plan to superior officers, and began their work. The result was among other things a revelation of a widespread use of narcotics at a high school, which resulted in several arrests. (Excerpt 3)

This excerpt shows how officers use information systems and records for decision-making and to be able to know what to give priority. When these information systems were not available, the amount of data for decision making was limited and there were limitations in the possibilities to search information about persons, addresses and types of crime.

#### Information communication process

Police officers not only retrieve and gather information actively, they also get information through automated communication processes.

> A police patrol has been ordered to an address to check an incoming call where a person has heard loud screams and thought it could be a domestic disturbance. While the police patrol is driving to the address, the dispatch centre carries out searches in STORM to see if the address has figured before and, if so, what happened, and when? The address had many notifications in STORM, and previously police officers had acted as arbitrators. The information is communicated to the police patrol by telephone to decrease the risk of the communication line being tapped. This information was also well known by the police officers on duty. (Excerpt 4)

This excerpt shows how one of the information systems is used as a source of vital information, which could be basis for decision making. STORM gives police officers possibilities to search information on whether an address, person or vehicle has been of interest to the police previously. This possibility did not exist before, when the tasks STORM handles, were documented on paper.

Many police officers are aware of the possibilities of the police information systems and, therefore, are not always satisfied with the help and support they get from the dispatch centre. However, they are aware that without the information systems the support from the dispatch centre would be even more limited.

Police officers working in pairs do communicate a lot. They talk with each other during their working shift. The communication is mainly about work-related topics. Police officers find that the informal communication around the coffee table, within the police vehicle, during physical training etc., are occasions when vital information is spread. Much of this information has its origin in the police information systems. Electronic records have been a new source of information, information that may be communicated during such informal meetings. At every work shift change, police officers traditionally share work-related information with each other.

GroupWise is the police e-mail system, which is well used for communication and distribution of information. GroupWise has become an alternative to telephone or internal paper mail. For example, police officers could get a request to write a memo as a complement to a police report through GroupWise, and s/he writes the memo and returns it by GroupWise. In this example e-mail has been used as a tool both to deliver information and to return information. A memo is one of several official documents produced in police work that has no legal requirement to be signed and, therefore, can be sent by e-mail.

#### Decision making

The majority of the police officers in these three studies interpreted information and knowledge as being of equal status. They need reliable and relevant information in order to make correct decisions both tactically and legally. Therefore, it is natural for the officers to gather as much information as possible. When they are standing in the middle of a situation, information and knowledge are their weapons. During assignments there are needs for a specific type of decision support. This is information about addresses, criminals or vehicles that are stored in the police information systems. Information about what has happened earlier at a specific location could be of vital importance for the police officers to choose the right tactical approach. In police practice the dispatch centre supports police officers with information, when they are out on duty. The above-mentioned active information search performed by police officers also aims to increase knowledge for enabling a decision making support. Police officers are aware of how quickly a situation can appear and, therefore, information can be part of a successful solution.

According to Swedish national laws, the police must contact the public prosecutor in some types of serious crimes. Often the public prosecutor has to decide if the police patrol is going to use means of coercion, or similar tasks. As basis for the decision the public prosecutor is going to take, police officers must be able to present historical information about the suspect of the crime. The historical information can be obtained with help from the dispatch centre. In some cases the information has its origin in electronic records.

The information systems RAR, DurTVÅ, STORM, PASS and VÄS have possibilities to deliver statistical data. These data are used as basis for decisions on a strategic level by the police board and the superior officers in command of the police force in the county. The statistical data are also used to analyse crime trends and to measure police effectiveness.

#### Valuation of information

Police officers assume that information delivered from the information systems mentioned above is valid and that the information itself has a legal value. The information systems were developed to replace previous manually-handled tasks, and, therefore, it is natural for police officers not to question the authenticity of the information. No police officers in these three studies have questioned the reliability and authenticity of information originating from electronic records within information systems. The management of the information systems within the police is strictly regulated and this ensures the trustworthiness of the information. Information derived from police information systems can be used as evidence both for the public prosecutor and in court proceedings. Both RAR and DurTVÅ, which are the central systems in criminal investigations, produce information used in courts of law. No doubts are expressed from other parts of the Swedish legal system about the authenticity of the electronic records produced within police information systems.

#### More accessibility

Police officers interviewed in these three studies were all disappointed with the possibility of accessing the police information system during operational work. They meant the information could be distorted when, for example, the dispatch centre mediated information from information systems. The officers wanted to be able to gather information whenever they want or need it, instead of being dependent on help from others. The cellular telephone and wearable radio are the only technical tools a police patrol can use to communicate with the dispatch centre. This causes difficulties in information gathering. Police officers comment that this affects their willingness to ask for all information they want. If they want to search for all wanted information, they must do it themselves at the police station.

The police officers never reflected upon whether their uses of records have changed when the records have become electronic instead of paper-based. They also never reflected upon the advantages this use has brought to police practice, compared to the use of traditional records. The police officers also noted how natural the use of the information systems has become in police work. Even if there were complaints about the systems, no police officer wanted the old paper-based environment back. Newly qualified police officers do not even know any other work environment.

## Discussion

In this section a discussion of the results is presented based upon the categories presented above, categories that are the result of the analysis of the empirical data.

### Presence of record

The previously presented results give some examples of how police officers use electronic records in their operational work. Every information system able to manage records is seen by police officers as a support tool for police work, not as an electronic record management system. The electronic record has an active presence in ordinary police work, and the use of electronic records is natural for the officers in these three studies. The use of electronic records can be seen as embedded in police practice where police officers are not aware of the implication of electronic records.

### Use of record in two dimensions

Roughly speaking the use of electronic records within police practice can be visualized in two dimensions:

*   Decision making for situated action
*   Decision making for planned action

Police officers use electronic records mainly as information sources to be able to make decisions. The empirical data have given some examples of this use. 'Situated action' refers to situations where police officers are on a specific assignment, irrespective of how it was assigned. In these situations information could be vital for making correct decisions and judgments. The officers use information from electronic records as one of their information sources. The widespread use of electronic records could have its basis in the authenticity of records. But in situated action the police officer has limited time and, therefore, information from other sources could also be used. Information from electronic records has increased the quality of the used information. Information derived from records in general should, by its nature, be authentic and have evidential value ([Thomassen, 2001](#Thomassen)). If an information system is developed and accepted to be used in police work, it is, by implication, also reliable. Reliability is guaranteed by the rigorous and regulated system development process of police information systems. The quality criterion, accountability, has also been visible for more officers in electronic records compared to paper-based records, because of the ease of access to electronic records.

In planned action the police officers have more time to gather authentic and trustworthy information. In these situations almost every police information system is used. The planned action needs to be well founded and in these situations electronic records play a vital role. In reactive police work police officers may need to choose among a few different tasks, but of equal importance. It is necessary to have the possibility to gather as much information as possible from reliable sources and electronic records are those kinds of sources. When using electronic records as information sources police officers do not need to estimate the quality of the information, as they need to do when using other information sources.

### Record vs. police knowledge

In this paper a situation where police officers often communicate and inform each other with work-related information is presented. The police officers in this research use electronic records as one source of information. It is observable that police officers tightly connect police-related information to police-related knowledge. Researchers within the archival science community also present the connection between knowledge and records. According to Thomassen ([2001](#Thomassen)) records are tightly connected to knowledge and memory and the information is meant to be re-used in the future. Thomassen describes recorded information as memory of individuals, society and organizations. The recorded information, for example, can serve as basis for decision-making, creating policy, presenting history, legal evidence, etc. In this research electronic records have not been found to replace any old ways whereby police officers become informed. Instead, electronic records have become a new possibility for fast and quick access to information that is reliable and authentic. Whether or not electronic sources have decreased the use of other information sources is beyond the scope of this research.

### New police practice

When the Swedish police began to develop information systems, which replaced previous manually-handled tasks, a change in police practice occurred. This paper gives examples of how police practice is today, but not how it was before the introduction of information systems. The use of information systems has made the huge amount of records handled and managed by the police:

*   searchable with rapid response times compared to manual search; and
*   accessible, independently of time and space, even if police officers need to be helped by the dispatch centre when they are outside the police station.

These two quality aspects of information and electronic records have changed police practice, a change towards a more information-dependent practice. When a record is accessible and searchable, police officers can use information from electronic records as a tool for undertaking good police work. Electronic records have given the police the possibility of an information advantage.

### Misuse of information technology

The information systems presented in Table 1 all manage records, but none of the systems is capable of managing the record over time and transforming it to a format that enables electronic archiving. The systems are all designed to transform a manual, administrative task to a very similar information system supported task. To print out contents from information systems on regular basis disables the option of searching, quick access and decision support. Police officers not working with criminal investigations do not use the police archives. The archives are not available and accessible to every police officer, as the access is restricted. The police officers want available and accessible information sources and for this the information systems in Table 1 serve their purpose. If the police also had the possibility of managing the records in electronic archives the total amount of available information would increase and police officers would also use information from electronic archives if they were given access.

## Conclusions

The research question this paper tried to answer was:

How are electronic records used within operational work, and how does the use of electronic records affect that work?

In police practice the possibility to access and use electronic records has opened up a new way of gathering information for police officers. This has created and developed new ways of working for police officers: they can gather more information when involved in operational work as well as when they are present physically at the police station. Information systems have brought them better options to be prepared for assignments by providing reliable and trustworthy information. Some of the quality criteria of electronic records are the provision of evidential value, authenticity and accountability. These criteria increase the value of the information retrieved from electronic records. Information from electronic records has already been validated and therefore the police officer can trust such information.

The options to get historical information about an address, person or vehicle have been a new chance for police officers to be prepared for what could be expected when they get an assignment. In Sweden violence against police officers has increased ([Olsson 2004](#Olsson), and one way to reduce that risk of getting hurt, during an assignment, is to make judgments on reliable and authentic information.

This paper does not give any evidence that police officers did not get the information they needed previously, when electronic records did not exist, but it describes how new possibilities have occurred using electronic records. In pro-active police work, information is crucial to be able to make the right priority, and without electronic records this sort of decision-making could be difficult.

Electronic records have become a tool for success for police officers, even if they are unaware of the concept and definition of electronic records. To develop this use further, record management through information systems must be developed and designed to support access independently of the physical location of police officers and mobile data systems are one option. The police need to make visible the presence of electronic records, and make police officers aware of this presence. If not, the use of electronic records within police practice will probably not progress further. The Swedish police should also develop electronic archives further. To create solutions that enable access to archives during operational work, could open up new possibilities for more advanced information retrieval in operational police work.

## References

*   <a name="Andersson"></a>Andersson, U. (1996). [_Short version of the Sesam report. Philosophy and rules concerning electronic archives and authenticity_](http://europa.eu.int/ISPO/dlm/dlm96/proceed-en3.pdf). Paper presented at the DLM Forum '96, Brussels. Retrieved 4 October, 2004 from http://europa.eu.int/ISPO/dlm/dlm96/proceed-en3.pdf
*   <a name="Bearman"></a>Bearman, D. (1994). _Electronic evidence: strategies for managing records in contemporary organizations_. Pittsburgh, PA: Archives and Museum Informatics.
*   <a name="Brahan"></a>Brahan, J.W., Lam, K.P., Chan, H., & Leung, W. (1998). AICAMS: artificial intelligence crime analysis and management system. _Knowledge-Based Systems_, **11**(5-6), 355-361.
*   <a name="Chen"></a>Chen, H., Schroeder, J., Hauck, R.V., Ridgeway, L., Atabakhsh, H., Gupta, H., _et al._ (2002). COPLINK connect: information and knowledge management for law enforcement. _Decision Support Systems_, **34**(3), 271-285.
*   <a name="Cook"></a>Cook, T. (2001). Archival science and postmodernism: new formulations for old concepts. _Archival Science_, **1**(1), 3-24.
*   <a name="Delmas"></a>Delmas, B. (2001). Archival science facing the information society. _Archival Science_, **1**(1), 25-37.
*   <a name="Duranti2000"></a>Duranti, L. (2000). _[The impact of technological change on archival theory](http://www.google.com/url?sa=U&start=1&q=http%3A//www.interpares.org/documents/ld_sevilla_2000.pdf&ei=xdmDQtWmHMHe4QGgs-mMDA)_. Paper presented at the International Council on Archives Conference, September 16, 2000, Seville, Spain. Retrieved 12 May, 2005 from http://www.google.com/url?sa=U&start=1&q=http%3A//www.interpares.org/documents/ld_sevilla_2000.pdf&ei=xdmDQtWmHMHe4QGgs-mMDA
*   <a name="Duranti2001"></a>Duranti, L. (2001). Concepts, principles, and methods for the management of electronic records. _The Information Society_, **17**(4), 271-279.
*   <a name="Gehrlich"></a>Gehrlich, J.L. (2002). The archival imagination of David Bearman, revisted. _Journal of Archival Organization_, **1**(1), 5-18.
*   <a name="Gilliand-Swetland"></a>Gilliland-Swetland, A.J. (2000). _Enduring paradigm, new opportunities: the value of the archival perspective in the digital environment._ Washington, D.C.: Council on Library and Information Resources.
*   <a name="Hornfeldt"></a>Hörnfeldt, T. (1998). The concept of record: on being digital. In K. Abukhanfusa (Ed.), _The concept of record: report from the Second Stockholm conference on archival science and the concept of record, 30-31 May 1996_ (pp. 67-75). Stockholm: Swedish National Archives.
*   <a name="ICA"></a>International Council on Archives. (2000). _ISAD(G): general international standard archival description, adopted by the Committe on descriptive standards, Stockholm, Sweden, 19-22 September 1999_ (2nd ed.). Madrid: Subdirección General de Archivos Estatales.
*   <a name="Kostiainen"></a>Kostiainen, E., Valtonen, M.R., & Vakkari, P. (2003). Information seeking in pre-trial investigation with particular reference to records management. _Archival Science_, **3**(2), 157-176.
*   <a name="Lundin"></a>Lundin, J. & Nuldén, U. (2003). _[Supporting workplace learning for police officers. Looking for design implications in mobile situations.](http://w3.msi.vxu.se/users/per/IRIS27/iris27-1057.pdf)_ Paper delivered to a Working Group of IRIS 27, Falkenberg, Sweden, August 14-17, 2004\. Retrieved 13 May, 2005 from http://w3.msi.vxu.se/users/per/IRIS27/iris27-1057.pdf
*   <a name="lansstyrelsen2005"></a>Länstyrelsen. (2005). [About the county of Västernorrland.](http://www.y.lst.se/english/aboutthecountyofvasternorrland.4.17431b9f544f8dca97fff1734.html) Retrieved 22 April, 2005, from http://www.y.lst.se/english/aboutthecountyofvasternorrland.4.17431b9f544f8dca97fff1734.html
*   <a name="Lovblad2000"></a>Lövblad, H. (2000). Munk, knekt eller konstnär?: arkivarien, varat och vetandet.[Monk, Knight or Artist?: the archivist, being and knowing.] _Arkiv, Samhälle och Forskning,_ (1), 28-50.
*   <a name="Lovblad2002"></a>Lövblad, H. (2002). Munk, knekt eller konstnär? (II): arkivarien, hammaren och städet.[Monk, Knight or Artist?(II): the archivist, the hammer and the anvil.] _Arkiv, Samhälle och Forskning,_ (1), 18-38.
*   <a name="Meijer"></a>Meijer, A. (2001). Accountability in an information age: opportunities and risks for records management. _Archival Science_, **1**(4), 361-372.
*   <a name="Menne-Haritz"></a>Menne-Haritz, A. (2001). Access-the reformulation of an archival paradigm. _Archival Science_, **1**(1), 57-82.
*   <a name="Oatley"></a>Oatley, G.C., & Ewart, B.W. (2003). Crimes analysis software: ‘pins in maps’, clustering and Bayes net prediction. _Expert Systems with Applications_, **25**(4), 569-588.
*   <a name="Olsson"></a>Olsson, D. (2004). [_”Jag ska sticka kniven i dig snutjävel!”:Narkotikapåverkad man uppsökte polis i hemmet_](http://www.polistidningen.org/start/article.cfm?Art_ID=495).["I will stab you with my knife, police officer!": man under the influence of narcotics visited police officer in his house.] _Polistidningen_, (2). Retrieved 4 October, 2004 from http://www.polistidningen.org/start/article.cfm?Art_ID=495
*   <a name="Pettersson"></a>Pettersson, M., Ankarberg, B., Hallgren, E.-M., & National Police Board. (2001). [_Polisen: a presentation of the Swedish police service_ ](http://www.polisen.se/inter/mediacache//4347/4637/poliseng.pdf). Stockholm: Rikspolisstyrelsen. Retrieved 4 October, 2004 from http://www.polisen.se/inter/mediacache//4347/4637/poliseng.pdf
*   <a name="Redmond"></a>Redmond, M., & Baveja, A. (2002). A data-driven software tool for enabling cooperative information sharing among police departments. _European Journal of Operational Research_, **141**(3), 660-678.
*   <a name="Sprehe"></a>Sprehe, T.J. (2000). Intergrating records management into information resources management in U.S. government agencies. _Government Information Quarterly_, **17**(1), 13-26.
*   <a name="Svensson"></a>Svensson, P.-G., & Starrin, B. (Eds.). (1994). _Kvalitativ metod och vetenskapsteori._ [Qualitative method and theory of science.] Lund: Studentlitteratur.
*   <a name="Thomassen"></a>Thomassen, T. (2001). A first introduction to archival science. _Archival Science_, **1**(4), 373-385.
*   <a name="Ulfsparre"></a>Ulfsparre, A.C. (1998). Archival science in Sweden. In K. Abukhanfusa (Ed.), _The concept of record: report from the second Stockholm conference on archival science and the concept of record, 30-31 May 1996._ (pp. 57-65). Stockholm: Swedish National Archives.

