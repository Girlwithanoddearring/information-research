####  Vol. 3 No. 3, January 1998

<
# Health information for the teenage years: what do they want to know?

#### J. Rolinson  
Department of Information and Library Studies  
Loughborough University, UK

#### Abstract

> What are the health Information needs of adolescents? What do they want to know? Sex 'n drugs 'n rock and roll? Reviewed are the information needs of adolescents within the educational setting, analyses of previous use and sources of Health Information and also the anticipated needs and preferred sources for the future. The changing need for Health Information from medical information to information relating to image and sexuality is reported. The restrictions of the educational system and the National Curriculum in teaching Health Information are identified, as are the differences in roles and responsibilities of the teachers who have a designated interest teaching Health Information.

## Introduction

In July 1992, the Government launched _[The Health of the Nation,](#heal)_ (1992). Within the document five key areas of public health were identified as focal points for government policy and action: coronary heart disease and stroke, cancers, mental illness, accidents and HIV/AIDS and sexual health. Within each key area, national targets were set.

The production of _The Health of the Nation,_ was a definite shift in philosophy from the treatment of the illness in an individual to the development of a healthy nation of individuals. Prevention rather than cure had emerged as the way forward for public health. While some targets were vaguely determined, others were clearly stated, e.g.

> "... to reduce by at least 50% the rate of conceptions amongst the under sixteen’s by the year 2000."(p.19)

Therefore the spotlight here is directly focused upon the adolescent and their behaviour, and ultimately their use and recourse to health information in this specific area.

Together with the _[Children's Act](#chil) 1993,_ and _[Children First](#audi), A Study of Hospital Services, 1993,_ the need for good personal and written information for children is emphasised. It is highlighted in the _Children First_ report, that 90% of all health care for children is provided by the primary sector, therefore it is imperative that health information for adolescents is available across the whole spectrum of the subject; not merely sexual behaviour and the use of drugs. Within schools, awareness of health information for adolescents is provided within the context of Personal and Social Education (PSE). However, recourse to adolescent needs is limited. "Censorship of some types of "adult" information, then, is constructed as legitimate before that information is deemed suitable for consumption by adolescents. The rationale for such censorship is that the innocence of children and adolescents must be protected and maintained, even if the price this extracts is their continuation in a state of ignorance. Adolescents, therefore, are denied some information and, in contrast to most adults, are in a state of relative information poverty." ([Kerslake & Rolinson](#kers), 1996:58). A short regional study inthe Department of Information and Library Studies at Loughborough University, funded by the School of Humanities, (now the Faculty of Science) aimed to investigate this under-researched area .

It is acknowledged that health information messages are thrown at adolescents from many directions, organisations and people:- the Department of Education and the Department of Health, NHS, Health Promotion, Family Planning clinics, pharmaceutical companies, doctors, school nurses, social workers and friends, youth organisations such as Scouts and Guides and of course, the mass media in various forms - teenage magazines, newspapers, television and the cinema. The focus of the study was seen as a starting point to investigate these conflicting messages. Trying to measure what health information is needed, and _where_ and _how_ it should be delivered is strewn with difficulties because of the vast scope of the remit.

## What is health information?

Health information may be defined as information on a continuum between health education and health promotion. Therefore access to health information may contribute to health education and promote healthy lifestyle choices.

> " Information is the first step to every healthy choice. Improvements in our health depend on us taking control over, and responsibility for, health as an important component of our everyday lives. This active participation requires full and continuing access to information: information about our bodies, their workings in health and illness, and the services available to us in treatment and care, support and co-operation" ([Gann](#gann), 1986)

A literature search has revealed little or no evidence of the expressed needs of adolescents addressed in the provision of health information within the community setting.

## Aims and objectives of the study

The aim of the study was to investigate the needs for health information of a sample set of Leicestershire and Nottinghamshire adolescents between the ages of 13 - 16 years. The study spanned both the private and the state sectors.

The objectives were to:

*   ascertain what the sample set of adolescents consider to be their priorities for health information needs, if any.
*   identify the various sources of information e.g. public library, GP surgery, school.
*   investigate the various media used for the communication of health information to adolescents
*   identify best practice in the provision of health information
*   identify the adolescents’ preferred source and media for the provision of health information
*   consider if the adolescents’ expressed needs are reflected in recent government health legislation

## Study protocol

The sensitive nature of personal health information, combined with seeking the views of adolescents dictated that the study investigated the various communications media used in the provision of health information for adolescents rather than the observation or monitoring of oral communication of such information on a one-to-one basis.

## Focus of the study

The focus of the study was adolescents within the community. To achieve a sample population across the whole spectrum of society, it was decided to target a sample population within the educational setting including both the state and private sector.

24 schools were short listed within Leicestershire and Nottinghamshire. Of these, 12 were selected, sufficient to give an insight into local practices yet yield enough information for discussion in the wider context. The study population was a sample of 635 students aged 13 - 16 years who were attending years 9,10 and 11 in 12 schools in Leicestershire and Nottinghamshire. The gender split was approximately 50% boys and 50% girls.

The schools were selected to represent the different types of communities within the two counties. Including schools representing the city area, a suburban area, a market town, a mining villages and a rural village, these were roughly matched across the two counties, although comparing the responses between the two counties was not part of the research protocol. Two Independent Schools, a girl’s school in Nottinghamshire and a boy’s school in Leicestershire, also formed part of the study.

Initial telephone calls were made to the schools to the Principle, Vice Principle or Head Teacher to establish protocols and ethical approval for the study. These initial telephone calls identified the member of staff who was responsible for teaching health information and who would be the contact for the study within the school. Also the purpose and methodologies were also fully explained. All details of the study were confirmed in writing to the schools.

Data were collected using a questionnaire which sought information about the health information needs of the students. They were completed in class time, with assistance and supervision from the teaching staff. The use of postal questionnaires was rejected as not being suitable because of the known low response rate to this methodology. Completion under supervision in the class room was considered essential in order to avoid the joint completion of the questionnaires by groups of students within the school or elsewhere , and also non completion. Thus a high response rate of over 98% was achieved. One-to-one interviews were also conducted with school staff responsible for, or involved in, delivering health information education.

Both quantitative and qualitative data were sought in the study. The questionnaire was designed to determine the adolescents understanding of health information and its importance in their lifestyles. It sought to find out if they had ever needed health information, and if so where they had obtained it. Their views were recorded on the quality of the information in terms of needs met and if it was written in terms that could be understood, or was it too technical. Finally the questionnaire asked for information on the sources that adolescents would prefer to use to obtain health information.

## Adolescent Views of Health Information

In response to the question " What is Health Information ", overall, and encouragingly, most respondents, regardless of gender, said that health information imparted advice and guidance, rather than facts and figures or warnings.

<table width="282" border="1" cellpadding="7" align="center" bgcolor="#FCF9BA" style="font-family: Arial, Helvetica, sans-serif;"><caption style="font-family: Arial, Helvetica, sans-serif;" align="bottom">  
Table 1: What is health information?</caption>

<tbody>

<tr>

<td valign="top" width="45%"> </td>

<th valign="top" width="28%">Boys</th>

<th valign="top" width="28%">Girls</th>

</tr>

<tr>

<td valign="top" width="45%">**Advice/Guidance**</td>

<td align="center" valign="top" width="28%">232</td>

<td align="center" valign="top" width="28%">234</td>

</tr>

<tr>

<td valign="top" width="45%">**Facts/Figures**</td>

<td align="center" valign="top" width="28%">40</td>

<td align="center" valign="top" width="28%">40</td>

</tr>

<tr>

<td valign="top" width="45%">**Warnings**</td>

<td align="center" valign="top" width="28%">50</td>

<td align="center" valign="top" width="28%">24</td>

</tr>

<tr>

<td valign="top" width="45%"> </td>

<td align="center" valign="top" width="28%">n = 322</td>

<td align="center" valign="top" width="28%">n = 298</td>

</tr>

</tbody>

</table>

Most rated health information either 'very important' (40%) or 'quite important' (54%), rather than 'not important' (6%).

In response to the question "have you ever needed health information?", of 632 adolescents who answered, two thirds of them stated that they had needed health information at some time in the past.

<table bgcolor="#FCF9BA" border="1" cellpadding="7" width="180" align="center" style="font-family: Arial, Helvetica, sans-serif;"><caption style="font-family: Arial;" align="bottom">  
Table 2: Have you ever needed Health Information?</caption>

<tbody>

<tr>

<th valign="top" width="50%">Yes</th>

<td align="center" valign="top" width="50%">424</td>

</tr>

<tr>

<th valign="top" width="50%">No</th>

<td align="center" valign="top" width="50%">208</td>

</tr>

</tbody>

</table>

Over half the boys (59%) and three quarters of the girls (76%) said they had previously needed health information.

## Previous use of Health Information Sources

The study population had used a variety of sources to meet their information needs. The sources were grouped into four 'macro' areas - places, written sources, people and multi-media - and four groups were then divided into a total of 23 sub categories.

<table bgcolor="#FCF9BA" border="1" cellpadding="7" width="402" align="center" style="font-family: Arial, Helvetica, sans-serif;"><caption align="bottom">  
Table 3: Sources used by adolescents to meet Health Information needs</caption>

<tbody>

<tr>

<td valign="top" width="28%"> </td>

<td valign="top" width="21%">

<font size="2">**All boys**</font>

</td>

<td valign="top" width="24%">

<font size="2">**All girls**</font>

</td>

<td valign="top" width="27%">

<font size="2">**All respondents**</font>

</td>

</tr>

<tr>

<td valign="top" width="28%"><font size="2">**Places**</font></td>

<td valign="top" width="21%">

<font size="2">57%</font>

</td>

<td valign="top" width="24%">

<font size="2">72%</font>

</td>

<td valign="top" width="27%">

<font size="2">63%</font>

</td>

</tr>

<tr>

<td valign="top" width="28%"><font size="2">**Written sources**</font></td>

<td valign="top" width="21%">

<font size="2">40%</font>

</td>

<td valign="top" width="24%">

<font size="2">65%</font>

</td>

<td valign="top" width="27%">

<font size="2">51%</font>

</td>

</tr>

<tr>

<td valign="top" width="28%"><font size="2">**People**</font></td>

<td valign="top" width="21%">

<font size="2">55%</font>

</td>

<td valign="top" width="24%">

<font size="2">72%</font>

</td>

<td valign="top" width="27%">

<font size="2">62%</font>

</td>

</tr>

<tr>

<td valign="top" width="28%"><font size="2">**Multi-media**</font></td>

<td valign="top" width="21%">

<font size="2">32%</font>

</td>

<td valign="top" width="24%">

<font size="2">52%</font>

</td>

<td valign="top" width="27%">

<font size="2">41%</font>

</td>

</tr>

</tbody>

</table>

Although the percentages by gender show some differences, the sources are still ranked in closely the same order so that the top ranking macro information sources, for all respondents, were places and people. Within each of these groups, the top three selections were:

*   in the places grouping: GP's surgery, dentists and opticians;
*   in the written sources: leaflets, booklets and magazines;
*   in the people grouping: doctors, parents and friends; and,
*   in the multi-media grouping: television, video and films.

The absence from this listing of school nurses in the people grouping is unsurprising given the unequal provision of school nursing services in the two counties. Some schools had a full time nurse on the premises, others had a part time nurse, and some only had a contact name.

Internet resources are also noticeable by their absence from the top three multi-media information sources. They were used by merely 3% of respondents. This is surprising given the apparent IT and Internet awareness of adolescents and the encouraged and extended use of IT equipment in schools, homes and public libraries. The evidence of the study shows that the availability of the Internet facilities within schools was limited and not widely used.

The use of a telephone help line as a source of health information was also recorded as surprisingly small, only 3% of respondents preferred this method of obtaining information. The success of "Help for Health", "Health Information First" and the Trent Help Line do not appear to have penetrated the adolescent market. This is surprising given the availability and easy access to telephones within our society, and also the confidential and professional nature of the services provided. Yet the runaway success of the charity Childline, shows that a telephone service can be successful for children / adolescents. Therefore the study shows that the majority of adolescents prefer to talk to a professional face-to-face to obtain health information.

The previous use of health information above was contrasted intwo ways with anticipated future use. First, one question aimed to gauge which sources adolescents were prepared to use to find health information. Responses showed that young people were happy to visit places (where the most frequently selected category was 'GP's surgery'), use written sources (where the most frequently selected category was 'leaflets'), to talk to people (where the most frequently selected category was 'parent') or to use multi-media (where the most frequently selected category was 'television' ) to get health information. Approximately two-thirds of the total respondents selected each of these categories.

## Preferred Sources of Health Information

Respondents were asked for their preferred method of receiving health information across all macro groupings. Here responses were fairly evenly balanced between the places (27%), people (24%) and written sources (32%) macro groupings. The much-hyped multi-media sources were the preferred source of health information for merely 9% of respondents. As this rejection is by young people who may well be the most computer literature section of society, this should indicate a warning on the appropriateness of channelling substantial amounts of health information via this source. Within the 23 sub-groupings, the overall preference was to go to a GP's surgery ( selected by 16% of respondents ) and the second preference was to talk to a doctor ( selected by 11% ). The difference in these two responses is that at a GP’s surgery it may be possible to speak to another health professional other than the doctor i.e. the practice nurse or health visitor.

## Future use of Health Sources

The survey also examined the type of health information which respondents said they had previously needed and that which they anticipated needing in the future.

<table bgcolor="#FCF9BA" border="1" cellpadding="7" width="502" align="center" style="font-family: Arial, Helvetica, sans-serif;"><caption align="bottom">  
Table 4: Previous and future health information needs (ranked by frequency of response)</caption>

<tbody>

<tr>

<th valign="top" width="39%">Description</th>

<th valign="top" width="31%">Previous needs</th>

<th valign="top" width="30%">Anticipated needs</th>

</tr>

<tr>

<td valign="top" width="39%">Dental care</td>

<td align="center" valign="top" width="31%">1</td>

<td align="center" valign="top" width="30%">4</td>

</tr>

<tr>

<td valign="top" width="39%">Diagnosed medical condition</td>

<td align="center" valign="top" width="31%">2</td>

<td align="center" valign="top" width="30%">5</td>

</tr>

<tr>

<td valign="top" width="39%">Eye care</td>

<td align="center" valign="top" width="31%">3</td>

<td align="center" valign="top" width="30%">6</td>

</tr>

<tr>

<td valign="top" width="39%">Sports/Exercise</td>

<td align="center" valign="top" width="31%">4</td>

<td align="center" valign="top" width="30%">1</td>

</tr>

<tr>

<td valign="top" width="39%">Diet/Food</td>

<td align="center" valign="top" width="31%">5</td>

<td align="center" valign="top" width="30%">2</td>

</tr>

<tr>

<td valign="top" width="39%">Allergies</td>

<td align="center" valign="top" width="31%">6</td>

<td align="center" valign="top" width="30%">11</td>

</tr>

<tr>

<td valign="top" width="39%">Skin care</td>

<td align="center" valign="top" width="31%">7</td>

<td align="center" valign="top" width="30%">10</td>

</tr>

<tr>

<td valign="top" width="39%">Sexual issues</td>

<td align="center" valign="top" width="31%">8</td>

<td align="center" valign="top" width="30%">3</td>

</tr>

<tr>

<td valign="top" width="39%">Drugs</td>

<td align="center" valign="top" width="31%">9</td>

<td align="center" valign="top" width="30%">7</td>

</tr>

<tr>

<td valign="top" width="39%">Smoking</td>

<td align="center" valign="top" width="31%">10</td>

<td align="center" valign="top" width="30%">9</td>

</tr>

<tr>

<td valign="top" width="39%">Alcohol</td>

<td align="center" valign="top" width="31%">11</td>

<td align="center" valign="top" width="30%">8</td>

</tr>

<tr>

<td valign="top" width="39%">Safety in the home</td>

<td align="center" valign="top" width="31%">12</td>

<td align="center" valign="top" width="30%">12</td>

</tr>

</tbody>

</table>

These responses demonstrate a shift in concern, which is particularly noticeable in the top three ranking anticipated needs, away from health information for dental care, medical care, and eye care, towards health information relating to sports / exercise, diet and sexual issues, i.e. body image and sexuality. As these are prime sites for socially constructed 'norms' of sexual attractiveness, it is unsurprising that the third ranked future need is explicitly on sexual issues. Looking at the percentages of respondents involved, the percentage saying they anticipated needing information on sexual issues was 35%, almost treble the figure who said they had already sought information in this area (13%).

## Teaching staff responsible for health information education in schools

The study revealed an array of different titles and rank of the members of staff nominated as responsible for health information education in each school. This indicates the diverse and non comparable approach to the subject by both counties and the schools within them. It does not suggest a systematic approach to the teaching of the subject. This is reflected in policy and practice.

<table style="font-family: Arial, Helvetica, sans-serif;" bgcolor="#FCF9BA" border="1" cellpadding="1" width="574" align="center">

<tbody>

<tr>

<th valign="top" width="19%">Type of School</th>

<th valign="top" width="42%">Nottinghamshire</th>

<th valign="top" width="39%">Leicestershire</th>

</tr>

<tr>

<td valign="top" width="19%">Inner City</td>

<td valign="top" width="42%">Assistant Deputy Head</td>

<td valign="top" width="39%">Head of Modern Languages</td>

</tr>

<tr>

<td valign="top" width="19%">Suburb</td>

<td valign="top" width="42%">Head of Food and Textiles - Co-ordinator for Health Education</td>

<td valign="top" width="39%">Assistant Principal</td>

</tr>

<tr>

<td valign="top" width="19%">Mining Village</td>

<td valign="top" width="42%">Deputy Head</td>

<td valign="top" width="39%">Principal</td>

</tr>

<tr>

<td valign="top" width="19%">Market Town</td>

<td valign="top" width="42%">Acting Deputy Head</td>

<td valign="top" width="39%">Head of Student Welfare</td>

</tr>

<tr>

<td valign="top" width="19%">Rural Village</td>

<td valign="top" width="42%">Head</td>

<td valign="top" width="39%">Head of Personal & Social Education</td>

</tr>

<tr>

<td valign="top" width="19%">Independent</td>

<td valign="top" width="42%">Deputy Head</td>

<td valign="top" width="39%">Chaplain</td>

</tr>

</tbody>

</table>

Table 5: Teaching Staff Responsible for Health Information Education within Schools

The responsibility for teaching for health information education was not easily apparent and identified to one specialist or individual within the 12 schools. Only one school within the study identified a co-ordinator for Health Education. PSE was usually taught in year tutor groups by the form teacher. Obviously the differing ability of individuals to provide education for a range of subjects including sex, contraception, drug use/misuse and emotional adolescent changes was vast and not unlike the curate’s egg, good in parts. However the study also shows that the teaching of sex education is often provided after a proportion of students have become sexually active i.e. in year 11.

Training for individuals within schools also differed. The training of teachers in this particular field was reduced drastically in 1993 in that the Department of Education announced the cessation of 7 year part funding of health education units providing in-service training to teachers in sex and drugs education.

## National Curriculum

This lack of comparability of identification of teaching staff for health information education, together with the different years in which different aspects of the subject are taught throughout the various schools indicates different interpretations of the National Curriculum - key stages 3 and 4\.

The National Curriculum, Key Stage 3, section 2 - Humans as Organisms, ([Department for Education](#depa), 1995: 17, 18) details the following subjects: nutrition, circulation, movement, reproduction, breathing, respiration and health. Health had the following three specific points:

*   that the abuse of alcohol, solvents and other drugs affects health;
*   that bacteria and viruses can affect health;
*   that the body’s natural defences may be enhanced by immunisations and medicines.

Reproduction has the following specific point about:

*   the physical and emotional changes that take place during adolescence.

The issues surrounding these physical and emotional changes are, by their very nature, complex and difficult to explain in a group situation. They often require, therefore, one-to-one teaching and/or counselling, thus impinging on the all too little precious time available within the National Curriculum for Science. The depth of teaching required to fulfil the implications of the National Curriculum statement on the emotional changes that take place during adolescence really demands a separate timetable slot other than the meagre allowance within science. In the study, it was observed that this level of information provision was not successfully achieved within the PSE allocation. It would appear that emphasis within schools in science teaching remains predominantly in the factorial not the pastoral. Indeed, reminiscent of Mr. Gradgrind in Dicken’s "Hard Times":

> " Now, what I want is, Facts. Teach these boys and girls nothing but Facts. Facts alone are wanted in life. Plant nothing else, and root out everything else. You can only form the minds of reasoning animals on Facts: nothing else will ever be of any service to them. This is the principle on which I bring up my own children, and this is the principle on which I bring up these Children. Stick to Facts, sir!" ([Dickens](#dick),1974: 1)

Despite the differing standards in health information education observed in the study, and the declared anticipated needs of the adolescents for information surrounding sexual issues, "Section 241 of the Education Act 1993 gives parents the right to withdraw their children from any of all parts of a school’s programme of sex education, other than those elements which are required by the National Curriculum Science Order" ([Allen](#alle), 1987: 20)

Once again this emphasises the state of relative information poverty the adolescents can find themselves in if the parental right is exercised. However there is no evidence within the study to suggest that parents were exercising their right to withdraw children.

## Health Promotion Events

To increase general awareness of all health information issues, several schools had taken the initiative to hold special events and open sessions for health professionals to bring the health information message to the school and students. The types of professionals invited to these events span the whole spectrum of health care - school nurses, dieticians, beauty therapists, ophthalmologists and also the police specialist drug and rape teams. The most favoured times for these events was either immediately after school or lunch times. However on both occasions attendances by students and parents had been very poor. On a rudimentary note the timing of the events, either lunch time or immediately after school closes are not conducive to the majority of working parents’ hours or for parents who look after family at home. Also given that these events were aimed at adolescents both parents and the adolescents may assume that the parental accompanying role was unwelcome.

Conscious effort has to be made by the adolescent to go to the event either during lunch time effectively whilst still at school or after school outside official hours. The objectives of these events were to bring health information professionals and adolescents together in a non threatening environment, to remove all difficulties of finding the right type of information and greatly improve access.

However to attend this sort of event demands that the adolescent conforms in an information behavioural role, and is seen to conform to this model by others. Effectively an adolescent may have a passive interest in health information. However by attending a special event dedicated to the subject demands a paradigm shift from passive behaviour to active behaviour. This is obviously not a shift easily undertaken by the majority of adolescents in the study. It may not be considered "cool" to be seen seeking health information.

## Conclusions

Three major conclusions have arisen from this research:

> *   the shift of emphasis of the adolescent’s need for information about medical conditions to information relating to body image and sexuality;
> *   the emphasis of schools' health information education on the mechanics of sex education, in accordance with key stages 3 and 4 of the National Curriculum, thus leaving little time for discussion of the emotional changes surrounding these issues; and,
> *   the apparent lack of a systematic approach to health information education in either county or sector.

The evidence would suggest that health information needs of adolescents are not being fully met within the community setting.

As the second phase of this research, the conclusions have been explored with focus groups of adolescents from the schools which participated in the study reported here, to achieve a greater understanding of their health information needs.

### References

*   <a name="alle"></a>Allen, Isobel,(1987) _Education in Sex and Personal Relations,_ Research Report No 665\. London: Policy Studies Institute.
*   <a name="audi"></a>Audit Commission, _Children First, A Study of Hospital Services. (1993)_ London: Her Majesty’s Stationary Office.
*   <a name="chil"></a>_The Children's Act. (1989)_ London: Her Majesty’s Stationary Office.
*   <a name="depa"></a>Department for Education. (1995) _The National Curriculum,_ London: Her Majesty’s Stationary Office.
*   <a name="dick"></a>Dickens. C.(1974)_Hard Times,_ Harmondsworth, Middlesex, England: Penguin.
*   <a name="gann"></a>Gann. R.(1986) _The Health Information Handbook: Resources for Self Care,_ Aldershot, England: Gower.
*   <a name="heal"></a>_The Health of the Nation: A Strategy for Health in England (1992)_ London: Her Majesty’s Stationary Office.
*   <a name="kers"></a>Kerslake, E., and Rolinson, J. (1996) "In the Name of Innocence: Adolescents and Information about Sex." _The New Review of Children’s Literature and Librarianship,_ **2**, 58