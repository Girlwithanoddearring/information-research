#### Information Research, Vol. 3 No. 1, July 1997

# Using a NLSI to deliver the Effective Learning Programme: Problems and Practicalities

#### [Bob Hunter](mailto:bhunter@humber.ac.uk)  
[Learning Development Unit](http://www.humber.ac.uk/ldu/),  
University of Lincolnshire and Humberside  
Hull, UK

<div>Abstract</div>

> In 1996 the University of Lincolnshire and Humberside implemented its Effective Learning programme (ELP). This is delivered by a Networked Learning Support Intranet (NLSI). This paper discusses some of the issues raised in linking NLSI to centralised databases and the changing staff roles within Learning Support required to develop, deliver and support ELP and the NLSI.

## Introduction

The University of Lincolnshire and Humberside's (ULH) mission is:

> _"To provide our students world-wide with the best employment prospects and to equip them to become life-long learners"._

To achieve this ULH has developed a University Skills and Capability Curriculum (USCC) that is core for all students at all three levels of study. At level one of USCC there is the Effective Learning Programme (ELP). The main aim of ELP is to develop independent life long learners. 1996 was the first full year of delivery of ELP. This has been fully evaluated and is being modified in the light of the evaluation report. However ELP started off as Learning to Learn (L2L). L2L was two years in development before being piloted. Since those early days of development the technology and the available software have changed significantly. The development phases reflect this. Outlined below are the brief details of the outcomes of the L2L pilots and the changes in systems, resources and staff roles that are required to develop and implement ELP.

## Learning to Learn

In late1993 ULH made a decision to develop a core curriculum for students' learning and study skills at level one. The brief was to use the University's computer networks and libraries so that students, and their tutors could access this core curriculum on any University campus. Development teams were established. These included staff from the recently converged department of Learning Support (bringing together the library and academic computing staff), and a number of academic staff from across the University. By 1995 the University was ready to pilot Learning to Learn (L2L), a 12 CATS point unit, with around 740 students. At this stage L2L consisted of a number of profiling tools, written in Toolbook, the University of Lincolnshire and Humberside's (ULH) own software for assessing IT skills and a range of key skills delivered by print based open learning materials (1).

Some of the main findings of the L2L pilot included the following:

*   different interfaces and access points on the desktop for the components of L2L caused confusion for some students
*   some students thought that the name Learning to Learn was patronising (they already knew how to learn)
*   some students wanted automated assessment on demand and the addition of self diagnostic tests for IT skills
*   the tutors wanted detailed tutor guides to support the delivery of L2L
*   some tutors had difficulty in getting additional copies of the print based learning materials.

## The Effective Learning Programme

The above findings led to a number of changes. The first was to change the name from L2L to the Effective Learning Programme (ELP). ELP was also developed further to fit in with the University's modular scheme and is now a 24 CATS point unit with eight modules. (See the Appendix for more details on the structure of ELP.)

To fit this structure, a new range of learning materials was required, with detailed tutor guides. A common user interface was also needed so that students could access ELP on any of ULH's four campus. This interface also needed to provide networked learner support.

### Networked Learner Support

A computer based networked learner support system would need to provide the following, all within one interface, readily accessible on any of ULH's campuses:

*   profiling tools
*   self-diagnostic tests
*   automated assessment
*   feedback to the student
*   feedback for the tutor
*   information on ELP
*   electronic materials delivery

The development of L2L started in 1993; by 1996 the outcomes from the L2L pilots had lead to the development of ELP. During these three years the World Wide Web had come to the fore. Software is now available to dynamically link databases to web browsers. This and other software developments now enable web browsers to act more like intelligent interfaces. These developments have meant that both industry and education have begun to change the way they use technology for learning and communication. BP have produced a report, "Technology for Learning"(2) giving us a vision of what the future learning environment could be like. The future role of technology in providing the learner with easy access to a wide range of information is highlighted by JISC: the Joint Information Systems Committee (3). One of the key issues JISC raises is the need to establish common user interfaces and standards for information access for a wide range of learners. The developments in software for the web are enabling these visions to become a reality. It was in July 1996 that ULH decided to use this web software to pilot a Networked Learning Support Intranet (NLSI) to support the delivery of ELP.

### Development of the Networked Learning Support Intranet

To develop the NLSI a number of organizational and strategic issues had to be addressed. These are briefly discussed below.

_**Links to ULH's Student Management System**_  
Developing a link to the Student Management System (SMS) was key. SMS holds the information on all students registered on ULH's programmes of study. By gaining access to SMS the NLSI would enable tutors to :

*   create and manage student groups
*   search for students
*   monitor how students were using the NLSI
*   update each student's assessment record.

However, there were a number of problems getting access to SMS. SMS is managed by the Registrar and access is restricted for security reasons. Yet for the NLSI to be effective we needed an up-to date record of SMS, otherwise the tutor could not manage the student groups in ELP. After long negotiations with the Registrar we were allowed read only access.

_**Developing web based profiling and assessment tools**_  
ULH is also in the process of re-developing the profiling tools, self diagnostic tests and the automated assessment (used in L2L) so that they can be delivered and supported via a web browser. One of the profiling tools, Approaches to Study, has been converted and was used by students in the second semester.

_**Software standards**_  
This raises another issue: the standardization of software. In 1993 ULH decided that the software package that it would use for most of its computer based learning would be written in Toolbook. At the time of writing, the software that enables Toolbook to run in a web browser does not track what students are doing. Tracking students is a key requirement for the feedback in ELP. So we had to make the decision to do the re-development so that the profiling tools could run and be monitored in a web browser. It is interesting to note that the work done on study skills for a Macintosh platform, by Entwistle at the University of Edinbrough (4) is also being developed to work within a web browser.

_**Monitoring application usage within the NLSI**_  
All the activities that are required to be carried out by students, within the NLSI, are monitored and the resulting information sent to central databases. Because students are continually working within ELP these databases are always changing. This means that the NLSI is dynamic - the web pages are actually made up from the information within these databases. When a user logs on the system identifies the type of user and then searches the appropriate databases. This information is then sent to the web browser and pages are created. Features like graphics, headings, information on layout are also stored in a database and inserted into the web pages when required.

_**Achieving database integration - software choices**_  
Another key issue is what type of software do we use to integrate these databases into the NLSI? At the time of the development phase of NLSI (July 1996) only one piece of software seemed capable of doing all the things outlined in the above paragraph: "Cold Fusion". However, since then it appears that Microsoft has taken a serious look at linking web browsers dynamically to databases. So at the moment ULH is monitoring what Microsoft is doing.

Universities like ULH use a range of databases to manage their institutions. Access to these databases is going to be crucial if we are to provide effective NLSI systems that will facilitate the learning process, both for the tutor and the student.

### Electronic materials delivery

Because of the amount of print based learning material associated with ELP and the difficulty some tutors had in getting materials to students, in the L2L pilots, it was decided to make materials available electronically via the NLSI. This turned out to be a very simple thing to do. Using a piece of software called "Acrobat" we were able to make the print based learning materials available electronically. This enables the tutor to print off what they require. It is worth noting that this system is designed for small print runs, to print one or two copies of student guides or to reproduce handouts. What tutors have found most useful is the ability to scan the available materials on screen, and decide if it is suitable for their students.

One of the key issues in making learning materials available electronically is the development of just in time printing (JIT). At the moment ULH does not have large print run facilities on each campus so JIT is not really possible on a large scale. So we still need to order large print runs from a central unit or from outside ULH. We are, however, looking at JIT for each of the campuses.

## Staff roles

ELP has been developed so that it is delivered and supported by:

*   weekly tutorials
*   print-based open learning materials
*   self diagnostic tests (paper based and computer based)
*   profiling tools
*   IT assessments on request
*   the NLSI
*   Learning Support staff
*   other students.

This delivery and support structure provides what MacFarlane (5) would call an ISLE: an intensely supported learning environment. This learning environment is structured to develop independence in learning. The ISLE has had a significant effect on the roles of students, tutors, and Learning Support staff. The changes in these staff roles have initiated a heated debate, particularly in the role of Learning Support. These new staff roles cover a number of areas. To provide the ISLE for ELP a department like Learning Support should be able to provide the following:

*   the development of technology for learning - the NLSI and its component parts
*   materials development and production for ELP
*   delivering ELP
*   supporting ELP in Learning Support centres.

Solutions will have to be found to the above if Learning Support are to be actively involved in the development of systems like the NLSI and the delivery and support of learning and study skills across the University. Both Fielden (6) and Ford (7) outline similar changes in the student's learning environment and the need for new staff roles within departments like Learning Support to effect these changes. The changes in Learning Support staff roles are discussed below.

### The development of technology for learning - the NLSI and its component parts

When we look at centralised administrative university computer departments these usually cover two main areas:

*   Administrative Computing, this covers areas like financial control and student record keeping.
*   Computing and Network Services (CNS), this usually provides and supports the academic networks.

These departments have little day to day contact with students. To develop ELP, and the NLSI to support it, we needed software developers with some knowledge of how students interact in the learning environment of computing centres. The IT staff in Learning Support have day to day contact with students in computing centres. They understand the student population, from those that are techno-phobic to those that can hack into the University's networks! At ULH these IT staff also have the appropriate set of software development skills. They were ideally suited to develop the software for ELP. With this basic understanding of how students interact with their learning environment the IT staff were able to develop user-friendly interfaces for the profiling tools and a simple navigation system for the NLSI. With skills like these Learning Support are in an ideal position to take on a central development role - working with the academic community and central computer departments to develop systems and software that will help both tutors and students to both facilitate and manage the learning processes.

Learning Support are also in an ideal position to provide the administration functions required for NLSI and to maintain and update its component parts - like the profiling tools and the automated assessment facility. This updating can be done using the valuable feedback that will be gained form working on a daily basis with both staff and students working within ELP.

If Learning Support is to take a significant role in the development of systems like the NLSI then new roles within Learning Support need to be defined and closer working links with other central computer and information departments need to be made.

### Materials development and production

_**Development of IT skills for ELP**_  
If we are to provide an ISLE where students have a choice as to when, where and how they study then the way the curriculum is delivered needs to change. The lecture is no longer suitable, or effective as the major delivery mode (5). An example of the effective use of open learning materials was when ULH decided to convert its IT delivery from lectures to open access. Early in 1992 ULH recognised it had a problem delivering and supporting basic IT skills to large numbers of students. Students were arriving at ULH with a wide range of abilities, from those who treated the mouse like a remote control to those who had been word-processing and working in a Windows environment for a number of years. It was difficult to work with groups of students with this range of skills. ULH decided to develop open learning materials so that students could work in their own time and at their own pace. ULH now has wide range of open learning materials for developing IT skills. These are supported by self-diagnostic tests that students can access via the network, with automated assessment on request. Some of these IT materials have been integrated into ELP and are available for selection by the ELP tutors.

The IT skills materials were developed by a team in Learning Support in consultation with academic staff. The academic staff commented on the structure and the content of the IT manuals and on the content of the automated assessment. The team in Learning Support consists of an editor, authors and a desk top publisher. These duties are carried out as part of their normal daily routine within Learning Support. The funding for these posts was transferred from the academic departments, when there was no further requirement for academic input to basic IT skills development.

_**Other skills modules for ELP**_  
Other open learning materials were also developed or brought in for the other modules within ELP. Each was structured so that students would be able to work through them within a three week period and each has a tutor guide to help ELP tutors integrate the skills in ELP into their subject areas. These modules and the tutor guides were developed by both academic and Learning Support staff.

_**Materials production and delivery**_  
ELP is resource based learning (RBL). The main resources for ELP are print and computer based, with weekly tutorial support. With large numbers of students doing ELP there was the problem of getting the print based learning materials to students at the start of the first semester in 1996\. Any RBL that involves large numbers of students requires a very competent person to manage the production and delivery of the print-based learning material, together with reliable systems and procedures. So ULH seconded a member of staff from Learning Support to be the "Print Production Manager" for ELP. One of her two main functions was to act as managing editor for the materials. This involved managing and editing the:

*   IT manuals, "Getting Started with Word 6" and "Getting Started with Windows 95"
*   tutor guides for all the skills materials.

The above, relatively short, list would have been significantly longer if ULH had not looked around to see what other skills materials were available. After evaluating a wide range of skills materials that had been produced by the private sector and other universities ULH decided to use the materials that DeMontfort had produced. All ULH had to do then was to produce the tutor guides that would integrate the DeMontfort material into ELP. This has cut down the development costs significantly and helped to address the "not invented here" syndrome.

The other main role of the Print Production Manager was that of print buyer. She had to contact all the schools to determine how many students would be studying ELP and what skills materials they would be using. To pay for all these materials the schools had their budgets top-sliced and a central budget was set up so that all the materials could be bulk purchased. The following is an outline of what was ordered to support ELP in 1996:

*   4,000 IT guides, including Getting Started with: Word, Windows (3.1 and 95), Excel and SPSS
*   8,000 student guides from DeMontfort covering twelve different skills.
*   50 ELP tutor packs.

The total budget for printing of the ELP materials was just over £23,000\.

The Print Production Manager's final role was to ensure that all the ELP materials were delivered to a central point at ULH and she distributed to the various school offices.

Some universities do not have a print production facility for RBL. It is left to individuals to try and manage the production, printing and distribution of print based learning materials. Gibbs (8) gives us a mythical account of a lone lecturer's attempt to convert her degree course to RBL. It is well worth a read if you are considering converting any taught course to RBL.

If any university is to develop RBL then it will need to have in place a learning materials print production facility. A department like Learning Support is the ideal place for this. It has a major presence on each campus and therefore should also be able to provide just in time print facilities and print shops.

### Delivering the Effective Learning Programme

If students are to develop learning and study skills the best approach is to integrate these skills into the student's subject area. This will ensure that students do not learn skills in isolation, but have the opportunity to practice and develop them within the context of the core subject of their studies (9). With this integrated approach, the skills that are required within the subject should be made explicit to the students.

So what involvement, if any, should Learning Support have in the delivery of ELP? The delivery and support of information retrieval skills is usually seen as the responsibility of Learning Support. In some institutions so is the delivery of basic IT skills. But what about presentation skills, group skills etc? If the integration of any skills programme into the student's programme of study is the key to students developing these skills then how can Learning Support play an active role in integrating these skills into the student's subject area?

The current practice, at ULH, is for ELP to be delivered by the schools. ULH adopted this policy for two reasons. The first is the notion of ownership. ELP represents 20% of the curriculum at level one. All schools that are part of ULH's modular programme get the same funding to deliver ELP units as they do to deliver their subject units. With this approach schools get the funding to deliver ELP and therefore they should take ownership of ELP and integrate it into their subject area.

The second reason for giving ELP to schools is the Gibbs (9) argument. Integrating skills into the subject area should be done by tutors that have the subject knowledge. Therefore the ideal person to deliver ELP is the tutor in the student's subject area.

However, there is a viable alternative : the Learning Advisor. At ULH Learning Advisors are part funded by the subject areas and are situated in Learning Support. This is based on the model that Fielden (6) outlines in his report on "Supporting Expansion". At the time of writing, Learning Support at ULH is in the middle of restructuring its department so that it will have Learning Advisors in each subject area. Learning Support does, however, already have one Learning Advisor working in languages. This Learning Advisor attends the subject's course committees, is an active member of the subject's academic community and works closely with language students at all levels of study and she runs the ELP tutorials. This is a good example of how Learning Support can work closely with the academic community in delivering an integrated approach to the development of students' learning and study skills.

Creating the Learning Advisor post involved the Director of Learning Support spending a significant amount of his time talking to the deans to ensure that funds were made available from each subject area.

### Supporting ELP

_**Help Desks**_  
The main aim of ELP is to develop independent life-long learners. So there should be little demand made on the help-desks by ELP students. The enquiries the help-desk staff get from ELP students cover two main areas, one is the use of the IT materials. If a ELP student has a problem with IT then he or she is guided to the most appropriate IT materials. The other enquiries cover the use of the computer software for ELP. All help-desk staff are NLSI administrators and have had experience of all the ELP software on the staff development programmes that were run for all Learning Support staff. So if a student, or a member of staff, has a problem with any of the ELP software the people on the help-desks will be able to assist them.

At ULH we are currently attempting to re-define the functions of the help-desk. To do this we are looking at the student as a learner. To encourage students to become independent lifelong learners the way most help-desk staff interact with students needs to change. Staff on help desks like helping students, they sometimes do the student's work for them! Many students expect to be helped in this way so students' expectations will also need to change. To help re-define the functions of the help-desks Learning Support are running a series of workshops. It is hoped that by the end of these Learning Support will have defined a different role, and perhaps even a different name, for the help-desk!

## Conclusion

The development of the ELP and the NLSI has shown us that technology can not only deliver content: it can also help both tutors and students manage the learning process. If a system like the NLSI is to be effective then universities like ULH must give secured access to databases held within central departments like Registry.

We must, however, be aware that technology can not do everything that a student requires. The role of tutor or facilitator is vital if students are to develop and integrate key skills into their programme of study and into their lives beyond university. Developments like ELP also mean significant changes in staff roles, particularly in departments like Learning Support. If a Learning Support department wants to take a proactive role in changing the way a university delivers and supports the curriculum then it needs to consider changing both the structure of Learning Support and staff roles. These changes will most likely have to be made within the current budgetary constraints of most Learning Support departments.

## References

1.  Hunter, B and Cook, M., _Learning to Learn_, Paper presented at the 16th Annual Conference on Teaching and Learning in Higher Education of the Society for Teaching and Learning in Higher Education at the University of Ottawa, Canada, June 12-15 1996\.
2.  Lewis, R and Merton, B.,_Technology for Learning - Where are we going?,_ BP International Limited and the University of Lincolnshire and Humberside 1996\.
3.  JISC _Exploiting Information Systems in Higher Education: An Issues Paper,_ 1995\.
4.  Entwistle, N., _Personal Advice on Study Skills,_ TLTP project at the University of Edinbrough, on their web server at - http://129.215.172.45/, 1997\.
5.  MacFarlane, A., _Teaching and Learning in an Expanding Higher Education System,_ Committee of Scottish University Principals 1992\.
6.  Fielden, J. Consultancy, _Supporting Expansion,_ A report on Human Resource Management in Academic Libraries, for the Joint Funding Councils' Libraries Review Group 1993\.
7.  Ford, P., _Managing Change in Higher Education_, SRHE and the Open University Press 1996\.
8.  Gibbs, G., _Institutional strategies for implementing resource based learning_ in, Resource - Based Learning, edited by Sally Brown and Brenda Smith. Kogan Page 1996\.
9.  Gibbs, G., _Learning By Doing_ FEU 1988.

# Appendix - The Effective Learning Programme - its structure

ELP runs over the two semesters at level one. It helps students to develop learning and study skills. It is designed to build on the wide range of skills that students bring with them to university.

## Aims of the Effective Learning Programme

Some of the aims include:

*   helping students to 'acclimatise' quickly to the learning environment of the University
*   enabling students to become more effective learners. This means that students should be able to get the most out of their studies and will be more likely to succeed at university
*   equipping students with a range of skills they will need to complete their studies, and which should make students more employable.

## Structure

The ELP is a double-semester unit which is taken during the first year of study. It is made up of eight equal modules. Four of these modules (A, B, C and D) will be taken by all students taking the ELP, and are termed 'core modules'. The other four modules concentrate on skills which are particularly relevant to the subjects each student is studying. These are termed 'selected modules' (see Figure 1: note that you start from the bottom of the figure).

![Figure 1](../huntfig1.gif)

 
**Figure 1**

## Content of the core modules

The content of the core modules is as follows:

*   Module A - _**Introduction to the Effective Learning Programme**_: including the introduction to IT (using IT open learning guides, _Getting Started with Windows_ and _Getting Started with Word_) and how to use open learning materials
*   Module B - _**Learning Styles**_: including learning style profiling, action planning and reflection
*   Module C - _**Information Retrieval**_**:** including an information retrieval exercise, input on plagiarism and referencing, and the completion of the two ITguides from module A
*   Module D - _**Study Skills**_: including an independence in learning inventory, an Approaches to Study survey and action planning.

## The selected modules

The selected modules for most ELP students are:

*   _Time Management_
*   _Group Skills_
*   _Presentation Skills_
*   _Essay Writing OR Report Writing._

## Dates for completion of each of the modules

Core module A is taken first and core module D should be taken at the start of the second semester, all the others can be taken in any order. This order will be determined in the ELP tutor group. Students have an important role to play in the selection of this order. To do this they need to know when their subject areas will require the skills that they need developing in the ELP. For example, if a student is doing Essay Writing as an ELP module they will need to know when the first essay is to be handed in. Students will need to inform their ELP tutor of this date.

## Assessment

Assessment is associated with each of the eight modules over the two semesters. Some modules have several parts to their assessment, each of which must be passed to gain an overall pass for that module. These assessments will be spaced throughout the two semesters and are an integral part of the ELP tutorial groups. So students must attend regularly to avoid missing assessments. All eight module assessments must be passed to gain an overall pass for the unit. A variety of different types of assessment are used on the ELP. These range from computerised assessments for the IT modules, to the writing of essays/reports and the successful completion of group tasks. Once ELP tutor groups have resolved their programme, tutors list the assessments and the order that they will be taken in.

## Students' ELP portfolio

Tutors will keep a record of students' performance, using the ELP Intranet, in all the module assessments. It is also important that students keep their own records of work done for the ELP. This includes task and assessment sheets, handouts and all study guides. This material should be kept in a ring binder, forming an ELP portfolio. The portfolio has two purposes:

*   to act as a record of the work the student has done, results of profiling etc. for future reference
*   to provide evidence of attainment/effort which may be required for subject exam boards in the case of a marginal result for the overall assessment of the ELP unit.

More details of ELP can be found on the Learning Development Unit web site at [http//:www.humber.ac.uk/ldu/](http://www.humber.ac.uk/ldu/)

##### Paper delivered at the 2nd International Symposium on Networked Learner Support 23rd-24th June 1997, Sheffield, England  
**New services, roles and partnerships for the on-line learning environment**