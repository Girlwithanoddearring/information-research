#### Information Research, Vol. 7 No. 4, July 2002,

# An improved method of studying user-system interaction by combining transaction log analysis and protocol analysis

#### [Jillian R. Griffiths](mailto:j.r.griffiths@mmu.ac.uk), R.J. Hartley and Jonathan P. Willson  
Department of Information and Communications  
Manchester Metropolitan University  
Manchester  
United Kingdom

#### **Abstract**

> The paper reports a novel approach to studying user-system interaction that captures a complete record of the searcher's actions, the system responses and synchronised talk-aloud comments from the searcher. The data is recorded unobtrusively and is available for later analysis. The approach is set in context by a discussion of transaction logging and protocol analysis and examples of the search logging in operation are presented.

## Introduction

The investigation of how people use electronic information resources is important as a means of understanding how to make systems more usable and as a means of understanding information-seeking behaviour. Important developments have been the use of transaction logging data and of protocol analysis. However a trend in understanding information-seeking behaviour and in information system evaluation has been the move from controlled experiments to the study of information seeking in a natural setting. The collection of transaction logging data of end users searching full text electronic information sources in a natural setting poses numerous challenges and raises methodological concerns. This paper explores the challenges and describes a novel approach that was developed to address the limitations of traditional quantitative methodologies as applied to CD-ROM databases. The transaction logs for the research project were required to provide more qualitative data, capturing the search from start to finish as experienced and seen by end users.

The paper discusses transaction log analysis and protocol analysis as methods of data collection before outlining the novel methodology in detail, illustrating its operation with some examples of the data collected and concluding with a brief discussion on its use in a number of projects. The intention of the paper is to concentrate on the methodology itself rather than report results of specific research. Appendices are included which provide system requirements/technical detail and contact information of suppliers.

## Transaction Log Analysis

A useful definition of Transaction Log Analysis has been provided by [Peters _et al._](#pet93) (1993) who characterise it as:

> ...the study of electronically recorded interactions between on-line information retrieval systems and the persons who search for the information found in those systems.

Furthermore noting that its use as a research methodology is relatively new, they have divided its development into three phases:

*   mid 1960s to mid 1970s - emphasis placed on evaluating system performance, rather than on user behaviour and performance, Meister and Sullivan's (1967) evaluation of user reactions being one of the first uses of transaction log analysis;
*   late 1970s to mid 1980s - first applications of transaction log analysis to the study of on-line catalogue systems, equal interest in how the system was used and in the searching behaviour of end users; and
*   late 1980s to date - diversification of transaction log application, usually focusing on use of operational information retrieval systems by end users.

Peters _et al._ have identified a variety of uses of transaction log data namely:

*   improving an information retrieval system;
*   improving human utilisation of the system;
*   improving human (and system) understanding of how the system is used by information seekers;
*   identification of how a system is being used by end users; and
*   study of prototype systems or potential system improvements.

Transaction-logging systems are usually a feature of automated library systems, which can be classified into two broad categories:

*   systems which count transactions as they occur; and
*   systems which store the text of the transactions ([Flaherty, 1993](#fla93)).

Whilst systems of the first type may provide useful management information to librarians, it is only systems of the latter type which provide useful data for researchers because they capture and store users' entries and provide for retrieval of the stored data. Additionally some systems will also record information about the response returned by the system. Flaherty identifies typical transaction log data elements as:

*   patron's entry;
*   date and time;
*   terminal identification;
*   search file;
*   number of hits; and
*   system response (not all systems).

[Blecic _et al._](#ble98) (1998) define transaction log analysis as "the detailed and systematic examination of each search command or query by a user and the following database result or output by the OPAC" (p.40). According to this definition transaction log systems which do not record the system response to the user input are not true transaction logging systems.

Transaction logging systems have limitations. [Hancock-Beaulieu _et al._](#han90) (1990) noted that whilst transaction logs could provide useful diagnostic evidence of procedural and conceptual problems, they also had specific limitations. These limitations include a lack of "information or the right type of data to inform adequately on how or why users searched in the way they did" (p.11).

Other restrictions include:

*   not all systems have logging facilities;
*   problems in identifying individual search sessions;
*   system response not always recorded; and
*   volume of data generated causes difficulties for analysis.

Having identified the limitations of existing systems, [Beaulieu _et al._](#bea90) (1990) developed a PC-based transaction logging software package, OLIVE. This system logged the user-host dialogue, condensed and summarised the data, played back the logon screen, acted as an agent in a three-way exchange by intercepting the user-host dialogue, and was able to include pre- and post-search questionnaires. This could be adapted for use across different OPACs. This system concentrated more on "screen logging rather than a transaction logging focus" (p.73).

Kurth (1993) noted that,

> ...students of transaction log data have begun to ask as many questions about what the transaction logs cannot reveal as they have asked about what transaction logs can reveal. ([Kurth, 1993: 98](#kur93)).

According to Kurth these limitations fall into four categories:

*   system factors;
*   user/search process factors;
*   data analysis factors; and
*   ethical and legal factors .

System factors, which impede the collection of transaction log data, are divided by Kurth into:

*   mechanical;
*   temporal;
*   spatial; and
*   intersystem factors.

Mechanical limitations include such elements as complex programming requirements and immense data size. Other system factors are determined by the fact that "on-line systems are unique entities which exist in time and space" (p.99). Thus, changes to a bibliographic database, modifications to commands, indexing and displays will impact on the researcher's ability to replicate searches, and the same system on the same day will behave differently over a network. Intersystem factors are concerned with the development of transaction logging subsystems which vary widely, making comparisons across different on-line systems difficult.

A major "user/search process" limitation identified by [Kinsella and Bryant](#kin87) (1987) is the inability to isolate and characterise individual users of on-line systems in order to describe the pattern of their use. Users' perceptions of their searches are not recorded, transaction logs cannot measure the information needs that users' are unable to express in their search statements (input), and they cannot reflect users' satisfaction with search results (output). As Kurth states, "[the fact] that transaction logs are unable to address such cognitive aspects of on-line searching behaviour is a true limit of the methodology" ([Kurth, 1993: 100](#kur93)). Supplementary research, such as questionnaires, protocol analysis and interviews, must be undertaken in order to build a fuller picture of searching behaviour, success and satisfaction. Kurth identifies data analysis issues as being threefold:

*   execution;
*   conception; and
*   communication.

Execution creates collection, storage and analysis problems related to the large size and complexity of the data set. Conceiving a method of analysis that is appropriate to both the data analysed and the research question asked is also crucial to the success of the study. The limitations discussed above combine to create a large number of variables to control when developing a methodology for analysing transaction log data. Communication problems arise when researchers:

*   fail to sufficiently define terms and measurement tools upon which their transaction log analysis will depend; and
*   fail to provide descriptions of their methodologies in sufficient detail to allow other researchers to test and verify their results.

Whilst transaction log analysis has considerable value as a data collection method, it has its limitations and it is best used in conjunction with a method which captures data regarding users' real information needs, comments and reactions whilst using a system and satisfaction with the system.

## Protocol analysis

Protocol analysis has been widely used across many research areas and has stimulated considerable debate about its merits and limitations as a method of eliciting information from the participant. There has been significant development and use of verbal protocol analysis in educational psychology (for example [Ericsson & Simon, 1980](#eri80); [Long & Bourg, 1996](#lon96)) and as such it is relevant to consider lessons from this area. However, think-aloud protocols have been used increasingly in information retrieval research ([Flaherty, 1993](#fla93); [Keen & Wheatley, 1978](#kee78); [Markey, 1984](#mar84); [Hancock, 1987](#han87); [Manglano _et al._ , 1998](#man98)).

Hancock-Beaulieu _et al._ have argued that [Ericsson and Simon's](#eri80) (1980) paper on verbal reports as data "has led to the use of protocols or spoken thoughts in task analysis and decision making processes in a number of ... areas" ([Hancock-Beaulieu _et al._1990: 6](#han90)). Ericsson and Simon's central proposal is that verbal reports are data and, as such, require explication of the mechanisms by which the reports are generated, and the ways in which they are sensitive to experimental factors. As a method for understanding the cognitive processes of subjects, doubts had been raised about the validity of verbal reports ([Lashley, 1923](#las23); [Nisbett & Wilson, 1977](#nis77)). Ericsson and Simon sought to evolve a methodology for verbal reporting which would address the limitations that had discredited it by proposing a move away from informal analysis of verbalised information toward objective procedures for collection and analysis. The model they propose for the verbalisation processes of subjects is based upon the theoretical framework of human information processing theory ([Newell and Simon, 1972](#new72); [Anderson and Bower, 1973](#and73); [Simon, 1979](#sim79)). Ericsson and Simon conclude that,

> ...verbal reports, elicited with care and interpreted with full understanding of the circumstances under which they were obtained, are a valuable and thoroughly reliable source of information about cognitive processes. ([Ericsson & Simon, 1980: 247](#eri80)).

Long and Bourg report on three areas of research where verbal protocol analysis provides unique information:

*   the study of inferential processing during text comprehension;
*   the study of individual differences in comprehension performance; and
*   the study of conversational discourse and storytelling.

Long and Bourg's major premise is that.

> readers do not provide a veridical report of their underlying mental processes when they 'think aloud' ... rather, they construct a text representation and then use it to 'tell a story' about their understanding" ([Long & Bourg, 1996: 329](#lon96)).

Further, Long and Bourg in discussing [Magliano and Graesser's](#mag91) work (1991) state that "verbal protocols are an important component of the three-pronged method [for studying inferential processing during comprehension]" (p.330). They put forward that verbal protocols provide a rich and detailed database that can be used to generate hypotheses about what knowledge-based inferences occur during reading and that verbal protocols can be used to identify the knowledge structures that supply inferences. In the opinion of Long and Bourg:

> verbal protocol analysis is ideal for identifying those inferences that involve access to world knowledge and those that involve access to prior text information. We are somewhat less convinced that verbal protocols provide valuable information about the memory operations that underlie these inferences" ()

The second area of study often utilising protocol analysis is the study of individual differences. Here, verbal protocols are used to investigate individual differences in readers' text representations and comprehension strategies. Long and Bourg examined several studies in this area and concluded that verbal protocol analysis led to interesting hypotheses about differences in the text representations constructed by individuals, who vary in reading skill and memory capacity. Further they noted that verbal protocols also lead to hypotheses about individual differences in learning strategies and production skills. Research into the affect of learning styles ([Ford _et al._, 1993a](#for93a) and [1993b](#for93b)) and cognitive styles ([Griffiths, 1996](#gri96)) on successful information retrieval may be informed by this second approach. The third research area discussed by Long and Bourg is that of conversational discourse. Long and Bourg posit that "readers use their text representation to tell a story about their understanding of a story" ([Long & Bourg, 1996: 335](#lon96)) and, as such, verbal protocols may provide useful information about some of the principles involved in conversational discourse.

As long ago as 1978, protocol analysis was used to describe the searching behaviour of a group of experimental searchers. Keen and Wheatley conducted an experiment in which five different kinds of printed subject indexes were searched and, from transcripts of the verbalised tape-recordings, evidence of the way the entries were processed by the user was found. The test focused on one user and was designed to contrast this with two other methods for capturing a comprehensive record of the search. The two other methods under investigation were the use of detailed record sheets and marking the index copies. Keen and Wheatley concluded that "as an analogy to recording searching, verbalised recorded indexing be used as a tool for exploring the indexing process and studying indexing aboutness" ([Keen and Wheatley, 1978: 430](#kee78)). Keen has used the think-aloud methodology in subsequent studies ([Keen, 1995](#kee95)).

[Markey](#mar84) (1984) utilised protocol analysis to study manual searching of a library catalogue. With minimal prompting, users were encouraged to talk aloud as they searched. Their statements were recorded onto an audio tape and transcribed to produce a flow chart of the different steps of the complete search. From these protocols, patterns of searching behaviour emerged and models of different types of searches were identified. Transcription and coding was, however, a time consuming task.

[Hancock-Beaulieu _et al._](#han90) (1990) identified the main limitations of protocol analysis as being two-fold:

*   fears that a direct approach would interfere and possibly distort the search process; and
*   transcription and subsequent coding is time consuming and cumbersome.

To minimise these problems [Hancock](#han87) (1987) devised a combined observation and talk aloud technique to study searchers using a microfiche catalogue. Subjects were encouraged to talk aloud as they searched to give an indication of what they were looking for and to confirm whether or not they succeeded. The direct observation of searchers' actions provided Hancock with the framework of the activity and in this way the searchers' actions confirmed what they said they were doing. The verbal protocol data provided details which could not be easily observed or which were not observable.

## A new methodological approach

The foregoing analysis indicates that whilst there has been considerable methodological innovations in the study of user system interaction, there is scope for further improvement. Ideally we believe that transaction log analysis ought to have the following features:

*   show real time;
*   show elapsed time;
*   show mouse movement of end user;
*   show input of end user;
*   show response of the system, e.g. error messages, retrieved items;
*   be unobtrusive;
*   give control to the researcher;
*   synchronise transaction log with verbal protocols; and
*   be used across different interfaces.

We believe that the method outlined here meets these criteria and therefore constitutes a considerable step forward. The approach can utilise either of two types of technology. The first consists of a PC to video converter, called Grand Art, produced by Imagine Graphics. This equipment consists of hardware and software that was connected between a PC and monitor and a standard VHS video recorder. Once the software is installed, the screen output is unobtrusively re-routed to the video and recorded, and simultaneously displayed on the monitor to the end user (see [Appendix 1](#app1) for system requirements). The second is Lotus ScreenCam software that, once loaded, records all screen activity onto disc in the form of a film. Lotus ScreenCam is the authors preferred option as it records directly to PC (see [Appendix 2](#app2) for system requirements).

With both of these systems the end result is a real time film of the searching session. Each of these technologies is able to record the search session from start to finish in real time, showing pauses, movements made by mouse, input by participants, system responses (for example, error messages, retrieved results lists, instructions and prompts etc.) as well as a user's comments and as they work through their search session. Importantly, use of different electronic resources can be logged without any changes to the system. These films can be played back for analysis at a later date through a standard video player and television set or viewed directly on a PC. This method of recording the end users' interactions with electronic resources can also be described as "screen logging" ([Flaherty, 1993:.73](#fla73)) and provides a more qualitative observational approach to transaction logging.

The benefits of using Grand Art/ScreenCam as a method of transaction logging are threefold:

1\. Screen logging considerations

*   captures everything that the user sees and interacts with on the screen
*   shows system response

2\. Researcher considerations

*   unobtrusive
*   controlled by researcher
*   analysis off-line (at researcher's convenience)
*   video/stored files have archival permanence

3\. Practical considerations

*   portable
*   cost effective
*   can be used on standard equipment

The main limitation is that the data has to be transcribed by the researcher before analysis is possible although this allows for an in-depth knowledge of each search. The data may be qualitatively or quantitatively analysed, for example, entered into an analytical software package such as SPSS, Atlas.ti etc. Transcription may take a variety of forms dependent on the requirements of the research. For example, linear pathways may be drawn showing each action of the user and the system, thus:

<pre>Opening screen ![Forward arrow](forward.gif) types _"export licence"_
  ![Forward arrow](forward.gif) selects F1 [help] ![Forward arrow](forward.gif) 
  selects ESCAPE ![Forward arrow](forward.gif) selects ![Return](return.gif) ![Forward arrow](forward.gif) 
  selects SPACEBAR [to change operator from AND to OR] ![Forward arrow](forward.gif) 
  selects ![Return](return.gif) ![Forward arrow](forward.gif) types _"export 
  licences"_ ![Forward arrow](forward.gif) deletes _"export licences"_ 
  ![Forward arrow](forward.gif) selects ![Upwards arrow](up.gif) [to go up] ![Forward arrow](forward.gif) 
  selects SPACEBAR [to change to AND] ![Forward arrow](forward.gif) selects ![Upwards arrow](up.gif) 
  [to go up] ![Forward arrow](forward.gif) types _"*"_ [at end of _ 
  "export licence*"_ ] ![Forward arrow](forward.gif) selects F5 [search] 
  ![Forward arrow](forward.gif) "SEARCHING" ![Forward arrow](forward.gif) [2 ARTICLES 
  found] ![Forward arrow](forward.gif) selects F9 [to view headlines] ![Forward arrow](forward.gif) 
  selects F2 [to go back to search screen] ![Forward arrow](forward.gif) deletes _"licence*"_ 
  ![Forward arrow](forward.gif) selects ![Return](return.gif) ![Forward arrow](forward.gif) 
  selects ![Return](return.gif) ![Forward arrow](forward.gif) types _"licence*"_ 
  ![Forward arrow](forward.gif) selects F5 [search] ![Forward arrow](forward.gif) "SEARCHING" 
  ![Forward arrow](forward.gif) [7 ARTICLES found] ![Forward arrow](forward.gif) selects F9 
  [to view headlines] ![Forward arrow](forward.gif) selects ![Downwards arrow](down.gif) [to 
  move down to 5th article] ![Forward arrow](forward.gif) selects ![Upwards arrow](up.gif) 
  [to move to 1st article] ![Forward arrow](forward.gif) selects F9 [to view text] ![Forward arrow](forward.gif) 
  selects ![Downwards arrow](down.gif) [to move down text] ![Forward arrow](forward.gif) selects 
  F4 [to view headlines] ![Forward arrow](forward.gif) selects F10 [to exit] ![Forward arrow](forward.gif) 
  "DO YOU WANT TO FINISH? PRESS Y TO QUIT OR N TO CANCEL" ![Forward arrow](forward.gif) 
  selects Y</pre>

In this example a user is searching a newspaper on CD-ROM for information to satisfy their own need. User input is denoted by _"italics"_, user selections of system features is denoted by CAPITALS, system response by "CAPITALS" and researcher notes are enclosed by [ .]. This example is a real search by a user participating in a study currently being undertaken by Griffiths. Searches may also be presented graphically in the form of visual maps.

With Grand Art the participants' spoken comments can be recorded directly onto the same videotape as the search is being conducted by connecting a microphone to the video recorder. Recording via ScreenCam requires connection of a microphone to the PC. Thus a complete record of the search can be captured with a synchronised voice-over of the participant. [Hancock](#han87) (1987) observed searchers' actions in order to build a framework of activity and then used these actions to confirm what participants' said they were doing. Grand Art and ScreenCam captures all user and system activity along with participants' spoken aloud thoughts.

The following screen shots are presented to give an indication of the system in use and the manner in which transaction log data and verbal protocol data are brought together, although such a static example does not provide true picture of the benefits of the film version. The examples are taken from a recording of a user's searching session on a newspaper on CD-ROM.

## Example of transaction log and verbal protocol

The following presents an abridged example of a user's search on Chadwyck-Healey's _The Guardian on CD-ROM (including The Observer)_ and the verbalisations made during the search. Actions taken by the user are denoted by CAPITALS and verbalisations by _italics_. Observational notes by the researcher are indicated by the use of square brackets.

[Start of search]

<figure>

![Figure 1](../p139fig01.gif)

<figcaption>Figure 1</figcaption>

</figure>

_It's telling me the special keys are F1 and Escape but it doesn't tell me how to move around the page, so I'm going to guess that I have to use the arrow key._ USES DOWN ARROW TO MOVE TO THE OPTION 'SECTION OF PAPER'

<figure>

![Figure 2](../p139fig02.gif)

<figcaption>Figure 2</figcaption>

</figure>

_So which section of the paper do I want to choose? This is asking hard questions at this stage. I don't know. But it defaults to All Sections, but I actually want substantial articles. So let me, I was wondering, if I press Return. Again it's not telling me what to do_. PRESSES ENTER.

<figure>

![Figure 3](../p139fig03.gif)

<figcaption>Figure 3</figcaption>

</figure>

_Oh, so it's saying Sections means whether I want The Guardian or The Observer, not the sections within it. This time at least it's telling me to Enter to select._ PRESSES ENTER. _So that takes me back to_ [user anticipates the next screen].

<figure>

![Figure 4](../p139fig04.gif)

<figcaption>Figure 4</figcaption>

</figure>

_Oh!_ [user shows surprise as the screen appears, as they incorrectly anticipated where the system would take them to]_. Well now it's going on, telling me the options. Home, World, Features. Now, will Features? Again it's not clear whether Features will be both Home and Foreign/World or Features is some other category. I'm going to press F1._ PRESSES F1\. _For the first time I feel I need to ask for Help_.

<figure>

![Figure 5](../p139fig05.gif)

<figcaption>Figure 5</figcaption>

</figure>

_Now the Help has come up_ [reads from screen]. _The database has been organised into familiar sections - well they aren't, that's why I'm using Help_ [continues to read out loud and then pauses]. _So I can search one or more selected sections_ [continues to read out loud]. _I think that's so fundamental that that instruction_ ["You may select a section by highlighting with the up and down arrow keys, then pressing <SPACEBAR>. When you have finished your selections, press <ENTER>"] _should have been displayed on screen without me having to search Help to find it. OK, so I'll press Escape to clear the message. It tells me I can read more but I think I've got the message._ PRESSES ESCAPE. [The user goes on to select Home News, Foreign/World, Features and Leaders as sections of the newspaper to be searched].

<figure>

![Figure 6](../p139fig06.gif)

<figcaption>Figure 6</figcaption>

</figure>

_So I've chosen Home, Foreign, Features and Leaders. I'm happy with those dates_ [pause] a_nd now it's saying do I want to search Headline, Byline or Text. Well I want all of those_ [pause]. _Well , where do I put my search terms in?_ PRESSES DOWN ARROW FOR TEXT SEARCH. _Text search?_ PRESSES UP ARROW FOR HEADLINE SEARCH. _I wanted to search Text and Headline, it's not clear, again. Help?_ _F1 Help_. PRESSES F1.

<figure>

![Figure 7](../p139fig07.gif)

<figcaption>Figure 7</figcaption>

</figure>

[reads Help] _And all that's telling me is how to select the options, up and down arrow keys or using the first letter of the option. That's bugger all use I think_. PRESSES ENTER FOR MORE HELP.

<figure>

![Figure 8](../p139fig08.gif)

<figcaption>Figure 8</figcaption>

</figure>

_It's implying that I can only have one main menu choice, so I'll Escape._ PRESSES ESCAPE.

<figure>

![Figure 9](../p139fig09.gif)

<figcaption>Figure 9</figcaption>

</figure>

_Well it will have to be a Text Search then_. PRESSES DOWN ARROW FOR TEXT SEARCH. _It's not telling me whether a Text Search includes Headline searching. So, I press,_ [pause] _it's telling me to press Spacebar to view Headlines. No, but I'm doing a Text Search. I guess I'll just have to hit Enter. Again there's nothing on screen that I can see._ PRESSES ENTER.

<figure>

![Figure 10](../p139fig10.gif)

<figcaption>Figure 10</figcaption>

</figure>

[reads from screen] _Type in a word or phrase plus Enter to search the Story Text Index. So again it,_ [pause] _I'm not,_ [pause] _proximity?_ [pause] _So, I want to type privacy_ [user's first term] TYPES PRIVACY.

<figure>

![Figure 11](../p139fig11.gif)

<figcaption>Figure 11</figcaption>

</figure>

[pause] _Exact Match, or Word Between, no I don't want an Exact Match. In Same Paragraph? No, is there another option?_ [pause] _So, it's either going to be Exact Match or In Same Paragraph. Well, I just want it in the same article_ [pause]. _F2 is giving me an explanation but that's_ [pause]. _Am I going to have to use Help again?_ PRESSES F1.

<figure>

![Figure 12](../p139fig12.gif)

<figcaption>Figure 12</figcaption>

</figure>

[reads] _It looks like I can't do what I want to do so I'll have to Escape._ PRESSES ESCAPE.

<figure>

![Figure 13](../p139fig13.gif)

<figcaption>Figure 13</figcaption>

</figure>

_I'll just have to choose._ TYPES INTERNET.

<figure>

![Figure 14](../p139fig14.gif)

<figcaption>Figure 14</figcaption>

</figure>

_Privacy space internet, In Same Paragraph_. PRESSES DOWN ARROW FOR 'IN SAME PARAGRAPH'. [pause] _Hit Return._ PRESSES ENTER. _So now it's searching._

<figure>

![Figure 15](../p139fig15.gif)

<figcaption>Figure 15</figcaption>

</figure>

_And now it's found three stories. Press Spacebar, but then it's also saying on screen privacy and Internet in story it's found eleven stories. But it's selected three, which I'm guessing is telling me that it's only found three where they are in the same paragraph, but I've got to remember that from the screen before. I'm not very happy about that. It raises a lot of questions._

<figure>

![Figure 16](../p139fig16.gif)

<figcaption>Figure 16</figcaption>

</figure>

[the user continued by examining the articles retrieved from the search and then quitting the application].

## Conclusion

The use of Grand Art or ScreenCam enables transaction logging of end user interactions and system responses across any number of different interfaces and as such it is unlike those traditional transaction logging systems which are part of library automated systems. The equipment is robust and easy to operate, all relevant data is captured on a single media format and users may be individually identified. This approach fulfils the definition of transaction logging as proposed by [Blecic _et al._](#ble98) (1998) and overcomes many of the problems of more traditional methods of logging data. A user's interaction with an information retrieval system can be viewed more fully and in greater detail than traditional transaction logging systems.

This method of data collection and analysis can be used in a variety of situations including the study of information seeking behaviour, information retrieval, IR, research (use and evaluation of systems), human computer interaction, HCI, (design, development and usability aspects) on any type of electronic resource. This approach was first developed for a research project which sought to identify end user information seeking behaviour in relation to full text CD-ROM ([Griffiths, 1996](#gri96)). Further research (which developed from this initial project) is currently being undertaken and again utilises this transaction logging approach ([Griffiths, 2002](#gri02)). This work aims to determine the success factors of end user searching in relation to cognitive styles, HCI and IR. Both of these projects have required extensive user testing where transaction logging has been critical and this methodology has been proven to provide rich data suitable for analysis in a variety of ways, both quantitative and qualitative. This methodological approach has also been used in research looking at web based resource use of sighted and blind/visually impaired users ([Craven, 2002](#cra02); [Craven and Griffiths, 2002](#cra02b)). It also has wider implications in, for example, presentation of reports, use in teaching, or pre-recorded examples of searching systems for demonstration to an audience.

## References

*   <a id="and73"></a>Anderson, J.R. and Bower, G.H. (1973) _Human associative memory._ Washington, DC: V.H. Winston.
*   <a id="ble98"></a>Blecic, D., Bangalore, N.S., Dorsch, J.L., Henderson, C.L., Koenig, M.H. and Weller A.C. (1998) "Using transaction log analysis to improve OPAC retrieval results." _College and Research Libraries_, **59**(1), 39-50.
*   <a id="cra02"></a>Craven, J. (2002) [NoVA (Non-Visual Access to the Digital Library) project website.](http://www.cerlim.ac.uk/projects/nova.htm) Manchester: Manchester Metropolitan University, CERLIM. Available at: http://www.cerlim.ac.uk/projects/nova.htm [Site visited 12th July 2002]
*   <a id="cra02b"></a>Craven, J. and Griffiths, J.R. (2002) "30,000 different users, 30,000 different needs? Design and delivery of distributed resources to your user community". In Brophy, P., Fisher, S., Clarke, Z., eds. _Libraries without walls: the delivery of library services to distant users: proceedings of the 4th Libraries Without Walls Conference, 15th-17th September 2001._ London: Library Association [in press].
*   <a id="cra87"></a>Crawford, W. (1987) _Patron access: issues for online catalogs._ Boston, MA: G.K. Hall.
*   <a id="eri80"></a>Ericsson, K.A. and Simon, H.A. (1980) "Verbal reports as data." _Psychological Review,_ **87**(3), 215-251.
*   <a id="fla93"></a>Flaherty P. (1993) "Transaction logging systems: a descriptive summary." _Library Hi Tech_, **11**(2), 67-78.
*   <a id="for93a"></a>Ford, N. _et al._ (1993a) "Cognitive styles and online searching." _Information Research News_, **4**(1), 29-32.
*   <a id="for93b"></a>Ford, N. (1993b) "Cognitive styles and the design of information systems." _Information Research News_, **4**(1), 14-22.
*   <a id="gri96"></a>Griffiths, J.R. (1996) _Development of a specification for a full text CD-ROM user interface._ MPhil thesis. Manchester: Manchester Metropolitan University.
*   <a id="han87"></a>Hancock, M. (1987) "Subject searching behaviour at the library catalogue and at the shelves: implications for online interactive catalogues." _Journal of Documentation,_ **43**(4), 308-321.
*   <a id="han90"></a>Hancock-Beaulieu, M., Robertson, S. and Nielsen, C. (1990) _Evaluation of online catalogues: an assessment of methods_. London: The British Library Research and Development Department (BL Research Paper 78).
*   <a id="kee78"></a>Keen, E.M. and Wheatley, A. (1978) _Evaluation of printed subject indexes by laboratory investigation ( EPSILON)_. Aberystwyth: College of Librarianship Wales.
*   <a id="kee95"></a>Keen, E.M. (1995) _Interactive ranked retrieval project_. London: British Library Research and Development Department. British Library report 6200.
*   <a id="kin87"></a>Kinsella, J. and Bryant P. (1987) "Online public access catalogue research in the United Kingdom: an overview." _Library Trends,_ **35**(4), 619-629.
*   <a id="kur93"></a>Kurth M. (1993) "The limits and limitations of transaction log analysis." _Library Hi Tech,_ **11** (2), 98-104.
*   <a id="las23"></a>Lashley, K.S. (1923) "The behaviouristic interpretation of consciousness II." _Psychological Review,_ **30**, 329-353.
*   <a id="lon96"></a>Long, D. I. and Bourg, T. (1996) "Thinking aloud: telling a story about a story." _Discourse Processes,_ **21**, 329-339.
*   <a id="mag91"></a>Magliano, J.P. and Graesser, A.C. (1991) "A three-pronged method for studying inference generation in literary text." _Poetics,_ **20**, 193-232.
*   <a id="man98"></a>Manglano, V., Beaulieu, M. and Robertson, S. (1998) "Evaluation of interfaces for IRS: modelling end-user searching behaviour." Paper presented at British _Computer Society Information Retrieval Specialist Group Colloquium,_ Grenoble.
*   <a id="mar84"></a>Markey, K. (1984) _Subject searching in library catalogs: before and after the introduction of online catalogs._ Dublin, OH: OCLC.
*   <a id="mei67"></a>Meister, D. and Sullivan D. (1967) _Evaluation of user reactions to a prototype on-line information retrieval system._ Oak Brook, IL: Bunker-Ramo Corporation. (Report to NASA by the Bunker-Ramo Corporation. Report Number NASA CR-918.)
*   <a id="new72"></a>Newell, A. and Simon, H.A. (1972) _Human problem solving._ Englewood Cliffs, NJ: Prentice Hall.
*   <a id="nis77"></a>Nisbett, R.E. and Wilson, T.D. (1977) "Telling more than we know: verbal reports on mental processes." _Psychological Review,_ **84**, 231-259.
*   <a id="pet93"></a>Peters, T. (1993) "The history and development of transaction log analysis." _Library Hi Tech,_ **11**(2), 41-66.
*   <a id="sim79"></a>Simon, H.A. (1979) _Models of thought._ New Haven, CT: Yale University Press.

## <a id="app1"></a>Appendix 1 - Supplier and technical information for Grand Art

[http://www.keyzone.co.uk/frame_pc.html](http://www.keyzone.co.uk/frame_pc.html)

*   supports standard VGA modes (0-13).
*   NTSC system supports 16 million colours at 640x480\.
*   PAL system (UK) supports 64k colours at 800x600\.
*   allows simultaneous VGA monitor/TV viewing.
*   bundled with microphone, suitable for presentation.
*   PAL TV system can be changed to 60Hz field rate, to reduce field flicker.
*   composite NTSC (PAL) signal video output.
*   analogue RGB video output (scart).
*   separate Y/C NTSC (PAL) (S-Video) 4-pin output.
*   dimensions: 129mm x 103mm x 26mm.

Cost details can be obtained from the above website.

## <a name="app2"></a>Appendix 2 - Supplier and technical information for Lotus ScreenCam

[http://www.lotus.com/home.nsf/welcome/screencam](http://www.lotus.com/home.nsf/welcome/screencam)

Lotus ScreenCam is released in a variety of formats for the Windows 95 and Windows NT4x platforms. Each release is designed to work for specific operating systems. There is currently no version for Windows 2000 or Windows XP. The Windows 95 release will work on Windows 98 (and variants) with some limitations (due to some video cards driver/chip set manufacturers that do not comply with Microsoft recommendations and standards).

**ScreenCam for Win95 system requirements:**

Systems requirements for ScreenCam are IBM or compatible PC certified for use with MS Windows 95\. VGA 16-colour or higher graphics adapter and monitor. Parallel port or sound card-enabled device supported by Windows 95, speaker(s) or headphones, microphone and mouse. 10MB free space is recommended for recording.

**ScreenCam for NT4.0 System Requirements:**

Systems requirements for ScreenCam for WinNT4.0 are IBM or compatible PC certified for use with MS Windows NT4.0, S3 compatible graphics adapter. Parallel port or sound card-enabled device supported by Windows 85, speaker(s) or headphones, microphone and mouse. 10MB free space is recommended for recording.

Cost details can be obtained from the above website.