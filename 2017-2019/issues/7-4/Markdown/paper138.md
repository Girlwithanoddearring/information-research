#### Information Research, Vol. 7 No. 4, July 2002,

# Critical Success Factors and information needs in Estonian industry

#### [Aiki Tibar](mailto:tibar@lib.ttu.ee)  
Tallinn Technical University Library  
Tallinn, Estonia

#### **Abstract**

> The article reports the results of the study on the critical success factors and related information needs in Estonian industry conducted in 1999\. Data were collected by interviews with 27 managers and engineers from 16 manufacturing companies in various industries. Most of the critical success factors taken up were related to marketing, information management, quality management, product development and technological innovations. The information needs of managers and engineers were related to competitors, customers, markets, technology, regulations, etc. Some identified CSFs expressed also priorities for development by Estonian economic authorities: to support the implementation of new technologies and introduction of quality management methods. The finding that information management was perceived as a very critical area supports the result of the recent Finnish study on CSFs.

## Introduction

During the 1990s, the Estonian manufacturing industry has undergone structural changes. After the collapse of the Soviet Union and the reestablishment of independence of Estonia (August 1991) the target for economic reforms became the restoration of market economy and reorientation to the Western market. The goal of national industrial policy is the competitiveness of the country. [Kilvits](#kil01a) (2001a: 335-336) refers to fundamental principles of industrial policy in 1991-2000, which are as follows: openness of the Estonian industry; the accession of Estonia to the European Union; promotion of privatisation; and promotion of foreign capital inflow.

An important task and challenge for Estonia is the accession to the European Union (EU). The accession negotiations between Estonian and the European Communities began in 1998 ([Lumiste, 2000](#lum00)). Lumiste notes that most affected by the European Union is power engineering, agriculture and food industry.

The Ministry of Economic Affairs ([Purju, 1998](#pur98)) states that one of the shortages of many Estonian enterprises is the lack of quality control systems. The limited resources of Estonia's state budget should firstly be directed at rapid introduction of Western standards and conformity assessment procedures. There is an urgent need to develop the framework enabling the observance of norms and traditions regulating the EU internal market.

In 1999 the next phase has started in the restructuring of manufacturing industry ([Purju, 1999](#pur99)). The process calls for additional investments in product development, new technologies implementation, introduction of internationally acceptable quality requirements, and continued training of staff. According to [Kilvits](#kil01b) (2001b: 68) quality of products and services, as well as of management that conforms to the requirement of the external markets is an essential criterion for competitiveness.

The Estonian Association of Small and Medium-sized Enterprises conducted a survey ([Väikeettevõtluse, 1999](#vai99)) to find out data on business environment and problems of small and medium-sized enterprises. Among other findings the survey revealed what kind of means of support the entrepreneurs hope to receive from the government. The most important item was regarded tax allowances; on the second place it was put the general access to information; these two were proceeded with better access to funds (credits), and support in exporting matters and in renewing technology ([Väikeettevõtluse, 1999: 16](#vai99)).

Estonian enterprises, especially small and medium-sized, are advised and supported by various institutions and programmes. These include local business consulting firms and centres, finance institutions and related programmes, institutions and programmes to promote export and business contacts, government institutions, etc. ([Väikeettevõtluse, 1999](#vai99)).

The libraries have to make more efforts to promote their services and information provision to industry. It was affirmed by the survey ([Virkus & Tamre, 1995](#vir95)) carried out to obtain an overall picture of the actual use and needs of business information and business information services in Estonian firms. The survey results showed that the most used business information sources in the firms were newspapers, business catalogues and official publications. Critical comments on libraries were that information sources provided by them were not up-to-date, acquisition policy was not competent enough, acquisition and processing were too slow and service was not fast enough. On the other hand business people did not have appropriate knowledge about the collections and services of libraries.

Tallinn Technical University Library as the Estonian resource library in the field of technology is interested in developing personal contacts with actual and potential industrial customers, identifying their information needs and promoting the information services to industrial customers.

In 1999 the study on information needs and information-seeking behaviour of managers and engineers in industry was conducted. (See also: Tibar, [2000](#tib00), [2001](#tib01)). This paper focuses on the critical success factors (CSFs) and related information needs identified in the study.

## Research method

### The critical success factors approach

The critical success factors approach ([Daniel, 1961](#dan61); [Rockart, 1979](#roc79)) was used to identify information needs of managers and engineers in various industries. According to [Rockart](#roc79) (1979: 85), critical success factors support the attainment of organizational goals, and if results in these areas are satisfactory, the organization is competitive. The CSF method focuses on individual managers and on each manager's current information needs. Rockart (1979: 86-87) states that some CSFs may be related to the structure of the particular industry, the others are caused by environmental factors, and there are also temporal factors for a particular period of time.

[Pellow & Wilson](#pel93) (1993) explored the potential of CSFs methodology for assessing information requirements of heads of university departments. They conclude, by reviewing previous studies, that the main strengths of the method are: acceptance by senior managers; consideration of all the information needed, not only that which is easy to collect; and the critical success factors point to priorities for development.

The case studies carried out in two Finnish companies ([Huotari, 1997](#huo97); [Huotari, 2001](#huo01); [Huotari & Wilson, 1996](#huo96)) indicate the value of the CSFs approach in identifying corporate information needs. [Wilson's](#wil94) (1994) proposition to combine CSFs with the concept of value chain ([Porter, 1985](#por85)) was tested to identify information intensive areas in the pharmaceutical and the publishing company.

[Huotari & Wilson](#huow01) (2001) report the series of studies conducted in U.K. and Finland, in both academic and business institutions, and confirm the value of the CSFs approach in identifying organizational objectives and in relating the information needs of personnel to those objectives.

### Data collection

Letters were sent to 25 manufacturing companies, the winners of the contest of Estonian Industry TOP 50 in 1998 (Eesti edukad, 1998) in Tallinn and the surrounding area. Altogether, 27 managers and engineers from 16 companies agreed to take part in the study. Data were collected during the period of May-November 1999.

The data collection was based on semi-structured interviews. Respondents were asked to specify the critical success factors for their organizational level, which support the achievement of company's goals. When the critical success factors had been identified it was possible to specify related information needs.

Interview respondents (according to the Classification of European Union Countries) came from 7 medium-sized enterprises (50-249 employees), and 9 large enterprises (more than 250 employees). Respondents categorised by job title and industry are presented in Table 1.

<table><caption>Table 1: Respondents by the job title and industry</caption>

<tbody>

<tr>

<th>Industry</th>

<th>Number of firms</th>

<th>Number of respondents</th>

<th>Respondents by the job title</th>

</tr>

<tr>

<td>Food industry</td>

<td>6</td>

<td>8</td>

<td>export and marketing director, manager of quality centre, quality manager (2), chief engineer, process engineer (2), IT specialist</td>

</tr>

<tr>

<td>Chemical industry</td>

<td>3</td>

<td>5</td>

<td>production director, product manager, IT manager, purchase manager, chemical engineer</td>

</tr>

<tr>

<td>Transportation vehicles and devices industry</td>

<td>2</td>

<td>4</td>

<td>product development manager, assistant director of engineering centre, design engineer, CAD engineer</td>

</tr>

<tr>

<td>Energy</td>

<td>1</td>

<td>4</td>

<td>assistant production director, head of the department, head of the sector, leading engineer</td>

</tr>

<tr>

<td>Furniture industry</td>

<td>2</td>

<td>3</td>

<td>general director, development manager, chief process engineer</td>

</tr>

<tr>

<td>Electrotechnical and electronics industry</td>

<td>1</td>

<td>2</td>

<td>manager of quality service, manager of new technology group</td>

</tr>

<tr>

<td>Textile manufacturing</td>

<td>1</td>

<td>1</td>

<td>quality manager</td>

</tr>

<tr>

<th>Totals</th>

<th>16</th>

<th>27</th>

<td> </td>

</tr>

</tbody>

</table>

## Categories of critical success factors

The interview data indicate CSFs on different organizational levels in various companies. Using the CSF approach it was possible to focus on those areas of activity that needed careful attention from managers and engineers to support the attainment of company goals.

The statements of respondents on the critical success factors were analyzed and aggregated into ten broader subject categories that were close to some activity area. These categories are as follows: marketing; information management; quality management and quality assurance; product development; technological innovations; personnel; finance; general management; efficiency; pollution-free technology and environmental management.

### Marketing

Success factors related to marketing occurred most frequently. It was vital to analyse the market environment and look for the ways to meet consumer preferences.

The quality manager from the food industry stressed that it was essential to organize active selling. The export and marketing director from the same industry pointed out that to stay in competition it was necessary to be aware of marketing techniques. The director noted:

> In order to survive on the market it is essential to know the market where our products are distributed, it means what kind of preferences consumers have, and who are the competitors. We have developed various products; also the price level is suitable for the consumer. We have carried out advertising campaigns, like special offers. The growth of sales shows that we have found suitable markets for our products. So, our products can compete with goods in Estonia as well as abroad. More attention should be paid on design and package. We have to collect information concerning the offer, the demand, distribution, pricing, and the present state of the market.

A CSF for the production director from the chemical industry was responding quickly to market demands. The product manager regarded awareness of the market situation, including competitors as a CSF. For the purchase manager it was essential to use reliable suppliers in purchasing raw material. For this purpose the manager monitored suppliers and prices continuously in order to receive quality goods.

The product development manager from the transportation vehicles and devices industry stated that in order to defy competition it was necessary to know the market. The CAD engineer considered that awareness of competitors and their products was essential.

The general director from the furniture industry saw being informed of the market's demands and of the target market purchasing power as CSFs. A development manager from the same company considered also awareness of target market demands as a critical factor.

Managers from the energy company regarded as important the development of stronger relationships with customers through effective communication and customer service.

The manager of quality service from the electro-technical and electronics industry stated that it was essential to carry out market analysis in order to be aware of customers' demands.

### Information management

Information management as a CSF was mostly defined on the statements of respondents that expressed the requirement for develop the company's information systems and improve access to internal and external information.

The export and marketing director from the food industry stated that the company was developing a network inside the firm. At that time they had good software, but not enough hardware. The director considered essential to develop marketing information system to report and analyse statistical data on sales by channels and type of products.

A quality manager from textile manufacturing regarded the development of electronic information system for dissemination of firm's documentation as a CSF.

According to the IT manager from the chemical industry CSFs were information systems that meet the requirements of the company, and their reliability. The manager noted:

> It is vital that the information systems meet the requirements of the company. From the users point of view information systems must be reliable, simple to use and intelligible. The situation may be regarded satisfactory if there are few questions and complaints. Of course, I should like to have more positive feedback from the users.

From the IT specialist's standpoint CSF was the necessity to keep pace with the rapid development of information technology and at the same time find cheaper ways to renew hardware and software.

Two of the largest firms had in-house libraries (in energy and in transportation vehicles and devices industry). The assistant director of engineering centre from the transportation vehicles and devices industry regarded essential to reorganize the technical library of the company in order to supply the specialists with relevant information.

The manager of the new technology group from the electro-technical and electronics industry considered essential to receive needed information at the right time. The manager noted:

> Sometimes the access to information is 'closed' and you have to pay for it. As I know from my experience if the source of information is located then there are no difficulties to obtain it.

In order to keep up with the most recent developments in knowledge it was considered critical to have fast access to important sources of information, and also to be aware of different information seeking possibilities. In some occasion an access to internal and external information was insufficient because the respondent had no possibility to use a networked PC.

Modern information technology enables to achieve competitive advantage through fast and extensive dissemination of information and thereby gain the management effectiveness. This was tested and put forward in studies of Finnish manufacturing companies (Huotari, 1997; Huotari, 2001; Huotari & Wilson, 1996). Information management was identified as the most critical success factor for the pharmaceutical company but less critical for publishing.

### Quality management and quality assurance

Another major group of success factors was related to quality issues: implementation and development of quality control system, ensuring stable quality and product safety, strict quality control of raw materials, ensuring the quality of production processes and services.

The most mentioned CSFs for managers and engineers from the food industry were guaranteeing product quality and safety. According to the chemical engineer from the chemical industry, ensuring product quality and safety was also among the CSFs. The development manager from the furniture industry stated that important factors are quality of materials and product. Respondents stated that the companies had implemented or were going to implement a system of product safety assurance.

A manager of quality centre from the food industry noted:

> It is essential to guarantee the safety and stable quality of our products. Strict control of physical and chemical indicators of the technological process is required. In order to control the whole process we are introducing the quality management system.

Managers from the electro-technical and electronics industry considered also the development of quality management system in the firm important. The quality manager from the textile manufacturing regarded the quality of production and business processes as a CSF.

### Product development

Specialists from the food industry, furniture industry, and transportation vehicles and devices industry regarded production development as a CSF. The reason here is high competition in these branches of industry between Estonian producers as well as with external producers.

The quality manager from the food industry stated that a CSF was the launching of new products. The process engineer from another firm regarded the use of cheaper and healthier materials in products as a CSF. The engineer noted:

> We have developed various product groups and we are looking ways to use cheaper and healthier materials in products. New products are related to new raw materials. Sometimes it is not easy to change the materials due to old equipment, and because of many technical problems. Additional investments are needed to test and examine a new product, also to control the quality of raw materials.

Managers and engineers from the transportation vehicles industry considered the use of computer-based product design and development essential. According to the CAD engineer special software speeds up the process of product development. The product development manager pointed out the best price of a product and its suitability for the customer as CSFs.

The chief process engineer from the furniture industry stressed that the goods on offer must be up-to-date which in turn requires suitable equipment and materials. The development manager from another firm stated that the products must satisfy the standards, regulations, aesthetic requirements, and target market demands. The manager mentioned that customers want the products with an excellent quality-price ratio. It can be stated that product development is closely related to marketing and quality assurance.

### Technological innovations

This category includes factors that are related to flexibility and safety of production processes, and new technology implementation and equipment. Managers mentioned if new equipment is sought for production, it was necessary to consider its quality, power of production, flexibility and price.

CSFs for the production director from the chemical industry were the flexibility and safety of production processes. The director stressed that the company had to develop its ability to respond rapidly to market demands.

The development manager from the furniture industry noted that being modern is one part of technology, i.e. material- and labour-saving technologies should be introduced.

The manager of new technology group from electro-technical and electronics industry stated that the firm was going to build up a laboratory for technological innovations. The manager regarded the creation of a strong infrastructure in manufacturing as a critical factor. The manager noted:

> It is important to choose appropriate equipment and technologies that correspond to our conditions and possibilities in order to gain the optimum result regarding our productivity.

A quality manager from the food industry stressed that production engineering of the company should have to adjust to the sanitary and hygiene requirements of the European Union.

### Personnel

Several managers saw employees' qualification as a CSF. It was stressed that qualified labour force and training on an on-going basis was needed to raise the level of productivity. The increased quality requirements and new technologies require new knowledge and skills.

The product manager from the chemical industry stated that training seminars were held for the salespeople to train them according to the type of products and keep them up-to-date with the latest developments.

Personnel as a CSF was considered very important by the managers from energy company: in the transition from electricity-selling organization to a service-orientated organization, special attention was paid to staff qualification and training. A leading engineer suggested that the company should develop an engineering centre with highly qualified engineers/experts.

### Finance

The general director from the furniture industry regarded the fixed costs of the firm and cost per unit as a CSF. The development manager from the same firm mentioned low production costs as a CSF.

The manager from energy company noted that investments were needed in order to raise the effectiveness of energy production and implement pollution-free technology.

The export and marketing director from the food industry mentioned that the company was looking for investors to implement quality and environmental management systems. A process engineer and a quality manager from the same industry considered investments in product development and new equipment essential. Insufficient development expenses were one reason why firms had difficulties in making use of technologies that correspond to ISO standards.

### General management

Several managers saw rational job management and planning process as critical success factors.

The managers from the energy production stated that during the restructuring of the company it was important to build up competitive organization for the free market of energy. The quality manager from textile manufacturing also mentioned that the company was going through reorganization and improvement of the structure was one CSF.

The manager of the new technology group from the electro-technical and electronics industry stressed the importance of job management and making right decisions.

### Efficiency

Efficiency as a CSF was essential to turn processes more effective in order to achieve better results and cost savings.

Efficiency was stressed especially in relation to energy production, transportation and use. Two managers and the engineer from energy company mentioned production efficiency as one CSF. The chief engineer from the food industry related the efficiency with saving energy in the company.

The quality manager from textile manufacturing regarded the efficiency of production and business processes with relevant system for information management as a CSF.

### Pollution-free technology and environmental management

Pollution-free technology as a CSF was mentioned by one manager and the engineer from the energy production. The environmental management was seen vital by the export and marketing director and a process engineer from the food industry, and the production director from the chemical industry.

The manager from the energy company noted:

> One of our priorities is environmentally-friendly production of energy in order to prevent and control pollution. We have to measure waste products and carry out analysis. Pollution charges are used to regulate this sphere and stimulate protection of the environment.

## Types of information need

When the critical success factors had been identified it was possible to specify the information needed by managers and engineers in order to manage these factors. Interview results indicate that main areas in which information was needed were: 1) competitors, customers, suppliers and other market information; 2) products and technologies (information concerning product development and technological innovations); 3) resources (information on finance and workforce); 4) legislation and regulations (legal acts, directives, standards, norms); 5) economic and political trends.

These study findings are quite similar to the results of previous studies on the business information needs and uses. (See: [Roberts & Clifford, 1986](#rob86); [White & Wilson, 1988](#whi88); [Auster & Choo, 1993](#aus93), [1994](#aus94)).

[Roberts & Clifford](#rob86) (1986) investigated the demand and supply of business information of manufacturing firms. The main areas where information was needed were marketing, products, exporting, finance, competitors and patents.

[White & Wilson](#whi88) (1988) examined the relationship between managers' organizational roles and functions and their information needs and uses. The case studies of ten manufacturing firms indicated that a straightforward correlation between roles and information needs couldn't be asserted. The study identified four main areas were external information was needed: the need for information about customers, about competition, on statutory regulations, and the problems of export.

Auster & Choo ([1993](#aus93), [1994](#aus94)) have done research on how top managers acquire and use information about the external business environment in the Canadian publishing and telecommunications industries. The data suggest that the chief executives concentrate their scanning on the competition, customer, regulatory, and technological sectors of the environment.

The context of information needs of managers and engineers may be seen in the general information-seeking model developed by [Wilson](#wil99) (1999: 252) who suggests that information needs and information-seeking behaviour are influenced by following factors: the environments (political, economic, technological, etc.), role-related requirements, and personal factors. In this study: information needs and information-seeking behaviour of managers and engineers were influenced by the environment inside and outside the firm.

Several respondents mentioned that information was needed quickly and personal contacts were regarded reliable information source, which is also easy to access. [White & Wilson](#whi88) (1988) have found that information users rely on personal contacts because they regard it as 'trusted' source of information. [Choo & Auster](#cho93) (1993: 285) refer to previous research and conclude that informal sources, including personal contacts, are frequently as important as and sometimes more important than formal information sources.

### Competitors, customers, suppliers and other market information

Market information is needed for scanning the business environment and improving the competitiveness of the company. In relation with several CSFs managers and engineers stated that information is needed on competitor products, its prices, experience of other companies and new trends.

The study by [White & Wilson](#whi88) (1988) indicated that external information on competitor activity was cited regularly as being the most important and the most difficult to obtain. Respondents in Estonian study claimed that they have also had difficulties in obtaining information on competitors and their products.

Managers needed information for organizing marketing activity and defining the place of the firm in its industrial sector. For this purpose statistical data were needed about industrial sector as a whole. Information about firm's finances, distribution and income was also needed. In order to consider demands of customers, target market research was carried out. Customers' complaints were analyzed and data about target group's purchase power were needed.

Marketing information was needed in product development, working out price strategy, managing the processes more effectively. Managers were interested in management literature and the experience of foreign firms about management of working processes.

Some managers said that accurate and timely statistical data, both internal and external, were needed to make relevant decisions. For the general director it was difficult to acquire latest statistical data about the industrial sector in order to carry out planning process and to manage in changing business environment. The export and marketing director and assistant production director mentioned also that sometimes information comes too late. For these managers the quality of information was essential. This supports the finding of the survey by [Auster and Choo](#aus93) (1993) that between environmental uncertainty, source accessibility, and source quality, source quality is the most important factor in explaining source use in scanning.

### Products and technologies

Technological information is necessary for the development of products and materials and innovation of manufacturing technology. Specialists need information on material processing technologies and effective and environment-friendly production methods. Patent information is needed in product development and technological innovations.

In product improvement and production development attention is drawn to the quality and safety of manufacturing processes, reducing the costs of production, raising the efficiency of production and making it more flexible. For the implementation of a suitable technological process information is needed on machines, equipment and tools, their quality, production capacity and price. Information is looked for about maintenance and repairs requirements and safety rules of manufacturing equipment. Information on new models, raw materials and their characteristics is also very important.

In relation with technological innovations information was needed about firm's operation: manufacturing process, proportion between investments and accidents, working security, effective instructions for the introduction of a new production process.

Managers and engineers, responsible for product quality and safety in food industry needed information about new methods of analysis, raw material and its attributes, hygiene information, new equipment, washing materials and germicides for equipment, standards and EU directives on quality assurance and management. A quality manager from textile industry was responsible for quality of production and business process. This manager was looking for experience about restructuring of the firm, handbooks on modelling and analyzing business processes and standards on quality management.

Managers are more and more aware of the fact that production development and improvement of production processes' quality and efficiency depend on the development of the company's IT basis.

### Resources: information on finance and workforce

A company's internal financial data and information on investment possibilities is needed for the arrangement of marketing and product development, modernisation of technologies and equipment, safety and effectiveness of production processes, personnel training and development of information systems as well as acquisition of information and information sources.

Information was needed about firm's financial situation: sales, income, and analysis of production and realization, investment possibilities. Also analysis of foreign firm's cost proposals were carried out.

Human resource is evaluated according to its competency i.e. existing knowledge and skills or development of these. Staff training is especially important when a company is being restructured. In order to raise people's qualification it is necessary to have information on training courses and materials. Training possibilities were sought for every organisational level on working place and outside the firm.

### Legislation and regulations: legal acts, directives, standards, norms

In order to maintain present or enter a new market, managers had to be well informed of the legislation of target market. In the organisation of production processes and construction of products one needs information on standards and norms as well as legal acts which govern the field of health & safety etc. Export products to the Western countries must meet the requirements of the EU norms. Likewise it is necessary to know the export conditions (e.g. customs tariffs).

### Economic and political trends

Managers need information on external political and economic environment in order to make right decisions. Changes in economy or politics may in their turn bring about changes on markets and in the legislation. This information is necessary for keeping one's market share or expand it. The company cannot usually influence the factors of the macro-environment but it can scan the changes and make use of that environment.

Political developments of government, integration in the European Union, relations with Russia are essential factors among other things in managerial decision making. For example some managers stated that they have had problems in acquiring information from Russia due to the unstable political and economic situation in this country.

## Conclusion

The CSFs approach enabled to focus on priority areas for development in Estonian industry and related information needs. The findings of the study indicate that marketing as a CSF was mentioned the most. It is seen very important to run the company on the real knowledge of the market situation. Access to information on customers and their preferences, competitors and their activity was crucial for managers on different organizational levels.

Information management appeared to be a very critical factor in order to disseminate and manage the information within the company, and to access the internal and external information at the right time.

High priority was also given to the quality of manufacturing and products. An essential prerequisite of Estonian entrepreneurs for entering the Western market is the introduction of internationnally acceptable quality requirements. According to [Kilvits](#kil01b) (2001b: 70) implementation of quality management principles and obtaining ISO 9000 quality certificates by Estonian entrepreneurs improves the reputation of Estonia as a reliable and competitive business partner.

The study results may be compared in the context of Estonian business environment. Some identified CSFs expressed also priorities and problems raised by Estonian economic authorities ([Purju, 1999](#pur99)): one key issue is highly qualified labour respective to the industry's demand, and the emphasis must be laid on supporting the introduction of technological development and quality management methods.

During the study Tallinn Technical University Library developed personal contacts with actual and potential industrial customers, and its reputation as a "trusted source" of information was established.

Further studies and regular contacts with actual and potential customers in industry are needed in order to develop special library services for meeting their information needs.

Library-customer relationship can be developed in co-operation with following institutions and organisations:

*   The Innovation Centre of Tallinn Technical University is established to link R&D and business activities by, for example, communicating the needs of industry to research institutions and facilitating the transfer of technology and know-how to companies. The library of the university can co-operate in this network to ensure better use of its services and resources by industrial customers.
*   An efficient network should be developed with other Estonian libraries, like the National Library, the Estonian Academic Library, the Library of Standards and the Library of Patents, in order to co-operate in acquisition and share the information which helps the customers locate needed information sources and provide them quality and fast service.
*   Closer contacts with other institutions, like the Camber of Commerce and Industry, contribute to identifying the potential customers and meeting their information needs more effectively.

## References

*   <a id="aus93"></a>Auster, E. & Choo, C.W. (1993). "Environmental scanning by CEOs in two Canadian industries." _Journal of American Society for Information Science_, **44**(4), 194-203.
*   <a id="aus94"></a>Auster, E. & Choo, C.W. (1994). "How senior managers acquire and use information in environmental scanning." _Information Processing & Management_, **30**(5), 607-618.
*   <a id="cho93"></a>Choo, C.W. & Auster, E. (1993). "Environmental scanning: acquisition and use of information by managers." _Annual Review of Information Science and Technology_, **28**, 279-314.
*   <a id="dan61"></a>Daniel, D. R. (1961). "Management information crisis." _Harvard Business Review,_ **39** (September-October), 111-121.
*   <a id="ees98"></a>Eesti edukad (1998). _Eesti edukad ettevõtted 1998 [Successful Estonian enterprises 1998]_. Tallinn: ETTK.
*   <a id="huo97"></a>Huotari, M.-L. (1997). _Information Management and Competitive Advantage. The Case of a Finnish Publishing Company_. Tampere: University of Tampere, Department of Information Studies. _Finnish Information Studies 7_.
*   <a id="huo01"></a>Huotari, M.-L. (2001). _Information Management and Competitive Advantage. Case II: A Finnish Pharmaceutical Company_. Tampere: University of Tampere, Department of Information Studies. _Finnish Information Studies 19_.
*   <a id="huo96"></a>Huotari, M.-L. & Wilson, T.D. (1996). "The value chain, critical success factors and company information needs in two Finnish companies." In: Ingwersen, P. & Pors, N.O. (Eds.) _Information Science: Integration in Perspective_ (pp. 311-323). Copenhagen: The Royal School of Librarianship.
*   <a id="huow01"></a>Huotari, M.-L. & Wilson, T.D. (2001). "[Determining organizational information needs: the Critical Success Factors approach.](http://InformationR.net/ir/6-3/paper108.html)" _Information Research_, **6**(3) Available at: http://InformationR.net/ir/6-3/paper108.html [Site visited 9th July 2002]
*   <a id="kil01a"></a>Kilvits, K. (2001a). "Rational harmonisation of industrial policy with the requirements of the European Union. "In: Vensel, V. & Wihlborg, C. (Eds.) _Estonia on the threshold of the European Union: financial sector and enterprise restructuring in the changing economic environment: collection of papers_ (pp. 329-349). Tallinn: Tallinn Technical University.
*   <a id="kil01b"></a>Kilvits, K. (2001b). "Structural changes in industry." In: Ennuste, Ü. & Wilder, L. (Eds.) _Factors of convergence: A collection for the analysis of Estonian socio-economic and institutional evolution._ (pp. 37-81). Tallinn: Estonian Institute of Economics at Tallinn Technical University.
*   <a id="lum00"></a>Lumiste, R. (2000). "Estonian industry in 1998 and 1999". In: _Eesti edukad ettevõtted_ _1994-1999_ = _Successful Estonian enterprises 1994-1999_. (pp. 44-47). Tallinn: ETTK.
*   <a id="pel93"></a>Pellow, A. & Wilson, T.D. (1993). "The management information requirements of heads of university departments: a critical success factors approach." _Journal of Information Science,_ **19**(6), 425-437.
*   <a id="por85"></a>Porter, M.E. (1985). _Competitive advantage: creating and sustaining superior performance_. New York: Free Press.
*   <a id="pur98"></a>Purju, A., ed. (1998). _Estonian economy 1997-1998_. Tallinn: Republic of Estonia, Ministry of Economic Affairs.
*   <a id="pur99"></a>Purju, A., ed. (1999). _Estonian economy 1998-1999_. Tallinn: Ministry of Economic Affairs of Estonia.
*   <a id="rob86"></a>Roberts, N. & Clifford, B. (1986). _Regional variations in the demand and supply of business information: a study of manufacturing firms_. Sheffield: University of Sheffield, Department of Information Studies.
*   <a id="roc79"></a>Rockart, J. F. (1979). "Chief executives define their own data needs." _Harvard Business_ _Review_, **57**(2), 81-93.
*   <a id="tib00"></a>Tibar, A. (2000). "Information needs and uses in industry: the implications for information services." _The New Review of Information Behaviour Research_,**1**, 185-200.
*   <a id="tib01"></a>Tibar, A. (2001). "Information for the industry: the study of needs and provision. "In: _Libraries in Knowledge-based Society: Proceedings of the 3<sup>rd</sup> Nordic-Baltic Library Meeting, October 25-26, 2001, Tallinn, Estonia_ (pp. 147-151). Tallinn: Estonian Librarians Association.
*   <a id="vir95"></a>Virkus, S. & Tamre, M. (1995). "Business information - necessities and possibilities: why smaller countries need a co-ordinated approach to the access of business information from home country and from abroad. What experiences does Baltikum have in this field?". In: _Information Power: Proceedings from the 9<sup>th</sup> Nordic Conference on Information and Documentation_ (pp. 189-198). Oslo.
*   <a id="vai99"></a>Väikeettevõtluse (1999). _Väikeettevõtluse olukorrast Eestis: Aruanne 1998. a. kohta_. Tallinn : EL PHARE väikeettevõtluse toetamise programm Eestis.
*   <a id="whi88"></a>White, D.A. & Wilson, T.D. (1988). _Information needs in industry: a case study approach_. Sheffield: University of Sheffield, Department of Information Studies.
*   <a id="wil94"></a>Wilson, T.D. (1994). "Tools for the analysis of business information needs." _Aslib Proceedings_, **46**(1), 19-23.
*   <a id="wil99"></a>Wilson, T.D. (1999). "Models in information behaviour research." _Journal of_ _Documentation_, **55**(3), 249-270.

### Acknowledgements

The author would like to acknowledge the suggestions and ideas she has received from Professor Tom Wilson, University of Sheffield, and Professor Maija-Leena Huotari, University of Tampere, as well as the anonymous referees.