<header>

#### vol. 18 no. 4, December, 2013

</header>

<article>

# Comparative analysis of homepage Website visibility and academic rankings for UK universities

#### [Melius Weideman](#author)  
Website Attributes Research Centre, Cape Peninsula University of Technology,  
PO Box 652, Cape Town 8000, South Africa

#### Abstract

> **Introduction.** The pressure on universities worldwide has increased to transform themselves from isolated educational institutions to profit-generating businesses. The visibility of a Web page plays a role in attracting potential clients, as more and more young users are depending on the Web for their everyday information needs. One of the purposes of this study was to evaluate the visibility of homepages of top UK universities, and rank them. Secondly, the existence of a correlation between Website visibility ranking and academic ranking was investigated. The goal of this research was to provide a repeatable method of measuring university Website visibility, and for comparison with measurement of other institutes' Websites.  
> **Method.** Website visibility elements were identified, and content investigation used to rank them according to an academic model. A scoring system was designed to cater for subjective measurements, producing a ranked list of university homepages. This was compared to an industry standard academic ranking for UK universities.  
> **Results and Analysis.** Five sub-lists provided a wide span of resulting scores, combined into the final result list. In some cases it was clear that homepages were designed based on good practice (Universities of Liverpool and Cambridge), while in others little or no effort was expended to achieve a high degree of visibility. There was no correlation between the two types of ranking.  
> **Conclusions.** Website visibility is a design feature often ignored in the design of university homepages in the UK, which could lead to missed opportunities. Guidelines are provided to improve this situation.

<section>

## Introduction

It is clear from both the industry and academic research that marketing is essential to push products and services out to the consumer. The use of Websites to achieve this has been a common tactic since the early days of the Internet. Lately, universities have been forced to change from being isolated academic enterprises to real-life businesses, competing for common pools of paying clients. Their Websites have played a central role in this effort to market a university. A university's Website is expected to be a flag-waving device for its reputation for teaching and/or research. In parallel with this, there has been a paradigm shift of focus onto social media, where the younger generation is highly aware of and dependent on technology for their social interactions. It is a combination of these factors which have caused university Websites to shift from operating in a traditional academic environment to the public realm. On this platform, students expect to be able to interact with their university through its Website and/or the Learning Management System on a daily basis.

The objective of this research was to measure the visibility of a sample of UK university Website homepages to search engine crawlers. The results would give an indication of not only how the universities compare to each other but also show the degree of optimisation having been done on these Web pages. This was considered necessary to combat the inherent user reluctance to read past the first search engine results page - Website visibility for university homepages is no longer a nice-to-have option, but a must-have.

The total number of UK universities could not be determined exactly. Various sources claim different figures and 150 was found to be an approximate average number. A sample had to be taken, and the well-known Russell and 1994 groupings were used to define this sample from the population. A total of 38 universities were thus found and used for the empirical experiments.

In this project the focus was on the homepages of the sample taken from this population of universities – specifically the ease (or lack thereof) with which search engine crawlers could index them, enabling users to later find them through search engines.

This background has provided the motivation for this research project. No empirical evidence could be found that a study of this kind has been done specifically on UK universities. The research problem is that no guidance exists on the current comparative status of top UK university homepage visibility, leading to possible lost opportunities.

## Background and other research

### Search engines

Search engines are well established in the Internet world, and have grown from garage-driven, primitive software startups to financially successful corporations employing thousands of experts. Google and Yahoo!, currently the two leaders in USA market share ([Adamo 2013](#ada13)) provide search results in a similar format – natural results occupy the bottom left corner on a result screen, and paid results the top and right hand side of the screen.

The latest search engine market share figures confirm that Google, Bing and Yahoo! are the undisputed market leaders in the USA ([Adamo 2013](#ada13)). Their combined size of the search market is 96.2%, leaving other search engines a miniscule portion of the market.

### Website visibility

Websites have a number of attributes, some more easily measurable than others. Usability comes to mind, as well as a large number of usage metrics including hits, bounce rate, page views and pages per visit. Website visibility is another metric which describes the ease (or lack thereof) with which a search engine crawler can index a given Web page, once found. This degree of visibility to a crawler is a combination of a large number of factors, as opposed to a single value on a linear scale. Some of these factors which could improve the visibility of a Website include the quality and quantity of inlinks, keyword usage in Web page body text and the use of metatags ([Weideman 2009](#wei09)). The implementation of these factors is sometimes collectively termed as whitehat search engine optimisation (SEO). Similarly, some blackhat SEO techniques also exist. These are used by Website designers in an attempt to cajole crawlers and algorithms to allocate a higher ranking to a Website than it deserves, based on the quality of its content. Website visibility can be achieved by applying either or both whitehat and blackhat SEO ([Malaga 2010](#mal10)). With blackhat, however, the chances exist that a search engine could blacklist a Website and remove it from its index, one of the worst fates of any Website.

### Crawler access to Websites

Much research has been done on Website visibility and access to these Websites by search engine crawlers. Oralalp ([2010](#ora10)) has investigated the Internet visibility of Turkish Websites and had difficulty in using only one attribute for measuring this feature. Four separate activities and some tasks were identified, which were needed to evaluate the visibility of a Website ([Espadas _et al._ 2008](#esp08)). Aguillo ([2009](#agu09)) used the phrase 'Institution footprint in the Web' as a way to define Website visibility, describing three indicators for this measurement: usage, impact and activity. One recent Website visibility model ([Sullivan 2011](#sul11)) appeared in non-academic literature, which categorised elements having an effect on Website visibility. However, no evidence could be found for the basis for the Sullivan model. No empirical data, interviews or a literature study supported its construction. It appeared to have been based on practical experience and insight only, hence is not used further in this project, even though some similarities between the two are evident.

Future research could investigate links between the Sullivan and the Weideman model. The Sullivan model does provide practical guidance on, for example, the role of off-page and on-page search engine optimisation elements ([Malaga 2010](#mal10)). The only model on Website visibility found, based on standard academic research and peer review, was the Weideman model ([Weideman 2009](#wei09)). This model identifies and ranks both positive and negative visibility elements. The most highly rated positive elements are: inlinks, body keywords and anchor text. The quantity and quality of inlinks (hyperlinks on other Websites) seemed to play a large role in creating more traffic ([Rathimala 2010](#rat10); [Weideman 2009](#wei09)). Other authors did a hyperlink analysis on seven Canadian faculty Websites, identifying clusters of visibility patterns on these Websites ([Yi _et al._ 2008](#yi08)). It seems that these scholars agree on the value of hyperlinks pointing to a Website being an indication of trust in its intrinsic value. Table 1 indicates the respective weights allocated to the various elements contributing to the visibility of a webpage, which have been used in this research.

<table><caption>

Table 1: The Website visibility element scoring ([Weideman 2009](#wei09))</caption>

<tbody>

<tr>

<th>Elements</th>

<th>Score</th>

</tr>

<tr>

<td>Inlinks</td>

<td>82.3</td>

</tr>

<tr>

<td>Body keywords</td>

<td>54</td>

</tr>

<tr>

<td>Hypertext/anchor text</td>

<td>32.8</td>

</tr>

<tr>

<td>Metatags</td>

<td>27.3</td>

</tr>

<tr>

<td>TITLE Tag</td>

<td>19.3</td>

</tr>

<tr>

<td>H1 Tag</td>

<td>17.1</td>

</tr>

<tr>

<td>Outlinks</td>

<td>15.9</td>

</tr>

<tr>

<td>i-Age of site</td>

<td>12.1</td>

</tr>

<tr>

<td>Domain names</td>

<td>9.1</td>

</tr>

<tr>

<td>b-Manual search engine submission</td>

<td>5.0</td>

</tr>

<tr>

<td>b-Paid inclusion service</td>

<td>5.0</td>

</tr>

<tr>

<td>b-Paid placement service</td>

<td>5.0</td>

</tr>

<tr>

<td>i-Age of document</td>

<td>5.0</td>

</tr>

<tr>

<td>HTML naming conventions</td>

<td>4.4</td>

</tr>

<tr>

<td>i-Age of link</td>

<td>2.9</td>

</tr>

<tr>

<td>i-Topical relationship of linking site</td>

<td>2.1</td>

</tr>

<tr>

<td>i-Relevance of site's primary subject matter to query</td>

<td>0.7</td>

</tr>

</tbody>

</table>

### UK universities

Universities are competing for common pools of paying clients. Authors have proved that universities in general have to be positioned in a global market to guarantee economic survival ([Nicolae & Marinescu 2010](#nic10)).

The UK Higher Education landscape has changed in the past decade or two, due to a number of mergers and name changes ([Redesigning the information landscape for higher education 2011](#red11); [Teeside University 2011](#tee11); [The Higher Education Academy 2011](#the11)). A group of United Kingdom (UK) universities formed a 'cluster of excellence' in 1994, based on the fact that between them they draw two-thirds of the contract funding and research grants in the UK ([1994 Group 2011](#gro11)) The Russell Group is a similar collection of universities, grouping themselves into another cluster of excellence.

A sample had to be taken to be used in this research and the combination of these two lists was taken as the sample for the project – see Table 2.

<table><caption>Table 2: List of the selected UK universities</caption>

<tbody>

<tr>

<th>Source</th>

<th>No</th>

<th>Code</th>

<th>Name</th>

<th>Domain</th>

</tr>

<tr>

<td>994-21</td>

<td>1</td>

<td>BA</td>

<td>University of Bath</td>

<td>

http://www.bath.ac.uk/</td>

</tr>

<tr>

<td>RUS-01</td>

<td>2</td>

<td>BI</td>

<td>University of Birmingham</td>

<td>

http://www.birmingham.ac.uk/</td>

</tr>

<tr>

<td>994-22</td>

<td>3</td>

<td>BK</td>

<td>Birkbeck, University of London</td>

<td>

http://www.bbk.ac.uk/</td>

</tr>

<tr>

<td>RUS-02</td>

<td>4</td>

<td>BR</td>

<td>University of Bristol</td>

<td>

http://www.bristol.ac.uk/</td>

</tr>

<tr>

<td>RUS-03</td>

<td>5</td>

<td>CA</td>

<td>University of Cambridge</td>

<td>

http://www.cam.ac.uk/</td>

</tr>

<tr>

<td>RUS-04</td>

<td>6</td>

<td>CF</td>

<td>Cardiff University</td>

<td>

http://www.cardiff.ac.uk/</td>

</tr>

<tr>

<td>RUS-19</td>

<td>7</td>

<td>CO</td>

<td>University College London</td>

<td>

http://www.ucl.ac.uk/</td>

</tr>

<tr>

<td>994-23</td>

<td>8</td>

<td>DU</td>

<td>Durham University</td>

<td>

http://www.dur.ac.uk/</td>

</tr>

<tr>

<td>994-24</td>

<td>9</td>

<td>EA</td>

<td>University of East Anglia</td>

<td>

http://www.uea.ac.uk/</td>

</tr>

<tr>

<td>RUS-05</td>

<td>10</td>

<td>ED</td>

<td>University of Edinburgh</td>

<td>

http://www.ed.ac.uk/</td>

</tr>

<tr>

<td>994-25</td>

<td>11</td>

<td>ES</td>

<td>University of Essex</td>

<td>

http://www.essex.ac.uk/</td>

</tr>

<tr>

<td>994-26</td>

<td>12</td>

<td>EX</td>

<td>University of Exeter</td>

<td>

http://www.exeter.ac.uk/</td>

</tr>

<tr>

<td>RUS-06</td>

<td>13</td>

<td>GL</td>

<td>University of Glasgow</td>

<td>

http://www.glasgow.ac.uk/</td>

</tr>

<tr>

<td>994-27</td>

<td>14</td>

<td>GO</td>

<td>Goldsmiths, University of London</td>

<td>

http://www.gold.ac.uk/</td>

</tr>

<tr>

<td>RUS-07</td>

<td>15</td>

<td>IC</td>

<td>Imperial College</td>

<td>

http://www3.imperial.ac.uk/</td>

</tr>

<tr>

<td>994-28</td>

<td>16</td>

<td>IE</td>

<td>Institute of Education, University of London</td>

<td>

http://www.ioe.ac.uk/</td>

</tr>

<tr>

<td>RUS-08</td>

<td>17</td>

<td>KC</td>

<td>King's College London</td>

<td>

http://www.kcl.ac.uk/</td>

</tr>

<tr>

<td>994-29</td>

<td>18</td>

<td>LA</td>

<td>Lancaster University</td>

<td>

http://www.lancs.ac.uk/</td>

</tr>

<tr>

<td>994-30</td>

<td>19</td>

<td>LR</td>

<td>University of Leicester</td>

<td>

http://www.le.ac.uk/</td>

</tr>

<tr>

<td>RUS-09</td>

<td>20</td>

<td>LE</td>

<td>University of Leeds</td>

<td>

http://www.leeds.ac.uk/</td>

</tr>

<tr>

<td>RUS-10</td>

<td>21</td>

<td>LI</td>

<td>University of Liverpool</td>

<td>

http://www.liv.ac.uk/</td>

</tr>

<tr>

<td>994-31</td>

<td>22</td>

<td>LO</td>

<td>Loughborough University</td>

<td>

http://www.lboro.ac.uk/</td>

</tr>

<tr>

<td>RUS-11</td>

<td>23</td>

<td>LS</td>

<td>London School of Economics</td>

<td>

http://www2.lse.ac.uk/</td>

</tr>

<tr>

<td>RUS-12</td>

<td>24</td>

<td>MA</td>

<td>University of Manchester</td>

<td>

http://www.manchester.ac.uk/</td>

</tr>

<tr>

<td>RUS-13</td>

<td>25</td>

<td>NE</td>

<td>Newcastle University</td>

<td>

http://www.ncl.ac.uk/</td>

</tr>

<tr>

<td>RUS-14</td>

<td>26</td>

<td>NO</td>

<td>University of Nottingham</td>

<td>

http://www.nottingham.ac.uk/</td>

</tr>

<tr>

<td>994-35</td>

<td>27</td>

<td>OA</td>

<td>School of Oriental and African Studies</td>

<td>

http://www.soas.ac.uk/</td>

</tr>

<tr>

<td>RUS-15</td>

<td>28</td>

<td>OX</td>

<td>University of Oxford</td>

<td>

http://www.ox.ac.uk/</td>

</tr>

<tr>

<td>994-32</td>

<td>29</td>

<td>QM</td>

<td>Queen Mary, University of London</td>

<td>

http://www.qmul.ac.uk/</td>

</tr>

<tr>

<td>RUS-16</td>

<td>30</td>

<td>QU</td>

<td>Queen's University Belfast</td>

<td>

http://www.qub.ac.uk/</td>

</tr>

<tr>

<td>994-33</td>

<td>31</td>

<td>RE</td>

<td>University of Reading</td>

<td>

http://www.reading.ac.uk/</td>

</tr>

<tr>

<td>994-34</td>

<td>32</td>

<td>SA</td>

<td>University of St Andrews</td>

<td>

http://www.st-andrews.ac.uk/</td>

</tr>

<tr>

<td>RUS-17</td>

<td>33</td>

<td>SH</td>

<td>University of Sheffield</td>

<td>

http://www.sheffield.ac.uk/</td>

</tr>

<tr>

<td>RUS-18</td>

<td>34</td>

<td>SO</td>

<td>University of Southampton</td>

<td>

http://www.southampton.ac.uk/</td>

</tr>

<tr>

<td>994-36</td>

<td>35</td>

<td>SU</td>

<td>University of Surrey</td>

<td>

http://www.surrey.ac.uk/</td>

</tr>

<tr>

<td>994-37</td>

<td>36</td>

<td>SX</td>

<td>University of Sussex</td>

<td>

http://www.sussex.ac.uk/</td>

</tr>

<tr>

<td>RUS-20</td>

<td>37</td>

<td>WA</td>

<td>University of Warwick</td>

<td>

http://www.warwick.ac.uk/</td>

</tr>

<tr>

<td>994-38</td>

<td>38</td>

<td>YO</td>

<td>University of York</td>

<td>

http://www.york.ac.uk/</td>

</tr>

</tbody>

</table>

### Academic university rankings

Many universities and other organisations are maintaining lists of university rankings based on academic value, which is different from Website visibility ranking ([Sugak 2011](#sug11)). One of these rating systems, for example, is [Academic Ranking of World Universities](http://www.arwu.org/). Recent research identifies these kinds of studies as belonging to the emerging field of Webometrics ([Thelwall 2010](#the10)). Some of these ranking sources are listed in Table 3\. Different lists produce different results, as can be expected since they use widely varying methods of measurement to arrive at their ranking tables. University attributes such as research outputs, student perception, size, student-to-staff ratios and others are measured and used in the ranking formulae. The academic ranking of universities worldwide has been widely publicised lately, with many subsets being touted: UK only universities, all-African university lists, European universities only and others.

<table><caption>Table 3: Academic university ranking sources</caption>

<tbody>

<tr>

<th>Name</th>

<th>Domain</th>

</tr>

<tr>

<td>Academic Ranking of World Universities (ARWU)</td>

<td>

http://www.arwu.org</td>

</tr>

<tr>

<td>QS World University Rankings® 2010/2011</td>

<td>

http://www.topuniversities.com/university-rankings/world-university-rankings/2010</td>

</tr>

<tr>

<td>Ranking Web of World Universities</td>

<td>

http://www.webometrics.info/top12000.asp</td>

</tr>

<tr>

<td>Times Higher Education World University Rankings</td>

<td>

http://www.timeshighereducation.co.uk/world-university-rankings/2010-2011/top-200.html</td>

</tr>

<tr>

<td>Wikipedia Ranking of World Universities</td>

<td>

http://en.wikipedia.org/wiki/Academic_Ranking_of_World_Universities</td>

</tr>

</tbody>

</table>

However, similar patterns do emerge when comparing some of these rankings. For example, Oxford and Cambridge universities often appear in the top two or three positions of the UK lists.

In contrast to these academic rankings however, this research focusses on the ranking of university Websites according to the visibility of their Website homepages. As noted earlier, the quantity and quality of hyperlinks pointing to these homepages play a major role in search engine rankings. Much research has been done on hyperlinks and the patterns they create.

During the early phase of Internet development, Middleton and others presented a model suggesting both structure and content for a typical university Website ([Middleton _et al._ 1999](#mid99)). It strongly promoted user-centred design, arguing that individual, group and institutional information needs should be identified to guide the design process. No research was done on the perception search engine crawlers had of these Websites. This is understandable, since the meteoric rise of search engine traffic only started around the time Google took Yahoo! on as leader in the search engine wars, around 2006\. Furthermore, a survey was done by the same authors of the UK Higher Education Websites at the time, listing the size of each homepage. The size of a Web page determines to a large extent the time it takes to download, and therefore it influences the user experience. Google started including download time as one of its ranking factors ([Cutts 2010](#cut10)). The role played by the size of a Web page in download time has diminished as broadband access has become more common.

It is claimed in a 2006 study that hyperlinks provide the most common structure on which Web studies are based ([Heimeriks _et al._ 2006](#hei06)). Amongst others the links between EU universities were graphed in this study and it was clear that many universities can be grouped into clusters, often based on home language. The UK, German and French universities showed a particularly strong interlinking structure. A similar pattern was evident inside a country; Dutch universities typically had more links to other Dutch universities than to those outside the country. Again this pattern was evident in social linking structures inside universities, between departments and humans.

The clustering of regional university links was confirmed in another study ([Thelwall _et al._ 2008](#the08)) where Microsoft search results were used. These authors found that some countries were poorly connected, while particularly the UK and Germany had well established linkage structures. This again confirmed the findings of Heimeriks.

A number of authors have made the claim that there is a strong correlation between the concept of trust and value implied by referencing a source, as used in search engine algorithms and the citation concept of the academic world ([Cronin 2001](#cro01); [Weideman 2009](#wei09)). A series of two research articles report on findings of interlinking between three different university departments in three different countries. The first determined that linking between departments is a good reflection of informal scholarly communication ([Li _et al._ 2005a](#lix05a)). The second builds on this citation/linking similarity and further investigates the link patterns formed by these interlinks ([Li _et al._ 2005b](#lix05b)). A difference in these patterns was noted, reflecting differences between disciplinary and national boundaries but still mirroring offline scholarly communication.

It is clear that hyperlink studies done on the interlinking been Websites play a large role in not only user perception of Website value or importance but also the value search engine algorithms allocate to domains. This fact is reflected in the high score given by search engines to inlinks when determining ranking of a Website on a search engine result page ([Weideman 2009](#wei09)).

A previous study identified a strong correlation between visibility and academic rankings ([Weideman 2011](#wei11)). It was proved that out of the top eight visibility scorers in South African universities, seven were also in the top eight academic rankings for the country. No reason could be found for the existence of this correlation. High visibility comes from a concerted effort on the design of the university Website, while high academic rankings stem from a variety of academically oriented factors. These could include a number of different elements such as the number of Nobel laureates and the research output produced by a university. This study also focused on determining if the same strong positive correlation existed between the two types of scores for top scoring UK universities.

## Methodology

The domain for each university from the sample list was taken from Internet sources and then checked for authenticity. It was inspected in terms of the sub-domains, news content, links pointed to, contact detail and logo to ensure that it was actually the official university Website. If any one of these elements indicated that it was not the official university Website, the process was repeated with another domain until the correct one was found.

The sub-motives for this research project included the demystifying of Website visibility, enabling the design team to include visibility as part of the design strategy. It was assumed that not all universities would have access to expensive specialist test programs to measure Website visibility. As a result, it was decided to use only freely accessible Web-based tools for measurements, to make this process repeatable. If any visibility elements could not be measured in this way, they would be omitted. The three test programs used are:

*   [Alexa](http://www.alexa.com),
*   [Grader](http://www.grader.com) and
*   [Ranks](http://www.ranks.nl)

[Alexa Internet](http://www.alexa.com) was founded in 1996 and attempted to provide intelligent Web navigation based on user feedback. This service currently provides free global metrics for competitive analysis, benchmarking, market research, or business development. The program gathers user information from those users who have downloaded their toolbar and provides developers with a set of tools with a wide scope of application. As with many online services, it has a free (limited) option and other paid plans with more powerful tools. Users can:  
- add traffic and review widgets to Websites by simply copying and pasting supplied HTML code,  
- view traffic and many other sets of statistics,  
- do a Website audit,  
- create and host a customised toolbar  
- register a Website with Alexa and a number of other features.  
Alexa was used in this research to provide the number of inlinks, i.e., hyperlinks from other Websites to the one in question. One of its more powerful features is the ability to 'Claim your site', i.e., register it with Alexa (after proving ownership). Owners of claimed sites can update title and description tags, and act on user reviews of the site.

The Grader program was designed to measure and analyse Website marketing efforts. It currently consists of a suite of four sets of programs, titled: [Marketing.Grader.com](http://marketing.grader.com), Twitter.Grader.com, Book.Grader.com and Search.Grader.com. The search function was used to provide automated feedback on the details of the university Websites' use of tags - title tag, description and heading respectively. It is possible to obtain this information manually from any live Website but updating it normally involves special access (typically through FTP) to the Website. This program automates this feature, saving the researcher much time and effort. Grader allows users to view keyword ranking positions, paid search status and monthly searches for top keywords.

Ranks is a program aimed at developers and other more technical users. It has a set of Webmaster tools which allows optimisers, for example, to check the status of various on-page factors. Again it has a free, limited version, with the option to pay and get access to more powerful tools. Some of these include:  
- Web page analyser,  
- search engine comparator,  
- social network checker,  
- file difference comparator,  
- link validator, etc.  
In this research, Ranks was used to check the frequency and density of body keywords, which gives an indication of the weight keywords and key phrases carry on a given Web page.

Based on a detailed inspection of these university Website homepages and on expertise in the use of the chosen programs, it became clear that these programs can provide some of the measurements required: see Figure 1.

<figure>

![Measurement program functions](../p599fig1.jpg)

<figcaption>Figure 1: Measurement program functions</figcaption>

</figure>

Because of the large volume of evaluative work involved, the first six elements identified in the Weideman model only will be measured and investigated. These six are:

*   Inlinks
*   Body keywords
*   Anchor text
*   Metatags
*   Title tag and
*   H1 tag

When adding their weights and comparing it to the total weight, these six (out of 17) contribute a weight of 232 out of 300.

The test programs were used in the following way:

*   Inlink counts - provided by Alexa
*   Keyword optimisation - provided by Ranks
*   Hypertext - not done
*   Description metatag - provided by Grader
*   Title tag - provided by Grader
*   H tags - provided by Grader

Other useful measurements provided by these programs but not used in this project include traffic rankings, readability levels, and others (see Figure 1).

It became clear that only one of the proposed measurements would provide a simple list of values which could be ranked by sorting, the inlink count. The others would produce data which is more subjective and other methods had to be found to classify and then rank them.

Figure 2 lists these methods.

*   The **position** of a university in a list is simply a number used to identify it. The list could be sorted alphabetically, for example, where the position is no indication of performance.
*   The **weight** is a value associated with an element of Website visibility, as allocated by the model on which this research is based. A higher weight indicates that an element has a higher positive influence on the visibility of a Web page
*   The **rank** of a university is the position it occupies in a list of universities as a result of its performance in a certain area. University P could be ranked 38, which implies that it has performed the best after a given measurement has been done.
*   In some cases an objective measurement was not possible, for example when comparing the value of different description metatags. In these cases, a **class** had to be created inside the rank, where universities whose measurements were similar could be grouped as being in the same class, effectively earning the same (averaged) rank. In all cases where universities were grouped in a class, the new averaged rank replaced the initial rank figure. In the example of Figure 2, universities T, U and V are in the same class (4), earning them a rank of (32+33+34)/3 = 33.
*   Finally, a **score** was calculated by multiplying each weight with the final rank of the university for that measurement.

<figure>

![Measurement methods](../p599fig2.jpg)

<figcaption>Figure 2: Measurement methods</figcaption>

</figure>

## Results

### Inlinks

The number of inlinks to a given Website has been described as playing a major role, by some as the most important one, in Website visibility ([Sullivan 2011](#sul11); [Weideman 2009](#wei09)). This element was considered first, and allocated the highest weight of 82.3 (see Figure 1).

It was not necessary to define classes, since the thirty-eight universities produced thirty-eight unique inlink counts. Thus the class and the rank had the same value, namely from 38 down to one. Alexa was used for this measurement; see Figure 3 for an example of how Alexa displays this value.

<figure>

![Example of inlink count from Alexa](../p599fig3.jpg)

<figcaption>Figure 3: Example of inlink count from Alexa</figcaption>

</figure>

The thirty-eight universities were sorted according to the highest number of inlinks, with the highest rank first, down to the 38th position. The rank was then allocated, with the university listed first achieving the highest rank (38). Finally, for each university the rank was multiplied by the weight to achieve the score. This system was used to ensure consistency across all the measurements, since there would always be a total of thirty-eight universities competing and the same table of weights as multiplying factor will be used. Table 4 contains the list with these rankings.

<table><caption>Table 4: Comparing number of university homepage inlinks</caption>

<tbody>

<tr>

<th>Pos</th>

<th>Code</th>

<th>Inlinks</th>

<th>Rank</th>

<th>Score</th>

<th></th>

<th>Pos</th>

<th>Code</th>

<th>Inlinks</th>

<th>Rank</th>

<th>Score</th>

</tr>

<tr>

<td>1</td>

<td>WA</td>

<td>4378</td>

<td>38</td>

<td>

**_3127_**</td>

<td style="background-color:rgb(30,30,30)"> </td>

<td>20</td>

<td>LA</td>

<td>2315</td>

<td>19</td>

<td>

**_1564_**</td>

</tr>

<tr>

<td>2</td>

<td>LS</td>

<td>4175</td>

<td>37</td>

<td>

**_3045_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>21</td>

<td>LO</td>

<td>2221</td>

<td>18</td>

<td>

**_1481_**</td>

</tr>

<tr>

<td>3</td>

<td>LE</td>

<td>4066</td>

<td>36</td>

<td>

**_2963_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>22</td>

<td>SA</td>

<td>2106</td>

<td>17</td>

<td>

**_1399_**</td>

</tr>

<tr>

<td>4</td>

<td>DU</td>

<td>3897</td>

<td>35</td>

<td>

**_2881_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>23</td>

<td>QU</td>

<td>2086</td>

<td>16</td>

<td>

**_1317_**</td>

</tr>

<tr>

<td>5</td>

<td>NO</td>

<td>3893</td>

<td>34</td>

<td>

**_2798_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>24</td>

<td>MA</td>

<td>1768</td>

<td>15</td>

<td>

**_1235_**</td>

</tr>

<tr>

<td>6</td>

<td>LI</td>

<td>3666</td>

<td>33</td>

<td>

**_2716_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>25</td>

<td>CF</td>

<td>1715</td>

<td>14</td>

<td>

**_1152_**</td>

</tr>

<tr>

<td>7</td>

<td>CA</td>

<td>3635</td>

<td>32</td>

<td>

**_2634_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>26</td>

<td>BR</td>

<td>1583</td>

<td>13</td>

<td>

**_1070_**</td>

</tr>

<tr>

<td>8</td>

<td>YO</td>

<td>3466</td>

<td>31</td>

<td>

**_2551_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>27</td>

<td>SU</td>

<td>1474</td>

<td>12</td>

<td>

**_988_**</td>

</tr>

<tr>

<td>9</td>

<td>OX</td>

<td>3465</td>

<td>30</td>

<td>

**_2469_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>28</td>

<td>OA</td>

<td>1438</td>

<td>11</td>

<td>

**_905_**</td>

</tr>

<tr>

<td>10</td>

<td>LR</td>

<td>3378</td>

<td>29</td>

<td>

**_2387_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>29</td>

<td>BK</td>

<td>1364</td>

<td>10</td>

<td>

**_823_**</td>

</tr>

<tr>

<td>11</td>

<td>BA</td>

<td>3220</td>

<td>28</td>

<td>

**_2304_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>30</td>

<td>RE</td>

<td>1316</td>

<td>9</td>

<td>

**_741_**</td>

</tr>

<tr>

<td>12</td>

<td>KC</td>

<td>3192</td>

<td>27</td>

<td>

**_2222_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>31</td>

<td>EX</td>

<td>875</td>

<td>8</td>

<td>

**_658_**</td>

</tr>

<tr>

<td>13</td>

<td>GL</td>

<td>3185</td>

<td>26</td>

<td>

**_2140_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>32</td>

<td>QM</td>

<td>728</td>

<td>7</td>

<td>

**_576_**</td>

</tr>

<tr>

<td>14</td>

<td>NE</td>

<td>3123</td>

<td>25</td>

<td>

**_2058_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>33</td>

<td>SH</td>

<td>686</td>

<td>6</td>

<td>

**_494_**</td>

</tr>

<tr>

<td>15</td>

<td>IC</td>

<td>2984</td>

<td>24</td>

<td>

**_1975_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>34</td>

<td>GO</td>

<td>644</td>

<td>5</td>

<td>

**_412_**</td>

</tr>

<tr>

<td>16</td>

<td>SX</td>

<td>2485</td>

<td>23</td>

<td>

**_1893_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>35</td>

<td>IE</td>

<td>540</td>

<td>4</td>

<td>

**_329_**</td>

</tr>

<tr>

<td>17</td>

<td>EA</td>

<td>2386</td>

<td>22</td>

<td>

**_1811_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>36</td>

<td>CO</td>

<td>315</td>

<td>3</td>

<td>

**_247_**</td>

</tr>

<tr>

<td>18</td>

<td>ES</td>

<td>2380</td>

<td>21</td>

<td>

**_1728_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>37</td>

<td>SO</td>

<td>310</td>

<td>2</td>

<td>

**_165_**</td>

</tr>

<tr>

<td>19</td>

<td>ED</td>

<td>2356</td>

<td>20</td>

<td>

**_1646_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>38</td>

<td>BI</td>

<td>171</td>

<td>1</td>

<td>

**_82_**</td>

</tr>

</tbody>

</table>

### Body keywords

Secondly, the use of body keywords in a Web page has been proven to be the second most important contributing factor to Website visibility ([Weideman 2009](#wei09)). However, this is a more subjective measure than mere counting as for inlinks, so the class system had to be used to group Web pages which are of equal quality in terms of body keyword usage. Ranks provides a keyword analysis tool, by graphing the frequency of keywords and key terms used on the target Web page. See Figure 4 for an example of how Ranks measures and displays these values.

<figure>

![Example of how Grader displays keyword usage](../p599fig4.jpg)

<figcaption>Figure 4: Example of how Grader displays keyword usage</figcaption>

</figure>

Next, the universities were ranked on a scale, using the classes, based on how relevant the keyword optimisation was done (knowingly or unknowingly) on each homepage. An assumption was made that prospective students would be looking for universities by name, and the classes were created accordingly.

*   Class 1 is the most relevant keyword grouping, down to Class 5 being the least relevant.
*   Class 1: First keyword/phrase is the full university name in separate keywords (the official spelling was taken to be the name on the university homepage).
*   Class 2: First keyword/phrase is not the full university name in separate keywords, second keyword/phrase is the full university name in separate keywords.
*   Class 3: First and second keyword/phrase combined is the full university name in separate keywords.
*   Class 4: None of Class 1, 2 or 3 contains parts of the name, but the university name appears in the first five keywords or key phrases, other keywords or key phrases are descriptive of a university.
*   Class 5: University name not used in first five keywords or key phrases but other related terms are present.

This second visibility element has a weight of 54.0, according to Figure 1\. This weight was used to create the final score for each university, where the rank times the weight equals the score. Where more than one university occupies a class, the average for that class was allocated. For example, from Table 5, the first seven universities are in the same class and therefore have the same rank (averaged). The next group of 11 again were all in Class 2, and had the same score.

<table><caption>Table 5: Comparing keyword optimisation inside university homepages</caption>

<tbody>

<tr>

<th>Pos</th>

<th>Code</th>

<th>Rank</th>

<th>Score</th>

<th style="background-color:rgb(30,30,30)"></th>

<th>Pos</th>

<th>Code</th>

<th>Rank</th>

<th>Score</th>

</tr>

<tr>

<td>1</td>

<td>CF</td>

<td>35</td>

<td>

**_1890_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>20</td>

<td>BA</td>

<td>16</td>

<td>

**_864_**</td>

</tr>

<tr>

<td>2</td>

<td>IE</td>

<td>35</td>

<td>

**_1890_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>21</td>

<td>BR</td>

<td>16</td>

<td>

**_864_**</td>

</tr>

<tr>

<td>3</td>

<td>LO</td>

<td>35</td>

<td>

**_1890_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>22</td>

<td>EA</td>

<td>16</td>

<td>

**_864_**</td>

</tr>

<tr>

<td>4</td>

<td>NE</td>

<td>35</td>

<td>

**_1890_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>23</td>

<td>LE</td>

<td>16</td>

<td>

**_864_**</td>

</tr>

<tr>

<td>5</td>

<td>QU</td>

<td>35</td>

<td>

**_1890_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>24</td>

<td>NO</td>

<td>16</td>

<td>

**_864_**</td>

</tr>

<tr>

<td>6</td>

<td>ES</td>

<td>35</td>

<td>

**_1890_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>25</td>

<td>OX</td>

<td>16</td>

<td>

**_864_**</td>

</tr>

<tr>

<td>7</td>

<td>LI</td>

<td>35</td>

<td>

**_1890_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>26</td>

<td>SH</td>

<td>16</td>

<td>

**_864_**</td>

</tr>

<tr>

<td>8</td>

<td>DU</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>27</td>

<td>WA</td>

<td>16</td>

<td>

**_864_**</td>

</tr>

<tr>

<td>9</td>

<td>LA</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>28</td>

<td>BK</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>10</td>

<td>SU</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>29</td>

<td>IC</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>11</td>

<td>BI</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>30</td>

<td>KC</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>12</td>

<td>CA</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>31</td>

<td>QM</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>13</td>

<td>ED</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>32</td>

<td>OA</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>14</td>

<td>GL</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>33</td>

<td>CO</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>15</td>

<td>LR</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>34</td>

<td>EX</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>16</td>

<td>MA</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>35</td>

<td>RE</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>17</td>

<td>SO</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>36</td>

<td>SA</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>18</td>

<td>SX</td>

<td>26</td>

<td>

**_1404_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>37</td>

<td>YO</td>

<td>7</td>

<td>

**_351_**</td>

</tr>

<tr>

<td>19</td>

<td>GO</td>

<td>16</td>

<td>

**_864_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>38</td>

<td>LS</td>

<td>1</td>

<td>

**_54_**</td>

</tr>

</tbody>

</table>

### Anchor text

During the experiments it was noted that anchor text could not be identified, graded or listed by any one of the defined tools. As a result, this feature was not measured and its scoring omitted from this research.

### Description metatag

The fourth most important element identified in earlier research was the description metatag. This is an optional textual Web page component which the browser does not display, except if the user specifically requests it through the menus. Most search engines will display the first part of this metatag on their result pages. It is supposed to contain a keyword-rich description of the Website's content, to allow crawlers to evaluate a Website and users to scan it for relevance. The Grader tool extracted this metatag, allowing further inspection and evaluation. See Figure 5 for an example of how Grader displays this detail.

<figure>

![Example of how Grader displays the description metatag](../p599fig5.jpg)

<figcaption>Figure 5: Example of how Grader displays the description metatag</figcaption>

</figure>

A ranking was again done on the 38 universities, based on the relevance of their homepage description metatags. In general, best practice prescribes that this metatag should be one or more full sentences, keyword rich and include the most important keywords towards the start. A class definition was also necessary, since some university homepages had different but similar tags in terms of value. This element had a weight of 27.3 (from Table 1), hence the score was calculated by multiplying the weight by the rank.

*   Class 1: Multiple sentence, keyword rich, well written, strong university related.
*   Class 2: Multiple sentence, university related, some relevant keywords.
*   Class 3: Single sentence, university related, some relevant keywords.
*   Class 4: Short phrase, few relevant keywords.
*   Class 5: No relevant keywords.
*   Class 6: No metatag.

The resultant scores are listed in Table 6.

<table><caption>Table 6: Comparison of description metatag usage in university homepages</caption>

<tbody>

<tr>

<th>Pos</th>

<th>Code</th>

<th>Rank</th>

<th>Score</th>

<th style="background-color:rgb(30,30,30)"></th>

<th>Pos</th>

<th>Code</th>

<th>Rank</th>

<th>Score</th>

</tr>

<tr>

<td>1</td>

<td>BI</td>

<td>37</td>

<td>

**_1010_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>20</td>

<td>CA</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>2</td>

<td>ED</td>

<td>37</td>

<td>

**_1010_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>21</td>

<td>EA</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>3</td>

<td>GL</td>

<td>37</td>

<td>

**_1010_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>22</td>

<td>ES</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>4</td>

<td>IE</td>

<td>34</td>

<td>

**_928_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>23</td>

<td>LR</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>5</td>

<td>OA</td>

<td>34</td>

<td>

**_928_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>24</td>

<td>OX</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>6</td>

<td>SX</td>

<td>34</td>

<td>

**_928_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>25</td>

<td>SO</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>7</td>

<td>BK</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>26</td>

<td>SA</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>8</td>

<td>CF</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>27</td>

<td>WA</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>9</td>

<td>IC</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>28</td>

<td>YO</td>

<td>17</td>

<td>

**_464_**</td>

</tr>

<tr>

<td>10</td>

<td>KC</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>29</td>

<td>QU</td>

<td>10</td>

<td>

**_273_**</td>

</tr>

<tr>

<td>11</td>

<td>LO</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>30</td>

<td>DU</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

<tr>

<td>12</td>

<td>QM</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>31</td>

<td>LS</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

<tr>

<td>13</td>

<td>BA</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>32</td>

<td>NE</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

<tr>

<td>14</td>

<td>LI</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>33</td>

<td>SU</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

<tr>

<td>15</td>

<td>SH</td>

<td>28</td>

<td>

**_764_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>34</td>

<td>EX</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

<tr>

<td>16</td>

<td>GO</td>

<td>17</td>

<td>

**_464_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>35</td>

<td>LE</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

<tr>

<td>17</td>

<td>LA</td>

<td>17</td>

<td>

**_464_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>36</td>

<td>MA</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

<tr>

<td>18</td>

<td>CO</td>

<td>17</td>

<td>

**_464_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>37</td>

<td>NO</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

<tr>

<td>19</td>

<td>BR</td>

<td>17</td>

<td>

**_464_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>38</td>

<td>RE</td>

<td>5</td>

<td>

**_137_**</td>

</tr>

</tbody>

</table>

### Title tag

The fifth visibility element to be considered was the content of the title tag. It differs from the description metatag in that search engines display fewer of its characters but it is also optional, invisible to the browser and it is displayed on search engine result pages as the title of each result. Most browsers also display the content of this title tag – see Figure 6\. According to prior research this element has a weight of 19.3 (see Figure 1).

<figure>

![Example of how the TITLE tag is displayed by a browser](../p599fig6.jpg)

<figcaption>Figure 6: Example of how the TITLE tag is displayed by a browser</figcaption>

</figure>

Similar to the previous steps, a ranking scale was developed (38 down to one), and again Classes had to be created to group similar entries.

*   Class 1: Starts with full university name, plus other highly relevant keywords.
*   Class 2: Full university name mixed with other relevant terms.
*   Class 3: Only full university name.
*   Class 4: University full name first, plus no-value terms (e.g., Welcome, Homepage.
*   Class 5: Starts with no-value words.

A summary of the scores earned by the homepages for the title tag is given in Table 7.

<table><caption>Table 7: Comparing use of the title tag inside university homepages</caption>

<tbody>

<tr>

<th>Pos</th>

<th>Code</th>

<th>Rank</th>

<th>Score</th>

<th style="background-color:rgb(30,30,30)"></th>

<th>Pos</th>

<th>Code</th>

<th>Rank</th>

<th>Score</th>

</tr>

<tr>

<td>1</td>

<td>RE</td>

<td>38</td>

<td>

**_733_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>20</td>

<td>LE</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>2</td>

<td>BK</td>

<td>35</td>

<td>

**_666_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>21</td>

<td>LR</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>3</td>

<td>NE</td>

<td>35</td>

<td>

**_666_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>22</td>

<td>LI</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>4</td>

<td>CO</td>

<td>35</td>

<td>

**_666_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>23</td>

<td>MA</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>5</td>

<td>BR</td>

<td>35</td>

<td>

**_666_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>24</td>

<td>NO</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>6</td>

<td>GL</td>

<td>35</td>

<td>

**_666_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>25</td>

<td>SH</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>7</td>

<td>SA</td>

<td>35</td>

<td>

**_666_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>26</td>

<td>SO</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>8</td>

<td>DU</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>27</td>

<td>SX</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>9</td>

<td>IC</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>28</td>

<td>YO</td>

<td>21</td>

<td>

**_405_**</td>

</tr>

<tr>

<td>10</td>

<td>IE</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>29</td>

<td>KC</td>

<td>9</td>

<td>

**_164_**</td>

</tr>

<tr>

<td>11</td>

<td>LO</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>30</td>

<td>LS</td>

<td>9</td>

<td>

**_164_**</td>

</tr>

<tr>

<td>12</td>

<td>QM</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>31</td>

<td>QU</td>

<td>9</td>

<td>

**_164_**</td>

</tr>

<tr>

<td>13</td>

<td>OA</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>32</td>

<td>ES</td>

<td>9</td>

<td>

**_164_**</td>

</tr>

<tr>

<td>14</td>

<td>SU</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>33</td>

<td>CF</td>

<td>4</td>

<td>

**_68_**</td>

</tr>

<tr>

<td>15</td>

<td>BA</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>34</td>

<td>GO</td>

<td>4</td>

<td>

**_68_**</td>

</tr>

<tr>

<td>16</td>

<td>BI</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>35</td>

<td>LA</td>

<td>4</td>

<td>

**_68_**</td>

</tr>

<tr>

<td>17</td>

<td>CA</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>36</td>

<td>EA</td>

<td>4</td>

<td>

**_68_**</td>

</tr>

<tr>

<td>18</td>

<td>ED</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>37</td>

<td>OX</td>

<td>4</td>

<td>

**_68_**</td>

</tr>

<tr>

<td>19</td>

<td>EX</td>

<td>21</td>

<td>

**_405_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>38</td>

<td>WA</td>

<td>4</td>

<td>

**_68_**</td>

</tr>

</tbody>

</table>

### Heading tags

Finally, the H1 tags (with a weight of 17.1) of each homepage were considered. Again the (Heading) tag is an optional mechanism which browsers use to display headings or other important sections in a text environment slightly larger and bold against their surroundings. This is similar to the way larger fonts and boldfacing is used in word processing to emphasise text. Various levels are available to the coder: H1 is the highest (biggest font), down to H6 being the smallest. Apart from highlighting text to the human eye by using H tags, the crawler also attaches more value to an H1 tag than to other levels, or to ordinary text. Again best practice prescribes that only one H1 should be used per page, keywords should be included and some other lower level H-tags should be present. See Figure 7 for an example of how Grader displays a Web page's H tags.

<figure>

![Example of how the TITLE tag is displayed by a browser](../p599fig7.jpg)

<figcaption>Figure 7: Example of how Grader displays H tags</figcaption>

</figure>

A ranking scale was developed (38 down to one), and again classes had to be created to group similar entries.

*   Class 1: One H1, very descriptive, some H2 and H3.
*   Class 2: One H1, descriptive, some other Hs.
*   Class 3: One H1, some H2 and/or H3.
*   Class 4: One H1.
*   Class 5: No H1, some H2 and H3.
*   Class 6: Multiple H1 OR no Hs OR Hs present but no-value content.

A summary of the scores earned by the homepages for the H tags is given in Table 8.

<table><caption>Table 8: Comparing use of the H (heading) tag inside university homepages</caption>

<tbody>

<tr>

<th>Pos</th>

<th>Code</th>

<th>Rank</th>

<th>Score</th>

<th style="background-color:rgb(30,30,30)"></th>

<th>Pos</th>

<th>Code</th>

<th>Rank</th>

<th>Score</th>

</tr>

<tr>

<td>1</td>

<td>BK</td>

<td>38</td>

<td>

**_650_**</td>

<th style="background-color:rgb(30,30,30)"> </th>

<td>20</td>

<td>CF</td>

<td>18</td>

<td>

**_299_**</td>

</tr>

<tr>

<td>2</td>

<td>OA</td>

<td>37</td>

<td>

**_624_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>21</td>

<td>DU</td>

<td>18</td>

<td>

**_299_**</td>

</tr>

<tr>

<td>3</td>

<td>NO</td>

<td>37</td>

<td>

**_624_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>22</td>

<td>LS</td>

<td>18</td>

<td>

**_299_**</td>

</tr>

<tr>

<td>4</td>

<td>GO</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>23</td>

<td>NE</td>

<td>18</td>

<td>

**_299_**</td>

</tr>

<tr>

<td>5</td>

<td>IC</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>24</td>

<td>CO</td>

<td>13</td>

<td>

**_214_**</td>

</tr>

<tr>

<td>6</td>

<td>IE</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>25</td>

<td>BA</td>

<td>13</td>

<td>

**_214_**</td>

</tr>

<tr>

<td>7</td>

<td>KC</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>26</td>

<td>BR</td>

<td>13</td>

<td>

**_214_**</td>

</tr>

<tr>

<td>8</td>

<td>LA</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>27</td>

<td>ED</td>

<td>13</td>

<td>

**_214_**</td>

</tr>

<tr>

<td>9</td>

<td>LO</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>28</td>

<td>LR</td>

<td>13</td>

<td>

**_214_**</td>

</tr>

<tr>

<td>10</td>

<td>QU</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>29</td>

<td>YO</td>

<td>13</td>

<td>

**_214_**</td>

</tr>

<tr>

<td>11</td>

<td>SU</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>30</td>

<td>QM</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

<tr>

<td>12</td>

<td>BI</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>31</td>

<td>ES</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

<tr>

<td>13</td>

<td>CA</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>32</td>

<td>EX</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

<tr>

<td>14</td>

<td>EA</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>33</td>

<td>GL</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

<tr>

<td>15</td>

<td>LI</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>34</td>

<td>LE</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

<tr>

<td>16</td>

<td>MA</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>35</td>

<td>OX</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

<tr>

<td>17</td>

<td>SO</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>36</td>

<td>RE</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

<tr>

<td>18</td>

<td>SA</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>37</td>

<td>SH</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

<tr>

<td>19</td>

<td>WA</td>

<td>28</td>

<td>

**_470_**</td>

<th style="background-color:rgb(30,30,30)"></th>

<td>38</td>

<td>SX</td>

<td>5</td>

<td>

**_86_**</td>

</tr>

</tbody>

</table>

### Cumulative scores

The cumulative total of all university homepage scores was then calculated. This was the total of all five score columns determined in the previous sections; see Table 9\. A higher degree of Website visibility for a homepage is indicated by a higher score.

<table><caption>Table 9: Final scores for UK universities</caption>

<tbody>

<tr>

<th>Pos</th>

<th>Code</th>

<th>University</th>

<th>Total score</th>

</tr>

<tr>

<td>1</td>

<td>LI</td>

<td>University of Liverpool</td>

<td>

**_6246_**</td>

</tr>

<tr>

<td>2</td>

<td>CA</td>

<td>University of Cambridge</td>

<td>

**_5377_**</td>

</tr>

<tr>

<td>3</td>

<td>GL</td>

<td>University of Glasgow</td>

<td>

**_5305_**</td>

</tr>

<tr>

<td>4</td>

<td>DU</td>

<td>Durham University</td>

<td>

**_5126_**</td>

</tr>

<tr>

<td>5</td>

<td>NE</td>

<td>Newcastle University</td>

<td>

**_5049_**</td>

</tr>

<tr>

<td>6</td>

<td>LO</td>

<td>Loughborough University</td>

<td>

**_5011_**</td>

</tr>

<tr>

<td>7</td>

<td>WA</td>

<td>University of Warwick</td>

<td>

**_4993_**</td>

</tr>

<tr>

<td>8</td>

<td>LR</td>

<td>University of Leicester</td>

<td>

**_4874_**</td>

</tr>

<tr>

<td>9</td>

<td>NO</td>

<td>University of Nottingham</td>

<td>

**_4828_**</td>

</tr>

<tr>

<td>10</td>

<td>SX</td>

<td>University of Sussex</td>

<td>

**_4716_**</td>

</tr>

<tr>

<td>11</td>

<td>ED</td>

<td>University of Edinburgh</td>

<td>

**_4679_**</td>

</tr>

<tr>

<td>12</td>

<td>BA</td>

<td>University of Bath</td>

<td>

**_4552_**</td>

</tr>

<tr>

<td>13</td>

<td>LE</td>

<td>University of Leeds</td>

<td>

**_4454_**</td>

</tr>

<tr>

<td>14</td>

<td>ES</td>

<td>University of Essex</td>

<td>

**_4332_**</td>

</tr>

<tr>

<td>15</td>

<td>CF</td>

<td>Cardiff University</td>

<td>

**_4173_**</td>

</tr>

<tr>

<td>16</td>

<td>QU</td>

<td>Queen's University Belfast</td>

<td>

**_4114_**</td>

</tr>

<tr>

<td>17</td>

<td>IE</td>

<td>Institute of Education, University of London</td>

<td>

**_4023_**</td>

</tr>

<tr>

<td>18</td>

<td>YO</td>

<td>University of York</td>

<td>

**_3985_**</td>

</tr>

<tr>

<td>19</td>

<td>KC</td>

<td>King's College London</td>

<td>

**_3972_**</td>

</tr>

<tr>

<td>20</td>

<td>LA</td>

<td>Lancaster University</td>

<td>

**_3970_**</td>

</tr>

<tr>

<td>21</td>

<td>IC</td>

<td>Imperial College</td>

<td>

**_3966_**</td>

</tr>

<tr>

<td>22</td>

<td>OX</td>

<td>University of Oxford</td>

<td>

**_3950_**</td>

</tr>

<tr>

<td>23</td>

<td>LS</td>

<td>London School of Economics</td>

<td>

**_3699_**</td>

</tr>

<tr>

<td>24</td>

<td>EA</td>

<td>University of East Anglia</td>

<td>

**_3677_**</td>

</tr>

<tr>

<td>25</td>

<td>MA</td>

<td>University of Manchester</td>

<td>

**_3651_**</td>

</tr>

<tr>

<td>26</td>

<td>SU</td>

<td>University of Surrey</td>

<td>

**_3404_**</td>

</tr>

<tr>

<td>27</td>

<td>BI</td>

<td>University of Birmingham</td>

<td>

**_3372_**</td>

</tr>

<tr>

<td>28</td>

<td>SA</td>

<td>University of St Andrews</td>

<td>

**_3350_**</td>

</tr>

<tr>

<td>29</td>

<td>BR</td>

<td>University of Bristol</td>

<td>

**_3278_**</td>

</tr>

<tr>

<td>30</td>

<td>BK</td>

<td>Birkbeck, University of London</td>

<td>

**_3254_**</td>

</tr>

<tr>

<td>31</td>

<td>OA</td>

<td>School of Oriental and African Studies</td>

<td>

**_3214_**</td>

</tr>

<tr>

<td>32</td>

<td>SO</td>

<td>University of Southampton</td>

<td>

**_2908_**</td>

</tr>

<tr>

<td>33</td>

<td>SH</td>

<td>University of Sheffield</td>

<td>

**_2613_**</td>

</tr>

<tr>

<td>34</td>

<td>GO</td>

<td>Goldsmiths, University of London</td>

<td>

**_2277_**</td>

</tr>

<tr>

<td>35</td>

<td>QM</td>

<td>Queen Mary, University of London</td>

<td>

**_2182_**</td>

</tr>

<tr>

<td>36</td>

<td>RE</td>

<td>University of Reading</td>

<td>

**_2047_**</td>

</tr>

<tr>

<td>37</td>

<td>CO</td>

<td>University College London</td>

<td>

**_1942_**</td>

</tr>

<tr>

<td>38</td>

<td>EX</td>

<td>University of Exeter</td>

<td>

**_1637_**</td>

</tr>

</tbody>

</table>

The minimum score any university could obtain was calculated as follows: the lowest rank possible is 1, and totalling the weights of the five elements considered yields: 82.3 + 54 + 27.3 + 19.3 + 17.1 = 200\. The maximum score possible would be the sum of the individual weights (200) multiplied by the number of top scores achieved (38), giving a theoretical maximum of 7600.

## Discussion and conclusion

### Visibility result interpretation

From Table 9 the calculated versus actual minimum and maximum scores are: 200 vs 1637 and 7600 vs 6246\. These figures seem to indicate that the top-scoring university's score was not far below the maximum, while the bottom scorer was well above the minimum possible. The highest scorer had a score of 3.8 times that of the lowest one. This is in stark contrast to the results of a similar study done on South African universities. Here the highest scorer out of 23 universities was 8.4 times better than the lowest one ([Weideman 2011](#wei11)).

When comparing the final results with those obtained during the phases of measurement, the two highest scorers overall exhibited an interesting pattern. Neither of them ever featured in the top five during the five measurements. However, both scored consistently in the top half where it mattered most, namely where the weighting of the visibility factors was high. On the inlink measurement (highest weight), Liverpool and Cambridge were 6<sup>th</sup> and 7<sup>th</sup>, on the second highest weight (keywords) they were 7<sup>th</sup> and 12<sup>th</sup>, and on the fourth highest (description metatag) they scored 14<sup>th</sup> and 20<sup>th</sup>. This indicates that it is relatively easy to improve a university's Website visibility ranking, compared to increasing its academic ranking, for example. The focus has to be on the few highest scoring elements, to ensure that the harvest on the heavier weighted elements is richer.

The results also indicate that there is a surprisingly wide spread on the scale of scores. It has to be accepted that older domains (correlating to older universities) have a higher trust value with search engines, and have had more time and opportunities to harvest inlinks (highest weight) from outside sources. However, any Website owner should have at least the homepage content rewritten by a technical copywriter, to enhance the keyword weighting (second highest weight) for its specific target market. Combined with the relative ease of writing a good description metatag (4<sup>th</sup> highest weight), title tag (5<sup>th</sup> highest weight) and H tags (6<sup>th</sup> highest weight), the lack of a high number of inlinks can be offset by these other elements.

Some of the results exposed some gross ignorance of best practice in Website visibility design. Considering only the easy-to-do changes (metatags and H tags), the following were serious omissions:

*   The 10 Class 6 occupants (more than 25% of the total) of the description metatag group had no entry here at all.
*   The 10 Class 5 and 4 occupants (more than 25% of the total) of the title tag group contained no-value terms such as 'Home' or 'Web page'
*   There were fifteen occupants of the lower classes in the H tag group, where even the most basic guidelines were ignored.

It should be noted that any user can view the source code of most university homepages. In this way one can learn from the leaders and translate their good ideas, well-written text blocks and metatags into relevant text for one's own homepage.

### Academic ranking result interpretation

For the purposes of this research, the 2010/2011 Times Higher Education rankings were used in the comparison. Only the top thirty-one universities from the UK, Ireland, Scotland and Wales are listed here: see Table 10.

<table><caption>Table 10: Times academic rankings</caption>

<tbody>

<tr>

<th>Pos</th>

<th>Region Rank</th>

<th>University</th>

<th>Overall score</th>

</tr>

<tr>

<td>1</td>

<td>1</td>

<td>University of Cambridge</td>

<td>

**_91.2_**</td>

</tr>

<tr>

<td>2</td>

<td>1</td>

<td>University of Oxford</td>

<td>

**_91.2_**</td>

</tr>

<tr>

<td>3</td>

<td>3</td>

<td>Imperial College London</td>

<td>

**_90.6_**</td>

</tr>

<tr>

<td>4</td>

<td>5</td>

<td>University College London</td>

<td>

**_78.4_**</td>

</tr>

<tr>

<td>5</td>

<td>7</td>

<td>University of Edinburgh</td>

<td>

**_69.2_**</td>

</tr>

<tr>

<td>6</td>

<td>13</td>

<td>University of Bristol</td>

<td>

**_61.4_**</td>

</tr>

<tr>

<td>7</td>

<td>14</td>

<td>Trinity College Dublin</td>

<td>

**_60.3_**</td>

</tr>

<tr>

<td>8</td>

<td>15</td>

<td>King's College London</td>

<td>

**_59.7_**</td>

</tr>

<tr>

<td>9</td>

<td>16</td>

<td>University of Sussex</td>

<td>

**_59.5_**</td>

</tr>

<tr>

<td>10</td>

<td>17</td>

<td>University of York</td>

<td>

**_59.1_**</td>

</tr>

<tr>

<td>11</td>

<td>19</td>

<td>Durham University</td>

<td>

**_58.9_**</td>

</tr>

<tr>

<td>12</td>

<td>20</td>

<td>London School of Economics and Political Science</td>

<td>

**_58.3_**</td>

</tr>

<tr>

<td>13</td>

<td>21</td>

<td>University of Manchester</td>

<td>

**_58_**</td>

</tr>

<tr>

<td>14</td>

<td>22</td>

<td>Royal Holloway, University of London</td>

<td>

**_57.9_**</td>

</tr>

<tr>

<td>15</td>

<td>24</td>

<td>University of Southampton</td>

<td>

**_57.7_**</td>

</tr>

<tr>

<td>16</td>

<td>26</td>

<td>University College Dublin</td>

<td>

**_57.5_**</td>

</tr>

<tr>

<td>17</td>

<td>31</td>

<td>University of St. Andrews</td>

<td>

**_56.5_**</td>

</tr>

<tr>

<td>18</td>

<td>35</td>

<td>Queen Mary, University of London</td>

<td>

**_54.6_**</td>

</tr>

<tr>

<td>19</td>

<td>37</td>

<td>Lancaster University</td>

<td>

**_54.4_**</td>

</tr>

<tr>

<td>20</td>

<td>40</td>

<td>University of Glasgow</td>

<td>

**_54.2_**</td>

</tr>

<tr>

<td>21</td>

<td>45</td>

<td>University of Sheffield</td>

<td>

**_52.5_**</td>

</tr>

<tr>

<td>22</td>

<td>47</td>

<td>University of Dundee</td>

<td>

**_52.2_**</td>

</tr>

<tr>

<td>23</td>

<td>52</td>

<td>University of Birmingham</td>

<td>

**_51.8_**</td>

</tr>

<tr>

<td>24</td>

<td>54</td>

<td>University of Aberdeen</td>

<td>

**_51.4_**</td>

</tr>

<tr>

<td>25</td>

<td>56</td>

<td>Birkbeck, University of London</td>

<td>

**_51.2_**</td>

</tr>

<tr>

<td>26</td>

<td>56</td>

<td>Newcastle University</td>

<td>

**_51.2_**</td>

</tr>

<tr>

<td>27</td>

<td>60</td>

<td>University of Liverpool</td>

<td>

**_50_**</td>

</tr>

<tr>

<td>28</td>

<td>63</td>

<td>University of Leeds</td>

<td>

**_49.8_**</td>

</tr>

<tr>

<td>29</td>

<td>68</td>

<td>University of East Anglia</td>

<td>

**_49_**</td>

</tr>

<tr>

<td>30</td>

<td>68</td>

<td>University of Nottingham</td>

<td>

**_49_**</td>

</tr>

<tr>

<td>31</td>

<td>74</td>

<td>University of Exeter</td>

<td>

**_47.6_**</td>

</tr>

</tbody>

</table>

An attempt was made to determine whether or not there was a statistical correlation between the Website visibility rankings and the academic rankings of the top thirty-one UK universities. A Pearson correlation was initially considered but rejected because of its reliance on a normal distribution, which rankings do not present.

Finally, a Spearman (raw) test was done, which produced a p-value of 0.942 (see Table 11), thereby proving that there is no significant correlation between the two rankings and any apparent correlation is merely coincidental.

<table><caption>Table 11: Spearman correlation between visibility and academic ranking</caption>

<tbody>

<tr>

<th colspan="3">Spearman's rho Correlations</th>

</tr>

<tr>

<td colspan="2"></td>

<td>Academic ranking</td>

</tr>

<tr>

<td rowspan="3">Web visibility ranking</td>

<td>Correlation coeffcient</td>

<td>-0.017</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td>0.942</td>

</tr>

<tr>

<td>N</td>

<td>21</td>

</tr>

</tbody>

</table>

In conclusion it can be claimed that there is no correlation between academic and Website visibility rankings for UK universities. The author did not expect a strong correlation, since the factors used to determine them have no apparent link. Academic rankings are based on various elements of academic excellence, while visibility rankings are based on the application of good practice Web design for visibility. The UK results are according to the expectation.

These findings are in stark contrast to the study done on South African university Websites ([Weideman 2011](#wei11)). The South African results are against the expectation and are considered to be an exception. The reasons for this difference in correlation between UK and South African university Website rankings are not immediately clear. One speculative answer is that it could be the result of the age difference between the two sets of universities. The United Kingdom as a country is much older than South Africa (only 361 years old in 2013). Secondly, some of the younger, and recently merged, South African universities are still hampered by the country's past of racial segregation, and of being deprived of basic resources. Expertise in Website design is one of these resources. Thirdly, the sample sizes for the two studies differ: twenty-three out of a population of twenty-three for South Africa, versus thirty-eight out of approximately 150 for the UK. Finally, another possible reason for this difference is the attitude of the staff members of the respective universities from the two countries. The UK is an established first world country, which has achieved much over centuries, including academic excellence. South Africa, being relatively young, still has to establish itself in many areas and a spirit of competition is evident on many levels of society. It could be that some South African academics automatically strive towards more than one type of achievement, both academic and technical in this case. UK university staff could be more complacent, where academics focus more strongly on excelling in their own sphere, rather than including other areas as well. Even though academics normally do not design university Web pages (typically done by technical staff), they often have to provide content to populate these Web pages. However, this is pure speculation and detailed research on the last concept falls outside the scope of this study. Further research on the technical issues at stake would be required to investigate elements of this phenomenon.

Another expansion of this research project could include the use of professional programs to do the measurement of the visibility of homepages. A new approach would be to obtain the keywords and phrases for which home pages should be optimised (from the universities themselves). These should then be used to do actual searches on search engines and use the search engine result positions of the participating university homepages to rank them.

## Acknowledgements

The author would like to acknowledge Mrs Prashini Rughanandan and Mrs Corrie Uys, for their assistance with the empirical and statistical work of this research project.

## <a id="author"></a>About the author

**Melius Weideman** is a Professor heading the Website Attributes Research Centre at the Cape Peninsula University of Technology in Cape Town. He graduated with a PhD in Information Science from the University of Cape Town in 2001, and focuses his research on Website visibility and usability. He can be contacted at: [weidemanm@cput.ac.za](mailto:weidemanm@cput.ac.za)

</section>

<section>

## References

<ul>
<li id="gro11">
<a href="http://www.webcitation.org/6KDOyehOg"><em>1994 Group</em></a>. (2011). Retrieved 5 October, 2013 from http://en.wikipedia.org/wiki/1994_Group (Archived by WebCite&reg; at http://www.webcitation.org/6KDOyehOg)
</li>
<li id="ada13">Adamo, S. (2013). <a href="http://www.webcitation.org/6KDO9bq0n"><em>comScore releases August 2013 U.S. search engine rankings</em></a>. Retrieved 8 October, 2013 from http://searchenginewatch.com/article/2108991/August-2011-Search-Engine-Share-from-comScore-Hitwise (Archived by WebCite&reg; at http://www.webcitation.org/6KDO9bq0n)
</li>
<li id="agu09">Aguillo, I. (2009). Measuring the institution's footprint on the web. <em>Library Hi Tech</em>, <strong>27</strong>(4), 540-556.
</li>
<li id="cro01">Cronin, B. (2001). Bibliometrics and beyond: some thoughts on Web-based citation analysis. <em>Journal of Information Science</em>, <strong>27</strong>(1), 1-7.
</li>
<li id="cut10">Cutts, M. (2010). <a href="http://www.webcitation.org/6KDP8Q3Ce"><em>Gadgets, Google and SEO</em></a>. Retrieved 07 October, 2013 from http://www.mattcutts.com/blog/site-speed/ (Archived by WebCite&reg; at http://www.webcitation.org/6KDP8Q3Ce)
</li>
<li id="esp08">Espadas, J., Calero, C. &amp; Piattini, M. (2008). Web site visibility evaluation. <em>Journal of the American Society for Information Science and Technology</em>, <strong>59</strong>(11), 1727-1742.
</li>
<li id="hei06">Heimeriks, G. &amp; van den Besselaar, P. (2006). <a href="http://www.webcitation.org/6KDPKPayB">Analyzing hyperlink networks: the meaning of hyperlink-based indicators of knowledge</a>. <em>Cybermetrics</em>, <strong>10</strong>(1), paper 1. Retrieved 06 October, 2013 from http://heimeriks.net/Analyzing%20hyperlink%20networks.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6KDPKPayB)
</li>
<li id="the11">The Higher Education Academy. (2011). <a href="http://www.webcitation.org/6KDOmYHLT"><em>Trinity Laban - harmony &amp; counterpoint: structures for merger</em></a>. York, UK: The Higher Education Academy. Retrieved 08 October, 2013 from http://www.heacademy.ac.uk/projects/detail/changeacademy/change_academy_trinity_laban_2006 (Archived by WebCite&reg; at http://www.webcitation.org/6KDOmYHLT)
</li>
<li id="red11">Higher Education Statistics Agency. (2011). <a href="http://www.webcitation.org/6KDOUYasX"><em>Redesigning the information landscape for higher education</em></a>. Cheltenham, UK: Higher Education Statistics Agency. Retrieved 07 October, 2013 from http://www.hesa.ac.uk/index.php (Archived by WebCite&reg; at http://www.webcitation.org/6KDOUYasX)
</li>
<li id="lix05a">Li, X., Thelwall, M., Wilkinson, D. &amp; Musgrove, P. B. (2005a). National and international university departmental web site interlinking, part 1: validation of departmental link analysis. <em>Scientometrics</em>, <strong>64</strong>(2), 151-185.
</li>
<li id="lix05b">Li, X., Thelwall, M., Wilkinson, D. &amp; Musgrove, P. B. (2005b). National and international university departmental web site interlinking, part 2: link patterns. <em>Scientometrics</em>, <strong>64</strong>(2), 187-208.
</li>
<li id="mal10">Malaga, R. A. (2010). Search engine optimization—black and white hat approaches. <em>Advances in Computers</em>, <strong>78</strong>, 1-39.
</li>
<li id="mid99">Middleton, I., McConnell, M. &amp; Davidson, G. (1999). Presenting a model for the structure and content of a university World Wide Web site. <em>Journal of Information Science</em>, <strong>25</strong>(3), 219-227.
</li>
<li id="nic10">Nicolae, M. &amp; Marinescu, R. (2010). <a href="http://www.webcitation.org/6KDPOfrye">University marketing - innovative communication for effective international survival</a>. <em>Journal for Communication Studies</em>, <strong>3</strong>(1), 117-138. Retrieved 2 October, 2013 from http://www.essachess.com/index.php/jcs/article/view/89 (Archived by WebCite&reg; at http://www.webcitation.org/6KDPOfrye)
</li>
<li id="ora10">Oralalp, S. (2010). <em>Analysis of Turkey's visibility on global Internet</em>. Unpublished Master of Science thesis, Middle East Technical University, Ankara, Turkey.
</li>
<li id="rat10">Rathimala, K. &amp; Marthandan, G. (2010). Exploring hyperlink structure of electronic commerce websites: a Webometric study. <em>International Journal of Electronic Business</em>, <strong>8</strong>(4-5), 391-404.
</li>
<li id="sug11">Sugak, D. B. (2011). Rankings of a University's Web sites on the Internet. <em>Scientific and Technical Information Processing</em>, <strong>38</strong>(1), 17-19.
</li>
<li id="sul11">Sullivan, D. (2011). <a href="http://www.webcitation.org/6KDPVXO0J"><em>Introducing: the periodic table of SEO ranking factors</em></a>. Retrieved 6 October, 2013 from http://searchengineland.com/introducing-the-periodic-table-of-seo-ranking-factors-77181 (Archived by WebCite&reg; at http://www.webcitation.org/6KDPVXO0J)
</li>
<li id="tee11">
<a href="http://www.webcitation.org/6KSQk6wck"><em>Teesside University</em></a>. (2011). Retrieved 05 October, 2013 from http://en.wikipedia.org/wiki/Teesside_University (Archived by WebCite&reg; at http://www.webcitation.org/6KSQk6wck)
</li>
<li id="the08">Thelwall, M. &amp; Zuccala, A. (2008). A university-centred European Union link analysis. <em>Scientometrics</em>, <strong>75</strong>(3), 407-420.
</li>
<li id="the10">Thelwall, M. (2010). <a href="http://www.webcitation.org/6KDPh4X4P">Webometrics: emergent or doomed?</a>. <em>Information Research</em>, <strong>15</strong>(4) colis 713. Retrieved 01 October, 2013 from http://InformationR.net/ir/15-4/colis713.html (Archived by WebCite&reg; at http://www.webcitation.org/6KDPh4X4P)
</li>
<li id="wei09">Weideman, M. (2009). <em>Website visibility: the theory and practice of improving rankings</em>. Oxford: Chandos Publishing.
</li>
<li id="wei11">Weideman, M. (2011). <a href="http://www.webcitation.org/6LIqQNVVc">Rogue's gallery – South African university website visibility</a>. In A. Koch and P.A. van Brakel (Eds.). <em>Proceedings of the 13th Annual Conference on World Wide Web Applications, 14-16 September 2011, Johannesburg, South Africa.</em> Retrieved 21 November, 2013 from http://www.zaw3.co.za/index.php/ZA-WWW/2011/paper/viewFile/474/140 (Archived by WebCite&reg; at (Archived by WebCite&reg; at http://www.webcitation.org/6LIqQNVVc))
</li>
<li id="yi08">Yi, K. &amp; Jin, T. (2008). Hyperlink analysis of the visibility of Canadian library and information science school web sites. <em>Online Information Review</em>, <strong>32</strong>(3), 325-347.
</li>
</ul>

</section>

</article>