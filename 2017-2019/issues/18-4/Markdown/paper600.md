<header>

#### vol. 18 no. 4, December, 2013

</header>

<article>

# Being private in public: information disclosure behaviour of Israeli bloggers

#### [Jenny Bronstein](#author)  
Department of Information Science, Bar-Ilan University, Ramat-Gan 52900, Israel

<section>

#### Abstract

> **Introduction.** This study investigated different elements of the information disclosure behaviour of Israeli bloggers. The study examined the four different elements: self-disclosure patterns, the role that anonymity plays in the disclosure of information, the connection bloggers have with their readers and how the readers' comments influence the bloggers' information disclosure behaviour.  
> **Method.** The study was conducted using an online survey. The sample consisted of eighty-two regularly maintained personal blogs written in Hebrew.  
> **Analysis.** Data analysis consisted of two phases. First, a statistical analysis yielded quantifiable demographic data about the participants, about different elements of self-presentation and self-disclosure, and about the effect their audiences have on their blogging practices. Second, a thematic analysis of the participants' textual answers, that will provide a different insight into the participants' quantitative responses.  
> **Results.** Findings show that blogs represented a protected space in which bloggers felt free to disclose personal information, expose their 'sensitive side', to reveal embarrassing information about themselves, to take down their defences, when blogging. A connection was found between the anonymity provided by the blog and the self-awareness of the bloggers. Respondents describe their blogs as a place to express themselves freely and an outlet for feelings and emotions; as a tool that helps them share information and connect with people with whom they have something in common; as a place to keep a record of their lives. These findings do not support the claim of the deindividuation experienced by users of computer-mediated environments, since findings show that participants were able to regulate their own behaviour according to their own personal values and were concerned about other people's feelings.  
> **Conclusions.** Participants struggled with the dichotomy between the public and the private spheres in blogging, that is based on the balance needed between the need for privacy and the need for community based on the identification with others.

## Introduction

Personal blogs are a form of online diary that are characterised by high amounts of self-disclosure ([Chen 2012](#che12); [Herring _et al._ 2004](#her04); [Viegas 2005](#vie05)) that foster the development of social connections and the communication of one's identity ([Hollenbaugh 2011](#hol11); [Nardi _et al._ 2004](#nar04); [Stefanone and Jang 2007](#ste07)). Serfaty [(2004)](#ser04) explains that personal blogs are a form of self-representational writing and are essentially online diaries. However, blogs are in no means restricted to the blogger's domain, these diaries are posted in a public space open to a potentially large audience and therefore this public exposure of the blogger's life may involve some risks. For this reason, blogging services offer users different options in terms of anonymity. Bloggers can choose to be totally anonymous, pseudonymous or identifiable ([Qian and Scott 2007](#qia07)). They can also choose the way they present themselves in their blogs and the nature of the information they disclose. Blogs have become personal communicative spaces that offer different layers of interactivity which can be controlled or turned off completely. These multiple layers of interactivity create a personal '_protected space_' ([Gumbrecht 2005](#gum05)), that tends to be less adversarial and more reflective in nature, where bloggers feel secure and comfortable to write. Hence, the physical invisibility that characterises online communication facilitates self-expression and provides bloggers power over the content on their blogs and over the identity they choose to reveal to, or conceal from, their audience. In other words, every blogger can establish a customised comfort zone for communicating with his/her audience and maintain it. Finally, the creation of such a space may affect his/her decision to disclose personal information in a blog, which is in effect a public medium.

Information disclosure behaviour relates to the disclosure of personal or intimate information through self-presentation and self-disclosure facilitated by the Internet, as well as the avoidance or suppression of disclosure of information ([Bortree 2005](#bor05); [Bronstein 2012](#bro12); [Papacharissi 2007](#pap07); [Trammell _et al._ 2006](#tra06); [Viegas 2005](#vie05)). Previous studies ([Joinson 2001](#joi01); [McKenna and Bargh 1998](#mck98), [2000](#mck00)) have shown that computer-mediated environments and general Internet-based behaviour can be characterised as containing high levels of self-disclosure. Understanding information disclosure is important since it enables people to relax, enhancing physical health and subjective well-being ([Ko and Chen 2009](#ko09)), to establish and develop intimacy ([Gibbs _et al._ 2006](#gib06)) and to manage their image online ([Omarzu 2000](#oma00)). This study investigated self-presentation and self-disclosure patterns in personal blogs and examined the dichotomy that exists in blogging between the private and the public spheres.

## Literature survey

Computer-mediated environments allow users to create and develop virtual identities and spaces that occupy neither space nor time ([Papacharissi 2002](#pap02)). McKenna and Bargh ([2000](#mck00)) proposed four domains in which social interaction in a computer-mediated environment differs from other conventional media: relative anonymity, attenuation of physical distance, reduced importance of physical appearance and greater control over the time and pace of interactions. Of particular relevance to the present study are the notion of anonymity and the control that computer-mediated environments, such as blogs, allow individuals over their interactions with others. According to Suler ([2004](#sul04)), anonymity is the concealment of identity that becomes possible when people have the opportunity to separate their actions online from their in-person lifestyle and identity; they feel less vulnerable about self-disclosing because whatever they say or do cannot be directly linked to the rest of their lives. Anonymity is central to understanding most of computer-mediated communication, including online self-disclosure and deindividuation ([Spears and Lea 1994](#spe94); [Whalter 1996](#wha96)). The application of de-individuation to computer-mediated environments is related primarily to the concept of anonymity ([Joinson 2001](#joi01)) and it is seen as a key factor in computer-mediated communication ([McKenna and Bargh 2000](#mck00); [Yao and Flanagin 2006](#yao06)). According to the theory of deindividuation certain conditions may cause individuals to decrease self-awareness and to cease viewing themselves as unique individuals, causing anti-normative and uninhibited behaviour ([Zimbardo 1969](#zim69)). Furthermore, deindividuation could result in an individual having less public self-awareness that might lead to caring less about what others think of his or her behaviour and may even have a reduced awareness of what others have said or done ([McKenna and Bargh 2000](#mck00)). Duval and Wicklund ([1972](#duv72)) distinguish between private and public self-awareness. Private self-awareness relates to personal beliefs, hidden inner feelings, thoughts and memories. Public self-awareness consists of overt displays such as physical appearance, accent and social behaviour. The present study investigates if the information disclosure of bloggers writing personal blogs is affected by patterns of deindividuation.

This study focused on two elements of information disclosure behaviour, self-presentation and self-disclosure. Self-presentation refers to how people present themselves in a variety of ways, tailoring the images they convey to others that vary according to situational demands ([Goffman 1959](#gof59)). It is also called impression management, which emphasises people's attempts to control how their self is presented ([Min and Lee 2011](#min11)). Mckenna and Bargh ([2000: 65](#mck00)) posit that when interacting with strangers over the Internet, individuals tend to present idealised versions of themselves because the Internet allows people to open up new and high quality possibilities for individuals who wish to construct '_a certain image of self and claiming an identity for oneself_'. This process of selective self-presentation is relevant to the self-awareness perspective. For individuals to engage in selective self-presentation they must have heightened public and private self-awareness. That is, they should be aware of their own physical appearance, behaviour and traits but at the same time be connected to their thoughts and feelings. Closely related to the concept of self-presentation is the propensity an individual has for revealing personal information to others ([Archer 1980](#arc80)). According to Schau _et al._ ([2003](#sch03)) the propensity for self-disclosure an individual has relates to the content of the self-presentation they choose to display. Strategies of self-presentation often revolve around repressing personal information or supplanting it with modified details that disclose a more desired self.

The Internet and other computer-mediated environments' attributes of visual anonymity and asynchroneity stimulate self-disclosure by eliminating the dangers of face-to-face communications, such as ridicule or rejection ([Peter _et al._ 2005](#pet05); [Suler 2004](#sul04)). The Internet enables people to fully disclose aspects of themselves that are difficult to disclose physically or to conceal those aspects they find undesirable ([Schau _et al._ 2003](#sch03)). Hence the anonymity provided by the Internet protects users, lessens their vulnerability and therefore reduces their need to safeguard their privacy by not disclosing personal information ([Ben-Ze'ev 2003](#ben03)). Miller and Shepherd ([2004](#mil04)) explain that self-disclosure on blogs serve some important purposes: providing better understanding of self, confirming one's beliefs, offering rewards in social interaction and manipulating others. Van House ([2004](#van04)) states that blogs constitute a communication genre strongly related to individuality and self-representation in which self-disclosure has become a norm.

Self-disclosure has been studied in a number of different settings using computers. For instance, Parks and Floyd ([1996](#par96)) studied the relationships formed by Internet users and found that people disclosed significantly more in their Internet relationships compared to their real life relationships. Similarly, in their study of '_coming out on the Internet_', McKenna and Bargh ([1998: 682](#mck98)) argue that participation in online newsgroups gives people the benefit of '_disclosing a long secret part of one's self_'. Chesney's study ([2005](#che05)) about a small-scale study of online diaries, reported high levels of disclosure of sensitive information with half of his participants claiming to never withhold information when writing their diaries. Self-disclosure also has a role in the relationship between bloggers and their audiences; as Couture ([2004: 4](#cou04)) explains '_a private life revealed in public expression is the effort to use identity as a way to reach and influence someone else_'. Rak ([2005](#rak05)) suggests that blogs function as a social space in which writers balance between public and private spheres and between the virtual and real worlds, defining themselves through the style and design of their sites, their personal links, and the self-disclosures they make in order to gain readers that will develop into a community of similarly oriented, trusting and sharing individuals. She further observes that identity creation in blogging is based on a need for privacy and on a need for community based on identification with others. The need to socialise, and to create and maintain social ties, is a strong motivation to blog because '_by identifying, formulating, and discussing problems and interests, a socially shared view can evolve through the interaction of others_' ([Mosel 2005](#mos05)). Socialising through blogging might be strengthened by the fact that our concern for privacy is directly related to an emotional connection with our interlocutor; in other words, we find it easier to disclose personal information to strangers who play no significant role in our lives ([Ben-Zeev 2003](#ben03)). In a qualitative study, Nardi _et al._ ([2004](#nar04)) proposed several motivations for blogging that were associated with an audience: socialisation, expressing opinions to influence others, seeking others' opinions and feedback, and the blogger's consciousness of an audience influencing the blogger's thought process and writing style. This study also found that although participants were keen to forge a connection with their readers, they felt the need to maintain a certain distance and to retain control over the interactions with them. Along the same line, Gumbrecht ([2005](#gum05)) claims that bloggers use ambiguous language in order to protect themselves and select their audiences. The current study examines the relationship between the bloggers and their audiences and the impact of this relationship on the self-presentation and self-disclosure patterns of bloggers.

## Methods

### Research questions

This study examines the following questions regarding the information disclosure behaviour of bloggers when creating a personal space in their blogs:

1.  What characterises participants' self-disclosure and what influences does the anonymity provided by the blog have on the bloggers' self-disclosure?
2.  To what extent participants engage in selective self-presentation?
3.  What role does the blog as a personal space play in the blogger's life?
4.  To what extent bloggers know their audiences and how the readers' comments influence the bloggers' self-presentation and self-disclosure?

The survey (Appendix A) was largely qualitative and was not designed to produce data for statistical (other than descriptive) analysis. The aim of the study was not to characterise a particular population of bloggers but rather to explore information disclosure behaviour in blogging practices. The study was conducted for one month (December 15, 2011 to January 15, 2011) using an online survey. The survey was short, simply designed and easy to answer, to lessen many of the drawbacks of online questionnaires ([Baron and Siepmann 2000](#bar00); [Gunn 2002](#gun02)). It was written in Hebrew since it appealed to a Hebrew speaking population. The online questionnaire was divided in four main sections:

1.  Bloggers' demographic data: questions addressing the blogger's personal data such as age, time blogging, sex and education.
2.  Self presentation on the blog: questions about different elements of self- presentation.
3.  Disclosure of personal information: questions addressing different elements of self disclosure.
4.  Open ended questions: questions requesting that participants explain in their own words their answers to the following five items in the questionnaire:
    1.  The kind of information they perceived as being too personal to disclose in their blogs
    2.  The subjects that they will disclose only in their blogs
    3.  The difference between a written diary and a blog
    4.  The role their blog plays in their life
    5.  The influence that their readers' comments have on their self-disclosure patterns.

The blogs in the sample were selected from the personal blogs category in two Israeli blog directories:  

1.  [Hablogia](http://www.tapuz.co.il/blog/)
2.  [Israblog](http://isablog.nana10.co.il/)

The sample consisted of regularly maintained personal blogs written in Hebrew. The content of each blog was examined to verify that it did not serve any commercial or marketing purpose. Three hundred blogs matching the above specified criteria were identified in the two directories. Using the email address provided in the blogs, the researcher sent emails to the bloggers inviting them to participate in the study. The introductory email included a short explanation of the study and the address of the Website containing the online questionnaire. Because blogging is an online activity, contacting the bloggers and collecting the data online in their natural setting was the most effective approach. Of the 300 bloggers initially contacted, 82 responded to the survey representing a 27.33% response rate.

The data analysis consisted of two phases. In the first phase a statistical analysis yielded quantifiable demographic data about the participants, about different elements of self-presentation and self-disclosure, and about the effect their audiences have on their blogging practices. The second phase involved a thematic analysis of the participants' textual answers that will provide a different insight into the participants' quantitative responses. Thematic content analysis is a descriptive presentation of qualitative data that identifies common themes '_in order to give expression to the communality of voices across participants_' ([Anderson 2007](#and07): 1). Thematic analysis facilitates identifying, analysing and reporting patterns or themes within data. A theme is a pattern found in the information that describes and organises the possible observations ([Boyatzis 1998](#boy98): 4).

## Findings

The following section presents the descriptive data resulting from the statistical analysis and the categories resulting from the thematic analysis of the bloggers' textual answers.

### Demographic data

The bloggers that participated in the study were adults who maintain a personal blog on the Internet.

<table><caption>Table 1: Demographic data of participants</caption>

<tbody>

<tr>

<th>Demographics</th>

<th>%</th>

<th>N</th>

</tr>

<tr>

<td colspan="3">

_**Age Range**_</td>

</tr>

<tr>

<td>18-24</td>

<td>24</td>

<td>20</td>

</tr>

<tr>

<td>25-35</td>

<td>35</td>

<td>29</td>

</tr>

<tr>

<td>36-45</td>

<td>23</td>

<td>19</td>

</tr>

<tr>

<td>46-55</td>

<td>9</td>

<td>7</td>

</tr>

<tr>

<td>56-65</td>

<td>6</td>

<td>5</td>

</tr>

<tr>

<td>65+</td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td colspan="3">

_**Sex**_</td>

</tr>

<tr>

<td>Male</td>

<td>20</td>

<td>16</td>

</tr>

<tr>

<td>Female</td>

<td>81</td>

<td>66</td>

</tr>

<tr>

<td colspan="3">

_**Level of education**_</td>

</tr>

<tr>

<td>Grade school education</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>High school graduate</td>

<td>29</td>

<td>24</td>

</tr>

<tr>

<td>Bachelor's degree</td>

<td>70</td>

<td>57</td>

</tr>

</tbody>

</table>

Table 1 shows that the majority of respondents were female bloggers (81% n = 66) between the ages of 18 to 45 (83%). This finding concurs with previous studies that found that women are more likely to blog about their personal experiences than men ([Hollenbaugh 2010](#hol10); [Lenhart and Fox 2006](#len06); [Nowson and Oberlander 2006](#now06)) and contradicts other findings ([Bronstein 2012](#bro12); [Herring _et al._ 2004](#her04); [Viegas 2005](#vie05); [Yu 2007](#yu07)) in which the majority of respondents were males. As in previous studies ([Bronstein 2012](#bro12); [Herring _et al._ 2004](#her04); [Li 2005](#li05); [Viegas 2005](#vie05)) the majority of respondents in this study had a college education (70 % n = 57).

### Patterns of self-disclosure

The first research question examined the participants' self-disclosure patterns. Table 2 presents the mean and standard deviation of different aspects of self-disclosure that bloggers rated on a 1-7 Likert scale (1 lowest value 7 highest value).

<table><caption>Table 2: Mean and standard deviation of elements of self-disclosure</caption>

<tbody>

<tr>

<th>"To what extent do you..."</th>

<th>M</th>

<th>SD</th>

</tr>

<tr>

<td>...disclose personal information on your blog</td>

<td>5.82</td>

<td>1.39</td>

</tr>

<tr>

<td>...show your sensitive side on your blog</td>

<td>5.68</td>

<td>1.71</td>

</tr>

<tr>

<td>...disclose embarrassing information on your blog</td>

<td>4.4</td>

<td>1.81</td>

</tr>

<tr>

<td>...let your defences down on your blog</td>

<td>4.76</td>

<td>1.8</td>

</tr>

<tr>

<td>...disclose information about threatening or stressful aspects of your life on your blog</td>

<td>5</td>

<td>1.89</td>

</tr>

</tbody>

</table>

In order to further understand the nature of self-disclosure in a blog, bloggers were asked if they have revealed something about themselves in their blogs that they will not disclose to their close family and 55% (n = 45) of bloggers answered affirmatively. When asked to explain their answers bloggers gave three main reasons: to avoid personal judgments or confrontations, written communication is easier than face-to-face communication, being able to express oneself without harming other people.

_To avoid the passing of personal judgments or confrontations._ The anonymity and the physical distance provided by their blogs allowed bloggers to disclose personal information without the fear of being judged and to avoid personal confrontations with their families and friends.

> _My readers do not know me so they cannot judge me.  
> When blogging there is no need to deal with embarrassing situations or with undesirable reactions from other people.  
> Maybe because people that are close to you tend to be more judgmental than strangers that don't know you._

_Written communication is easier than face-to-face communication._ Blogging as a communication tool is a recurrent element in the research on blogging ([Nardi _et al._ 2004](#nar04)). Bloggers in this study explained that they found blogging an easier way to communicate and express themselves than face-to-face communication.

> _It is just easier to write about someone or something that is bothering me, it will be harder to say the same things to someone face-to-face.  
> For example I blogged about how a girl from work offended me the other day. But I didn't say anything to her, it was easier to just write about it in my blog.  
> Blogging is easier than talking because of the anonymity. When I'm blogging I have control over the information I reveal and the people who read my blog don't know me._

_Being able to express oneself without harming other people._ Again, the anonymity and the physical distance provided by the blog allow bloggers to express themselves freely without worrying about hurting other people.

> _When I blog about someone it doesn't get to him or her, you don't always have to say everything face-to-face and my blog is a neutral place.  
> Blogging allows me to vent my ideas to the world without disclosing them to the people close to me and maybe hurting them.  
> [It is easier to blog...] because there are people who are not willing to listen to the truth about them, like my mother._

### Limitations on disclosure

Despite feeling protected by the anonymity to disclose personal information, bloggers demonstrated having public self-awareness and were aware of the public nature of their blogs. When asked if there are subjects they will never write about, 70% (n = 57) of bloggers affirmed that there were issues or subjects they will not include in their posts. When asked to describe these subjects or issues the bloggers' responses concentrated on three main issues: disclosure of information about family members and others that might harm them, their sexual life and the disclosure of deep seeded feelings. The following statements present examples of the bloggers' textual answers.

_Writing about family members and others._ Although the majority of bloggers wrote their blogs anonymously they were afraid to be identified by their postings and were concerned about harming family members or other people in their lives.

> _I don't write about my children because it was my choice to expose myself on my blog and not theirs.  
> I don't write very personal details about myself and others, things that might hurt someone._

_Sex_. The bloggers' sexual life was perceived by many of the bloggers as a subject that is off limits when blogging.

> _I will never write about my sex life, it is too personal.  
> [I will not write about]… Everything related to my sex life with my husband because I know some of the people who read my blog._

_Disclosure of deep-seated feelings._ Although bloggers reported disclosing personal information in their blogs, there were certain feelings they were not willing to blog about.

> _I have never written about being of Georgian descent and what it means for me to be a part of that community.  
> I have never written about the scar my grandmother's death left in me. In fact, I never write about the subject of death, it is too painful._

_The bloggers' connection to the audiences._ A third difference between a diary and a blog is physical distance provided by the Internet that allows bloggers not to know who their readers are.

> _Blogs are read by people I don't know that is why it is less embarrassing.  
> A blog is read by random people. A personal diary feels different. I would not feel comfortable letting anyone read my diary.  
> Maybe the difference is that most of the people who read my blog don't know me._

### Self-disclosure on different environments

To further understand the respondents' disclosure behaviour, bloggers were asked if, in their opinion, there are any differences between a personal written diary and a blog. Seventy eight percent (n = 59) of bloggers answered they will not allow other people to read their personal diary. When asked to explain their answers bloggers gave three main differences between a written diary and a blog: the anonymity provided by the blog, the difference in the purpose of each medium, the bloggers' connection to the audiences. Following there are some examples of the blogger's statements.

_The anonymity provided by the blog._ Bloggers regard blogs in a different manner than personal diaries because of the anonymity and the physical distance provided by the blogs.

> _There is no face behind the words in a blog.  
> A diary has a known author while a blog is anonymous.  
> Anonymity is very important to me. There are very few people who know that I have a blog and only a handful of them have read it._

_The purpose of each medium._ One of the reasons that bloggers perceive blogs as being different from written diaries is that each medium has a different purpose.

> _Writing a diary is a reflective non-communicative form of writing whereas blogging is a form of communicative writing destined for other eyes and written with the realisation that strangers will read it.  
> I suppose that if I were to write a diary I would not want other people to read it but blogs in their very essence are meant to be read by other people.  
> From my blog I expect help and support from my readers, a diary is a way to vent so I will not blow up._

### Self-presentation

The second research question examined the issue of selective self-presentation of bloggers. Findings show that the majority of bloggers that participated in this study created an anonymous virtual presence on their blogs. Bloggers chose to maintain some kind of anonymity by identifying themselves using a pseudonym (61% n = 50) or a variant of their real name (18% n =15); only 21% (n = 17) of the bloggers identified themselves by their real names and only 21% (n = 17) displayed a personal photo on their blog. Moreover, 64% (n = 62) of bloggers stated that preserving this anonymity was important for them. This tendency for self-presentation does not concur with findings from other studies in which the majority of bloggers identified themselves using their real names and posted a personal photograph of themselves in their blog ([Bronstein 2012](#bro12); [Guadagno _et al._ 2008](#gua08); [Viegas 2005](#vie05)).

### The role of the blog in the blogger's life.

The third research question examines the role or significance that the blog has on the blogger's life. When asked if their blog has an impact on their life, 60% (n = 49) of bloggers responded affirmatively. In addition, bloggers were asked to describe in their own words the role their blogs have in their lives. The textual description respondents gave of the role their blogs have in their personal life confirms the notion that they perceive it as a personal space. Findings from the content analysis revealed that respondents describe their blogs as: an outlet to express feelings and thoughts, a tool that helps them share information and connect with people, and a way to document their lives.

_My blog as an outlet to express feelings and thoughts._ Blogging is perceived by bloggers as an easy and friendly venue for free expression that allows them to voice their innermost thoughts and feelings in a protected space.

> _My blog serves as an outlet for thoughts that are not suited to be expressed anywhere else.  
> Blogging for me is letting everything out, like therapy. After I have vent[ed] everything out on my blog I feel better, less stressed more at peace.  
> [My blog is…] an echo to my thoughts._

_My blog as a tool that helps me share information and connect with people._ Blogs have a socialisation role through which bloggers communicate with their audiences who act as a support system.

> _My blog is a very tangible virtual reality. I don't know who said that a home is not the place you live in but the place where you feel accepted and understood. For me, my blog is the only place in the world where I can be myself and my readers accepted me for who I am.  
> To share my life experiences with others so they will know they are not alone. I have received a lot of feedback from my readers who have become my friends.  
> To unload, to find a sympathetic ear and a shoulder to cry on._

_My blog as a way to document my life._ Bloggers perceive their blogs as a place to keep a record of their lives, to document their experiences, to keep track of thoughts and feelings over time.

> _My blog is my memoir.  
> I blog to document my thoughts, so I'll have a record of my life that I can go back to and remember. When things are going bad I blog more frequently, that way I can share with other[s] my pain and I can go back and read what I have just written and maybe gain some insight of my life._

### The bloggers' connection to their audiences and the impact comments have on the disclosive content of blogs

The fourth research question asks to what extent bloggers know their audiences and the impact that their readers' comments have on the bloggers' self-presentation and self-disclosure. Schiano _et al._ ([2004](#sch04)) state that the blogger's primary audience is the blogger himself because blogging is often used as an outlet for thoughts and feelings. Findings in this study support this statement, 81% (n = 66) of bloggers answered that they did not know who their readers are, 60% (n = 49) reported never checking the entry log of their blog to check who accessed it and 65% (n = 53) reported that they do not think about their readers when blogging. However, 45% (n = 37) of bloggers reported that their readers' comments have some effect on the content of their blogs. When asked to explain their answer bloggers presented the following explanations.

> _If I see that a specific subject arouses a lot of comments I will continue to write about that particular subject.  
> Comments can be empowering, supportive and they can give great advice.  
> Up until now I have received very positive comments from my readers about my writing, these comments make me feel more secure about my writing_

Although bloggers write their blogs mainly for themselves, they are careful when disclosing information about other people in their lives. 85% (n = 70) of bloggers reported not using other people's real names when writing about them in their blogs and 79% (n = 65) answered they have never had any trouble when writing about other people.

## Discussion and conclusion

This study investigated the self-presentation and self-disclosure patterns in the information disclosure behaviour of personal bloggers. Findings show that blogs have become a communication genre strongly related to individuality, self-representation, and self-disclosure; they serve the purpose of personal expression well and represent an ideal medium for the creation of an online presence. The asynchronous and anonymous nature of blogs acts as a buffer and protects bloggers from threatening social interactions, creating a secure venue for self-expression and socialisation. Lee _et al._ ([2008](#lee08)) posited that since self-disclosure depends on the perception that the information divulged is secured, blogs are perceived as protected spaces because they provide a dyadic (i.e. safe) boundary within which self-disclosure is considered secured. This boundary is created by the physical invisibility that characterises online communications, that facilitates self-expression, and gives bloggers power over the content on their blogs and over the identity they choose to reveal to, or conceal from, their audience.

A connection was revealed between anonymity and heightened private self-awareness that led to increased self-disclosure. The anonymity possible in blogging facilitated the creation of a protected space in which bloggers felt free to disclose personal or embarrassing information, to show their sensitive side, and to let their defences down thus allowing the creation of a personal space. Qian and Scott ([2007](#qia07)) explained the need for anonymity by claiming that bloggers who are motivated to blog to vent their thoughts and emotions would be likely to seek anonymity, while those whose goal of blogging is not self-disclosure might be more inclined to identify themselves using their real names. Bloggers in this study wished to preserve their anonymity because they perceived their blogs as venues for self-disclosure, that allow them emotional release, support and understanding from their audiences.

Although bloggers protected themselves behind their anonymity, they applied strategies of self-presentation that limited their disclosure of personal information, resulting in carefully edited, rather than 'tell-all' accounts, of the blogger's life. Echoing Krasnova _et al._'s ([2010: 111](#kra10)) claim that bloggers engage in '_a conscious "privacy calculus" when cognitively deciding whether or not to self-disclose_', the majority of bloggers in this study said that there were certain subjects, such as sex and death, that were perceived as too personal or too painful to disclose on their blogs. By not disclosing information on certain sensitive subjects, the bloggers drafted a strategy of impression management that supports Mckenna and Bargh's ([2000](#mck00)) claim that when interacting with strangers over the Internet individuals tend to present idealised versions of themselves that will not jeopardise other people's impressions of them. Thus, since bloggers are aware of the perceived cost of self-disclosure in blogging ([Ko and Chen 2009](#ko09)), they needed to control the nature and degree of their self-disclosure that will match the identity they wanted to convey on their blogs. Furthermore, the need to present an idealised version of oneself suggests that bloggers did not experience a process of deindividuation that could have lessened their public self-awareness, resulting in anti-normative and uninhibited behaviour ([Zimbardo 1969](#zim69)). The restrained self-disclosure reported by Israeli bloggers differs from the information disclosure revealed in other studies in which bloggers tended to present themselves accurately and disclosed personal information about their sex lives and romantic relationships ([Huffacker and Calvert 2005](#huf05); [Lenhart and Fox 2006](#len06)). I would like to suggest that this inhibition of the bloggers' information disclosure behaviour is elicited by their own consciousness and by the social consequences they envisioned that reflect the impact that online behaviour might have on the offline environment of the bloggers.

In order to understand how bloggers perceived the personal space they have created in their blogs, they were asked to describe the role those blogs played in their lives. The three roles attributed by participants to their blogs coincide with findings from other studies that found that bloggers were motivated to blog to express themselves freely, to share information with others and create social connections, and to document their lives ([Bronstein 2012](#bro12); [Li 2005](#li05); [Nardi _et al._ 2004](#nar04)). Self-expression and sharing information in an easy to publish environment are related to what Recuero ([2008: 100](#rec08)) calls '_creating a personal space on the Internet_'. A blog becomes a channel for bloggers to '_express themselves and to share the knowledge they have_'. Ascribing a blog the role of a documenting tool supports Hollenbaugh's ([2010](#hol10)) study that found that women who wrote primarily for themselves were more likely to disclose large amounts of personal information to archive and organise their thoughts.

The relationship between the bloggers and their audiences revealed in the study is an interesting one and it embodies the dichotomy inherent in blogging. On the one hand, the majority of the participants reported they did not know their audience nor did they think about them when blogging. I would like to suggest that not knowing their audience makes them feel secure that whatever is shared will not reach people in their social sphere, emphasising the blog's role as a venue for free self-expression. This finding reinforces the notion that the blogger's primary audience is the blogger him/herself ([Schiano _et al._ 2004](#sch04)) and that blogging is some kind of 'private writing' that allows the writer to write freely and value his or her writing without having to worry about the reactions of others ([Miller and Shepherd 2004](#mil04)). On the other hand, this study showed that the readers serve as some kind of support system that empowers the bloggers and keeps them blogging by providing support and encouragement. This finding supports results from other studies that claim that bloggers' perceptions of who their audiences are might impact their self-disclosure ([Hollenbaugh, 2010](#hol10)) and that audiences affect many dimensions of blogging behaviour ([Nardi _et al._ 2004](#nar04)).

In summary, participants struggled with the dichotomy between the public and the private spheres in blogging that is based on the balance needed between the need for privacy and the need for community based on identification with others. The self-disclosure patterns revealed in this study demonstrate that blogs play an important role in the participants' lives, by providing a venue for self-expression and by supporting the creation of a social network that offers rewards in social interaction. Moreover, findings refute the existing notion of the deindividuation experienced by computer-mediated environment's users since bloggers were able to regulate their own information disclosure behaviour according to their own personal values and were concerned about other people's feelings. Although participants were not inclined to get to know their audiences they did, as Couture ([2004](#cou04)) posited, use the identity created in their online personal space to reach and influence others.

## Limitations of the study and recommendations for further research

In the survey presented, bloggers were asked to self-report on a number of blogging behaviour issues regarding information disclosure, self-presentation and connection with an audience. As in any research based on self-reported behaviour the perceptions people have of their own behaviour can differ from their actual behaviour, therefore accuracy is difficult to verify. Unlike other studies where researchers accessed participant's blogs and conducted content analysis of the blogs no external validation was conducted for this study. Keeping in mind the possible biases introduced by the self-reporting methodology it is interesting to note the similarities between the findings presented in this study and those in previous studies on blogging behaviour.

Future research should investigate the information disclosure behaviour in other computer-mediated environments such as social networks and micro-blogging sites in order to fully understand how people create personal spaces on different virtual networks and platforms. In addition, research should be done on blogging behaviour in populations that have yet to be investigated to have a more complete picture of the differences in blogging patterns around the world.

## <a id="author"></a>About the author

Jenny Bronstein is a lecturer at the Information Science department at Bar-Ilan University. Her research interests are in education and professional development, self presentation and self disclosure on different social platforms and information seeking behavior and has published in refereed information science journals. She teaches courses in information retrieval techniques, information behavior, academic libraries and business information. In the past, Dr. Bronstein served as an academic librarian and an information professional in corporate information centers. She holds a PhD and a M.S. in Information Science from Bar-Ilan University and a B.A. in History and Linguistics from Tel-Aviv University She can be contacted at: [jenny.bronstein@biu.ac.il](mailto:jenny.bronstein@biu.ac.il)

</section>

<section>

## References

<ul>
<li id="and07">Anderson, R. (2007). <a href="http://www.webcitation.org/6L4tgzsFT)">Thematic content analysis (TCA): descriptive presentation of qualitative data.</a> Williams, OR: Wellknowing Consulting Services. Retrieved 13 January, 2012 from http://www. wellknowingconsulting. org/publications/pdfs/ThematicContentAnalysis.pdf. (Archived by WebCite® at http://www.webcitation.org/6L4tgzsFT)
</li>
<li id="arc80">Archer, J.L. (1980). To reveal or not to reveal: a theoretical model of anonymous communication. <em>Communication Theory</em>, <strong>8</strong>(4), 381-401.
</li>
<li id="bar02">Bargh, J.A., McKenna, K.Y. &amp; Fitsimons, G.M. (2002). Can you see the real me? Activation and expression of the "True Self" on the Internet.<em>Annual Review of Information Science and Technology</em>, <strong>58</strong>(1), 33-48.
</li>
<li id="bar00">Baron, J. &amp; Siepmann, M. (2000). Techniques for creating and using online questionnaires in research and testing. In M. H. Birnbaum, (Ed.), <em>Psychological experiments on the Internet</em> (pp. 235-265). San Diego, CA: Academic Press.
</li>
<li id="ben03">Ben-Ze'ev, A. (2003). Privacy, emotional closeness, and openness in cyberspace. <em>Computers in Human Behavior,</em> <strong>19</strong>(4), 451-467.
</li>
<li id="bor05">Bortree, D.S. (2005). Presentation of self on the Web: an ethnographic study of teenage girls' Weblogs. <em>Education, Communication &amp; Information</em>, <strong>5</strong>(1), 25-39.
</li>
<li id="boy98">Boyatzis, R. (1998). <em>Transforming qualitative information: thematic analysis and code development.</em> Thousand Oaks, CA: Sage.
</li>
<li id="bro12">Bronstein, J. (2012). Blogging motivations for Latin American bloggers: a uses and gratifications approach. In T. Dumova, (Ed.), <em>Blogging in the global society</em> , (pp. 200-215). Hershey, PA: Information Science Reference.
</li>
<li id="che12">Chen, G.M. (2012). Why do women write personal blogs? Satisfying needs for self-disclosure and affiliation tell part of the story. <em>Computers in Human Behavior,</em> , <strong>28</strong>(1), 171-180.
</li>
<li id="che05">Chesney, T. (2005). Online self disclosure in diaries and its implications for knowledge managers. In D. Wainwright, (Ed.) <em>Proceedings of the UK Academy for Information Systems 10th Conference 2005, 22-24 March 2005, Northumbria University, Newcastle upon Tyne.</em> Newcastle upon Tyne, UK: Northumbria University. (CD-ROM).
</li>
<li id="cou04">Couture, B. (2004). Reconciling private lives and public rhetoric: what's at stake? In B. Couture &amp; T. Kent, (Eds.), <em>The private, the public, and the published: reconciling private lives and public rhetoric,</em> (pp. 1-16). Logan, UT: Utah University Press.
</li>
<li id="duv72">Duval, S. &amp; Wicklund, R.A. (1972). <em>A theory of objective self-awareness.</em> New York, NY: Academic Press.
</li>
<li id="ell99">Ellis, D., Allen, D.K. &amp; Wilson, T.D. (1999). Information science and information systems: conjunct subjects disjunct disciplines. <em>Journal of the American Society for Information Science,</em> <strong>50</strong>(12), 1095-1107.
</li>
<li id="gas99">Gaslikova, I. (1999). <a href="http://www.webcitation.org/63UQ672d7">Information seeking in context and the development of information systems</a>. <em>Information Research</em>, <strong>5</strong>(1). Retrieved 13 January, 2012 from http://informationr.net/ir/5-1/paper67.html (Archived by WebCite® at http://www.webcitation.org/63UQ672d7).
</li>
<li id="gib06">Gibbs, J.L., Ellison, N.B. &amp; Heino, R.D. (2006). Self-presentation in online personals: the role of anticipated future interaction, self-disclosure, and perceived success in Internet dating. <em>Communication Research,</em> <strong>33</strong>(2), 152-177.
</li>
<li id="gof59">Goffman, E. (1959).<em>Presentation of self in everyday life.</em> Garden City, NY: Anchor.
</li>
<li id="gua08">Guadagno, RE., Okdie, B.M. &amp; Eno, C.A. (2008). Who blogs? Personality predictors of blogging. <em>Computers in Human Behavior</em>, <strong>24</strong>(5), 1993-2004.
</li>
<li id="gum05">Gumbrecht, M. (2005). <a href="http://www.webcitation.org/6KPQMK3GK"><em>Blogs as "protected space".</em></a> Paper presented at the Workshop on the Weblogging Ecosystem: Aggregation, Analysis and Dynamics, New York. Retrieved 2 May, 2011 from http://www.blogpulse.com/papers/www2004gumbrecht.pdf (Archived by WebCite® at http://www.webcitation.org/6KPQMK3GK).
</li>
<li id="gun02">Gunn, H. (2002). <a href="http://www.webcitation.org/6L4uR3lPg">Web-based surveys: changing the survey process.</a> <em>First Monday,</em> <strong>7</strong>(12). Retrieved 20 January 2012 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1014/935 (Archived by WebCite® at http://www.webcitation.org/6L4uR3lPg)
</li>
<li id="her04">Herring, S. C., Scheidt, L. A., Bonus, S., &amp; Wright, E. (2004). <a href="http://www.webcitation.org/6L4w9kt3S">Bridging the gap: a genre analysis of weblogs.</a> In: <em>Proceedings of the 37th Annual Hawaii International Conference on System Sciences (HICSS'04)</em> - Track 4. Los Alamitos, CA: IEEE Computer Society. Retrieved 22 December 2011 from http://doi.ieeecomputersociety.org/10.1109/HICSS.2004.1265271 (Archived by WebCite® at http://www.webcitation.org/6L4w9kt3S)
</li>
<li id="hol10">Hollenbaugh, E.E. (2010). Personal journal bloggers: profiles of disclosiveness. <em>Cyberpsychology, Behavior, and Social Networking,</em> <strong>36</strong>(6), 1657-1666
</li>
<li id="hol11">Hollenbaugh, E.E. (2011). Motives for maintaining personal journal blogs. <em>Cyberpsychology, Behavior, and Social Networking,</em><strong>14</strong>(1-2), 13-20
</li>
<li id="huf05">Huffaker, D.A. &amp; Calvert, S.L. (2005). <a href="http://www.webcitation.org/6KPQUvLh8">Gender, identity and language use in teenage blogs.</a> <em>Journal of Computer-Mediated Communication,</em> <strong>10</strong>(2), article 1. Retrieved 3 July, 2011 from http://jcmc.indiana.edu/vol10/issue2/huffaker.html (Archived by WebCite® at http://www.webcitation.org/6KPQUvLh8).
</li>
<li id="joi01">Joinson, A.N. (2001). Self-disclosure in computer-mediated communication: the role of self-awareness, choice and dissonance. <em>European Journal of Social Psychology</em>, <strong>31</strong>(2), 177-192.
</li>
<li id="ko09">Ko, H. &amp; Chen, T. (2009). Understanding the continuous self-disclosure of bloggers from the cost-benefit perspective. In L. L. Bello &amp; G. Iannizzotto (eds.) <em>Proceedings of the HSI 09 2nd conference on Human System Interactions</em>,(pp. 520-527), Cantania, Italy, May 21-23, 2009. Los Alamitos: IEEE Press.
</li>
<li id="kra10">Krasnova, H., Spiekermann, S., Koroleva, K. &amp; Hildebrand, T. (2010). Online social networks: why we disclose. <em>Journal of Information Technology,</em> <strong>25</strong>(2), 109-125.
</li>
<li id="lee08">Lee, D., Im, S. &amp; Taylor, C.R. (2008). Voluntary self-disclosure of information on the Internet: a multimethod study of motivations and consequences of disclosing information on blogs. <em>Psychology &amp; Marketing,</em> <strong>25</strong>(2), 692-710.
</li>
<li id="len06">Lenhart, A. &amp; Fox, S. (2006). <a href="http://www.webcitation.org/6KPQqUarq">Bloggers: a portrait of the Internet's new storytellers.</a> Pew Internet &amp; American Life Project. Retrieved 24 April, 2011 from http://www.pewinternet.org/Reports/2006/Bloggers.aspx (Archived by WebCite® at http://www.webcitation.org/6KPQqUarq)
</li>
<li id="li05">Li, D. (2005). <a href="http://www.webcitation.org/6KPTJDGKY"><em>Why do you blog: a uses-and-gratifications inquiry into bloggers' motivations.</em></a> Paper presented at the Annual Meeting of the International Communication Association, 24 May 2007, TBA, San Francisco, CA. Retrieved July 14, 2011 from http://www.allacademic.com/meta/p171490_index.html (Archived by WebCite® at http://www.webcitation.org/6KPTJDGKY).
</li>
<li id="mck98">McKenna, K.Y.A. &amp; Bargh, J.A. (1998). Coming out in the age of the Internet: identity 'demarginalization' through virtual group participation. <em>Journal of Personal Social Psychology,</em> <strong>75</strong>(3), 681-94.
</li>
<li id="mck00">McKenna K.Y.A and Bargh J.A.(2000). Plan 9 from cyberspace: the implications of the Internet for personality and social psychology. <em>Personality and Social Psychology Review,</em> <strong>4</strong>(1), 57–75.
</li>
<li id="mck08">McKenzie, H.M. (2008). <em>Why bother blogging? Motivations for adults in the United States to maintain a personal journal blog.</em> Unpublished Master's dissertation, North Carolina State University, Raleigh, North Carolina, USA.
</li>
<li id="mil04">Miller, C. R. &amp; Shepherd, D. (2004). <a href="http://www.webcitation.org/6KPRlBTH9">Blogging as social action: a genre analysis of the Weblog.</a> In L. Gurak, S. Antonijevic, S. Johnson, C. Ratliff &amp; J. Reyman, (Eds.) <em>Into the blogosphere.</em> Retrieved 10 July, 2011 from http://blog.lib.umn.edu/blogosphere/blogging_as_social_action_a_genre_analysis_of_the_weblog.html (Archived by WebCite® at http://www.webcitation.org/6KPRlBTH9).
</li>
<li id="min11">Min, J. &amp; Lee, H. (2011). The change in user and IT dynamics: blogs as IT-enabled virtual presentation. <em>Computers in Human Behavior,</em> <strong>27</strong>(6), 2339-2351.
</li>
<li id="mos05">Mosel, S. (2005). <a href="http://www.webcitation.org/6KPS43U99">Self directed learning with personal publishing and microcontent</a>. In Hug, T., M. Lindner &amp; P. Bruck (Eds.) Microlearning: Emerging Concepts, Practices and Technologies after e-Learning, June 23-24, 2005, Insbruk, Austria. Proceedings of Microlearning 2005. Learning &amp; Working in New Media. Innsbruck: Innsbruck University Press. Draft retrieved 20 May, 2011 from http://www.microlearning.org/micropapers/MLproc_2005_mosel.pdf (Archived by WebCite® at http://www.webcitation.org/6KPS43U99)
</li>
<li id="nar04">Nardi, B.A., Schiano, D.J., Gumbrecht, M.,&amp; Swartz, L. (2004). Why we blog. <em>Communications of the ACM,</em> <strong>47</strong>(12), 41-46.
</li>
<li id="now06">Nowson, S., &amp; Oberlander, J. (2006). The identity of bloggers: openness and gender in personal weblogs. In <em>Proceedings of the AAAI Spring Symposium Computational Approaches to Analyzing Weblogs,</em> March 27-29, 2006 in Palo Alto, California (pp. 163-167). Palo Alto, CA: Stanford University.
</li>
<li id="oma00">Omarzu, J. (2000). A disclosure decision model: determining how and when individuals will self-disclose. <em>Personality and Social Psychological Review,</em> <strong>4</strong>(2), 174-185.
</li>
<li id="pap02">Papacharissi, Z. (2002). The presentation of self in virtual life: characteristics of personal home pages. <em>Journalism and Mass Communication,</em> <strong>79</strong>(3), 643-660.
</li>
<li id="pap07">Papacharissi, Z. (2007). Audiences as media producers: content analysis of 260 blogs. In M. Tremayne (ed.) <em>Blogging, citizenship and the future of media.</em> (pp. 21-38). London: Routledge.
</li>
<li id="par96">Parks, M.R. &amp; Floyd, K. (1996). <a href="http://www.webcitation.org/6KPSxe5oC">Making friends in Cyberspace.</a> <em>Journal of Computer-mediated Communication,</em> <strong>1</strong>(4). Retrieved 20 May, 2011 from http://jmc.huji.ac.il/vol1/issue4/parks.html (Archived by WebCite® at http://www.webcitation.org/6KPSxe5oC)
</li>
<li id="pet05">Peter, J., Valkenburg, P. M., &amp; Schouten, A. P. (2005). Developing a model of adolescent friendship formation on the Internet. <em>CyberPsychology &amp; Behavior,</em> <strong>8</strong>(5), 423-430.
</li>
<li id="qia07">Qian, H. &amp; Scott, C.R. (2007). <a href="http://www.webcitation.org/6KPSnnWhz">Anonimity and self-disclosure on Weblogs.</a> <em>Journal of Computer-Mediated Communication,</em> <strong>12</strong>article 14. Retrieved 20 May, 2011 from http://jcmc.indiana.edu/vol12/issue4/qian.html (Archived by WebCite® at http://www.webcitation.org/6KPSnnWhz)
</li>
<li id="rak05">Rak, J. (2005). The digital queer: weblogs and Internet identity. <em>Biography,</em> <strong>28</strong>(1), 166-182.
</li>
<li id="rec08">Recuero, R. (2008). Information flows and social capital in weblogs: a case study in the Brazilian blogosphere. In P. Brusilovsky &amp; H.C. Davis, (eds.), <em>Proceedings of the nineteenth ACM conference on Hypertext and hypermedia</em>, (pp. 97-107). New York, NY: ACM.
</li>
<li id="sch03">Schau, H.J. &amp; Gilly, M.C. (2003). We are what we post? Self-presentation in personal Web space. <em>Journal of Consumer Research,</em> <strong>30</strong>(3), 385-404.
</li>
<li id="sch04">Schiano, D.J., Nardi, B.A., Gumbrecht, M. &amp; Swartz, L. (2004). Blogging by the rest of us. In <em>Extended abstracts of the 2004 conference on Human factors and computing systems</em>, (pp. 1143-1146). New York, NY: ACM.
</li>
<li id="ser04">Serfaty , V. (2004). <em>The mirror and the veil: an overview of American online diaries and blogs.</em> New York, NY: Rodopi.
</li>
<li id="spe94">Spears, R. &amp; Lea, M. (1994). Panacea or panopticon? The hidden power in computer-mediated communication. <em>Communication Research,</em> <strong>21</strong>(4), 427-459.
</li>
<li id="ste07">Stefanone, M. A., &amp; Jang, C. Y. (2007). Writing for friends and family: The interpersonal nature of blogs. <em>Journal of Computer-Mediated Communication,</em> <strong>13</strong>(1), 123-140.
</li>
<li id="sul04">Suler, J. (2004). The online disinhibition effect. <em>CyberPsychology and Behavior,</em> <strong>7</strong>(3), 321-326
</li>
<li id="tra06">Trammell, K., Tarkowski, A., Hofmokl, J. &amp; Sapp, A.(2006). Rzeczpospolita blogow [Republic of Blog]: Examining Polish bloggers through content analysis. <em>Journal of Computer-Mediated Communication,</em> <strong>11</strong>(3), 702-722
</li>
<li id="val05">Valdekenburg, P.M. &amp; Schouten, A.P. (2005). Developing a model of adolescent friendship formation on the Internet. <em>CyberPsychology,</em> <strong>8</strong>(1), 423-430.
</li>
<li id="van04">Van House, N. (2004). Weblogs: credibility and collaboration in an online world. <em>Paper prepared for the CSCW Workshop on Trust, October, 2004 [workshop subsequently cancelled].</em> Retrieved 20 May, 2011 from http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1 (Archived by WebCite® http://www.webcitation.org/6KPT7cnTZ)
</li>
<li id="vie05">Viegas, F.B. (2005). <a href="http://www.webcitation.org/6KPTxv7ux">Bloggers' expectations of privacy and accountability: an initial survey.</a> <em>Journal of Computer-Mediated Communication,</em> <strong>10</strong>(3), article 12. Retrieved 5 June, 2011 from http://jcmc.indiana.edu/vol10/issue3/viegas.html (Archived by WebCite® http://www.webcitation.org/6KPTxv7ux)
</li>
<li id="wal00">Walker, K. (2000). "It's difficult to hide it": the presentation of self on Internet home pages. <em>Qualitative Sociology,</em> <strong>23</strong>(1), 99-120.
</li>
<li id="wha96">Whalter, J.B. (1996). Computer-mediated communication: impersonal, interpersonal, and hyperpersonal. <em>Communication Research,</em> <strong>23</strong>(1), 342-369.
</li>
<li id="yao06">Yao, M.Z. &amp; Flanagin, A.J. (2006). A self-awareness approach to computer-mediated communication. <em>Computers in Human Behavior,</em> <strong>22</strong>(3),518-544.
</li>
<li id="yu07">Yu H.(2007). <em>Exploring the Chinese blogosphere: the motivations of blog authors and readers.</em> Unpublished Master Thesis, National University of Singapore.
</li>
<li id="zim69">Zimbardo, P.G. (1969). The human choice: Individuation, reason and order versus deindividuation, impulse, and chaos. In W.J. Arnold &amp; D. Levine (Eds.) <em>Nebraska Symposium on Motivation. 1969</em>, (pp. 237-307). Lincoln, NE: University of Nebraska Press. (Current theory and research in motivation, vol. 17)
</li>
</ul>

</section>

</article>

## Appendix: Questionnaire - Information about my blog

1.  How old are you?
    1.  18-24
    2.  25-35
    3.  36-45
    4.  46-55
    5.  55-65
    6.  Over 65
2.  What is your gender?
    1.  Male
    2.  Female
3.  How many years of schooling do you have?
    1.  Primary school
    2.  Secondary school
    3.  High school
    4.  University
4.  What is your blog's URL? _______________________________
5.  How do you identify yourself in your blog?
    1.  I use my real name
    2.  I use a variation of my name
    3.  I use a pseudonym
6.  For how long have you been blogging? ____________________________
7.  How often do you update your blog?
    1.  A few times a day
    2.  Once a day
    3.  At least three times a week
    4.  Once a week
    5.  Twice a week
    6.  Once a month
    7.  Other ________________________________
8.  Do you post a photograph of yourself on your blog?
    1.  Yes
    2.  No
9.  What role does your blog play in your life? _________________________________
10.  Does your blog have an impact on your life?
    1.  Yes
    2.  No
11.  If you answered yes above, what impact does your blog have on your life?  
    __________________________________________________________
12.  Please rate the following questions using a 1 – 7 scale in which 1 is the lowest rating and 7 is the highest
    1.  To what extent is the information you post in your blog personal ?
    2.  How important is for you to remain anonymous on your blog?
    3.  To what extent do you know your readers?
    4.  How often do you check your blog's log to see who is reading your posts?
    5.  How important is to you that people you know read your blog?
    6.  To what extent do your readers' comments affect your writing?
    7.  To what extent do you show your sensitive side on your blog?
    8.  To what extent do you disclose embarrassing information on your blog?
    9.  To what extent do you “put your defenses down” when you blog?
    10.  To what extent do you disclose frightening or stressful information on your blog?
13.  Imagining that you write a diary in the same manner that you write your blog, would you allow other people to read your diary?
    1.  Yes
    2.  No
14.  If your answer is no, can you explain?_________________________________________
15.  Have you ever deemed a subject to be too personal for blogging?
    1.  Yes
    2.  No
16.  If your answer is yes, please provide an example of a subject about which you will not blog?  
    ___________________________________________________________
17.  When you write about other people on your blog, do you ask for their permission?
    1.  Yes
    2.  No
18.  When you write about other people on your blog, do you use their real name?
    1.  Yes
    2.  No
19.  Have you ever disclosed something on your blog that those close to you don't know?
    1.  Yes
    2.  No
20.  If your answer is yes, why do you think it is easier to disclose personal information on your blog than face-to-face?  
    _________________________________________________________________________