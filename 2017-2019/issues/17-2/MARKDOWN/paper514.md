#### vol. 17 no. 2, June 2012

# The use of an online forum for health information by married Korean women in the United States

#### [Soojung Kim](#authors)  
Department of Library and Information Science, Chonbuk National University, Jeonju, South Korea  
[JungWon Yoon](#authors)  
School of Information, University of South Florida, Tampa, FL, USA.

#### Abstract

> **Introduction**. The purpose of this study is to examine the use of an online health forum by married Korean women living in the USA who sought help for health and medical issues.  
> **Method**. A thousand messages posted to a health forum designed for married Korean women living in the USA were collected in September 2010.  
> **Analysis**. Content analysis of the messages identified the purposes of asking questions, relationship between enquirers and patients, major topics of questions, whether questions were asked before or after consulting a doctor, and more.  
> **Results**. A typical questioning behaviour in the forum was a woman asking for recommendations of hospitals or doctors, including doctors who spoke Korean; seeking to know the cause of the symptom she was experiencing, or wanting to learn about treatment methods before consulting a doctor. The reasons for the enquirers seeking online help before or instead of consulting doctors included dissatisfaction with the health care system, financial concerns and language barriers.  
> **Conclusion**. This study demonstrates that the online health forum has potential to be useful for Korean immigrants in self-diagnosis, self-treatment, and the selection of hospitals or doctors by removing language barriers.

## Introduction

This study examines married Korean women's use of an online forum for health information. This study builds on existing knowledge of health information seeking by immigrants, especially barriers to health care, and the use of the Internet for health information.

The number of immigrants arriving each year to the USA increased in the 2000s (Martin and Midgley 2010). More than 14% of the USA population was born abroad, approximately 20% of the population of the USA speaks a language other than English at home, and 8.7% speaks English less than "very well" ([US Census Bureau 2012](#usa12)). The Korean-American population is one of the largest immigrant populations in the USA. As of 2007, it was the 7th largest immigrant group with a growth rate of 52% between 1990 and 2000 and 21% between 2000 and 2007\. The majority (62.7%) of Korean immigrants were of working age (between 18 and 54), and 56.8% of them were women. Korean immigrants had a high educational attainment; 51.3% of those aged 25 and older had a bachelor's degree or higher, which was higher than all immigrants (27%), and only 9.5% of Korean immigrants had no high school diploma or the equivalent, compared to 31.9% of all immigrants. However, 57% of Korean immigrants aged 5 and older reported that they spoke English less than "very well," which was slightly higher than all immigrants (52.4%) ([Terrazas 2009](#ter09)).

The state of immigrant health is an important component of American public health policy ([Zhao 2010](#zha10)). Barriers such as linguistic and cultural differences, low income, low levels of education, lack of access to technologies, and unfamiliarity with health care systems result in immigrants' inability to effectively cope with health problems ([Courtright 2004](#cou04); [Cashen _et al._ 2004](#cas04); [Chen _et al._ 2010](#che10); [Gany _et al._ 2006](#gan06); [Lee _et al._ 2010](#leejy); [Sadler _et al._ 1998](#sad98); [Yu and Wu 2005](#yu05)). These barriers can result in serious consequences; for instance, mortality from many cancers is rising for immigrants while decreasing for the USA-born ([Gany _et al._ 2006](#gan06); [Zhao 2010](#zha10)).

Among those barriers, unfamiliarity with the USA health care system, and linguistic and economic barriers have been identified as critical, particularly to Korean immigrants. Immigrants who are not familiar with the local health care system have difficulties in knowing where to go in an emergency, how to use health insurance adequately, what kinds of services to expect from public and private health organisations, and where to find further information ([Courtright 2004](#cou04)). Lee _et al._ ([2010](#leejy)) explain that this is why Korean immigrants go back to their country for health care - Korean immigrants who encounter a different health care system in their adopted country experience confusion and frustration, so they seek effective health care in their native country. Moreover, language barriers have been known to compromise immigrants' health care and understanding of health information. For example, one in five Spanish-speaking Latinos reported choosing not to go to the hospital, even when they were sick ([Cashen _et al._ 2004](#cas04)) because visits to a doctor involve arduous efforts to make themselves understood and to understand in turn what the doctor is saying ([Courtright 2004](#cou04)). Similarly, Korean and Chinese women's mammography use was found to be associated with their ability to speak English ([Yu and Wu 2005](#yu05)). In addition, lack of employment-based health insurance among Korean immigrant groups causes inadequate access to health care services, especially among low-income groups ([Shin _et al._ 2005](#shi05)).

In summary, barriers to health information can result in immigrants having less access to health care or receiving lower quality health care than USA-born individuals. The first step to establishing more effective strategies for providing immigrants with appropriate health information, should be to understand how immigrants seek information.

## Health information seeking on the Internet

Research shows that the most popular resources used by immigrants for medical and health information are social networks (family and friends), mass media (TV and radio), written materials (newspapers and magazines) in their primary language ([Ahmad _et al._ 2004](#ahm04); [Chen _et al._ 2010](#che10); [Woodall _et al._ 2009](#woo09); [Woodall _et al._ 2006](#woo06)) and among some immigrant groups, healthcare professionals ([Eriksson-Backa 2008](#eri08); [Morey 2007](#mor07)). However, health information is now more available on the Internet than any other resource ([Chen _et al._ 2010](#che10)). According to the Pew Internet and American Life Project report ([Fox 2011](#fox11)), 85% of Internet users have looked online for health information. While health professionals are still considered the most appropriate source of information for health and medical issues, people increasingly use the Internet. A number of studies consider the use of online health information by various immigrant groups: Chinese living in the USA and Canada ([Woodall _et al._ 2009](#woo09)), Hispanics in the USA ([DeLorme _et al._ 2010](#del10); [Pena-Purcell 2008](#pen08)), and Spanish-speaking immigrant women in Canada ([Thomson and Hoffman-Goetz 2009](#tho09)).

One of the most interesting aspects of the Internet phenomenon is that people organise themselves and form online communities to gather information, share stories and discuss concerns with others on matters of health. One of the many reasons people rely on peers with whom they are connected through online communities is that they are dissatisfied with the information received from their physicians ([Eysenbach and Diepgen 1999](#eys99); [Himmel _et al._ 2005](#him05)). Major sources of this dissatisfaction include doctors' professional incompetency ([Umefjord _et al._ 2003](#ume03)), treatment delays or failure ([Homewood 2004](#hom04)), empathy problems with the doctor ([Homewood 2004](#hom04)), insufficient time with doctors ([Umefjord _et al._ 2003](#ume03)), and the long waiting time in hospital emergency departments and to see specialists ([Ahmad _et al._ 2004](#ahm04)). In summary, many people are not getting important needs fulfilled by their doctors. They therefore turn to online forums, for example to fulfill their unmet needs for information or emotional support ([Lee and Hawkins 2010](#leeH10)).

To understand the nature of online health communities, many researchers have analysed the messages posted to online health forums or discussion boards. Analysis of unsolicited messages has been particularly useful in revealing the reasons people ask questions in online health communities, their needs and requirements, and their expectations of, or dissatisfaction with, healthcare professionals ([Barney _et al._ 2011](#bar11)). For example, Coulson _et al._ ([2007](#cou07)) found that members of a Huntington's disease bulletin board most frequently offered informational support (providing information or advice) (56.2%) and emotional support (communicating love, concern, or empathy) (51.9%), followed by network support (communicating belonging to a group of persons with similar concerns or experiences) (48.4%). Other studies confirm informational support and emotional support are a key function of online communities ([Gooden and Winefield 2007](#goo07)). When people seek informational support, symptoms, diagnosis, treatment, and medication are major topics ([Cousineau _et al._ 2006](#cou06); [Eysenbach and Diepgen 1999](#eys99); [Himmel _et al._ 2005](#him05); [Homewood 2004](#hom04); [Shuyler and Knight 2003](#shu03); [White 2000](#whi00)).

Changrani, _et al._ ([2008](#cha08)) assert that online forums may be a promising option, particularly for immigrants, for several reasons: they can be a cost-effective method of psychological intervention from professionals with little or no travel; they address geographic isolation as well as needs for privacy, both of which may be prominent among immigrants. Considering that immigrants have identified as critical the need for health information in their primary language ([Ahmad _et al._ 2004](#ahm04); [Thomson and Hoffman-Goetz 2009](#tho09)), an online forum where immigrants can exchange information in their native language is likely to lead to improved communication of health information and thus, an enhanced sense of health and well-being. Unfortunately, research is limited on immigrants and their use of online forums for health information. In fact, there is no previous study on Korean immigrants' use of online forums.

Therefore, this study aims to analyse the questions and needs of a sub-group of an immigrant group, namely married Korean women living in the USA, who sought help for health and medical issues in an online forum. One reason for selecting this sub-group is that women are typically the primary family caregivers across all ethnic groups and they tend to be active seekers of information about health issues on the Internet ([Pena-Purcell 2008](#pen08)). Examination of married Korean women's health information needs could give a glimpse of the general needs of Korean immigrant families. Specific research questions include: What do Korean women seek when they turn to the online forum for health information? To whom do they look for health information? What is the relationship between the use of online forums and doctor visits? Additionally, this study gives insights into both married Korean women's unmet health needs and the role which an online forum plays when they are seeking health information.

## Methods

This study is based on content analysis of one thousand messages posted between November 20 2009 and September 24 2010 to a health forum on the online Website, [MissyUSA](http://www.missyusa.com). MissyUSA is the largest online community among Korean immigrants in the USA, where married Korean women living in the country share information about living in their adopted country. According to the site, MissyUSA started in 2002 and currently serves more than 270,000 members. The site contains a number of forums for discussion of various topics. In the forum, the members of the site can post messages or reply to the posted messages related to health issues. On average, 100 messages are posted to the health forum per month. From this forum, the most recently posted 1,000 messages (about ten months worth) were collected in September 2010 to take a snapshot of current health information needs of Korean married women. Since the focus of this study is on health information needs through the analysis of health questions, messages with no questions (e.g., advertisements or messages simply sharing information) were removed from the data set. Also, associated replies and personal notes exchanged privately among members were not collected.

To analyse the questions, a tentative coding system with seven categories was developed based on previous studies (Shuyler and Knight 2003; White 2000) and the health topics in [MedlinePlus](http://www.nlm.nih.gov/medlineplus/). However, as the data set has unique features which were not reflected in the tentative framework, modifications were made in the coding system by adding/deleting coding options and updating their scopes. Table 1 shows the final coding system.

<table class="center" style="width:95%;"><caption>  
Table 1: Coding system</caption>

<tbody>

<tr>

<th colspan="2">Purpose of asking questions*</th>

</tr>

<tr>

<td colspan="2">Seeking advice  
Seeking information  
Seeking interpretation of medical terminology, test results, etc.</td>

</tr>

<tr>

<th colspan="2">Relationship of enquirer to patient*</th>

</tr>

<tr>

<td colspan="2">Enquirer is the patient  
Enquirer is a family member of patient  
Enquirer is a friend of patient  
Relationship unknown</td>

</tr>

<tr>

<th colspan="2">Patients' demographic groups</th>

</tr>

<tr>

<td colspan="2">Children and teenagers  
Men  
Women  
Unknown</td>

</tr>

<tr>

<th colspan="2">Topic**</th>

</tr>

<tr>

<td colspan="2">**1) Non-medical**  

Health care expense  
Insurance  
Recommendations of hospitals or doctors  
Requests for reviews of specific hospitals or doctors  

**2) Medical**  

Diagnosis: all aspects of diagnosis, including examination and symptoms  
Diet: food issues related to the disease  
Epidemiology: incidence, prevalence, spread of disease  
Etiology: causes of disease  
Health: effects of exercise or other physical activity, general health issues, vitamins, herbs  
Medication: recommendation and use of drugs  
Prevention: all aspects of prevention including annual check-up and vaccination  
Prognosis: side-effect of treatment and/or outcome of disease  
Advice on whether to consult a doctor  
Treatment: other types of treatment besides medication</td>

</tr>

<tr>

<th colspan="2">Body location or system***</th>

</tr>

<tr>

<td>Blood, heart and circulation  
Bones, joints and muscles  
Brain and nerves  
Digestive systems  
Ear, nose and throat  
Endocrine system  
Eyes and vision  
Immune system</td>

<td>Kidneys and urinary system  
Lungs and breathing  
Mouth and teeth  
Skin, hair and nails  
Female reproductive system  
Male reproductive system  
Mental  
Multiple body parts</td>

</tr>

<tr>

<td colspan="2">Urgency</td>

</tr>

<tr>

<td colspan="2">Urgent  
Not urgent</td>

</tr>

<tr>

<th colspan="2">Before or after consulting a doctor</th>

</tr>

<tr>

<td colspan="2">Before consulting a doctor: posting questions before consulting a doctor  
After consulting a doctor: posting questions after consulting a doctor</td>

</tr>

<tr>

<td colspan="2">Note: * These categories were adapted and modified from Shuyler and Knight ([2003](#shu03)).  
** The topic categories were adapted and modified from White ([2000](#whi00)). White's coding system groups non-medical questions into one category. The sub-topics of the category were newly added in the current study.  
*** The body location/systems category was adopted and modified from [MedlinePlus](http://www.nlm.nih.gov/medlineplus/healthtopics.html)</td>

</tr>

</tbody>

</table>

The two authors applied the coding system to the halves of the data set individually. With 10% of the data set, percentage agreement was calculated to check coding consistency between the authors. The percentages of inter-coder agreement were 91.43% on average (purpose: 86%, patients' demographic group: 94%, relationship of patient to enquirer: 93%, topic: 89%, body location: 90%, before/after consulting a doctor: 91%, and urgency: 97%).

## Results

Of the 1,000 messages collected, 914 had one or more questions that called for answers. On average, one message included 1.2 questions and in total, 1,127 questions were identified.

### Purposes of asking questions

The most frequent purpose of asking questions in MissyUSA was seeking information (61.3%), followed by seeking advice (37.3%), such as asking for personal opinions or recommendations for a doctor, a hospital, medicine etc. Only 1.4% requested assistance in interpreting diagnostic tests such as magnetic resonance imaging (MRI). Examples of questions by purpose are shown in Table 2\. The prevalence of information-seeking questions in the health forum can be explained by Chuang and Yang's study ([2010](#chu10)), which examined an online alcoholism support community. They found that different types of computer-mediated communication tools promote different levels of social support: a discussion board/forum allows users to pose questions and identify others having similar problems while journals and notes allow for maintaining social relationships initially formed through the discussion board/forum. The health forum examined in this study seems to allow for information support rather than social support.

<table class="center" style="width:95%;"><caption>  
Table 2: Purposes of asking questions</caption>

<tbody>

<tr>

<th>Purpose</th>

<th>Frequency</th>

<th>%</th>

<th>Examples of questions</th>

</tr>

<tr>

<td style="vertical-align:top;">Seeking information</td>

<td style="text-align:center; vertical-align:top;">691</td>

<td style="text-align:center; vertical-align:top;">61.3</td>

<td style="vertical-align:top;">"I've lived in the USA for 4 years, but have no primary doctor. I have a health insurance, though. Does the insurance company choose a primary doctor for me? Otherwise, how can I have one?" *</td>

</tr>

<tr>

<td style="vertical-align:top;">Seeking advice</td>

<td style="text-align:center; vertical-align:top;">420</td>

<td style="text-align:center; vertical-align:top;">37.3</td>

<td style="vertical-align:top;">"Could you recommend Omega-3, multi-vitamin, and Vitamin C for two-years-old baby?"</td>

</tr>

<tr>

<td style="vertical-align:top;">Seeking an interpretation</td>

<td style="text-align:center; vertical-align:top;">16</td>

<td style="text-align:center; vertical-align:top;">1.4</td>

<td style="vertical-align:top;">"I've got a result of my blood test, but I don't understand what the result means.  
neutrophils - 44 (normal 45-70)  
lymphocytes - 48 (normal 20-40)"  
What does this mean?"</td>

</tr>

<tr>

<td>Total</td>

<td>1,127</td>

<td>100.0</td>

<td> </td>

</tr>

<tr>

<td colspan="4">*Note: The authors translated the messages originally written in Korean into English</td>

</tr>

</tbody>

</table>

### Enquirer

In 57.2% of the questions, the enquirer was the patient (Table 3). The actual percentage may be higher, as the wording of some messages left the intended information recipient unclear (15.9%). Less than one third clearly asked questions on behalf of others. For example, one person asked a question "_My husband has suffered from coughing for over a week. It is so severe that he can't even sleep at night. What medicine is the best to stop a cough?_" Only when the identity of the patient was explicitly stated, were the relationship of the enquirer to the patient and the demographic group of the patient determined and coded accordingly. About two thirds of the patients were women since many visitors to the site asked questions for themselves. The other two groups - children and teenagers, and men - amounted to 20%.

<table class="center" style="width:50%;"><caption>  
Table 3: Relationship of patient to enquirer</caption>

<tbody>

<tr>

<th>Relationship of enquirer to patient</th>

<th>Frequency</th>

<th>%</th>

</tr>

<tr>

<td>Enquirer is patient</td>

<td style="text-align:center;">645</td>

<td style="text-align:center;">57.2</td>

</tr>

<tr>

<td>Enquirer is a family member of patient</td>

<td style="text-align:center;">288</td>

<td style="text-align:center;">25.6</td>

</tr>

<tr>

<td>Enquirer is a friend of patient</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">1.3</td>

</tr>

<tr>

<td>Relationship unknown</td>

<td style="text-align:center;">179</td>

<td style="text-align:center;">15.9</td>

</tr>

</tbody>

</table>

<table class="center" style="width:50%;"><caption>  
Table 4: Demographic characteristics of patients</caption>

<tbody>

<tr>

<th>Demographic groups of patients</th>

<th>Frequency</th>

<th>%</th>

</tr>

<tr>

<td>Women</td>

<td style="text-align:center;">716</td>

<td style="text-align:center;">63.5</td>

</tr>

<tr>

<td>Children and teenagers</td>

<td style="text-align:center;">126</td>

<td style="text-align:center;">11.2</td>

</tr>

<tr>

<td>Men</td>

<td style="text-align:center;">101</td>

<td style="text-align:center;">9.0</td>

</tr>

<tr>

<td>Unknown</td>

<td style="text-align:center;">184</td>

<td style="text-align:center;">16.3</td>

</tr>

</tbody>

</table>

In the cases in which the enquirers were asking for themselves or their family members, seeking information and seeking advice were the two main purposes (Table 5). When the enquirers were friends of the patients, however, seeking information was the sole dominant purpose.

<table class="center" style="width:70%;"><caption>  
Table 5: Purpose of asking questions by relationship between enquirer and patient</caption>

<tbody>

<tr>

<th>Information  
activity</th>

<th colspan="2">Enquirer is patient</th>

<th colspan="2">Enquirer is a family member of patient</th>

<th colspan="2">Enquirer is a friend of patient</th>

<th colspan="2">Relationship unknown</th>

<th> </th>

</tr>

<tr>

<td> </td>

<td style="text-align:center;">Frequency</td>

<td style="text-align:center;">%</td>

<td style="text-align:center;">Frequency</td>

<td style="text-align:center;">%</td>

<td style="text-align:center;">Frequency</td>

<td style="text-align:center;">%</td>

<td style="text-align:center;">Frequency</td>

<td style="text-align:center;">%</td>

<td style="text-align:center;">Total</td>

</tr>

<tr>

<td>Seeking information</td>

<td style="text-align:center;">390</td>

<td style="text-align:center;">60.5</td>

<td style="text-align:center;">187</td>

<td style="text-align:center;">64.9</td>

<td style="text-align:center;">13</td>

<td style="text-align:center;">86.7</td>

<td style="text-align:center;">101</td>

<td style="text-align:center;">56.4</td>

<td style="text-align:center;">691</td>

</tr>

<tr>

<td>Seeking advice</td>

<td style="text-align:center;">249</td>

<td style="text-align:center;">38.6</td>

<td style="text-align:center;">98</td>

<td style="text-align:center;">34.0</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">13.3</td>

<td style="text-align:center;">71</td>

<td style="text-align:center;">39.7</td>

<td style="text-align:center;">420</td>

</tr>

<tr>

<td>Seeking interpretation</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">0.9</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">1.1</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">7</td>

<td style="text-align:center;">3.9</td>

<td style="text-align:center;">16</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center;">645</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">288</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">179</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">1,127</td>

</tr>

</tbody>

</table>

### Topics

Almost one third (29.6%) of the questions were non-medical. White's study (2000), on which our coding system is based, groups all non-medical questions indiscriminately into one category without further analysis. Considering the significance of the category in this study, it was further analysed and categorised into sub-topics as shown in Table 6\. This category is primarily about requests for recommendations of a hospital or a doctor. Health care expense, insurance and requests for reviews of hospitals/doctors were marginally mentioned; some enquirers wanted to estimate expected health care expense before consulting a doctor, others requested recommendations for health insurance or information on insurance coverage, policy, or process and others requested reviews of a particular hospital or doctor.

Interestingly, of the 237 questions in which the enquirer requested a recommendation for, or review of, a hospital or doctor, fifty-four questions sought information specifically about Korean doctors practicing in the USA and among them 29 questions were looking for hospitals or doctors specializing in Asian medicine. Also, twelve questions requested information about a hospital or doctor in Korea.

<table class="center" style="width:70%;"><caption>  
Table 6: Topics of questions</caption>

<tbody>

<tr>

<th>Topics</th>

<th>Frequency</th>

<th>%</th>

</tr>

<tr>

<th colspan="3">_Non-medical_</th>

</tr>

<tr>

<td>Recommendation of hospitals or doctors</td>

<td style="text-align:center;">214</td>

<td style="text-align:center;">19.0</td>

</tr>

<tr>

<td>Health care expense</td>

<td style="text-align:center;">46</td>

<td style="text-align:center;">4.1</td>

</tr>

<tr>

<td>Insurance</td>

<td style="text-align:center;">29</td>

<td style="text-align:center;">2.6</td>

</tr>

<tr>

<td>Requests for reviews of hospitals or doctors</td>

<td style="text-align:center;">23</td>

<td style="text-align:center;">2.0</td>

</tr>

<tr>

<td>Others</td>

<td style="text-align:center;">22</td>

<td style="text-align:center;">1.9</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center;">334</td>

<td style="text-align:center;">29.6</td>

</tr>

<tr>

<th colspan="3">_Medical_</th>

</tr>

<tr>

<td>Diagnosis</td>

<td style="text-align:center;">249</td>

<td style="text-align:center;">22.1</td>

</tr>

<tr>

<td>Treatment</td>

<td style="text-align:center;">172</td>

<td style="text-align:center;">15.3</td>

</tr>

<tr>

<td>Medication</td>

<td style="text-align:center;">114</td>

<td style="text-align:center;">10.1</td>

</tr>

<tr>

<td>Health</td>

<td style="text-align:center;">94</td>

<td style="text-align:center;">8.3</td>

</tr>

<tr>

<td>Advice on whether to consult a doctor</td>

<td style="text-align:center;">67</td>

<td style="text-align:center;">5.9</td>

</tr>

<tr>

<td>Diet</td>

<td style="text-align:center;">39</td>

<td style="text-align:center;">3.5</td>

</tr>

<tr>

<td>Prevention</td>

<td style="text-align:center;">25</td>

<td style="text-align:center;">2.2</td>

</tr>

<tr>

<td>Prognosis</td>

<td style="text-align:center;">13</td>

<td style="text-align:center;">1.2</td>

</tr>

<tr>

<td>Others</td>

<td style="text-align:center;">13</td>

<td style="text-align:center;">1.2</td>

</tr>

<tr>

<td>Etiology</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">0.4</td>

</tr>

<tr>

<td>Epidemiology</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">0.2</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center;">793</td>

<td style="text-align:center;">70.4</td>

</tr>

<tr>

<th>Total</th>

<th>1,127</th>

<th>100.0</th>

</tr>

</tbody>

</table>

Among medical topics, diagnosis was the most frequently asked topic, accounting for 22.1%. Most diagnosis questions described symptoms and asked "_What do you think is happening to me?_" or "_Is there anyone who's experienced this?_" This type of question was sometimes asked with a question about whether to consult a doctor. The advice on whether to consult a doctor topic was not found in White's study. This may be because in the colon cancer electronic list she examined, people who already had the cancer or cared about someone with that cancer asked the questions.

Other popular topics included treatment (15.3%), medication (10.1%), and health (8.3%). Questions in the treatment category included: 1) questions asking what they should do after explaining their symptoms -- this type of questions mostly expressed the enquirers' frustration with previous treatments, and 2) questions asking for information on a specific treatment, usually before receiving the treatment. Medication questions included 1) asking for recommendations of over-the-counter-drugs, and 2) asking for information about prescription drugs (e.g., side effects of long-term use). The health category included questions on nutritional supplements and herbal remedies, especially Asian herbal medicines. People also asked for recommendations, prices, and places to buy nutritional supplements and herbs. Questions on fitness equipment and health products (e.g., chairs, pillows, beds, etc.) were also found.

Relatively few questions were asked about diet (3.4%), prevention (2.2%), or prognosis (1.2%). Diet questions asked where a specific dietary supplement could be purchased, whether a dietary supplement would improve their health or symptoms, or which food would be good for a disease or symptom. The prevention category included questions on annual checkups or vaccines and prognosis questions were concerned with the probable course, or outcome of a disease.

### Body systems or locations and diseases represented

As women were the primary users of the site, and they tended to ask questions for themselves in the health forum, it is not surprising that the female reproductive system was the body system about which the enquirers were most concerned (19%) (Table 6).

<table class="center" style="width:60%;"><caption>  
Table 7: Body-location of concern</caption>

<tbody>

<tr>

<th>Body location</th>

<th>Frequency</th>

<th>%</th>

</tr>

<tr>

<td>Female reproductive system</td>

<td style="text-align:center;">178</td>

<td style="text-align:center;">19.0</td>

</tr>

<tr>

<td>Skin, hair and nails</td>

<td style="text-align:center;">89</td>

<td style="text-align:center;">9.5</td>

</tr>

<tr>

<td>Bones, joints and muscles</td>

<td style="text-align:center;">74</td>

<td style="text-align:center;">7.9</td>

</tr>

<tr>

<td>Blood, heart and circulation</td>

<td style="text-align:center;">73</td>

<td style="text-align:center;">7.8</td>

</tr>

<tr>

<td>Digestive system</td>

<td style="text-align:center;">69</td>

<td style="text-align:center;">7.4</td>

</tr>

<tr>

<td>Mouth and teeth</td>

<td style="text-align:center;">52</td>

<td style="text-align:center;">5.4</td>

</tr>

<tr>

<td>Ear, nose and throat</td>

<td style="text-align:center;">48</td>

<td style="text-align:center;">5.1</td>

</tr>

<tr>

<td>Kidneys and urinary system</td>

<td style="text-align:center;">32</td>

<td style="text-align:center;">3.4</td>

</tr>

<tr>

<td>Endocrine system</td>

<td style="text-align:center;">31</td>

<td style="text-align:center;">3.3</td>

</tr>

<tr>

<td>Brain and nerves</td>

<td style="text-align:center;">30</td>

<td style="text-align:center;">3.2</td>

</tr>

<tr>

<td>Immune system</td>

<td style="text-align:center;">24</td>

<td style="text-align:center;">2.6</td>

</tr>

<tr>

<td>Eyes and vision</td>

<td style="text-align:center;">22</td>

<td style="text-align:center;">2.3</td>

</tr>

<tr>

<td>Multiple body parts</td>

<td style="text-align:center;">22</td>

<td style="text-align:center;">2.3</td>

</tr>

<tr>

<td>Lungs and breathing</td>

<td style="text-align:center;">13</td>

<td style="text-align:center;">1.4</td>

</tr>

<tr>

<td>Male reproductive system</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">0.4</td>

</tr>

<tr>

<td>Mental</td>

<td style="text-align:center;">7</td>

<td style="text-align:center;">0.8</td>

</tr>

<tr>

<td>Not known</td>

<td style="text-align:center;">171</td>

<td style="text-align:center;">18.2</td>

</tr>

<tr>

<th>Total</th>

<th>939</th>

<th>100.0</th>

</tr>

</tbody>

</table>

A total of 114 types of diseases was mentioned, most of them only once. The most commonly mentioned diseases, mentioned more than five times, are listed in Table 8\. Allergy was the most frequently asked about, followed by thyroid cancer, hyperthyroidism and disc disorders. In this list, four are female-specific diseases.

<table class="center" style="width:60%;"><caption>  
Table 8: Disease or condition of concern</caption>

<tbody>

<tr>

<th>Disease or condition</th>

<th>Frequency</th>

</tr>

<tr>

<td>Allergy</td>

<td style="text-align:center;">19</td>

</tr>

<tr>

<td>Thyroid cancer, hyperthyroidism</td>

<td style="text-align:center;">17</td>

</tr>

<tr>

<td>Disc disorders</td>

<td style="text-align:center;">11</td>

</tr>

<tr>

<td>Diabetes</td>

<td style="text-align:center;">8</td>

</tr>

<tr>

<td>Breast cancer</td>

<td style="text-align:center;">8</td>

</tr>

<tr>

<td>Uterine myoma</td>

<td style="text-align:center;">8</td>

</tr>

<tr>

<td>Hemmorrhoids</td>

<td style="text-align:center;">8</td>

</tr>

<tr>

<td>Menopause</td>

<td style="text-align:center;">7</td>

</tr>

<tr>

<td>High blood pressure</td>

<td style="text-align:center;">7</td>

</tr>

<tr>

<td>Endometritis</td>

<td style="text-align:center;">7</td>

</tr>

<tr>

<td>Common cold</td>

<td style="text-align:center;">6</td>

</tr>

<tr>

<td>Irregular menstruation</td>

<td style="text-align:center;">6</td>

</tr>

</tbody>

</table>

### Before or after consulting a doctor

68.2% (n=769) of questions were asked before consulting a doctor, and only 12.9% (n=145), after consulting a doctor. 18.9% (n=213) of the questions were either hard to decide if the enquirer had already consulted a doctor or the questions were of a general medical nature, such as "_Is it true that lemons weaken bones?_"

Table 9 shows the purposes of the questions pursued before and after consulting a doctor. Enquirers sought advice more often before consulting a doctor than after. After consulting a doctor, they tended to request verification of the information received from the doctor rather than trying to get additional advice.

<table class="center" style="width:70%;"><caption>  
Table 9: Purposes before or after consulting a doctor</caption>

<tbody>

<tr>

<th rowspan="2">Purpose</th>

<th colspan="2">Before</th>

<th colspan="2">After</th>

<th colspan="2">Unknown</th>

<th> </th>

</tr>

<tr>

<th>Frequency</th>

<th>%</th>

<th>Frequency</th>

<th>%</th>

<th>Frequency</th>

<th>%</th>

<th>Total</th>

</tr>

<tr>

<td>Seeking information</td>

<td style="text-align:center;">431</td>

<td style="text-align:center;">56.0</td>

<td style="text-align:center;">96</td>

<td style="text-align:center;">66.2</td>

<td style="text-align:center;">164</td>

<td style="text-align:center;">77.1</td>

<td style="text-align:center;">691</td>

</tr>

<tr>

<td>Seeking advice</td>

<td style="text-align:center;">338</td>

<td style="text-align:center;">44.0</td>

<td style="text-align:center;">34</td>

<td style="text-align:center;">23.5</td>

<td style="text-align:center;">48</td>

<td style="text-align:center;">22.5</td>

<td style="text-align:center;">420</td>

</tr>

<tr>

<td>Seeking an interpretation</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">10.3</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0.4</td>

<td style="text-align:center;">16</td>

</tr>

<tr>

<th>Total</th>

<th>769</th>

<th>100.0</th>

<th>145</th>

<th>100.0</th>

<th>213</th>

<th>100.0</th>

<th>1,127</th>

</tr>

</tbody>

</table>

It is understandable that non-medical questions (e.g. requests for recommendations or reviews of hospitals and doctors, information on health care expense or insurance) were more common before consulting a doctor. However, questions on treatment, medication, health, and prognosis were often posted after consulting a doctor (Table 10).

<table class="center" style="width:70%;"><caption>  
Table 10: Topics before or after consulting a doctor</caption>

<tbody>

<tr>

<th rowspan="2">Topic</th>

<th colspan="2">Before</th>

<th colspan="2">After</th>

<th colspan="2">Unknown</th>

<th> </th>

</tr>

<tr>

<th>Frequency</th>

<th>%</th>

<th>Frequency</th>

<th>%</th>

<th>Frequency</th>

<th>%</th>

<th>Total</th>

</tr>

<tr>

<th colspan="8" style="text-align:left;">_Non-medical_</th>

</tr>

<tr>

<td>Recommendations of hospital or doctors</td>

<td style="text-align:center;">206</td>

<td style="text-align:center;">26.8</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">4.1</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">0.9</td>

<td style="text-align:center;">214</td>

</tr>

<tr>

<td>Health care expense</td>

<td style="text-align:center;">42</td>

<td style="text-align:center;">5.5</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">2.8</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">46</td>

</tr>

<tr>

<td>Insurance</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">1.2</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2.1</td>

<td style="text-align:center;">17</td>

<td style="text-align:center;">8.0</td>

<td style="text-align:center;">29</td>

</tr>

<tr>

<td>Requests for reviews of hospitals or doctors</td>

<td style="text-align:center;">21</td>

<td style="text-align:center;">2.7</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">1.4</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">23</td>

</tr>

<tr>

<td>Others</td>

<td style="text-align:center;">18</td>

<td style="text-align:center;">2.3</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0.7</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">1.4</td>

<td style="text-align:center;">22</td>

</tr>

<tr>

<td>_Total_</td>

<td style="text-align:center;">_296_</td>

<td style="text-align:center;">_38.5_</td>

<td style="text-align:center;">_16_</td>

<td style="text-align:center;">_11.0_</td>

<td style="text-align:center;">_22_</td>

<td style="text-align:center;">_10.3_</td>

<td style="text-align:center;">_334_</td>

</tr>

<tr>

<th colspan="8" style="text-align:left;">_Medical_</th>

</tr>

<tr>

<td>Diagnosis</td>

<td style="text-align:center;">202</td>

<td style="text-align:center;">26.3</td>

<td style="text-align:center;">39</td>

<td style="text-align:center;">26.9</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">3.8</td>

<td style="text-align:center;">249</td>

</tr>

<tr>

<td>Treatment</td>

<td style="text-align:center;">108</td>

<td style="text-align:center;">14.0</td>

<td style="text-align:center;">48</td>

<td style="text-align:center;">33.1</td>

<td style="text-align:center;">16</td>

<td style="text-align:center;">7.5</td>

<td style="text-align:center;">172</td>

</tr>

<tr>

<td>Medication</td>

<td style="text-align:center;">53</td>

<td style="text-align:center;">6.9</td>

<td style="text-align:center;">21</td>

<td style="text-align:center;">14.5</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">18.8</td>

<td style="text-align:center;">114</td>

</tr>

<tr>

<td>Health</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">0.7</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">2.8</td>

<td style="text-align:center;">85</td>

<td style="text-align:center;">39.9</td>

<td style="text-align:center;">94</td>

</tr>

<tr>

<td>Advice on whether to consult a doctor</td>

<td style="text-align:center;">67</td>

<td style="text-align:center;">8.7</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">67</td>

</tr>

<tr>

<td>Diet</td>

<td style="text-align:center;">11</td>

<td style="text-align:center;">1.4</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2.1</td>

<td style="text-align:center;">25</td>

<td style="text-align:center;">11.7</td>

<td style="text-align:center;">39</td>

</tr>

<tr>

<td>Prevention</td>

<td style="text-align:center;">14</td>

<td style="text-align:center;">1.8</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.4</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">2.8</td>

<td style="text-align:center;">25</td>

</tr>

<tr>

<td>Prognosis</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">0.3</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">5.5</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">1.4</td>

<td style="text-align:center;">13</td>

</tr>

<tr>

<td>Others</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">1.0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">2.3</td>

<td style="text-align:center;">13</td>

</tr>

<tr>

<td>Etiology</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">0.3</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">1.4</td>

<td style="text-align:center;">5</td>

</tr>

<tr>

<td>Epidemiology</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0.1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0.7</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>_Total_</td>

<td style="text-align:center;">_473_</td>

<td style="text-align:center;">_61.5_</td>

<td style="text-align:center;">_129_</td>

<td style="text-align:center;">_89.0_</td>

<td style="text-align:center;">_191_</td>

<td style="text-align:center;">_89.7_</td>

<td style="text-align:center;">_793_</td>

</tr>

<tr>

<th>Total</th>

<th>769</th>

<th>100.0</th>

<th>145</th>

<th>100.0</th>

<th>213</th>

<th>100.0</th>

<th>1,127</th>

</tr>

</tbody>

</table>

To summarise, a typical questioning behaviour in the health forum was that a woman asked a question for herself to get recommendations of hospitals or doctors or to know the cause of symptoms or treatment methods related to the female reproductive system before consulting a doctor. Unlike online health communities which revolve around a single-disease, the general online health forum examined in the current study is more geared to short-term problem solving to identify adequate hospitals or doctors or treat the symptoms the enquirers are currently experiencing.

### Urgency

Only 6.6% of the questions explicitly mentioned that the enquirer had an urgent information need and thus, an immediate answer was called for. Most of the questions did not express urgency even though some of them included the description of symptoms that sounded alarming or warranted a trip to a doctor's office.

### Dissatisfaction with thehealth care system of the USA

Some posts contained opinions of the health care system in the U.S., offering useful insights into why the enquirers were dissatisfied with the health care system and turned to online help before or instead of consulting a doctor. A major source of the dissatisfaction was the perceived inefficiency and slowness of health care services in the USA as the following comments illustrate:

> I think my symptom is obvious, but here [U.S.A], the process of a gynecological examination is too slow. They examine only one thing a month.  
>   
> I first noticed a small lump on my shoulder the other day. It didn't hurt then, but now, it is increasing and the entire arm is so sore. I'm really panicking! Since it is holiday season, my appointment is the middle of January. My arm would be decayed by then.  
>   
> Because it was Friday afternoon, there was no doctor, no nurse. All I could do was leaving a message.

Long waiting times and insufficient timely access to health care especially on Fridays and Saturdays caused high anxiety and frustration amongst enquirers. Some enquirers stated that they would have had faster services in Korea.

In addition, the enquirers experienced difficulties in choosing doctors and complained about fewer options to choose from in the USA This is caused by issues such as the general practitioner referral system and the variety of health plans which the enquirers would not have experienced in their home country.

> The pain is getting severe. Having HMO insurance, however, I can't go to any doctor. If I were in Korea, I could already be under treatment. I'm so frustrated.  
>   
> I got an outbreak of little bumps and an itchy rash on my bikini-line. Should I go see my primary doctor? He is likely to refer me to a specialist. An obstetrician doesn't seem to be a right choice. A dermatologist may be all right, but I will get tired of waiting before meeting one.

Several enquirers also mentioned that resources such as the Korean yellow pages or insurance company handbooks did not help in choosing doctors because they merely listed contact information without reviews. It appears that the lack of trusted sources of critical reviews of doctors or hospitals was one of the major reasons for requests for recommendations from other Koreans living in the USA through the online forum.

Two other reasons hindering the enquirers' access to health care emerged as high health care costs and language barriers. Financial concerns caused by high health care costs were particularly acute among people who were uninsured.

> Unlike in Korea, the health care system in the USA is not good for ordinary people like me. I never thought lack of money would prevent me from getting health care, but it has become reality to me. I'm hesitant to see a doctor with no insurance.

Those who asked questions about health care expense often showed interest in free medical examinations and federal programmes for citizens with a low-income, such as Medicaid. Many enquirers wanted to estimate expected health care expense before consulting a doctor and some of them asked if it would be more economical to receive a certain treatment or surgical operation in Korea than in the USA

> I'm planning to go to Korea this summer with my two daughters and considering having them get the cervical cancer vaccine. I hear that the cost of the vaccine is around $450 in Korea. Is it cheaper [in Korea]? How much is it in New Jersey without insurance?

It is obvious that costs are a significant factor in deciding in which country to get health care services.

Some enquirers had experienced language difficulties in communicating with doctors. They felt frustrated by not being able to explain their illness well to their doctors or to understand what doctors said.

> My primary doctor is American. Today he showed me the MRI results, but I simply didn't understand it. Would anyone translate the following results? Exactly what is my problem? What treatment will I get? Please give me a response.

## Discussion

This study shows the main purposes for using the online health forum in MissyUSA are seeking information or, less commonly, seeking advice. This is consistent with previous research which indicates that the most frequent reasons people use an online community for health and medical information are information and advice ([Eysenbach and Diepgen 1999](#eys99); [O'Connor and Johanson 2000](#oco00); [Shuyler and Knight 2003](#shu03); [Gooden and Winefield 2007](#goo07)). It is understandable that the Korean women were most interested in female-related diseases, but over 26% of them sought information for others, which corroborates previous research showing women are active in seeking information on behalf of others ([Abrahamson 2008](#abr08)).

In addition, the majority of the questions asked were about diagnosis, treatment, and recommendations of hospitals or doctors. These three topics accounted for more than the half the questions. Medical topics related to treatment, therapy, symptoms, medication, and diagnosis have been identified as major concerns in online health forums ([Cousineau _et al._ 2006](#cou06); [Eysenbach and Diepgen 1999](#eys99); [Himmel _et al._ 2005](#him05); [Homewood 2004](#hom04); [Shuyler and Knight 2003](#shu03); [White 2000](#whi00)) and this holds true for the forum examined in the current study.

The general-purpose online health forum in MissyUSA however is different from online forums oriented to particular diseases or medical conditions in several ways.

First, while single-issue online health forums include many treatment and medication questions, the most frequent question in the MissyUSA health forum was to know the cause of the symptom an enquirer was experiencing and this type of questions was often followed by the question of whether medical help was necessary to treat the symptom.

Second, the popularity of non-medical questions (e.g. recommendations and reviews of hospitals/doctors) shows that the enquirers were often interested in non-medical as well as medical topics. This category of information seeking deviates from the coding model White used (White 2000).

Third, while a well-known reason for turning to online health communities is patients' dissatisfaction with the initial advice given by their doctors ([Eysenbach, and Diepgen 1999](#eys99)), the enquirers in the current study considered the site as the first place to ask questions prior to or with no intention of consulting doctors.

In brief, the MissyUSA health forum is often used as a guide to finding an adequate doctor, getting a diagnosis, or learning about treatment options before or instead of consulting doctors. It mainly supports the intention of information seeking for immediate decision-making, confirming the findings of Chuang and Yang ([2010](#chu10)) that discussion boards or forums are usually used for posing questions and identifying others who have similar experiences, rather than maintaining social relationships.

The underlying reasons for seeking online help rather than consulting doctors in person included dissatisfaction with health care services, financial issues, and languages barriers. The dissatisfaction with health care services and their perceived inefficiency could be attributed to the enquirers' unfamiliarity with the USA health care system, and its difference to the system in their home country. Korea's health care system is characterized by the dominance of a private sector, patients' unconstrained choice of provider, and the limited functional differentiation of providers ([Noh _et al._ 2006](#noh06)). Appointments are not needed in Korea. Patients can go to any hospital, even on Saturday, meet a specialist of their choice or get referred to a specialist residing within the same hospital, and get diagnosed on that day. Korean patients are accustomed to self-diagnosis, choosing their own doctors, and checkup dates at their convenience.

Moreover, financial concerns caused by high health care costs and lack of health insurance prevented some enquirers from accessing health care. It is known that the high rate of uninsured Korean immigrants in the USA is associated with low utilisation of physician services, especially among low-income groups ([Shin _et al._ 2005](#shi05)). In contrast, the National Health Insurance programme of Korea covers the entire population in its compulsory social insurance system so that every citizen can access adequate health care services. Korean immigrants who used to be covered by this system in their native country might be puzzled by the variety of commercial health plans and high health care expenses in the USA This could feasibly lead some of the married Korean women to seek free medical examinations or federal programmes for low-income citizens or to use the online forum for self-diagnosis or self-treatment purposes.

Language barriers are also a significant problem for health care utilisation by immigrants ([Cashen _et al._ 2004](#cas04)). Indeed some enquirers in this study expressed a preference for doctors who speak Korean. The online forum provides an easy way to find such doctors particularly for novice immigrants with no social network in the USA Even those who did not explicitly seek doctors who spoke Korean expressed frustration in communicating with doctors who did not speak the language and wanted translation. The consequence of poor communication between the enquirers and the doctors who did not speak Korean led to increased misunderstanding, which led to increased feelings of frustration.

Korean immigrants carry with them expectations generated in their home country which can lead to unsatisfactory experiences with health care services in the USA This problem is compounded by language and financial barriers. This study identified several areas where the online health forum helps to resolve these problems. First, the online forum removes language barriers by allowing the enquirers to obtain health information in their native language as well as providing translation help before or after consulting a doctor. Second, the forum offers recommendations of hospitals and doctors from other Korean health consumers. Third, the forum supports self-diagnosis. Manson ([2010](#man10)) insisted that patients' interests in health information should be separable from their decision-making on medical treatments. Although the current study does not reveal how Korean married women utilise the obtained information, it seems that some want information before or instead of consulting a doctor. However, a concern is raised about the quality and accuracy of information provided by non-professional health providers. An enquirer should keep in mind that there are few safeguards in place to ensure the accuracy of peer information in an unmediated online health forum like MissyUSA and that the experience of one individual does not necessarily apply to another.

The results of this study present useful suggestions for health care providers in offering culturally and linguistically appropriate care to Korean immigrants. Health care providers need to recognise Korean immigrants' expectations of health care services, their unmet needs, and the reasons for common complaints. Providers should also increase awareness about the USA health care system among Korean immigrants and educate them properly to ultimately enhance their health literacy. More importantly, this study identified several areas of information needs, which could satisfy a significant portion of Korean immigrants' health questions: a list of Korean hospitals and doctors, insurance information, and free medical examination and federal programmes for people with a low-income, and treatment options for certain diseases. This information is currently distributed through major health information resources such as print or online directories, insurance agents, and government Websites, as well as the online forum examined in this study. Korean immigrants with little English proficiency and/or no Internet access may have difficulty accessing and understanding the information in those resources. More diverse resources should be utilised to deliver information in their native language to reach a broader population of Korean immigrants. Korean speaking professionals, such as physicians or medical librarians, should provide access to the information in a more effective and systematic way and through more diverse resources including ethnic media such as newspapers, TV, and brochures for new immigrants.

## Limitations and future research

The following are this study's limitations. First, this study examined the messages posted to a single online community, MissyUSA, created for married Korean women living in the USA This means that the results of the study may not be representative of the health information needs of the entire Korean immigrant population. Future research would benefit from the use of a larger representative sample of Korean immigrants so that the results can be generalised. Second, using content analysis only, it was not possible to identify detailed socio-demographic data of the enquirers such as age, education level, and the length of stay in the USA From previous studies, it is known that people who use the Internet for health information are relatively younger and more educated than those who do not (Lenquirer _et al._ 2005). Therefore, it is conjectured that the enquirers in the forum are relatively young educated married women. Future research may consider the use of a survey or other data collection methods to gather enquirers' socio-demographic information and associate it to their questioning behaviour in the forum. Future research should also examine the role of an online health forum in the broader context of health information seeking. The usefulness of interpersonal sources, libraries, health insurance companies, and public health agencies for the same type of information could be examined. Knowing the comparative importance of health online forums among various health information sources would provide a more complete picture of Korean immigrants' health information seeking behaviour.

In spite of these limitations, this study contributes to the better understanding of immigrants' use of an online forum for health information and shows how the Internet, specifically an online forum, has the potential to empower immigrants and play a role in improving their health outcomes.

## About the authors

Soojung Kim is an Assistant Professor in the Department of Library and Information Science, Chonbuk National University, Jeonju, South Korea. She received her PhD from the College of Information Studies at the Univeristy of Maryland, College Park. She can be contacted at [kimsoojung@jbnu.ac.kr](mailto:kimsoojung@jbnu.ac.kr).

JungWon Yoon is an Assistant Professor in the School of Information, University of South Florida. She received her Bachelor's degree in Library and Information Science and Master of Library and Information Science from Ewha Womans University, Seoul, Korea, and her Ph.D. in Information Science from the University of North Texas. She can be contacted at [jyoon@usf.edu](mailto:jyoon@usf.edu).

## Acknowledgement

The authors thank to Ms. Kelsey Adams, a master's student at the University of South Florida, for her assistance in editing the paper and enabling the authors to satisfy the style requirements of the journal.

#### References

*   Ahmad, F., Shik, A., Vanza, R., Cheung, A., George, U. & Stewart, D.E. (2004). Popular health promotion strategies among Chinese and East Indian immigrant women. _Women & Health_, **40**(1) 21-40.
*   Abrahamson, J.A., Fisher, K.E., Turner, A.G., Durrance, J.C. & Turner, T.C. (2008). Lay information mediary behavior uncovered: exploring how nonprofessionals seek health information for themselves and others online. _Journal of the Medical Library Association_, **96**(4), 310-323.
*   Barney, L.J., Griffiths, K.M. & Banfield, M.A. (2011). [Explicit and implicit information needs of people with depression: a qualitative investigation of problems reported on an online depression support forum](http://www.webcitation.org/66HZdbPWs). _BMC Psychiatry_, **11**, 88\. Retrieved 19 March 2010 from http://www.biomedcentral.com/1471-244X/11/88 (Archived by WebCite® at http://www.webcitation.org/66HZdbPWs)
*   Cashen, M.S., Dykes, P. & Gerber, B. (2004). eHealth technology and Internet resources: barriers for vulnerable populations. _Journal of Cardiovascular Nursing_, **19**(3) 209-214.
*   Changrani, J., Lieberman, M., Golant, M., Rios, P., Damman, J. & Gany, F. (2008). Online cancer support groups: experiences with underserved immigrant Latinas. _Primary Psychiatry_, **15**(10), 55-62.
*   Chen, C.J., Kendall, J. & Shyu, Y.I. (2010). Grabbing the rice straw: health information seeking in Chinese immigrants in the United States. _Clinical Nursing Research_, **19**(4), 335-353.
*   Chuang, K.Y. & Yang, C.C. (2010). Helping you to help me: exploring supportive interaction in online health community. _Proceedings of the American Society for Information Science and Technology_, **47**(1), 1-10.
*   Coulson, N.S., Buchanan, H. & Aubeeluck, A. (2007). Social support in cyberspace: A content analysis of communication within a Huntington's disease online support group. _Patient Education & Counseling_, **68**(2), 173-178.
*   Courtright, C. (2004). [Health information-seeking among Latino newcomers: an exploratory study](http://www.webcitation.org/60GpMWdUP). _Information Research_, **10**(2). paper 224\. Retrieved 15 July 2011 from http://informationr.net/ir/10-2/paper224.html (Archived by WebCite® at http://www.webcitation.org/60GpMWdUP)
*   Cousineau, T.M., Rancourt, D. & Green, T.C. (2006). Web chatter before and after the women's health initiative results: a content analysis of on-line menopause message boards. _Journal of Health Communication_, **11**(2), 133-147.
*   DeLorme, D.E., Huh, J. & Reid, L.N. (2010). Evaluation, use, and usefulness of prescription drug information sources among Anglo and Hispanic Americans. _Journal of Health Communication_, **15**(1), 18-38.
*   Eriksson-Backa, K. (2008). [Access to health information: perceptions of barriers among elderly in a language minority](http://www.webcitation.org/60GpQme49). _Information Research_, **13**(4), paper 368\. Retrieved 15 July 2011 from http://InformationR.net/ir/13-4/paper368.html (Archived by WebCite® at http://www.webcitation.org/60GpQme49)
*   Eysenbach, G. & Diepgen, T.L. (1999). Patients looking for information on the Internet and seeking teleadvice: motivation, expectations, and misconceptions as expressed in e-mails sent to physicians. _Archives of Dermatology_, **135**(2), 151-156.
*   Fox, S. (2011). _[The social life of health information 2011](http://www.webcitation.org/60BuRHcpS)_. Washington, DC: Pew Internet & American Life Project. Retrieved 15 July 2011 from http://www.pewinternet.org/~/media//Files/Reports/2011/PIP_Social_Life_of_Health_Info.pdf (Archived by WebCite® at http://www.webcitation.org/60BuRHcpS)
*   Gany, F.M., Herrera, A.P., Avallone, M. & Changrani, J. (2006). Attitudes, knowledge, and health-seeking behaviors of five immigrant minority communities in the prevention and screening of cancer. A focus group approach. _Ethnicity and Health_, **11**(1), 19-39.
*   Gooden R.J. & Winefield H.R. (2007). Breast and prostate cancer online discussion boards - a thematic analysis of gender differences and similarities. _Journal of Health Psychology_, **12**(1), 103-114.
*   Himmel, W., Meyer, J., Kochen, M.M. & Michelmann, H.W. (2005). [Information needs and visitors' experience of an Internet expert forum on infertility](http://www.webcitation.org/66HaFN99C). _Journal of Medical Internet Research_, **7**(2), e20\. Retrieved 19 March 2012 from http://www.jmir.org/2005/2/e20/ (Archived by WebCite ® at http://www.webcitation.org/66HaFN99C)
*   Homewood, J. (2004). Consumer health information e-mails: content, metrics and issues. _Aslib Proceedings_, **56**(3), 166-179.
*   Lenquirer, J.N., Sogolow, E.D. & Sharim, R.R. (2005). [The role of an online community for people with a rare disease: content analysis of messages posted on a primary biliary cirrhosis mailinglist](http://www.webcitation.org/66HaTOJs8). _Journal of Medical Internet Research_, **7**(1), e10\. Retrieved 19 March 2012 from http://www.jmir.org/2005/1/e10/ (Archived by WebCite® at http://www.webcitation.org/66HaTOJs8)
*   Lee, S. Y. & Hawkins, R. (2010). Why do patients seek an alternative channel? The effects of unmet needs on patients' health-related Internet use. _Journal of Health Communication_, **15**(2), 152-166.
*   Lee, J.Y., Kearns, R.A. & Friesen, W. (2010). Seeking affective health care: Korean immigrants' use of homeland medical services. _Health & Place_, **16**(1), 108-115.
*   Manson, N.C. (2010). Why do patients want information if not to take part in decision making? _Journal of Medical Ethics_, **36**(12), 834-837.
*   Martin, P. & Midgley, E. (2010, June). [Immigration in America 2010](http://www.webcitation.org/66HaknpJy). _Population Bulletin Update_. Retrieved 30 September 2011 from http://www.prb.org/pdf10/immigration-update2010.pdf (Archived by WebCite® at http://www.webcitation.org/66HaknpJy)
*   Morey, O. (2007). [Health information ties: preliminary findings on the health information seeking behaviour of an African-American community](http://www.webcitation.org/60GpUrA0w). _Information Research_, **12**(20), paper 297\. Retrieved 15 July 2011 from http://InformationR.net/ir/12-2/paper297.html (Archived by WebCite® at http://www.webcitation.org/60GpUrA0w)
*   Noh, M., Lee, Y., Yun, S., Lee, S., Lee, M. & Khang, Y. (2006). Determinants of hospital closure in South Korea: use of hierarchical generalized linear model. _Social Science and Medicine_, **63**(9) 2320-2329.
*   O'Connor, J.B. & Johanson, J.F. (2000). Use of the Web for medical information by a gastroenterology clinic population. _Journal of the American Medical Association_, **284**(15), 1962-1964.
*   Pena-Purcell, N. (2008). Hispanics' use of Internet health information: an exploratory study. _Journal of the Medical Library Association,_ **96**(2), 101-107.
*   Sadler, G.R., Nguyen, F., Doan, Q., Au, H. & Thomas, A.G. (1998). Strategies for reading Asian Americans with health information. _American Journal of Preventative Medicine_, **14**(3) 224-228.
*   Shin, H., Song, H., Kim, J. & Probst, J. C. (2005). Insurance, acculturation, and health services utilization among Korean-Americans. _Journal of Immigrant Health_, **7**(2), 65-74.
*   Shuyler, K.S. & Knight, K.M. (2003). [What are patients seeking when they turn to the internet? qualitative content analysis of questions asked by visitors to an orthopaedics web site](http://www.webcitation.org/66HcXbbEy). Journal of Medical Internet Research, 5(4), e24\. Retrieved 19 March 2012 from http://www.jmir.org/2003/4/e24/ (Archived by WebCite® at http://www.webcitation.org/66HcXbbEy)
*   Terrazas, A. (2009). _[Korean immigrants in the United States](http://www.webcitation.org/60BuYNWt8)_. Washington, DC: Migration Policy Institute. Retrieved 17 July 2011 from http://www.migrationinformation.org/usfocus/display.cfm?ID=716\. (Archived by WebCite® at http://www.webcitation.org/60BuYNWt8)
*   Thomson, M.D. & Hoffman-Goetz, L. (2009). Acculturation and cancer information. preferences of Spanish-speaking immigrant women to Canada: a qualitative study. _Health Care for Women International_, **30**(12), 1131-1151.
*   Umefjord, G., Petersson, G. & Hamberg, K. (2003). [Reasons for consulting a doctor on the Internet: Web survey of users of an Ask the Doctor service](http://www.webcitation.org/66Hc8JHlW). _Journal of Medical Internet Research_, **5**(4), e26\. Retrieved 19 March 2012 from http://www.jmir.org/2003/4/e26/ (Archived by WebCite® at http://www.webcitation.org/66Hc8JHlW)
*   USA _Census Bureau_. (2012). _2010 American community survey_. Washington, DC: USA Census Bureau. Retrieved 23January 2012 from http://factfinder2.census.gov/faces/nav/jsf/pages/searchresults.xhtml?refresh=t#
*   White, M.D. (2000). Questioning behavior on a consumer health electronic list. _Library Quarterly_, **70**(3), 302-334.
*   Woodall, E.D., Taylor, V.M., Teh, C., Li, L., Acorda E., Tu, S.P., Yasui, Y. & Hislop, G. (2009). Sources of health information among Chinese immigrants to the Pacific Northwest. _Journal of Cancer Education_, **24**(4), 334-340.
*   Woodall, E.D., Taylor, V.M., Yasui, Y., Ngo-Metzger, Q., Burke, N., Thai, H. & Jackson, J.C. (2006). Sources of health information among Vietnamese American men. _Journal of Immigrant and Minority Health_, **8**(3) 263-271.
*   Yu, M.Y. & Wu, T.Y. (2005). Factors influencing mammography screening of Chinese American women. _Journal of Obstetric, Gynecologic & Neonatal Nursing_, **34**(3), 386-394.
*   Zhao, X. (2010). Cancer information disparities between U.S.- and foreign-born populations. _Journal of Health Communication_, **15**(1), 5-21.