#### vol. 20 no. 2, June, 2015

# Using digital libraries non-visually: understanding the help-seeking situations of blind users

#### [Iris Xie](#author) and [Rakesh Babu](#author)  
School of Information Studies, University of Wisconsin-Milwaukee, 2025 E Newport, Milwaukee, WI 53211, USA  
[Soohyung Joo](#author)  
School of Library and Information Science, University of Kentucky, 352 Little Library Bldg., Lexington, KY 40506, USA  
[Paige Fuller](#author)  
School of Information Studies, University of Wisconsin-Milwaukee, 2025 E Newport, Milwaukee, WI 53211, USA

#### Abstract

> **Introduction**. This study explores blind users' unique help-seeking situations in interacting with digital libraries. In particular, help-seeking situations were investigated at both the physical and cognitive levels.  
> **Method**. Fifteen blind participants performed three search tasks, including known-item search, specific information search, and exploratory search, using the selected digital library. Pre-questionnaire, pre- and post-interviews, transaction logs and think-aloud protocols were used to collect data.  
> **Analysis**. Open coding analysis was used to identify help-seeking situations the physical and cognitive levels.  
> **Results**. The study identified seventeen help-seeking situations that blind users encountered while using digital libraries, including nine at the physical level and eight at the cognitive level. To be more specific, physical help-seeking situations were categorised into 1) difficulty accessing information, 2) difficulty identifying current status and path, and 3) difficulty evaluating information efficiently. Cognitive help-seeking situations were classified into 1) confusion about multiple programs and structures, 2) difficulty understanding information, 3) difficulty understanding or using digital library features, and 4) avoidance of specific formats or approaches.  
> **Conclusion**. The identified help-seeking situations reveal a gap between current digital library design practices and special needs of blind users. Practical implications for the design of help features for more blind-friendly digital libraries are suggested based on the findings.

## Introduction

Digital libraries have emerged as one of the information retrieval systems that produce and provide a wide variety of digitised resources available to diverse user groups via the Web. Many libraries, museums, or other organisations have digitsd different collections, such as pictures, books, audio files, and video clips, to make them available on the Web. In this study, Digital libraries are defined as the collections of digitized or digitally born items that are stored, managed, serviced, and preserved by libraries or cultural heritage institutions, excluding the digital content purchased from publishers ([Matusiak, 2012)](#mat12). Digital libraries present a variety of resources created in digital format as well as those converted from analogue materials through digitisation efforts, including print materials, manuscripts, images, audio, and video. Digital libraries are highly dynamic and ephemeral in technical, collection and informational needs. In addition, the digital library structure is by nature highly complex, in that the content of digital libraries is heterogeneous in its format and system dimensions ([Fox and Urs, 2002](#fox02); [O’Day and Nardi, 2003](#oda03)). This complexity can cause problems in information retrieval, giving rise to help-seeking situations for the user, especially for blind users, who rely on screen readers to access digital libraries.

Help mechanisms play a critical role in overcoming these challenges. Help mechanisms refer to the collection of explicit and implicit system features that guide users in effective and efficient systems interaction ([Xie and Cool, 2009](#xie09)). Since digital libraries pervade the information society, it is imperative they include help mechanisms appropriate for blind users.

The blind comprise an atypical group of users that employs unique cognitive and interaction strategies in digital environments ([Babu, 2011](#bab11)). In this study, a _blind user_ is someone without the sight necessary to see information on a computer screen or point and click with the mouse. Blind users rely on screen readers to interact with computers and smart phones. The screen reader is a text-to-speech software that identifies text content on the screen and presents this aurally through a synthetic voice ([Di Blas, Paolini, and Speroni, 2004](#dib04)). It offers specialised keyboard shortcuts for a range of operations. Such shortcuts, along with those offered by the operating system, enable the user to operate the computer non-visually. Commonly used screen readers in terms of market share are JAWS (50%), NVDA (18%), and VoiceOver (10%) ([WebAIM, 2014](#web14)).

For the blind user, Web interaction is a listening activity. Arriving on a Web site, she typically hears three screen reader announcements: title of the Web page, composition of the page in terms of interface objects (e.g., _Page has three frames, two headings, twenty links, three tables,..._), and all text information available on the page serially from top left to bottom right ([Babu, 2011](#bab11), [2013a](#bab13a), [2013b](#bab13b)). The Tab key moves focus to the next link or form control, and then announces its label ([Buzzi, Buzzi, Leporini, and Akhter, 2010](#buz10)). Interestingly, the screen reader presents a link or form control (e.g., button, checkbox, input field) in a dedicated line. Consequently, the user perceives a single line of text with hyperlinked words to be spread over multiple lines. Numerous keyboard commands are available for a multitude of operations, majority of which are rarely used ([Leuthold, Bargas-Avila, and Opwis, 2008](#leu08)).

The above simplistic description of Web interaction experience with a screen reader is intended to help the reader gain a basic understanding of how a blind user may engage with a digital library. It is noteworthy that other screen readers employ different interaction styles. One aspect worth mentioning of the VoiceOver interaction model that sets it apart from other commonly used screen readers is the Web Rotor. The Web Rotor makes navigation across different interface elements on a Web page easier for a Mac user. The user can invoke this feature with the Control+Option+U key combination on the Web page. This presents a heads up display containing multiple lists that each corresponds to a specific element type (e.g. section headers, images, visited links, tables, frames, and form controls in that order). She can cycle between different element lists with the Left and Right Arrows, and between items of a list with the Up and Down Arrows. Pressing the Return key on an item at keyboard focus takes the user to that item on the page for interaction.

In the field of digital libraries, there has been little research that investigates the help needs of blind users. Researchers have made practical contributions to the improvement of the accessibility to blind users of Web information systems. However, less research has been conducted to systematically understand specific help-seeking situations of blind users in interacting with digital libraries. This study addresses the research problem that help-seeking situations have not been well understood in the context of blind users' searching digital libraries. In order to better understand blind users' help needs, the authors intend to investigate the problems that blind users face in interacting with digital libraries. In addition, digital library design implications are discussed based on the findings.

## Literature review

This section presents a summary of relevant literature on help-seeking situations and blind users’ Web experiences.

### Help-seeking in information retrieval

In the discipline of information retrieval, researchers have studied types of help, as well as situations in which users seek help. In this study, _help-seeking situation_ refers to users needing some sort of help to pull through the problematic circumstances within the search process that prevent them from achieving their search goals. Users engage in the process of information searching with an information retrieval system to achieve their tasks or goals, and find themselves having to consult some sort of help in that process ([Xie and Cool, 2006](#xie06); [2009](#xie09)). The provider of help can be either humans or systems. Human help might be ideal if the human intermediary has sufficient skills in information retrieval to assist others, but it is expensive and training is required to ensure the quality of help providers. This study focuses on system help, which is broadly available to any online user. System help exists into two types, explicit help and implicit help.

Explicit help generally indicates separate help sections that are usually labelled "help" in an information retrieval system. Previous literature addressed the ineffectiveness of explicit help in information retrieval systems. Explicit help is often perceived as inadequate in many cases to assist users to solve context-sensitive problems ([Trenner, 1989](#tre89); [Dworman and Rosenbaum, 2004](#dwo04)). Such ineffectiveness of explicit help often results in infrequent use of help ([Monopoli, Nicholas, Georgiou and Korfiati, 2002](#mon02)). Although many users acknowledge the importance of system help, searchers perceive the existing help as insufficiently useful, and consequently tend to avoid the use of help in the search process ([Xie and Cool, 2006](#xie06); [Wu, 2010](#wu10)). Dworman and Rosenbaum ([2004](#dwo04)) argued that explicit help would not be useful if it does not contain useful and well-organized help information.

If users infrequently use explicit help, then implicit help can be a compelling alternative as a means of supporting users. Implicit help includes a broader range of system features. Implicit help is not direct instructions, but it still provides assistance to users. Examples include lists of frequently asked questions (FAQ), information feedback, and search tips ([Trenner, 1989](#tre89); [Liew, 2011](#lie11)). Previous studies show that searchers often prefer implicit, context-sensitive help mechanisms, such as relevance feedback, term weighting, and term suggestion, rather than explicit help pages ([Othman and Halim 2004](#oth04); [Liew, 2011](#lie11)). In addition, implicit context-sensitive help can improve search performance, since it is provided at the right time and situation ([Kriewel and Fuhr, 2007](#kri07); [Moraveji, Russell, Bien, and Mease, 2011](#mor11)).

Previous literature identifies the help-seeking situations that users encounter in conducting different types of sub-tasks during the search process. Searchers' help needs differ by type of sub-task during the information retrieval process ([Kim, 2006](#kim06)). While conducting searches, searchers often encounter help-seeking situations and look for some sorts of help for their query-related tactics. In particular, searchers experience difficulties articulating information needs into search statements. Unstructured search needs are often a major factor that hinders users from constructing effective search statements ([Bundorf, Wagner, Singer and Baker, 2006](#bun06)).

Query creation usually requires some level of domain knowledge, system knowledge, and information retrieval skills ([Hsieh-Yee, 1993](#hsi93); [Wildemuth, 2004](#wil04); [Hembrooke, Granka, Gay and Liddy, 2005](#hem05)), and system help supplies an aid to increase the associated knowledge of users in creating more appropriate terms for their search goals. For example, Savenkov and Agichtein ([2014](#sav14)) found searchers often seek help when reformulating their current queries. Poor structure or imprecise search statements lead to irrelevant search results, which place searchers in help-seeking situations to refine the search terms ([Keselman, Browne and Kaufman, 2008](#kes08)). In particular, help is required especially when searchers need to specify or broaden queries, find related terms, or correct misspellings ([Zeng, Crowell, Plovnick, Kim, Ngo and Dibble, 2006](#zen06); [Hu, Lu and Joo, 2013](#hu13)). Several researchers insisted that a range of help features are needed for users refining search results, such as context-sensitive search tips, tutorial sections, FAQ, relevance feedback, and thesaurus-driven term expansion ([Othman and Halim, 2004](#oth04); [Zeng, _et al.,_ 2006](#zen06)).

Help is needed in navigation. Uncertainty, unclearness, or confusion are the main reason why searchers look for help during navigation. For example, Vigo and Harper ([2013](#vig13)) considered unexpected banners, lack of information, or unfamiliar content as the trouble situations that prevent searchers from reaching the target information. Unclear structure also leads searchers into help-seeking situations. Herrouz, Khentout, and Djoudi ([2013](#her13)) distinguished between disorientation and the state of being lost during navigation. They identified three types of situations: (1) do not know where to go, (2) we know where to go but not know how to get there, and (3) we do not know the overall structure of the site. In addition, the complexity and length of navigational paths to the target information raises help-seeking situations because more navigational choices and judgement would affect navigation efficiency, cognitive overload, and lostness ([Blustein, Ahmed and Instone, 2005](#blu05); [Kammerer, Scheiter and Beinhauer, 2008](#kam08)). Previous literature suggested various system features to support users' navigation, such as toolbar options, footprints, breadcrumbs, and adaptive navigation ([Wexelblat and Maes, 1999](#wex99); [Zeilger and Esnault, 2008](#zei08)).

Some of help-seeking situations are related to the subtask of search result evaluation. Searchers often come across help-seeking situations when they need to assess the relevance or quality of information (Arora, Hesse, Rimer, Viswanath, Clayman, and Croyle, 2008). Searchers need help to identify criteria for evaluating search results or individual items (Xie and Benoit 2013). One of the typical help-seeking situations is when there are too many search results. A large amount of search results can lead to information overload (Gwizdka, 2010). Faceted display of search results is an effective way to help users assess search results when the result list is lengthy (Mu, Ryu, and Lu, 2011). The help-seeking situation in individual item evaluation is usually related to searchers' domain knowledge. Limited topic knowledge or topic interest makes searchers to perceive a search task difficult (Liu and Kim, 2013), whereas searchers with higher topic knowledge tend to be more effective and successful in the information retrieval process (Blackmon, Kitajima, and Polson, 2005).

Less research has been conducted in the context of digital libraries. Xie and Cool (2009) explored different types of help-seeking situations unique to digital library systems. They identified fifteen specific types of help-seeking situations from qualitative analysis of 120 subjects' interaction with digital libraries. Those situations were classified into seven categories in which users were unable to complete a task without a certain type or types of help: (1) inability to get started, (2) inability to identify relevant digital collections, (3) inability to browse for information, (4) inability to construct search statements, (5) inability to refine searches, (6) inability to monitor searches, and (7) inability to evaluate search results. Matusiak ([2012](#mat12)) pointed out the usability issues that put users into help-seeking situations in using digital libraries. The complexity of digital libraries is a barrier underscored when users express difficulties in navigating a digital library site.

### Accessibility and usability of the Web for blind users

It is widely believed that the Web is sight-centred by design, and thereby presents significant interaction problems for blind users ([Babu, 2013a](#bab13a), [2013b](#bab13b); [Bradbord and Peters, 2008](#bra08); [Leuthold, _et al._, 2008](#leu08)). Babu ([2011](#bab11)) conducted an in-depth examination of the interaction experiences of blind users with a Web-based learning system. Analysis revealed seven major problems that blind users are likely to face. These include: (1) confusion while navigating across pages with frame-based structure; (2) inability to tell if a new page was available in the absence of feedback for link activation; (3) difficulty submitting online forms when selection controls do not follow standard keyboard navigation procedure; (4) inability to negotiate security information pop-up that use alert dialogue box; (5) disorientation on encountering text formatting toolbar; (6) difficulty locating text areas with inappropriate labels and surrounding clutter; and (7) prone to unintended exit from application when Backspace triggers browser’s Back button.

Navigation problems are a significant hurdle for blind users. Leuthold. _et al._ ([2008](#leu08)) conducted a qualitative field study with 39 blind Web users. They found that blind users are likely to face three major problems in Website navigation: (1) inability to determine relationships between primary and secondary navigation items expressed visually; (2) difficulty recognising navigation options recurrent across pages; and (3) difficulty exploring navigation options available. Structural problems in browsing is another navigation issue ([Salampasis, Kouroupetroglou and Manitsaris, 2005](#sal05)). Moreover, important cues embedded in colour, images and videos that aid in navigation and interpretation are lost ([Leuthold, _et al.,_ 2008](#leu08)).

Many of the help-seeking situations are caused by blind users’ reliance on screen reader. Chandrashekar ([2010](#cha10)) reported that Web interaction with the screen reader is significantly more complicated than doing so by sight, creating cognitive overload for the blind user. Barreto ([2008](#bar08)) pointed out that screen readers only offer textual contents, and they are only able to represent graphic content based on the description provided by the creators. Moreover, some of the key information embedded in the layout of the web page is not well presented. Lazar and others ([2007](#laz07)) employed the Time Diary method to investigate the Web experiences of 100 blind subjects. They found that this experience can be quite frustrating owing to: confusing screen reader feedback in complex layouts; conflict between the screen reader and Web applications; poorly designed and unlabelled forms; visual items without alternative text descriptions; misleading links; inaccessible PDFs; and screen reader crashes ([Lazar, Allen, Kleinman and Malarkey, 2007](#laz07)).

Blind users encounter more problems in searching for information and interacting with dynamic websites. Jones, Farris, Elgin, Anders and Johnson ([2005](#jon05)) employed verbal protocol analysis to study the Web interaction experience of a blind user. They found that a blind user is likely to face four major challenges in searching for information online: (1) executing actions (e.g., typing and performing key commands) takes time; (2) recognising the state of the website; (3) perceiving the response of the website to an action; and (4) evaluating a page for relevant information. Bigham, Cavender, Brudvik, Wobbrock, and Lander ([2007](#big07)) found that blind users experience significant difficulty in websites that employ dynamic features (e.g., AJAX, JavaScript, Flash, etc.). Giraud, Colombi, Russo, and Therouanne ([2011](#gir11)) reported that this problem with dynamic features impedes blind users’ ability to share information in real time.

The comparison study between information seeking behavior of visually impaired users relying on screen readers and sighted users further reveals the problem. Sahib, Tombros, and Stockman ([2011](#sah11)) compared information seeking behavior between visually impaired users and sighted users focusing on four search stages, including query formulation, search results exploration, query reformulation, and search results management. Among the four stages, they found that significant differences existed in query formulation. For example, compared to sighted users, visually impaired users entered a lower number of queries, and in particular, they used querying support features less frequently, such as query suggestions and spelling suggestions. Also, visually impaired users showed limited exploration of search results.

In summary, literature explains that blind users’ Web interactions are inherently challenging due to sight-centred design practices. Most of the help-seeking research has focused on sighted users, and no systematic research that investigates the experiences of blind users in interacting with digital libraries. Consequently, there is a literature gap about the help-seeking situations that blind users face in retrieving information from a digital library.

Research problem and question

Universal accessibility of digital libraries is the goal. The limitation of the existing research calls for the need to investigate help-seeking situations of blind users in their interactions with digital libraries. Without such understanding, we cannot develop effective help mechanisms to aid blind users’ information retrieval process. This exploratory research involves an in-depth investigation of blind users’ experiences interacting with a digital library for a better understanding of their help-seeking situations in information retrieval. Specifically, it intends to answer the following research question:

> What are the types of help-seeking situations that blind users face in interacting with digital libraries?

## Research methods

### Sample

Fifteen blind Web users were recruited from a city in the Midwest of the United States. They responded to the fliers distributed to different regional blind associations. In the description in the flyer, the requirements for potential subjects were stated as: (a) they must use a screen reader to access the Internet, (b) they must have at least three years of experience in using the Internet, and (c) they must be 18 years and older. Subjects were invited to the usability testing lab in an iSchool of a state university. One hundred dollars were given for the completion of the participation, which took up approximately three to four hours for each session including pre-questionnaire, pre-and post-interviews and three search tasks. Table 1 summarises the demographic information and Internet literacy of the subjects. On average, subjects have 13.8 years of Internet use experience, and most of them use the Internet on a daily basis.

<table class="center"><caption>  
Table 1: Demographic characteristics of the sample</caption>

<tbody>

<tr>

<th>Characteristics</th>

<th>Category</th>

<th>Frequency (%)</th>

</tr>

<tr>

<td rowspan="5" style="text-indent:20px;">Age</td>

<td style="text-align:center;">18-29</td>

<td style="text-align:center;">2 (13.3)</td>

</tr>

<tr>

<td style="text-align:center;">30-39</td>

<td style="text-align:center;">0 (0.0)</td>

</tr>

<tr>

<td style="text-align:center;">40-49</td>

<td style="text-align:center;">2 (13.3)</td>

</tr>

<tr>

<td style="text-align:center;">50-59</td>

<td style="text-align:center;">6 (40.0)</td>

</tr>

<tr>

<td style="text-align:center;">60 & over</td>

<td style="text-align:center;">5 (33.3)</td>

</tr>

<tr>

<td rowspan="2" style="text-indent:20px;">Sex</td>

<td style="text-align:center;">Female</td>

<td style="text-align:center;">8 (53.3)</td>

</tr>

<tr>

<td style="text-align:center;">Male</td>

<td style="text-align:center;">46.7</td>

</tr>

<tr>

<td rowspan="3" style="text-indent:20px;">Ethnicity</td>

<td style="text-align:center;">Caucasian</td>

<td style="text-align:center;">13 (86.7)</td>

</tr>

<tr>

<td style="text-align:center;">African American</td>

<td style="text-align:center;">1 (6.7)</td>

</tr>

<tr>

<td style="text-align:center;">Asian</td>

<td style="text-align:center;">1 (6.7%)</td>

</tr>

<tr>

<td style="text-indent:20px;">Years of Internet use</td>

<td style="text-align:center;">Average years</td>

<td style="text-align:center;">13.8 (5.46)</td>

</tr>

<tr>

<td rowspan="4" style="text-indent:20px;">Information search skills</td>

<td style="text-align:center;">Beginner</td>

<td style="text-align:center;">1 (6.7)</td>

</tr>

<tr>

<td style="text-align:center;">Intermediate</td>

<td style="text-align:center;">9 (60.0)</td>

</tr>

<tr>

<td style="text-align:center;">Advanced</td>

<td style="text-align:center;">5 (33.3)</td>

</tr>

<tr>

<td style="text-align:center;">Expert</td>

<td style="text-align:center;">0 (0.0)</td>

</tr>

<tr>

<td rowspan="4" style="text-indent:20px;">Frequency of Internet use</td>

<td style="text-align:center;">Rarely use</td>

<td style="text-align:center;">0 (0.0)</td>

</tr>

<tr>

<td style="text-align:center;">Occasionally use</td>

<td style="text-align:center;">1 (6.7)</td>

</tr>

<tr>

<td style="text-align:center;">Often use</td>

<td style="text-align:center;">0 (0.0)</td>

</tr>

<tr>

<td style="text-align:center;">Use daily</td>

<td style="text-align:center;">14 (93.3)</td>

</tr>

<tr>

<td rowspan="3" style="text-indent:20px;">Vision lost</td>

<td style="text-align:center;">Congenital blind</td>

<td style="text-align:center;">11 (73.3)</td>

</tr>

<tr>

<td style="text-align:center;">Acquired blind</td>

<td style="text-align:center;">3 (20)</td>

</tr>

<tr>

<td style="text-align:center;">No response</td>

<td style="text-align:center;">1 (6.7)</td>

</tr>

<tr>

<td style="text-indent:20px;">Language</td>

<td style="text-align:center;">Native English</td>

<td style="text-align:center;">15 (100)</td>

</tr>

<tr>

<td rowspan="3" style="text-indent:20px;">Computer use experience before losing sight</td>

<td style="text-align:center;">Yes</td>

<td style="text-align:center;">1 (6.7)</td>

</tr>

<tr>

<td style="text-align:center;">No</td>

<td style="text-align:center;">13 (86.7)</td>

</tr>

<tr>

<td style="text-align:center;">No response</td>

<td style="text-align:center;">1 (6.7)</td>

</tr>

</tbody>

</table>

### Data collection procedures

This study was designed to investigate users' help-seeking situations in searching digital libraries. American Memory Digital Collections (http://memory.loc.gov/ammem) were selected for the user study mainly because it contains digital collections that blind users are interested in, and it includes different types of help features. As to screen reader, JAWS 12.0 was installed in the experimental computer (http://www.freedomscientific.com), which is one of the most widely used among blind users.

In this study, no Braille display was used, and only JAWS was provided as a screen reader. In the participant recruitment flyer, participants were required to have experience using a screen reader program. Of the fifteen participants, thirteen used JAWS, while three mainly used Window-Eyes. Overall, the participants had average of fourteen years of experience in using a screen reader program, with one having three years of experience, one having six years of experience, and thirteen having more than tenyears of experience. Simultaneously, blind participants rated themselves as having an average of 5.2 in terms of familiarity with a screen reader program, according to a seven-point Likert scale. Participants had a level of skill using JAWS necessary for working on tasks assigned in this study. For recording user activities, Morae 3.1 version was used, which records monitor and user voice simultaneously. Also, Morae software provides detailed transaction logs including page views, key strokes, and time stamp.

Subjects were asked to conduct three typical search tasks – known-item search (Find the Letter written by Alexander Graham Bell to Helen Keller dated March 23, 1907\. Use two different search approaches—keyword search and browse), specific information search (Find when President Abraham Lincoln and President James A. Garfield were assassinated and how they were assassinated), and exploratory search (Identify at least two issues regarding immigration policy in the U.S.A., using as many sources from the digital library as you can. Each issue you identify should have a different source.). By going through the three types of typical tasks that blind users normally perform, the authors were able to identify a list of help-seeking situations that blind participants encountered in searching digital libraries.

Subjects were given thirty minutes to complete each task. The researchers used multiple methods to collect data:

1.  Questionnaires: Subjects were instructed to fill out a questionnaire requesting their demographic information and their experience in using the Internet;
2.  Pre-search interviews: The researchers investigated subjects' perceptions of help features and their help-seeking behaviour in using the Internet;
3.  Think-aloud protocols and transaction logs: Subjects were instructed to "think aloud" during the search process, and their search processes were recorded by Morae. Think-aloud protocol has been widely used in previous usability studies with screen reader users (e.g, Borsci and Federici, 2009; Chandrashekar, Stockman, Fels, and Benedyk, 2006; Stefano, Borsci, and Stamerra, 2010). In this study, no participant complained about thinking aloud process. All participants had years of experience of using JAWS, and they did not express any difficulty talking right after listening to JAWS; and
4.  Post-search interviews: After the searches were done, subjects were interviewed about their interactions with digital libraries, in particular their help-seeking situations encountered, help feature used, help features desired, as well as their overall assessment of the digital library and its help features.

The recorded data, which include subjects' verbalisation, screen reader information, and the researchers' observations, were then transcribed.

### Data analysis

For data analysis, the open coding method was used, which is the process of breaking down, examining, comparing, conceptualising, and categorising unstructured textual transcripts ([Strauss and Corbin, 1990](#str90)). The coding scheme applied in this study includes the following elements: types of help-seeking situations, example (with quotes), factors, existing help feature used, desired help feature, outcome, associated time stamp, and others (Figure 1). The coding scheme was developed to investigate different aspects that are related to help-seeking situations, such as types, help features, factors, and others. This study focuses on the exploration of types of help-seeking situations that blind users would experience in using digital libraries.

Four independent coders participated, and any disagreement was resolved by group discussions to ensure the reliability of data analysis. According to Holsti’s (1969) formula, the inter-coder reliability turned out to be 100% as all the coders agreed with the types of the observed help-seeking situations. Based on the coding scheme, twenty-nine help-seeking situations were initially identified by analysing forty-five search sessions. The initial sets of help-seeking situations included all cases that possibly impede users' interactions with the system. This study focuses on unique help-seeking situations that blind users experienced in interacting with digital libraries. Based on the following criteria, twelve situations were removed from the initial pool: (1) situations that commonly applied for both sighted and blind users; (2) situations that are not uniquely related to digital library systems; and (3) individual errors, rather than digital library system issues (e.g., a user kept using Windows key instead of the Alt key).

After screening, seventeen situations were finally identified as the unique help-seeking situations that blind users encountered in interacting with digital libraries. Those seventeen situations were further divided at two levels: physical and cognitive. Physical level situations refer to users' difficulty or inability in accessing, identifying, operating, and perceiving something required to proceed with the search process. Cognitive level situations represent users' difficulty or inability in understanding, sense-making, and reasoning about something during the search process. Situations in each level were further categorised into different types. Physical level situations were categorised into three sub-gategories: (1) difficulty in accessing information, (2) difficulty in identifying current status and path; and (3) difficulty in efficiently evaluating information. Cognitive situations fell into four sub-categories: (1) confusion about multiple programs and structures; (2) difficulty in understanding information; (3) difficulty in understanding or using digital library features; and (4) avoidance of specific formats or approaches.

<figure class="centre">![Figure1: Coding scheme](p673fig1.png)

<figcaption>Figure 1: Coding scheme</figcaption>

</figure>

## Results

This section reports the help-seeking situations derived from the data. The results are illustrated with evidence captured in participant utterances, screen reader announcements (enclosed within < >), and researcher observation (enclosed within { }). The help-seeking situations identified from this study can be classified into two categories: help-seeking situations at the physical level and help-seeking situations at the cognitive level. While help-seeking situations at the physical level are mainly related to accessibility issues, help-seeking situations at the cognitive level are more associated with problems of understanding and comprehension.

### Help-seeking situations at the physical level

Nine main help-seeking situations at the physical level emerged from the data. They can be further classified into three sub-gategories: 1) difficulty in accessing information, 2) difficulty in identifying current status and path, and 3) difficulty in efficiently evaluating information.

#### Difficulty in accessing information

Blind users cannot view documents via their eyes, so they have to rely on screen readers to interact with digital libraries. Accessibility is a key issue. Difficulty in accessing information in the main element in situation types a, b, and c, listed below.

a) Difficulty in accessing format information of an item

First, blind users need to access information about the format of an item before viewing it. The situation of "difficulty in accessing format information of an item" was observed when blind users could not find sufficient clues to access the format information of an item. The criterion of item format is one of the important things to consider when selecting information to use. In particular, blind users prefer resources in text or audio format, so accessing format information of items is necessary for blind users to decide what to use in their information search. As shown in Situation S9s31, the subject checked the format of items when evaluating search results. Blind users tend to avoid visual format items, usually checking whether items are available in a non-visual format when evaluating search results. In many cases, screen reader accessible information about item format is lacking, which presents problems to blind users attempting to evaluate an item’s usefulness. In Situation S11s37, the subject failed to identify the format of an item due to the lack of information about the item. The subject wanted to know whether the item was a picture or not before accessing its content. The lack of metadata about format, however, caused the subject not to understand the format of the item:

> That could be a picture right there. Or is it an article? You see, that doesn’t tell us that.

In this way, blind users require additional information about item format when judging if the item is understandable to them.

> (S9s31) <{Subject reads next 4 search results line by line.}>  
> Some of them I can kind of guess that they might be a photo, but some of them sound like they might be articles, and then they turn out to just be pictures.  
> (S11s37)  
> <Visited link left bracket Louis F. Swift and Count James Minotto standing in front of the United States Immigration. Link Chicago Daily. 2.>  
> It would be good if we knew that was a picture. It’s not clear.  
> <Link left bracket Mary A. Smith, standing at door of the United States Immigration Service General Offices right bracket. >  
> That could be a picture right there. Or is it an article? You see, that doesn’t tell us that.

b) Difficulty in finding alternative text for an image

As blind users cannot directly view image items, they usually look for alternative text that describes the image or provides some additional information. The American Memory Digital Library provided some alternative text for image items, especially scanned images that included text. In Situation S12s14, S12 was looking for "an accessible part," a text version of scanned page images. However, he could not find any link to screen reader readable text about those images. If there is no alternative text for an image item, the item is useless for blind users.

> (S12s14) {Subject has activated a link from the results list, and is now attempting to find information within the resource.}  
> <George Washington papers at the library of Congress, 1741 dash 1799 colon series 2 letterbooks. Philadelphia emigration society to George Washington, February 21, 1796, immigration. Separator. Page has 20 links. George Washington.>  
> Again, it has links. Now I have to see if there’s an accessible part where I can access it.  
> <Visited link graphic ammem slash. Link prev. Link next. Link item list. Link new search.>  
> Down arrow.  
> <Blank. George Washington papers at. John Nichol. Philadelph. Letterbook. Image 2\. Turn to image button. [Bip.] Edit 215\. Link. Vertical. Link prev image. Vertical bar. Link next image. Link George Washington papers home. Vertical bar. Link archival grayscale slash col. Blank. Separator. Blank. Link graphic image 215 of. Separator. Blank. George.>  
> I don’t believe I saw the word 'transcription'.  
> In several cases, the subjects were not able to find a link to accessible text even though there was a link to text. S2 reached the page that includes a relevant image with a link to text accessible for a screen reader. However, because of the unclear label, he failed to find a link to the alternative text. The American Memory Digital Library named a link to alternative test as "transcription", but the subject did not think it would be a link to the text. Without the observer's help, the subject was not able to find alternative text.  
> (S2s5)  
> {Subject is reading through the desired information resource page. He understands that the page contains a scanned image of the letter and is looking for an accessible version.}  
> Let me see if I can find the actual text. <Image 1 of 2\. Image 1 of 2.>  
> And it’s an actual image. So the image has not been OCR’d {processed by optical character recognition}. I have no way of reading this letter. In my mind, I have no way of reading this letter, because it tells me it’s an image.  
> {Subject gives up on finding accessible content}  
> — —  
> It’s just an image of the letter. {Observer points out 'Transcription' link.} There is a transcript? I will use a links list, looking for a T, looking for transcript.  
> <Jaws dialog t. Library of. Transcription.>  
> Transcription. I’m assuming that means transcription of the letter. I’m entering on that. I saw that before but I didn’t know what it means. That item that is listed as "Transcription", should say "Transcription of the letter" because there needs to be more description. What do you mean "transcription?" That wasn’t clear to me. Now that you explained that, that made good sense. But I had to have your input to know what that transcription meant.

c) Difficulty in recognising pre-existing text in the input box.

The situation of '_difficulty in recognising pre-existing text in the input box_' is closely related to interface design. Since blind users are not able to see what is in the input box, they sometimes do not recognise text already existing there. Although there is text remaining from the previous search, they assume that the search box is clear.

In many cases, subjects did not recognise this problem even after the search results were shown. In Situation S6s4, the participant did not recognise that there had been extra terms included until the observer told her. This kind of situation was not easy to recover from. Often, users who experienced this situation failed to solve the problem. To overcome this situation, blind users needed certain system support, but the American Memory Digital Library did not give any help to users to check what was in the edit box. The only way they could check was to manually read the edit box by moving focus from the beginning.

> (S6s4)  
> {Subject has decided to begin task by performing a keyword search}  
> I’m going to search here.  
> <Pasted from clipboard. {Date from previous search still in search box.} Enter. Search button>  
> — —  
> I tried to do a search, to search all collections. I was doing a search for '_Abraham Lincoln and James Garfield assassinated and how_', and it took me to a Web page where it was difficult for me to understand, and I had troubles navigating through it. So I went back and I looked under presidents, and I went under presidents and I searched for the same thing, 'Abraham Lincoln and James Garfield assassinated and how'. And right now I’m seeing what information it brought up. ... ... I couldn’t find any information on that.  
> {Subject decides to consult the American Memory help page. Only about 15 minutes later, when the observer tells her, does she reals her query included terms from the previous task.}

#### Difficulty in identifying current status and path

Checking the status and path is another important subcategory of help-seeking situations for blind users at the physical level. Situation types d, e, and f provide examples of help-seeking situations that fall into this subcategory.

d) Difficulty in identifying the current location

Blind users need to know where they are in the digital library at all times. The analysis found that blind users are sometimes unable to identify their current location in the digital library. When blind users interact with a digital library, they rely on keyboard focus and screen reader vocalisation. A screen reader reads a page linearly, but if the page structure is complex, blind users could fail to identify the location of their current focus. In Situation S1s25, the subject was not sure whether he returned from the help menu, and just navigated back three pages. After he had attempted to go back to the page he previously visited, he remained lost, not finding the page he wanted. In this case, the subject tried to trace back his search paths to revisit the page he had viewed, but he eventually gave up. Blind users are vulnerable to becoming lost when they try multiple jumps between pages, since they do not have a clear idea about the structure of paths in a digital library.

> (S1s25)  
> {Subject has returned to Immigration, American expansion browse category page by pressing Alt-left arrow multiple times. He must now get his bearings.}}  
> <An American time capsule colon three centuries of broadsides and other printed ephemera.>  
> Does that mean that I’m still in the help menu?  
> <American memory from the library of Congress dash browse by category. Link broadsides and other printed ephemera tilde ca 1600 dash 2000.>  
> Because... I don’t know ...  
> <Back.> {This brings subject to American Memory home.}  
> <Back.> {This brings subject back to last screen encountered in previous task.}  
> <{Silence.} Back. {New page loads.} American Memory from the library of Congress. 3\. Link oral history with 83 year old female, Colorado left paren transcription right paren. Link American English dialect recordings colon the centre for applied linguistics collection. Link. 3\. Link oral history with 83 year old. Alt-enter.>  
> Argh. Let’s see what this is. It might actually be something that might tell me about immigration policy. And it’s a transcription.  
> <{Silence.}>  
> I clicked on a link to an oral history interview with an 83 year old Coloradan woman.  
> <{Silence.} 4\. Link New Year’s greeting from presidential assassin Charles Julius Guiteau to his jailor, 31 December, 1881.>  
> {Observer: Could you describe for me where you are on the site right now?}  
> {Laughs.} Very good question. I’m completely lost.

e) Difficulty in returning to home

The situation of "returning to a homepage or previous pages" is another challenge for blind users. In particular, when a link label for the homepage is not clear, it makes blind users confused. Usually, sighted users click the logo of a site or a link to homepage to go to a home page, and blind users similarly find a hyperlink to the home. However, when a label of a homepage link does not clearly say that its destination is the home page, blind users get confused. For example, the subject could not find a way to go back the homepage since the link to the home has an unclear label, "ammem".

> (S11s25)  
> {The subject has just discovered a spelling error in her search query and wishes to return to American Memory home to retype the query.}  
> <Same page guide slash bin slash query. List of 7 items. Visited link ammem slash index.>  
> Oh, no.  
> <The library of Congress American Memory.>  
> Where is the home page?  
> <Visited link ammem slash index.>  
> Is that it? That’s confusing. Is that it, the home?  
> <Let me just go back here. Alt-tab. Alt-tab. American memory from the lib. The library of Congress. Visited link ammem slash index.>  
> Let me go back here.  
> <Links list dialog. Links. Ammem slash index. Ammem slash index. Browse. About. Help. Contact. Guide slash bin slash. Ammem slash index. Ammem slash. Browse. Ammem slash index. Browse.>  
> I don’t like that, but I don’t know what to do.

f) Difficulty in recognising page loading status

As blind users cannot see the transition between pages, sometimes they are not able to ascertain page loading status. They try to read a new page even though it has not been fully loaded. There are no explicit messages to inform blind users of page loading status. This problem was also observed in users' interaction with the American Memory Digital Library. In Situation S4s27, the subject was never aware of page load delay while reading a new page, because there was no feedback regarding page loading. This situation can occur anytime a system network is temporarily slow or the size of a page is large. Therefore, some kind of feedback is required to tell blind users about the delay of a page loading.

> (S4s27)  
> {The subject has clicked on a link labelled “text,” looking for an accessible version of the resource.}  
> <{Long pause while page loads. Subject believes new page to have loaded, begins to explore.}Blank. Call number. Blank. Link text. Blank. Call num. Blank. X F. Blank. Repository. Blank. Digi. Blank. cubci. Blank. Blank. Link graphic Ammem. Previous. Next. Link new. Blank. Blank. Blank. Blank. Blank. Blank. Blank. Blank. Blank.>  
> Either I am, I think I am back at the same thing I was at with Lincoln and Garfield.  
> <Communication from the. Communication. Link graphic Ammem slash American. Previous. Next. Link new search. Blank. Link the Chinese. Blank. Separator. Blank. Click on image for larger image, full. Link left bracket. Blank. Separator. Blank. Blank. Left bracket.> {Page loads. The transition from former to loaded page is almost instantaneous. There is never any feedback regarding page loading.}  
> {Subject continues to explore the new page using directional arrows. There is no evidence she ever becomes aware of the page load delay.}

#### Difficulty in efficiently evaluating information.

Blind users cannot efficiently find specific information and evaluate an item because they have to rely on the screen reader to find specific words/phrases, locate heading information, or skim an item. Situation types g, h, and i illustrate help-seeking situations that fall into this subcategory.

g) Difficulty in finding a specific word or phrase in the digital library pages

Blind users had difficulty in effectively finding specific words or phrases presented in a page. They often use the JAWS Find function to find a specific word, which is the same as "Ctrl+F" for sighted users. Since they cannot quickly skim the content of a page, blind users were more likely to use JAWS Find functions than sighted users using "Ctrl+F". Since JAWS Find is based on exact matching, users are required to type in a correct term to find a specific word in the page.

> (S12s11)  
> {Subject has just copied the word 'immigration' from the task instruction and has now returned to the American Memory home page.}  
> I’m going to see if the word 'immigration' was one of those browsable categories, just by chance, with Control-F.  
> <Virtual find. Pasted from clipboard.>  
> Paste the word in.  
> <Immigration.>  
> And Enter.  
> <Enter. [Ding.] Screen find.>  
> It’s not. JAWS did not find the word. [Note: 'Immigration, American expansion' is indeed one of the browse categories. I am not sure why the JAWS search did not work].  
> {Subject gives up on browse categories and conducts keyword search for “immigration” instead.}

Blind users sometimes type a term incorrectly, which leads to failure to find the term they want. In this study, several cases were observed where blind users made a typo when using the JAWS Find function. The problem is that no aid is provided for checking spellings in the JAWS Find dialog, so it is not easy for blind users to check whether they typed a term correctly in the edit box.

> (S8s3) {The subject is reading a search results list, struggling to find the desired item. She interrupts that strategy by using a JAWS search instead.}  
> I’m gonna do a find to see if somehow it got missed.  
> <Virtual find. JAWS find dialog. Find what colon edit. A L W W E X E E A N D E R. Tab. Maintain history of. Tab. Direction for. Tab. Ignore case check box ch. Tab. Find button. Tab. Delete button. Shift-tab. Find button. Enter. Link letter from Alexander Graham Bell to Helen Keller May 26, 1899\. Tab. A G Bell. Tab. Letter from Alexander Graham Bell to Helen Keller, November 20, 1908 link. Tab. A G Bell. 6\. Left bracket President Coolidge. Virtual find. JAWS find dialog. Find what colon edit combo. Alexander. Tab. Tab. Direction. Tab. Ignore case. Tab. Find button. To activate, press. Space. Coolidge era link. Link Letter from Alexander Graham Bell to Helen Keller, June 5, 1908.>  
> Okay, I’ll just find it some other way.

h) Difficulty in finding heading information

For blind users, heading information is very important for understanding the overall structure of a page. Blind users face difficulty in navigation if there are no headings. Therefore, when there were no headings on a page, they often expressed frustration. Heading information is also used for blind users to decide what to do in a page. As S14 mentioned, headings are useful to narrow down pages and determine search strategies. As S12 addressed, blind users require obvious paragraph headers that describe the layout or content of a page to proceed with their search process.

> (S14s12) <The library of Congress link. Page has 12 links. American memory from the library of Congress same page. Query. List of. Link the library of Congress. There are no headings on this page. Search all collections. Search. List ends.>  
> I like headings. It really narrows your pages down, and JAWS has a new feature called 'regions', and those are nice too because you just hit the semicolon and go from one region to another quickly. If a digital library and regions or headings it would be very much simpler for us.

> (S12s1) There’s no headings. It would have been nice to be able to hit H and get, “This is the new content area.” Whatever that content area would be, whether it just said there is a letter or there isn’t. Maybe there’s a quick little element down there that’ll help me, I just haven’t gone all the way through.

i) Difficulty in efficiently evaluating information

The situation of "difficulty in efficiently evaluating information" is unique to blind users because of the linear nature of reading with a screen reader. Whereas sighted users tend to move around their focus non-linearly within a page as they selectively read interesting or highlighted information, thus quickly finding the information they desire, it is not easy for blind users to jump around the page to select specific information. Blind users have no option but to listen to what a screen reader reads for them in a linear order from the top to the bottom of a page.

Therefore, this limitation is closely related to an efficiency issue in the search process. Since they need to listen what the screen reader says, it usually takes more time for blind users to extract useful information from the page compared to sighted users. For example, S14 was complaining about listening through all text in a page.

> (S14s15)  
> <Telegram extra. Assassination of Abraham Lincoln. J Wilkie Booth the assassin. Secy Seward and his son Frederick wounded dot dot dot War Department 1 point 3 0 A dot M April 15, 1865\. Telegram extra. Assassination of Abraham Lincoln. J Wilkie Booth the assassin. Secy Seward and his son. Telegram. April. Link. Link next. Link previous. Link graphic ammem slash American.>  
> I don’t like this, how it has a bunch of 'nexts' and things on it. That’s just very bad. It’s time-consuming.  
> Along similar lines, in order to find out specific information in a page, blind users were required to read a page thoroughly from top to bottom. In order to avoid missing a desired piece of information, they must read all extra text that is not relevant to the task.

> (S7s20)  
> {He has been using directional arrows to read the page from top to bottom.}  
> One of the most annoying things is, with the screen reader, reading all the top of this page. Did we really want that? Look at all this time being wasted, because I’m not sure where the actual help is on the screen. So it’s better to read it through, rather than to grope, because you might miss something.

This situation is frequently observed in evaluating an individual item or browsing through a list of items. In order to evaluate the relevance or usefulness of an item, users need to read through the content. Also, browsing usually requires skimming through a list as shown in Situation S15s19 below. Again, because it is difficult for blind users to skip unnecessary reading, they require more time to evaluate or browse.

> (S15s19)  
> {She is attempting to locate the content by using Down arrow.}  
> <The assassination. Link graphic. Link previous. Link next. Link item. Link new. Blank. Link by popular demand. Blank. Separator. Blank. Item 2 of 2 hun. Separator. Blank. Click on picture for larger. Link left. Blank. Separator. Blank. Blank. Link graphic 3 of. Blank. The assassination of President Lincoln colon at Ford’s theatre. Blank. Blank. Link Currier and Ives. Blank. Created slash published. New York colon Currier and Ives. Blank. Notes. Reference colon Currier and Ives. Blank. Blank. Subjects. Link Lincoln, Abraham dash dash. Link Booth. Link Ford’s theatre. Link assassinations dash dash Washington left paren D dot C right paren dash dash 1860 dash 1870\. Link lithographs dash dash. Blank. Link medium. 1 print colon. Blank. Call number. PG. Blank. Reproduction num. L. LC. Blank. Repository. Library. Blank. Digital ID. Blank. Left. cph30\. Blank. Left paren link colour film. cp. Blank. Link graphic ammem slash American. Link previous. Link next. Link item list. Link new search. Link new search. Link new search.>  
> I find this cumbersome, because you have to go through everything to find the information.  
> {Subject continues to explore using Down arrow, until she decides this resource is not relevant. She navigates away from the resource page by clicking on a subject heading link.}

### Help-seeking situations at the cognitive level

Eight main help-seeking situations at the cognitive level were derived from the data. They can be further classified into four sub-gategories: 1) confusion about multiple programs and structures, 2) difficulty in understanding information, 3) difficulty in understanding and using digital library features, and 4) avoidance of formats or approaches.

#### Confusion about multiple programs and structure

In interacting with a digital library, blind users must interact with the digital library, Web browser and the screen reader at the same time. This increases the cognitive load they experience, as compared to a sighted user performing the same interaction. In addition, subjects were also confused about the structure of library organizational units, browsing categories, and organization of search results in the American Memory Digital Library. Situation types a and b are examples of situations included in this subcategory.

a) Confusion about multiple programs

'Confusion about multiple programs' refers to the amount of information and interactions that must be processed simultaneously. In this study, it specifically refers to the difficulty in processing a large volume of information needed for a digital library search at the same time. This kind of situation was observed when subjects tried to interpret the information conveyed by the digital library site, the browser, and the screen reader simultaneously. They were unable to distinguish clearly the three programs from each other, thereby failing to determine the appropriate course of action. The following illustrates the disoriented state of a participant who thought he was on the American Memory site but was actually trapped in the browser’s address bar.

> (S2s14)  
> Why didn’t that...The Jaws search didn’t provide anything for Lincoln. I wasn’t expecting that.  
> <Compatibility checkbox not checked. Title list AMLC dash Browse by Category. Windows Internet Explorer. Escape.>  
> {Sigh}. Well, we’re in the right spot.  
> <Escape. Compatibility check box not checked.>  
> I seem to have gotten out of browse mode somehow.  
> <Tool bar refresh left paren f5\. Toolbar. Compatibility check box not checked. Escape.>  
> I’m in some kind of a menu system I don’t like. I’m getting out of there by hitting escape. Using the h button I was expecting to go back to the headers. And it’s not.  
> <Escape. PC Curser>  
> How did I get out of my browse mode?…I’m stuck in a tool bar.

b) Confusion about digital library structure

Blind users need to make sense of different structures within digital libraries based on logical thinking. Subjects faced a help-seeking situation when they could not make sense of interface structures, browsing categories, or organisation of search results. They could not logically understand structures as they could perceive only a small fraction of the content at a time provided by the screen reader. The following illustrates such a situation, where a subject was navigating down a long list of browse categories, but could not understand them, in particular the relationships among the categories and sub-gategories.

> (S14s7)  
> I very seldom use categories like this, because they’re too slow. You have to read too much, and you don’t know what their categories are, so then you have to go through a whole bunch of extraneous stuff… It sounds to me like instead of broadening the collection, you could probably go up to that link where it says, “browse the entire collection” and maybe they’d give you basic categories instead of sub-gategories.

#### Difficulty in understanding information

Understanding and making sense of information in the digital library is essential for blind users to effectively interact with digital libraries. Situation types c and d demonstrate help-seeking situations belonging to this subcategory.

c) Difficulty in recognising a label

The situation of 'difficulty in recognising a label' was one of the most frequently observed in blind users' interactions with the digital library. Blind users rely solely on the name of a label when selecting a hyperlink or item while sighted users use both a label and a visual icon. Therefore, it is crucial for blind users to correctly understand what a label or hyperlink indicates in the digital library. If hyperlink labels are not clear, blind users have difficulty understanding what the label means. In Situation S4s2, the subject did not understand the meaning of 'Ammem slash amemican', which was the hyperlink to the American Memory homepage. Since the label of 'Ammem' was ambiguous, blind users did not use it: '_I don’t need it ... I just ignore it_.

Blind users' search activities include a number of interactions with text labels. Accordingly, clear labels are imperative to better support blind users' search processes, in particular, browsing and navigation. However, the American Memory Digital Library sometimes did not provide specific enough labels for some features. In Situation S12s1, the subject guessed the meaning of the label 'transcription' because the label was vague in the situation he encountered. If the label provided more information, such as 'typed transcription of a letter', the subject would not have experienced any confusion. However, in many cases, labels of the American Memory Digital Library tended to be short, which resulted in ambiguity.

> (S4s2)  
> {While using directional arrows to move through the header of an information resource page, subject gets distracted by an oddly labelled link.}  
> <Link Ammem slash amemican. Previous. Link Next.>  
> Yeah, there’s that stupid Ammem dash amedicum, or whatever.  
> <Link graphic Ammem slash amemican.>  
> What is it? Can you tell me?  
> <Link graphic Ammem slash amemican. Previous. Link Next. Link Item list. Link. New search.>  
> I don’t need it, I’m just curious. Like before, I just ignore it, but I’m just always curious about things.  
> {Subject ignores link and continues using directional arrows to explore page.}

> (S12s1)  
> There was one key word, I don’t know what will come of it, but the word “transcription” indicates that maybe that’s a typed transcription of the letter, something different from an image. I’m not sure what that word “transcription” is going to give me, but it’s my last guess at the moment.  
> <Enter. Transcription link.>  
> Okay, this is it.

d) Difficulty in understanding help information

This study revealed that blind subjects had problems in understanding help information of the American Memory Digital Library. In Situation S10s3, the subject did not obtain any relevant results from his search due to the spelling error. Then, the subject decided to consult the American Memory help pages to resolve the situation of failing search. He found and read a long page titled 'Search Help'. However, the subject found it would not be useful to solve his problem and expressed frustration after reading 'Search Help'. In this case, the help page was not helpful to resolve the situation, which was caused from an incorrect query. The American Memory help pages included descriptions about how to search in general, such as search all collections or individual collections, bibliographic record search, term matching options, and others. These help instructions are more general rather than detailed help solutions for specific situations. In particular, there was no instruction about how to clear the input box, which blind users frequently experienced. In this way, the situation of "difficulty in understanding help information" was mainly caused by lack of help instructions tailored to blind users.

> (S10s3)  
> <Type a text. Search all collections. [Bip.] Edit history. A L E X A N D E R space G R A H A M space B B E L L. Enter. {Note the misspelling of 'Bell'. Also, the word 'history' remains in the search box from the previous search.} {Search results page loads.} 100%. Legal link. Page has 12 links. Link legal. Link legal. Link legal. Vertical bar. Link the library of Congress. Blank. Page for tips on searching American memory. Link search help. Visit our. Blank. We were unable to find any matches for your search.>  
> {Subject has performed a keyword search with a spelling error, yielding no results. She followed a link labelled ‘Search help,’ and has spent a significant amount of time reading the long search page with Down arrow.}  
> {Observer: So, what is this page about?} Yeah, it’s giving me a number of different options, and part of it is trying to figure out which one ends up working for me. So far, I’m feeling somewhat frustrated. Trying to figure out which one, which way to go.

#### Difficulty in understanding and using digital library features

In addition to understanding information in the digital libraries, it is also crucial for blind users to make sense of functions offered in the digital library. Situation types e and f are examples of help-seeking situations in this subcategory.

e) Difficulty in understanding how to use a specific function

The analysis found that blind users were unable to understand how to use some functions provided by the American Memory Digital Library. In particular, blind users were observed experiencing difficulty in interacting with a complex interface, such as advanced search. In S4s22a, the subject was presented with options for search and browse in quick succession, and was unable to distinguish between the two. Also, the subject did not understand the meaning of "browse by author, title, genre, and geographical". As he did not understand the meaning of labels correctly, he decided not to use those browsing functions. In the continuation S4s22b, the subject tried to use the search function. The subject expected the search box would be a basic edit box, but it was actually an advanced search box for bibliographic records. The subject did not understand its function, and eventually abandoned its use.

> (S4s22a) <Link search. Blank. Link search. By keywords vertical bar browse by. Link author. Vertical bar. Link title. Vertical bar. Link genre. Vertical. Link Geographical. Separator. Link Vertical bar. Link author. By keywords vertical bar browse by.>  
> If I want to do a search by keyword, I am not understanding how I can do a search by keyword. Because it is not a link, but author, title, and genre are.  
> <Link search. Link title. Vert. By keyword. Link search.>  
> And yet there is a link for “search.” So will that bring up a search box? I don’t know. I guess we’ll find out.  
> <Enter.>

> (S4s22b)  
> <{New page loads.} Printed ephemera collection search. Page has 1 heading and 60 links. Printed ephemera collection. Search descriptive information left paren bibliographic records right paren colon. Edit. Search button.>  
> Oh, brother. {Observer asks about strong reaction.} I just wanted to have a nice little pretty box that I could type 'policy' into.  
> <Search descriptive information left paren bibliographic records right paren colon. Edit.>  
> Bibliographic records. Who the heck knows? Well, I’m just going to put 'policy' in.  
> <Search button. [Bip.] Type a text. p o l i c y. >  
> How horrible can it be? It can only bring me down the wrong path if it wants to. {Observer: just to be clear: you’re not sure what this box is searching exactly.}No, I’m not.

f) Difficulty in making sense of organisation criteria

Blind users experienced difficulty in understanding organisation criteria of search results in using AM Digital Library. This situation was frequently observed in Task 1, which asked the subjects to find a known item from a specific year. In Situation S3s5, the subject attempted to check what kind of organisation criterion was applied in the search results. At the beginning, the subject guessed it would be sorted by alphabetical order. However, after she realised that it was not, the subject checked again if it was ordered by year. Again, she discovered that her hypothesised organisational criterion was incorrect.

When the sorting order of search results were not clear to blind users, they got confused when evaluating search results. For sighted users, the sorting order of search results would be relatively less problematic because they can pick out relevant items if they are on the search result page, regardless of order. Of course, sighted users could also benefit from clear sorting criteria in the search results. However, blind users are more likely to be influenced by sorting order of search results because they tend to evaluate the search results from the top linearly until they find a relevant item.

> (S3s5)  
> {Subject has brought up a list of correspondence between Bell and Keller. She is not attempting to locate the specific letter she needs.}  
> We’re looking for a letter from Helen, so...  
> <{Subject skips ahead, happens to begin reading at result 15\. Subject reads search results 15 through 7 in reverse order.}>  
> I thought it was in alphabetical order, but I guess not. So it might be in date order, so I’m going to check...  
> <7\. Link letter from Evaline H. Keller to Alexander Graham Bell, April 20, 1887.>  
> April 20th.  
> <{Subject reads search results 8 through 13 line by line.}>  
> I need to look at this a little bit more.

#### Avoidance of a specific type of format or approach

A final subcategory of help-seeking situations at the cognitive level is '_avoidance of a specific type of format or approach_'. In order to more quickly find desired information, blind users learn to reject material that is unlikely to be accessible or efficient to use. Help-seeking situation types g and h demonstrate two types of avoidance.

g) Avoidance of visual items

In many cases blind subjects tended to avoid using visual items even without trying to understand them. For example, as shown in Situation S11s29, when encountering an image item, the subject immediately believed that it would not be useful:'_Photo. Don’t want a photo. That won’t do me any good_'. Digital libraries usually include many multimedia items, such as image and video items, which are less useful to blind users. Therefore, when designing a digital library system, it is important always to provide alternative text to image or video items to enable blind users to use such non-textual information.

> (S11s29)  
> <American memory from the library of Congress. Wrapping to top. Search results. Link. Greater. Visited link Am. Gr. Link. Page 1 of. Blank. Dis. Link. Go. Link. Link. Link. Prev. Link next. Summary. Blank. Table with. Item titles. Collection. 1\. Link left bracket old Ford’s theatre where Lincoln was assassinated right bracket. Link photo graphs from the Detroit Publishing Company 1880 dash 1920.>  
> Photo. Don’t want a photo. That won’t do me any good.

h) Avoidance of browsing approach

The situation of '_avoidance of browsing approach_' is related to blind users' avoidance of browsing methods while using digital libraries. Blind users prefer keyword searching to browsing even more than sighted users. Browsing, by nature, takes a longer time for blind users because they have to read through all category labels provided by the digital library linearly until they find a category of interest. Sighted users can relatively quickly choose relevant categories from the browsing list, so browsing is one of the two key strategies for sighted users in searching digital libraries. However, our analysis found that blind users would prefer to use keyword searching over browsing. S14 addressed the efficiency problem of browsing approach. As S14 mentioned, browsing entails too much reading, and it requires users to understand what each category label indicates. Even though browsing could be useful in some tasks, many blind users avoided it because of its inefficiency.

> (S14s7)  
> <Link. Link. Link. Link more. Link government. Link continental. Link more. Link. Link Chinese in California. Link more. Link literature. Link Walt Whitman. Link more. Link maps. Link Civil War. Link more. List ends. List of 8\. Link Native American. Link Pacific North. Link more. Link performing arts, music.>  
> I like doing searches better. I think they’re more efficient. You can narrow down the keywords, and I always find what I want with searches. I very seldom use categories like this, because they’re too slow. You have to read too much, and you don’t know what their categories are, so then you have to go through a whole bunch of extraneous stuff, and that’s boring.

> (S15s24) I might do browse collections, but I probably wouldn’t, because that would be overwhelming. This is already too much.

## Discussion

Overall, the help-seeking situations made the three search tasks difficult or impossible for blind subjects to complete with existing help features. The discussion focuses on the design implications in terms of how to design better digital library help features to facilitate blind users’ effective information retrieval.

### Design implications for physical situations

Physical situations fell into three sub-categories: difficulty in accessing information; difficulty in identifying current status and path; and difficulty in evaluating information efficiently. The following summarises the results pertinent to situations under each subcategory, along with discussion of design implications.

#### Difficulty in accessing information

The difficulty in accessing information manifested itself in three ways. First, subjects had difficulty accessing information about the format of an item. They could not readily tell if a document was a picture, text, or scanned image of text. Identifying format of an item is significant because blind users consider graphical information to be for the consumption of sighted users, and typically ignore it. This situation implies that the digital library design did not furnish cues on item format in a manner suitable for screen reader enabled perception and operation in information search. Metadata are necessary to support users to recognise the format, and those metadata should be presented in a column in search results. It is important to ensure that metadata of list items identifies item format and should be readily accessible with a screen reader.

Second, subjects had difficulty in finding text descriptions of images such as a scanned image of a document or pictorial representations. This constitutes a serious impediment as they cannot perceive information embedded in these images. This situation implies two possible errors in digital library design. One, it did not supplement graphical information with non-graphic alternatives (e.g., text description, transcript, audio, etc.) for screen reader users. Two, it did not elucidate the path to available alternatives in a manner appropriate for screen reader enabled perception and operation in information search. Digital libraries should provide clear labels for alternative text. Currently alt text is often gibberish, such as 'crb220158'. Digital library designers and developers should emphasise alt text that is meaningful or useful for blind users.

Third, subjects had difficulty in recognising pre-existing text in input fields such as the keyword search box. Consequently, irrelevant terms were unintentionally entered into the search query, resulting in a failed search. This situation implies that digital library design did not alert the user of the pre-existing text of the input field in ways appropriate for screen reader enabled perception. This also indicates that digital library design did not warn of consequences of leaving this text on search outcomes in a manner perceptible and understandable non-visually. Digital library systems need to ensure that queries are cleared from the input field after search query execution.

#### Difficulty in identifying current status and path

The difficulty identifying in current status and path manifested itself in three ways. First, subjects had difficulty identifying their current location on the digital library site. This was particularly evident in cross-page navigation where arrival on a certain page was not apparent. A negative outcome was quitting the digital library site without realising it. This situation points to a failure of digital library design in providing contextual cues and location information in a manner appropriate for screen reader enabled perception in information search. To support users in this type of situation, each digital library page should have: 1) a meaningful page title at the top, followed by descriptive breadcrumb trail; 2) a standard layout, particularly the collection home page; and 3) meaningful headings assigned to different sections to facilitate screen reader navigation.

Second, subjects had difficulty in returning to the home page from the depth of the digital library site. They could not find the “Home” link as expected. This caused frustration. This situation reflects a failure of digital library design to present the Home link with meaningful label in a manner suitable for screen reader enabled perception and operation in information search. This situation also implies that the digital library design did not describe the path to an available Home link in a way appropriate for screen reader enabled perception. In this situation, clear labelling of the link to Home is imperative. Digital library designers should ensure that the Home logo is labelled "Home" in its alt attribute. Along these lines, the digital library can provide noticeable links to a small number of key anchor pages, such as a main browsing page, list of all collections, and help page, which users might frequently need to access from anywhere.

Third, subjects had difficulty recognising a new page following a link activation. This was particularly pronounced on pages that loaded without full and continuous feedback. As explained earlier, blind users recognise a new page using at least two announcements: page loading status and page composition. The page loading status tells if all content has downloaded, while the page composition tells if the page was ready for interaction. The difficulty in recognising a new page implies the digital library design failed to communicate when a link activation brings up the destination page in a manner appropriate for screen reader enabled perception and operation in information search. Digital library design should ensure whether full and continuous feedback of page loading status and downloaded page composition is available to screen reader. It is also worth noting that the lack of feedback about web pages loading is screen reader specific. For the time being, multiple ways have been applied to users to aware the page loading status, such as announcing percentages as the page loads and providing a rising tone if a page is loading slowly.

#### Difficulty in efficiently evaluating information.

The difficulty in efficient evaluation of information manifested itself in three ways. First, subjects had difficulty finding a specific word or phrase on the digital library page. Blind users typically rely on the Find command to locate words or phrases, and not through skim-reading the content of the page as sighted users do. However, a limiting feature of the Find command is a reliance on exact matching, leaving no room for typographical errors. The difficulty in finding specific words or phrases on a page implies that digital library design did not highlight a typographical error or suggest correct spelling of searched words or phrases in a manner suitable for screen reader enabled perception in information search. Digital library Find functions need to accommodate for spelling errors and use of related terms, not restricted to exact matching.

Second, subjects faced difficulty in finding header information on a page. Header information is relevant on pages that organize content into different sections. It refers to the title assigned to a specific section such as 'Search Results'. The utility of header information for blind users is comparable, to some extent, to the utility of skim reading for sighted users; it helps gain an overview of information presented on a page. The difficulty finding header information implies possibly two failures of digital library design. First, it did not organize page content into sections in a manner appropriate for screen reader enabled perception and operation in information search. Second, it did not mark section headers in a manner suitable for screen reader enabled perception and navigation in skim-reading a page.

Therefore, it is important to make sure every page is organized into sections with meaningful headings marked for screen reader navigation. Also, standardisation of the use of headers will enhance the usability by enhancing predictability. Moreover, digital library design can provide additional alt text to compliment headings to facilitate users easily navigate information. The alt text will help blind users better understand the overall structure of the page.

Third, subjects had difficulty in quickly accessing relevant information on a page. This difficulty was particularly pronounced when evaluating an item or items list for relevance. Reading with a screen reader happens linearly. This linear reading approach imposes sequential information processing, as opposed to parallel information processing afforded by visual reading. This situation implies that digital library design did not afford filter and skim-reading mechanisms in a manner appropriate for screen-reader-enabled perception and operation in browsing pages for relevant items. To resolve this problem, search results need to be display for blind users as follows: 1) include a meaningful section header marked as Level 1 heading; 2) grid view is easily switched to list view; 3) each result has a meaningful title marked as Level 2 heading; 4) associated metadata is sufficiently detailed to include subject classification.

### Design implications for cognitive situations

Cognitive situations fell into four sub-categories: confusion about multiple programs and structures; difficulty understanding information; difficulty understanding or using digital library features; and avoidance of specific formats or approaches. The following summarises the results pertinent to situations under each sub-category, along with discussion of design implications.

#### Confusion about multiple programs and structure

The confusion about multiple programs and structures manifested itself in two ways. First, subjects got confused in simultaneous interaction with multiple programs including the Web browser, the digital library site, and the screen reader dialogues. This significantly increased the amount of information and interactions to be processed simultaneously. Apparently, this blurred the boundaries between applications, making the selection of appropriate commands difficult. This situation reflects a failure of digital library design to clearly identify various programs in the mix for non-visual cognition. To enable users distinguish different applications more clearly, the system can provide different tones or voices for different programs. Also, we can make screen reader quickly read the name of the application when users switching programs.

Second, subjects could not make sense of a digital library structure, such as sections of a page, categories to browse, and how search results were organized. The fact that they perceived only a snippet of information at a given time made logically understanding page or site structure challenging. This ambiguity created confusion and disorientation in the information search process. This situation reflects a failure of digital library design to communicate relevant structure information in a manner appropriate for non-visual cognition for effective navigation and interaction. Therefore, detailed header information about page layout is imperative, and the screen reader needs to continuously provide information about the current section in a page. Standardisation is also important. Standardisation of collection home and resource page layout would reduce confusion and enable users more easily to predict what will appear next.

#### Difficulty in understanding information

The difficulty in understanding information manifested itself in two ways. First, subjects had difficulty recognising a hyperlink or an item without a meaningful label. While some subjects kept guessing the utility of the poorly labelled object, others did not bother exploring its relevance. This situation implies two possible errors in digital library design. First, it may have failed to present an object for screen reader enabled perception and operation. Second, it may have failed to expose its affordances and expected response to actions in a manner consistent with non-visual cognition in information search. This implies labels should be clear and self-explanatory. Consistent labelling can also help blind users to better understand labels across pages in a digital library. In addition, digital libraries can provide further information about labels upon users' request, such as longer description and meta-information hidden behind labels.

Second, subjects had difficulty in understanding help information furnished by the digital library. Two kinds of such 'not so helpful' help information observed were (1) error message following failed search, and (2) 'Search Help' page guidance. In the first kind of situation, subjects could not understand the reasons for the failed search, and did not receive any guidance on overcoming the failure. This implies two possible problems in digital library design. First, it did not explain the reason of the failed search in a manner conducive to non-visual cognition. Second, it did not communicate to bind users regarding how to recover from the failure in ways that suit non-visual cognition. In the second kind of situation, subjects did not find the instructions on the 'search help' page catered to their needs and challenges. The help information was of generic nature, and therefore not helpful enough. This implies digital library design failed to offer more specific guidance tailored to the needs and challenges of screen-reader-mediated interactions. Task-specific help that describes structure of task environment could be certainly useful to blind users. In this case, the 'special help for blind users' would ideally be context sensitive.

#### Difficulty in understanding and using digital library features

The difficulty in understanding or using digital library features manifested itself in two ways. First, subjects had difficulty understanding how to use complex digital library functions such as advanced search. Two complex functions are worth mentioning. The first is a search functionality that combined browsing and keyword search functions that subjects could not figure out how to use. The second was the bibliographic record search, the purpose of which was not apparent for subjects. They were reluctant to use it, because they were unsure what to expect from a search. This confusion and intimidation of complex functions implies digital library design did not present component parts, corresponding utilities, and expected outcomes of use appropriately for non-visual cognition. In addition, step-by-step instruction on using the function with screen reader can be a compelling aid. It is clear that blind users and sighted users experience different help-seeking situations. While sighted users normally encounter query formulation and query formulation related help-seeking situations that require advanced search features ([Hu _et al.,_ 2013](#hu13); [Kim, 2006](#kim06); [Savenkov and Agichtein, 2014](#sav14); [Zeng _et al.,_ 2006](#zen06)), blind users have more help-seeking situations related to access and understanding, such as access and understanding basic and advanced searching and browsing functions.

Second, subjects faced difficulty in understanding the criterion for organising search results. This situation was particularly prevalent in known item searching where subjects could not tell if the results were ordered alphabetically, chronologically, or using some other criterion. This situation implies that the digital library design failed to clearly specify the criterion for organising search results in a manner suitable for non-visual cognition in information search. To help blind users to clearly understand organisation criteria, the search result should be supplemented with the followings: 1) text describing sorting criteria applied; 2) self-explanatory sorting criteria; and 3) step-by-step instruction to select different sorting criteria with screen reader.

#### Avoidance of a specific type of format or approach

The avoidance of specific formats or approaches was shown in two ways. First, subjects avoided visual items such as images and videos. Often, they did not even bother to look for non-visual alternative (e.g., transcript of videos and text description of images). Their perception was they were not worth the effort since visual items require sight. This situation reflects two possible failures of digital library design: failing to provide non-visual alternatives (in text or audio) to visual items and failing to provide the path to any available non-visual alternatives. A real problem here is that blind users avoid resources, even if they only think the resource might be visual. Clearer metadata, as well as display of format information in the search results lists, would help reduce this problem. To encourage blind users to explore visual items, thorough alternative text and detailed description for a visual item should be provided. Metadata elements tailored to blind users would be ideal.

Second, subjects avoided applying the browsing approach in interacting with the digital library. Instead, they relied on keyword search as much as possible. They perceived browsing through categories as time-consuming, cumbersome, inefficient and overwhelming, a method that involves uncertainty and too much listening. This reflects a lack of an accurate mental model for browsing a collection for relevant information. It implies digital library browsing function design does not conform to non-visual cognition and decision-making. Digital library design needs to provide self-explanatory browsing categories and semantic relationships amongst those categories to guide blind users to adopt browsing strategies.

In summary, the identification of seventeen help-seeking situations identified a gap between current design practices and the needs and behaviour of blind digital library users. The digital library design implications correspond to blind users’ unique perceptions, actions and cognitions in information search. Suggested design improvements must be experimentally validated from blind users before they become good practices, design principles and standards for blind-minded digital libraries.

## Conclusion

This study is one of a few studies that qualitatively investigated various help-seeking situations that blind users encountered in interacting with digital libraries. The blind subjects experienced different types of help-seeking situations in accomplishing three different search tasks. Seventeen help-seeking situations unique to blind users were identified at the physical and cognitive levels. Physical situations concern mostly accessibility and perception issues, while cognitive situations were more related to comprehension and attitude.

This paper provides some unique insights into blind users’ digital library interaction experiences. However, the study has some limitations of which the reader should be aware. First, fifteen subjects might be insufficient to represent the blind user group, even though forty-five search sessions were observed. Second, this study did not investigate associated factors that led to the help-seeking situations. Third, cognitive load has not been measured quantitatively because of the sample size and exploratory nature of this study. These limitations call for future studies that incorporate a larger sample with diverse demographic characteristics and include quantitative analysis. More important, factors related to help-seeking situations will be investigated to better understand what leads blind users to seek help in interacting with digital libraries. In particular, future research will need to measure cognitive status, such as cognitive load (e.g., by using NASA TLX) and cognitive style quantitatively by using pre-validated measurement instruments. Further research will also focus on the usability studies of the design of different help features based on blind users’ unique help-seeking situations.

## Acknowledgements

The authors would like to thank Dr. Wooseob Jeong for his contribution to the research design, data collection and data analysis part of the study. In addition, we also appreciate the insightful and constructive comments from the reviewers.

## About the authors

**Iris Xie** is a professor in the School of Information Studies at the University of Wisconsin-Milwaukee. Her research areas focus on interactive information retrieval, digital libraries, and human-computer interaction. She has engaged in several research projects related to the identification of help-seeking situations and help feature design for both blind and sighted users. She can be contacted at: [hiris@uwm.edu](mailto:hiris@uwm.edu).  
**Rakesh Babu** is an assistant professor in the School of Information Studies at the University of Wisconsin-Milwaukee. He is blind and has strong motivation to undertake and promote research topics relevant to the empowerment of the blind in the information society. His research expertise includes Web accessibility and usability, Human-Centered Computing, Cognitive Models, Online Education, and Social computing. He can be contacted at: [babu@uwm.edu.](mailto:babu@uwm.edu).  
**Soohyung Joo** is an assistant professor in the School of Library and Information Science at the University of Kentucky. His main research areas include digital libraries, information retrieval, information system design, and data analytics. He can be contacted at: [soohyung.joo@uky.edu](mailto:soohyung.joo@uky.edu).  
**Paige Fuller** is an adjunct instructor and researcher at the School of Information Studies at the University of Wisconsin-Milwaukee, where he has contributed to a variety of projects studying blind users’ experience of the Web. He can be contacted at: [pfuller@uwm.edu](mailto:pfuller@uwm.edu).

#### References

*   Arora, N.K., Hesse, B.W., Rimer, B.K., Viswanath, K., Clayman, M.L. & Croyle, R.T. (2008). Frustrated and confused: the American public rates its cancer-related information-seeking experiences, _Journal of General Internal Medicine, 23_(3), 223-228.
*   Babu, R. (2011). Developing an understanding of the accessibility and usability problems blind students face in web-enhanced instruction environments. Unpublished doctoral dissertation, University of North Carolina at Greensborough, Greensborough, NC, USA
*   Babu, R. (2013a). [Developing more accurate competence models for improved e-learning for the blind.](http://www.webcitation.org/6YxkMzcb2) _Journal of Information Science & Technology, 9_(2). Retrieved from: http://pascal.iseg.utl.pt/ojs/index.php/jist/article/view/139\. (Archived by WebCite® at http://www.webcitation.org/6YxkMzcb2)
*   Babu, R. (2013b). [Understanding challenges in non-visual interaction with travel sites: an exploratory field study with blind users.](http://www.webcitation.org/6YxkcV20u) _First Monday, 18_(12). Retrieved from: http://firstmonday.org/ojs/index.php/fm/article/view/4808\. (Archived by WebCite® at http://www.webcitation.org/6YxkcV20u)
*   Barreto, A. (2008). Visual impairments. In S. Harper & Y. Yesilada (Eds.), _Web accessibility: a foundation for research. (_pp. 3-13). London: Springer.
*   Bigham, J.P., Cavender, A.C., Brudvik, J.T., Wobbrock, J.O. & Lander, R.E. (2007). WebinSitu: a comparative analysis of blind and sighted browsing behavior. In _Proceedings of the 9th International ACM SIGACCESS Conference on Computers and Accessibility_ (pp. 50-58). New York, NY: ACM.
*   Blackmon, M.H., Kitajima, M. & Polson, P.G. (2005). Tool for accurately predicting website navigation problems, non-problems, problem severity, and effectiveness of repairs. In _Proceedings of the SIGCHI Conference on Human Factors in Computing Systems_ (pp. 31-40). New York, NY: ACM.
*   Blustein, J., Ahmed, I. & Instone, K. (2005). An evaluation of look-ahead breadcrumbs for the WWW. In _Proceedings of the Sixteenth ACM Conference on Hypertext and Hypermedia_ (pp. 202-204). New York, NY: ACM.
*   Borsci S. & Federici S. (2009). The partial concurrent thinking aloud: a new usability evaluation technique for blind users. In P. L. Emiliani, L. Burzagli, A. Como, F. Gabbanini & A.-L. Salminen (Eds.), _Assistive technology from adapted equipment to inclusive environments—AAATE 2009_ (pp. 421-425). Amsterdam, The Netherlands: IOS Press.
*   Bradbard, D.A. & Peters, C. (2008). Web accessibility: a tutorial for university faculty. _Communications of the Association for Information Systems, 22_(1), 143-164.
*   Bundorf, M. K., Wagner, T. H., Singer, S. J. & Baker, L. C. (2006). Who searches the internet for health information? _Health Services Research, 41_(3), 819-836.
*   Buzzi, M. C., Buzzi, M., Leporini, B. & Akhter, F. (2010). Is Facebook really "open" to all? In K. Michael (Ed.), _2010 IEEE International Symposium on Technology and Society_ (pp. 327-336). Piscataway, NJ: IEEE.
*   Chandrashekar, S. (2010). Is hearing believing? Perception of online information credibility by screen reader users who are blind or visually impaired. Unpublished doctoral dissertation, University of Toronto, Toronto, ON, Canada.
*   Chandrashekar, S., Stockman, T., Fels, D. & Benedyk, R. (2006). Using think aloud protocol with blind users: a case for inclusive usability evaluation methods. In _Proceedings of the 8th International ACM SIGACCESS Conference on Computers and Accessibility_ (pp. 251-252). New York: ACM.
*   Di Blas, N., Paolini, P. & Speroni, M. (2004). [“Usable accessibility” to the web for blind users.](http://www.webcitation.org/6YxkxYUqW) _In Adjunct Proceedings of the 8th ERCIM Workshop: User Interfaces for All_. Retrieved from: http://www.ui4all.gr/workshop2004/files/ui4all_proceedings/adjunct/accessibility/109.pdf. (Archived by WebCite® at http://www.webcitation.org/6YxkxYUqW)
*   Dworman, G. & Rosenbaum, S. (2004). Helping users to use help: improving interaction with help systems. In _CHI ’04 Extended Abstracts on Human Factors in Computing Systems_ (pp.1717-1718). New York, NY: ACM.
*   Fox, E.A. & Urs, S.R. (2002). Digital libraries. _Annual Review of Information Science and Technology, 41_, 503-589.
*   Giraud, S., Colombi, T., Russo, A. & Therouanne, P. (2011). Accessibility of rich Internet applications for blind people: a study to identify main problems and solutions. In _Proceedings of the 9th International Conference on Computer-Human Interaction-Italian Chapter_ (pp. 163-166). New York, NY: ACM Press.
*   Gwizdka, J. (2010). Distribution of cognitive load in web search. _Journal of the American Society for Information Science and Technology, 61_(11), 2167-2187.
*   Hembrooke, H.A., Granka, L.A., Gay, G.K. & Liddy, E.D. (2005). The effects of expertise and feedback on search term selection and subsequent learning. _Journal of the American Society for Information Science and Technology, 56_(8), 861-871.
*   Herrouz, A., Khentout, C. & Djoudi, M. (2013). [Navigation assistance and web accessibility helper.](http://arxiv.org/pdf/1307.5260v1.pdf) _International Journal of Application or Innovation in Engineering and Management, 2_(5), 517-523\. Retrieved from http://arxiv.org/pdf/1307.5260v1.pdf
*   Holsti, O. (1969). _Content analysis for the social sciences and humanities_. Reading, MA: Addison-Wesley.
*   Hsieh-Yee, I. (1993). Effects of search experience and subject knowledge on the search tactics of novice and experienced searchers. _Journal of the American Society for Information Science, 44_(3), 161-174.
*   Hu, R., Lu, K. & Joo, S. (2013). Effects of topic familiarity and search skills on query reformulation behavior. _Proceedings of the Annual Meeting of the American Society for Information Science and Technology_, 50, 1-9.
*   Kammerer, Y., Scheiter, K. & Beinhauer, W. (2008). Looking my way through the menu: the impact of menu design and multimodal input on gaze-based menu selection. In _Proceedings of the 2008 Symposium on Eye Tracking Research & Applications_ (pp. 213-220). New York, NY: ACM.
*   Keselman, A., Browne, A.C. & Kaufman, D.R. (2008). Consumer health information seeking as hypothesis testing. _Journal of the American Medical Informatics Association, 15_(4), 484-495.
*   Kim, J. (2006). Task as a predictable indicator for information seeking behavior on the Web. Unpublished doctoral dissertation, Rutgers University, New Jersey, USA.
*   Kriewel, S. & Fuhr, N. (2007). Adaptive search suggestions for digital libraries. In D. H. L. Goh, T.H. Cao, I. Solvberg & E. Rasmussen (Eds.), _Asian Digital Libraries. Looking Back 10 Years and Forging New Frontiers_ (pp. 220-229). Berlin-Heidelberg: Springer.
*   Jones, K.S., Farris, J.S. Elgin, P.D. Anders, B.A. & Johnson, B.R. (2005). A report on a novice user's interaction with the Internet through a self-voicing application. _Journal of Visual Impairment & Blindness, 99_(1), 40-54.
*   Lazar, J., Allen, A., Kleinman, J. & Malarkey, C. (2007). What frustrates screen reader users on the Web: a study of 100 blind users. _International Journal of Human-Computer Interaction, 22_(3), 247-269.
*   Leuthold, S., Bargas-Avila, J.A. & Opwis, K. (2008). Beyond Web content accessibility guidelines: design of enhanced text user interfaces for blind internet users. _International Journal of Human-Computer Studies, 66_(4), 257-270.
*   Liew, C. L. (2011). Help with health information on the web. _The Electronic Library, 29_(5), 621-636.
*   Liu, J. & Kim, C.S. (2013). Why do users perceive search tasks as difficult? Exploring difficulty in different task types. In _Proceedings of the Symposium on Human-Computer Interaction and Information Retrieval_ (pp. 5), New York, NY: ACM.
*   Matusiak, K. (2012). Perceptions of usability and usefulness of digital libraries. _International Journal of Humanities and Arts Computing, 6_(1-2), 133-147.
*   Monopoli, M., Nicholas, D., Georgiou, P. & Korfiati, M. (2002). A user-oriented evaluation of digital libraries: case study the “electronic journals” service of the library and information service of the University of Patras, Greece. _Aslib Proceedings, 54_(2), 103-117.
*   Moraveji, N., Russell, D., Bien, J. & Mease, D. (2011). Measuring improvement in user search performance resulting from optimal search tips. In _Proceedings of the 34th International ACM SIGIR Conference on Research and Development in Information Retrieval_ (pp. 355-364). New York, NY: ACM.
*   Mu, X., Ryu, H. & Lu, K. (2011), Supporting effective health and biomedical information retrieval and navigation: a novel facet view interface evaluation. _Journal of Biomedical Informatics, 44_(4), 576-586.
*   O’Day, V.L. & Nardi, B.A. (2003). An ecological perspective of digital libraries, In A.P. Bishop, A.A. Van House & B.P. Butterfield (Eds.), _Digital library use: social practice in design and evaluation_ (pp. 65-84), Cambridge, MA: MIT Press.
*   Othman, R. & Halim, N. S. (2004). Retrieval features for online databases: common, unique, and expected. _Online Information Review, 28(_3), 200-210.
*   Sahib, N.G., Tombros, A. & Stockman, T. (2011). A comparative analysis of the information-seeking behavior of visually impaired and sighted searchers. _Journal of the American Society for Information Science and Technology, 63_(2), 377-391.
*   Salampasis, M., Kouroupetroglou, C. & Manitsaris, A. (2005). Semantically enhanced browsing for blind people in the WWW. In _Proceedings of the Sixteenth ACM Conference on Hypertext and Hypermedia_ (pp. 32-34). New York, NY: ACM.
*   Savenkov, D. & Agichtein, E. (2014). To hint or not: exploring the effectiveness of search hints for complex informational tasks. In _Proceedings of the 37th International ACM SIGIR Conference on Research & Development in Information Retrieval_ (pp. 1115-1118). New York, NY: ACM.
*   Stefano, F., Borsci, S. & Stamerra, G. (2010). Web usability evaluation with screen reader users: implementation of the partial concurrent thinking aloud technique. _Cognitive Processing, 11_(3), 263-272.
*   Strauss, A. & Corbin, J.M. (1990). _Basics of qualitative research: grounded theory procedures and techniques_. Newbury Park, CA: Sage Publications.
*   Trenner, L. (1989). A comparative survey of the friendliness of online 'help' in interactive information retrieval systems. _Information Processing and Management, 25_(2), 119-136.
*   Vigo, M. & Harper, S. (2013). Coping tactics employed by visually disabled users on the web. _International Journal of Human-Computer Studies, 71_(11), 1013-1025.
*   WebAIM. (2014). _Screen reader user survey #5 result_s. Logan, UT: WebAIM, Center for Persons with Disabilities, Utah State University
*   Wexelblat, A. & Maes, P. (1999). Footprints: history-rich tools for information foraging. In P_roceedings of the SIGCHI Conference on Human Factors in Computing Systems_ (pp. 270-277). New York, NY: ACM.
*   Wildemuth, B. M. (2004). The effects of domain knowledge on search tactic formulation. _Journal of the American Society for Information Science and Technology, 55_(3), 246-258.
*   World Health Organization (2014). _[Visual impairment and blindness: fact sheet](http://www.webcitation.org/6Yxmb1wJs)_. Geneva, Switzerland: World Health Organization. Retrieved from http://www.who.int/mediacentre/factsheets/fs282/en. (Archived by WebCite® at http://www.webcitation.org/6Yxmb1wJs)
*   Wu, L. (2010). Help seeking behaviors in computer-based tasks: preliminary analysis. _Proceedings of the Annual Meeting of the American Society for Information Science and Technology, 47_, 1-2.
*   Xie, I. & Benoit, E. (2013). Search result list evaluation versus document evaluation: similarities and differences. _Journal of Documentation, 69_(1), 49-80.
*   Xie, I. & Cool, C. (2006). Toward a better understanding of help seeking behavior: an evaluation of help mechanisms in two IR systems. _Proceedings of the Annual Meeting of the American Society for Information Science and Technology, 43_, 1-16.
*   Xie, I. & Cool, C. (2009). Understanding help seeking within the context of searching digital libraries. _Journal of the American Society for Information Science and Technology, 60_(3), 477-494.
*   Zeiliger, R. & Esnault, L. (2008). The constructivist mapping of internet information at work with Nestor. In T. Sherbone, S.J.B. Shum, & A. Okada (Eds.), _Knowledge cartography_ (pp. 89-111), London: Springer.
*   Zeng, Q. T., Crowell, J., Plovnick, R. M., Kim, E., Ngo, L. & Dibble, E. (2006). Assisting consumer health information retrieval with query recommendations. _Journal of the American Medical Informatics Association. 13_(1), 80-90.