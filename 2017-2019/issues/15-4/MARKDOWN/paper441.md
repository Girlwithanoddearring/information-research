#### vol. 15 no. 4, December, 2010

# A study of labour market information needs through employers' seeking behaviour

#### [Sonia Sanchez-Cuadrado](#author); [Jorge Morato](#author), and [Yorgos Andreadakis](#author)  
Carlos III University of Madrid. Department of Computer Science, 28911 Leganes, Spain

#### [Jose Antonio Moreiro](#author)  
Carlos III University of Madrid. Department of Library and Information Science, 28903 Getafe, Spain

#### Abstract

> **Introduction.** The objective of this study is understand the information needs that businesses have while seeking Library and Information Science professionals and analyse how they formulate those needs.  
> **Method.** The analysis is performed by examining the professional skills and capabilities demanded in job offers published. A total of 1,020 job offers collected from a Spanish employment agency Website have been analysed for the period between 2006 and 2008\.  
> **Analysis.** Knowledge representation techniques using thesauri have been used for the automatic content analysis based on natural language processing. Data extracted from the corpora have been analysed statistically.  
> **Results.** Results of this study indicate a demand for skills related to technological advances and the management of electronic resources as well as to technical aspects associated with the Informatics domain. The knowledge of languages and the possession of an academic title represent essential factors in the job offers.  
> **Conclusions.** This method permits the identification of changes in the information needs and the contexts inherent to the profession. The advantage of using thesauri permits other research groups to reproduce the results. The re-use of semantic categories, common to other fields, facilitates the reproduction of this method with other occupational groups or social roles.

## Introduction

The principal task of information professionals is to satisfy the various information needs of users in different contexts, studying their requirements, their seeking habits and their management and use of information. According to information behaviour researchers, information seeking behaviour analysis is the study of the 'activities a person may engage in when identifying their own needs for information, searching for such information in any way and using or transferring that information' ([Wilson 1999: 249](#wil99)).

To date, information seeking behaviour research has the creation of models using quantitative or qualitative methods. Data are collected by interviews (structured or unstructured), questionnaires, observations, etc. User groups are usually created starting from different occupational groups, social roles, or demographic groups. One of the groups most studied has been library users, but in recent times new user behaviour research has put under scrutiny user behaviour in the field of informatics. In this field, methodologies were initially centred on studying the relationships with the interfaces of information systems, but recently more attention has been drawn to behavioural studies on the use of the Internet with different methodologies and user groups ([Choo _et al._ 1998](#cho98); [Rodden _et al._ 2007](#rod07)).

Some of the contextual parameters of information behaviour are: information goal, knowledge and skills, roles, engagement or work tasks ([Freund _et al._ 2005](#fre05)). In other words, understanding a profession's information seeking behaviour involves the study the users' roles, skills and tasks. Information and communication technologies, and especially internet technologies, have caused changes in the employment market which in turn have directly influenced the requirements of the knowledge and dexterities that graduates in the field of library and information science need to possess ([Zhou 1996](#zho96); [Xu 1996](#xu96); [Khurshid 2003](#khu03)). As the domain evolves the perception of the roles assumed by professionals, change. According to Fourie ([2006: 1](#fou06)) '_LIS professionals need to be confident that they can prepare for new challenges, deal with information anxiety and information overload and claim new professional roles_'.

In 1993, Sanz ([1993](#san93)) underlined the alarming scarcity of these studies in business. This author points to the work done by MacNabb at Queen's University and a project carried out by Spanish National Research Council, in which the differences in information needs in the business context were confirmed. Since then most of the research may be seen in the corporate and business domain ([Bigdeli 2007](#big07); [Yousefi 2007](#you07); [Ikoja and Ocholla 2004](#iko04)). These studies were performed through interviews, surveys and questionnaires which could present a certain bias ([Fowler 1993](#fow93); [Wiseman 1972](#wis72); [Crawford 1997](#cra97); [Naftali 1982](#naf82)) and impede their reproduction and extension.

Research results dating as far back as 1998, indicated an extension of the employment market towards the information technologies sector ([Tabah and Bernhard 1998](#tab98)). Other authors ([Albitz 2002](#alb02); [Khurshid 2003](#khu03); [Quarmby _et al._ 1999](#qua99)) have focused their investigation on the skills and knowledge required in the job advertisements. Results suggested that the most appreciated skills were the management of electronic resources, information search, database design and the creation of HTML documents. All these associated with skills in the informatics domain ([Quarmby _et al._ 1999](#qua99); [Albitz 2002](#alb02)). In Khurshid's studies ([2003](#khu03)), metadata schemes such as Dublin Core, the Text Encoding Initiative (TEI), Extensible Mark-up Language (XML) and the Resource Description Framework (RDF) acquired a great importance in the employment offers, because university libraries with large document collections demanded this type of knowledge.

The objective of this study is understand the information needs that businesses have while seeking Library and Information Science professionals and analyse how they formulate those needs. Our research was guided by the following questions:

1.  Is it feasible to determine the information seeking behaviour of the employers and the information needs of the labour market, by examining job advertisements?
2.  Do the roles, skills and tasks required from information professionals in the labour market present, currently, any particularity?
3.  What is the interrelation amongst the different parameters and contexts? For example, does increasing the demand for informatics knowledge, augment the need for other factors?

The method applied in this study is herewith explained. The method of data compilation and the creation of the resources needed for the performance of the analysis are also detailed. Then, the results are presented, in which the distinct roles, skills and tasks are analysed. Moreover, a study of the interrelationships amongst the distinct parameters is realized and finally, the conclusions are presented.

## Method

The market's information needs have been studied using an unobtrusive observation methodology, based on a mixed analysis (qualitative and quantitative) of job advertisements seeking information professionals. This approach shows employers' and headhunters' information behaviour when they seek such professionals through job advertisements.

Phases of this study include:

1.  Information sources: selection and development of a job advertisements corpus, as source of information.
2.  Thesaurus development and enrichment: development of a thesaurus, with the domain's terminology about a specific work role, along with the tasks and skills involved in this role. In the case that a domain thesaurus is not available, this phase is realized in two stages.
    *   Development: initially, a thesaurus is developed adapting a similar one or creating one from scratch.
    *   Enrichment: the thesaurus is evaluated. If it is necessary the thesaurus is adjusted and enriched with the categories and terms required.
3.  Content analysis of the job offers by means of automatic indexing using the thesaurus. This step includes a statistical analysis of the main categories of the thesaurus and the correlations amongst them.
4.  Evaluation: characteristics of the information needs are evaluated.

### Selection of information sources

The documents used to compile the elaborated corpus were 1,020 job advertisements, in Spanish taken from the documentacion.com.es Website ([Leiva and Sola 2008](#lei08)) corresponding to the period between 2006 and 2008 (the site ceased operations in 2008). This particular resource represented at that time the best communication channel specializing in the domain of library and information science in Spain. Its main characteristics are: it was dedicated to the Spanish ambit; it was organized by years and months and provided a history of requests, which facilitates temporal investigation.

### Thesaurus development

A study of the information resources related to employment is performed to elaborate the ontological model. The thesaurus contains the academic profiles of professionals in library and information science. This thesaurus includes concepts, relationships and instances and is composed of diverse taxonomies. It reflects different standardized and ad-hoc classifications, which are united by relationships and lists of instances. The principal resources that comprise the reference thesaurus were manually established and are listed below:

*   Knowledge. Using the _Library and Information Science Thesaurus_ of the Institute of Scientific and Technological Studies (previously known as CINDOC), we have extracted the terminology relative to library and information science and other related domains. We have based our work on 1,480 terms from this thesaurus, which are distributed in seven categories: _information science: history, theory and systems_; _documents and information sources_; _information science research and methodologies_; _professionals and users_; _information representation and retrieval_; _information systems_ and; _information technologies_. Knowledge categories that have been selected were: _information science sources_; _information science research and methodology_; _information representation and retrieval_; _information systems_ and _information technology_; and _academic degrees_ ([Mochon and Sorli 2002](#moc02)).
*   Competences. _transferable competences_ and _specific competences_. The transferable and specific graduates' competences which have been selected to be incorporated in the reference thesaurus come from the proposed classification in the library and information science _White Paper_ of the Agencia Nacional de Evaluación de la Calidad y Acreditación ([2004](#ane04)).
*   The professional profile of library and information science graduates. Analysis of the employment demand managed by the Centre for Information and Orientation for Employment of Complutense University of Madrid ([2005](#coi05)).
*   The ISO 639-1 ([2002](#iso02)) code for the representation of 187 language names.
*   Skills elaborated ad-hoc, used by human resources, were also added as a category.

### Enrichment of the thesaurus

After some initial tests it was realized that the thesaurus was not up to date with the necessary skills' terminology and this impeded the automatic identification of terms in the job advertisements. So, 479 terms (in Spanish) were added to the thesaurus based on the frequency of those terms in the _White Paper_ ([ANECA 2004](#ane04)) and the number of job advertisements that contained those terms. Some examples include: _natural language processing_, _ontology_, _usability_, _digitization_, _content managers_, _CMS_, _Joomla_, _Wiki_, _Moodle_, _SEO_, _search engine positioning_, _semantic web_ and _RSS_.

Also, a new category was created that included skills required by human resources and did not exist in the _White Paper_. For example, _charisma_, _customer care_, _commitment_, _efficiency_ and _methodical character_. Those terms added were relocated within the thesaurus to generic categories that could incorporate them depending on their predominant meaning in the job offers.

### Content analysis of the job advertisements

[QDA Miner](http://www.provalisresearch.com/QDAMiner/QDAMinerDesc.html) and [WordStat](http://www.provalisresearch.com/wordstat/Wordstat.html) software have been used to process the advertisements. By indexing the documents with the thesaurus we obtain the terms that appear in the documents along with their frequency and co-occurrence. In this study, one of the main characteristics of this content analysis and automatic indexing system is the identification of compound terms and the use of the thesaurus during indexing. Grouping the terms based on semantic relationships allows the frequencies and the co-occurrence values to relate to concepts and not to tokens.

### Evaluation

The analysis of the employment offers is realized in levels one, two and three of the hierarchy corresponding to knowledge and competences. We have established three types of competences: transferable competences, specific training competences in library and information science and competences used by human resources professionals. Result values are presented as a percentage of documents. A cluster analysis is performed on the job offers and the course descriptions to obtain the relationships amongst the thesaurus's families. Then, the matrix co-occurrence between knowledge and competences is analysed and finally an analysis of the results is performed without aggregating the various thesaurus categories.

## Results and discussion

### Reference thesaurus elaboration result

The reference thesaurus is formed of 1,850 terms distributed in twelve categories to represent knowledge and competences. In the hierarchy, knowledge and competences are differentiated as separate concepts to analyse their representation inside the job offers.

### Competences

The first results of the automatic process illustrate the most generic competences. In figure 1 we can observe how transversal competences are the ones that acquire greater importance in the job offers, even surpassing specific graduate competences. An analysis of the transversal competences' subtypes, illustrate that the most required transversal competences are the instrumental competences which greatly exceed systemic and personal competences (figure 2).

It is important to note that the sum of instrumental, systemic and personal competences in Figure 2 exceed transverse competences in Figure 1\. This is because various competences may appear in the same employment offer and so overlapping may arise.

<div style="text-align: center;">![Figure 1: Competence types](p441fig1.gif)</div>

<div style="text-align: center;">  
**Figure 1: Competence types**</div>

<div style="text-align: center;">![Figure 2:  Transferable competences](p441fig2.gif)</div>

<div style="text-align: center;">  
**Figure 2: Transferable competences**</div>

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 1: Transferable competences subtypes found in job advertisements**  
</caption>

<tbody>

<tr>

<th>Type</th>

<th>Competences</th>

<th>Advertisements (%)</th>

</tr>

<tr>

<td>Instrumental</td>

<td>Computer skills in the field</td>

<td align="center">37.80%</td>

</tr>

<tr>

<td>Instrumental</td>

<td>Knowledge of a foreign Language</td>

<td align="center">37.4%</td>

</tr>

<tr>

<td>Instrumental</td>

<td>Organizational skills</td>

<td align="center">8.3%</td>

</tr>

<tr>

<td>Systemic</td>

<td>Initiative and entrepreneurial spirit</td>

<td align="center">5.6%</td>

</tr>

<tr>

<td>Instrumental</td>

<td>Capacity to analyse and synthesize</td>

<td align="center">2.3%</td>

</tr>

<tr>

<td>Systemic</td>

<td>Creativity</td>

<td align="center">1.7%</td>

</tr>

<tr>

<td>Systemic</td>

<td>Self-learning</td>

<td align="center">1.5%</td>

</tr>

<tr>

<td>Instrumental</td>

<td>Oral and written communication in native language</td>

<td align="center">1.3%</td>

</tr>

<tr>

<td>Personal</td>

<td>Teamwork</td>

<td align="center">1.0%</td>

</tr>

<tr>

<td>Personal</td>

<td>Interpersonal communication skills</td>

<td align="center">1.0%</td>

</tr>

<tr>

<td>Instrumental</td>

<td>Information management skills</td>

<td align="center">0.4%</td>

</tr>

<tr>

<td>Instrumental</td>

<td>Decision making</td>

<td align="center">0.4%</td>

</tr>

<tr>

<td>Systemic</td>

<td>Leadership</td>

<td align="center">0.2%</td>

</tr>

</tbody>

</table>

Within _transferable competences_, _instrumental competences_ stand out over _personal competences_ and _systemic competences_. They acquire a particular relevance in the job offers because they include the _knowledge of foreign languages_ and _computer skills in the field_. Advertisements appear to give more importance to the computer skills in the field and to knowledge of a foreign language (Table 1). The languages in demand are: German, Spanish, Catalan, French and especially English. Moreover, in Table 1 we can notice standing out the transferable competences: _organizational skills_, _initiative and entrepreneurial spirit_ and _capacity to analyse and synthesize_. The rest of the values in the table appear scarcely represented. This low level of representation could be caused by the brief description of the employment offers.

<div style="text-align: center;">![Figure 3: Library and information science competences in job offers](p441fig3.gif)</div>

<div style="text-align: center;">  
**Figure 3: Library and information science competences in job offers**</div>

The specific library and information science skills that appear in the advertisements are illustrated in Figure 3\. The skills standing out are: _Knowledge of the professional environment in libraries and information science_, _information technology - telecommunications_; _information analysis and representation_ and _identification and evaluation of information sources and resources_.

### Knowledge

Table 2 contains the principal families related to thesaurus knowledge, along with examples. These categories group together hundreds of specific terms.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 2: Knowledge families: generic descriptors and specific examples**</caption>

<tbody>

<tr>

<th>Generic Descriptor</th>

<th>Specific Descriptor Examples</th>

</tr>

<tr>

<td>Information Technology (“Tecnologías de la Información”)</td>

<td>Library computing, hardware, software, Internet</td>

</tr>

<tr>

<td>Information Systems (“Sistemas de Información”)</td>

<td>Libraries and archives (types, services and techniques), documentation centre</td>

</tr>

<tr>

<td>Science and Technology (“Ciencia y Tecnología”)</td>

<td>academic disciplines (medicine, semantics, engineering, …)</td>

</tr>

<tr>

<td>Information Representation and Retrieval (“Representación y Recuperación de la Información”)</td>

<td>control languages, thesaurus, cataloguing, information representation, IRS, retrieval evaluation, retrieval models</td>

</tr>

<tr>

<td>Documents and Information Sources (“Información. Documentos. Fuentes de Información”)</td>

<td>information flow, sources, resources, business intelligence</td>

</tr>

<tr>

<td>Information Science Research and Methodologies (“Investigación y Metodología Documental”)</td>

<td>bibliometrics, citation analysis, statistics, research methods</td>

</tr>

<tr>

<td>Information Science: History, Theories and Systems (“Ciencias de la Documentación: Historia, teorías, sistemas”)</td>

<td>scientific policy, archivist theories, library science theories</td>

</tr>

</tbody>

</table>

<div style="text-align: center;">![Figure 4: Representativeness of the skills' generic descriptors](p441fig4.gif)</div>

<div style="text-align: center;">  
**Figure 4: Representativeness of the skills' generic descriptors**</div>

Figure 4 illustrates the relevance of the skills and knowledge according to their presence in the advertisements. This figure shows some generic categories of skills and the number of documents that reference these skills. We have observed that the jobs are not always described using a technical vocabulary for the respective domain. On the other hand, in the advertisements the importance of _information technology_ and _information systems_ is often over-emphasized. The theoretical content which is often related to research is rarely present.

Table 3 illustrates some examples of skills at a more specific level. The objective of this table is to show how the thesaurus has been structured and what kinds of terms have been added to it. Terms that did not exist in the original resources but were found in the advertisements were added to the thesaurus during its elaboration phase. The first column holds the name of the specific descriptor prepended with the initials of the family (or generic descriptor) in which it belongs added to the beginning (note Figure 4). Column 2 contains specific descriptors of the generic descriptor in the respective row of column 1.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 3: Skill subtypes (terms added appear in italics)**  
</caption>

<tbody>

<tr>

<th>Descriptor</th>

<th>Examples</th>

</tr>

<tr>

<td>Information technology—software</td>

<td>_office automation_, programming languages, _CMS_, _positioning tools_, SEO</td>

</tr>

<tr>

<td>Information technology—Internet</td>

<td>HTML, XML, metadata vocabularies, _Web_ _architecture_, _semantic Web_</td>

</tr>

<tr>

<td>Science & technology—science disciplines</td>

<td>aero spatial sciences, earth sciences, semantics, geology</td>

</tr>

<tr>

<td>Information representation & retrieval—Information representation</td>

<td>cataloguing, _ontologies_, indexing, abstracting, _content management_</td>

</tr>

<tr>

<td>Information technology—library computing</td>

<td>MARC21, databases, _digitizing_, _usability_, accessibility, library automation</td>

</tr>

<tr>

<td>Information systems—archives</td>

<td>_administrative archives_, municipal archives</td>

</tr>

<tr>

<td>Documents & information sources—documents</td>

<td>video, films, books, journals</td>

</tr>

<tr>

<td>Information systems—library techniques</td>

<td>library management, library use</td>

</tr>

<tr>

<td>Information science research—research methods</td>

<td>case study, user survey, scientific output</td>

</tr>

<tr>

<td>Information systems—libraries</td>

<td>digital library, _school library_</td>

</tr>

<tr>

<td>Information science research—statistical methods</td>

<td>statistics analysis, sampling</td>

</tr>

<tr>

<td>Information systems—archiving techniques</td>

<td>library stocks classification and arrangement</td>

</tr>

</tbody>

</table>

<div style="text-align: center;">![Figure 5: Level 2 demanded skills](p441fig5.gif)  
</div>

<div style="text-align: center;">  
**Figure 5: Level 2 demanded skills**</div>

Figure 5 illustrates the skills most demanded for the jobs. The importance of skills associated with _software_, _Internet_ and _library computing_ is clearly represented at this level. These technical skills are prerequisites in seeking a job. However, the fact that the job offers are published in a Website could imply a certain slant towards those variables.

To dig into concrete skills in demand, we have extracted the most frequent terms as a percentage of the number of cases. Without grouping into generic categories or synonyms, the following figures stand out in the list. In Figure 6, the required language skills are presented. In Figure 7, skills in management and editing of digital and Web content are presented, along with other desired knowledge. In Figure 8, we illustrate knowledge and skills related to information retrieval and optimization. Finally, in Figure 9, knowledge centred on information technologies and programming languages is presented. In all these cases, the great importance of technologies, especially those related to the _Internet_, _office automation_, _data bases_ and _content management_, is made apparent.

<div style="text-align: center;">![Figure 6: Skills in languages](p441fig6.gif)</div>

<div style="text-align: center;">  
**Figure 6: Skills in languages**</div>

<div style="text-align: center;">![Figure 7: Skills in management and edition of digital and Web content](p441fig7.gif)</div>

<div style="text-align: center;">  
**Figure 7: Skills in management and edition of digital and Web content**</div>

<div style="text-align: center;">![Figure 8: Skills in retrieval and optimization](p441fig8.gif)</div>

<div style="text-align: center;">  
**Figure 8: Skills in retrieval and optimization**</div>

<div style="text-align: center;">![Figure 9: Information technologies and programming languages](p441fig9.gif)</div>

<div style="text-align: center;">  
**Figure 9: Information technologies and programming languages**</div>

To test the relationships amongst knowledge and skills we analyse the similarity matrix to identify clusters. We select the phi coefficient to measure similarity because: 1) it takes into consideration the documents that do not have any occurrence of any of the two elements; and 2) it is not sensitive to the direction of coding.

<div style="text-align: center;">![Figure 10: Tree diagram of qualifications and skills in job offers](p441fig10.gif)</div>

<div style="text-align: center;">  
**Figure 10: Tree diagram of qualifications and skills in job offers**</div>

In the dendrogram in Figure 10, a cluster is observed that separates the possession of an academic title from the rest. Within the skills we notice that skills in information technology, the _transferable competences_ (e.g., _English language abilities_), _specific training competences in library and information science_ and the _competences used by human resources_ (e.g., _customer care_ or _orientation to results_) are united and have a higher frequency. On the other hand theoretical skills and skills specific to the discipline remain separated.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 4: Concurrence of knowledge and competences in job offers**  
</caption>

<tbody>

<tr>

<th></th>

<th>Archives</th>

<th>Libraries</th>

<th>Knowledge of a foreign lang</th>

<th>Science disciplines</th>

<th>Library comp.</th>

<th>Internet</th>

<th>Information representation.</th>

<th>Software</th>

<th>Marketing techniques</th>

<th>Academic title</th>

</tr>

<tr>

<th>Libraries</th>

<td align="center">42</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Knowledge of a foreign language</th>

<td align="center">77</td>

<td align="center">42</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Science disciplines</th>

<td align="center">87</td>

<td align="center">68</td>

<td align="center">**144**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Library computing</th>

<td align="center">56</td>

<td align="center">37</td>

<td align="center">102</td>

<td align="center">100</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Internet</th>

<td align="center">74</td>

<td align="center">55</td>

<td align="center">**317**</td>

<td align="center">**243**</td>

<td align="center">**266**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Information representation</th>

<td align="center">63</td>

<td align="center">84</td>

<td align="center">104</td>

<td align="center">**164**</td>

<td align="center">113</td>

<td align="center">**225**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Software</th>

<td align="center">96</td>

<td align="center">48</td>

<td align="center">**260**</td>

<td align="center">**206**</td>

<td align="center">**195**</td>

<td align="center">**725**</td>

<td align="center">**152**</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Marketing techniques</th>

<td align="center">11</td>

<td align="center">2</td>

<td align="center">**133**</td>

<td align="center">29</td>

<td align="center">25</td>

<td align="center">162</td>

<td align="center">24</td>

<td align="center">95</td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Academic title</th>

<td align="center">**181**</td>

<td align="center">**244**</td>

<td align="center">**291**</td>

<td align="center">**275**</td>

<td align="center">**171**</td>

<td align="center">**362**</td>

<td align="center">**353**</td>

<td align="center">**322**</td>

<td align="center">54</td>

<td> </td>

</tr>

<tr>

<th>Users</th>

<td align="center">53</td>

<td align="center">152</td>

<td align="center">44</td>

<td align="center">99</td>

<td align="center">71</td>

<td align="center">103</td>

<td align="center">115</td>

<td align="center">71</td>

<td align="center">4</td>

<td align="center">**391**</td>

</tr>

</tbody>

</table>

Table 4 presents the concurrence of knowledge and competences most demanded in the job offers. Term frequencies with more significant values in respect to the other categories have been formatted as bold. Categories with smaller frequencies have been eliminated for brevity. The _transferable competency_ of the _knowledge of a foreign language_, the possession of a relative _academic title_ and the knowledge of information technologies (_Internet_, _software_ and _library computing_) do stand out. It can be deducted from the table that knowledge in archive and library science is frequently associated with the possession of an academic title. The knowledge of languages and the possession of an academic title represent the two essential factors in the job offers. The importance of Internet is again decisive especially along with knowledge of languages, _software_ and _marketing techniques_. It is important to point out, that important concepts such as _search engine optimisation tools_ and _search engines positioning_ are not included into _marketing techniques_ even though they are related.

## Conclusions

The information needs of businesses requiring library and information science professionals to search for information have been approached, through an unobtrusive observing methodology, using the analysis of employment offers published in Websites addressed to this sector.

Skills and knowledge that library and information science professionals should have, as those are shaped by employers' demands, seem to have their own characteristics. These characteristics do not seem to reflect the typical role of a librarian or an archives expert.

transferable competences are the ones that acquire a greater relevance in the job offers (43%). Within the transferable competences, instrumental competences stand out because they contain competences relative to the knowledge of a foreign language, to skills relative to the discipline studied and to organizational skills. This underlines the importance of the knowledge of languages to access the job market in the library and information science domain.

The concurrency matrix shows the value that the employers grant to an academic title (mainly in library and information science). An interesting datum is given between _marketing techniques_ and _Internet_ which when analysed points to what seems to be new tasks for information professionals: _Web positioning_ and _search engine optimisation_. Information technologies as can be seen in the concurrency matrix are strongly related to the knowledge of Internet and _software_, or _Internet_ and _library computing_.

The readjustment of a thesaurus for the analysis facilitates the reproduction of results. Also, it permits the identification of changes in the information needs and the contexts inherent to the profession. Moreover, the advantage of using thesauri permits other research groups to reproduce the results and improve the thesaurus until a shared conceptualization is achieved. The extension of the vocabulary and the reuse of semantic categories common to other fields, permits in its turn the reproduction of this method with other occupational groups, social roles, or demographic groups.

The authors consider that this unobtrusive approach is complementary to questionnaires, surveys and interviews which are typical to information behaviour studies.

## Acknowledgments

This study has been financed by the Carolina Foundation training programme and the Hispano-Brazilian programme of University Cooperation of the Ministry of Science and Innovation and the mobility programme of the Ministry of Education (PHB2007-0099-PC).

We would like to thank documentacion.com.es webmasters, Javier Leiva and María José Sola, for supplying the corpus of job offers and authorizing its publication for research purposes.

## About the authors

Sonia Sánchez-Cuadrado Sonia Sánchez-Cuadrado works as an Assistant Professor in the Department of Informatics at Carlos III University of Madrid. In 2007, she received her PhD in Library Science and Digital Environment, designing a methodology for the automatic construction of knowledge organization systems and NLP. She can be contacted at [ssanchec@ie.inf.uc3m.es](mailto:ssanchec@ie.inf.uc3m.es)

Jorge Morato is currently a professor of Information Science in the Department of Informatics at the Carlos III University of Madrid (Spain). In 1999, he received his PhD in Library Science from Carlos III University. He can be contacted at [jmorato@inf.uc3m.es](mailto:jmorato@inf.uc3m.es)

Yorgos Andreadakis is currently researching for the implementation of his doctoral thesis with title 'Advanced Research on Natural Language Processing techniques applied to Semantic Extraction from Free Text' in the Department of Informatics at the Carlos III University of Madrid (Spain). He can be contacted at [gand@ie.inf.uc3m.es](mailto:gand@ie.inf.uc3m.es)

Jose Antonio Moreiro joined Carlos III University of Madrid in 1991\. He is Professor at the Department of library and information science of Carlos III University and leader of the Information Engineering Group researching on knowledge organization systems. He can be contacted at [jamore@bib.uc3m.es](mailto:jamore@bib.uc3m.es)

## References

*   Albitz, R.S. (2002). Electronic resource librarians in academic libraries: a position announcement analysis, 1996-2001\. _Portal: Libraries and the Academy_, **2**(4), 589-600.
*   ANECA. (2004). [_Libro blanco del título de grado en información y documentación_](http://www.webcitation.org/5tJJiurP2) [White paper on library and information science degree]. Madrid: ANECA. Retrieved 1 October, 2010 from http://www.aneca.es/media/150424/libroblanco_jun05_documentacion.pdf (Archived by WebCite® at http://www.webcitation.org/5tJJiurP2).
*   Bigdeli, Z. (2007). [Iranian engineers' information needs and seeking habits: an agro-industry company experience.](http://www.webcitation.org/5tJK69IHI)_Information Research_, **12**(2) paper 290\. Retrieved 1 October, 2010 from http://informationr.net/ir/12-2/paper290.html (Archived by WebCite® at http://www.webcitation.org/5tJK69IHI).
*   Choo, C.W.; Detlor, B. & Turnbull, D. (1998). [A behavioral model of information aeeking on the Web: preliminary results of a study of how managers and IT specialists use the Web](http://www.webcitation.org/5tEQZMt8V). In C.M. Preston (Ed.), __Proceedings 61st ASIS Annual Meeting__ , **35**, 290-302\. Retrieved 1 October, 2010 from http://www.ischool.utexas.edu/~donturn/papers/asis98/asis98.html (Archived by WebCite® at http://www.webcitation.org/5tEQZMt8V).
*   Universidad Complutense Madrid. _Centro de Orientación, Información y Empleo._ (2005). [_Inserción laboral. Consejo Social_](http://www.webcitation.org/5tEWPMx2n) [Labour insertion. Social Council]. Retrieved 1 October, 2010 from http://www.ucm.es/info/ucmp/cont/descargas/documento7415.pdf (Archived by WebCite® at http://www.webcitation.org/5tEWPMx2n).
*   Crawford, I.M. (1997). [_Marketing research and information systems_](http://www.webcitation.org/5tEQzSQ50). Rome: Food and Agriculture Organization. Retrieved 1 October, 2010 from http://www.fao.org/docrep/w3241e/w3241e06.htm#respondent induced bias (Archived by WebCite® at http://www.webcitation.org/5tEQzSQ50).
*   European Council of Information Associations. (2004). [_Euro-referencial en información y documentación. Competencias y aptitudes de los profesionales europeos de información y documentación_](http://www.webcitation.org/5tJLXqBvM) [Euroguide in library and information science. Competences and aptitudes for European information professionals]. Madrid: SEDIC. Retrieved 1 October, 2010 from http://www.certidoc.net/es1/euref1-espanol.pdf (Archived by WebCite® at http://www.webcitation.org/5tJLXqBvM).
*   Fourie, I. (2006). _How library & information science professionals can use alerting services_. Oxford: Chandos.
*   Fowler, F.J. (1993). _Survey research methods_. Thousand Oaks, CA: Sage Publications.
*   Freund, L., Toms, E. & Waterhouse, J. (2005). [Modeling the information behavior of software engineers using a work - task framework](http://www.webcitation.org/5tItVEb0u). _Proceedings of the American Society for Information Science and Technology_, **42** (1). Retrieved 1 October, 2010 from http://onlinelibrary.wiley.com/doi/10.1002/meet.14504201181/pdf (Archived by WebCite® at http://www.webcitation.org/5tItVEb0u).
*   Ikoja, R. & Ocholla, D. (2004). [Information seeking behavior of the informal sector entrepreneurs: the Uganda experience.](http://www.webcitation.org/5v5yk27tR) _Libri_, **54**(1), 54-66\. Retrieved 16 December, 2010 from http://www.librijournal.org/pdf/2004-1pp54-66.pdf (Archived by WebCite® at http://www.webcitation.org/5v5yk27tR)
*   International Standards Organization. (2002). [_ISO 639-1:2002\. Code for the representation of the names of languages_](http://www.webcitation.org/5tEQqmi6B). Retrieved 1 October, 2010 from http://xml.coverpages.org/iso639a.html (Archived by WebCite® at http://www.webcitation.org/5tEQqmi6B).
*   Khurshid, Z. (2003). The impact of information technology on job requirements and qualifications for catalogers. _Information Technology and Libraries_, **22**(1), 18-21.
*   Mochon, G. & Sorli, A. (2002). [_Tesauro de biblioteconomía y documentación_](http://www.webcitation.org/5tEV3q0cx) [Library and information science thesaurus]. Madrid: Centro de Informatión y Documentación Científica. Retrieved 1 October, 2010 from http://thes.cindoc.csic.es/index_BIBLIO_esp.php (Archived by WebCite® at http://www.webcitation.org/5tEV3q0cx).
*   Leiva, J. & Sola, M.J. (2008). _Documentacion.com.es: Documentación - ofertas de empleo y cursos_ [Documentacion.com.es: Library Science- courses and job offers]. Retrieved 31 March, 2008 from http://www.documentacion.com.es (the site ceased operations in 2008).
*   Naftali, A. (1982). _Questionnaire design, interviewing and attitude measurement_. London: Continuum.
*   Quarmby, K.L., Willett, P. & Wood, F.E. (1999). Follow-up study of graduates from the MSc information management programme at the University of Sheffield. _Journal of Information Science_, **25**(2), 147-155.
*   Rodden, K., Ruthven, I. & White, R. (2007). Workshop on Web information seeking and interaction. _ACM SIGIR Forum_, **41**(2), 63-67\.
*   Sanz, E. (1993). La realización de estudios de usuarios una necesidad urgente [Users' studies realisation, an urgent necessity]. _Revista General de la Información y Documentación_, **3**, 154-166.
*   Tabah, A.N. & Bernhard, P. (1998). Emplois occupés après la diplômation et champs d'activité percues comme importants "aujoud'hui" et "dans cinq ans" selon l'enquête menée auprès des diplômes de l'Ebsi (1991-1995) [Positions held after graduation and fields of activity viewed as important now and in five years' time. A survey conducted among 1991-1995 EBSI graduates]. _Argus_, **27**(22), 5-13.
*   Universidad Complutense de Madrid. _Centro de Orientación, Información y Empleo._ (2005). [_Inserción laboral. Consejo Social_](http://www.webcitation.org/5tEWPMx2n) [Labour insertion. Social Council]. Retrieved 1 October, 2010 from http://www.ucm.es/info/ucmp/cont/descargas/documento7415.pdf (Archived by WebCite® at http://www.webcitation.org/5tEWPMx2n).
*   Wilson, T.D. (1999). [Models in information behaviour research.](http://www.webcitation.org/5v5zPHJvl) _Journal of Documentation_, **55**(3), 249-270\. Retrieved 16 December, 2010 from http://informationr.net/tdw/publ/papers/1999JDoc.html (Archived by WebCite® at http://www.webcitation.org/5v5zPHJvl)
*   Wiseman, F. (1972). Methodological bias in public opinion surveys . _The Public Opinion Quarterly_, **36**(1) (Spring, 1972), 105-108.
*   Xu, H. (1996). The impact of automation on job requirements and qualifications for cataloguers and reference librarians in academic libraries. _Library Resources and Technical Services_, **40**(1), 9-31.
*   Yousefi, A. & Yousefi, S. (2007). [Information need and information seeking behavior of professionals at an Iranian company](http://www.webcitation.org/5tER9M4y9). _Library Student Journal_. Retrieved 1 October, 2010 from http://www.librarystudentjournal.org/index.php/lsj/article/viewArticle/65/124 (Archived by WebCite® at http://www.webcitation.org/5tER9M4y9).
*   Zhou, Y. (1996). Analysis of trends in demand for computer-related skills for academic librarians from 1974-1994\. _College & Research Libraries_, **57**(3), 259-272.