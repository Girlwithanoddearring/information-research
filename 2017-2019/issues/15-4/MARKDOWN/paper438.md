#### vol. 15 no. 4, December, 2010

# Dietary blogs as sites of informational and emotional support

#### [Reijo Savolainen](#author)

Department of Information Studies and Interactive Media, FIN-33014 University of Tampere, Finland

#### Abstract

> **Introduction.** This paper investigates the strategies that people employ to solicit and provide informational and emotional support in the blogosphere.  
> **Method.** A sample of eight Finnish blogs on dietary issues was selected for the study. The data contain 489 postings and 1,117 comments submitted to the discussion areas in 2009\. Descriptive statistics was used to calculate the percentage distributions for the categories of soliciting and providing support. In addition, qualitative content analysis was employed to characterize the nature of blog postings and comments.  
> **Results.** The bloggers mainly solicited emotionally-oriented support by describing problems faced in the dieting efforts and reporting experiences of personal success. About 63% of the postings solicited support of some kind. The readers commenting on the bloggers' postings primarily offered informational support, esteem support and emotional support. About 65% of the comments provided support of various kinds. The high percentage is mainly due to the specific nature of the blog as a person-centred forum in which the readers are expected to behave like invited visitors and provide support for the blogger's ongoing project.  
> **Conclusions.** Blogs represent empathic communities that also enable the seeking and provision of informational support. However, blogs do not primarily offer factual information since the main emphasis is laid on the sharing of experiences and opinions..

## Introduction

People seeking answers to everyday problems often find electronic support groups useful because they function as 'empathic communities' ([Preece 1999](#Preece1999)). In such communities, people not only seek information, but also make contact with others facing similar problems, tell their stories, and be heard. By sharing practical advice with one another, users may gain the wisdom that experience brings ([Loader _et al_. 2002](#Loader2002): 53). Because other participants share their interests, electronic support groups are likely to be congenial information environments, where information in which they are interested is likely to be found, even if they do not have explicit queries ([Burnett 2000](#Burnett2000)). Blogs afford new ways to make use of these 'empathic communities' online. Distinct from more traditional forums such as electronic mailing lists, blogs can focus more strongly on how individuals cope with everyday problems.

Since blogs represent a relatively new phenomenon, previous studies have not reviewed their use in sufficient detail. The present article contributes to the under-researched field by examining how people solicit and provide informational as well as emotional support in blogs. To this end, a case study was made by focusing on Finnish dietary blogs. This subject area is highly topical and far from trivial. National surveys conducted in the Western countries indicate that over 65% of adults are overweight or obese. The negative health outcomes associated with obesity include high blood pressure, type 2 diabetes, heart disease, and certain types of cancer ([Fraze and Wong 2008](#Fraze2008)). To make their dieting efforts more effective and to reduce health risks related to overweight, people tend to seek information about diet and physical exercise. Knowledge of health risks and benefits are a precondition to change. If people lack such knowledge there is little reason for them to change habits they enjoy (such as eating high-fat food). Thus, information is crucial for bringing about change ([Bar-Ilan _et al_. 2006](#Bar-Ilan2006)). Blogs and online communities can occupy a significant role in this regard.

The present investigation was preceded by a study that analysed the nature of interaction between bloggers and blog readers interested in dieting ([Savolainen 2010](#Savolainen2010)). This study drew on the same empirical data as the present one and used the categories of _Interaction Process Analysis_ originally developed by Bales ([1950](#Bales1950)). The study showed that the interaction mainly occurred through giving information and opinion, while the role of presenting questions to others remained secondary in this regard. It also appeared that the blog postings and comments exhibited more positive than negative reactions. The present study addresses a new issue, since it concentrates on the ways in which the blog contributors solicit and provide support, both informational and emotional. To find out whether blogs exhibit unique features as 'empathic communities', the study compares the ways in which people seek and provide support in blogs and in health-related online support forums.

## Literature review

### Blogs and their use

Weblogs, or blogs, can be generally defined as frequently updated websites consisting of dated entries arranged in reverse chronological order so the most recent posting appears first ([Rettberg 2009](#Rettberg2009): 191). Blog postings are primarily textual, but they may contain photos or other multimedia content. The blogging tools enable permanent links to the posted messages and simple archiving, and they allow readers of postings to comment on messages ([Aharony 2009](#Aharony2009): 175).

Previous studies have charted the number of blogs and sociodemographics of bloggers, as well as the motives and ways of blogging. Surveys have also investigated the frequency of posts, the usage of blog features, and the frequency of commenting ([Herring _et al_. 2005](#Herring2005)). For example, a survey conducted by Pew Internet & American Life Project revealed that 8% of Internet users aged 18 and over, reported keeping a blog in 2006\. Further, 39% of Internet users reported reading blogs ([Lenhart and Fox 2006](#Lenhart2006)). This survey also demonstrated that 87% of bloggers allowed comments on their blogs; 82% of bloggers said they have posted a comment to someone else's blog ([Lenhart and Fox 2006](#Lenhart2006): 20).

There has also been extensive research on blogging about political topics, as well as the relationship between blogs and journalism. The studies have shown that the majority of blogs are of the personal journal type, which reports the bloggers' personal experiences and reflections ([Schmidt 2007: 1409-1410](#Schmidt2007)). Nardi and her associates ([2004](#Nardi2004)) investigated the motives of blogging. Based on the interviews with twenty-three bloggers they identified five major motives: update others on activities and whereabouts; express opinions to influence others; seek others' opinions and feedback; 'think by writing'; and release emotional tension. These drivers were not mutually exclusive, since some blogs were motivated by more than one motive.

So far, there is a dearth of empirical studies characterizing how blogs are used in health-related contexts. Kovic and associates ([2008](#Kovic2008)) demonstrated that sharing practical knowledge and skills, as well as influencing the way other people think, were major reasons for blogging among the medical bloggers in the United States. The bloggers predominantly wrote about topics aimed at their fellow colleagues, specialists in various health related fields, or patients. However, only a small fraction of bloggers constructed their blogs around their personal lives, which would be of far greater interest to their friends and families.

Chung and Kim ([2008](#Chung2008)) found that cancer patients and companions found blogging activity to be most helpful for emotion and information sharing, followed by problem solving and prevention and care. In addition, cancer patients and their companions reported gaining information through their blogging activities and also found the information to be satisfactory. More recently, Karimi and Poo ([2009](#Karimi2009)) investigated the factors affecting medical bloggers' information sharing behaviour. Enjoyment in helping others and reputation were found to have significant direct affect on information sharing behaviour, while encouragement by others, identification, and interaction ties showed no significant direct affect.

### The use of health-related online support groups

Empirical studies have reviewed the role of online support groups in subject areas such as tobacco cessation ([Frisby _et al._ 2002](#Frisby2002)) and diabetes ([Loader _et al_. 2002](#Loader2002)). A study examining the use of a Usenet newsgroup on diabetes revealed that the overwhelming majority of messages provided informational support, for example, by specifying individual case histories and diet-related issues ([Loader _et al_. 2002](#Loader2002)). In addition, the newsgroup served the ends of social companionship support and self-esteem support, that is, encouragement and attempts to bolster the poster's spirits.

Coulson ([2005](#Coulson2005)) found that within a computer-mediated support group for individuals living with irritable bowel syndrome, informational support was most frequently provided. More recently, Meier and associates ([2007](#Meier2007)) explored how cancer survivors offer support on cancer-related Internet mailing lists. The most common kind of support was information and advice based on the experience of survivors. Four major themes were associated with survivors' offers of information and advice: specific treatments; communicating with health care providers to find the best treatment; problem management strategies; and coping with cancer recurrence. Explicit emotional support was less frequent than informational support. In general, the seekers reported that they were looking for both informational and emotional support.

Eichhorn ([2008](#Eichhorn2008)) drew similar conclusions in an investigation of online eating disorder support groups. Using a sample of 490 postings, she analysed the type of social support provided and the strategies used to solicit social support on the top five Yahoo! eating disorder discussion boards. About 54% of the messages posted solicited some type of social support, while about 56% provided some other type of support. Of the messages that solicited support, shared experiences accounted for 52% of the messages, followed by requesting information (25%), self-deprecating comments (15%), statements of personal success (6%) and statements of extreme behaviour (1%) ([Eichhorn 2008](#Eichhorn2008): 73). Of the messages, about 30% provided informational support, 28% emotional support, 21% social network support, and 10% esteem support ([Eichhorn 2008](#Eichhorn2008): 73). These findings are consistent with the Braithwaite, Waldron and Finn's ([1999](#Braithwaite1999)) study,which examined the social support provided in computer-mediated groups for people with disabilities. The authors concluded that informational and emotional support were most frequently offered on these online support groups.

Finally, Chang ([2009](#Chang2009)) investigated online supportive interactions within a psychosis online support group in Taiwan. To this end, the full sequences of supportive interactions within this group were observed over a year period from February 2004 to July 2006\. The results indicated that the most exchanged support types were information and network links. Informational support facilitated actions to solve problems while network support promoted comfort.

## Conceptual framework and research questions

The studies of online support groups discussed above indicate that researchers have converged in four different types of support that people may solicit and provide: informational, esteem, emotional, and social network support ([Chang 2009](#Chang2009): 1505-1506). The present study hypothesizes that in the context of blogs, too, people may solicit and provide support of these types. However, would blogs differ from traditional online support groups in this regard? This is possible since discussion groups are built around general topics such as eating disorders, while blogs report the personal experiences of an individual blogger. Different from the online support groups, one person, that is, the blogger sets the agenda for discussion. On the other hand, interactive blogs may have parallels with discussion groups since the blogs can concentrate on specific issues or projects that are most topical from the viewpoint of the blogger. More or less intentionally, the blogger may write the postings to solicit support for his or her ongoing projects such as dieting. On their part, the readers can employ various strategies to provide support for such projects. However, all postings and comments do not necessarily deal with soliciting and providing support.

In order to specify the strategies for soliciting support in the blogs, the present study employs the categories identified by North ([1997](#North1997)). This investigation focused on computer-mediated communication and social support among eating disordered individuals who contributed to _alt.support.eating-disordered_ newsgroup. More recently, Eichhorn ( [2008](#Eichhorn2008)) employed the same categories in a study reviewing the use of Yahoo! eating disorder discussion boards. The strategies are detailed in Table 1\. The examples illustrating the nature of individual strategies are taken from the empirical data of the present study.

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">**Table 1\. Categories for soliciting support ([North 1997](#North1997); [Eichhorn 2008](#Eichhorn2008))**</caption>

<tbody>

<tr>

<th>Strategy</th>

<th>Example</th>

</tr>

<tr>

<td valign="top">Presenting statements of self-disclosure or description of an experience that is found problematic</td>

<td>I have exercised, biked and walked. I have eaten in a healthy way everyday, max. 1700 calories. Alas, however, the scales persistently shows 95 kilos</td>

</tr>

<tr>

<td valign="top">Presenting statements of personal success and positive improvements</td>

<td>This morning the scales showed 83 kilos. So, within two weeks, by means of low carbohydrate diet I have managed to lose 3.7\. kilos</td>

</tr>

<tr>

<td valign="top">Presenting self-deprecating comments or negative statements about oneself</td>

<td>I would like to sink beneath the surface of the Earth! I weigh now a three digit number of kilos</td>

</tr>

<tr>

<td valign="top">Presenting statements of extreme behaviour that are above and beyond the _normal_ range of daily activities</td>

<td valign="top">Yesterday night I relapsed into needless eating</td>

</tr>

<tr>

<td valign="top">Presenting requests for information</td>

<td>Do you have any food tips for me? There might be carbohydrates 5-8 grams and protein 20-30 grams per portion, no limit for fat, vegetables 150-200 grams per portion</td>

</tr>

</tbody>

</table>

Even though these categories are particularly characteristic of people with eating disorders, they are general enough to describe the strategies employed by bloggers who make attempts to lose weight. Significantly, the above strategies are instrumental in trying to solicit informational as well as emotional support.

Second, to identify the types of support provided by blog readers, the present study draws on the categories developed by Cutrona and Russell ([1990](#Cutrona1990)), and Cutrona and Suhr ([1992](#Cutrona1992)). They observed different types of support provided to individuals who had just lost their jobs or family members. The above researchers proposed an optimal matching model of stress and support where different kinds of supportive messages would be significantly beneficial following different kinds of stressful events. Even though dieting may be a less dramatic process than losing one's job, the above model is applicable to the needs of the present study. The model elaborates in sufficient detail what kind of messages can be provided to support people struggling with stressful events related to dieting.

More specifically, Cutrona and Suhr ([1992](#Cutrona1992): 156) proposed that the controllability of the stressful event is of prime importance in determining the type of social support that will be optimally beneficial. In the context of controllable events, supportive acts that are directed at eliminating the source of stress or decreasing its severity will be most useful. Such acts are action-facilitating, and they include informational support such as providing advice. In the context of uncontrollable events, support components that diminish the severity of aversive emotions (for example, self-blame) will be most beneficial. Support of this includes expressions of caring (emotional support), and expressions of belonging and shared concerns (social network support). Esteem support can be found to be helpful in the context of both high- and low-controllable events. Expressing belief in a person's abilities may be helpful in promoting active problem solving when confronting a controllable event. Telling someone that he or she is valued may be helpful in maintaining self-esteem following an uncontrollable event such as a relapse into binging.

Four main categories describing strategies for offering support to other people are specified in Table 2 below. The main strategies such as providing informational support are specified into sub-strategies like providing suggestions/advice. Again, the examples illustrating the individual strategies are taken from the empirical data of the present study.

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">**Table 2\. Categories for providing support ([Cutrona & Suhr 1992: 161](#Cutrona1992))**</caption>

<tbody>

<tr>

<th>Strategy</th>

<th>Example</th>

</tr>

<tr>

<td valign="top">Providing informational support: Suggestions and advice / Referrral / Teaching</td>

<td>I always recommend light food products and less cold cuts with bread You might be interested to read Patrik Borg's new book A good source of protein from the viewpoint of snacking is cheese</td>

</tr>

<tr>

<td valign="top">Providing emotional support: Expressing sympathy / Expressing understanding and empathy / Encouragement / Expressing close relationship</td>

<td valign="top">I'm sure you are not the only person who could not resist gormandizing last week Oh, my dear friend. I was close to tears while reading your message Losing 6 kilos means a substantial achievment. Just cheer up! I hug you and wish you a great time for the weekend</td>

</tr>

<tr>

<td valign="top">Providing esteem support: Compliment / Relief of blame</td>

<td valign="top">Your self-discipline is admirable Don't slide into depression. I'm sure all of us sometimes experience similar feelings</td>

</tr>

<tr>

<td valign="top">Providing social network support: Providing access to other people / Indicating new companions</td>

<td valign="top">In case you would be interested in reading my slimming blog, please e-mail me: x.x.@ gmail.com :) I have read your blog since the beginning of this year and it has greatly inspired me</td>

</tr>

</tbody>

</table>

In sum: the conceptual framework of the present study consists of the identification of five strategies that can be employed to solicit support, and four strategies that can be used to provide support in the blogosphere.

Drawing on the framework specified above, the present study addresses the following research questions:

*   What kind of strategies do the bloggers employ to solicit support for the furtherance of the dietary projects?
*   What kind of strategies do the readers commenting on the blog postings employ to provide support for such projects?

## Empirical data and analysis

The empirical data were gathered in summer 2009 from Finnish blogs focused on dieting. The blogs were identified from the list of Finnish language blogs _Bloglista.fi_. By using keywords such as 'dieting' and 'weight loss', 644 individual blogs were identified. To obtain a preliminary picture of the dietary blogs, a sample of fifty blogs with most recent updates was selected from this list. The preliminary reading disclosed a considerable variation with regard to the content of individual postings, as well as the ways the readers commented on them. To identify blogs that would serve best the ends of the study, the sampling criteria were specified. Blogs to be taken into the empirical analysis should:

*   contain at least ten postings and thirty comments in order to enable the study of the interaction between the bloggers and readers commenting on the postings;
*   contain postings that cover a sufficient period of time in order to reflect the pursuance of the dieting project. The period should cover at least one month; however, the period should not exceed six months because a longer sample frame would have resulted in a disproportionately high number of postings and comments.

Of the fifty blogs selected for the preliminary sample, eight blogs met the above criteria. Purposive sampling was employed because the goal of the study is to make inferences about the nature of soliciting and providing support in the blogs rather than the size of the blogosphere or individuals active in blogging. More specifically, following Hayes ([2005](#Hayes2005): 43) and Gobo ([2006](#Gobo2006): 414), no random sampling was taken because the representativeness of the sample was evaluated by considering the variance of the types of support solicited or provided, instead of the variance of the individuals who contribute to such processes.

The period of time covered by the blogs varied from six months (5 January - 30 June 2009) to five weeks (19 May - 23 June 2009). The blogs contained 489 postings and 1,117 comments submitted to the blog's discussion areas. Thus, on average, a posting attracted two comments. Of the comments, 67% were written by the readers and 33% by the bloggers. Altogether 108 blog readers contributed by writing comments. The number of postings ranged from 14 to 125\. The lowest number of comments for a blog was 35 and the highest 297\. The length of the postings varied considerably. The longest posting contained no less than 1, 827 words (corresponding roughly to 3.5 A4 pages (c. 500 words per page), while the shortest one entailed only five words. In general, the comments were shorter than postings; typically, a comment was comprised of three to four sentences.

The background data provided by the bloggers revealed that all of them were females. Apparently, the majority of the blog readers were females, too, even though their nicknames or pseudonyms were not necessarily sex-specific. However, in most cases, the sex of the readers could be inferred easily from the content of their messages (for example, some commenters presented themselves as mothers).

The postings and comments were first transferred from the blogs to a separate file by cutting and pasting. Then, the postings were coded inductively by identifying the major issues constitutive of the content of the dieting project. The categories included, for example, the difficulties encountered in the dieting efforts, and physical exercise (see Table 3 below). In the coding of the content of the dieting project, the unit of coding was an individual issue discussed in a posting. Then, the postings were coded by identifying categories describing strategies for soliciting and providing support. These categories are detailed in Tables 1 and 2 above. In the coding, the unit of coding was an individual issue discussed in a posting. A posting was coded only once for every topical category (for example, difficulties encountered in the dieting efforts) when it was identified for the first time in the message. In long postings, it was not unusual that the same issue was discussed in several segments of the same posting. In these cases, once the posting was coded for a category, other instances were simply ignored. The same procedure was applied in the coding of messages indicating support solicited or provided.

In the coding, the categories discussed above appeared to be specific enough to capture the variation in soliciting and providing support of various kinds. To enhance the consistency of the coding, the present researcher scrutinized the material several times. As a result, the initial coding was refined until there were no anomalies. In the refining of the coding, the main difficulty concerned the category of _presenting statements of self-disclosure_ which appeared to be subject to multiple interpretations. To avoid ambiguities, only postings that explicitly reported problems faced in the dieting efforts were coded into this category. Thus, for example, sentences merely reporting facts about the nature of daily meals were excluded because in this case the blogger did not indicate any doubt about the suitability of her diet.

The data were analysed by means of descriptive statistics. First, the percentage distribution was calculated for the issues related to dieting. Most importantly, the percentage distribution for the categories of soliciting support were calculated with regard to the postings. Then, the percentage distribution for the categories of providing support were calculated with regard to the comments submitted by the blog readers. The quantitative findings are illustrated qualitatively by providing a few quotations taken from the postings and comments. Importantly, the sample of eight blogs appeared to be large enough for the drawing of a good qualitative and indicative quantitative picture of the nature of interaction in these forums. As to the qualitative analysis, the number of postings and comments appeared to be sufficient because the data became saturated. Thus, it became evident that the analysis of additional blogs would not have essentially changed the qualitative picture. As to the quantitative study, the data were sufficient for the needs of descriptive statistics, that is, the determination of percentage distribution of the categories of support solicited or provided.

Even though the blogs analysed in the present study are publicly available, the anonymity of the bloggers and commenters is protected in two ways. First, the blog contributors will not be identified by their nicknames or pseudonyms. Instead, the contributors are referred to by technical codes; for example, B-2 stands for Blogger 2 and R-75 for Reader 75 commenting on the postings. Second, all information about the submission dates for postings and comments was deleted from the quotations. This procedure makes it very unlikely that individual blog contributors could be identified from the excerpts written in Finnish.

## Empirical findings

### The main issues discussed in the blogs

The bloggers discussed a variety of issues related to dieting. The analysis revealed thirteen main issues that were referred to 1,273 times in 489 postings. The former number is higher because several issues may be discussed in an individual posting (see Table 3).

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom"><a name="Table3">**Table 3\. The 13 main issues discussed (n = 1273) in the postings (n = 489**</a></caption>

<tbody>

<tr>

<th>Issue</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>The evaluation of results obtained in dieting</td>

<td>284</td>

<td>58.0</td>

</tr>

<tr>

<td>Diet and food</td>

<td>235</td>

<td>48.0</td>

</tr>

<tr>

<td>Physical exercise</td>

<td>135</td>

<td>27.6</td>

</tr>

<tr>

<td>Miscellaneous issues of overweight (e.g., television programmes on this topic)</td>

<td>109</td>

<td>22.2</td>

</tr>

<tr>

<td>Personal goals of dieting</td>

<td>93</td>

<td>19.0</td>

</tr>

<tr>

<td>Contextual factors of dieting efforts</td>

<td>74</td>

<td>15.1</td>

</tr>

<tr>

<td>Problems caused by overweight</td>

<td>60</td>

<td>12.2</td>

</tr>

<tr>

<td>Problems encountered in dieting</td>

<td>57</td>

<td>11.6</td>

</tr>

<tr>

<td>Specific methods of dieting (e.g., Atkins diet)</td>

<td>53</td>

<td>10.8</td>

</tr>

<tr>

<td>The consequences of the dieting efforts</td>

<td>18</td>

<td>3.7</td>

</tr>

<tr>

<td>The schedule of the dieting efforts</td>

<td>17</td>

<td>3.4</td>

</tr>

<tr>

<td>The nature of overweight</td>

<td>11</td>

<td>2.2</td>

</tr>

<tr>

<td>Issues not related to dieting (e.g., shopping)</td>

<td>127</td>

<td>25.9</td>

</tr>

</tbody>

</table>

The bloggers reported most often how they had succeeded in the dieting efforts. Topics frequently discussed also included the nature of the daily diet, and physical exercise undertaken. The bloggers also commented on television programmes and books discussing the issues of overweight and dieting. To some extent, the bloggers paid attention to the personal objectives of dieting, and diverse contextual factors such as earlier attempts to lose weight. The topics also included specific methods of dieting such as the Atkins Diet. Many bloggers reported daily problems caused by being overweight, (for example, difficulties faced in walking up stairs). There were three topics that were discussed quite seldom: the consequences of dieting (for example, lowering the risk of diabetes), the schedule of the dieting project, and the features of being overweight (for example, the nature of one's obesity). On the other hand, the bloggers devoted considerable attention to themes not directly related to dieting for example, shopping (about 26% of the postings).

### The strategies for soliciting support

Of the postings (n = 489), 309 (that is, about 63%) included expressions that served the ends of soliciting support of some kind. There were some variations among the bloggers since the share of such postings varied from 34.9 % (lowest) to 85.7% (highest). Table 4 specifies the quantitative picture of strategies employed by the bloggers to solicit support from the blog readers. In total, the number of items assigned to strategies for soliciting support was 358\. Since a posting could serve multiple support strategies, the sum of percentages exceeds 100 in Table 4.

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="50%"><caption align="bottom">**Table 4\. Categories for soliciting support in the dietary blogs (the number of postings are 309; the number of items assigned to the postings is 358)**</caption>

<tbody>

<tr>

<th>Strategy</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>Self-disclosure of an experience</td>

<td align="center">152</td>

<td align="center">49.2</td>

</tr>

<tr>

<td>Statements of personal success</td>

<td align="center">124</td>

<td align="center">40.1</td>

</tr>

<tr>

<td>Statements of extreme behaviour</td>

<td align="center">30</td>

<td align="center">9.7</td>

</tr>

<tr>

<td>Requests for information</td>

<td align="center">26</td>

<td align="center">8.4</td>

</tr>

<tr>

<td>Self-deprecating comments</td>

<td align="center">26</td>

<td align="center">8.4</td>

</tr>

</tbody>

</table>

The bloggers drew most frequently on the strategy of disclosing problematic experiences. This strategy was employed almost in every second posting that indicated soliciting support. The issues related to such experiences varied considerably. Quite often, the bloggers reported their attempts to start a new diet or failures faced in the slimming efforts.

> On Friday morning the scales showed 78.5 kilos, all too familiar for me. At that time I decided to eat a minimal amount of carbohydrates, in other words, to start the Atkins Diet. (B-3)

Reporting personal success in dieting efforts appeared to be a popular strategy, since 40% of the postings soliciting support drew on it. Most often, the dieters characterized their successful efforts to lose weight. In some cases, they also reported about their ways to increase the amount of physical exercise, despite problems faced in the initial phase of the dieting project.

> A wonderful day. I jogged about 5 kilometers in the morning. Then I became really delighted after having weighed myself. Yippee! The scales showed minus 1.8 kilos. I weigh now 83.2 kilos. I'm very satisfied with this result. (B-1)

Compared to the above strategies, the bloggers less frequently reported incidents of extreme behaviour in order to solicit support. More specifically, this strategy is based on disclosing behaviour that is above and beyond the 'normal' range of daily activities for a person pursuing dieting efforts.

> I have crammed my mouth full of food. In working days I have managed to keep some control but at weekends there are no limits. This all is called BED (Binge Eating Disorder). (B-4)

Similarly, the bloggers rarely wrote self-deprecating comments to solicit support. Typically, this strategy manifested itself in the presentation of negative statements about oneself, due to the failures faced in the dieting efforts.

> I would like to sink beneath the surface of the Earth! I weigh now a three digit number of kilos. I just cannot dress my appalling feelings into words. I'm feeling like vomiting, disgust, anger, ache. (B-5)

Finally, the bloggers seldom requested for information to solicit support. Only about 8% of the postings could be classified into this category. The topics of the requests for information mainly revolved around the diet.

> I just pondered whether to buy a chromium-based product to fight the lust for something sweet. Does anyone have positive experiences of these products? Are there other methods to keep the lust for something sweet in control? (B-6)

### The strategies for providing support

The readers presented altogether 1,117 comments to the postings. Of them, 728, (that is, about 65%) provided support of some kind. Thus, two comments out of three appeared to be relevant from the perspective of supporting the dieting projects. Table 5 specifies the quantitative picture of the strategies employed in the provision of support. Since a comment could serve multiple support strategies, the sum of percentages exceeds 100 in Table 5.

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">**Table 5\. Categories for providing support in the dietary blogs** (the number of comments providing support is 728; items assigned to providing support are 1,202)</caption>

<tbody>

<tr>

<th>Strategy</th>

<th>n</th>

<th>%</th>

</tr>

<tr style="font-weight: bold;">

<td>Informational support</td>

<td align="center">528</td>

<td align="center">72.6</td>

</tr>

<tr>

<td>Teaching</td>

<td align="center">471</td>

<td align="center">64.8</td>

</tr>

<tr>

<td>Suggestions or advice</td>

<td align="center">51</td>

<td align="center">7.0</td>

</tr>

<tr>

<td>Referral</td>

<td align="center">6</td>

<td align="center">0.8</td>

</tr>

<tr style="font-weight: bold;">

<td>Esteem support</td>

<td align="center">323</td>

<td align="center">44.4</td>

</tr>

<tr>

<td>Compliment</td>

<td align="center">290</td>

<td align="center">39.9</td>

</tr>

<tr>

<td>Relief of blame</td>

<td align="center">33</td>

<td align="center">4.5</td>

</tr>

<tr style="font-weight: bold;">

<td>Emotional support</td>

<td align="center">275</td>

<td align="center">37.8</td>

</tr>

<tr>

<td>Encouragement</td>

<td align="center">169</td>

<td align="center">23.2</td>

</tr>

<tr>

<td>Understanding and empathy</td>

<td align="center">88</td>

<td align="center">12.1</td>

</tr>

<tr>

<td>Sympathy</td>

<td align="center">16</td>

<td align="center">2.2</td>

</tr>

<tr>

<td>Relationship</td>

<td align="center">2</td>

<td align="center">0.3</td>

</tr>

<tr style="font-weight: bold;">

<td>Social network support</td>

<td align="center">75</td>

<td align="center">10.3</td>

</tr>

<tr>

<td>Companions</td>

<td align="center">65</td>

<td align="center">8.9</td>

</tr>

<tr>

<td>Access to new companions</td>

<td align="center">10</td>

<td align="center">1.5</td>

</tr>

</tbody>

</table>

The readers mainly provided support for the bloggers by offering information. About 73% of comments providing support drew on this category. According to Cutrona and Suhr ([1992](#Cutrona1992): 156), informational support is most helpful if it is directed at eliminating the source of stress or decreasing its severity. In this way, informational support may facilitate one's possibilities to continue action, for example, problem-solving. The readers mainly provided informational support by ”teaching”. It may offer detailed information, facts, or news about the situation or about skills needed to deal with the situation. Often, facts or opinion were provided about diets that had been found effective to lose weight.

> The lust for something sweet was calmed down when I started eating soundly: sufficient breakfast, sufficient intake of proteins and good fat, but less carbohydrates. (R-49)

The role of other types of informational support remained marginal. However, some readers provided suggestions or advice, or referred the blogger to some other source of information, for example, a website. Again, the suggestions or advice primarily dealt with the nature of the daily diet.

> You should decrease the amount of rice, pasta and potato as much as possible, and increase the amount of vegetables, at least 500 grams a day. (R-75)

> Please, visit http://www.karppaus.info. You may find interesting things there. (R-60)

The provision of esteem support occupied a significant role in the readers' comments since 44% of them were related to it. Cutrona and Suhr ([1992](#Cutrona1992): 156) suggest that esteem support is most helpful when other people express a belief in a person's abilities in problem solving. In addition, esteem support may be useful when someone is told that he or she is valued. The provision of compliment, that is, saying positive things about the blogger was most popular way to provide esteem support. This strategy was strongly built on encouraging people's efforts to further their projects.

> Wow, wow and wow! I cannot help admiring you. You have done this all just by changing your diet. (R-27)

Esteem support was also be offered by alleviating one's self-blame or feelings of guilt about the situation. Sometimes, the reader consoled the blogger by sharing her experiences about similar failures.

> Don't worry even though your weight has been stuck. Maddening weeks like this are characteristic of the slimming effort. Your body is just asking you whether you are serious with your project. (R-26)

The readers also provided a considerable amount of emotional support. According to Cutrona and Suhr ([1992](#Cutrona1992): 156), emotional support is most useful in the context of uncontrollable events in which the person may experience aversive emotions. Most often, emotional support was offered by encouraging the dieting efforts and by providing the bloggers with hope and confidence. Similar to the esteem support, the positive emotional tone was strongly emphasized in this context.

> Encourage yourself before you weigh yourself tomorrow! Whatever the result, do not give way to distress. (R-107)

Emotional support was also provided by expressing understanding of the situation, and disclosing a personal situation that communicates understanding.

> Oh, this sounds all too familiar for me because I struggling with the same problem. I have gained extra 700 grams within a week. (R-107)

Sometimes, the readers provided emotional support by expressing sympathy. More specifically, they expressed sorrow or regret for the recipient's situation or distress. However, the readers rarely expressed emotional support by stressing the importance of closeness and love in relationship with the recipient.

> Oh, no! I hope you can survive your failure! (R-15)

Finally, the provision of social network support appeared to be rare. Such support can be expressed by indicating that a person belongs to a community. About 10% of the comments offered support of this kind. In most cases, the readers reminded the blogger of the availability of companions who struggle with similar problems.

> In case you would be interested in reading my slimming blog, please e-mail me: x.x.@ gmail.com ![smiley](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAWaUlEQVR4Xu2bebAlV33fP79zum/fe9+99+3z3qyaGc0iCQ3LzEhIMpalICQKx0JYyEAkgTEJlAtX4qLKdhHsgBIndoWYONj8gR0gWAI5BoPAC7KQUyJG20gapAgxZvbhzfZm3r7crfuck+5TXdXFvEUjBymqsk/Vd86dmdPn/j7f/p2tXz9xzvGPuSj+UZd/MuCfDAh4mcs9Iuquu9gaOHaheI0I2wRGlZY+HFUAhKY1bsbBWec4jOWFRHj+vvs4+nHnLC9jeVkmQUnLsbu52gm3hpqbVShX6FBVVUmhSoIoAS0AhYzDWYftZrKY2DZt7H4QGx4Sxze33Ms+l5ZXtQHP3CrVwT5+QWl+qVRWP6UrSqmqRkUZuEOURbRLJSAXGOAczmQSnFXYrmA7Fts0mJa13bZ91Bo+PznDn+35pmu+quaAR26U4Ph75RdHhuXJakN/oWc0+ulofaSidQHRsKM0rCmNDlHe9nqiK28h2vNeytf8MpXr/o1X+tn/W/p/vk3aNrvGX+v7SPvK+sz6zr4j+67sO18VGXDwTrm6HPKfyjX95qA3IGhoJDKockDQtxE1+gbUyB6ktg0pD0BQBdEgAA4AEHCAM5A0ce0p3MJh7Pgz2LPfI5kZw7YTXEeTzBmS2YT2gvnbdsy/3fElt+//iwGSlqN38pFyVe4p9Yc9ui9M2SyqGhCM7EJfcjNq+GqoDORwCWAhNxwBHMt8FkCBBCBAawp7fh/mxEMk489jmwlJU2FmYrrT8WK76T6+9Ut8yqXllTDAgz99B43hCn9Yruu7wqESQa9GVwzBmh3obbej1lwDQRlcF5xBRPEPKc7ZPFtKkLSx557AHP5zknMHMS1NMmuIJ7q0581951v8yt6vMOeNeJkM8PD7b2XtwABfqvYGN4QjKXxdCGppfektqM23I1Ef2DYCSyc6uVhylk6QAKqM68xgj6cmHPkbkoUuybwjHu/SnE0emZrizt3f5MxLMSF4qfBDAzxQHgyvioYjdB2C/n6Cy9+DGvlpcDGSzILIamAvXQ4EIOmA1uhtdyL1TciB+xE1jVJlCDo3KOIH0hhvE5GLNiG4WPgHb6E/hb/fw49m8ELYP0jwmg8g/ZcjZhaQJWP7J1IKFMQBNFFrriYM68gLnwM9SaQjgKuGiO9PY32HiExfjAnBxcAD6rJRPl3uD66P1kTohkrh+wguvxvV2AzxNIgq4HnZDCgywrSQxmaCK+6GA18EmSGyEVh3/WUu+TTwPhGxL2ZCcDHwB+/iI5WGvjNcUyLo0wT1tN76s0htIyQzHh63DHBUgkBDqwPW8pKKUlCJIDHQ6S5vSNLOYvCxcPCrCF2sLVFJ3J0H7zLP7biPTxUmvKRJsIB/7A72bO6X/1VeX+4pDUcEDQg2vgm1/iYgARGE5eBDnt9/mB8eOs0/f+teyrWyh7moEmjaC23+8sGn2bl9Hbt2b4NOvMQEB+DZAuyph0nGvksyB93zHdqn2ovHp90/u+4rPAOsaEKwGvzdI5TX1/lkNBj2hP0hQU2h+0dRQ28Auwg4JKdHfhz+hf2H+K3f/AJTkx0OHjjEb3z03WgsWFYvCkyi+NR//VMe/NZzDAxG/Mfffj+vef3WHzfBgeA8P4iPSS8cBXsWl4S4tulZn3Q/mTK87d5x2kUmXNwQEEB97CbeVanp64MUXtdCVEWhBq8ErcA0EVl+qbOdDv/zyw/RCDpsujTk2Sef46lHL+ean7kCWl1WLaUST33neX/N69NrZxZ8X3zi8rtQ1oJbksJ4LB2gBnfhmufRiRD0GyqL5vqP3WTede+XuBdwwIsZUKT+R6+m0RPJr3v4eqqyoGpDSHUUzAIiDpwsTf1Qc/zQec786BSXDIdEocLE8MRjz3PNdZvBxeBWsT1OfNvhGqxpKHorYdZX2ucYW7cPQ2wAlmaCFaQ64mPU8XlsPfQm9CzYX//o1e4bv7OPWRFxWXmxDFCAfs82bo8aemfQCFCVVJFCautBHGKbK6/1UYnDh04S2A6NckQYCAP1gNNjZ2nNzFKpKjArOKAlbdPM2vprapEQacXEbMf3uXVnL5juSpMZTgIfo2pO+piz2KNGsvM925LbUwO+CDjALG9AcffljQNE9bJ8IGhoVE+ALiskipByL2JbgIUV+Iktp0+fpxIKoU6loFJSzC02GT83xebN9ZVXBK3SNvN0mk36epS/Fi1ZX75P4jbY7iqbpRiyGKMIHXewPf5wRn3OfOCNA+5Pn5zCSJ4GK2WAAPp3r2d3pSp7dS1AlTWqpJBSBVEBmBaIA1Y2YHZ6nigUtAIleCOcSZidmQNXAmNWmv2zNr5tqBVKvCdZX75Pb4BZZQ5xksXoY1Wl2MeeMVSq8d7fvd7tvvEBHgcssKIB3vORmro1rGn/MEOXNBIoCCIgWX0MA1hLu90m0IIIXkrhodutRaABNmb5Evo2GINSKr+erC/fJ7bIgJUlPlYJlI/dVDUZy0iNW8HuAwxgCwMumPy29hNWyu5G8U9yNBIqJBCUDsB2eNF1zBp/B5UIQu6qgADOxjlEsvK1Nkbya4D8s88gsK3CvBWlfKw2EB+7Z6hqKuXkxozt6DRJMQwguHDp+/getkZl2akrGilpVGaA1oDxBuBWMECKk1sUBTQdRXGgAkUYxBDPpUqKTCwwIQiyNr4tDii69H3iukUGrLiSKMD4mFXoPEPGkjF9fI/b+r6HOZCzLmuA3lhnVxCpin+OFwjoPA9dB2yTFTf8LpP17WoNmLQFg3UQhJpaeQoWZyC2yxAIWJW2sb6tdQWntfg+cfNgbA4JwNJYnPMxID52z5CxZEwb63YXcDCHYNkM6KuoK3QkSJhKK0SJFy6GZB5U+YL9qAEbg8tkQMOaYcth42PxSgyEkWKwX3mAFU9KxmZtfNvE2Px6iI3vE+Jx6AKiQUJQqUQXPJBnaZzHnTOEQsaUsYH9+koGqExR4C6VQOHhtYBSxZqfLIJ0QVQBnwlHEYBm44YII4KxDqWETtfSP1qmvxFAe5UxbPFt+odKtM42sRXt+zAivk8sxXfSBSMgujDBWQ8P5LOvZ/AsGVMUJJeScxYGUKz/gA6FNeSpL5LDowpG1y0ARJbeza5l2+YKPX0pRCdGRFhsO153RR0dAq1VjskO32ZH2vax44v01qDVcVlfvk+63R8fcuSGkFAUKSTWM6AFAsGzgfbI+USoLrgy1AF9ko97dDHii71DAY1jqRJHowa73zTAuemEyZmEoFHi2msb0IpBWLkIvk3a1l+TXuv7SPvyfZK45Xf0TnCuqFOuoonHFTKmjA0IV50DlFAWlYM7L8T/UZhQ0IKw1Bfm2tzylj4mZgxHfjDHO9+3gTU1C9MGRFi1xI41/Zbb/9UmvvrFk1x5XcP3xeQCCAV9URWflzkv56EjCjI2QC1nQLFkC3j/XOGA82OriD3vdeUnPwbKswt84P3DtNUo5fkWnFvM4d3qyyjAdJPdW3q44vd2ULYGTs2BsUUbdwG4W6aTIn7AMyEChQFeLrhgLVHG0saQQxucE8TpvFGRGYIDWQZHcnUNcmKSst8GO9AX95TM5QxMzFOeXsh/ZgioAhhZCu/jLDIij9PgZS0Y8GxFT27Zs0BimcsucCZJJeByM6RYZ5VyEAlY5+Vc4Ssur3EFTGHzcmVFZzy4AJqlk1++HRAtgGA7zpvgkMIVDx7nLJaMbbXnAQ6gkzDhHbOZewpnFYKlyBHH7ILlqccT1o0Il25WRI3cAAsY8KYASKF/0ENQDdjCH5RAkJsdw/wUHDhsiGPh6l0BWgvOFkY561KZnMV6NkCWHIZcWkTEAcx25cS62Pm09Q66BKzgFAiCKju+9rU2n/l8m9EBxaYNiu3bFVfsVGzZLN6UWg2kLKBzEilAsKtkgyyzsgqQCLbpmJ2BsVOOI0cdB/7ecPio5eRpy2IC/+3fV9n7uhDbLoYGzjPgEoeLnWcDR47slsuA5MScPbKzq7HGYa1FOZWP9yIdDbChoRiMHBM/Sjh+EP76r6DSA319MLpWs3ZU0hpG1giDg1BvCOVK8aA4LIHWgIM4gaSbykC7Da0mzM45Jifg7LhLBafPWMbPWubnoNuCioZGGTY1NBNNRywOlEGUgC224BmDZ+lCxgaYlY7DFkgePcPRN2+3bWJbdkbhhwOCKEAsAJdsgZ7IURvqY+ueG2m12kycPMTcxBTz8zOMP2fY9xQUx2EPTKUCpciDE+ZGOCCOIUllDLRb3gT/dzwAiEBUhkojYmB9L72jIwyt3YbqLDD+zCP0Bwlr1+Y703y9w4CzeSYnFtOx7YwNiAG7kgHxn7zAyY9cxZFyx73GJXkKofK1FB/RJZvBhQ4zuI4rf+6DlIIAl7RJ4i7NmTMsTE3SWphhdvwIi1Oz/izfac/TmZ9J6wUfVDu2JE0DCGGkUZFQ0pr6aINyT18K3EO5ElEfXkN9aAuVnlpajxLVBglKJSSoMHHqGIf2P8rQSMLwEGAcqAwYHOBjj52fIBdaHMnYVjPAAZ3FmHh8Tp4YzAzwTXMBCGBgwzphYKPi6InjzE+cZP2m7agopFQqEW3aSpjWQRCgtUJEobTCJV1M3MSktQCCFCdrBeBwgA4rXojGp6+zmMSQJAndTodOp0XcjUEFjI8d5FS2Z7g2pKcOdhaQIk5vRKaWI2NajD1RZ+kQKCbCGGg9PGa/u2Ojen/Qdsp08RMfThAFDqjW4bVXBTx73yJ/99Cfcdudv8rQ8AhBoFEC4gwKRagDb0QYhqnqlEpr0UHo2+nMIAGAxFiM8ZAkcUw37qaQXTx0Cuuwvs9AK1Slig4SThw9yKMPfxWlYe81CiygQBxYBxiHyeDbjqTtbMYEtIA4Y13pFRkLND/1FH8/Neu+b5oW17XY2IEFBDxhF274Gc26Yc2hfQ/xh//hQ9z/+d/j/+x/gsXFRaJyhWpPnUq1RikqEwQhWmuKE6RFUlEs+DhrvESEQOvMNH9tT62W9lUjCCOmJs/zxN99my/8wT388X/+FeZPHeGyywLe8FoFHRDvPmDAJg7XsZiWJWPJmIAmYFd7LG6B1lxM69nT7oGbh+1rXVtBFbA+TiQQ6Dp2bBOu/SnNs49CqzPG/oe/zOPf/jK9QxvYsPlyLrtyNxsv2cHmbTtZM7KOWi0zpJeoFHJhCYLAC2Cx2UpNXGBhYYFTY0c5cewIxw+/wMED3+PMiR/Smp+iFsDaOqiy5pa3BdSq4NoAAhYweep3HG7ekrFkTOC1ggHFMGgBcx/7Lt9540Z7fKDXblYVQVVASgq0AxHoOG57V8CpH1h6gpCdQzAXW6aaJzn7wkkOPfNtDBBWqvTUB+kbGGZgzTr6B0YolatUUkWlEs5lBrbotFu0mvNMT5xh8vwZZqcnaC1MknRiIgWNClxSg4HBFFgL8aKjsll481sULDrwW25SCT5rWxYzb9N+7PGMBZgDWhfzg5EEmD8yy/zjY+5P3tpn/53uUdgKqAgIBTTQxm98bnxnyL6vxGzoUyit6VpFy8Bi4liMHc24RbM7Rmt8jB+N7edwAsbm4zSTeKEBrSEKoFKCDamqo0JPqFMpqgFUNIQidDqOMzG845dCqiHgBEHACi5/7moWHGbGkjFkLOCVLL8VXpoFC8DML/8t33lird2/tm52S9VnASpUoAUJgVnHLW/XnD1hmf6+Yf2wohQITsBYIXaOxCq6lrSG2Hl4/OrqKHbXgJZ8p6u8x4TKf6aU1SJo8dfQ7sLZSceb7gi5cpfAOQciuAw+gXzck8waxifs/owBmAEWXsoPRzvAzGyb3s8+6z7zGw3zGamoskRCGFiUVhCAWCHoWO74cMi9n4RzpyzrhhVR6IEQkeJUCljnLjil5pJifhUyCSLgRfFccbHjOD3t2PmWgJvfoWHCgCh8fwZs2/qhkcxa2lOmncWeMswBMzkTL25AkQVzwOTvP82RGzbK566vmA/rSLChQgILgaQCaUGj1/KeXyvx1T+IOXXMsnZYqOQ7Pa1ACtJVXvsQYOl53+QPRRe6KfyUY9tNAW9/b4CaMmBU/hgQXNtiU3ibwptJw5PH5HO//7Q7AkwCcxf/fkChLjAFVG/7uv3GU3XZvi1MbiYMQBRKCSoARJB5RzpZcmdqwl/fG3PiyYSRXqFeE0qSmyAXosqKh0DncngLnZhsGWMmdlz17hI3vFUhEwY6+aTXBdsCO++IZyzd8wlHxuxDt33dfQOYyBm6AKsasMpcMAVU7v5L90cP3G7XjGjzevIxKlohCE5AZqGnx/DOD4Y8faVm3zdjZs87BnuhWhHvm1KgAN9+OXgHNr/r3QQWFh1TKVh9s+a2O0K2bQXOWegKLgZfNx12zhJPpzpnsmH4bBYrMO3hlx37hfQnPvEJVirp/7l77rnHAEy00M+e5/s3jborKophAkEE8DAKEIhBWo71OxQ7rguII+H0acfMtKMbCw6wgPMSbCHi/G4vtv0dZ6oJekSx5+0lbv6FkOGK9fAuFjx4W7BNm8I7PPx4Bm8O/Mtvud/ZP84p4AwwlbJ3/59flBSROjACrH/TRtZ99hb5tXWb1BvC0QDdqwjqgvQoJAIJHRIAPcCAZqElHD5gOfqcYXrM0p13kBTDorjroCKI+hQjWxVbX6fZuk0oWQuTFjr5JjIRXAtc05LMO8ysJT6bcHrMfu9DD7r/8t2TnAROA+Mp2/xP7E1REekH1gCj23sZuv82+eDWTermcFij+1VugqDKApHkr8UDVaChIFI02zA97ZibhsUZR9x1AERVod4vNHqF/gEoBQ4WLcw6PLj14NB12LbDLTiSfJ2PzxsO/8h++1/8hfvvh6c4C17nU65pgJ+kAQL0A8N5NjT+/OfV26671L2/MqjLwaBGNwRdU0gZJBKkBBKACKDxxlABQoFAipOIARLnAWllNWBy8Kzu5tvaFphFi5lzJFOG1qRpP3ZEvnD71+y3gBlgPIMHZlxaXo53hRXQCwzlRgz86z1s+dAe+cW1o2q3HlAEfQqVZUJFpQIJMwloEA/tQFY4hjnAb2jyM33sPLxt47e2Nl/jzZTlzFm7/7PPuP/x6Wc4Ckzl4JPAbMpkX9a3xYE6MJirv6Kpf+atXHfDFvn5viG1RfcqdF2hqoJUBF0CSuLNQIOoYtcD5OAOH7bJwIGOw3T9WR7bdJgF68f7zIQ99sgx97UPP8hjLcM8MO3BvZh3aXmlfl+glmdDP9AHNNbXqP32Dey9dqPcMtgvu4KakswEXVXFkPDZgJdSAGBzcFye6nnKm6b18MmCdZNT7vnHT7q/+c1HePrUAguQ7/BgOr/rC6/4b4yISJRnQyM3oQ7UgOBX97LlZ7ervVv63d5anc2liopUJBAKEoAKfjwD/Pk9AfJHWN2W7SzMc/zYtDz9V4fs0+mO9BiQgIefz+Hn8rveAXjFDSjmBSoevlAV6AEiDerndrDm+o2s3zmoNg5V3bpqyEAUUAs1EUBs6HQSFpoxUxNNOf3DSTv2v8c49RcHOWfAAh1gEWh6+EKtYry/0gYsNSLIjejJVc2NKQOlXDqXLWoAFGCK2qubqw20cvjFTDl48ur7vcHCiAgo56p4+EIaUHlNIQxgC/hcHp52rs4S8FeVAUtXi/ACBb4uDChmgcKAGEh8TSFXBMqr24DV5woNKK/lz0M2l/Fj+xUo/xd+DYsy448VUQAAAABJRU5ErkJggg==) (R-78)

## Discussion

The empirical findings indicate that interactive blogs can provide a significant forum for soliciting and providing support, both informational and emotional. About 63% of the postings solicited support of various kind, whereas about 65% of the readers'comments provided support of some kind. The strategies for soliciting support drew most frequently on the disclosure of personal experiences about dieting efforts and reporting success achieved in such projects. Interestingly, support was rarely solicited by requesting for information (see Table 4 above). This suggests that the bloggers primarily conceive blogs as forums that serve the ends of sharing experiences and opinions. On their part, the readers mainly provided informational support such as facts and opinions. To a considerable extent, the readers also offered esteem support and emotional support, while the role of social network support remained marginal (see Table 5 above). In this regard, the findings specified the picture obtained from the previous study drawing on Bales's ([1950](#Bales1950)) Interaction Process Analysis ([Savolainen 2010](#Savolainen2010)). This investigation showed that the emotional reactions exhibited by the blog contributors are predominantly positive; the negative reactions are rare.

At first glance, the findings suggest that the needs and provision for support may not match optimally in the dietary blogs. The bloggers primarily solicited emotionally-oriented support by sharing their experiences, while the readers favoured the provision of cognitively-oriented support, especially informational support. As a whole, however, the support provided by the readers seems to meet fairly well the bloggers' needs for support. This is because the readers also provided a considerable amount of emotionally-oriented support. Of their comments indicating support of some kind, 44% offered esteem support and about 38% emotional support such as encouragement (see Table 5 above). As a whole, the empirical findings confirm the results obtained from the use studies of online support groups employing diverse technologies such as Usenet newsgroups and Internet mailing lists. For example, Loader and associates ([2002](#Loader2002)), Coulson ([2005](#Coulson2005)), and Meier and associates ([2007](#Meier2007)) drew similar conclusions about the popularity of informational support, as compared to the provision of esteem support and emotional support.

However, when comparing the findings, Eichhorn's ([2008](#Eichhorn2008)) investigation of online eating disorder support groups is most interesting since she used the same categories as the present study. Eichhorn found that 54% of the messages solicited and 56% provided some type of support. The bloggers discussed in the present study were somewhat more active in soliciting support (63% of the postings). The blog readers were also somewhat more eager to provide support (65% of the comments). The latter difference (56% vs. 65%) may be mainly due to the fact that in contrast to Yahoo! discussion boards, blogs represent person-centred forums in which the relationships between the contributors tend to become more intimate. The blogger has the control over the topic and the comments are generally made in relation to what the blogger writes ([Brake 2009](#Brake2009): 171). Bloggers perceive their blogs as though it is their home and readers are invited to come over ([Boyd 2006](#Boyd2006)). Blog readers are regarded as guests or visitors and, as such, they are expected to respect the sovereignty of their host ([Boyd 2006](#Boyd2006): 634). Therefore, readers are expected to behave in a friendly and supportive manner in the blog's discussion area.

Eichhorn ([2008](#Eichhorn2008)) also found that self-disclosure of experiences was the most frequent strategy to solicit support (52% of the messages). The bloggers resembled those contributing to online support groups since 49% of the postings could be classified into this category (see Table 4 above). However, there appeared to be differences, too. Those suffering from eating disorders were more active than bloggers in requesting information (25% vs. 8% of the messages) and presenting self-deprecating comments (15% vs. 8%). On the other hand, the bloggers favoured more strongly the reporting of personal success (40%) than those suffering from eating disorders (6%). The bloggers also reported more frequently about extreme behaviour (10%) than the eating disorder group (1%) (see Table 5 above). These differences are probably due to specific nature of problems faced by the above groups rather than the technological capabilities of the online forums of various types. The dietary bloggers may have more frequent opportunities to report about personal success such as the gradual loss of weight, as compared to those chronically suffering from problems caused by anorexia, for example.

The findings of the present study also confirmed Eichhorn's ([2008](#Eichhorn2008)) results in that informational support was provided more frequently than other types of support. About 30% of the messages posted by those suffering from eating disorders and 73% of the blog postings were classified into the category of informational support. Interestingly, however, the bloggers were more active to provide esteem support (44%) than the eating disorder group (10%). The former also were more eager to offer emotional support (38%) than the latter (21%), while the bloggers were somewhat less interested to offer social network support (10%) than those suffering from eating disorders (21%) (see Table 5 above). Again, the differences may originate from the fact that the dietary issues discussed by the bloggers tend to be coloured more positively than those related to eating disorders, as in Eichhorn's study. Thus, independent of the type of an online forum, it may be easier to provide those who discuss dietary issues with compliments and encouragement, as compared to those suffering from anorexia, for example.

The present study has a few limitations. First, the findings are based on the study of a narrow slice of the blogosphere (eight Finnish-language blogs devoted to dieting). Thus, the findings cannot be expanded and presented as if true of all blogs in general. Second, the study focuses on the textual material available on the blogs. Thus, no attempt was be made to review the role of audio-visual material such as photos and videos attached to the postings. Third, the study did not investigate how the readers solicited support from the bloggers or fellow readers. Fourth, no attention was devoted to how the bloggers evaluated the usefulness of the support provided by the blog readers. Finally, no attempt was made to explore the connections between the strategies for soliciting and providing support. This is because questions such as how the strategy of presenting requests for information gives rise to the provision of informational support would have required a separate study.

## Conclusion

Similar to traditional online support groups, blogs represent _empathic communities_ that also enable the seeking and provision of informational support. However, blogs do not primarily offer factual information since the main emphasis is laid on the sharing of experiences and opinions. This finding has practical implications for those considering blogs as potential forums of health-related information seeking in particular. Online forums of other types, for example, Question and Answer services may be more useful in seeking for health-related facts, while blogs may be more helpful as providers of emotional support.

Since the present study concentrated on blogs discussing a specific topic, there is a need to conduct comparative studies on blog use in other subject areas. Such studies would shed additional light on the specific ways in which blogs are used in information seeking and sharing. Further, it would be intriguing to explore in more detail how the nature of information seeking and sharing is dependent on the affordances of online forums of various types, for example, blogs, _Facebook_ and Internet discussion groups.

## About the author

Reijo Savolainen (PhD, 1989, University of Tampere, Finland) is currently Professor at the Department of Information Studies and Interactive Media, University of Tampere, Finland. He can be contacted at [reijo.savolainen@uta.fi](mailto:reijo.savolainen@uta.fi)

## References

*   Aharony, N. (2009).Librarians and information scientists in the blogosphere: An exploratory analysis. _Library & Information Science Research_, **31**(3), 174-181.
*   Bales, R.F. (1950). A set of categories for the analysis of small group interaction. _American Sociological Review_, **15**(2), 257-263.
*   Bar-Ilan, J., Shalom, N., Shoham, Baruchson-Arbib, S & Getz, I. (2006). [The role of information in a lifetime process: a model of weight maintenance by women over long time periods](http://www.webcitation.org/5tOPkgVJI). _Information Research_, **11**(4) (2006). Retrieved 3 May 2010, from http://InformationR.net/ir/11-4/paper263.html (Archived by WebCite® at http://www.webcitation.org/5tOPkgVJI)
*   Boyd, D. (2006). [A blogger’s blog: exploring the definition of a medium](http://www.webcitation.org/5tOQ3ILp4)._Reconstruction: Studies in Contemporary Culture_, **6**(4). Retrieved 3 May 2010, from http://reconstruction.eserver.org/064/boyd.shtml (Archived by WebCite® at http://www.webcitation.org/5tOQ3ILp4)
*   Braithwaite, D.O., Waldron, V.R. & Finn, J. (1999). Communication of social support in computer-mediated groups with people with disabilities._Health Communication_, **11**(2), 123-151.
*   Brake, D.R. (2009). [‘_As if nobody’s reading’? The imagined audience and socio-technical biases in personal blogging practice in the UK_](http://www.webcitation.org/5tOQEM4L1). Unpublished doctoral dissertation, London School of Economics and Political Science, University of London, UK. Retrieved from http://bit.ly/8MBZBT (Archived by WebCite® at http://www.webcitation.org/5tOQEM4L1)
*   Burnett, G. (2000). [Information exchange in virtual communities: a typology.](http://www.webcitation.org/5tOQOQNKJ) _Information Research_, **5**(4), paper 82\. Retrieved 3 May, 2010, from http://informationr.net/ir/5-4/paper82.html (Archived by WebCite® at http://www.webcitation.org/5tOQOQNKJ)
*   Chang, H-J. (2009). Online supportive interactions: using a network approach to examine communication patterns within a psychosis social support group in Taiwan. _Journal of the American Society for Information Science and Technology_, **60**(7), 1504-1518.
*   Chung, D.S. & Kim, S. (2008). Blogging activity among cancer patients and their companions: uses, gratifications, and predictors of outcomes. _Journal of the American Society for Information Science and Technology_, **59**(2), 297-306.
*   Coulson, N.S. (2005). Receiving social support online: an analysis of a computer-mediated support group for individuals living with irritable bowel syndrome. _CyberPsychology & Behavior_, **8**(6), 580-584.
*   Cutrona, C.E. & Russell, D.W. (1990). Types of social support and specific stress: toward a theory of optimal matching. In B.R. Sarason, I.G. Sarason & G.R. Pierce (Eds.), _Social support: an interactional view_ (pp. 319-366). New York, NY: Wiley.
*   Cutrona, C.E. & Suhr, J.A. (1992). Controllability of stressful events and satisfaction with spouse support behaviours. _Communication Research_, **19**(2), 154-174.
*   Eichhorn, K.C. (2008). Soliciting and providing social support over the Internet: an investigation of online eating disorder support groups. _Journal of the Computer-Mediated Communication_, **14**(1), 67-78.
*   Fraze, T. & Wong, N. (2008). [_Seeking and scanning for lifestyle information from media sources: healthy-weight, overweight, and obese older Americans_](http://www.webcitation.org/5tOROEWqS). Paper presented at the Annual Meeting of the International Communication Association, Montreal, Quebec, Canada, May 21, 2008\. Retrieved 3 May, 2010, from http://www.allacademic.com/meta/p232936_index.html (Archived by WebCite® at http://www.webcitation.org/5tOROEWqS)
*   Frisby, G., Bessell, T.L, Borland, R. & Anderson, J.N. (2002). [Smoking cessation and the Internet: a qualitative method examining online consumer behaviour](http://www.webcitation.org/5tORUimAx). _Journal of Medical Internet Research_, **4**(2). Retrieved 3 May 2010, from http://www.jmir.org/2002/2/e8/ (Archived by WebCite® at http://www.webcitation.org/5tORUimAx)
*   Gobo, G. (2006). Sampling: representativeness and generalizability. In C. Seale, G. Gobo, J.F. Gubrium & D. Silverman (Eds), _Qualitative research practice_ (pp. 405-426). London: Sage Publications.
*   Hayes, A.F. (2005). _Statistical methods for communication science_. Mahwah, NJ: Erlbaum.
*   Herring, S.C., Scheidt, L.A.,Wright, E. & Bonus, S. (2005). Weblogs as a bridging genre. _Information, Technology & People_, **18**(2), 142-171.
*   Hodkinson, P. (2007). Interactive online journals and individualisation. _New Media & Society_, **9**(4), 625-650.
*   Karimi, F. & Poo, D.C.C (2009). [Personal and external determinants of medical bloggers' knowledge sharing behaviour](http://www.webcitation.org/5tORuT6bP). In _ASIS&T Annual Meeting, November 6-11, 2009, Vancouver, British Columbia, Canada_. Retrieved 3 May 2010, from http://www.asis.org/Conferences/AM09/open-proceedings/ (Archived by WebCite® at http://www.webcitation.org/5tORuT6bP)
*   Kovic, I., Lulic. I. & Brumini, G. (2008). [Examining the medical blogosphere: an online survey of medical bloggers](http://www.webcitation.org/5tOS2SevQ). _Journal of Medical Internet Research_, **10**(3). Retrieved 3 May 2010, from http://www.jmir.org/2008/3/e28/HTML (Archived by WebCite® at http://www.webcitation.org/5tOS2SevQ)
*   Lenhart, A. & Fox, S. (2006). [_Bloggers: a portrait of the Internet's new storytellers_.](http://www.webcitation.org/5tOS7F0Wb) Washington, D.C.: Pew Internet & American Life Project. Retrieved 3 May 2010, from http://www.asiaing.com/bloggers-a-portrait-of-the-internets-new-storytellers.html (Archived by WebCite® at http://www.webcitation.org/5tOS7F0Wb)
*   Loader, B.D., Muncer, S., Burrows, R., Pleace, N. & Nettleton, S. (2002). Medicine on the line? Computer-mediated social support and advice for people with diabetes. _International Journal of Social Welfare_, **11**(1), 53-65.
*   Meier, A., Lyons, E.J., Frydman, G., Forlenza, M.J. & Rimer, B.K. (2007). [How cancer survivors provide support on cancer-related Internet mailing lists](http://www.webcitation.org/5tOSEN1qA). _Journal of Medical Internet Research_, **9**(2). Retrieved 3 May 2010, from http://www.jmir.org/2007/2/e12/HTML (Archived by WebCite® at http://www.webcitation.org/5tOSEN1qA)
*   Nardi, B.A., Schiano, D.J. & Gumbrect, M. (2004). Blogging as social activity, or would you let 900 million people read your diary? In _Proceedings of the Conference on Computer-Supported Cooperative Work_ (pp. 222-231). New York, NY: ACM Press.
*   North, C.L. (1997). _Computer-mediated communication and social support among eating disordered individuals: an analysis of the alt.support.eating-disordered newsgroup_. Unpublished doctoral dissertation, University of Oklahoma, Oklahoma, USA.
*   Preece, J. (1999). Empathic communitites: balancing emotional and factual communication. _Interacting with Computers_, **12**(1), 63-78.
*   Rettberg, J.W. (2009). _Blogging._ Cambridge: Polity Press.
*   Savolainen, R. (in press). Asking and sharing information in the blogosphere: the case of slimming blogs. _Library & Information Science Research_
*   Schmidt, J. (2007). [Blogging practices: an analytical framework](http://www.webcitation.org/5uwpOQxDQ). _Journal of Computer-Mediated Communication_, **12**(4), 1409-1427\. Retrieved 12 December, 2010 from http://jcmc.indiana.edu/vol12/issue4/schmidt.html (Archived by WebCite® at http://www.webcitation.org/5uwpOQxDQ)
