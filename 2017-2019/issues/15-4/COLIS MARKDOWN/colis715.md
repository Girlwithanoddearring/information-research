#### vol. 15 no. 4, December, 2010

## Proceedings of the Seventh International Conference on Conceptions  
of Library and Information Science—"Unity in diversity" — Part 2

# Time as a framework for information science: insights from the hobby of gourmet cooking

#### [Jenna Hartel](#author)  
Assistant Professor, Faculty of Information, University of Toronto, 140 St. George Street, Toronto, Ontario M5S 3G6

#### Abstract

> **Introduction.** A framework for information science based on time is proposed, derived from an exploratory, ethnographic study of information in the hobby of gourmet cooking.  
> **Method.** Participant obseration of the social world of the hobby, plus twenty semi-structured interviews with hobby cooks, followed by a photographic inventory of their household culinary information collections.  
> **Analysis.** Data were analysed using grounded theory facilitated by NVivo software. While coding information activities and resources, time emerged as a critical aspect of the hobby and its information phenomena.  
> **Results.** The hobby unfolds as three _temporal arcs_. The _career arc_ that lasts for years or decades and represents a lifetime experience; the _subject arc_, made up of shorter periods in which a topic organizes culinary activity; and the _episode arc_, which entails distinct cooking projects. Each arc serves as a context for certain information activities and harbours quintessential information resources.  
> **Conclusions.** The _temporal arcs_ are a springboard to entertain a time-sensitive framework of the discipline. Several research specialties in information science appear to have an affinity for one arc, where their concerns and problems come into sharpest view. Overall, information science research may seem unsynchronized because of differing temporal perspectives.

## Introduction

The theme of the CoLIS 7 conference is an invitation to explore the relationships between specialties and disciplines that study information. To this end, it is necessary to know the fundamental forces that shape information, binding together or separating different research traditions. My paper offers evidence that _time_ is a force that impacts information phenomena on individual and disciplinary levels.

## Literature review

To begin, a _framework_ is '_a skeletal structure designed to support or enclose something_' ([dictionary.com](#fra10)). Frameworks are the conceptual devices often employed to demark the boundaries of research specialties and disciplines. There have been many efforts to create frameworks that show the structure or unity of information science as a field; altogether there are too many to review here ([Bates 2005](#bat05), is a good start). It is illuminating, however, to consider two that have had a significant impact, from scholars who have pondered these matters for decades: Marcia Bates and Howard White.

At the CoLIS 6 conference three years ago, Bates presented a unifying framework for the information disciplines ([2007](#bat07)). It was a culmination of a lifetime of creative thinking, and a direct extension of ideas in her award-winning paper, _The invisible substrate of information science_ ([1999](#bat99)). The conception coalesced during her editorial work on the _Encyclopedia of Library and Information Science, 3rd ed._ ([2010](#bat10)), assisted by historian Mary Niles Maack and an advisory board of fifty scholars.

<div align="center">![Figure 1: The spectrum of the information disciplines](colis715fig1.jpg)</div>

<div align="center">  
**Figure 1: The spectrum of the information disciplines ([Bates 2007](#bat07)).**</div>

Bates's view is that within academe, the information sciences are a meta-discipline composed of eleven specialties that run above and across the traditional _content_ disciplines of the arts and humanities, social and behavioural sciences, and natural sciences (Figure 1). Our work facilitates knowledge production in these realms; in short, we aim to, '_collect, organize, store, preserve, retrieve, transfer, display and make available the cultural record in all its manifestations_' ([Bates 2010](#bat10): xii). Compared to the traditional disciplines, we are relative newcomers still in a formative stage. Since the information sciences are inextricably linked to the work of the entire academic spectrum, we reflect a range of ideographic and nomothetic approaches that account for our metatheoretical and methodological diversity and interdisciplinarity. While Bates's conception is centred on academe, it is a structure that she extends to include trades, avocations, and hobbies, as well.

White is another scholar with a passion to define and unify the discipline. In _External memory_ ([1995](#whi95)) he locates the information sciences at the intersection of people and the universe of recorded information. His _Visualizing a discipline: an author co-citation analysis of information science_ ([1998](#whi98)), with Katherine McCain, employs author co-citation analysis to further illuminate the conceptual and social dynamics of the field.

This well regarded study identifies twelve research specialties, namely: experimental retrieval, citation analysis, online retrieval, bibliometrics, general library systems, science communication, user theory, online public access catalogues, indexing theory, citation theory and communication theory ([1998](#whi98): 335). The dozen specialties coalesce around two themes: _domain analysis_, which includes all research into the features of literatures and information _retrieval_, which focuses on information access. Importantly, there is little integrative scholarship between the two realms. Indeed, information science scholarship appears centripetally flung apart and populated like Australia, with a vast unsynthesized interior. Later, in an impassioned call to action, White posted a mock-advertisement in the _Journal of the American Society for Information Science and Technology_, entitled _Scientist-poets wanted_ ([1999](#whi99)), which petitioned scholars to creatively synthesize the central ideas of information science.

There are some similarities between these two frameworks, such as the identification of roughly a dozen research specialties and their orientation to the documentary record. The parallels are unsurprising, since Bates and White graduated from the information science programme at the University of California (Berkeley), during the 1970s, and both were students of luminaries William Paisley and Patrick Wilson. Personally, as a doctoral student, these frameworks generated _eureka!_ moments that crystallized my understanding of information science.

While Bates's and White's frameworks are calibrated to disciplinary dynamics, they are not well synchronized with the information experience of any workaday information user. Could a lay-person make sense of either framework? How does one locate mundane information phenomena within these rubrics, such as collecting, reading, or searching for a recipe to make dinner?

## Methodology

The framework presented here sprang from exploratory ethnographic research into information in the hobby of gourmet cooking in America ([Hartel 2007](#har07)). Upon first impression, this popular, decidedly decadent pursuit may not seem a hotbed for conceptualizing information science. However, hobbies such as gourmet cooking are instances of _serious leisure_ ([Stebbins 1982](#ste82), [2001](#ste01)), sustained, cherished, free-time activities centred upon the acquisition of special skills and knowledge.

On a social level, serious leisure and hobby realms are geographically diffuse _social worlds_ ([Unruh 1979](#unr79), [1980](#unr80)) that share knowledge through mediated communication. In the case of gourmet cooking, this mainly takes the form of recipes, menus, cookbooks and Bernardassociated information in every possible medium. Resembling an academic research specialty, serious leisure social worlds, such as the hobby of gourmet cooking, feature information patterns that are amenable to study through a socio-cognitive or domain analytic meta-theoretical approach ([Hjørland and Albrechtsen 1995](#hjo95); [Hartel 2003](#har03), [2005](#har05)), following in the tradition of studies of information behaviour among groups like academics ([Talja 2005](#tal05)) or professionals.

My study was exploratory and it asked: What is the hobby of gourmet cooking? What do hobby cooks _do_? And most importantly: what is happening with information? To answer these questions I was a participant observer of the social world and I conducted twenty semi-structured interviews ([Bernard 2002](#ber02)) with gourmet hobby cooks from Los Angeles, California and Boston, Massachusetts. Participants were recruited through purposive sampling ([Berg 2000](#ber00)) at public culinary events and through family and friends. The interviews occurred in the cook's home and covered their participation in the hobby and the role of information. After the interview, I performed a photographic inventory ([Collier and Collier 1986](#col86)) to capture their culinary information collections. The fieldwork explored _all_ kinds of culinary information in the home; however, primary attention was placed on printed resources. My fieldwork generated a rich ethnographic record ([Spradley 1979](#spr79)) consisting of roughly several hundred pages of interview transcripts and descriptive fieldnotes, which included almost 500 photographs and diagrams.

I analysed the data using grounded theory ([Strauss and Corbin 1998](#str98)), facilitated by NVivo software. While reading and reflecting upon the field data, a key step was to name or code recurring concepts of interest. My codes for _information activities_ (Appendix 1) included the anticipated '_searching for recipes_' as well as surprises such as '_eating out_'. In an effort to distill the findings, it was tempting to group similar activities together, following a long tradition of information behaviour models ([Wilson 1999](#wil99)). Yet, groupings of this kind abstracted the activities and removed them from the context of the hobby and their meaningful place within the cook's narrative.

At this critical analytic juncture, I noticed that information activities have a temporal dimension and unfold at different rates _in time_. For instance, cooks mentioned '_making a shopping list_', an information activity that was relatively quick and a key step in a hands-on cooking project. They also reported '_subscribing_' for _years_ to culinary magazines. The activity of acquiring culinary information collection sprawled across decades. In this way, time emerged as an important variable in understanding and locating each information activity in the context of the hobby.

Another analytical goal of the project was to identify the information resources used by gourmet hobby cooks. By studying the transcripts and photographs, I coded dozens of resources (Appendix 2) ranging from '_cookbooks_' to '_calendars_'. With a librarian's sensibility, it was tempting to group items based on format, genre, or channel. Again, doing so would detach the resource from its natural context. Here, too, I noticed how time shaped the nature of the materials and their use. For instance, the all-important '_recipe_' was commonly used fleetingly during a hands-on cooking project, yet had a different role as an element of a cherished recipe collection passed down through generations.

It was my good fortune that in 2006 the theorist Savolainen was also thinking about _time_. In _Time as a context for information seeking_ ([2006](#sav06)) he reviewed more than information seeking and use studies which he scrutinized for their treatment of time. He reported that, although time is always a major variable, there is relatively little conceptual development. He goes on to describe three main approaches information behaviour researchers have taken to time. My own emerging thinking on this matter fit with his first approach: time was a fundamental attribute of gourmet cooking as a context and this effected information phenomena.

## Findings

To capture these insights, I invented a framework that illustrates time as a context for information in gourmet cooking (Figure 3). It consists of three _temporal arcs_. The arcs represent a linked series of activities that unfold through time. This conception of the hobby resembles Sonnenwald and Iivonen's ([1999](#son99)) integrated framework for information behaviour, which includes eons (a long, continuous period of time), _intervals_ (a shorter period of time with a distinct starting and ending) and _episodes_ (a short period of time). In the framework, illustrated below, time unfolds from left to right.

<div align="center">![Figure 2: Three temporal arcs](colis715fig3.jpg)</div>

<div align="center">  
**Figure 2: Three temporal arcs (career, subject, episode) in the hobby of gourmet cooking. The stages of the career arc are common across hobbyists; whereas subjects and episodes vary depending on the cook.**</div>

There is the long-running _career arc_ that lasts for years or decades and represents the cook's lifetime experience of the hobby. The _subject arc_ is made up of shorter periods (weeks or months) in which a topic organizes culinary activity. Then, the _episode arc_ entails distinct hands-on cooking projects that happen closer to real-time.

Unpacking the hobby temporally allows for a clearer understanding of information phenomena. This paper focuses on two related propositions: 1.) Each arc serves as a context for certain information activities and 2.) Each arc harbors quintessential information resources. Next, I will introduce each arc and illustrate the propositions. Later, the arcs become a springboard to entertain a time-sensitive framework of the discipline that also reflects the human information experience.

### Career arc

The career arc represents the path of experience taken through the hobby and it lasts years, decades, or a lifetime. This insight is not original to my study; prior research on serious leisure confirms that avid participants experience something akin to a work career, with turning points, highs and lows, and increasing competency. Stebbins has named five typical stages to the serious leisure or hobby career: beginning, development, establishment, maintenance, and decline ([2001](#ste01), p. 10).

<div align="center">![Figure 3: The hobby career arc](colis715fig4.jpg)</div>

<div align="center">  
**Figure 3: The hobby career arc (adapted from [Stebbins, 2001](#ste01): 9-10).**</div>

Gourmet cooks told me their history in the hobby and it was easy to detect the career and its stages. To start, in _beginning_, cooking captures the imagination and they take an active interest in the hobby. In _development_, they begin to cook and acquire fundamental skills. As one cook says, '_I practiced how to dice an onion over and over again_'. In _establishment_, cooking becomes a routine and is instituted in their life, among other things they have learned the lingo, stocked the pantry, perhaps renovated the kitchen, and attracted a circle of like-minded _foodie_ friends. The _maintenance_ stage is the heyday of the hobby. They are now known as great cooks, and as one reports, '_All my friends call me when they have cooking problems_'. At this point, some become teachers and share their culinary passion and know-how informally with family and friends. In _decline_, hobby activity stops, often because of a lifestyle change or the limitations of older age.

Distinct information activities occur at this arc. The hobby career is essentially a path of _learning_. Each of the five stages represents a different state of culinary knowledge and skill. Hobbyists begin with minimal abilities and advance to experts. This arc is also the context for _collecting_ and _managing_ culinary information, activities that unfold relatively slowly and incrementally. My study shows that over the years, most hobbyists generate _personal culinary libraries_, constellations of cooking-related information resources and structures in the home, and an associated set of upkeep tasks ([Hartel 2007](#har07): Ch. 8; [Hartel in press](#har0a)) . Personal culinary libraries contain recipes, cookbooks, gastronomy, precious culinary keepsakes passed down from relatives and access to a wealth of online resources through a home computer. Understanding the nature of both the learning path and the personal culinary libraries requires an orientation to the career arc, what historians call the _longue durée_. The quintessential information resource that captures the essence of this arc is the _culinary memoir_, a biographical work that tells the life story of a cook.

### Subject arc

The subject arc displays how topical interests organize the hobby for weeks or months. Gourmet cooking is not pursued haphazardly. Instead, cooks select a limited area of the culinary universe and study it for a while. A subject most often takes the form of a cuisine, such as Thai or French; or, it can be a cooking style, like barbecue or baking (the subjects illustrated in Figure 5 are only examples).

<div align="center">![Figure 4: The subject arc.](colis715fig5.jpg)</div>

<div align="center">  
**Figure 4: The subject arc. One at a time, gourmet hobby cooks focus on topics that organize the hobby activity.**</div>

Cooks reveal their experience with subjects in statements like, "That summer, I was _really_ into French." Such a phase would be marked by reading French cookbooks, purchasing special French equipment, taking a class on French cooking, perhaps even taking a trip to Paris. Eventually, the cook masters that subject, and moves onto another one. Subjects are pictured in Figure 5 as loops of different sizes to reflect how interest varies in duration and intensity, with some overlap, as cooks transition from one to the next. The ongoing engagement with new subjects keeps the hobby interesting and fresh.

What information activities dominate the subject arc? Above all, at this scope, hobbyists _read_ cookbooks. Several cooks in this study had dedicated weekly times for reading and favorite locations, such as a cozy chair in their den or neighborhood caf�. Instead of implementing recipes in the text, cooks study their composition and imagine the results, a mode jokingly called food porn ([O'Neill 2003](#one03)). They focus on narrative sections about ingredients, techniques, and culinary traditions. The act of _reading_ cookbooks is an intriguing anomaly, because, structurally, cookbooks are reference books ([Bates 1986](#bat86)), meaning they are designed for '_look up_' and '_use_' (to be discussed in the episode arc), **not** reading.

Further, when engaging a subject, cooks _classify_ culinary information. In information science we associate classification with bibliographic control, but, on a more basic level, Kwasnik ([1999](#kwa99)) defines classification as, "the meaningful clustering of experience." When making sense of subjects, gourmet cooks reveal a faceted conception. In her own words, a cook says, "I see every cuisine from a number of angles, such as its traditional recipes, ingredients, techniques, seasonal uses. [.] Each cuisine has a spectrum." Knowledgeable hobbyists use their understanding of subject structure to master new subjects more efficiently.

The quintessential information resource at this scope is the subject-themed cookbook, as in the landmark from Julia Child, _Mastering the art of French cooking_. It would function as a textbook for studying French cookery as a subject. When immersed in a subject, cooks do not seem content with just one text, but gather and read several that are later added to the personal culinary library. Subject-themed cookbooks contrast with more universal works such as _The Joy of Cooking_ which encompasses innumerable cuisines and techniques.

In addition to subject-themed cookbooks, each culinary subject manifests an information domain with a constellation of books, serials, Web sites, experts, and other multimedia resources. The characteristics of these domains (meaning, for example, their size, genres and terminology) vary from subject to subject and can differ dramatically. For instance the domain of French cookery is marked by voluminous writings and precise recipes. Differently, Iranian cookery features far fewer recipes that offer inexact instructions and assume significant tacit knowledge. These diverse information environments have not been mapped by information scientists, though sociologist Newman ([2001](#new01)) has nicely characterized the literature of Chinese cookery. Hobbyists become familiar with the features of these differing domains and in time they can navigate and search them capably.

For the gourmet cook, subjects are topical mid-range phenomena that bring focus, structure, and dynamism to the hobby. This departs from the traditional sense of subjects as, '_the thought contents of a document_' ([Satija 2000](#sat00): 223) to a more dynamic conception as organized activity in the context of a specific information domain. The subject arc is a very conceptual space in the hobby and it is a backdrop for the hands-on cooking discussed next.

### Episode arc

An episode is a hands-on cooking project that leads to an edible outcome. Episodes can be simple and quick, like sautéed chicken breasts, which require thirty minutes to prepare. Or, they can be more complex and laborious, such as a Thai feast which takes several days to implement. Either way, episodes are conceived by the cook as a complete, singular project. Some episodes are brand new; others are '_repeaters_' drawn from a repertoire that is enjoyed again and again.

<div align="center">![Figure 5: Individual episodes compose the episode arc](colis715fig6.jpg)</div>

<div align="center">  
**Figure 5: Individual episodes compose the episode arc.**  
</div>

Episodes are shown in Figure 5 as circles, with some examples named. Many thousands of cooking episodes fill a hobby career (Figure 6 is not to scale). Often episodes are related to a culinary subject being pursued at the time, as when a period of interest in Greek cookery inspires an afternoon devoted to making baklava. Other times, episodes may be motivated by the events and circumstances of life, such as holidays, seasons, or a bountiful crop of tomatoes from a garden.

Each episode entails a cycle of nine distinct steps, since there is a logical and necessary flow of events in cooking. The process is too complex to describe in detail here, but is presented in Hartel ([2005](#har05)). Briefly, an episode begins with _exploring_ for something to cook which generates a concept or vision, such as, '_I'll make a pecan pie!_'. With a culinary concept in mind, _planning_ happens next, which involves creating a menu, searching for recipes, and making a shopping list and a workplan, among other things. Subsequently, in _provisioning_, necessary ingredients and equipment are gathered. Then the hobbyist rolls up his/her sleeves for hands-on _prepping_, _assembling_, and _cooking_. This is the '_core activity_' ([Stebbins 2001](#ste01)) or heart of the hobby. Upon completion, the food is _served_, _eaten_, and _evaluated_. At the end of the episode, the cook is poised to begin again.

Once the steps within the episode are illuminated, we can more precisely identify, locate, and analyse information activity. For instance, _searching_ for recipes happens during the planning stage and resembles a _berrypicking_ search ([Bates 1989](#bat89)) beginning at home in the personal culinary library. Prepping-assembling-cooking feature a particular type of information use in which recipes are repeatedly consulted to determine next steps. During _evaluating_, the hobbyist often asks others for input on the outcome, and then mark notes on the recipe. The important point is, a distinct constellation of relatively _quick_ information activities function at the level of the episode. The quintessential information resource at this arc is the recipe, which steers much of the activity.

### Summary of findings

This research project discovered that unpacking the hobby of gourmet cooking _temporally_ allows for a clearer understanding of information phenomena. To review, the hobby career lasts a lifetime; it is the broadest measure of learning and each of its five stages has a different knowledge state as cooks advance from novices to experts. This is the context for the ongoing, incremental effort of collecting and managing culinary information in a personal culinary library. The reality at this arc is captured in a culinary memoir. At the subject arc, temporary topical interests organize activities that can last weeks or months. Instead of proceeding scattershot, cooks meaningfully cluster (or classify) their experience. The primary way to study culinary topics is to read subject-themed cookery books. Each culinary subject is an information domain with distinct patterns and characteristics. Finally, the episode arc harbours hands-on cooking projects that entail nine sequential tasks. Many different, relatively quick, information activities occur, such as seeking, using, and evaluating culinary information. The quintessential information resource of the episode is the recipe-the instructions for cooking.

This framework, based upon time, helped in answering my original research questions: What is the hobby of gourmet cooking? What do hobby cooks _do_? And, what is happening with information? In the tradition of ethnography, the findings are context-bound to the gourmet cooking social world, but broader implications are hard to resist. Next, the arc framework is used as a springboard to propose a temporal framework for information science.

## Discussion: time as a framework for information science

The three temporal arcs spark the notion that time influences the research areas of information science. To acknowledge, this leap takes empirical data drawn from individuals and projects it to the level of a discipline. It is admitted that several, though not all, specialties in information science appear to have an affinity for one arc, and, as such, this framework is not offered as comprehensive. Still, for certain research traditions, an arc is where, or, more precisely, _when_, their research concerns or problems come into sharpest view.

<div align="center">![Figure 6: Several specialties of information science have an affinity for one arc](colis715fig7.jpg)</div>

<div align="center">  
**Figure 6: Several specialties of information science have an affinity for one arc.**</div>

The career arc spans decades and, at the individual level, it involves lifelong learning and information collecting. On a disciplinary plane, this is the concern of information institutions such as libraries, archives and museums charged with the provision of materials _for perpetuity_. To illustrate, the mission statement of the Library of Congress is to, '_sustain and preserve a universal collection of knowledge and creativity for future generations_'. This arc also harbors scholarship within White and McCain's '_domain analysis_', namely bibliometrics, which considers the evolution of literatures over extended periods of time. Indeed, White and McCain's study examined a twenty-four-year span of co-citation patterns. Here, too, are our field's historians who naturally take a long view.

For gourmet cooks, the subject arc involves reading for weeks or months in order to master a topic, in an effort to understand its facets and relationship to other culinary traditions and practices. Within information science, this is the site of classification, research similarly concerned with subjects and their dimensions. At this arc, the distinct information domains that coalesce around subjects become apparent, which mirrors the traditional heart of the field: bibliography. Perhaps without realizing it, scholars in these traditions help people succeed with medium-term intellectual pursuits.

The episode arc is the vital realm of real-time information experience. This is the home of specialties such as information retrieval. Most forms of information retrieval occur in instances lasting seconds or minutes. A major concept in this area, relevance, is a situational assessment that is difficult to imagine stretched over any longer time span. Research into information behaviour also resides at this arc. Here, the predominance of studies focus on a singular bounded transaction, whether a student's research project ([Kuhlthau 1993](#kuh93)) or a work task ([Byström and Hansen 2002](#bys02)). The breakthrough ideas of _information grounds_ ([Pettigrew 1999](#pet99)) and _information encountering_ ([Erdelez 1997](#erd97)), are at heart temporal, temporary, usually fleeting information events.

There are important implications of this temporal framework of information science. If research specialties orient their attention to information phenomena at a single arc, there is a risk of adopting an incomplete view of the user experience. When engaging information, people are impacted by contextual forces from every arc. A gourmet cook's quick recipe search is shaped by the informational characteristics of the particular episode; both the subject and career stages are at play. It is a challenge to study and understand information phenomena on so many cascading levels.

## Conclusion

Music, a most temporally-aware discipline, provides a closing reflection for this paper. Information and music are cousins, of sorts, because both unfold in time when experienced by a user. Sometimes a gathering of information scientists, like any CoLIS conference, contains unsynchronized voices and perspectives. Perhaps this is because our specialties engage information in different time frames, or tempos. Some focus on the incremental accumulation of materials at information institutions, or the gradual shifts in citation patterns within literatures; efforts akin to a waltz. Others orient to a faster tune that coalesces, dissolves, and reorganizes anew after a short while; like jazz improvisation. A vocal majority explores real-time information problems and solutions, which pulse like contemporary trance music. Despite being in the same key (information), when played altogether the three tunes generate a cacophony. While it may be impossible to synchronize information science research into a singular harmony, we benefit by knowing the reason for the noise.

## Acknowledgements

Greg Leazer, Sanna Talja, Robert Stebbins, Reijo Savolainen, and Jarkko Kari provided essential support and guidance throughout my culinary research. Thanks also to Joseph Hartel for comments that improved this manuscript, and to Ashleigh Burnet for excellent copyediting.

## <a id="author" name="author"></a>About the author

Jenna Hartel is an Assistant Professor at the Faculty of Information, University of Toronto. Her research is organized around the question: What is the nature of information in the pleasures of life? She is investigating this matter through the concatenated study of information phenomena in serious leisure -- cherished, information-rich pursuits such as hobbies. Her empirical research explores the content, structure, and use of leisure information on personal and social levels, and her theoretical work aims to characterize the nature of information in leisure realms. She can be contacted at [jenna.hartel@utoronto.ca](mailto:jenna.hartel@utoronto.ca)

#### References

*   Bates, M.J. (1986). What is a reference book? A theoretical and empirical analysis. _RQ_, **26**(1), 37-57
*   Bates, M.J. (1989). The design of browsing and berrypicking techniques for the online search interface. _Online Review_, **13**(5), 407-424
*   Bates, M.J. (1999). The invisible substrate of information science. _Journal of American Society for Information Science_, **50**(12), 1043-1050.
*   Bates, M.J. (2005). [Information and knowledge: an evolutionary framework for information science](http://InformationR.net/ir/10-4/paper239.html). _Information Research_, **10**(4), paper 239 Retrieved 22 September, 2010 from http://InformationR.net/ir/10-4/paper239.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5swBZFY2Z)
*   Bates, M.J. (2007). [Defining the information disciplines in encyclopedia development](http://InformationR.net/ir/12-4/colis/colis29.html). _Information Research_, **12** (4), paper colis29 Retrieved 22 September from http://InformationR.net/ir/12-4/colis/colis29.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5swBeSXry)
*   Bates, M.J. (2010). Introduction. In M.J. Bates and M.N. Maack (Eds.). _Encyclopedia of library and information science._ (3rd ed.). (pp. xiii-xx) New York, NY: Taylor and Frances.
*   Behgtol, C. (2003). Classification for information retrieval and classification for knowledge discovery. Relationships between 'professional' and 'naive' classification. _Knowledge Organization_, **30**(2), 64-73
*   Berg, B. (2000). _Qualitative research methods for the social sciences_. Boston, MA: Allyn & Bacon.
*   Bernard, H.R. (2002). _Research methods in anthropology: qualitative and quantitative approaches_. Walnut Creek, CA: Alta Mira Press.
*   Byström, K. & Hansen, P. (2002). Work tasks as units for analysis in information seeking and retrieval studies. In H. Bruce, R. Fidel, P. Ingwersen & P. Vakkari (Eds.), _Emerging frameworks and methods: proceedings of CoLIS4_, (pp. 239-251). Greenwood Village, CO: Libraries Unlimited.
*   Chatman, E.A. (1999). A theory of life in the round. _Journal of the American Socieety for Information Science_, **50**(3), 207-217
*   Collier, J. & Collier, M. (1986). _Visual anthropology: photography as a research method_. Alburquerque, NM: University of New Mexico.
*   Erdelez, S. (1997). Information encountering: a conceptual framework for accidental information discovery. In P. Vakkari, R. Savolainen, and B. Dervin (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts_, (pp. 412-421). London: Taylor Graham.
*   [Framework](http://www.webcitation.org/5swCa4aSP) (n.d.). In _Dictionary.com_. Retrieved 24 January, 2010 from http://dictionary.reference.com/browse/framework (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5swCa4aSP)
*   Hartel, J. (2003). The serious leisure frontier in library and information science: hobby domains. _Knowledge Organization_, **30**(3/4), 228-238
*   Hartel, J. (2005). Serious leisure. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior: a researcher's guide_ (pp. 313-317). Medford, NJ: Information Today.
*   Hartel, J. (2006). [Information activities and resources in an episode of gourmet cooking](http://informationr.net/ir/12-1/paper282.html). _Information Research_, **12**(1), paper 282\. Retrieved 22 September, 2010 from http://informationr.net/ir/12-1/paper282.html (Archived by WebCite® at http://www.webcitation.org/5swCx3iuY)
*   Hartel, J. (2010). Hobby and leisure information and its user. In M.J. Bates and M.N. Maack (Eds.), _Encylopedia of Library and Information Science_ (3rd ed.) (pp. 3263-3274). New York, NY: Taylor and Frances.
*   Hartel, J. (2010). Managing documents at home for serious leisure. A case study of the hobby of gourmet cooking. _Journal of Documentation_, **66**(6), 847-874
*   Hartel, J. (2007). _Information activities, resources, and spaces in the hobby of gourmet cooking._ Unpublished doctoral dissertation, University of California, Los Angeles, California, United States. [Dissertations & theses: full text. (Publication No. AAT 3304683)].
*   Hjørland, B. & Albrechtsen, H. (1995). Toward a new horizon in information science: domain analysis. _Journal of the American Society for Information Science_, **46** (6), 400-425
*   Kuhlthau, C.C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation_, **49**(4), 339-355
*   Kwasnik, B.H. (1999). The role of classification in knowledge representation and discovery. _Library Trends_, **48**(1), 22-47
*   Newman, J.M. (2001). Chinese food: perceptions and publications in the United States. _Chinese Studies in History_, **34**(3), 66-81
*   O'Neill, M. (2003, September/October). Food porn. _Columbia Journalism Review_, 38-45
*   Pettigrew, K.E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behavior among attendees at community clinics. _Information Processing & Management_, **35**(6), 801-817
*   Savolainen, R. (2006). Time as a context of information seeking. _Library & Information Science Research_, **28**(1), 110-127
*   Satija, M.P. (2000). Library classification: an essay in terminology. _Knowledge Organization_, **27**(4), 221-229
*   Sonnenwald, D.H. & Iivonen, M. (1999). An integrated human information behavior research framework for information studies. _Library and Information Science Research_, **21**(4), 429-457
*   Spradley, J.P. (1979). _The ethnographic interview_. New York, NY: Holt, Rinehart and Winston.
*   Stebbins, R.A. (1982). Serious leisure: a conceptual statement. _Pacific Sociological Review_, **25**(2), 251-272
*   Stebbins, R.A. (2001). _New directions in the theory and research of serious leisure_. New York, NY: Edwin Mellen Press.
*   Strauss, A. & Corbin, J. (1998). _Basics of qualitative research: techniques and procedures for developing grounded theory_ (2nd ed.). Thousand Oaks, CA: Sage Publications.
*   Talja, S. (2005) The domain analytic approach to scholars' information practices. In K. Fisher, S. Erdelez and L. McKechnie (Eds.), _Theories of information behavior: a researcher's guide_. Medford, NJ: Information Today.
*   Unruh, D. (1979). Characteristics and types of participation in social worlds. _Symbolic Interaction_, **2**(2), 115-129
*   Unruh, D. (1980). The nature of social worlds. _Pacific Sociological Review_, **23**(3), 271-296
*   White, H.D. (1995). External memory. In Howard D. White, Marcia J. Bates and Patrick Wilson. _For information specialists, interpretations of reference and bibliographic work_ (pp. 249-294). Norwood, NJ: Ablex.
*   White, H.D. & K.W. McCain. (1998). Visualizing a discipline: an author co-citation analysis of information science, 1972-1995\. _Journal of the American Society for Information Science_, **49**(4), 327-355
*   White, H.D. 1999\. Scientist-poets wanted. _Journal of the American Society for Information Science_. **50**(12), 1052-1063
*   Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270

## Appendix 1

Sample of codes used for information activities:

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td>traveling  
receiving (information as a gift)  
dining with other cooks  
eating out (studying menus, foods)  
visiting a market  
blogging  
browsing  
contemplating  
watching television  
reading (cookbooks, gastronomy)  
taking cooking classes  
teaching cooking</td>

<td>consulting  
talking (about cooking)  
publishing  
subscribing (to magazines)  
designing recipes or menus  
comparing (recipes)  
searching for recipes  
checking (a recipe)  
displaying (recipes)  
translating  
listing (ingredients)  
</td>

</tr>

</tbody>

</table>

## Appendix 2

Sample of codes used for information resources:

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td>cookbooks  
magazines  
newspapers  
newsletters  
recipes  
reference soures  
fund-raising cookbooks  
ephemera (calendars, brochures)  
gastronomy writings  
pictures  
ratings and rankings  
lists  
timelines  
journals (diaries)</td>

<td>display boards  
stories  
experiences  
people (family, friends, chefs)  
fairs  
cooking classes  
restaurants  
trips  
public library  
parties  
retail stores  
web pages  
e-mails  
television</td>

</tr>

</tbody>

</table>