#### vol. 20 no. 3, September, 2015

# Everyday information behaviour of Asian immigrants in South Australia: a mixed-methods exploration

#### [Safirotu Khoir](#author), [Jia Tina Du](#author) and [Andy Koronios](#author)  
School of Information Technology and Mathematical Sciences, University of South Australia

#### Abstract

> **Introduction.** This paper investigates the everyday information behaviour of Asian immigrants, including their information needs, information seeking behaviour, information grounds and information sharing, as well as how these relate to their settlement experiences in South Australia.  
> **Method.** Using a mixed-methods approach with surveys, photovoice and interviews, sixteen Asian immigrants participated in this exploratory study.  
> **Analysis.** The survey data were descriptively analysed by using an online survey tool (SurveyGizmo.com). The photovoice data were analysed by participatory analysis, while the interview data were categorised by open coding.  
> **Results.** The results report everyday information needs in detail by distinguishing newcomers and longer-established immigrants. For newcomers, the information needs were categorised as personal, general and official needs. For longer-established immigrants, their information needs were labelled as full participation. Asian immigrants indicated that information was accessed via multiple sources, such as the Internet, people, mass media and formal organisations. Information grounds where the immigrants met and socially interacted were characterised as places, information and people. Information sharing was considered an indispensable part of the identified information grounds.  
> **Conclusions.** Based on the findings, a model was developed to illustrate Asian immigrants' information behaviour in relation to their settlement in South Australia.

## Introduction

According to the Report for the Department of Immigration and Citizenship ([Hugo and Harris, 2011, p. 7](#hug11)), immigrants are _'persons arriving with the intention of settling permanently in Australia'_. The three most recent consecutive censuses (i.e., 2001, 2006 and 2011) issued by the Australian Bureau of Statistics show a rapidly growing trend of immigration to Australia over the past decade. According to the latest census in 2011 ([Australian Bureau of Statistics, 2011a](#abs11a)), approximately 30.2% of the Australian population was born overseas; that is, nearly one in three people in Australia was an immigrant. Of them, 23% were Asian, with the majority from China, Vietnam and India. South Australia is the fourth-largest state and is located in the southern central part of the country. In South Australia, 26.7% of the population consists of immigrants and the number of immigrants from Asian countries, especially from India, China, Vietnam, the Philippines and Malaysia, has increased significantly over the last decade ([Australian Bureau of Statistics, 2011b](#abs11b)). As shown in the Australian population projection, in the next ten years, the number of immigrants in Australia and South Australia will increase by 10% and 5.8%, respectively ([Australian Bureau of Statistics, 2014](#abs14)).

Adapting to life in a new country often requires substantial efforts, such as learning a new language, accepting and adjusting to different customs, lifestyles, social norms and social relationships and dealing with unfamiliar rules and laws ([Organista, Organista and Kurasaki, 2003](#org03)). Settling down in a new country involves not only physical presence in an unfamiliar place but the processes of acquiring and managing a large amount of information about the new physical and social environments ([Lingel, 2011](#lin11); [Sin and Kim, 2013](#sin13)). The timely delivery of needed information is thus critical. If immigrants' information needs are not met, the process of settlement will be more difficult ([Caidi and Allard, 2005](#cai05)). Immigrant settlement is an adaptation process in a new place involving strategies to cope with different circumstances in new social ties and in information seeking activities such as searching for accommodation, jobs and schools for their children, managing health care and using a new language, English ([Caidi, Allard and Quirke, 2010](#cai10)). Information grounds are the places where immigrants meet, share with other people and get involved in social interactions. These are crucial activities during their settlement that deserve to be further explored ([Fisher, Durrance and Hinton, 2004a](#fis04a)).

The investigation of immigrants' everyday life information behaviour has been relatively sparse. Research on the information behaviour of immigrants is believed to be challenging and is in need of further exploration ([Fisher, Marcoux, Miller, Sánchez and Cunningham, 2004b](#fis04b)). In the Australian context, a few studies have focused on social inclusion and information literacy programs for refugees ([Kennan, Lloyd, Qayyum and Thompson, 2011](#ken11); [Lloyd, Kennan, Thompson and Qayyum, 2013](#llo13); [Lloyd, Lipu and Kennan, 2010](#llo10)), with less attention paid to immigrants' daily information behaviour in general. Internationally, there have been such studies in other countries such as the United States and Canada ([Caidi and Allard, 2005](#cai05); [Caidi _et al._, 2010](#cai10); [Fisher _et al._, 2004b](#fis04b)), which have provided a fruitful avenue of research for understanding immigrants' information behaviour. However, these do not necessarily reflect the situation in Australia because of different social systems, rules, regulations and cultures. With the increasingly significant number of immigrants in Australia, there is an imperative need for more empirical research on immigrants' information needs and the information behaviour associated with their settlement process. In this study, we investigate Asian immigrants' everyday information needs, their preferred sources of information, the information grounds for social interaction and how these information grounds and interactions prompt the sharing of information and socialisation during their settlement process in South Australia. Specifically, we address the following research questions:

RQ1\. What sorts of information do Asian immigrants need for their settlement in South Australia?  
RQ2\. How do Asian immigrants seek information to satisfy their everyday needs?  
RQ3\. Where do Asian immigrants usually meet and share information?  

In the present study, we employed a mixed-methods approach to ensure that the biases, limitations and weaknesses of one method are counterbalanced with another method ([Fidel, 2008](#fid08)). This evidence-based, migrant-centred study will contribute to an understanding of South Australian Asian immigrants' information needs and their information behaviour. The findings may also provide insight into the formulation of policies to facilitate the settlement and social inclusion of Asian immigrants and to support better service planning and management. Moreover, the results may help immigrants themselves by providing ideas as to how they can seek and share information more effectively. As the number of the study participants is small and not representative of all Asian immigrants, the results in this study may not be generalised to all Asian immigrants in Australia.

## Literature review

### Information behaviour in everyday life context

Information behaviour is considered to be a significant component of information studies. Information behaviour describes the way in which humans seek, manage, use and share information in diverse contexts both to meet their information needs and to resolve problems, thereby helping to ensure their survival in a particular environment ([Du, 2014](#du14); [Du, Liu, Zhu and Chen, 2013](#du13); [Sonnenwald and Iivonen, 1999](#son99); [Wilson, 2000](#wil00)). As an important aspect of information behaviour, everyday life information seeking offers an understanding of information practices in the context of problem solving in daily life ([Savolainen, 1995](#sav95); [Spink and Cole, 2001](#spi01)). ELIS refers to how people gain access to and use information to satisfy their everyday life information needs ([Savolainen, 2010](#sav10)). This involves active information seeking, where expected and/or unexpected information is obtained from first-hand sources and referrals, as well as the process of _'incidental information acquisition'_, in which people may use informal information sources or may engage with people without the primary intention of seeking information ([McKenzie, 2003](#mck03); [Williamson, 1998](#wil98)). During the course of the development of everyday life information seeking research, several models have emerged ([McKenzie, 2003](#mck03)).Understanding information behaviour associated with daily life issues is challenging and may cover many interests and concerns (Given, 2002). This approach provides a useful basis for understanding information behaviour in an immigrant community ([Caidi et al., 2010](#cai10); [Lingel, 2011](#lin11)). Information behaviour in the present study refers to everyday life practices associated with information needs and information seeking, including the places where certain information is sought, exchanged and shared.

### Information behaviour of immigrants

The general assumption concerning immigrants in a new country is that they are information poor, vulnerable and disadvantaged ([Caidi and Allard, 2005](#cai05); [Caidi _et al._, 2010](#cai10)). Limited research has been conducted into immigrants' information behaviour in different countries and areas ([Courtright, 2005](#cou05); [Fisher _et al._, 2004b](#fis04b); [Lingel, 2011](#lin11); [Silvio, 2006](#sil06); [Tompkins, Smith, Jones and Swindells, 2006](#tom06)). For example, Fisher _et al._ ([2004b](#fis04b)) used information habits and information grounds as frameworks to study migrant Hispanic farm workers in central Washington State in the United States. The authors found that interpersonal sources such as family members, friends, acquaintances and social service organisations including radios and libraries were chosen as the primary sources of information. Similarly, Silvio's study ([2006](#sil06)) on southern Sudanese youth in the city of London, Ontario, in Canada showed the significant role of personal networks in immigrants' information sources. The identified information needs were rights concerning discrimination (25%), employment (20%), political (10%) and health (5%) needs. Meanwhile, an exploratory study by Courtright ([2005](#cou05)) investigated the health information seeking behaviour of Latino newcomers to southern Indiana in the United States. She found that social networks with strong ties, including family members and volunteers in health care, played an essential role in terms of immigrants' health information seeking. More recently, Lingel ([2011](#lin11)) investigated the information tactics of new immigrants in daily life in New York City. Multiple sources of information, such as the Internet and friends, were found to be important in immigrants' everyday information seeking.

In Australia, a few studies have been undertaken to examine information poverty, computer literacy and social inclusion of immigrants including refugees. For instance, Gray, Graycar and Nicolaou ([2012](#gra12)) provided a framework for a longitudinal survey design on humanitarian immigrants in Australia. The design suggested having a balanced number of representative immigrants and refugees in every state in Australia to participate in the survey. As it was only a conceptual design, a follow-up empirical study is expected. Lloyd _et al._ ([2013](#llo13)) found that visual information sources (e.g., shopping catalogues and store flyers) and social and embodied information sources (e.g., previous stories from either service providers or other immigrants) are useful for refugees. The barriers that influenced their information literacy were also identified, including the English language, computer literacy and unfamiliarity with the information environment.

### Information grounds and information sharing

Information grounds have been considered to be one of the approaches to investigate information behaviour ([Fisher _et al._, 2004b](#fis04b)). Pettigrew ([1999, p. 811](#pet99)) defines an information ground as _'an environment temporarily created by the behaviour of people who have come together to perform a given task, but from which emerges a social atmosphere that fosters the spontaneous and serendipitous sharing of information'_. It is believed that information grounds may occur anywhere, including places that are not purposefully designed for information sharing. This is because wherever and whenever social interaction is the focus of gathering, information flows naturally as a consequence ([Fisher, Durrance and Hinton, 2004a](#fis04a)). Various social settings of information grounds have been identified, such as community health clinics ([Pettigrew, 1999](#pet99)), public libraries ([Fisher _et al._, 2004a](#fis04a)) and mobile social networking sites ([Counts and Fisher, 2010](#cou10)). The study of information grounds, however, as integral to information flow and social interaction has not yet received sufficient attention ([Fisher, Landry and Naumer, 2007](#fis07)). Understanding where people mingle and share information is crucial in developing social spaces to support interactions among places, people and information ([Fisher _et al._, 2007](#fis07)). Using Fisher _et al._'s information grounds theory, this research attempts to uncover the nature of Asian immigrants' information grounds, including the places and spaces where immigrants meet, communicate and share information.

People tend to share information within their information grounds ([Fisher _et al._, 2007](#fis07)). Information sharing describes the behaviour of explicit and implicit information exchanges between people, groups, organisations and technologies ([Du, 2014](#du14)). How people share information is deemed to be an important but relatively unexplored aspect of everyday life information behaviour ([Du _et al._, 2013](#du13); [Talja and Hansen, 2006](#tal06); [Wilson, 2000](#wil00)). Social networks (both actual and virtual) and information sharing are integrally linked ([Dekker and Engbersen, 2013](#dek13)) when people meet and communicate (either in physical places or via online media) in the form of social interaction. People often end up sharing information and as a result, information may flow in many directions. The present study takes into account the technological development that may affect information grounds and information sharing activities. For example, the development of the Internet telecommunication system (e.g., Skype or Facebook) makes connection between people and information fundamentally different from what was available ten years ago. These shifts are promoting a generation of immigrants to become transnational citizens who keep a strong connection with their country of origin while becoming a member of their new country ([Vertovec, 2004](#ver04)). Research has not yet been undertaken into how Asian immigrants in Australia use new information grounds and networking skills in their everyday information behaviour.

## Research design

### Study participants

The target group of study participants in the present study was Asian immigrants living in Adelaide, the capital city of South Australia, who were born in the top five source countries, namely India, China, Vietnam, the Philippines and Malaysia ([Khoir, Du and Koronios, 2015](#kho15)). Using university e-mail networks as well as the snowball sampling technique ([Neuman, 2006](#neu06)) in which one participant gave the researcher the name of another immigrant, a total of sixteen Asian immigrants (ten males and six females) were recruited, including four Indians, four Chinese, three Vietnamese, three Filipinos and two Malaysians (Table 1).

<table class="center" style="width:95%;"><caption>  
Table 1: Demographic information of participants</caption>

<tbody>

<tr>

<th>No.</th>

<th>Country of origin</th>

<th>Length of stay</th>

<th>Completed highest level of education</th>

<th>Obtained highest degree</th>

<th>Current situation</th>

<th>Visa category</th>

<th>Intention to become Australian citizen</th>

<th>Job</th>

</tr>

<tr>

<td>1.</td>

<td>Philippines</td>

<td style="text-align:center">21y</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">Philippines</td>

<td style="text-align:center">Full-time student</td>

<td style="text-align:center">Family</td>

<td style="text-align:center">Citizen</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>2.</td>

<td>Vietnam</td>

<td style="text-align:center">6y</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">Australia</td>

<td style="text-align:center">Full-time worker</td>

<td style="text-align:center">Family</td>

<td style="text-align:center">Citizen</td>

<td style="text-align:center">IT staff</td>

</tr>

<tr>

<td>3.</td>

<td>India</td>

<td style="text-align:center">4.5y</td>

<td style="text-align:center">Master</td>

<td style="text-align:center">Australia</td>

<td style="text-align:center">Full-time worker</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>4.</td>

<td>China</td>

<td style="text-align:center">3y</td>

<td style="text-align:center">Master</td>

<td style="text-align:center">Australia</td>

<td style="text-align:center">Full-time student</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>5.</td>

<td>Vietnam</td>

<td style="text-align:center">6y</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">Australia</td>

<td style="text-align:center">Full-time worker</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">IT staff</td>

</tr>

<tr>

<td>6.</td>

<td>Philippines</td>

<td style="text-align:center">4.5y</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">Philippines</td>

<td style="text-align:center">Student & worker</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">No</td>

<td style="text-align:center">Voluntary work & nurse</td>

</tr>

<tr>

<td>7.</td>

<td>China</td>

<td style="text-align:center">6y</td>

<td style="text-align:center">Master</td>

<td style="text-align:center">Australia</td>

<td style="text-align:center">Full-time worker</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">IT staff</td>

</tr>

<tr>

<td>8.</td>

<td>China</td>

<td style="text-align:center">2y</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">China</td>

<td style="text-align:center">Homemaker</td>

<td style="text-align:center">Family</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>9.</td>

<td>India</td>

<td style="text-align:center">4.5y</td>

<td style="text-align:center">Master</td>

<td style="text-align:center">India</td>

<td style="text-align:center">Full-time student</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>10.</td>

<td>India</td>

<td style="text-align:center">3y</td>

<td style="text-align:center">Master</td>

<td style="text-align:center">India</td>

<td style="text-align:center">Full-time student</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>11.</td>

<td>Malaysia</td>

<td style="text-align:center">9m</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">Malaysia</td>

<td style="text-align:center">Homemaker</td>

<td style="text-align:center">Family</td>

<td style="text-align:center">No</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>12.</td>

<td>Vietnam</td>

<td style="text-align:center">5.5y</td>

<td style="text-align:center">Master</td>

<td style="text-align:center">Vietnam</td>

<td style="text-align:center">Full-time student</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>13.</td>

<td>China</td>

<td style="text-align:center">6y</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">China</td>

<td style="text-align:center">Full-time worker</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Citizen</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>14.</td>

<td>Malaysia</td>

<td style="text-align:center">6y</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">Australia</td>

<td style="text-align:center">Full-time worker</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">No</td>

<td style="text-align:center">Project assistant & voluntary work</td>

</tr>

<tr>

<td>15.</td>

<td>Philippines</td>

<td style="text-align:center">16y</td>

<td style="text-align:center">Master</td>

<td style="text-align:center">Australia</td>

<td style="text-align:center">Full-time worker</td>

<td style="text-align:center">family</td>

<td style="text-align:center">Citizen</td>

<td style="text-align:center">Research staff & own business</td>

</tr>

<tr>

<td>16.</td>

<td>India</td>

<td style="text-align:center">6y</td>

<td style="text-align:center">Bachelor</td>

<td style="text-align:center">India</td>

<td style="text-align:center">Full-time worker</td>

<td style="text-align:center">Skilled</td>

<td style="text-align:center">Yes</td>

<td style="text-align:center">IT staff</td>

</tr>

</tbody>

</table>

The participants consisted of both newly arrived immigrants (n=7, 44%) who have lived in South Australia for less than 5 years and longer-established immigrants (n=9, 56%) who have lived there for more than five years ([Caidi and Allard, 2005](#cai05)). English is the second language for all the participants and this study was conducted using English. At home, most of the participants (62.5%) preferred to speak their native languages, such as Chinese (Mandarin), Malay, Tamil, Telugu, Vietnamese, Filipino and Hindi, while some (37.5%) spoke English with their families or housemates. While there was a demographic spread in most areas, all participants in this study had a university degree.

### Data collection

The majority of information behaviour studies have employed a single method, such as questionnaires, interviews, transaction log analyses, experiments, citation analysis or ethnography ([Sonnenwald and Iivonen, 1999](#son99)), with a lack of methodological triangulation as can be achieved with mixed-methods research ([Julien, Given and Opryshko, 2013](#jul13)). Questionnaires and interviews still make up the largest proportion of methods used in information behaviour work ([Julien, Pecoskie and Reed, 2011](#jul11)). Mixed methods exploration employs a combination of several data collection methods that are related and work together to support one another to answer the research questions as a whole ([Fidel, 2008](#fid08)). In the current study, a mixed-methods approach was designed that combined questionnaires, photovoice and interviews to present a fuller picture of Asian immigrants' information behaviour, including how information needs changed at different periods of settlement, the participants' past and present experiences and how their life experiences in the new country influenced their way of dealing with information. The data collection procedures followed the guidelines of the university Human Research Ethics Committee. The procedures and process of data gathering were explained to the participants. Consent forms were provided to participants to sign.

### Questionnaires

Questionnaires (printed and online versions) were used to gather such data. In the questionnaires, we asked demographic information, information needs and sources, how they made use of information to help with the settlement in South Australia and the places to meet and share information. The online questionnaire was created using the survey tool SurveyGizmo.com. Half of the participants preferred the printed version; for them a hard copy was provided at a face-to-face meeting by appointment or an e-copy of the questionnaire was attached and sent to the participants' designated e-mail addresses. For those who preferred online questionnaire, the survey link was sent through to their e-mail addresses. The items in the questionnaire were designed with reference to previous related studies. The questions were tested with a few immigrants in the pilot study ([Khoir, Du and Koronios, 2014](#kho14)).

### Photovoice

Following the completion of the questionnaires, the participants were guided to take photos using a method called photovoice ([Julien _et al._, 2013](#jul13); [Wang and Burris, 1994](#wan94)) for a period of five to seven days. Their photos were related to their information needs, sources for acquiring information and experiences with social interactions and information sharing. Pioneered by Wang and Burris ([1994](#wan94)), the method of photovoice employs images to gain a deeper understanding from the participants' point of view ([Julien _et al._, 2013](#jul13)). Images can be a bridge to the expression of ideas ([Briden, 2007](#bri07); Wang and Burris, [1994](#wan94), [1997](#wan97); [Weber, 2008](#web08)). The purpose of using photovoice is to empower the participants to identify, represent and enhance their community through images ([Wang and Burris, 1997](#wan97)). The participants may also feel more comfortable as they decide for themselves what to represent ([Julien _et al._, 2013](#jul13)). The photovoice method has been adapted recently as a visual method (i.e., photographs) to stimulate interviews and to assist in content analysis ([Hartel and Thomson, 2011](#har11); [Wagner, 1979](#wag79); [Weber, 2008](#web08)). The current research applies the photovoice method with some modifications, namely the use of mobile devices with photo features for taking pictures. The captured images recorded the participants' information-related behaviour, stimulated follow-up interviews and helped in analysing the subjects. All participants chose to use their own mobile phones to capture photos as required. Before taking photos, the researcher provided training and guidelines on several topic themes related to the research project and explained the ethical considerations which included the necessity to seek permission of others in the photo. The participants presented seventy five photos and gave their permission for their use in the research and presentations. These were later selected and categorised into several themes. Two photos were discarded because of unclearness and irrelevance. Therefore, a total of seventy three pictures were collected for further analysis.

### Interviews

The participants were asked to attend semi-structured interviews to discuss with the researcher the photovoice data of the captured photographs. The participants brought the photos, either in printed form or e-copy, to the interview sessions and the researcher collected them. During the interview, the participants were encouraged to tell the story behind each photo, guided by an interview protocol relating to their thoughts, feelings, experiences, knowledge, skills, ideas and preferences ([Johnson and Turner, 2003](#joh03); [King and Horrocks, 2010](#kin10)). The interviews also provided the opportunity for participants to clarify their answers on the questionnaires if anything was unclear, for example, enabling them to provide examples of their information needs. While the original method of photovoice employed small or large group discussions ([Wang and Burris, 1997](#wan97)), this study applied the interview method. These Asian immigrants were of diverse nationality and with varied demographic backgrounds (some housewives with kids and some full-time workers). Interviews, though time consuming, were seen as being more flexible bearing in mind the participants' situations and as providing the opportunity for deeper and more personal reflections. The interviews were undertaken in places convenient to the individual participants and lasted between thirty and sixty minutes on average. The interviews were audio recorded and transcribed later for further analysis.

## Data analysis

Descriptive statistical analysis was used to analyse the questionnaire data. The online survey tool SurveyGizmo.com was employed to assist with the statistical analysis for both printed and online questionnaires. For the printed ones, the researcher entered the data into the online survey tool manually.

For the analysis of photovoice data, the final set of seventy three photos was observed and discussed using participatory analysis ([Wang and Burris, 1997](#wan97)) in the following three phases: 1) selecting images that the participants had taken for further discussion; 2) contextualising, that is, describing and developing stories based on the images; and 3) codifying stories, themes and issues that arose from the discussion. Phases 1 and 2 were undertaken during the interview sessions. In the third phase, open coding ([Strauss and Corbin, 1998](#str98)) was used to codify the photos as well as the interview transcriptions (i.e., self-explanations of the photos) to gain additional insight. Using participatory analysis and open coding, those pictures and interview transcriptions were interpreted and classified.

To test the reliability of the coding, inter-coder consistency was calculated. The researcher coded all the data and a second coder was involved in the process of checking the coding. Using the Holsti formula ([Holsti, 1969](#hol69)), the level of consensus was measured as 0.8, which indicates an acceptable level for drawing conclusions ([Huberman and Miles, 2004](#hub04)).

## Results

The following sections report the results based on the analysis of questionnaires, photographs taken by the participants and interview transcriptions and relate them to each research question in turn.

### RQ1\. What sorts of information do Asian immigrants need for their settlement in South Australia?

Adjustment to a new living environment is an ongoing process, but it is usually more demanding in the early stages ([Caidi _et al._, 2010](#cai10)). Following Caidi and Allard ([2005](#cai05)) division of immigrants in terms of the length of stay, the information needs of Asian participants were examined for newcomers and longer-established immigrants, respectively (Table 2).

<table class="center"><caption>  
Table 2: Asian immigrants’ information needs</caption>

<tbody>

<tr>

<th rowspan="3">Overall rank</th>

<th colspan="4">Information needs</th>

</tr>

<tr>

<th colspan="3">New immigrants</th>

<th>Longer established immigrants</th>

</tr>

<tr>

<th>Personal</th>

<th>General</th>

<th>Official</th>

<th>Full participation</th>

</tr>

<tr>

<td style="text-align:center">1</td>

<td>Health care</td>

<td style="text-align:center">Job vacancies</td>

<td style="text-align:center">Citizenship</td>

<td style="text-align:center">Further education</td>

</tr>

<tr>

<td style="text-align:center">2</td>

<td>English literacy</td>

<td style="text-align:center">Housing</td>

<td style="text-align:center">Local education or degree</td>

<td style="text-align:center">Better employment opportunities</td>

</tr>

<tr>

<td style="text-align:center">3</td>

<td>Meeting new friends</td>

<td style="text-align:center">Transportation</td>

<td style="text-align:center">Finance</td>

<td style="text-align:center">Running own business</td>

</tr>

<tr>

<td style="text-align:center">4</td>

<td>Computer skills</td>

<td style="text-align:center">Job application</td>

<td style="text-align:center">Employment rights</td>

<td style="text-align:center">Social events participation</td>

</tr>

<tr>

<td style="text-align:center">5</td>

<td>Hobbies</td>

<td style="text-align:center">Driving and traffic</td>

<td style="text-align:center">Communication with schools for kids</td>

<td style="text-align:center">Political involvement</td>

</tr>

<tr>

<td style="text-align:center">6</td>

<td>Cultural/religious groups</td>

<td style="text-align:center">Adelaide features</td>

<td style="text-align:center">Legal aid</td>

<td style="text-align:center">—</td>

</tr>

<tr>

<td style="text-align:center">7</td>

<td>—</td>

<td style="text-align:center">Leisure activities</td>

<td style="text-align:center">—</td>

<td style="text-align:center">—</td>

</tr>

</tbody>

</table>

The participants were asked to rank each identified information need using a 7-point Likert scale (1 = most important information need, 7 = least important need). The user-generated rankings were further analysed using the automatic weighted calculation method in SurveyGizmo.com. The items ranked highest by the participants were valued higher than the following rankings. The item with the highest score at the end was ranked as number 1\. As such, the overall importance of information needs was drawn, as shown in Table 2 (Column 1).

For Asian newcomers, their information needs were grouped into _personal, general_ and _official_ needs. Personal information needs referred to information related to individual concerns such as seeking information on health care, English literacy, how to meet new friends, computer skills, personal hobbies and cultural or religious groups. Of these, health care was listed as the most pressing personal need, which indicated new immigrants' aspirations to know about Australian medical systems, medical insurance and how to make an appointment with a doctor. For example, SP10 was a newcomer who was living with her son alone in Adelaide and had experienced one emergency situation when her son got a high fever. She stated that she needed to keep herself informed of any health-related information. From that experience, she learned that in case of emergency, she should go directly to the hospital; in a non-emergency situation, it would better for her to make a doctor's appointment. English literacy was ranked as the second most important concern for newcomers, which was primarily related to understanding Australian accents and speed of talking as well as their own English skills. All participants learned English in their home countries in either formal or non-formal education. However, when they arrived in Australia, they still needed to improve their English, particularly their speaking and listening skills.

General information needs were basic needs required for living in a place. The participating new immigrants ranked job vacancies as their most important general information need, which referred to the needs of job hunting, finding out the availability of positions, previous work experience, educational backgrounds and job references. Unlike job vacancies, information needs on job applications focused on how to prepare job application forms, cover letters, position descriptions and interviews. Housing was also a significant concern, particularly in terms of the rental system, because legal contracts, weekly rent, inspections and bonds were new for most of the fresh immigrants. Other identified general information needs included transportation, driving/traffic, Adelaide features and leisure activities. In this context, leisure activities for the newcomers included things to do for relaxation, where to find places of entertainment and what to do at weekends.

In addition to personal and general needs, official information needs showed newcomers' demand for official involvement in both government and institutions. Several official information needs would (sometimes) require ongoing documentation to support their new life, such as information on obtaining citizenship, pursuing local education, financing (e.g., applying for a credit card), understanding employment rights, communicating with children's schools and seeking legal aid. Of these, becoming an Australian citizen, including the gathering of application procedures, legal forms and required documentation, was the number one official need, followed by the need to pursue local education or a degree. Pursuing local education becomes an alternative option for many newcomers if they cannot find a job immediately after arriving in Australia. For instance, driven by scholarship information offered to immigrants, SP4 decided to apply for a scholarship and pursue a degree in a local university to expand her knowledge and skills. She expected to secure a job with her local degree in the future. Moreover, for those arriving with family, finding information on children's education was a priority. They expressed an imperative need to understand the Australian educational system and preparation classes for their children who were from non-English backgrounds. For example, SP10 experienced a different system of education between Australia and India. As she stated,

> "In India, the school will be from June to June, for one academic year, here [in Australia] is from January to January... so when I bring my son from India, I have to wait till January, I again put him in year one because here they are not considering about which class he finished, they considering only about the age of children so I think it is hard... he is 6 years old, even though he finished year one [in India], he had to do year one again here."

For longer-established immigrants, their information needs were labelled as _full participation_ by considering various aspects of increased civic participation and social connections ([Australian Social Inclusion Board, 2012](#asib12)). As mentioned, a longer-established immigrant was someone who had been in the host country for five years or more and was generally deemed to be better engaged with the host country in all respects. As a newcomer becomes a longer-established immigrant, his or her information needs are bound to evolve ([Caidi and Allard, 2005](#cai05)). The focus of the longer-established immigrants' information needs was found to be on (from the most important to the least important) pursuing further education, better employment opportunities, running one's own business, social/events participation and political involvement. In this context, further education for longer-established immigrants refers to their intention to take more specific courses, either toward a degree or not, for purposes such as to improve their current knowledge and skills, to learn new things, or to find a better job. In regards to employment need, new immigrants, as mentioned above, focused more on how to find a job, while longer-established immigrants tended to find a better position compared to their current employment.

For example, SP15 as a longer-established immigrant stated that though she had a job doing research projects for community development, she still wanted to pursue further education. She was very interested in doing research and was considering pursuing a doctoral degree. Upon completion of the degree, she expected to find a better job. Longer-established immigrants, following the initial demands of adaption to a new country, became freer to pursue more individual goals. Their widening social connections involve them in a broadening network of participation. To conclude, the ranges of identified information needs for different periods of staying may provide some detailed pictures of Asian immigrants and their strategies to adapt to new environments.

### RQ2\. How do Asian immigrants seek information to satisfy everyday needs?

Immigrants seek information to meet their needs and make sense of their situation, which will help them understand the new country they are moving to and the new life they are about to begin ([Shoham and Strauss, 2008](#sho08)). The participating Asian immigrants were identified to purposefully approach a total of 26 different information sources to satisfy their everyday information needs, which were further categorised into four types of sources (Table 3).

<table class="center" style="width:60%;"><caption>  
Table 3: Information sources used by Asian immigrants</caption>

<tbody>

<tr>

<th>Category and sub-categories  
of information sources</th>

<th>Frequency</th>

<th>%</th>

</tr>

<tr>

<th>The Internet</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Facebook</td>

<td style="text-align:center">5</td>

<td style="text-align:center">11</td>

</tr>

<tr>

<td>Specialised website</td>

<td style="text-align:center">4</td>

<td style="text-align:center">9</td>

</tr>

<tr>

<td>Chinese social media</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Google search engine</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Google maps</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Government websites</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Skype</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Twitter</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Supermarket membership</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Subtotal_</td>

<td style="text-align:center; background-color:#F3EFAB;">19</td>

<td style="text-align:center; background-color:#F3EFAB;">40</td>

</tr>

<tr>

<th>Interpersonal source</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Friends</td>

<td style="text-align:center">9</td>

<td style="text-align:center">19</td>

</tr>

<tr>

<td>Family</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Work colleagues</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Subtotal_</td>

<td style="text-align:center; background-color:#F3EFAB;">11</td>

<td style="text-align:center; background-color:#F3EFAB;">23</td>

</tr>

<tr>

<th>Mass media</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Newspapers</td>

<td style="text-align:center">3</td>

<td style="text-align:center">7</td>

</tr>

<tr>

<td>Television</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Children story books</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>House billboards</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Magazine</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Radio</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Telephone directory</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Tourism information board</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Subtotal_</td>

<td style="text-align:center; background-color:#F3EFAB;">11</td>

<td style="text-align:center; background-color:#F3EFAB;">23</td>

</tr>

<tr>

<th>Formal organisation</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Community libraries</td>

<td style="text-align:center">2</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Bank branches</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Campus central</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Government offices</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Information centres</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>University libraries</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Subtotal_</td>

<td style="text-align:center; background-color:#F3EFAB;">7</td>

<td style="text-align:center; background-color:#F3EFAB;">14</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Total_</td>

<td style="text-align:center; background-color:#F3EFAB;">48</td>

<td style="text-align:center; background-color:#F3EFAB;">100</td>

</tr>

</tbody>

</table>

It is interesting to note that the participants employed several types of information sources to satisfy their various information needs. The sources of information were classified into four categories: The Internet (40%), interpersonal sources (23%), mass media (23%) and formal organisations (14%). Similar results were reported by immigrants in New Zealand ([Mason and Lamain, 2007](#mas07)), who accessed information via both impersonal and people sources: the Internet (28%), media (15%), friends (21%) and family (13%). In this study, all participants reported that they had access to the Internet at home, nearly half of the participants used the Internet for up to five hours per day and around a quarter used the Internet between eleven and fifteen hours per day. The Internet sources used by the participants included Google search engine, social media (e.g., Facebook and Twitter), specialised websites (e.g., real estate websites), government websites and communication tools (e.g., Skype). For instance, SP11 (a newcomer) used the Centrelink website to find out about Child Care Benefit for her five-year-old daughter. Centrelink is an Australian government agency and part of the Department of Human Services that is responsible for delivering social services and payments to eligible members. The website provides information for migrants, refugees and visitors regarding child support, jobs, family, disabilities and health information. The benefit enabled her to pay less for her daughter's childcare services. Eligibility for benefit, required supporting documents and submission procedures can be found on the Centrelink website. When requiring or clarifying further information, she would make a phone call with Centrelink staff or follow up in person.

Interpersonal sources refer to people the immigrants rely on. This category (23%) represented people such as friends, family members and colleagues who were approached to obtain information in the settlement process. All participants had at least one friend or family member in South Australia before they arrived. For example, SP5 stated that she always asked questions of her brother and friends who had stayed in Adelaide for a longer time, as they were more reliable in providing formal or practical information based on their previous knowledge or practical experience. She also used this interpersonal source to clarify information that she found unclear from the Internet.

Some participants mentioned their information sources in the form of mass media (23%) both in the form of printed (e.g., parents magazines, Hindi newspaper) or electronic (e.g., radio, television). For example, SP4 presented a photo of Adelaide Child-the Best Guide for Parents, which is a magazine for parents. She received the magazine for free from a public library and explained that the magazine was a useful source for keeping her aware of kids' activities available in Adelaide. She could plan where to bring her daughter based on the information in this monthly magazine.

The participants also went to several formal organisations (14%) to seek information, including community libraries, information centres and government offices. In terms of government offices as an information source, the participants indicated that they accessed government institutions both by visiting the office physically and via their websites. In using libraries, SP8 and SP10 regularly visited a community library near their house, particularly the kids, toys and storybooks section. As a homemaker with a 3-year-old daughter, SP8 needed to find out entertainment for her child. She found out information on borrowing toys from local librarians. She also obtained information about storytelling sessions that were designed for children at her daughter's age. For Asian immigrants in this study, community libraries were more often used to satisfy family information needs, especially recreational needs for children. Similar results were reported in a study of immigrants in Queens, New York ([Fisher _et al._, 2004a](#fis04a)). Thus, in this study, the immigrants used community libraries more for the services rather than as places to find information to support the settlement process.

### RQ3\. Where do Asian immigrants usually meet and share information?

As mentioned earlier, information grounds create a social environment in which unexpressed/unexpected information gathering and information sharing are usually prompted. Immigrants may gather together and share knowledge without purposefully seeking information ([Fisher _et al._, 2004a](#fis04a)). Social networking/interaction is one of the principal ways by which information needs are satisfied and information sharing takes place ([Shoham and Strauss, 2008](#sho08)). Thus, understanding the information grounds of immigrants may lead to support for the exchanges of information ([Fisher _et al._, 2007](#fis07)). Table 4 shows the migrant participants' preferred information grounds to meet, communicate and share information.

<table class="center"><caption>  
Table 4: Information grounds preferred by Asian immigrants to meet and share information</caption>

<tbody>

<tr>

<th>Types of information grounds  
and examples</th>

<th>Frequency</th>

<th>%</th>

</tr>

<tr>

<th>Virtual space</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Facebook</td>

<td style="text-align:center">12</td>

<td style="text-align:center">20</td>

</tr>

<tr>

<td>Twitter</td>

<td style="text-align:center">5</td>

<td style="text-align:center">8.5</td>

</tr>

<tr>

<td>Adelaide BBS forum</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>LinkedIn</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Skype</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Subtotal_</td>

<td style="text-align:center; background-color:#F3EFAB;">21</td>

<td style="text-align:center; background-color:#F3EFAB;">35.5</td>

</tr>

<tr>

<th>Physical place</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Cafe</td>

<td style="text-align:center">4</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Home</td>

<td style="text-align:center">4</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Playground</td>

<td style="text-align:center">3</td>

<td style="text-align:center">5</td>

</tr>

<tr>

<td>Beach</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>Restaurant</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>Study room in university</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Tennis court</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>University lobby</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Subtotal_</td>

<td style="text-align:center; background-color:#F3EFAB;">18</td>

<td style="text-align:center; background-color:#F3EFAB;">29</td>

</tr>

<tr>

<th>Association or group</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Church</td>

<td style="text-align:center">4</td>

<td style="text-align:center">6</td>

</tr>

<tr>

<td>Telugu association</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Malaysian association</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Community centre</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Golf club</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Parent's club</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Student organisation</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Subtotal_</td>

<td style="text-align:center; background-color:#F3EFAB;">10</td>

<td style="text-align:center; background-color:#F3EFAB;">18</td>

</tr>

<tr>

<th>Social event</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>English course</td>

<td style="text-align:center">5</td>

<td style="text-align:center">8.5</td>

</tr>

<tr>

<td>Barbeque</td>

<td style="text-align:center">3</td>

<td style="text-align:center">5</td>

</tr>

<tr>

<td>University gathering</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Community project</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Subtotal_</td>

<td style="text-align:center; background-color:#F3EFAB;">10</td>

<td style="text-align:center; background-color:#F3EFAB;">17.5</td>

</tr>

<tr>

<td style="text-align:right; background-color:#F3EFAB;">_Total_</td>

<td style="text-align:center; background-color:#F3EFAB;">59</td>

<td style="text-align:center; background-color:#F3EFAB;">100</td>

</tr>

</tbody>

</table>

Fisher _et al._'s ([2007](#fis07)) information grounds included three dimensions: information-centred, place-centred and people-centred. Using these dimensions, we classified the information ground results into four categories, including virtual space (35%), physical places (29%), associations or groups (18%) and social events (17.5%).

The information-centred refers to places identified based on the usefulness and significance of (some) information obtained including specific topics and discussions and ways to share information ([Fisher _et al._, 2007](#fis07)). In this study, those places were identified as virtual spaces (35.5%). The information grounds in which the participants met and mingled online contained social networking platforms, e.g., Adelaide BBS forum, Facebook, Twitter, Skype and LinkedIn. The participants used virtual spaces to communicate with people both in Australia and in their home countries. For instance, Facebook was used by SP3 to keep in touch with her friends. She liked Facebook because she could communicate with others via live chatting or comment on friends' postings or photos. She learned from friends' postings about practical things in South Australia, such as driving lessons and parking permits. SP4 and SP8, who were newly arrived immigrants from China, mentioned that the Adelaide BBS forum (in Chinese) was the place to meet fellow Chinese people and they often discussed and shared aspects of information about China and Australia. Both of them used the forum almost every day, which made them feel comfortable when they lived in a new country which has a totally different culture and language. Discussions in these forums were in Chinese and included such topics as where to buy inexpensive goods, how to purchase an affordable house and where to go out for weekends.

The place-centred dimension refers to the places chosen by people based on several considerations such as comfort, easy to reach, non-noisy environment and places that allowed doing multiple things, such as eating while talking ([Fisher _et al._, 2007](#fis07)); these were identified as physical places (29%) in the current study. Of the physical information grounds (places), cafes, homes and playgrounds were the most preferred places. For instance, SP12 invited Australian neighbours to home and entertained them with traditional Vietnamese food; this facilitated his communications with local Australians. Sharing food and stories became a way to build relationships and ease the way into a new neighbourhood.

People-centred information grounds reflect places where immigrants go by considering size and type, familiarity and intimacy among people ([Fisher _et al._, 2007](#fis07)); these were identified as associations/groups and social events (35.5%) in this study. In terms of associations/groups, the most preferred ground was churches. For example, SP1 regularly went to the church to pray and to meet people with the same belief. She obtained a lot of information in this way, which helped her to settle down well in South Australia. By going to church and being involved with the community, she felt accepted and secure in her religious observance (Figure 1). SP11 from Malaysia joined a parents' club of a kindergarten to meet and share information with other parents. She found the sharing activities among parents were useful for her. One of the topics she enjoyed most was tips and tricks for preparing a child's lunch box for her five-year-old daughter.

<figure class="centre">![Figure 1: Church community](p687fig1.png)

<figcaption>Figure 1: Church community</figcaption>

</figure>

Social events, as another dimension in the people characteristic, indicated that people from various cultural backgrounds gathered in diverse activities such as English courses, barbeques, community projects and university gatherings. For example, almost all participants had concerns about understanding Australian accents and slang, even though they had studied English prior to coming to Australia. To overcome this barrier, some participants attended courses to improve their English skills. For instance, SP8 attended a free English course offered by the local community library and found that the English course provided a useful opportunity to meet and communicate with other people who had similar levels of English skills. She felt less nervous in this informal class situation. Just as SP8 commented, attending the English classes prompted the giving and sharing of information, such as discussing with classmates about appropriate activities for kids in Adelaide.

Furthermore, a barbeque party is a very popular and common social activity in Australia. SP12 often held a barbeque party at home to meet friends, relax and create a network. Public spaces in South Australia such as playgrounds and parks have barbeque equipment for use by the public. Furthermore, SP15 got involved actively in a cultural community project in the Barossa Region in South Australia in the form of culture sharing, particularly in basket weaving artwork, on which she was an expert. She received an Arts Award from the local government to recognise her contribution and dedication. SP1 was originally from the Philippines and had spent several years in Japan. She was an expert in origami, which is the Japanese art of folding paper into decorative shapes and figures. She was not only proud of being a Filipino, but delighted to share her skills in origami arts with local people. Her involvement captures the possibilities of a multicultural society where an immigrant feels confident in community cultural sharing (Figure 2). The stories behind the photos provide a better understanding of the participants' information behaviour during their settlement process.

<figure class="centre">![Figure 2\. Cultural sharing sessions ](p687fig2.png)

<figcaption>Figure 2: Cultural sharing sessions</figcaption>

</figure>

## Discussion

Our findings provide some insight into what sorts of information Asian immigrants needed, where and how they fulfilled their everyday information needs and where they met and shared information.

### Asian immigrants' information needs and sources of information

In terms of information needs, our results show how Asian immigrants' needs evolved over time as they settled down in South Australia. Specifically, the newly arrived immigrants expressed their needs for information associated with their survival, such as finding out about employment opportunities, how to rent a house, how the medical care system worked for individual requirements, how to improve their English skills and which places were good for meeting new friends.

Depending on the sort of information they were looking for, several different types of sources were used to satisfy their specific information needs such as to find a job and to apply for citizenship. For example, prior to her migration, newcomer SP4 had working experiences as a business analyst in e-commerce in Singapore. Finding a local job was a challenge for her. She checked information from www.seek.com, one of the most popular job websites in Australia. She also asked her friends and looked at local newspapers. After trying unsuccessfully for some time, she ended up pursuing a higher degree (a doctoral degree) with scholarships. It is noted that having a higher educational level does not necessarily make one able to secure a good job. Finding a job was far more challenging for newcomers, even if they had already had working experiences elsewhere before they moved to South Australia. Similar results were reported in Qayyum, Thompson, Kennan and Lloyd ([2014](#qay14)) that securing a job is the priority for refugees in Australia who relied on the strength of word-of-mouth and informal channels for job information. In a similar vein, looking for job opportunities was also a pressing need for Hispanic farmworker immigrants in the Pacific Northwest of the United States ([Fisher _et al._, 2004b](#fis04b)). These Hispanic immigrants found it hard to find jobs better than farmworker positions.

When comparing the prioritised information needs of new and longer-established immigrant participants, one can see that the information needs of newcomers covered all aspects of their lives that were critical to their experiences of settlement as they learned to adapt to a new country. However, with the security of having become an integral part of the new country, longer-established immigrants were able to assess their settlement process and began to make plans for a better future by contributing to their new society. Longer-established immigrants were believed to have more experiences and more confidence in dealing with information in South Australia. They were usually more prepared to have advanced considerations such as to pursue further education, plan business ventures and get involved in political activities. Despite the fact that newcomers and longer-established immigrants demonstrated different information needs, their choices of information sources were found to be similar.

Of the information sources identified in this study, Internet sources (40%) including Facebook and websites played the most significant role in the immigrants' settlement. This result was supported by the fact that all the participants had access to the Internet at home as well as in other public places such as public libraries and universities, making online searches part of their daily activity. Interestingly, despite the fact that immigrants were deemed to be _'vulnerable'_ and _'marginalised'_ ([Caidi and Allard, 2005](#cai05)), Asian immigrants in this study had good capacities to satisfy their information needs online and did not find any significant obstacle in using the Internet. This is perhaps because of the fact that all participants had smartphones with data packages, had a high level of education and primarily came from Generation Y, meaning they were born during the 1980s and early 1990s ([Weiler, 2005](#wei05)). However, studies of immigrants in Ireland ([Komito, 2011](#kom11)), London ([Silvio, 2006](#sil06)) and the Pacific Northwest of the United States ([Fisher _et al._, 2004b](#fis04b)) reported family and friends rather than the Internet were the most preferred sources of information. This might be because of the fact that the Internet was not as heavily used as today and more recent communication technologies (e.g., smartphones and social media) were still in their infancy.

Asian immigrants considered interpersonal sources (23%), such as friends, family and work colleagues, to be the second most important information source. Friends (19%) were approached more frequently than others. In the current study, catching up with friends on Facebook to seek information was categorised as sourcing information from the Internet (i.e., an Internet source). The process of finding and making friends can be started from offline contacts then move to online connection through social media ([Lampe, Ellison and Steinfield, 2007](#lam07)) which may create a new practice of interpersonal communication.

### Characteristics of Asian immigrants' information grounds and information sharing

Asian immigrant participants demonstrated a strong preference for various information grounds that facilitate the development of social interactions on various topics.

Social spaces, both physical and virtual, form the basis of everyday life information grounds and information sharing for Asian immigrants. While the purpose of such gatherings is social connection, they prompt the construction of stronger relationships and the sharing of information between family, friends and fellow immigrants ([Caidi _et al._, 2010](#cai10); [Counts Fisher, 2010](#cou10); [Fisher _et al._, 2007](#fis07)). For example, SP4 initially intended to ask fellow Chinese people on the Adelaide BBS forum about kids' activities. She obtained additional information on garage sales as an alternative place to buy cheap goods serendipitously. The findings further reflect the observations of homophily in social networks as _'contact between similar people occurs at a higher rate than among dissimilar people'_ ([McPherson, Smith-Lovin and Cook, 2001, p. 416](#mcp01)). The digital world creates possibilities for immigrants to stay connected to their old life as well as develop a new one ([Srinivasan and Pyati, 2007](#sri07); [Vertovec, 2004](#ver04)). Virtual spaces for Generation Y are increasingly familiar territory and provide spaces in which the participants reported gathering and sharing information.

It is interesting to note that virtual spaces, particularly Facebook, appeared in both information sources and information grounds, as shown in Tables 3 and 4\. The results confirm the findings reported by Bates and Komito ([2012](#bat12)), who found that immigrants have a strong connection with social media networks. Counts and Fisher ([2010, p. 104](#cou10)) also concluded the importance of online settings as information grounds. Facebook was comfortably used as a source and place to share perhaps also because of the fact that all participants owned smartphones that enabled them to have information easily at hand. Social network sites have been considered a valuable information source ([Counts and Fisher, 2010](#cou10); [Huang, Chu and Chen, 2014](#hua14); [Ravindran, Kuan, Chua and Hoe Lian, 2014](#rav14)), and Facebook is the main identified source. As Facebook is a free social media application, which means that everyone can be a member; the information shared through Facebook may be formal or informal, depending on the information source. Therefore, the credibility of information gathered through Facebook should be taken into account ([Osatuyi, 2013](#osa13)). One advantage of using Facebook is that people maintain social interactions with minimal efforts and time ([Lampe _et al._, 2007](#lam07); [Osatuyi, 2013](#osa13)) with a single user or multiple users at the same time, either in a public wall or in an embedded private chat service. Supported by emoticons, attachment sharing and voice chat ([Joinson, 2008](#joi08)), our results suggest that social media such as Facebook promote a new but indispensable method of interaction for immigrants.

Nevertheless, there is a strong preference for face-to-face meetings since this enables people to easily develop conversation topics and have more spontaneous reactions. The story from SP8 with the involvement in an English course showed the social gathering leading to unexpected information sharing. A meeting in person in a relaxed and less formal information ground allows people to have more interactive communication with facial expressions that can help to ensure reliability and trust ([Fisher _et al._, 2004b](#fis04b)). Within various information grounds, the participants shared what they have achieved, what they did in/for the communities and what made them anxious and worried in the settlement. The results show how these experiences in dealing with information may assist Asian immigrants to settle down well in Australia. For example, SP1's story of sharing not only information but also origami culture depicted how immigrants could play a role and be accepted in the community, which then leads to enhance their self-confidence. Sharing experience was found to have a direct and highly influential impact on the ways that information was acquired, used and circulated. The one-to-one meetings in the data collection process, in a relaxed environment with the selected photographic images, opened the richness of story that could not be found in questionnaires. Interestingly, English as the second language of all participants was actually not a significant barrier to sharing information and engaging in community activities in the new land. SP15 even showed a high achievement by winning a community participation award from a local government in South Australia. In fact, a multicultural country such as Australia creates a new opportunity for immigrants. A feeling of belonging and a feeling of competence will bring immigrants a successful experience in a new country ([Walsh and Horenczyk, 2001](#wal01)).

### Toward an information behaviour model

Based on the findings and discussions, the information behaviour model of Asian immigrants in South Australia was developed (Figure 3).

<figure class="centre">![Figure 3\. Asian immigrants’ information behaviour model ](p687fig3.png)

<figcaption>Figure 3: Asian immigrants’ information behaviour model</figcaption>

</figure>

The model illustrates the information behaviour of Asian immigrants during their settlement process in South Australia. Information behaviour through everyday experiences is studied as a holistic concept ([Du _et al._, 2013](#du13)) with several facets including information needs, information seeking, information sources, information grounds and information sharing. Newcomers and longer-established immigrants expressed different information needs, which led to the use of multiple sources to satisfy these information needs. The Internet was used more frequently as a source of information, including communicating with people through online sources such as Facebook and Chinese social media. Online communication provides a new type of interpersonal sources for Asian immigrants. All aspects of information behaviour (in everyday activities) and the settlement process may interplay with each other. This indicates that the success of information behaviour is important in immigrants' settlement and the success of settlement is shaped by their capacity to deal with information in everyday experiences. The findings enhance our understanding of how Asian people deal with information, satisfy their information needs and adapt to other cultures.

### Practical recommendations

This study identifies the information needs of Asian immigrants based on the duration of stay, which may bring some insight to immigration policy makers and public service providers in delivering better services and support. For example, information on job recruitment and job application is highly demanded by newcomers. The South Australian Government currently provides support with careers and jobs in _'Skills for All'_ through its online services (www.skills.sa.gov.au) for all Australians, but not specifically for immigrants. The immigrants may need a more engaged environment and a small number of practical sessions to help them resolve relevant issues such as how to write an Australian-style resume, how to find appropriate references (since they are new arrivals) and how to prepare for and perform better in interviews. Such small-scale sessions would enable immigrants to get closer, get more attention and learn a lot and could be organised by local governments, associations and community libraries as the identified sources of information and disseminated through online social and mass media. Organising workshops by inviting some companies to present what is expected from job applicants in the recruitment process would also be helpful.

## Conclusion and future research

This paper examined the everyday information behaviour of a group of Asian immigrants who were drawn to Australia, a new country, through skill and family reunion streams. Because of the differences in language, culture, lifestyle and social norms, various sorts of information needs have emerged for settling well into a new life and society. The more specific information needs are satisfied, the smoother the settlement process will be. The proposed information behaviour model incorporates information grounds and information sharing in everyday experiences.

Developing methodologies that research immigrant information behaviour is a real challenge, as immigrant experiences vary according to personal capabilities and cultural determinants ([Sonnenwald and Iivonen, 1999](#son99)). The mixed-methods exploration reported in this paper enables a comprehensive investigation of Asian immigrants' information needs and behaviour, helps to overcome any language barriers (by means of photos) and provides the opportunity to triangulate data. The photovoice images revealed many of the participants' information-related behaviour, viewpoints and personal experiences that could not be easily expressed verbally.

In spite of providing an enhanced understanding of immigrants' information behaviour, this study has some limitations. Even though the immigrants came from five countries, this study must be considered exploratory in nature. In terms of the number of participants, age and educational backgrounds, the results may not represent all Asian immigrants in South Australia. Rather, our findings offer evidence for young adults and well-educated Asian immigrants from certain countries. The results may contradict those of prior studies that assumed that immigrants tend to be vulnerable. We are currently undertaking a large-scale survey involving some 200 Asian immigrants. This will enable a more detailed analysis of their information behaviour, barriers and strengths as they adapt to a new environment. Settling in a new country is an ongoing process and consequently requires research over a longer time span.

## Acknowledgements

This research is fully funded and supported by Australian Awards Scholarship. The authors thank Professor Robert Davison for his insightful comments on the article draft, Wendy Baker and the anonymous reviewers for their valuable comments and suggestions.

## About the authors

**Safirotu Khoir** is currently a PhD student at School of Information Technology and Mathematical Sciences, University of South Australia, Australia. Her research is on information behaviour of Asian immigrants in South Australia. Earlier, she was a faculty member at Graduate School of Information and Library Management, Universitas Gadjah Mada and director of Nation Building Corner (2009-2012) at the same university. She was previously involved in a project conducted by Curtin University Library, Western Australia, ['Elizabeth Jolley Research Collection Project’](http://john.curtin.edu.au/jolley/) in 2007\. She can be contacted at [safirotu.khoir@mymail.unisa.edu.au](mailto:safirotu.khoir@mymail.unisa.edu.au).  
**Jia Tina Du** is a Lecturer in the Information Management Program, School of Information Technology and Mathematical Sciences at the University of South Australia, Australia. She was 2010 Highly Commended Award Winner of the prestigious International Emerald/EFMD Outstanding Doctoral Research Awards in the Information Science Category for her PhD thesis on Web users’ multi-tasking search behaviour. She has produced a consistent stream of high-quality publications since completing her PhD. Her research interests are in interactive/cognitive information retrieval, Web research, multitasking search and human information behaviour. She can be contacted at [tina.du@unisa.edu.au](mailto:tina.du@unisa.edu.au).  
**Andy Koronios** is the Head of the School of Information Technology and Mathematic Sciences at the University of South Australia, Australia. Professor Koronios has extensive experience in both commercial and academic environments and has interests in electronic commerce, information quality, IT-mediated business models, enterprise architecture and information governance. He also has a keen interest in the changing role of the modern CIO and the development of IT professionals. He can be contacted at [andy.koronios@unisa.edu.au](mailto:andy.koronios@unisa.edu.au).

#### References

*   Australian Bureau of Statistics. (2011a). [_Basic community profile (Australia)_](http://www.webcitation.org/6aiTk1yt0). Canberra: Australian Bureau of Statistics. Retrieved from http://www.censusdata.abs.gov.au/census_services/getproduct/census/2011/communityprofile/0?opendocument&navpos=230 (Archived by WebCite® at http://www.webcitation.org/6aiTk1yt0)
*   Australian Bureau of Statistics. (2011b). [_Basic community profile (South Australia)_](http://www.webcitation.org/6aiTtMw4v). Canberra: Australian Bureau of Statistics. Retrieved from http://www.censusdata.abs.gov.au/census_services/getproduct/census/2011/communityprofile/4?opendocument&navpos=230 (Archived by WebCite® at http://www.webcitation.org/6aiTtMw4v)
*   Australian Bureau of Statistics. (2014). [_Population Projections, Australia 2012 to 2101_](http://www.webcitation.org/6aiU2F0NI). Canberra: Australian Bureau of Statistics. Retrieved from http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/3222.02012%20%28base%29%20to%202101?OpenDocument (Archived by WebCite® at http://www.webcitation.org/6aiU2F0NI)
*   Australian Social Inclusion Board. (2012). [_Social inclusion in Australia. How Australia is faring_](http://www.webcitation.org/6aiU9yHmb). Retrieved from http://ppcg.org.au/dev/wp-content/uploads/2011/08/HAIF_report_final.pdf (Archived by WebCite® at http://www.webcitation.org/6aiU9yHmb)
*   Bates, J. & Komito, L. (2012). Migration, community and social media. In G. Boucher, A. Grindsted & T. L. Vicente (Eds.), _Transnationalism in the global city_ (pp. 97-112). Bilbao, Spain: Universidad de Deusto.
*   Briden, J. (2007). Photo surveys: eliciting more than you knew to ask for. In N. F. Foster & S. Gibbons (Eds.), _Studying students: the undergraduate research project at the University of Rochester._ Chicago: Association of College and Research Libraries.
*   Caidi, N. & Allard, D. (2005). Social inclusion of newcomers to Canada: an information problem? _Library & Information Science Research, 27_(3), 302-324.
*   Caidi, N., Allard, D. & Quirke, L. (2010). Information practices of immigrants. _Annual Review of Information Science and Technology, 44_, 493-531.
*   Counts, S. & Fisher, K. (2010). Mobile social networking as information ground: a case study. _Library & Information Science Research, 32_(2), 98-115.
*   Courtright, C. (2005). [Health information-seeking among Latino newcomers: an exploratory study.](http://www.webcitation.org/6aiRzSzsX) _Information Research, 10_(2), paper 224\. Retrieved from http://informationr.net/ir/10-2/paper224.html (Archived by WebCite® at http://www.webcitation.org/6aiRzSzsX)
*   Dekker, R. & Engbersen, G. (2013). How social media transform migrant networks and facilitate migration. _Global Networks, 14_(4), 401-418.
*   Du, J. T. (2014). The information journey of marketing professionals: Incorporating work task-driven information seeking, information judgments, information use and information sharing. _Journal of the Association for Information Science and Technology, 65_(9), 1850-1869.
*   Du, J. T., Liu, Y. H., Zhu, Q. & Chen, Y. (2013). [Modelling marketing professionals’ information behaviour in workplace: towards a holistic understanding.](http://www.webcitation.org/6aiSKhoCw) _Information Research 18_(1), paper 560\. Retrieved April 5, 2013 from http://informationr.net/ir/18-1/paper560.html (Archived by WebCite® at http://www.webcitation.org/6aiSKhoCw)
*   Fidel, R. (2008). Are we there yet?: mixed methods research in library and information science. _Library & Information Science Research, 30_(4), 265-272.
*   Fisher, K.E. , Durrance, J. C. & Hinton, M. B. (2004a). Information grounds and the use of need-based services by immigrants in Queens, New York: a context-based, outcome evaluation approach. _Journal of the American Society for Information Science and Technology, 55_(8), 754-766.
*   Fisher, K.E. , Landry, C. F. & Naumer, C. (2007). [Social spaces, casual interactions, meaningful exchanges: 'information ground' characteristics based on the college student experience.](http://www.webcitation.org/6aiSVOaEK) _Information Research, 12_(2), paper 291\. Retrieved from http://www.informationr.net/ir/12-2/paper291.html (Archived by WebCite® at http://www.webcitation.org/6aiSVOaEK)
*   Fisher, K. E., Marcoux, E., Miller, L. S., Sánchez, A. & Cunningham, E. R. (2004b). [Information behaviour of migrant Hispanic farm workers and their families in the Pacific Northwest](http://www.webcitation.org/6aiShnvdc). _Information Research, 10_(1), paper 199\. Retrieved 2013 from http://InformationR.net/ir/10-1/paper199.html (Archived by WebCite® at http://www.webcitation.org/6aiShnvdc)
*   Given, L. M. (2002). The academic and the everyday: investigating the overlap in mature undergraduates' information–seeking behaviors. _Library & Information Science Research, 24_(1), 17-29.
*   Hartel, J. & Thomson, L. (2011). Visual approaches and photography for the study of immediate information space. _Journal of the American Society for Information Science and Technology, 62_(11), 2214-2224.
*   Holsti, O. R. (1969). _Content analysis for the social sciences and humanities._ Reading MA: Addison-Wesley.
*   Huang, H., Chu, S. K. & Chen, D. Y. (2014). Interactions between English-speaking and Chinese-speaking users and librarians on social networking sites. _Journal of the American Society for Information Science and Technology, 66_(6), 1150-1166.
*   Huberman, A. M. & Miles, M. B. (2004). _Qualitative data analysis._ Thousand Oaks, CA: Sage Publications.
*   Johnson, B. & Turner, L. (2003). Data collection strategies in mixed methods research. In A. Tashakkori & C. Teddlie (Eds.), _Handbook of mixed methods in social & behavioral research._ Thousand Oaks, CA: Sage Publications.
*   Julien, H., Given, L. M. & Opryshko, A. (2013). Photovoice: a promising method for studies of individuals' information practices. _Library & Information Science Research, 35_(4), 257-263.
*   Julien, H., Pecoskie, J. & Reed, K. (2011). Trends in information behavior research, 1999–2008\. A content analysis. _Library & Information Science Research, 33_(1), 19-24.
*   Kennan, M. A., Lloyd, A., Qayyum, A. & Thompson, K. (2011). Settling in: the relationship between information and social inclusion. _Australia Academic & Research Libraries, 42_(3), 191-210.
*   King, N. & Horrocks, C. (2010). _Interviews in qualitative research._ London: Sage Publication.
*   Khoir, S., Du, J. T. & Koronios, A. (2015). Linking everyday information behaviour and Asian immigrant settlement processes: Towards a conceptual framework. _Australian Academic & Research Libraries. 46_(2), 86-100.
*   Komito, L. (2011). Social media and migration: virtual community 2.0\. _Journal of the American Society for Information Science and Technology, 62_(6), 1075-1086.
*   Lingel, J. (2011). [Information tactics of immigrants in urban environments.](http://www.webcitation.org/6aiSsvPtv) _Information Research, 16_(4), paper 500\. Retrieved from http://www.informationr.net/ir/16-4/paper500.html (Archived by WebCite® at http://www.webcitation.org/6aiSsvPtv)
*   Lloyd, A, Kennan, M. A., Thompson, K. M. & Qayyum, A. (2013). Connecting with new information landscapes: information literacy practices of refugees. _Journal of Documentation, 69_(1), 121-144.
*   Lloyd, A., Lipu, S. & Kennan, M. A. (2010). On becoming citizens: examining social inclusion from an information perspective. _Australian Academic & Research Libraries, 41_(1), 42-52.
*   Mason, D. & Lamain, C. (2007). [_Nau mai haere mai ki Aotearoa: information seeking behaviour of New Zealand immigrants_](http://www.webcitation.org/6aiUmcFMa). Wellington: New Zealand Federation of Ethnic Councils. Retrieved from http://www.victoria.ac.nz/cacr/research/migration/info-seeking-behaviour-of-new-zealand-immigrations/Info-seeking-behaviour-of-NZ-immigrants.pdf (Archived by WebCite® at http://www.webcitation.org/6aiUmcFMa)
*   McKenzie, P.J. (2003). A model of information practices in accounts of everyday-life information seeking. _Journal of Documentation, 59_(1), 19-40.
*   McPherson, M., Smith-Lovin, L. & Cook, J.M. (2001). Birds of a feather: homophily in social networks. _Annual Review of Sociology, 27_, 415-444.
*   Neuman, W. L. (2006). _Social research methods: qualitative and quantitative approaches._ Boston, MA: Pearson/Allyn and Bacon.
*   Organista, P. B., Organista, K. C. & Kurasaki, K. (2003). The relationship between acculturation and ethnic minority health. In K. M. Chun, K. Kurasaki, P. B. Organista & M. Gerardo (Eds.), _Acculturation: Advances in theory, measurement and applied research_ (pp. 139-161). Washington, DC.: American Psychological Association.
*   Osatuyi, B. (2013). Information sharing on social media sites. _Computers in Human Behavior, 29_(6), 2622-2631.
*   Pettigrew, K. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behaviour among attendees at community clinics. _Information Processing & Management, 35_(6), 801-817.
*   Qayyum, M. A., Thompson, K. M., Kennan, M. A. & Lloyd, A. (2014). [The provision and sharing of information between service providers and settling refugees.](http://www.webcitation.org/6aiT5kfUn) _Information Research, 19_(2), paper 616\. Retrieved from http://www.informationr.net/ir/19-2/paper616.html (Archived by WebCite® at http://www.webcitation.org/6aiT5kfUn)
*   Ravindran, T., Kuan, Y., Chua, A. & Hoe Lian, D.G. (2014). Antecedents and effects of social network fatigue. _Journal of the Association for Information Science and Technology, 65_(11), 2306-2320.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of "way of life". _Library & Information Science Research, 17_(3), 259-294.
*   Savolainen, R. (2010). Everyday life information seeking. In M. J. Bates & M. N. Maack (Eds.), _Encyclopedia of library and information science._ (3rd ed., pp. 1780-1789). London: Taylor & Francis.
*   Shoham, S. & Strauss, S. K. (2008). [Immigrants' information needs: their role in the absorption process.](http://www.webcitation.org/6aiTC1SIz) _Information Research, 13_(4), paper 359\. Retrieved from http://informationr.net/ir/13-4/paper359.html (Archived by WebCite® at http://www.webcitation.org/6aiTC1SIz)
*   Silvio, D. H. (2006). The information needs and information seeking behaviour of immigrant southern Sudanese youth in the city of London, Ontario: an exploratory study. _Library Review, 55_(4), 259-266.
*   Sin, S. J. & Kim, K. S. (2013). International students' everyday life information seeking: the informational value of social networking sites. _Library & Information Science Research 35_, 107-116.
*   Sonnenwald, D. H. & Iivonen, M. (1999). An integrated human information behavior research framework for information studies. _Library & Information Science Research, 21_(4), 429-457.
*   Spink, A. & Cole, C. (2001). Introduction to the special issue: everyday life information-seeking research. _Library & Information Science Research, 23_(4), 301-304.
*   Srinivasan, R. & Pyati, A. (2007). Diasporic information environments: reframing immigrant-focused information research. _Journal of the American Society for Information Science and Technology, 58_(12), 1734-1744.
*   Strauss, A. & Corbin, J. (1998). _Basics of qualitative research: techniques and procedures for developing grounded theory._ Thousand Oaks. CA: Sage Publications.
*   Talja, S. & Hansen, P. (2006). Information sharing. In A. Spink & C. Cole (Eds.), _New directions in human information behavior_ (pp. 113-134). Berlin: Springer.
*   Tompkins, M., Smith, L., Jones, K. & Swindells, S. (2006). HIV education needs among Sudanese immigrants and refugees in the Midwestern United States. _AIDS and Behavior, 10_(3), 319-323.
*   Vertovec, S. (2004). _Trends and impacts of migrant transnationalism._ Oxford: University of Oxford: Centre on Migration, Policy & Society.
*   Wagner, J. (1979). _Images of information: still photography in the social sciences_. Beverly Hills, CA: Sage Publications.
*   Walsh, S. & Horenczyk, G. (2001). Gendered patterns of experience in social and cultural transition: the case of English-speaking immigrants in Israel. _Sex Roles, 45_(7), 501-528.
*   Wang, C. & Burris, M. A. (1994). Empowerment through photo novella: portraits of participation. _Health Education Quarterly, 21_(2), 171-186.
*   Wang, C. & Burris, M. A. (1997). Photovoice: concept, methodology and use for participatory needs assessment. _Health Education & Behavior, 24_(3), 369-387.
*   Weber, S. (2008). Using visual images in research. In J. G. Knowles & A. L. Cole (Eds.), _Handbook of the arts in qualitative research: perspectives, methodologies, examples and issues_ (pp. 41-54). London: Sage Publications.
*   Weiler, A. (2005). Information-seeking behavior in Generation Y students: motivation, critical thinking and learning theory. _The Journal of Academic Librarianship, 31_(1), 46-53.
*   Williamson, K. (1998). Discovered by chance: the role of incidental information acquisition in an ecological model of information use. _Library & Information Science Research, 20_(1), 23-40.
*   Wilson, T.D. (2000). Human information behavior. _Informing Science, 3_(2), 49-56