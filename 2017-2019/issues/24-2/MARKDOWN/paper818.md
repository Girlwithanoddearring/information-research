### vol. 24 no. 2, June, 2019

<article>

# Comparing born-digital artefacts using bibliographical archeology: a survey of Timothy Leary's published software (1985–1996)

## [James A. Hodges](#author)

> **Introduction.** This paper presents bibliographical archaeology as a method for comparing unique characteristics among many copies of the same computer software program. The process is demonstrated using celebrity psychologist Timothy Leary's Mind Mirror software as a case study.  
> **Method**. After retrieving a suitable corpus, data are examined for patterns, and emergent patterns are interpreted using historical inference. This approach builds on Dalbello-Lovric's case for bibliographical archaeology, expanding it to include new consideration for the unique qualities of born-digital artefacts.  
> **Analysis**. Each artefact is classified according to publication date and presence of supporting documentation, before introducing historical sources for additional context.  
> **Results**. Variations among copies of the same software artefact are found to proliferate well after the objects' initial date of publication. Bibliographical archaeology succeeds in highlighting and contextualising features of an artefact that were previously overlooked.  
> **Conclusions**. Findings support a media-archaeological view of born-digital artefacts, and bibliographical archaeology is shown to provide a programmatic approach in identifying significant archaeological characteristics among artefacts that have yet to be exhaustively studied.

<section>

## Introduction

Archaeological approaches to the study of born-digital cultural artefacts are growing in popularity within the interdisciplinary academic field of media studies, motivated by the need for materially centered and media-specific descriptive language ([Piccini 2015](#pic15)). At the same time, scholars working within the field of information science have made significant strides in recording, interpreting, and supporting the archaeological work of acquiring, classifying, analysing, and contextualising artefacts produced by both born-digital and non-digital cultures ([Dallas 2016](#dal16), [Huvila 2011](#huv11)).

Despite the significant achievements of scholars in both media archaeology and information science, existing research regarding the archaeological treatment of born-digital artefacts is limited in significant ways. While information science literature is established and growing within such subfields as managing born-digital archaeological data and the information practices of working archeologists, little attention within information science has been paid to the information practices of working media archaeologists or the potential to formalise media archaeologists' practices within a methodological programme that encompasses the preservation of unique material characteristics during the acquisition, classification, and interpretation of born-digital artefacts. Within the emergent field of media archaeology, on the other hand, authors such as Ernst ([2015](#ern15)) and Parikka ([2015](#par15)) have made significant contributions in the philosophical and critical evaluation of media materiality, historicity, and the epistemology of technically mediated memory. While such work contributes greatly to the overall development of archaeological approaches to born-digital artefacts, its tendency toward humanistic, expository, and narrative presentation makes it resistant to formalization within a reproducible research programme.

Despite both media archaeologists' resistance to programmatic formalization and their field's relative disconnection from information science, certain bibliographic approaches to media artefacts demonstrate the possibility of bridging disciplinary gaps between information science and media archaeology. Dalbello-Lovric's ([1999](#dal99)) bibliographical archaeology demonstrates the case for '_adjusting the principles of archaeological processing to bibliographic artefacts_' (p. 1) in order to understand textual ephemera as artefacts from fleeting and scarcely documented cultural contexts. Dalbello-Lovric's bibliographical archaeology is informed by Jean-Claude Gardin's ([1980](#gar80)) theory of archaeological reasoning, which holds in particular that formal logic can mediate between the compilation of archaeological facts and their subsequent explanation. Much as Dalbello-Lovric ([1999](#dal99)) repurposes Gardin's ([1980](#gar80)) approach to archaeology in order to apply it within a bibliographic context, this paper will repurpose Dalbello-Lovric's bibliographic archaeology in order to apply it within a born-digital context, thus demonstrating the analysis of born-digital artefacts using bibliographical archaeology.

By submitting born-digital artefacts to a bibliographic analysis, this paper will also build on the work of Kirschenbaum ([2002](#kir02)), who has shown that bibliographic approaches to born-digital media _'provide us with the intellectual precedents and critical tools'_ needed to understand digital objects as '_functions of the material and historical dimensions that obtain for all artefacts_' (p. 26). More recently, Kirschenbaum ([2008](#kir08)) has shown several ways that digital bibliography, enabled by tools and techniques developed within digital forensics, can reveal microscopic variations in the physical makeup of digital objects, much as computer-assisted forensic techniques now enable archaeologists to detect characteristics within non-digital artefacts that would be imperceptible to an unaided human practitioner.

Applying elements of Kirschenbaum's ([2002](#kir02); [2008](#kir08)) forensic approach within a study structured according to Dalbello-Lovric's ([1999](#dal99)) model of bibliographic archaeology, this paper will present and demonstrate an approach to classification and interpretation of born-digital artefacts using bibliographic archaeology, compiling and explaining the notable material characteristics of individual samples within a corpus comprising many slightly varied copies of a single computer program. Although findings revealed in the case study are likely of interest to software historians, this paper's broader purpose is its advocation in favor of techniques for seeking, retrieving, classifying, and interpreting media artefacts without preconceived theoretical or meta-historical findings in mind.

This paper's bibliographic and archaeological approach is selected in order to answer several overlapping calls for such methodologies in existing software preservation literature. For example, Winget and Sampson ([2011](#win11)) argue that traditional records produced and collected in the course of software's lifecycle, ranging from '_game design documents to email correspondence and business reports_' are an insufficient representation of '_the project or the game creation process as a whole_' (p. 29). In order to counteract this shortcoming, the authors advocate collecting '_numerous versions of games and game assets as well as those game assets that are natural byproducts of the design process like gamma and beta versions of the game_' ([ibid](#win11)). Similarly, Sköld's ([2018](#sko18)) review of software preservation literature identifies a '_recurring viewpoint_' among scholars concerning the '_incompatibility between the traditional archival concepts and the characteristics of videogames_,' (p. 140) and advances increased emphasis on empirical research as one possible way of developing appropriate new conceptual frameworks (p. 143). In light of such calls for empirically grounded accounts of variable formats in software preservation, this study demonstrates bibliographical archaeology as one such possible methodological approach.

### Background: Information science, media archaeology, and archaeology-as-such

Despite the lack of direct engagement within existing literature, many scholars working in both information science and media archaeology share similar concerns for physical artefacts' uniqueness and the complex evidentiary status of digital objects. In a special issue of the _Journal of Contemporary Archaeology_ devoted to the topic of media archaeology, Piccini ([2015](#pic15)) defines media archaeology as a '_material methodology that enables investigation of the cultural layers of technology_' (p. 6). Within the same volume, Parikka ([2015](#par15)) emphasises that viewing media artefacts archaeologically must acknowledge that their historical and cultural contexts are '_not merely reflected but actively produced_' during processes of gathering, classification, interpretation, and description (p. 13). These assertions, common within the field of media archaeology, are generally informed by Foucault's ([1972](#fou72)) _Archaeology of Knowledge_, wherein the author advocates interpreting historical texts and archival materials using techniques of '_description that questions the already-said at the level of its existence_' (p. 131). In Foucault's conception of archaeological analysis, comparison between artefacts '_does not have a unifying, but a diversifying, effect_' (p. 160). This diversifying effect is the result of analysis that emphasises the layers of association, context, and variation that accumulate during an object's passage through time and space.

Although operating within an information science context, Dalbello-Lovric's ([1999](#dal99)) study of Croatian diaspora almanacs also meets Parikka's ([2015](#par15)) call for critical engagements with the production of historical narrative; it does so by quantifying observable characteristics within a corpus of interrelated mass-produced texts and postponing historical inference until after preliminarily enumerating notable characteristics. Structuring the study within distinct phases of acquisition, classification, pattern recognition, and historical reference, Dalbello-Lovric describes the approach as a '_strategy for recognising a body of printed artefacts meaningfully related to their social context_' (p. 15). First, materials are acquired. Second, '_the recovered materials are inventoried, and descriptive data about the artefacts are compiled_' (p. 2). Third, artefacts are classified within '_typological series that reveal underlying patterns in the data_' (p. 2). Finally, the underlying patterns are contextualised by referencing '_independent historical evidence_' in order to '_formulate hypotheses about the events and functions related to the formation of the artefacts_' (p. 2).

Dalbello-Lovric's ([1999](#dal99)) bibliographical archaeology is largely informed by the work of Jean-Claude Gardin ([1980](#gar80)), whose theoretical conceptions of archaeology advocate that scholars '_acknowledge the interdependence between data constitution on the one hand and scholarly argumentation on the other and, thus, implicitly, the futility of attempting to publish one in separation from the other_' ([Dallas 2016](#dal16), p. 317). In other words, Gardin argues that the processes of explanation and classification are inherently linked, and that deep engagement with both sides of the overall interpretive process requires analysis to begin with a critical approach to classification and description practices' potential to harbor unexamined historical, material, or narrative assumptions. While Gardin's work is frequently disparaged as '_prescriptive, positivist dogma_' in Anglophone archaeological communities ([Dallas 2016](#dal16), p. 306), his skepticism regarding received classification schemes and popular narrative assumptions shares much in common with media archaeology's concern for the '_material relations and regularities that enable a 'thing' to come into existence as a thing_' ([Bollmer 2015](#bol15), p. 68). By treating software artefacts archaeologically, this study will demonstrate the identification of meaningful properties according to the specific institutional and historical factors unique to any given artefact. Rather than accepting the contextual and formalised properties of a single edition or archival copy of the work in question, this study will focus on techniques for surveying and interpreting the breadth of historical possibilities which guide the ongoing construction of narrative accounts concerning an object's material uniqueness and historical significance.

</section>

<section>

## The Case: Varied Editions of _Timothy Leary's Mind Mirror_ (1986)

In order to demonstrate the utility of an archaeological approach to the bibliographic analysis of born-digital artefacts, this paper will undertake a case study on the numerous extant copies of the 1986 Electronic Arts home computer program _Timothy Leary's Mind Mirror_ presently available within both institutional and unauthorised archives, labs, and repositories_._ By focusing on the variations within a corpus composed of copies of the same digital text, the case study will highlight the ways that media artefacts resist linear narrative explanation and unified temporal characterizations. In this sense, the corpus of _Mind Mirror_ files selected for analysis provides an example of the ways that cultural artefacts accumulate multiple overlapping layers of variation during their unique paths from creation to analysis. By emphasising such characteristics, I hope to connect the case study to a broader project, advocated by archival scholars including Sköld ([2018](#sko18)), as well as Winget and Sampson ([2011](#win11)), of developing empirically grounded conceptual frameworks regarding the ontological status of computer software.

Designed by celebrity psychologist Timothy Leary, _Mind Mirror_ was first published by Electronic Arts for IBM, Commodore, and Apple home computers between 1985 and 1986\. The program allows users to role-play a variety of text-based scenarios in an interactive psychological therapy session, culminating in a series of charts which evaluate users' personality traits according to classification techniques established in Leary's 1950 PhD thesis ([Hodges 2016](#hod16)). Although Leary is perhaps most famous for his work advocating the use of psychedelic drugs to induce religious experiences while employed as a lecturer at Harvard University in the early 1960s, he began promoting computers as a newer, safer tool for self-examination during the 1980s ([Devonis 2012](#dev12), p. 20). During the 1980s and 1990s, Leary developed over a dozen interrelated digital software projects, including collaborations with the rock band DEVO, science fiction author William Gibson, artist Keith Haring, future Massachusetts Institute of Technology Media Lab chair Joi Ito, and software designer Brenda Laurel.

As a result of Leary's celebrity affiliations, copies of _Mind Mirror_ remain widely available today, both through the efforts of unauthorised enthusiast distribution efforts as well as through the collecting and preservation work of numerous institutional collections related to twentieth-century American culture, historical literature, and history of computing. The New York Public Library purchased most of Leary's personal archives and manuscripts in 2011, while Leary's vibrant cult following within computer hobbyist circles led to unauthorised and sometimes altered copies of the program being disseminated widely online. At the same time, physical copies of the program were frequently traded among collectors, and commercially produced copies have found their way into institutional collections as well.

As an artefact produced within the context of countercultural computing in the 1980s United States, _Mind Mirror_ could easily be interpreted according to the popular narratives of disruption and innovation prevalent within both popular and academic computer history literature. Against this possibility, Dalbello-Lovric ([1999](#dal99)) contends that such popular-culture topics stand to benefit greatly from bibliographic and archaeological analysis '_because bibliographical inference helps the researcher to be disengaged from the imposed discursive frameworks for interpretation_' ([Dalbello-Lovric 1999](#dal99), p. 15). Today, the various copies of _Mind Mirror_ preserved within both institutional and unauthorised settings warrant sustained bibliographical analysis because they are digital objects which ostensibly contain the same content, yet reveal themselves to possess unique variations when subjected to bibliographic analysis and treated as media-archaeological artefacts. By observing and enumerating the unique characteristics of each instance of the program, this study aims to understand the ways that the program's varied paths through history attest to the unique patterns of preservation that bring popular media artefacts from recent historical eras into contemporary availability ([Dalbello-Lovric 1999](#dal99), p. 15). For computer software, these patterns of preservation have often entailed such a paucity of secondary documentation that '_it is difficult to quantify the level of loss_' already affecting available resources ([Gooding and Terras 2008](#goo08), p. 33). This study aims to begin compensating for such a loss of knowledge by demonstrating techniques for evaluating the variable nature of existing records in a wide variety of settings and collections.

### Method

For the rest of this paper, I will be examining the formal specificities of _Mind Mirror_ as it appears in each of the identified settings, in order to enumerate and interpret the physical variations among surviving copies of the program. This case study will be structured similarly to Dalbello-Lovric's ([1999](#dal99)) bibliographic archaeology, which is in turn organised around the four phases of research outlined in Gardin's ([1980](#gar80)) model of archaeological processing. By transposing Gardin's archaeological approach into the field of enumerative bibliography, Dalbello-Lovric makes a case for the value of employing archaeological perspectives in the study of print culture from relatively recent historical periods. This study applies a similar structure to the analysis of born-digital artefacts, and takes place in four phases: retrieval of samples, classification of samples, pattern recognition among classified samples, and interpretation using inference from historical literature. Much as Dalbello-Lovric proposes bibliographical archaeology as a technique for understanding the relatively fragmentary print records of diaspora communities, this study applies similar methods in order to understand software artefacts that have proliferated in formats and settings well beyond those recorded in the limited existing documentation.

#### Retrieval

The mass production of born-digital cultural artefacts is a relatively recent historical phenomenon, beginning only with the advent and popularization of digital computing and media technologies in the second half of the twentieth century. Preservation of such artefacts within memory institutions such as libraries, archives, and museums is an even more recent phenomenon, and thus many objects and records produced during the early decades of born-digital cultural production have to date been inconsistently or incompletely preserved and documented. The rampant unlicensed copying of early commercial software makes it especially difficult to reliably estimate the popularity of any given artefact, or the significance of a given artefact's presence within a given collection or institution ([Graft 1984-5](#gra84), p. 993). With this background knowledge in mind, the retrieval of sources for my study was guided by a theoretical sampling approach, evaluating the need for additional sources based on findings from the data collected during previous rounds of sampling. In other words, data from each round of research was analysed before continuing, and then used to guide subsequent rounds of retrieval. In this way, my sampling methods remained '_responsive to the data rather than established before the research begins_' ([Corbin and Strauss 2015](#cor15), p. 144). This theoretical sampling approach was selected in order to provide a flexible and agile approach to retrieving materials that have been scattered throughout several geographically and institutionally disparate locations.

According to its practitioners, theoretical sampling is a useful strategy for scholars who wish to '_appropriately explore underrepresented areas within the sample_,' because '_researchers do not begin with a theory but develop one as relevant information emerges_'([Yingling and McClain 2015](#yin15), p. 3). This approach is particularly useful '_when little information is known about your phenomenon of study_' (p. 14). Since game preservation literature widely acknowledges the incomplete nature of existing records and documentation concerning historical software ([Gooding and Terras 2008](#goo08), p. 33), I contend that a theoretical sampling approach stands to provide preservation scholarship with a much-needed combination of empirical grounding and conceptual flexibility.

Beginning with an analysis of records located within the Timothy Leary Papers at New York Public Library (NYPL), I identified, located, and retrieved available resources concerning copies of _Mind Mirror_ housed within five different institutions, comprising fourteen copies of _Mind Mirror_ in total. The persons and institutions responsible for preserving the identified copies of _Mind Mirror_ include universities (one copy at a public university and four copies in a private university), a public library (five copies), a nonprofit digital preservation website (two copies), and an unauthorised vintage software enthusiast website (two copies). The provenance of these items varies, with nine of the fourteen copies emerging from within the personal libraries and archives of historical figures, while three survive today thanks to the distribution efforts of unauthorised enthusiasts. Finally, one copy was purchased on the secondary market for research purposes at an interdisciplinary academic lab, while another lone copy, retrieved from the aforementioned enthusiast-led website, arrived without any additional provenance data.

Analysis following each phase of source retrieval comprised comparison of available bibliographic metadata for all identified sources, comparison of associated supporting documentation when applicable, execution of binary digital files when applicable, and analysis of ASCII data present in digital files using a hex editor when applicable. Following each round of analysis, categories of variation between editions of the software were compared and expanded. Early iterations of this process frequently revealed references to additional formats and editions of the software, which informed the pursuit of subsequent copies. Retrieval of sources ended upon achieving saturation, defined as a condition in which all '_major categories show depth and variation_' ([Corbin and Strauss 2015](#cor15), p. 144). This condition was met upon retrieving copies of the program representing all of the formats and features discussed within documents accompanying or referenced within the _Mind Mirror_ development materials in Leary's archive.

Locating extant copies of _Mind Mirror_ began with a brief survey of fourteen subject headings related to software development and digital archives within the Timothy Leary Papers finding aid at New York Public Library. Records within the initial 14 subject headings included references to seven additional subject headings pertaining directly to the _Mind Mirror_ software, as well as one additional volume of disk images. At this time, I located five copies of the _Mind Mirror_ software classified within a series of records entitled '_Development Disks_.'

In order to place findings from these sources in proper context, I also reviewed several news reports concerning the New York Public Library's purchase of Leary's archives in 2011\. These sources include news reports from specialist publications such as _Public Libraries_ ([Keller 2011](#kel11), July-August), as well as newspapers including the _New York Times_ and _Daily News_ ([Schuessler 2013](#sch13), 27 September; [Associated Press 2013](#ass13), 18 September). Based on findings from these sources, I located the _Timothy Leary Archives_ website and blog, operated by representatives of Leary's trust, using the [ICANN WHOIS](https://whois.icann.org/en) locater. From the trust's website, I located additional records and artefacts produced during Leary's life, housed at the Internet Archive as well as within the Ludlow-Santo Domingo Library collection at Harvard University's Houghton Library ([Internet Archive, n.d.](#intnd); [Rein 2012](#rei12), 12 November).

During a review of the data from previous steps in the retrieval of sources, I located correspondence between Leary and his publisher, located in Leary's archive, discussing the inclusion of copy protection technology within commercial copies of the program ([Leary to Bonn 1985](#lea85), 7 November). In order to investigate the possible presence of both copy-protection and unauthorised duplication among extant copies of _Mind Mirror_, I procured two unauthorised copies of the program from a popular vintage software enthusiast website _My Abandonware_. Finally, upon presenting my preliminary findings to colleagues within the software preservation community, I was offered the opportunity to examine a previously unopened commercial copy of _Mind Mirror_ on loan from the Media Archaeology Lab at University of Colorado-Boulder (see [Hodges 2016](#hod16)). Examining both unauthorised copies of the program and an unaltered commercial copy of the program revealed evidence of both copy protection as well as user-produced subversions of the aforementioned protections, thus achieving saturation of all identified categories. After identifying and retrieving the sources, I proceeded with analysis by comparing available bibliographic information, included documentation, and digital files (as applicable) present in each instance of the program.

</section>

<section>

#### Classification

During the retrieval phase of research, I located fourteen copies of _Mind Mirror,_ hosted by a variety of institutions. Depending on the state of digital preservation strategy, policy, and implementation within each copy's hosting institution, not all copies of the _Mind Mirror_ program were accessible using the same hardware or software tools. While this uneven distribution of accessibility prevents comparison of file performance during execution of binary files across all identified samples, it does not affect the scholar's ability to compare a number of bibliographic characteristics including date published and the presence of supporting documentation. In keeping with this study's goal of demonstrating iterative and flexible interpretive practices, this section of the current paper will address classification according only to shared observable characteristics revealed during the initial retrieval phase of research.

Following retrieval, the next step of bibliographic archaeological analysis as described by Dalbello-Lovric ([1999](#dal99)) consists of classification. During this phase of research, the goal is to establish a typology based on the '_ordering of objects according to predetermined characteristics_' (p. 8). In order to protect against the over-determining influence of popular historical narratives, conjectures regarding the possible significance of features will be avoided during this phase of research. This decision is consistent with Dalbello-Lovric's ([1999](#dal99)) concept of bibliographical archaeology, as well as with the precepts of media archaeology as practiced by the likes of Ernst and Parikka ([Ernst 2015](#ern15); [Parikka 2015](#par15)). The goal of this step in the analysis is to establish a framework for classification that encompasses the substantial variation among objects, while also facilitating comparison according to characteristics that are shared across the objects.

Due to each copy of _Mind Mirror_'s unique physical format and institutional location, many of the samples' characteristics cannot be directly compared with one another. For example, copies of _Mind Mirror_ housed at the Internet Archive are accessible via remote access only, using emulation as a service to run disk image files in the user's web browser. Access to the disk image files via any other software is restricted, which prevents researchers from examining the binary files with any additional software tools. The Internet Archive's metadata also does not disclose the provenance or previous physical format of their disk images. In other settings, however, _Mind Mirror_ has been found in both one-disk and two-disk formats. Harvard University's Houghton Library, for example, possesses IBM-compatible copies of the program comprising both one and two disks. Such differences in cataloging practices and material format attest to the wide degree of variation present within multiple copies of the same born-digital work, as well as to the inconsistent treatment of such works within various collecting institutions. In order to retain focus on methods for evaluating historical artefacts themselves, however, the classification phase of this study will address individual variations among artefacts, without attempting to link such traits to institutional policies. This process will begin with classification according to variation within categories that are comparable across all samples.

In order to develop a classification scheme and typology that is applicable to all of the identified copies of _Mind Mirror_, I have chosen two classifiable traits that can be observed equally across all samples. These characteristics are date of publication and presence of supplementary materials.

First, each copy of _Mind Mirror_ identified during the retrieval phase of this study possesses a date published, whether conveyed within a hand-written adhesive label, commercially produced packaging, or within its associated digital metadata. In two cases, copies of _Mind Mirror_ were classified within a range of dates, rather than a single year. Contained within designer Timothy Leary's personal archive and classified by the processing archivists, these two copies of the program are classified within a range of dates spanning from 1985 to 1996, based on the years between Leary's earliest work designing software and his death in 1996\. Although imprecise, these dates still provide an account of the disks' general date of origin.

In addition to possessing a publication date, each copy of _Mind Mirror_ also possesses some form of supporting documentation, whether it takes the form of a commercially distributed instruction manual, a hand-written note from the developer, or a digital record affixed to the files long after their original production. Together, the artefacts' publication dates and forms of supporting documentation constitute an appropriate classification scheme given the fragmentary nature of existing knowledge concerning _Mind Mirror._ Though it represents a very limited breadth of categories, this classification strategy is in keeping with the iterative and non-deterministic values espoused by bibliographical archaeology and media archaeology alike. In the next phase of the study, I will build from data highlighted in this classification scheme to begin identifying patterns within my corpus of born-digital artefacts.

</section>

<section>

#### Pattern Recognition

During the previous phase of this study, I classified each copy of _Mind Mirror_ according to two variables: the year of publication, and the presence of supporting documentation. Using the year of publication, researchers can identify patterns related to the original production and distribution life cycle of a born-digital artefact. Supporting documentation, on the other hand, can be altered, affixed, or removed from an artefact continuously during its subsequent lifespan. As a result, the presence of various supplementary materials can more clearly attest to the effects of multiple users and distribution techniques encountered during an object's path from original production to its eventual preservation within a historical collection. Additionally, the possibility to affix, alter or remove supplementary materials from an object at various points during its life cycle leads to very high proliferation of variation. In many cases, a single copy of the _Mind Mirror_ program contains multiple forms of supporting documentation. Thus, the number of possible paratextual configurations accompanying the work greatly exceeds the number of copies that were examined.

The distribution of _Mind Mirror_ copies according to their original publishing dates and their forms of associated supporting documentation are presented in Tables 1 and 2, respectively.

<table class="center" style="width:50%;"><caption>  
Table 1: Date of Publication</caption>

<thead>

<tr>

<th>Year of Publication</th>

<th>Number of Copies</th>

</tr>

</thead>

<tbody>

<tr>

<td>1985</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td>1986</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td>1988</td>

<td style="text-align:center;">5</td>

</tr>

<tr>

<td>1990</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>1985-1996</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>Total copies</td>

<td style="text-align:center;">14</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table 2\. Presence and Format of Supporting Documentation</caption>

<thead>

<tr>

<th>Supplemental Material</th>

<th>Number of Copies</th>

</tr>

</thead>

<tbody>

<tr>

<td>Commercially printed paper instruction manual</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Digital files containing instructions for copying game files, included alongside executable files on program disk</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td>Publisher's warranty card</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td>Publisher's warranty reply card</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td>Digital text files containing support documentation for IBM-DOS included alongside executable files on program disk</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>Commercially printed records modified with hand annotations</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>Digital text files containing user-generated background/historical information contained alongside executable files in folder containing disk image files</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>Photocopy or scan of commercially printed instruction manual</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>Publisher's contest entry form</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>Digital inscriptions inserted within binary files by previous users claiming responsibility for cracking copy protection software</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>Digital file containing previously saved game contained alongside executable files on program disk</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>Corrupted digital readme file contained alongside executable files on program disk</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>Incorrect hand-written label affixed to floppy disk</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td colspan="2">_Note: Many individual copies of the program contain more than one form of documentation._</td>

</tr>

</tbody>

</table>

The data collected present a number of insights regarding the patterns of production, copying and distribution responsible for guiding _Mind Mirror_ to its present forms and settings. By collecting, classifying, and identifying patterns within a corpus of _Mind Mirror_ copies, I hope to demonstrate the potential for bibliographical archaeology to identify meaningful characteristics among many forms of digital media artefacts for which relatively little secondary scholarship or official documentation is currently available.

First, while most copies of the program list copyright dates of 1985 and 1986 on their title screens, there are just as many copies that attest to later publication dates on their labels or other associated records, and records associated with one copy even list a date of 1990\. Additionally, three of the five floppy disks containing copies of the program within Leary's personal archive at New York Public Library bear machine-printed publication dates reading 1988\. The two remaining Mind Mirror disks in Leary's archive possess handwritten labels without years, and have been classified by archivists within the range of years spanning from 1985 to 1996\. In total, 6 of the fourteen samples examined (42.9%) bear dates of publication which specifically reference years occurring after the 1985 and 1986 publication dates listed on both the programs' title screens and the annotated bibliography produced by Leary's original archivist ([Horowitz et al 1988](#hor88), p. 82). In terms of publication date, these findings suggest that _Mind Mirror_ did not cease production following its initial publication between 1985 and 1986\. Instead, copies of the program continued to proliferate, accumulating variations in the process. All of the copies' publication dates are listed in Table 1.

The supporting documentation and paratextual materials accompanying copies of _Mind Mirror_ display an even greater level of variability than the programs' publication dates. Displayed in Table 2, thirteen different categories of supplemental materials appeared within the corpus of born-digital artefacts.

The most common form of supplemental material is instructional in nature. Overall, six of the fourteen identified copies of _Mind Mirror_ currently possess commercially produced instruction manuals. Seven of the copies examined in this study contain some form of digital text file alongside the program's binary files. Thus while many instances of the publisher's official documentation have been discarded over time, new forms of supporting texts have been produced at a similar rate.

Many of the supporting documents have been produced well after the program was initially published, meaning that variations among editions of a digital artefact can continue to diversify well after their initial production. For example, one of the copies acquired from vintage software enthusiast website _My Abandonware_ arrives packaged with a readme.txt file reading, in full, '_Mind Mirror by Electronic Arts / For saving/loading select DRIVE *B*_' ([Leary 1986a](#lea86a)). This copy also includes a note inserted within the program's binary files, readable using a hex editor, containing following text: '_Dos driver by Mok <mokmok@usa.net>_.' Although the program title screen and _My Abandonware_ metadata both attest to a 1986 publication date, domain registration records, revealed by the [ICANN WHOIS](https://whois.icann.org/en) locater, suggest that the e-mail address provided could not have existed before 1998\. Similarly, another copy of the program procured from _My Abandonware_ includes a file named version.nfo, which contains the text '_Designed by THE Timothy Leary (The 'High Priest' of LSD who died on the net in 1996)_' ([Leary 1986b](#lea86b)). Since it references Leary's 1996 death, this text also appears to have been produced well after the program's initial publication. With this fact in mind, the paratextual content accompanying this particular copy of the _Mind Mirror_ program appears to have been added several years after initial publication. This finding brings into question the reliability of publication dates as reported by other sources of metadata-- a point that should be taken into consideration whenever processing or conducting research with born-digital historical materials. Bibliographical archaeology assists in highlighting such findings by treating each artefact as one unique instance among many other similar artefacts, rather than fixating on its role within a single collection or system of records.

The findings discussed above, relating to diversity in terms of publication date and form of supplementary materials, affirm common media-archaeological arguments regarding nonlinear accumulation and diversification of media artefacts' unique characteristics. Much as Emerson ([2014](#eme14)) advocates comparison between artefacts from multiple points of temporal origin in order to work '_against the teleological narrative of technological improvement_' (p. 130), and Parikka ([2007](#par07)) argues that anomalous artefacts should be understood as '_hypertrophical of the “normal” functioning of a technological machine_' (p. 5), the _Mind Mirror_ disks associated with publication dates that fall outside of commonly cited time periods show that the development, publication, and distribution of computer software progress in uneven and ongoing processes that appear more clearly when a comparative bibliographical approach is employed to identify and quantify their varied manifestations over time.

</section>

<section>

#### Historical Inference

In a general sense, the patterns identified in data above show that born-digital artefacts such as computer software can possess considerable variation, even when they represent multiple instances of the same mass-produced item. On their own, however, these findings provide only limited specific historical explanation for the source or significance of such variations. By introducing data collected from a review of historical literature, I will now provide historical inference regarding the significance of characteristics observed within the corpus of _Mind Mirror_ copies that were retrieved and classified during earlier phases of the research. Relevant historical sources for this particular study include literature concerning Timothy Leary's biography and career, as well as the histories of 20th-century computing and countercultural social movements within which Leary participated. While the findings discussed in this section are largely descriptive in nature, they are presented in order to demonstrate the role of historical inference in connecting material characteristics with plausible interpretation during bibliographical archaeology research with artefacts about which relatively little is known.

A review of secondary literature concerning Leary himself suggests that Leary's long career and outsise public reputation have made _Mind Mirror_ more likely than other historically contemporaneous software programs to receive attention from preservationists and enthusiasts. Leary's many celebrity affiliations are significant in this regard. For example, in a 1986 interview with _Guide to Computer Living_ magazine, Leary discusses tentative upcoming collaborative works with a variety of well-known contemporary cultural figures including science fiction author William Gibson and artist Keith Haring, as well as historical figures such as the writer William S. Burroughs ([Chase 1986](#cha86), p. 15). Although never completed, early versions of these works do exist within Leary's archive ([Schuessler 2013](#sch13), [Dietrich et al 2016](#die16)). After his death in 1996, Leary's _New York Times_ obituary listed several icons of the 1960s among Leary's associates as well, including Marshall McLuhan, Yoko Ono, and John Lennon ([Mansnerus 1996](#man96)). Rather than fading from the spotlight in his later years, Leary continued to pursue contemporary projects and affiliations, including both his many software projects and a number of public appearances, including an on-stage appearance at the Lollapalooza alternative music festival in 1993 ([Greenfield 2006](#gre06), p. 571). Within academic histories, Leary has been called '_one of the most visible psychologists in America between 1960 and 1990_' ([Devonis 2012](#dev12), p. 16), and '_a progenitor of today's transhumanist movement_' ([McCray 2016](#mcc16), p. 240). These reputation-related factors have undoubtedly contributed to an elevated interest in Leary's _Mind Mirror_ software between its original publication and today. This elevated interest, in turn, helps to explain the proliferation of user-created paratexts accompanying many of the copies of _Mind Mirror_ identified for examination in this study.

While Leary's celebrity status helps to explain the widespread interest in collecting and preserving copies of _Mind Mirror_, it does not completely account for the widespread variations, modifications, and alterations noted by the present study. At various points during _Mind Mirror_'s life cycle, individual users outside of Leary's own circles appear to have created new textual accompaniments for distribution both alongside and within _Mind Mirror_ files. A review of historical literature concerning computer culture in the 1980s suggests that these alterations are the result of _Mind Mirror_'s embedded presence within a vibrant scene of unauthorised distribution and modification by enthusiasts. According to Reunanen, Wasiak, and Botz ([2015](#reu15)), the skillful modification of program files in order to subvert copy protection software was a source of great pride among hackers within a particular warez scene concerned with the unauthorised distribution of commercial software in the 1980s and 1990s (p. 799). The warez scene hackers frequently inserted new title screens and other forms of textual attribution for their work in making commercial software freely available to other scene participants ([Rehn 2004](#reh04), p. 362). Such alterations can be seen in both of the copies of _Mind Mirror_ retrieved from _My Abandonware_ ([Leary 1986a](#lea86a), [1986b](#lea86b)).

Based on the above review of secondary literature, widespread interest in Timothy Leary as a cultural figure appears significant in the continued survival of numerous editions of _Mind Mirror_. Enthusiast communities' propensity to produce their own forms of secondary documentation also affect processes of copying and sharing the program. Based on sampling a broad variety of similar artefacts, these findings show that bibliographical archaeology succeeds in identifying and understanding variations and idiosyncrasies among artefacts that would not necessarily appear significant when the same artefacts are examined in isolation.

</section>

<section>

## Conclusion

Timothy Leary's _Mind Mirror_ software, like most born-digital artefacts, survives today because of its ability to be copied, rather than its uniqueness as a unified singular artefact. Nevertheless, each copy of the program retains unique bibliographic qualities. Taking an archaeological approach to the enumeration of such bibliographic characteristics highlights the breadth of variation within a corpus composed of artefacts that purport to be the same. In order to demonstrate the utility of a bibliographic archaeology approach to processing and interpreting born-digital sources, this paper has discussed the retrieval of a corpus, classification of the patterns and data collected, recognition of patterns within the classified data, and interpretation of findings using historical inference. This approach brings together the philosophical and theoretical underpinnings of humanistic media archaeology with the programmatic structure of Jean-Claude Gardin's ([1980](#gar80)) conception of archaeology. By connecting these two intellectual paths, I hope to demonstrate the possibility of a more robust exchange between media archaeology and its disciplinary neighbors, including archaeology-as-such and information science.

The case study addressed in this paper has also introduced the topic of Timothy Leary's various software projects for more sustained consideration in future research. Although he remains more well-known for his written works and public appearances, Leary's celebrity status and close engagement with the psychological and technological trends of his era make his born-digital works relatively widely collected, despite their lack of comprehensive and authoritative supporting documentation. The current study is limited in terms of scope by its focus on a single software work, as well as the relatively small number of copies of the work that could be retrieved. Nevertheless, even this relatively limited demonstration has shown that bibliographical archaeology of born-digital artefacts can produce useful insights about digital files' overlapping and nonlinear paths through time and space. Such findings are useful for their ability to highlight objects' unique material characteristics during both archival processing and historical research, thus counteracting over-determination of interpretive research by popular historical narratives.

</section>

<section>

## Acknowledgements

The author would like to thank the special issue editors for creating the occasion to present such research. Additionally, he would like to acknowledge that this article has been greatly improved thanks to the generous and helpful commentary from two anonymous reviewers.

## About the author

**James A. Hodges** is a Ph.D. Candidate in the School of Communication and Information at Rutgers University. His research interests include software preservation and genealogies of interdisciplinary computing. Before beginning doctoral studies, James earned an M.A. From New York University's department of Media, Culture, and Communication. He can be contacted at: james.hodges@rutgers.edu

</section>

<section class="refs">

## References

*   Associated Press (2013, 18 September) [What a trip: Timothy Leary's files made available at New York Public Library](http://www.webcitation.org/71InPi1DD). _Daily News_. Retrieved fromhttp://www.nydailynews.com/news/national/trip-timothy-leary-files-made-public-nyc-library-article-1.1459435\. (Archived by WebCite® at http://www.webcitation.org/71InPi1DD).
*   Bollmer, G. (2015) Fragile storage, digital futures. _Journal of Contemporary Archaeology, 2_(1), 61-72.
*   Chase, R. (1986, December) Interview with Dr. Timothy Leary. _Guide to Computer Living for_ _Commodore Owners, 3_(8) 6-16.
*   Corbin, J. & Strauss, A. (2015). _Basics of qualitative research: techniques and procedures for developing grounded theory_. 4th ed., Thousand Oaks, CA: Sage Publications.
*   Dalbello-Lovric, M. (1999). The case for bibliographical archeology. _Analytical & Enumerative Bibliography, 10_(1), 1–20.
*   Dallas, C. (2016). Jean-Claude Gardin on archaeological data, representation and knowledge: implications for digital archaeology. _Journal of Archaeological Method and Theory, 23_(1), 305–330.
*   Devonis, D.C. (2012). Timothy Leary's mid-career shift: clean break or inflection point? _Journal of the History of the Behavioral Sciences, 48_(1): 16-39.
*   Dietrich, D., Kim, J., McKeehan, M. & Rhonemus, A. (2016). [How to party like it's 1999: emulation for everyone](http://www.webcitation.org/77MVcwhS4). _Code4Lib Journal_, (No. 32). Retrieved from https://journal.code4lib.org/articles/11386 (Archived by WebCite® at http://www.webcitation.org/77MVcwhS4)
*   Emerson, L. (2014). _Reading writing interfaces: from the digital to the bookbound_. Minneapolis, MN: University of Minnesota Press.
*   Ernst, W. (2015) Media archaeology-as-such: occasional thoughts on (més-)alliances with archaeologies. _Journal of Contemporary Archaeology 2_(1), 15-23.
*   Foucault, M. (1972). _The archaeology of knowledge_ (A. M. Sheridan Smith, Trans.) New York, NY: Pantheon Books.
*   Gardin, J. C. (1980). _Archaeological constructs: an aspect of theoretical archaeology_. New York, NY: Cambridge University Press.
*   Gooding, P. & Terras, M. (2008). 'Grand theft archive': a quantitative analysis of the state of computer game preservation. _International Journal of Digital Curation, 3_(1), 19–41.
*   Graft, W.C. (1985). Combating software piracy: a statutory proposal to strengthen software copyright. _DePaul Law Review, 34_(4), 993-1031.
*   Greenfield, R. (2006). _Timothy Leary: a biography_. Boston, MA: Houghton Mifflin Harcourt.
*   Hodges, J.A. (2016). [MALware technical report: Timothy Leary's Mind Mirror](http://www.webcitation.org/71KBrHTBj). Boulder, CO: University of Colorado at Boulder Media Archaeology Lab. Retrieved from http://mediaarchaeologylab.com/wp-content/uploads/2016/10/JamesHodges_MindMirror_MALwareTechnicalReport_10-2016-1.pdf. (Archived by WebCite® at http://www.webcitation.org/71KBrHTBj).
*   Horowitz, M., Walls, K. and Smith, B. (1988) _An annotated bibliography of Timothy Leary._ Hamden, CT: Archon Books.
*   Huvila, I. (2011). The politics of boundary objects: hegemonic interventions and the making of a document. _Journal of the American Society for information Science and Technology, 62_(12), 2528-2539.
*   Internet Archive. (n.d.). _[Timothy Leary archive: videos](http://www.webcitation.org/77McgQOqI)_. The Internet Archive. Retrieved from https://archive.org/details/Tim_Leary_Archive? (Archived by WebCite® at http://www.webcitation.org/77McgQOqI)
*   Keller, J.A. (2011). NYPL acquires Timothy Leary papers. _Public Libraries_(July-August), 15-16.
*   Kirschenbaum, M.G. (2002). Editing the interface: textual studies and first generation electronic objects. _Text, 14_(1), 15-51.
*   Kirschenbaum, M.G. (2008). _Mechanisms: new media and the forensic imagination_. Cambridge, MA: MIT Press.
*   Leary, T.L. (1985, 7 November). _Letter to Stewart Bonn of Electronic Arts_. Timothy Leary Papers, New York Public Library Rare Books and Manuscripts Division. New York, NY. MssCol 18400, box 241, folder 1.
*   Leary, T.L. (1985a). _Timothy Leary's Mind Mirror_ (one 5.25” floppy disk with warranty card and contest entry form). Julio Mario Santo Domingo Collection, Harvard University Houghton Library. Cambridge, MA. Barcode 5715341.
*   Leary, T.L. (1985b). [_Timothy Leary's Mind Mirror (4am and san inc crack)_](http://www.webcitation.org/71J4GY2JS) (Commodore 64 edition cracked by 4am and san inc, accessible via web using emulation as a service). Internet Archive Apple II Library: The 4am Collection. Retrieved from https://archive.org/details/MindMirror4amCrack. (Archived by WebCite® at http://www.webcitation.org/71J4GY2JS)
*   Leary, T.L. (1985c). [_Timothy Leary's Mind Mirror_](http://www.webcitation.org/76ABp5kg3) (Commodore 64-compatible disk image files with version.nfo text and PirateBusters attribution present in binary files). My Abandonware. Retrieved from https://www.myabandonware.com/game/timothy-learys-mind-mirror-67#Commodore%2064\. (Archived by WebCite® at http://www.webcitation.org/76ABp5kg3)
*   Leary, T.L. (1986a). [_Timothy Leary's Mind Mirror_](http://www.webcitation.org/71J1ZtLjk) (IBM-DOS compatible disk image files with “DOS driver by Mok” text present in binary files). My Abandonware. Retrieved from https://www.myabandonware.com/game/timothy-learys-mind-mirror-67\. (Archived by WebCite® at http://www.webcitation.org/71J1ZtLjk).
*   Leary, T.L. (1986b). [_Timothy Leary's Mind Mirror_](http://www.webcitation.org/71J1ZtLjk) (Commodore 64-compatible disk image files with “version.nfo file” text referencing Leary's death). My Abandonware. Retrieved from[https://www.myabandonware.com/game/timothy-learys-mind-mirror-67#Commodore%2064](https://www.myabandonware.com/game/timothy-learys-mind-mirror-67#Commodore%2064). (Archived by WebCite® at http://www.webcitation.org/71J1ZtLjk)
*   Leary, T.L. (1986c). _Timothy Leary's Mind Mirror_ (two Commodore 64-compatible floppy disks with commercially printed instruction manual, publisher's limited warranty card and warranty reply card). San Mateo, CA: Electronic Arts.
*   Leary, T.L. (1986-1996a). _Timothy Leary's Mind Mirror_ (embedded within _Mind Adventure_ software, one 5.25” floppy disk with hand-written label reading 'Mind Prober [sic]'; disk also includes support files for IBM-DOS). Timothy Leary Papers, New York Public Library Rare Books and Manuscripts Division. New York, NY. MssCol 18400, disk image series 10, disk image {2,} MI8400-0196.
*   Leary, T.L. (1986-1996b). _Timothy Leary's Mind Mirror_ (one 5.25” floppy disk with hand- written label and support files for IBM-DOS). Timothy Leary Papers, New York Public Library Rare Books and Manuscripts Division. New York, NY. MssCol 18400, disk image series 10, disk image MI8400-0125.
*   Leary, T.L. (1988a). _Timothy Leary's Mind Mirror_ (one 5.25” floppy disk with machine-printed label and read.me file containing instructions for copying the program). Timothy Leary Papers, New York Public Library Rare Books and Manuscripts Division. New York, NY. MssCol 18400, disk image series 10, disk image M18400-0093.
*   Leary, T.L. (1988b). _Timothy Leary's Mind Mirror_ (one 5.25” floppy disk with machine-printed label and read.me file containing instructions for copying the program; also contains one user- generated saved-game file). Timothy Leary Papers, New York Public Library Rare Books and Manuscripts Division. New York, NY. MssCol 18400, disk image series 10, disk image MI8400-0296.
*   Leary, T.L. (1988c). Timothy Leary's Mind Mirror (one 5.25” floppy disk with machine-printed label and corrupted readme.txt file). Timothy Leary Papers, New York Public Library Rare Books and Manuscripts Division. New York, NY. MssCol 18400, disk image series 10, disk image MI8400-0297.
*   Leary, T.L. (1988d). _Timothy Leary's Mind Mirror_ (one 5.25” floppy disk with commercially printed instruction manual, publisher's limited warranty card and warranty reply card; label text printed in plain text). Julio Mario Santo Domingo Collection, Harvard University Houghton Library. Cambridge, MA. Barcode 6446558.
*   Leary, T.L. (1988e). _Timothy Leary's Mind Mirror_ (one 5.25” floppy disk with commercially printed instruction manual, publisher's limited warranty card and warranty reply card; label text printed in hollow font; manual autographed by Leary). Julio Mario Santo Domingo Collection, Harvard University Houghton Library. Cambridge, MA. Barcode 5717163.
*   Leary, T.L. (1990). Timothy Leary's Mind Mirror (two 5.25” floppy disks with folded instruction sheet; published by MindWare). Julio Mario Santo Domingo Collection, Harvard University Houghton Library. Cambridge, MA. Barcode 5717354.
*   Mansnerus, L. (1996, June 1). [Obituary: Timothy Leary, Pied Piper of psychedelic 60's, Dies at 75](http://www.webcitation.org/71InLRakh). _The New York Times_. Retrieved from https://archive.nytimes.com/www.nytimes.com/learning/general/onthisday/bday/1022.html. (Archived by WebCite® at http://www.webcitation.org/71InLRakh).
*   McCray, W.P. (2016). Timothy Leary's trans humanist SMI<sup>2</sup>LE. In D. Kaiser & W.P. McCray (Eds.), _Groovy Science: Knowledge, nnovation, and American counterculture_. (pp. 238-269). Chicago, IL: University of Chicago Press.
*   Rehn, A. (2004). The politics of contraband: the honor economies of the warez scene. _Journal of Socio-Economics 33_(3), 359–374.
*   Rein, L. (2012, November 12) [Timothy Leary and Harvard, Reunited At Last](http://www.webcitation.org/71Iu9JTJd). _Timothy Leary Archives_. Retrieved fromhttp://www.timothylearyarchives.org/timothy-leary-and-harvard-reunited-at-last/. (Archived by WebCite® at http://www.webcitation.org/71Iu9JTJd).
*   Reunanen, M., Wasiak, P. & Botz, D. (2015). Crack intros: piracy, creativity and communication. _International Journal of Communication, 9_, 798-817.
*   Parikka, J. (2007). _Digital contagions: a media archaeology of computer viruses._ New York, NY: Peter Lang.
*   Parikka, J. (2015). Sites of media archaeology: producing the contemporary as a shared topic. _Journal of Contemporary Archaeology, 2_(1), 8-14.
*   Piccini, A.A. (2015) Media-archaeologies: an invitation. _Journal of Contemporary Archaeology, 2_(1), 1-8.
*   Schuessler, J. (2013, September 27) [Timothy Leary video games unearthed in archive](http://www.webcitation.org/71ImHGOOE). _The New York Times_. Retrieved from https://artsbeat.blogs.nytimes.com/2013/09/27/timothy-leary-video-games-unearthed-in-archive. (Archived by WebCite® at http://www.webcitation.org/71ImHGOOE).
*   Sköld, O. (2018). Understanding the "expanded notion" of videogames as archival objects: a review of priorities, methods, and conceptions. _Journal of the Association for Information Science and Technology, 69_(1), 134-145\.
*   Winget, M.A. & Sampson, W.W. (2011). Game development documentation and institutional collection development policy. In _Proceedings of the 11th Annual International ACM/IEEE Joint Conference on Digital Libraries,_ (pp. 29-38). New York, NY: ACM.
*   Yingling, J. & McClain, M.B. (2015). _Snowball sampling, grounded theory, and theoretical sampling: roles in methamphetamine markets._ London: SAGE Publications. (SAGE research methods cases).

</section>

</article>