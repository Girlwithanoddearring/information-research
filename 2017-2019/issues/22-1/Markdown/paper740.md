<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Análisis de metadatos de noticias para la extracción de información del código fuente. El software METADATOSHTML

## [María-José Baños-Moreno, Eduardo R. Felipe, Juan-Antonio Pastor-Sánchez, Gercina Lima](#author), y [Rodrígo Martínez-Bejar](#author).

> **Introducción.** Los objetivos de este trabajo son determinar qué esquemas se utilizan para título, resumen, palabras clave, autoría y periódico en prensa; conocer qué pautas siguen los periódicos en la implementación de dichos esquemas; y averiguar cómo esto afecta a la extracción de información.  
> **Metodología.** Para ello, se define una muestra de diarios y se analiza su código fuente, identificando esquemas utilizados y patrones de uso. Esto permite extraer valores de dato utilizando la aplicación MetadadosHTML.  
> **Resultados.** Se han detectado esquemas estándar, ad hoc y propios de los periódicos. Se han hallado diversas prácticas, como valores agrupados en una misma línea de código o por separado; ruido en un valor y errores al referir los nombres de los atributos de esquemas estándar. Estos problemas afectan a la extracción de datos basada en esquemas de metadatos y metadatos en MetadadosHTML  
> **Conclusiones.** Es necesario avanzar en el uso de esquemas estándar, como Dublin Core o schema.org, favoreciendo la implantación de estos (u otros) en los códigos fuente de noticias. También resulta imprescindible la adopción de buenas prácticas al explicitar datos y valores de datos. Sólo así es posible evolucionar en la interoperabilidad entre sistemas y en la recuperación y reutilización de información.

[Abstract in English](#abs)

<section>

## Introducción

Los metadatos son el conjunto de atributos y valores de dato que representan un recurso, facilitando su identificación ([Felipe, 2016](#felipe2016); [Abbud Grácio and Fadel, 2010, pp. 10–11](#abbud2010); [AENOR, 2012](#aenor2012); [Hillman, 2005](#hillman2005); [Kallipolitis, Karpis, and Karali, 2012](#kallipolitis2012); [Yaginuma, Pereira, and Baptista, 2003a](#yaginuma2003a)). Más específicamente: _‘son descripciones estructuradas y codificadas que describen características y propiedades de objetos y recursos para facilitar su localización, recuperación, valoración, administración, persistencia e interoperabilidad’_ ([Pastor-Sánchez, 2011, p. 22](#pastor2011)). Los metadatos tienen como funciones facilitar el descubrimiento de recursos, así como su evaluación y consumo ([Díaz, Granell, Beltrán Fonollosa, Llaves, and Gould, 2008](#diaz2008)).

Cuando esos metadatos se organizan en una estructura de elementos comunes ([Pereira and Baptista, 2004](#pereira2004); [Yaginuma, Pereira, and Baptista, 2003b](#yaginuma2003b)) se habla de esquemas de metadatos.Estas estructuras, como DC (Dublin Core), están orientadas a la descripción (normalizada) de documentos mediante conjuntos de atributos similares a los contemplados por las normas ISBD, entre otras, para la descripción bibliográfica en catálogos de bibliotecas ([Ortiz-Repiso Jiménez, 1999](#ortiz1999)). Es decir, pueden aplicarse para el análisis documental de documentos ([Abadal and Codina, 2009](#abadal2009); [Baptista and Barbosa Machado, 2001](#baptista2001); [Kallipolitis _et al._, 2012](#kallipolitis2012); [Yaginuma _et al._, 2003a](#yaginuma2003a), [2003b](#yaginuma2003b)). Un enfoque, complementario a los anteriores, se centra en el descubrimiento de información a partir de metadatos descriptivos ([Vallez, Rovira, Codina, and Pedraza-Jiménez, 2010](#vallez2010); [Rovira and Marcos, 2006](#rovira2006); [Vásquez Paulus, 2008](#vasquez2008); [Ortiz-Repiso Jiménez, 1999](#ortiz1999)). Así, ([Guallar, Abadal, and Codina, 2016](#guallar2016), [2012](#guallar2012)), entre otros, ofrecen indicadores para el análisis de los sistemas de consulta de hemerotecas de periódicos, teniendo en cuenta, entre otros, las opciones de acotación temporal, uso de índices (de autores, de personajes, etc.) y búsqueda por palabrasclave. Estos elementos no son más que metadatos asignados a artículos de prensa, que pueden ser recuperados gracias al uso de esquemas de metadatos en los sistemas de recuperación de información, aumentando su efectividad ([Garshol, 2004](#garshol2004); [Vásquez Paulus, 2008](#vasquez2008)).

Ahora bien, una recuperación de información exhaustiva, sólo es posible cuando los metadatos son computables, es decir, tratables por máquinas ([Díaz _et al._, 2008](#diaz2008)), estandarizados y de uso generalizado, especialmente si se pretende la interoperabilidad entre sistemas. Esto aún no se ha producido, principalmente con las etiquetas meta de los encabezados HTML, cuya información muchas veces no describe el contenido real del documento web ([Burgués, 2009, pp. 3–4](#burgues2009)). El trabajo actual pone el foco, precisamente, en cinco metadatos de descripción de artículos de prensa escrita: título, resumen, palabras clave, autor/a y medio, recogidos bien como etiquetas meta bien como atributos en otras etiquetas. Continúa así la investigación iniciada en ([Baños-Moreno, Felipe, Pastor-Sánchez, Martínez-Béjar, Rodrigo, and Lima, 2015](#banos2015)), donde se expone qué esquemas de metadatos existen en el ámbito periodístico desde dos perspectivas: bibliografía especializada y códigos fuente de una selección de periódicos. En esta publicación se destacan, entre otras: 1) Que existen diversos esquemas, generales y especializados, con distintos grados de implantación; 2) Que los especializados en prensa apenas son utilizados en los códigos analizados; 3) Que en la bibliografía apenas se referencia los esquemas visibles en los códigos fuente; 4) Todo ello dificulta la descripción unificada de recursos y, con ello, la recuperación y reutilización de información disponible en web.

Partiendo de estas cuestiones, y con vistas a la reutilización de información de actualidad para el modelado de una ontología de dominio ([Baños-Moreno, Pastor-Sánchez, and Martínez-Béjar, 2013](banos2013)), se plantean tres objetivos: 1) determinar qué esquemas se utilizan para asignar los atributos indicados; 2) conocer qué pautas se siguen en el uso de dichos esquemas en etiquetas meta y atributos de etiquetas HTML; y 3) averiguar cómo la elección de esquemas y pautas de uso afectan a la extracción de valores de datos (_data values_). Para ello, se analizan los códigos fuente de una muestra de noticias, identificando tanto esquemas de metadatos como patrones de uso en los elementos de estudio. Para la extracción se emplea una aplicación desarrollada ad hoc que, basándose en los datos de las etapas previas, obtiene automáticamente valores de dato.

## Metodos

Se definió una muestra de periódicos ([Tabla 1](#tabla1)) para la cual se dividió el mundo en 9 zonas, de acuerdo a características geopolíticas, históricas y socioeconómicas. En cada área se seleccionaron aquellos países con mayor Producto Interior Bruto (PIB). Después, se escogió el diario generalista más leído en cada país, de acuerdo a datos de accesos Web y compra de edición en papel de 4International Media and Newspaper (4IMN).

<table id="tabla1"><caption>

**Tabla 1\. Periódicos utilizados a los que se ha accedido en este trabajo**</caption>

<thead>

<tr>

<th>Zona del mundo</th>

<th>País</th>

<th>Periódico</th>

<th>Código</th>

</tr>

</thead>

<tbody>

<tr>

<td rowspan="5">Europa</td>

<td>Alemania</td>

<td>Süddeutsche Zeitung</td>

<td>101</td>

</tr>

<tr>

<td>Francia</td>

<td>Le Monde</td>

<td>111</td>

</tr>

<tr>

<td rowspan="3">Reino Unido</td>

<td>The Daily Telegraph</td>

<td>121</td>

</tr>

<tr>

<td>Financial Times</td>

<td>122</td>

</tr>

<tr>

<td>The Economist</td>

<td>123</td>

</tr>

<tr>

<td rowspan="2">Norteamérica</td>

<td rowspan="2">Estados Unidos</td>

<td>The New York Times</td>

<td>201</td>

</tr>

<tr>

<td>The Wall Street Journal</td>

<td>202</td>

</tr>

<tr>

<td rowspan="2">Latinoamérica</td>

<td>Brasil</td>

<td>O Globo</td>

<td>301</td>

</tr>

<tr>

<td>México</td>

<td>El Universal</td>

<td>311</td>

</tr>

<tr>

<td>Ex-Repúblicas rusas</td>

<td>Rusia</td>

<td>Pravda</td>

<td>401</td>

</tr>

<tr>

<td colspan="1" rowspan="2">África Subsahariana</td>

<td>Nigeria</td>

<td>Nigerian Tribune</td>

<td>501</td>

</tr>

<tr>

<td>Sudáfrica</td>

<td>Independent Tribune</td>

<td>511</td>

</tr>

<tr>

<td rowspan="3">Asia</td>

<td>China</td>

<td>China Daily</td>

<td>601</td>

</tr>

<tr>

<td>India</td>

<td>The Times of India</td>

<td>611</td>

</tr>

<tr>

<td>Japón</td>

<td>Asahi Shimbun</td>

<td>621</td>

</tr>

<tr>

<td rowspan="4">Oriente</td>

<td>Arabia Saudí</td>

<td>Arab News</td>

<td>701</td>

</tr>

<tr>

<td>Emiratos Árabes Unidos</td>

<td>Gulf News</td>

<td>711</td>

</tr>

<tr>

<td>Israel</td>

<td>Yedioth Aharonot</td>

<td>721</td>

</tr>

<tr>

<td>Turquía</td>

<td>Today's Zaman</td>

<td>731</td>

</tr>

<tr>

<td>Oceanía</td>

<td>Australia</td>

<td>The Australian Financial Review</td>

<td>801</td>

</tr>

<tr>

<td>Norte de África</td>

<td>Egipto</td>

<td>The Daily News Egypt</td>

<td>901</td>

</tr>

</tbody>

</table>

Los códigos de la columna _Código_ de la [Tabla 1](#tabla1) identifican los periódicos por: zona del mundo (primera cifra), país (segunda cifra) y periódico (tercera cifra). Estos códigos son utilizados a lo largo de este documento para simplificar el texto.

Para el análisis, se accedió a tres noticias por periódico en días diferentes durante enero y  
febrero de 2016\. Para la identificación de metadatos, se revisó el código fuente de los diarios, tanto de forma manual como utilizando el servicio de validación de marcado de contenidos web del W3C (_Markup Validation Service_).

<figure id="figura1">

![Figura 1: Datos y valores de datos para noticia redactada por (Parker, 2016)](../p740fig1.png)

<figcaption>

**Figura 1: Datos y valores de datos para noticia redactada por ([Parker, 2016](#parker2016))**</figcaption>

</figure>

Se tuvo en cuenta para ello tanto las etiquetas meta como los atributos que aparecen en el cuerpo de la página web (`<body>`), en función de las diferentes formas que pueden adoptar. En este sentido, un metadato está compuesto por pares de datos y valores de datos, de la forma en que se muestra en la [Figura 1](#figura1). No se tiene en cuenta en esta categorización aquellos atributos que tienen como único fin la separación en secciones de una página web. Por ejemplo, _`<div id="Title_e">`_ ó _`<div id="article_head">`_, utilizados por China Daily y Asahi Shimbun, respectivamente. Tampoco la información en scripts, aunque algunos medios aplican algunos de los esquemas nombrados anteriormente, como schema.org (The Wall Street Journal o Pravda.ru, entre otros).

A partir del análisis, se hallaron tres tipos de esquemas: estándares, ad hoc y propietarios, descritos en el apartado Resultados. Además, se identificaron una serie de características e incidencias para título, resumen, palabras clave, autor/a y nombre del medio.

Teniendo en cuenta estos aspectos se diseñó un algoritmo para la extracción de  información. Implementado en MetadadosHTML, el algoritmo accede, extrae y almacena las descripciones de noticias de una muestra de más de 5500 noticias obtenidas de los diarios indicados en la [Tabla 1](#tabla1). La idea es similar a ([Burgués, 2009](#burgues2009)) que, en ausencia de un estándar de uso común, extrae la información de las etiquetas HTML, para luego generar un XML con el que indexar los documentos y reconoce problemas similares a los hallados en este trabajo, que se detallan a continuación. Otras aproximaciones, como ([Bohannon _et al._, 2012](#bohannon2012)) diseñan algoritmos que, entre otros elementos del código fuente, también emplean las meta-etiquetas para identificar el contenido de los documentos y sus atributos. Sin embargo, no se tienen en cuenta los estándares de metadatos, y asumen la presencia de ruido en la información extraída, contrarrestándola con reglas de aprendizaje automático para eliminarlo.

## Resultados

El análisis de los esquemas identificados en los códigos fuente llevó a su clasificación en tres categorías, que se describen brevemente a continuación:

### 1\. Esquemas externos y reconocidos internacionalmente (esquemas estándar)

Dentro de esta categoría están Open Graph Protocol (en adelante OG), Dublin Core (DC), Twitter Cards (TC), hAtom o schema.org (SC). También se incluyen algunas etiquetas meta de HTML, donde el atributo name indica el atributo, o tipo de dato y content el valor del dato. Estas etiquetas meta se refieren al contenido del documento y pueden considerarse ‘nombres de metadatos estándar’ (Hickson _et al._, 2014): 
_`<meta name="author" content=”...”>`_, _`<meta name="description" content=”...”>`_ y _`<meta name=”keywords” content=”...”>`_. No incluye _`<meta name="title" content=”...”>`_.

En el caso de utilizar XHTML, uno de los usos de los atributos de metadatos, como property, es representar ‘el contenido del documento, ya que la misma cadena literal puede utilizarse para especificar tanto el contenido del documento como los metadatos’ ([Axelsson _et al._, 2006](#axelsson2006)). Así, _`<span property="dc:title" content="...">`_, de Nigerian Tribune (501), utiliza el esquema DC para referirse al título de una noticia.

En cuanto a schema.org (SC), usa microdatos para, mediante el atributo itemprop, referirse a una meta-información (Baños-Moreno _et al._, 2015). Este atributo se inserta habitualmente en las etiquetas del cuerpo (<body>), aunque también puede insertarse en las etiquetasmeta (Leithead, Pfeiffer, Navara, and O’Connor, 2012) (véase la Figura 2). Algo similar ocurre con el microformato hAtom 0.1., utilizado por The Daily News Egypt (901), que  inserta meta información en <body> tal y como puede verse en la [figura 2](#figura2).

<figure id="figura2">

![Figura 2\. Etiquetas meta de The New York Times (201) con el atributo itemprop, sin valores de dato.](../p740fig2.png)

<figcaption>

**Figura 2\. Etiquetas meta de The New York Times (201) con el atributo itemprop, sin valores de dato.**</figcaption>

</figure>

La [figura 3](#figura3) resume los esquemas estándar utilizados para título, resumen, palabras clave, autor y nombre del medio.

<figure id="figura3">

![Figura 3: Frecuencia de esquemas estándar en la muestra de periódicos por tipos de metadatogure1: The conceptual framework](../p740fig3.png)

<figcaption>

**Figura 3: Frecuencia de esquemas estándar en la muestra de periódicos por tipos de metadatogure1: the conceptual framework**</figcaption>

</figure>

Para los atributos analizados, los estándares más utilizados, con diferencia son HTML (100%), OG (90%), TC (71%) y SC (52%), con distintos grados de implementación, especialmente en cuanto a título, resumen y palabras clave. La cifra más elevada (22 diarios) para el nombre del medio, en OG, se debe al uso de dos atributos: _`site_name`_ y _`article:publisher`_, también ocurre con schema.org (SC).

Para algunos de estos esquemas estándar se han desarrollado validadores que analizan la corrección de las etiquetas y el uso de atributos, como Markup Validation Service, de la W3C, Card validator, de Twitter Cards, ó Structured Data Testing Tool, de Google.

### 2\. Esquemas externos para un propósito concreto (esquemas ad hoc).

Se han identificado cuatro tipos (Sailthru, Bi3d, Parse.ly y dcsext). La [figura 4](#figura4) muestra algunos ejemplos de código, para tres de estos esquemas propietarios.

<figure id="figura4">

![Figura 4\. Etiquetas ad hoc de los esquemas de metadatos de Sailthru, Bi3d y Parse.ly](../p740fig4.png)

<figcaption>

**Figura 4\. Etiquetas ad hoc de los esquemas de metadatos de Sailthru, Bi3d y Parse.ly**</figcaption>

</figure>

**Sailthru**, y su tecnología propietaria Horizon ‘realiza un seguimiento del comportamiento en el sitio, móvil y del correo electrónico para desarrollar un perfil de interés único para cada usuario final’ ([Sailthru, 2013](#sailthru2013)).

El esquema desarrollado por **Bi3d** (Believe in 3D) ‘desarrolla la próxima generación de interfaces humano-ordenador interactivos para la industria del marketing y el entretenimiento’ ([BI3D, 2013](#bi3d2013)). Parece que, en este momento, no está operativa.

**Parse.ly** es otra plataforma de análisis diseñada específicamente para los editores en línea y ofrece tres formas de incorporar estos datos a la página web: JSON-LD; repeated metadata; parsely-page. Parse.ly sigue el esquema NewsArticle de schema.org. The Daily Telegraph (121) utiliza la opción parsely-page, ya obsoleta y reemplazada por JSON-LD pero que aún puede ser rastreada ([Parse.ly, s.f.](#parsesf)).

Webtrends, otra empresa de análisis web, escanea el código fuente buscando parámetros de consulta Webtrends para añadir a los datos. En la muestra, The Daily Telegraph recurre a su espacio de nombre, **DCSext** para coleccionar datos.

En este grupo podría incluirse la etiqueta de **Google**, _`<meta name="news_keywords" content="...">`_ creada para que los periódicos indiquen las palabras más relevantes de una noticia ([Google Inc., 2016](#google2016)).

El siguiente gráfico muestra los esquemas ad hoc utilizados para los datos analizados: título, resumen, palabras clave, autor y nombre del medio.

<figure id="figura5">

![Figura 5\. Frecuencia de esquemas estándar en la muestra de periódicos por tipos de metadato.](../p740fig5.png)

<figcaption>

**Figura 5\. Frecuencia de esquemas estándar en la muestra de periódicos por tipos de metadato.**</figcaption>

</figure>

Tanto los esquemas externos de propósito específico como los propietarios son utilizados  
de forma residual, por un sólo medio en cada caso (5%), a excepción de la etiqueta meta de Google, _`news_keywords`_ (30% de la muestra).

### 3\. Esquemas propios, desarrollados por el periódico (esquemas propietarios).

Se utilizan para la estructuración y categorización de sus artículos. Este tipo de esquema es el menosfrecuente, con presencia solamente en dos medios, The New York Times (201) y The Wall Street Journal (202), con dos esquemas propios por cada medio. La [tabla 2](#tabla2) muestra algunos elementos del esquema de The New York Times.

<table id="tabla2"><caption>

**Tabla 2\. Algunos elementos del esquema utilizado por The New York Times. Fuente: ([Harris, 2007](#harris2007))**</caption>

<thead>

<tr>

<th>Campo</th>

<th>Significado</th>

<th>Correspondiencia</th>

</tr>

</thead>

<tbody>

<tr>

<td>hdl</td>

<td>Headline</td>

<td>Titular</td>

</tr>

<tr>

<td>byl</td>

<td>Byline</td>

<td>Autor del artículo</td>

</tr>

<tr>

<td>lp</td>

<td>Lead Paragraph</td>

<td>Resumen</td>

</tr>

<tr>

<td>des</td>

<td>Subjects Descriptive Terms</td>

<td>Palabras clave</td>

</tr>

<tr>

<td>per</td>

<td>People</td>

<td>Identificador para personas</td>

</tr>

<tr>

<td>org</td>

<td>Organizations</td>

<td>Identificador para organizaciones</td>

</tr>

<tr>

<td>geo</td>

<td>Geographic Locations</td>

<td>Identificador para lugares geográficos</td>

</tr>

</tbody>

</table>

En ocasiones, también se pueden incrustar datos no visibles y personalizados (_embedded custom non-visible data_) gracias a HTML5\. Tal y como puede verse en la [figura 6](#figura6) son atributos sin espacio de nombre cuyo name comienza con la cadena data- y tienen, al menos un carácter después del guión ([Hickson _et al._, 2014](#hickson2014)) (ver Figura 4). Puesto que son elementos personalizables, se clasifican como esquemas propietarios.

<figure id="figura6">

![Figura 6\. Atributos data- para artículos en The New York Times y The Wall Street Journal, respectivamente](../p740fig6.png)

<figcaption>

**Figura 6\. Atributos data- para artículos en The New York Times y The Wall Street Journal, respectivamente**</figcaption>

</figure>

En relación al tipo de dato, los más asentados son título y resumen. Estos aparecen, mediante etiquetas meta o atributos, en todos los diarios de la muestra. Destaca la ausencia de metadatos, en cualquiera de las formas identificadas, en palabras clave (16%), nombre del medio (19%) y autoría (33%), pues todos son elementos básicos del análisis de contenido (palabras clave) y formal (autor y publicación) de un documento. En el ámbito específico de medios de comunicación impresa, ([Yaginuma _et al._, 2003b](#yaginuma2003b)), en el marco del proyecto Omnipaper, los califica de imprescindibles para la correcta representación de una noticia.

A continuación, se muestran las particularidades de las líneas de código para cada uno de los atributos objeto de este análisis.

### Titular de la noticia

Para el titular, se utiliza, en todos los casos, la etiqueta _`<title>`_, que se refiere al título de una página web cualquiera y, en este caso, para titulares de noticias. Por el contrario, _`<meta name="title" content="...">`_ está menos extendida. Además, se han identificado esquemas de los tres tipos indicados, aunque con un marcado incorrecto. Por ejemplo, en: _`<h1 itemprop="headline name">`_, de The Economist, aparece el atributo _headline name_, que no reconoce schema.org . También se han hallado atributos con diferente denominación para referirse a este elemento, como _hdl_, _headline_ o _article.origheadline_ (ver [Tabla 3](#tabla3)).

<table id="tabla3"><caption>

**Tabla 3\. Formas de identificar el titular en los medios de la muestra. Fuente: Elaboración propia a partir de los códigos fuente de los diarios de la muestra**</caption>

<thead>

<tr>

<th>Línea de código</th>

<th>Tipo de esquema</th>

<th>N</th>

</tr>

</thead>

<tbody>

<tr>

<td>

<**title**>...</title></td>

<td>Estándar (HTML)</td>

<td>21</td>

</tr>

<tr>

<td>

`<meta property="`**`og:title`**`" content="...">`</td>

<td>Estándar (OG)</td>

<td>19</td>

</tr>

<tr>

<td>

`<meta name="`**`twitter:title`**`" content="...">`</td>

<td>Estándar (TC)</td>

<td>10</td>

</tr>

<tr>

<td>

`<h1 itemprop="`**`headline`**`">...</h1>`  
`<h1 itemprop="`**`headline name`**`">...</h1>`  
`<h3 itemprop="`**`headline`**`" class="headline">...</h3>`  
`<meta itemprop="`**`name`**`" content="..."/>`
</td>

<td>Estándar (SC)</td>

<td>6</td>

</tr>

<tr>

<td>

`<h1 class="`**`entry-title`**`">...</h1>`
</td>

<td>Estándar (hAtom 0.1)</td>

<td>1</td>

</tr>

<tr>

<td>

`<span property="`**`dc:title`**`" content="" class="...">`
</td>

<td>Estándar (DC)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`sailthru.flytitle`**`" content="...">`
</td>

<td>Ad hoc (Sailthru)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`bi3dArtTitle`**`" content=" ">`
</td>

<td>Ad hoc (Bi3D)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="parsely-page" content='{"`**`title`**`": "..." .../>`</td>

<td>Ad hoc (Parse.ly)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`hdl`**`" content="...">`
</td>

<td>Propietario (201)</td>

<td>1</td>

</tr>

<tr>

<td>

`<div id="..." aria-label="..." role="..." class="..." `**`data-title`**`="..."...>...</div>`</td>

<td>Propietario (201)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`article.origheadline`**`" content="...">`  
`<meta name="`**`article.headline`**`" content="...">`
</td>

<td>Propietario (202)</td>

<td>1</td>

</tr>

<tr>

<td>

`<li class="..." ... `**`data-headline`**`="...">...</li>`
</td>

<td>Propietario (202)</td>

<td>1</td>

</tr>

</tbody>

</table>

En ocasiones, además, el texto incluye ruido, información que no es propiamente el titular. Por ejemplo: _`<title>Jeremy Corbyn tries to force public schools to open up music, arts, sport facilities to state schoolchildren - Telegraph</title>`_.

### Resumen o descripción.

Este elemento cuando es una noticia, suele tratarse de un subtítulo o bien la entradilla, ‘_un resumen lo suficientemente completo y autónomo como para que el lector conozca lo fundamental de la noticia sólo con leerlo_’ ([Lajusticia, 2000](#lajusticia2000)).

Se han identificado esquemas de los tres tipos indicados ([tabla 4](#tabla4)) pero, como con el titular, con un marcado incorrecto. Así, en `<p class="article entry-summary>`, de Süddeutsche Zeitung (101), el atributo _class="article entry-summary"_ no se corresponde con el definido por el esquema de hAtom, en el que parece estar basado, siendo _class="entry-summary"_ el correcto.

<table id="tabla4"><caption>

**Tabla 4\. Formas de identificar el resumen en los medios de la muestra. Fuente: Elaboración propia a partir de los códigos fuente de los diarios de la muestra**</caption>

<thead>

<tr>

<th>Línea de código</th>

<th>Tipo de esquema</th>

<th>N</th>

</tr>

</thead>

<tbody>

<tr>

<td>

`<meta name="`**`description`**`" content="...">`
</td>

<td>Estándar (HTML)</td>

<td>18</td>

</tr>

<tr>

<td>

`<meta property="`**`og:description`**`" content="...">`
</td>

<td>Estándar (OG)</td>

<td>17</td>

</tr>

<tr>

<td>

`<meta name="`**`twitter:description`**`" content="...">`
</td>

<td>Estándar (TC)</td>

<td>9</td>

</tr>

<tr>

<td>

`<h2 class="..." itemprop="`**`description`**`">...</h2>`  
`<meta itemprop="`**`description`**`" name="..." content="...">`
</td>

<td>Estándar (SC)</td>

<td>5</td>

</tr>

<tr>

<td>

`<p class="`**`article entry-summary`**`">...</p>`</td>

<td>Estándar (hAtom 0.1)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`lp`**`" content="...">`
</td>

<td>Propietario (201)</td>

<td>1</td>

</tr>

<tr>

<td>

`<div id="..." aria-label="..." role="..." class="..." `**`data-description`**`="..." ...>...</div>`
</td>

<td>Propietario (201)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`article.summary`**`" content="...">`
</td>

<td>Propietario (202)</td>

<td>1</td>

</tr>

<tr>

<td>

`<li class="..." ... `**`data-summary`**`="...">...</li>`
</td>

<td>Propietario (202)</td>

<td>1</td>

</tr>

</tbody>

</table>

A veces, también el texto incluye ruido. Por ejemplo, El Universal comienza el texto con su perfil de Twitter: _`<meta name="description" content="metropoli@eluniversal.com.mx ...">`_.

### Palabras clave.

Las palabras clave son términos que, en su conjunto, representan el contenido de un documento. En general, se utilizan dos formas para referirse a este elemento: _keywords y tag_. En la [tabla 5](#tabla5)  
muestra el código a extraer, de qué esquema y en qué periódicos aparece.

<table id="tabla5"><caption>

Tabla 5\. Formas de identificar las palabras clave en los medios de la muestra. Fuente: Elaboración propia a partir de los códigos fuente de los diarios de la muestra</caption>

<thead>

<tr>

<th>Línea de código</th>

<th>Tipo de esquema</th>

<th>N</th>

</tr>

</thead>

<tbody>

<tr>

<td>

`<meta name="`**`keywords`**`" content="...">`
</td>

<td>Estándar (HTML)</td>

<td>14</td>

</tr>

<tr>

<td>

`<meta property="`**`article:tag`**`" content="...">`
</td>

<td>Estándar (OG)</td>

<td>4</td>

</tr>

<tr>

<td>

`<meta itemprop="`**`keywords`**`" content="...">`
</td>

<td>Estándar (SC)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`sailthru.tags`**`" content="...">`
</td>

<td>Ad hoc (Sailthru)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="parsely-page" content='{...,"`**`tags`**`": ["..."]}'/>`
</td>

<td>Ad hoc (Parse.ly)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`news_keywords`**`" content="...">`
</td>

<td>Ad hoc (Google)</td>

<td>7</td>

</tr>

<tr>

<td>

`<meta name="`**`DCSext.cmsSect`**`" content="... ">`
</td>

<td>Ad hoc (DCSext)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`org`**`" content="...">`  
`<meta name="`**`des`**`" content="...">`  
`<meta name="`**`geo`**`" content="...">`  
`<meta name="`**`per`**`" content="...">`</td>

<td>Propietario (201)</td>

<td>1</td>

</tr>

</tbody>

</table>

Para este dato, destacan algunos aspectos que influyen en la extracción de información a partir  
del código fuente:

1.  **Campos repetibles:** Casi todos los medios incluyen en una misma línea el conjunto de  
    palabras clave que representa el contenido de un artículo. Algunos periódicos, sin  
    embargo, repiten la meta etiqueta con valores diferentes, tantas veces como palabras  
    clave refiera, como se muestra en las tablas [6](#tabla6) y [7](#tabla7).

<table id="tabla6"><caption>

**Tabla 6\. Periódicos que repiten las etiquetas meta para las palabras clave**</caption>

<thead>

<tr>

<th>Línea de código</th>

<th>Repiten etiqueta</th>

<th>No la repiten</th>

<th>Total</th>

</tr>

</thead>

<tbody>

<tr>

<td>

`<`**`meta name`**`="keywords" `**`content`**`="...">`  
</td>

<td>1</td>

<td>13</td>

<td>14</td>

</tr>

<tr>

<td>

**`<meta property="`**`article:tag`**`" content="...">`**</td>

<td>3</td>

<td>1</td>

<td>4</td>

</tr>

<tr>

<td>

`<meta name="`**`org`**`" content="...">`  
`<meta name="`**`des`**`" content="...">`  
`<meta name="`**`geo`**`" content="...">`  
`<meta name="`**`per`**`" content="...">`</td>

<td>203</td>

<td>---</td>

<td>1</td>

</tr>

</tbody>

</table>

1.  **Mismo propósito, información diferente:** The New York Times (201) muestra las  
    palabras clave en distintas etiquetas meta y rompe el patrón de otros esquemas, pues no  
    recoge la misma información, aunque el fin del esquema es el mismo.
2.  **Escasa categorización:** The New York Times es el único diario que en un esquema  
    propio, el tipo de palabras clave ([tabla 7](#tabla7)): des → descriptores; org → organización;  
    per → persona; geo → lugar.

<table id="tabla7"><caption>

Tabla 7\. Etiquetas meta de The New York Times (201) para mostrar las palabras clave</caption>

<thead>

<tr>

<th>Tipo de esquema</th>

<th>Línea de código fuente</th>

</tr>

</thead>

<tbody>

<tr>

<td>Estándar (OG)</td>

<td>

`<meta property="article:tag" content="Presidential Election of 2016">`  
`<meta property="article:tag" content="Republican Party">`  
`<meta property="article:tag" content="Cruz, Ted">`  
`<meta property="article:tag" content="Primaries and Caucuses">`  
`<meta property="article:tag" content="Iowa">`
</td>

</tr>

<tr>

<td>Propietario</td>

<td>

`<meta name="des" content="Presidential Election of 2016">`  
`<meta name="org" content="Republican Party">`  
`<meta name="per" content="Cruz, Ted">`  
`<meta name="des" content="Primaries and Caucuses">`  
`<meta name="geo" content="Iowa">`
</td>

</tr>

<tr>

<td>Estándar (HTML)</td>

<td>

`<meta name="keywords" content="Presidential Election of 2016,Republican Party,Cruz Ted,Primaries and Caucuses,Iowa">`
</td>

</tr>

<tr>

<td>Ad hoc (Google)</td>

<td>

`<meta name="news_keywords" content="Iowa">`
</td>

</tr>

</tbody>

</table>

1.  **Ruido en el valor de dato:** Como en los casos de titular y resumen, a veces se incluye en esta etiqueta información que no corresponde a este espacio. Por ejemplo:
    1.  En _`<meta name="news_keywords" content="VW, Matthias M&uuml;ller, Volkswagen, Wirtschaft">`_, Süddeutsche Zeitung (101) incluye el nombre del autor del artículo. También lo hace The Daily Telegraph (121).
    2.  En _`<meta name="keywords" content="israel, news, yedioth, ahronoth, english, ..., breaking news, jew, jews, www.ynetnews.com">`_ Yedioth Aharonot (721) incluye el nombre del medio, la sección de la noticia y la URL del medio.
    3.  Arab News (701) repite la etiqueta _`<meta name="keywords" content="..."/>`_: la primera vez hace referencia a la sección en la que se ha clasificado la noticia; la segunda recoge las palabras clave ([figura 7](#figura7)).

<figure id="figura7">

![ Figura 7\. Etiquetas meta estándar (HTML) usadas por Arab News (7011) para sección y las palabras clave](../p740fig7.png)

<figcaption>

**Figura 7\. Etiquetas meta estándar (HTML) usadas por Arab News (7011) para sección y las palabras clave**</figcaption>

</figure>

1.  **Código fuente con fines publicitarios:** Finalmente, se puede destacar la orientación a cuestiones publicitarias de algunos elementos de descripción de noticias, habitualmente mediante Javascripts. Este aspecto no se ha tenido en cuenta en este trabajo.

Todos los periódicos recogen en su código fuente líneas para palabras clave, con tres excepciones: Le Monde (111), Nigerian Tribune (501) e Independent Online (502).

### Autor de la noticia.

Autor/a es la persona o personas que han escrito una noticia y en los códigos fuente suele aparecer con dos denominaciones: _author_ ó _creator_ ([tabla 8](#tabla8)).

Para este dato, destacan estos aspectos:

1.  **Mismo propósito, información diferente:** el valor de dato de la etiqueta meta no siempre se corresponde con el nombre de quien ha redactado el artículo. Por ejemplo, _`<meta name="author" content="...">`_, de Süddeutsche Zeitung (101), muestra el nombre del medio. Algo similar sucede con _`<meta name="twitter:creator">`_, de The Economist (122), que indica como valor la sección en que se publica la noticia. Otros utilizan esta misma línea para hacer referencia al perfil de Twitter del medio
2.  **Errores de marcado:** The Daily Telegraph (121) marca incorrectamente el atributo de OG para el objeto article en _`<meta property="article:authorName" content="...">`_, en lugar de _article:author_.
3.  **Dos atributos, mismo valor:** The New York Times (201) hace referencia a autor y creador en la misma línea, con el mismo valor ([Web Hypertext Application Technology Working Group (WHATWG)], 2016a): _`<span ... itemprop="author creator" ...>`_.
4.  **Campos repetibles:** Cuando una noticia ha sido escrita por varios autores, habitualmente se repiten las líneas de código.
5.  Otros medios recogen este dato como una palabra clave de la noticia o bien lo que aparece es un enlace al conjunto de noticias redactadas por la misma persona.

<table id="tabla8"><caption>

**Tabla 8\. Formas de identificar autor/a en los medios de la muestra**</caption>

<thead>

<tr>

<th>Línea de código</th>

<th>Tipo de esquema</th>

<th>N</th>

</tr>

</thead>

<tbody>

<tr>

<td>

`<meta name="`**`author`**`" content="...">`
</td>

<td>Estándar (HTML)</td>

<td>4</td>

</tr>

<tr>

<td>

`<meta property="article:authorName" content="...">`  
`<meta property="article:`**`author`**`" content="...">`
</td>

<td>Estándar (OG)</td>

<td>2</td>

</tr>

<tr>

<td>

`<span itemprop="`**`author`**`" class="...">...</span>`  
`<div class="..." itemprop="`**`author`**`" ...>...</div>`  
`<b itemprop="`**`author`**`" ...>...</b>`  
`<span class="byline" itemprop="`**`author creator`**`" itemscope itemtype="http://schema.org/Person" itemid="...">...</span>`
</td>

<td>Estándar (SC)</td>

<td>4</td>

</tr>

<tr>

<td>

`<meta name="twitter:`**`creator`**`" content="... ">`
</td>

<td>Estándar (TC)</td>

<td>6</td>

</tr>

<tr>

<td>

`<meta name="parsely-page" content='{...,"`**`authors`**`": ["..."]}'/>`
</td>

<td>Ad hoc (Parse.ly)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`DCSext.author`**`" content="...">`
</td>

<td>Ad hoc (DCSext)</td>

<td>1</td>

</tr>

<tr>

<td>

`<meta name="`**`byl`**`" content="...">`
</td>

<td>Propietario (201)</td>

<td>1</td>

</tr>

<tr>

<td>

`<div id="..." aria-label="..." role="..." class="..." data-shares="..." ... `**`data-author`**`="..." ...>...</div>`
</td>

<td>Propietario (201)</td>

<td>1</td>

</tr>

<tr>

<td>

`<li class="..."... `**`data-authors`**`="...">...</li>`
</td>

<td>Propietario (202)</td>

<td>1</td>

</tr>

</tbody>

</table>

Para este dato no se identificó ningún metadato en Pravda (401), Nigerian Tribune (501), China Daily (601), The Times of India (602), Asahi Shimbun (603), Arab News (701) ni en Yedioth Aharonot (721).

HTML5 permite expresar la autoría mediante la etiqueta _rel=”author”_, Este atributo, escaso en la muestra, no se ha tenido en cuenta en este trabajo, ya que HTML5 aún no se ha asentado.

### Nombre del periódico

El medio es el que canaliza y publica los artículos de sus periodistas, agencias e invitado/as. Esta información es susceptible de ser expresada en diversos campos, pues el periódico es, normalmente, el propietario del sitio web que recoge los artículos, dueño del copyright de la información que publica, tiene uno o varios perfiles en redes sociales, etc.

Para este dato, destacan estos aspectos:

1.  **Diferentes atributos para un mismo tipo de información.** Existen varios atributos que, aunque en principio están destinados a fines diferentes, suelen mostrar el nombre del medio: publicador (_publisher_), copyright, nombre del sitio web (_site name_), etc.
2.  **Mismo atributo, diferentes valores de dato.** Un aspecto a destacar es el uso de códigos para identificar el medio, como hacen O Globo (301) o The Daily Telegraph (121), de quien es esta etiqueta meta: _`<meta property="article:publisher" content="143666524748">`_; Otros, como The Economist (122), The Wall Street Journal (202), The Australian Financial Review (801) o Daily News Egypt (901), lo completar con la URL de su perfil en Facebook.
3.  **Otros datos como ruido en la etiqueta meta o atributo correspondiente.** En ocasiones, este metadato es declarado en las etiquetas donde debería aparecer como valor de dato el nombre del autor de un artículo, aún cuando en la noticia aparece una persona con nombre y apellidos.
4.  **Nombre del medio como ruido en otros datos.** Es frecuente encontrar el nombre del medio entre las etiquetas `<Title>` y `</Title>`, complementando al título, e incluso entre las palabras clave de la noticia. No se han tenido en cuenta en la Tabla 9\. Por otro lado, como se ha indicado en el bloque anterior, Süddeutsche Zeitung (101) recoge este dato en la etiqueta meta de autor, contabilizándose en la Tabla 10, por lo que no se tiene en cuenta en este punto. Lo mismo sucede con _`<meta name="twitter:creator" content="...">`_, usada por O Globo (301).
5.  **Etiquetas que no son estándar o han dejado de ser utilizadas.** Entre las etiquetas meta, las cuatro vistas anteriormente son consideradas estándar ([Hickson _et al._, 2014](#hickson2014); [WHATWG, 2016b](#whatwg2016b)). No ocurre así con _`<meta name="copyright" content="...">`_, que podría ser sustituida por dcterms.rights, entre otras posibilidades. Sólo es utilizada por Süddeutsche Zeitung (101) y no se recoge en la Tabla que sigue. También se ha encontrado un caso de uso de la etiqueta _`<acronym title="...">...</acronym>`_, por The Financial Times (123), dónde el valor del atributo “title” es el nombre del medio. Esta etiqueta ya no es soportada en HTML5 y ha sido reemplazada por <abbr>, sin uso en la muestra.
6.  Finalmente, The New York Times (201), como con el autor de la noticia, usa una misma línea para hacer referencia a tres atributos, poseedor del copyright de la noticia, proveedor de la noticia y organización fuente, con el mismo valor ([WHATWG, 2016a](#whatwg2016a)): _`<span itemprop="copyrightHolder provider sourceOrganization" itemscope itemtype="http://schema.org/Organization" itemid="...">`_.

La [tabla 9](#tabla9) resume los esquemas identificados para el nombre del medio.

<table id="tabla9"><caption>

**Tabla 9\. Formas de identificar autor/a en los medios de la muestra**</caption>

<thead>

<tr>

<th>Línea de código</th>

<th>Tipo de esquema</th>

<th colspan="2" rowspan="1">N</th>

</tr>

</thead>

<tbody>

<tr>

<td>

`<acronym title="...">...</acronym>`
</td>

<td>Estándar (HTML)</td>

<td colspan="2" rowspan="1">1</td>

</tr>

<tr>

<td>

`<meta property="og:`**`site_name`**`" content="...">`
</td>

<td colspan="1" rowspan="2">Estándar (OG)</td>

<td>16</td>

<td colspan="1" rowspan="2">22</td>

</tr>

<tr>

<td>

`<meta property="article:`**`publisher`**`" content="...">`
</td>

<td>6</td>

</tr>

<tr>

<td>

`<span id="publisher" itemprop="`**`Publisher`**`" ...>...</span>`  
`<meta itemprop="`**`publisher`**`" content="...">`  
`<div class="logo" itemscope itemprop="`**`publisher`**`" itemtype="http://schema.org/Organization">...</div>`
</td>

<td colspan="1" rowspan="2">Estándar (SC)</td>

<td>4</td>

<td colspan="1" rowspan="2">6</td>

</tr>

<tr>

<td>

`<span itemprop="`**`copyrightHolder provider sourceOrganization`**`" itemscope itemtype="http://schema.org/Organization" itemid="...">...</span>`  
`<div itemscope itemprop="`**`copyrightHolder`**`" itemtype="http://schema.org/Organization">...</div>`  
`<meta itemprop="name" content="...">`
</td>

<td>2</td>

</tr>

<tr>

<td>

`<meta name="twitter:**site`**`" content="...">`  
`<meta name="twitter:**site`**`" value="...">`
</td>

<td>Estándar (TC)</td>

<td colspan="2" rowspan="1">10</td>

</tr>

<tr>

<td>

`<meta name="`**`DC.publisher`**`" content="...">`
</td>

<td>Estándar (DC)</td>

<td colspan="2" rowspan="1">1</td>

</tr>

<tr>

<td>

`<meta name="`**`cre`**`" content="...">`
</td>

<td>Propietario (201)</td>

<td colspan="2" rowspan="1">1</td>

</tr>

<tr>

<td>

`<meta name="`**`page.site`**`" content="wsj">`
</td>

<td>Propietario (201)</td>

<td colspan="2" rowspan="1">1</td>

</tr>

</tbody>

</table>

Igual que ocurre con el autor, existe un atributo _rel=”publisher”_, que indica que el dato que aparece en la etiqueta, habitualmente una página web o el perfil de alguna red social del medio, se refiere a un publicador. Véase, por ejemplo, _`<a href="https://plus.google.com/+arabnews" rel="publisher" ...>`_, de Arab News (701). No se ha tenido en cuenta en la tabla anterior.

Para este metadato no se identificó ningún metadato en Nigerian Tribune (501), China Daily (601), Arab News (701) ni en Gulf News (711).

## El software MetadadosHTML.

Dada la diversidad de notaciones identificadas para los distintos datos, se desarrolló un software para la extracción de información de la muestra de noticias en HTML, almacenadas inicialmente en una hoja de cálculo. Para la base de datos se utilizó Microsoft Access. De esta forma, esposible mantener disponible en el tiempo la información extraída para su consulta. Para la construcción del algoritmo se utilizó el lenguaje Object Pascal. La primera etapa consistió en la construcción de una base de datos, compuesta por tres Tablas ([tabla 10](#tabla10)), que facilitara la relación entre sus elementos. En esta etapa también se importaron los datos de la hoja de cálculo a la Tabla URL.

<table id="tabla10"><caption>

**Tabla 10\. Tablas que componen la base de datos de MetadadosHTML**</caption>

<thead>

<tr>

<th>Nombre de la tabla</th>

<th>Objetivo</th>

</tr>

</thead>

<tbody>

<tr>

<td>URL  
</td>

<td>Almacenar las direcciones (URLs) de las noticias</td>

</tr>

<tr>

<td>Tags</td>

<td>Almacenar las etiquetas meta y atributos que se desean extraer</td>

</tr>

<tr>

<td>Metadados</td>

<td>Almacena el contenido de las etiquetas meta y atributos de la Tabla Tags</td>

</tr>

</tbody>

</table>

Posteriormente, a partir del análisis previo, se registraron etiquetas meta y atributos en la tabla Tags. Este proceso conlleva un importante esfuerzo pues, como ya se ha indicado, los periódicos optaron por notaciones diferentes, sin seguir un patrón, a veces, en formato propietario. Con esta infraestructura, se inició el desarrollo propiamente dicho del software.

MetadadosHTML tiene tres funcionalidades:

1.  Acceso al código fuente de las URLs contenidas en la hoja de cálculo inicial. Debido al gran número de URLs, se descargaron todos los códigos HTML de las noticias en local, de forma que su análisis fuera realizado sin necesidad de acceder de nuevo a las páginas web. De esta forma se favorece la independencia de la conexión de datos y mayor velocidad de procesamiento posterior.
2.  Definición de las etiquetas meta y atributos de cada medio, teniendo en cuenta sus particularidades y formatos. Esta realiza el análisis del documento para la extracción de los valores de dato deseados. Debido a la ausencia de patrones de notación en parte del corpus seleccionado, hubo una dificultad adicional para la extracción de información. La estrategia adoptada para resolver este problema fue, junto a las etiquetas meta y atributos de la Tabla Tags, la inclusión de los marcadores de inicio y fin de cada elemento. El objetivo es relacionarlos en el algoritmo por el nombre. Esto sería suficiente para documentos bien formados y válidos según la notación W3C.
3.  Extracción del contenido de los códigos fuente, exportación a una hoja de cálculo y almacenamiento para su consulta y análisis posterior.

La [figura 8](#figura8) muestra una captura de pantalla de la aplicación MetadadosHTML.

<figure id="figura8">

![ Figura 8\. Captura de pantalla de MetadadosHTML](../p740fig8.png)

<figcaption>

Figura 8\. Captura de pantalla de MetadadosHTML</figcaption>

</figure>

Los datos extraídos reflejan la variabilidad de la problemática descrita anteriormente, con campos vacíos, porque no son utilizados por un periódico, o ruido informativo por la inclusión de datos que no corresponden al metadato que se analiza.

<table id="tabla11"><caption>

**Tabla 11\. Información sobre el metadato "palabras clave" para su extracción con MetadadosHTML**</caption>

<thead>

<tr>

<th>Orden</th>

<th>Línea de código</th>

<th>Medios de dónde extraer</th>

<th>Nota</th>

</tr>

</thead>

<tbody>

<tr>

<td>1</td>

<td>

`<meta name="`**`keywords`**`" content="">`
</td>

<td>101; 121; 123; 202; 311; 401; 601; 602; 603; 701; 711; 721</td>

<td>Para 201 se utiliza 4, 5, 6 y 7 Campo repetible en 701</td>

</tr>

<tr>

<td>2</td>

<td>

`<meta name="`**`news_keywords`**`" content="">`
</td>

<td>122; 731; 801</td>

<td>Para 201 se utiliza 4, 5, 6 y 7</td>

</tr>

<tr>

<td>3</td>

<td>

`<meta property="article:`**`tag`**`" content="">`
</td>

<td>301; 901</td>

<td>Campo repetible</td>

</tr>

<tr>

<td>4</td>

<td>

`<meta name="`**`des`**`" content="">`
</td>

<td>201</td>

<td>Palabras clave</td>

</tr>

<tr>

<td>5</td>

<td>

`<meta name="`**`org`**`" content="">`
</td>

<td>201</td>

<td>Organizaciones</td>

</tr>

<tr>

<td>6</td>

<td>

`<meta name="`**`per`**`" content="">`
</td>

<td>201</td>

<td>Personas</td>

</tr>

<tr>

<td>7</td>

<td>

`<meta name="`**`geo`**`" content="">`
</td>

<td>201</td>

<td>Lugares</td>

</tr>

</tbody>

</table>

Una posible solución pasa por establecer un orden de extracción, en el que se defina, en cada periódico, qué esquema utilizar y con qué condiciones (repetición del proceso, por ejemplo). La [tabla 11](#tabla11) indicaría el orden de extracción para las palabras clave.

## Conclusiones.

La literatura sobre estructuras para la descripción de documentos y sobre cómo aplicarlos en sistemas de información es abundante (véanse las numerosas referencias sobre ISBD, MARC, FRBR, EAD y DC, por ejemplo ([Chen and Ke, 2013](#chen2013); [Hillman, 2005](#hillman2005)). Sin embargo, son pocos los trabajos que, como ([Rovira and Marcos, 2006](#rovira2006)), analizan el uso de estos esquemas, menos en los códigos fuente de documentos web. Tampoco abundan las aportaciones que ahondan en las prácticas de aplicación de estos esquemas, como el ya citado ([Rovira and Marcos, 2006](#rovira2006)), ó ([Chuttur, 2014](#chuttur2014)), que destaca la reducción del número de errores cuando, en la asignación de metadatos se usan códigos de buenas prácticas. Ya en el área de medios de comunicación impresa, ([Valdés Pérez, Alemán Jiménez, and García Rivas, 2016](#valdes2016)), entre otras cuestiones, analizan la descripción de sitios de prensa cubana con metadatos y subrayan que, menos de un tercio de los portales analizados utilizan meta-etiquetas correctamente. Este trabajo aporta nuevas evidencias sobre el estado actual de la descripción de noticias web mediante metadatos. Asimismo, puede resultar útil para mejorar la precisión en la recuperación de noticias web en sistemas de información y su detección por web crawlers, programas de rastreo de páginas web a través de sus enlaces que actúan sobre el código fuente de las páginas web ([Blázquez Ochando, 2013](#blazquez2013)).

La variedad de esquemas de metadatos hallados en los códigos fuente puede considerarse ligada a los distintos propósitos por los que se desarrollan y utilizan: 1) Facilitar la interoperabilidad entre sistemas mediante elementos puestos en común, como schema.org (SC) o Dublin Core (DC); 2) Optimizar la búsqueda y recuperación de información en motores, como las etiquetas meta keywords, news_keywords o los esquemas de The New York Times, The Wall Street Journal; 3) Mejorar la visualización de la información a través de redes sociales (esencialmente Facebook y Twitter); 4) Realizar acciones de seguimiento y analítica web, como Sailthru o Webtrends. Esta variedad de esquemas, con distintos grados de aplicación, muestra que ningún estándar ha logrado la aceptación global. Por tanto, sigue siendo necesario avanzar hacia ‘estándares para que los datos estén disponibles en distintos formatos, que hagan más versátil su reutilización’ ([Pastor Sánchez, 2016](#pastor2016)). La evolución en la implantación de esquemas de metadatos o, mejor aún, su uso generalizado, es una de las cuestiones que marcará el camino hacia el intercambio y recuperación de información en y entre sistemas, sin importar el cambio  
de tecnología. Para ello, se debe trabajar en que los metadatos sean: únicos, estables, seguros, de acceso público y persistentes (Vásquez Paulus, 2008). En este sentido, ([García-Marco, 2013](#garcia2013)) destaca dos esquemas fundamentales como candidatos: a) schema.org, cuyo uso va extendiéndose en la descripción física y semántica de documentos web; b) Dublin Core (DC), bien asentado en la comunidad científica, ya utilizado para diversos tipos de documentos, como revistas científicas ([Felipe, 2016](#felipe2016)), el más estable y extendido, y aspirante preferente para la descripción de recursos digitales ([Piedra _et al._, 2015](#piedra2015); [Vásquez Paulus, 2008](#vasquez2008)).

Una segunda conclusión tiene que ver con la calidad de los metadatos, esto es, con la forma de expresar los valores de datos. Se han encontrado prácticas divergentes como el uso de una o varias líneas de código, la inclusión de un valor en un dato que no corresponde (nombre del medio entre las palabras clave, por ejemplo). Esto, de nuevo, dificulta la extracción de información, por cuanto es necesario no sólo conocer esquemas y atributos para cada tipo de información y periódico, también qué prácticas sigue cada medio. Además, se ha observado que algunos periódicos cometen errores al referir los nombres de los atributos de esquemas estandarizados, lo que impediría la extracción de información siguiendo los esquemas. Resulta, por tanto, imprescindible la adopción de buenas prácticas en el marcado de las noticias, en la forma en que los estándares lo indican. Una buen comienzo sería explicitar qué esquemas de metadatos utilizan los periódicos, documentando qué se utiliza y cómo, con textos explicativos y espacios de nombre. En definitiva, se trata de crear una cultura de metadatos ([Vásquez Paulus, 2008](#vasquez2008)).

En cuanto a MetadadosHTML, en su estado actual, está limitado por posibles cambios en los códigos fuente de los medios. Si un periódico redefine alguna de sus etiquetas meta o comienza a utilizar un nuevo esquema, el software no puede aprovecharlas, lo que obliga a una continua revisión. Por otro lado y, en relación con las conclusiones antes indicadas, ejemplifica la dificultad de recoger todos los metadatos de códigos fuente, pues no siempre están disponibles, no se (re)conocen los esquemas utilizados o no siguen pautas comunes en la adición de los valores de dato. De acuerdo con esto, se debería definir una política de indización común entre los periódicos, que esté alineada con los principales estándares de metadatos. Es evidente que la simple adopción de metadatos no es suficiente para el descubrimiento y accesibilidad de los documentos. En muchos sistemas de recuperación de información dichos documentos no serán objeto de análisis ni de indización. Los resultados iniciales llevan al establecimiento de un orden de preferencia en la extracción de valores de datos de unos esquemas frente a otros. Ello modificaría el software teniendo en cuenta frecuencia de uso de los esquemas y el grado de normalización de las prácticas de cada medio. El propósito es asegurar la correcta extracción y mayor cantidad de valores de datos con el menor número de procesos.

## <a id="author"></a>Sobre los autores

**María-José Baños-Moreno**, becaria predoctoral en el Doctorado en Gestión de la Información y de la Comunicación en las Organizaciones, Departamento de Ingeniería de la Información y las Comunicaciones, Facultad de Informática de la Universidad de Murcia, Campus de Espinardo, C.P. 30100, Murcia, + 34 868888787, [mbm41963@um.es](mbm41963@um.es). ORCID No. [orcid.org/0000-0001-9137-1330](http://orcid.org/0000-0001-9137-1330)  
**Eduardo R. Felipe**, mestrado em Ciências da Informação, Escola de Ciência da Informação, Universidade Federal de Minas Gerais, da UFMG, Avenida Antônio Carlos, 6627 - Pampulha, Belo Horizonte - MG, 31270-901, Brasil, +55 31 3409-5249, [erfelipe@hotmail.com](http://orcid.org/0000-0003-0735-3856)  
**Gercina Lima**, professor adjunto da Universidade Federal de Minas Gerais e Coordenadora do Programa de Pós Graduação em Ciência da Informação da ECI/UFMG, Escola de Ciência da Informação, Universidade Federal de Minas Gerais, da UFMG, Avenida Antônio Carlos, 6627 - Pampulha, Belo Horizonte - MG, 31270-901, Brasil, +55 31 34095232, [glima@eci.ufmg.br](mailto:glima@eci.ufmg.br). ORCID No. [orcid.org/0000-0003-0735-3856](http://orcid.org/0000-0003-0735-3856)  
**Juan Antonio Pastor-Sánchez**, profesor contratado doctor del Departamento de Información y Documentación de la Universidad de Murcia, Facultad de Comunicación y Documentación de la Universidad de Murcia, Campus de Espinardo, C.P. 30100, Murcia, [pastor@um.es](mailto:pastor@um.es), +34 868 887252\. ORCID No. [orcid.org/0000-0002-1677-1059](http://orcid.org/0000-0002-1677-1059)  
**Rodrigo Martínez-Béjar**, Catedrático, Departamento de Ingeniería de la Información y las Comunicaciones, Facultad de Informática de la Universidad de Murcia, Campus de Espinardo, C.P. 30100, Murcia, + 34 868888787, [rodrigo@um.es](mailto:rodrigo@um.es). ORCID No. [orcid.org/0000-0002-9677-7396](http://orcid.org/0000-0002-9677-7396)

[Abstract in English](#abs)

> **Introduction.** The objectives of this work are to determine which schemes are used for title, abstract, keywords, authorship and newspaper in press; to know what guidelines newspapers follow in the implementation of these schemes; and to find out how this affects the extraction of information.  
> **Methodology**. For this purpose, a newspaper sample is defined and its source code is analysed, identifying the schemas used and usage patterns. This allows us to extract data values using the MetadadosHTML application.  
> **Results**. Standard, _ad hoc_ and newspaper schemes have been detected. Various practices have been found, such as values grouped in the same line of code or separately; noise in a value; and errors when referring to the names of the attributes of standard schemas. These issues affect data extraction based on metadata and metadata schemas in MetadadosHTML  
> **Conclusions**. It is necessary to make progress in the use of standard schemas such as Dublin Core or schema.org, favouring the implementation of these (or others) in the news source codes. It is also imperative to adopt good practices in making explicit data and data values. Only in this way is it possible to evolve interoperability between systems and the retrieval and reuse of information.

</section>

<section>

## References

<ul>
<li id="abadal2009">Abadal, E., &amp; Codina, L. (2009). Bases de datos documentales. Características, funciones
y método. Madrid: Síntesis.</li>

<li id="abbud2010">Abbud Grácio, J. C., &amp; Fadel, B. (2010). <a
href="http://www.webcitation.org/6mIsBSOPu">Estratégias de preservação digital</a>. In <em>Gestão, mediação
e uso da informação</em> (pp. 58–83). São Paulo: Editora UNESP; Cultura Acadêmica. Retrieved from
http://books.scielo.org/id/j4gkh/pdf/valentim-9788579831171-04.pdf (Archived by WebCite&reg; at
http://www.webcitation.org/6mIsBSOPu) </li>

<li id="aenor2012">AENOR. (2012). <em>Información y documentación. Metadatos para la gestión de documentos.
Parte 3: Método de auto-evaluación.</em> UNE-ISO/TR 23081-3. AENOR.</li>

<li id="axelsson2006">Axelsson, J., Birbeck, M., Dubinko, M., Epperson, B., Ishikawa, M., McCarron, S.,&nbsp;
Pemberton, S. (Eds.). (2006). <a href="http://www.webcitation.org/6mItkzTcv">XHTML 2.0 (W3C Working Draft 26
July 2006)</a>. W3C. Retrieved from http://www.w3.org/TR/2006/WD-xhtml2-20060726. (Archived by WebCite&reg;
at http://www.webcitation.org/6mItkzTcv)</li>

<li id="banos2015">Baños-Moreno, María-José, Felipe, E. R., Pastor-Sánchez, J. A., Martínez-Béjar, Rodrigo,
&amp; Lima, G. (2015). <a href="http://www.webcitation.org/6mItul0Zm">Metadatos en noticias: un análisis
internacional para la representación de&nbsp; contenidos&nbsp; en periódicos</a>. Presented at the II
Congreso ISKO España y Portugal XII Congreso ISKO, Facultad de Comunicación y Documentación, Universidad de
Murcia, Murcia (España). Retrieved from
http://www.iskoiberico.org/wp-content/uploads/2015/11/43_Ba%C3%B1os.pdf (Archived by WebCite&reg; at
http://www.webcitation.org/6mItul0Zm)</li>

<li id="banos2013">Baños-Moreno, M. J., Pastor-Sánchez, J. A., &amp; Martínez-Béjar, R. (2013). <a
href="http://www.webcitation.org/6mIu20sMG">Propuesta de actualización de macro-tesauros a partir de
noticias de divulgación científico-tecnológica</a>. In <em>Informação e/ou Conhecimento: as duas faces de
Jano</em> (pp. 99–112). Porto (Portugal): Faculdade de Letras da Universidade do Porto CETAC.MEDIA.
Retrieved from http://hdl.handle.net/10760/20684 (Archived by WebCite&reg; at
http://www.webcitation.org/6mIu20sMG)</li>

<li id="baptista2001">Baptista, A. A., &amp; Barbosa Machado, A. (2001). <a
href="http://www.webcitation.org/6mIu9aivT">Metadata usage in an online journal - an application
profile</a>. In A. Hübler, P. Linde, &amp; J. W. . Smith (Eds.), <em>Electronic Publishing ’01 - 2001 in the
Digital Publishing Odyssey</em> (pp. 59–64). Kenterbury, UK: University of Kent. Retrieved from
http://elpub.scix.net/cgi-bin/works/Show?200106 (Archived by WebCite&reg; at
http://www.webcitation.org/6mIu9aivT)</li>

<li id="bi3d2013">BI3D. (2013). <a href="http://www.webcitation.org/6nnp1tX1b">Believe in 3D!</a> [Video de
Youtube]. Retrieved from https://www.youtube.com/user/BelieveIn3D (Archived by WebCite&reg; at <a
href="http://www.webcitation.org/6nnp1tX1b">http://www.webcitation.org/6nnp1tX1b</a>)</li>

<li id="blazquez2013">Blázquez Ochando, M. (2013). <a href="http://www.webcitation.org/6mKsd4M3a">Nuevos retos
de la tecnología web crawler para la recuperación de información</a>. <em>MÉI: Métodos de Información,
4</em>(7), 115–128. Retrieved from https://dialnet.unirioja.es/servlet/articulo?codigo=4767039 (Archived by
WebCite&reg; at http://www.webcitation.org/6mKsd4M3a) </li>

<li id="bohannon2012">Bohannon, P., Dalvi, N., Filmus, Y., Jacoby, N., Keerthi, S., &amp; Kirpal, A. (2012).
Automatic Web-scale Information Extraction. In <em>Proceedings of the 2012 ACM SIGMOD International Conference
on Management of Data</em> (pp. 609–612). New York, NY, USA: ACM.</li>

<li id="burgues2009">Burgués, M. (2009, February 20). <a href="http://www.webcitation.org/6mKssmHLz">Obtención
automática de metadatos de páginas Web para mejorar la ordenación de los resultados de una búsqueda</a>.
Rosario (Argentina). Retrieved from http://rephip.unr.edu.ar/xmlui/handle/2133/3252 (Archived by WebCite&reg;
at http://www.webcitation.org/6mKssmHLz) </li>

<li id="chen2013">Chen, Y. N., &amp; Ke, H. R. (2013). FRBRoo-based approach to heterogeneous metadata
integration. <em>Journal of Documentation, 69</em>(5), 623–637.</li>

<li id="chuttur2014">Chuttur, M. Y. (2014). <a href="http://www.webcitation.org/6mKt4u6W5">Investigating the
effect of definitions and best practice guidelines on errors in Dublin Core metadata records</a>.
<em>Journal of Information Science, 40</em>(1), 28–37. </li>

<li id="diaz2008">Díaz, L., Granell, C., Beltrán Fonollosa, A., Llaves, A., &amp; Gould, M. (2008). <a
href="http://www.webcitation.org/6mKt8U3Ax">Extracción semiautomática de metadatos: hacia los metadatos
implícitos</a>. In <em>Actas de las II Jornadas de SIG Libre</em>. Girona, Spain: Mar. Retrieved from
http://hdl.handle.net/10256/1160 (Archived by WebCite&reg; at http://www.webcitation.org/6mKt8U3Ax)</li>

<li id="felipe2016">Felipe, E. R. (2016). A importância dos metadados em bibliotecas digitais da organização à
recuperação da informação. In <em>Biblioteca digital hipertextual. Caminhos para a Navegação em Contexto</em>
(pp. 159–180). Rio de Janeiro: Interciência.</li>

<li id="garcia2013">García-Marco, F. J. (2013). Schema.org: la catalogación revisitada. <em>Anuario ThinkEPI,
7</em>(0), 169–172.</li>

<li id="garshol2004">Garshol, L. M. (2004). <a href="http://www.webcitation.org/6mKtJ83cT">Metadata? Thesauri?
Taxonomies? Topic Maps! Making sense of it all</a>. <em>Journal of Information Science, 30</em>(4), 378–391.
</li>

<li id="google2016">Google Inc. (2016). <a href="http://www.webcitation.org/6mKtPjoPL">Ayuda para editores:
Palabras clave y consultas de búsqueda</a>. Retrieved from
https://support.google.com/news/publisher/answer/68297?hl=es&nbsp; (Archived by WebCite&reg; at
http://www.webcitation.org/6mKtPjoPL) </li>

<li id="guallar2012">Guallar, J., Abadal, E., &amp; Codina, L. (2012). <a
href="http://www.webcitation.org/6mKtVH1I9">Hemerotecas de prensa digital. Evolución y tendencias</a>.
<em>El Profesional de La Información, 21</em>(6), 595–605. Retrieved from http://hdl.handle.net/10760/18199
(Archived by WebCite&reg; at http://www.webcitation.org/6mKtVH1I9) </li>

<li id="guallar2016">Guallar, J., Abadal, E., &amp; Codina, L. (2016). Hemerotecas digitales de prensa. In
<em>Calidad en sitios web. Método de análisis general, e-Commerce, imágenes, hemerotecas y turismo</em> (pp.
129–152). Barcelona: UOC.</li>

<li id="harris2007">Harris, J. (2007, October 23). <a href="http://www.webcitation.org/6mKtbX12J">Messing around
with metadata</a>. [Web log entry]. <em>New York Times</em>. Retrieved from
http://open.blogs.nytimes.com/2007/10/23/messing-around-with-metadata/ (Archived by WebCite&reg; at
http://www.webcitation.org/6mKtbX12J)</li>

<li id="hickson2014">Hickson, I., Berjon, R., Faulkner, S., Leithead, T., Navara, E. D., O’Connor, E., &amp;
Pfeiffer, S. (Eds.). (2014, October 28). <a href="http://www.webcitation.org/6mKtfYne4"><em>HTML5. A
vocabulary and associated APIs for HTML and XHTML</em></a> (W3C Recommendation 28 October 2014). W3C.
Retrieved from https://www.w3.org/TR/html5/document-metadata.html#the-meta-element (Archived by WebCite&reg;
at http://www.webcitation.org/6mKtfYne4) </li>

<li id="hillman2005">Hillman, D. (2005, November 7). <a href="http://www.webcitation.org/6mKtmZzXl">Using Dublin
Core (DCMI Recommended Resource)</a>. Retrieved from http://dublincore.org/documents/usageguide/ (Archived
by WebCite&reg; at http://www.webcitation.org/6mKtmZzXl)</li>

<li id="james2013">Janes, D. (2013, August). <a href="http://www.webcitation.org/6mKtpzT0k">hAtom 0.1 [Draft
Specification]</a>. Retrieved from http://microformats.org/wiki/hatom (Archived by WebCite&reg; at
http://www.webcitation.org/6mKtpzT0k)</li>

<li id="kallipolitis2012">Kallipolitis, L., Karpis, V., &amp; Karali, I. (2012). Semantic search in the World
News domain using automatically extracted metadata files. <em>Knowledge-Based Systems, 27</em>(1), 38–50.</li>

<li id="lajusticia2000">Lajusticia, M. R. B. (2000). <a href="http://www.webcitation.org/6mKu5H6vL">Estructura
textual, macroestructura semántica y superestructura formal de la noticia</a>. <em>Estudios sobre el mensaje
periodístico</em>, (6), 239–258. Retrieved from http://dialnet.unirioja.es/servlet/articulo?codigo=184782
(Archived by WebCite&reg; at http://www.webcitation.org/6mKu5H6vL) </li>

<li id="leitlhead2012">Leitlhead, T., Pfeiffer, S., Navara, &amp; O’Connor, E. (Eds.). (2012, August 22). <a
href="http://www.webcitation.org/6mKuAh69j">HTML5. A vocabulary and associated APIs for HTML and XHTML</a>
(Editor’s Draft 22 August 2012). W3C. Retrieved from
http://dev.w3.org/html5/spec-preview/Overview.html#contents<br>(Archived by WebCite&reg; at
http://www.webcitation.org/6mKuAh69j) </li>

<li id="ortiz1999">Ortiz-Repiso Jiménez, V. (1999). <a href="http://www.webcitation.org/6oFpBAdwG">Nuevas
perspectivas para la catalogación: Metadatos Versus Marc</a>. <em>Revista española de Documentación
Científica, 22</em>(2), 198–219. Retrieved from
http://redc.revistas.csic.es/index.php/redc/article/view/338/546 (Archived by WebCite&reg; at
http://www.webcitation.org/6oFpBAdwG)</li>

<li id="parker2016">Parker, A. (2016, February 14). <a href="http://www.webcitation.org/6mKuKYdDY">Jeb Bush adds
a weapon (his brother) despite worries it could misfire</a>. <em>New York Times</em>. Retrieved from
http://www.nytimes.com/2016/02/15/us/politics/jeb-bush-uses-secret-weapon-his-brother-despite-worries-it-could-misfire.html
(Archived by WebCite&reg; at http://www.webcitation.org/6mKuKYdDY)</li>

<li id="parsesf">Parse.ly. (s.f.). <a href="http://www.webcitation.org/6mKuP2ZJI">Technical documentation. The
metadata tag</a>. Retrieved from https://www.parsely.com/docs/integration/metadata/jsonld.html (Archived by
WebCite&reg; at http://www.webcitation.org/6mKuP2ZJI) </li>

<li id="pastor2016">Pastor Sánchez, J. A. (2016, January 29). <a
href="http://www.webcitation.org/6mKuU29rd">Quince años de web semántica: de las tecnologías a las buenas
prácticas</a>. IWETEL. Retrieved from https://listserv.rediris.es/cgi-bin/wa?A2=IWETEL;4919cf48.1601E
(Archived by WebCite&reg; at http://www.webcitation.org/6mKuU29rd) </li>

<li id="pastor2011">Pastor-Sánchez, J. A. (2011). <em>Tecnologías de la web semántica</em> (Edición: 1).
Barcelona: Editorial UOC, S.L.</li>

<li id="pereira2004">Pereira, T., &amp; Baptista, A. A. (2004). <a
href="http://www.webcitation.org/6mKuZsoDv">Incorporating a semantically enriched navigation layer onto an
RDF metadatabase</a>. In J. Engelen, S. M. S. Costa, &amp; A. C. S. Moreira (Eds.), <em>Building digital
bridges: linking cultures, commerce and science.</em> Brasília. Retrieved from
http://repositorium.sdum.uminho.pt/handle/1822/604 (Archived by WebCite&reg; at
http://www.webcitation.org/6mKuZsoDv)</li>

<li id="piedra2015">Piedra, N., Chicaiza, J., Quichimbo, P., Saquicela, V., Cadme, E., López, J., … Tovar, E.
(2015). Marco de Trabajo para la Integración de Recursos Digitales Basado en un Enfoque de Web Semántica.
<em>RISTI - Revista Ibérica de Sistemas e Tecnologias de Informação,</em> (spe3), 55–70.</li>

<li id="rovira2006">Rovira, C., &amp; Marcos, M. C. (2006). <a
href="http://www.webcitation.org/6mKujdb0h">Metadatos en revistas-e de Documentación de libre acceso</a>.
Retrieved from http://eprints.rclis.org/9343/ (Archived by WebCite&reg; at
http://www.webcitation.org/6mKujdb0h)</li>

<li id="sailthru2013">Sailthru. (2013, October 1). <a href="http://www.webcitation.org/6mKumGv3m">Custom Horizon
Tags</a>. Retrieved from http://getstarted.sailthru.com/site/horizon-overview/custom-horizon-tags/ (Archived
by WebCite&reg; at http://www.webcitation.org/6mKumGv3m)</li>

<li id="valdes2016">Valdés Pérez, H. L., Alemán Jiménez, Y., &amp; García Rivas, D. (2016). <a
href="http://www.webcitation.org/6mKwNpd00">Evaluación de la prensa digital cubana respecto a la
optimización de motores de búsqueda</a>. Serie científica, 9(5), 58–60. Retrieved from
http://publicaciones.uci.cu/index.php/SC/article/view/1766 (Archived by WebCite&reg; at
http://www.webcitation.org/6mKwNpd00)</li>

<li id="vallez2010">Vallez, M., Rovira, C., Codina, L., &amp; Pedraza-Jiménez, R. (2010). <a
href="http://www.webcitation.org/6mKuy6oWw">Procedimientos para la extracción de palabras clave de páginas
web basados en criterios de posicionamiento en buscadores</a>. Hipertext.net, 8. Retrieved from
http://www.upf.edu/hipertextnet/numero-8/extraccion_keywords.html (Archived by WebCite&reg; at
http://www.webcitation.org/6mKuy6oWw)
</li>

<li id="vasquez2008">Vásquez Paulus, C. (2008). <a href="http://www.webcitation.org/6mKv1WyKU">METADATOS:
Introducción e historia</a>. Retrieved from https://users.dcc.uchile.cl/~cvasquez/introehistoria.pdf
(Archived by WebCite&reg; at http://www.webcitation.org/6mKv1WyKU) </li>

<li id="whatwg2016a">Web Hypertext Application Technology Working Group (WHATWG). (2016a, February 4). <a
href="http://www.webcitation.org/6mKv6eQSe">Microdata</a>. Retrieved from
https://html.spec.whatwg.org/multipage/microdata.html#names:-the-itemprop-attribute (Archived by WebCite&reg;
at http://www.webcitation.org/6mKv6eQSe)</li>

<li id="whatwg2016b">Web Hypertext Application Technology Working Group (WHATWG). (2016b, February 4). <a
href="http://www.webcitation.org/6mKvAG02K">Standard metadata names</a>. Retrieved from
https://html.spec.whatwg.org/multipage/semantics.html#standard-metadata-names (Archived by WebCite&reg; at
http://www.webcitation.org/6mKvAG02K) </li>

<li id="yaginuma2003a">Yaginuma, T., Pereira, T., &amp; Baptista, A. A. (2003a). <a
href="http://www.webcitation.org/6mKvFExb7">Design of metadata elements for digital news articles in the
omnipaper project</a>. In S. M. de Souza Costa, J. A. Carvalho, A. A. Baptista, &amp; A. C. Santos Moreira
(Eds.), <em>From information to knowledge: 7th ICCC/IFIP International Conference on Electronic
Publishing</em> (pp. 132–139). Minho, Portugal: Universidade do Minho. Retrieved from
http://repositorium.sdum.uminho.pt/handle/1822/170 (Archived by WebCite&reg; at
http://www.webcitation.org/6mKvFExb7)</li>

<li id="yaginuma2003b">Yaginuma, T., Pereira, T., &amp; Baptista, A. A. (2003b). <a
href="http://www.webcitation.org/6mKvJVT2w">Metadata elements for digital news resource description</a>. In
<em>Proceedings CLME’2003 - 3o Congresso Luso-Moçambicano de Engenharia</em> (pp. 1317–1326). Maputo.
Retrieved from http://repositorium.sdum.uminho.pt/handle/1822/279 (Archived by WebCite&reg; at
http://www.webcitation.org/6mKvJVT2w)</li>
</ul>

</section>

</article>