<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Metatheory or methodology? Ethnography in library and information science

## [Brian L Griffin](#author)

> **Introduction.** Ethnography has become popular in library and information science, though researchers seldom describe their methodology clearly. Ethnography is also described as a metatheory and a methodology. However, ethnography is best understood as a methodology, which inherits ontological assumptions from the metatheory with which it is being used.  
> **Theoretical argument.** Metatheory is similar to one of Thomas Kuhn’s meanings of paradigm. Paradigms share common ontological stances. Ethnography is not a metatheory but a methodology because ethnographic methods-as-tools, such as interviewing and participant observation, can be used with different paradigms.  
> **Analysis.** In library and information science, each of the four main metatheories (scientific, critical, constructivist, and postmodern) is associated with different types of ethnography based on shared worldview or ontological assumptions. Ethnographic methods-as-tools can be used in different types of ethnography and are often adapted to reflect a study’s ontological assumptions, as illustrated in information research examples.  
> **Conclusions.** Information researchers should explicitly articulate their ontological stance and theoretical paradigm and explain how their methodological choices, including the use of ethnography, are consistent with their stance and paradigm. This would help eliminate confusion about whether ethnography is a metatheory or methodology and improve the alignment of theory to method of ethnographic research.

<section>

## Introduction

Given its origins in libraries and public service, library and information science was originally concerned with cataloguing documents and objects, and the discipline’s primary practice was bibliography ([Rayward, 1983](#ray83)). However, library and information science scholars are increasingly interested in the social and cultural implications of libraries, archives, and museums, and the discipline has expanded to include new fields and concerns, including critical information studies, information systems design, and knowledge management, among others. Two recent manifestations of these disciplinary changes include the creation of the iCaucus and the associated rebranding of existing departments and faculties as iSchools in the early 2000s as well as the renaming of the American Society for Information Science to American Society for Information Science and Technology (ASIST) in 2000\. This new emphasis on social, cultural, and technological issues requires new theories and methodologies, and library and information science scholars frequently turn to other disciplines, including anthropology, sociology, and philosophy, for the tools necessary to address these new concerns ([Buckland, 1999](#buc99); [Thomas and Nyce, 1998, p. 112](#tho98)). These changes, in part, explain the proliferation of papers and studies that aim to define and understand the nature of information (e.g., [M. J. Bates, 2006](#bat06), [2010](#bat10); [Capurro and Hjørland, 2003](#cap03); [Cornelius, 2002](#corn02); [Zins, 2007b](#zin07b)) and the boundaries and subfields of the discipline itself (e.g., [M. J. Bates, 2005](#batmj05); [Case, 2012](#cas12); [Dillon, 2012](#dil12); [Zins, 2007a](#zin07a)).

Meanwhile, ethnography has become a popular methodology within library and information science ([Carlsson, Hanell, and Karolina, 2013](#car13); see [Khoo, Rozaklis, and Hall, 2012](#kho12)). A wide range of studies in library and information science are ethnographies, including diverse topics like the information needs of senior citizens ([Chatman, 1992](#cha92)), the meanings of reading among lesbians ([Rothbauer, 2005](#rot05)), and human-computer interactions ([Prasad, 1997, pp. 109–11](#pra97)). In "the best overview of approaches to information science available" ([Hjørland, 2007, p. 1448](#hjo07)), Bates describes ethnography as one of several metatheoretical approaches and as a "technique… to enable the researcher to become immersed in a culture, identify its many elements, and begin to shape an understanding of the experience and world views of the people studied" ([M. J. Bates, 2005, p. 12](#batmj05)). She also acknowledges that "ethnographic methods" have been used by "both nomothetically and idiographically oriented researchers" ([M. J. Bates, 2005, p. 12](#batmj05)). This latter point implies that ethnography is better understood as a methodology, rather than a metatheory or theoretical approach because it can be combined with either nomothetic or idiographic theoretical perspectives. Here, methodology refers to the "the process of how we seek out new knowledge[,]…[t]he principles of our inquiry[,] and how inquiry should proceed" ([Lincoln, Lynham, and Guba, 2011, p. 102, Table 6.5](#lin11)). In addition, recent work calls for the development of a deeper understanding of the range of metatheoretical approaches that inform the use of ethnographic methodologies and methods in library and information science ([Carlsson et al., 2013](#car13); [Khoo et al., 2012, p. 86](#kho12)). I address this need by answering the questions: in library and information science, is ethnography a metatheory or methodology, and what are the implications of this for ethnographic research in library and information science?

The necessary first step is to define and examine the relationships between paradigm, theory, and methodology, paying particular attention to Bates’s ([2005](#batmj05)) discussion of these concepts. I explain that paradigms are consistent with a coherent set of metatheoretical assumptions that support particular theoretical approaches. In addition, while some observational methods of data collection, such as interviewing and participant observation, can be combined with different types of paradigms or theoretical approaches, the use of such methods may vary depending on the ontological assumptions of the paradigm or theory itself. That is, sometimes ethnography, as a methodology, is interpretive, and other times it may be used to test hypotheses, as in positivist or realist ethnography. To illustrate and justify this position, I then discuss a range of library and information science studies, including those mentioned by Bates ([2005](#batmj05)), that vary in their use of ethnography and theoretical perspectives, which is consistent with ethnography being a methodology rather than metatheory. I conclude by recommending that researchers in library and information science make more explicit the ontological assumptions underpinning their theoretical approach and take greater care that their methodological approach be consistent with those assumptions.

## What is a metatheory, or paradigm? And, how is it related to theory and methodology?

Bates ([2005](#batmj05)) defines a metatheory as "the philosophy behind the theory, the fundamental set of ideas about how phenomena of interest in a particular field should be thought about and researched" (p. 2). Further, Bates claims that the metatheory concept has "a lot of overlap with the term ‘paradigm’ which was given its modern understanding in science by Thomas Kuhn" ([2005, p. 3](#batmj05)). Kuhn ([1996 [1962]](#kuh96)) originally introduced the concept of paradigm in his study of the nature of scientific progress, or accumulation of scientific knowledge. In response to widespread criticism of the ambiguous nature of the use of the word paradigm in his work ([Case, 2012, p. 144](#cas12); [Masterman, 1970](#mas70); [Ritzer, 1975, p. 156](#rit75)), Kuhn later offered two refined senses of the concept. First, paradigm represents "the entire constellation of beliefs, values, techniques, and so on shared by the members of a given community" ([Kuhn, 1996, p. 175](#kuh96)). This is a sociological understanding of paradigm, which focuses on the social nature of science and the shared beliefs and methods of a particular scientific community. Second, paradigms are the "concrete puzzle-solutions which, employed as models or examples, can replace explicit rules as a basis for the solution of the remaining puzzles of normal science" ([Kuhn, 1996, p. 175](#kuh96)). In the social sciences, these "puzzle-solutions" are bodies of theoretical explanations or approaches that share common ontological and epistemological assumptions. This second sense of paradigm is consistent with Bates’s characterization of metatheories as "philosophy" and "fundamental set of ideas" ([2005, p. 2](#batmj05)). It is also similar to the use of ontology to refer to "worldviews" that encompass assumptions about the nature of knowledge ([Lincoln et al., 2011, p. 102](#lin11)).

Other information scholars have also taken up Kuhn’s notion of paradigm, generating a sometimes inconsistent set of categories or types of approaches in the discipline (for example, in information behaviour, see [Talja, Tuominen, & Savolainen 2005](#tal05), [Case 2007](#cas12)). Case ([2007](#cas12)), in particular, uses the term paradigm in a way that is consistent with a set of ontological assumptions that guide theory and method. In addition, Bawden and Robinson ([2012 chapter 3](#baw12)) discuss both metatheories and paradigms as distinct ways of organizing approaches and research in library and information science. Bawden and Robinson ([2012, pp. 42–7](#baw12)) equate paradigms with different "turns" in library and information science, or the rise of different substantive topics or areas of interest in particular decades, including systems, cognitive, and socio-cognitive paradigms. This use of the term "paradigm" emphasizes the popularity of a new approach or a collective shift (or turn) in disciplinary focus among researchers, and appears most consistent with Kuhn’s first sense of the term, which is the more sociological understanding. Bawden and Robinson ([2012, pp. 40–1](#baw12)) discuss separately the primary metatheories in library and information science, which include realism, constructivism, and critical theory. They explain that metatheories derive from particular ontological or philosophical assumptions about the world, which is similar to Kuhn’s second sense of paradigm and Bates’s use of the term.

This second sense of paradigm, as a set of theoretical propositions or tools, is also the one more commonly referenced in general discussions about ontology, epistemology, and methodology in the social sciences. Although Lincoln, Lynham & Guba ([L2011](#lin11)) are not library and information science scholars, research in library and information science spans all the social science paradigms they identify, making it a useful categorization for organizing library and information science research. For example, they ([Lincoln et al., 2011, p. 98](#lin11)) explain that variations in ontological, epistemological, and methodological beliefs and approaches are what separates and divides the principal paradigms in the social sciences: positivism, postpositivism, critical theory, constructivism, and postmodernism. For example, they explain that both positivism and postpositivism assume there exists a "real" reality, although they differ in the extent to which scientists can measure or observe that reality without error ([Lincoln et al., 2011, p. 98, Table 6.1](#lin11)). As a result, these paradigms favor more scientific methods that test nomothetic theories, or general theoretical laws ([Lincoln et al., 2011, pp. 102–4, Table 6.5](#lin11)). In contrast, critical approaches (e.g., feminist theory) and constructivism start from either an ontological position that emphasizes the ways reality is constructed through historical processes of oppression (critical approaches) or through "local" and subjective experiences (constructivism) ([Lincoln et al., 2011, p. 98, Table 6.1](#lin11)). These ontological stances reject the "objective" claims of scientific methodologies or nomothetic theory, and instead, make a case for participatory (critical) or hermeneutic (constructivism) methodologies ([Lincoln et al., 2011, p. 104, Table 6.5](#lin11)). Goodman ([2011, p. 2](#goo11)) articulates a similar list of relevant paradigms that use ethnographic methods in library and information science: positivist, critical, constructivist, ecological and social network.

Consequently, methodologies are informed by the theoretical and ontological (or paradigmatic) stances that researchers assume. As such, the methodology, or principles of research strategy, associated with different paradigms will vary. However, claiming that methodologies are informed by the worldviews of the theories or ontologies that motivate them does not preclude researchers with different ontological stances from using the same method-as-tool, or strategy of data collection and analysis. They will just use the method in different ways or for different ends ([Denzin and Lincoln, 2005, p. 649](#den05)). For example, postpositivists and critical theorists do not share the same ontological assumptions about the world. Postpositivists assume there is a reality out there to be discovered, albeit imperfectly. In contrast, critical theorists assume that reality is a reflection of historical processes of oppression and exploitation (cf. [Lincoln, Lynham & Guba 2011, p. 102](#lin11)). However, both types of research may use informant interviews, though researchers will use interviews to collect different data and use that data in different ways. Postpositivists may ask questions about events, and would take those accounts as imperfect accounts of the past. Such accounts could be used as evidence to test scientific hypotheses about why the event happened. In contrast, critical theorists would use interviews to understand the ways in which systems of privilege or oppression are experienced or sustained. Responses would be treated as evidence that can be leveraged to reveal the inequities and injustice of social relations. In each instance, the types of questions and the ways the answers would be used as evidence would be different. This simple example illustrates the idea that ethnography, too, might span multiple paradigms, or ontological positions. If so, then ethnography is best understood as a methodology, which inherits different characteristics depending upon the particular theoretical paradigm within which it is used. In other words, there is not only one type of ethnography but many.

## Ethnography in library and information science: One or many?

In library and information science, researchers often describe their study as an ethnography or as having used ethnographic methods without providing detailed information about the actual methods of data collection or taking a clear metatheoretical or ontological stance ([Khoo et al., 2012, p. 86](#kho12)). This reflects a general problem in qualitative library and information science research, which tends to be "unclear and lax" in its description and discussion of methodology or method ([Cibangu, 2013, p. 204](#cib13)). This tendency to minimize discussion of methodology, theory or metatheory may in part be a functional response to the typical structure of an article in library and information science journals, which is implicitly based on scientific or positivistic principles and does not lend itself to interpretive or critical narratives ([Khoo et al., 2012, p. 85](#kho12)). In library and information science, the general term ethnography may also be misunderstood because it is often used to describe various ethnographic methods or tools, most of which are flexible enough to be combined with different theoretical and ontological perspectives. For example, a recent content analysis of research methods used in library and information science included "ethnography" as a method alongside ethnographic tools, such as interviewing, focus groups, and observation ([Chu, 2015](#chu15)). Thinking about ethnography as a collection of ethnographic methods has the advantage of acknowledging that ethnography, as a methodology, does not represent a single method-as-tool. In fact, ethnographic methods include a range of data collection and analysis tools, such as questionnaires, focus groups, interviewing, narrative interviewing, field site descriptions, visual inventory, participant observation, audio/visual recordings, self-reporting, and researcher reflection (see [Agar, 2004](#aga04); [Collier and Collier, 1986, pp. 45–64](#col86); [Emerson, Fretz, and Shaw, 2011](#eme11); [Goodman, 2011](#goo11); [Hartel and Thomson, 2011](#har11); [J. A. Bates, 2005](#batja05); [Khoo et al., 2012](#kho12); [Spradley, 1980](#spr80)).

This ambiguity in the use and meaning of the term ethnography in library and information science also contributes to confusion and debate because different researchers are often familiar with particular types of ethnography or ethnographic methods, which may or may not be those familiar to other researchers. For example, an early debate about the use of ethnography between Sandstrom and Sandtrom ([1995](#san95), [1998](#san98)) and Nyce and Thomas ([1999](#nyc99); [Thomas and Nyce, 1998](#tho98)) largely revolved around whether ethnographic methods could or should be used for nomothetic (or scientific) ends or idiographic (or hermeneutic) ends in library and information science. Each side of the debate asserted the superiority or claimed as the exemplar their preferred version of ethnography. Sandstrom and Sandstrom ([1995, p. 167](#san95), [1998](#san98)) assumed a positivist or postpositivist stance and argued in favor of the scientific (or realist) use of ethnography, which they traced back to Boas and Malinowski (e.g. [Erickson, 2011, pp. 45–8](#eri11)). Meanwhile, Nyce and Thomas ([1999](#nyc99); [Thomas and Nyce, 1998](#tho98)) also tried to suggest that there was only one valid way to do ethnography, and though they would prefer to imply that they describe the ethnographic method (see [Thomas and Nyce, 1998, pp. 111–2](#tho98)), they described what others call interpretive ethnography (e.g. [Geertz, 1973](#gee73)). In a sense, both sides of the debate correctly described a particular type of ethnographic methodology associated with a particular theoretical and ontological stance but incorrectly asserted the superiority of their preferred stance. Their disagreement was not so much about method as it was about ontology (or, paradigm and metatheory).

Within library and information science, ethnographic methods-as-tools are used to answer many types of research questions. As a discipline, library and information science was originally concerned with administrative research questions that aimed to improve library (and later other information) service provision, often from a scientific ontological position ([Case, 2012, p. 144](#cas12); [Goodman, 2011, p. 3](#goo11)). Library and information science has since expanded to include a wider range of research questions that extend beyond libraries to other information settings and take non-scientific ontological stances, including critical theory, constructivism, and postmodernism ([Buckland, 1999](#buc99); [Case, 2012, pp. 144–5](#cas12); [Goodman, 2011, p. 5](#goo11); [Kari and Hartel, 2007](#kar07); [M. J. Bates, 2005, pp. 9–14](#batmj05); [Talja, Tuominen, and Savolainen, 2005](#tal05)). Since library and information science now encompasses a wide range of metatheories (or paradigms), including positivism, postpositivism, critical approaches, constructivism, and postmodernism, ethnography in library and information science will inherit different assumptions and aims from each paradigm. This is consistent with understandings of ethnography in anthropology, which recognizes that ethnography can be used according to different methodological principles associated with different ontological positions. In other words, ethnography can be scientific, interpretive, critical, or reflexive (among others) depending on the ontological stances upon which it is based ([Erickson, 2011](#eri11); [Goodman, 2011, p. 2](#goo11); see also [Khoo et al., 2012](#kho12)).

Based on the preceding discussion of the relationship between paradigm (or metatheory), theory, and ethnography, Table 1 provides a partial overview of paradigms (or metatheories) and their use of ethnography and ethnographic methods in library and information science. First, scientific paradigms (either positivist or postpositivist) refer to studies that share an ontological assumption that reality exists, though they differ in the extent to which they believe we can accurately observe that reality ([LLincoln et al., 2011, p. 98](#lin11)). In library and information science, this broad paradigm encompasses a range of theoretical approaches, including theories in information behavior (e.g., social network theory or browsing behavior) and other approaches informed by biology or evolutionary theory ([M. J. Bates, 2005](#batmj05)). When information researchers use ethnography within this paradigm, they use a version of ethnography that assumes that researchers are able to uncover reality through observations and interaction with participants. This is consistent with the earliest types of ethnography practiced by anthropologists, such as Boas, Malinowski, or Harris ([Erickson, 2011, p. 47](#eri11); [Sandstrom and Sandstrom, 1998, p. 167](#san98)). In addition, within this paradigm in library and information science, ethnography is more likely to be "rapid" ([Khoo et al., 2012, p. 86](#kho12)) or "focused" ([Knoblauch and Tuma, 2011](#kno11)) over shorter periods of time or asking narrower research questions. For example, Pettigrew ([2000](#pet00)) describes the use of ethnographic methods within a scientific paradigm. This study examines information behavior in a health-care setting, and emphasizes the strategies used to minimize bias or observer effect and ensure trustworthy data collection "to ensure reliability and validity of this research study… as appropriate to either the naturalistic or positivist paradigm" ([Pettigrew, 2000, p. 58](#pet00)). This included a combination of semi-structured interviews and participation observation and careful coding of interview and fieldwork notes.

<table><caption>

**Table 1: Paradigms, ethnography, and ethnographic methods in library and information science**</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="4">Paradigm (or metatheory)</th>

</tr>

<tr>

<th>Scientific (positivist/post-positivist)</th>

<th>Critical theory</th>

<th>Constructivism (interpretivist)</th>

<th>Postmodern</th>

</tr>

<tr>

<td>Examples of theoretical frameworks</td>

<td>social network theory, browsing behavior, evolutionary, physical</td>

<td>feminist theory, queer theory</td>

<td>actor-network theory, cognitive constructivism, social constructivism</td>

<td>post-colonialism, constructionist, discourse-analytic</td>

</tr>

<tr>

<td>Ontological assumptions</td>

<td>Reality exists (postpositivism: though our ability to observe it may be limited)</td>

<td>historical realism (Lincoln, Lynham, & Guba 2011, p. 98) that reflects patterns of oppression</td>

<td>reality is individually or socially constructed</td>

<td>Reality is socially constructed through discourse and co-creation</td>

</tr>

<tr>

<td>Methodological assumptions</td>

<td>Should use scientific methods (or the best approximate) to rigorously test theoretical propositions</td>

<td>Should use participatory and emancipatory methods that challenge hegemony and injustice</td>

<td>Should use methods that uncover individual or community meanings</td>

<td>Should use methods that deconstruct representations of reality (or discourse) & that are participatory or democratic</td>

</tr>

<tr>

<td>Types of ethnography</td>

<td>realist ethnography, cognitive ethnography</td>

<td>critical ethnography</td>

<td>interpretive ethnography, cognitive ethnography</td>

<td>reflexive ethnography, autoethnography, performance ethnography</td>

</tr>

<tr>

<td>Types of ethnographic method (or method-as-tool)</td>

<td>questionnaires, focus groups, interviews, participant, observation,</td>

<td>interviews, personal experience, participant observation,</td>

<td>focus groups, interviews, participant observation, member checks</td>

<td>participant observation, personal experience</td>

</tr>

<tr>

<td>Library and information science examples or reviews</td>

<td>Chatman 1992; Pettigrew 2000; Wilson & Streatfield 1981; Kwasnik 1992</td>

<td>Myers 1997; Rothbauer 2004</td>

<td>Prasad 1997; Chatman 1999; Turkle 1984; Lee & Trace 2009; Lingel & boyd 2013</td>

<td>Latour & Woolgar 1986</td>

</tr>

<tr>

<td colspan="5">

Note: Author’s elaboration based on Bates ([2005](#batmj05)); [Talja, Tuominen, and Savolainen (2005)](#tal05); Goodman ([2011](#goo11)); and Lincoln, Lynham, and Guba ([2011](#lin11)).</td>

</tr>

</tbody>

</table>

Second, critical theory takes a "historical realist" ontological stance that assumes that reality reflects historical patterns of oppression and injustice ([Lincoln et al., 2011, p. 98](#lin11)). Examples of theoretical approaches that take a critical ontological stance include feminist and queer theory. Consequently, critical theorists require methods that challenge oppressive structures and provide opportunities for participation and emancipation of people who are oppressed ([Lincoln et al., 2011, p. 104](#lin11)). This implies that the dominant ethnographic methodology is critical ethnography, which "involves the researchers participating in an ongoing dialog and discussion with those who are being studied" in order to uncover and challenge systems of oppression ([Erickson, 2011, pp. 51–02](#eri11); [Goodman, 2011, p. 2](#goo11)). Critical ethnography is also preoccupied with critical hermeneutics, which "critici[zes] … nondialectical views of ethnographic work" and instead requires that the researcher take a critical stance embedded in a particular set of historically situated "social, economic and political relationships" ([Myers, 1997, pp. 282, 283](#mye97)). Critical ethnographers may use a variety of ethnographic methods to create this dialogue, including interviews, participant observation, informal social contact, document analysis and personal experience (if the researcher is also a member of the oppressed community). For example, Myers ([1997](#mye97)) describes a critical ethnography of the development of an information system in mental health in New Zealand ([Young, 1995](#you95)). According to Myers ([1997](#mye97)), the study combined ethnographic methods, such as participant observation and structured and unstructured interviews, with critical analysis of documents, such as unpublished meeting minutes and news reports to produce a critical ethnography ([Myers, 1997, p. 284](#mye97)). In particular, this critical ethnography focused on the ways that the imposition of "time-based costing," or tracking of worker effort based on time spent in different activities, was resisted by clinicians who saw this feature as an effort by management and government to limit their autonomy ([Myers, 1997, pp. 289–93](#mye97)). The attempts to include this feature in the information system was understood in the context of a broader project of government reform and efficiency efforts, which was historically situated ([Myers, 1997, pp. 289–90](#mye97)).

Third, constructivism is another major paradigm in the social sciences, including library and information science. The theoretical perspectives included under this paradigm all share an ontological assumption that realities are "relative and local" and "constructed or co-constructed" ([Lincoln et al., 2011, p. 98](#lin11)). In information behavior, two theoretical approaches that share this ontological position are constructivism (or cognitive constructivism) and collectivism (or social constructivism). Both approaches share the assumption that reality is constructed, though they differ as to whether the reality is shaped by individual (cognitive constructivism) or collective (social constructivism) experiences (Talja et al., 2005, p. 81). For example, Chatman’s ([1999, p. 212](#cha99)) study of the information poverty of female prison inmates highlights the ways in which norms of behavior and use of information are socially constructed, particularly by prison insiders who have more information about the prison system. In constructivism, methodological approaches have a hermeneutic or interpretive aim, which means they seek to uncover the meanings present in individually or socially constructed experiences ([Lincoln et al., 2011, p. 104](#lin11)). Thus, the most common type of ethnography combined with constructivist approaches is interpretive ethnography, which is generally associated with Geertz’s ([1973](#gee73)) critique of realist ethnography and call for "thick description" in anthropology (in LIS see [Goodman, 2011, p. 2](#goo11)). The actual ethnographic methods used may include focus groups, interviews, and participant observation. In some instances, preliminary interpretations are also presented to participants to verify the findings (e.g., [Lingel and boyd, 2013, pp. 984–5](#lin13)). Overall, interpretive ethnography is used to understand how experiences shape individually and socially constructed meanings.

Finally, in postmodernism, the last major paradigm discussed here, reality is assumed to be socially constructed, particularly through discourse, but also is subjectively experienced by the researcher ([Lincoln et al., 2011, p. 102](#lin11); [Talja et al., 2005, pp. 89–90](#tal05)). This ontological stance implies the use of methodologies that are reflexive, involve deconstruction of text or discourse, and promote democratic and "co-creation" of meaning ([Lincoln et al., 2011, pp. 104–5](#lin11)). Postmodern ethnography demands reflexivity and co-participation of research subjects and researchers and most often uses reflexive ethnography, autoethnography, or performative ethnography (Erickson, 2011, pp. 52–3; [Hamera, 2011](#ham11); [Spry, 2011](#spr11)). These methodologies embrace the deconstruction of text by imagining alternative constructions of research reports or envisioning alternative, participatory ways of engaging with the research subject ([Erickson, 2011, pp. 52–3](#eri11)). For example, Latour and Woolgar’s ([1986, pp. 277–9](#lat86)) study of the scientific practices at the Salk Labs emphasizes the ways in which scientists socially construct "scientific" facts and seeks to deconstruct such practices through ethnographic methods that required that the researchers become scientists and directly participate in the construction of science. Although, strictly speaking, Latour and Woolgar ([1986](#lat86)) is not a library and information science study, Talja, Tuominen, and Savolainen ([2005, p. 92](#tal05)) cite this study as an example of constructionism.

## Discussion and conclusions

The preceding explanation highlights the ways in which ethnography in library and information science spans four major paradigms (or metatheories) that share fundamental ontological assumptions about the nature of reality. These include (primarily) realist, critical, interpretive, and reflexive ethnography, which correspond generally to scientific, critical, constructivist, and postmodern approaches respectively. Each type of ethnography inherits a set of ontological assumptions that reflect the paradigm with which they are associated. In practice, however, different types of ethnography (interpretive, critical, etc.) often employ very similar ethnographic methods, or tools for collecting and analyzing evidence. The most common methods-as-tools used in studies of all ethnographic types are interviews and participant observation. In addition, some studies combine ethnographic methods-as-tools with the analysis of documents, discourse, or other data sources. The distinction between ethnography as methodologies that reflect certain ontological assumptions and ethnographic methods as tools for collecting evidence is an important one. Also, not every study that uses interviews or participant observation need call itself ethnography. Even researchers that disagree about which type of ethnography is a superior or more appropriate methodology for library and information science can agree that it is important to not confuse methodology with method and to recognize that all types of ethnography must be consistent with each type’s ontological assumptions to effectively address important questions ([Thomas and Nyce, 1998, p. 111](#tho98)).

Unfortunately, frequently library and information science research that claims to use ethnography or ethnographic methods falls short of this standard. Often, it is difficult to identify the metatheory or paradigm that informs the use of ethnographic methods in library and information science because many studies do not report enough detail about the methods used or articulate an explicit ontological or methodological stance ([Khoo et al., 2012, p. 86](#kho12)). Furthermore, sometimes researchers use research methods in ways that are inconsistent with the ontological and methodological assumptions of the theoretical paradigm in which they work ([Talja et al., 2005, p. 82](#tal05)). These types of ambiguities might be eliminated if researchers were more aware of and made explicit their implicit methodological, theoretical, and ontological assumptions. This includes reconciling theory with method to ensure ontological consistency. However, existing norms in many library and information science journals, which promote a scientific narrative and privilege brevity over depth, limit the ability of researchers, particularly those using critical, interpretive, or postmodern approaches, to present the methods and findings of their work in sufficient detail (see also [Khoo et al., 2012, p. 86](#kho12)). Editors, peer-reviewers, and authors would all need to consciously change the implicit (and perhaps in some cases explicit) norms and standards to facilitate greater flexibility in the presentation of research in peer-reviewed library and information science publications.

Finally, to return to the original research question that motivated this paper: is ethnography a methodology or metatheory? The literature agrees that metatheory is similar to paradigm and refers to a set of assumptions about reality or worldview that shapes the theoretical and methodological approaches used within the paradigm. Furthermore, this discussion has demonstrated that there are multiple types of ethnography in library and information science, each associated (primarily) with a particular paradigm, or metatheory. This suggests that it does not make sense to generically claim to "do ethnography" or conduct an "ethnographic study." Instead, researchers need to think carefully about the ontological assumptions of their theoretical approach and ensure that it aligns with the type of ethnography they are doing, which will shape both how they gather evidence and to what ends they use it. These decisions should be explicit and clearly articulated. Furthermore, researchers should remember that ethnographic research, regardless of type, usually entails a number of specific ethnographic methods-as-tools (e.g., interviewing, participant observation), which in and of themselves (albeit with some exceptions) do not imply particular methodological or ontological stances.

## <a id="author"></a>About the author

**Brian L. Griffin** is a PhD candidate in the Faculty of Information at the University of Toronto, Canada. He can be contacted at brian.griffin@mail.utoronto.ca.

</section>

<section>

## References

<ul>
<li id="aga04">Agar, M. (2004). Ethnography. In N. J. Smelser &amp; Baltes (Eds.), International Encyclopedia of the Social &amp; Behavioral Sciences (Vol. 7, pp. 4857–4862). Amsterdam: Elsevier.
</li>
<li id="batja05">Bates, J. A. (2005). Use of narrative interviewing in everyday information behavior research. Library &amp; Information Science Research, 26(1), 15–28.
</li>
<li id="batmj05">Bates, M. J. (2005). An introduction to metatheories, theories, and models. In K. E. Fisher, S. Erdelez, &amp; L. McKechnie (Eds.), Theories of Information Behavior (pp. 1–24). Medford (N.J.): Information Today.
</li>
<li id="bat06">Bates, M. J. (2006). Fundamental forms of information. Journal of the American Society for Information Science and Technology, 57(8), 1033–1045.
</li>
<li id="bat10">Bates, M. J. (2010). Information behavior. Encyclopedia of Library and Information Sciences, 3, 2381–2391.
</li>
<li id="baw12">Bawden, D., &amp; Robinson, L. (2012). Introduction to information science. New York: Neal-Schuman.
</li>
<li id="buc99">Buckland, M. (1999). The landscape of information science: The American Society for Information Science at 62. Journal of the American Society for Information Science, 50(11), 970–974.
</li>
<li id="cap03">Capurro, R., &amp; Hjørland, B. (2003). The concept of information. Annual Review of Information Science and Technology, 37(1), 343–411.
</li>
<li id="car13">Carlsson, H., Hanell, F., &amp; Karolina, C., Hanna; Hanell, Fredrik; Lindh. (2013). Exploring multiple spaces and practices: a note on the use of ethnography in research in library and information studies. In Proceedings of the Eighth Internaitonal Conference on Conceptions of Library and Information Science (Vol. 18). Copenhagen, Denmark: Information Research.
</li>
<li id="cas12">Case, D. O. (2012). Looking for information: a survey of research on information seeking, needs and behavior. Emerald Group Publishing.
</li>
<li id="cha92">Chatman, E. A. (1992). The information world of retired women. Westport, CT: Greenwood Press.
</li>
<li id="cha99">Chatman, E. A. (1999). A theory of life in the round. JASIS, 50(3), 207–217.
</li>
<li id="chu15">Chu, H. (2015). Research methods in library and information science: A content analysis. Library &amp; Information Science Research, 37(1), 36–41.
</li>
<li id="cib13">Cibangu, S. K. (2013). A memo of qualitative research for information science: toward theory construction. Journal of Documentation, 69(2), 194–213.
</li>
<li id="col86">Collier, J., &amp; Collier, M. (1986). Visual anthropology: photography as a research method. UNM Press.
</li>
<li id="corn02">Cornelius, I. (2002). Theorizing information for information science. Annual Review of Information Science and Technology, 36(1), 392–425.
</li>
<li id="den05">Denzin, N. K., &amp; Lincoln, Y. S. (2005). Methods of collecting and analyzing empirical materials. In N. K. Denzin &amp; Y. S. Lincoln (Eds.), Handbook of qualitative research (3rd ed., pp. 641–649). Thousand Oaks, CA: Sage Publications.
</li>
<li id="dil12">Dillon, A. (2012). What it means to be an iSchool. Journal of Education for Library and Information Science, 53(4), 267–273.
</li>
<li id="eme11">Emerson, R. M., Fretz, R. I., &amp; Shaw, L. L. (2011). Writing ethnographic fieldnotes. University of Chicago Press.
</li>
<li id="eri11">Erickson, F. (2011). A history of qualitative inquiry in social and educational research. In N. K. Denzin &amp; Y. S. Lincoln (Eds.), The Sage handbook of qualitative research (Vol. 4, pp. 43–59). Thousand Oaks, CA: Sage Publications.
</li>
<li id="gee73">Geertz, C. (1973). The interpretation of cultures: selected essays. Basic Books.
</li>
<li id="goo11">Goodman, V. D. (2011). Applying ethnographic research methods in library and information settings. Libri, 61(1), 1–11.
</li>
<li id="ham11">Hamera, J. (2011). Performance Ethnography. In N. K. Denzin &amp; Y. S. Lincoln (Eds.), The Sage Handbook of Qualitative Research (4th ed., pp. 317–330). Thousand Oaks, CA: SAGE Publications.
</li>
<li id="har11">Hartel, J., &amp; Thomson, L. (2011). Visual approaches and photography for the study of immediate information space. Journal of the American Society for Information Science and Technology, 62(11), 2214–2224.
</li>
<li id="hjo07">Hjørland, B. (2007). Information: Objective or subjective/situational? Journal of the American Society for Information Science and Technology, 58(10), 1448–1456.
</li>
<li id="kar07">Kari, J., &amp; Hartel, J. (2007). Information and higher things in life: Addressing the pleasurable and the profound in information science. Journal of the American Society for Information Science and Technology, 58(8), 1131–1147.
</li>
<li id="kho12">Khoo, M., Rozaklis, L., &amp; Hall, C. (2012). A survey of the use of ethnographic methods in the study of libraries and library users. Library &amp; Information Science Research, 34(2), 82–91.
</li>
<li id="kno11">Knoblauch, H., &amp; Tuma, R. (2011). Videography. An interpretative approach to video-recorded micro-social interaction. The SAGE Handbook of Visual Research Methods, 414–430.
</li>
<li id="kuh96">Kuhn, T. S. (1996). The structure of scientific revolutions (3rd. ed.). Chicago: University of Chicago Press.
</li>
<li id="lat86">Latour, B., &amp; Woolgar, S. (1986). Laboratory life: the construction of scientific facts. Princeton University Press.
</li>
<li id="lin11">Lincoln, Y. S., Lynham, S. A., &amp; Guba, E. G. (2011). Paradigmatic controversies, contradictions, and emerging confluences, revisited. In N. K. Denzin &amp; Y. S. Lincoln (Eds.), The Sage handbook of qualitative research (Vol. 4, pp. 97–128). Thousand Oaks, CA: Sage Publications.
</li>
<li id="lin13">Lingel, J., &amp; boyd, danah. (2013). ‘Keep it secret, keep it safe’: Information poverty, information norms, and stigma. Journal of the American Society for Information Science and Technology, 64(5), 981–991.
</li>
<li id="mas70">Masterman, M. (1970). The nature of a paradigm. In I. Lakatos &amp; A. Musgrave (Eds.), Criticism and the Growth of Knowledge (pp. 59–89). New York: Cambridge University Press.
</li>
<li id="mye97">Myers, M. D. (1997). Critical ethnography in information systems. In A. S. Lee, J. Liebenau, &amp; J. I. DeGross (Eds.), Information systems and qualitative research: Proceedings of the IFIP TC8 WG 8.2 international conference on information systems and qualitative research (pp. 276–300). Philidelphia, PA.
</li>
<li id="nyc99">Nyce, J. M., &amp; Thomas, N. P. (1999). Can a‘ Hard’ Science Answer‘ Hard’ Questions? A Response to Sandstrom and Sandstrom. The Library Quarterly, 295–298.
</li>
<li id="pet00">Pettigrew, K. E. (2000). Lay information provision in community settings: How community health nurses disseminate human services information to the elderly. The Library Quarterly, 47–85.
</li>
<li id="pra97">Prasad, P. (1997). Systems of meaning: ethnography as a methodology for the study of information technologies. In Information systems and qualitative research (pp. 101–118). Springer.
</li>
<li id="ray83">Rayward. (1983). The development of library and information science: disciplinary differentiation, competition, and convergence. In Machlup, F. &amp; Mansfield, U. (Eds.), The Study of information: interdisciplinary messages (pp. 343–363). New York: Wiley and Sons.
</li>
<li id="rit75">Ritzer, G. (1975). Sociology: A Multiple Paradigm Science. The American Sociologist, 10(3), 156–167.
</li>
<li id="rot05">Rothbauer, P. (2005). Finding and creating possibility: reading in the lives of lesbian, bisexual and queer young women.
</li>
<li id="san95">Sandstrom, A. R., &amp; Sandstrom, P. E. (1995). The use and misuse of anthropological methods in library and information science research. The Library Quarterly, 161–199.
</li>
<li id="san98">Sandstrom, A. R., &amp; Sandstrom, P. E. (1998). Science and nonscience in Qualitative Research: A response to Thomas and Nyce. The Library Quarterly, 249–254.
</li>
<li id="spr80">Spradley, J. P. (1980). Participant observation. New York: Holt, Rinehart and Winston.
</li>
<li id="spr11">Spry, T. (2011). Performative Ethnography. In N. K. Denzin &amp; Y. S. Lincoln (Eds.), The SAGE handbook of qualitative research (pp. 497–511). London: Sage.
</li>
<li id="tal05">Talja, S., Tuominen, K., &amp; Savolainen, R. (2005). ‘Isms’ in information science: constructivism, collectivism and constructionism. Journal of Documentation, 61(1), 79–101.
</li>
<li id="tho98">Thomas, N. P., &amp; Nyce, J. M. (1998). Qualitative research in LIS: Redux: A response to a [re] turn to positivistic ethnography. The Library Quarterly, 108–113.
</li>
<li id="you95">Young, L. W. (1995). The implementation of an information system in mental health: a critical hermeneutical interpretation. University of Auckland.
</li>
<li id="zin07a">Zins, C. (2007a). Conceptions of information science. Journal of the American Society for Information Science and Technology, 58(3), 335–350.
</li>
<li id="zin07b">Zins, C. (2007b). Conceptual approaches for defining data, information, and knowledge. Journal of the American Society for Information Science and Technology, 58(4), 479–493.
</li>
</ul>

</section>

</article>