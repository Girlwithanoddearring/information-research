<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Handling inconsistencies between information modalities - workplace learning of newly qualified nurses

## [Anita Nordsteien](#author)

> **Introduction.** Information-related activities of nurses touch on questions of life and death, this makes their work life challenging, particularly when dealing with inconsistencies between different information sources. This paper provides insights into how, when and why newly qualified nurses respond to these information contradictions.  
> **Methods.** A longitudinal ethnographically inspired study was conducted on a nurse training programme in a hospital in Norway. Focus groups and interviews were used to collect data from 25 newly qualified nurses.  
> **Analysis.** Thematic analysis was carried out with NVivo10, and three themes were identified: reliance on information, challenging practice and complying with practice.  
> **Results.** Participation in a training programme appears to affect how newly qualified nurses handle information inconsistencies. Training-nurses seem to rely on theoretical information, and use this to substantiate and challenge existing practices. The new nurses adapt to the routines on their wards, but have a strong impetus to improve practice promoting their prior knowledge, motivation and values.  
> **Conclusion.** Theoretical information plays a major role in new nurses’ learning processes; they exchange this information with experienced colleagues in return for experiential information about critical situations. This generates mutual learning and development of practices in organisations.

<section>

## Introduction

A central issue in information studies is how information is used in the workplace context (cf. [Case, 2012](#cas12)). Hospitals are complex information environments, and health care professionals have to learn to handle different and sometimes contradictory types of information ([Bonner and Lloyd, 2011](#bon11); [Isah and Byström, 2016](#isa16); [Lloyd, 2009](#llo09)). Information work and negotiations between health care professionals are a substantial part of the work tasks in hospitals ([Strauss, 1985](#str85)). Nurses are a particularly interesting case for information studies for several reasons; they are the largest occupational group in hospitals, and pivotal for information processes between patients and their relatives, physicians and other health providers. Moreover, these information processes often deal with critical situations; the consequences may be fatal if information is not handled correctly. The view on information is, in this study, inspired by Lloyd ([2010, p. 12](#llo10)), who describes information as a ‘_product of a negotiated construction between individuals interacting with the artefacts, texts, symbols, actions and in consort with other people in context_’.

Workplace learning is a well-known research area in pedagogical and organisational contexts ([Fenwick, 2008](#fen08)), but there are only a few information studies related to workplace learning (e.g. [Bonner and Lloyd, 2011](#bon11); [Isah and Byström, 2016](#isa16); [Lloyd, 2009](#llo09); [Moring, 2011](#mor11)). Workplace learning is embedded in everyday work activities ([Billett, 2014](#bil14); [Fenwick, 2008](#fen08)). All employees at the workplace are continuously learning ([Billett, 2008](#bil08)), but learning may be particularly intensive for newcomers to a workplace ([Edwards, Hawker, Carrier and Rees, 2015](#edw15); [Moring, 2011](#mor11)). One significant challenge can be the transition from an education environment to the workplace ([Eraut, 2004](#era04); [Lloyd, 2009](#llo09), [2011](#llo11)). Recently, several training programmes for new nurses have been established to ease this transition process, and thereby prevent nurses leaving their workplace or profession already in the first year ([Edwards _et al_., 2015](#edw15)).

There are a vast number of information studies about different occupational groups (cf. [Case, 2012](#cas12)). Some of the more recent studies about the nursing profession explore information sources and activities in everyday nursing practice, which are generally related to daily administrative tasks, such as documentation and rapid reference enquiry textbooks, procedure handbooks and Internet use for practical information about diseases, diagnoses and procedures ([Ebenezer, 2015](#ebe15); [Hedman, Lundh and Sundin, 2009](#hed09); [MacIntosh-Murray and Choo, 2005](#mac05); [McKnight, 2006](#mck06), [2007](#mck07)). Factors that influence nurses’ relation to information are mainly time constraints ([Ebenezer, 2015](#ebe15); [McKnight, 2004](#mck04); [Spenceley, O'Leary, Chizawsky, Ross and Estabrooks, 2008](#spe08)). However, Spenceley and colleagues ([2008](#spe08)) indicate that healthcare and nurses’ information needs seem to change according to the demands for evidence-based knowledge and narrowing the research-practice gap. Another highly relevant focus is on the new generation of nurses; dealing with theoretical nursing information as a professional project, and its impact on nursing identity and power relations in the workplace ([Johannisson and Sundin, 2007](#joh07); [Sundin, 2002](#sun02), [2003](#sun03); [Sundin and Hedman, 2005](#sun05)). All these studies mainly focus on textual information sources. An exception is Bonner and Lloyd ([2011](#bon11)), who emphasise the importance of social and physical information sources in nursing practice.

Still, newly qualified nurses and the transition between nursing education and the workplace have received little attention in information studies. This study aims to supplement previous knowledge by focusing on information challenges of newly qualified nurses’ and the potential of training programmes for newcomers in the workplace. This paper addresses the following research question: How do newly qualified nurses handle inconsistencies between different information modalities in the workplace?

</section>

<section>

## Theoretical perspectives and concepts

The theoretical framework of this study is based on practice-oriented theories related to workplace learning ([Benner, 1984](#ben84); [Billett, 2014](#bil14); [Lloyd, 2010](#llo10); [Wenger-Trayner and Wenger-Trayner, 2015](#wen15)). These theories claim that learning is social and embedded in practice. Wenger-Trayner and Wenger-Trayner ([2015](#wen15)) emphasise that members of a community bring their personal experiences to a practice, which may challenge and change the socially defined competence of the community. However, most commonly the community transforms the experiences of individuals to align with the defined competence. Thus, inconsistencies between experience and competence are constantly negotiated, and learning happens through this process:

> Learning to become a practitioner is... developing a meaningful identity of both competence and knowledgeability in a dynamic and varied landscape of relevant practices... Practitioners need to negotiate their role, optimize their contribution, know where relevant sources of knowledge are, and be practiced at bringing various sources of knowledge to bear on unforeseen and ambiguous situations.([Wenger-Trayner and Wenger-Trayner, 2015](#wen15), pp. 23-24)

Another related view is that learning through work includes workplace affordances, its invitational qualities and how individuals choose to engage with these ([Billett, 2014](#bil14); [Hodkinson _et al_., 2004](#hod04)). Individuals bring their prior knowledge, experiences and skills, which they adapt and develop to their new workplace ([Hodkinson _et al_., 2004](#hod04)). Fuller, Hodkinson, Hodkinson and Unwin ([2005](#ful05)) find that newcomers in some cases are considered to be the experts (having the most up-to-date knowledge), and are passing on this knowledge and skills to their experienced colleagues. These contributions emphasise that social structures and individual agency are relational and interdependent, and that individuals construct knowledge and bring about both individual and social change when employing their capacities, interests and values in work. This is related to Giddens ([1984](#gid84)), who promotes the idea of an equal emphasis between these social structures and human agency as a mutually repeating duality. Giddens’ social structures include rules and resources that shape human agency, but these can be changed when humans reproduce them differently in their activities.

Practice-based learning in nursing is elaborated on in the influential study of Benner ([1984](#ben84)). Benner applies the Dreyfus model to nursing, which suggests that nurses move through five competency levels in their learning process: novice, advanced beginner, competent, proficient and expert. The individual nurse’s competence develops by a move from 1) reliance on theory, rules and procedures to the use of personal experience from concrete situations, 2) the view of a situation as a collection of equal parts to a complete whole with certain relevant parts, and 3) the act of a detached observer to an involved performer. The expert has a deep intuitive understanding of situations that involves tacit knowledge ([Benner, 1984](#ben84)).

In information studies, Lloyd ([2010](#llo10)) has connected different types of information to practice-based learning. Knowing is attached to information modalities, which refer to ‘_broad sites of information that are established within a context_’ (p. 161). Lloyd describes epistemic modalities as sites of factual and disciplinary know-why and know-that information: e.g. textbooks, guidelines and procedures. The social modalities are sites of information acquired through social interaction and reflection, e.g. experiential information, values and beliefs; tacit shared understandings that may contest epistemic information. The corporeal modalities represent the embodied know-how or practical tacit information acquired through sensory input, demonstration and observation of practice. Information coupling is the process where these modalities are drawn together, and newcomers become full participants of the practice. Experienced members introduce newcomers to the legitimised information and knowledge sites in the domain ([Lloyd, 2010](#llo10)).

</section>

<section>

## Methods

This paper presents some initial findings from a qualitative study in a two-year training programme for newly qualified nurses in a large community hospital in Norway. An ethnographically inspired approach was applied, involving different types of data collection over time to record how experiences potentially change when the individuals become more confident in their context ([Grbich, 2013](#grb13)). The training programme was established in August 2014 to enhance the nurses’ generalist competence by providing each nurse an opportunity to work on surgical, medical and psychiatric wards for periods of eight months. Clinical placements during their three-year undergraduate nursing education are normally 8-10 weeks in each of these fields (in addition to 20 weeks in community health services). The training-nurses have no preceptor or other support beyond what other new nurses have. However, the training-nurses meet once a month to participate in practical exercises and lectures concerning relevant procedures (instructions on how to perform patient-related tasks, such as catheterisation) in the hospital. To become a participant in the training programme, they are required to go through a normal job recruitment process.

### Sample

In August 2014, 12 newly qualified nurses were recruited to participate in the training programme as well as in this study, and in August 2015, six more nurses were recruited. Additionally, newly qualified nurses not participating in the training programme were recruited to highlight possible differences in information-related activities between those involved in training and those not. A list provided by the hospital wards of 15 new nurses in the summer of 2014 made it possible to contact them, and seven nurses agreed to participate. These nurses are working either on a surgical or medical ward; the majority are working 75% part-time in a temporary position, which is normal for the first years of employment. None of these seven nurses were applying for a position in the training programme. They explained that they wanted the stability of working on only one ward. Most of them were employed on the ward where they had their last clinical placement during their nursing education. In total, the participants included 22 women and three men aged between 24 and 48.

</section>

<section>

### Data collection

This paper presents the findings from the initial focus groups and interviews. Focus groups were chosen as a data collection method, because of the possibility to acquire concentrated data through group interaction on a specific topic ([Morgan, 1997](#mor97)). Semi-structured interview outline was e-mailed to all the participants a week prior to each focus group discussion. The first series of focus groups were conducted during the nurses’ first months in the workplace. This included two groups of six training-nurses in 2014, and one group of four training-nurses recruited in 2015\. It proved impossible to gather the non-training-nurses for a focus group due to their varying work shifts. As a result of this two interviews were conducted with two of the non-training-nurses and one interview with another three. The second series of focus groups and interviews were conducted a year later; two focus groups of respectively five and six training-nurses and one interview with three non-training-nurses. An iterative approach was used, striving to reach data saturation and construct meaning together with the participants ([Grbich, 2013](#grb13)).

Independent of the number of participants in each group, all discussions proved to be engaging and interactive. The nurses explained that they were used to discussing and responding to each other in reflection groups during their education. All the focus groups and interviews lasted about 60 minutes, and they were digitally recorded and transcribed verbatim into 170 pages. The first series of data collection involved questions concerning how the nurses use different information sources to cope with the theory - practice gap and the transition between education and workplace. Preliminary data analysis was undertaken during both data collection and transcription ([Grbich, 2013](#grb13)). One of the main themes that appeared was handling conflicting information from different information modalities, and this theme was further elaborated on in the second series of discussions.

</section>

<section>

### Ethical considerations

The researcher gained access to this study field, because the hospital sought an external partner for the evaluation of the training programme. Due to being viewed as an external partner, the participants seemed to talk freely about their situation at the hospital, thus it is extremely important to protect the confidentiality and anonymity of the participants. Written informed consent was obtained from all the participants and a representative of the hospital management. Names of the participants were replaced and assigned codes from N1 to N25, and names of the hospital and wards were removed in the transcriptions. The Norwegian Social Science Data Services approved the study in June 2014 (project no. 39107).

### Data analysis

Thematic analysis was conducted using NVivo10, inspired by the interpretative framework in table 1 of Brinkmann and Kvale ([2015](#bri15)). This consists of three interpretative levels of increasing abstraction: self-understanding represents the condensed meaning of the participants’ statements; a critical common-sense understanding means critically interpreting the content of statements or what statements may tell about the participants; a theoretical understanding connects statements to theory. In this paper the theme of information inconsistency is in focus. Through the data analysis, three sub-themes were identified: reliance on information, challenging practice and complying with practice.

<table><caption>Table 1: Interpretative framework</caption>

<tbody>

<tr>

<th>Self-understanding</th>

<th>Critical common-sense understanding</th>

<th>Theoretical understanding</th>

</tr>

<tr>

<td>

The new nurses related that their skilled colleagues handle critical situations very quickly;  
their hands just seem to know what to do and what will work</td>

<td>

Complying with practice:  
Experienced nurses as role models in critical situations</td>

<td>

Social structure:  
Competence of community  
Deep intuitive understanding  
Corporeal information modalities</td>

</tr>

<tr>

<td>Member validation</td>

<td>Audience validation</td>

<td>Peer validation</td>

</tr>

</tbody>

</table>

According to this framework, validation should be conducted by discussing each of the three different types of understanding with the respective participants (members), general public (audience) and research community (peers). This will be elaborated on later in the methodological considerations.

</section>

<section>

## Findings

The three sub-themes that emerged through the analysis are interconnected; the new nurses have to decide what information to rely on, and challenging practice and complying with practice include findings on _how, when_ and _why_ the new nurses behave in the face of inconsistencies.

### Reliance on information

Both groups of nurses related that there is a strong focus on complying with the medical procedures in the hospital information system. They explained that these research-based procedures are regularly updated, therefore, all employees are supposed to consult the procedures before conducting patient-related tasks. Even a year after starting their employment, the training-nurses reported that they consult procedures on a daily basis. However, there is often no time to locate and read the procedures during a shift, and some of the them expressed that they sometimes print out specific procedures to read at home in their spare time, so they can prepare for forthcoming work tasks. In cases where they are not able to prepare, the training-nurses reported a culture of consulting experienced colleagues as well as other health professionals. However, several of the training-nurses have experienced getting inaccurate answers when they ask their colleagues about procedures:

> There are experienced nurses to consult and a very good culture of asking questions, but I read the actual procedure before I go to the patients, because I can’t trust that colleagues are up-to-date on procedures, I’ve experienced that colleagues say something quite different from the current procedures. (Training-nurse N4)

A strategy used if the training-nurses felt that the information was incorrect was to ask another colleague. These nurses claimed that they ask colleagues a lot, but when it comes to procedures that they do not know, they double-check. They related that some of the most experienced nurses seem not to confer the up-dated procedures very often, or they claim that a procedure is wrong, and choose to do it another way. However, there were some cases where the experienced nurses told the training-nurses not to listen to them in case they were wrong, but to consult the procedure and come back and tell them what the right answer is.

The non-training-nurses pointed out that there might be reasons for deviating from the procedures in some cases, and there were several examples of trusting experienced colleagues without conferring the procedures:

> When you’ve got a lot to do, you don’t look up the procedure [...] then you rather go and ask the nurses who have been working there for three years how they do it, without knowing 100% if the response is right according to the procedure-system. (Non-training-nurse N15)

> They may not be the safest information source, as you said, when changes occur frequently, but they have worked here for several years and know a lot, and one has to have confidence in experienced colleagues too, sometimes at least. So, I ask a lot of questions, since I’ve only been working as a nurse for a year. (Non-training-nurse N14)

The medical procedures appear to be considered as the most legitimate information source for all nurses, but owing to time constraints, consulting colleagues emerges as the most frequent practice. However, getting inaccurate answers frustrates the new nurses, because they are afraid of doing anything wrong. Several of the non-training-nurses related that they were given too much responsibility without knowing about emergency procedures and other critical situations. They reported that they were anxious that patients would die on their shift, thus they constantly checked upon their patients and even dreamt about patient situations at night.

### Challenging practice

There are several examples, which indicate that the training-nurses are substantiating, questioning or challenging existing practices, even in the first months, when they are new to the workplace. These nurses bring epistemic knowledge from their education and the training programme. Many of the training-nurses said that they have insisted on doing specific tasks according to their epistemic knowledge, and not the way some of the experienced nurses do it. They related that their colleagues most often seem to be fine with that, but that they want to continue doing the tasks the way they always have done them. There are also some examples when the training-nurses have succeeded in convincing the experienced nurses, and have actually changed the practice on a ward.

The training-nurses expressed that it is important to live up to their own professional standards, and find their own identity as a nurse:

> I’ve experienced that nurses tell me not to do it that way [...] I want to do it that way, that’s what I’m comfortable with [...] You should maintain your good habits, although other nurses tell you not to do it that way, because they’re not used to it. This is the way I do it, because I know it’s justifiable and right. (Training-nurse N3)
> 
> Yes, that’s how to find your own identity and your professional standards [...] Even though the other nurses do it that way, I’ll not, I can’t justify doing it that way, because I want to be a skilled professional [...] In a way, it’s your responsibility to seek and acquire the knowledge you need. (Training-nurse N2)
> 
> You have to feel on your own, how you want to practice. (Training-nurse N8)

A theme that was discussed in the focus groups with the training-nurses was how to deal with situations when other nurses do something procedurally incorrect. Different strategies were used; most frequently asking carefully for an explanation. In other cases, telling the colleague that the procedures or other written sources indicate that it should be done in a different way. There were even examples of reporting errors electronically and notifying a superior:

> It’s important to report errors, otherwise the routines degenerate and people do things differently, and then it becomes very difficult without any continuity on the ward. So, you have to tighten procedures to get everybody on track again. (Training-nurse N20)
> 
> If you come to a ward where you notice that there is a culture where it’s not okay to speak out, then we as newcomers should try to initiate it. [...] I want to be up-to-date on the most recent information. I want to tell the patient that: ‘The most recent research shows…’ I want to be the best nurse. (Training-nurse N21)

Most commonly, the training-nurses appear to be respected and acknowledged for their epistemic knowledge. They reported that their nursing colleagues as well as the physicians listen to their clinical assessments.

The non-training-nurses also reported several occasions of questioning practice:

> When you’re a newcomer and try to convey new knowledge, it’s sometimes not appreciated. When we do as we’ve learned in our education, someone will come and tell us that we’re wrong: ’No, don’t do it that way’. ’Oh! Why? ’No, we do it another way here, that’s how we’ve done it for 20 years’. ’Oh, okay’. (Non-training-nurse N13)

However, the negotiation processes of the non-training-nurses seemed to end at the initial questioning. Some of these nurses related that they did not want to further confront the experienced nurses as, in contrast, the training-nurses generally did.

### Complying with practice

In certain situations, both groups of newly qualified nurses comply with practice. The outlined scenario in table 1 covers cases of critical situations like sudden cardiac arrest, which the experienced colleagues are characteristically said to handle swiftly and in an automatic manner. One non-training-nurse expressed that she feels confident with the routine-work, but not so much acute situations:

> I may observe that they [patients] become critically ill, and I document my observations, and inform the physician, but the quick responses... They [the experienced nurses] just say “I do it like this and then that…” They do it so fast. I hope I will be like them in a few years. I’m not just standing there doing nothing, but I see that they have it at their fingertips, and they know what works. (Non-training-nurse N14)

The training-nurses simulate such real life situations several times during their programme, and they express less concern about potential acute situations that may arise than the non-training-nurses. However, both groups of nurses acknowledge that they have to be exposed to several such situations to be able to handle them.

Research-based tools for improving patient safety have recently been implemented in the hospital concerned; one example is Modified Early Warning Score (MEWS), which is an assessment tool aiming to efficiently communicate patients’ condition between the staff in the hospital. The training-nurses are thoroughly informed and trained to use such tools, and expressed that they feel confident with these tools and find such overall guidelines useful. They even promote them amongst their experienced colleagues, who are more reluctant to use them, because they have not been given the same information and training to use such tools.

In cases when the procedures are inconsistent with the theoretical knowledge from the educational context, both groups of newly qualified nurses seem to reluctantly comply with the procedures:

> I have changed the way of doing this task, because now there is a new procedure. I have to comply with the procedure-system, even if I did it differently during my education. (Non-training-nurse N14)

The same thing also happens with regard to the daily routines on the wards and how work is organised. Some of the wards are said to be highly structured and characterised by teamwork and a good flow of information, while in others new nurses may be left more on their own and everything is more fluid. This is illustrated by the number of initial training days, which varies considerably between the wards. Some nurses reported having two training days before starting to work on their own, while others got three weeks. There was no difference between the two groups. However, the training-nurses seemed to get into their wards’ daily routines very quickly, but it was challenging for them to change ward after eight months, dealing with a completely different culture, information processes and way of working:

> It was okay to change to another ward, exciting... Nice people. But there are things you have to unlearn; some of the routines you acquired on the previous ward. (Training-nurse N10)

Both groups of nurses adapt to the routines and the way of working on the ward they are allocated. They comply with tools, guidelines and procedures they are supposed to use in the hospital, and they acknowledge the experience of their colleagues when it comes to critical situations.

</section>

<section>

## Discussion

Information-related activities are a significant part of workplace learning of newly qualified nurses. However, to be confident and competent nurses, they have to learn how to handle inconsistencies, and in some cases contradictions, between information sources. The first findings from this on-going ethnographically inspired study indicate that participation in training programmes affect how newcomers handle information inconsistencies. The training-nurses seem to rely more on theoretical information, and use it to substantiate and challenge the practices of their more experienced colleagues. In such cases, epistemic knowledge is being used to contest social knowledge. This finding appears to be contrary to what is usual, and contradicts the notion that experienced members in a workplace hold the power and introduce newcomers to the legitimised socially shared knowledge (cf. [Lloyd, 2010](#llo10)). As the present findings demonstrate, the relationship between these training-nurses and their experienced colleagues seems to be more equal. The type of information need decides which party might have the most relevant information, and who is the tutor and who is the learner. The training-nurses share the newest epistemic information with their colleagues in everyday situations, while in critical situations they turn to the skilled experienced nurses holding the experience-based tacit knowledge required (cf. [Benner, 1984](#ben84); [Lloyd, 2010](#llo10)). This interchanging of information sharing enables mutual learning in the workplace, which in turn leads to the development of practices.

Complying with practice seems mainly to be related to social structures (cf. [Giddens, 1984](#gid84)); both groups new nurses adapt to the routines and the way of working on the ward they are allocated. They comply with tools, guidelines and procedures they are supposed to use in the hospital, and they acknowledge the experience of their colleagues when it comes to critical situations. They are not yet able to handle complex situations requiring personal experience and a holistic understanding using various knowledge sources, which also Wenger-Trayner and Wenger-Trayner ([2015](#wen15)) emphasise. The nurses are aligning with the context and the desired competence of the community in such cases. However, the non-training-nurses trust and comply to a greater extent with their experienced colleagues in all situations, contrary to the training-nurses who in everyday situations constantly negotiate their role and use personal experience, and challenge the competence of the community.

Challenging practice is connected to individual agency, which constitutes a strong impetus to improve practice by questioning and substantiating based on the individual’s prior knowledge, motivation and personality. The training-nurses clearly transfer their prior knowledge to the workplace, mainly from their educational background, but also from former work experiences. They use the knowledge from these settings to at times contest the practices at hospital. New nurses are working more or less on their own after two days of training, even when facing critical situations. As with the studies of Billett ([2014](#bil14)), Fuller _et al_. ([2005](#ful05)) and Hodkinson _et al_. ([2004](#hod04)), individual agency seems to be a prominent factor in these findings; here there are aspects of motivation, personality and integrity, which affect the response to practice. Moreover, workplace affordances (cf. [Billett, 2014](#bil14); [Hodkinson _et al_., 2004](#hod04)), such as training programmes, appear to be important.

Sundin (e.g. [2002](#sun02); [2003](#sun03)) found that dealing with scientific information may be used as a strategy by the new generation of nurses to increase their professional status and identity. However, the main reason for relying on scientific information in this study seems to be a genuine commitment to the patients’ well-being; providing the patient with the best treatment possible. These training-nurses are ambitious, they want to be skilled professionals and conduct their work optimally, and the community in which they are working usually appreciate this attitude. In this context, epistemic and especially scientific knowledge seem to have more impact than reflected in related studies previously. 25 years of evidence-based practice may be one explanation for this (cf. [Spenceley _et al_., 2008](#spe08)), but also that new generations of nurses want to be knowledgeable; they want to refer to research in their practice, and this is a trend seen in society at a large, which as such can be seen as a continuation of the development of the nursing profession that Sundin addresses.

### Methodological considerations

This study will not be representative for nursing practice in general, because the context is a specific training programme in a country with relatively equal relations within and between professions. Moreover, the participants are selected and regarded as highly skilled. There is also an imbalance in the number of participants of the two groups of nurses, which makes it difficult to compare the groups. However, the study gives some indications of what can be accomplished by suitable workplace affordances for learning. Member validation (table 1) of the main findings of this study was conducted through a meeting with the participants (cf. [Brinkmann and Kvale, 2015](#bri15)). The training-nurses confirmed what information they rely on and in what situations they respectively challenge or comply with practice. Audience validation was conducted through evaluation meetings with the training-nurses’ managers and trainers, who confirmed and were enthusiastic about the new epistemic impulses the training-nurses bring to their wards and their impetus to substantiate practice. Peer validation was conducted by presenting and discussing the findings with both nursing and information science researchers.

</section>

<section>

## Conclusion

The findings of this study emphasise the importance of the affordances of the workplace environment to support new employees’ ability to handle conflicting information. These affordances relate both to the openness of the employees to new information and the opportunities in the workplace for sharing information. This study demonstrates that a workplace training programme can facilitate new employees’ transition between education and the workplace. This process takes place by acknowledging and supporting the newcomers’ up-to-date epistemic knowledge, and by demonstrating how this knowledge can be integrated with practical knowledge and experience. The participants in this workplace training programme want to ground their clinical patient work on epistemic information. The new generation of nurses seem to be comfortable navigating in the digital information landscape; they know where to find reliable information and how to use it, which has become a critical skill in health care. The new nurses share this information with their experienced colleagues, who in turn share experiential information about how to handle atypical or critical situations with the new nurses. Thus, the training-nurses are both tutors and learners, and this enables the information sharing to be fluid and less hierarchical, since different information is needed for different work tasks. This exchange of information allows mutual learning between experienced and new members in a practice. In conclusion, the training-nurses are not passively transitioned into the practice and sustaining it; there appears to be a significant degree of transition of practices when these nurses are supplementing or countering them. These findings point to areas of information practice research that are less studied, but that are clearly of value for better understanding information sharing in organisations and the development of practices.

</section>

<section>

## Acknowledgements

The author would particularly like to thank the participants of the study, supervisor Katriina Byström, nurse educator May-Elin Thengs Horntvedt, copy-editors Nicola Johnston and Kristin Solli and the anonymous reviewers for their valuable contributions to this paper.

## <a id="author"></a>About the author

**Anita Nordsteien** is a PhD student at the Department of Archivistics, Library and Information Science, Oslo and Akershus University College of Applied Sciences, Norway. Her research interests include information practices and workplace learning in healthcare. She received her Bachelor's degrees in Library and Information Science and in Radiography and her Master's degree in Value-Based Leadership. She can be contacted at [Anita.Nordsteien@hioa.no](mailto:anita.nordsteien@hioa.no).

</section>

<section>

## References

<ul>
<li id="ben84">Benner, P. (1984). <em>From novice to expert: excellence and power in clinical nursing practice.</em> Menlo Park, CA: Addison-Wesley.
</li>
<li id="bil08">Billett, S. (2008). Learning throughout working life: a relational interdependence between personal and social agency. <em>British Journal of Educational Studies, 56</em>(1), 39-58.
</li>
<li id="bil14">Billett, S. (2014). Learning in the circumstances of practice. <em>International Journal of Lifelong Education, 33</em>(5), 674-693.
</li>
<li id="bon11">Bonner, A. &amp; Lloyd, A. (2011). What information counts at the moment of practice? Information practices of renal nurses. <em>Journal of Advanced Nursing, 67</em>(6), 1213-1221.
</li>
<li id="bri15">Brinkmann, S. &amp; Kvale, S. (2015). <em>InterViews: learning the craft of qualitative research interviewing</em> (3rd ed.). Thousand Oaks, CA: Sage.
</li>
<li id="cas12">Case, D. O. (2012). <em>Looking for information: a survey of research on information seeking, needs, and behavior</em> (3rd ed.). Bingley, UK: Emerald.
</li>
<li id="ebe15">Ebenezer, C. (2015). Nurses' and midwives' information behaviour: a review of literature from 1998 to 2014. <em>New Library World, 116</em>(3/4), 155-172.
</li>
<li id="edw15">Edwards, D., Hawker, C., Carrier, J. &amp; Rees, C. (2015). A systematic review of the effectiveness of strategies and interventions to improve the transition from student to newly qualified nurse. <em>International Journal of Nursing Studies, 52</em>(7), 1254-1268.
</li>
<li id="era04">Eraut, M. (2004). Transfer of knowledge between education and workplace settings. In H. Rainbird, A. Fuller &amp; A. Munro (Eds.), <em>Workplace learning in context</em> (pp. 201-221). London: Routledge.
</li>
<li id="fen08">Fenwick, T. (2008). Workplace learning: emerging trends and new perspectives. <em>New Directions for Adult and Continuing Education, 2008</em>(119), 17-26.
</li>
<li id="ful05">Fuller, A., Hodkinson, H., Hodkinson, P. &amp; Unwin, L. (2005). Learning as peripheral participation in communities of practice: a reassessment of key concepts in workplace learning. <em>British Educational Research Journal, 31</em>(1), 49-68.
</li>
<li id="gid84">Giddens, A. (1984). <em>The constitution of society: outline of the theory of structuration</em>. Cambridge: Polity Press.
</li>
<li id="grb13">Grbich, C. (2013). <em>Qualitative data analysis: an introduction</em> (2nd ed.). London: Sage.
</li>
<li id="hed09">Hedman, J., Lundh, A. &amp; Sundin, O. (2009). <em>Att lära informationssökning för yrkeslivet: om bibliotekarier, lärare och sjuksköterskor</em> [Learning information seeking for professional life: on librarians, teachers and nurses]. In J. Hedman &amp; A. Lundh (Eds.), Informationskompetenser: om lärande i informationspraktiker och informationssökning i lärandepraktiker [Information literacies: on learning in information practices and information seeking in learning practices] (pp. 133-158). Stockholm: Carlssons.
</li>
<li id="hod04">Hodkinson, P., Hodkinson, H., Evans, K., Kersh, N., Fuller, A., Unwin, L. &amp; Senker, P. (2004). The significance of individual biography in workplace learning. <em>Studies in the Education of Adults, 36</em>(1), 6-24.
</li>
<li id="isa16">Isah, E. E. &amp; Byström, K. (2016). Physicians' learning at work through everyday access to information. <em>Journal of the Association for Information Science and Technology, 67</em>(2), 318-332.
</li>
<li id="joh07">Johannisson, J. &amp; Sundin, O. (2007). Putting discourse to work: information practices and the professional project of nurses. <em>Library Quarterly, 77</em>(2), 199-218.
</li>
<li id="llo09">Lloyd, A. (2009). Informing practice: information experiences of ambulance officers in training and on-road practice. <em>Journal of documentation, 65</em>(3), 396-419.
</li>
<li id="llo10">Lloyd, A. (2010). <em>Information literacy landscapes: information literacy in education, workplace and everyday contexts.</em> Oxford: Chandos Publishing.
</li>
<li id="llo11">Lloyd, A. (2011). Trapped between a rock and a hard place: what counts as information literacy in the workplace and how is it conceptualized? <em>Library Trends, 60</em>(2), 277-296.
</li>
<li id="mac05">MacIntosh-Murray, A. &amp; Choo, C. W. (2005). Information behavior in the context of improving patient safety. <em>Journal of the American Society for Information Science and Technology, 56</em>(12), 1332-1345.
</li>
<li id="mck04">McKnight, M. (2004). Hospital nurses: no time to read on duty. <em>Journal of Electronic Resources in Medical Libraries, 1</em>(3), 13-23.
</li>
<li id="mck06">McKnight, M. (2006). The information seeking of on-duty critical care nurses: evidence from participant observation and in-context interviews. <em>Journal of the Medical Library Association, 94</em>(2), 145-151.
</li>
<li id="mck07">McKnight, M. (2007). A grounded theory model of on-duty critical care nurses' information behavior: the patient-chart cycle of informative interactions. <em>Journal of documentation, 63</em>(1), 57-73.
</li>
<li id="mor11">Moring, C. (2011). Newcomer information practice: negotiations on information seeking in and across communities of practice. <em>Human IT: Journal for Information Technology Studies as a Human Science, 11</em>(2), 1-21.
</li>
<li id="mor97">Morgan, D. L. (1997). <em>Focus groups as qualitative research</em> (2nd ed.). Thousand Oaks, CA: Sage Publications.
</li>
<li id="spe08">Spenceley, S. M., O'Leary, K. A., Chizawsky, L. L., Ross, A. J. &amp; Estabrooks, C. A. (2008). Sources of information used by nurses to inform practice: an integrative review. <em>International Journal of Nursing Studies, 45</em>(6), 954-970.
</li>
<li id="str85">Strauss, A. L. (1985). <em>Social organization of medical work</em>. Chicago: University of Chicago Press.
</li>
<li id="sun02">Sundin, O. (2002). Nurses' information seeking and use as participation in occupational communities. <em>New Review of Information Behaviour Research</em>, <em>3</em>, 187-202.
</li>
<li id="sun03">Sundin, O. (2003). <em>Informationsstrategier och yrkesidentiteter: en studie av sjuksköterskors relation till fackinformation vid arbetsplatsen</em> [Information strategies and professional identities: a study of nurses’ relation to professional information at the workplace]. (PhD thesis), University of Gothenburg, Gothenburg.
</li>
<li id="sun05">Sundin, O. &amp; Hedman, J. (2005). Professions and occupational identities. In L. McKechnie, K. E. Fisher, &amp; S. Erdelez (Eds.), <em>Theories of information behavior</em> (pp. 293-297). Medford, NJ: ASIST.
</li>
<li id="wen15">Wenger-Trayner, E. &amp; Wenger-Trayner, B. (2015). Learning in landscapes of practice: a framework. In E. Wenger-Trayner, M. Fenton-O'Creevy, S. Hutchinson, C. Kubiak &amp; B. Wenger-Trayner (Eds.), <em>Learning in landscapes of practice: boundaries, identity, and knowledgeability in practice-based learning</em> (pp. 13-30). London: Routledge.
</li>
</ul>

</section>

</article>