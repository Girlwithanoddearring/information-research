<header>

#### vol. 22 no. 1, March, 2017

</header>

## Proceedings of the Ninth International Conference on Conceptions of Library and Information Science, Uppsala, Sweden, June 27-29, 2016

<article>

<section>

# Methodological frameworks for developing a model of the public sphere in public libraries

## [Michael M. Widdersheim](#author) and [Masanori Koizumi](#author)

> **Introduction.** This paper identifies research gaps in the literature related to the public sphere and public libraries. The paper then proposes methodological frameworks and data collection techniques to extend research in this area.  
> **Method.** A review of the literature related to the public sphere and public libraries is conducted to identify research gaps and develop promising research approaches.  
> **Analysis.** Previous literature is analyzed according to epistemological and ontological approaches  
> **Results.** Existing literature related to the public sphere and public libraries exhibits epistemological and ontological shortcomings. Empirical studies are relatively few and existing studies remain tied to a limited public sphere concept.  
> **Conclusions.** Two new methodological frameworks are proposed to guide future work in this area. One framework is the circulation of power model; the other framework is a custom-built model with multiple axes and dimensions

## Introduction

### Research purpose

Public libraries have been associated with the sociological concept of the public sphere, but these associations are still being developed. Attempts have been made to construct a more detailed and empirically-based model of the public sphere in public libraries, but such a project faces a number of challenges, including the development of appropriate research methods. The purpose of this preliminary research note is therefore to present methodological frameworks and data collection methods that might guide future work in this area. It is hoped that concrete research methods that are tied closely to sociological theory will lead to a more sophisticated description of the public sphere in public libraries

### Definition of the public sphere

The phrase public sphere is the English translation of the German word Ö¦fentlichkeit, which refers to a type of transparent, undistorted communication that is open to anyone, oriented to common interests, and focused on argumentative give-and-take. Public spheres occur in face-to-face or mediated contexts, and they represent communicative interactions at various societal scales. While public sphere themes can be literary or cultural in nature, this study focuses on the political public sphere.

In its most basic and generalised sense, the political public sphere expresses a reciprocal speaker-audience relationship (see Figure 1). On the one side of this interaction are networks of private people who form a public. As a public, these people deliberate to reach an understanding about a given situation. On the other side of the interaction are governing bodies, people such as legislators or engineers whose decisions affect the social lives of the public in question. Because they remain under scrutiny by a critical public, the decision-makers must justify their actions in a way that is more than just a display, a way that uses reasons and justification to account for their decisions. Publics in turn petition decision-makers for resolutions that reflect their concerns. The public can at any point question the truth, sincerity, or normative rightness of the actions of the decision-making body and demand further warrants or grounds from them. While it may be the case that publics or decision-makers can resort to strategic action, the public sphere relationship emphasizes a bond of reciprocal communicative power, not coercive power. Decision-making bodies wield authority because their decisions are valid; the public legitimates the decisions because they pass the tri-fold test of truth, sincerity, and rightness. The public sphere is a normative concept that emphasizes a deliberative process ([Habermas, 1992](#hab1992), p. 452).

<figure>

![Figure 1: The political public sphere](../colis1643fig1.png)

<figcaption>Figure 1: The political public sphere.</figcaption>

</figure>

The public sphere concept is most commonly associated with German philosopher Jï¿½ï¿½n Habermas, whose work on the subject animated English-language research in a number of fields beginning in the late 1980s and early 1990s (Habermas, [1984](#hab1984), [1989a](#hab1989a), [1989b](#hab1989b)), and whose more recent work has continued to resonate with media and political science scholars (Habermas, [1996](#hab1996), [2006](#hab2006)). A central feature of the public sphere concept is it ambivalent nature-the question of whether the public sphere is "there" in an authentic sense or not, whether it has been overrun by power and money, and whether it might be reconstituted from a deteriorated state (Habermas, [1989a](#hab1989a)). Though originally conceived as an intermediary between civil society groups and state bodies, globalizing economic tendencies and the formation of the European Union have resulted in a transnationalization of the public sphere concept ([Habermas, 1998)](#hab1998). The public sphere continues to be revisited and revised in secondary literature (e.g., [Crossley and Roberts, 2004](#crossl2004)), a sign of its continued relevance.

### History and development of the public sphere concept

The concept of the public sphere has been developed extensively by Habermas since his first deployment of the term in 1962\. The basic criteria of public sphere discourse-inclusiveness of participants, unrestricted exchange reasons for and against validity claims, orientation toward mutual understanding and consensus-have not changed since that time, but they represent only one dimension of the concept. The public sphere is a sociological category that has been utilized to describe social action-how and why people behave as they do in social contexts. Habermas takes a communicative approach to explaining action, one with a strong systems theory flavor. In the theory of society developed in Habermas ([1984](#hab1984), [1989b](#hab1989b)), the public sphere is presented as an area of overlap between system and lifeworld (see Figure 2). In this model, the public sphere is situated in the societal component of the lifeworld along with the private sphere. The two other components of the lifeworld are culture and personality. In this application of the public sphere concept, the public sphere forms an area of interchange with the administrative system, or state, a subsystem on the system side of Habermas's action model. In the public sphere arena in this model, private actors fill the roles of citizens and clients.

<figure>

![Figure 2: The public sphere as an area of overlap between lifeworld and system](../colis1643fig2.png)

<figcaption>Figure 2: The public sphere as an area of overlap between lifeworld and system.</figcaption>

</figure>

As clients, private actors input taxes (money) and receive organizational accomplishments (power), and as citizens, they input mass loyalty (power) and receive political decisions (power) as outputs ([Habermas, 1989b, p. 320](#hab1989b)). In the end, the model is supposed to demonstrate how the state and economic systems have come to dominate people's actions and have led to pathological consequences for culture, institutional orders, and identify formation. The public sphere is one channel through which this colonisation occurs.

As Baxter ([2011](#baxter2011)) points out, this model from Habermas ([1984](#hab1984), [1989b](#hab1989b)) suffers from numerous problems, and the model has been revised in Habermas's more recent theories of society and politics. In Habermas ([1996](#hab1996), [2006](#hab2006)), the lifeworld/system and public/private distinctions are still present, but they form continua rather than sharp contrasts. Baxter ([2011](#baxter2011)) also points out that in more recent models, the "private sphere" from Habermas ([1984](#hab1984), [1989b](#hab1989b)) has been replaced by "civil society" (Habermas, [1996](#hab1996), [2006](#hab2006)) and "media system" has been added as a system ([Habermas, 2006](#hab2006)). Habermas's recent societal theories situate the public sphere concept in a "circulation of power" model ([Baxter, 2011](#baxter2011)). In this model from Habermas ([1996](#hab1996)), the public sphere is an intermediary network located between civil society groups, economic groups, and media groups, on the one hand, and the decision-making bodies of the political system, on the other (see Figure 3). Using the public sphere, the various groups on the political system's periphery influence political decisions by transmitting "communicative power" to the political system's core. Their communicative power affects the "administrative power" wielded by decision-making bodies at the center. As Baxter ([2011](#baxter2011)) notes, Habermas ([1996](#hab1996)) equivocates on this point, but the public sphere appears to be situated outside the political system to form the system's environment. Various groups and associations that inform public sphere communication are also located beyond the political system on its outer periphery. The function of the public sphere is to receive and package various messages from the outer periphery and relay them to decision-making bodies. When functioning without distortion from money and power, the public sphere ensures that collective values and interests are translated into legislative, judicial, and executive decisions. As Habermas ( [2006](#hab2006)) notes, however, the perennial danger is that public sphere discourses are influenced by economic, media, and social power in a way that marginalizes certain perspectives and privileges others. The challenge is to check these powers in a way that protects rights to free speech.

<figure>

![Figure 3: Circulation of power model](../colis1643fig3.png)

<figcaption>Figure 3: Circulation of power model</figcaption>

</figure>

## Library literature and the public sphere

### Overview

Lending libraries were first associated with the public sphere in Habermas ([1962](#hab1962); [1989a, p. 50](#hab1989a)). The public sphere was first associated specifically with public libraries by Thauer and Vodosek ([1978](#thau1978)) and Schuhböck ([1983](#schu1983), [1994](#schu1994)) in their discussions of public libraries in Germany. Associations between the public sphere and public libraries began to appear in Britain by Greenhalgh, Landry, and Worpole ([1993](#green1993)), Greenhalgh, Worpole, and Landry ([1995](#green1995)), and Webster ([1995](#web1995)). The public sphere concept became more widely used and associated with libraries in Scandinavia ([Emerek and Ørum, 1997](#emerek1997); Vestheim, [1997](#vest1997a), [1997b](#vest1997b)), North America ([Buschman, 2003](#busch2003); [Leckie and Buschman, 2007](#leckie2007)), and elsewhere.

Recent research on the public sphere in public libraries finds two main dimensions: in one dimension, public libraries constitute public sphere commons; in the other dimension, public libraries are objects of public sphere discourse where they are governed and legitimated ([Widdersheim and Koizumi, 2016](#wid2016)). With respect to their commons function, several authors have discussed the social value of the public sphere in public libraries ([McCook, 2004, pp. 188-189](#mcc2004)). Social trust, social inclusion, solidarity, and social capital have been said to be positive spillovers of public sphere meetings in public libraries ([Aabø 2005](#aabo2005); [Aabø and Audunson, 2012](#aabo2012); [Aabø, Audunson and Vårheim, 2010](#aabo2010); [Audunson, Vårheim, Aabø and Holm, 2007](#aabo2007)). Public libraries are not passive or neutral agents in the constitution of public sphere commons ([Andersen and Skouvig, 2006](#andersen2006)). Several authors question the quality of public sphere discourse in public libraries ([Alstad and Curry, 2003](#alstad2003);[Leckie and Hopkins, 2002](#leckie2002); [McNally, 2014/2015](#mcn2014)). On the other side, where public libraries are objects of public sphere discourse, one theme of study is the meanings and justifications given by various stakeholders for public library creation, closings, and purpose ([Evjen, 2015](#evjen2015); [Ingraham, 2015](#ing2015); [Newman, 2007](#newman2007)). Communicative power is generated both to legitimize and to govern public libraries (Widdersheim and Koizumi, [2015](#wid2015), [2016](#wid2016)).

### Epistemological limitations of previous research

Existing literature about the public sphere in public libraries suffers from several methodological problems. Within existing literature, relatively few studies present results based on empirical data. Existing data collection methods include interviews ([Evjen, 2015](#evjen2015); [Greenhalgh et al., 1993](#green1993); [Newman, 2007](#newman2007)), surveys (Audunson _et al._, 2007; [Vårheim, Steinmo and Ide, 2008](#var)), ethnography (Aabø and Audunson, 2012; [Leckie and Hopkins, 2002](#leckie2002)), histories ([Emerek and Ørum, 1997](#emerek1997); [Vestheim, 1997a](#vest1997a)), and content analysis (Widdersheim and Koizumi, [2015](#wid2015), [2016](#wid2016)). Still, most studies are conceptual in nature. More empirical studies are needed because research about the public sphere requires a mix of theory, conceptual analysis, and empirical evidence.

### Ontological limitations of previous research

The second problem is ontological. An ontology specifies basic categories of experience and defines how they are organized. In a sociological theory, for example, the ontology is the set of basic concepts used to explain social action and social change. An ontological limitation in social research means that either the research does not define these basic concepts and use them to orient the research project or that the research does not contribute to theoretical development. In library research on the public sphere, for example, no studies articulate a cohesive sociological framework, such as the circulation of power model, as a framework to guide the research. Moreover, no studies about libraries and the public sphere contribute to existing theory about the public sphere concept. Using the circulation of power model as a methodological framework, for example, might present the following questions in a research study: Where does this communicative power travel? How is this public sphere discourse affected by other societal variables, such as the economy and state? Because the methodological frameworks of existing research are limited ontologically, the research can offer little to the development of sociological theory, especially regarding the public sphere concept.

## Proposed methodological frameworks

### Framework 1: Circulation of power

In light of the epistemological and ontological limitations of existing research noted above, future research about the public sphere in public libraries must adopt more sophisticated methodological frameworks. A methodological framework establishes the guideposts for a research project. It is based on existing literature and theory about a topic, and it therefore incorporates the basic conceptual map-the ontology-that is central to investigating that topic. A methodological framework is important because it frames research problems and questions, and it suggests a general approach for collecting data to address the research questions. Data collection and analysis that is guided by existing literature about a topic produces results that are epistemologically sound.

[1996](#hab1996)) as a normative reconstruction of how political systems work, but it also serves as a useful methodological framework because 1) it specifies three main arenas that must be described: political center, inner periphery, and outer periphery; 2) it specifies two main types of power that might be observed: communicative and administrative; and 3) it identifies several key actors that might be studied in interaction: economic groups, civil society groups, media groups, quasi-state institutions, and decision-making bodies. A study of the public sphere in public libraries might use this framework to investigate how library systems operate along the center-periphery continuum, the mechanisms and practices through which communicative and administrative power circulate through libraries, and how libraries convert one type of power into another. This framework recommends a strongly qualitative approach to data collection, and it suggests that researchers focus on contexts where power exchanges occur between groups and across boundaries.

### Framework 2: Societal quadrants with temporal and scalar dimensions

The circulation of power model above offers one potential methodological framework for future research about the public sphere in public libraries, but the model is limited to studies about the political system. A second, custom-built framework that includes the public sphere but that is not focused on the political system is therefore presented. Rather than a center/periphery axis, this second framework uses three other axes drawn both from early Habermas ([1984](#hab1984), [1989b](#hab1989b)) and later Habermas ([1996](#hab1996), [2006](#hab2006)): civil society/system, public/private, and time. The civil society/system and public/private axes form societal quadrants: the public sphere, private sphere, state, and economy (see Figure 4). At the intersection of all three axes is the object of study: public library systems. In other words, this framework supposes that public library systems contain aspects of all four quadrants simultaneously, that this mixed composition produces tensions and inconsistencies within the system, and that the dominant features of the system may change over time.

The spheres in the middle of Figure 4 represent theoretical developments of the public sphere in public libraries that 1) account for each of the four societal dimensions; 2) account for changes in the dimensions over time; and 3) offer three scalar perspectives-micro, meso, and macro-to each of the three axes. These scales are necessary because public spheres occur in localized, inter-organizational, and society-wide contexts, and also because temporal patterns may be dynamic, durative, or cyclical. The attractiveness of this second framework is that it encourages a complex, multifaceted view of the public sphere in public library systems, and it avoids ahistorical and isolationist studies. Table 1 describes the objects of theoretical development for the three scalar dimensions in each societal quadrant.

Like the methodological framework based on the circulation of power model, Framework 2 suggests a primarily qualitative approach to data collection. This framework emphasizes a historical account of the public sphere in public libraries, but also one that incorporates first-person or emic perspectives. Differentiating between public and private, civil society and system are hermeneutic and interpretive tasks that emphasize the role of the researcher and require qualitative data. Ethnographic methods with a strong historical focus therefore offer a promising approach. Ethnographic research could focus on numerous micro-level contexts in library systems of different sizes and locales in order to develop micro-, meso-, and macro-level theories. Ethnohistorical, anthrohistorical, and historical-anthropological methods may provide further insight into this area.

<figure>

![Figure 4: Visualization of framework 2](../colis1643fig4.png)

<figcaption>Figure 4: Visualization of framework 2.</figcaption>

</figure>

<table><caption>Table 1: Quadrants and theoretical scales for framework 2.</caption>

<tbody>

<tr>

<th>Quadrant</th>

<th>Quadrant characteristics</th>

<th>Micro-level theory</th>

<th>Meso-level theory</th>

<th>Macro-level theory</th>

</tr>

<tr>

<td>Public Sphere</td>

<td>

Openness  
Common concern  
Debate  
Communicative power</td>

<td>Person-level communication, e.g. hearings, forums</td>

<td>Inter-organizational communication, networks</td>

<td>Society-level communication</td>

</tr>

<tr>

<td>Private Sphere</td>

<td>

Culture  
Solidarity, norms, order  
Personality, identity</td>

<td>Personal interactions</td>

<td>Informal associations and groups</td>

<td>Society-wide movements</td>

</tr>

<tr>

<td>State</td>

<td>

Executive bodies  
Legislative bodies  
Judicial bodies  
Bureaucracy  
Administrative power</td>

<td>Local laws, local agencies, and local government bodies</td>

<td>Regional laws and government bodies</td>

<td>Federal and international laws and government bodies</td>

</tr>

<tr>

<td>Economy</td>

<td>

Money  
Media power  
Economic power</td>

<td>Person-level media exchanges</td>

<td>Media groups and corporations</td>

<td>National and international media systems</td>

</tr>

</tbody>

</table>

## Conclusion

This research note proposed methodological frameworks that might guide future research related to the public sphere in public libraries. These frameworks are intended to overcome the epistemological and ontological limitations identified in previous work. Research about the public sphere in public libraries is significant because it contributes to an understanding of the public sphere concept and because it describes the social value of public libraries. Future studies about the public sphere and public libraries can both contribute to the public sphere concept and continue to explore how public libraries function in society.

## <a id="author"></a>About the author

**Michael M. Widdersheim** is a PhD candidate in the School of Information Sciences at the University of Pittsburgh, USA. He can be reached at [mmw84@pitt.edu](mailto:mmw84@pitt.edu).  

**Masanori Koizumi** is Assistant Professor in the Faculty of Library, Information and Media Science at the University of Tsukuba, Japan. He can be reached at [koizumi@slis.tsukuba.ac.jp](mailto:koizumi@slis.tsukuba.ac.jp).

</section>

<section>

## References

<ul>
<li id="aabo2005">Aabø, S. (2005). The role and value of public libraries in the age of digital technologies. Journal of Librarianship and Information Science, 37(4), 205-211.
</li>
<li id="aabo2012">Aabø, S. &amp; Audunson, R. (2012). Use of library space and the library as place. Library &amp; Information Science Research, 34(2), 138-149.
</li>
<li id="aabo2010">Aabø, Audunson, R. &amp; Vårheim, A. (2010). How do public libraries function as meeting places? Library &amp; Information Science Research, 32(1), 16-26.
</li>
<li id="alstad2003">Alstad, C. &amp; Curry, A. (2003). Public space, public discourse, and public libraries. Libres, 13(1).
</li>
<li id="andersen2006">Andersen, J. &amp; Skouvig, L. (2006). Knowledge organization: A sociohistorical analysis and critique. Library Quarterly, 76(3), 300-322.
</li>
<li id="aabo2007">Audunson, R., Vårheim, A., Aabø, S. &amp; Holm, E. D. (2007). Public libraries, social capital, and low intensive meeting places. information research, 12(4).
</li>
<li id="baxter2011">Baxter, H. (2011). Habermas: The discourse theory of law and democracy. Stanford, CA: Stanford Law Books.
</li>
<li id="busch2003">Buschman, J. E. (2003). Dismantling the public sphere: Situating and sustaining librarianship in the age of the new public philosophy. Westport, CT: Libraries Unlimited.
</li>
<li id="crossl2004">Crossley, N. &amp; Roberts, J. M. (Eds.). (2004). After Habermas: New perspectives on the public sphere. Oxford, UK: Blackwell.
</li>
<li id="emerek1997">Emerek, L. &amp; Ørum, A. (1997). The conception of the bourgeois public sphere as a theoretical background for understanding the history of Danish public libraries. In N. W. Lund (Ed.), Nordic Yearbook of Library, Information, and Documentation Research (Vol. 1, pp. 27-57). Oslo, Norway: Novus.
</li>
<li id="evjen2015">Evjen, S. (2015). The image of an institution: Politicians and the urban library project. Library &amp; Information Science Research, 37(1), 28-35.
</li>
<li id="green1993">Greenhalgh, L., Landry, C. &amp; Worpole, K. (1993). Borrowed time? The future of public libraries in the UK. Gloucestershire, UK: Comedia.
</li>
<li id="green1995">Greenhalgh, L., Worpole, K. &amp; Landry, C. (1995). Libraries in a world of cultural change. London, UK: UCL Press.
</li>
<li id="hab1962">Habermas, J. (1962). Strukturwandel der Öffentlichkeit: Untersuchungen zu einer Kategorie der bügerlichen Gesellschaft. Frankfurt a. M.: Suhrkamp.
</li>
<li id="hab1984">Habermas, J. (1984). The theory of communicative action, volume 1: Reason and the rationalization of society (T. McCarthy, Trans.). Boston, MA: Beacon Press.
</li>
<li id="hab1989a">Habermas, J. (1989a). The structural transformation of the public sphere: An inquiry into a category of bourgeois society (T. Burger &amp; F. Lawrence, Trans.). Cambridge, MA: MIT Press.
</li>
<li id="hab1989b">Habermas, J. (1989b). The theory of communicative action, volume 2: Lifeworld and system: A critique of functionalist reason (T. McCarthy, Trans.). Boston, MA: Beacon Press.
</li>
<li id="hab1992">Habermas, J. (1992). Further reflections on the public sphere. In C. Calhoun (Ed.), Habermas and the public sphere. Cambridge, MA: MIT Press.
</li>
<li id="hab1996">Habermas, J. (1996). Between facts and norms: Contributions to a discourse theory of law and democracy (W. Rehg, Trans.). Cambridge, MA: MIT Press.
</li>
<li id="hab1998">Habermas, J. (1998). The inclusion of the other: Studies in political theory (C. Cronin &amp; P. D. Greif Eds.). Cambridge, MA: MIT Press.
</li>
<li id="hab2006">Habermas, J. (2006). Political communication in media society: Does democracy still enjoy an epistemic dimension? The impact of normative theory on empirical research. Communication Theory, 16(4), 411-426.
</li>
<li id="ing2015">Ingraham, C. (2015). Libraries and their publics: Rhetorics of the public library. Rhetoric Review, 34(2), 147-163.
</li>
<li id="leckie2007">Leckie, G. J. &amp; Buschman, J. E. (2007). Space, place, and libraries: An introduction. In J. E. Buschman &amp; G. J. Leckie (Eds.), The library as place: History, community, and culture (pp. 3-25). Westport, CT: Libraries Unlimited.
</li>
<li id="leckie2002">Leckie, G. J. &amp; Hopkins, J. (2002). The public place of central libraries: Findings from Toronto and Vancouver. Library Quarterly, 72(3), 326-372.
</li>
<li id="mcc2004">McCook, K. d. l. P. (2004). Introduction to public librarianship. New York, NY: Neal-Schuman.
</li>
<li id="mcn2014">McNally, M. B. (2014/2015). Response to Dr. Samuel E. Trosow's Keynote Address. Progressive Librarian, 43, 30-34.
</li>
<li id="newman2007">Newman, J. (2007). Re-mapping the public. Cultural Studies, 21(6), 887-909.
</li>
<li id="schu1983">Schuhböck, H. P. (1983). Die gesellschaftliche Funktion von Bibliotheken in der Bundesrepublik Deutschland: Zur neueren Diskussion nach 1945 [The societal function of libraries in the Federal Republic of Germany: The recent discussion since 1945]. Bibliothek: Forschung und Praxis, 7(3), 203-222.
</li>
<li id="schu1994">Schuhböck, H. P. (1994). Bibliothek und Ö¦fentlichkeit im Wandel [Libraries and the public sphere in transition]. Bibliothek Forschung und Praxis, 18(2), 217-229.
</li>
<li id="thau1978">Thauer, W. &amp; Vodosek, P. (1978). Geschichte der Ö¦fentlichen Bï¿½ï¿½rei in Deutschland [History of the public library in Germany]. Wiesbaden, West Germany: Otto Harrassowitz.
</li>
<li id="var08">Vårheim, A., Steinmo, S. &amp; Ide, E. (2008). Do libraries matter? Public libraries and the creation of social capital. Journal of Documentation, 64(6), 877-892.
</li>
<li id="vest1997a">Vestheim, G. (1997a). Fornuft, kultur og velferd: Ein historisk-sosiologisk studie av norsk folkebibliotekpolitikk [Reason, culture and welfare: A historical-sociological study of Norwegian public library policy]. (Fil.Dr.), Goteborgs Universitet, Sweden.
</li>
<li id="vest1997b">Vestheim, G. (1997b). Libraries as agents of the public sphere: Pragmatism contra social responsibility. In N. W. Lund (Ed.), Nordic Yearbook of Library, Information, and Documentation Research (Vol. 1, pp. 115-124). Oslo, Norway: Novus.
</li>
<li id="web1995">Webster, F. (1995). Theories of the information society. New York, NY: Routledge.
</li>
<li id="wid2015">Widdersheim, M. M. &amp; Koizumi, M. (2015). Signal architectures of US public libraries: Resolving legitimacy between public and private spheres. Paper presented at the ASIS&amp;T Annual Meeting, St. Louis, MO. https://www.asist.org/files/meetings/am15/proceedings/submissions/papers/50paper.pdf (Archived by WebCiteî&nbsp;¡t http://www.webcitation.org/6lJsSvzmO)
</li>
<li id="wid2016">Widdersheim, M. M. &amp; Koizumi, M. (2016). Conceptual modelling of the public sphere in public libraries. Journal of Documentation, 72(3).
</li>
</ul>

</section>

</article>