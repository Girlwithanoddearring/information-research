<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Attention and altmetrics

## [Terttu Kortelainen](#author), [Mari Katvala](#author) and [Anni-Siiri Länsman](#author)

> **Introduction.** The concept of attention is applied in two altmetric studies concerning: (1) the use of social media tools on the web pages of scientific journals, and indications of attention in these pages received, and (2) attention received by the radio news on the web page of an indigenous radio station. The purpose is to reveal characteristics of web publications connected to the attention they receive.  
> **Method.** Altmetric data describing attention data was compiled from the Facebook sites of scientific journals and the Facebook pages of an indigenous radio station news programme, as well as the news programme’s web site. The study was both quantitative and qualitative.  
> **Analysis.** Attention was operationalized in the form of comments, likes or sharing of Facebook postings and these were analyzed quantitatively. The contents of the postings were analysed by qualitative content analysis.  
> **Results.** The characteristics of the postings receiving most attention were relevance, community (in the sense of ownership), belonging and co-creation, and engagement in the meaning of interactivity.  
> **Conclusions.** In altmetric research, data originating from several sources can reflect the societal impact a project or a publication may have, not only the impact it has in science. Attention economy theory supports the interpretation of altmetric data.

<section>

## Introduction

In scientific publications citations indicate the citing author’s awareness of the cited publication and attention paid to it. Earlier the study of awareness of publications was limited to citation analysis and consequently restricted to scientific publications. The launching of Web 2.0 and social media has made it possible to apply the same principles to many other kinds of materials, e.g. news sites, blog postings and public or personal web sites. In this context online mentions can indicate something of wider interests ([Holmberg _et al._ 2015](#hol15)).

The attention economy existed long before the Internet (see e.g. [Simon 1971](#sim71)) and has been applied especially in the fields of audience research and marketing, but also to citation analysis ([Frank 2002](#fra02)). Since the launching of social media, it has also been possible to base research into attention economics on digital foot-prints.

The purpose of this paper is to study the contribution of attention economy theory in an altmetric study of social media. Altmetrics tracks the impact outside academia from sources that are not peer-reviewed ([Priem, Taborelli, Groth and Neylon 2010](#pritar10)), for example in treatment guidelines, teaching materials, mass media, and the internet. This paper consists of a review of attention theory, describing characteristics that influence attention. The research question of this study is:

Which characteristics of web publications are most clearly connected to the attention they receive?

The study is focused on two different kinds of materials: the Facebook pages of scientific journals (see [Kortelainen and Katvala 2012](#kor12)), and radio news programmes of an indigenous radio station presented on its Web site (see [Kortelainen and Länsman 2015](#kor15)). The combination of these two types of research materials enables the study of the attention received by both the scholarly and news publications on two different forums, with different audiences, in order to reach a multi-fold picture of factors connected to receiving attention in web communication. The strength of altmetric methods is their unobtrusiveness, compared, for example, to surveys or diaries in audience research.

## Attention economy

The root word of _attention_ is the verb _to attend_. To attend to something is to tend it: to take care of it. Attention has been defined as time spent interacting with someone or something ([Simon, 1971, p. 41](#sim71)) and as a form of focused mental engagement on a particular item of information ([Davenport and Beck, 2001, p. 20](#dav01)). It can also be viewed as a mutual means of constructing the content of an internet site ([Chatfield 2013](#cha13)).

Attention is a selective cognitive process ([Davenport and Beck 2001, 25](#dav01)). It has its background in the biological evolution of human beings: we are essentially “serial devices”, that can “attend to only one thing at a time” ([Simon 1971](#sim71)). In today’s world, both at work and in their free time, most people meet with information overload: they have more things to do than they have mental resources or time to complete. Therefore, they must establish priorities: through a selective cognitive process people absorb selected information. Hence the most important function of attention is not taking information in, but screening it out. ([Davenport and Beck 2001, 25; 58.](#dav01)) This makes attention scarce and it has been compared to currency ([Simon 1971, 40](#sim71)), land ([Ott 2011](#ott11)), gold and oil ([Chatfield 2013](#cha13)).

Attention problems did not exist in earlier centuries when the number of publications was a small fraction of the current quantity. It was not a problem to find time to read, but rather to find enough reading material, and information was a seller’s market ([Davenport and Beck 2001, 4](#dav01)). Instead nowadays, according to Yardi et al. ([2009](#yar09)), the reader drives the attention economy, and it is attention that binds together the reader, author and content in media.

Attention can take many forms: devoted listening to your companion, reading a text, or watching TV. It is easy to see if somebody focuses his or her attention on a presentation – or does not. However, it is not easy to conclude what happens in her or his mind. Some clues, however, indicate the focusing of attention on a piece of information. In scientific texts citations show that the author is aware of a cited text. In the web environment attention data or indicators of use ([Borgman, 2007](#bor07)) often consist of user-generated content enabled by Web 2.0 technologies or social media. User generated content may consist of text, pictures or audio material podcasted or published on a blog. It may also take the form of Wiki writing ([Gray, Thompson, Clerehan, Sheard, and Hamilton, 2008](#gra08)), or posting on Facebook or Twitter or publishing videos on YouTube, for example. User-generated content may originate from professionals, artists ([Yardi, Golder, and Brzozowski, 2009a, p. 2071](#yar09)) or interest groups, but it can also refer to something other than texts or pictures: passive reading or viewing. If this leaves a digital footprint, it produces attention data. Because it originates from various sources enabling citizen participation, user generated content can also reflect something of the societal impact of science, different projects or publications ([Holmberg _et al._ 2015](#hol15)).

The strength of attention cannot be measured. As a proxy, Simon ([1971](#sim71)) used time spent on a message, Frank ([2002](#fra02)) made use of citations, Priem and Hemminger ([2010](#pri10)) utilized mentions on bookmarks and blogs, Eysenbach ([2011, 9](#eys11)) employed tweets and Kortelainen and Katvala ([2011, 662](#kor12)) used comments and likes on Facebook. In the radio and television industries attention has been studied through surveys or by asking people to keep diaries of what they have been watching on TV, or by using metering devices ([Finnpanel 2016](#fin16)). Attention has also been measured as a physiological phenomenon by studying what a person looks at on a web page. ([Davenport and Beck 2001, 35.](#dav01))

Scarcity of attention especially stands out on the Internet where the imbalance between the amount of published information and attention it receives is evident. The amount of attention does not grow to the same degree as (scientific or other) information, leading to a proportional decrease in the amount of attention. If one medium gains attention, this happens at the expense of others that consequently are left with less attention. ([Davenport and Beck 2001, 7, 11, 94.](#dav01)) Attention is not divided evenly: some people, websites, products and information become its focus, while others are deprived of it.

Earlier studies have identified characteristics of messages that are connected to gains in attention and these include: relevance, community, engagement and convenience.

_Relevance_ means content that fulfils a need, is meaningful, has an appropriate scope, is authoritative, frequently updated, offering a steady stream of benefits and is what customers like. It can also include authority or trustworthiness, or offer the “earth’s biggest selection” of something. ([Davenport and Beck 2001, 115, 118, 195.](#dav01))

_Community_ can mean a sense of ownership or belonging, focusing on a particular audience, co-creation, personalization and customization ([Davenport and Beck 2001, 115, 118](#dav01)). Co-creation can be achieved by investing user generated information in the website, such as in a web discussion or on Facebook pages ([Chatfield 2013](#cha13)).

_Engagement_ can consist of interactivity, competition, production values, entertainment and narrative. This may be realized with interactive features, such as the opportunity to upload readers’ opinions on the content of the site, or competitions for visitors ([Davenport and Beck 2001, 115, 118](#dav01); [Kortelainen and Katvala 2012](#kor12)).

_Convenience_ means quick downloads, intuitive navigation, story-like, or, bite-sized chunks of information and minimal distractions ([Davenport and Beck 2001, 115, 118](#dav01)).

Although these characteristics do not necessarily guarantee success, a site is unlikely to succeed without them. Despite having a large amount of content a site may not be attractive if the other components, such as convenience or engagement, are not fulfilled ([Davenport and Beck 2001, 195](#dav01)).

## Research materials and methods

This study is based on two sets of research materials. The first set consists of eight scientific journals, the Facebook sites of which were studied over a month (March 2012). The journals were part of a larger body of study material consisting of a hundred scientific journals from ten different fields and their social media applications. Eight of the journals had a Facebook site and are part of this study. These include: Annals of Internal Medicine, the British Medical Journal, Gender and Society, the Journal of the American Medical Association, the Journal of the American Medical Informatics Association, The Lancet, MIS Quarterly and the New England Journal of Medicine.

The second set of research material includes the radio news of an indigenous radio station, presented on its website over two months, between October 21<sup>st</sup> and November 27<sup>th</sup>, 2013, and between April 15<sup>th</sup> and June 2<sup>nd</sup>, 2014\. The radio station is YLE Sápmi, part of the Finnish Broadcasting Company YLE, and broadcasts radio programmes and news programmes in minority Sámi languages spoken in four Northern countries, Finland, Norway, Russia and Sweden. The audience of the radio station represents speakers of Sámi languages in these four countries. The Sámi are the only indigenous people in Europe and in Finland there are about 10, 000 members in this group.

In both study materials attention was operationalized in the form of comments, likes and sharing of postings on Facebook and in the case of the radio station also as comments published on the website of the radio station.

The study combines qualitative and quantitative methods. The number of indications of attention (comments, likes, shares) are studied quantitatively, whereas the content of the attention is studied qualitatively through content analysis. The Facebook postings for the scientific journals were divided into postings connected to the contents of the journals and postings not connected to it. The radio news was divided into political, environmental, cultural, economic and other topics. The news items were also divided on the basis of whether they concerned the indigenous Sámi group or not.

It would also be possible to carry out research based on “clicks” showing visits to a website, but this research is based on the digital footprints of the audience, showing not only the amount of attention but also its orientation.

## Results

The scientific journals published a total of 131 postings on their Facebook pages during the month studied. Four of them published at least 20 postings, and the other four 2-13\. On the average they received 48 indications of attention per posting. This figure, however, does not indicate the highly skewed distribution of attention varying between 0.18 and 293.26 indications per posting.

The radio station presented 473 news items on its website during the study periods. Only 113 of them (24 %) received any attention on the website or Facebook pages. The news items received a total of 272 comments, 167 during the first and 105 during the second study period. Respectively, the news items were liked 683 times during the first and 2,616 times during the second study period.

Both the attention received by the scientific journals and the radio news items in this study show that attention is scarce and highly unevenly divided between the items. In the following the attention directed to the Facebook postings of the eight scientific journals and the news items of the indigenous radio station are studied from the viewpoint of relevance, community, engagement and convenience.

### Relevance

On the Facebook pages of six (out of eight) journals, more than 70 percent of (and in three cases all) postings were connected to the contents of the journal itself. All these postings received attention in the form of likes, postings or shares, suggesting the relevance of the contents for the audience.

Two journals differed in this respect. The former of them had only 10 % of postings connected to the journal itself, and the latter had none. 90 % of postings of the former and 44 % of postings of the latter did not receive any attention on Facebook.

The indigenous radio station published news stories on its website on politics (30 %), the economy (15 %), the environment (15 %) and cultural topics (40 %). The items concerned global and national news topics, and the indigenous group. The national and global level news items not concerning the indigenous group neither received attention from the audience on the radio website nor on the Facebook pages. Attention was clearly focused on news concerning the indigenous group either from a political or cultural viewpoint. This emphasizes the connection of the meaningfulness and relevance of the news items to the attention they received.

### Community

Community (in the sense of ownership) belonging or co-creation was clearly present in both sets of research materials. In the case of the scientific journals, this was indicated by the co-creation of the contents through commenting (two journals) or liking the postings (all journals). Additionally, sharing can be viewed as an indication of ownership and belonging to a professional group. During the month studied, the readers of the New England Journal of Medicine shared its Facebook postings 1,274 times, the readers of the Lancet did so 398 times, and for the British Medical Journal 42 times. The readers of the Journal of the American Medical Association shared the postings 25 times, while the readers of Annals of Internal Medicine shared its Facebook posting 7 times. This means that the readers had found interesting information and wanted other people to know about it.

In the case of the radio station, the audience concentrated its attention only on topics concerning the indigenous group, indicating both community and ownership of the news contents, and suggesting that the radio station contributed to making the indigenous culture visible. This was evident in the cultural news that received a lot (in four cases more than 100) of likes on Facebook, although this news received no comments on the radio website, or on Facebook. The most liked news items concerned the Finnish Sámi winner of a transnational music grand prix (370), the Sámi language TV news (362), a Sámi super star concert (309) and a young Sámi Cosplay role game player (106). Regardless of the topic, news items which did not concern the indigenous people, its culture, politics or economy did not receive any attention.

### Engagement

Engagement in terms of interactivity, competitions for visitors, entertainment and narrative, as well as the opportunity to upload user generated content or opinions on the site, were present in both research materials in different ways.

In the scientific journals, especially competitions and polls asking for readers’ opinions resulted in high attention scores. Image Challenges published by the New England Journal of Medicine received on average 31 readers’ proposals to solve the problem and hundreds of likes, and the Facebook postings received 293 indications of attention on average.

Additionally, opinion polls published in the British Medical journal about medicine, health care and related legislation were popular. Contents of both cases included elements of ownership, belonging and uploading user generated content. Participation both in the polls and the Image Challenges required a degree of medical expertise.

The most engaging elements on the radio website were news items that provoked feelings, comments and discussion, in most cases concerning the definition of the indigenous group and belonging to it. This could result from many different original news topics: a doctoral dissertation concerning the indigenous group (64 comments), the ILO convention on indigenous and tribal peoples (47), and the renewal of a law concerning the definition of the indigenous group (40 comments). Independent of where the discussion started, it eventually led to the question of the definition of the indigenous group and who should belong to it. Discussion on the website was anonymous. This also required some knowledge of the topic or at least opinions about it.

### Convenience

All of the targets of research were easily accessible using high speed broadband connections, representing convenience, as most users would have fast internet access. Further, the texts were rather concise; the Facebook postings by the scientific journals were short, mostly briefly announcing new articles, and many of the news texts of the radio station consisted of a few lines, although there were also articles that were a few pages long. In these respects, the convenience of the web postings was about the same in all cases, and the research materials did not produce any differences.

The anonymity of discussion can also be viewed as an element of convenience; from time to time on the radio station website there were heated anonymous discussions and contradictory views about the conditions of membership of the indigenous group. These topics were missing entirely from the respective Facebook site where participants must publish opinions under their own names.

## Conclusions

In the results of both research materials: relevance, community and engagement are clearly involved in gains of attention for the web publications.

The audiences of both scientific journals and the YLE Sápmi radio station are special groups: the former mostly represents medical expertise, and the latter represents an indigenous group of speakers of Sámi minority languages. The Facebook postings of the medical journals concerned the contents of the journals and received plenty of likes, shares and comments. The significance of the relevance was emphasized by the lack of attention to the Facebook postings not connected to the articles of the respective journals. Likewise, attention was concentrated on the YLE Sápmi radio news items concerning the indigenous group, and its cultural and political news, whereas topics outside the group were left without attention. This clearly reveals the selectivity of the attention process ([Davenport and Beck 2001](#dav01)).

Some scientific journals had engaging elements ([Davenport and Beck 2001, 115](#dav01)), such as competitions (Image Challenge) and polls that encouraged readers to participate. These devices produced the highest figures in the attention data. This also produced community in the sense of belonging, ownership, and co-creation ([Chatfield 2013](#cha13)), and was expressed by uploading the reader’s own information on Facebook. In the case of the scientific journals this required some medical expertise.

In the radio news the most engaging items were news stories touching on the definition of the indigenous group. The engaging aspect originated from controversial views surrounding the definition, producing critical and sometimes lengthy discussions. The discussion originated from a variety of topics such as changes in legislation, exploitation of cultural artefacts and a doctoral dissertation concerning the Sámi group. Eventually, however, the discussion inevitably turned to the definition of the Sámi and who should belong to the indigenous group. Participating in this discussion required a degree of knowledge, or at least opinions about the indigenous group. The discussion was anonymous and took place on the radio news website, while it did not exist on the Facebook site showing participants’ names. Instead, any Facebook postings mostly concerned positive cultural topics. Liking these items indicates support of the culture of the minority group, and strengthens their ownership.

Convenience in terms of the users’ fast internet connections and availability over the internet was present in all cases. However, only in the case of the radio station did it lead to any differences: the anonymity policy concerning web discussions produced critical discussions from time to time that were not present on the Facebook site of the radio station.

The scarcity of attention ([Simon 1971, 40](#sim71); [Davenport and Beck 2001, 25](#dav01); [Ott 2011](#ott11); [Chatfield 2013](#cha13)) was indicated in both sets of the materials studied. Only 24 percent of YLE Sápmi radio news items received any attention at all. All Facebook postings of six journals studied received some attention, but 90 % of one and 44 % of another journal were left without any attention. The distribution of attention was highly skewed.

Social media brings the roles of the authors and publishers closer to that of the audience. Readers can adopt the role of “produsers” who not only read but also publish their views and are in this way involved in the co-creation of content. In addition to attention, altmetric research material simultaneously implies connections and impact beyond academia which is regarded as an important attribute of research projects and publications.

Research material in an altmetric study is produced for other reasons, not for research purposes, and can consequently be regarded as authentic and unobtrusive. While citations as indications of attention only concern scientific publications, digital footprints can originate from many sources, representing highly varying audiences and indicating attention to many different kinds of publications, projects or websites. The limitation in this respect is that some indications of attention, such as liking, do not require applying much thought to the topic. Nevertheless, it is user generated, indicating awareness and attention.

<a id="author"></a>

## About the authors

**Mari Katvala**, Ph. D., is information specialist at the Oulu University Library, P.O. Box 7500, 90014 University of Oulu, Finland, with open science and publishing as her area of special expertise. She completed her PhD at the University of Oulu in the field of evolutionary ecology, and she has also worked as a post doctorate researcher at the University of Uppsala focusing on questions of diversification of species. In addition to biology, she has studied information studies. She can be contacted at [mari.katvala@oulu.fi](mailto:mari.katvala@oulu.fi).  
**Terttu Kortelainen**, Ph.D., docent, is professor of Information Studies at the University of Oulu, P.O. Box 1000, 90014 University of Oulu, Finland. Her research interests are in the attention economy, informetrics, social media, and information literacy. Her publications consist of study books and articles on informetrics, and on several research projects. She is member of the Finnish Publication Forum Project, the editorial board of Tampere University Press, and the publication board of the Finnish Information Studies publication series. She can be contacted at [terttu.kortelainen@oulu.fi](mailto:terttu.kortelainen@oulu.fi)  
**Anni-Siiri Länsman** is D.Soc.Sc. in sociology. She is director of the Giellagas Institute for Studies of the Sámi cultures and languages in the University of Oulu, P.O. Box 1000, 90014 University of Oulu, Finland. Her research interests are in cultural studies, especially in indigenous issues and Sámi cultures and Sámi society. She is an active member of the Nordic network of Sámi studies and the member of the board of the Sámi Research Programme II in the Research Council of Norway. She can be contacted at [anni-siiri.lansman@oulu.fi](mailto:anni-siiri.lansman@oulu.fi).

</section>

<section>

## References

<ul>
<li id="bor07">Borgman, C. (2007). Scholarship in the digital age: information, infrastructure, and the internet. Cambridge (Mass.): MIT Press.
</li>
<li id="cha13">Chatfield, T. (2013). The attention economy. Aeon Magazine October, 2013. http://aeon.co/magazine/world-views/does-each-click-of-attention-cost-a-bit-of-ourselves/. (Archived by WebCite® at <a href="http://www.webcitation.org/6jsOefuRq" target="_blank">http://www.webcitation.org/6jsOefuRq</a>).
</li>
<li id="dav01">Davenport, T.H. &amp; Beck, J.C. (2001). The attention economy. Understanding the new currency of business. Boston, MA: Harvard Business School Press.
</li>
<li id="eys11">Eysenbach, G. (2011). Can Tweets predict citations? Metrics of social impact based on Twitter and correlation with traditional metrics of scientific impact. Journal of Medical Internet Research, 13(4):e123. Doi:10.2196/jmir.2012. http://www.jmir.org/2011/4/e123/ URL (Archived by WebCite® at <a href="http://www.webcitation.org/6jsQ6VAww" target="_blank">http://www.webcitation.org/6jsQ6VAww</a>).
</li>
<li id="fin16">Finnpanel (2016). TV audience measurement. http://www.finnpanel.fi/en/tv.php (Archived by WebCite® at <a href="http://www.webcitation.org/6jsQFhHH5" target="_blank">http://www.webcitation.org/6jsQFhHH5</a>).
</li>
<li id="fra02">Frank, G. (2002). The scientific economy of attention: a novel approach to the collective rationality of science. Scientometrics, 55(1), 3-6.
</li>
<li id="gra08">Gray, K., Thompson, C., Clerehan, R., Sheard, J. &amp; Hamilton, M. (2008). Web 2.0 authorship: issues of referencing and citation for academic integrity. The Internet and Higher Education, 11(2), 112-118. (Archived by WebCite® at <a href="http://www.webcitation.org/6jsQilhYc" target="_blank">http://www.webcitation.org/6jsQilhYc</a>).
</li>
<li id="hol15">Holmberg, K., Digegah, F., Bowman, S., Bowman, T.D. &amp; Kortelainen, T. (2015). Measuring the societal impact of open science – presentation of a research project. Informaatiotutkimus 34(4), 119-123. http://ojs.tsv.fi/index.php/inf/article/view/53511/16668 (Archived by WebCite® at <a href="http://www.webcitation.org/6jsQtfXnU" target="_blank">http://www.webcitation.org/6jsQtfXnU</a>).
</li>
<li id="kor12">Kortelainen, T. &amp; Katvala, M. (2012). “Everything is plentiful – Except attention”. Attention data of scientific journals on social web tools. Journal of Informetrics, 6: 661-668.
</li>
<li id="kor15">Kortelainen, T. &amp; Länsman, A.-S. (2015). Challenging the status and constructing the culture of an indigenous group : attention paid to YLE Sápmi radio digital news in Finland. European Journal of Cultural Studies 18 (6), 690-708 .
</li>
<li id="ott11">Ott, A.C. (2011). The invisible hand of time: How attention scarcity creates innovation opportunity. Ivey Business Journal March/April 2011. (Archived by WebCite® at <a href="http://www.webcitation.org/6jyILtghy" target="_blank">http://www.webcitation.org/6jyILtghy</a>).
</li>
<li id="pri10">Priem, J. &amp; Hemminger, B. (2010). Scientometrics 2.0: toward new metrics of scholarly impact on the social web. Firstmonday, 15(7 July) http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/2874/2570 (Archived by WebCite® at <a href="http://www.webcitation.org/6jsRWzmAa" target="_blank">http://www.webcitation.org/6jsRWzmAa</a>).
</li>
<li id="pritar10">Priem, J., Taraborelli, D., Groth, P. &amp; Neylon, C. (2010). Altmetrics: a manifesto, 26 October 2010. http://altmetrics.org/manifesto (Archived by WebCite® at <a href="http://www.webcitation.org/6jsRcO2fw" target="_blank">http://www.webcitation.org/6jsRcO2fw</a>).
</li>
<li id="sim71">Simon, H.A. (1971). Designing organizations for an information-rich world. In: Greenberger, M. (Ed.) Computers, communications, and the public interest. Baltimore, MD: The John Hopkins Press. p. 38-72.
</li>
<li id="yar09">Yardi, S., Golder, S.-A. &amp; Brzozowski, M.J. (2009). Blogging at work and the corporate attention economy. In: CHI 2009 – Social software in office April 4-9. http://www.academia.edu/2914126/Blogging_at_Work_and_the_Corporate_Attention_Economy. (Archived by WebCite® at <a href="http://www.webcitation.org/6jyIlXj6c" target="_blank">http://www.webcitation.org/6jyIlXj6c</a>).
</li>
</ul>

</section>

</article>

* * *

<section>

## Appendices

Links to the web pages of the research material:

Annals of Internal Medicine [http://annals.org/](http://annals.org/)

British Medical Journal [http://www.bmj.com/thebmj](http://www.bmj.com/thebmj)

Gender & Society [http://gas.sagepub.com/](http://gas.sagepub.com/)

Journal of the American Medical Association [http://jama.jamanetwork.com/journal.aspx](http://jama.jamanetwork.com/journal.aspx)

Journal of the American Medical Informatics Association [http://jamia.oxfordjournals.org/](http://jamia.oxfordjournals.org/)

The Lancet [http://www.thelancet.com/](http://www.thelancet.com/)

MIS Quarterly [http://www.misq.org/](http://www.misq.org/)

New England Journal of Medicine [http://www.nejm.org/](http://www.nejm.org/)

YLE Sápmi News, Finnish Broadcasting Company YLE [http://yle.fi/uutiset/sapmi/](http://yle.fi/uutiset/sapmi/)

</section>