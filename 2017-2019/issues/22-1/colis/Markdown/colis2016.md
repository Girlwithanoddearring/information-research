<header>

#### vol. 22 no. 1, March, 2017

</header>

# Proceedings of CoLIS: 9th International Conference on Conceptions of Library and Information Science - Uppsala University, June 27-29, 2016

<article>

<section>

## Keynote address

Louise Limberg | [Synthesizing or diversifying library and information science. Sketching past achievements, current happenings and future prospects, with an interest in including or excluding approaches](colis1600.html)

## Perspectives to practices

Michael Olsson and Annemaree Lloyd | [Being in place: embodied information practices.](colis1601.html)

Ola Pilerot, Björn Hammarfelt and Camilla Moring | [The many faces of practice theory in library and information studies.](colis1602.html)

Sally Irvine-Smith | [Information through the lens: information research and the dynamics of practice.](colis1603.html)

## Literacies, competences and learning.

Cecilia Andersson | [The front and backstage: pupils’ information activities in lower secondary school.](colis1604.html)

Trine Schreiber | [E-learning objects and actor-networks as configuring information literacy teaching.](colis1605.html)

Syeda Hina Batool and Sheila Webber | [Conceptions of school libraries and the role of school librarians: findings from case studies of primary schools in Lahore.](colis1606.html)

Gillian Oliver | [The records perspective: a neglected aspect of information literacy.](colis1607.html) [Short paper]

Eamon Tewell | [Resistant spectatorship and critical information literacy: strategies for oppositional readings.](colis1610.html)

Veronica Johansson and Louise Limberg | [Seeking critical literacies in information practices: reconceptualising critical literacy as situated and tool- mediated enactments of meaning.](colis1611.html)

Åse Hedemark and Jenny Lindberg | [Stories of story-time: the discursive shaping of professional identity among Swedish children’s librarians.](colis1612.html)

Jutta Haider | [Controlling the urge to search. Studying the informational texture of practices by exploring the missing element.](colis1613.html)

## Information and its users

Hilary Yerbury and Ahmed Shahid | [Social media activism in Maldives: information practices and civil society.](colis1614.html)  

John Mowbray, Hazel Hall, Robert Raeside and Peter Robertson | [The role of networking and social media tools during job search: an information behaviour perspective.](colis1615.html)

Bhuva Narayan and Medina Preljevic | [An information behaviour approach to conspiracy theories: listening in on voices from within the vaccination debate.](colis1616.html)

Polona Vilar and Alenka Šauperl | [Archivists about students as archives users.](colis1617.html)

Kjell Ivar Skjerdingstad | [The anatomy of excitement: teenagers’ conceptualizations of literary quality.](colis1618.html)

Sylwia Frankowska-Takhari, Andrew MacFarlane, Ayşe Göker and Simone Stumpf | [Selecting and tailoring of images for visual impact in online journalism](colis1619.html)

Tanja Merčun and Maja Žumer | [Exploring the influences on pragmatic and hedonic aspects of user experience.](colis1621.html) [Short paper]

## Research on research

Pär Sundling | [Library and information science according to the citing pattern of students: a bibliometric study.](colis1622.html)

Fredrik Åström, Björn Hammarfelt and Joacim Hansson | [Scientific publications as boundary objects: theorizing the intersection of classification and research evaluation.](colis1623.html) [Short paper]

Fereshteh Didegah, Ali Gazni, Timothy D. Bowman and Kim Holmberg | [Internationality in Finnish Research: an examination of collaborators, citers, tweeters, and readers.](colis1624.html) [Short paper]

Tove Faber Frandsen | [Citing the innovative work of the original inventors: an analysis of citation to prior clinical trials.](colis1625.html)

Terttu Kortelainen, Mari Katvala and Anni- Siiri Länsman | [Attention and altmetrics.](colis1626.html)

## Concepts, theories and paradigms

Jeppe Nicolaisen | [The problem of probability: an examination and refutation of Hjørland’s relevance equation.](colis1627.html)

Birger Hjørland | [A rejoinder to Nicolaisen’s refutation of Hjørland’s relevance definition.](colis1628.html)

Ciaran B. Trace | [Phenomenology, experience, and the essence of documents as objects.](colis1630.html)

Theresa Anderson and Simon Knight | [Learning analytic devices: co-forming, re-forming, in- forming. [Short paper]](colis1633.html)

Fidelia Ibekwe-Sanjuan | [The journey of information: how students perceive information in France using the draw and write technique.](colis1634.html)

Jenna Hartel | [Information behaviour, visual research, and the information horizon interview: three ways.](colis1635.html)

Tim Gorichanaz | [Genre, format and medium across the information professions.](colis1636.html)

## Methods and methodologies

Johanna Rivano Eckerdal and Charlotte Hagström | [Qualitative questionnaires as a method for information studies research.](colis1639.html)

Brian L. Griffin | [Metatheory or methodology? Ethnography in library and information science.](colis1640.html)

## Public libraries

Ragnar Andreas Audunson and Sunniva Evjen | [The public library: an arena for an enlightened and rational public sphere? The case of Norway.](colis1641.html)

Andreas Vårheim | [Public libraries, community resilience, and social capital.](colis1642.html)

Michael Widdersheim and Masanori Koizumi | [Methodological frameworks for developing a model of the public sphere in public libraries.](colis1643.html) [Short paper]

Michael Widdersheim | [Late, lost, or renewed? A search for the public sphere in public libraries.](colis1644.html)

Shannon Crawford Barniskis | [To what ends, by which means? The development of the library faith from moral uplift to makerspace.](colis1646.html)

## Knowledge organization

Jack Andersen | [Genre, the organization of knowledge and everyday life.](colis1647.html)

Deborah Lee | [Conceptions of knowledge about classification schemes: a multiplane approach.](colis1648.html)

Melanie Feinberg | [The value of discernment: making use of interpretive flexibility in metadata generation and aggregation.](colis1649.html)

## Information and work

Esther Ebole Isah and Katriina Byström | [Enacting workplace information practices: the diverse roles of physicians in a health care team](colis1650.html)

Katriina Byström, Ian Ruthven and Jannica Heinström | [Work and information: which workplace models still work in modern digital workplaces?](colis1651.html)

Anita Nordsteien | [Handling inconsistencies between information modalities - workplace learning of newly qualified nurses](colis1652.html)

## Mediation

Sanjica Faletar Tanackovic, Koraljka Golub and Isto Huvila | [The meaning of interoperability and its implications for archival institutions: challenges and opportunities in Croatia, Finland and Sweden.](colis1653.html)

Kim Tallerås and Nils Pharo | [Mediation machines: how principles from traditional knowledge organization have evolved into digital mediation systems.](colis1654.html)

Ulrika Kjellman | [Images as scientific documents in Swedish race biology: two practices.](colis1655.html)

</section>

</article>

* * *

> Thanks to Isto Huvila of Uppsala University, for organizing the conversion of the papers to html and to Jenny Phan and Astrid Wiezell, for carrying it out so well. The papers were double-blind, peer-reviewed for the Conference but have not been through the journal's copy-editing and final proof-reading and, in general, may not fully conform to the journal's style requirements and standards. The papers are listed under the session theme headings and, within themes, in the order of the conference programme.