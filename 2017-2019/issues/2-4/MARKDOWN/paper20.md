####  Vol. 2 No. 4, April 1997


# Distance education as a new possibility for librarians in Estonia

#### [Sirje Virkus](mailto:sirvir@tpu.ee)  
[Department of Information Studies](http://www.tpu.ee/~sirvir/depart.htm)  
Faculty of Social Sciences  
[Tallinn Pedagogical University](http://www.tpu.ee)  
Estonia

## Introduction

Since Estonia regained its independence in 1991, essential technical, economic, political and social changes have taken place here. All sectors of our economy require greater access to information than ever before. Complex changes are also facing libraries, librarians and education in Estonia.

The systematic transition to modern information technology in Estonian libraries started in 1992 when a plan for establishing information system for libraries was worked out. According to the Plan, basic technology will be converted to modern information technology during the period 1996-2005\. As a result of that plan 1500 workplaces with modern information technology will be established in 591 libraries in Estonia. At present there are 1284 libraries in Estonia, among them 604 public libraries, 745 school libraries, 141 special and research libraries. There are 3135 librarians working in Estonian libraries, of whom 48% are professionally educated [(Markus 1996)](#markus).

To define continuing education needs, a special questionnaire was used by the Estonian Librarians Association (ELA) among the members of ELA in February 1996\. Almost 300 librarians answered the questionnaire. The analysis of the results showed that education and training is needed mostly in the fields based on modern information technology [(Talvi 1996)](#talvi).

According to the results of the questionnaire and the development plan for Estonian libraries mentioned above, the demand for continuing professional education for library and information professionals with the knowledge and skills of modern information technology exists and will increase significantly.

In order to help information professionals to keep abreast of the rapidly changing environment and to cope with growing continuing education needs, the Centre for Information Work was established within the Department of Information Studies at the Tallinn Pedagogical University (TPU) in June 1995\. In addition to traditional face-to-face courses, distance education as a new form and method of education has been implemented in the Centre.

## Distance education in Estonia

Distance higher education in Europe is at the beginning of the third phase in its evolution [(Bang 1995)](#bang) and distance learning, flexible learning, resource-based learning, open learning and computer-mediated learning are becoming commonplace in the progressive educator's vocabulary. Still, there is no widespread familiarity with the concept of modern distance education in Estonia. Distance education is associated by most people with the kind of correspondence education that was offered under the old regime and is often also associated with its most evident manifestation, i.e. technology. It is therefore important to convey an understanding of modern distance education to the Estonian educational systems [(Distance 1993](#distance)).

At their meeting January 29, in 1993, the Nordic Council of Ministers made a ruling to support the Action Programme for the Baltic countries and neighbouring areas. In this Action Programme provisions were also made for educational projects in distance education. On this basis, the Council accepted an application from the President of the European Distance Education Network (EDEN) on behalf of the national associations of distance education in Finland, Norway, and Sweden to perform a feasibility study in the Baltic countries. The aims of the feasibility study were to:

*   assess the state of art of distance education in the three Baltic states, as well as to
*   identify the needs that exist for further development of distance education in the region [(Distance..., 1993)](#distance).

In March 1993 the Central and Eastern European countries proposed, in the framework of the working group on regional initiatives in human resources, to establish a Regional Distance Education Network in co-operation with PHARE. As a first step a feasibility study on the development of such a Regional Distance Education Network in Central and Eastern Europe was commissioned and the European Association of Distance Teaching Universities  (EADTU) was contracted to carry out this study with the Ministry of Culture and Education of Hungary as co-ordinator. Feasibility studies on the development of modern distance education were carried out in 1993 by groups of experts engaged by the Nordic Council of Ministers and PHARE.

In October 1993, at a seminar in Budapest for the official representatives of all eleven PHARE countries, it was agreed unanimously that a Regional Distance Education Network should be set up with a long-term perspective. Following the feasibility study, a financing proposal was agreed in 1994 for a three million ECU pilot project called PHARE Pilot Project for Multi-Country Co-operation in Distance Education.

The pilot project aimed:

*   to act as a catalyst for national policy formulation in the field of distance education through measures for awareness raising, staff development, the presentation of existing models and mechanisms of distance education;
*   to establish a network of National Contact Points in the participating countries and to develop the necessary infrastructure in all countries to allow them to co-operate on an equal basis;
*   to develop on an experimental basis two pilot courses (European Studies and Training of Distance Trainers), thereby testing the feasibility of joint development by the participating countries of core course modules which can then be adapted to national requirements and context;
*   on the basis of the above items, to define areas of common interests in which regional co-operation can produce an important added value, in terms of enhanced quality of outputs, speed of development and/or economies of scale [(Benders, 1996)](#benders).

The Estonian National Contact Point was established on 7th November,1994 and Regional Centres were set up at the Tallinn Pedagogical University, the Tartu University and the Tallinn Technical University in the framework of PHARE Programme for Multi-Country Co-operation in Distance Education. On August 22nd 1996 the international seminar ["Modern Training and University Education"](http://www.tpu.ee/~i-foorum/act.htm) was arranged at the Tallinn Pedagogical University to discuss what has happened in the field of distance education in Estonia. At this Seminar the main obstacles to delivering distance education widely in Estonia were pointed out:

*   lack of appropriate literature and printed materials;
*   lack of modern knowledge;
*   old-fashioned academic staff and subject-oriented teaching;
*   lack of ideology and/or policy at the strategy level about distance education;
*   few active learners [(Jõgi 1996)](#jogi).

Still, it was mentioned that the rapid development of a national telecommunication network provides a possibility to make use of the most recent information technology in the field of distance education. Connecting the libraries, information centres, universities and schools to the Internet gives good opportunity to disseminate distance education all over Estonia.

## Distance education in library and information science education

Education is in the midst of a monumental technological paradigm shift, one that will eventually change the way that all instructors teach and the way all students learn ([Jensen 1993)](#jensen). Information technology offers many options for the delivery of continuing education. Teaching, learning and learner support activity in higher education is moving increasingly into the new educational space created by electronic networks. A new mode of learning - _network learning_ - is set to become a basic feature of higher education environment, and also holds considerable promise for the delivery of continued professional education and training [(Levy & Fowell, 1995)](#levy).

For the transition from conventional learning to flexible learning, especially targeted pilot projects will be used in the Centre for Information Work of the Department of Information Studies at the TPU. The Department is currently exploring and developing new pedagogic models for learning, using electronic support and learner-centred approaches to provide education to remote students. Our department has taken great interest in the use of network possibilities for distance learning purposes.

## Distance learning pilot project for school librarians

The distance education pilot project for school librarians is the first project in the Centre adopting flexible learning methods. Steps in the design and development of courses, identification of areas of need, selection of appropriate learning media, etc. began in 1995\. The distance education pilot project for school librarians started in May 1996 and will end in June 1997\.

### The Aim of the Project

The aim of the project is to bring quality in service and continuing education to school librarians in Estonia. This project also aims to enable participants to become aware of the potential of network possibilities, to gain some basic skills about network information seeking and retrieval, publishing on the Internet via the World Wide Web, public relations and marketing, user education, etc. Today, from the computer at home or at workplace, the educator or learner can access to the vast amount of library catalogues, journal indexes, reference books, full text of journal articles and books, art exhibits, employment notices, discussion groups, business data, etc. The learners in schools need help to convert information into knowledge, to avoid information overload and to identify the best sources for the specific needs and abilities of each learner. We do hope that the school library will play an active part in the educational process of every school in Estonia and will support the work of teachers and students in showing how to make use of the modern information sources available to them.

### The choice of technology

Today there is a variety of technology available for transfer of knowledge from competence centres to specific target group: print - electronic publishing; television and radio broadcasts through earth stations, cable and satellites; audio and video cassettes; computer based training; interactive video, CDI and CD-ROM; multimedia; telephony; audio conferencing; video conferencing; e-mail and computer conferencing; audio graphics, etc. [(Bang 1995)](#bang). This technology gives the possibility of reaching target groups, who - under normal circumstances - are disadvantaged regarding geography, economy, family, job and so on. We may say that recent innovations in technology have expanded distance education opportunities and even blurred the boundaries between distance and traditional education.

The Internet is a relatively new medium for enhancing and delivering distance learning courses. As a tool for distance learning, the Internet can both deliver the content and serve as a multipurpose communication tool. The Internet also supports the open learning concept: allowing students to connect to educational resources when it is convenient for them, to explore the educational resources in an order that suits their needs and to learn at their own pace.

The choice of media will depend on the nature of the subject matter and skills to be covered, the knowledge level and prior skills of the learners, and the learners' and instructors' access to equipment. The choice of instructional media also depends greatly upon the budget of course developers.

Given the relatively high degree of access which schools have to computing and network facilities, electronic communication, especially synchronous and asynchronous communication possibilities of Internet, has an important role in the promotion of distance education to school librarians in Estonia. Many schools possess computers in Estonia and our schools are much more better equipped with information technology than our public libraries. The online aspect of distance education will continue to grow in Estonia as the number of schools equipped with telecommunications equipment and computers increase. There is a project called the ["Tiger Leap"](http://www.tiigrihype.ee) for school computerisation in Estonia. During the first step of the "Tiger Leap" project in 1997-1999 it is planned to supply all Estonian schools with computers (one computer with Internet connection per 10 to 20 students). Delivering distance education through Internet is also relatively inexpensive compared with another media in Estonia and teachers of informatics in schools may also support school librarians in their network learning activity.

We have used possibilities of the Internet in the Pilot Project for:

*   presentation of the learning material;
*   delivering course materials;
*   interaction between the learner and the learning material, and
*   communication between the learner and the teacher/tutor or among the learners themselves.

We have used synchronous communication possibilities such as talk and asynchronous communication possibilities such as electronic mail, listserves and WWW, telnet, ftp, gopher. Learning materials have been distributed as ordinary mail (as ASCII-files) or as attachments (for formatted documents) to learners. The learning materials also appear in the form of World-Wide Web (WWW) pages accessible over the Internet and students are encouraged to explore the local and global networked information resources as well. We have created a set of Web pages for the course with pointers to some useful information sources for school librarians.

In order to offer common communication during a course, when all the group members shall receive the same information, KR-LIST has been created for school librarians. It enables course discussions and sending questions or comments to teaching faculty or classmates. Students submit written assignments to teaching faculty through e-mail, and assignments are returned with comments and suggestions in the same fashion.

### The content

The Pilot project consists of four modules. Each module consists of 40 hours and includes face to face session, self studying, tutorial and group works. An introductory session of 16 hours was given during the first module using the traditional face-to-face method and 12 additional hours were offered in order to prepare students for the telematics-based components of the course. Key aspects of distance education were covered and terms as distance education, flexible learning, resource-based learning and open learning were introduced to school librarians. An overview about challenges and school possibilities was also given. Topics covered include basic network concepts, Internet basic tools, network information seeking and retrieval, publishing on the Internet via the World Wide Web, public relation and marketing, user education and consulting.

Some of the advantages and difficulties experienced up to the present in using the Internet for educational delivery have been described below. The complete evaluation of the Pilot Project will follow in June 1997.

Advantages:

*   providing learners with the ability to connect to learning resources when it is convenient for them;
*   the possibility of learning at their own pace;
*   allowing learners to use the educational resources in an order that suits their needs;
*   lower costs in producing of course materials compared to printing the same materials;
*   faster methods for revising and re-distributing course materials compared to print materials;
*   the ability to re-use lecture materials by simply providing links to previous electronic course modules or externally stored materials on the Internet;
*   using the World Wide Web for delivering course materials allows lecturers to produce content on a single platform, still the content is accessible by learners using a wide range of computing platforms and WWW browsers.

Difficulties:

*   school librarians' experience in information and communication technology was relatively low; only one had practical network experience before;
*   it seems to us that even when school librarians have access to computers and knowledge about them generally, they still need considerable time to master the techniques involved in using e-mail for discussions and communications;
*   not all students are suited for Internet-based education; they are not able to express themselves as well using the computer-based communication methods as in direct conversation with their lecturers in the classroom;
*   some students feel isolated, lack confidence in their own abilities, and require careful support and encouragement;
*   students are not always highly motivated for personal competence development to study in isolation;
*   the cost of computer equipment and communications infrastructure limits the number of students who can afford an Internet-based course;
*   poor technical support or tutorial help can lead to incorrect usage of software tools.

On 20th September 1996 the Department of Information Studies received a subsidy from the Open Estonian Foundation to start the long-term project on distance education for library and information professionals entitled Information Technology in Libraries. The aim of the project is to start continuing professional education for library and information professionals using distance education methods in order to promote the realisation of the development program of the Estonian Library Information System.

The Project is planned for the period 1996-2000 and will include three stages:

*   distance education for librarians of scientific libraries;
*   distance education for librarians of central public libraries;
*   distance education for librarians of the other public libraries and school libraries.

The project includes different modules: basic computer skills, integrated library systems, Internet basic tools, information seeking and retrieval in Internet and in commercial databases, generation of databases, electronic publishing, methods of analysing of information, Estonian information resources in the network environment, user education and consulting, etc.

## Conclusion

This paper presents a brief overview of distance education as a new important field within the overall development of continuing education in Estonia. The Department of Information Studies of TPU is currently exploring and developing new pedagogic models for learning, using electronic support and learner-centred approaches to provide education to remote students. Our department has taken a great interest in the use of network possibilities for distance learning purposes. The distance education pilot project for school librarians has indicated the obvious distance education advantages of Internet, the difficulties in using this rapidly changing technology and has given us experience to improve the quality of distance learning in the Department of Information Studies of the TPU in future and enable to apply obtained skills and knowledge in the long-term project entitled Information technology in libraries (1996-2000). Through the pilot project the technology will be tested and evaluated in order to develop methods for future use.

## References

<a name="bang">Bang, J. (1995)</a>. Curriculum, pedagogy and educational technologies. _EADTU-News_ (18): 35-42\.

<a name="benders">Benders, J. (1996)</a>. A new multi-country distance education network for Central Europe: Implementation of the transregional component of the PHARE pilot project for multi-country co-operation in distance education through technical assistance by European Association of Distance Teaching Universities (EADTU). PHARE Workshop, 17 January 1996\.

<a name="distance">Distance (1993)</a>. Distance education in Estonia, Latvia and Lithuania. Report on a feasibility study to the Nordic Council of Ministers. Oslo.

<a name="jensen">Jensen, R. (1993)</a>. The technology of the future is already here. _CAUT Bulletin_, 40: 14-15\.

<a name="jogi">Jõgi, L. (1996)</a>. Introduction distance education possibilities in the Program for continuing training of teachers trainers in Estonia: distance education as a new possibility. Paper presented at the international seminar "Modern Training and University Education", 22 August, 1996, Tallinn/Estonia.

<a name="levy">Levy, P. and Fowell, S. (1995)</a>. Networked learning in LIS education and training. Paper presented at the EUCLID-FID/ET Conference, 21-22 November, 1995, Copenhagen.

<a name="markus">Markus, T. (1996)</a>. Library staff in the context of changing labour market. Papers of the 5th Congress of Baltic Librarians "Independence and Libraries", Tallinn/Estonia, October 21-22, 1996, 161-164\.

<a name="talvi">Talvi, K. (1996)</a>. Continuing education of Estonian librarians: needs and reality. Papers of the 5th Congress of Baltic Librarians "Independence and Libraries", Tallinn/Estonia, October 21-22, 1996, 152-155\.

<a name="virkus">Virkus, S. (1996)</a>. Distance education in library and information science education in Estonia. Papers of the 5th Congress of Baltic Librarians "Independence and Libraries", Tallinn/Estonia, October 21-22, 1996, 84-92\.