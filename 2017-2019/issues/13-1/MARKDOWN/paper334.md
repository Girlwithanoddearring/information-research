#### vol. 13 no. 1, March 2008



# Information behaviour, health self-efficacy beliefs and health behaviour in Icelanders' everyday life



#### [Ágústa Pálsdóttir](mailto:agustap@hi.is)  
Department of Library and Information Science, University of Iceland, Oddi v/Sturlugötu, IS-101 Reykjavík, Iceland

#### Abstract

> **Introduction.** The aim of this study is to gather knowledge about how different groups of Icelanders take advantage of information about health and lifestyle in their everyday life.  
> **Method.** A random sample of 1,000 people was used in the study and data was gathered as a postal survey. Response rate was 50.8%.  
> **Analysis.** K-means cluster analysis was used to draw four clusters based on the participants' purposive information seeking. To validate the cluster classification and describe the clusters further, they were examined in relation to a number of external variables related to information behaviour, as well as the variables sex, age and education. The health self-efficacy beliefs and the health behaviour of the clusters were also examined.  
> **Results.** The results indicate that four distinct groups of people exist, that differ not only regarding their information behaviour, but also in relation to their health self-efficacy beliefs and health behaviour.  
> **Conclusions.** The findings indicate that information seeking which is accompanied by a critical approach in the selection of information sources and low information behaviour barriers, together with high health self-efficacy beliefs, relates to the most healthy behaviour.



## Introduction

It is well known that people's lifestyles affect their health and well-being. Nevertheless, in spite of campaigns aimed at educating people about healthy living, there are indications that gaps exist between different groups in society, with some groups able to benefit more from these campaigns than others ([Nutbeam 2000](#nut00)). This calls attention not only to the need to encourage people to behave in ways that enhance their health, but also to the importance of enabling people to acquire the necessary skills that allow them to benefit from the knowledge that is available about healthy lifestyles. An understanding of people's information behaviour, and the abilities that different groups within society have to take advantage of information about healthy behaviour, is of importance to improve the outcome of health educational efforts ([Ginman, 2000](#gin00)).

The following literature review starts by discussing factors of information behaviour, which are information seeking behaviour, relevance judgments, barriers to information behaviour, as well as the connection with health behaviour. It then moves on by discussing the construct of self-efficacy and related studies.

## Information behaviour

Information behaviour has been defined as '_the totality of human behaviour in relation to sources and channels of information, including both active and passive information seeking and information use_' (Wilson, [2000](#wil00): 49). Active information seeking stands for a behaviour where individuals experience a lack of knowledge and act on it by purposively seeking information. It has, however, been pointed out that information seeking is not restricted to purposive information seeking. Wilson ([1997](#wil97)) discussed 'passive attention', for example, when the use of mass media results in information acquisition, although information seeking was not intended, while others have used terms such as 'incidental information acquisition' (Williamson, [1997](#wilk97)) and 'serendipitous retrieval' (Foster and Ford, [2003](#fos03); Toms, [2000](#tom00)). Erdelez ([1997](#erd97)) introduced the term 'information encountering' and further suggested the categorisation of super-encounterers, encounterers, occasional encounterers and non-encounterers.

Relevance judgements are an essential aspect of information behaviour. With an increasing variability of sources and channels to seek information from it has not only become more important to pay attention to what information sources are being favored by different groups of people but also the criteria that are being used when people choose between the various sources.

Saracevic ([1996)](#sar96) has put forward a classification model for relevance criteria that consists of five manifestations, two of them are of special interest here. Cognitive relevance stands for the relation between information and the person's knowledge state and the cognitive information need, suggesting cognitive correspondence, informativeness, information novelty and quality as a possible criteria. The other is situational relevance or utility, referring to how well the information relates to the problem dealt with, suggesting criteria such as the usefulness and the appropriateness of information.

Another important aspect is barriers that can influence information behaviour, such as difficulties in interpreting information ([Agada 1999](#aga99); [Mettlin and Cummings 1982](#met82)); beliefs about reliability ([Agada 1999](#aga99); [Chatman 1985](#cha85); [Julien 1999](#jul99)) and usefulness ([Mettlin and Cummings 1982](#met82); [Taylor 1991](#tay91)), as well as access to information ([Agada 1999](#aga99); [](#mck02)Wilson, [1981](#wil81), [1997](#wil97)).

Studies have underscored information behaviour as one of the most important factors for a successful outcome of health promotion and indicated that a relationship exists between information behaviour and health behaviour. Access to information ([Hafstad and Aarø 1997](#haf97); [Måns 1991](#man91)) and better knowledge about healthy behaviour ([Kurtz _et al._ 2003](#kur03); [Osler and Schroll 1995](#osl95)) has for example been associated with better lifestyles. However, it has also been pointed out that improvements in health do not affect all social classes equally, with the higher social classes being influenced first ([Prättälä _et al._ 1992](#pra92)). Among the explanations are that the higher classes can more easily interpret knowledge and convert it into a change in lifestyle ([Osler and Schroll 1995](#osl95) ). Seeking health information is, furthermore, not restricted to peoples own information needs as it also involves the caretaking of the family, with women being more likely to seek information for others than men ([Fox _et al._, 2000](#fox00); [Fox and Fallows 2003](#fox03); [Pennbridge _et al._ 1999](#pen99)).

While efforts are being made to provide people with information about healthy lifestyles, there are also indications about health inequalities across different groups in society which are not fully understood. Furthermore, few studies examine the factors that influence the everyday life information behaviour of people that are normally thought of as healthy.

## Self-efficacy

The history of health education has demonstrated that simply presenting information about health behaviour is insufficient. Newertheless, behaviour change theories such as the social cognitive theory by Bandura ([1997](#ban97)), have improved the understanding of how intrapersonal factors such as knowledge and beliefs, the social environment and behaviour, work together in a complex relationship ([Nutbeam, 2000](#nut00)). The theory emphasizes the importance of self-efficacy as a cognitive mediator of action.

Self-efficacy beliefs are people's expectations about whether or not they will be able to master a behaviour, and if so, how successful they will be. Individuals who believe that they will be able to perform well, and that the behaviour will lead to a favourable outcome, are considered likely to be more strongly motivated, set themselves higher goals and to have the strength to carry out the act than those who are low on self-efficacy beliefs ([Bandura 1997](#ban97)). Self-efficacy is being included as a component in most theoretical models of health behaviour, for example, the Theory of planned behaviour ([Ajzen 1985](#ajz85)), Protection motivation theory ([Maddux and Rogers 1983](#mad83)), the Health belief model ([Becker and Rosenstock 1987](#bec87)), the Health action process approach ([Schwarzer and Fuchs 1996](#sch96)) and the Transtheoretical Model of Change ([Prochaska and Velicer 1997](#pro97)).

Self-efficacy has been examined extensively in relation to healthy behaviour. The connection between people's health information behaviour and their judgements about how capable they are of managing their health in a successful way, however, has not gained much interest, although previous studies have examined the relationship between self-efficacy and aspects such as information seeking activity ([Rimal 2001](#rim01)), satisfaction with the information provided ([Stewart _et al._ 2004](#ste04)) and health education ([Dishman _et al._ 2004](#dis04); [Winkelby _et al._ 2004](#win04)).

The aim of the study is to examine how different groups of Icelanders take advantage of information about health and lifestyle in their everyday life. The study will classify people on the basis of their purposive information seeking behaviour, and further describe the characteristics of the different groups in relation to other aspects of their information behaviour, as well as their health behaviour and self-efficacy beliefs. As such, the thesis takes a social cognitive approach, but it is also based on concepts and ideas drawn from a review of the literature of information science. The overall research questions in the study were the following:

*   Is there a relationship between purposive information seeking about health and lifestyle and other aspects of information behaviour, and if so, what is the nature of this relationship?
*   Is there a relationship between information behaviour and health self-efficacy beliefs, and if so, what is the nature of this relationship?
*   Is there a relationship between information behaviour and health behaviour, and if so, what is the nature of this relationship?
*   Is there a relationship between health self-efficacy beliefs and health behaviour, and if so, what is the nature of this relationship?

## Method

### Data collection

The data were gathered as a postal survey during the autumn 2002\. The sample consists of 1,000 people at the age of eighteen to eighty, randomly selected from the National Register of Persons. Response rate was 50.8% which is satisfactory for statistical analysis ([Babbie 1979](#bab79)). The characteristics of the respondents which were compared with population parameters derived from Statistics Iceland ([Statistics Iceland 2003](#hag03)) are presented in Table 1\.

<table width="80%" border="1" cellspacing="0" cellpadding="5" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Characteristics of the respondents'sex, age and education compared with the population**</caption>

<tbody>

<tr>

<th>Demographic characteristics</th>

<th>Sample % (raw number)</th>

<th>Variation %</th>

<th>Confidence interval</th>

<th>Population % (raw number)</th>

</tr>

<tr>

<th colspan="5" style="text-align: left;">Sex</th>

</tr>

<tr>

<td>Men</td>

<td align="center">45.4% (231)</td>

<td align="center">+/- 4.3</td>

<td align="center">41.0-49.7</td>

<td align="center">50.01 (144.287)</td>

</tr>

<tr>

<td>Women</td>

<td align="center">54.6% (277)</td>

<td align="center">+/- 4.3</td>

<td align="center">50.3-59.0</td>

<td align="center">49.99 (144.184)</td>

</tr>

<tr>

<td>Total</td>

<td align="center">100% (508)</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">100% (203.046)</td>

</tr>

<tr>

<th colspan="5" style="text-align: left;">Age</th>

</tr>

<tr>

<td>18-29</td>

<td align="center">22.6% (115)</td>

<td align="center">+/- 3.7</td>

<td align="center">18.9-26.3</td>

<td align="center">15.1 (51.736)</td>

</tr>

<tr>

<td>30-39</td>

<td align="center">20.8% (106)</td>

<td align="center">+/- 3.6</td>

<td align="center">17.2-24.4</td>

<td align="center">20.3 (41.714)</td>

</tr>

<tr>

<td>40-49</td>

<td align="center">22.0% (112)</td>

<td align="center">+/- 3.7</td>

<td align="center">18.4-25.7</td>

<td align="center">21.1 (41.258)</td>

</tr>

<tr>

<td>50-59</td>

<td align="center">16.0% (81)</td>

<td align="center">+/- 3.2</td>

<td align="center">12.7-19.2</td>

<td align="center">25.4 (31.482)</td>

</tr>

<tr>

<td>60-80</td>

<td align="center">18.6% (94)</td>

<td align="center">+/- 3.4</td>

<td align="center">15.2-22.0</td>

<td align="center">18.1 (36.856)</td>

</tr>

<tr>

<td>Total</td>

<td align="center">100% (508)</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">100% (203.046)</td>

</tr>

<tr>

<th colspan="5" style="text-align: left;">Education</th>

</tr>

<tr>

<td>Primary</td>

<td align="center">31.6% (161)</td>

<td align="center">+/- 4.1</td>

<td align="center">27.5-35.7</td>

<td align="center">43.9 (89.137)</td>

</tr>

<tr>

<td>Secondary</td>

<td align="center">44.2% (224)</td>

<td align="center">+/- 4.3</td>

<td align="center">39.9-48.5</td>

<td align="center">43.9 (89.137)</td>

</tr>

<tr>

<td>University</td>

<td align="center">24.2% (123)</td>

<td align="center">+/- 3.7</td>

<td align="center">20.5-27.9</td>

<td align="center">12.2 (24.772)</td>

</tr>

<tr>

<td>Total</td>

<td align="center">100% (508)</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">100% (203.046)</td>

</tr>

</tbody>

</table>

Inspection of the confidence ntervals shows that the distribution of age falls within the 95% confidence limits for three out of five age groups. The sample is therefore considered to be representative of the Icelandic population regarding division of age. Examination of the confidence intervals for sex found that the distribution of men and women does not quite reflect the population. Educational level was measured as the highest level of completed education. Three levels were distinguished: primary school includes those who have finished compulsory education (ten years of education); secondary education (fourteen years) includes those who have completed vocational training, or secondary school; and university education. Inspection of confidence intervals shows that ratio of people with secondary education is representative of the division in the population. More people with university education and fewer with primary school education responded to the questionnaire compared with the division in the population.

### Measurements

_**Socio-demographic information.**_ Three variables were used: sex, age and education. The choice of the variables is based on results of previous analysis of the data.

_**Information seeking clusters**._ Respondents purposive information seeking was examined by asking: 'Have you **sought** information about health and lifestyle in any of the following sources'? A list of twenty-three information sources was presented at each question and people were asked to give an answer about every source on the scale 1-5, where 1 is lowest activity of purposive seeking and 5 is highest (see also [Pálsdóttir 2005](#agu05)). The information sources were grouped into four information channels, named: Media, Health specialists, Internet and Interpersonal sources. Total mean scores were computed for each channel. The information channels were tested for internal reliability, using Cranach's alpha. Reliability reflects the precision of each measure ([Chronbach 1951](#cro51)) and it is considered that a reliability of 0.70 is adequate ([Nunnally 1978](#nun78)). In all cases Cranach's alpha proved satisfactory, it was 0.88 for Media, 0.82 for Health specialists, 0.89 for Internet and 0.83 for Interpersonal sources.

_**Information behaviour**_, including the following:

_Information encountering_ was examined by asking: 'Have you **come across** information about health and lifestyle in any of the following sources, although you were not seeking this information'? The question had a five-point response scale (5: Very often - 1: Never).

_Relevance judgements_ were examined by asking two questions: 'How **useful** do you find information about health and lifestyle in the following sources'?, and 'How **reliable** do you find information about health and lifestyle in the following sources'? The questions had a five-point response scale (5: Very useful/reliable - 1: Don't know).

At the questions about information encountering and both questions about relevance judgement the same list of information sources was presented as at the question about purposive information seeking. It was expected that some of the items on the list were measuring the same factor or different aspects of the same factor, and that a scale could be used to measure each factor. The Principal Axis Factoring method was chosen above Principle Component Analysis as the aim was to extract latent factors rather than to simply reduce the data set ([Fabrigar _et al._ 1999](#fab99)). Three factors with an eigenvalue greater than 1.00 were extracted for each question and three scales constructed as a measure of information encountering, information usefulness and information reliability. The scales were named: Media, Internet, and Health specialists. Together, the factors explained 57% or more of the total variance of each question. Internal reliability of the scales was satisfactory; Cronbach's alpha ranges from 0.84 to 0.92 (see also [Pálsdóttir 2005](#agu05)).

_Barriers_, where ten questions about possible barriers, identified by previous studies were asked, including evaluations of cost hindrances (time and money), lack of awareness of information, beliefs about the availability, accessibility, and the trustworthy of information, and the ability to interpret or understand the information. A five-point response scale was used (5: Strongly disagree - 1: Strongly agree).

Factor analysis was conducted with Principal Axis Factoring with oblimin rotation to compute scales based on questions. Two factors with an eigenvalue greater than 1.00 were extracted, accounting for 56.3% of the total variance, and named: Cognitive barriers and Physical barriers. Cognitive barriers consisted of seven items. Three of them refer to capabilities of evaluating information relevance, one to the impact of language skills or educational capabilities, and three items refer to beliefs about access to information. Physical barriers consisted of three items which all refer to the situation that people live in, that is difficulties in getting away from home, as well as cost hindrances (time and finances). Internal reliability of the scales was satisfactory; Cronbach's alpha is 0.83 for Cognitive barriers and 0.77 for Physical barriers (see also [Pálsdóttir 2005](#agu05)).

_Motivation_, where two questions, about interest in health and lifestyle and how often the topic is discussed with others, were asked. Both questions had a five-point response scale (5: Very interested/Very often - 1: No interest at all/Never).

**_Health self-efficacy_** was measured by The Perceived Health Competence Scale (PHCS) which is an eight-item scale measuring both health outcome expectancies and health behavioural expectancies, at an intermediate level of specificity ([Smith _et al._ 1995](#smi95)). Each question has a five-point response scale (5: Strongly disagree - 1: Strongly agree). The scale was tested for internal reliability, and Cranach's alpha was 0.85 which is satisfactory ([Nunnally 1978](#nun78)).

A cluster analysis was conducted based on the responses to the scale. Two stages of cluster analysis were performed. First a Ward's method was used to create a dendogram showing the possible number of clusters. The Ward's method indicated that three to four clusters might exist. After that k-means clustering method was then used to draw a four-cluster solution ([Everitt _et al._ 2001](#eve00)). Table 2 shows the number of cases in each cluster.

<table width="50%" border="1" cellspacing="0" cellpadding="5" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Number of cases in the self-efficacy clusters**</caption>

<tbody>

<tr>

<td>Cluster 1 (Low self-efficacy cluster)</td>

<td align="center">76</td>

</tr>

<tr>

<td>Cluster 2 (Moderately low self-efficacy cluster)</td>

<td align="center">104</td>

</tr>

<tr>

<td>Cluster 3 (Moderate high self-efficacy)</td>

<td align="center">116</td>

</tr>

<tr>

<td>Cluster 4 (High self-efficacy cluster)</td>

<td align="center">207</td>

</tr>

<tr>

<td>Valid cases</td>

<td align="center">503</td>

</tr>

<tr>

<td>Missing cases</td>

<td align="center">4</td>

</tr>

</tbody>

</table>

**_Health behaviour_**. As an indicator of health behaviour two questions were asked on a five-point response scale were 1 is the lowest activity and 5 is highest.

_Exercise activity_ was examined by asking the respondents how often they exercised until they get breathless, their heartbeat gets stronger or they sweat.

_Dietary behaviour_ was examined by asking the respondents how often they consumed light food products (e.g. low fat milk and cheese, fish or low fat meat) rather than more fatty food products.

### Data analysis

Cluster analysis was conducted to determine how the respondents formed distinct groups, based on their purposive information seeking in twenty-three information sources. Cluster analysis is a multivariate statistical procedure that explores data sets and attempts to reorganize them into relatively homogeneous clusters, in such a way that within-group variation is minimized and between-group variation maximized. The intention is that each group should consist of objects that resemble each other in some respect and are as different from objects in other groups as possible ([Aldenderfer and Blashfield 1984](#ald84);[Everitt _et al._ 2001](#eve00)).

The cluster analysis was performed in two steps, as in the previous cluster analysis (self-efficacy clusters). The first step involved an agglomerative hierarchical clustering method, the Ward's method, which is a Euclidian distance method ([Everitt _et al._ 2001](#eve00)) and has been shown to outperform most other methods in identifying the number of clusters ([Aldenderfer and Blashfield 1984](#ald84)). This method was used to produce a dendogram illustrating a cluster fusion in order to identify the possible number of existing clusters. The next step in the analysis involved k-means clustering method, based on Euclidian distances. The advantage of using this method is that it does not require the individuals to be allocated to a cluster irrevocably. In order to improve the statistical fit of the solution the method reassigns individuals iteratively to clusters until each person is closer to the mean of their cluster than to any other cluster mean ([Everitt _et al._ 2001](#eve00)). Therefore, this method is likely to create more homogeneous clusters than, for example, hierarchical methods. The k-means method, however, requires that the number of clusters is specified beforehand. Ward's method indicated that three to four clusters might exist and in order to follow the recommendation to use the highest number of clusters, a four-cluster solution was drawn by the k-means method ([Everitt _et al._ 2001](#eve00)).

To assess the clusters preferences for the information channels Media, Health specialists, Internet and Interpersonal sources, and to further test for statistically significant differences across the clusters, a post-hoc test was conducted for each of the twenty-three information sources and statistical differences in the total mean scores across the clusters examined.

It needs, however, to be kept in mind that _post hoc_ tests were not performed to examine differences in channel preferences within the clusters. Nevertheless, by examining the total mean scores together with a set of the four highest mean scores for specific information sources, it is believed that assumptions can be made about each cluster's preference for information channels.

As a validation test, and in order to describe further the characteristics of the information seeking clusters, their relations to a number of external variables on information behaviour were examined ([Aldenderfer and Blashfield 1984](#ald84)). In the analysis of questions about information encountering and relevance judgements, the scales Media, Health specialists and Internet, constructed by factor analysis, were used. For the analysis of questions about barriers in information behaviour, scales constructed by factor analysis, named Cognitive barriers and Physical barriers, were used. The analysis was made in three steps:

1.  ANOVA was used to examine if there is a significant difference between the means of the clusters on the dependent variable. A post-hoc test (Tuckey) was then conducted to examine which of the four clusters differ significantly. When the dependent variable was skewed, a binary logistic regression was used.
2.  The relationship between the dependent variable and the background variables, sex, age and education, was measured by an appropriate significance test (t-test, F-test or chi-squared).
3.  Finally, the better to examine the relationship between the dependent variable and the clusters, a multiple analyses controlling for the background variables which were significantly related to the dependent variable was employed. Factorial analysis of variance (FANOVA), or binary logistic regression, was used in the final model. A _post-hoc_ test (Tuckey) was conducted to examine which of the four clusters differ significantly.

The variable information encountering on the Internet was skewed. It was transformed into a dichotomous variable and binary logistic regression used in the analysis. Respondents who encounter information on the Internet more often were given the value 1, and respondents who encounter information less often the value 0\. The 'passive' cluster was used as a comparison group which the other clusters were measured against.

In order to assess the self-efficacy beliefs of the information seeking clusters the relationship between the self-efficacy clusters and the information behaviour clusters was examined by chi-squared.

Finally, the relationship between the information seeking clusters and variables on health behaviour, which are exercise and diet, as well as the relationship between the self-efficacy clusters and variables on health behaviour, was examined. The analyses was made in three steps (as described above), controlling for the background variables sex, age and education in the final step of the analysis. Since the variables exercise and diet were skewed, both variables were dichotomized and binary logistic regression used in the analysis.

Regular exercisers were given the value 0 and non-regular exercisers were given the value 1\. Consumers of light food products were given the value 0 and non-consumers were given the value 1\. For the information seeking clusters, The 'passive' cluster was used as a comparison group which the other clusters were measured against. For the self-efficacy clusters, the 'high' self-efficacy cluster was used as a comparison group.

## Results

Based on the respondents purposive information seeking four clusters were drawn by cluster analysis. The study explored the socio-demographic characteristics of the information seeking clusters, their purposive information seeking behaviour and the relationship between the clusters and other factors of information behaviour, their health self-efficacy beliefs, as well as their health behaviour.

### The information seeking clusters

The number of cases in each of the information seeking clusters is shown in Table 3.

<table width="50%" border="1" cellspacing="0" cellpadding="5" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Number of cases in the information seeking clusters**</caption>

<tbody>

<tr>

<td>Cluster 1 (Active cluster)</td>

<td>77</td>

</tr>

<tr>

<td>Cluster 2 (Moderately passive cluster)</td>

<td>90</td>

</tr>

<tr>

<td>Cluster 3 (Moderately active cluster)</td>

<td>112</td>

</tr>

<tr>

<td>Cluster 4 (Passive cluster)</td>

<td>192</td>

</tr>

<tr>

<td>Valid cases</td>

<td>471</td>

</tr>

<tr>

<td>Missing cases</td>

<td>36</td>

</tr>

</tbody>

</table>

### Socio-demographic characteristics of the information seeking clusters

Results on the socio-demographic characteristics of the information seeking clusters are shown in Table 4.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Socio-demographic characteristics of the information seeking clusters**</caption>

<tbody>

<tr>

<th>Demographic characteristics</th>

<th>Passive % (raw number)</th>

<th>Moderately passive % (raw number)</th>

<th>Moderately active % (raw number)</th>

<th>Active % (raw number)</th>

</tr>

<tr>

<td colspan="5">**Sex**</td>

</tr>

<tr>

<td>Men</td>

<td align="center">56.8 (109)</td>

<td align="center">42.2 (38)</td>

<td align="center">39.3 (44)</td>

<td align="center">26.0 (20)</td>

</tr>

<tr>

<td>Women</td>

<td align="center">43.2 (83)</td>

<td align="center">57.8 (52)</td>

<td align="center">60.7 (68)</td>

<td align="center">74.0 (57)</td>

</tr>

<tr>

<th>Total</th>

<td align="center">100 (192)</td>

<td align="center">100 (90)</td>

<td align="center">100 (112)</td>

<td align="center">100 (77)</td>

</tr>

<tr>

<td colspan="5">**Age**</td>

</tr>

<tr>

<td>18-29</td>

<td align="center">17.4 (33)</td>

<td align="center">27.3 (25)</td>

<td align="center">16.4 (18)</td>

<td align="center">48.1 (37)</td>

</tr>

<tr>

<td>30-39</td>

<td align="center">20.1 (39)</td>

<td align="center">22.7 (21)</td>

<td align="center">22.7 (25)</td>

<td align="center">23.4 (18)</td>

</tr>

<tr>

<td>40-49</td>

<td align="center">25.0 (48)</td>

<td align="center">25.0 (22)</td>

<td align="center">21.8 (25)</td>

<td align="center">16.9 (13)</td>

</tr>

<tr>

<td>50-59</td>

<td align="center">14.7 (28)</td>

<td align="center">15.9 (14)</td>

<td align="center">18.2 (20)</td>

<td align="center">10.3(8)</td>

</tr>

<tr>

<td>60-80</td>

<td align="center">22.8 (44)</td>

<td align="center">9.1 (8)</td>

<td align="center">20.9 (24)</td>

<td align="center">1.3 (1)</td>

</tr>

<tr>

<th>Total</th>

<td align="center">100 (192)</td>

<td align="center">100 (90)</td>

<td align="center">100 (112)</td>

<td align="center">100 (77)</td>

</tr>

<tr>

<td colspan="5">**Education**</td>

</tr>

<tr>

<td>Primary</td>

<td align="center">37.7 (72)</td>

<td align="center">19.1 (18)</td>

<td align="center">33.9 (38)</td>

<td align="center">22.1 (17)</td>

</tr>

<tr>

<td>Secondary</td>

<td align="center">45.0 (87)</td>

<td align="center">40.4 (36)</td>

<td align="center">43.8 (49)</td>

<td align="center">46.8 (36)</td>

</tr>

<tr>

<td>University</td>

<td align="center">17.3 (33)</td>

<td align="center">40.4 (36)</td>

<td align="center">22.3 (25)</td>

<td align="center">31.2 (24)</td>

</tr>

<tr>

<th>Total</th>

<td align="center">100 (192)</td>

<td align="center">100 (90)</td>

<td align="center">100 (112)</td>

<td align="center">100 (77)</td>

</tr>

</tbody>

</table>

Women are a majority in all the clusters, except the 'passive' cluster. The 'active' cluster has the highest rate of young participants; almost half of its members are between eighteen and twenty-nine. There is also a tendency towards younger people being more prevalent in the 'moderately passive' cluster. The 'moderately active' and the 'passive' clusters have a fairly even age distribution and a higher rate of members from the older age groups than the other two clusters. The 'moderately passive' cluster is the best educated one, with the highest rate of members with university education and the lowest with primary school education. The 'active' cluster has the second best education. The 'passive' cluster has the lowest education, with the highest rate of members with primary school education and the lowest with a university degree.

### Purposive information seeking behaviour

Table 5 presents an overview of the mean scores for purposive information seeking across the information seeking clusters.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Mean scores for purposive information seeking accross the information seeking clusters**</caption>

<tbody>

<tr>

<th valign="top" width="345">Information channels and sources</th>

<th valign="top" width="150" align="center">Passive cluster (N=192)</th>

<th valign="top" width="165">Moderately passive cluster (N= 90)</th>

<th valign="top" width="150" align="center">Moderately active cluster(N=112)</th>

<th valign="top" width="150" align="center">Active cluster (N=77)</th>

</tr>

<tr>

<th valign="top" width="345">_Media_</th>

<td valign="top" width="150" align="center"><u>1.73</u> <sup>a</sup></td>

<td valign="top" width="165" align="center"><u>2.13</u><sup>b</sup></td>

<td valign="top" width="150" align="center"><u>3.11</u> <sup>c</sup></td>

<td valign="top" width="150" align="center"><u>3.07</u><sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Newspapers</td>

<td valign="top" width="150" align="center">1.88 <sup>a</sup></td>

<td valign="top" width="165" align="center">2.49 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.59 <sup>c</sup></td>

<td valign="top" width="150" align="center">3.35 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Newspaper or journal advertisements</td>

<td valign="top" width="150" align="center">1.69 <sup>a</sup></td>

<td valign="top" width="165" align="center">1.75 <sup>a</sup></td>

<td valign="top" width="150" align="center">3.05 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.16 <sup>b</sup></td>

</tr>

<tr>

<td valign="top" width="345">TV or radio news</td>

<td valign="top" width="150" align="center">1.85 <sup>a</sup></td>

<td valign="top" width="165" align="center">2.43 <sup>b</sup></td>

<td valign="top" width="150" align="center">**3.86 <sup>d</sup>**</td>

<td valign="top" width="150" align="center">**3.51 <sup>c</sup>**</td>

</tr>

<tr>

<td valign="top" width="345">TV or radio entertainment programmes</td>

<td valign="top" width="150" align="center">1.70 <sup>a</sup></td>

<td valign="top" width="165" align="center">1.76 <sup>a</sup></td>

<td valign="top" width="150" align="center">3.24 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.00 <sup>b</sup></td>

</tr>

<tr>

<td valign="top" width="345">TV or radio sport programmes</td>

<td valign="top" width="150" align="center">1.72 <sup>a</sup></td>

<td valign="top" width="165" align="center">1.92 <sup>a</sup></td>

<td valign="top" width="150" align="center">2.89 <sup>b</sup></td>

<td valign="top" width="150" align="center">2.89 <sup>b</sup></td>

</tr>

<tr>

<td valign="top" width="345">TV or radio documentary programmes</td>

<td valign="top" width="150" align="center">**2.17 <sup>a</sup>**</td>

<td valign="top" width="165" align="center">2.94 <sup>b</sup></td>

<td valign="top" width="150" align="center">**3.79 <sup>c</sup>**</td>

<td valign="top" width="150" align="center">**3.51 <sup>c</sup>**</td>

</tr>

<tr>

<td valign="top" width="345">TV or radio discussion programmes</td>

<td valign="top" width="150" align="center">**1.99 <sup>a</sup>**</td>

<td valign="top" width="165" align="center">2.77 <sup>b</sup></td>

<td valign="top" width="150" align="center">**3.57 <sup>c</sup>**</td>

<td valign="top" width="150" align="center">3.42 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">TV or radio advertisements</td>

<td valign="top" width="150" align="center">1.58 <sup>a</sup></td>

<td valign="top" width="165" align="center">1.68 <sup>a</sup></td>

<td valign="top" width="150" align="center">3.00 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.12 <sup>b</sup></td>

</tr>

<tr>

<td valign="top" width="345">Journals</td>

<td valign="top" width="150" align="center">1.59 <sup>a</sup></td>

<td valign="top" width="165" align="center">2.42 <sup>b</sup></td>

<td valign="top" width="150" align="center">2.70 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.31 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Novels</td>

<td valign="top" width="150" align="center">_1.11 <sup>a</sup>_</td>

<td valign="top" width="165" align="center">_1.13 <sup>a</sup>_</td>

<td valign="top" width="150" align="center">1.38 <sup>b</sup></td>

<td valign="top" width="150" align="center">_1.38 <sup>b</sup>_</td>

</tr>

<tr>

<th valign="top" width="345">_Health specialists_</th>

<td valign="top" width="150" align="center"><u>1.58</u><sup>a</sup></td>

<td valign="top" width="165" align="center"><u>2.78</u> <sup>b</sup></td>

<td valign="top" width="150" align="center"><u>2.63</u><sup>b</sup></td>

<td valign="top" width="150" align="center"><u>3.01</u><sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">12.6pt;text-indent:-0.1pt">Health journals</td>

<td valign="top" width="150" align="center">1.61 <sup>a</sup></td>

<td valign="top" width="165" align="center">2.74 <sup>b</sup></td>

<td valign="top" width="150" align="center">2.97 <sup>b c</sup></td>

<td valign="top" width="150" align="center">3.28 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Brochures from health authorities</td>

<td valign="top" width="150" align="center">1.89 <sup>a</sup></td>

<td valign="top" width="165" align="center">**2.99 <sup>b</sup>**</td>

<td valign="top" width="150" align="center">3.22 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.06 <sup>b</sup></td>

</tr>

<tr>

<td valign="top" width="345">Brochures from others than health authorities</td>

<td valign="top" width="150" align="center">1.47 <sup>a</sup></td>

<td valign="top" width="165" align="center">2.17 <sup>b</sup></td>

<td valign="top" width="150" align="center">2.44 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.01 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Encyclopaedias or Medical books</td>

<td valign="top" width="150" align="center">1.44 <sup>a</sup></td>

<td valign="top" width="165" align="center">2.79 <sup>c</sup></td>

<td valign="top" width="150" align="center">2.32 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.08 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Schools, through education</td>

<td valign="top" width="150" align="center">1.38 <sup>a</sup></td>

<td valign="top" width="165" align="center">**3.01 <sup>c</sup>**</td>

<td valign="top" width="150" align="center">2.20 <sup>b</sup></td>

<td valign="top" width="150" align="center">3.18 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Discussions with health professionals</td>

<td valign="top" width="150" align="center">1.69 <sup>a</sup></td>

<td valign="top" width="165" align="center">2.92 <sup>b</sup></td>

<td valign="top" width="150" align="center">2.57 <sup>b</sup></td>

<td valign="top" width="150" align="center">_2.84 <sup>b</sup>_</td>

</tr>

<tr>

<th valign="top" width="345">_Internet_</th>

<td valign="top" width="150" align="center"><u>1.19</u><sup>a</sup></td>

<td valign="top" width="165" align="center"><u>1.51</u><sup>b</sup></td>

<td valign="top" width="150" align="center"><u>1.20</u> <sup>a</sup></td>

<td valign="top" width="150" align="center"><u>2.92</u><sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Internet discussion- or newsgroups</td>

<td valign="top" width="150" align="center">_1.10 <sup>a</sup>_</td>

<td valign="top" width="165" align="center">_1.26 <sup>a</sup>_</td>

<td valign="top" width="150" align="center">_1.06 <sup>a</sup>_</td>

<td valign="top" width="150" align="center">_2.60 <sup>b</sup>_</td>

</tr>

<tr>

<td valign="top" width="345">Internet journals or newspapers</td>

<td valign="top" width="150" align="center">_1.21 <sup>a</sup>_</td>

<td valign="top" width="165" align="center">1.61 <sup>b</sup></td>

<td valign="top" width="150" align="center">_1.16 <sup>a</sup>_</td>

<td valign="top" width="150" align="center">3.23 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Websites by the health authorities</td>

<td valign="top" width="150" align="center">1.24 <sup>a</sup></td>

<td valign="top" width="165" align="center">1.93 <sup>b</sup></td>

<td valign="top" width="150" align="center">1.43 <sup>a</sup></td>

<td valign="top" width="150" align="center">3.06 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Websites by others than the health authorities</td>

<td valign="top" width="150" align="center">1.27 <sup>a b</sup></td>

<td valign="top" width="165" align="center">_1.51 <sup>b</sup>_</td>

<td valign="top" width="150" align="center">_1.23 <sup>a</sup>_</td>

<td valign="top" width="150" align="center">3.12 <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Internet advertisements</td>

<td valign="top" width="150" align="center">_1.10 <sup>a</sup>_</td>

<td valign="top" width="165" align="center">_1.24 <sup>a</sup>_</td>

<td valign="top" width="150" align="center">_1.09 <sup>a</sup>_</td>

<td valign="top" width="150" align="center">_2.60 <sup>b</sup>_</td>

</tr>

<tr>

<th valign="top" width="345">_Interpersonal sources_</th>

<td valign="top" width="150" align="center"><u>2.09</u><sup>a</sup></td>

<td valign="top" width="165" align="center"><u>3.37</u><sup>b</sup></td>

<td valign="top" width="150" align="center"><u>3.55</u><sup>b c</sup></td>

<td valign="top" width="150" align="center"><u>3.76</u> <sup>c</sup></td>

</tr>

<tr>

<td valign="top" width="345">Discussions with family, relatives or close friends</td>

<td valign="top" width="150" align="center">**2.22 <sup>a</sup>**</td>

<td valign="top" width="165" align="center">**3.50 <sup>b</sup>**</td>

<td valign="top" width="150" align="center">**3.68 <sup>b</sup>**</td>

<td valign="top" width="150" align="center">**3.78 <sup>b</sup>**</td>

</tr>

<tr>

<td valign="top" width="345">Discussions with others (e.g., work mates or sport trainers)</td>

<td valign="top" width="150" align="center">**1.95 <sup>a</sup>**</td>

<td valign="top" width="165" align="center">**3.22 <sup>b</sup>**</td>

<td valign="top" width="150" align="center">3.44 <sup>b c</sup></td>

<td valign="top" width="150" align="center">**3.75 <sup>c</sup>**</td>

</tr>

</tbody>

</table>

Total mean scores are presented for the information channels: Media, Health specialists, Internet and Interpersonal sources. Mean scores are presented for each information source. A cluster mean is significantly different from another mean (Tukey, p<0.05) if they have different superscripts. A cluster mean with two superscripts is not statistically different from the means of two of the other clusters. For example, for information seeking in Health journals the 'moderately active' cluster is marked as <sup>bc</sup> which means that the cluster does not differ significantly from the 'moderately passive' cluster and the 'active cluster', but a statistically significant difference exists across the 'moderately active' cluster and the 'passive' cluster.

The four highest mean scores of each cluster are marked in bold and the four lowest mean scores in italics. While the clusters were found to differ in their preferences for information channels, they all have in common that the total mean scores for Interpersonal sources ranked highest. In the following, however, the presentation will concentrate on differences regarding the information channels Media, Health specialists and Internet.

### Other factors of information behaviour

#### Information encountering

The analysis of information encountering in the Media (_p_<.001) and in Health specialists (_p_<.001) revealed significant differences across the clusters, but because the background variable age was entered as a covariance in the final analysis _post-hoc_ tests were not possible. However, results from the first step of the analysis, where the background variables are not controlled for, show a similar pattern as the results from purposive information seeking, information encountering in the Media (_F_(3.456)=50.8, _p_<.001) and Health specialists (_F_(3.458)=71.4, _p_<.001). The results are presented in mean figures in Table 6, where 1 is the lowest information encountering activity and 5 is highest.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 6: Information encountering in Media and Health specialists accross the information seeking clusters**</caption>

<tbody>

<tr>

<th>Information encountering</th>

<th>Passive cluster</th>

<th>Moderately passive cluster</th>

<th>Moderately active cluster</th>

<th>Active cluster</th>

</tr>

<tr>

<td>Health specialists</td>

<td align="center">1.99 <sup>a</sup></td>

<td align="center">2.99 <sup>a b</sup></td>

<td align="center">2.72 <sup>a b</sup></td>

<td align="center">3.34 <sup>b</sup></td>

</tr>

</tbody>

</table>

Binary logistic regression was used in the analysis of information encountering on the Internet. The Passive cluster was used as a comparison group which the other clusters were measured against (see Table 7).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 7: Information encountering on the Internet accross the information seeking clusters**</caption>

<tbody>

<tr>

<th>Information encountering</th>

<th>Passive cluster</th>

<th>Moderately passive cluster</th>

<th>Moderately active cluster</th>

<th>Active cluster</th>

</tr>

<tr>

<td>Internet</td>

<td>1.29 <sup>a</sup></td>

<td>1.67 <sup>b</sup></td>

<td>1.33 <sup>a</sup></td>

<td>2.84 <sup>b</sup></td>

</tr>

</tbody>

</table>

In the results a value of more than 1 on Exp (B) indicates that the odds of being an information encounterer get greater as the value of the independent variable gets higher. Values under 0 indicate that the odds of being an information encounterer get smaller as the value of the independent variable gets lower. A significant difference was found across the 'passive cluster' and the 'active' cluster, Exp (B) is 43.8 (_p_<.001), and across the 'passive' cluster and the 'moderately passive' cluster, Exp (B) is 4.26 (_p_<.001). The 'moderately active' cluster did not differ significantly from the 'passive' cluster (p=.691).

#### Relevance judgements

The respondents were asked how useful they considered information in the different information sources, and how reliable they considered the information. The scales: Media, Internet, and Health specialists, resulting from the factor analysis were used in the analysis. A significant difference was revealed across the clusters regarding judgements of the usefulness of information in the Media (F(3,300)=8.0, p<.001). The clusters did not differ significantly in their judgements of the usefulness of information from Health specialists (p=.172) nor on the information on the Internet (p=.077).

Significant difference was found across the clusters in judgements of the reliability of information in the Media (_F_(3.239)=10.14, _p_<.001), Health specialists (_F_(3.338)=3.31, _p_<.05) and on the Internet (_F_(3.131)=5.03, _p_<.01).

The results are presented in Table 8 in mean figures, where 1 indicates that the information is considered to have the lowest usability/ reliability and 5 the highest.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 8: Relevance judgements accross the information seeking clusters**</caption>

<tbody>

<tr>

<th>Relevance judgements</th>

<th>Passive cluster</th>

<th>Moderately passive cluster</th>

<th>Moderately active cluster</th>

<th>Active cluster</th>

</tr>

<tr>

<td colspan="5">**Usefulness of information**</td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Media</td>

<td style="PADDING-LEFT: 5.4pt;" width="16%">2.54 <sup>b</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="20%">2.30<sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="18%">2.78 <sup>b</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="13%">2.61 <sup>b</sup></td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Health specialists</td>

<td style="PADDING-LEFT: 5.4pt;" width="16%">2.88 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="20%">3.14 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="18%">3.10 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="13%">3.08 <sup>a</sup></td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Internet</td>

<td style="PADDING-LEFT: 5.4pt;" width="16%">2.01 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="20%">2.13 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="18%">2.25 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="13%">2.52 <sup>a</sup></td>

</tr>

<tr>

<td colspan="5" style="PADDING-LEFT: 5.4pt;" width="30%">**Reliability of information**</td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Media</td>

<td style="PADDING-LEFT: 5.4pt;" width="16%">2.36 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="20%">2.19 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="18%">2.66 <sup>b</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="13%">2.60 <sup>b</sup></td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Health specialists</td>

<td style="PADDING-LEFT: 5.4pt;" width="16%">3.19 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="20%">3.48 <sup>b</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="18%">3.36 <sup>a b</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="13%">3.35 <sup>a b</sup></td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Internet</td>

<td style="PADDING-LEFT: 5.4pt;" width="16%">1.97 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="20%">1.98 <sup>a</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="18%">2.20 <sup>a b</sup></td>

<td style="PADDING-LEFT: 5.4pt;" width="13%">2.41 <sup>b</sup></td>

</tr>

</tbody>

</table>

#### Barriers to information behaviour

Information behaviour barriers were examined by using the scales: Cognitive barriers and Physical barriers, constructed by factor analysis. Significant difference was found across the clusters regarding Cognitive barriers (F(3,464)=5.4, p<.001) and Physical barriers (F(3,458)=3.3, p<.05). The results are presented in mean figures in Table 9, where 1 stands for the lowest barriers and 5 the greatest barriers.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 9: Barriers to information behaviour**</caption>

<tbody>

<tr>

<th width="30%">Barriers</th>

<th width="16%">Passive cluster</th>

<th width="20%">Moderately passive cluster</th>

<th width="18%">Moderately active cluster</th>

<th width="13%">Active cluster</th>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Physical barriers</td>

<td style="PADDING-LEFT: 15pt;" width="16%">2.12 <sup>b</sup></td>

<td style="PADDING-LEFT: 15pt;" width="20%">1.82 <sup>a</sup></td>

<td style="PADDING-LEFT: 15pt;" width="18%">1.85 <sup>a b</sup></td>

<td style="PADDING-LEFT: 15pt;" width="13%">1.91 <sup>a b</sup></td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Cognitive barriers</td>

<td style="PADDING-LEFT: 15pt;" width="16%">2.50 <sup>b</sup></td>

<td style="PADDING-LEFT: 15pt;" width="20%">2.16 <sup>a</sup></td>

<td style="PADDING-LEFT: 15pt;" width="18%">2.26 <sup>a b</sup></td>

<td style="PADDING-LEFT: 15pt;" width="13%">2.27 <sup>a b</sup></td>

</tr>

</tbody>

</table>

#### Motivation

The clusters differ significantly regarding interest in information about health and lifestyle (_F_(3.456)=38.7, _p_<.001) and how often they discuss the topic health and lifestyle with others (_F_(3.454)=54.3, _p_<.001). The results are presented in mean figures in Table 10 where 1 stands for the lowest motivation and 5 the highest.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 10: Motivation for information behaviour**</caption>

<tbody>

<tr>

<th width="30%">Motivation</th>

<th width="16%">Passive cluster</th>

<th width="20%">Moderately passive cluster</th>

<th width="18%">Moderately active cluster</th>

<th width="13%">Active cluster</th>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Interest</td>

<td style="PADDING-LEFT: 15pt;" width="16%">2.92<sup>a</sup></td>

<td style="PADDING-LEFT: 15pt;" width="20%">3.85 <sup>b</sup></td>

<td style="PADDING-LEFT: 15pt;" width="18%">3.75 <sup>b</sup></td>

<td style="PADDING-LEFT: 15pt;" width="13%">4.04 <sup>b</sup></td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;" width="30%">Discussions</td>

<td style="PADDING-LEFT: 15pt;" width="16%">2.71 <sup>a</sup></td>

<td style="PADDING-LEFT: 15pt;" width="20%">3.73 <sup>b</sup></td>

<td style="PADDING-LEFT: 15pt;" width="18%">3.79 <sup>b</sup></td>

<td style="PADDING-LEFT: 15pt;" width="13%">3.92 <sup>b</sup></td>

</tr>

</tbody>

</table>

### Description of the information behaviour of the information seeking clusters

#### The 'passive' cluster

The Of the four clusters, the 'passive' cluster is the least motivated towards information behaviour. The 'passive' cluster is both significantly less interested in health and lifestyle and discusses this issue significantly less often than the other three clusters. The 'passive' cluster seeks information the least often and only to a very limited extent. It is conservative in choice of information sources and prefers traditional everyday sources, such as the Media (purposive seeking 1.73, encountering 2.56). The mass media generally belongs to people's everyday environment and it forms a part of an information environment that people have been familiar with for a long time and are accustomed to use. It may therefore not demand a special effort to gather information from the Media. More unconventional sources, such as those on the Internet (purposive seeking 1.19 1.29) are rarely sought for information. For purposive seeking in Health specialists the total mean score is 1.58, mean score for encountering information is 1.99\. Although the 'passive' cluster preferred to seek information in the Media rather than by Health specialists, it considered sources by Health specialists to be both the most useful (2.88) and most reliable (3.19), while Internet sources were considered least useful (2.01) and least reliable (1.97). For Media, mean figures for usefulness were 2.54 and for reliability 2.36.

#### The 'moderately passive' cluster

The 'moderately passive' cluster, which seeks information less often than both the 'moderately active' and the 'active' clusters, appreciates sources by Health specialists (purposive seeking 2.78, encountering 2.99). When information are sought in the Media (purposive seeking 2.13, encountering 2.99), documentary (2.94) or discussion programmes (2.76), where information with the professional opinions are likely to be found, are most highly preferred. Furthermore, the consistency in choice of information sources is also evident for information seeking on the Internet (purposive seeking 1.51, encountering 1.67), where Websites by health authorities (1.93) are most highly preferred.

Additionally, the relevance judgments of the 'moderately passive' cluster are in harmony with its information source preferences. Information by Health specialists are considered as both most useful (3.14) and reliable (3.48) and Internet sources as least useful (2.13) and least reliable (1.98). For Media, mean figures for usefulness were 2.30 and for reliability 2.19\.

Hence, the 'moderately passive' cluster is consistent in its selection of information sources, it aims for the experts' opinions, and it seems to know where to seek the kind of information that is preferred. This cluster also seems to be critical in the selection of information, since information in the sources that it considers the most relevant are also those that are mainly being sought.

#### The 'moderately active' cluster

The 'moderately active' cluster seeks information less often than the 'active' cluster but more often than both the 'moderately passive' and the 'passive' clusters, with the exception of seeking information on the Internet where it does not differ significantly from the 'passive' cluster.

This cluster preferred information in the Media the most (purposive seeking 3.11, encountering 3.46). Thus, the 'moderately active' cluster is conservative in choice of information sources and appreciates sources that traditionally belong to peoples nearest surroundings. Health specialist sources (purposive seeking 2.63, encountering 2.72) are used less often and sources on the Internet are rarely sought (purposive seeking 1.20, encountering 1.33).

Furthermore, although the 'moderately active' cluster prefers to seek information in the Media it considers information by Health specialists to be the most useful (3.10) and reliable (3.36), while Internet sources (2.25) are judged as the least useful and the least reliable (2.20). For Media, the mean figures were 2.78 for usefulness and 2.66 for reliability.

#### The 'active' cluster

Of all the clusters, the 'active' cluster seeks information most frequently and it has a preference for the broadest selection of information sources. The 'active' cluster is, furthermore, characterised by a much higher preference for Internet sources than the other clusters (purposive seeking 2.92, encountering 2.84). For purposive seeking in the Media the total mean score is 3.07 and for information encountering the mean score is 3.55, for Health specialists the scores for purposive seeking is 3.01 and encountering 3.34, and for the Internet the scores for purposive seeking is 2.92 and encountering 2.84\. Being flexible in choice of information sources, however, also means that the 'active' cluster is not very selective.

As with the other clusters, sources by Health specialists were considered to be the most useful (3.08) and most reliable (3.35) and the Internet sources the least useful (2.52) and least reliable (2.41). For the Media the mean scores for usefulness were2.61 and for reliability 2.60.

### Self-efficacy beliefs of the information seeking clusters

The relationship between the self-efficacy clusters and the information behaviour clusters was examined and a statistically significant relationship was revealed. The results are presented in a cross tabulation (Table 11).

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 11: Rate of members of the self-efficacy clusters in the information seeking clusters (%)**  
(_Chi squared_ (9) = 17.70, _p_<.05)</caption>

<tbody>

<tr>

<th>Health self-efficacy</th>

<th>Passive % (raw number)</th>

<th>Moderately passive % (raw number)</th>

<th>Moderately active % (raw number)</th>

<th>Active % (raw number)</th>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;">Low self-efficacy cluster</td>

<td style="PADDING-LEFT: 15.4pt;">14.2 (27)</td>

<td style="PADDING-LEFT: 15.4pt;">12.3 (11)</td>

<td style="PADDING-LEFT: 15.4pt;">18.9 (21)</td>

<td style="PADDING-LEFT: 15.4pt;">15.6 (12)</td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;">Moderate to low self-efficacy cluster</td>

<td style="PADDING-LEFT: 15.4pt;">25.8 (50)</td>

<td style="PADDING-LEFT: 15.4pt;">20.0 (18)</td>

<td style="PADDING-LEFT: 15.4pt;">19.8 (22)</td>

<td style="PADDING-LEFT: 15.4pt;">9.1 (7)</td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;">Moderate to high self-efficacy cluster</td>

<td style="PADDING-LEFT: 15.4pt;">21.6 (41)</td>

<td style="PADDING-LEFT: 15.4pt;">14.4 (13)</td>

<td style="PADDING-LEFT: 15.4pt;">24.3 (27)</td>

<td style="PADDING-LEFT: 15.4pt;">28.6 (22)</td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;">High self-efficacy cluster</td>

<td style="PADDING-LEFT: 15.4pt;">38.4 (74)</td>

<td style="PADDING-LEFT: 15.4pt;">53.3 (48)</td>

<td style="PADDING-LEFT: 15.4pt;">36.9 (42)</td>

<td style="PADDING-LEFT: 15.4pt;">46.8 (36)</td>

</tr>

<tr>

<td style="PADDING-LEFT: 5.4pt;">Total</td>

<td style="PADDING-LEFT: 15.4pt;">100 (192)</td>

<td style="PADDING-LEFT: 15.4pt;">100 (90)</td>

<td style="PADDING-LEFT: 15.4pt;">100 (112)</td>

<td style="PADDING-LEFT: 15.4pt;">100 (77)</td>

</tr>

</tbody>

</table>

The highest rate of members in all the information behaviour clusters is in the High self-efficacy cluster, although the highest rate comes from the 'moderately passive' cluster, followed by the 'active' cluster. This is consistent with results from previous studies, indicating that people are generally confident about their ability to control their health in a successful way (Smith _et al._ 1995). However, by highlighting the difference between high and low self-efficacy beliefs a clearer picture can be presented. This can be done by combining the rate of members from the High self-efficacy and the Moderate to high self-efficacy clusters and compare it with the combined rate of the Low self-efficacy and Moderate to low self-efficacy clusters.

If this method is used, the 'active' cluster has the highest self-efficacy beliefs, with 75.4% of its members belonging to the High self-efficacy and Moderate to high self-efficacy clusters, against 67.7% in the 'moderately passive' cluster, 61.2% in the 'moderately active' cluster, and 60% in the 'passive' cluster. The 'passive' cluster has the lowest self-efficacy beliefs, with 40% of its members belonging to the Low self-efficacy and Moderate to low self-efficacy clusters, against 38.7% in the 'moderately active' cluster, 32.3% in the 'moderately passive' cluster, and 24.7% in the 'active' cluster. The 'passive' cluster and the 'moderately active' cluster have almost identical self-efficacy beliefs, although the 'passive' cluster has slightly lower self-efficacy beliefs the difference across these clusters is very small.

The results indicate that the 'active' and the 'moderately passive' clusters believe more strongly that controlling their health is likely to lead to a favourable outcome and that they are able to do so successfully, than the 'passive' and the 'moderately active' clusters.

The clusters differ significantly regarding interest in information about health and lifestyle (_F_(3,456)=38.7, _p_<.001) and how often they discuss the topic health and lifestyle with others (_F_(3,454)=54.3, _p_<.001). The results are presented in mean figures in Table 10 where 1 stands for the lowest motivation and 5 the highest.

### Relations to health behaviour

Binary logistic regression was used in the analysis of the health behaviour. The 'passive' cluster was used as a comparison group for the information seeking clusters and the High self-efficacy cluster for the self-efficacy clusters.

In the results, a value greater than 1 on Exp (B) indicates that the odds of being a non-exerciser or non-consumer of light food products gets greater as the value of the independent variable gets higher. A value of less than 1 indicates that the odds of being a non-exerciser or non-consumer get smaller as the value of the independent variable gets lower.

The odds of being a non-exerciser were found to be greater for the 'passive' cluster than the 'moderately passive' cluster, Exp (B) is 0.17 (_p_<.01). The 'active' and Moderately clusters did not differ significantly from the 'passive' cluster. Also, the odds of being a non-consumer of light food products are greater for the 'passive' cluster, than the 'active' cluster, Exp (B) is .275 (_p_<.001), the 'moderately active' cluster, Exp (B) is .404 (_p_<.001), and the 'moderately passive' cluster, Exp (B) is .424 (_p_<.01).

The 'moderately passive' cluster was therefore found to lead a healthier life than the other information behaviour clusters, and the behaviour of the 'passive' cluster is the least healthy.

Furthermore, the odds of being a non-exerciser are greater for the Low self-efficacy cluster than the High self-efficacy cluster, Exp (B) is 5.62 (_p_<.001), and the odds of being a non-exerciser are greater for the Moderate to low self-efficacy cluster than the High self-efficacy cluster, Exp (B) is 4.19 (_p_<.001). The High self-efficacy and the Moderate to high self-efficacy did not differ significantly. The results also show that the odds of a being non-consumer of light food products are greater for the Moderate to low self-efficacy cluster than the High self-efficacy cluster, Exp (B) is 1.76 (_p_<.05). The Low self-efficacy and the Moderate to high self-efficacy clusters did not differ significantly from the High self-efficacy cluster.

The results show, therefore, that the behaviour of the High self-efficacy cluster is healthier than both the Moderate to low self-efficacy cluster and the Low self-efficacy cluster.

## Discussion and conclusion

Information behaviour barriers were examined by using the scales: Cognitive barriers and Physical barriers, constructed by factor analysis. Significant differences were found across the clusters regarding Cognitive barriers (_F_(3,464)=5.4, _p_<.001) and Physical barriers (_F_(3,458)=3.3, p<.05). The results are presented in mean figures in Table 9 (above, where 1 stands for the lowest barriers and 5 the greatest barriers.

The finding that the 'passive' cluster, which is least often engaged in seeking information, consists of more men than women and has the lowest level of education, whereas the 'active' cluster, which seeks information most often, consists mainly of women and is the second best educated cluster, is in line with previous findings that higher educated people are more likely to seek health information than those with lower education ([Beier and Ackermans 2003](#bei03); [Kenkel 1990](#ken90)), and that women generally seek more health information than men ([Connell and Crawford 1988](#con88); [Kassulke _et al._ 1993](#kas93); [Rakowski _et al._ 1990](#rak90)).

The 'moderately passive' cluster seeks information less often than the 'moderately active' and the 'active' clusters. When this is combined with the fact that the 'moderately passive' cluster has the highest educational level, it does not agree with previous findings, that more educated people seek more health information, and from a wider collection of sources, than less educated people ([Bishop _et al._ 1999](#bis99); [Muha and Smith 1989](#muh89); [Kenkel 1990](#ken90)). The 'moderately passive' and the 'active' clusters can be compared to Heinström's ([2002](#hei02)) study of university students which describes the categories of Deep divers and Broad scanners. Deep divers put an effort into information seeking and aim more at finding information of high quality than to seek a large quantity of information, while Broad scanners seek information from a broad selection of sources. While the 'active' cluster collects a large amount of information from a wide range of sources, the 'moderately passive' cluster seeks information less often, but it is consistent in selection of information sources and aims at information of high quality. Furthermore, when the relevance judgements of the clusters were compared with their information source preference, it was found that all the clusters considered information by Health specialists to be the most useful and most reliable. Yet, the 'passive' cluster and the 'moderately active' cluster preferred to seek information in the Media rather than by Health specialists and the 'active' cluster preferred information in the Media, by Health specialists and on the Internet to a similar extent. Thus, there was an inconsistency between the relevance judgements of these clusters and their preference for information sources. Only the relevance judgements of the 'moderately passive' cluster were found to be in harmony with its information source preference. The 'moderately passive' cluster seems to have a more critical approach in the selection of information sources than the other clusters, in the sense that when its members seek information about health and lifestyle, they prefer to seek in the sources and channels that they consider to be the most reliable and most useful.

The 'active' cluster was characterised by seeking information on the Internet considerably more often than the other clusters. The 'moderately passive' cluster sought information on the Internet more often than the 'passive' and the 'moderately active' clusters, which did so very seldom. This confirms previous findings that lower educated people seek health information on the Internet less often than those who are better educated ([Carlson 2000](#car00); [Cotton and Gupta 2004](#cot04); [Fox and Fallows 2003](#fox03); [Fox and Raine 2002](#fox02); [Fox _et al._ 2000](#fox00)) and that the oldest age groups seek health information on the Internet less often than the youngest age groups ([Carlson 2000](#car00); [Cotton and Gupta 2004](#cot04); [Fox 2003](#fox03)). The 'passive' and the 'moderately active' clusters have a higher rate of members from the oldest age group and a lower rate from the youngest group, their members have less education and their behaviour is less healthy, than the 'active' and the 'moderately passive' clusters. Studies have also found lower educated people ([O'Keefe _et al._ 1998](#oke98); [Wade and Schramm 1969](#wad69)), or those who have less health orientation ([Dutta-Bergman 2004](#dut04)), to prefer the mass media as a source of health information, which was the case with the 'passive' and the 'moderately active' clusters.

The relatively low preference for Internet sources by other than the 'active' cluster is noteworthy, especially since the ratio of citizens with Internet access in Iceland is among the highest in the world. In 2000, a total of 77.8% of Icelanders had access to the Internet, 77.9% women and 77.7% men ([Forsœtisráðuneytið 2000](#for00)). Other studies have shown a steady rise in health information seeking on the Internet, although most people do so infrequently ([Cotton and Gupta 2004](#cot04); [Fox _et al._ 2000](#fox00); [Fox and Rainie 2002](#fox02); [Fox and Fallows 2003](#fox03)). Since the data for this study were collected in autumn 2002 it would be interesting to examine whether or not there has been a change over time among Icelanders in the use of the Internet, and other information channels, as well as in the reliability judgements of the information channels.

Enhancements of people's belief that they have the ability to act on the information provided and perform the necessary changes in their behaviour has been pointed out as an important factor in order for health education to be effective. Health information needs to contain more than advice about behaviour, as people must also be persuaded that they have the means to act upon it ([Bandura 1997](#ban97); [Rimal 2001](#rim01); [Rimal, Flora and Schooler 1999](#rim99)). Although self-efficacy has been extensively examined in relation to health behaviour, the connection between self-efficacy and health information seeking has not gained much interest. In this study a relationship was found between health and lifestyle information behaviour, health behaviour and health self-efficacy. The self-efficacy beliefs of the 'active' cluster and the 'moderately passive' cluster were found to be higher than the 'passive' and the 'moderately active' clusters.

The information behaviour of the 'active' and 'moderately passive' clusters is more challenging than the other two clusters, indicating a relation between a strong information behaviour profile and a belief in controlling one's health. The behaviour of the 'passive' cluster, which was the least healthy, was related to an information behaviour that combined the lowest frequency of information seeking with the lowest motivation and higher information behaviour barriers, and a preference for sources that in general belong to people's traditional information environment and to which they are accustomed to use. More frequent information seeking was related to more healthy behaviour, which is the case for the 'moderately active' and the 'active' clusters. However, the best health behaviour was not related to the most frequent information seeking, but to information seeking that happens less often, accompanied by a consistent and a critical approach in the selection of information sources, and the lowest information behaviour barriers, as with the 'moderately passive' cluster.

Although the study did not explore this, it is possible that members of the clusters did not only seek information for themselves and that they also sought information for others, e.g. family members ([Fox _et al._ 2000](#fox00); [Fox and Fallows 2003](#fox03); [Pennbridge _et al._ 1999](#pen99)). This may be worth pursuing in future studies. Another interesting aspect is the _worried well_. There are some indications that increased access to information may have caused some people to become unnecessarily worried about the state of their health ([Smith _et al._ 2002](#smi02); [Weatherhead and Lawrence 2006](#wea06)). How this connects with health information seeking is not clear, but it is possible that, for example, the 'moderately active' cluster, who seek information frequently, although it behaved in a less healthy way than the 'moderately passive' and the 'active' clusters, can be _worried well_. This may be an intereting speculation for future studies.

The findings of the study may further our knowledge of how different groups in society take advantage of the information that are available about health and lifestyle. A better understanding of the factors that relate to people's information behaviour has implications for health promotional activities and may, hopefully, be used to improve the outcome of health education.

## Acknowledgements

The study reported here was conducted as part of a PhD programme, in the Department of Social and Political Sciences, Information Studies, at Åbo Akademi University, Å (Turku), Finland, and funded by the University of Iceland Research Fund. The author wishes to acknowledge the contribution of The Social Science Research Institute, University of Iceland, for assistance and statistical advice at the data collection and data analysis stages, as well as in preparing this paper for publication. She also thanks anonymous referees for their constructive comments and suggestions to an earlier draft of the paper.

## References

*   <a id="aga99" name="aga99"></a>Agada, J. (1999). Inner-city gatekeepers: an exploratory survey of their information use environment. _Journal of the American Society for Information Science_, **50**(1), 74-78.
*   <a id="ajz85" name="ajz85"></a>Ajzen, I. (1985). From intentions to action: a theory of planned behaviour. In J. Kuhl & J. Beckman, (Eds.). _Action control: from cognitions to behaviors_ (pp. 11-39). New York, NY: Springer.
*   <a id="ald84" name="ald84"></a>Aldenderfer, M.S. & Blashfield, R.K. (1984). _Cluster analysis_. Sage University Papers: Iowa. (Quantitative Applications in the Social Sciences, 44).
*   <a id="bab79" name="bab79"></a>Babbie, E.R. (1979). _The practice of social research._ Belmont, CA: Wadsworth.
*   <a id="ban97" name="ban97"></a>Bandura, A. (1997). _Self-efficacy: the exercise of control._ New York, NY: W.H. Freeman.
*   <a id="bec87" name="bec87"></a>Becker, M.H. & Rosenstock, I.M. (1987). Comparing social learning theory and the health belief model. In W.B. Ward (Ed.). _Advances in health education and promotion_, Vol 2\. (pp. 245-249). Greenwich, CT.: JAI Press.
*   <a id="bei03" name="bei03"></a>Beier, M.E. & Ackerman, P. L. (2003). Determinants of health knowledge: an investigation of age, gender, abilities, personality, and interests. _Journal of Personality and Social Psychology_, **84**(2), 439-447.
*   <a id="bis99" name="bis99"></a>Bishop, A.P., Tidline, T.J., Shoemaker, S. & Salela, P. (1999). Public libraries and networked infomation services in low-income communities. _Library & Information Science Research_ **21**(3), 361-390\.
*   <a id="car00" name="car00"></a>Carlson, M. (2000). Cancer patients seeking information from sources outside the health care system. _Support Care Cancer_, **8**(6), 453-457.
*   <a id="cha85" name="cha85"></a>Chatman, E.A. (1985). Information, mass media use and the working poor. _Library & Information Science Research_, **7**(2), 97-113\.
*   <a id="con88" name="con88"></a>Connell, C.M. & Crawford, C.O. (1988). How people obtain their health information: a survey in two Pennsylvania counties. _Public Health Reports_ **103**(2), 189-195\.
*   <a id="cot04" name="cot04"></a>Cotton, S.R. & Gupta, S.S. (2004). Characteristics of online and offline health information seekers and factors that discriminate between them. _Social Science & Medicine_, **59**(9), 1795-1806\.
*   <a id="cro51" name="cro51"></a>Cronbach, L.J. (1951). Coefficient alpha and the internal structure of tests. _Psychometrika_ **16**(3) 297-334\.
*   <a id="dis04" name="dis04"></a>Dishman, R.K., Motl, R.W., Saunders, R., Felton, G., Ward, D.S., Dowda, M. & Pate, R.R. (2004). Self-efficacy partially mediates the effect of a school-based physical-activity intervention among adolescent girls. _Preventive Medicine_, **38**(5), 628-636.
*   <a id="dut04" name="dut04"></a>Dutta-Bergman, M.J. (2004). Primary sources of health information: comparisons in the domain o fhealth attitudes, health cognitions, and health behaviours. _Health Communication_ **16**(3) 273-288.
*   <a id="erd97" name="erd97"></a>Erdelez, S. (1997). Information encountering: a conceptual framework for accidental information discovery. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts 14-16 August 1996, Tampere, Finland_ (pp. 412-421). London: Taylor Graham.
*   <a id="eve00" name="eve00"></a>Everitt, B.S., Landau, S. & Leese M. (2001). _Cluster analysis_ 4th ed. London: Arnold.
*   <a id="fab99" name="fab99"></a>Fabrigar, L.R., Wegener, D.T., MacCullum, R.C. & Stahan, E.J. (1999). Evaluating the use of exploratory factor analysis in psychological research. _Psychological Methods_, **4**(3), 272-299.
*   <a id="fos03" name="fos03"></a>Foster, A. & Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation_, 59(3), 321-340\.
*   <a id="fox02" name="fox02"></a>Fox, S. & Rainie, L. (2002). _[Vital decisions: how Internet users decide what information to trust when they or their loved ones are sick.](http://www.webcitation.org/5TVxXOrrY)_ Washington, DC: Pew internet & American life project. Retrieved 21 November, 2007 from http://www.pewinternet.org/pdfs/PIP_Vital_Decisions_May2002.pdf
*   <a id="fox03" name="fox03"></a>Fox, S. & Fallows, D. (2003). _[Internet health resources.](http://www.webcitation.org/5TVxMDgGo)_ Washington, DC: Pew internet & American life project. Retrieved 21 November, 2007 from http://www.pewinternet.org/pdfs/PIP_Health_Report_July_2003.pdf
*   <a id="fox00" name="foxr00"></a>Fox, S., Rainie, L., Horrigan, J., Lenhart, A., Spooner, T., Burke, M., Lewis, O. & Carter, C. (2000). _[The online health care revolution: how the Web helps Americans take better care of themselves.](http://www.webcitation.org/5TVxjjBmV)_ Washington, DC: Pew internet & American life Project. Retrieved 21 November, 2007 from http://www.pewinternet.org/pdfs/PIP_Health_Report.pdf
*   <a id="gin00" name="gin00"></a>Ginman, M. (2000). Health information and quality of life. _Health Informatics_, **6**(4), 181-188\.
*   <a id="hav97" name="haf97"></a>Hafstad, A. & Aaro, L.E. (1997). Activating interpersonal influence through provocative appeals: evaluation of a mass media-based antismoking campaign targeting adolescents. _Health Communication_, **9**(3) 253-272.
*   <a id="hei02" name="hei02"></a>Heinström, J. (2002). _Fast surfers, broad scanners and deep divers: personality and information-seeking behaviour._ Åbo, Finland: Åbo Akademis Förlag - Åbo Akademi University Press. (Doctoral thesis).
*   <a id="for00" name="for00"></a>Iceland. _Prime Minister's Office._ (2000). _Aðgengi að Internetinu haustið 2000: niðurstöður rannsóknar um aðgang að Interneti í september árið 2000 sem PricewaterhouseCoopers vann fyrir Verkefnisstjórn um upplýsingasamféagið_. (Access to the Internet in autumn 2000: results from a study on access to the Internet in September 2000, by PricewaterhouseCoopers for the Information Society Taskforce.) Reykjavík: Forsœtisráðuneytið. Retrieved 24 July 2005 from http://www.forsaetisraduneyti.is/utgefid-efni/nr/78
*   <a id="hag03" name="hag03"></a>Iceland. _Statistics Iceland_. (2003). _[Statistics](http://www.webcitation.org/5TVygpKd5)_. Retrieved 31 July 2005 from http://www.hagstofa.is/?PageID=622
*   <a id="jul99" name="jul99"></a>Julien, H.E. (1999). Barriers to adolescents' information seeking for career decision making. Journal of the American Society for Information Science, 50(1), 48-48\.
*   <a id="kas93" name="kas93"></a>Kassulke, D., Stenner-Day, K., Coory, M. & Ring, I. (1993). Information-seeking behaviour and sources of health information: associations with risk factor status in an analysis of three Queensland electorates. _Australian Journal of Public Health_, **17**(1), 51-57\.
*   <a id="ken90" name="ken90"></a>Kenkel, D. (1990). Consumer health information and the demands for medical care. _The Review of Economics and Statistics_, **LXXII**(4), 587-595\.
*   <a id="kur03" name="kur03"></a>Kurtz, M. E., Kurtz, J. C., Contreras D. & Booth, C. (2003). Knowledge and attitudes of economically disadvantaged women regarding exposure to environmental tobacco smoke. European Journal of Public. _Health_, **13**(2) 171-176\.
*   <a id="mad83" name="mad83"></a>Maddux, J.E. & Rogers, R.W. (1983). Protection motivation and self-efficacy: a revised theory of fear appeals and attitude change. _Journal of Experimental Social Psychology,_ **19**(5), 469-479.
*   <a id="mck02" name="mck02"></a>McKenzie, P. J. (2002). Communcation barriers and information-seeking counterstrategies in accounts of practitioner-patient encounters. _Library & Information Science Research_, **24**(1), 31-47\.
*   <a id="man91" name="man91"></a>Måns, R. (1991). Levnadsvanor (Life habits).. In F. Diderichsen, P. Östlin, G. Dahlgren & C. Hogstedt (Eds.), Klass och ohälsa: en antologi om orsaker till den ojämlika ohälsan (Class and illness: an anthology of reasons for inequalities in health.) (pp. 144-163). Stockholm: Tiden/Folksam.
*   <a id="met82" name="met82"></a>Mettlin, C. & Cummings, M. (1982). Communication and behavior change for cancer control. _Progress in Clinical & Biological Research_, **83** 135-148\.
*   <a id="muh89" name="muh89"></a>Muha, C. & Smith, K.S. (1989). The use and selection of sources in information seeking: the cancer information service experience. Part 8\. _Journal of Health Communication_, **3**(3) 109-120.
*   <a id="nun78" name="nun78"></a>Nunnally, J.C. (1978). _Psychometric theory._ (2nd. ed.). New York, NY: McGraw-Hill.
*   <a id="nut00" name="nut00"></a>Nutbeam, D. (2000). Health literacy as a public health goal: a challenge for contemporary health education and communication strategies into the 21st century. _Health Promotion International_ **15**(3), 259-267\.
*   <a id="oke98" name="oke98"></a>O'Keefe, G.J., Boyd, H.H. & Brown, M.R. (1998). Who learns preventive health care information from where: cross-channel and repertoire comparisons. _Health Communication_ **10**(1) 25-36\.
*   <a id="osl95" name="osl95"></a>Osler, M. & Schroll, M. (1995). Lifestyle and prevention of ischaemic heart disease in Denmark: changes in knowledge and behaviour 1982-1992\. _European Journal of Public Health_, **5**(2), 109-112\.
*   <a id="agu05" name="agu05"></a>Pálsdóttir, A. (2005). Health and lifestyle: Icelanders' everyday life information behaviour. Åbo, Finland: Åbo Akademis Förlag - Åbo Akademi University Press. (Doctoral thesis).
*   <a id="pen99" name="pen99"></a>Pennbridge, J., Moya, R. & Rodrigues, L. (1999). Questionnaire survey of California consumers' use and rating of sources of health care information including the Internet. _Western Journal of Medicine_ **171**(5/6), 302\.
*   <a id="por03" name="por03"></a>Pors, N.O. (2003). Loyalty and scepticism among members of a professional union: the case of the Union of Librarians in Denmark. _New Library World_, **104**(1184/1185) 20-28\.
*   <a id="pra92" name="pra92"></a>Prättälä, R., Berg, M.A. & Puska, P. (1992). Diminishing or increasing contrasts? Social class variation in Finnish food consumption patterns 1979-1990\. _European Journal of Clinical Nutrition,_ **46**(4), 279-287\.
*   <a id="pro97" name="pro97"></a>Prochaska, J.O. & Velicer, W.F. (1997). The transtheoretical model of health behavior. _American Journal of Health Promotion_ **12**(1), 38-48\.
*   <a id="rak90" name="rak90"></a>Rakowsky, W., Assaf, A.R., Lefebvre, R.C., Lasater, T.M., Niknian, M. & Caraleton, R.A. (1990). Information seeking about health in a community sample of adults: correlates and assoications with other health-related practices. _Health Behaviour Quarterly_, **17**(4), 379-393\.
*   <a id="rim01" name="rim01"></a>Rimal, R.N. (2001). Perceived risk and self-efficacy as motivators: understanding individuals' long-term use of health information. _Journal of Communication_, **51**(4), 633-654\.
*   <a id="rim99" name="rim99"></a>Rimal, R.N., Flora, J.A. & Schooler, C. (1999). Achieving improvements in overall health orientation: effects of campaign exposure, information seeking, and health media use. _Communication Research_, **26**(3), 322-348.
*   <a id="sar96" name="sar96"></a>Saracevic, T. (1996). Relevance reconsidered '96\. In P. Ingwersen & N.O. Pors (Eds.), _CoLIS2: second international conference on conceptions of library and information science: integration in perspective: October 13-16, 1996._ (pp. 201-218). Copenhagen: The Royal School of Librarianship.
*   <a id="sch96" name="sch96"></a>Schwarzer, R. & Fuchs, R. (1996). Self-efficacy and health behaviours. In M. Conner & P. Norman (Eds.). _Predicting health behaviour: research and practice with social cognition models_. (pp. 163-195). Buckingham: Open University Press.
*   <a id="smi95" name="smi95"></a>Smith M.S., Wallston K.A. & Smith C.A (1995). The development and validation of the perceived health competence scale. _Health Education Research_, **10**(1), 51-64\.
*   <a id="smi02" name="smi02"></a>Smith, RC., Gardiner, J.C., Lyles, J.S., Johnson, M., Rost, K.M., Luo, Z. _et al._ (2002). Minor acute illness: a preliminary research report on the 'worried well'. _Journal of Family Practices_, **51**(1) 24-29\.
*   <a id="ste03" name="ste03"></a><a id="ste04" name="ste04"></a>Stewart, D.E., Abbey, S.E., Shnek, Z.M., Irvine, J. & Grace, S.L. (2004). Gender differences in health information needs and decisional preferences in patients recovering from an acute ischemic coronary event. _Psychosomatic Medicine_, **66**(1), 42-48\.
*   <a id="str86" name="str86"></a>Strecher, V.J., DeVellis, B.M., Becker, M.H. & Rosenstock, I.M. (1986). The role of self-efficacy in achieving health behaviour change. _Health Education Quarterly_, **13**(1), 73-91\.
*   <a id="tay91" name="tay91"></a>Taylor, R.S. (1991). Information use environments. _Progress in Communication Sciences_, **10**, 217-255.
*   <a id="tom00" name="tom00"></a>Toms, E.G. (2000). [Serendipitous information retrieval](http://www.webcitation.org/5TW6JdUsa). In _Proceedings of the First DELOS Network of Excellence Workshop on 'Information Seeking, Searching and Querying in Digital Libraries', Zurich, Switzerland, December 11-12, 2000._. Sophia Antipolis, France: ERCIM. Retrieved 2 February 2003 from http://www.ercim.org/publication/ws-proceedings/DelNoe01/3_Toms.pdf
*   <a id="von04" name="von04"></a><a id="wad69" name="wad69"></a>Wade, S. & Schramm, W. (1969). The mass media as sources of public affairs, science, and health knowledge. _Public Opinion Quarterly_, **33**(2), 197-209\.
*   <a id="wea06" name="wea06"></a>Weatherhead, S.C. & Lawrence, C.M. (2006). Melanoma screening clinics: are we detecting more melanomas or reassuring the worried well? _British Journal of Dermatology_, **154**(3), 539-541\.
*   <a id="wilk97" name="wilk97"></a>Williamson, K. (1997). The information needs and information-seeking behaviour of older adults: an Australian study. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information seeking in context: proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts 14-16 August 1996, Tampere, Finland_. (pp. 337-350). London: Taylor Graham.
*   <a id="wil81" name="wil81"></a>Wilson, T.D. (1981). [On user studies and information needs.](http://www.webcitation.org/5TWF2GEm9) _Journal of Documentation_, **37**(1), 3-15\. Retrieved 21 November, 2007 from http://informationr.net/tdw/publ/papers/1981infoneeds.html
*   <a id="wil97" name="wil97"></a>Wilson, T.D. (1997). Information behaviour: an interdisciplinary perspective. _Information Processing & Management_, **33**(4), 551-572.
*   <a id="wil00" name="wil00"></a>Wilson, T.D. (2000). [Human information behavior](http://www.webcitation.org/5TW9ovzu7). _Informing Science_, **3**(2), 49-56\. Retrieved 21 November, 2007 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf
*   <a id="win04" name="win04"></a>Winkelby, M.A., Feighery, E., Dunn, M., Kole, S., Ahn, D. & Killen, J.D. (2004). Effects of an advocacy intervention to reduce smoking among teenagers. _Archives of Pediatrics & Adolescent Medicine_, **158**(3), 269-275.

</fieldset>

</form>

* * *