#### Vol. 12 No. 2, January 2007

* * *

# Information sharing between different groups: a qualitative study of information service to business in Japanese public libraries

#### [Shunsaku Tamura](mailto:tamaran@slis.keio.ac.jp)  
Keio University, 2-15-45 Mita, Minato-ku, Tokyo, Japan  
#### [Makiko Miwa](mailto:miwamaki@nime.ac.jp)  
National Institute of Multimedia Education, 2-12 Wakaba, Mihama-ku, Chiba-shi, Japan  
#### [Yasunori Saito](mailto:saitoy@kisc.meiji.ac.jp)  
Meiji University, 1-1 Kanda-Surugadai, Chiyoda-ku, Tokyo, Japan  
#### [Mika Koshizuka](mailto:mika.koshizuka@gakushuin.ac.jp)  
Gakushuin Women's College, 3-20-1 Toyama, Shinjuku-ku, Tokyo, Japan  
#### [Yumiko Kasai](mailto:ykasai@edu.tamagawa.ac.jp)  
Tamagawa University, 6-1-1 Tamagawagakuen, Machida-shi, Japan  
#### [Mamiko Matsubayashi](mailto:mamiko@slis.tsukuba.ac.jp)  
University of Tsukuba, 1-1-1 Tennodai, Tsukuba-shi, Japan  
#### [Nozomi Ikeya](mailto:nikeya@parc.com)  
Palo Alto Research Center, 333 Coyote Hill Road, Palo Alto, CA 94304 USA

#### Abstract

> **Introduction.** This paper is the first report of a research project on the effects of information service to business in Japanese public libraries. The overall goals of the project are to develop a conceptual framework for understanding effects of a library service and then to examine them in business information service. The objective of this first report is to get an overview of current practice of business information service in Japanese public libraries and examine images of users and uses by librarians in charge.  
> **Method.** The project consists of three stages. At the first stage a series of field trips was conducted with semi-structured interviews in twenty-two libraries throughout Japan and a focus group interview of librarians in charge of the service was also conducted (not reported here).  
> **Results.** A variety of services is provided by public libraries. Levels of reference service and relationships with other agencies and organizations are the most important factors in determining the nature and kind of service provided. Providing the service is actually a complex process influenced by many factors.  
> **Conclusions.** Results suggest strongly the complex process of value creation. Images of users and uses are formed not only by direct contact with users but as a result of this complex process. Sometimes images have political connotations as both librarians and other stakeholders hope the service to be useful in promoting local business and/or industry and advancing local lives.

## Introduction

This paper is the first report of a research project on the effects of information service to business in Japanese public libraries. The overall goals of the project are: 1\. to develop a conceptual framework for understanding effects of a library service from the point of view of sharing of information and resulting sharing of values of the service, and then, 2\. to examine them in business information service.

The project consists of three stages: 1\. a survey of the current practices of the service, to develop a framework for studying its effects and to examine images of users and uses of the service by librarians in charge; 2\. field work to observe interactions between librarians and users, to interview users about the sharing and use of information obtained from the service in their work context and to interview librarians about the sharing of information on users and user needs; 3\. analysis of data from field work, to find consistencies and inconsistencies on the notions of the people concerned as to uses and values (effects) of the service. This paper reports the results of the first stage, i.e., the development of a framework for studying the effects of the business information service, based upon the literature review and the survey of the current practices of the service and to examine the general notion of librarians about users and uses of the service.

It seems that there is a chasm between information behaviour research and the library use study. In spite of the suggestions by Zweizig and Dervin ([1977](#zweizig77)), the library use study continues to be quantitative and has become simpler and easier to conduct. A typical example is the Library Performance Indicators Standard by ISO (ISO 11620:1998). None could deny its success in evaluating performance and improving management. However, when the focus of evaluation shifts from output of services to outcome, difficulties seem to arise. It is certain that calculating such records of use as circulation, use of reference service and programme attendance is more clear-cut and therefore, easier than assessing effects of library use, because they relate to both cognitive and behavioural aspects of human actions and are difficult to define as the meaning of 'use' and 'effect' will change in the course of action. Limitations of quantitative study are evident here and some qualitative methodology is called for.

On the other hand, since Dervin and Nilan ([1986](#dervin86)) proclaimed the 'paradigm shift', studies of information behaviour, at least at the theoretical level, have been focusing too much upon individuals in different settings, while neglecting how they use information services or sources. As Wilson ([1981](#wilson81)) pointed out, only user-centred approaches can give bias-free data which show appropriateness and relative importance of the service or information source concerned. However, emphasis on the cognitive and behavioural aspects of individuals in the studies of information behaviour have made it rather difficult to pay attention to, and to develop methodologies for looking at how users use particular information services or sources in actual contexts except, recently, in the case of use of Internet-based services (see for example, [Martzoukou, 2005](#martzoukou05))

The present project is an attempt to bridge the gap between the library use and evaluation study and the information behaviour research by introducing concepts and methods of the latter to the former. To this end, the authors first develop the general framework of the research and then conducted a survey of the field, i. e., information service to business in Japanese public libraries.

The business information service is new to Japanese public libraries. Stimulated by the services of the public libraries in the USA, they started the service in the late 1990s. A group of librarians, journalists and academics formed a society for the promotion of the service called Bijinesu Shien Toshokan Suishin Kyogikai in 2000\. Its members have hoped that starting the service will be a step toward reforming Japanese public libraries which are at times criticised for their overemphasis on circulation and recreational reading. They see in the service an opportunity for public libraries to move forward to the direction of supporting more diverse aspects of citizens' lives, a chance for the 'redefinition' of their services, so to speak. On the other hand the government expects public libraries to help people start businesses and thus to participate in the government's plan to revitalize Japanese business and industry.

The service is suitable for the project because there is an urgent need for investigation in order to make guidelines for service provision based upon the appropriate assessment of needs and effects and there is much to observe and ask, as librarians and users have to negotiate the scope of the service in the absence of a standard definition.

There are reports on practices of business information service in public libraries ([Welch, 2005](#welch05)) and studies of business information needs ([Vaughan 1997](#vaughan97)), especially in the U.K. ([Roberts & Clifford 1986](#roberts86); [Webber 1999](#webber99)). Webber pointed out that business people do not see information seeking problems as such but as 'business problems', so that they do not handle the problem properly nor think of public libraries as places for solving it. Conceptions of public libraries among business people and how they relate to the use are major issues to investigate in this project.

## Framework of the overall project

### Theoretical background

The most relevant previous study focusing upon a particular service is Taylor's value-added process model ([Taylor 1986](#taylor86)). He proposes a model which explains the value-added activities in various types of information systems such as libraries.

Taylor's work is seminal as it defines the nature of information systems as value-added processes for anticipated uses and thus opens the way to evaluating interactions between users and systems from the users' perspective. The actual value of a system is only realised in the dynamic negotiation process between a system and a user in a problem situation. However, his model still presupposes a kind of isomorphism between the values assigned by users and the enhancements of systems. This assumption cannot be held, as there is no reason to think that users assign the same value to messages in the use environment as in the negotiating space. In fact, O'Day and Jeffries ([1993](#oday93)) found that regular users of libraries acted themselves as intermediaries, sharing information they had received from library searches with others in their work settings. There they acted as information artisans, creating new artefacts by transforming and enhancing their search results before passing them on to others in collaborative settings.

At this point Normann and Ramirez's ([1994](#normann94)) theory of value constellation will help elaborate on the conceptual framework posed by Taylor. It provides an interactive and dynamic approach to explaining the logic of value creation in networked environments (see also [Huotari & Chatman, 2001](#huotari01)). In traditional theories value is created through a 'value chain'. Normann and Ramirez pointed out that they fit well with traditional industrial economy but not with modern service economy. Instead, they argue that modern companies in networked environments unite with each other flexibly as stakeholders and form a 'value constellation', where companies work together to create joint value which is manifest in 'offerings', i.e., products or services that contain value

### Framework of the project

The authors take the perspective that every action is situated in a context ([Suchman 1987](#suchman87); [Davenport 2002](#davenport02)) and that value is jointly created in concerted actions by participants in the context. From this perspective, it seems that Normann and Ramirez' theory can be extended to include all the organizational contexts.

These concerted actions sometimes succeed and sometimes fail, the result of which will be consistencies and inconsistencies in valuations among different groups. _Effects_ will be defined here as these consistencies in valuations. Then consistencies will come from communication and the resulting sharing of information about needs and uses of the service.

Thus, the basic assumption of the project is that the effect of business information service is the consistency of values created in interactions and the resulting information sharing among library staff, between the librarian and the user and among people in the user's community. The hypothetical image of the interactions and information sharing is as follows:

<figure>

![Figure 1](../p306fig1.jpg)

<figcaption>

**Image of the interactions and information sharing in the service**</figcaption>

</figure>

Three different groups of interactions are shown in the Figure. The first group (a) is that of interactions among librarians. Here details of the service are agreed upon as to staffing, available resource, manuals and so on, based upon knowledge (images) of users and uses of the service. Here value means enhancements or anticipated use, according to Taylor. They will become resources in the user-librarian interactions (b).

The second group of interactions (b) is between the user and the librarian in charge. Negotiations are carried on over the user's need and the relevance of the information. Here value means negotiated use of information. It will be a resource in information sharing in the user community (c).

In interactions in the user community (c) the information the user receives will be modified and put into practice as part of their work. Values will be assigned retrospectively to uses of information they put to work. At the same time images of libraries and librarians as to their usefulness will be formed.

Three different values will be identified in different groups: (a) enhancements of anticipated use (Taylor) or offerings (Normann and Ramirez) which librarians will expect users to get from service; (b) negotiations which will be achieved between users and the library; (c) uses that users and other members of the user community will find in information or services they have received at the library. Finding consistencies and inconsistencies among these three conceptions of value is the main problem to be investigated in this project.

### Research questions and methodology of the first stage

With the above mentioned framework in mind, the following three research questions are set for the first stage:

1.  What kind of services is currently provided in business information service in Japanese public libraries? It is necessary to get the overall knowledge of the field concerned, i. e., characteristics of the libraries offering the service, varieties of services provided, and so on.
2.  What kind of users do librarians expect, and have in reality? Users and uses, both in expectation and in reality, are together determining factors in providing the service.
3.  What kind of uses or values do librarians think users get from the service? Based upon the anticipated valuations by users, librarians organize their service, and, conversely, the organization of service will attract certain types of users.

A series of field trips and a focus-group interview have been conducted. There was no list of the libraries offering the service. Therefore, the project started by locating and listing them and thirty-one libraries were identified, from which a total of twenty-two libraries were selected for the field trips. The selection criteria were the location and the level of service: included were those libraries known to be active in offering the service.

After the trip, a more comprehensive survey was conducted by Bijinesu Shien Toshokan Suishin Kyogikai [the Society for the Promotion of the Business Information Service in Public Libraries] in 2006\. Of 1,045 libraries responding, the survey identified 121 libraries currently offering business information service ([Society for the Promotion... 2006](#bijinesu06)). The difference between this figure and that which the present authors identified certainly shows the rapid growth of the service in Japanese public libraries. However, it should be noted that as the survey by the Society did not define the business information service but left the definition to responding libraries, and their figure might include a number of libraries only holding business books and providing no special service to the business community.

At each visit a semi-structured interview was conducted. Following the research questions, the interview guide included such questions as: goals and objectives of the service, history, services provided, collection development and other preparatory work, approximate number of users and their profiles and relationships with other agencies and organizations. The last question was added because it was known that other groups or stakeholders would communicate with libraries and/or users and have strong influence over the service.

In order to collect data supporting the findings of the field trips, a group interview was conducted on 6 February. Six librarians participated.

## Findings and interim analyses of the results

Following are some of the results focusing mainly on users and uses librarians have expected and perceived.

Based on the services provided, a typology of libraries was developed: a number of libraries provided only a couple of shelves or a corner with shelves for business material. This group of libraries can be called _libraries with minimal service_. Most libraries which claim they are offering the service actually fall in this category. Another group of libraries offered reference service as well as shelves or a corner. This group may be called the _reference service group_. A third group offered a consultation service by appointment with specialists like business consultants, solicitors and retired managers. This group may be called the _consultation service group_. Libraries provided various programmes, which were offered by all three groups of libraries and did not relate directly to this typology..

Levels of general reference service and relationships with other agencies and organizations were the most important factors in determining the level and content of the business information service. A certain level of reference service and a number of reference transactions seemed essential in successful provision of the service in the reference service group. Some libraries in this group responded that they started the service because they had already received a number of reference questions, some of which related to business. The commencement of a business information service in this group means reorganisation of the reference department in order to respond more effectively to the needs of the community and induce more demands from business people.

Also, relationships with other agencies and organizations were essential in providing a consultation service. The department of business and industry of the local government, the chamber of commerce and industry and the public employment security office were examples of such agencies and organizations. A public library is normally regarded as one of the most popular places in the community. Many people visit it for various purposes: they read books and magazines in the library; sometimes they consult books looking for information, but very few people ask librarians for help at most of the libraries. Most people, including librarians themselves, normally think that libraries can provide books and magazines but that librarians do not have the ability to provide specialised information. Therefore, when a library plans to provide a specialised information service, such as a business consultation service, it is difficult to persuade local government senior officials, unless they provide the service in cooperation with other agencies and organizations that have specialised knowledge or skills. On the other hand, such agencies and organizations often need appropriate service points where people can easily and comfortably stop by and ask for consultation or help.

Characteristics of the local community were also a factor in determining the level and content of the service. For example, provision of information on part-time jobs was the most popular service in a library serving a low income population. A library in a rural area may provide information on agriculture under the name of business information service as they find few entrepreneurs in their community and most needs relate to agriculture.

As a result, the shape of the service is diverse. A library in an area with a lot of small engineering factories provided a collection of science, technology and industry books, while some libraries in rural area collected books in agriculture and/or fisheries. Some librarians regarded their business information service as a part of providing local information, while others said many people came a long way from all over Japan to get consultation by specialists. Some libraries failed to attract other agencies and organizations, while others appeared only to provide a space for other organizations that offered the consultation service. Because there are no definitions or guidelines on business information service and advocates generally think such a task of defining, and thus limiting the territory of the service, is unneccessary at this stage of rapid growth, various types of services are provided under the name of business information service.

The resulting images of users by librarians are also diverse. Some librarians in the reference service group said their users were mainly men with business suits, while others (also in the reference service group) felt their users were more usually women looking for part-time jobs. Some consultation services specialised in advising people planning to start a small business, while others tended to support people with problems in everyday life, such as the procedure of importing goods, question about paying taxes, or even finding something worth doing for mothers who have just been freed from child care. Not surprisingly, libraries with a consultation service usually have deeper understanding of user needs, but even a library with only shelves and notice boards for business information could talk much of characteristics of their users. In that case the librarian has had close contact with local business groups and Bijinesu Shien Toshokan Suishin Kyogikai. Thus, it is understood that images of users are formed not only by direct contact with users but also by complex relationship with various stakeholders.

Differences were found in the librarians' conceptions of the values of the service. Librarians in the minimal and reference service groups said that people would be attracted to the organized rich collection of libraries, while librarians and specialists in the consultation service group often mentioned the openness and comfort of use as the reason why local government agencies and other organisations wanted to support the business information service in libraries. They thought of libraries as a first contact point for those people who wanted to start a business or needed help in everyday life or were looking for a job. Librarians in the minimal and reference service groups expressed difficulty in identifying users' needs and uses, while specialists (not librarians) in the consultation service group could describe the characteristics of users and uses clearly. It was impressive to find that librarians in the minimal and reference service groups tended to think that their services met the specific needs of users while specialists in the consultation service group tended to think they met users' needs in Kuhlthau's prefocus stage ([Kuhlthau, 2004](#kuhlthau04)) owing to libraries' popularity, openess and ease of use.

## Conclusion

Results of the field trips suggest strongly the complex process of value creation in preparation of business information service. The initial framework must be modified because of the importance of stakeholders such as relating agencies and organizations as well as specialists in the consultation service group. Relationships with them seem to be one of the most important factors in determining the nature of business information service. The authors will include them in the second stage of the Project.

Images of users and uses vary in accordance with values created in this process. Very roughly speaking, there are two types of users: users of the minimal and reference service groups can be called library users, while many users in the consultation service group may not use libraries, as previous studies suggested.

Also images are formed not only by direct contact with users but as a result of this complex process. Sometimes images have political connotations as both librarians and local government officials hope the service to be useful in promoting local business and/or industry and advancing local lives.

## Acknowledgements

This work is supported by a Grant-in-Aid for Scientific Research from the Ministry of Education, Science, Sports and Culture of Japan.

## References

*   <a id="bijinesu06"></a>Bijinesu Shien Toshokan Suishin Kyogikai [Society for the Promotion of the Business Information Service in Public Libraries] (2006). [_Bijinesu shien toshokan sabisu zenkoku anketo no gaiyo [Summary of a national survey on business information service in public libraries]_](http://www.webcitation.org/5LhWL1VLr). Paper presented at Dai 8-kai Toshokan Sogo Ten [8th Library Fair and Forum 2006]. Retrieved 6 January, 2007 from http://www.business-library.jp/activity/syousai/20061129-2.pdf
*   <a id="davenport02"></a><a>Davenport, E. & Hall, H. (2002) Organizational knowledge and communities of practice. _Annual Review of Information Science and Technology_, **36**, 171-227.</a>
*   <a id="dervin86"></a>Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33.
*   <a id="huotari01"></a>Huotari, M.-L. & Chatman, E. (2001). Using everyday life information seeking to explain organizational behavior. _Library and Information Science Research_, **23**(4), 351-366.
*   <a id="kuhlthau04"></a>Kuhlthau, C.C. (2004) . _Seeking meaning: a process approach to library and informationj services_. (2nd ed.). Westport, Conn.: Libraries Unlimited.
*   <a id="martzoukou05"></a>Martzoukou, K. (2005). [A review of Web information seeking research: considerations of method and foci of interest](http://informationr.net/ir/10-2/paper215.html). _Information Research_, **10**(2) Retrieved 4 January, 2007 from http://informationr.net/ir/10<wbr>-2/paper215.html
*   <a id="normann94"></a>Normann, R. & Ramirez, R. (1994). _Designing interactive strategy: from value chain to value constellation_. Chichester, UK: John Wiley.
*   <a id="oday93"></a>O'Day, V.L. & Jeffries, R. (1993). Information artisans: patterns of result sharing by information searchers. _Proceedings of the Conference on Organizational Computing Systems_. (pp.98-107). New York: ACM Press.
*   <a id="roberts86"></a>Roberts, N. & Clifford, B. (1986). Regional variations in the demand and supply of business information: a study of manufacturing firms. _International Journal of Information Management_, **6**(3), 171-183.
*   <a id="suchman87"></a>Suchman, L.A. (1987) . _Plans and situated actions: the problem of human-machine communication_. Cambridge: Cambridge University Press.
*   <a id="taylor86"></a>Taylor, R. S. (1986). _Value-added processes in information systems_. Norwood, NJ: Ablex.
*   <a id="vaughan97"></a>Vaughan, L.Q. (1997). Information search patterns of business communities. _Reference & User Services Quarterly_, **37**(1), 71-78.
*   <a id="webber99"></a>Webber, S. (1999). Helping small business encounter information. In A. Scammell (Ed.). _I in the sky_. (pp.185-190). London: ASLIB.
*   <a id="welch05"></a>Welch, J.M. (2005). Silent partners: public libraries and their services to small businesses and entrepreneurs. _Public Libraries_, **44**(5), 282-286.
*   <a id="wilson81"></a>Wilson, T.D. (1981). [On user studies and information needs.](http://www.webcitation.org/5LhWdc5z4) _Journal of Documentation_, **37**(1), 3-15\. Retrieved 6 January, 2007 from http://informationr.net/tdw/publ/papers/1981infoneeds.html [Also reprinted, _Journal of Documentation_, **62**(6), 2006, 658-670]
*   <a id="zweizig77"></a>Zweizig, D. & Dervin, B. (1977). Public library use, users, uses: advances in knowledge of the characteristics and needs of the adult clientele of American public libraries. _Advances in Librarianship_, **7**, 231-255.