#### Vol. 12 No. 2, January 2007

* * *

# Diffusion and usage patterns of Internet services in the European Union

#### [José Manuel Ortega Egea](mailto:jmortega@ual.es), [Manuel Recio Menéndez](mailto:mrecio@ual.es) and [María Victoria Román González](mailto:mvroman@ual.es)  
Depto. de Dirección y Gestión de Empresas,
Universidad de Almería,
Carretera de Sacramento S/N,
La Cañada de San Urbano,
04120 Almería, Spain

#### Abstract

> **Introduction.** This paper offers an investigation of European citizens' adoption and use of Internet-related technologies. European citizens are classified according to their Internet usage patterns. Next, the demographic and behavioural profiles of the identified groups are thoroughly examined, which clarifies citizens' Internet-related behaviour and how external variables are related to Internet uses.  
> **Method.** Telephone survey, using a random method, in order to guarantee the representativity of each national sample for its universe. Citizens from all European countries (EU-15) were surveyed about their Internet usage patterns.  
> **Analysis.** Two-Step Cluster Analysis has been applied, to identify the different groups of European Internet users. Discriminant and Correspondence Analysis have been respectively applied to assess the internal and external validity of the identified segments. The statistical package SPSS v11.5 was employed for the empirical analyses.  
> **Results.** Four segments of European Internet users have been identified: Laggards, Confused and Adverse, Advanced Users, Followers and Non-Internet Users. The behavioural and demographic profiles of the previously identified segments are examined. Next, the internal and external validity of the _clusters_ is assessed.  
> **Conclusion.** The identified Internet user segments show clear differences with regard to country, sex, occupation, education and location. These results confirm the existence of sociodemographic entry barriers to the use of digital services. Such barriers may to the development and widening of a digital divide among European citizens.

## Introduction

The availability of Internet technologies to a non-scientific or military audience has introduced fundamental changes in current societies. The generalized acceptance of such services as the World Wide Web and e-mail is fostering the diffusion of Internet-related technologies both for commercial and social purposes (Alba _et al._ [1997](#alb97); Dickson [2000](#dic00)).

The Internet sector has experienced a tremendous growth in the past decade, both in terms of the development of digital infrastructures and total number of Internet users. Nevertheless, the existence of significant national and regional differences in Internet adoption, owing to deficiencies in technological infrastructures, citizens' reluctance, or lack of needed skills to take advantage of Internet technologies, limits the potential benefits provided by new information services ([Hoffman _et al._ 1995](#hof95); [Hoffman & Novak 1996](#hof96); [White 1997](#whi97)). A central concern in this regard is clarifying why Internet usage varies across countries and regions.

A deeper understanding of the adoption rates, purposes and characteristics of Internet users in different nations should also be beneficial in two main areas: companies operating in domestic and international markets should account for current differences in customers' acceptance and adoption of digital services; a detailed analysis of citizens' Internet usage patterns should help public authorities develop policies aimed at further developing digital infrastructures and promoting the use of Internet-related services.

By gaining a stronger understanding of current uses of Internet services among European citizens, this paper makes three main contributions: (1) technology adoption theories are extended by means of a cross-cultural research of Internet usage; (2) European citizens are classified according to their Internet usage patterns; (3) the demographic and behavioural profiles of the identified groups are thoroughly examined, which clarifies citizens' Internet-related behaviour and how external variables are related to Internet uses.

## Theoretical foundation

Using as a starting point studies on the adoption of technological innovations and Internet services, this section reviews the main theoretical frameworks and factors related to individuals' use of Internet technologies. A special emphasis has been placed on reviewing the behavioural, demographic and geographic characteristics of people currently using the Internet services. This paper focuses on the analysis of such variables with regard to Europeans' Internet usage.

### Theoretical frameworks for Internet usage

The use of technological innovations is influenced by diverse factors, such as technological and infrastructural factors, the economic and legal context, or dimensions related to individuals' perceptions, demographic and geographic characteristics ([Korgaonkar and Wolin 1999](#kor99); [Rogers 2003](#rog03); [Venkatesh and Morris 2000](#ven00b)). Diverse theoretical frameworks have been applied in previous studies to characterize the acceptance and use of technological services.

According to Rogers' Diffusion of Innovations Model ([2003](#rog03)), the following factors determine the innovation adoption process: (1) innovation characteristics; (2) communication channels used to communicate its benefits; (3) elapsed time since the introduction of the innovation; and (4) the social system in which the innovation is going to spread. This model offered the following categorization of consumers, based on their adoption rates of technological innovations over time: Innovators (around 2.5%), Early Adopters (13.5%), Early Majority (34%), Late Majority (34%) and Laggards (16%).

The Technology Acceptance Model (TAM) ([Davis _et al._ 1989](#dav89)) is the most-widely cited explanatory model of an individual's acceptance of a specific technological innovation. Because of the technological characteristics of the Internet, the Technology Acceptance Model is increasingly applied to account for the main antecedents and determinants (external, perceptual and attitudinal) of Internet usage ([Venkatesh and Davis 2000](#ven00a); [Venkatesh _et al._ 2002](#ven02)). In particular, several authors have selected the TAM to characterize individuals' perceptions and intentions toward the use of Internet services ([Muthitacharoen and Palvia 2002](#mut02); [Pavlou 2003](#pav03)).

Recent studies especially show that more research is needed into the effect of external variables, such as demographics, geographic characteristics, previous experience with related technologies, etc., on individuals' use of technological innovations ([Venkatesh and Morris 2000](#ven00b)). This paper contributes to this stream of research by analysing how such external indicators influence the use of Internet services in a cross-national setting.

### Characterization of Internet usage

According to the purposes of this research, a review is provided of the main behavioural and socio-demographic characteristics, which have been found in previous studies to be significantly related to the use of Internet services. This review aims to provide the theoretical justification for the hypotheses in the present study's cross-national setting.

#### Behavioural characteristics

Consumers currently using the Internet show high behavioural differences compared to non-Internet users. Behaviour such as information searching have been found to be substantially different between current users of Internet technologies and non-Internet users ([Peterson and Merino 2003](#pet03)). Among other behavioural differences, adopters of Internet services are likely to be more active information searchers ([Dickerson and Gentry 1983](#dic83)) and have more previous experience in the use of computers and other related technologies ([Bain 1999](#bai99); [Rogers 2003](#rog03); [Samiee 1998](#sam98)). Previous studies have also found a positive association between online shopping and variables related to previous experience and frequency of Internet usage ([Loiacono _et al._ 2002](#loi02)).

Recent research has confirmed that both groups of current Internet users and non-Internet users are heterogeneous segments, which differ in their Internet-related behaviour, preferences and motivations to use diverse Internet services. A lifestyle segmentation analysis of Internet consumers identified four segments of online shoppers and four segments of online non-shoppers ([Swinyard and Smith 2003](#swi03)). Three segments of online shoppers were identified in a recent study, according to individuals' purchase behaviour of several product categories ([Bhatnagar and Ghose 2004](#bat04)).

Therefore, this research expects to identify different segments of European citizens on the basis of their online behaviour patterns. Variables reflecting different degrees of innovativeness in individuals' online behaviour are included in the segmentation analyses, which will enable to differentiate between more and less innovative Internet user segments. Therefore, the first hypothesis can be stated as follows:

Hypothesis 1\. Different groups of European citizens will be identified on the basis of their Internet behaviour patterns.

#### Demographics

Previous research on the use of technological innovations has found that demographic traits such as education, age, or income are significantly associated with usage rates of technological innovations ([Dickerson and Gentry 1983](#dic83); [Zeithaml and Gilly 1987](#zei87)). Dickerson and Gentry showed that adopters of personal computers, an innovation with several similarities to Internet technologies since both require the establishment of new behaviour patterns, tend to be middle-aged, with higher income and highly educated.

In extensions of Technology Acceptance Model, demographic factors such as sex and age have also provided significant effects on the use of technology. Sex is a significant moderator of the relationship between TAM's core constructs (perceived usefulness, perceived ease of use and subjective norm) and the intention to use a technological innovation ([Venkatesh and Morris 2000](#ven00b); [Venkatesh _et al._ 2000](#ven00c)). Age differences have also been identified in technology usage rates, suggesting that individuals' needs and preferences follow a life-cycle orientation. Age is argued to be negatively related to technology usage, usefulness perceptions and positively related to perceived difficulty ([Morris and Venkatesh 2000](#mor00)).

Recent studies seem to confirm the existence of significant associations between user demographics and Internet usage patterns ([Atkin _et al._ 1998](#atk98)). Web-users have been profiled on the basis of their demographic characteristics, suggesting the influence of sex, education, age and income on the use several Web-based services. Web-users were found to be male, highly-educated, with average income and middle-aged or young ([Korgaonkar and Wolin 1999](#kor99)). Income and education levels are especially relevant in explaining the use of Internet services and other technological devices ([Guillén 2002](#gui02)). For instance, the adoption of home Internet services involves several costs, both in terms of the financial resources and skills needed for the use of new technologies. Additionally to income, sex, age and education effects, there is also evidence of ethnic differences in Internet use ([Katz and Aspden 1997](#kat97)).

With regard to the use of online shopping services, previous research has found significant sex, age and income influences ([Bain 1999](#bai99)). Online shoppers have been found to be predominantly male, younger and wealthier than non-online shoppers ([Swinyard and Smith 2003](#swi03); [Teo 2001](#teo01)).

Previous studies evidence concerns about the development and widening of a _digital divide_ based on citizens' location. In this regard, a higher share of individuals currently using Internet services is usually located in metropolitan than in rural settings ([Bell _et al._ 2004](#bel04); [Kehoe _et al._ 1999](#keh99)).

The preceding discussion suggests the existence of significant differences in Europeans' sex, age, income, location and education levels, with regard to their use of the Internet services analysed in this study and leads to the following hypotheses:

Hypothesis 2\. The identified Internet user segments will show significant differences based on sex: more frequent and innovative Internet uses are expected among male respondents.

Hypothesis 3\. The identified Internet user segments will show significant differences based on age: more frequent and innovative Internet uses are expected among middle-aged respondents.

Hypothesis 4\. The identified Internet user segments will show significant differences based on education levels: more frequent and innovative Internet usage patterns are expected among respondents with higher education levels.

Hypothesis 5\. The identified Internet user segments will show significant differences based on professional activity: more frequent and innovative Internet usage patterns are expected among respondents employed in service sectors.

Hypothesis 6\. The identified Internet user segments will show significant differences based on location: more frequent and innovative Internet usage patterns are expected among respondents located in metropolitan areas.

Nevertheless, there is also conflicting evidence with regard to the demographic profiles of Internet users. For instance, a recent study did not find significant differences in Internet usage based on age or educational level and the use of several Internet services (i.e., messaging, browsing, downloading and purchasing services) did not show uniform relationships with demographic factors (i.e., sex, age and educational level). According to Rogers ([2003](#rog03)), demographic differences of adopters and non-adopters of more mature media are becoming less and less significant. Because of the characteristics of the Internet as a communications medium in early stages of diffusion, it could be reasonable to expect that, as the use of Internet services in the population becomes more widespread, demographic traits become progressively less helpful in explaining Internet usage patterns ([Korgaonkar and Wolin 1999)](#kor99).

#### Country differences

Several studies confirm the existence of clear national and regional differences, with regard to the penetration of Internet technologies in the European Union ([Guillén 2002](#gui02); [Ngini _et al._2002](#ngi02); [Quelch 1996](#que96)). Although the use of Internet services is increasing rapidly in most countries, Internet markets are not developing at similar paces in all regions ([Crosby and Johnson 2002](#cro02)). Such national and regional differences limit significantly the potential benefits offered by Internet technologies in certain countries.

Consistent with previous economical and technological trends, this study expects to find significant differences in Internet adoption between European countries. This can be stated in the following hypothesis:

Hypothesis 7: The relative distribution of the identified Internet user segments will show significant differences between European Countries: more frequent and innovative Internet usage patterns are expected in Northern European countries.

## Study design

### Measures

The following measures have been selected to clarify the research questions of this study:

(1) Behavioural segmentation of European citizens based on their Internet usage patterns. The following behavioural indicators of Internet usage have been selected to segment European respondents: frequency of access to the Internet, frequency of online purchases, use of eGovernment services and other uses of Internet technologies. The segmentation analyses will identify groups of European citizens with differentiated Internet usage patterns. The analysis of more advanced Internet uses, such as online shopping and eGovernment services, will enable to differentiate between more and less innovative Internet user groups.

(2) Demographic and geographic profiling of the identified segments. The previously identified segments will be profiled on the basis of demographic and geographic indicators (sex, age, education, professional activity, location and country), in order to clarify the relationship between external variables and Internet usage.

### Suitability of the behavioural segmentation approach

Several segmentation approaches can be applied to group Internet users or online consumers sharing certain characteristics. Researchers from the segmentation field illustrate the usefulness of a behavioural profiling of consumers, which enables the identification of consumer categories such as current users, non-users, light users, or heavy users ([Ram and Jung 1990](#ram90); [Weinstein 2004](#wei04)). The purposes and frequency of product use are the principal dimensions used for behavioural segmentation. This approach has been selected to segment European citizens, according to their use of diverse Internet services. A detailed characterization of Europeans' Internet behaviour should be valuable both to private companies and public authorities, in order to ensure an effective implementation of Internet technologies in all European countries.

## Methodology

### Sample

The empirical analyses are based on the survey data included in the Flash-Eurobarometer Nº 125 _Internet and the general public_, conducted on a representative sample of European citizens. The Eurobaromenter surveys are regularly carried out on behalf of the European Commission in the EU-15 countries and provide access to comparative and representative survey data in European countries, which are of great value for the analysis of international research questions. Without access to this kind of secondary data sources and due to the lack of needed financial and human resources, most researchers would not be able to carry out this kind of comparative research. Table 1 shows the characteristics of the sample and the sampling procedure.

<table><caption>

**Table 1: Sample characteristics**</caption>

<tbody>

<tr>

<th>Sample</th>

</tr>

<tr>

<td>Universe: European citizens with fixed telephone lines in their households</td>

</tr>

<tr>

<td>Geographical scope: EU-15 countries</td>

</tr>

<tr>

<td>Sampling procedure: Telephone survey, using a "constant interval" procedure, in order to guarantee the representativity of each national sample for its universe</td>

</tr>

<tr>

<td>Sample size: 30,336 respondents (aprox. 2,000 per country)</td>

</tr>

<tr>

<td>Sampling error: 0.6%</td>

</tr>

<tr>

<td>Confidence level: 95%</td>

</tr>

<tr>

<td>Collected information:  
- Demographic variables: age, country, sex, professional activity, etc.  
- Behavioural indicators of the place of access, frequency and purposes of Internet usage</td>

</tr>

<tr>

<td>Dates: Interviews conducted between November 4th-19th, 2002</td>

</tr>

</tbody>

</table>

The _constant interval_ sampling procedure was applied to the selection of numbers from each country's published telephone lists. First, the constant interval was determined by dividing the total number of telephone records by the desired sample size in each country ([Statistics Canada 2006](#sta06)). Then, a group of telephone numbers, equaling the size of the constant interval, was selected from the initial records in the national telephone lists. This group of numbers represents the pool from which the first record in the national subsamples was randomly drawn. Finally, the constant interval was added to the number of the first and succeeding records, in order to obtain the country samples .

In this study, sampled telephone numbers include only fixed telephone lines. The increasing prominence of mobile phones ['cell phones' in the USA] in certain European countries, especially in Northern European and Scandinavian countries (e.g., Finland), might represent a threat to the representativeness of usual telephone surveys. A recent study carried out in the United States found individuals using only mobile phones to be significantly different in many ways from those reachable on a landline phone. According to this report, _cell-only_ users have a considerably different demographic profile, especially with respect to sex, race, age, education and home ownership. Especifically, cell-only respondents are _'younger, less affluent, less likely to be married or to own their home and more liberal on many political questions'_. With regard to their general opinion of computers and technology, these individuals are _'much more positive toward computers and technology than landline-only respondents and somewhat more positive than other cell phone users who are accessible on a landline'_ ([The Pew Research Center 2006](#pew06)).

On most issues, landline samples resemble to a higher extent the characteristics of the general population (The Pew Research Center [2006](#pew06)). Therefore, not including cell-only users should not significantly affect the representativeness of the present study, which examines people with diverse demographic profiles. However, cell-only samples should be very useful in other studies focusing on certain kind of respondents, especially younger people.

To avoid potential biases in the national samples, χ<sup>2</sup> difference tests were performed to compare the sample and national demographics (CIA [2006](#cia06)). Separate tests for each EU-country did not show statistically significant differences at the 95% confidence level, in terms of sex or age. Both the sample and national data show very similar sex and age structures. These results provided further evidence that each national subsample is representative of the population aged 15 and over.

### Statistical methodology

In order to identify the different groups of European Internet users, the Two-Step Cluster procedure has been applied, as implemented in the SPSS v11.5 software. The algorithm included in the Two-Step Cluster method is very suitable for the segmentation analysis of this study for two main reasons ([Chiu _et al._ 2001](#chi01); [Zhang _et al._ 1996](#zha96)): (1) It has been designed to handle very large data sets; (2) It provides great flexibility for dealing with continuous and categorical variables. Next, Discriminant Analysis has been applied to assess the internal validity of the identified segments. Finally, Correspondence Analysis has been selected to carry out the external validation of segments on the basis of socio-demographic variables.

## Results

### Descriptive statistics

Table 2 includes a demographic characterization of the sample, with regard to the Internet uses selected to identify the different Internet user segments. The analysis of the relative number of respondents currently using the Internet, online shopping services and eGovernment services in each country suggests the existence of significant national differences in Internet adoption in the European Union. The data evidence higher Internet adoption rates in Northern European countries. Respondents from different countries do not show uniform usage patterns of the diverse Internet services analysed in the study.

While several researchers have pointed out that demographic traits should become less relevant in explaining Internet uses ([Korgaonkar and Wolin 1999](#kor99)), the demographic profiling of European Internet users suggests the following: (1) male respondents are more frequent users of practically all analysed Internet services; (2) middle-aged individuals are the most frequent Internet users, especially with regard to online shopping and e-government services; (3) the relative use of more innovative Internet services (i.e., online shopping and eGovernment services) increases among urban and metropolitan respondents, compared to rural citizens; (4) education (measured by the age respondents finished school) and professional activity provide some of the strongest associations with the use of digital services.

A preliminary review of the sample's socio-demographic characteristics confirms the results of previous research relating country, sex, age, education and professional category to the use of Internet services ([Atkin _et al._ 1998](#atk98); [Dickson 2000](#dic00); [Morris and Venkatesh 2000](#mor00); [Swinyard and Smith 2003](#swi03)). The cluster and correspondence factor analysis will further clarify the existing relationships between these socio-demographic indicators and the analysed Internet-related uses.

<table><caption>

**Table 2: Sample characteristics**</caption>

<tbody>

<tr>

<td rowspan="2">

**COUNTRY**</td>

<td colspan="4">

**Demographics**</td>

</tr>

<tr>

<th>Total</th>

<th>Internet users</th>

<th>E-commerce users</th>

<th>E-Government users</th>

</tr>

<tr>

<td colspan="5"></td>

</tr>

<tr>

<td>Belgium</td>

<td>6.56%</td>

<td>5.43%</td>

<td>3.16%</td>

<td>5.58%</td>

</tr>

<tr>

<td>Denmark</td>

<td>6.64%</td>

<td>8.76%</td>

<td>9.41%</td>

<td>11.15%</td>

</tr>

<tr>

<td>Germany</td>

<td>6.59%</td>

<td>7.18%</td>

<td>9.84%</td>

<td>7.45%</td>

</tr>

<tr>

<td>Greece</td>

<td>6.59%</td>

<td>2.14%</td>

<td>0.57%</td>

<td>1.40%</td>

</tr>

<tr>

<td>Spain</td>

<td>6.61%</td>

<td>4.63%</td>

<td>1.95%</td>

<td>4.14%</td>

</tr>

<tr>

<td>France</td>

<td>6.62%</td>

<td>5.84%</td>

<td>5.31%</td>

<td>6.77%</td>

</tr>

<tr>

<td>Ireland</td>

<td>6.59%</td>

<td>6.68%</td>

<td>5.95%</td>

<td>4.63%</td>

</tr>

<tr>

<td>Italy</td>

<td>6.60%</td>

<td>4.95%</td>

<td>2.49%</td>

<td>4.20%</td>

</tr>

<tr>

<td>Luxembourg</td>

<td>6.59%</td>

<td>7.68%</td>

<td>8.56%</td>

<td>7.60%</td>

</tr>

<tr>

<td>Holland</td>

<td>6.61%</td>

<td>8.35%</td>

<td>8.30%</td>

<td>8.92%</td>

</tr>

<tr>

<td>Austria</td>

<td>6.59%</td>

<td>7.32%</td>

<td>7.10%</td>

<td>6.21%</td>

</tr>

<tr>

<td>Portugal</td>

<td>6.59%</td>

<td>3.78%</td>

<td>1.44%</td>

<td>2.78%</td>

</tr>

<tr>

<td>Finland</td>

<td>6.59%</td>

<td>7.45%</td>

<td>6.82%</td>

<td>6.17%</td>

</tr>

<tr>

<td>Sweden</td>

<td>6.59%</td>

<td>8.04%</td>

<td>9.74%</td>

<td>12.25%</td>

</tr>

<tr>

<td>United Kingdom</td>

<td>7.62%</td>

<td>11.79%</td>

<td>19.33%</td>

<td>10.76%</td>

</tr>

<tr>

<td colspan="5">

**SEX**</td>

</tr>

<tr>

<td>Men</td>

<td>46.09%</td>

<td>52.20%</td>

<td>57.99%</td>

<td>55.42%</td>

</tr>

<tr>

<td>Women</td>

<td>53.91%</td>

<td>47.80%</td>

<td>42.01%</td>

<td>44.58%</td>

</tr>

<tr>

<td colspan="5">

**AGE**</td>

</tr>

<tr>

<td>15-24</td>

<td>15.54%</td>

<td>24.14%</td>

<td>19.29%</td>

<td>18.39%</td>

</tr>

<tr>

<td>25-39</td>

<td>28.80%</td>

<td>36.56%</td>

<td>42.62%</td>

<td>40.53%</td>

</tr>

<tr>

<td>40-54</td>

<td>24.51%</td>

<td>26.31%</td>

<td>27.71%</td>

<td>28.93%</td>

</tr>

<tr>

<td>&gt;55</td>

<td>30.99%</td>

<td>12.89%</td>

<td>10.28%</td>

<td>12.09%</td>

</tr>

<tr>

<td colspan="5">

**AGE FINISHING SCHOOL**</td>

</tr>

<tr>

<td>Not schooled</td>

<td>1.78%</td>

<td>0.39%</td>

<td>0.33%</td>

<td>0.42%</td>

</tr>

<tr>

<td>15&lt;</td>

<td>22.56%</td>

<td>8.65%</td>

<td>5.58%</td>

<td>5.17%</td>

</tr>

<tr>

<td>16-20</td>

<td>44.74%</td>

<td>48.79%</td>

<td>46.05%</td>

<td>43.24%</td>

</tr>

<tr>

<td>&gt;21</td>

<td>29.18%</td>

<td>41.20%</td>

<td>47.05%</td>

<td>50.30%</td>

</tr>

<tr>

<td colspan="5">

**LOCALITY TYPE**</td>

</tr>

<tr>

<td>Metropolitan</td>

<td>25.86%</td>

<td>27.52%</td>

<td>27.21%</td>

<td>29.23%</td>

</tr>

<tr>

<td>Urban zone</td>

<td>39.25%</td>

<td>41.30%</td>

<td>42.47%</td>

<td>42.40%</td>

</tr>

<tr>

<td>Rural zone</td>

<td>34.85%</td>

<td>31.15%</td>

<td>30.31%</td>

<td>28.36%</td>

</tr>

<tr>

<td colspan="5">

**PROFESSIONAL ACTIVITY**</td>

</tr>

<tr>

<td>Self-employed</td>

<td>8.58%</td>

<td>9.34%</td>

<td>10.50%</td>

<td>10.40%</td>

</tr>

<tr>

<td>Employee</td>

<td>31.08%</td>

<td>44.30%</td>

<td>50.44%</td>

<td>51.33%</td>

</tr>

<tr>

<td>Manual worker</td>

<td>13.67%</td>

<td>13.22%</td>

<td>12.81%</td>

<td>12.18%</td>

</tr>

<tr>

<td>Without professional activity</td>

<td>46.11%</td>

<td>32.53%</td>

<td>25.66%</td>

<td>25.55%</td>

</tr>

</tbody>

</table>

### Identification of segments

In order to identify the optimal number of segments, the values of the Log-likelihood Distance measure and Schwarz's Bayesian Information Criterion (BIC) have been analysed. This information criterion weights both model fit and parsimony of different models ([Magidson and Vermunt 2004](#mag04)). In clustering contexts, the BIC criterion has been found to outperform other classification criteria ([Biernacki and Govaert 1999](#bie99)). A model with a lower BIC value should be preferred over a model with a higher BIC value. Nevertheless, when analysing large samples, it is difficult to identify a minimum in BIC. In such cases, researchers should examine the percentual reductions in BIC between different segment-solutions.

Table 3 shows the values of BIC for each of the fifteen potential cluster-solutions compared in this study. Each of the cluster-models is characterized by a different number of segments (i.e., cluster models including a number of groups between 1 and 15). Taking into account the existence of significant percentual reductions in BIC, as well as a high _distance measure_, which reflects the degree of dissimilarity between segments, a four-segment model was identified as the optimal classification of Europeans Internet users. Individuals currently not using the Internet were not considered in the cluster analyses. Including this group of _Non-Internet Users_, European citizens can be classified into five final segments, according to their current Internet usage patterns.

Therefore, _Hypothesis 1 is supported_: different groups of European citizens have been identified on the basis of their Internet behaviour patterns.

Rogers' classification is the most-widely known categorization of consumers with regard to technology adoption (Rogers [2003](#rog03)). Rogers' categories are related to the time needed for the adoption of technological innovations by different consumers. The present study, though, identifies segments by examining the innovativeness of citizens' Internet usage. The application of different criteria for the identification of segments limits the possibilities to draw comparisons between Rogers's and this study.

<table><caption>

**Table 3: BIC for cluster-solutions with different number of segments**  
(Non-Internet Users not included)</caption>

<tbody>

<tr>

<th>Number of clusters</th>

<th>BIC</th>

<th>BIC change</th>

<th>BIC change %</th>

<th>Distance measure</th>

</tr>

<tr>

<td>1</td>

<td>-926081.87</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>2</td>

<td>-976801.49</td>

<td>-50719.61</td>

<td>1</td>

<td>1.4523</td>

</tr>

<tr>

<td>3</td>

<td>-1011542.14</td>

<td>-34740.66</td>

<td>68.50%</td>

<td>1.6517</td>

</tr>

<tr>

<td>

**4** **segments**  
(optimal solution)</td>

<td>

**-1032344.38**</td>

<td>

**-20802.23**</td>

<td>

**41.01%**</td>

<td>

**1.6111**</td>

</tr>

<tr>

<td>5</td>

<td>-1045034.21</td>

<td>-12689.83</td>

<td>25.02%</td>

<td>1.2023</td>

</tr>

<tr>

<td>6</td>

<td>-1055490.56</td>

<td>-10456.35</td>

<td>20.62%</td>

<td>1.1658</td>

</tr>

<tr>

<td>7</td>

<td>-1064376.40</td>

<td>-8885.84</td>

<td>17.52%</td>

<td>1.0612</td>

</tr>

<tr>

<td>8</td>

<td>-1072716.27</td>

<td>-8339.87</td>

<td>16.44%</td>

<td>1.2706</td>

</tr>

<tr>

<td>9</td>

<td>-1079155.62</td>

<td>-6439.35</td>

<td>12.70%</td>

<td>1.1649</td>

</tr>

<tr>

<td>10</td>

<td>-1084600.62</td>

<td>-5445.00</td>

<td>10.74%</td>

<td>1.1484</td>

</tr>

<tr>

<td>11</td>

<td>-1089266.25</td>

<td>-4665.63</td>

<td>9.20%</td>

<td>1.0033</td>

</tr>

<tr>

<td>12</td>

<td>-1093914.41</td>

<td>-4648.16</td>

<td>9.16%</td>

<td>1.0406</td>

</tr>

<tr>

<td>13</td>

<td>-1098358.57</td>

<td>-4444.16</td>

<td>8.76%</td>

<td>1.3373</td>

</tr>

<tr>

<td>14</td>

<td>-1101534.28</td>

<td>-3175.72</td>

<td>6.26%</td>

<td>1.0947</td>

</tr>

<tr>

<td>15</td>

<td>-1104384.87</td>

<td>-2850.58</td>

<td>5.62%</td>

<td>1.0265</td>

</tr>

</tbody>

</table>

### Behavioural characterization of the identified segments

In order to characterize the identified segments of Internet users, the intra-conglomerate variation has been analysed (95% confidence intervals). Next, a behavioural characterization of the identified segments is provided, based on the variables used for the identification of segments:

Segment 1: Laggards (16%)

Europeans included in the _Laggards_ group are characterized by an occasional and infrequent use of Internet services. None of these respondents use Internet technologies to get in contact with the Public Administration (eGovernment services). The Internet is rarely used for private purposes.

Segment 2: Confused or Adverse (2%)

The frequency of Internet use among _Confused or Adverse_ citizens is intermediate. These respondents' Internet behaviour is characterized by high variability. Confusion about Internet services seems to be another characteristic, as these respondents give _Don't Know_ or _No Answer_ as an answer very often. Internet technologies are rarely used for private purposes or for contacts with the Public Administration. With regard to the use of online shopping services, most of these users have never purchased online and it is the only segment showing previous negative experiences with online purchases.

Segment 3: Advanced users (19%)

_Advanced Users_ are the most frequent users of the Internet among surveyed people. It is the group showing the most frequent use of eGovernment services, not only for administrative tasks (e.g., to search for administrative information, to fill up forms, or to carry out administrative transactions), but also for other purposes. The most frequent European online shoppers can also be found among _Advanced Users_.

Segment 4: Followers (19%)

Respondents included in the _Followers_ segment use Internet technologies quite frequently, but not on a daily basis. Digital technologies are used to get in contact with the Public Administration, although not as frequently as _Advanced Users_. The main difference between this group and _Advanced Users_ relates to the fact that _Followers_ do not use Internet technologies for purchasing purposes.

Segment 5: Non-Internet users (44%)

It is important to emphasize the high share of _Non-Internet Users_ among European citizens. Private companies and public authorities should account for the diverse factors related to the use of Internet technologies, in order to increase the Internet usage rates in the European Union.

### Validation of the identified segments

Once the segments have been identified, the results need to be validated using the following types of variables: (1) Behavioural variables used in the segmentation process (internal validation); and (2) socio-demographic variables, not considered for the identification of segments (external validation).

#### Internal validation

In order to perform the internal validation of segments, Discriminant Analysis (stepwise estimation method) has been applied. The identified groups have been included in the analysis as the dependent variable, while the behavioural indicators used to form the groups have been included as independent variables. Five discriminant functions have been found, which classify 89.2% of cases correctly (see Tables 4 and 5). These results suggest that the identified cluster model is very suitable for predictive purposes. Sorted from higher to lower discriminant capacity, Table 6 shows all the variables included in the stepwise discriminant analysis.

<table><caption>

**Table 4: Coefficients of classification functions for each segment**</caption>

<tbody>

<tr>

<td rowspan="2"> </td>

<td colspan="5">

**Cluster Number**</td>

</tr>

<tr>

<th>1</th>

<th>2</th>

<th>3</th>

<th>4</th>

<th>5</th>

</tr>

<tr>

<td>Q6AR - Online contacts with a Public Administration to find administrative information</td>

<td>15.712</td>

<td>15.932</td>

<td>15.875</td>

<td>15.993</td>

<td>0.000</td>

</tr>

<tr>

<td>Q6BR - Online contacts with a Public Administration to send them an e-mail</td>

<td>5.240</td>

<td>5.397</td>

<td>5.478</td>

<td>5.410</td>

<td>0.000</td>

</tr>

<tr>

<td>Q6CR - Online contacts with a Public Administration to fill in forms or carry out procedures online</td>

<td>7.684</td>

<td>7.849</td>

<td>7.948</td>

<td>8.013</td>

<td>0.000</td>

</tr>

<tr>

<td>Q6DR - Online contacts with a Public Administration for other reasons</td>

<td>-3.376</td>

<td>-0.539</td>

<td>9.424</td>

<td>-4.041</td>

<td>0.000</td>

</tr>

<tr>

<td>Q6ER - Never contacted a Public Administration through the Internet</td>

<td>25.333</td>

<td>26.153</td>

<td>22.990</td>

<td>23.855</td>

<td>0.000</td>

</tr>

<tr>

<td>Q6FR - DK/NA about contacts with a Public Administration</td>

<td>31.679</td>

<td>376.852</td>

<td>21.456</td>

<td>29.802</td>

<td>0.000</td>

</tr>

<tr>

<td>Q7BR - Occasionally purchases products or services through the Internet for private use</td>

<td>43.173</td>

<td>43.934</td>

<td>42.859</td>

<td>43.252</td>

<td>0.000</td>

</tr>

<tr>

<td>Q7CR - Rarely purchases products or services through the Internet for private use</td>

<td>45.408</td>

<td>45.794</td>

<td>43.875</td>

<td>45.461</td>

<td>0.000</td>

</tr>

<tr>

<td>Q7DR - _'I purchased, but I will never purchase again'_ products or services through the Internet for private use</td>

<td>77.052</td>

<td>445.224</td>

<td>37.432</td>

<td>76.522</td>

<td>0.000</td>

</tr>

<tr>

<td>Q7ER - Never purchased products or services through the Internet for private use</td>

<td>87.712</td>

<td>79.460</td>

<td>41.811</td>

<td>91.665</td>

<td>0.000</td>

</tr>

<tr>

<td>Q7FR - DK/NA about purchases of products or services through the Internet for private use</td>

<td>74.874</td>

<td>271.248</td>

<td>41.862</td>

<td>78.126</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10AR - Private use of the Internet to send/retrieve e-mail</td>

<td>10.247</td>

<td>13.581</td>

<td>16.403</td>

<td>16.126</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10BR - Private use of the Internet to carry-out online banking operations</td>

<td>1.459</td>

<td>2.123</td>

<td>3.075</td>

<td>2.465</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10CR - Private use of the Internet to look for news or topical items</td>

<td>4.905</td>

<td>4.763</td>

<td>6.867</td>

<td>6.212</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10DR - Private use of the Internet to seek health-related advice or information</td>

<td>0.581</td>

<td>0.629</td>

<td>0.836</td>

<td>0.835</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10ER - Private use of the Internet to find job ads</td>

<td>0.263</td>

<td>0.444</td>

<td>1.160</td>

<td>0.590</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10FR - Private use of the Internet to take part in forums or group discussions (_chats_)</td>

<td>0.657</td>

<td>0.928</td>

<td>1.538</td>

<td>1.496</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10GR - Private use of the Internet to improve your training or education</td>

<td>0.822</td>

<td>0.731</td>

<td>1.437</td>

<td>1.378</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10HR - Private use of the Internet to seek information on travels, plane tickets, etc.</td>

<td>3.189</td>

<td>2.790</td>

<td>3.788</td>

<td>3.961</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10IR - Private use of the Internet to book tickets for shows or events</td>

<td>1.047</td>

<td>0.973</td>

<td>1.763</td>

<td>1.401</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10JR - Use of the Internet for other private use</td>

<td>-5.825</td>

<td>-3.278</td>

<td>6.367</td>

<td>-7.702</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10KR - Do not use the Internet for private use</td>

<td>17.042</td>

<td>24.196</td>

<td>17.576</td>

<td>15.353</td>

<td>0.000</td>

</tr>

<tr>

<td>Q10LR - DK/NA about private use of the Internet</td>

<td>9.319</td>

<td>284.180</td>

<td>21.839</td>

<td>14.172</td>

<td>0.000</td>

</tr>

<tr>

<td>(Constant)</td>

<td>-61.438</td>

<td>-224.293</td>

<td>-46.355</td>

<td>-70.864</td>

<td>-1.609</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 5: Classification matrix of original and predicted segments**</caption>

<tbody>

<tr>

<td rowspan="2"> </td>

<th colspan="7">Predicted Group</th>

</tr>

<tr>

<th>Cluster Number</th>

<th>1</th>

<th>2</th>

<th>3</th>

<th>4</th>

<th>5</th>

<th>Total</th>

</tr>

<tr>

<td rowspan="5">

**Original Group**  

Cluster Number</td>

<td>1</td>

<td>2585</td>

<td>0</td>

<td>277</td>

<td>1826</td>

<td>2</td>

<td>4690</td>

</tr>

<tr>

<td>2</td>

<td>27</td>

<td>571</td>

<td>7</td>

<td>9</td>

<td>0</td>

<td>614</td>

</tr>

<tr>

<td>3</td>

<td>103</td>

<td>0</td>

<td>5382</td>

<td>219</td>

<td>164</td>

<td>5868</td>

</tr>

<tr>

<td>4</td>

<td>643</td>

<td>0</td>

<td>0</td>

<td>5185</td>

<td>0</td>

<td>5828</td>

</tr>

<tr>

<td>5</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>13336</td>

<td>13336</td>

</tr>

<tr>

<td rowspan="5">

%</td>

<td>1</td>

<td>55.12%</td>

<td>0%</td>

<td>5.91%</td>

<td>38.93%</td>

<td>0.04%</td>

<td>100%</td>

</tr>

<tr>

<td>2</td>

<td>4.40%</td>

<td>93.00%</td>

<td>1.14%</td>

<td>1.47%</td>

<td>0%</td>

<td>100%</td>

</tr>

<tr>

<td>3</td>

<td>1.76%</td>

<td>0%</td>

<td>91.72%</td>

<td>3.73%</td>

<td>2.79%</td>

<td>100%</td>

</tr>

<tr>

<td>4</td>

<td>11.03%</td>

<td>0%</td>

<td>0%</td>

<td>88.97%</td>

<td>0%</td>

<td>100%</td>

</tr>

<tr>

<td>5</td>

<td>0%</td>

<td>0%</td>

<td>0%</td>

<td>0%</td>

<td>100%</td>

<td>100%</td>

</tr>

<tr>

<td rowspan="5">

**Cross Validation**  

Cluster Number</td>

<td>1</td>

<td>2585</td>

<td>0</td>

<td>277</td>

<td>1826</td>

<td>2</td>

<td>4690</td>

</tr>

<tr>

<td>2</td>

<td>27</td>

<td>571</td>

<td>7</td>

<td>9</td>

<td>0</td>

<td>614</td>

</tr>

<tr>

<td>3</td>

<td>103</td>

<td>0</td>

<td>5381</td>

<td>219</td>

<td>165</td>

<td>5868</td>

</tr>

<tr>

<td>4</td>

<td>643</td>

<td>0</td>

<td>0</td>

<td>5185</td>

<td>0</td>

<td>5828</td>

</tr>

<tr>

<td>5</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>13336</td>

<td>13336</td>

</tr>

<tr>

<td rowspan="5">

%</td>

<td>1</td>

<td>55.12%</td>

<td>0%</td>

<td>5.91%</td>

<td>38.93%</td>

<td>0.04%</td>

<td>100%</td>

</tr>

<tr>

<td>2</td>

<td>4.40%</td>

<td>93.00%</td>

<td>1.14%</td>

<td>1.47%</td>

<td>0%</td>

<td>100%</td>

</tr>

<tr>

<td>3</td>

<td>1.76%</td>

<td>0%</td>

<td>91.70%</td>

<td>3.73%</td>

<td>2.81%</td>

<td>100%</td>

</tr>

<tr>

<td>4</td>

<td>11.03%</td>

<td>0%</td>

<td>0%</td>

<td>88.97%</td>

<td>0%</td>

<td>100%</td>

</tr>

<tr>

<td>5</td>

<td>0%</td>

<td>0%</td>

<td>0%</td>

<td>0%</td>

<td>100%</td>

<td>100%</td>

</tr>

<tr>

<td colspan="8">

(1) Cross Validation only applies to the cases of the analysis. In this case, each case is classified based on the functions derived from the rest of cases.</td>

</tr>

<tr>

<td colspan="8">

(2) 89.2% of cases have been correctly classified.</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 6: Stepwise discriminant results**</caption>

<tbody>

<tr>

<th>Variables</th>

<th>Tolerance</th>

<th>F to delete</th>

<th>Wilks' Lambda</th>

</tr>

<tr>

<td>Q7ER - Never purchased products or services through the Internet for private use</td>

<td>0.459</td>

<td>23464.231</td>

<td>0.00252693</td>

</tr>

<tr>

<td>Q10AR - Private use of the Internet to send/retrieve e-mail</td>

<td>0.865</td>

<td>2166.419</td>

<td>0.00079318</td>

</tr>

<tr>

<td>Q6FR - DK/NA about contacts with a Public Administration</td>

<td>0.447</td>

<td>20486.357</td>

<td>0.00228451</td>

</tr>

<tr>

<td>Q7DR - "I purchased, but I will never purchase again" products or services through the Internet for private use</td>

<td>0.398</td>

<td>16201.586</td>

<td>0.00193571</td>

</tr>

<tr>

<td>Q10LR - DK/NA about private use of the Internet</td>

<td>0.716</td>

<td>3917.686</td>

<td>0.00093574</td>

</tr>

<tr>

<td>Q10CR - Private use of the Internet to look for news or topical items</td>

<td>0.857</td>

<td>357.472</td>

<td>0.00064592</td>

</tr>

<tr>

<td>Q7FR - DK/NA about purchases of products or services through the Internet for private use</td>

<td>0.813</td>

<td>1918.008</td>

<td>0.00077296</td>

</tr>

<tr>

<td>Q7CR - Rarely purchases products or services through the Internet for private use</td>

<td>0.344</td>

<td>5018.137</td>

<td>0.00102533</td>

</tr>

<tr>

<td>Q7BR - Occasionally purchases products or services through the Internet for private use</td>

<td>0.364</td>

<td>4664.862</td>

<td>0.00099657</td>

</tr>

<tr>

<td>Q10JR - Use of the Internet for other private use</td>

<td>0.899</td>

<td>918.348</td>

<td>0.00069158</td>

</tr>

<tr>

<td>Q6ER - Never contacted a Public Administration through the Internet</td>

<td>0.301</td>

<td>2411.843</td>

<td>0.00081316</td>

</tr>

<tr>

<td>Q6AR - Online contacts with a Public Administration to find administrative information</td>

<td>0.416</td>

<td>1202.290</td>

<td>0.0007147</td>

</tr>

<tr>

<td>Q6CR - Online contacts with a Public Administration to fill in forms or carry out procedures online</td>

<td>0.623</td>

<td>337.060</td>

<td>0.00064426</td>

</tr>

<tr>

<td>Q10KR - Do not use the Internet for private use</td>

<td>0.832</td>

<td>441.566</td>

<td>0.00065277</td>

</tr>

<tr>

<td>Q6DR - Online contacts with a Public Administration for other reasons</td>

<td>0.940</td>

<td>239.458</td>

<td>0.00063632</td>

</tr>

<tr>

<td>Q10HR - Private use of the Internet to seek information on travels, plane tickets, etc.</td>

<td>0.839</td>

<td>121.367</td>

<td>0.0006267</td>

</tr>

<tr>

<td>Q6BR - Online contacts with a Public Administration to send them an e-mail</td>

<td>0.677</td>

<td>168.739</td>

<td>0.00063056</td>

</tr>

<tr>

<td>Q10BR - Private use of the Internet to carry-out online banking operations</td>

<td>0.918</td>

<td>117.150</td>

<td>0.00062636</td>

</tr>

<tr>

<td>Q10FR - Private use of the Internet to take part in forums or group discussions (_chats_)</td>

<td>0.940</td>

<td>38.751</td>

<td>0.00061998</td>

</tr>

<tr>

<td>Q10GR - Private use of the Internet to improve your training or education</td>

<td>0.920</td>

<td>31.500</td>

<td>0.00061939</td>

</tr>

<tr>

<td>Q10IR - Private use of the Internet to book tickets for shows or events</td>

<td>0.894</td>

<td>26.914</td>

<td>0.00061902</td>

</tr>

<tr>

<td>Q10ER - Private use of the Internet to find job ads</td>

<td>0.931</td>

<td>19.366</td>

<td>0.0006184</td>

</tr>

<tr>

<td>Q10DR - Private use of the Internet to seek health-related advice or information</td>

<td>0.905</td>

<td>8.181</td>

<td>0.00061749</td>

</tr>

</tbody>

</table>

#### External validation

Correspondence Analysis has been applied in order to perform the external validation of the identified segments. The following conclusions can be extracted with regard to the demographic characteristics of the Internet user groups:

_Sex_. The results of the correspondence analysis suggest that sex is still a useful variable to differentiate between more and less advanced Internet users. A higher share of men can be found in the _Advanced Users_ group, whereas a higher relative number of women are included in the _Non-Internet Users_ segment. As a result, _Hypothesis 2 is supported_: sex provides significant differences between the identified Internet user segments: more frequent and innovative Internet uses are found among male respondents.

_Age_. Very clear age trends can be identified with regard to Internet usage: older respondents report using the Internet less frequently than younger people. These results are consistent with several recent investigations suggesting the existence of age differences in Internet use.

Respondents between 20 and 29 years old can be regarded as the most advanced Internet users, followed by people between 30 and 39 years old. However, there is a significant share of _Non-Internet Users_ among people in their thirties. Concerning the sophistication of Internet uses, it should also be noted that youngest respondents (teenagers under 19 years old) are not among the most advanced Internet users, accounting for a high share of the _Laggards_ and _Followers_ segments.

According to these results, _Hypothesis 3 is supported_: Internet user segments show significant differences based on age. Middle-aged respondents show the most frequent and innovative Internet uses among European citizens.

_Education_. European citizens' education also marks clear differences between segments of more and less innovative Internet users. While most Advanced Users hold college degrees, the majority of people without finished formal studies can be currently characterized as Non-Internet Users.

_Hypothesis 4 is supported_: Internet user segments differ greatly on the basis of respondents' education levels. More frequent and innovative Internet uses are observed among highly-educated citizens.

_Professional activity_. Consistent with the results based on respondents' education, Europeans show highly differentiated Internet usage patterns according to their occupation. A high number of Non-Internet Users can be found among unemployed people, which point to the existence of both financial and knowledge entry barriers in the use of Internet services. On the other hand, the highest percentage of Advanced Users has been found among service workers (white-collar employees).

These results _confirm Hypothesis 5_: Internet usage patterns show significant differences based on Europeans' professional activity. Service workers show the most frequent and innovative Internet uses.

_Location_. The results of this study suggest that location is also related to the frequency and innovativeness of Internet uses. Most advanced Internet users are usually located in metropolitan areas, whereas a higher share of _Non-Internet Users_ can be found in rural surroundings. The importance of respondents living in smaller cities is also relatively higher among the _Laggards_ and _Confused/Adverse_ segments. These results confirm to a certain extent the concerns expressed in previous studies and reports related to the risk that location becomes a source for the development and widening of a _digital divide_ between metropolitan and rural citizens ([Bell _et al._ 2004](#bel99); [Kehoe _et al._ 1999](#keh99)).

_Hypothesis 6 is also supported_: The identified Internet user groups will show significant differences based on location. More frequent and innovative Internet users are usually located in metropolitan areas.

### Demographic characterization of segments

According to the demographic patterns identified in the Correspondence Analyses, the identified groups of European Internet users show the following demographic profiles:

Segment 1: Laggards

With respect to age, the relative number of respondents under 19 years of age is quite high in the _Laggards_ segment (29.7% of under-19-year olds belong to this segment). _Laggards_ tend to be manual (blue-collar) workers, with lower to middle education levels. These respondents are more likely to live in small cities and towns.

Segment 2: Confused or Adverse

The _Confused or Adverse_ segment involves older people, with a high relative importance of citizens between 50-59 years old. These people are usually employed in non-manual jobs (white-collar workers) and are characterized by average education levels. They are also most likely to be found in smaller cities.

Segment 3: Advanced users

The _Advanced Users_ segment is mostly composed of younger people, with respondents between 20-29 and 30-39 years old accounting for 52.7% of the segment. This segment is characterized by a greater presence of men (60%), non-manual workers (white collars), students and people with higher levels of university education. European citizens showing more advanced Internet usage patterns are mostly located in metropolitan areas.

Segment 4: Followers

The _Followers_ segment is also mainly formed by young people (20-39 years old). Unlike in the _Advanced Users_ segment, there is a high relative importance of teenagers (35.5% of under-19-year-olds belong to this segment). _Followers_ can also be characterized by self-employed people, with average levels of university education and living in metropolitan areas.

Segment 5: Non-Internet users

European citizens not currently using Internet technologies are mainly people over 50 years old. The percentage of respondents not using the Internet increases with age. It should also be noted that 30.3% of people between 30 and 39 years old and 35.3% of people between 40 and 49 years old belong to the _Non-Internet Users_ group. The _Non-Internet Users_ segment is characterized by a higher share of women (60%), unemployed people and people without finished formal studies (87%) and with basic education levels. _Non-Internet Users_ are to a high extent composed of citizens located in rural areas.

Country differences

The segmentation of European citizens has been carried out without consideration of the countries where surveys were conducted ([Steenkamp and Hofstede 2002](#ste02)). Nevertheless, it was hypothesized that the relative distribution of the identified segments in European countries would show significant differences. Figure 1 offers a graphical representation of the segments' relative weight in each European country and points to the existence of clear country trends with regard to Internet usage in Europe:

Backward countries. These countries are characterized by a high share of _Non-Internet Users_. The number of _Advanced Users_ and _Followers_ is still relatively low in comparison with other European countries. Greece, Portugal, Spain and Italy are in a backward situation with regard to Internet use.

Countries in average situation. European countries with an intermediate development of Internet usage are characterized by a moderate number of _Advanced Users_ and a high number of _Non-Internet Users_. The following countries have been classified in this group: Germany (39% of Non-Internet Users and 27.6% of Advanced Users), Austria (Non-Internet Users, 37.8%; Advanced Users, 19.1%), Ireland (Non-Internet Users, 43.25%; Advanced Users, 16.1%), France (Non-Internet Users, 50.6%; Advanced Users, 15%) and Belgium (Non-Internet Users, 53.6%; Advanced Users, 9%).

Advanced countries. Countries with more advanced Internet usage patterns are characterized by a low share of _Non-Internet Users_ and a very high number of _Advanced Users_. The United Kingdom is the leading EU-country with regard to Internet use (13.24% of Non-Internet Users and 53.4% of Advanced Users). Other advanced countries are Denmark (Non-Internet Users, 26.1%; Advanced Users 28.2%), Holland (Non-Internet Users, 29.2%; Advanced Users, 24.7%), Sweden (Non-Internet Users, 27.8%, Advanced Users, 31.7%), Luxembourg (Non-Internet Users, 34.7%; Advanced Users, 24%) and Finland (Non-Internet Users, 21.2; Advanced Users, 36.6%).

The analysis of the relative distribution of the identified segments and Internet usage patterns in each country (see Table 7 and Figure 1) confirms the existence of a North/South divide in the European Union, with regard to the frequency and innovativeness of Internet uses. These results provide _support for Hypothesis 7_, confirming more frequent and innovative Internet uses in Northern European countries. Besides the lower rates of Internet use in Southern European countries, the data show significant differences in the purposes of Internet use among Northern and Southern European respondents. Internet technologies are mostly used for communication purposes in Southern European countries, while Northern countries they are more attractive for both communications and commercial transactions.

<table><caption>  


**Table 7: Country differences**</caption>

<tbody>

<tr>

<td></td>

<td colspan="4">

**Southern Europe**</td>

<td colspan="6">

**Central Europe**</td>

<td colspan="5">

**Northern Europe**</td>

</tr>

<tr>

<td>

**Countries**</td>

<td>Greece</td>

<td>Portugal</td>

<td>Spain</td>

<td>Italy</td>

<td>Germany</td>

<td>Belgium</td>

<td>Austria</td>

<td>France</td>

<td>Luxem-bourg</td>

<td>Holland</td>

<td>Ireland</td>

<td>United Kingdom</td>

<td>Denmark</td>

<td>Finland</td>

<td>Sweden</td>

</tr>

<tr>

<td>

**Sophistication of Internet uses**</td>

<td>Low</td>

<td>Low</td>

<td>Low</td>

<td>Low</td>

<td>Middle</td>

<td>Middle</td>

<td>Middle</td>

<td>Middle</td>

<td>High</td>

<td>High</td>

<td>Middle</td>

<td>High</td>

<td>High</td>

<td>High</td>

<td>High</td>

</tr>

<tr>

<td colspan="16">

**Relative distribution of identified clusters (%)**</td>

</tr>

<tr>

<td>Advanced Users</td>

<td>1.60</td>

<td>4.10  
</td>

<td>5.69  
</td>

<td>7.25  
</td>

<td>27.60  
</td>

<td>9.05  
</td>

<td>19.15  
</td>

<td>15.03  
</td>

<td>24.05  
</td>

<td>24.68  
</td>

<td>16.15  
</td>

<td>53.14  
</td>

<td>28.24  
</td>

<td>21.25  
</td>

<td>27.85</td>

</tr>

<tr>

<td>Followers</td>

<td>5.40</td>

<td>15.55  
</td>

<td>19.15  
</td>

<td>20.49  
</td>

<td>13.40  
</td>

<td>17.60  
</td>

<td>24.70  
</td>

<td>15.03  
</td>

<td>21.60  
</td>

<td>29.86  
</td>

<td>18.70  
</td>

<td>13.24  
</td>

<td>26.80  
</td>

<td>23.80  
</td>

<td>23.70</td>

</tr>

<tr>

<td>Laggards</td>

<td>9.85</td>

<td>11.10  
</td>

<td>13.52</td>

<td>13.64  
</td>

<td>17.65  
</td>

<td>16.74  
</td>

<td>13.05  
</td>

<td>18.42  
</td>

<td>17.70  
</td>

<td>14.46  
</td>

<td>20.95  
</td>

<td>16.57  
</td>

<td>16.18  
</td>

<td>17.15  
</td>

<td>14.75</td>

</tr>

<tr>

<td>Confused / Adverse</td>

<td>1.35</td>

<td>1.40  
</td>

<td>0.90  
</td>

<td>0.70  
</td>

<td>2.35  
</td>

<td>3.02  
</td>

<td>5.30  
</td>

<td>0.90  
</td>

<td>1.90  
</td>

<td>1.74  
</td>

<td>0.95  
</td>

<td>3.76  
</td>

<td>2.68</td>

<td>1.15  
</td>

<td>2.00</td>

</tr>

<tr>

<td>Non-Internet Users</td>

<td>81.80</td>

<td>67.85  
</td>

<td>60.75  
</td>

<td>57.92  
</td>

<td>39.00  
</td>

<td>53.59  
</td>

<td>37.80  
</td>

<td>50.62  
</td>

<td>34.75  
</td>

<td>29.26  
</td>

<td>43.25  
</td>

<td>13.28  
</td>

<td>26.10  
</td>

<td>36.65  
</td>

<td>31.70</td>

</tr>

<tr>

<td colspan="16">

**Internet usage patterns**</td>

</tr>

<tr>

<td>Online shopping</td>

<td>0.57</td>

<td>1.44</td>

<td>1.95</td>

<td>2.49</td>

<td>9.84</td>

<td>3.16</td>

<td>7.10</td>

<td>5.31</td>

<td>8.56</td>

<td>8.30</td>

<td>5.95</td>

<td>19.33</td>

<td>9.41</td>

<td>6.82</td>

<td>9.74</td>

</tr>

<tr>

<td>Online contacts with the Public Administration</td>

<td>1.40</td>

<td>2.78</td>

<td>4.14</td>

<td>4.20</td>

<td>7.45</td>

<td>5.58</td>

<td>6.21</td>

<td>6.77</td>

<td>7.60</td>

<td>8.92</td>

<td>4.63</td>

<td>10.76</td>

<td>11.15</td>

<td>6.17</td>

<td>12.25</td>

</tr>

<tr>

<td>Internet use to send/retrieve e-mail</td>

<td>12.4</td>

<td>24.7</td>

<td>29.5</td>

<td>32.0</td>

<td>47.6</td>

<td>34.6</td>

<td>54.1</td>

<td>37.1</td>

<td>52.9</td>

<td>64.8</td>

<td>46.7</td>

<td>73.7</td>

<td>63.7</td>

<td>51.4</td>

<td>58.5</td>

</tr>

<tr>

<td>Online banking operations</td>

<td>1.3</td>

<td>6.9</td>

<td>7.9</td>

<td>6.7</td>

<td>21.3</td>

<td>14.3</td>

<td>22.7</td>

<td>15.1</td>

<td>14.2</td>

<td>33.4</td>

<td>13.7</td>

<td>28.5</td>

<td>37.2</td>

<td>42.9</td>

<td>35.1</td>

</tr>

<tr>

<td>Internet use to look for news or topical items</td>

<td>6.5</td>

<td>24.5</td>

<td>34.7</td>

<td>34.9</td>

<td>46.2</td>

<td>28.3</td>

<td>51.6</td>

<td>32.6</td>

<td>47.4</td>

<td>54.3</td>

<td>41.3</td>

<td>59.1</td>

<td>60.4</td>

<td>41.6</td>

<td>52.4</td>

</tr>

<tr>

<td>Internet use to seek health-related advice or information</td>

<td>3.0</td>

<td>9.7</td>

<td>12.5</td>

<td>19.9</td>

<td>26.4</td>

<td>16.7</td>

<td>20.8</td>

<td>13.1</td>

<td>21.3</td>

<td>39.0</td>

<td>27.2</td>

<td>39.7</td>

<td>29.2</td>

<td>23.8</td>

<td>21.0</td>

</tr>

<tr>

<td>Internet use to find job ads</td>

<td>2.3</td>

<td>5.1</td>

<td>10.1</td>

<td>9.9</td>

<td>21.6</td>

<td>11.6</td>

<td>12.8</td>

<td>14.7</td>

<td>8.2</td>

<td>25.5</td>

<td>17.3</td>

<td>30.5</td>

<td>26.0</td>

<td>24.2</td>

<td>27.8</td>

</tr>

<tr>

<td>

Internet use to take part in forums or group discussions (_chats_)</td>

<td>2.9</td>

<td>8.8</td>

<td>16.8</td>

<td>8.8</td>

<td>13.8</td>

<td>11.1</td>

<td>13.5</td>

<td>11.5</td>

<td>12.3</td>

<td>18.4</td>

<td>7.4</td>

<td>15.1</td>

<td>12.9</td>

<td>15.3</td>

<td>12.8</td>

</tr>

<tr>

<td>Internet use to improve your training or education</td>

<td>7.7</td>

<td>18.7</td>

<td>21.5</td>

<td>24.9</td>

<td>34.0</td>

<td>17.3</td>

<td>28.4</td>

<td>19.9</td>

<td>19.0</td>

<td>24.1</td>

<td>22.7</td>

<td>37.4</td>

<td>18.3</td>

<td>29.9</td>

<td>13.7</td>

</tr>

<tr>

<td>Internet use to seek information on travels, plane tickets, etc.</td>

<td>4.4</td>

<td>12.9</td>

<td>20.4</td>

<td>25.2</td>

<td>42.0</td>

<td>26.4</td>

<td>29.7</td>

<td>28.5</td>

<td>36.1</td>

<td>52.6</td>

<td>44.1</td>

<td>66.1</td>

<td>48.1</td>

<td>41.5</td>

<td>49.8</td>

</tr>

<tr>

<td>Internet use to book tickets for shows or events</td>

<td>0.9</td>

<td>4.8</td>

<td>8.1</td>

<td>8.3</td>

<td>19.3</td>

<td>12.5</td>

<td>13.2</td>

<td>11.5</td>

<td>15.9</td>

<td>24.6</td>

<td>23.3</td>

<td>33.0</td>

<td>27.5</td>

<td>15.2</td>

<td>30.8</td>

</tr>

</tbody>

</table>

<figure>

![Figure 1](../p302fig1.jpg)

<figcaption>

**Figure 1: Distribution of Internet user segments in European countries**</figcaption>

</figure>

## Conclusions

As expected, different segments of European Internet users have been identified on the basis of behavioural variables related to the purposes, frequency and innovativeness of Internet uses. Five groups have been differentiated with regard to respondents' Internet usage patterns: _Laggards_, _Confused or Adverse_, _Advanced Users_, _Followers_ and _Non-Internet Users_.

The segments have also been characterized based on external variables, such as country, sex, age, occupation, education and location. Significant differences between groups of European Internet users have been identified for sex, age, occupation, education and location. These results have confirmed all hypotheses related to the demographic characterization of European Internet users.

This study expected to find significant country differences in Internet usage in the European Union. Authors anticipating a fast generalization of Internet technologies in Europe will find that only Northern EU-countries are in an outpost situation, whereas Southern European countries still lag far behind. Therefore, the results have also provided support for the hypothesis related to the existence of a North/South divide in Europeans' Internet use. There are also differences in the role played by the Internet channel in Northern and Southern European countries. Internet technologies are mostly used for communication purposes in Southern European countries, while Northern countries are more attractive both for communications and commercial transactions.

The results confirm previous research into the existence clear age differences in Internet usage ([Zeithaml and Gilly 1987](#zei87)). Older people report to use Internet services less frequently than younger people. Respondents in ages between 20 and 39 years old show the most frequent and advanced uses of Internet technologies. However, the higher importance of younger people among _Advanced Users_ and lower Internet usage rates among older respondents could be regarded as a transitory situation, due to relatively recent introduction of Internet technologies in the European market. It should also be noted that teenagers (respondents under 19 years old) are not among the most advanced user segments. Two arguments could account for this behaviour: teenagers' lack of economic independence prevents them to use the Internet for purchasing purposes or contacts with the Public Administration; in addition, due to the high usage rates of mobile phones among teenagers, the cheap means of communication provided by fix Internet services (e.g., e-mail and instant messaging services) may be replaced by mobile communications (e.g., through SMS messages).

The characterization of Internet user segments also provides clear differences with regard to sex, occupation, education and location. Based on these variables, people showing more advanced and frequent Internet uses can be characterized as men, service workers, highly educated and living in metropolitan areas. On the other hand, a higher share of women, unemployed, illiterate and rural citizens is found among less advanced Internet-usage groups.

This research confirms the existence of diverse entry barriers to the use of digital services, arising from country, age, financial, educational and location-related factors. Such entry barriers are contributing to the development and widening of a _digital divide_ among citizens of the European Union. Both private companies and public authorities should account for these factors, in order to reduce the number of European citizens currently not using Internet technologies (44%).

## Implications

Internet skills are becoming a new type of general skill, which is increasingly needed to perform certain daily activities ([OECD 2002](#oec02)). Thus, governments are implementing an array of policies targeting different segments of the population in order to promote basic and advanced Internet skills. In this regard, the behavioural and socio-demographic characterization of Internet usage patterns should be valuable both to companies and public authorities, in order to develop the suitable Internet promotion strategies targeted at each group of users and non users of Internet services.

While the patterns of Internet uptake have been similar everywhere, usage levels differ significantly across countries and regions (e.g., wide national differences exist in the share of online shoppers). Differences in access to Internet technologies and personal computers give rise to a _digital divide_ between individuals able to benefit from the opportunities provided by Internet technologies and those who cannot. Additionally, as access issues are progressively reduced, social exclusion challenges are expected to arise from more complex _use divide issues_ ([OECD 2004](#oec04)). Such social differences may appear between skilled and non-skilled individuals to use the Internet for certain purposes (e.g., relevant Internet uses like e-learning and online job searching).

_Digital divide_ issues should be regarded as main challenges in the diffusion of Internet technologies ([Room _et al._ 2004](#roo04)). According to OECD reports, technology diffusion to individuals, IT education and training initiatives and the promotion of trust online are key policy areas for governments and private firms with regard to social inclusion issues ([OECD 2002](#oec02), [2004](#oec04)).

The segments identified in the present study could be very useful for European public authorities, where government initiatives, through technological training and promotion, are relatively more relevant in bridging the _digital divide_ between users and non-users of Internet services. While public support is a characteristic of most countries in the promotion of Internet technologies, significant national differences in the extent of digital divide require different strategies to overcome the problems raised by social exclusion in Internet use.

Concerning Europeans' use of the Internet for commercial purposes, this paper has shown that the use of online shopping services is limited to one single segment (Advanced Users). The remaining segments are only willing to use the Internet for less-risky purposes (e.g., personal communications through e-mail and chats) and, in the _Followers_ segment, for contacts with the Public Administration. Consumers may associate a smaller risk with e-Government services than with online shopping services. Therefore, e-Government services are likely to be a suitable way to introduce consumers into online shopping services, since the experience with e-Government services can help people feel confident to undertake e-commerce operations. It is important for European companies that the number of online shoppers reaches threshold levels in different countries, in order to increase the suitability of the Internet as a domestic and international distribution and purchasing channel.

With regard to _Non-Internet Users_ and _Laggards_, governments should facilitate both training and easier of access to Internet technologies. These segments, which are composed mainly of people with little training, employed in basic jobs and located in rural or less-favoured areas, may be excluded from the huge advantages provided by the Internet. Unless these policies are implemented, Internet services may become a new source of social exclusion.

Governments should foster Internet education and training to face complex social exclusion challenges, mainly arising from access and use digital divides. General and specific policies may be needed to target socio-economic groups currently lagging in the use of Internet services.

The characterization of Internet user segments also provides clear differences with regard to sex, occupation, education and location. Based on these variables, people showing more advanced and frequent Internet uses can be characterized as men, service workers, highly educated and living in metropolitan areas. On the other hand, a higher share of women, unemployed, illiterate and rural citizens is found among less advanced Internet-usage groups.

## Limitations and future research

Future research could overcome some of the main limitations of this study and further clarify how Internet adoption is evolving among European citizens:

The use of secondary data involves several limitations, mainly related to the lack of flexibility in the measures to include in the study. Nevertheless, this lack of flexibility in the study design stage is clearly balanced by the access to representative and comparative transnational survey data in all countries of the European Union.

Further research should check if demographic and geographic differences continue to exist between more and less advanced Internet-usage groups in the future. Among other topics, future studies should clarify if the age characterization of current Internet users continues to exist.

Two additional lines of investigation should be approached in the future. It should be analysed if country differences exist in the demographic characterization of Internet-usage segments. Additionally, the existence of potential differences on the regional level should also be examined. The relevance of such factors as the degree of economic development and location (differences between metropolitan and rural areas) in the configuration of segments also suggests the existence of regional trends in Internet usage.

## Acknowledgements

The authors would like to thank the Central Archive for Empirical Social Research (University of Cologne, Germany) for their support and providing access to the database used in this study.

## References

*   <a id="alb97"></a>Alba, J., Lynch, J., Weitz, B., Janiszewski, C., Lutz, R., Sawyer, A. & Wood, S. (1997). Interactive home shopping: consumer, retailer and manufacturer incentives to participate in electronic marketplaces. _Journal of Marketing_, **61**(3), 38-53.
*   <a id="atk98"></a>Atkin, D.J., Jeffres, L.W. & Neuendorf, K.A. (1998). Understanding internet adoption as telecommunications behaviour. _Journal of Broadcasting & Electronic Media_, **42**(4), 475-490.
*   <a id="bai99"></a>Bain, M.G. (1999). _Business to consumer eCommerce: an investigation of factors related to consumer adoption of the Internet as a purchase channel_ . London: Centre for Marketing.
*   <a id="bel04"></a>Bell, P., Reddy, P. & Rainie, L. (2004). _[Rural areas and the Internet](http://www.webcitation.org/5LE2okQ13)_. Washington, DC: Pew Internet & American Life Project. Retrieved 18 December, 2006 from http://www.pewinternet.org/pdfs/PIP_Rural_Report.pdf
*   <a id="bat04"></a>Bhatnagar, A. & Ghose, S. (2004). A latent class segmentation analysis of e-shoppers. _Journal of Business Research_, **57**(7), 758-767.
*   <a id="bie99"></a>Biernacki, C. & Govaert, G. (1999). Choosing models in model-based clustering and discriminant analysis. _Journal of Statistical Computation and Simulation_, **64**(1), 49-71.
*   <a id="cia06"></a>Central Intelligence Agency. (2006). [_The World Factbook._](https://www.cia.gov/cia/publications/factbook/index.html) Retrieved 13 September, 2006 from the CIA Web Site: https://www.cia.gov/cia/publications/factbook/index.html
*   <a id="chi01"></a>Chiu, T., Fang, D., Chen, J., Wang, Y. & Jeris, C. (2001). A robust and scalable clustering algorithm for mixed type attributes in large database environment. In D. Lee, M. Schkolnick, F. Provost & R. Srikant (Eds.), _Proceedings of the Seventh ACM SIGKDD International Conference on Knowledge Discovery and Data Mining._ (pp. 263-268). New York: ACM Press.
*   <a id="cro02"></a>Crosby, L.A. & Johnson, S.L. (2002). The globalization of relationship marketing. _Marketing Management_, **11**(2), 10-11.
*   <a id="dav89"></a>Davis, F.D., Bagozzi, R.P. & Warshaw, P.R. (1989). User acceptance of computer-technology - a comparison of 2 theoretical models. _Management Science_, **35**(8), 982-1003.
*   <a id="dic83"></a>Dickerson, M.D. & Gentry, J.W. (1983). Characteristics of adopters and non-adopters of home computers. _Journal of Consumer Research_, **10**(2), 225-235.
*   <a id="dic00"></a>Dickson, P.R. (2000). Understanding the trade winds: the global evolution of production, consumption and the Internet. _Journal of Consumer Research_, **27**(1), 115-122.
*   <a id="gui02"></a>Guillén, M.F. (2002). What is the best global strategy for the Internet? _Business Horizons_, **45**(3), 39-46.
*   <a id="hof95"></a>Hoffman, D.L., Novak, T.P. & Chatterjee, P. (1995). [Commercial scenarios for the Web: opportunities and challenges](http://www.webcitation.org/5LE38bjA9). _Journal of Computer-Mediated Communication_, **1**(3). Retrieved 18 December, 2006 from http://jcmc.indiana.edu/vol1/issue3/hoffman.html
*   <a id="hof96"></a>Hoffman, D.L. & Novak, T.P. (1996). Marketing in hypermedia computer-mediated environments: conceptual foundations. _Journal of Marketing_, **60**(3), 50-68.
*   <a id="kat97"></a>Katz, J. & Aspden, P. (1997). Motivations for and barriers to Internet usage: results of a national public opinion survey. _Internet Research: Electronic Networking Applications and Policy_, **7**(3), 170-188.
*   <a id="keh99"></a>Kehoe, C., Pitkow, J., Sutton, K., Aggarwal, G. & Rogers, J.D. (1999). [Results of GVU's tenth World Wide Web user survey](http://www.webcitation.org/5LE3PrbMH). Atlanta, GA: Georgia Institute of Technology, GVU-Center. Retrieved 18 December, 2006 from http://www.gvu.gatech.edu/user_surveys/survey-1998-10/tenthreport.html
*   <a id="kor99"></a>Korgaonkar, P.K. & Wolin, L.D. (1999). A multivariate analysis of Web usage. _Journal of Advertising Research_, **39**(2), 53-68.
*   <a id="loi02"></a>Loiacono, E.T., Watson, R.T. & Goodhue, D.L. (2002). WebQual: a measure of WebSite quality. _Proceedings of the AMA Winter Educators' Conference_, **13**, 432-438.
*   <a id="mag04"></a>Magidson, J. & Vermunt, J. (2004). Latent class models. In D. Kaplan (Ed.), _The Sage handbook of quantitative methodology for the social sciences_. (pp. 175-198). Thousand Oaks, CA: Sage Publications.
*   <a id="mor00"></a>Morris, M.G. & Venkatesh, V. (2000). Age differences in technology adoption decisions: implications for a changing work force. _Personnel Psychology_, **53**(2), 375-403.
*   <a id="mut02"></a>Muthitacharoen, A. & Palvia., P. (2002). B2C Internet commerce: a tale of two nations. _Journal of Electronic Commerce Research_, **3**(4), 201-212.
*   <a id="ngi02"></a>Ngini, C.U., Furnell, S.M. & Ghita, B.V. (2002). Assessing the global accessibility of the Internet. _Internet Research: Electronic Networking Applications and Policy_, **12**(4), 329-338.
*   <a id="oec02"></a>Organisation for Economic Co-operation and Development. (2002). _[OECD information technology outlook 2002](http://www.webcitation.org/5LE3xo22r)_. Paris: Organisation for Economic Co-operation and Development. Retrieved 18 December, 2006 from http://www.oecd.org/dataoecd/63/60/1933354.pdf
*   <a id="oec04"></a>Organisation for Economic Co-operation and Development. (2004). _[OECD information technology outlook 2004](http://www.webcitation.org/5LE438mQt)_. Paris: Organisation for Economic Co-operation and Development. Retrieved 13 February 2005 from http://www.oecd.org/dataoecd/20/47/33951035.pdf
*   <a id="pav03"></a>Pavlou, P.A. (2003). Consumer acceptance of electronic commerce: integrating trust and risk with the technology acceptance model. _International Journal of Electronic Commerce_, **7**(3), 101-134.
*   <a id="pet03"></a>Peterson, R.A. & Merino, M.C. (2003). Consumer information search behaviour and the Internet. _Psychology & Marketing_, **20**(2), 99-121.
*   <a id="que96"></a>Quelch, J.A. & Klein, L.R. (1996). International business and the Internet. _Sloan Management Review_, **37**(3), 60-75.
*   <a id="ram90"></a>Ram, S. & Jung, H.-S. (1990). The conceptualization and measurement of product usage. _Journal of the Academy of Marketing Science_, **18**(1), 67-75.
*   <a id="rog83"></a>Rogers, E.M. (2003). _Diffusion of innovations_. (5th ed.). New York, NY: The Free Press.
*   <a id="roo04"></a>Room, G., Gould, N., Winnett, A., Kamm, R., Powell, P., Vidgen, R., _et al._ (2004). _[Final report on conceptualisation and analysis of the new information economy](http://www.webcitation.org/5LE4FfIfz)_. Retrieved 18 April 2006 from http://www.bath.ac.uk/soc-pol/research/nesis/D5.3%2027th%20August%20submitted1.doc?cPath=10135&products_id=1080
*   <a id="sam98"></a>Samiee, S. (1998). Exporting and the Internet: a conceptual perspective. _International Marketing Review_, **15**(5), 413-426.
*   <a id="sta06"></a>Statistics Canada (2006). _[Sampling methods, probability sampling: systematic sampling.](http://www.webcitation.org/5LE4YXPtE)_ Retrieved 17 September 2006 from http://www.statcan.ca/english/edu/power/ch13/probability/probability.htm#sys
*   <a id="ste02"></a>Steenkamp, J.-B.E.M. & Hofstede, F.T. (2002). International market segmentation: issues and perspectives. _International Journal of Research in Marketing_, **19**(3), 185-213.
*   <a id="swi03"></a>Swinyard, W.R. & Smith, S.M. (2003). Why people (don't) shop online: a lifestyle study of the Internet consumer. _Psychology & Marketing_, **20**(7), 567-597.
*   <a id="teo01"></a>Teo, T.S.H. (2001). Demographic and motivation variables associated with Internet usage activities. _Internet Research: Electronic Networking Applications and Policy_, **11**(2), 125-137.
*   <a id="pew06"></a>The Pew Research Center (2006). _[The cell phone challenge to survey research.](http://www.webcitation.org/5LE4hNMR9) Washington, DC: The Pew Research Center. Retrieved 18 December, 2006 from http://people-press.org/reports/pdf/276.pdf_
*   <a id="ven00a"></a>Venkatesh, V. & Davis, F.D. (2000). A theoretical extension of the Technology Acceptance Model: four longitudinal field studies. _Management Science_, **46**(2), 186-204.
*   <a id="ven00b"></a>Venkatesh, V. & Morris, M.G. (2000). Why don't men ever stop to ask for directions? Gender, social influence and their role in technology acceptance and usage behaviour. _MIS Quarterly_, **24**(1), 115-139.
*   <a id="ven00c"></a>Venkatesh, V., Morris, M.G. & Ackerman, P.L. (2000). A longitudinal field investigation of gender differences in individual technology adoption decision-making processes. _Organizational Behavior and Human Decision Processes_, **83**(1), 33-60.
*   <a id="ven02"></a>Venkatesh, V., Speier, C. & Morris, M.G. (2002). User acceptance enablers in individual decision making about technology: toward an integrated model. _Decision Sciences_, **33**(2), 297-316.
*   <a id="wei04"></a>Weinstein, A. (2004). _Handbook of market segmentation: strategic targeting for business and tecnology firms_. (3rd ed.). Binghamton, NY: The Haworth Press.
*   <a id="whi97"></a>White, G.K. (1997). International online marketing of foods to US consumers. _International Marketing Review_, **14**(5), 376-384.
*   <a id="zei87"></a>Zeithaml, V.A. & Gilly, M.C. (1987). Characteristics affecting the acceptance of retailing technologies: a comparison of elderly and nonelderly consumers. _Journal of Retailing_, **63**(1), 49-68.
*   <a id="zha96"></a>Zhang, T., Ramakrishnon, R. & Livny, M. (1996). BIRCH: an efficient data clustering method for very large databases. _Proceedings of the ACM SIGMOD Conference on Management of Data, Montreal, Canda, June 1996._, (pp. 103-114). New York, NY: ACM Press