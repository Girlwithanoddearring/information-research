#### Vol. 12 No. 2, January 2007

* * *

# A study of interpersonal information seeking: the role of topic and comment in the articulation of certainty and uncertainty of information need

#### [Kyunghye Yoon](mailto:kyoon@cs.oswego.edu)  
Computer Science Department,  
State University of New York at Oswego,  
Oswego, NY, USA

#### Abstract

> **Introduction.** This paper presents the findings of, and discusses, the qualitative analysis of users' information seeking articulation in interpersonal interactions. The discussion is focused on the role of the _uncertainty_ and _certainty_ and the _topic_ and _comment_ in communicating the user's cognitive uncertainty and certainty in the quest for salient information seeking interaction effectiveness.  
> **Method.** The study used transcripts of information seeking interaction between information user and a source person and the user's description of the event from a debriefing interview.  
> **Analysis.** Qualitative analysis was carried out using the sequence of the _uncertainty_ and _certainty_ and the _topic_ and _comment_ in the user's utterances during the interaction in order to explore the use of these elements. Descriptive statistics were used to describe the sample and the distribution of responses to each question. Sex differences on key questions were analysed using the Chi-squared test.  
> **Results.** The findings suggested some patterns in the employment of _uncertainty_ and _certainty_ and the _topic_ and _comment_. Users initiated the need description with uncertainty and then provided certainty to describe the need in detail. Both topic and comment were used in every stage of information seeking interaction, based on which the source person provided information.  
> **Conclusions.** The study confirmed that the user's certainty and uncertainty are important for describing the user's information need and that both topic and comment are essential to communicate the need.

## Introduction

This paper presents a study of users' information seeking articulation in the interpersonal interactions to explore the role of the topic and comment in communicating the user's cognitive uncertainty and certainty. The user's _uncertainty_ and _certainty_ are defined as what a user perceived as not knowing and as knowing in relation to their information seeking, respectively. The _topic_ and _comment_ are the two distinctive components of what complete a linguistic meaning ([Nilan and Fletcher 1987](#nil87)) and are defined as two dimensions of meaningful description of the user's understanding of information seeking; _topic_ is what the user is speaking about and _comment_ is what the user attaches to the topic.

The qualitative analysis was conducted with the sequence of the uncertainty and certainty and the topic and comment in the user's utterances during the interpersonal information seeking interaction. The aim of the analysis was to explore the employment of these elements the user's articulation of his or her information need. The findings suggested a pattern in the sequence of elements, which confirmed that the uncertainty and certainty aspects were important in the user's information seeking and that topic and comment were the necessary components in communicating. The discussion is two-fold: first, there is a focus on the user's employment of _certainty_ and _uncertainty_ in information seeking interaction; secondly, the role of _topic_ and _comment_ are discussed, showing that both are essential in the interaction process. The meaning is completed by the comment, and the interaction proceeds for the user to obtain further information that is tailored to the specific need. The study was conducted as a doctoral dissertation ([Yoon 2002](#yoo02)) and the conceptual background and the preliminary results of quantitative analysis were reported earlier ([Yoon and Nilan 1999](#yoo99))

The purpose of this study is to empirically describe the user's internal cognitive and related linguistic behaviour in an information seeking interaction, which will provide a conceptual basis for understanding the user's information need specification and help to gain insights into interpersonal communication in information seeking. By looking at the face-to-face interaction between a user and a human information source as the most universally available and natural form for human communication, it was intended to provide a model of information-seeking where meaning is conveyed with little mediation or without the functional limitations inherent in automated system environments and to bridge the research in user behaviour to system design.

## Methodology

### Data gathering

The study was designed to observe a natural occurrence of interpersonal information seeking interaction between a user and a source person. Two different settings were selected for data gathering: one consisted of interactions between a reference librarian and a user at a university library with reference services for library materials and information resources. The other was interactions between a faculty member and a student in a student advisory setting. It included a variety of academic and career planning advising for graduate students such as planning for the next semester, transferring into the department, concentrating on a major track, or job searching and internships. These two settings were selected as they deal with different types of information seeking: a reference service that helps to point to or locate the content, and advising, which provides actual information content.

Data were collected at the Syracuse University Bird Library and School of Information Studies at Syracuse University. Respondents were recruited in each setting when they came to talk to the source person (mostly in the library setting) or when they made an appointment with the source person (mostly in the advising setting). When they agreed to participate in the study, the interaction was audio-taped. The interaction was also transcribed for analysis. The duration of interaction ranged from five to thirty minutes in the library setting and from fifteen minutes to one hour in the advising setting. Following the interaction, the user was interviewed with a modified time-line interview technique to further understand the user's internal state of mind during the interaction, which might not have been conveyed by the expression of need. The time-line interview technique has been widely adapted for in-depth interviews of respondents' retrospective description of a situation in Sense-Making research ([Dervin 1983](#der83), [Dervin and Nilan 1986](#der86), [Savolainen 1993](#sav93)). In the modified method for this study, the transcript of the interaction and the audio tape recording provided the actual sequence of events of the user and the source person articulation in the interaction. The interview focused on each utterance in the transcript with a set of debriefing questions to have the user elaborate on her/his perception at that time. The total number of respondents was twenty-eight: thirteen from the advisory setting and fifteen from the library setting.

### Data coding

An utterance was a unit of analysis for the study; it was defined as a single turn that occurred between the user and the source person in the interaction as a turn-taking. Each utterance was coded to classify data (utterance) into the coding categories for each of the major concepts (uncertainty and certainty, topic and comment). There was another coding category: the information seeking stages, used to explore the data in relation to the two major concepts. The coding started with the four pre-determined categories of information seeking stages; user's need description, source's need understanding, source's information giving, and user's information evaluation. The categories were modified inductively as the coding proceeded.

#### Uncertainty and certainty

Each of the user's utterances were divided into the cognitive states of uncertainty (i.e., not knowing) and certainty (i.e., knowing) depending on the user's perceived orientation toward the information seeking aspects represented at the utterance. Uncertainty was operationally defined as what users point to as what they did not know and needed to find out at the time of the interaction. Certainty was operationally defined as what users know from their own knowledge, experience, or from their expectation of future understanding, in relation to the information seeking. Each utterance was coded to the dichotomous states of uncertainty and certainty rather than the continuous degree of how much certain or uncertain. If the user was not sure about the aspect that s/he was pointing to with an utterance, it was coded as uncertainty. The coding was based on the user's description from the debriefing interview. One of the debriefing questions in the time-line interview asked the user for her/his own description of the cognitive state associated with each utterance. Each utterance was coded based on the user's response to this item but it was also coded independently by the researcher. There was little disagreement between the users' definitions and the researcher's coding of uncertainty and certainty..

#### Topic and comment

Each utterance was also coded according to whether it functioned semantically as either topic or comment. The classification was based on the abstract notion of the dominant linguistic function of the topic and comment ([Begthol 198](#beg86); [Van Dijk 1979](#van79)) in the user's articulation. According to functional linguistics, _topic_ is what the speaker is speaking about and _comment_ is what s/he attaches to the _topic_ that makes the meaning complete. This notion of _functional sentence perspective_ has been applied to the whole text at the level of discourse in the field of information science as _functional text perspective_ ([Janos 1979](#jan79)). Topic can be seen as the _aboutness_ of the whole text (or a discourse), which can be captured as a relatively static and generic theme while comment reflects aspects of the topic that adds new information by specifying the significance of aspects of the topic to the user. For the coding, an utterance was coded as _topic_ when the user interpreted it as what s/he wanted to talk about in the information seeking interaction. _Comment_ was coded when the utterance conveyed more than simply the topic by adding new aspects of the user-specific context of the information need, which, in turn, led to a focus on those new aspects in the interaction with the information source.

#### Information seeking stage

The information seeking stage was inductively developed on the basis of the temporal structure of the information seeking interaction, dividing that interaction into different stages. The uncertainty/certainty and topic/comment sequences were then identified within each stage. Initial coding was based on the logical progression of the user-source interaction: 1) the user's description of his/her need; 2) the source person's need to understand that description; 3) the source person's information giving; and 4) the user's evaluation of the source person's information. The first two stages were the utterances where the user and the source person attended to the user need, and the last two stages were those where they attended to the information from the source person. As the coding proceeded, the categories were modified inductively and more categories were developed and coded: most of these were added as sub-categories under each of the four stages. The initial need description emerged as a dominant category during the data analysis and was coded as a subset of the overall need description. One particular category of _search process_ emerged independently and was later added as one of the main stages. It occurred in many cases in the library setting where both the user and the source person attended to the search process itself, talking about search strategies and tools for searching the system. This agreed with Wilson's _nested_ model ([Wilson 1999](#wil99)) which shows information searching as included within information seeking. The table shows the description of each stage based on the coding process.

<table><caption>

**Table 1: Categories found in information seeking stages**  
(Note: Sometimes an utterance is assigned to more than two categories)</caption>

<tbody>

<tr>

<th>Information seeking stages and sub-stages</th>

<th>Descriptions and examples</th>

</tr>

<tr>

<td>

1\. User's need description</td>

<td>User describes the information need</td>

</tr>

<tr>

<td>

1.1 Initial need description</td>

<td>The initial introduction of the user's need indicating that the user started or shifted to a new focus of attention introduce or address a new subject focus in the interaction</td>

</tr>

<tr>

<td>

1.2 Subsequent description</td>

<td>Remaining utterances further describe the need the user continued or stayed on the same focus of attention</td>

</tr>

<tr>

<td>

1.3 Procedural questions</td>

<td>User's question or remarks related to the procedural process, usually as a follow up</td>

</tr>

<tr>

<td>

2\. Source's understanding of need</td>

<td>Source attends and responds to the user need</td>

</tr>

<tr>

<td>

2.1 Source question</td>

<td>Source asks questions for further description to better understand</td>

</tr>

<tr>

<td>

2.2 Source understanding</td>

<td>Source confirms understanding the need</td>

</tr>

<tr>

<td>

3\. Search process</td>

<td>Both user and source attend to the search process and work together to search the system</td>

</tr>

<tr>

<td>

3.1 Search plan</td>

<td>Plan for search strategies and process</td>

</tr>

<tr>

<td>

3.2 Search scope</td>

<td>Define the scope for search</td>

</tr>

<tr>

<td>

3.3 Select resources</td>

<td>Decide on the search resources and databases</td>

</tr>

<tr>

<td>

3.4 Search evaluation</td>

<td>Evaluate the search results and procedures related to search</td>

</tr>

<tr>

<td>

4\. User's evaluating information</td>

<td>User attends and responses to the source's information</td>

</tr>

<tr>

<td>

4.1 Encountering/Learning</td>

<td>User tries to understand the source's information</td>

</tr>

<tr>

<td>

4.2 Evaluating</td>

<td>User responds with evaluative remarks to the source's information, including neutral/ negative/ positive description</td>

</tr>

</tbody>

</table>

### Data analysis

The two major concepts of uncertainty-certainty and topic-comment were paired, yielding the four pair-wise categories of uncertainty-topic; certainty-topic; certainty-comment; and uncertainty-comment for each utterance. These categories were analysed separately. The analysis focused on the sequence of these four paired categories in the information seeking stages in the interaction.

## Findings: uncertainty-certainty and topic-comment in the sequence of information seeking stage by setting

The findings suggested a pattern in the sequence of uncertainty-certainty and topic-comment, which confirmed that utterances reflecting both uncertainty and certainty aspects were essential in users' portrayal of their information seeking situations and both topic and comment were the fundamental components in the users' attempts at communicating this to the source person. The following is the summary of findings from the analysis of the sequence of uncertainty-certainty and topic-comment in each setting.

### The pattern in the library setting

1.  An interaction usually included one initial need description (one subject of focus).
2.  The interaction started with a description of uncertainty (uncertainty-comment) and then was followed by one or more utterances of certainty (certainty-topic; certainty-comment).
3.  The typical sequence of the user's need description was first uncertainty-topic, or certainty-comment with uncertainty-topic and then a series of certainty-topic and certainty-comment (Figure 1).
4.  The overall pattern of the information-seeking stage was: first, an utterance of the user's need description and the user's understanding of the need; second, a shift to the search stage; last, the source person's explanation or answer as information giving and the user's feedback (Figure 2). In some cases, the last stage included the user's questions about procedural matters to locate material or for check-out. This was not a new subject of focus but a follow up.

<figure>

![Figure 1](../p304fig1.png)

<figcaption>

**Figure 1: Sample sequence in the need description stage in the library setting**</figcaption>

</figure>

<figure>

![Figure 2](../p304fig2.png)

<figcaption>

**Figure 2: Sample sequence of information seeking stages in the library setting.**</figcaption>

</figure>

### The pattern in the advising setting

1.  Interactions usually involved more than one subject of focus. Within the subject of focus sub-concerns were laid out.
2.  Within one subject of focus, there was usually one utterance of uncertainty (i.e., initial need description), then a group of utterances (i.e., subsequent need description parameters) of certainty, either certainty-topic or certainty-comment. In most cases, the initial description was expressed as uncertainty-topic, but there were also some uncertainty-comment pairs. If the uncertainty was addressed as comment, the following certainty aspects were also comment, whereas uncertainty-topic by both comment-topic and certainty-comment. That is, the pattern of the user's utterance in the need description would follow the sequence of uncertainty-topic and then a series of certainty-comment and certainty-topic; or in some cases the sequence would be uncertainty-comment and then a series of certainty-comment pairs. (Figure 3). Often, certainty-comment did not occur as a single utterance but as a series of utterances.
3.  The overall pattern of the information-seeking stage was from the user need to the source person's answer within each subject focus. In the beginning, the expression of the user need occurred more often; later, the source person's answer occurred more frequently. The search stage rarely occurred in the advising setting. The source person's understanding of the need did not occur with the user's need description but with the source person's answering (Figure 4).
4.  User's feedback was not explicitly articulated but was (mentioned in the debriefing interview.

<figure>

![Figure 3](../p304fig3.png)

<figcaption>

**Figure 3: Sample sequence of the need description stage in the advising setting**</figcaption>

</figure>

<figure>

![Figure 4](../p304fig4.png)

<figcaption>

**Figure 4: Sample sequence of the information seeking stages in the advising setting**</figcaption>

</figure>

## Discussion: the pattern in the sequence of uncertainty-certainty

The sequence analysis of uncertainty-certainty in each information seeking stage of the interaction shows that users employ both certainty and uncertainty aspects of cognitive states in information seeking interactions. Most of the need description stage initially started with the uncertainty aspect and then moved to certainty aspect. There was little difference in the use of certainty and uncertainty in the two different settings.

In the library setting, all of the first user utterances started with uncertainty. In the advising setting also, most of the user's initial need description employed uncertainty. This might be because users tend to start the interaction by introducing the general area of their need as something they do not know and are looking for, and then elaborate it in terms of what they knew (certainty) afterward. The users mentioned at the debriefing interview that their utterances in the subsequent need description were also made under the assumption that the uncertainty was commonly agreed upon between the user and the source person. In other words, both the user and the source person interacted on the grounds of the uncertainty aspect of the user's need, and the user's need was communicated first by defining the topic or subject area to the source person. This supports the traditional emphasis on the uncertainty aspect of information need and the topic-based criteria for matching in information retrieval.

The findings, however, shows that users employ the certainty aspect in communicating their uncertainty, i.e., the information need. This suggests that the traditional emphasis on uncertainty alone is not sufficient for the source person to understand the user's need. Most of the initial need description of uncertainty seems to merely express the need at its most general and abstract level. The majority of utterances in the subsequent need description were addressed as certainty. In many cases, the initial need description of uncertainty was followed by many utterances of certainty. The user's need was first introduced as a question or a search statement like _'I'm looking for'_ or _'I need to look at'_ in terms of topic, then followed by descriptions of what the user knows about the topic to point out the need and to help the source person better understand. The user attempted to describe with a more specific details of what s/he knew so that it could be shared with the source person. It seems to be consistent with ASK principle that it is not easy to articulate or describe anomalous state of knowledge ([Belkin _et al._ 1982](#bel82)) unless the user knows at least some part of it.

The use of certainty was also found essential in the information encountering and evaluating stage. When users encountered information they tended to see whether the information was a continuation of what they knew and then they tried to make a connection between what they needed to find out and what they knew. Making connections of the information that they encountered with what they had already known seems to agree with Dervin's Sense Making process as a cognitive movement of one's own context ([Dervin and Nilan 1986](#der86)). Even though the users did not have information, in an uncertain state, they were able to employ their certainty of what they could have done, what was important and where they were heading cognitively. The user's ability to judge whether the information provided was useful or not (e.g., redundant) was also based on prior knowledge of the topic.

Overall, the users' employment of their cognitive state in information seeking interaction tends to focus on certainty rather than uncertainty. The employment of certainty aspect was emphasized throughout the information seeking interaction not only in the need description stage but also in the encountering and evaluating information stage in both settings. It seems to suggest a relationship to Kuhlthau's model ([1991](#kuh91)) in the sense that users were found to construct meaning by assimilating and accommodating new information with what they already knew or experienced. Users seem to create their meaning by selectively attending to new information that relates to what they already know.

The tendency to focus on certainty was found not only in the information seeking interaction between the user and the source person but was also apparent in the debriefing interview between the user and the researcher. The users tended to focus on the result of the information seeking interaction rather than in the sequential steps involved. In describing what had happened, they often repeated the most impressive results of the interaction. Often they jumped ahead in elaborating an utterance to where they had found out the answer or information. They emphasized what they had found as a new and informative piece of knowledge as the result of the information seeking interaction as well as the change in their knowledge state, even though the actual steps in getting that information had occurred later in the interaction. The transcript between the user and the source person helped the interview process stay focused on the specific point in time which elaborated on their cognitive state at that moment when the users tended to jump ahead of the actual occurrence after new information had revised their cognition. This indicates that, when seeking information, users naturally stressed the certainty aspect in the interaction with a source person. When their roles were reversed, users stressed certainty aspect by the way providing answers in the debriefing interview with the researcher. When they elaborated on an utterance, the users were more focused on the outcome of the interaction (i.e., what they found out) rather than on the time and space moment where that particular utterance occurred.

## Discussion: the pattern in the sequence of topic-comment and its relationship to uncertainty-certainty

Certainty and uncertainty states conceptually represent the cognitive meaning of a user regarding the internal, i.e., mental, information seeking: it may or may not involve an interaction with a source person as an external resource. For the information seeking interaction between the user and the source person, however, the meaning of the internal cognition of certainty and uncertainty is communicated as articulated utterances. The analysis of the sequence of topic-comment was to explore the role of these elements in communicating the user's meaning of certainty and uncertainty.

The overall pattern in the sequence of topic-comment supports the belief that both are salient in linguistic interaction. In every interaction, both topic and comment were included before the source person's information giving and the user's information encountering stages. This empirically illustrates that user's meaning cannot be completely shared through topic alone, but is dependent on comment as an integral part of the communicative interaction. Topic sets up the ground upon which the speaker and the listener agree, while comment addresses the connection to the new aspects of the topic added by the speaker at the time([Van Dijk 1979](#van79), [Begthol 1986](#beg86)).

The user and the source person tried to agree on the user's information need first by topic then they moved on to the specific user's meaning by comment. For example, one student in the advising setting had a question in which topic (uncertainty-topic) was employed first: _'Is it counted as IT [information technology]?'_ and then continued with comment, _'I did one with a law firm'_. The answer from the source person depended on the comment component, not only topic: _'It doesn't count as an internship that's already done'_. Given the high frequency of utterances employing comment, the role of comment to achieve mutual understanding in the interaction is critical.

It seems that the comment dimension is essential to help the source person understand the user's need. Often the user's comment followed the source person's questioning, which showed that the source person demanded some of the background context of the user's information need other than topic. After the user's meaning was understood, the source person was able to provide an appropriate searching method and strategy for information tailored to the particular user's need context.

The source persons were able to use various searching schemes within a topic area while they engaged in different users' agendas. For example, the same source person was used for a multiple interaction on the same subject (i.e., career advising, and a reference librarian in the map section), and they presented information in different ways (e.g., with different focus, ordering, etc.). This was not only from the librarians whose expertise covered the different searching tools and strategies but also from the source persons in the advising setting.

This underlies the source's ability to adapt and apply a searching scheme appropriate for the user according to the particular information need context as well as to provide content from his or her stock of knowledge. A searching scheme is a part of a source person's knowledge resource, i.e., information is a stock of knowledge on a topic and the appropriate organization of that stock. An implication would underlie the source person's ability to find out appropriate searching schemes that fit better to a particular context of users' need specifications into the information system design. It could be multiple dimensions of classification schemes, indexing or clustering techniques.

The implication is that comment dimension could be applied to information seeking research whether it is interpersonal or between a human and a system. Current information retrieval and other systems have been based on the topic dimension for information representation and matching. Lately, it has been contended that topic was not sufficient for effective matching and some researchers proposed to incorporate the comment dimension ([Ruthven 1999](#rut99), [Sauperl 2004](#sau04), [Krifka 2005](#kri05)). Multiple levels for information representation have also been proposed ([Jacob & Shaw 1998](#jac98), [Tuominen _et al._ 2003](#tuo03)). But the discussion here emphasizes the dynamic information representation that contains multiple schemes to organize and represent information content as a part of information system and capable of creating and providing the unique representation layout readily available for various individual users' needs.

## Conclusion

The pattern in the sequence of uncertainty-certainty stresses the uncertainty in the initial need description, and certainty aspects were employed in the subsequent need description. Information need addresses the uncertainty of user cognition but the uncertainty is not specific enough or articulated as a clear question that represents the information need. Rather it is used to define the broad area of the information problem or interest. The information seeking is based on the sharing of the certainty aspect of the user, which was the basis for sharing the information need and for understanding and evaluating the information provided.

The pattern in the sequence of topic and comment showed that the information from the source person was provided after both topic and comment was employed in the user's need description. Information seeking is a linguistic interaction based on the multiple dimensions of information aspects not on topic alone. Topic simply sets up what the speaker is talking about, a necessary first step. Subsequent to the agreement on a topic, the role of comment can be seen as a specification of the speaker's situation, problem and context that is conceptually orthogonal to topic in establishing a basis for the exchange of meaning. Topic and comment are identifiable and can be applied in information seeking interaction.

## Acknowledgements

## References

*   <a id="bel82"></a>Belkin, N.J., Oddy, R.N. & Brooks (1982). ASK for information retrieval: Part 1\. _Journal of Documentation,_, **38**(2), 61-71
*   <a id="beg86"></a>Begthol, C. (1986). Bibliographic classification theory and text linguistics: aboutness analysis, inter textuality and the cognitive act of classifying documents. _Journal of Documentation_, **42**(2), 84-113
*   <a id="der83"></a>Dervin, B. (1983). _An overview of Sense-Making research: concepts, methods, and results to date_. Paper presented at the annual meeting of the International Communication Association, Dallas, TX. [Available, upon completion of access form, from http://communication.sbs.ohio-state.edu/sense-making/art/artabsdervin83smoverview.html
*   <a id="der86"></a>Dervin, B. & Nilan, M.S. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33
*   <a id="jac98"></a>Jacob, E. & Shaw, D. (1998). Sociocognitive perspectives on representation. _Annual review of Information Science and Technology_, **33**, 131-186
*   <a id="jan79"></a>Janos, J. (1979). Theory of functional sentence perspective and its application for the prposes of automatic extracting, _Information processing and management_, **15**(1), 19-25
*   <a id="kri05"></a>Krifka, M. (2005). _[A functional similarity between bimanual coordination and topic/comment structure.](http://www.webcitation.org/5LNXmKW4Y) _Paper presented at the Blankensee Colloquium, Berlin-Schmöckwitz. Language evolution: cognitive and cultural factors, July 14-16, 2005\. Retrieved 24 December, 2006 from http://amor.cms.hu-berlin.de/~h2816i3x/BimanualCoordination.pdf
*   <a id="kuh91"></a>Kuhlthau, C.C. (1991). Inside the search process: information seeking from user's perspective, _Journal of the American Society for Information science_, **42**(5), 361-371
*   <a id="mai01"></a>Mai, J. (2001). Semiotics and indexing: an analysis of the subject indexing process. _Journal of Documentation_, **57**(5), 591-622
*   <a id="nil87"></a>Nilan, M.S. & Fletcher, P.T. (1987). Information behaviours in the preparations of research proposals: a user study. _Proceedings for American Society for Information Science_, **24**, 186-192
*   <a id="rut99"></a>Ruthven, I. (1999). Incorporating aspects of information use into relevance feedback._Journal of information retrieval_, **2**(1), 1-5
*   <a id="sau04"></a>Sauperl, A. (2004). Cataloger's common ground and shared knowledge. _Journal of the American Society for Information Science_, **55**(1), 55-63
*   <a id="sav93"></a>Savolainen, R. (1993). The Sense-Making theory: reviewing the interests of a user-centered approach to information seeking and use. _Information Processing and Management_, **29**(1), 13-28
*   <a id="tuo03"></a>Tuominen, K., Talja, S. & Savolainen, R. (2003). Multiperspective digital libraries: the implications of constructionism for the development of digital libraries. _Journal of the American Society for Information Science and Technology_, **54**(6) 561-569
*   <a id="van79"></a>Van Dijk, T.A. (1979). Relevance assignment in discourse comprehension. _Discourse Processes_, **2**, 113-126
*   <a id="wil99"></a>Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270
*   <a id="yoo02"></a>Yoon, K. (2002). _Certainty, uncertainty and the role of topic and comment in interpersonal information seeking interactions._ Unpublished doctoral dissertation, School of Information Studies, Syracuse University, Syracuse, New York, USA
*   <a id="yoo99"></a>Yoon, K. & Nilan, M.S. (1999). Toward a reconceptualization of information seeking research: focus on the exchange of meaning. _Information Processing and Management_, **35**(6), 871-890