#### Vol. 12 No. 2, January 2007

* * *

# Health information ties: preliminary findings on the health information seeking behaviour of an African-American community

#### [Ophelia T. Morey](mailto:otmorey@buffalo.edu)  
Health Sciences Library, University at Buffalo, Buffalo, New York USA

#### Abstract

> **Introduction.** The purpose of this study is to explore the health information seeking behaviour of African-Americans using Granovetter's strength of weak ties as the theoretical framework.  
> **Method.** A listed household telephone survey of 216 residents who reside on the Near East Side ofBuffalo, New York. Using the Rand() feature in MS Excel a random sample was drawn from the telephone numbers randomly selected from census tracts covering the target area. Random samples were drawn three times until at least 200 telephone surveys were completed.  
> **Analysis.** Data analysis was completed using SPSS 13.0 for Windows. Both bivariate and multivariate analyses were performed along with analysing descriptive statistics and the chi-squared test to determine the association between the source of health information as related to demographics and tie strength between individuals.  
> **Results.** Overall, most respondents rely on a health service professional for health information followed by a web site. Respondents reported having a 'somewhat close' relationship with health care professionals as compared to having a 'very close' relationship with family members. The importance of health service professionals as a significant or weak tie in health information seeking is consistent with the strength of weak ties theory. Results on proxy searching behaviour or searching on behalf of another person are also examined. Most respondents sought information for themselves, although 22.2 percent sought health information on behalf of another person.  
> **Conclusions.** These findings confirm the important role of health service professionals in health information seeking by under-served populations and suggest the need for investigation on the information giving behaviour of these professionals. These findings also demonstrate the need for further research on strong ties or close family and friends as a source of health information, in particular their role in proxy health information behaviour.

## Introduction

The library literature is almost void of theory-based studies addressing consumer health information seeking behaviour. Wilson ([2000](#wil00)) defined information seeking behaviour as the purposive seeking for information because of a need to satisfy some goal. Many studies address information seeking behaviour in general, but surprisingly only a few studies have focused on consumer health information-seeking issues.

Baker and Pettigrew ([1999](#bak99)) state that very few consumer health information studies conducted by library and information science professionals have been theory-based and thus the value of their results is limited. They suggest that a more formal framework is needed in order to understand consumer health information seeking behaviour as compared to the non-theoretically grounded generalizations that library and information science professionals often use to implement information services to consumers seeking health information. They propose that the strength of weak ties, a theory from sociology, can explain and possibly predict consumer health information seeking behaviour. Strength of weak ties is a social network theory introduced by Mark Granovetter. He defines the strength of the tie as a combination of the amount of time, the emotional intensity, the intimacy (mutual confiding) and the reciprocal services that characterize the tie. The stronger the tie between individuals is an indication that they are part of the same social network ([Dixon, 2005](#dix05)). Based on his analysis of tie strength Granovetter ([1973](#gra73)) asserts that more people are reached through weak ties or acquaintances, though having strong ties or close friends and family may result in creating a bridge to other social networks. Further, people who are weakly connected to an individual are more useful sources for obtaining information and other resources than people who are strongly connected to the person seeking information ([Princeton Research Associates 2006;](#boa06) [Courtright 2005;](#cou05) [Granovetter 1983](#gra83)).

Social network analysis can be expanded upon as an approach and set of techniques for the study of information exchange ([Haythornthwaite 1996](#hay96)). According to Haythornthwaite, social network analysis identifies ties between pairs of individuals, but also examines the relationships that form and maintain those ties. Haythornthwaite observes that information is an important resource, one that depends on making and maintaining contact with the right people. This focus on patterns of tie relationships makes the social network approach applicable for studying information access.

Tie strength has not been given a precise conceptual definition. This broad definition of tie strength is identified as a deficiency in Granovetter's theory ([Dixon 2005](#dix05)). Haythornwaite ([1996](#hay96)) defines ties in general, but does not give a definition of tie strength, however Marsden and Campbell ([1984](#mar84)) note that numerous measures of strength have been used and further explain that:

> The most common tactic used to measure tie strength has been to use indications of the 'closeness' of a relationship; thus, close friends have been said to be 'strong' ties, while acquaintances or friends of friends have been called 'weak' ties. Sometimes this approach to measurement is combined with one that presumes that the source of a relationship is an accurate indication of its strength; often, therefore, relatives are assumed strong ties, while neighbours or co-workers are treated as weak ones. ([Marsden and Campbell 1984](#mar84): 483)

According to the authors, 'this variety of measures indicates that tie strength is at least a sensitizing concept that squares with the intuitions of many researchers. What is not clear is whether these intuitionsrefer to one concept or to several' ([Marsden and Campbell 1984](#mar84): 484). From their research using Granovetter's concepts they conclude that a measure of 'closeness' or the emotional intensity of a relationship is the best indicator of the concept of tie strength as compared to breadth of discussion topics, mutual confiding, duration and frequency.

In the library and information science field, Pettigrew is the only the scholar to apply Granovetter's theory to health information seeking ([Dixon 2005](#dix05)). Pettigrew's ([1998,](#pet98) [1999,](#pet99) [2000](#pet00)) ethnographic research applies Granovetter's theory to a health service environment where the flow of information between nurses and elderly patients at a foot clinic was investigated.

As mentioned previously few library and information science research studies focus on consumer health information seeking, especially as it pertains to minority populations. Although not theory-based, studies by Gollop ([1997](#gol97)) and Spink, _et al._ ([1997](#spi97)) focused on the health and lifestyle-related information behaviour of African-Americans. Recently, Courtright ([2005](#cou05)) framed her exploratory study of Latino immigrants' health information seeking by looking at information practices within social networks, the local institutional context and the use and non-use of information technologies. Therefore, the focus of this paper will be to present preliminary results from a telephone survey of an under-served population. The survey explores consumer health information seeking behaviour using Granovetter's theory as the conceptual framework. Results on proxy searching behaviour ([Fisher _et al._ 2005](#fis05)) or searching on behalf of another person are also examined.

## Methods

A telephone survey was the selected data collection method mainly because ninety percent of the residents in the target community have a telephone ([University at Buffalo. _Center for Urban Studies_ 2001](#uab01)). In addition, many of the respondents may have a low literacy level. The East Side Study reports that one third of the respondents had not completed high school ([University at Buffalo. _Center for Urban Studies_ 2001](#uab01)). Telephone interviewing allows for clarification of interview questions as compared to self-administered mail questionnaires. Finally, respondents may be more comfortable answering certain questions over the telephone than in-person.

Furthermore, a telephone survey was suitable for this project because of the small sample size goal of 200 adults. The advantages of sampling a small group to identify the attributes of a larger population are tied to time and cost. A larger study would be more difficult to manage and the length of the interview period would make it more time consuming to analyse the data.

I purchased a random listed household telephone number sample for census tracts covering the target area on Buffalo's Near East Side and systematically drew a sample telephone numbers from the list. Random samples were drawn three times using the Rand() feature in MS Excel until at least 200 interviews were completed.

The questionnaire was designed using selected questions from Pew Internet and American Life Project questionnaire for _The online health care revolution_ ([Fox and Rainie 2000](#fox00)) and _The strength of Internet ties_' ([Princeton Research Associates 2006](#boa06)) reports along with original questions based on recommendations by Fowler ([1995](#fow95)). Questions included in the survey were those that a participant would be likely to know the answers to or those that were relevant to them ([Babbie 1990](#bab90)). The design of the survey is based on the following questions:

1.  Which members of their social networks do participants interact with the most when seeking consumer health information? How do the participants define the 'closeness' of this relationship?
2.  Where do participants seek and obtain consumer health information?
3.  Which age group is more likely to seek and obtain consumer health information?
4.  Which sex is more likely to seek and obtain consumer health information?
5.  Did the participant look for consumer health information for himself or herself or someone else?

To capture the strength of weak ties concepts a question (see [Figure 1](#fig1)) was worded to determine if the respondent has a 'very close' relationship with the person they had mentioned in terms of whether they discuss important matters with, are in frequent contact with, or seek help from them ([Princeton Research Associates 2006](#boa06)). This approach to determine a strong or core tie uses three key dimensions of Granovetter's definition of tie strength; emotional intimacy, contact, and the availability of social network capital ([Princeton Research Associates 2006](#boa06)). To determine a weak or significant tie the question is worded to capture a 'somewhat close' relationship by defining this relationship as more than an acquaintance but not as close as family or friends. This type of relationship is characterized as important when people seek help or advice ([Princeton Research Associates 2006](#boa06)).

The survey instrument was tested in August 2005 and revised before the final instrument was administered. A student assistant and I made telephone calls during September and October 2005 using [Sawtooth's Computer Assisted Telephone Interviewing](http://www.sawtooth.com/products/#cati) system. The student assistant received training and I was available in the laboratory when the student made calls to assure that the questionnaire was administered correctly. Calls were made every day, Monday to Sunday, between the hours of 9 a.m. and 9 p.m. After calling 940 randomly selected telephone numbers 216 surveys (n=139 women, n=77 men) were completed. The sample of 216 respondents has a 95% confidence level and a (+/- 6.64%) confidence interval. All valid telephone numbers were dialled at least five times at various times throughout the week. The instrument includes sixty-four questions, not including the screening questions. Participants were asked a range of between eighteen and twenty-nine questions depending on when and where they sought health information. The average completed interview length is 8.33 minutes.

Household residents were screened by first asking to speak to the youngest male age 18 or older, who was at home. If no male was present the interviewer asked to speak with the oldest female, age 18 or older, who was at home. Simply talking to the first person who answers the phone actually introduces bias into the sample. Dillman ([1978](#dil78)) has shown the potential for biased results when interviewing whoever answers the phone in a household. This is also the introduction that was used in the questionnaire for _The strength of Internet ties_ study ([Princeton Research Associates 2006](#boa06b)). The introduction was changed to screen for the youngest female once 100 interviews were completed in an effort to interview more females younger than 45 years of age. This strategy was implemented to help eliminate bias and increase the likelihood of a good sample with a broad distribution of men and women of all ages.

## Findings

The respondents were African-Americans, males and females, ages 18-74 who reside on the East Side of Buffalo, New York in the Masten and Ellicott districts. These districts are located in the following Erie County census tracts: 14\.02 15 25\.02 26, 31, 32\.01, 32\.02, 33\.01, and 33\.02\. The average median household income for this area is \$17,884 as compared to \$24,536 for the City of Buffalo and \$38,567 for Erie County ([US. _Census Bureau_ 2006a](#ame06a)). In 2004 22.54% of survey respondents reported having a household income of at least $20,000 but under \$30,000 19.72% had a household income of \$10,000 to under \$20,000 and 17.37% reported a household income of less than \$10,000\. Although this is a poorer neighbourhood, it is located near many businesses, educational institutions and health care facilities.

We interviewed 216 respondents; 66.5% (n=139) are female and 33.5% (n=77) are male. The average age of males for the target area is 34 years and 39 years for females ([US. _Census Bureau_ 2006b](#ame06b)). The average age of male respondents is 47.58 years and 52.31 years for females. The overall average age for all respondents is 50.63 years and 37.48 years for the target area ([US. _Census Bureau_ 2006b](#ame06b)). The age range distribution is shown in Table 1\. Eighty-two percent of the respondents hold at least a high school diploma, while 30.05% have a high school diploma or General Educational Development test and 30.52% have attended college.

<table><caption>

**Table 1: Respondents' age range**  
</caption>

<tbody>

<tr>

<th rowspan="3">Age</th>

</tr>

<tr>

<th colspan="2">Men</th>

<th colspan="2">Women</th>

<th colspan="2">Both</th>

</tr>

<tr>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>18 - 24</td>

<td>8</td>

<td>10.4</td>

<td>13</td>

<td>9.4</td>

<td>21</td>

<td>9.7</td>

</tr>

<tr>

<td>25 - 34</td>

<td>8</td>

<td>10.4</td>

<td>11</td>

<td>7.9</td>

<td>19</td>

<td>8.8</td>

</tr>

<tr>

<td>35 - 44</td>

<td>18</td>

<td>23.4</td>

<td>20</td>

<td>14.4</td>

<td>38</td>

<td>17.6</td>

</tr>

<tr>

<td>45 - 54</td>

<td>14</td>

<td>18.1</td>

<td>27</td>

<td>19.4</td>

<td>41</td>

<td>19.0</td>

</tr>

<tr>

<td>55 - 64</td>

<td>13</td>

<td>16.9</td>

<td>25</td>

<td>18.0</td>

<td>38</td>

<td>17.6</td>

</tr>

<tr>

<td>65 - 74</td>

<td>16</td>

<td>20.8</td>

<td>43</td>

<td>30.9</td>

<td>59</td>

<td>27.3</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

77</td>

<td>

100.0</td>

<td>

139</td>

<td>

100.0</td>

<td>

216</td>

<td>

100.0</td>

</tr>

</tbody>

</table>

### Source of health information seeking

At the start of each interview respondents were asked if they had looked for health information in the last six months. If the respondent replied 'yes', they were read a list of health information sources. If the respondent continued with the interview, they were asked a set of detailed questions based on the answer for their health information source. If the respondent answered 'no', they were asked, 'When was the last time you looked for health information?' The respondents were read two options: More than six months ago, but less than a year ago or More than a year ago. After answering this question, the respondent was asked where they found health information the last time they went looking. After selecting their choice, they were asked a set of questions to gather demographic information. Those who had looked for health information more than six months ago were not asked the detailed set of questions because of possible recall problems.

<table><caption>

**Table 2: Source of health information**</caption>

<tbody>

<tr>

<th>Source</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>Health service professional</td>

<td>97</td>

<td>45.3</td>

</tr>

<tr>

<td>Family living with</td>

<td>4</td>

<td>1.9</td>

</tr>

<tr>

<td>Family not living with</td>

<td>17</td>

<td>7.9</td>

</tr>

<tr>

<td>Friend living with</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<td>Friend not living with</td>

<td>9</td>

<td>4.2</td>

</tr>

<tr>

<td>Neighbour</td>

<td>1</td>

<td>0.5</td>

</tr>

<tr>

<td>Librarian</td>

<td>2</td>

<td>0.9</td>

</tr>

<tr>

<td>Co-worker</td>

<td>3</td>

<td>1.4</td>

</tr>

<tr>

<td>Web site</td>

<td>31</td>

<td>14.5</td>

</tr>

<tr>

<td>Television</td>

<td>9</td>

<td>4.2</td>

</tr>

<tr>

<td>Telephone hotline</td>

<td>1</td>

<td>0.5</td>

</tr>

<tr>

<td>Radio</td>

<td>1</td>

<td>0.5</td>

</tr>

<tr>

<td>Book</td>

<td>3</td>

<td>1.4</td>

</tr>

<tr>

<td>Magazine</td>

<td>10</td>

<td>4.7</td>

</tr>

<tr>

<td>Newspaper</td>

<td>4</td>

<td>1.9</td>

</tr>

<tr>

<td>Other</td>

<td>21</td>

<td>9.8</td>

</tr>

<tr>

<td>Don't know/Not sure</td>

<td>1</td>

<td>0.5</td>

</tr>

<tr>

<td>Missing data</td>

<td>2</td>

<td>0.0</td>

</tr>

<tr>

<th>Total</th>

<th>216</th>

<th>100.0</th>

</tr>

</tbody>

</table>

Table 2 shows data for all respondents regardless of when they looked for health information. Most respondents (45.3) sought health information from a health service professional; 14.5 percent from a Web site and 9.8% from another source. No statistically significant difference exists between men and women in their health information source (Table 3: χ<sup>2</sup>=15.501, df =15, p=.416). However, older respondents were more likely to have looked and obtained health information as compared to younger respondents. In addition, younger respondents were more likely to have sought health information from a Web site, while older respondents tend to rely on a health service professional (Table 4: χ<sup>2</sup>=95.284, df =75, p=.057).

<table><caption>

**Table 3: Source of health information by sex**</caption>

<tbody>

<tr>

<th rowspan="3">Source</th>

<th colspan="4">Sex</th>

<th colspan="2" rowspan="2">

**Group Total**</th>

</tr>

<tr>

<th colspan="2">Female</th>

<th colspan="2">Male</th>

</tr>

<tr>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>Health service professional</td>

<td>60</td>

<td>61.9</td>

<td>37</td>

<td>38.1</td>

<td>97</td>

<td>100.0</td>

</tr>

<tr>

<td>Family living with</td>

<td>1</td>

<td>25.0</td>

<td>3</td>

<td>75.0</td>

<td>4</td>

<td>100.0</td>

</tr>

<tr>

<td>Family not living with</td>

<td>14</td>

<td>82.4</td>

<td>3</td>

<td>17.6</td>

<td>17</td>

<td>100.0</td>

</tr>

<tr>

<td>Friend living with</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<td>Friend not living with</td>

<td>5</td>

<td>55.6</td>

<td>4</td>

<td>44.4</td>

<td>9</td>

<td>100.0</td>

</tr>

<tr>

<td>Neighbour</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

<td>1</td>

<td>100.0</td>

</tr>

<tr>

<td>Librarian</td>

<td>1</td>

<td>50.0</td>

<td>1</td>

<td>50.0</td>

<td>2</td>

<td>100.0</td>

</tr>

<tr>

<td>Co-worker</td>

<td>3</td>

<td>100.0</td>

<td>0</td>

<td>0.0</td>

<td>3</td>

<td>100.0</td>

</tr>

<tr>

<td>Web site</td>

<td>22</td>

<td>71.0</td>

<td>9</td>

<td>29.0</td>

<td>31</td>

<td>100.0</td>

</tr>

<tr>

<td>Television</td>

<td>6</td>

<td>66.7</td>

<td>3</td>

<td>33.3</td>

<td>9</td>

<td>100.0</td>

</tr>

<tr>

<td>Telephone hotline</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

<td>1</td>

<td>100.0</td>

</tr>

<tr>

<td>Radio</td>

<td>1</td>

<td>100.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

</tr>

<tr>

<td>Book</td>

<td>3</td>

<td>100.0</td>

<td>0</td>

<td>0.0</td>

<td>3</td>

<td>100.0</td>

</tr>

<tr>

<td>Magazine</td>

<td>7</td>

<td>70.0</td>

<td>3</td>

<td>30.0</td>

<td>10</td>

<td>100.0</td>

</tr>

<tr>

<td>Newspaper</td>

<td>2</td>

<td>50.0</td>

<td>2</td>

<td>50.0</td>

<td>4</td>

<td>100.0</td>

</tr>

<tr>

<td>Other</td>

<td>12</td>

<td>57.1</td>

<td>9</td>

<td>42.9</td>

<td>21</td>

<td>100.0</td>

</tr>

<tr>

<td>Don't know/ Not sure</td>

<td>1</td>

<td>100.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

</tr>

<tr>

<td>

**Total**</td>

<td>138</td>

<td>64.5</td>

<td>76</td>

<td>35.5</td>

<td>214</td>

<td>100.0</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 4: Source of health information by age range**</caption>

<tbody>

<tr>

<th rowspan="3">Source</th>

<th colspan="12">

**Age Categories**</th>

<th colspan="4" rowspan="2">

**Group Total**</th>

</tr>

<tr>

<th colspan="2">18 - 24</th>

<th colspan="2">25 - 34</th>

<th colspan="2">35 - 44</th>

<th colspan="2">45 - 54</th>

<th colspan="2">55 - 64</th>

<th colspan="2">65 - 74</th>

</tr>

<tr>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>Health service professional</td>

<td>7</td>

<td>7.2</td>

<td>8</td>

<td>8.2</td>

<td>10</td>

<td>10.3</td>

<td>15</td>

<td>15.5</td>

<td>25</td>

<td>25.8</td>

<td>32</td>

<td>33.0</td>

<td>97</td>

<td>100.0</td>

</tr>

<tr>

<td>Family living with</td>

<td>1</td>

<td>25.0</td>

<td>1</td>

<td>25.0</td>

<td>1</td>

<td>25.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>25.0</td>

<td>4</td>

<td>100.0</td>

</tr>

<tr>

<td>Family not living with</td>

<td>6</td>

<td>35.3</td>

<td>1</td>

<td>5.9</td>

<td>4</td>

<td>23.5</td>

<td>4</td>

<td>23.5</td>

<td>1</td>

<td>5.9</td>

<td>1</td>

<td>5.9</td>

<td>17</td>

<td>100.0</td>

</tr>

<tr>

<td>Friend living with</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<td>Friend not living with</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>11.1</td>

<td>1</td>

<td>11.1</td>

<td>1</td>

<td>11.1</td>

<td>6</td>

<td>66.7</td>

<td>9</td>

<td>100.0</td>

</tr>

<tr>

<td>Neighbour</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

</tr>

<tr>

<td>Librarian</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>50.0</td>

<td>1</td>

<td>50.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>2</td>

<td>100.0</td>

</tr>

<tr>

<td>Co-worker</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>2</td>

<td>66.7</td>

<td>1</td>

<td>33.3</td>

<td>0</td>

<td>0.0</td>

<td>3</td>

<td>100.0</td>

</tr>

<tr>

<td>Web site</td>

<td>5</td>

<td>16.1</td>

<td>6</td>

<td>19.4</td>

<td>11</td>

<td>35.5</td>

<td>7</td>

<td>22.6</td>

<td>1</td>

<td>3.2</td>

<td>1</td>

<td>3.2</td>

<td>31</td>

<td>100.0</td>

</tr>

<tr>

<td>Television</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>11.1</td>

<td>2</td>

<td>22.2</td>

<td>3</td>

<td>33.3</td>

<td>1</td>

<td>11.1</td>

<td>2</td>

<td>22.2</td>

<td>9</td>

<td>100.0</td>

</tr>

<tr>

<td>Telephone hotline</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

</tr>

<tr>

<td>Radio</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

<td>1</td>

<td>100.0</td>

</tr>

<tr>

<td>Book</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>33.3</td>

<td>1</td>

<td>33.3</td>

<td>1</td>

<td>33.3</td>

<td>0</td>

<td>0.0</td>

<td>3</td>

<td>100.0</td>

</tr>

<tr>

<td>Magazine</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>2</td>

<td>20.0</td>

<td>0</td>

<td>0.0</td>

<td>2</td>

<td>20.0</td>

<td>6</td>

<td>60.0</td>

<td>10</td>

<td>100.0</td>

</tr>

<tr>

<td>Newspaper</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>25.0</td>

<td>1</td>

<td>25.0</td>

<td>1</td>

<td>25.0</td>

<td>1</td>

<td>25.0</td>

<td>4</td>

<td>100.0</td>

</tr>

<tr>

<td>Other</td>

<td>2</td>

<td>9.5</td>

<td>1</td>

<td>4.8</td>

<td>4</td>

<td>19.0</td>

<td>4</td>

<td>19.0</td>

<td>4</td>

<td>19.0</td>

<td>6</td>

<td>28.6</td>

<td>21</td>

<td>100.0</td>

</tr>

<tr>

<td>Don't know/ Not sure</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>1</td>

<td>100.0</td>

<td>1</td>

<td>100.0</td>

</tr>

<tr>

<td>

**Group Total**</td>

<td>21</td>

<td>9.8</td>

<td>18</td>

<td>8.4</td>

<td>38</td>

<td>17.8</td>

<td>41</td>

<td>19.2</td>

<td>38</td>

<td>17.8</td>

<td>58</td>

<td>27.1</td>

<td>214</td>

<td>100.0</td>

</tr>

</tbody>

</table>

### <a id="fig1"></a>Closeness of relationship

Participants who had looked for health information in the last six months and who had sought the information from individuals in their social network were asked the following question:

<table><caption>

**Figure 1: Closeness of relationship survey question** ([Princeton Research Associates 2006](#boa06b))</caption>

<tbody>

<tr>

<td>

**(READ)** Thinking about the person you just mentioned; would you describe your relationship with this persons as being VERY CLOSE or SOMEWHAT CLOSE OR NOT CLOSE at all? For example...  

People who are VERY CLOSE might include those who discuss important matters with, regularly keep in touch, or are there for you when you need help.  

People who are SOMEWHAT CLOSE might include those you are more than just casual acquaintances, but they are not close as some friends and relatives  

People who are NOT CLOSE at all are just casual acquaintances.  

**(READ)** You would describe your relationship with the person you just mentioned as...

1.  Very close
2.  Somewhat close
3.  Not close
4.  **(DO NOT READ)** Don't know/Refused

</td>

</tr>

</tbody>

</table>

Relationships identified as 'very close' have been identified as core ties ([Princeton Research Associates 2006](#boa06)). The concept of core ties defines relationship strength by emotional intimacy, contact, and the availability of social network capital. In contrast, significant ties are characterized by a 'somewhat close' relationship. Significant ties are weaker than core ties in that the respondent is less likely to discuss important matters, are in less frequent contact, and are less apt to seek help from these individuals ([Princeton Research Associates 2006](#boa06)).

In this survey (Tables 5 and 6), most respondents (42.4%) sought health information from a health service professional (weak tie) and most (52.8%) identified their relationship with this person as being 'somewhat close' (significant tie). Family members (strong tie) living with (2.4%) and not living with (8.8%) were the second choice for seeking health information. They are identified as 'very close' (core tie). Data analysis indicates that there is a significant statistical difference between the closeness of the relationship and the source of health information (Table 6: χ<sup>2</sup>=34.573, df =10, p<=.001). the importance of health service professionals as a significant or weak tie in health information seeking is consistent with the strength of weak ties theory ([Granovetter 1983](#gra83), [1973](#gra73)).

<table><caption>

**Table 5: Source of health information last six months**  
</caption>

<tbody>

<tr>

<th>Source</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>Health service professional</td>

<td>53</td>

<td>42.4</td>

</tr>

<tr>

<td>Family living with</td>

<td>3</td>

<td>2.4</td>

</tr>

<tr>

<td>Family not living with</td>

<td>11</td>

<td>8.8</td>

</tr>

<tr>

<td>Friend living with</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<td>Friend not living with</td>

<td>3</td>

<td>2.4</td>

</tr>

<tr>

<td>Neighbour</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<td>Librarian</td>

<td>2</td>

<td>1.6</td>

</tr>

<tr>

<td>Co-worker</td>

<td>2</td>

<td>1.6</td>

</tr>

<tr>

<td>Web site</td>

<td>23</td>

<td>18.4</td>

</tr>

<tr>

<td>Television</td>

<td>5</td>

<td>4.0</td>

</tr>

<tr>

<td>Telephone hotline</td>

<td>1</td>

<td>0.8</td>

</tr>

<tr>

<td>Radio</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<td>Book</td>

<td>2</td>

<td>1.6</td>

</tr>

<tr>

<td>Magazine</td>

<td>7</td>

<td>5.6</td>

</tr>

<tr>

<td>Newspaper</td>

<td>3</td>

<td>2.4</td>

</tr>

<tr>

<td>Other</td>

<td>10</td>

<td>8.0</td>

</tr>

<tr>

<td>

**Total**</td>

<td>125</td>

<td>100.0</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 6: Closeness of relationship for source of health information last six months**  
</caption>

<tbody>

<tr>

<th rowspan="3">Source</th>

<th colspan="6">Closeness of relationship</th>

<th colspan="2" rowspan="2">Total</th>

</tr>

<tr>

<th colspan="2">Very close</th>

<th colspan="2">Somewhat close</th>

<th colspan="2">Not close</th>

</tr>

<tr>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>Health service professional</td>

<td>13</td>

<td>24.5</td>

<td>28</td>

<td>52.8</td>

<td>12</td>

<td>22.6</td>

<td>53</td>

<td>100.0</td>

</tr>

<tr>

<td>Family living with</td>

<td>3</td>

<td>100.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>3</td>

<td>100.0</td>

</tr>

<tr>

<td>Family not living with</td>

<td>10</td>

<td>90.9</td>

<td>1</td>

<td>9.1</td>

<td>0</td>

<td>0.0</td>

<td>11</td>

<td>100.0</td>

</tr>

<tr>

<td>Friend living with</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>100.0</td>

</tr>

<tr>

<td>Friend not living with</td>

<td>1</td>

<td>33.3</td>

<td>2</td>

<td>66.7</td>

<td>0</td>

<td>0.0</td>

<td>3</td>

<td>100.0</td>

</tr>

<tr>

<td>Neighbour</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>100.0</td>

</tr>

<tr>

<td>Librarian</td>

<td>0</td>

<td>0.0</td>

<td>0</td>

<td>0.0</td>

<td>2</td>

<td>100.0</td>

<td>2</td>

<td>100.0</td>

</tr>

<tr>

<td>Co-worker</td>

<td>0</td>

<td>0.0</td>

<td>2</td>

<td>100.0</td>

<td>0</td>

<td>0.0</td>

<td>2</td>

<td>100.0</td>

</tr>

<tr>

<td>

**Total**</td>

<td>27</td>

<td>36.5</td>

<td>33</td>

<td>44.6</td>

<td>14</td>

<td>18.9</td>

<td>74</td>

<td>100.0</td>

</tr>

</tbody>

</table>

Further analysis shows that the observed relationship between the closeness of relationship and the source of health information is statistically significant at the .01 level but not at .001 (χ<sup>2</sup>=25.530, df =10, p=.004) for women. There is no significant statistical difference between the closeness of relationship and the source of health information for male respondents (χ<sup>2</sup>=8.613, df = 4, p=.072).

Results on proxy searching ([Fisher _et al._ 2005](#fis05)) or searching on behalf of another person show that respondents who looked for health information in the past six months were more likely to have sought the information for themselves (76.2%), although 22.2 percent sought health information on behalf of another person, such as a child, parent, another relative or non-relative. In addition, women slightly more often exhibit proxy searching behaviour than do men.

## Implications and future work

These findings confirm previous research that health service professionals (weak tie) play a major role as consumer health information providers for African-Americans ([Gollop 1997](#gol97); [Spink _et al._ 1997](#spi97)). It appears for this population that the Internet does not replace traditional sources for consumer health information, including family members. Courtright's research also indicates that other under-served populations are not seeking health information online ([Courtright 2005](#cou05)). These findings could suggest further research on digital disparities as related to health information seeking behaviour and the investigation of under-served minority populations as related to the information giving behaviour of health service professionals. Health literacy could also be a factor in whether someone seeks health information online or from a person. I plan to focus future analysis on the characteristics of individuals who looked online for health information versus those who had not. This will involve further exploring the age factor, health status including the presence of chronic diseases, health insurance and the nature of the health information the respondents received or were seeking.

In conclusion, the results show that individuals also seek strong ties or close family and friends as a source of health information. Other research also indicates the prominent role of strong ties in health information seeking ([Princeton Research Associates 2006](#boa06); [Courtright 2005](#cou05); [Spink _et al._ 1997](#spi97)). A weakness of strength of weak ties is that it does not emphasize the role of strong ties ([Dixon 2005](#dix05)). Although the quality of the health information maybe limited when received from strong ties [Courtright 2005](#cou05), there may be a reason to further investigate the role of strong ties and the impact of the health information they give. Research indicates that information exchange between strong ties is very useful, while this relationship may limit access to key information and offer an explanation for those who are information poor ([Courtright 2005](#cou05); [Dixon 2005](#dix05)). Strong tie relationships are also emphasized when individuals seek health information on behalf others (proxy searching). More-in-depth social network analysis on proxy health information behaviour may reveal insights into the role and impact of strong tie relationships.

## Acknowledgments

This project was funded by the Dr. Nuala McGann Drescher Affirmative Action/Diversity Leave Program sponsored by the State of New York/United University Professions Affirmative Action/Diversity Committee and the University at Buffalo University Libraries. The author would like to thank Michael Lichter, University at Buffalo; Beth Staebell, University at Buffalo; Barry Wellman, University of Toronto and Jeffrey Boase, University of Toronto for their assistance with this project. In addition, a special _Thank you_ to the survey respondents.

## References

*   <a id="bab90"></a>Babbie, E. (1990). _Survey research methods_ (2nd. ed.). Belmont, CA: Wadsworth Publishing Company.
*   <a id="bak99"></a>Baker, L., & Pettigrew, K. (1999). Theories for practitioners: two frameworks for studying consumer health information-seeking behaviour. _Bulletin of the Medical Library Association_, **87**(4), 444-450.
*   <a id="cou05"></a>Courtright, C. (2005). [Health information-seeking among Latino newcomers: an exploratory study](http://informationr.net/ir/10-2/paper224.html). _Information Research_ , **10**(2), paper 224\. Retrieved 17 December 2006, from http://informationr.net/ir/10-2/paper224.html.
*   <a id="dil78"></a>Dillman, D.A. (1978). _Mail and telephone surveys: the total design method_. New York, NY: John Wiley & Sons.
*   <a id="dix05"></a>Dixon, C.M. (2005). Strength of weak ties. In K.E. Fisher, S. Erdelez & L. E.F. Mckechnie (Eds.), _Theories of information behavior._ (pp. 344-348). Medford, NJ: Information Today, Inc.
*   <a id="fis05"></a>Fisher, K.E., Abrahamson, J.A., Turner, A.G., Edwards, P.M., & Durrance, J.C. (2005). Lost, found, and feeling better: exploring proxy health information behavior. [Poster presentation] _Proceedings of the American Society for Information Science and Technology_, **42**(1)
*   <a id="fow95"></a>Fowler, F.J. (1995). _Improving survey questions: design and evaluation_ Thousand Oaks, CA: Sage Publications, Inc.
*   <a id="fox00"></a>Fox, S. & Rainie, L. (2000). [The online health care revolution: how the Web helps americans take better care of themselves.](http://www.Webcitation.org/5LCXvk8R9) Washington, DC: Pew Internet and American Life Project. Retrieved 17 December 2006, from http://207.21.232.103/pdfs/PIP_Health_Report.pdf
*   <a id="gol97"></a>Gollop, C. J. (1997). Health information-seeking behavior and older African-American women. _Bulletin of the Medical Library Association_, **85**(2) 141-146.
*   <a id="gra83"></a>Granovetter, M. (1983). The strength of weak ties: a network theory revisited. _Sociological Theory_, **1** 201-233.
*   <a id="gra73"></a>Granovetter, M.S. (1973). The strength of weak ties. _American Journal of Sociology_, **78**(6) 1360-1380.
*   <a id="hay96"></a>Haythornthwaite, C. (1996). Social network analysis: an approach and technique for the study of information exchange. _Library and Information Science Research_, **18**(4), 323-342.
*   <a id="mar84"></a>Marsden, P.V., & Campbell, K. C. (1984). Measuring tie strength. _Social Forces_, **63**(2), 482-501.
*   <a id="pet98"></a>Pettigrew, K.E. (1998). _The role of community health nurses in providing information and referral to the elderly: a study based on social network theory._ Unpublished Dissertation, The University of Western Ontario Canada, London, Ontario.
*   <a id="pet99"></a>Pettigrew, K.E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behaviour among attendees at community clinics. _Information Processing and Management_, **35**(6), 801-817.
*   <a id="pet00"></a>Pettigrew, K.E. (2000). Lay information provision in community settings: how community health nurses disseminate human services information to the elderly. _Library Quarterly_, **70**(1), 47-85.
*   <a id="boa06"></a>Princeton Research Associates. (2006). [The strength of internet ties](http://www.Webcitation.org/5LCMJMrmv). Washington, DC: Pew Internet and American Life Project. Retrieved January 2006, from http://www.pewinternet.org/PPF/r/172/report_display.asp.
*   <a id="spi97"></a>Spink, A., Jaeckel, G., & Sidberry, G. (1997). Information seeking and information needs of low income African American households: Wynnewood health neighbourhood project. _Proceedings of the ASIS Annual Meeting_, **34**, 271-279.
*   <a id="uab01"></a>University at Buffalo. _Center for Urban Studies_. (2001). _Health status of the Near East Side black community: a study of wellness and neighbourhood conditions_. Buffalo, NY: University at Buffalo.
*   <a id="ame06a"></a>US. _Census Bureau._ (2006a). _[American factfinder. Custom table. Data set: census 2000 summary file 3 (SF 3) - sample data](http://www.webcitation.org/5LStCtMmw)_. Washington, DC: Census Bureau. Retrieved 17 December 2006, from http://tinyurl.com/ylalvg.
*   <a id="ame06b"></a>US. _Census Bureau._ _American factfinder._ (2006b). _[American factfinder. Data Set: Census 2000 Summary File 1 (SF 1) 100-Percent Data](http://www.webcitation.org/5LT0qvmzC) _. Washington, DC: Census Bureau. Retrieved 17 December 2006, from http://tinyurl.com/yh3nwo.
*   <a id="wil00"></a>Wilson, T.D. (2000). [Human information behavior](http://www.Webcitation.org/5LCZJnFl2). _Informing Science_, **3**(2), 49-45\. Retrieved 17 December 2006 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf