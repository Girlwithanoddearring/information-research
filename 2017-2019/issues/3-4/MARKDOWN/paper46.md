

####  Vol. 3 No. 4, April 1998



# Business use of the World Wide Web: a report on further investigations

#### Hooi-Im Ng, Ying Jie Pan, and [T.D. Wilson](mailto:t.d.wilson@shef.ac.uk)  
Department of Information Studies  
University of Sheffield, U.K.

#### Abstract

> As a continuation of a previous study this paper reports on a series of studies into business use of the World Wide Web and, more generally the Internet. The use of the World Wide Web as a business tool has increased rapidly for the past three years, and the benefits of the World Wide Web to business and customers are discussed, together with the barriers that hold back future development of electronic commerce. As with the previous study we report on a desk survey of 300 randomly selected business Web sites and on the results of an electronic mail questionnaire sent to the sample companies. _An extended version of this paper has been submitted to the International Journal of Information Management_

## Introduction

This study is a follow-up to the paper by [Cockburn and Wilson](#coc),(1996) which takes into account previously unpublished research by [Pan](#pan) (1996) and by [Ng](#ng) (1997). The surveys were carried out in 1995, 1996, and 1997 and, therefore, provide a useful series of snapshots of business activity over this period and, having been carried out over a three year period, provide useful information on trends.

Today, commercial activity on the Web has increased to the point where hundreds of new companies are adding Web pages daily. In 1995, more than $83 million was spent world-wide on Web site development, and by 1998, the total Web expenditures are expected to increase to nearly $2.6 billion ([CyberAtlas, 1996](#cyb)). One study prepared on behalf of the WebSite Consultancy concluded that the main reasons for companies setting up a Web site were their concern about _"being left behind and a strong feeling that companies cannot be perceived as market leaders if they do not have a Web presence_" ([Taylor, 1997](#tay)). The Internet has already connected fifty to sixty million of the world's population and, according to some, could grow to 550 million, or 10% of the world's population by the year 2000 ([Economist, 1997](#eco)). At that point, if a store is placed anywhere on the World Wide Web, it will have, potentially, a global market.

In terms of absolute growth, Byles suggests that business-to-business commerce will grow faster in 1997 than business-to-consumer commerce (quoted by [Wilder, 1996](#wil)). Similarly, in a survey by _The Economist_ it was found that, at this stage,

> "the great profit is not in consumer shopping but in business-to-business commerce, since most business transactions were already done at a distance, whether by fax, telephone, post, or private electronic links."([Economist, 1996](#eco))

The benefits to business of using and selling over the Internet are largely unexplored, but the following are apparent on an _a priori_ basis:

1.  _the ability to establish a global presence_ ([Esprit programme, 1997](#esp));
2.  _establishing and maintaining a competitive edge_ ([Meroz, 1996](#mer));
3.  _shortening or eradication of supply chains_;
4.  substantial transaction cost savings ([Esprit programme,](#esp) 1997; [Hoffman, et al.](#hof), 1997);
5.  _market research advantage_

Meanwhile, the consumer can benefit from global choice, personalised products and services, price reductions, and the ability to increase choice through exposure to wider advertising. However, as pointed out by [Rebello, Armstrong and Cortese, (1996)](#reb) , "_all the multibillion-dollar makers in cyberspace are only a speculative gleam in forecasters' eyes"._ Before they become reality, the Web has to continue to evolve. The growth of electronic commerce will depend on better payment systems and most importantly, the confidence of consumers in the Web's security. The average level of educational and computer literacy will also need to be improved, otherwise the huge potential markets of the developing world will be particularly difficult to exploit.

## The surveys

### Methods

As with the previous studies, this survey had two parts: observation of Web sites and the electronic mail survey of sample companies. Three hundred companies were chosen from those listed in the _Yahoo! Directory_ to have their Web sites observed in detail. A systematic, stratified sampling strategy was used to ensure representation across the whole field of business activity.

Following the selection of companies, and to enable comparison with the previous studies, the company Web sites were classified according to the typology of Cockburn and Wilson :

*   A Web presence with basic information about the company but no further details on specific products or services.
*   A Web presence with company information and some information about products or services.
*   A Web presence with company information and products or services information together with some price details but with facilities for conventional purchasing only.
*   A Web presence with company information and products or services information with price details and the ability to order products or services via electronic mail (but with billing occurring conventionally).
*   A Web presence with company information and products or services information (including price details) with the ability to cope with on-line ordering and payment.
*   A Web presence with company information and products or services information (including price details) with pre-registration of credit card details by conventional means to gain account number which may be used to order goods on-line.
*   A Web presence with company information and providing free products or services.

The following categories were applied to examine the use of multimedia on the company's Web pages: Text only; Text and graphics; Text and graphics and photographs; Text and graphics or photographs with sound or video clips

To gain further insight into the trends and latest development of the application of the World Wide Web for business purposes, an electronic mail questionnaire was sent to the three-hundred randomly-selected sample companies. Out of the three-hundred selected companies, 10 of them could not be contacted because of the lack of contact e-mail addresses, whilst the remaining 290 were sent questionnaires. Fifteen delivery failures were recorded due to unknown addresses or technical problems. The reply rate of 37.5% (i.e. 103 out of 275) was achieved, However, five were irrelevant and were excluded, leaving 98 completed questionnaires, equating to a relevant response rate of 36%. The result is analysed and compared with the results from the previous two years.

### Results

#### The Number of Companies in the _Yahoo! Directory_

The number of companies registered with the _Yahoo! Directory_ expanded rapidly between August 1995 and August 1997\. The rapid growth indicates the increased awareness amongst the business community of the business potential of the World Wide Web. The Standard Industrial Classification was used to classify the companies and the breakdown of the companies, together with the 1995 figures and the percentage change, is shown in Table 1\. The table shows that the companies in the _Yahoo! Directory_ are predominantly related to real estate, business services and retailers; this is followed by manufacturing and personal services. None of the companies selected belong to the "fishing" or "Private" classes.

<table width="444" cellspacing="1" cellpadding="3" align="center" bgcolor="#FCF9BA" style="font-family: Arial;" hspace="12"><caption align="bottom">  
Table 1: Companies in the Yahoo directory</caption>

<tbody>

<tr>

<th width="55%" valign="TOP">SIC industry classification</th>

<th width="14%" valign="TOP">1995 %</th>

<th width="14%" valign="TOP">1997 %</th>

<th width="17%" valign="TOP">change %</th>

</tr>

<tr>

<td width="55%" valign="TOP">Agriculture, hunting and forestry</td>

<td align="center" width="14%" valign="TOP">1.0</td>

<td align="center" width="14%" valign="TOP">1.0</td>

<td align="center" width="17%" valign="TOP">0.0</td>

</tr>

<tr>

<td width="55%" valign="TOP">Fishing</td>

<td align="center" width="14%" valign="TOP">0.0</td>

<td align="center" width="14%" valign="TOP">0.0</td>

<td align="center" width="17%" valign="TOP">0.0</td>

</tr>

<tr>

<td width="55%" valign="TOP">Mining and quarrying</td>

<td align="center" width="14%" valign="TOP">0.7</td>

<td align="center" width="14%" valign="TOP">0.2</td>

<td align="center" width="17%" valign="TOP">-0.5</td>

</tr>

<tr>

<td width="55%" valign="TOP">Manufacturing</td>

<td align="center" width="14%" valign="TOP">31.0</td>

<td align="center" width="14%" valign="TOP">10.1</td>

<td align="center" width="17%" valign="TOP">-20.9</td>

</tr>

<tr>

<td width="55%" valign="TOP">Utilities</td>

<td align="center" width="14%" valign="TOP">1.0</td>

<td align="center" width="14%" valign="TOP">0.7</td>

<td align="center" width="17%" valign="TOP">-0.3</td>

</tr>

<tr>

<td width="55%" valign="TOP">Construction</td>

<td align="center" width="14%" valign="TOP">0.7</td>

<td align="center" width="14%" valign="TOP">2.5</td>

<td align="center" width="17%" valign="TOP">+1.8</td>

</tr>

<tr>

<td width="55%" valign="TOP">Wholesale and retail trade</td>

<td align="center" width="14%" valign="TOP">22.0</td>

<td align="center" width="14%" valign="TOP">20.4</td>

<td align="center" width="17%" valign="TOP">-1.6</td>

</tr>

<tr>

<td width="55%" valign="TOP">Hotels and restaurants</td>

<td align="center" width="14%" valign="TOP">2.0</td>

<td align="center" width="14%" valign="TOP">0.3</td>

<td align="center" width="17%" valign="TOP">-1.7</td>

</tr>

<tr>

<td width="55%" valign="TOP">Transport and communication</td>

<td align="center" width="14%" valign="TOP">9.3</td>

<td align="center" width="14%" valign="TOP">6.4</td>

<td align="center" width="17%" valign="TOP">-2.9</td>

</tr>

<tr>

<td width="55%" valign="TOP">Financial intermediation</td>

<td align="center" width="14%" valign="TOP">2.7</td>

<td align="center" width="14%" valign="TOP">4.5</td>

<td align="center" width="17%" valign="TOP">+1.8</td>

</tr>

<tr>

<td width="55%" valign="TOP">Real estate and business activities</td>

<td align="center" width="14%" valign="TOP">26.3</td>

<td align="center" width="14%" valign="TOP">36.6</td>

<td align="center" width="17%" valign="TOP">+10.3</td>

</tr>

<tr>

<td width="55%" valign="TOP">Public administration</td>

<td align="center" width="14%" valign="TOP">0.0</td>

<td align="center" width="14%" valign="TOP">0.2</td>

<td align="center" width="17%" valign="TOP">+0.1</td>

</tr>

<tr>

<td width="55%" valign="TOP">Education</td>

<td align="center" width="14%" valign="TOP">0.7</td>

<td align="center" width="14%" valign="TOP">0.7</td>

<td align="center" width="17%" valign="TOP">0.0</td>

</tr>

<tr>

<td width="55%" valign="TOP">Health</td>

<td align="center" width="14%" valign="TOP">0.7</td>

<td align="center" width="14%" valign="TOP">4.4</td>

<td align="center" width="17%" valign="TOP">+3.7</td>

</tr>

<tr>

<td width="55%" valign="TOP">Community and personal services</td>

<td align="center" width="14%" valign="TOP">2.0</td>

<td align="center" width="14%" valign="TOP">12.1</td>

<td align="center" width="17%" valign="TOP">+10.1</td>

</tr>

<tr>

<td width="55%" valign="TOP">Private</td>

<td align="center" width="14%" valign="TOP">0.0</td>

<td align="center" width="14%" valign="TOP">0.0</td>

<td align="center" width="17%" valign="TOP">0.0</td>

</tr>

<tr>

<td width="55%" valign="TOP">Extra-territorial organisation</td>

<td align="center" width="14%" valign="TOP">0.0</td>

<td align="center" width="14%" valign="TOP">0.02</td>

<td align="center" width="17%" valign="TOP">+0.02</td>

</tr>

</tbody>

</table>

Table 1 also shows that, since 1995, there have been only slight changes in percentage for most of the classes. However, companies under "manufacturing" have fallen from 31% in 1995 to 10.1% in 1997\. On the other hand, the number of companies under "business activities" and "personal services" has increased about 10% over the same period.

#### Size of the Companies

In general, it would be expected that the size (in term of number of personnel) of a company would affect its business strategies. It was not possible to discover the size of a company from its Web site alone and, therefore, the e-mail questionnaire (discussed later) was used to collect information on company size, measured by the number of employees. The results show that a large proportion of businesses sites (67.3%) belong to companies that employ fewer than fifty employees. Only about 18.4% of the sample companies are distributed over the groups covering 50 to 299 employees. However, the "more than 300" category includes a significant number of very large international companies with more than 100,000 employees (about 15.3% of companies can be considered "large").

#### Categorisation of business users according to the Typology of Business Use

The _Yahoo! Directory_ recorded 205,596 companies under "Business and Economy", at the 9<sup>th</sup> of June 1997\. Very diverse types of businesses are using the World Wide Web with "Computer related" as the most common(16.33%). The major reasons are: first, the services and products involved are closely linked to the Internet applications; and secondly they are easily demonstrated and transported to the customer through the Internet (e.g., software). In addition, most Internet users, who are the prospective customers targeted by the computer companies are computer literate.

"Entertainment" is another strong sector (14.1%) due to the inclusion of sports and the rapid growth in the presence of the music. "Engineering and manufacturing" also features heavily at 11.95%, mainly due to the fast growing automotive-related industries. Lower percentages are found for "transport, travel, storage and communication" (6.25%) and "health" (5.11%). The rest of the classes contain small percentages of companies.

Table 2 shows that only five of the nineteen classes have increased since 1996, particularly in the "miscellaneous" class, which contains a wide variety of companies. Year on year, the increases are small - the highest increase in 1997 is +8.2% (Miscellaneous) and the highest decrease is -5.3%. It should be borne in mind that changes as small as those shown could well be a result of changes in the sampling procedures.

<table width="527" cellspacing="1" cellpadding="3" align="center" bgcolor="#FCF9BA" style="font-family: Arial;"><caption align="bottom">  
Table 2: Changes in percentage of companies in Yahoo! Directory</caption>

<tbody>

<tr>

<th width="37%" valign="TOP">Industry classification</th>

<th width="12%" valign="TOP">1995 %</th>

<th width="12%" valign="TOP">1996 %</th>

<th width="15%" valign="TOP">change %</th>

<th width="12%" valign="TOP">1997 %</th>

<th width="14%" valign="TOP">change %</th>

</tr>

<tr>

<td width="37%" valign="TOP">Financial services</td>

<td align="center" width="12%" valign="TOP">3.9</td>

<td align="center" width="12%" valign="TOP">4.6</td>

<td align="center" width="15%" valign="TOP">_+0.8_</td>

<td align="center" width="12%" valign="TOP">4.5</td>

<td align="center" width="14%" valign="TOP">_-0.1_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Real estate, renting and business activities</td>

<td align="center" width="12%" valign="TOP">14.2</td>

<td align="center" width="12%" valign="TOP">15.6</td>

<td align="center" width="15%" valign="TOP">_+1.4_</td>

<td align="center" width="12%" valign="TOP">10.4</td>

<td align="center" width="14%" valign="TOP">_-5.2_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Computer related</td>

<td align="center" width="12%" valign="TOP">22.5</td>

<td align="center" width="12%" valign="TOP">21.6</td>

<td align="center" width="15%" valign="TOP">_-0.9_</td>

<td align="center" width="12%" valign="TOP">16.3</td>

<td align="center" width="14%" valign="TOP">_-5.3_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Internet related</td>

<td align="center" width="12%" valign="TOP">13.9</td>

<td align="center" width="12%" valign="TOP">6.0</td>

<td align="center" width="15%" valign="TOP">_-7.9_</td>

<td align="center" width="12%" valign="TOP">1.7</td>

<td align="center" width="14%" valign="TOP">_-4.3_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Transport, travel, storage and communication</td>

<td align="center" width="12%" valign="TOP">10.4</td>

<td align="center" width="12%" valign="TOP">10.5</td>

<td align="center" width="15%" valign="TOP">_+0.1_</td>

<td align="center" width="12%" valign="TOP">6.2</td>

<td align="center" width="14%" valign="TOP">_-4.3_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Utilities</td>

<td align="center" width="12%" valign="TOP">0.4</td>

<td align="center" width="12%" valign="TOP">0.7</td>

<td align="center" width="15%" valign="TOP">_+0.3_</td>

<td align="center" width="12%" valign="TOP">0.4</td>

<td align="center" width="14%" valign="TOP">_-0.3_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Construction</td>

<td align="center" width="12%" valign="TOP">0.4</td>

<td align="center" width="12%" valign="TOP">1.5</td>

<td align="center" width="15%" valign="TOP">_+1.1_</td>

<td align="center" width="12%" valign="TOP">3.2</td>

<td align="center" width="14%" valign="TOP">_+1.7_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Education</td>

<td align="center" width="12%" valign="TOP">1.0</td>

<td align="center" width="12%" valign="TOP">1.8</td>

<td align="center" width="15%" valign="TOP">_+0.8_</td>

<td align="center" width="12%" valign="TOP">0.7</td>

<td align="center" width="14%" valign="TOP">_-1.1_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Retailing</td>

<td align="center" width="12%" valign="TOP">6.4</td>

<td align="center" width="12%" valign="TOP">5.1</td>

<td align="center" width="15%" valign="TOP">_-1.2_</td>

<td align="center" width="12%" valign="TOP">3.5</td>

<td align="center" width="14%" valign="TOP">_-1.6_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Mining and quarrying</td>

<td align="center" width="12%" valign="TOP">0.2</td>

<td align="center" width="12%" valign="TOP">0.1</td>

<td align="center" width="15%" valign="TOP">_-0.1_</td>

<td align="center" width="12%" valign="TOP">0.2</td>

<td align="center" width="14%" valign="TOP">_+0.1_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Agriculture</td>

<td align="center" width="12%" valign="TOP">0.1</td>

<td align="center" width="12%" valign="TOP">0.4</td>

<td align="center" width="15%" valign="TOP">_+0.3_</td>

<td align="center" width="12%" valign="TOP">0.8</td>

<td align="center" width="14%" valign="TOP">_+0.6_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Publishing</td>

<td align="center" width="12%" valign="TOP">6.6</td>

<td align="center" width="12%" valign="TOP">5.6</td>

<td align="center" width="15%" valign="TOP">_-1.0_</td>

<td align="center" width="12%" valign="TOP">4.5</td>

<td align="center" width="14%" valign="TOP">_-1.1_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Entertainment</td>

<td align="center" width="12%" valign="TOP">9.8</td>

<td align="center" width="12%" valign="TOP">11.0</td>

<td align="center" width="15%" valign="TOP">_+1.2_</td>

<td align="center" width="12%" valign="TOP">14.1</td>

<td align="center" width="14%" valign="TOP">_+3.1_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Food</td>

<td align="center" width="12%" valign="TOP">1.6</td>

<td align="center" width="12%" valign="TOP">1.6</td>

<td align="center" width="15%" valign="TOP">_0.0_</td>

<td align="center" width="12%" valign="TOP">3.4</td>

<td align="center" width="14%" valign="TOP">_+1.8_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Engineering and manufacturing</td>

<td align="center" width="12%" valign="TOP">3.4</td>

<td align="center" width="12%" valign="TOP">7.2</td>

<td align="center" width="15%" valign="TOP">_+3.8_</td>

<td align="center" width="12%" valign="TOP">12.0</td>

<td align="center" width="14%" valign="TOP">_+4.8_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Scientific and environmental</td>

<td align="center" width="12%" valign="TOP">1.2</td>

<td align="center" width="12%" valign="TOP">3.0</td>

<td align="center" width="15%" valign="TOP">_+1.8_</td>

<td align="center" width="12%" valign="TOP">1.3</td>

<td align="center" width="14%" valign="TOP">_-1.7_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Miscellaneous</td>

<td align="center" width="12%" valign="TOP">2.6</td>

<td align="center" width="12%" valign="TOP">2.7</td>

<td align="center" width="15%" valign="TOP">_+0.1_</td>

<td align="center" width="12%" valign="TOP">10.9</td>

<td align="center" width="14%" valign="TOP">_+8.2_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Shopping centres</td>

<td align="center" width="12%" valign="TOP">1.4</td>

<td align="center" width="12%" valign="TOP">0.8</td>

<td align="center" width="15%" valign="TOP">_-0.6_</td>

<td align="center" width="12%" valign="TOP">0.6</td>

<td align="center" width="14%" valign="TOP">_-0.2_</td>

</tr>

<tr>

<td width="37%" valign="TOP">Health</td>

<td align="center" width="12%" valign="TOP">-</td>

<td align="center" width="12%" valign="TOP">-</td>

<td align="center" width="15%" valign="TOP">_-_</td>

<td align="center" width="12%" valign="TOP">5.1</td>

<td align="center" width="14%" valign="TOP">_-_</td>

</tr>

</tbody>

</table>

#### Internet access and WWW presence

The use of Internet for business purposes has increased dramatically, since it is widely recognised as an efficient and cost-effective way for business world to communicate among themselves. In the e-mail survey the companies were asked to state how long they had had access to the Internet in general and the results compared with those from previous years. Figure 3 below compares the data over time and shows that the majority of companies in this survey have had an internet presence for more than one year, and that a significant minority of this year's sample has had a presence virtually since the early days of the World Wide Web.

<div>![](p46fig1.gif)</div>

<div>  
Figure 1: Access to the Internet, 1995-1997</div>

On the other hand, the percentage of business having had access to the Internet for less than six months is falling, suggesting that the increase in the number of new entrants is not exponential as was experienced two years ago. For the period between six months to one year, although there was a slight growth in 1996, there was a fall to 15% from 26% in 1996\.

The sample companies were also questioned about how long they have had a Web presence and the results were compared with previous years. Figure 2 shows a close relationship to the figure illustrating Internet access, suggesting that the World Wide Web has been the main factor attracting business. The Figure also shows that the largest distribution of length of Web presence for this year's sample is between six months and two years (61%). This result illustrates the rapid growth of the World Wide Web during the past two years. Only about 25% of the companies said that they have had Web presence for more than two years.

<div>![](p46fig2.gif)</div>

<div>  
Figure 2: Web presence, 1995-1997</div>

#### The Purpose of Web Sites

The Web sites of the sample companies were categorised according the typology described above, to identify the nature of the Web activity. The the majority of the sample companies use their Web sites to publish information about their companies and their products or services, though without any price information.

The next ranked categories of use are: to provide price information (15.3%), on-line shopping (15.3%) and e-mail order (12%). The first of these is similar to basic Web presence, but encouraging the potential customer to consume by including price information alongside products or services details. However, the customers have to order the products or services by conventional means. In spite of the issue of payment security, most of the companies observed have operated without sophisticated security measures. E-mail ordering refers to companies that allow customers to send the order via e-mail but the payment is still carried out using conventional methods, such as credit cards. Only a small percentage of the sample companies use registration schemes to provide their customers with account identity numbers(protected by user-defined passwords) for direct purchase (4.3%). Examination of the sites showed little change in the distribution of different uses, over a three-year period.

The results of the observation of Web sites was borne out by the responses to the e-mail survey, in which the sample companies were asked to state the purpose of their Web sites in terms of the categories shown in Table 3 (respondents were allowed to select one or more classes of business purpose):

<table align="center" width="367" cellspacing="1" cellpadding="3" style="background-color: #F5FFB3; border: thin solid #ECFF62; font-family: Arial;" hspace="12"><caption align="bottom">  
Table 3: Use of Web sites</caption>

<tbody>

<tr>

<td width="84%" valign="TOP">General publicity</td>

<td width="16%" valign="TOP">71.4%</td>

</tr>

<tr>

<td width="84%" valign="TOP">Advertising of specific products and services</td>

<td width="16%" valign="TOP">67.3%</td>

</tr>

<tr>

<td width="84%" valign="TOP">On-line selling of products and services</td>

<td width="16%" valign="TOP">38.8%</td>

</tr>

<tr>

<td width="84%" valign="TOP">Customer support and liaison</td>

<td width="16%" valign="TOP">36.7%</td>

</tr>

</tbody>

</table>

#### Cost of setting up Web site

It is known that one of the reasons of the rapid growth of the World Wide Web is the low cost of set up a Web site. The companies were asked about both the costs of setting up and maintaining a Web site. 72.4% of companies said that the cost of setting up a Web site was not more than $10,000 a year and 76.5% also stated that the cost maintaining the site was not more than $10,000 a year. Web-based publicity, therefore, is a relatively cheap way of selling the companies to the public compared to other means such as advertising on television or in magazines, or in sponsoring sports.

#### Use of multimedia

One of the main attractions of the World Wide Web for the business community is the ability to develop Web pages containing graphics, photographs, sound and video or animation together with texts. The sample companies were investigated to find out how many of them have adopted multimedia. Forty-two percent of the sample companies use text, graphics and photographs in their Web pages and 27% use sound or video, in addition to text, graphics and photographs. This is followed by about 24.7% using text and graphics. Only 6.3% of companies are making no attempt at all to produce visually interesting Web sites. Thus, the majority of the sample are making use of multimedia in order to attract users, as well as to give potential customers a clearer picture of their products.

#### The use of electronic mail

Electronic mail is the most widely used application on the Internet. The sample sites were examined to determine how they were making use of electronic mail, using the following categories:

*   No e-mail address advertised on the web site.
*   E-mail enquiries encouraged only about the Web pages themselves.
*   E-mail enquiries encouraged in relation to information about the company or its products or services.
*   Orders for products or services sent via e-mail.
*   Credit card information sent over the Internet via e-mail.

<div>![](p46fig3.gif)</div>

<div>Figure 3: Use of electronic mail</div>

As Figure 3 shows, a large percentage of the companies surveyed encourage users to mail them with enquiries regarding the company itself, product information including price details, problems they might be experiencing or any other general questions. In addition, comments about the content of the Web pages are commonly welcomed. About 31% of the companies have contact e-mail addresses attached to their Web site which are meant for users who encounter technical difficulties or problems in the company's Web sites. Only a small percentage of companies do not have any e-mail addresses advertised on their Web sites. Approximately 35% of the companies have more than one e-mail address on their Web pages.

#### Other features

Apart from the factors discussed above, there are other interesting features of Web site that point to trends in Web development. Features such as recruitment, advertising, survey and search facilities have appeared to become a common feature on the Web pages.

In rank order:

*   about 20% of sample companies provide search facilities to the users. Most of the searching facilities are used for searching information for particular products or services, but some are the common search engines such as Yahoo!, Excite, and HotBot.
*   about 15% use the Web as a tool for recruiting potential employees.
*   about 14% of sample companies have allocated specific columns for advertisement on their sites. The majority of these advertisements are to Link Exchange (which provides automatic links to other member sites of the system without charge) although some of the advertisements are commercial.
*   a small percentage of companies (4.7%) have on-line questionnaires for visitors to complete. This information will be used for marketing purposes and to gain ideas on how to improve the Web pages.

### The impact

#### Effectiveness of World Wide Web

It is an undisputed fact that the popularity of the World Wide Web has grown rapidly in the business community. However, it is useful to ask how the companies involved in the e-mail survey rate the effectiveness of World Wide Web for typical commercial applications such as publicity, advertising, online selling and customer service as compared to other media. The results are shown in Figure 4.

<div>![](p46fig4.gif)</div>

<div>  
Figure 4: Perceived effectiveness of Web site for particular purposes</div>

The results show that companies view positively their use of the World Wide Web, as most rated all of the surveyed areas on the upper bands of scale. The Web is seen as an effective mean of enhancing publicity and advertising the purpose and characteristics of products and service, but quite a large percentage (approximately 20%) of companies do not have any experience in the areas of on-line selling and customer support. This is mainly due to immature payment and security systems and it can be expected that commercial activities will increase as soon as these technologies become inexpensive. This is clearly indicated by the expectations and confidence expressed by the companies on the future of World Wide Web (Figure 5) when they were asked about the prospect of such applications.

<div>![](p46fig5.gif)</div>

<div>  
Figure 5: Perceived value of the Web for particular purposes</div>

#### Direct Transactions

Another survey question is aimed to establish the extent of direct business transaction in the World Wide Web and the payment methods currently employed by the sampled companies. The survey result showed that 37% of the companies carry out direct business transactions on the Web, with credit card payment schemes being most widely accepted as the most secure and well-established method of payment. Just over 30% of companies used this method: the other payment schemes included E-cash, customer ID account, cheque and money order.

#### Effect of Web site on turnover

To find out if owning a Web site is currently making any difference to business, companies were asked about the effect of their site on turnover. A majority (56%) indicated that their presence on the World Wide Web did not produce any noticeable change in their turnover, whilst 13% did not know of any effect. This may be due to some of the respondent having had their Web presence only for a few months and it being too early to determine the effect. In some cases, it may be impossible to tell if the presence on the Web has had any effect, as, at this stage of development, most of the companies are still trying to find the right strategy for conducting business on the Web. On the other hand, about 31% of the companies stated that their Web sites did make a difference to their turnover. The companies who claimed that their Web sites had had a positive effect on turnover were from many different industry sectors, but approximately 30% of them are computer-related companies and booksellers.

The percentage of respondents stating that the Web has enhanced their turnover is quite similar in 1995\. 1996 and 1997\. However, there is an increase of about 20% in the percentage of companies saying that the presence of their company on the web does not produce an improvement in their turnover. It may be, of course, that companies are actually unable to determine what is the impact of Web publicity and advertising, since they may not have in place the systems and mechanisms to separate the impact of this medium from those of the media they normally use. This would be particularly the case for those companies that do not engage in on-line selling.

#### Problems facing electronic commerce

Electronic commerce must overcome a number of problems before it can emerge as a dominant form of business. The companies were asked what they believed to be the main hurdles to the success of electronic commerce by selecting one or more of the following categories:

*   Development of secure sites
*   Development of suitable payment systems (e.g. e-cash)
*   Faster connection times
*   Wider access
*   Information overload for potential customers
*   The unattractiveness of on-line shopping
*   Other

Many respondents believe that the most important problems facing the future of electronic commerce are security (57.1%) and connection time (49%). The priority given to security is not surprising, since it has been perceived to be a problem since electronic commerce was first mooted and the business community believes that the general public will not be comfortable in carrying out business transactions on the World Wide Web until they are convinced of its security. Regarding the other technical difficulties of connection time, it is believed that unless the connection time become faster, the Web will put off many frustrated users. Of course, with developments under way, these issues may be resolved in the near future although, in the UK, the position is linked to the issue of charges for local telephone calls.

The issue of payment is viewed as the next important problem (37.8%). Although electronic payment schemes such as E-cheque and digital-cash are being introduced, they remain embryonic and much needs to be done before they can receive a wider acceptance. The issue of access also is considered as an important problem, with a percentage slightly lower than payment issue (36.7%). In general, many businesses feel that it is necessary for more potential customers to gain access to the Web. From the view point of companies, information overload is not seen as a great problem (23.5%). The issue of unattractiveness of business sites (14.3%) is also rated by companies to be trivial.

Almost ten percent (9.7%) of respondents thought that there were other important problems facing electronic commerce. Several respondents were concerned that the business community and general public will not accept the World Wide Web as a valid commercial medium. A few respondents felt that the difficulty in searching for the right products as well as a conservative business approach are the current problems faced by electronic commerce.

<div>![](p46fig6.gif)</div>

<div>  
Figure 6: Problems faced by electronic commerce, 1995-1997</div>

In comparison with the previous two years, opinion on the problems of electronic commerce is much the same. The security and connection issues are significant problems, but the problem of means of payment appears to have declined in perceived importance by about 20%.

#### Future of Web Sites

<table align="center" border="" cellspacing="1" cellpadding="3" width="493" style="background-color: #F5FFB3; border: thin solid #ECFF62; font-family: Arial;"><caption align="bottom">  
Table 4: Future enhancements to Web sites</caption>

<tbody>

<tr>

<th width="68%" valign="TOP">Enhancement</th>

<th width="32%" valign="TOP">No. of respondents</th>

</tr>

<tr>

<td width="68%" valign="TOP">More interaction with users</td>

<td align="center" width="32%" valign="TOP">17</td>

</tr>

<tr>

<td width="68%" valign="TOP">More general content to be added</td>

<td align="center" width="32%" valign="TOP">9</td>

</tr>

<tr>

<td width="68%" valign="TOP">The addition of more products and services</td>

<td align="center" width="32%" valign="TOP">15</td>

</tr>

<tr>

<td width="68%" valign="TOP">More business</td>

<td align="center" width="32%" valign="TOP">8</td>

</tr>

<tr>

<td width="68%" valign="TOP">The inclusion of "fun pages"</td>

<td align="center" width="32%" valign="TOP">2</td>

</tr>

<tr>

<td width="68%" valign="TOP">The addition of links to and from the page</td>

<td align="center" width="32%" valign="TOP">2</td>

</tr>

<tr>

<td width="68%" valign="TOP">Sharing of information and building a community</td>

<td align="center" width="32%" valign="TOP">7</td>

</tr>

<tr>

<td width="68%" valign="TOP">On-line ordering and shopping</td>

<td align="center" width="32%" valign="TOP">12</td>

</tr>

<tr>

<td width="68%" valign="TOP">Uncertain of future direction</td>

<td align="center" width="32%" valign="TOP">7</td>

</tr>

<tr>

<td width="68%" valign="TOP">More advertising</td>

<td align="center" width="32%" valign="TOP">3</td>

</tr>

<tr>

<td width="68%" valign="TOP">The inclusion of forms</td>

<td align="center" width="32%" valign="TOP">0</td>

</tr>

<tr>

<td width="68%" valign="TOP">The site will remain as it is</td>

<td align="center" width="32%" valign="TOP">3</td>

</tr>

<tr>

<td width="68%" valign="TOP">Search facility</td>

<td align="center" width="32%" valign="TOP">5</td>

</tr>

<tr>

<td width="68%" valign="TOP">Frequent updating</td>

<td align="center" width="32%" valign="TOP">7</td>

</tr>

<tr>

<td width="68%" valign="TOP">Other</td>

<td align="center" width="32%" valign="TOP">10</td>

</tr>

</tbody>

</table>

The companies were asked to describe how they see the future development of their Web site. The ideas given varied widely. For consistency, the classification scheme created by [Cockburn & Wilson](#coc) (1996) is used to group these comments. However, two new classes are added, that is, search facility and the Web site being frequently updated. The classes and number of entries (out of 80 replies to this question) are given in the Table 4.

It is anticipated that the marketing strategies based on interactive approaches and addition of more product and service information in a more exciting format will become more common, for example:

> "Developing into an electronic catalogue which in turn develops a mailing list of potential customers that we can e-mail special promotions to keep ourselves in their minds."

> "It will expand to include new products and services, which includes customer access to their information so they can make desired changes to their data"

Some of the respondents stated that there is a need to promote their web sites to the general public, quote:

> "I have to invest the money in many advertisements throughout selected shopping malls."

> "Better marketing and promotion of the Web site to drive prospective customers to the site."

## Conclusions

Apart from the increasing number of businesses involved in making use of the Internet and the World Wide Web, this survey reveals an increasing sophistication in use as well as increasing diversity of business sectors. In 1995, the dominant sector, by a large margin, was the computer sector: today, other sectors, such as business services, financial intermediation, and entertainment have shown significant growth. Among these companies, the development of multimedia is gaining force and becoming accepted by business as a way of increasing the impact of their publicity or of increasing sales. It is likely that, as technology progresses, the design of Web sites will become more interesting and attractive. In this survey, it was also noticed that the virtual shopping centres have adopted this technology more quickly than individual businesses on the Web. Electronic mail is widely used at most of the sites observed and will remain an important tool for communication, particularly between businesses and users.

This survey clearly indicates that electronic commerce is still in its infancy, but has great potential. Of the 300 companies surveyed, only 15.3% of companies were engaged in on-line transaction. In addition, the number of the secure transaction sites is still low. This may well be a barrier to the visitor considering buying products or services over the Internet.

The results obtained from the electronic mail questionnaire made a useful supplement to the observational findings. Most of the respondent companies have had access to the Internet for less than three years and had their presence on the Web for under two years. The majority of companies are using their Web sites for publicity and advertising purposes. Most companies looked to the potential of World Wide Web as a marketing tool. For the sites with direct transaction facility, credit cards appear to be the most popular payment mode. From this survey, in spite of the problem of the security, the opinions given by the respondents suggest that businesses will continue to adopt the Internet as a means of enlarging their markets and, indeed, that in some sectors, the World Wide Web will enable small and medium-sized companies to tap into a global market that would otherwise be out of their reach.

## References

*   <a name="coc"></a>Cockburn C. & Wilson T.D.(1995) [Business Use of the World Wide Web](http://InformationR.net/ir/paper6.html). _[Information Research](http://InformationR.net/ir/)_,**1**(2) Site accessed: 22/1/1997\. (See also: Cockburn, C. & Wilson, T.D. "Business Use of the World Wide Web" _International Journal of Information Management_, **16**, 83-102)
*   <a name="cyb"></a>[CyberAtlas](http://www.cyberatlas.com/).(1996) (http://www.cyberatlas.com/) CyberAtlas. Site accessed: 3/2/97.
*   <a name="eco"></a>Economist.(1996) [In search of the perfect market](http://www.economist.com/surveys/elcom/). (http://www.economist.com/surveys/elcom/) Economist. Site accessed: 15/5/97.
*   <a name="esp"></a>Esprit Programme.(1997) [Electronic commerce - An introduction](http://www.cordis/lu/esprit/src/ecomint.htm=INTRODUCTION). (http://www.cordis/lu/esprit/src/ecomint.htm=INTRODUCTION) European Community. Site accessed: 2/7/97.
*   <a name="hof"></a>Hoffman DL, Novak TP, Chattejee P. (1997) [Commercial scenarios for the Web: opportunities and challenges](http://www.2000.ogsm.vanderbilt.edu/patrali/). (http://www.2000.ogsm.vanderbilt.edu/patrali/) Site accessed: 2/7/97.
*   <a name="mer"></a>Meroz Y.(1996) [Commercialization of the Internet](http://ils.unc.edu/yael/commerce). (http://ils.unc.edu/yael/commerce) Site accessed: 20/2/97.
*   <a name="ng"></a>Ng, H-I.(1997) Business use of the World Wide Web [MSc in Information Management]. Sheffield: University of Sheffield, 1997.
*   <a name="pan"></a>Pan YJ.(1996) Business Use of the World Wide Web. [MSc. in Information Management] University of Sheffield, 1996.
*   <a name="reb"></a>Rebello K.(1996) [Making Money on the Net](http://www.businessweek.com/). (http://www.businessweek.com/) Business Week. Site accessed: 15/3/97\.
*   <a name="tay"></a>Taylor D. (1997) [Inside the firewall - What's an intranet? Ways you can spin an internal Web site](http://www.infoworld.com/). (http://www.infoworld.com/) InforWorld, 18(14). Site accessed: 3/7/97.
*   <a name="wil"></a>Wilder C.(1996) [Online transactions: pouring cash into the Internet](http://techweb.cmp.com/iw/560/). (http://techweb.cmp.com/iw/560/) Site accessed: 20/2/97.
