<!DOCTYPE html>
<html lang="en">

<head>
	<title>Representing WWW navigational data: a graphical methodology to support qualitative analysis</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="keywords"
		content="graphical method, World Wide Web, navigation, behaviour, qualitative analysis, medicine, textual data considered">
	<meta name="description"
		content="This paper concerns the development and use of a graphical methodology in the analysis of individuals' World Wide Web navigational behaviour. Specifically, it reports on the comparison of this methodology to other methodologies of qualitative analysis. Examples are given of the graphical representations of individuals' interactions with particular World Wide Web medical resources, and the integration of these graphical representations with textual data considered. Indications for further development in this area are also suggested.">
	<meta name="VW96.objecttype" content="Document">
	<meta name="ROBOTS" content="ALL">
	<meta name="DC.Title"
		content="Representing WWW navigational data: a graphical methodology to support qualitative analysis">
	<meta name="DC.Creator" content="Honey Lucas">
	<meta name="DC.Subject"
		content="graphical method, World Wide Web, navigation, behaviour, qualitative analysis, medicine, textual data considered">
	<meta name="DC.Description"
		content="This paper concerns the development and use of a graphical methodology in the analysis of individuals' World Wide Web navigational behaviour. Specifically, it reports on the comparison of this methodology to other methodologies of qualitative analysis. Examples are given of the graphical representations of individuals' interactions with particular World Wide Web medical resources, and the integration of these graphical representations with textual data considered. Indications for further development in this area are also suggested.">
	<meta name="DC.Publisher" content="Professor T.D. Wilson">
	<meta name="DC.Coverage.PlaceName" content="Global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4>Information Research, Vol. 4 No. 3, February 1999</h4>
	<h1>Representing WWW
		navigational data: a graphical methodology to support qualitative analysis</h1>
	<h4><a href="MAILTO:hs_lucas@yahoo.com">Honey Lucas</a></h4>
	<p>Research Student<br>
		Department of Information Studies<br>
		University of Sheffield, Sheffield, UK</p>
	<h4>Abstract</h4>
	<blockquote>
		<p>This paper concerns the development and use of a graphical methodology in the analysis of individuals' World
			Wide Web navigational behaviour. Specifically, it reports on the comparison of this methodology to other
			methodologies of qualitative analysis. Examples are given of the graphical representations of individuals'
			interactions with particular World Wide Web medical resources, and the integration of these graphical
			representations with textual data considered. Indications for further development in this area are also
			suggested.</p>
	</blockquote>
	<h2>1. Introduction</h2>
	<p>The growth of the World Wide Web (WWW, Web) in recent years has led to its becoming an important information
		resource for any number of academic subject domains. Of these, medicine finds itself particularly well
		represented, and a large number of Web resources are now available for nurses, doctors and researchers at both
		student and practitioner levels. Given the size and variety of such sites, however, the effectiveness with which
		medical students and practitioners at all levels can use different resources may vary. The need for careful
		observation and analysis of actual user behaviour in these circumstances is summarised by Raya Fidel's (1991)
		comment that 'understanding user behavior is essential to the design of advanced, user-orientated systems'. As a
		consequence, my study has sought to model actual user responses and stated needs to medical Web resources
		displaying a variety of information-retrieval characteristics.</p>
	<p>Specifically, this study has sought to:</p>
	<ul>
		<li>Identify patterns of use of WWW resources</li>
		<li>Analyse users' perceptions of WWW resources and of the information-retrieval mechanisms displayed by these
			resources</li>
		<li>Assess whether users' requirements are being met by existing resources</li>
	</ul>
	<ul>
		<li>all within a discrete subject domain. An alternative way to phrase these objectives would be to ask the
			questions: why do Web resources display a variety of information-retrieval characteristics? Or,
			alternatively: what effect does the availability of these different information-retrieval characteristics
			have on the behaviour of users?</li>
	</ul>
	<p>This paper will I hope go some way to explain the methodology by which I have attempted to answer these
		questions, and as such is an extension and up-dating of a paper given at the 5<sup>th</sup> International ISKO
		Conference, at Lille<sup>1</sup>.</p>
	<h2>2. Methodology</h2>
	<p>Thirty volunteers were drawn from several fields of nursing and medicine at undergraduate, postgraduate,
		researcher and practitioner levels here at the University of Sheffield. Figure 1 shows the break-down of these
		30 into the various groups represented:</p>
	<p><img src="p59fig1.gif" alt="Figure 1"></p>
	<p>Figure 1: Composition of Sample Group</p>
	<p>Each volunteer was qualitatively interviewed to establish the context of their habitual information-seeking
		behaviour. Data concerning their history of Web use was also collected, and Riding's cognitive styles test was
		administered and its results recorded. Volunteers were then asked to perform a search on two medical World Wide
		Web resources, each of which offered both search-based and directory-based opportunities for accessing
		information. During the searching exercise, a Think Aloud Protocol was created, and observational data
		collected. Finally, a last qualitative interview collected the volunteer's thoughts about the exercise he or she
		had just undertaken.</p>
	<h2>3. Analysis of the Data</h2>
	<p>The qualitative data collected from these interviews and Think Aloud Protocols has been analysed using Glaser and
		Strauss' Grounded Theory. The observational data collected during the searching exercise was then used as the
		basis for a graphical representations of each volunteer's unique interactions with each system. By using a
		graphical methodology to display each volunteer's navigational behaviour, it was then possible to collect more
		qualitative and quantitative data from the diagrams themselves, in line with Tufte's (1983) statement that
		'graphics reveal data'.</p>
	<h2>4. Symbols Employed</h2>
	<p>Before considering one of the completed diagrams, it is worth pausing for a moment to explain the choice of the
		symbols used, and which are displayed in Figure 2.</p>
	<p><img src="p59fig8.gif" alt="Figure 2"></p>
	<p>Figure 2: Symbols Used</p>
	<p>The first important point to make concerns the visibility of the highly distinct 'Navigational Option' and 'Input
		Search Terms' symbols. These provide an instant indication of the information retrieval mechanism selected by
		the user at different periods of the interaction: either using his or her own query terms, or using existing
		navigational options provided by the system: and these would be for instance, 'Return to Search Screen',or 'Next
		Records'. Similar navigational behaviour is displayed in the use of the grey 'Browser Functions' symbol, which
		allows for the use of 'Back', 'Forward' or 'Stop' at Browser level to be indicated. At the next stage of the
		interaction, the results of the navigational choices made by the user are discernible in the 'Output', 'Web
		Site' and 'Document' symbols. These help to isolate the system reaction to the search terms or navigational
		options chosen by the user. The user reaction to this output may be clearly seen by the progress of the
		interaction, as indicated in strict sequence by the arrow, or 'Sequence Indicator'. Further indication of the
		approval with which the user perceived documents or sites during the search is provided by the tick 'Useful'.
		Lastly, and for thoroughness, the 'Other Action Required' symbol allows unusual or unexpected actions taken by
		the user to be represented in their correct moment in the interaction.</p>
	<h2>5. Application of Methodology</h2>
	<p>Figure 3 shows the graphical representation of the interaction of volunteer MA04 and the Internet Grateful Med
		system<sup>2</sup>, found at the US National Library of Medicine's website.</p>
	<p><img src="p59fig3.gif" alt="Figure 3"></p>
	<p>Figure 3</p>
	<p>MA04's choice of information-retrieval mechanism available at different stages of his search, and at different
		locations within the site, is clear: green, 'Input Search Terms' boxes show the use of the volunteer's own query
		terms, and blue, 'Navigational Option' rectangles can also be clearly seen. The process whereby each of these
		mechanisms has been employed at different stages of the search, and in response to a variety of search outcomes
		is also highly visible by use of the 'Sequence Indicators'. Thus, the use of this methodology of graphically
		representing user navigational behaviour presents the researcher with a number of useful and immediate insights
		into the choices of information retrieval mechanism used by the volunteer during the interaction. Analysis of
		the graphical representation of each interaction therefore allows detailed data concerning their use, re-use or
		abandonment of specific methods of interaction with each system to be seen. The provision of this holistic and
		visual overview of all actions and responses made during the interaction, in their correct sequence, gives the
		researcher the ability to obtain secondary and useful impressions from the consideration of the navigational
		options selected and the pattern of navigational process displayed both for each individual interaction and
		comparatively between interactions.</p>
	<p><img src="p59fig4.gif" alt="Figure 4"></p>
	<p>Figure 4</p>
	<p>By contrast, Figure 5 shows that MB01 took 16 actions and almost exclusively used existing navigational options.
	</p>
	<p><img src="p59fig5.gif" alt="Figure 5"></p>
	<p>Figure 5</p>
	<p>Lastly, Figure 6 shows that MA01 can be seen to have used a massive 51 actions in 13 minutes, and used both her
		own query terms and navigational options, as well as very extensive use of the browser function Back.</p>
	<p><img src="p59fig6.gif" alt="Figure 6"></p>
	<p>Figure 6</p>
	<h2>6. Benefits to other kinds of analysis</h2>
	<p>But what influence can such diagrams have on the analysis of the data? These diagrams, apart from being a
		convenient way in which to represent the navigational actions of each volunteer, also assist the researcher in
		understanding and collecting data of other kinds. Principally, there are three areas in which this process may
		occur:</p>
	<ul>
		<li>Guidance to interesting and important points within the qualitative data</li>
		<li>Creation of new quantitative data from the diagrams themselves</li>
		<li>Comparison of the diagram to the results of Riding's Cognitive Styles Analysis</li>
	</ul>
	<p>I will now go on to look at the first two of these points in depth, and consider important indications for future
		research with the last point.</p>
	<h3>6.1 Guidance To Important Points Within The
		Qualitative Data</h3>
	<p>During the conduct of this study, several elements of interest have arisen concerning the use of the differing
		navigational options and their effects on system response, search outcome and the user. Preliminary indications
		of such elements of interest can be provided by the diagrams, and these can then be followed in the qualitative
		data.</p>
	<p>To consider Figure 3 once again, MA04's interaction may be described as one where a relatively high number of
		actions were taken for a relatively small number of useful hits: he makes 4 separate inputs, works with a set of
		102, takes 29 actions and marks just 7 records as useful. This impression, drawn from the diagram, may then be
		compared with the qualitative data. Here, the volunteer was asked for his impressions of this resource, and
		while he approved of the layout and appearance of the site, he expressed dissatisfaction with the outcome of
		this interaction, saying:</p>
	<blockquote>
		<p>'it was difficult to use though, because it didn't actually get what I wanted from it, which. what's the
			point? As an information source it . sort of misses the point really.'</p>
	</blockquote>
	<p>A statement which may be seen to have a lot in common with the interaction this volunteer had just completed: one
		of high efforts for low returns, spread out over 16 minutes.</p>
	<h3>6.2 Comparison to Quantitative Data</h3>
	<p>From the same diagram, it is possible to extract new quantitative data, which can then be compared to that
		collected from other diagrams - for instance, representing the same volunteer searching an alternative system:
	</p>
	<table>
		<tbody>
			<tr>
				<td></td>
				<td>
					<p><strong>Grateful Med</strong></p>
				</td>
				<td>
					<p><strong>MedWeb</strong> [3]</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>No. Actions</strong></p>
				</td>
				<td>
					<p>29</p>
				</td>
				<td>
					<p>47</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>Duration</strong></p>
				</td>
				<td>
					<p>16</p>
				</td>
				<td>
					<p>45</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>No. Inputs</strong></p>
				</td>
				<td>
					<p>4</p>
				</td>
				<td>
					<p>6</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>No. Navigational Movements</strong></p>
				</td>
				<td>
					<p>14</p>
				</td>
				<td>
					<p>19</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>Total Hits</strong></p>
				</td>
				<td>
					<p>6892</p>
				</td>
				<td>
					<p>0</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>Browser Functions Used</strong></p>
				</td>
				<td>
					<p>1: BACK</p>
				</td>
				<td>
					<p>5: BACK<br>
						1: STOP</p>
				</td>
			</tr>
		</tbody>
	</table>
	<p>Figure 7</p>
	<p>Figure 7 shows the comparative quantitative data drawn from two such diagrams: these are the diagrams
		representing the same volunteer's interactions with two different systems. As you can see, while almost the same
		number of actions were taken over a similar length of time, the volunteer in this interaction got absolutely no
		hits, and made much more extensive use of the browser functions to navigate within the site.</p>
	<p>Again, a comparison with the qualitative data is also informative. When asked for his impressions about MedWeb,
		the volunteer expressed his annoyance at the coverage of the resource, which unlike Internet Grateful Med, does
		not hold document surrogates but links to institutional WebPages: something he found confusing, and which
		therefore reduced the effectiveness of his search:</p>
	<p>'<em>Again, extremely irritating because it doesn't . actually tell you that when you're searching for things
			that you're searching for institutions, it took a long time to work that one out, and that's a bit
			irritating,'</em></p>
	<p>So again, the diagrams may provide useful quantitative data, and this may in turn lead to a re-examination of the
		qualitative data already collected.</p>
	<h2>7. Future Research</h2>
	<p>But there may be a third way in which these diagrams can prove useful, and that, as suggested earlier, is in the
		potential comparisons between such representations of navigational behaviour and the data relating to an
		individual's field independence, or specifically, comparisons between the graphical representations of
		interactions made by volunteers exhibiting particular scores on Riding's Cognitive Styles Analysis. In this
		study, Riding's test has been mainly used to collect field independence data, as this data was considered
		potentially relevant to the question of why different users prefer or avoid systems with different
		information-retrieval characteristics.</p>
	<p>Riding places individuals in any one of nine positions, relative to their scores on the Wholist-Analyst scale,
		and Verbal-Imager scale. While the number of volunteers used in this study is really too small for any valid
		comparison of cognitive style data at the statistical level, it may yet be possible to contrast the navigation
		patterns and choices of navigation options between subjects displaying different cognitive styles. It is hoped
		that, in the near future, a comparison may be able to be made of the diagrams of navigational behaviour from one
		individual with those of others who also share the same cognitive style. Again, the numbers involved in this
		study may mean that no valid comparisons can be made, but it will be interesting to see whether any overall
		patterns emerge, concerning, for instance, bias towards one type of interaction - all within the framework of
		the particular Web resources used.</p>
	<h2>10. Findings and Summary</h2>
	<p>The use of these visual representations of the patterns of navigation within specific user-system interactions is
		one that may offer to the researcher particular benefits of clarity, low memory-load and indications of
		important areas of data. Work is continuing to develop and further research this methodology, and apply it to
		the data collected during this study, specifically in the areas of comparison between individuals, and between
		the behaviours of the same individual on different systems. . It is hoped that other researchers also
		undertaking qualitative research, and where representation of individuals navigation of Web resources is
		important, may also find this methodology one worth applying, and may also seek to develop it to meet their own
		requirements.</p>
	<h2>References</h2>
	<ul>
		<li>Fidel, R. (1991). &quot;Searchers' selection of search keys: I I. Controlled vocabulary or free text
			searching&quot;. <em>Journal Of The American Society For Information Science</em>, <strong>42</strong>,
			501-514</li>
		<li>Glaser, B.G., &amp; Strauss, A.L. (1967_). The Discovery Of Grounded Theory: Strategies For Qualitative
			Research._ New York: Aldine de Gruyter</li>
		<li>Lucas, H. (1998) &quot;Representing WWW Navigational Data: A Graphical Methodology to Support Qualitative
			Analysis&quot;, <u>In:</u> El Hadi, Widad Mustafa, Maniez, Jacques &amp; Pollitt, Steven A. (editors),
			<em>Structures and Relations in Knowledge Organization. Proceedings of the 5<sup>th</sup> International ISKO
				Conference.</em> Wurzburg: Ergon-Verlag. 402-408.</li>
		<li>Riding, R.J. (1991). <em>Cognitive Styles Analysis</em>. Birmingham: Learning and Training Technology.</li>
		<li>Tufte, E.R. (1983). <em>The Visual Display of Quantitative Information</em>. Cheshire, Conn.: Graphics
			Press.</li>
	</ul>
	<h2>Notes</h2>
	<p>1. Lucas, H. (1998) &quot;Representing WWW Navigational Data: A Graphical Methodology to Support Qualitative
		Analysis&quot;, <u>In:</u> El Hadi, Widad Mustafa, Maniez, Jacques &amp; Pollitt, Steven A. (editors),
		<em>Structures and Relations in Knowledge Organization. Proceedings of the 5<sup>th</sup> International ISKO
			Conference.</em> Wurzburg: Ergon-Verlag. 402-408.</p>
	<p>2. This site may be found via the page:<a href="http://www.nlm.nih.gov/">http://www.nlm.nih.gov</a></p>
	<p>3. This site may be found at: http://www.gen.emory.edu/MEDWEB/</p>

</body>

</html>