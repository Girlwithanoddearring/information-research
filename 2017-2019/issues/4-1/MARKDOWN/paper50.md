#### Information Research, Vol. 4 No. 1, July 1998



# Ethnomethodology and the study of online communities: exploring the cyber streets

#### [Steven R. Thomsen](MAILTO:steven_thomsen@byu.edu), Joseph D. Straubhaar, Drew M. Bolyard  
_respectively_: Assistant Professor of Communication, Professor of Communication,  
and Graduate Student  
Brigham Young University  
Provo, Utah, USA

<div>Abstract</div>

> Drawing from the authors’ current research programs, this essay explores the basic dimensions of online communities and the concomitant need for scholars to rethink the assumptions that undergrid historic paradigms about the nature of social interaction, social bonding, and empirical experience ([Cerulo, 1997](#cer)). In so doing, we argue that online communities are far from the “imagined” or pseudo communities explicated by [Calhoun](#cal) (1991); that they are, in fact, “real” in the very way in which they reflect the changing nature of human relations and human interaction. Finally this paper discusses the epistemological and methodological implications of studying cyber communities. We will discuss how computer-mediated interaction, or telelogic communication, as it has been characterized by a number of theorists ([Ogan, 1993](#oga); [Ball-Rokeach &](#bal); [Reardon, 1988](#rea)), can be analyzed to contribute to phenomenological or ethnographic understandings of what it means to be a member of a cyber-community. We suggest that one of the best approaches to taking such a phenomenological snapshot is through a multi-method triangulation, employing qualitative interviews and descriptive and inferential analyses of message content. We also will address limitations and restrictions for using the Internet to do ethnomethodology.

## Introduction

> _". . .Rather than being constrained by the computer, the members of these groups creatively exploit the systems' features so as to play with new forms of expressive communication, to explore possible public identities, to create otherwise unlikely relationships, and to create behavioral norms. In so doing, they invent new communities."_ ([Reid, 1995](#rei))

In mid-October 1997, “Julie” faced an 8-hour brain surgery to remove a tumor growing on the vestibular nerves that affect her hearing and balance. As is often now the case in this age of interactive, online communications, Julie turned to her “friends” on the Internet not only for emotional support but also for medical advice, particularly from those who had previously had this type of surgery. Several months earlier, she had found the Acoustic Neuroma newsgroup and had become an active member of this cyber-community of individuals hoping to overcome the challenges of a rare form of debilitating brain tumors. In the weeks prior to the surgery, daily e-mail created a strong bond between Julie and her new friends. Following the surgery, Julie’s husband chronicled the ups and downs of her recovery by posting daily updates to the group.

Although they transcend the physical and spatial boundaries that have traditionally defined a “community,” “cyber-communities” are often a primary form of social interaction for the growing number of individuals who often spend hours each day surfing the net. A growing number of scholars have begun to explore the impact of newsgroups, mailing lists, community networks, and electronic bulletin boards on the participants and the “communities” they serve. [Garramone, Harris, and Pizante](#gar) (1986), for example, explored the motivations for participation in a political bulletin board. [Furlong](#fur) (1989) examined the need fulfillment provided by an online network for senior citizens. [Downing](#dow) (1989) explored the impact of PeaceNet on what he described as “grassroots teledemocracy.” [Nelson](#nel) (1994, 1995) studied the “virtual communities” created by disabled Internet users, [Murray](#mur) (1996) described the way in which nurses shared information and exchanged ideas on a specialized newsgroup, and [Thomsen](#tho) (1996) examined the uses and gratifications associated with participation in a newsgroup for public relations practitioners.

The often highly specialized nature of these online communities, and the fact that they transcend geography and the need for physical presence, pose a challenge for sociologists and communication researchers, however, because they do not possess all the traditional dimensions of “real” communities that have often been the focus of ethnographic and social research. [Wolfe](#wol) (1996) points out that researcher/social critic historically has placed himself or herself as a participant observer, physically and intellectually immersed in the community to be studied and able to interact face-to-face with its members. [Robert and Helen Lynd](#lyn1) (1929, 1939) went to Muncie, Indiana, to study a typical American community in their pioneering “Middletown USA” project in the 1920s. [Margaret Mead](#mea) (1928) went to Samoa. Others have gone into mental institutions, tattoo parlors, funeral homes, and inner-city neighborhoods in large U.S. metropolitan areas ([Wolfe](#wol), 1996). In his seminal work, “Street Corner Society,” [Whyte](#why) (1955) lived, worked, and even got married in the Italian slum of a U.S. city that he studied for nearly four years.

When the researcher selects an online community as the focus of his study, however, where does he actually go and what is he really observing? The answer to these questions is the focus of this essay.

## "Real" versus "Imagined" communities

Computer-mediated communication is changing the way we define and view the concept of a “community.” The change, however, is not without some resistance. The emergence of online communication modalities also has fostered a new dialogue among scholars as to whether these cyber subcultures are worthy of our attention or whether they are simply ephemeral, imagined communities, too fleeting, too superficial, and too “virtual” to warrant serious exploration.

[Calhoun](#cal) (1991) argues that the modern condition is one of “indirect social relationships” in which connectivity with others is more imagined, or parasocial, than “real.” The media’s ability to broaden the range of our experiences creates the illusion of greater contact or membership in large-scale social organizations. Rather than creating “communities,” however, were are merely developing “categorical identities” or “imagined communities,” that are nothing more than the “feeling” of belonging to some group. He argues that a true “community” requires direct relationships among its members:

> _I want to argue. . .that there is a great deal of difference between social _groups_ formed out of direct relationships among their members, although often sharing an imaginatively constructed cultural identity, and social _categories_ defined by common cultural or other external attributes of their members and not necessarily linked by any dense, multiplex, or systematic web of interpersonal relationships._ (p. 107)

In contrast, [Oldenburg](#old) (1989) argues that online communities may fill a need that has been all but abandoned in modern societies, where the closeness and social bonding of the gemeinschaft has been replaced by the emotional disconnect of the gesellschaft. According to Oldenburg, an individual moves about through three basic environments: where he works, where he lives, and the place where he joins with others for conviviality. The latter environment, the place of “idle talk and banter with acquaintances and friends,” is often where the sense of membership in a “community” is achieved and experienced. Cafes, barber shops, and pubs once provided this environment, but in the age of shopping malls, drive-in fast food, shrinking public space, and residential “cocooning,” this need for conviviality is left unfulfilled. Modernity, Oldenburg argues, has established a culture in which the home and the workplace remain as the only two interactive spheres of existence. It should not be surprising, then, that millions of people throughout the world turn to the Internet to re-create and re-establish the third sphere of conviviality.

[Rheingold](#rhe) (1993), in his book, “The Virtual Community,” suggests that the logging onto online services and checking e-mail and chat rooms is “similar to the feeling the of peeking into the cafe, the pub, the common room, to see who’s there, and whether you want to stay around for a chat” (p. 26). For him the community was “real,” he explains, because it was grounded in his “everyday physical world.” Rheingold attributes the rise of virtual communities to the hunger people have for a sense of “community” as they struggle with the disappearance of informal public spaces in their lives.

[Jones](#jon) (1995), like Rheingold and Oldenburg, shares the view that newsgroups, bulletin boards, and other forms of computer-mediated communication have sprung out of the need to re-create this sense of community, that participants join and become involved with the express purpose of reestablishing a social bonds.

Critical to the rhetoric surrounding the information highway is the promise of a renewed sense of community and, in many instances, new types and formations of community. Computer-mediated communication, it seems, will do by way of electronic pathways what cement roads were unable to do. Namely, connect us rather than atomize us, put as at the controls of a “vehicle” and yet not detach us from the rest of the world. ([Jones](#jon), 1995: 11)

### New assumptions for the online world

[Cerulo](#cer) (1997) writes that in order to effectively study online communities, sociologists and communication researchers must reframe the way in which they view the computer-mediated world and past assumptions about human interactions:

> _Recent developments have touched issues at the very heart of sociological discourse--the definition of interaction, the nature of social ties, and the scope of experience and reality. Indeed, the developing technologies are creating an expanded social environment that requires amendments and alterations to ways in which we conceptualize social processes._ (p. 49)

Technologically generated communities, she writes, force us to reformulate the way in which we view three key analytic concepts: social interaction, social bonding, and empirical experience.

<u>Redefining social interaction</u>. The traditional stance in sociological analysis, Cerulo explains, is that physical co-presence is the determining factor in judging the significance and quality of a communicative exchange. “We speak of the closeness and trust born of such mediated connections using terms such as pseudo-gemeinschaft, virtual intimacy, or imagined community,” she writes. “Such designations reify the notion that interactions void of the face-to-face connection are somehow less than the real thing” (p. 50). [Purcell](#pur) (1997) also notes that the type and extent of social contact determines the richness of an exchange and that intimate, face-to-face exchanges have been viewed as the most substantive, and legitimate, bonding forms of interaction. That view, Purcell suggests, is not accurate in all settings:

> _Co-presence does not insure intimate interaction among all group members. Consider large-scale social gatherings in which hundreds or thousands of people gather in a location to perform a ritual or celebrate an event. In these instances, participants are able to see the visible manifestation of the group, the physical gathering, yet their ability to make direct, intimate connections with those around them is limited by the sheer magnitude of the assembly._ (p. 102)

[Cerulo](#cer) (1997) argues that a closer examination of much of the interaction taking place among members of online communities contradicts the standard that physical co-presence is necessary for intimate quality interactions. A series of messages posted by the father of a 19-year-old acoustic neuroma patient to a newsgroup for those who suffer from that form of tumor reveals the degree to which self disclosure and intimacy often slowly occur. A month earlier, the father had posted his first message. Clearly devastated by the news that his son, a freshman at an U.S. university, had a brain tumor, the father asked group members for advice and support. Dozens of replies were posted and the interaction continued between the father and members in a thread that lasted for more than a month and continues to the present. Days before the surgery, the father wrote:

> _We’ve cried our tears, met with doctors, sought advice from you all, learned all we could, prayed for help, made our decisions, and set our plans. My wonderful 19-year-old son ____ will have his 4 cm acoustic neuroma removed on Friday, January 23rd, at the Barrow Neurological Institute (at St. Joseph’s Hospital) in Phoenix. The surgery team will be led by Dr. Philip Daspit (neurotologist) and Dr. Robert Spetzler (neurosurgeon). They estimate surgery will take 12-18 hours, that ____ will be in intensive care for about 3 days, in a regular hospital room for 4-5 days, and hopefully, home after that. . .  
>   
> We finalized these plans about a week ago. Since then, _____ and I have gone on daily hikes, had some wonderful family dinners, hugged a lot, and spent a bunch of time talking to our many friends. . . including many new ones that we met through this ANA listserv. Thank you for your support.  
>   
> I ask for your prayers for ____, his surgeons, and for our family. . . .as we go into this surgery, and as we learn to deal with whatever comes next. Your prayers will make a difference. Thank you!!!!_

Following the surgery, the father posted the following update:

> _My wonderful son____ is back. Home. Whole. Smiling. Recovering. Thank God, and thank you for your support, and especially for your prayers. . . .  
>   
> I’ve read the ANA postings since our surgery with interest. Our family has found the responses we’ve received to be most supportive, and we gathered much valuable information that helped us make the decisions on the surgery, the doctors, the hospital, and insurance coverage. We have made many friends as result of the ANA. I thank you again for your support and prayers. They really did make the difference for _____ .  
>   
> I close with two thoughts. First, I am so thankful that my wonderful son ____ will continue to “shine.” Second, when I thank our surgeons, I told them that “it will be good to get back to normal”. . . .they responded that John and our family will “never be normal or the same again. You will be more appreciative for what you have, more empathetic to others, and put more value on how much the support of others means. You will never be the same. You will be much, much better for having had this experience. I couldn’t agree more._

Another example of intimacy which can occur within postings to a newsgroup can be found in message posted by Julie’s husband to the Acoustic Neuroma group to weeks prior to surgery. In this message, he talks about his wife, his daughter, a grandmother, and some personal thoughts as the family prepares for the surgery. The tone of the posting is much like a family letter--its assumes that readers sense a certain degree of intimacy and a willingness to share very personal information.

> _We’ve been out here lurking for a while waiting for our surgery date. We were supposed to go in September, but due to scheduling problems at the Drs. office we didn’t get scheduled until Oct. 20\. So, now the time is counting down and we are starting to realize the implications of what may happen. We have read so many e-mails of how people go back to work in a couple of weeks and are very encouraged by that. We also read the e-mails of all the possible complications and that is what makes us nervous.  
>   
> Just a reminder, my wife has a 2 cm AN. We are in the final stages of weaning our daughter who turns one year old on Friday. She started walking last week and we are real excited about that. She is growing in so many ways. It really is amazing. We’ve been saying that we were expecting the worst, but hoping for the best. My wife is pretty resigned to losing her hearing but really doesn’t want to lose any facial control. She is still young and basically doesn’t want to go through life that way. Can’t say that I blame her. I was talking to a co-worker who is in a wheel chair and she was telling me about how many people tell her how lucky she is to be able to sit down all day. Even though they are joking, and she has a good sense of humor about it, comments like that still hurt. . . .  
>   
> We are going to get family portraits done before surgery so we won’t have to worry about what Julie looks like for Christmas cars. (Julie has always worn her hair long, and she doesn’t want to have her buzz cut immortalized.)  
>   
> Grandma is coming in a couple days to help take care of us. She is coming early mainly so that out daughter gets used to having her around before mom and dad take a week to go through the surgery and recovery. With mom in the hospital and dad driving back and forth, we figure it will be hard on her for while. Depending on the after effects and recovery time, grandma is going to be here for a while. The other grandma is willing to come out after so we are covered for a couple of months if need be.  
>   
> Well, keep us in your prayers and I will try to keep you posted as to how things go.  
>   
> Take care and God bless._

[Cerulo](#cer) (1997) argues that exchanges such as these, and those that she has observed, indicate that online encounters are more than a “one-shot deal.” Online exchanges, such as those found on the Acoustic Neuroma newsgroup, “typically serve as catalysts for long-term and meaningful relationships” (p. 54).

### Redefining social bonds

One criticism of computer-mediated communities is that they are unable to foster substantive and genuine personal relationships ([Parks](#par), 1996; [Beninger](#ben), 1987) and that they are more likely to produce social isolation than connectivity ([Kiesler, Siegel, & McGuire](#kie), 1984). In addition, because the exchanges that take place online may lack the assumed level of intimacy and self-disclosure characteristic of more traditional interaction they are thought to be unable to produce legitimate social bonding ([Cerulo](#cer), 1997; [Calhoun](#cal), 1991).

[Parks](#par) (1996), however, in his study of the members of 24 different newsgroups found that more than 60 percent of his subjects said they had formed a personal relationship with someone they first contacted through a newsgroup. Parks and others (See [Thomsen, 1996](#tho), for example), note that these relationships build over time and often are continued through the use of other communications channels (i.e., telephone, the postal service) and often lead to face-to-face encounters. In fact, length of time and degree of participation, not surprisingly, contribute to greater rates of relationship building ([Parks, 1996](#par)). [Reid](#rei) (1995) has also suggested that the problems in relationship building posed by computer-mediated communication are easily overcome. She explains that the social information required for relationship development can be obtained via computer-mediated interaction, but the process simply takes longer and requires slightly more effort on the part of the participants.

David Minger has been a member of the Acoustic Neuroma newsgroup for more than two years. Although his surgery also was more than two years ago, he continues to post messages to new members--often as many as 10 a day. In an interview with one of the authors of this essay, he explained that his involvement is an attempt on his part to “give back to the group what it has given to me” ([D. Minger](#min), personal communication, December 8, 1997). Recently he hosted an in-person meeting for other newsgroup members who live in the Pacific Northwest near his home. On January 25, 1998, Minger posted a message to the newsgroup recounting the success of that gathering:

> _Our first official meeting of the Seattle Acoustic Neuroma Association took place earlier today.  
> We had 23 attendees, which included 7 friends or spouses and 1 youngster, the remainder being AN patients. A number of other persons who could not attend today plan to attend in the future.  
> We spent 3 hours visiting, introducing ourselves, sharing stories, concerns, tips and thoughts. We met lots of great people and look forward to all of us being able to keep in touch and meet periodically. We will be meeting every three months, so the next meeting will fall in the last part of April 1998. . . .  
> Our plans include an annual event where we have speakers from the medical community. We will also be developing strong liaisons with local hospitals and doctors to let them know we are a resource available for newly diagnosed patients or their friends or families who wish to learn more about AN..._

In his study of the newsgroup PRForum, [Thomsen](#tho) (1996) also found that online bonding was a springboard to other forms of off-line interaction, including face-to-face encounters. In the case of PRForum, which is a newsgroup for public relations professionals, the off-line interaction often led to working relationships and increased opportunity for both professional and social involvement. One PRForum member explained:

> _My cyber-meeting with another member lent me immense credibility when I met her in the flesh at the regional PRSA conference. This helped me get into a position where I became a technical advisor for her book and into other work with PR professionals._

### Redefining empirical experience

Equally important to this line of reasoning is the perception of newsgroup member that they are, in fact, a part of a “real” community or “village.” As Rheingold suggested, these virtual encounters overlap into their physical worlds and their daily, lived experiences. As one Acoustic Neuroma newsgroup member explained:

> _I was wrestling on the mat of indecision even two weeks ago as to my choices of surgery available. First recognizing how wonderful it is to even have choices, I was, some may recall, typically unsure what my “odds” were to be with micro-surgery vs. the radiological approach.  
>   
> Well, I took advice from many sources, including some from this here AN listserver. Listen to all opinions, weigh the differences, then act, and you’ll be fine I heard.  
>   
> And I listened to my “inner light” telling me to choose with those communities closest to me. Afterall, “it takes a village.” Science doesn’t do it all. Indeed, I’ve just gotten through the deep dark forest but not yet out of the woods!  
>   
> Thanks gain for notes and so many, many opinions. . .from all sorts of places. Portland, Seattle, Phoenix, NY, England, and Israel. Indeed this is a village!_

Another Acoustic Neuroma newsgroup member posted the following message:

> _Dear all of you:  
> I feel like George Bailey during the final scene of “It’s a Wonderful Life” when Harry walks in and toasts:  
>   
> “To George Bailey, the richest man in town.”  
>   
> Your support and encouragement make me feel like the richest gal in cyberspace. Can you tell my eyes are getting moist? Thank you all so much!_

[Virnoche and Marx](#vir) (1997) have suggested the need to redefine “community” in light of the increasing overlap between the “virtual” and “real” worlds of these individuals who interact via newsgroups, bulletin boards, chat relays, and MUDs. They propose a “model” in which “community is constituted by individual identification of, and involvement in, a network of particular associations.”

Having argued in support of a reconceptualization of “community” to accommodate social interaction, bonding, and empirical experience in online environments, we now turn our attention to epistemological and methodological issues related to the study of cyber-communities. In so doing, we propose a model for the ethnomethodological enterprise built upon textual or discourse analysis and supported by the use of qualitative interviewing as a means of providing the “thick description” necessary to understand the cultural norms and codes ([Geertz](#gee), 1983).

## Epistemological and methodological considerations

“Ethnography,” [Apgar](#apg) (1983) writes, “is essentially a decoding operation.” “[A] description of shared knowledge, or cognition,” Apgar continues, “enables us to decode the observed behavior.” To do this the ethnographer must examine, and essentially “learn,” the group’s language or discourse, the means through this knowledge is transmitted and intercommunicated. The ethnographer must identify the key concepts, the basic unit of cognitive psychology, and their associated linguistic labels or lexemes ([Emerson](#eme), 1983). In so doing, the ethnographer or researcher must establish the “authority” to write from the insider’s perspective.

It might be argued that the ability to “see” or observe actual interaction in a “real” community produces a better, or more legitimate, analysis. Our point here, however, is to argue that careful analysis of online discourse or text can lead the ethnographer to an appropriately “thick description” ([Geertz](#gee), 1983) of the online community. To apply these notions to an ethnography of an online community, which exists primarily or solely as textual creation, we must think about how to adapt these epistemological and methodological concerns to the techniques available for textual and discourse analysis.

### Textual analysis and ethnography

Ethnographies have always taken advantage of written materials from a culture, but that has usually formed only a part of the evidence for analysis. Online communities present the researcher with nothing but text. The ethnographer cannot observe people, other than through their textual contributions to a forum. All behavior is verbal in the form of text. There are no other artifacts to analyze other than text. Interviewing presents possibilities to meet people in person, but given the dispersed geographical nature of most current online communities, interviewing must usually be done online, again via text.

This necessary emphasis on text presents both opportunities and severe limits. In one sense, there is less for the ethnographer to miss in a text-based world of interaction. All speech, behavior, community rules, and community history is, in principle, likely to be available online for the researcher's inspection. This may make the task seem deceptively easy. A researcher could sit down at his or her own computer, browse through a community's archive, monitor current postings, and have the world's easiest fieldwork conditions.

Researchers must realize, however, the limits, and pros and cons, of text analysis. There are several textual analysis traditions and techniques upon which to build. This paper will not focus much on the tradition of literary analysis, but will concentrate on a potentially rich tradition, the use of both quantitative and qualitative content analysis by communications scholars and historians.

Given the vast amount of text available online, using quantitative content analysis methods to survey a number of communities over larger spans of time seems an intriguing possibility. Quantitative content analysis does enable the reduction of large amounts of text into numerical data that can be analyzed statistically ([Holsti, 1969](#hol); [Krippendorff, 1980](#kri)). However, this type of content analysis tends to be limited to very straightforward categories, which often miss the more subtle nuances of interaction that can meaningfully describe a community. To achieve statistical reliability and validity, categorical definitions must be so straightforward that any two trained coders will code the same material into the same category over 90 percent of the time. This is a desirable level of rigor, necessary for coding large quantities of text into comparable categories, but it removes the traditional ethnographer's tool of gaining individual insight through his or her own experience with the material and the community in question. While traditional ethnographies assume the trained insight of the researcher as a useful tool, quantitative content analysis tends to remove any insight that cannot be duplicated by another observer with relatively straightforward definitions.

One approach, however, has been to treat the individual message as a the unit of analysis and code it on the basis of form, function, and content ([Sias, 1995](#sia); [Thomsen, 1996](#tho)). “Function” categories (i.e., associational, business exchange, debate, surveillance, etc.), for example, provide insights into the way in which the messages are “used” by the group’s members. This can be useful in understanding the role of the newsgroup in its members lives.

In addition, certain kinds of things about online communities could usefully be counted in a straightforward way and quantified. One could count, for example, how often men versus women post to an online community. One could also count the main topics each gender chooses to post about. However, even these seemingly simple tasks may be difficult or hard to validate as reliable quantitative data. As [Turkle](#tur) (1995) points out, people frequently use potential online communities, like multiple-user domains or chat-rooms, to deliberately experiment with their own identity and personality. Personas presented online may not be literally truthful in terms of age, gender, personality or even interests. One could allow for such behaviors and simply count people and their behavior as presented, but the ambiguities involved seem to lend themselves to a more qualitative analysis, rather than rigorous counting and quantitative statistical analysis. However, counting topics cited, numbers and types of people who post, the trends in topics over time may well be useful.

To go much beyond simple categorizations, other forms of content and narrative analysis may be more useful. It seems likely that ethnography of online communities will probably have to use discourse or narrative analysis techniques to supplement quantitative content analysis techniques.

### E-mail as discourse

Newsgroups are made possible through sophisticated software that enables e-mail messages to be sent from one individual to all group members simultaneously. While e-mail is a textually based form of communication, it is also a dialogue among group members. An ethnographer might listen in on a conversation between friends as he conducts his fieldwork. He might also “listen in” on ongoing dialogues, or “threads,” as he lurks and visits a cyber-community. In other words, e-mail can be conceptualized as “talk” among members, and can be analyzed as such.

Discourse or conversation analysis actually covers a broad range of methodologies and is, as a result, difficult to define. At its essence, is an attempt to recognize patterns, rules, or procedures that occur among participants and the way in which these structures or conventions influence meaning and effect. Unlike content analyses, which typically treat each e-mail posting as an individual unit of analysis, discourse or conversation analysis requires that the researcher “see” a complete conversation, which, in effect, may constitute a series of several exchanges. The context of a particular message, [Silverman](#sil) (1993) explains, is actually found in the messages or statements that precede it.

Ethnographers examining e-mail as discourse should look for the structural organization of the talk. Are there patterns followed by the participants? Has a jargon or group speak emerged? Has the group developed its own abbreviated “emoticons”--a means of using language as both an inclusive and exclusive device. Often use of these tools distinguish between the group’s “newbies” and its “veterans,” creating both a power structure and a class-like hierarchy for participation. Often, threads follow a clearly defined sequential organization, most notably using the “<<<“ symbols to bring previous statements into current responses and conversations. [Thomsen](#tho) (1996), in fact, found that 74 percent of the messages posted to the PRForum during his observation period were responses to previous questions or continuations of ongoing dialogue. Ethnographies can also explore the way in which the somewhat asynchronous nature of e-mail affects “turn-taking” among participants. Finally, the ethnographer should become keenly aware of they way in which language is used by group members to exchange “coded” information, in which the meaning far transcends the mere words. For example, insights into the way in which a group and its members see or define themselves can often be found in the root metaphors used in conversations.

### Epistemological validity and authority

Epistemological validity or authority is a measure of the ethnographer’s ability to accurately know and reconstruct the world of his subjects. The legitimacy of the observations and resulting analysis are a direct function of the researcher’s ability to immerse himself or herself in the world of those being observed. Earlier, however, we raised the issue of where it is the ethnographer actually “goes” when an online community is the focus of his study. How does he immerse himself in such a community? In other words, how does the researcher develop the epistemological validity and the authority necessary to produce an informed perspective and accurately analyze and deconstruct the “text” of computer-mediated communications among members of a specific community? These issues are the focus of this section. First, we will examine the traditional criteria for immersion. We also will offer our thoughts and observations on how these criteria may be applied to the study of online communities.

[Goffman](#gof) (1989) has noted that the “technique” of the participant observer/ethnographer is to subject himself into the “set of contingencies that play upon a set of individuals” so that he can “physically and ecologically penetrate their circle of response to their social situation, or their work situation, or their ethnic situation, or whatever” (p. 125). The goal, he explained, is to be “close to them while they are responding to what life does to them.” The immersion, Goffman continues, requires the researcher to subject himself to the life circumstances of those being observed and to assume that he or she is bound to that group. He explains:

> _. . .You’re empathetic enough--because you’ve been taking the same crap they’ve been taking--to sense what it is that they’re responding to. To me, that’s the core of observation. If you don’t get yourself in that situation, I don’t think you can do a piece of serious work._ (p. 126)

Lincoln and Guba (1985), in their work on “naturalistic inquiry,” suggest that the ethnographer’s work must meet a rigorous criteria of “trustworthiness” or credibility. The end product of the ethnographer’s work, they explain, must demonstrate “truth value.” To do this, the ethnographer/researcher must show that he or she has represented those multiple constructions adequately, that is, that the reconstructions (for the findings and interpretations are also constructions, it should never be forgotten) that have been arrived at via the inquiry are credible to the constructors of the original multiple realities. (p. 296)

To establish this credibility, [Lincoln and Guba](#lin) explain that the researcher is required to

> >_“carry out the inquiry in such a way that the probability that the findings will be found to be credible is enhanced and second, to demonstrate the credibility of the findings by having them approved by the constructors of the multiple realities being studied”_ (p. 296).

Clearly, textual analysis alone, without interaction by the ethnographer with the observed community, is not enough to obtain this level of credibility. The main issues probably lie in the adaptability of some of the classic strategies of participant observation. First, we will address Lincoln and Guba’s suggestions for enhancing the credibility of the ethnographer’s findings and offer our insights as to how these principles might be applied or transfer to the study of cyber-communities. They suggest a number of activities; we will focus on prolonged engagement, persistent observation and the use of “insiders” or informants. We will also address issues of depth of participation, sampling versus over time involvement, learning rhetorical codes, and approaching dissimilar people as an online observer.

### Prolonged engagement and persistent observation.

Lincoln and Guba describe “prolonged engagement” as the “investment of sufficient time” to truly learn a community’s “culture,” to test for the misinterpretation of information and observations, and to build trust and establish rapport with the members of the community. The objective is to effectively penetrate the community, even to the point, Goffman suggests, that the researcher becomes a “member.” [Goffman](#gof) explains:

> _The sights and sounds around you should get to be normal. You should be able to even play with the people, and make jokes back and forth. . ._(p. 129)

The researcher needs to be willing to commit as much time to online ethnography as he or she would to a more traditional on-site study. Just because a researcher does not have to physically travel to a site, they still have to "case the scene" ([Schatzman & Strauss, 1973](#sch)), create a strategy for entering and getting access, engage the culture, slowly get to know people, create a strategy for "watching" and "listening" ([Schatzman & Strauss, 1973](#sch)) via text, create categories, engage in ongoing or even constant comparative analysis over time, and create analytic models. Properly done, this is probably no shorter than the amount of time taken, minus physical logistics, to do conventional ethnography.

### Depth of analysis and sampling.

Online ethnography offers a new temptation. One can access the record of an online community via archived e-mail from a listserv or newsgroup. In theory, one could power through that written record in a compressed amount of time and do a vastly foreshortened sort of field work. However, this won't supply the requisite engagement called for by Lincoln and Guba or the effective penetration of the community called for by Goffman. One cannot sample a community's experience from the records, even if you have access to everything that has ever been said. The researcher must participate to achieve that depth of analysis. He or she needs continuity over time. One must learn their codes from the inside by participation. The researcher must effectively gain membership.

### Learning rhetorical codes over time

As the researcher slowly immerses himself into the community, he seeks to identify the issues, characteristics, and elements that are the most relevant to defining the complexities and culture of the group--he learns the secret language of the group in order to unlock the codes, meanings and constructed realities that have been embedded into the communication and actions of the members. “If prolonged engagement provides scope,” [Lincoln and Guba](#lin) (1985) write, “persistent observation provides depth.”

While there are no “sights and sounds,” _per se_, the observation and analysis of e-mail texts can and should provide insights into the codes or behavioral norms that exists within an online community. The natural tendency for a researcher might be to “sample” the content of the e-mail, taking a snapshot, so to speak, of an ongoing conversation. The problem with this approach, however, is that it fails to take into account the “history” of the dialogue in these conversations. Embedded in most postings are veiled allusions or references to past threads and subthreads, abbreviated thoughts, whose meaning for the group members extends far beyond the mere words. In his work on symbolic convergence theory, [Ernest Bormann](#bor) (1972, 1983) refers to communities as being “rhetorical,” in that group members come to a shared meaning and collective vision born out of history and experience that is transmitted through language and stories. The text of an e-mail message carries meaning for its members that transcends the mere words themselves. Single words or simple references evoke complex meaning and group memories. In the Acoustic Neuroma newsgroup for example, a posting that refers to radiological procedures must be analyzed in the context of a long running debate that divides that community’s members. Specific mention of a particular radiological procedure evokes a history that divides group members often on the basis of age, physicians’ skills, a belief that traditional surgery is overly invasive, and long-term outcomes. In the political ecology of the group, this also is an issue around which many members have attempted to establish expertise or leadership based on their experiences and desire to “help” group “newbies.”

Related to this is the fact that in many groups the language evolves into a pseudo-dialect of abbreviations and jargon reflective of the newsgroup’s focus. It is not uncommon for members of the AN group to throw out abbreviations of the medical terms for their surgical procedures. “I had the trans lab approach,” one poster wrote, referring to a surgical entry point from directly behind the ear. When another talks about her “middle fossa” approach, members know that she must have some hearing to preserve. In this approach, the surgeon enters the skull from above the ear in attempt to do as little damage to the inner ear and hearing mechanisms as possible.

When one poster to the Acoustic Neuroma group writes, “Just a reminder, my wife has a 2 cm AN,” group members understand, because of their own lived experience, that this person has a moderate sized tumor--anything less than one centimeter is considered small with very good potential outcomes. Members of the group exchange and compare tumor sizes, always measured in centimeters, as a “code,” a means of expressing their fears or comfort with the potential outcome. Offline, through information from doctors and personal research, these members know that the size of the tumor is positively correlated with increased dangers of hearing losses and facial paralysis. So when a member writes, “Just a reminder, my wife has a 2 cm AN,” other members know this is an expression of fear and, in effect, a plea for comfort and support. To the group outsider, however, the statement may contain nothing more than a clinical description of tumor size. For the online ethnographer, then, it may take a great deal of time and a commitment to persistent observation to recognize theses codes.

### Dealing with dissimilar people

There is a classic _New Yorker_ cartoon of one dog saying to another, "On the Internet no one knows you're a dog." Turkle emphasizes how people experiment with identity in how they present themselves on the Internet. Online communities would seem to offer an almost ideal minimization of the intrusive nature of the outside observer. “On the Internet, nobody knows that you are really an ethnographer.” However, communities still develop identities and online communities seem to guard membership identity as closely as any. The authors have all experienced hostility in one online community group or another when someone figured out that we were studying the group, even if we were participating actively as members.

It still helps to either start as insider or develop sufficient community bonds through participant observation that one is trusted. Otherwise, online community members, just like real villagers, maybe be evasive, deceptive or even hostile. A researcher who is not a "natural" member of an online community, who enters the community for the purpose of participation observation, still has to gain trust.

The Internet does greatly facilitate "casing the scene" prior to creating a strategy for entering into active participation. It is much easier to lurk on the Internet in most cases than to unobtrusively hang out in an Amazon village. However, many Internet communities are very specific as to membership. [Bennett](#ben2) (1997) compared online communities of Christian gays, Mormon intellectuals, college media advisers, and parents of children with learning disabilities. In several, she required a key informant who could gain access for her.

### Using informants

Ethnographers and social scientists have often relied on informants or insiders, with whom they have gained confidence and trust, to provide “native” insights and explanations that are not always apparent to the community “outsider.” A bi-product of prolonged engagement and persistent observation for the online ethnographer is the growing recognition of “tribal leaders”--group members who, by virtue of membership tenure, experience, or self-proclaimed expertise are among the most frequent posters. In his study of the PRForum, [Thomsen](#tho) (1996) found that although the group had nearly 1,400 members, a small cadre of 10 individuals dominated the conversations accounting for more than 30 percent of the messages posted during the observation period. By building relationships online with these individuals, Thomsen was later able to conduct offline interviews that provided rich insights into the history and collective cognitions of the group. Insights from these informants add to the credibility of the analysis.

## Summary and conclusions

In reality, volumes have been written about ethnomethodology, the study of communities, and the work of an ethnographer. Likewise, applying and adapting this knowledge to the study of online communities is well beyond the scope of a single paper or monograph. Our goal here has to been to initiate a dialogue or discussion among scholars that would serve as a starting point for those interested in exploring the phenomenon of the online, or virtual, community. We expect this dialogue to continue.

For our contribution to the ongoing discussion, we propose a multi-method approach to the study of online communities that involves the use of text and discourse analysis, a prolonged commitment to involved participant observation, and the use of qualitative interviews with group members as a means of further teasing out the “meanings” they ascribe to the experiences of membership and participation. This would enable a multi-source, multi-method triangulation.

As we have suggested, effective participant observation requires time and persistent observation. We argue that the ethnographer/researcher must find a way to penetrate the online community, effectively gain entry and membership, and then remain as an active participant for sufficient time to understand, and become a part of, the world of his subjects. Only then, can he effectively analyze and interpret the discourse or text that he “sees” in his cyber fieldwork. We recognize the challenges posed by this. A researcher cannot become disabled to study a newsgroup for paraplegics, nor could he develop a tumor to participate in the Acoustic Neuroma group. Nonetheless, a gradual means of entry must be obtained, perhaps through the development of contacts and the building of trust with the group’s members. The challenge is for the ethnographer to find the means to overcome the barriers to effective entry into the group.

Qualitative interviews serve as an effective means of triangulation and effectively improve the credibility of the findings and the interpretations of the analysis ([Lincoln & Guba, 1985](#lin)). In his study of the PRForum, [Thomsen](#tho) (1996), for example, followed up his analysis of the e-mail conversations with qualitative interviews with several of the group’s members. Although these interviews were also conducted online (via e-mail), they nonetheless provided insights into the effectiveness of the analysis. The interviews focused primarily on the perceived uses, gratifications, and benefits associated with group participation. The anecdotes, observations, and comments provided by the interviews gave meaning and richness to the descriptive “data” that had been taken from the e-mail postings. While the initial analysis, for example, revealed that one function of the group was for associational purposes, the interviews helped the researcher better understand the contributions of participation to the development of self-esteem and self-validation as a public relations practitioner, particularly for those members who felt they suffered from both social and professional isolation. In effect, triangulation enhances the credibility of the analysis by using multiple measurement processes ([Lincoln & Guba, 1985](#lin)).

Earlier, we noted that [Lincoln and Guba](#lin) (1985) have argued that the researcher must “demonstrate the credibility of the findings by having them approved by the constructors of the multiple realities being studied” (p. 296). Among their recommendations is the use of “member checks,” a means of asking the members themselves to validate the interpretations of the researchers. They write:

> _The member check, whereby data, analytic categories, interpretations, and conclusions are tested with members of those stakeholding groups from whom the data were originally collected, is the most crucial technique for establishing credibility. If the investigator is to be able to purport that his or her reconstructions are recognizable to audience members as adequate representations of their own (and multiple) realities), it is essential that they be given the opportunity to react to them._ (p. 314)

While Lincoln and Guba propose a relatively formal approach to the process of conducting “member checks,” we feel that the qualitative interviews used to support the interpretation of text/discourse also can serve this purpose. In addition to providing “thick description,” they can serve to validate the researchers conclusions and observations.

Finally, we recognize that there have been some issues that have not been addressed within this paper. Those who study online communities and e-mail exchanges recognize, for example, the existence of a certain amount of ambiguity as to the degree to which participants perceive the newsgroup as public or private space. Because of this ambiguity, ethicists have raised issues about the examination of online conversations without the consent of the group’s members. The ethnographer/social scientist’s presence as a researcher is often unseen and unknown. At the same time, many newsgroup members maintain they have an expectation of privacy. Researchers, in turn, have argued that many members would alter their normal communication patterns if they were made aware of the observation taking place. This is clearly an issue that should be explored in future research.

In conclusion, we call for additional dialogue among scholars that addresses the ontological, epistemological and methodological issues of doing ethnography in cyberspace.

## References

<a name="apg"></a>Apgar, M. (1983) Ethnography and Cognition. In R. M. Emerson (ed.), _Contemporary Field Research: A Collection of Readings_ , Prospect Heights, IL: Waveland. pp. 68-77.

<a name="bal"></a>Ball-Rokeach, S. J. and Reardon, K. (1988) Monologue, Dialogue, and Telelog. In R. P. Hawkins, J. M. Wiemann, and S. Pingree (eds.), _Advancing Communication Science: Merging Mass and Interpersonal Process._ Newbury Park, CA: Sage. pp. 135-161

<a name="ben1"></a>Beninger, J. R. (1987) Personalization of mass media and the growth of pseudo-community. _Communication Research_ **14**, 352-371 .

<a name="ben2"></a>Bennett, J. W. (1997) _An Analysis of Virtual Communities and their Implications for the Spiral of Silence_. An unpublished master’s thesis, Department of Communications, Brigham Young University.

<a name="bor1"></a>Bormann, E. G. (1972) Fantasy and Rhetorical Vision: The Rhetorical Criticism of Social Reality. _Quarterly Journal of Speech,_ **58**, 396-407.

<a name="bor2"></a>Bormann, E. G. (1983) Symbolic Convergence: Organizational Communication and Culture. In L. Putnam and M. E. Pacanowsky (eds.), _Communication and Organizations: An Interpretive Approach._). Beverly Hills: Sage. pp. 99-122

<a name="cal"></a>Calhoun, C. (1991) Indirect Relationships and Imagined Communities: Large-Scale Social Integration and the Transformation of Everyday Life. In P. Bourdieu and J. S. Colemen (eds.), _Social Theory for a Changing Society_. Boulder, CO: Westview Press. pp. 95-120

<a name="cer"></a>Cerulo, K. A. (1997) Reframing Social Concepts for a Brave New (Virtual) World. _Sociological Inquiry_, **67** (1), 48-58.

<a name="den"></a>Denzin, N. K. (1997) _Interpretive Ethnography: Ethnographic Practices for the 21st Century._ Thousand Oaks, CA: Sage.

<a name="dow"></a>Downing, J. D. H. (1989) Computers and Political Change: PeaceNet and Public Data Access. _Journal of Communication_ **39**, 154-162.

<a name="eme"></a>Emerson, R. M. (1983) _Contemporary Field Research: A Collection of Readings._ Prospect Heights, IL: Waveland.

<a name="fai"></a>Fairclough, N. (1989) _Language and Power_. London: Longman.

<a name="fur"></a>Furlong, M. S. (1989) An Electronic Community for Older Adults: The SeniorNet Network. _Journal of Communication,_ **39**, 145-153.

<a name="gar1"></a>Garramone, G. M., Harris, A. C., and Anderson, R. (1986) Uses of Political Bulletin Boards. _Journal of Broadcasting and Electronic Media,_ **30**, 325-339 .

<a name="gar2"></a>Garramone, G. M., Harris, A. C., and Pizante, G. (1986) Predictors of Motivation to Use Computer-Mediated Political Communication Systems. _Journal of Broadcasting and Electronic Media_ **30**, 445-457.

<a name="gee"></a>Geertz, C. (1983) Thick Description: Toward an Interpretive Theory of Culture. In R. M. Emerson (ed.), _Contemporary Field Research: A Collection of Readings_. Prospect Heights, IL: Waveland. pp. 37-59,

<a name="gof"></a>Goffman, E. (1989) On Fieldwork. _Journal of Contemporary Ethnography_ **18** (2), 123-132\. [This is a transcription based on a 1974 presentation]

<a name="hol"></a>Holsti, O. R. (1969) _Content analysis for the social sciences and humanities_. Reading, Mass., Addison-Wesley Pub. Co.

<a name="jon"></a>Jones, S. G. (1995) Understanding Community in the Information Age. In S. G. Jones (ed.), _Cybersociety: Computer-Mediated Communication and Community_ ). London: Sage. pp. 10-35.

<a name="kie"></a>Kiesler, S. Siegel, J. and McGuire, T. W. (1984) Social Psychological Aspects of Computer-Mediated Communication. _American Psychologist,_ **39** (10), 1123-1134 .

<a name="kri"></a>Krippendorff, K. (1980) _Content analysis : an introduction to its methodology._ Beverly Hills: Sage Publications.

<a name="lin"></a>Lincoln, Y. S. and Guba, E. G. (1985) _Naturalistic Inquiry_. Newbury Park, CA: Sage.

<a name="lyn1"></a>Lynd, R. and Lynd, H. (1929) _Middletown: A Study in American Culture._ New York: Harcourt Brace.

<a name="lyn2"></a>Lynd, R. and Lynd, H. (1937) _Middletown in Transition: A Study of Cultural Conflicts_. New York: Harcourt Brace.

<a name="mar"></a>Marcus, G. E. and Cushman, D. (1982) Ethnographies as text. _Annual Review of Anthropology,_ **11**, 25-69.

<a name="mea"></a>Mead, M. (1928) _Coming of Age in Samoa_. New York: William Morrow.

<a name="min"></a>Minger, D. Personal Communication. (8 December 1997).

<a name="mur"></a>Murray, P. J. (1996) _Nursing the Internet: A Case Study of Nurses’ use of Computer-Mediated Communication._ An unpublished manuscript. Milton Keynes: School of Health and Social Welfare, The Open University.

<a name="nel1"></a>Nelson, J. (1994) The Virtual Community: A Place for the No-Longer Disabled. _Proceedings of the Virtual Reality and Persons With Disabilities, Second Annual Conference,_ 98-102.

<a name="nel2"></a>Nelson, J. (1995) The Internet, the Virtual Community, and Those with Disabilities. _Disabilities Studies Quarterly,_ **15** (2), 15-20.

<a name="oga"></a>Ogan, C. (1993) Listserver Communication During the Gulf War: What Kind of Medium is the Electronic Bulletin Board? _Journal of Broadcasting and Electronic Media,_ **37**, 177-198.

<a name="old"></a>Oldenburg, R. (1989) _The Great Good Places_. New York: Paragon House.

<a name="par"></a>Parks, M. (1996) Making Friends in Cyberspace. _Journal of Communication,_ **46** (1), 80-97.

<a name="pur"></a>Purcell, K. (1997) Towards a Communication Dialectic: Embedded Technology and the Enhancement of Place. _Sociological Inquiry,_ **67** (1), 101-112.

<a name="rei"></a>Reid, E. (1995) Virtual Worlds: Culture and Imagination. In S. G. Jones (ed.), _Cybersociety: Computer-Mediated Communication and Community_. London: Sage. pp. 164-183

<a name="rhe"></a>Rheingold, H. (1993) _The Virtual Community: Homesteading on the Electronic Frontier_. Menlo Park, CA: Addison-Wesley.

<a name="sch"></a>Schatzman, L. and Strauss, A. (1973) _Field Research: Strategies for a Natural Sociology_. New York: Prentice-Hall.

<a name="sia"></a>Sias, P. (1995) Constructing Perceptions of Fairness: An Analysis of Co-Worker Discourse. _A paper presented at the annual meeting of the Speech Communication Association, San Antonio, TX, November 1995._

<a name="sil"></a>Silverman, D. (1993). _Interpreting Qualitative Data: Methods for Analyzing Talk, Text and Interaction._ London: Sage.

<a name="sto"></a>Stone, P. J. (1966) _The general inquirer; a computer approach to content analysis_. Cambridge, Mass.: M. I. T. Press.

<a name="tho"></a>Thomsen, S. R. (1996) @Work in Cyberspace: Exploring Practitioner Use of the PRForum. _Public Relations Review,_ **22** (2), 115-132.

<a name="tur"></a>Turkle, S. (1995) _Life on Screen: Identity in the Age of the Internet_. New York: Simon and Schuster.

<a name="vir"></a>Virnoche, M. E. & Marx, G. T. (1997) “Only Connect”--E. M. Forester in an age of electronic communication: Computer-mediated association and community networks. _Sociological Inquiry_ **67** (1), 85-100.

<a name="why"></a>Whyte, W. F. (1955) _Street Corner Society: The Social Structure of an Italian Slum._ Chicago: University of Chicago Press.

<a name="wol"></a>Wolfe, A. (1996) _Marginalized in the Middle_. Chicago: University of Chicago Press.