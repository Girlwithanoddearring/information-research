#### Vol. 11 No. 1, October, 2005



# Estudio cienciométrico de la colaboración científica en la Universidad Politécnica de Valencia, España

#### [Adolfo Alonso-Arroyo](mailto:Adolfo.Alonso@uv.es)  
Universidad de Valencia, España  
[Antonio Pulgarín](mailto:apulgue@alcazaba.unex.es)  
Universidad de Extremadura, Badajoz, España  
[Isidoro Gil-Leiva](mailto:isgil@um.es)  
Universidad de Murcia, Murcia, España


#### Resumen

> **Introducción**. El artículo ofrece algunas características de la colaboración científica en la Universidad Politécnica de Valencia (UPV). El estudio se ha llevado a cabo mediante un análisis cienciométrico de la circulación de artículos de revistas y comunicaciones a congresos producidos por esta universidad, entre 1973-2001\. Concretamente, se pretende conocer el grado de cooperación entre los autores, las entidades y los países que colaboran con la UPV.  
> **Método.** El estudio comprende la consulta a un total de 213 bases de datos nacionales e internacionales, el tratamiento de 5464 artículos de revistas y 1111 comunicaciones a congresos, obtenidos de la búsqueda, y el análisis de la colaboración científica en ese período de tiempo.  
> **Análisis.** Se ha realizado un análisis cuantitativo de los datos obtenidos, después de ser tratados con el programa gestor de bibliografía "Reference Manager".  
> **Resultados.** Los resultados muestran un alto porcentaje de colaboración científica de la UPV. Indican que la colaboración científica y la visibilidad están directamente relacionadas. Y se obtienen similares resultados en el caso de la cooperación internacional.  
> **Conclusiones.** La UPV presenta una tasa de colaboración superior al 86%, en general, y mas del 40% internacional. Ha colaborado con 576 instituciones de 51 países diferentes.



## Introducción

Los estudios sobre colaboración científica, en sus distintos niveles (local, nacional o internacional), tienen una alta presencia en las investigaciones de tipo cienciométrico. La medida de la colaboración científica viene dada por la estadística del número de documentos publicados, conjuntamente, por dos o más autores, instituciones, países, etc.

Existen múltiples factores que pueden ser motivo de la alta colaboración científica, que se viene experimentando en las últimas décadas. Entre los principales destacan los de índole económica, debido a los altos costes de la instrumentalización científica o al de las sucesivas generaciones de tecnología, y los factores de índole política, como, por ejemplo, el apoyo a la investigación de la Comisión Europea o los cambios políticos en la Europa del Este, que han hecho que se incremente la colaboración en general. También suelen influir otros factores de tipo científico, preferentemente en el primer nivel (local o individual), ya que la ciencia es una institución social donde su avance depende, sobre todo, de las interacciones entre científicos. Una aproximación a las razones o motivos que llevan a los científicos a trabajar y publicar en colaboración han sido descritas por diversos autores ([Weinstock 1971](#wei); [Beaver & Rosen 1978](#bea78), [1979](#bea79a); [Luukkonen _et al._ 1992](#luu92); [Luukkonen, _et al._ 1993](#luu93); [Beaver 2001](#bea01)).

La colaboración científica es una característica de la Gran Ciencia, de la ciencia actual (Big Science) y un signo de la madurez y del grado de profesionalidad alcanzado en los diferentes campos científicos ([Beaver & Rosen 1978](#bea78), [1979b](#bea79b); [Pao 1992](#pao92); [Arora & Pawan 1995](#aro)).

En todas las disciplinas y a todos los niveles, el número de trabajos en colaboración se viene incrementando desde el siglo XVIII, de tal forma que se asume como un fenómeno notable. Price ([1963](#pri63)) predijo que para 1980 los trabajos con un solo autor se extinguirían. Pero la pregunta que se viene haciendo actualmente es, si este incremento de la colaboración científica influye o va pareja a otros procesos cienciométricos, como la producción científica ([Price & Beaver 1966](#pri66); [Zuckerman 1967](#zuc); [Beaver & Rosen 1979a](#bea79a); [Pao 1980](#pao80), [1982](#pao82); [Pravdic & Oluic-Vukovic 1986](#pra); [Qin 1995](#qin)), la visibilidad ([Beaver & Rosen 1979a](#bea79a)) o el número de citas recibidas ([Arora & Pawan 1995](#aro)), por ejemplo.

Los hallazgos de Lotka ([1926](#lot)) han conducido a que algunos investigadores se hayan preguntado si los autores con mayor producción científica son, a su vez, mayores colaboradores que los menos prolíficos. Investigaciones al respecto parecen indicar que una alta productividad está correlacionada con altos niveles de colaboración. Price & Beaver ([1966](#pri66)) encuentran una fuerte correlación ente productividad y colaboración para los químicos: Los que trabajaron solo o con un coautor no produjeron más de cuatro trabajos en un período de tiempo de 5 años, mientras que en los casos que colaboraron más de 12 autores produjeron al menos 14 trabajos en el mismo período. Pao ([1982](#pao82)) ha investigado la relación entre la colaboración y la productividad en musicología computacional, encontrando un alto grado de correlación entre ellos. Y Pradvic & Oluic-Vukovic ([1986](#pra)), analizando modelos de colaboración en las publicaciones de químicos croatas, observaron que la producción científica era muy dependiente de la frecuencia de colaboración entre los autores. También hay ejemplos de lo contrario, esto es, una disminución con el crecimiento de la coautoría. Braun, Glänzel & Schubert ([2001](#bra)) han mostrado un efecto similar en el campo de la neurociencia.

Respecto a la relación entre productividad y visibilidad, también se han publicado trabajos confirmando esta relación:

> ...así como la colaboración condujo a un incremento en la productividad desde 1800 a 1860, también tendió a incrementar la visibilidad... Entonces podemos concluir que la investigación realizada en colaboración era de hecho más visible que la investigación individual" ([Beaver & Rosen 1979a](#bea79a)).

Pao ([1992](#pao92)) dice que los datos apoyan la teoría de que la colaboración científica sirve como medio para el avance de la investigación, así como de mecanismo para incrementar la visibilidad y la autoría del altamente productivo. Cole & Cole ([1973](#col)) determinaron el nivel de información que tenían los encuestados acerca del trabajo de ciertos físicos; la visibilidad en este caso era una medida del trabajo de un conjunto de científicos. Ellos encontraron que la autoría simple está poco correlacionada con la visibilidad, y que la calidad percibida de un estudio era el determinante más importante.

Y a igual que en el caso de la productividad, también se han realizado estudios en los que no se confirma esta relación. Usando tasa de citas como medida de la visibilidad, Bayer ([1982](#bay)) no encontró relación significativa entre el número de autores y la visibilidad en literatura sobre matrimonio y familia.

En cuanto a la relación entre colaboración y citación, se ha demostrado en varios estudios esta posibilidad ([Glänzel 2001](#gla)). Narin & Whitlow ([1990](#nar90)) han encontrado evidencia de que los trabajos, con autoría múltiple internacional, duplican la frecuencia de citas de aquellos que no presentan colaboración.

El número de indicadores para medir el grado de coautoría es variado, fundamentalmente se emplean los índices de similitud, como el coseno de Salton o el Índice de Jaccard, así como los métodos o técnicas de mapeo para su representación gráfica ([Tijssen 1992](#tij); [Luukkonen _et al._ 1993](#luu93)).

<div align="center">![fig1](p245fig1.gif)    ![fig2](p245fig2.gif)</div>

A partir de la colaboración se puede entender la estructura social de la comunidad científica, por ejemplo la estructura de lo que se conoce como "Colegios Invisibles" ([Price 1963](#pri63); [Price & Beaver 1966](#pri66); [Crane 1972](#cra)). Price ([1970](#pri70)) estudió la autoría en la colaboración como indicios de vínculos sociales, y de este modo analizar los colegios invisibles y los grupos homogéneos.

El grado de colaboración ha variado según la época de estudio y según la materia estudiada, revelándose un fuerte incremento de la colaboración científica en la actualidad y en mayor proporción en aquellas disciplinas de carácter científico que en ciencias sociales y en humanidades. Los resultados del estudio de Bandyopadhyay ([2001](#ban)), sobre colaboración en una serie de disciplinas, confirman este hecho. Los datos obtenidos por este autor fueron los siguientes: física nuclear (72,5%), física (62,24%), ingeniería mecánica (36,6), matemáticas (36,3), filosofía (12,3) y ciencias políticas (3,8).

En el caso de España, para todos los campos combinados, Glänzel ([2001](#gla)) asigna un número de trabajos, en el bienio 1985/86, de 10.409 y una tasa de colaboración del 15,1%, mientras que el período 1995/96 fue de 29.538 trabajos y del 30% la tasa de colaboración.

Beaver ([2001](#bea01)) dice que la autoría en la colaboración sigue una distribución de Poisson, lo que significa un suceso relativamente raro, de modo gradual va tendiendo hacia una distribución binomial negativa a medida que la colaboración se hace más frecuente.

El objetivo de este artículo es ofrecer un análisis estadístico y bibliométrico de la colaboración científica de la Universidad Politécnica de Valencia a través de la circulación de los artículos de revistas y las comunicaciones a congresos recuperados de las principales Bases de Datos documentales (nacionales e internacionales) durante el periodo comprendido entre los años 1973-2001, ambos inclusive. Concretamente, se pretende conocer la colaboración entre los autores de artículos y los de comunicaciones a congresos, las entidades y los países que colaboran con la UPV.

## Fuentes y métodos

La Universidad Politécnica de Valencia fue creada como Instituto Politécnico Superior de Valencia en 1968, y en 1971 se constituye definitivamente en Universidad Politécnica de Valencia. En la actualidad, la comunidad universitaria está formada por unos 39.000 miembros. De ellos, cerca de 35.000 son alumnos 2.387 son profesores y 1.593 integran el grupo de personal de la administración. La Universidad está constituida por 15 centros universitarios, de los que 10 son escuelas técnicas superiores, 3 son facultades y 2 son escuelas politécnicas superiores. Además, cuenta con 5 centros adscritos.

La estructura departamental que se creó con la constitución de la Universidad se ha ido modificando a lo largo de los años; algunos han desaparecido, otros se han fusionado, se han creado nuevos departamentos, etc. En la actualidad se cuenta con 44 departamentos 10 laboratorios 19 institutos y 7 centros de investigación vinculados o adscritos.

La Universidad Politécnica de Valencia también ha crecido involucrándose plenamente en el ámbito de la Investigación y el Desarrollo (I+D) y contribuyendo con su capacidad científica y técnica. El crecimiento de las actividades de investigación, desarrollo tecnológico e innovación (I+D+I) ha experimentado un crecimiento paulatino, destacando en investigación contratada (proyectos bajo demanda de empresas, públicas y privadas) frente a un ajuste en el crecimiento de la investigación competitiva (proyectos sujetos a evaluación, selección externa financiados por subvención, etc.).

Para llevar a cabo la investigación se han consultado un total de 213 bases de datos entre nacionales e internacionales, de tal manera que cubrieran todas las disciplinas en las que produce la UPV. Finalmente, sólo 47 bases de datos proporcionaron documentos que fueron gestionados con Referente Manager.

El estudio de la producción y colaboración de la UPV abarca un período de tiempo de 1973 (año en el que aparece el primer trabajo publicado por la UPV) hasta 2001.

Las bases de datos Science Citation Index, producidas por el Institute for Scientific Information (ISI), son usadas, también, como fuentes de este estudio. El método de recuento de autores que se ha empleado ha sido el "normal or complete count", el cual concede el mismo número de créditos a todos los coautores (ver [Lindsey 1980](#lin)).

En la investigación se efectúa una distinción entre artículos de revistas y comunicaciones a congresos, porque consideramos que el comportamiento en el proceso de la colaboración científica y los motivos que llevan a este proceso son distintos en unos y en otros.

Respecto a la ecuación de búsqueda, se hizo en el campo "afiliación del autor" y se seleccionaron las palabras clave: Universidad, Politécnica, Valencia y UPV, realizando búsquedas aleatorias y adaptando estos términos a cada una de las bases de datos mediante truncamiento, con el fin de evitar tanto los problemas ortográfico como los idiomáticos.

Los resultados obtenidos, de las distintas búsquedas realizadas en las bases de datos, fueron gestionados por el programa Referente Manager, realizando un minucioso proceso de depuración con objeto de eliminar aquellos documentos recuperados no pertenecientes a nuestros objetivos (ruidos).

Debido al gran número de bases de datos utilizadas y al tratamiento diferente que cada una hace de la información, fue necesario establecer un proceso exhaustivo de normalización respecto a los autores, considerando que un autor podía aparecer de múltiples formas; con un solo apellido y el nombre; un apellido y la inicial; con dos apellidos y el nombre; con dos apellidos y la inicial ; los apellidos unidos formando uno solo; iniciando la descripción por el segundo apellido seguido de las iniciales, etc. A esta gran variedad de formas de aparición de los autores habría que añadir los errores tipográficos de las bases de datos.

## Resultados

La temática ha sido diversa, predominando las de tipo científico-técnicas sobre las ciencias sociales y humanidades. En total han sido 11 las disciplinas estudiadas: Agricultura; Alimentación; Ciencia y Tecnología; Ciencias Sociales y Humanidades; Informática, Electrónica y Comunicaciones; Ingeniería y Arquitectura; Matemáticas; Medicina; Medio Ambiente y Ciencias de la Vida; Química y Física; Multidisciplinar (Tabla 1).

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 1: Número de ocurrencias y porcentaje por disciplinas.**</caption>

<tbody>

<tr>

<th>Disciplinas</th>

<th>Ocurrencia de artículos</th>

<th>%</th>

<th>Ocurrencia de comunicaciones</th>

<th>%</th>

<th>Total</th>

<th>%</th>

</tr>

<tr>

<td>Agricultura</td>

<td align="center">2291</td>

<td align="center">15,32</td>

<td align="center">98</td>

<td align="center">5,80</td>

<td align="center">2389</td>

<td align="center">14,35</td>

</tr>

<tr>

<td>Alimentación</td>

<td align="center">267</td>

<td align="center">1,78</td>

<td align="center">5</td>

<td align="center">0,30</td>

<td align="center">272</td>

<td align="center">1,63</td>

</tr>

<tr>

<td>Ciencia y tecnología</td>

<td align="center">1522</td>

<td align="center">10,17</td>

<td align="center">43</td>

<td align="center">2,54</td>

<td align="center">1565</td>

<td align="center">9,40</td>

</tr>

<tr>

<td>Ciencias sociales y humanidades</td>

<td align="center">355</td>

<td align="center">2,37</td>

<td align="center">23</td>

<td align="center">1,36</td>

<td align="center">378</td>

<td align="center">2,27</td>

</tr>

<tr>

<td>Informática, electrónica y comunicaciones</td>

<td align="center">934</td>

<td align="center">6,24</td>

<td align="center">644</td>

<td align="center">38,08</td>

<td align="center">1578</td>

<td align="center">9,48</td>

</tr>

<tr>

<td>Ingeneiería y arquitectura</td>

<td align="center">921</td>

<td align="center">6,16</td>

<td align="center">226</td>

<td align="center">13,36</td>

<td align="center">1147</td>

<td align="center">6,89</td>

</tr>

<tr>

<td>Matemáticas</td>

<td align="center">1212</td>

<td align="center">8,10</td>

<td align="center">221</td>

<td align="center">13,07</td>

<td align="center">1433</td>

<td align="center">8,61</td>

</tr>

<tr>

<td>Medicinia</td>

<td align="center">309</td>

<td align="center">2,07</td>

<td align="center">2</td>

<td align="center">0,12</td>

<td align="center">311</td>

<td align="center">1,87</td>

</tr>

<tr>

<td>Medio ambiente y ciencias de la vida</td>

<td align="center">603</td>

<td align="center">4,03</td>

<td align="center">16</td>

<td align="center">0,95</td>

<td align="center">619</td>

<td align="center">3,72</td>

</tr>

<tr>

<td>Química y física</td>

<td align="center">2265</td>

<td align="center">15,14</td>

<td align="center">44</td>

<td align="center">2,60</td>

<td align="center">2309</td>

<td align="center">13,87</td>

</tr>

<tr>

<td>Multidisciplinares</td>

<td align="center">4280</td>

<td align="center">28,61</td>

<td align="center">369</td>

<td align="center">21,82</td>

<td align="center">4649</td>

<td align="center">27,92</td>

</tr>

<tr>

<th>Total</th>

<th>14959</th>

<th>100</th>

<th>1691</th>

<th>100</th>

<th>16650</th>

<th>100</th>

</tr>

</tbody>

</table>

La información total recuperada de las bases de datos ha sido de 5.464 artículos de revistas y 1.111 comunicaciones a congresos (total 6.575 registros), destacando la materia Multidisciplinar entre los artículos de revistas (28,6%) y la de Informática, Electrónica y Comunicaciones (38%) entre los congresos.

La evolución temporal de la producción científica de la UPV, así como el porcentaje en bases de datos ISI respecto al total, se encuentra resumida en la Tabla 2\. Si bien todos los registros recuperados han ido creciendo a lo largo del tiempo, el incremento mayor lo sufren los artículos publicados en revistas, recogidas por las bases de datos de ISI, sobre todo a partir de 1989 donde superan el 50% y alcanzan el 69% en 2000.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 2: Distribución anual comparada entre artículos y comunicaciones**</caption>

<tbody>

<tr>

<th>Año de producción</th>

<th>Comunicación a congresos</th>

<th>Aartículos todas BDs</th>

<th>Artículos BDs de ISI</th>

<th>% Art. en ISI respecto al total de art. todas BDs</th>

</tr>

<tr>

<td align="center">1973</td>

<td align="center">-</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">100,00%</td>

</tr>

<tr>

<td align="center">1974</td>

<td align="center">3</td>

<td align="center">3</td>

<td align="center">-</td>

<td align="center">0,00%</td>

</tr>

<tr>

<td align="center">1975</td>

<td align="center">-</td>

<td align="center">5</td>

<td align="center">1</td>

<td align="center">20,00%</td>

</tr>

<tr>

<td align="center">1976</td>

<td align="center">-</td>

<td align="center">11</td>

<td align="center">1</td>

<td align="center">9,09%</td>

</tr>

<tr>

<td align="center">1977</td>

<td align="center">-</td>

<td align="center">17</td>

<td align="center">6</td>

<td align="center">35,29%</td>

</tr>

<tr>

<td align="center">1978</td>

<td align="center">-</td>

<td align="center">9</td>

<td align="center">4</td>

<td align="center">44,44%</td>

</tr>

<tr>

<td align="center">1979</td>

<td align="center">-</td>

<td align="center">25</td>

<td align="center">6</td>

<td align="center">24,00%</td>

</tr>

<tr>

<td align="center">1980</td>

<td align="center">2</td>

<td align="center">24</td>

<td align="center">16</td>

<td align="center">66,67%</td>

</tr>

<tr>

<td align="center">1981</td>

<td align="center">1</td>

<td align="center">30</td>

<td align="center">10</td>

<td align="center">33,33%</td>

</tr>

<tr>

<td align="center">1982</td>

<td align="center">8</td>

<td align="center">32</td>

<td align="center">12</td>

<td align="center">37,50%</td>

</tr>

<tr>

<td align="center">1983</td>

<td align="center">7</td>

<td align="center">45</td>

<td align="center">15</td>

<td align="center">33,33%</td>

</tr>

<tr>

<td align="center">1984</td>

<td align="center">13</td>

<td align="center">65</td>

<td align="center">25</td>

<td align="center">38,46%</td>

</tr>

<tr>

<td align="center">1985</td>

<td align="center">18</td>

<td align="center">64</td>

<td align="center">27</td>

<td align="center">42,19%</td>

</tr>

<tr>

<td align="center">1986</td>

<td align="center">16</td>

<td align="center">90</td>

<td align="center">41</td>

<td align="center">45,56%</td>

</tr>

<tr>

<td align="center">1987</td>

<td align="center">25</td>

<td align="center">100</td>

<td align="center">50</td>

<td align="center">50,00%</td>

</tr>

<tr>

<td align="center">1988</td>

<td align="center">40</td>

<td align="center">98</td>

<td align="center">39</td>

<td align="center">39,80%</td>

</tr>

<tr>

<td align="center">1989</td>

<td align="center">17</td>

<td align="center">157</td>

<td align="center">80</td>

<td align="center">50,96%</td>

</tr>

<tr>

<td align="center">1990</td>

<td align="center">55</td>

<td align="center">152</td>

<td align="center">74</td>

<td align="center">48,68%</td>

</tr>

<tr>

<td align="center">1991</td>

<td align="center">37</td>

<td align="center">227</td>

<td align="center">115</td>

<td align="center">50,66%</td>

</tr>

<tr>

<td align="center">1992</td>

<td align="center">60</td>

<td align="center">256</td>

<td align="center">121</td>

<td align="center">47,27%</td>

</tr>

<tr>

<td align="center">1993</td>

<td align="center">73</td>

<td align="center">274</td>

<td align="center">177</td>

<td align="center">64,60%</td>

</tr>

<tr>

<td align="center">1994</td>

<td align="center">62</td>

<td align="center">363</td>

<td align="center">215</td>

<td align="center">59,23%</td>

</tr>

<tr>

<td align="center">1995</td>

<td align="center">56</td>

<td align="center">354</td>

<td align="center">224</td>

<td align="center">63,28%</td>

</tr>

<tr>

<td align="center">1996</td>

<td align="center">62</td>

<td align="center">388</td>

<td align="center">257</td>

<td align="center">66,24%</td>

</tr>

<tr>

<td align="center">1997</td>

<td align="center">96</td>

<td align="center">448</td>

<td align="center">303</td>

<td align="center">67,63%</td>

</tr>

<tr>

<td align="center">1998</td>

<td align="center">129</td>

<td align="center">501</td>

<td align="center">320</td>

<td align="center">63,87%</td>

</tr>

<tr>

<td align="center">1999</td>

<td align="center">129</td>

<td align="center">555</td>

<td align="center">384</td>

<td align="center">69,19%</td>

</tr>

<tr>

<td align="center">2000</td>

<td align="center">126</td>

<td align="center">594</td>

<td align="center">411</td>

<td align="center">69,19%</td>

</tr>

<tr>

<td align="center">2001</td>

<td align="center">76</td>

<td align="center">576</td>

<td align="center">363</td>

<td align="center">63,02%</td>

</tr>

<tr>

<td align="center">**Total parcial**</td>

<td align="center">**1111**</td>

<td align="center">**5464**</td>

<td align="center">**3298**</td>

<td align="center">**60,36%**</td>

</tr>

<tr>

<td align="center">**TOTAL**</td>

<td align="center" colspan="2">**6575** </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

</tbody>

</table>

En cuanto a la colaboración ha resultado notablemente alta, en comparación con estudios realizados por otros autores ya mencionado, y teniendo en cuenta que también están incluidas las materias de Ciencias Sociales y Humanidades, que, como es bien conocido, presentan menor grado de colaboración, que las ciencias.

Los artículos de revistas han alcanzado un grado de colaboración del 86,29, destacando las colaboraciones entre 2, 3 y 4 autores (Tabla 3).  El número de firmas por artículo (Índice de colaboración) fue de 3,3.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 3\. Colaboración según número de firmas en artículos de revistas**</caption>

<tbody>

<tr>

<th>Firmas/artículo</th>

<th>No. de artículos</th>

<th>%</th>

<th>% Acumul. de artículos</th>

<th>Número de firmas</th>

<th>% Firmas</th>

<th>% Acumul. de firmas</th>

</tr>

<tr>

<td align="center">1</td>

<td align="center">749</td>

<td align="center">13,71%</td>

<td align="center">13,71%</td>

<td align="center">749</td>

<td align="center">4,12%</td>

<td align="center">4,12%</td>

</tr>

<tr>

<td align="center">2</td>

<td align="center">1211</td>

<td align="center">22,16%</td>

<td align="center">35,87%</td>

<td align="center">2422</td>

<td align="center">13,32%</td>

<td align="center">17,44%</td>

</tr>

<tr>

<td align="center">3</td>

<td align="center">1319</td>

<td align="center">24,14%</td>

<td align="center">60,01%</td>

<td align="center">3957</td>

<td align="center">21,77%</td>

<td align="center">39,21%</td>

</tr>

<tr>

<td align="center">4</td>

<td align="center">1038</td>

<td align="center">19,00%</td>

<td align="center">79,01%</td>

<td align="center">4152</td>

<td align="center">22,84%</td>

<td align="center">62,05%</td>

</tr>

<tr>

<td align="center">5</td>

<td align="center">557</td>

<td align="center">10,19%</td>

<td align="center">89,20%</td>

<td align="center">2785</td>

<td align="center">15,32%</td>

<td align="center">77,37%</td>

</tr>

<tr>

<td align="center">6</td>

<td align="center">292</td>

<td align="center">5,34%</td>

<td align="center">94,55%</td>

<td align="center">1752</td>

<td align="center">9,64%</td>

<td align="center">87,01%</td>

</tr>

<tr>

<td align="center">7</td>

<td align="center">173</td>

<td align="center">3,17%</td>

<td align="center">97,71%</td>

<td align="center">1211</td>

<td align="center">6,66%</td>

<td align="center">93,67%</td>

</tr>

<tr>

<td align="center">8</td>

<td align="center">59</td>

<td align="center">1,08%</td>

<td align="center">98,79%</td>

<td align="center">472</td>

<td align="center">2,60%</td>

<td align="center">96,27%</td>

</tr>

<tr>

<td align="center">9</td>

<td align="center">35</td>

<td align="center">0,64%</td>

<td align="center">99,43%</td>

<td align="center">315</td>

<td align="center">1,73%</td>

<td align="center">98,00%</td>

</tr>

<tr>

<td align="center">10</td>

<td align="center">13</td>

<td align="center">0,24%</td>

<td align="center">99,67%</td>

<td align="center">130</td>

<td align="center">0,72%</td>

<td align="center">98,72%</td>

</tr>

<tr>

<td align="center">11</td>

<td align="center">5</td>

<td align="center">0,09%</td>

<td align="center">99,76%</td>

<td align="center">55</td>

<td align="center">0,30%</td>

<td align="center">99,02%</td>

</tr>

<tr>

<td align="center">12</td>

<td align="center">6</td>

<td align="center">0,11%</td>

<td align="center">99,87%</td>

<td align="center">72</td>

<td align="center">0,40%</td>

<td align="center">99,42%</td>

</tr>

<tr>

<td align="center">13</td>

<td align="center">1</td>

<td align="center">0,02%</td>

<td align="center">99,89%</td>

<td align="center">13</td>

<td align="center">0,07%</td>

<td align="center">99,49%</td>

</tr>

<tr>

<td align="center">14</td>

<td align="center">2</td>

<td align="center">0,04%</td>

<td align="center">99,93%</td>

<td align="center">28</td>

<td align="center">0,15%</td>

<td align="center">99,64%</td>

</tr>

<tr>

<td align="center">15</td>

<td align="center">2</td>

<td align="center">0,04%</td>

<td align="center">99,96%</td>

<td align="center">30</td>

<td align="center">0,17%</td>

<td align="center">99,81%</td>

</tr>

<tr>

<td align="center">17</td>

<td align="center">1</td>

<td align="center">0,02%</td>

<td align="center">99,98%</td>

<td align="center">17</td>

<td align="center">0,09%</td>

<td align="center">99,90%</td>

</tr>

<tr>

<td align="center">18</td>

<td align="center">1</td>

<td align="center">0,02%</td>

<td align="center">100,00%</td>

<td align="center">18</td>

<td align="center">0,10%</td>

<td align="center">100,00%</td>

</tr>

<tr>

<th>TOTAL</th>

<th>5464</th>

<th>100%</th>

<th> </th>

<th>18178</th>

<th>100%</th>

<th> </th>

</tr>

</tbody>

</table>

Las comunicaciones a congresos han tenido aún mayor grado de colaboración que los artículos de revista, este ha sido del 89%, destacando, igualmente, las colaboraciones entre 2, 3 y 4 autores (Tabla 4). El número de firmas en este caso fue algo menor que en el de los artículos que fue de 3,1.

El hecho de tener un mayor porcentaje de colaboraciones y un menor índice de colaboración de las comunicaciones a congresos que las revistas se debe a que hay más trabajos en colaboración pero con menos autores en el caso de las comunicaciones a congresos.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 4\. Colaboración según número de firmas en comunicaciones**</caption>

<tbody>

<tr>

<th>Firmas/comun.</th>

<th>No. de comunic.</th>

<th>%</th>

<th>% Acumul.</th>

<th>No. de firmas</th>

<th>% Firmas</th>

<th>% Acumul.</th>

</tr>

<tr>

<td align="center">1</td>

<td align="center">116</td>

<td align="center">10,44%</td>

<td align="center">10,44%</td>

<td align="center">116</td>

<td align="center">3,33%</td>

<td align="center">3,33%</td>

</tr>

<tr>

<td align="center">2</td>

<td align="center">309</td>

<td align="center">27,81%</td>

<td align="center">38,25%</td>

<td align="center">618</td>

<td align="center">17,72%</td>

<td align="center">21,04%</td>

</tr>

<tr>

<td align="center">3</td>

<td align="center">296</td>

<td align="center">26,64%</td>

<td align="center">64,90%</td>

<td align="center">888</td>

<td align="center">25,46%</td>

<td align="center">46,50%</td>

</tr>

<tr>

<td align="center">4</td>

<td align="center">214</td>

<td align="center">19,26%</td>

<td align="center">84,16%</td>

<td align="center">856</td>

<td align="center">24,54%</td>

<td align="center">71,04%</td>

</tr>

<tr>

<td align="center">5</td>

<td align="center">109</td>

<td align="center">9,81%</td>

<td align="center">93,97%</td>

<td align="center">545</td>

<td align="center">15,63%</td>

<td align="center">86,67%</td>

</tr>

<tr>

<td align="center">6</td>

<td align="center">37</td>

<td align="center">3,33%</td>

<td align="center">97,30%</td>

<td align="center">222</td>

<td align="center">6,36%</td>

<td align="center">93,03%</td>

</tr>

<tr>

<td align="center">7</td>

<td align="center">16</td>

<td align="center">1,44%</td>

<td align="center">98,74%</td>

<td align="center">112</td>

<td align="center">3,21%</td>

<td align="center">96,24%</td>

</tr>

<tr>

<td align="center">8</td>

<td align="center">6</td>

<td align="center">0,54%</td>

<td align="center">99,28%</td>

<td align="center">48</td>

<td align="center">1,38%</td>

<td align="center">97,62%</td>

</tr>

<tr>

<td align="center">9</td>

<td align="center">3</td>

<td align="center">0,27%</td>

<td align="center">99,55%</td>

<td align="center">27</td>

<td align="center">0,77%</td>

<td align="center">98,39%</td>

</tr>

<tr>

<td align="center">11</td>

<td align="center">4</td>

<td align="center">0,36%</td>

<td align="center">99,91%</td>

<td align="center">44</td>

<td align="center">1,26%</td>

<td align="center">99,66%</td>

</tr>

<tr>

<td align="center">12</td>

<td align="center">1</td>

<td align="center">0,09%</td>

<td align="center">100,00%</td>

<td align="center">12</td>

<td align="center">0,34%</td>

<td align="center">100,00%</td>

</tr>

<tr>

<td align="center">**TOTAL**</td>

<td align="center">**1111**</td>

<td align="center">**100%**</td>

<td align="center"> </td>

<td align="center">**3488**</td>

<td align="center">**100%**</td>

<td align="center"> </td>

</tr>

</tbody>

</table>

Respecto a la colaboración institucional, tanto nacional como internacional, esta ha sido muy alta. En total han sido 2.472 colaboraciones del total de 5.464 artículos de revistas. El grado de diversificación, también consideramos que ha sido muy importante, ya que se han contabilizado 576 instituciones, entre ellas 317 universidades (70,5%), de las cuales 46 son españolas (Tabla 5). El número de países con los que colaboró la UPV fue de 51 países, lo que supone casi el 43% del total de la colaboración; el grado de colaboración con entidades españolas supone más del 57%. Destacan Estados Unidos y Reino Unido con casi un 6% cada uno, Alemania y Francia con más del 4% e Italia con el 2,7% del total de la colaboración.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 5: Entidades colaboradoras con la UPV y grado de colaboración**</caption>

<tbody>

<tr>

<th>Tipos de entidades</th>

<th>No. de entidades</th>

<th>% Entidades sobre total</th>

<th>Grado de colabor.</th>

<th>% Colab.</th>

</tr>

<tr>

<td>Universidades</td>

<td align="center">317</td>

<td align="center">55,03%</td>

<td align="center">1743</td>

<td align="center">70,51%</td>

</tr>

<tr>

<td>Institutos, Academias, Centros</td>

<td align="center">149</td>

<td align="center">25,87%</td>

<td align="center">503</td>

<td align="center">20,35%</td>

</tr>

<tr>

<td>Empresas privadas</td>

<td align="center">73</td>

<td align="center">12,67%</td>

<td align="center">104</td>

<td align="center">4,21%</td>

</tr>

<tr>

<td>Organismos públicos</td>

<td align="center">37</td>

<td align="center">6,42%</td>

<td align="center">122</td>

<td align="center">4,94%</td>

</tr>

<tr>

<td>**TOTAL**</td>

<td align="center">**576**</td>

<td align="center">**100%**</td>

<td align="center">**2472**</td>

<td align="center">**100%**</td>

</tr>

</tbody>

</table>

En cuanto al origen de las instituciones colaboradoras destacan las españolas con un porcentaje próximo al 28%, Estados Unidos con un 11,4%, Reino Unido con el 9%, Francia tonel 7,8%, Alemania con el 6,7% e Italia con 5,5% (Tabla 6).

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 6: Colaboración institucional por países**</caption>

<tbody>

<tr>

<th>Países colaboradores</th>

<th>Número de entidades</th>

<th>% Entidades</th>

<th>Grado de colabor.</th>

<th>% Colab.</th>

</tr>

<tr>

<td>Albania</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Alemania</td>

<td align="center">39</td>

<td align="center">6,77%</td>

<td align="center">120</td>

<td align="center">4,85%</td>

</tr>

<tr>

<td>Argelia</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Argentina</td>

<td align="center">9</td>

<td align="center">1,56%</td>

<td align="center">12</td>

<td align="center">0,49%</td>

</tr>

<tr>

<td>Australia</td>

<td align="center">5</td>

<td align="center">0,87%</td>

<td align="center">7</td>

<td align="center">0,28%</td>

</tr>

<tr>

<td>Austria</td>

<td align="center">3</td>

<td align="center">0,52%</td>

<td align="center">6</td>

<td align="center">0,24%</td>

</tr>

<tr>

<td>Bangla Desh</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Bélgica</td>

<td align="center">4</td>

<td align="center">0,69%</td>

<td align="center">6</td>

<td align="center">0,24%</td>

</tr>

<tr>

<td>Bolivia</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Botswana</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Brasil</td>

<td align="center">15</td>

<td align="center">2,60%</td>

<td align="center">35</td>

<td align="center">1,42%</td>

</tr>

<tr>

<td>Bulgaria</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Canadá</td>

<td align="center">11</td>

<td align="center">1,91%</td>

<td align="center">66</td>

<td align="center">2,67%</td>

</tr>

<tr>

<td>Chile</td>

<td align="center">3</td>

<td align="center">0,52%</td>

<td align="center">34</td>

<td align="center">1,38%</td>

</tr>

<tr>

<td>China</td>

<td align="center">4</td>

<td align="center">0,69%</td>

<td align="center">4</td>

<td align="center">0,16%</td>

</tr>

<tr>

<td>Colombia</td>

<td align="center">5</td>

<td align="center">0,87%</td>

<td align="center">13</td>

<td align="center">0,53%</td>

</tr>

<tr>

<td>Corea del Sur</td>

<td align="center">4</td>

<td align="center">0,69%</td>

<td align="center">8</td>

<td align="center">0,32%</td>

</tr>

<tr>

<td>Costa Rica</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">2</td>

<td align="center">0,08%</td>

</tr>

<tr>

<td>Cuba</td>

<td align="center">7</td>

<td align="center">1,22%</td>

<td align="center">11</td>

<td align="center">0,44%</td>

</tr>

<tr>

<td>Dinamarca</td>

<td align="center">5</td>

<td align="center">0,87%</td>

<td align="center">5</td>

<td align="center">0,20%</td>

</tr>

<tr>

<td>España</td>

<td align="center">161</td>

<td align="center">27,95%</td>

<td align="center">1425</td>

<td align="center">57,65%</td>

</tr>

<tr>

<td>Estados Unidos</td>

<td align="center">66</td>

<td align="center">11,46%</td>

<td align="center">147</td>

<td align="center">5,95%</td>

</tr>

<tr>

<td>Finlandia</td>

<td align="center">3</td>

<td align="center">0,52%</td>

<td align="center">14</td>

<td align="center">0,57%</td>

</tr>

<tr>

<td>Francia</td>

<td align="center">45</td>

<td align="center">7,81%</td>

<td align="center">103</td>

<td align="center">4,17%</td>

</tr>

<tr>

<td>Grecia</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">19</td>

<td align="center">0,77%</td>

</tr>

<tr>

<td>Holanda</td>

<td align="center">15</td>

<td align="center">2,60%</td>

<td align="center">25</td>

<td align="center">1,01%</td>

</tr>

<tr>

<td>Irlanda</td>

<td align="center">2</td>

<td align="center">0,35%</td>

<td align="center">5</td>

<td align="center">0,20%</td>

</tr>

<tr>

<td>Israel</td>

<td align="center">2</td>

<td align="center">0,35%</td>

<td align="center">2</td>

<td align="center">0,08%</td>

</tr>

<tr>

<td>Italia</td>

<td align="center">32</td>

<td align="center">5,56%</td>

<td align="center">67</td>

<td align="center">2,71%</td>

</tr>

<tr>

<td>Japón</td>

<td align="center">6</td>

<td align="center">1,04%</td>

<td align="center">8</td>

<td align="center">0,32%</td>

</tr>

<tr>

<td>Malasia</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Marruecos</td>

<td align="center">3</td>

<td align="center">0,52%</td>

<td align="center">9</td>

<td align="center">0,36%</td>

</tr>

<tr>

<td>México</td>

<td align="center">8</td>

<td align="center">1,39%</td>

<td align="center">25</td>

<td align="center">1,01%</td>

</tr>

<tr>

<td>Noruega</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">4</td>

<td align="center">0,16%</td>

</tr>

<tr>

<td>Pakistán</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Panamá</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Perú</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Polonia</td>

<td align="center">6</td>

<td align="center">1,04%</td>

<td align="center">23</td>

<td align="center">0,93%</td>

</tr>

<tr>

<td>Portugal</td>

<td align="center">6</td>

<td align="center">1,04%</td>

<td align="center">20</td>

<td align="center">0,81%</td>

</tr>

<tr>

<td>Reino Unido</td>

<td align="center">52</td>

<td align="center">9,03%</td>

<td align="center">144</td>

<td align="center">5,83%</td>

</tr>

<tr>

<td>República Checa</td>

<td align="center">2</td>

<td align="center">0,35%</td>

<td align="center">2</td>

<td align="center">0,08%</td>

</tr>

<tr>

<td>Rumania</td>

<td align="center">3</td>

<td align="center">0,52%</td>

<td align="center">6</td>

<td align="center">0,24%</td>

</tr>

<tr>

<td>Rusia</td>

<td align="center">7</td>

<td align="center">1,22%</td>

<td align="center">13</td>

<td align="center">0,53%</td>

</tr>

<tr>

<td>Sudáfrica</td>

<td align="center">2</td>

<td align="center">0,35%</td>

<td align="center">4</td>

<td align="center">0,16%</td>

</tr>

<tr>

<td>Suecia</td>

<td align="center">5</td>

<td align="center">0,87%</td>

<td align="center">8</td>

<td align="center">0,32%</td>

</tr>

<tr>

<td>Suiza</td>

<td align="center">14</td>

<td align="center">2,43%</td>

<td align="center">31</td>

<td align="center">1,25%</td>

</tr>

<tr>

<td>Taiwán</td>

<td align="center">2</td>

<td align="center">0,35%</td>

<td align="center">3</td>

<td align="center">0,12%</td>

</tr>

<tr>

<td>Ucrania</td>

<td align="center">2</td>

<td align="center">0,35%</td>

<td align="center">13</td>

<td align="center">0,53%</td>

</tr>

<tr>

<td>Uruguay</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">1</td>

<td align="center">0,04%</td>

</tr>

<tr>

<td>Venezuela</td>

<td align="center">3</td>

<td align="center">0,52%</td>

<td align="center">10</td>

<td align="center">0,40%</td>

</tr>

<tr>

<td>Zimbabwe</td>

<td align="center">1</td>

<td align="center">0,17%</td>

<td align="center">2</td>

<td align="center">0,08%</td>

</tr>

<tr>

<td>**TOTAL**</td>

<td align="center">**576**</td>

<td align="center">**100%**</td>

<td align="center">**2472**</td>

<td align="center">**100%**</td>

</tr>

</tbody>

</table>

## Resumen y discusión

En este estudio, se muestran las características estructurales de la UPV, respecto a la cooperación científica, así como su posible sintonía con el comportamiento general que se viene observando en las últimas décadas sobre este proceso.

De todos es bien conocido que en el desarrollo y avance de la ciencia tiene un fuerte protagonismo las interacciones entre científicos. De ahí la gran importancia que adquiere analizar la estructura de la colaboración científica.

El estudio que hemos realizado se puede considerar, desde el punto de vista del material tratado, como multidisciplinar (desde áreas científico-técnicas hasta ciencias sociales y humanidades), si bien predominan las de materias científico-técnicas. 

El material de estudio ha sido agrupado en artículos de revistas (80%) y en comunicaciones a congresos, aunque el comportamiento resultante ha sido muy similar en ambos grupos.

Los resultados globales muestran un alto porcentaje de colaboración científica en la UPV. A este aspecto dedicaremos nuestro siguiente comentario.

Aunque es cierta la existencia de una fuerte subida de la cooperación a todos los niveles, han sido escasos los casos que nos hemos encontrado con resultados superiores o similares a los obtenidos en el presente estudio (más del 86% de colaboración). En el estudio de Bandyopadhyay ([2001](#ban)), tan solo la física nuclear se aproxima (72,5%). En otro estudio, Glänzel ([2001](#gla)) obtiene un 30% de colaboración para España, en la última década, si bien se trata de una combinación de todos los campos científicos. En estudios llevados a cabo por alguno de nosotros se han obtenido resultado resultados similares en unos casos y muy distintos en otros. Por ejemplo, en Pulgarín et al. ([2003](#pul03)), el % de colaboración alcanzado por la Universidad de Extremadura (UEX) fue del 89%, aunque en este caso hay que explicar la procedencia de los datos con los que se trabajó. Estos datos no fueron todas las publicaciones de la UEX, sino solo aquellas que aparecieron en las bases de datos internacionales y, además, de áreas científico-técnicas. En cambio en Pulgarín et al. ([2004](#pul04)), en el que los datos procedían de bases de datos nacionales y de áreas, preferentemente, de ciencias sociales y humanidades, apenas se llegó al 46% de colaboración.

La procedencia de los datos, referida en el comentario anterior, nos da pie para iniciar un segundo comentario que tiene que ver con otro aspecto de los resultados de nuestro trabajo. Se trata de la existencia o no de una posible relación entre el grado de autoría y la calidad (según ciertos autores), impacto, difusión o visibilidad (según la generalidad de autores) de las publicaciones ([Cole & Cole 1973](#col); [Beaver & Rosen 1979a](#bea79a); [Pao 1992](#pao92)). Los resultados de este estudio, estarían de acuerdo con los de Narin et al. ([1991](#nar91)), quienes ven una posible relación entre la calidad y la colaboración internacional. Estos autores comprueban un mayor impacto y visibilidad cuando se trabaja en colaboración internacional que cuando la colaboración es nacional. En el mismo sentido, Gómez Caridad et al. ([1999](#gom)) consideran, también, que la colaboración internacional aumenta la visibilidad de los trabajos de investigación, al publicarse en revistas de mayor impacto, que los trabajos en colaboración nacional. Según nuestros resultados, no podemos estar más de acuerdo con estas revelaciones, ya que la UPV presenta un alto % de colaboración internacional (más del 40%), un número de publicaciones, considerablemente elevado, procedentes de las bases de datos de ISI (aproximadamente el 60% de la producción total) y la colaboración científica de la misma que supera el 85%. Con estos datos podemos deducir, por lo tanto, que existe una relación entre el número de colaboraciones y la difusión o visibilidad (entendida como porcentaje de apariciones en ISI) alcanzada por la UPV.

En apoyo de la aseveración que acabamos de hacer y manifestar nuestro acuerdo con otras características estructurales de la colaboración científica, iniciaremos este tercer comentario. Nos referimos a la colaboración institucional. Con objeto de glosar nuestro cometario nos referiremos al trabajo reciente de Lascurain & Sanz ([2002](#las)), sobre las universidades españolas que imparten psicología y en el que analizan la colaboración institucional, en dicha materia. Lascurain & Sanz encuentran que entre las publicaciones nacionales predominan las colaboraciones nacionales (solo el 0,48% de estos trabajos colaboran con otros países). En cambio en los trabajos publicados en fuentes internacionales, el grado de colaboración internacional creció de un modo muy considerable (cerca del 9%). En nuestro trabajo, el número de instituciones con las que se colaboró fue de 576, de 51 países diferentes. De estas colaboraciones institucionales (45% del total de colaboraciones), más del 42% fueron con instituciones internacionales, lo que nos lleva a suponer que estos datos estarían correlacionados con los manifestados en el estudio anterior, acerca de la posible relación, también, entre la colaboración científica internacional (entre instituciones) y la visibilidad de la UPV como institución.

## Referencias

*   <a name="aro" id="aro"></a>Arora, J., & Pawan, U. (1995). Collaborative research and authorship patterns in immunology: correlation between multiple authorship and citedness. _IASLIC Bulletin_, **40**(2), 73-83.
*   <a name="ban" id="ban"></a>Bandyopadhyay, A.K. (2001). Authorship pattern in different disciplines. _Annals of Library and Information Studies_, **48**(4), 139-147.
*   <a name="bay" id="bay"></a>Bayer, A.E. (1982). A bibliometric analysis of marriage and family literature. _Journal of Marriage and the Family_, **44**(3), 527-538.
*   <a name="bea01" id="bea01"></a>Beaver, D. de B. (2001). Reflections on scientific collaboration (and its study): past, present, and future. _Scientometrics_, **52**(3), 365-377.
*   <a name="bea78" id="bea78"></a>Beaver, D. de B.,& Rosen, R. (1978). Studies in scientific collaboration I. The professional origins of scientific co-authorship. _Scientometrics_, **1**(1), 65-84.
*   <a name="bea79a" id="bea79a"></a>Beaver, D. de B. & Rosen, R. (1979a). Studies in scientific collaboration II. Scientific co-authorship, research productivity and visibility in the French scientific elite 1799-1830\. _Scientometrics_, **1**(2), 133-149.
*   <a name="bea79b" id="bea79b"></a>Beaver, D. de B. & Rosen, R. (1979b). Studies in scientific collaboration III. Professionalization and the natural history of modern scientific co-authorship. _Scientometrics_, **1**(3), 231-245.
*   <a name="bra" id="bra"></a>Braun, T., Glänzel, W. & Schubert, A. (2001). Publication and cooperation patterns of the authors of neuroscience journals. _Scientometrics_, **51**(3), 499-510.
*   <a name="col" id="col"></a>Cole, J. R., & Cole, S. (1973). _Social stratification in science._ Chicago, IL: University of Chicago Press.
*   <a name="cra" id="cra"></a>Crane, D. (1972). _Invisible college: diffusion of knowledge in scientific communities_. Chicago, IL: University of Chicago Press.
*   <a name="gla" id="gla"></a>Glänzel, W. (2001). National characteristics in international scientific co-authorship relations. _Scientometrics_, **51**(1), 69-115.
*   <a name="gom" id="gom"></a>Gómez-Caridad, I., Fernández, M.T. & Sebastián, J. (1999). Analysis of the structure of international scientific cooperation networks through bibliometric indicators. _Scientometrics_, **44**(3), 441-457.
*   <a name="las" id="las"></a>Lascurain-Sánchez, M.L., & Sanz-Casado, E. (2002). Análisis de la estructura de la colaboración internacional en las universidades españolas en las que se imparte Psicología. _Revista de Historia de la Psicología,_ **23**(3-4), 575-583.
*   <a name="lin" id="lin"></a>Lindsey, D. (1980). Production and citation measures in the sociology of science: the problem of multiple authorship. _Social Studies of Science_, **10**(1), 145-162.
*   <a name="lot" id="lot"></a>Lotka, A.J. (1926). The frequency distribution of scientific productivity. _Journal of the Washington Academy of Sciences_, **16**(12), 317-323.
*   <a name="luu92" id="luu92"></a>Luukkonen, T., Persson, O., & Sivertsen, G. (1992). Understanding patterns of international scientific collaboration. _Science, Technology and Human Values_, **17**(1) 101-126.
*   <a name="luu93" id="luu93"></a>Luukkonen, T., Tijssen, R.J.W., Persson, O., & Sivertsen, G. (1993). The measurement of international scientific collaboration. _Scientometrics_, **28**(1), 15-36.
*   <a name="nar90" id="nar90"></a>Narin, F., & Whitlow, E.S. (1990). _Measurement of scientific cooperation and coauthorship in CEC-related areas of science._ Luxebourg: European Community. (Report EUR 12900)
*   <a name="nar91" id="nar"></a>Narin, F., Stevens, K., & Whitlow, E.S. (1991). Scientific co-operation in Europe and the citation of multinationally authored papers. _Scientometrics_, **21**(3), 313-323.
*   <a name="pao80" id="pao80"></a>Pao, M.L. (1980). Co-authorship and productivity. _Proceedings of the American Society for Information Sciences,_ **17**, 279-289.
*   <a name="pao82" id="pao82"></a>Pao, M.L. (1982). Collaboration in computation musicology. _Journal of the American Society for Information Science_, **33**(1), 38-43.
*   <a name="pao92" id="pao92"></a>Pao, M.L. (1992). Global and local collaboration: a study of scientific collaboration. _Information Processing & Management_, **28**(1), 99-109.
*   <a name="pra" id="pra"></a>Pravdic, N. & Oluic-Vukovic, V. (1986). Dual approach to multiple authorship in the study of collaborator/scientific output relationship. _Scientometrics_, **10**(5-6), 259-280.
*   <a name="pri63" id="pri63"></a>Price, D.J. de Solla (1963). _Little science, big science_. New York, NY: Columbia University Press.
*   <a name="pri70" id="pri70"></a>Price, D.J. de Solla (1970). Citation measures of hard science, soft science, technology and non-science. In: C.E. Nelson, & D.K. Pollack (Eds.), _Communication among scientists and engineers_, (pp.3-22). Lexington, MA: D.C. Heath.
*   <a name="pri66" id="pri66"></a>Price, D.J. de Solla, & Beaver, D. de B. (1966). Collaboration in an invisible college. _American Psychologist_, **21**(11), 1011-1018.
*   <a name="pul03" id="pul03"></a>Pulgarín, A., González-Calatrava, I., Escalona-Fernández, I., & Pérez-Pulido, M. (2003). _Estudio bibliométrico de la producción científica y tecnológica de la Universidad de Extremadura: análisis de la difusión alcanzada en bases de datos internacionales. Período 1991-2000._ Cáceres, Spain: Universidad de Extremadura.
*   <a name="pul04" id="pul04"></a>Pulgarín, A., González-Calatrava, I., Escalona-Fernández, I., & Pérez-Pulido, M. (2004). _Estudio bibliométrico de la producción científica de la Universidad de Extremadura: Análisis de la difusión alcanzada en bases de datos nacionales. Período 1974-2001._ Cáceres, Spain: Universidad de Extremadura.
*   <a name="qin" id="qin"></a>Qin, J. (1995). Collaboration and publication productivity: an experiment with a new variable in Lotka's Law. In M. Koening, & A. Bookstein (Eds.), _Proceedings of the Fifth Biennial International Conference of the International Society for Scientometrics and Informetrics_, (pp. 445-454). Medford, NJ: Learned Information.
*   <a name="tij" id="tij"></a>Tijssen, R.J.W. (1992). _Cartography of science: scientometric mapping with multidimensional scaling methods_. Leiden, The Netherlands: DSWO Press.
*   <a name="wei" id="wei"></a>Weinstock, M. (1971). Citation indexes. In A. Kent, (Ed.) _Encyclopaedia of Library and Information Science_, Volume 5 (pp. 16-40). New York, NY: Marcel Dekker.
*   <a name="zuc" id="zuc"></a>Zuckerman, H.A. (1967). Nobel laureates in science: patterns of productivity, collaboration, and authorship. _American Sociological Review_, **32**(3), 391-403.

