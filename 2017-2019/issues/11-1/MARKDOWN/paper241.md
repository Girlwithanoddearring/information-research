#### Vol. 11 No. 1, October, 2005



# Environmental scanning: how developed is information acquisition in Western European companies?

#### [David Benczúr](mailto:david@benczur.hu)  
Department of Information and Knowledge Management  
Budapest University of Technology and Economics,  
Mûegyetem rkp. 3-9, H-1111 Budapest, Hungary



#### Abstract

> **Introduction.** A number of theoretical works focus on the potential revolutionary impact of the Internet and other Information and Communication Technologies (ICT) upon Competitive Intelligence, but only a few empirical research papers can be found on it. Is the real impact still unknown, or is it too insignificant to talk about? The present paper searches for the answers to this question both in literature and on the field, focusing on the point where the impact is expected to be the greatest: Information Gathering.  
> **Method.** Important empirical essays in academic literature were overviewed, including American and also French surveys. Based on literature, hypotheses were established and tested on an existing database, containing information on more than 500 firms collected over two consecutive years. The sample was constructed through telephone interviews and was destined to test the effect of information and communication technologies on several aspects of management.  
> **Analysis.** Hypotheses were classed into seven groups with respect to the influence of external environment, internal structure and development of IT on the information acquisition activities of firms. Quantitative methods were used to carry out tests on the sample.  
> **Results.** Internal structure shows only partial influence, but for IT and external environment, the correlation was high. On the other hand, firms are still underdeveloped.  
> **Conclusion.** The presented results provide better understanding of the level of development of Information Gathering in firms and of the factors influencing it, and suggest new ways for further research in order to understand why firms are still underdeveloped.



## Introduction

'External Information is a key input in strategic decision-making' ([Pawar & Sharda 1997: 111](#Pawar)). Competitive intelligence is a set of practices or formalised processes in organizations aiming to gather relevant information about competitors to stay one step ahead in middle- and long-range planning ([Teo & Choo 2000](#Teo), [Ettore 1995](#Ettore)). Information about competitors is merely one piece of all the relevant information, thus scanning must also address technology, success of competitors' products in the market and the whole environment, including economic, legal, cultural and demographic background ([Revelli 2000](#Revelli), [Hermel 2001](#Hermel))

Environmental scanning is predominantly an information acquisition activity ([Aguilar 1967](#Aguilar)), while _business intelligence_ or _competitive intelligence_ are broader terms including information processing and dissemination in addition to information acquisition ([Herring 1988](#Herring)). Nevertheless, the terms competitive intelligence and environmental ecanning are both used in this paper, as the focus is on information acquisition, which is common for both competitive intelligence and environmental scanning. This _information gathering_ differs from industrial or economic espionage in strictly avoiding illegal practices. Legality is the key in using the Internet for competitive intelligence, and 90% of strategically relevant information is freely and legally accessible on the Internet ([McGonagle & Vella 1998](#McGonagle), [Revelli 2000](#Revelli)).

A number of works discussed how information and communication technologies would make easier and more effective competitive intelligence, Chief Executive monitoring or environmental scanning ([Huber 1990](#Huber), [Bournois & Romani 2000](#Bournois), [Revelli 2000](#Revelli), [Choo 2001](#Choo), [Benczúr 2002](#Benczur)). However, these works often present only a theoretical perspective, without providing field studies. As Teo and Choo observed, 'there is little empirical work on the impact of Internet on CI' ([2001: 67](#Teo)).

Z. Karvalics observed a lack of papers not only for empirical works, but also for the entire topic. 'Corporate Input Information is hardly covered by the literature compared to Internal Information Flow' ([Z. Karvalics 2001: 76](#ZKarvalics)). This means that a lot has been written on how information systems process business information inside organizations to help workflow, communication, knowledge management and decision-making, but only few focused on the input interface of these systems to external information. (_Input information_ is the term used to define external information that has an impact upon decision-making in companies.) This statement can be easily verified: if one types _'business intelligence'_ into Google, it provides some 45,000,000 references. If we test major internal information sources: _'business intelligence_ AND _ERP'_ provides more than 10,000,000; _'business intelligence_ AND _CRM'_ more than 15,000,000; _'business intelligence_ AND _data warehousing'_ more than 7,000,000 million. On the contrary _'business intelligence_ AND _environmental scanning'_ provides only 11,400 occurrences, _'business intelligence_ AND _competitive intelligence'_ 225,000, while _'environmental scanning'_ itself has 232,000 references and _'competitive intelligence'_ little more than 2,000,000.

In fact, there are two important questions: first, how do companies really manage input information, and secondly, why are there so few studies on it. This paper aims to investigate what companies really do; how do they benefit from the Internet to gather strategically relevant information and what factors may be identified that are responsible for the (under)development of environmental scanning. Earlier empirical works are reviewed first, then seven groups of hypotheses, derived in part from the literature, are verified by means of a large survey of companies. The theoretical basis of environmental scanning and competitive intelligence is not discussed here, but the interested reader may find good summaries among the referenced papers ([Aguilar 1967](#Aguilar), [Choo 2001](#Choo), [Correia & Wilson 2001](#Correia2)). As for a general approach from the point of view of strategic management we recommend the chapter on the Environmental School in Mintzberg _et al._ ([1998](#Mintzberg2)).

## Earlier empirical works

Environmental scanning emerged in the 1960s, when company executives began to speak about it openly and it became part of strategic planning ([Russel & Prince 1992](#Russel)). But it was only in the 1980s that business intelligence systems and management information systems appeared, promising more effective use of external information. Competitive intelligence had quite large and interesting literature at that time, which is where the following literature review starts. It is not a presentation of the state of the art, it is rather the overview of the most important empirical works. The emphasis is more upon what was examined than on what was found. With this approach, the intention is to look for the reasons for the lack of empirical work.

### Studies before the World Wide Web

The first study to be mentioned is Fulds ([1985](#Fuld1)). The findings therein were based on a questionnaire, nineteen pages long, answered by only thirteen leading planning and market analysts. The study concluded that the most wanted information were return on products, marketing strategy and production costs, independently of sector. He also found that most competitive intelligence systems were rather underdeveloped.

Later, Fuld completed the study ([1988](#Fuld2)) with personal and telephone interviews, which showed that competitive intelligence had become more widely known over the course of three years, but systems were still underdeveloped. The other important result was that competitive intelligence was just as important for small companies as for large ones.

In the same year Sutton ([1988](#Sutton)) published his results on 308 surveyed firms. The most wanted information was found to be price, and the best source was the seller himself. Only 3% of companies had fully developed competitive intelligence systems.

Fann and Smeltzer ([1989](#Fann)) found contrary results to Fuld's. After forty-eight interviews with small firm executives, the paper concluded that the surveyed executives did not use competitive intelligence systems for either strategic or operative decisions.

Gelb _et al._ ([1991](#Gelb)) had a different approach to input information. The biggest problem was not information gathering itself, but the lack of existing technologies that support decision making as to the accuracy of information. Findings were based on twenty interviews with industrial firm executives.

Folsom ([1991](#Folsom)) focused on identifying sources of information. He concluded after 101 interviews with commercial company executives that information mostly comes from customers, suppliers and the other company owners.

We have to mention also Brockhoff ([1991](#Brockhoff)) who made eighty interviews with marketing and research and development directors in Germany. Focusing on technological scanning, the study found that all participants thought it important to know about technical development of competitors, but only 50% had a system to monitor competitors' developments.

In 1989 Prescott and Smith realised the largest ever survey on competitive intelligence up to that time. They sent a questionnaire to 172 companies, and all the questionned Competitive Intelligence Officers were members of the Society of Competititive Intelligence Professionals (SCIP). The sample, therefore, was not representative for all firms, competitive intelligence techniques were well over-represented, but the ninety-five returned questionnaires contained many interesting details. Focus, average length and budget of competitive intelligence projects were investigated, and the profile of the average project participant was addressed, with the analysis of its tasks and sources. The competitive intelligence systems were clearly underdeveloped in these companies, too, but they knew about the potential gains. The most serious problems included the lack of experts, insufficient involvement of executives and ethical/legal issues. In the conclusion, the following prerequisites were suggested for successful competitive intelligence:

*   'Institutionalise competitive analysis' (involvement of the whole company),
*   'CI should be project based',
*   'CI personnel should be result oriented',
*   'No research finding stands alone' (findings should be integrated into a knowledge bank, modifying or reinforcing what is already known),
*   'Recognise decision makers' _comfort zones_' (anticipate managers' expectations when results are presented. If findings do not meet expectations, managers tend to dismiss them.)  
    ([Prescott & Smith 1989: 13](#Prescott))

The paper of Subramanian and Ishak ([1998](#Subramanian)) was received by the journal in October 1995, so it does not mention the World Wide Web. However, the study, based on eighty-five returned questionnaires has, among many others, two relevant findings. First, it identifies the most important types of information in different sectors:

*   sales by products for pharmaceutical, consumer products and retailing companies;
*   strengths and weaknesses of competitors for banking, services, transportation and utilities;
*   new products and services in development for automotive, health care, computers and telecommunication, etc.

Secondly, they compared Return On Assets with the level of development of competitive intelligence and concluded, that 'high-performing firms exhibit more advanced competitor analysis systems than low-performing firms' [Subramanian & IsHak 1998: 19](#Subramanian).

### Recent studies

'Information systems have not adequately met the information requirement for strategic decisions in the past [in the late 1980s]' ([Pawar & Sharda 1997: 112](#Pawar)). But the Internet was very promising in terms of its multitude of sources, and also the amount of information and readiness (speed and free access). A number of papers described how the Internet would transform business intelligence in organizations, of which Huber ([1990](#Huber)) was among the first .

It is curious that recent field studies almost ignore the appearance of the World Wide Web. They develop mostly the same themes with mostly the same tools as earlier ones, focusing on special cases. Although Correia and Wilson's work ([1997](#Correia1)), who explored the Portuguese chemical industry, was too early to take the Web into account, it could have appeared by many others. For example, Ogunmokun ([1999](#Ogunmokun)) explored the relation between environmental scanning and performance in Australian exporting firms in 1999; O'Sawyerr ([2000](#OSawyerr)) studied information sources and their effectiveness in Nigeria; Ngamkroeckjoti and Johri ([2000](#Ngamkroeckjoti)) focused on environmental scanning of comapnies in Thailand; May in Russia ([2000](#May)); Bergeron's ([2000](#Bergeron)) theme was the information gathering of Canadian regions, while Raymond's ([2001](#Raymond)) attention was drown towards the technical monitoring of small industries.

The 'Eagles and Ostriches' study over three consecutive years ([Futures 1995, 1996, 1997](#Futures)) showed very little improvement in firms, although this was the critical period when the World Wide Web became a truly worldwide reality. While 17% of firms did not have any kind of scanning activity (Ostriches), those that had the most developed systems (Eagles) were the most satisfied.

McKenna's ([1996](#McKenna)) results show a clear scepticism. Most executives did not feel any reason to formalise environmental scanning activities. They considered that when information was needed, the owner of the information was to be asked directly. If he did not have the answer, it could not be that important.

Bournois and Romani examined environmental scanning in French companies. The study published in 2000 is one of the most exhaustive and valuable. With the help of the Institute of Higher Education in National Defense, some 5000 large companies were questionned, more than one fourth of whom answered. First, a definition of environmental ccanning (economic intelligence) was asked for, and a synthesised definition was constructed: 'an organized activity of the group responsible for strategy, which improves a firm's competitiveness by gathering, processing and internally diffusing information, in order to rule the environment' ([Bournois & Romani 2000](#Bournois): 19). Five approaches were distinguished: environmental (eco-logy), strategic (teleo-logy), stakeholders (psycho-socio-logy), tools (techno-logy) and networks (reti-logy).

Although most of the examined firms coped with world-wide competition and found environmental scanning important, they did not set it as strategic priority. They often lived business as a war, where the most common weapons are lobby, influencing, misinformation, but also engaging managers from business rivals, industrial espionage and computer attacks. The use of the Internet or databases was less than 8%, and their searches were mostly _ad-hoc_. Only one in twenty companies had intelligent systems.

Finally, studies on the quality of information available on the Internet and on retrieval techniques can be mentioned. Teo and Choo ([2001](#Teo)) for example concluded that the Internet had a positive impact on the quality of competitive intelligence information, which had positive impact on the organization. (Results were based on a sophisticated Confirmatory Factor Analysis model applied to a large sample of firms.) But these studies are on the edge of the direct scope of our work, as they concern more _ad hoc_ searches for information on a given theme. The focus of this paper is on the constant scanning for relevant information in the environment. In the model of Daft and Weick ([1984](#Daft1)) the first mode is called 'Searching', the second 'Conditioned viewing'. However, the technologies used are often the same in both cases.

## Method

To know how firms really use the Internet for environmental scanning, it was decided to lay down a set of new hypotheses and test them on the field.

### Telephone interview survey

For the test, an already existing database was used as sample, which was created by the [Observatoire E-management](http://www.observatoireemanagement.com), in order to test the impact of new information and communication technologies on different aspects of management (penetration rate of different technologies such as Enterprise Resource Planning or Customer Relationship Management, effects of the technologies on the control of employees, on the number of meetings, on information sharing, etc.)

The survey consisted of two questionnaires, one on the employees the other on the company. They were carried out in two consecutive years: 2001 and 2002\. Our research used the data obtained by the survey of companies using telephone interview schedules of five pages, administered by a leading market research company. Respondents were mostly managers, in more than 50% of the cases the Chief Executive Officer. The question categories were: organizational information (e.g., number of hierarchical levels, sales revision frequency), questions about information technology (Enterprise Resource Planning sytems, intranets), evaluation of practices (5-point Likert scales about e-learning, shared agendas, productivity, etc.), clustering information (sales, number of employees).

As almost the same interview schedule was used and the number of companies was similar, data from 2001 and 2002 could be merged for our study. Thus, the sample contained valid information on 514 companies in 2001 and 505 in 2002\. It concerned both public and private companies of more than 50 employees, 75% had between 50 and 500 employees, but more than 8% were over 10,000\. Two-thirds were present in countries other than France.

This database allowed us to test the development level of environmental scanning and its correlation with information and communication technology development and other organizational characteristics of companies.

### Limitations of the sample

The sample is large enough to be considered as representative of Western European countries, but it was collected mainly in France with some contirbutions from Great Britain, Germany, Spain and Portugal. Findings should apply to other mature economies, but it would be interesting to test our hypotheses on culturally different American or Japanese, fast growing Eastern European our partly state-owned Chinese companies.

The second limitation concerning the sample comes from the subject itself. In fact, competitive intelligence is a very sensitive domain. Companies having advantage in it, may not want to reveal this to competitors, and in turn, underdeveloped companies may not want to admit their lack. Answers in questionnaires should normally be considered as reliable in such a large sample, but it may be that in this special case, the sample would rather be a good field for research in 'non-cooperative games'.

On the longitudinal analysis, we must admit, that talking about tendencies and improvement may need more than two years of observation. We hope to have the opportunity to complete this research with results from further years.

Finally, the questionnaire contained only technical parameters to assess environmental scanning. The model could be fine-tuned with other aspects of environmental scanning and other concepts of information systems.

### Classification

Following Subramanian and IsHak ([1998](#Subramanian)), companies were first classified into three groups on the basis of the development level of their competitive intelligence systems. The questionnaires contained three questions about competitive intelligence:

*   For your environmental scanning activities do you use information available on websites of competitors?
*   For your environmental scanning activities do you use information available in external databases?
*   For your environmental scanning activities do you use information available in discussion forums?

A new variable was constructed: development level of the environmental scanning or competitive intelligence system. Its value is 3, meaning the most developed level, if the firm uses information in discussion groups, because this needs intelligent-agent-based tools ([Revelli 2000](#Revelli), [Benczar 2002](#Benczur)). Firms having systems of medium development level (value = 2) monitor competitors' Web pages and external databases ([Revelli 2000](#Revelli), [Bournois & Romani 2000](#Bournois)), the least developed ones, which do not use information in discussion groups, competitor Web-pages or external databases have a development value of 1\. This does not mean that these firms do not scan their environment, but their information gathering is based on personal reations, other informal channels and traditional sources (e.g., newspapers). We considered them as underdeveloped, because when informal channels are not (yet) available and especially when information gathering on competitors is problematic, the Internet is considered to be the most appropriate tool ([Erdelez & Ware 2001](#Erdelez)).

This new variable was the dependent variable in most of our analyses. Our first hypothesis is about its frequency.

From Fuld's writing ([1985](#Fuld1)) to Bournois and Romani's study ([2000](#Bournois)), all articles found that most firms are underdeveloped in environmental scanning or competitive intelligence.

> Hypothesis 1: Most firms belong to the least developed group.

### Longitudinal improvement

The next question is, 'Is there any significant change between 2001 and 2002?' The Futures Group ([1995, 1996, 1997](#Futures)) performed a longitudinal study in three succeeding years and found no significant change. But if we consider that Bournois and Romani ([2000](#Bournois)) found 8% of firms searching in databases and only 5% using intelligent tools, compared to frequencies in our sample, there seems to be some overall improvement. Can we track it between 2001 and 2002?

The first independent variable to be examined was the year of study.

> Hypothesis 2: There is a significant change in the number of firms in each of our three groups between 2001 and 2002.

### Environmental effects

Cunlan ([1983](#Cunlan)), Sormunen and Daft ([1988](#Daft2)), Correia and Wilson ([1997](#Correia1)) reported positive association between environmental uncertainty and the intensity of environmental scanning activities. It seems doubtless, that a more turbulent business climate would lead to more developed environmental scanning techniques. Is this turbulence measurable?

The questionnaire contained the following items:

*   In your main activity sector, intensity of competition is low.  
    (5 points Likert type scale: Disagree -> Totally agree)
*   In your company you must reconsider your sales forecasts every ...?  
    (month, 3 months, 6 months, 9 months, 1 year)

The first test was whether, if the feeling of the intensity of competition was higher in a given sector, this had any influence on management.

> Hypothesis 3a: Companies facing more competition in their sector reconsider sales forecasts more often.

Then the link between sales revision frequency and development level of environmental scanning was explored.

> Hypothesis 3b: Companies reconsidering more often their sales expectations have more developed environmental scanning systems.

Finally, we looked for the direct effect of competition in the sector and the development level of environmental scanning.

> Hypothesis 3c: Companies facing more competition in their sector have more developed environmental scanning systems.

### Internal Aspects

Of internal, organizational aspects, size is one of the most important. According to Fuld ([1988](#Fuld2)) competitive intelligence is just as important for small firms as it is for large ones. Others, such as Bournois and Romani ([2000](#Bournois)) found that bigger companies were more developed in competitive intelligence. The second result was felt to be more plausible, as environmental scanning needs extra investment in technologies and human resources.

There are several ways of defining the size of a company, we compared development of environmental scanningS to the number of hierarchical levels, the number of employees and sales results.

*   In your company, how many hierarchical levels are there?  
    (2, 3, 4, 5, 6 or more than 6)
*   How many persons are employed in your company?  
    (less than 50, 50 - 500, 500 - 5000, 5000 - 10 000, more than 10 000)
*   How much are your sales?  
    (less than €7.6 million (FF50 million), €7.6 million - €76 million, €76 million - €150 million, €150 million - €1.5 billion, more than €1.5 billion  
    The 2001 question used French francs, but for 2002 limits were recalculated (€1 = FF6.56), so answers could be merged.

> Hypothesis 4a: The more hierarchical levels are there in a company, the higher will be the development level of its environmental scanning system.

The F-test for hierarchical levels shows a signification of 0.238, which is far above the limit of 0,05\. Threfore, H4a is rejected.

> Hypothesis 4b: The more employees the company has, the higher will be the development level of its environmental scanning system.

Nor is the number of employees significant, with a significance value of 0,180.

> Hypothesis 4c: The bigger the company's sales are, the higher will be the development level of its environmental scanning system.

The sales results are significantly correlated with the development of environmental scanning (F-test, Sig. 0.00). This is the only aspect of size seeming to influence scanning practices.

The other internal indicator we analyzed was the orientation of organizational structure.

*   The organization of your company is rather oriented towards ... (functions, market, country or projects?)

Traditional organizations are structured by positions, so they are rather turned inwards. We supposed that those firms that are oriented towards market, country or project are more turned outwards, and therefor they should be more developed in environmental scanning. This corresponds to the terms of outwardness ([Correia & Wilson 2001](#Correia2)), project organization ([Prescott & Smith 1989](#Prescott)) and 'adhocratic nature' ([Gilad 2001](#Gilad)) in the literature.

> Hypothesis 5: Companies whose organizational structure is market, project or country oriented have more developed environmental scanning systems than those being post-oriented.

According to Figure 2, representing frequencies, it seems that this hypothesis is supported, and the Chi-squared test confirms this (Sig. 0.003). Comparing means of environmental scanning development in these two types of firms with an F-test, the result is also significant (Sig. 0.012). H5 can be retained.

### Information technology development

Competitive intelligence can be considered as an important component of Knowledge Management ([Carvalho & Ferreira 2001](#Carvalho)). We suggest that having developed environmental scanning systems is correlated with the use of other information management tools. Carvalho and Ferreira classified what they term 'KM software' into ten categories: Intranet-based systems, electronic document management, groupware, workflow, artificial-intelligence-based systems, business intelligence, knowledge map systems, innovation support tools, competitive intelligence tools, and knowledge portals. In our survey we were able to test workflow (Enterprise Resource Planning Systems), business intelligence (Management Information Systems) and Intranets, including groupware and knowledge portals. First, we analysed correlation with having Enterprise Resource Planning Systems.

*   Does your company use Enterprise Resource Planning systems (ERP)?

> Hypothesis 6a: Companies having Enterprise Resource Planning (ERP) Systems have more developed environmental scanning systems.

*   [If your company uses Enterprise Resource Planning Systems (ERP)] does it have strategic Management Information Systems?

> Hypothesis 6b: Companies having strategic Management Information Systems (MIS) have more developed environmental scanning systems.

The questionnaire had two more items about MIS:

*   To constitute MIS reports, does the information system automatically consolidate sales data?
*   To constitute MIS reports, does the information system automatically consolidate production, procurement, quality, and marketing data?

The answers to these two questions were merged, to get a unique variable for automatic data consolidation. The new variable is a logical OR of the two items, and was used as an indicator for sophisticated MIS.

> Hypothesis 6c: Those companies whose Management Information System (MIS) uses automatically consolidated data coming from other internal information systems, have more developed environmental scanning systems.

After Enterprise Resource Planning Systems, which means combining and transfering internal data and Management Information Systems, which means collecting and analysing such data, the next point to be examinated was the Intranet which is mainly used for information share and dissemination. The questionnaire was asking about several intranet functionalities: groupware tools, general data on company, shared calendar, search engines, profession-specific data, e-learning, and knowledge bases.

*   Does your intranet have... features?

A new variable was created in order to have an indicator of global development of intranets: the number of its features.

> Hypothesis 6d: The more features the Intranet has, the more developed the company's environmental scanning system will be.

One of the most important hypotheses was about Internet access. Environmental scanning can be made directly by employees too. Correia and Wilson ([2001](#Correia2)) proposed that the more open the organization was to its environment, the more individuals in the organization would experience exposure to relevant information. It was felt obvious that Internet access is correlated with the development level of the environmental scanning system.

*   Does your intranet allow access to external Internet sites?

> Hypothesis 6e: Where employees have access to the Internet, environmental scanning systems are more developed.

### Environmental scanning in different sectors

Finally, whether the sector of activity has any influence or not on environmental scanning was tested. We wanted to see especially if turbulent and high-tech sectors are more developed. The questionnaire distinguished the following categories:

*   Construction
*   Manufacturing and other industries
*   Commerce
*   Hotels and restaurants
*   Transportation services
*   Information technology, telecommunications
*   Finance industry
*   Services to businesses
*   Personal services
*   Other

Hypothesis 7: Environmental scanning systems are more developed in the IT/Telecom sectors than in the others.

## Results

H1 is supported. 58% of firms belong to the least developed group, and less than 7% to the most developed. (See Figure 1.) It is as Fuld ([1985](#Fuld1), [1988](#Fuld2)), Bournois and Romani ([2000](#Bournois)) and the others have observed.

<div align="center">![p241fig1](p241fig1.jpg)</div>

<div align="center">  
**Figure no 1\. Frequency analysis of firms classed by development level of environmental scanning**</div>

To test if the difference between 2001 and 2002 in the level of ES-development is significant, a Chi-squared analysis was used on the frequencies of companies in each group. The significance measure was found to be far above the limit of 0.05 (0.258). An F-test was also performed to see if average development shows significant evolution, but once again, significance was low (0.594). H2 had to be rejected, as no significant improvement is found. The Futures Group ([1995, 1996, 1997](#Futures)) reached the same conclusion.

The F-test showed significant correlation between competition and reconsidering sales expectations, so H3a could be retained. (Sig. 0.000)

H3b, regarding, reconsidering sales expectations more often and development level of environmental scanning is also retained (F-test, Sig. 0.020). Bournois and Romani ([2000](#Bournois)) had the same observation.

There is also a positive correlation between environmental uncertainty and the development level of environmental scanning systems (see Table 1). H3c was also supported.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: F-test on intensity of competition and development level of environmental scanning**</caption>

<tbody>

<tr>

<th></th>

<th>Sum of squares</th>

<th>df</th>

<th>Mean square</th>

<th>F</th>

<th>Sig.</th>

</tr>

<tr>

<th>Between Groups</th>

<td>4,700</td>

<td>4</td>

<td>1,175</td>

<td>3,061</td>

<td>0,016</td>

</tr>

<tr>

<th>Within Groups</th>

<td>385,720</td>

<td>1005</td>

<td>0,384</td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>Total</th>

<td>390,420</td>

<td>1009</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

Among internal aspects, the F-test for the effects of hierarchical levels shows a signification of 0.238, which is far above the limit of 0.05\. H4a, therefore, is rejected.

Nor is the number of employees significant, with a significance measure of 0.180: this rejects H4b.

However, sales results are significantly correlated with the development of ESS (F-test, Sig. 0.00), this is the only aspect of size that seems to influence scanning practices. Consequently H4c is retained. Is this due to better understanding or is it because environmental scanning is expensive? One thing is sure, higher sales do not necessarily mean a larger organization, so Fuld ([1988](#Fuld2)) seems to be correct on that point.

H5 stated that companies whose organizational structure is market, project or country oriented have more developed environmental scanning systems than those being position-oriented. According to Figure 2, representing frequencies, this hypothesis is supported, and the Chi-Squared test comfirms this (Sig. 0.003). When we compared means of environmental scanning development in these two types of firms with an F-test, the result was also significant as well (Sig. 0.012), consequently, H5 can be retained. Prescott and Smith ([1989](#Prescott)) had emphasised that project organization was more supportive of competitive intelligence.

<div align="center">![figure2](p241fig2.jpg)</div>

<div align="center">  
**Figure 2\. Frequencies of position- and market-, project- or country-oriented firms in each group of environmental scanning development**</div>

Our F-test does not confirm H6a about a correlation with Enterprise Resource Planning, but significance is just above the limit (Sig. 0.057) and, therefore, it is worth continuing investigations.

H6b had to be rejected (Sig. 0.871). Having a management information system in general does is not associated with more developed environmental scanning.

H6c stated, that if the management information system uses automatically consolidated data from other internal information systems, this means more developed environmental scanning systems. The F-test showed a significance of 0.000, so H6c is supported. Why are only high-technology internal information systems correlated with advanced external information gathering? An answer will be proposed to this question later.

H6d explored the number of intranet features. As proposed features were not the same in 2001 and 2002, this test had to be made separately for the two years. The F-test shows significance of 0.001 for 2001 and 0.000 for 2002, so H6d is supported.

For the last question about information technology, an F-test was used to test the effect of the Internet. It showed significant correlation (Sig. 0.038), so H6e is supported. Correia and Wilson ([2001](#Correia2)) predicted positive correlation between environmental scanning and exposure to information.

Finally the different sectors of activity were compared. As shown in Figure 3, the average development in the information technology sector is well above the others except the _other_ category. The F-test reports that differences are significant (Sig. 0.000), so H7 is supported.

<div align="center">![figure3](p241fig3.jpg)</div>

<div align="center">  
**Figure 3\. Average development of environmental scanning systems by sectors**</div>

A final test was performed to verify if the intensity of competition was indeed the strongest in the information technology sector. It was found that the most competitive sectors were Commerce, Manufacturing industry and Information technology and telecommunications, while de least competitive one was Services to Customers. (F-test, Sig. 0.000)

Table 2\. Shows a summary of hypotheses, methods used and findings.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Summary of Results**</caption>

<tbody>

<tr>

<th>Hypothesis</th>

<th>Aspect of company tested</th>

<th>Test</th>

<th>Result</th>

</tr>

<tr>

<td>H1</td>

<td>Low ESS development in general</td>

<td>Frequency</td>

<td>Retained</td>

</tr>

<tr>

<td>H2</td>

<td>Improvment 2001 -> 2002</td>

<td align="center">χ<sup style="font-size: x-small;">2</sup></td>

<td>Rejected</td>

</tr>

<tr>

<td>H3a</td>

<td>More competition -> more reactivity</td>

<td>F-test</td>

<td>Retained</td>

</tr>

<tr>

<td>H3b</td>

<td>More reactivity -> higher ESS dev.</td>

<td>F-test</td>

<td>Retained</td>

</tr>

<tr>

<td>H3c</td>

<td>More competition -> higher ESS dev.</td>

<td>F-test</td>

<td>Retained</td>

</tr>

<tr>

<td>H4a</td>

<td>More hierarchy levels -> higher ESS dev.</td>

<td>F-test</td>

<td>Rejected</td>

</tr>

<tr>

<td>H4b</td>

<td>More employees -> higher ESS dev.</td>

<td>F-test</td>

<td>Rejected</td>

</tr>

<tr>

<td>H4c</td>

<td>Higher sales -> higher ESS dev.</td>

<td>F-test</td>

<td>Retained</td>

</tr>

<tr>

<td>H5</td>

<td>market/project/country orientation -> higher ESS dev.</td>

<td align="center">χ<sup style="font-size: x-small;">2</sup></td>

<td>Retained</td>

</tr>

<tr>

<td>H6a</td>

<td>Having ERP -> higher ESS dev.</td>

<td>F-test</td>

<td>Rejected</td>

</tr>

<tr>

<td>H6b</td>

<td>Having MIS -> higher ESS dev.</td>

<td>F-test</td>

<td>Rejected</td>

</tr>

<tr>

<td>H6c</td>

<td>MIS with direct input from other IS -> higher ESS dev.</td>

<td>F-test</td>

<td>Retained</td>

</tr>

<tr>

<td>H6d</td>

<td>More Intranet features -> higher ESS dev.</td>

<td>F-test</td>

<td>Retained</td>

</tr>

<tr>

<td>H6e</td>

<td>Internet access -> higher ESS dev.</td>

<td>F-test</td>

<td>Retained</td>

</tr>

<tr>

<td>H7</td>

<td>ICT sector -> highest ESS dev.</td>

<td>F-test</td>

<td>Retained</td>

</tr>

</tbody>

</table>

## Discussion

It seem now clear that companies are still underdeveloped, that no improvement can be seen between 2001 and 2002, that unstable environment, higher sales, project, market or country oriented organization and Internet access are correlated with more developed environmental scanning systems, but some questions still remain.

Information technology development especially for other information management tools seems to be particularly relevant. ERP systems directly providing input data for management information systems, or many features in intranets mean usually higher development level of environmental scanning. This is interesting compared to the results of the last test: firms in the Information technology and telecommunications sector are the most developed for environmental scanning. Could they be more developed in these systems than the average simply because they are developing and selling them?

And why are most firms still underdeveloped, large as well as small?

Gilad ([2001](#Gilad)) suggests an answer, saying that formalised competitive intelligence is an oxymoron. The larger a company gets, the more it tends to formalism and bureaucracy. But competitive intelligence needs creativity and the older a competitive intelligence group becomes, the less effective it will be. Some elements of it must always stay _ad hoc_ and depend on the personality of the manager. Besson and Possin ([1996](#Besson)) on the same issue, suggest that the key person in environmental scanning can be only the Chief Executive Officer, because he centralises all the information. Mintzberg ([1989](#Mintzberg1)) reached the same conclusion: in fact management is not a science, because decision circles stay mostly inside the brains of managers, and a decision often looks like 'intuition' or 'judgement' from outside.

But the way managers think does not explain why are there so few competitive intelligence or environmental scanning systems. Alter ([2000](#Alter)) has an original answer for this: innovation is the process of adopting an invention, a technology or a relationship. Decisions themselves are just inventions, and need other participants who find sense in them, to be adopted. For environmental scanning systems, we may lack such participants. They could be IT managers or Information Officers, but we all know that they have been very busy in the last eight to ten years with Enterprise Resource Planning systems, Customer Relationship Management, the Year 2000 millenium issue, security and other projects.

These theories provide possible explication of questions raised by this paper. For example, surfing on the Internet does not necessarily mean focused attention on company-related strategic information, but if it were so, it would not necessarily be a formalised scanning activity by dedicated group. We would still call this environmental scanning, but _ad hoc_ scanning, as opposed to the formally structured scanning we focused on. (Morrison _et al._ [1984](#Morrison)) use the terms _passive_ and _active_.) Or it can be that formalised scanning on the Internet did not become a common activity in firms, because it takes longer for the ordinary innovation takes place. And one step further: perhaps Gilad ([2001](#Gilad)) and McKenna ([1996](#McKenna)) were right saying that competitive intelligence cannot be entirely formalised, and, therefore, cannot be entirely described by organizational sciences, but rather require a cognitive science approach? Then the findings of this paper would be just the small visible part of something else that Mintzberg called decision circles that stay inside managers' brains.

## Limitations

The first limitations concerning the sample have already been mentioned. These are geographical limitations, the sensitivity of the subject, longitudinal aspects evaluated only over two years, and the focusing on technical parameters in assessing environmental scanning.

Another limitation applies to the possible causes of the underdevelopment of companies in environmental scanning. These are merely theories and need further research to be confirmed.

## Conclusions

Two questions were the focus of the present research: how developed was information acquisition in firms and why were there so few studies about it in the academic literature?

The results of our field study reinforced certain hypotheses on environmental scanning and competitive intelligence: firms are still underdeveloped, but higher sales, better information technology development, environmental uncertainty and an organization turned outwards were correlated with a higher level of development.

Other hypotheses could not be retained. There was no significant improvement between 2001 and 2002 and size of firms measured in number of hierarchical levels or number of employees is not correlated with development of environmental scanning systems.

And why are there so few empirical studies on environmental scanning? Probably because studies like this cover formal scanning activities only. The consciousness of employees to informal, _ad hoc_ practices remains unknown and the question whether it is pertinent to formalise these activities unanswered. After all, we all scan our environment, we did it before the Internet and computers and maybe long before Adam Smith. If the Internet is only an additional tool, and it is used for _ad hoc_ scans, there is no revolution in environmental scanning practices.

## Acknowledgements

I would like to thank Henri Isaac associate professor at the Paris Dauphine University for his useful comments and his help in getting access to the database of the Observatoire e-management, as well as Làszlò Z. Karvalics associate professor and Ferenc Kiss senior lecturer at the Budapest University of Technology and Economics for their suggestions and full support for my work. Finally I wish to thank Zsolt Pàndi from the Budapest University of Technology and Economics, for his preliminary review of the manuscript, as well as the anonymous referees for their useful suggestions..

## References

*   <a name="Aguilar" id="Aguilar"></a>Aguilar, F. J. (1967). _Scanning the business environment._ New York, NY: McMillan.
*   <a name="Alter" id="Alter"></a>Alter, N. (2000). _L'innovation ordinaire._ Paris: Presses Universitaires de France
*   <a name="Benczur" id="Benczur"></a>Benczúr, D. (2002). Internet és üzleti hírszerzés. [Internet and competitive intelligence.] In F. Kiss, (Ed.), _Sokszínû e-világ. [Colourful e-world]._ (pp. 49-60). Budapest: BME - ITM.
*   <a name="Bergeron" id="Bergeron"></a>Bergeron, P. (2000). Regional business intelligence: the view from Canada. _Journal of Information Science_, **26**(3), 153-160.
*   <a name="Besson" id="Besson"></a>Besson, B. & Possin, J-C. (1996). _Du renseignement à l'intelligence économique._ Paris: Dunod
*   <a name="Bournois" id="Bournois"></a>Bournois, F. & Romani, J-P. (2000). _L'intelligence économique et stratégique dans les entreprises françaises._ Paris: Economica
*   <a name="Brockhoff" id="Brockhoff"></a>Brockhoff, K. (1991). Competitor technology intelligence in German companies, _Industrial Marketing Management_, **20**(2), 265-280.
*   <a name="Carvalho" id="Carvalho"></a>Carvalho, R.B. & Ferreira, M.A.T. (2001). [Using information technology to support knowledge conversion processes.](http://InformationR.net/ir/7-1/paper118.html) _Information Research,_ **7**(1), paper 118 Retrieved 15 March, 2005 from http://InformationR.net/ir/7-1/paper118.html
*   <a name="Choo" id="Choo"></a>Choo C.W. (2001). [Environmental Scanning as information seeking and organizational learning.](http://InformationR.net/ir/7-1/paper112.html) _Information Research._ **7**(1), paper 112\. Retrieved 15 March, 2005 from http://InformationR.net/ir/7-1/paper112.html
*   <a name="Correia1" id="Correia1"></a>Correia, Z. & Wilson, T.D. (1997). [Scanning the business environment for information: a grounded theory approach.](http://InformationR.net/ir/2-4/paper21.html) _Information Research._ 2(4) Retrieved 15 March, 2005 from http://InformationR.net/ir/2-4/paper21.html
*   <a name="Correia2" id="Correia2"></a>Correia, Z. & Wilson, T.D. (2001). [Factors influencing environmental scanning in the organizational concept.](http://InformationR.net/ir/7-1/paper121.html) _Information Research_, **7**(1), paper 121\. Retrieved 15 March, 2005 from http://InformationR.net/ir/7-1/paper121.html
*   <a name="Cunlan" id="Cunlan"></a>Cunlan, M.J. (1983). Environmental scanning: the effects of task complexity and source accessibility on information gathering behavior. _Decision Science_, **14**(2), 194-205\.
*   <a name="Daft1" id="Daft1"></a>Daft, R.L. & Weick, K.E. (1984). Toward a model of organizations as interpretation systems. _Academy of Management Review_, **9**(2), 284-295
*   <a name="Daft2" id="Daft2"></a>Daft, R.L., Sormunen, J. & Parks D. (1988). Chief executive scanning, environmental characteristics and company performance: an empirical study. _Strategic Management Journal_, **9**(2), 123-139.
*   <a name="Erdelez" id="Erdelez"></a>Erdelez, S. & Ware, N. (2001). [Finding competitive intelligence on Internet start-up companies: a study of secondary resource use and information-seeking processes.](http://InformationR.net/ir/7-1/paper115.html) _Information Research_, **7**(1), paper 115\. Retrieved 15 March, 2005 from http://InformationR.net/ir/7-1/paper115.html
*   <a name="Ettore" id="Ettore"></a>Ettore, B. (1995). Managing competitive intelligence. _Management Review_, **20**(4), 15-19\.
*   <a name="Fann" id="Fann"></a>Fann, G. L. & Smeltzer, L.R. (1998). The use of information from and about the competitors in small business management. _Entrepreneurship Theory and Practice_, **13**(4), 35-46.
*   <a name="Folsom" id="Folsom"></a>Folsom, D. (1991). Market intelligence in small businesses. _Marketing Intelligence and Planning_, **9**(2), 16-19.
*   <a name="Fuld1" id="Fuld1"></a>Fuld, L.M. (1985). _Competitor intelligence: how to get it, how to use it._ New York, NY: John Wiley and Sons
*   <a name="Fuld2" id="Fuld2"></a>Fuld, L.M. (1988). _Monitoring the competition: find out what's really going on over there_. New York, NY: John Wiley and Sons
*   <a name="Futures" id="Futures"></a>Futures Group. (1995, 1996, 1997). _Ostriches and eagles._ Retrieved 30 June, 2003 from http://www.tfg.com/pubs/docs. [These documents have now been removed from the Futures Group site and are no longer available.]
*   <a name="Gelb" id="Gelb"></a>Gelb, B.D., Saxton M.J., Zinkhan, G.M. & Albers, N.D. (1991). Competitive intelligence, insight from executives. _Business Horizons,_ **34**(1), 43-47.
*   <a name="Gilad" id="Gilad"></a>Gilad, B. (2001). [_An ad hoc, entrepreneurial competitive intelligence model or: Have we succeeded? Are we happy?_](http://www.academyci.com/ResourceCenter/adhoc.doc) Academy of Competitive Intelligence Website. Retrieved 15 March, 2005 from http://www.academyci.com/ResourceCenter/adhoc.doc
*   <a name="Hermel" id="Hermel"></a>Hermel, L. (2001). _Maítriser et pratiquer la veille strategique._ Paris: Afnor
*   <a name="Herring" id="Herring"></a>Herring, P. (1988). Building a business intelligence system. _The Journal of Business Strategy_, **9**(3), 4-9.
*   <a name="Huber" id="Huber"></a>Huber, G.P. (1990). A theory of the effects of advanced information technologies on organizational design, intelligence and decision making. _Academy of Management Review_, **15**(1), 47-71\.
*   <a name="May" id="May"></a>May, C.R. (2000). Environmental scanning behavior in a transitional economy: Evidence from Russia. _Academy of Management Journal_, **43**(3), 403-428.
*   <a name="McGonagle" id="McGonagle"></a>McGonagle, J.J. & Vella, C.M. (1998). _Protecting your company against competitive intelligence._ Westport, CT: Quorum Books.
*   <a name="McKenna" id="McKenna"></a>McKenna, S.D. (1996). The darker side of the entrepreneur. _Leadership and organizational Development Journal_, **17**(6), p. 41.
*   <a name="Mintzberg1" id="Mintzberg1"></a>Mintzberg, H. (1989). _Mintzberg on management. Inside our strange world of organizations._ New York, NY: The Free Press.
*   <a name="Mintzberg2" id="Mintzberg2"></a>Mintzberg, H., Ahlstrand B. & Lampel J. (1998). _Strategy safari: a guided tour through the wilds of strategic management._ New York, NY: The Free Press.
*   <a name="Morrison" id="Morrison"></a>Morrison, J.L., Renfro, W.L. & Boucher, W.I. (1984). [_Futures research and the strategic planning process: implications for higher education._](http://horizon.unc.edu/projects/seminars/futuresresearch) Chapel Hill, NC: Horizon, University of North Carolina. (ASHE-ERIC Higher Education Report No. 9) Retrieved 15 March, 2005 from http://horizon.unc.edu/projects/seminars/futuresresearch
*   <a name="Ngamkroeckjoti" id="Ngamkroeckjoti"></a>Ngamkroeckjoti, Ch. & Johri, L.M. (2000). Management of environmental scanning processes in large companies in Thailand. _Business Process Management Journal_, **6**(4), p. 331.
*   <a name="Ogunmokun" id="Ogunmokun"></a>Ogunmokun, G.O. (1999). Environmental scanning practices and export performance in international marketing: a study of Australian exporters. _International Journal of Management_, **16**(1), 9-12.
*   <a name="OSawyerr" id="OSawyerr"></a>O'Sawyerr, O. (2000). Executive environmental scanning, information source utilisation, and firm performance: the case of Nigeria. _Journal of Applied Management Studies_, **9**(1). 95-116.
*   <a name="Pawar" id="Pawar"></a>Pawar, S.P. & Sharda, R. (1997). Obtaining business intelligence on the Internet. _Long Range Planning_, **30**(1), 110-121.
*   <a name="Prescott" id="Prescott"></a>Prescott J.E. & Smith D.C. (1989). The largest survey of 'leading edge' competitor intelligence managers. _Long Range Planning Review_, **17**(8), p.6.
*   <a name="Raymond" id="Raymond"></a>Raymond, L. (2001). Technological scanning by small Canadian manufacturers. _Journal of Small Business Management_, **39**(2), 123-139.
*   <a name="Revelli" id="Revelli"></a>Revelli, C. (2000). _Intelligence stratégique sur Internet._ Paris: Dunod.
*   <a name="Russel" id="Russel"></a>Russel, S. & Prince, M.J. (1992). Environmental scanning for social services. _Long Range Planning_, **25**(5), 106-113.
*   <a name="Subramanian" id="Subramanian"></a>Subramanian R. & Ishak S. T. (1998). Competitor analyses for US companies: an empirical investigation. _Management International Review._ **38**(17), 7-23.
*   <a name="Sutton" id="Sutton"></a>Sutton, H. (1998). _Competitive intelligence._ New York, NY: The Conference Board. (Research report no. 913).
*   <a name="Teo" id="Teo"></a>Teo, T.S.H. & Choo, W.Y. (2001) Assessing the impact of using the Internet for competitive intelligence. _Information and Management_, **39**(1), 67-83.
*   <a name="ZKarvalics" id="ZKarvalics"></a>Z. Karvalics, L. (2001). Quo vadis: input vállalati adatok. [Quo vadis: corporate input information.] In F. Kiss (Ed.), _Új és megújuló információs rendszerek. [New and renewing information systems]._ (pp. 76-83). Budapest: BME-ITM.


