#### Vol. 11 No. 1, October, 2005

# The development of children's Web searching skills - a non-linear model

#### [Ann Britt Enochsson](mailto:annbritt.enochsson@tii.se)  
Department for Educational Sciences, Karlstad University, and  
The Interactive Institute, Stockholm, Sweden



#### Abstract

> **Introduction.** The aim of this article is to determine the various skills necessary for seeking information on the Internet in educational settings. Throughout the article there is also an aim to present the students' perspective on possibilities and difficulties when using the Internet. 
> **Method.** The approach is ethnographic, which requires various data collection methods. In total 110 students in four different settings have participated.  
> **Analysis.** The analyses were partly made with the help of the software NUD*IST for qualitative analyses, where sentences both from interviews and field notes were coded. Some analyses were of qualitative nature and based on selected material from the coded texts. Others were strictly quantitative and compared data from coded qualitative material with questionnaires and computer logs in a database sheet. In ethnographic analyses the material is read several times and compared in different ways to see what themes will emerge. In this case the respondents have also commented upon the result.
> **Results.** The students regard six different skills as fundamental: language, knowledge about the technology, knowledge about different ways of information seeking, how search engines work, setting goals and being critical.
 
 

## Introduction

For a long time Swedish curricula have emphasized three basic skills: reading, writing and arithmetic. Calling them _basic skills_ means that they should be given first priority in teaching. Since the advent of the Internet there has been a discussion whether or not a fourth basic skill should be added. This would be the ability to handle the great flow of information in our society. Rask ([1999](#ras)) introduced the concept of _the fourth basic skill_ into the Swedish school system, and the research term used would be _information literacy_. The concept of _the fourth basic skill_ was easily accepted by Swedish teachers, but the road from accepting it to applying it seems to be long. Many teachers do not know how to teach the information-seeking skills associated with the Internet. Since there is a widespread myth that young people know everything about computers, some teachers just leave their students alone at the computers. Another solution often seen with regards to younger students is not letting them use the Internet at all. The problem many teachers mention is that they do not know where to start or what to do. One reason for this is that the Internet contains a lot of material we would prefer not to encounter.

The aim of this article is to discern the different skills necessary for seeking information on the Internet in educational settings in order to make it easier to develop the didactics of information-seeking on the Internet. Throughout the article there is an aim to give the students' perspective on using the Internet, and to show what they have found useful and difficult when working with information-seeking on the Internet.

### Related research

#### Different skills

As long ago as the early 1990s, Eisenberg and Berkowitz ([1990](#eis)) developed a widely-used approach to teaching information and technology skills which they called _Big6_. Like _the fourth basic skill_ above, Eisenberg and Berkowitz call information literacy _an essential life skill_. The six steps in this approach are (1) task definition, (2) information-seeking strategies, (3) location and access, (4) use of information, (5) synthesis, and (6) evaluation. Each step also contains two sub-steps generally referred to as _the little twelve_. This is a linear model where the development starts at one step and continues to the next. Bilal ([2002a](#bila)) suggests that the affective aspect, which Kuhlthau ([1993](#kuh)) found to be important in the information-seeking process, should be incorporated into this model.

Related to this affective aspect is the research that Gross ([2001](#gro)) conducted on what she calls imposed information-seeking. She found that many young children's research projects at school were assigned to the students by the teacher, rather than being their own.

#### More training and experience

Research has often found that children do not always succeed very well when seeking information on the Internet. It is claimed that there is a need for teaching and training for information literacy, since children often lack experience (Bilal [2002b](#bilb); Bilal & Kirby [2002](#bilc); Enochsson [2001b](#enob), [2003](#enod); Hultgren & Limberg [2003](#hul); Schacter _et al._ [1998](#sch); Slone [2003](#slob); Watson [2001](#wat)). Such training would include a variety of skills such as Web experience and regular access, structuring problems, understanding the process, and so on.

Slone ([2002](#sloa)) found that there were similarities between inexperienced searchers of all ages. This was also noted by Bilal and Kirby ([2002](#bilc)). Lack of experience often means lack of models of how the Internet works, and leads to less successful search results regardless of the age of the person seeking information. The children in Enochsson's study ([2001b](#enob)) also referred to their own lack of experience as a reason why they did not know how to do everything.

#### Formulating questions

Limberg and Alexandersson ([2003](#lim)) have seen that their informants, 11-year-olds, adopt a fact-finding approach, which they found to be related to the character of the assignments. They claim that the students should be able to formulate relevant _researchable_ questions, which generate a variety of explanations on which the students could be obliged to form an opinion. The fact-finding approach is indirectly supported by the teachers, who are not always aware of the difference between questions that lead to fact-finding and those that are researchable (Enochsson [2002](#enoc)).

#### The teacher's role

The teacher plays a central role in being a fellow creator who guides the students so that they can develop and become participants in the learning environments of which school is constituted. Bilal ([2002b](#bilb)) also makes this comment.

### Research questions

This paper is a response to Dresang ([1999](#dre)) in that research draws upon young person's own perspective and the data collection situations of information seeking were at least informal, in the sense of not having been constructed for the benefit of the researcher. Their naturalistic character is open to question, however.

As mentioned above, the aim of this study is to discern the skills necessary for seeking information on the Internet in educational settings and to give voice to the students' perspective on using the Internet. The following questions are asked:

*   What skills do the students claim are necessary to carry out a successful information-seeking process?
*   How can these skills be related to each other and to teaching?

### Theoretical framework

The data are analyzed within a socio-cultural perspective, which claims that knowledge is constructed and developed through communication with the environment, including people (e.g., Vygotsky [1986](#vyga)). This perspective also views children's learning as similar to that of adults, though children's more limited experience naturally plays a role (Vygotsky [1998](#vygb)). What a person learns is dependent on the environment in a wide sense. That is, the context is not only school or home, but also the form and content of teaching.

The study takes a users' perspective, and the users in this study are the students. The teachers' perspectives have been used to understand the situation in the classroom better and to triangulate the data. Gender and childhood are seen as cultural constructions and the participating students, regardless of age, are treated as participating subjects and co-constructors of data (James & Prout [1997](#jam)).

## Methods

The data on which this article is based were collected from 1998 to 2003 in four different schools, with students from pre-school class to ninth grade in the Swedish compulsory school participating. In total 110 students were observed and interviewed, as shown in Table 1.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: The participants in the study**</caption>

<tbody>

<tr>

<th>Settings</th>

<th>Participants</th>

<th>Age of students</th>

<th>Year</th>

</tr>

<tr>

<td>School A</td>

<td>51 students</td>

<td>6-10</td>

<td>2002</td>

</tr>

<tr>

<td>School B</td>

<td>34 students</td>

<td>9-11</td>

<td>1998-1999</td>

</tr>

<tr>

<td>School C</td>

<td>11 students</td>

<td>13-14</td>

<td>1999</td>

</tr>

<tr>

<td>School D</td>

<td>14 students</td>

<td>15-17</td>

<td>2003</td>

</tr>

<tr>

<td>Total:</td>

<td>110 students</td>

<td>6-17</td>

<td>1998-2003</td>

</tr>

</tbody>

</table>

The schools were chosen because of the teachers' intentions to use computers as a natural tool in the classroom. Their teaching was inquiry-based, and they all tried as much as possible to use the students' questions as starting points for their teaching, which led to a natural need for information seeking in the classroom. All the teachers believed that it was possible for their students to acquire information-seeking skills, even if to varying degrees. The aim of the study was to understand what students do when they search for information on the Internet at school and how they reflect on this when they are given the opportunity to work with it for a long period and with good guidance.

The students' access to computers varied from a few computers shared by several classes to computer labs where each student could sit at their own computer.

The approach was ethnographic, which requires various data collection methods, such as observation, interviews, conversations, questionnaires and examining written documents and computer logs. At one of the schools there was also a reading-comprehension test. Different methods were used for triangulation of data (Hammersley & Atkinson [1995](#ham)), which is a way of validating; one method's strength can compensate for another method's weakness (Merriam [1988](#mer)). What a student did in the classroom was compared with what he or she said in conversation, and also with the teacher's assessment of the student's ability. All the students and teachers read and commented on their own interviews. The teachers also read and commented on the results, and at school B the students also commented on the results. All these activities were done in order to validate the results of the study, and both teachers and students can be seen as co-researchers in this respect.

In ethnographic studies it can be difficult for the informants to know what data the researcher will use. Children's and young people's more limited life experience makes it even more difficult for them. This makes heavy demands on the researcher to be clear. The concept of _informed consent_ is complicated in school settings, and David _et al._ ([2001](#dav)) talk about _educated consent_, since students are taught to do what they are told.

Of course, the parents were informed and all gave their permission, but since the children and teenagers were the participants, their wish to participate had to be respected in the first place. In all of the four schools there were students who chose not to participate. In one, this caused a dilemma, since one boy's decision not to participate caused two other boys' dissociation from the study, as they were close friends. Dilemmas almost always occur when doing research with children. Problems have to be solved and demand sensitivity on the part of the researcher.

The analyses were mainly done from transcripts and notes taken from interviews and observations. The analyses were partly made with the help of the software NUD*IST for qualitative analyses, where sentences both from interviews and field notes were coded. Some analyses were of qualitative nature and based on selected material from the coded texts. In ethnographic analyses the material is read and reread several times and compared in different ways to see what themes will emerge. The categories found are of course a construction of the researcher, and since the first data collection was done in 1998-1999, the categories have been revised in this last analysis. This was not done due to new findings, but in order to construct a more functional structure. Other analyses were strictly quantitative and compared data from coded qualitative material with questionnaires and computer logs in a database sheet.

## Results

In my research I have identified six different skills, which the students point to as fundamental. The youngest students focus only on (1) language and (2) knowledge of technology, while the oldest students regard these skills as obvious and do not always mention them. These can be seen as basic skills, and are prerequisites for seeking information on the Internet. But there is always a need to develop them further. According to the students it is also important to have (3) knowledge of different ways of information seeking, and (4) how search engines work. These skills are very closely connected to the Internet. (5) Setting goals and (6) being critical, are skills that are also useful outside the Internet, and can also be seen as skills needed for understanding when _not_ to use the Internet.

### Language

Communicating is an essential skill and fundamental for an interest in information-seeking on the whole. When seeking information on the Internet there is usually a need to master the written language. (Alexandersson & Limberg [2004](#ale)). Even seeking pictures needs a written keyword. Language itself is needed for communication and to express and formulate questions about something you want to know.

At school A, with students from pre-school to grade 3, there had been a project the year before my study where the children had used the Internet for information-seeking (Jansson _et al._ [2001](#jan)). It could be seen that even the youngest children appreciated doing Internet searches, but there was a limit, which seemed to be connected with their reading skills. In my material it was obvious that the children became interested in reading when they saw the older ones working with the Internet and finding interesting information, which they shared with their classmates. If a student could not read, the solution was to find a classmate who could, so that the student could be helped do what he or she wanted to.

> I: What do you have to know to be able to use a computer? / - - - /  
> John (pre-school class): Oh, I know, I know! [shouts]  
> I: Yes?  
> J: You have to know how to read to be able to find anything. You have to read, because there is something written on every key.  
> I: If you don't know how to read, what do you do?  
> J: Then you have... spell! [shouts]  
> / - - - /  
> J: It's fun to read, I know that!

The results of the reading-comprehension test, which was conducted every year at this school, showed that all grade 1 students had reached a reading level corresponding to the average of grade 2\. Before the start of the project this had never happened. The grade 1 students had been working with the Internet since pre-school class.

Reading is a prerequisite for seeking information on the Internet, but working with the Internet in seeking information can, in turn, motivate learning to read.

### Knowledge of the technology

According to the curriculum of the Swedish comprehensive school, computer science is no longer a separate subject as it was earlier, in the 1980s. The computer is now seen as a tool for learning other subjects. Use of a computer therefore needs to be natural and 'transparent', and computer use is moving in that direction.

To the older students in the study, the computer is already a natural tool, which they do not reflect upon very much. To the youngest students, the lack of computer practice can be a problem. They are not always sure about how to get the cursor to move the way they want, where the letters are placed on the keyboard, or how to open up a program.

> I: What do you need to know to use the computer?  
> Eunice (pre-school class): The mouse and the cords.  
> I: Do you mean know what to do with them? Where to connect them?  
> E: My dad actually has got a mouse without a string. It runs by itself, you just take it in your hand and press something. Then it just comes out. It's super easy!  
> / - - - /  
> I: Then you don't have to know anything about cords?  
> E. No.

Among students of all ages it has been obvious that knowledge of how the Internet is constructed has made it easier for them to understand that everything on the Internet is not always what it seems to be at first sight. Turkle ([1984](#tur)) has shown that when Apple introduced the computer 'desktop' interface at the beginning of the 1980s, computers opened up to a different category of users than before. It was no longer necessary to know programming or have knowledge about the underlying technology. This is true in a sense, but my results also show that it is necessary to understand the technology enough to have an understanding of how the Internet system works, and the fact that anyone can put anything on the net (Enochsson [2001b](#enob)).

As seen above, technology has several dimensions that have to be trained if information seeking on the Internet is to be developed in a successful way. My results show that computer practice is important, and that some students have to be pushed in this respect. Among the younger students it was obvious that those who communicated through virtual communities or programs like MSN Messenger had a clearer picture of how the Internet system works, since through their activities they got a picture of how they and their friends could influence what they saw on the screen.

### Knowledge of different ways of seeking information

Generally, the Internet might not always be the best alternative for finding information. One of the teachers put it like this:

> Lisa (teacher grade 4): It's best when they have found it out themselves. When they have been sitting at the computer searching and searching without finding.  
> For example, when we worked with the Detective Story about Sweden (Sverige-deckaren), when I didn't lead at all how they searched. They could search just as they wanted. Then we compared the answers they had found, what time it took, and where they found them. And they laughed a lot when they found out that they had been sitting for two hours searching on the Internet and making phone calls, when someone else just opened their desk lid and found the same facts in three seconds. Then it became an 'ah-ha!' experience.

But she also wanted all students to learn how to do Internet searches, so that it could be a real option. What Lisa did was to let all the students find an answer to the same question by choosing their own way of searching. Through discussions in the classroom, the different answers and also the sources were compared. This way, her students developed knowledge about how different ways of seeking could work in different situations. The teachers in the other classes did the same as Lisa, although to varying extents.

On the Internet it is possible to search for and find information in different ways. Apart from using search engines, which is obviously the most common way of searching among young students, you can also search by linked sites or through the address space if you know how an address is composed.

> Tanya (grade 4): You need to have www first and something after.

Students with experience of information-seeking think in terms of how the person who has put the information on the Web might have thought. This is related to the most appropriate way to search.

> I: What do you need to know to search to find things?  
> Frank (grade 9): Write, press the mouse (laughs) / - - - / The people who put out their home pages write different key words to make it easier to find. Then you have to think about what they could have written. If you are looking for facts about Ethiopia, for example, it is not very difficult. Then you just have to write Ethiopia maybe "+facts". But you just have to think like they have done, and sort of... write like them.

### Knowledge of search engines

If the choice is to search for information through search engines such as Google and AltaVista, etc., it is easier if you know how to make a search more specific by using 'search engine maths' ([Search Engine Watch](http://www.searchenginewatch.com/ )) or Boolean logic.

The students mention the use of addition, subtraction and quotation marks. They also suggested that writing the search word with upper- or lower-case letters might have different consequences, as might using the space bar.

> I: Could you explain what to do to someone who hasn't got very much knowledge about searching on the Internet?  
> / - - - /  
> Ellie (grade 9): Well, first you go to a search engine, and then write what you want to find, preferably in lower-case letters, because it makes it wider. And if you want something... something more specific, then you can write a plus if you like, and then you can add quotation marks. But I don't really know why you do that, but...I've learnt to do that...  
> / - - -/  
> E: You can also put minus if you want something not to come up. Because it's easy to end up at a lot of porno sites if you search for bananas in Amazonas, for example. / - - - / So you get fewer sites if you put minus some stuff.  
> I: What can you put minus to get rid of the porno sites?  
> E: Well, "-nude girls" or something.

Some students say that they do not know very much, but they understand that there is more to learn to improve their technique. Several students have favorites among the search engines. They say that different search engines find interesting pages to different extents. How different search engines work is good to know, but not always easy to find out. Knowledge that they work somewhat differently can help.

Some students in the ninth grade did not know 'search engine maths' very well. They had never been taught, but had to rely on help from their classmates. Some had learnt enough from friends, but not all of them. A few students in this class had been taught how to search for information on the Internet in lower grades. They said that it had been very useful, especially the search engine maths.

### Setting goals

According to the students, there is a great risk of ending up just surfing around and/or doing something else if it is too difficult to find material. Goals can look different and have different degrees of specification. To function they have to be sufficiently specific.

> Alma (grade 4): It's good if you know exactly what to search for.  
> I: What else do you need to know to find things?  
> Christel (grade 9): I think it is to know what you are looking for. It's hard to know what you don't... You have to know what to search for. Because I've been looking for some things to write about in Social Sciences, and if I don't know what to look for, then I can't search for anything. So I have to know myself what I'm going to do on the net.

The students constantly refer to 'interest' as fundamental for an easier information-seeking process. Alexandersson and Limberg (2004) also found that an interest in the search task led to a greater understanding. At school, projects are often seen as doing a job to please the teacher (Gross [2001](#gro)). Some students can experience this kind of job as challenging, when they can find an interesting aspect of something the teacher has initiated. But that is not the common view (Enochsson [2001b](#enob)).

### Being critical

Different research traditions have different definitions of the concept of 'critical thinking'. One definition is a complex ability where critical scrutinizing in a logical sense is _one_ part, and emotional aspects, which include, for example, values and standpoints, are another. Values and standpoints have already been mentioned, and I will point now at two other parts: what kind of Web pages to rely on and understanding different perspectives.

There is a sort of concrete source critique, which consists of knowing what type of Web pages you can normally rely on. They can be governmental pages, pages belonging to established organizations, link sites from libraries, etc. Part of this concrete source critique is being suspicious of private home pages. Knowledge about what you are searching for is also essential.

> Michaela (grade 9): You find out if it is a subject you know something about, then you see it quite quickly. At least if it is a page where there are a lot of facts that are not right, then you can see it quite quickly because you know something about it. But if it is a page with a few things that are not right, and there might not be very many, then it's very hard to find it out. It's very easy to buy everything and believe in it.

The younger students describe it in terms of 'checking if it is reasonable', 'to think for yourself' or 'ask the teacher if you are uncertain'. But it is not always easy, because you need experience to understand that something might be wrong.

Understanding different perspectives is another part of critical thinking: to understand that there are other perspectives, that different standpoints can meet, and that none of the suggested answers has to be wrong, but are different sides of the same issue. The following quotation is an example of this:

> Anthony (grade 4): There can be some problems, since you can see all pages all over the world, there will be problems. In Iran they have other laws [than in Sweden], for example women cannot have shorts outdoors, and then, so to speak...There are the same Web pages with women having shorts outdoors and so...

My results show that when the teacher has a clear goal in her teaching and supports the students in developing critical thinking, things happen. At school A the first graders and the third graders had extra lessons where they talked and reflected about the reliability of the Internet. The lessons were studied during two periods in two consecutive semesters.; In the second semester there were significantly more students who understood my questions about reliability on the net and Internet competence. All these students were from grades one and three. Awareness was clearly increased in these grades, but not in grade two. At least how they talked about it. And since language has a role in development, this is extremely important.

Being critical bears a relationship to knowledge about how the Internet system works (Enochsson [2001b](#enob); [2005](#enoe)). This holds for students of all ages. As mentioned above, virtual communities and private chat programs like MSN Messenger work like gateways to understanding the system. Students who regularly use these kinds of applications show greater reflection about the reliability of the net. Almost all of the older students in the study use these applications, but there is a clear difference between the young users and the young non-users with regard to the degree of reflection on reliability.

## Discussion

Seeking information is a complex process, which demands knowledge and skills from various areas. At the same time it is important that these competencies are coordinated and form _one_ entity. Figure 1 shows a model where the different fields of competence are illustrated as radii from the centre of a spiral to its outer edge. As he or she gets older, the child moves from the centre and out, and also between the different fields of competence, which interact with each other and cannot be seen as separate entities. The development is described as a spiral, which symbolizes the fact that the different areas and competencies are trained constantly and recurrently.

<div align="center">![figure1](p240fig1.jpg)</div>

<div align="center">  
Figure 1: The various skills of the Intenet-seeking process as a spider's Web metaphor.  
* 'compulsory school' is the required school attendance in Sweden between the ages of 6 and 16</div>

The model of the spider's Web is borrowed from Corsaro ([1997](#cor)) who seeks to show that children's development can be seen from a reproductive view in contrast to a linear view with development stages. According to a reproductive view, children do not simply imitate or internalize the world around them, but participate in it and strive to interpret and make sense of the adult world. This model describes well the students' explanations about continuous training.

Compared to Eisenberg and Berkowitz' ([1990](#eis)) model _Big 6_, there are almost the same skills represented. But in _Big 6_ language is not regarded as a part of the information seeking process. The reason for bringing it up here is that the children stress language as an essential skill. But it is also important to remember that there are specific languages for each context, and in the information seeking process there is a certain way to use the language that has to be learned.

From evaluations of computer projects in the Swedish compulsory school, we have seen that there is a risk that the time for collective conversations will diminish when students spend more time in front of the computers (Nissen [2002](#nis)). This leads to diminishing prerequisites for learning and developing _the fourth basic skill_, that is, seeking, finding and scrutinizing information (Rask [1999](#ras)).

In all the studies mentioned in the introduction of this article, the researchers advocate more training and practice, so that students can become more skilful information seekers. Knowledge about search engines and different ways of searching are the skills most often mentioned. What the children talk about in addition to this is the importance of mastering language. Both formulating questions and problems and spelling correctly in search engines are necessary skills according to the children. Bilal ([2002b](#bilb)) makes comments on misspellings and syntax, but this can also be seen as a problem of the search engines. Bilal also suggests redesigning the search engine's interface. But there will always be words that are spelled quite similarly and knowing how to spell correctly must be trained even if search engines become more sensitive in this respect. From the students' point of view, language cannot be separated from the information-seeking process.

Knowing how to handle the technology is of course important when searching for information on the Internet. But knowledge about the system is also needed. Slone ([2002](#sloa)) as well as Enochsson ([2001a](#enoa); [2001b](#enob)) have highlighted the importance of having a model of the information system to become a successful information finder, which is well in line with what the students say.

One reason why teachers prefer to avoid information-seeking on the Internet is that there is so much unwanted information like pornography, racism, and violence on the net. The students also see this as a problem and stress the importance of learning how to handle it, since at least the older students think it is a Utopia to believe that filters can stop it.

## Conclusions

All the teachers in this study spent a lot of time reflecting together with the students in the classroom. They all pointed to, and also let the students themselves discover, possibilities and problems. This was done by letting the students work with information-seeking in combination with a great deal of time for discussion in the classroom. According to my results, this seems to be an important part of integrating the different skills into a single process. The teachers have to be attentive to all the different skills, but that does not mean that the different skills have to be trained separately. It is more important to train and practice in a meaningful context. Solomon ([1994](#sol)) discussed the importance of context in a pre-Internet study.

An important comment students of all ages make, is that it takes time to learn to seek information on the Internet, and sometimes it can be stressful. The older students say that it takes time to learn to become a skilful Internet searcher. More training and experience is also needed according to several researchers, as can be seen in the introduction of this article. The older students all know that source criticism is very important, but lack of time can lead to negligence. If you are interested in good marks, it can be better to read the books the teacher has already chosen instead of seeking the information on the Internet. The students also ask for guidance.

What this paper aims to show is young students' view on seeking information on the Internet, mainly in school settings, and how this can be related to a non-linear model in contrast to a linear model where the development are seen as stages. Linear models are often the way practitioners are used to look at learning, even though educational research has advocated other models for a long time. Compared to the space given to different skills in earlier research reports, it seems that the students in this study put a greater stress on basic language, basic computer skills, and critical thinking. Maybe the focus has been too strongly on search engines, and it is time to look more closely at other aspects of the information-seeking process.

## Acknowledgements

The author wishes to acknowledge the contributions of the Swedish Research Council, Karlstad University, the Knowledge Foundation, Sweden, the Interactive Institue, Stockholm and the suggestions of collegues and the anonymous referees.

## References

*   <a name="ale"></a>Alexandersson, M., & Limberg, L. (2004). _Textflytt och sökslump - informationssökning via skolbibliotek_. [Moving text and 'Googling': information seeking through the school library]. Stockholm: Myndigheten för skolutveckling.
*   <a name="bila"></a>Bilal, D. (2002a). Children's use of the Yahooligans! Web search engine. III. Cognitive and physical behaviors on fully self-generated search tasks. _Journal of the American Society for Information SCience and Technology,_ **53**(13), 1170-1183.
*   <a name="bilb"></a>Bilal, D. (2002b). Perspectives on children's navigation of the World Wide Web: does type of search task make a difference? _Online Information Review_, **26**(2), 108-117.
*   <a name="bilc"></a>Bilal, D., & Kirby, J. (2002). Differences and similarities in information seeking: children and adults as Web users. _Information Processing & Management_, **38**(5), 649-670.
*   <a name="cor"></a>Corsaro, W. A. (1997). _The sociology of childhood_. Thousand Oaks, CA: Pine Forge Press.
*   <a name="dav"></a>David, M., Edwards, R., & Alldred, P. (2001). Children and school-based research: 'informed consent' or 'educated consent'? _British educational research journal_, **27**(3), 347-365.
*   <a name="dre"></a>Dresang, E. T. (1999). More research needed: informal information-seeking behavior of youth on the Internet. _Journal of the American Society for Information Science,_ **50**(12), 1123-1124.
*   <a name="eis"></a>Eisenberg, M.B., & Berkowitz, R.E. (1990). _Information problem-solving: the Big Six skills approach to library & information skills instruction_. Norwood, NJ: Ablex.
*   <a name="enoa"></a>Enochsson, A. (2001a). Children choosing Websites. _The New Review of Information Behaviour Research_, **2**, 151-165.
*   <a name="enob"></a>Enochsson, A. (2001b). _Meningen med Webben_ [The use of the Web - fourth grader's experiences of doing Internet searches]. Doctoral dissertation, Karlstad University, Sweden. (Karlstad University Studies 2001:7).
*   <a name="enoc"></a>Enochsson, A. (2002). _[Datorn som redskap för lärande - konsekvenser för en förändrad lärarroll](http://hem.passagen.se/enochfri/IDUN.abe.pdf)_. [The computer as a tool for learning - consequences for a changed teacher-role.] Publication for the IDUN-project, Nordiska ministerrådet. Retrieved 15 September, 2005 from http://hem.passagen.se/enochfri/IDUN.abe.pdf
*   <a name="enod"></a>Enochsson, A. (2003). Seen through other eyes - children's abilities to find information on the net. _Tidsskrift for børne- og ungdomskultur_, (46), 87-107.
*   <a name="enoe"></a>Enochsson, A. (2005). [A gender perspective on Internet use: consequences for information seeking.](http://informationr.net/ir/10-4/paper237.html) _Information Research_, Retrieved 13 September, 2005 from http://informationr.net/ir/10-4/paper237.html
*   <a name="gro"></a>Gross, M. (2001). [Imposed information seeking in school library media centers and public libraries: a common behaviour?](http://informationr.net/ir/6-2/paper100.html) _Information Research_, **6**(2) paper 100\. Retrieved 15 September, 2005 from http://informationr.net/ir/6-2/paper100.html
*   <a name="ham"></a>Hammersley, M., & Atkinson, P. (1995). _Ethnography: principles in practice_. (2nd ed.). New York, NY: Routledge.
*   <a name="hul"></a>Hultgren, F., & Limberg, L. (2003). A study of research on children's information behaviour in a school context. _The New Review of Information Behaviour Research,_ **4**, 1-15.
*   <a name="jam"></a>James, A., & Prout, A. (ed.) (1997). _Constructing and reconstructing childhood: contemporary issues in the sociological study of childhood_. London: Falmer.
*   <a name="jan"></a>Jansson, M., Moll, B., Mossnelid, H., Norberg, K. & Olsson, E. (2001). _Maskrosen i världen_ [The dandelion in the world]. Forshaga, Sweden: Skivedsskolan. (ITiS-rapport).
*   <a name="kuh"></a>Kuhlthau, C.C. (1993). _Seeking meaning: a process approach to library and information services_. Norwood, NJ: Ablex.
*   <a name="lim"></a>Limberg, L., & Alexandersson, M. (2003). Constructing meaning through information artefacts. _The New Review of Information Behaviour Research,_ **4**, 17-30.
*   <a name="mer"></a>Merriam, S. B. (1988). _Case study research in education: a qualitative approach_. San Francisco, CA: Jossey-Bass.
*   <a name="nis"></a>Nissen, J. (ed.). (2002). _"Säg IT - det räcker"_. [Say IT - that's enough.] Stockholm: KK-stiftelsen.
*   <a name="ras"></a>Rask, S.-R. (1999). _Med eller utan filter?_ [With or without filter?]. Stockholm: KK-Stiftelsen. (KK-stiftelsens skriftserie 2000:5)
*   <a name="sch"></a>Schacter, J., Chung, G.K.W., & Dorr, A. (1998). Children's Internet searching on complex problems: performance and process analyses. _Journal of the American Society for Information Science,_ **49**(9), 840-849.
*   <a name="sloa"></a>Slone, D.J. (2002). The influence of mental models and goals on search patterns during Web interaction. _Journal of the American Society for Information Science and Technology,_ **53**(13), 1152-1169.
*   <a name="slob"></a>Slone, D. J. (2003). Internet search approaches: the influence of age, search goals, and experience. _Library and Information Science Research,_ **25**(4), 403-418.
*   <a name="sol"></a>Solomon, P. (1994). Children, technology, and instruction: a case study of elementary school children using an online public-access catalog. _School Library Media Quarterly_, **23**(1), 43-51.
*   <a name="tur"></a>Turkle, S. (1984). _The second self_. New York, NY: Simon & Schuster.
*   <a name="vyga"></a>Vygotsky, L. S (1986). _Thought and language_. London: MIT Press.
*   <a name="vygb"></a>Vygotsky, L. S. (1998). Imagination and creativity in childhood, In: R.W. Rieber (ed.) _The collected works of L.S. Vygotsky_. Vol. 5\. (pp. 151-166). New York, NY: Plenum.
*   <a name="wat"></a>Watson, J. S. (2001). Issues of confidence and competence: students and the World Wide Web. _Teacher Librarian_, **29**(1), 15-20.



