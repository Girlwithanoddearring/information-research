#### vol. 16 no. 4, November, 2011

# **Coverage of Spanish social sciences and humanities journals by national and international databases.**

#### [J. Mañana-Rodríguez,](#authors) and [E. Giménez-Toledo](#authors).  


G.I.de Evaluación de Publicaciones Científicas, Instituto de Estudios Documentales sobre Ciencia y Tecnología, Centro de Ciencias Humanas y Sociales, Spanish National Research Council, C/ Albasanz, 26-28\. 28037 Madrid, Spain

#### Abstract

> **Introduction**. This paper analyses the presence of Spanish journals in humanities and social sciences in seventy-nine databases of different types: specialized, specialized in a field different from that of the journal, and multidisciplinary databases, to determine the relative relevance of the databases by field and type of database.  
> **Method.** The mode of appearance of journals in databases as percentage of total appearance has been used as an indicator of relevance and coverage of the different databases.  
> **Analysis.** A descriptive analysis of the frequency of appearance of journals in databases has been used to describe the relevance and coverage of the databases. A logarithmic regression has been used to characterize the distribution of journals among databases.  
> **Results.** Strengths and weaknesses in the coverage of the databases studied by field and type of database were detected and Lotka's distribution was identified as the characteristic distribution of journals among databases.  
> **Conclusion:** Under-utilization of the databases of certain fields (e.g., geography) has been identified, together with the absence of specialized databases in some fields (e.g., law) and the need of quality improvement in certain databases. Proposals are offered for the improvement in the international visibility of journals and the diffusion strategies of publishers through the presence of journals.

## Introduction

The presence in international databases has been considered as an indirect indicator of quality and internationality of scientific journals. There have been numerous studies that have argued that the fact that a database has indexed a journal is directly related to the journal's visibility, with greater chance of being read by researchers from other countries and the consequent possibility of being cited ([Pérez Álvarez-Ossorio 1997](#per97); [Ren and Rousseau 2002](#ren02); [Gutiérrez Puebla 1999](#gut99); [Roman and Gimenez 2010](#rom10); [Alcain 2008](#alc08); [Mendoza-Parra 2009](#men09)). In fact, the presence in international databases is one of the factors taken into account by the Spanish Scientific Output evaluation agencies. Therefore, the National Agency for Quality Assessment and Accreditation ([ANECA](http://www.aneca.es/)), the National Commission for the Evaluation of Research Activity ([CNEAI](http://www.educacion.gob.es/horizontales/ministerio/organismos/cneai.html)), and the National Agency for Assessment and Prospective ([ANEP](http://www.micinn.es/portal/site/MICINN/menuitem.29451c2ac1391f1febebed1001432ea0/?vgnextoid=3cb39bc1fccf4210VgnVCM1000001d04140aRCRD)) ([Agencia Nacional 2007](#age07)) mention it as a criterion when considering the scientific level of a journal, specifying also the number and type of database in which a journal should be included to be considered a quality publication. Also The Spanish Foundation for Science and Technology's ([FECYT](http://www.fecyt.es/fecyt/seleccionarMenu1.do?strRutaNivel1=;la32fundaci243n&tc=gobierno_consejos)) call for promoting the internationalization of the Spanish journals includes, as one aspect to be considered in the evaluation process, the indexing of the publications by national and international databases in the speciality.

The project _European Reference Index for the Humanities_  mentions this aspect in their journal selection process, although not explicitly, as it refers to as "visibility"; in their _Summary guidelines_, a "Broad consensus within the field concerning international status and visibility" is mentioned as one of the characteristics of the publication to be categorized as A or B. Web of Science and Scopus refer to internationality aspects for the selection of journals, but do not refer to visibility through databases. Probably this indicator is used more by those countries that need to make their research results more visible and are in the process of internationalization.

In this sense, systems that work with Latin American journals are also a good example. Scielo supports, automatically, journals already indexed in Web of Science, Medline or [PsycInfo](http://www.scielo.org/php/level.php?lang=es&component=44&item=2). Moreover, various Latin American organizations involved in the management of science and technology and scientific information, asks the scholar community for the selection of publications to integrate what are often called _basic units of scientific journals_. These cores, also called _basic unit_s consist of limited sets of national scientific journals that meet high quality requirements and, for that reason, are supported in one way or another. The indexing of the journals by international databases is one of the criteria that most of these organisms handle such as in the case of the Argentinean Centre for Scientific and Technologic Information ([CAICYT](http://www.caicyt.gov.ar/index.php?option=com_content&view=article&id=32&Itemid=119&lang=es)), the Colombian National Commision for Scientific and Technologic Research ([CONICYT](http://www.conicyt.cl/573/article-3373.html)), or the Mexican National Council for Science and Technology ([CONACYT](http://www.conacyt.gob.mx/Indice/Documents/Indice-de-Revistas_Convocatoria-2008.pdf)).

The study of the presence of journals in databases is of interest, therefore, as a support tool for the evaluation of research activity. But it is also clear that it is interesting to publishers as being in databases means to be visible to the international scientific community and to have more ability to influence groups or research lines:

The fact that many journals are not indexed or are indexed in a maximum of two bibliographic bases also contributes to the invisibility of scientific production whose only future, it seems, is to disappear. The editorial groups responsible for these publications must accept the inevitability of the need to '_index or disappear_', just as authors had to learn to '_publish or perish_' ([Mendoza-Parra _et al._ 2009](#men09): 60).

Undoubtedly, the editor should play an active role with the producers of databases, providing them information regarding the publication and taking the necessary steps to distribute it in different databases. Somehow, the fact that a journal is visible on the international scene must be part of a journal's strategy. On the other hand, to be or not to be indexed in international databases is related to the quality of the publication because, in some cases, the journal has to meet certain quality requirements in order to be indexed by the producer of the database.

## Objectives

From the analysis of the presence of Spanish journals in humanities and social sciences in international databases (specifically, in seventy-nine done for the system of journal evaluation Difusión y Calidad Editorial de las Revistas Españolas de Humanidades y Ciencias Sociales y Jurídicas,  [DICE](http://www.webcitation.org/query?url=http%3A%2F%2Fdice.cindoc.csic.es%2Fbusqueda.php%3Fidioma%3Den&date=2011-11-27), difussion and editorial quality of Spanish social sciences and humanities journals), is the aim of this article to know the profile of each field in terms of visibility, measured by their presence in databases, in order to identify weaknesses and strengths in their strategy with regard to its international presence. More specifically, is the aim of this research to:

1.  know whether the subjects are, in general terms, well covered by international databases
2.  analyse and characterize the presence of Spanish humanities and social sciences scientific journals in multidisciplinary databases, databases specializing in the field and databases specializing in other fields.
3.  determine the relevance of each database in each field.
4.  identify the databases that cover more journals in each field (identify the mode).
5.  identify the fields that are more covered by databases according to the quality of the database, **A** being the highest quality, **B** an intermediate quality, and **C** a lower quality database (the criteria to classify the databases can be found in [DICE methodology](http://www.webcitation.org/query?url=http%3A%2F%2Fdice.cindoc.csic.es%2Fmetodologia.php&date=2011-11-27))

We intend to know, therefore, if the journals in each field choose selective and demanding databases, or if they seek simply to be in any database. It is the aim of this research also to see if publishers have strategies to reach their targeted audience and the best databases.

1.  To identify disadvantaged fields when compared to others because of the absence of specialized databases in the field and if the presence of the journals from these disadvantaged fields in multidisciplinary databases is comparatively high.
2.  To identify extreme or atypical cases of fields where journals are represented in databases of the different kinds to a greater extent than usual.
3.  Develop, according to the available data, suggestions for the editors of journals of different fields regarding the improvement of their presence in national and international databases. Thus, in addition to the improvement in international visibility, they would be more recognized in the assessment processes and by the relevant agencies.

Many of these aspects suggest how complex the assessment of diffusion may become, as many variables, such as thematic databases coverage, the frequency with which of a given journal is indexed in a database, and the criteria used by databases are involved in the process.

If the assessment agencies and information systems accept the presence of journals in databases as an indirect quality indicator, it is necessary to establish the differences between the possibilities that each journal has to be indexed in databases, as well as the quality of the databases themselves, since not all the studied databases are selective and, therefore, not all are rigorous in the selection of titles. Without making these distinctions an unequal treatment would be given to journals.

Beyond the specific results presented in this work, that are of particular interest to Spanish publishers, it is noteworthy that the methodology can be useful to any editor, especially for those seeking the improvement of their visibility in databases. On the other hand, database producers could also identify in this article fields lacking database coverage, for which it might be interesting to create databases.

Although presence in international databases as an indicator has been treated with a certain simplicity (a journal is present or not present in databases, is in X number of databases, or is in Web of Science, Scopus and other databases), the truth is that to attend to details, it is a complex aspect to be valued fairly ([Torrado _et al._ 2010](#tor10)).

First, not all databases are equal in their selection criteria: in some of them a journal can enter simply by asking to be indexed, in others it is sufficient that the journal belongs to the field covered by the database, and other selection processes are rigorous and evaluate different quality parameters. Moreover, not all fields have databases that specifically cover the field and, therefore, the journals of these fields are less likely to be visible through databases. Finally, the coverage of a journal by a database may not be stable and continuous; the database can make a selective index, or even index certain years but not others.

Because of these factors, there have been some attempts to assess the presence of a journal in a more complex form, considering the identified variables. For example, the project Information Matrix for Journals' Assessment, developed by Barcelona University (Matriu d'información per a l'Avaluació de Revistes, [MIAR](http://www.webcitation.org/query?url=http%3A%2F%2Fmiar.ub.es%2Fque.php&date=2011-11-27)) applies the Composite Index of Secondary Dissemination to Spanish and international journals. A score is assigned to each journal depending on whether the journal is in Web of Science or Scopus (highest score), if it is in other multidisciplinary databases (intermediate score) and in specialized databases.

Furthermore, this presence in databases is linked with the number of years the journal has been edited, on the basis that veteran journals have more opportunities to be indexed in different databases than the recently created ones. The indicator obtained for each journal produces a ranking according to the presence in databases.

A different approach, given the proliferation of tools that provide different quality indicators for scientific journals of different nature, is the proposal of the Integrated Classification of Scientific Journals developed in the work of Torres-Salinas _et al._ ([2010](#tos10)). This classification groups the journals in Group of Excellence, A, B, C and D having as root information their presence in different systems of evaluation of scientific journals and databases. More specifically, the following sources are considered: databases integrated in _Web of Science, Scopus, Scimago Journal Rank, The Spanish Social Sciences Impact Factor ([IN-RECS](http://www.webcitation.org/query?url=http%3A%2F%2Fec3.ugr.es%2Fin-recs%2F&date=2011-11-27)), The Spanish Law Journals Impact Factor ([IN-RECJ](http://www.webcitation.org/query?url=http%3A%2F%2Fec3.ugr.es%2Fin-recj%2F&date=2011-11-27)), Difusión y Calidad ([DICE](http://www.webcitation.org/query?url=http%3A%2F%2Fdice.cindoc.csic.es%2F&date=2011-11-27)), the European Reference Index for the Humanities ([ERIH](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.esf.org%2Fresearch-areas%2Fhumanities%2Ferih-european-reference-index-for-the-humanities.html&date=2011-11-27)) and the [Latindex](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.latindex.unam.mx%2F&date=2011-11-27) _catalogue.

The proposal of Torres-Salinas _et al._ ([2010](#tos10)) would be a by-product of various databases which apply quality indicators to journals and, therefore, differs from other initiatives, as it would take into account not the presence of journals in bibliographic databases, but in those with the aim of the evaluation of scientific activity.

## Material and methods

To tackle the analysis of the visibility of Spanish journals in humanities and social sciences Difusión y Calidad Editorial de las Revistas Españolas de Humanidades y Ciencias Sociales (difussion and editorial quality of Spanish social sciences and humanities journals, [DICE](http://www.webcitation.org/query?url=http%3A%2F%2Fdice.cindoc.csic.es%2F&date=2011-11-27)) database, which integrates over a hundred quality indicators for 2,384 journals, 1,887 of which are currently published has been used. Each journal is associated with a field (sometimes to more than one) and to the multidisciplinary or specialized databases in which it is indexed. The databases which are likely to appear total seventy-nine (See [Appendix I](#app1)) that are scanned periodically to check their coverage.

Once this information was obtained, it was broken down and the information regarding the databases was classified, obtaining not only the name of the database on which each journal was indexed, but whether it is multidisciplinary, specialized in the field of the journal or specialized in a field different from that of the journal.

To achieve this, the starting point was the thematic coverage declared by the databases themselves and the thematic classification assigned in _Difusión y Calidad Editorial_ for each journal. The information regarding the category of each of the databases (categorised below) based on the quality criteria required by the databases was also considered.

The frame of the information gathering process has been the "diffusion assessment” done by the [G.I. Evaluación de Publicaciones Científicas](http://www.webcitation.org/query?url=http%3A%2F%2Fepuc.cchs.csic.es%2Fplataformas.html&date=2011-11-27) (Evaluation of Scientific Publications Research Group). To calculate the indicator for journal's diffusion which can be found in _Difusión y Calidad Editorial_, the presence of each journal in the seventy-nine international national and international, multidisciplinary as well as specialized databases was gathered and verified. The databases are categorized according to the following schedule:

*   Category A: databases that require, at least, a peer review system, that the periodicity declared by the journals is fulfilled, that journals include the working place of the authors as well as an abstract.
*   Category B: those that require some journals to meet some of the above criteria, but not all.
*   Category C: databases that require only the journals to belong to the same field of the database.
*   Category SD (no data): which do not indicate their selection criteria on their websites or in the requests that have been made to that effect?

Finally, a score for the presence in each database according to their thematic coverage (if it is specialized, specialized in a different field, or multidisciplinary) and its category ([Alcain _et al._ 2008](#alc08)) is assigned to each journal. The sum of these scores is the indicator “diffusion assessment”, which also allows us to establish a ranking of journals.

Three different types of presence of journals in databases were found, according to their thematic coverage: there are journals of a particular field in databases specialized in the same field, journals that appear in databases specialized in a different field. Finally, it is possible to identify the presence of journals in multidisciplinary databases. This last set contains journals belonging to several different fields.

Once all this information for each journal was obtained, the data at the level of field was aggregated, because it was the aim to observe the visibility of the journals aggregated by fields, but not individually, and a summary of cases was done ([Appendix II](#app2)) in which it can be seen the frequency with which journals appear in each field, type of database (specialized in the field, specialized in a different field or multidisciplinary) and category (A, B, C or SD).

Once the information was properly structured and the non-normality of the distribution of frequencies of occurrence of journals in databases was verified (Kolmogorov-Smirnov nonparametric test) the modal database, i.e., the database of each type in which the greater number of journals in each specialty is concentrated, was identified. Since the range of these modal values is widely different in different fields, the percentage that the mode represents of the total frequency of appearance of journals in the databases for each field was calculated, allowing comparison of its value between fields. There are two reasons for expressing the mode as a percentage of total frequency of appearance in databases:

a) If only the mode is taken into account, it is possible to say that the database "X" is that which concentrates the largest frequency of the journals in field "Y", or that the database "A" is that which concentrates the largest presence of journals in field "B". However, it is possible that database "X" could represent 90% of the presence in databases of journals in field "Y", while database "A" may represent only the 20% of the frequency of presence of the journals in field "B". Expressing the modal databases as a percentage allows comparison of the proportions they represent.

b) The modal database for each type of database (specialized in the field, specialized in a different field or multidisciplinary) is an approach to the concentration or dispersion of journals among databases for each field, and also gives an idea of the relevance of each database.

The mode as a percentage can be understood here as an indicator of "relative relevance" that a database has in a particular field and /or between fields.

## Results

In first place, information about the frequency with which a number of journals present in a number 'n' of databases has been obtained. (Table I).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size:
      smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style:
      normal; font-family: verdana, geneva, arial, helvetica, sans-serif;
      background-color: #fdffdd"><caption align="bottom">  
**Table 1: Journal distribution among databases.**</caption>

<tbody>

<tr>

<th>Number of databases (n)</th>

<td align="center">1</td>

<td align="center">2</td>

<td align="center">3</td>

<td align="center">4</td>

<td align="center">5</td>

<td align="center">6</td>

<td align="center">7</td>

<td align="center">8</td>

<td align="center">9</td>

<td align="center">10</td>

<td align="center">11</td>

<td align="center">12</td>

<td align="center">13</td>

<td align="center">14</td>

</tr>

<tr>

<th>Number of journals in n databases</th>

<td align="center">499</td>

<td align="center">300</td>

<td align="center">166</td>

<td align="center">104</td>

<td align="center">53</td>

<td align="center">32</td>

<td align="center">20</td>

<td align="center">23</td>

<td align="center">6</td>

<td align="center">6</td>

<td align="center">4</td>

<td align="center">3</td>

<td align="center">1</td>

<td align="center">2</td>

</tr>

</tbody>

</table>

There are 499 journals that can be found in a single database, 300 in two databases, 166 in four databases, etc. It has been observed that these frequencies converge and are inversely proportional to the number of databases. Because of these two conditions, they could fit Lotka's Law ([Martín-Sobrino _et al._ 2008](#mar08)). . As a consequence of this, a logarithmic regression model of the variable Number of Journals (Njournals) on the variable Number Databases (NDB) has been applied to the data, obtaining the following results:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size:
      smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style:
      normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2a: Regression model summary.**</caption>

<tbody>

<tr>

<th>Model</th>

<th>R</th>

<th>R<sup>2</sup></th>

<th>Corrected R<sup>2</sup></th>

<th>Typical error of estimate</th>

</tr>

<tr>

<th>1</th>

<td align="center">0.952<sup>a</sup></td>

<td align="center">0.905</td>

<td align="center">0.898</td>

<td align="center">0.62401</td>

</tr>

<tr>

<td colspan="5">a. Predictor variable: (Constant), Number of databases.      The independent variable is the number of journals.</td>

</tr>

</tbody>

</table>

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size:
      smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style:
      normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2b: Regression model coefficients.**</caption>

<tbody>

<tr>

<th rowspan="2">Model</th>

<th colspan="2">Non standardized coefficient</th>

<th>Standardized coefficients</th>

<th rowspan="2">t</th>

<th rowspan="2">Sig.</th>

</tr>

<tr>

<th>B</th>

<th>Typical Err.</th>

<th>Beta</th>

</tr>

<tr>

<th>1 (Constant)</th>

<td align="center">7.313</td>

<td align="center">0.435</td>

<td align="center"> </td>

<td align="center">16.793</td>

<td align="center">0.000</td>

</tr>

<tr>

<th>NBD</th>

<td align="center">-2.397</td>

<td align="center">0.224</td>

<td align="center">-0.952</td>

<td align="center">-10.722</td>

<td align="center">0.000</td>

</tr>

</tbody>

</table>

According to Lotka's Law, the relationship between the number of journals and the number of databases is explained by the equation (-2.397 has been approximated to -2.4):

<div align="center">![Equation 1](p506equation1.jpg)</div>

This has been deduced from the logarithmic regression expression:

<div align="center">![Equation 2](p506equation2.jpg)</div>

Where Njournals is the number of journals in n databases and NDB is the number of databases in which the journals can be found.

<div align="center">![Logorithmic scale dispersion of journals](p506fig1.jpg)</div>

<div align="center">  
**Figure 1: Logarithmic scale dispersion of journals by number of databases.**</div>

As can be seen, the fit between observed and expected value according to regression model is acceptable, except for the number of journals present in one single database, signalled as an X in the graph.

It was concluded that the distribution of journals among databases for all fields of knowledge studied acceptably fits Lotka´s distribution.

The analysis of the data according to the method proposed allows us to highlight another result: the weight that the three sets of databases (specialized, specialized in a different field and multidisciplinary) present within each of the fields studied. The following chart presents a synthesis of this data set.

<div align="center">![Percentage of journals present in the databases](p506fig2.jpg)</div>

<div align="center">  
**Figure 2: Percentage of journals present in the three types of databases (by field).**</div>

The three segments of each column represent the three types of databases that have been studied. The top part of the bars (yellow) represents the percentage of journals per field which indexed in multidisciplinary databases. The middle part of the bars (dark blue) represents the percentage of journals found in specialized databases in a different field (e.g., literature journals found in _Regesta Imperii_, specializing in history). Finally, the lower part of the bars (pale blue) represents the percentage of journals found in specialized databases in the same field.

The following table summarizes the minimum and maximum values detected:

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size:
      smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style:
      normal; font-family: verdana, geneva, arial, helvetica, sans-serif;
      background-color: #fdffdd"><caption align="bottom">  
**Table 3: Minimum and maximum values present in different types of databases**</caption>

<tbody>

<tr>

<td> </td>

<th>Specialized</th>

<th>Specialized in a different field</th>

<th>Multidisciplinary</th>

</tr>

<tr>

<th>Maxima</th>

<td>   Town planning (60.3%)</td>

<td>   Geography (38.1%)</td>

<td>   Law (78.4%)</td>

</tr>

<tr>

<th>Minima</th>

<td>   Geography (4.2%)</td>

<td>   History (4\. 2%)</td>

<td>   Town planning (32.4%)</td>

</tr>

</tbody>

</table>

There are some remarkable percentages in these results. The first to be commented is geography: it has little presence in databases specialized in the field (only 4.2% of the journals can be found on them); however, this set of journals reasonably present in databases specialized in other fields (38\. 1% of the journals).

This can be explained by the interdisciplinary character of the field, which makes it more visible in other areas or at least more likely to be seen. A similar case, though with less pronounced differences, is prehistory and archaeology: 11% of its journals are indexed in specialized databases and 34% in databases of other fields. Again, its interdisciplinary nature could explain this diffusion pattern, which is less focused on specialized databases.

Other fields which also register low levels of presence in databases specialized in their field are anthropology and law, but, unlike geography, their presence in databases of other specialties is low (22% and 11% respectively). In the case of law (and possibly as compensation for its low level of presence in specialized databases) journals in the field (78%) are highly present in multidisciplinary databases.

Latin American studies, philosophy, fine arts and political science offer a similar profile in the sense that the distribution between specialized databases and databases specialized in other of other fields is similar (percentages close to 20%). This finding could point to a certain degree of interdisciplinarity and the existence of databases covering fields related to, or interested in each of the fields.

The cases of history, psychology, library and information science, and even linguistics, philology and literature could be clustered by their strong presence in specialized databases, which with no doubt is related to the existence of relevant disciplinary databases, which are highly identified with these fields.

Town planning is an outlier, as it presents a very high presence in specialized databases (above 60%). One of the reasons for this could be that one of the databases analysed, _Urbadoc_, specializes in the field and has a fairly wide coverage of journals; i.e., it is not as selective as other specialized databases. There is no other example like this in specialized databases.

Another level of analysis relates to databases that give more coverage to each field. This approach allows us to gather knowledge of the databases that are being used to diffuse Spanish journals and, conversely, those that are not used.

Moreover, and thanks to the categorization of databases mentioned in the methodology, fields that are prominently present in selective databases (Categories A and B) or in other types of databases can be identified.

From this study we can identify a) fields that do not have selective databases and, therefore, that lack of quality referents; b) fields that have high quality journals, which results in them being present in high quality databases; and c) fields that have selective internationally recognized databases, but do not have their journals indexed in these databases, either for lack of quality or absence of strategy to request their inclusion.

The following graphic representation synthesizes three types of information: the modal database among a field's journals, what percentage of journals in that field is covered by the database and finally, the category of that database, taking into account whether the presence of the journals occurs in a specialized database, in a database specialized in a different field from the journal one or in a multidisciplinary database.

<div align="center">![Distribution of modal databases specialized in the field](p506fig3.gif)</div>

<div align="center">  
**Figure 3: Distribution of modal databases specialized in the field as a percentage of total.**</div>

As may be seen, one of the ends of the graph is represented by geography, a relevant case, as 100% of the journals that are present in specialized databases in geography are in Geobase. However, there are three more databases that cover geography (_Anthropological Literature_, _Academic Search Complete_ and _Georef_) in which none of the journals considered is listed, which means that opportunities to disseminate and highlight the journals are being missed. At the minimum end of the graph, political science is identified, whose two modes account for 30.77% of total presence of journals in the field in specialized databases.

In a similar situation, except that in these cases there is presence of other journals in the specialized databases in the field, is possible to identify the journals belonging to educational sciences with a high concentration of journals in IRESIE (95.74 %) within the set of specialized databases in educational sciences, prehistory and archaeology, in which a 93.75% of the presence in specialized data bases occurs in _Anthropological Literature_. Similarly, in sociology, a 92.31% of the frequency of appearance in specialized databases is concentrated in _Sociological Abstracts_.

In all these cases, all the modal databases present category A or B and, therefore, are selective about the journals, which is a good indicator of quality for the journals in the field.

Immediately after, with lower percentages but still relatively high, there can be identified town planning, Latin America studies, philosophy, history and anthropology, psychology, economics, linguistics, philology and literature, and law.

Finally, fine arts, library and information science and political science, have modal values that represent a low percentage of the total presence of journals in these fields in specialized databases.

Regarding the category of mode specialized databases for each field, it can be noted that from a total of sixteen modal databases, five present category A, six category B, one category C and one category SD (Fine arts, RILM). There seems to be a predominance of high quality mode specialized databases. It should be noted also that in the case of Law, Academic Search Complete has Category B, while FLP, having the same frequency as the previous database presents SD category. In the case of political science, IPSA has Category A, and PAIS has B category.

<div align="center">![Figure 4: Distribution of modal databases specialized in another field as a percentage of total.](p506fig4.gif)</div>

<div align="center">  
**Figure 4: Distribution of modal databases specialized in another field as a percentage of total.**</div>

In the case of specialized databases in other fields the differences are more progressive, as shown in Figure 4\. Thus, one of the extremes of the graph is represented by fine arts with a large presence in _Regesta Imperii_ (a database specializing in history which appears frequently as a modal database specialized in field different from that of the journal, possibly due to the concomitance of fine arts and history through art history).

At the other extreme, anthropology presents the three modal databases, each of which represents an 18.18% of the total frequency of appearance of journals in databases specialized in other fields. These are _Historical Abstracts_, _Sociological Abstracts_ and _Urbadoc_ (town planning), three fields with which anthropology interacts. Therefore, its presence seems to match the interdisciplinary nature of anthropology.

In this sense, it is interesting to observe other specialized databases in which the journals of a field are present, as this can provide an approximation of the relationship between fields:

_Urbadoc_ database specializes in town planning in which there are present a larger number of journals from geography. Undoubtedly, this figure represents the thematic proximity and mutual interest of urban geography and town planning.

_Academic Search Complete_ is the modal database specializing in a different field for the journals belonging to library and information sciences and educational sciences, although with very different percentages (75% in the first case and 25% in the second). However, it is not specifically relevant data when we take into account the fact that the database can be considered multidisciplinary (as declared by the database).

_Regesta Imperii_ (specializing in history) can be highlighted in this analysis as modal database specialized in a different field for fine arts, linguistics, philology and literature, archaeology and prehistory and philosophy, which means that its thematic coverage is much larger than the database declares; that is, it covers many more fields than history and at the same time, shows the relationship between all these fields.

_Sociological Abstracts_ can be highlighted for political science, and as expected, Worldwide Political Science Abstracts can be highlighted for sociology. This symmetry is a reflection of the strong association between the two fields.

Other examples of interdisciplinarity can be observed, though to a more limited degree, in economics, law, psychology and history. In the first case, one of the modal databases from the analysis is _CAB Abstracts_ which specializes in agriculture.

Agricultural economics, as a subfield of economics but also related to agriculture perfectly fits in CAB Abstracts; psychology of language journals, as belonging to a subfield of psychology but also linked to linguistics, can be found in a database specialized in linguistics and language, _Linguistics & Language Behavior Abstracts_, and some economic history journals, related to both economy and history, are indexed in an economics specialized database: Econlit.

Also, the bordering fields of law and political science are reflected in the presence of law journals in _International Political Science Abstracts_. Another example of multidisciplinary is the fact that journals belonging to Latin America studies are present in _Anthropological Index Online_. Actually, Latin America studies are approached from different fields (economics, history, politics, etc.), but certainly the anthropological approach is prominent, so that the presence in that database fits the definition of this field. This behaviour pattern of journals, their presence in databases specialized in other fields, but which cover the different sub-fields of a field, should be observed more often. If more journals follow this pattern an optimum visibility and a logical use of the resources would be reached.

<div align="center">![Figure 5: Distribution of modal multidisciplinary databases as a percentage of total.](p506fig5.gif)</div>

<div align="center">  
**Figure 5: Distribution of modal multidisciplinary databases as a percentage of total.**</div>

This graph has a lower weight than the previous considering that the set of journals is composed by Spanish journals and the Spanish database that collects the national scientific production is ISOC. That is, the fact that ISOC is the modal database in all fields is conditioned by the data source. However, this database is selective and quality criteria are applied to the journals before deciding on their inclusion. This data is interesting to interpret the graph because it can be clearly seen which fields behave globally better, or which have better journals, which make them be present in ISOC.

It reaches the maximum value in the case of Juridical Science, as ISOC represents 79% of the presence of its journals in multidisciplinary databases while the minimum value (31%) corresponds to the journals of Latin America studies.

Although ISOC is a multidisciplinary database and may not exhaustively include all journals and therefore the presence of journals in specialized databases may be stronger quality-related information, it might be recommendable for the fields found in the left side of the chart to improve their quality, in order to reach visibility through their presence in a multidisciplinary database.

## Discussion and conclusions

First, it has been observed that the distribution of frequency of presence of journals in databases fits Lotka's law, it is to say, that few journals are in many databases and many journals are in few databases.

There are very significant variations in the percentage that the modal databases represent from the total database in which journals are present in a particular field: the range of values found in the case of mode specialized in the field databases goes from 30.77% to 100%. Since the data can be found in [Appendix II](#app2), here only those cases with a specific particularity will be commented.

With regard to the presence in specialized databases, _Town-Planning_ journals show a high percentage of journals in specialized databases (Urbadoc, due to the bias of the data source mentioned above) but its category is C. The journals belonging to Town-Planning might be more present in AIAP (category B) and in The Architecture Database, which, although without category, can provide opportunities of international projection for its publications.

_Geography_ represents a minimum in the percentage which the presence of its journals in specialized databases of the area represent respect the total presence in databases for the discipline, and a maximum in the databases specialized in other fields. In this sense, it could be said that journals from this field should strive to be more present in GeoRef (C), Antropological Literature (A) and Academic Search Complete (B).

In _Educational Sciences_ there has not been identified category A databases, although the editors have followed a good strategy by applying for inclusion in IRESIE (B) which represents 96% of the presence of the field's journals in specialized databases. In addition, there exist two other databases that could multiply the diffusion and are being underutilized: ERIC and ERA (both category B).

In a similar situation, journals belonging to _Archaeology & Prehistory_ are being collected mainly by Anthropological Literature (A), but are poorly covered by the other specialized databases in the field: AA and AIO (B).

The reference database in _Sociology_, Sociological Abstracts (B) represents a high percentage of the presence of Spanish journals in specialized databases (92%). However, in this field, other databases which include Sociolgy in their coverage seem to be little explored: Anthropological Literature (A), SSA (B), IPSA (A), Psyclit (A) and Academic Search Complete (B).

_Latin America studies_ shows a pattern that should be more frequent and that can be resumed as follows: the two databases of the field (HAPI and HLAS) cover a similar and relatively high percentage of journals. About a half of the editors have promoted the presence of its journals in these databases.

AIO represents 50% of the presence of _Anthropology_ journals in specialized databases in the field, but has neglected the presence in Antropological Literature (A) and Academic Search Complete (B). In short, anthropology could improve the percentage of journals found in databases and the variety of bases in which their publications could potentially be indexed.

_Philosophy_ journals could expand their presence in the reference database in the field: Philosopher's Index (PHI) which, furthermore, is an A category database. These journals could also improve their visibility requesting their inclusion in the Repertoire Bibliographique de la Philosophie de Louvain (RBPH).

America: History and Life (AH), L'Anée philologique (APH), International Medieval Bibliography (IMB), _Academic Search Complete_ and _Anthropological Literature_ are databases that can provide launching opportunities and visibility for Spanish _history_ journals, and which, nevertheless, are being underutilized. On the other hand, specialized databases in other fields represent a minimum percentage in the case of history.

_Geography_ has a particularly low (4.2%) percentage of presence in specialized databases compared with other fields.

_Law_ presents a low presence in specialized databases (9.9% constituted by two databases with the equal presence in both). This field has, at the same time, the maximum presence in multidisciplinary databases (78.4%), which might be related to a lack of specialised databases covering Spanish law.

The modal database for _fine arts_ represents a modest percentage (36%) of the distribution among databases of journals in the field, and on the other hand this is an SD category database.

In the case of modal specialized databases in a different field, this measure is identified as a possible indicator for measuring the proximity between fields: the specialized database of a different field in which there is a wider coverage of journals of a certain field may point towards a closer relationship between the two fields than between the first and any other, at least in terms of diffusion.

In connection with the comment above, _Regesta Imperii_, specializing in history, holds an abnormally high presence as modal database specialized in a different field, which could be interpreted as an evidence of the connection of history with other fields of social sciences and humanities.

Taking into account that the bibliographic databases are an element that contributes to the diffusion of scientific knowledge, are evaluated as indirect quality criteria, and require for their maintenance an economic investment, it is necessary in the light of the evidence presented, for the editors of scientific journals to review the status of each database within the scope of each field.

This review of the positioning of the journals in databases would be justified by the fact of knowing what fields can be explored and exploited by the Spanish publishers of scientific journals for the sake of a better use of existing resources that will directly increase their visibility. A very simple and obvious conclusion of this study is that in many cases mode represents a high percentage of the presence in specialized databases and, consequently, the remaining databases play a very secondary role. It would be desirable, therefore, to take advantage of the opportunities that the set of databases can offer, and especially those that are selective and provide a "seal of quality" to journals.

In the fields which modal database represents a small percentage of total presence of the journals from the field, there is a lack of clearly significant databases where the journals from the field can be found, it is, there is a high level of dispersion.

It would be possible to propose as databases in which representation of a journal would be beneficial those that are modal for the field, are in either category C or SD, or represent a very small percentage of the total presence in specialized databases of the journals of the field.

Finally it can be concluded that, although the mode as a percentage is considered in this study as an indicator of relevance of the databases, it can also be used to reflect the relative relevance of different study objects in infometrics and/or bibliometrics. The use of the mode as a percentage could be useful in asymmetric bibliometric distributions as the frequency of presence of scientific journals in databases. The reason for this usefulness is that it allows its replication in different contexts and/or moments. On the other hand, this kind of analysis can be useful for strategic decision making by scientific journal editors.

## Acknowledgements

This research has been funded by the project SEJ2007-68069-C02-02 **"**Valoracion Integrada de las Revistas Españolas de Ciencias Sociales y Humanas Mediante la Aplicacion de Indicadores Multiples**"** (Integrated rating of Spanish journals of social sciences and humanities through the application of multiple indicators) of the Spanish Ministry of Science and Innovation and the agreement between the Agency for Quality and Accreditation and the Spanish National Research Council: Seguimiento y valoración de las revistas españolas de Ciencias Sociales y Humanas (Monitoring and evaluation of Spanish journals in social sciences and humanities).

## About the authors

Jorge Mañana-Rodriguez is a PhD student at the Spanish National Research Council. He can be contacted at [jorge.mannana@cchs.csic.es](mailto:jorge.mannana@cchs.csic.es).

Elea Giménez-Toledo is Senior Researcher at the Spanish National Research Council, contactable at [elea.gimenez@cchs.csic.es](mailto:elea.gimenez@cchs.csic.es).

#### References

*   Alcain-Partearroyo, M.D., Román-Román, A., & Giménez-Toledo, E. (2008). Categorización de las revistas españolas de Ciencias Sociales y Humanidades en RESH. [Categorization of Spanish journals in Humanities and Social Sciences RESH] _Revista Española de Documentación Científica_, 31 (1), 85-95.
*   Agencia Nacional de Evaluación y Prospectiva _and_ Fundación Española para la Ciencia y la Tecnología. (2007). Criterios de calidad para la investigación en humanidades [Quality criteria for research in humanities] _._Madrid: ANEP/FECYT.
*   Gutiérrez-Puebla, J. (1999). Las revistas internacionales de Geografía: internacionalización e impacto. [International journals of geography: globalization and impact.] _Boletín de la Sociedad de Geógrafos Españoles_, 27, 117-134.
*   Martín-Sobrino, M.I., Pestana-Caldes, A.I. & Pulgarín-Guerrero, A. (2008). [Lotka Law applied to the scientific production of information science.](http://www.webcitation.org/63WuxSAmi) _Brazilian Journal of Information Science_, **2**(1), 16-32\. Retrieved 28 November 2011 from (Archived by WebCite® at http://www.webcitation.org/63WuxSAmi)
*   Mendoza-Parra, S., Paravic-Klijn, T., Muñoz-Muñoz, A.M., Barriga, O.A. & Jiménez-Contreras, E. (2009). Visibility of Latin American nursing research (1959-2005). _Journal of Nursing Scholarship_, **41**(1), 54-63.
*   Pérez-Álvarez-Ossorio, J.R., Gómez, I. & Martín-Sempere, M.J. (1997). International visibility of domestic scientific literature. _Journal of Information Science_, **23**(1), 98-101.
*   Ren, S. & Rousseau, R. (2002). International visibility of Chinese scientific journals. _Scientometrics_, **53**(3), 389-405.
*   Román-Román, A. & Giménez-Toledo, E. (2010). Cómo valorar la internacionalidad de las revistas de Ciencias Humanas y su categorización en ERIH. [How to assess the internationality of the social sciences journals and their categorization in ERIH)] _Revista Española de Documentación Científica_, **33**(3), 341-377.
*   Torrado-Morales S., Giménez-Toledo, E. & Rodríguez-Yunta, L. (2010). Presencia de revistas cientificas en bases de datos internacionales como criterio de calidad. Problemas metodológicos: [Presence of scientific journals in international databases as a quality criterion. Methodological problems]. In Castro, A. & Guillén-Riquelme, A. (Eds.), _VII Foro sobre evaluación de la calidad de la investigación y la educación superior: libro de capítulos._ (pp. 420-425). Granada: Asociación Española de Psicología Conceptual.
*   Torres-Salinas, D., Bordons, M., Giménez-Toledo, E. Delgado-López-Cózar, E., Jiménez-Contreras, E. & Sanz-Casado, Elías. (2010). Clasificación integrada de revistas cientificas (CIRC): propuesta de categorización de las revistas de ciencias sociales y humanas. [Integrated classification of scientific journals (CIRC): categorization proposal for social sciences and humanities journals]. _El profesional de la información_, **19**(6), 675-683.