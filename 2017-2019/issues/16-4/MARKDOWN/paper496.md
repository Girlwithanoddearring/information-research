#### vol. 16 no. 4, December 2011

# Information needs in a community of reading specialists: what information needs say about contextual frameworks

#### [Lorraine Normore](#author)  
University of Tennessee, School of Information Sciences, 451 Communications Building 1345 Circle Park Drive, Knoxville, Tennessee

#### Abstract

> **Introduction.** The perceived information needs of teachers who specialize in reading instruction for at-risk first graders were studied and related to frameworks for the role of social context in information needs, seeking and use. The frameworks considered were: disciplinarity, role theory in work settings, small worlds and information grounds and communities of practice.  
> **Method.** Two focus groups of Reading Recovery teachers and teacher leaders were conducted to learn the kinds of information each group needed to support their teaching and outreach efforts.  
> **Analysis.** Participants' suggestions about information needs were gathered and prioritized using participant-generated votes to assign weights to the group's suggestions. The suggestions were organized for ease of interpretation into three groups: communication, specific skills or resource needs and personal professional development.  
> **Results.** The two groups showed both common and distinct information needs related to their roles as educators, their relationships with administrators, other teachers and parents and the social ties within their professional community. Communication needs, both within their group and to others, especially parents, were noted. Print and audiovisual materials were most valued. Traditionally accepted resource types (e.g., lesson plans, skill sheets) were mentioned, but were not highly valued.  
> **Conclusions.** Interpreting these data from the different theoretical frameworks provided alternative views of these data. However, doing so resulted in substantial differences in what was learned about the relationship between information needs and social context and about the ability of the frameworks to drive theoretical exploration. These findings enlarge our view of the information needs of professional educators and provide an illustration of the utility of contrasting frameworks for the development of theory.

## Introduction

If we are to improve our understanding the relationship between users and their information needs, we need to examine how the user has been conceptualized and to develop a rich enough model of the user to be useful for analysis and as a support for the design of information systems. One aspect of this model is the culture of the work environment. This paper examines the expressed information needs of a group of educators who are immersed in a programme for improving literacy skills among young children who are in the bottom five percent of the first grade population of beginning readers. Among the information needs that they identify are factors that are drawn from the types of information necessary for the development of skills and professional expertise within their community as well as factors that reflect the process of developing a sense of social identification as shown in their communication patterns. The analysis will begin with a brief history of some of the ways users have been conceptualized within the study of information needs, focusing on notions of the socio-cultural environment. It will then present a description of the group of educators who are the central characters in this analysis. Next it will describe the focus group studies that provide the content for this analysis. It will end with an examination of what the selected theoretical frameworks and the data reported herein tell us about the socio-cultural nature of the group.

## Literature review

### A brief history of conceptualizations of the user within library and information science.

Leckie ([2005](#lec05)) points out, that, in information science, early works looked at the effect of scholarly discipline as a major differentiating feature among users. This comment can be illustrated by inspecting the titles and content of the chapters of the first few issues of the _Annual Review of Information Science and Technology_, which defined the study of information needs and uses as the information practices characteristic of scientists and engineers and other academic disciplines (cf. [Menzel 1966](#men66); [Herner and Herner 1967](#her67); [Paisley 1968](#pai68); [Allen 1969](#all69); [Lipetz 1970](#lip70); [Crane 1971](#cra71)). More recently the tendency to look to scholarly discipline as the basis for separating users into groups has been referred to as 'domain analytic' ([Talja 2005](#tal05a); [Fry 2006](#fry06)), following the guidance of Hjørland and Albrechtsen who introduced the term in [1995](#hjo95).

Over the same time period, however, the study of user needs has expanded past disciplinarity under the influence of broader models for the information seeking process as a whole. Wilson's ideas as represented in Figure 3 of his classic [1981](#wil81) paper about the nature of the environment of information seeking will be used to illustrate this broader view. In this Figure, within a box labelled _environment_, Wilson includes a set of features that he labels _role_ and a group of features including _work environment_, _socio-cultural environment_, _politico-economic environment_ and _physical environment_. The _role_ group is further subdivided into _person_, _work role_ and _performance level_. In the years since the publication of this paper, various researchers have focused on two of the attributes Wilson associates with the person, namely cognitive and affective needs, the third being physiological needs. The cognitive dimension can be seen as a part of Dervin's focus on sense-making, first articulated in 1983 ([Dervin 2005](#der05)), in Belkin's anomalous state of knowledge concept, proposed in 1977 ([Belkin 2005](#bel05)) and in Ingwersen's ([1996](#ing96)) cognitive information retrieval theory. Affect is a key component in the development of Kuhlthau's information seeking process model ([Kuhlthau 2005](#kuh05)) and the focus of a book edited by Nahl and Bilal ([2007](#nah07)). Work on the socio-cultural environment and on work environment and work role has been pursued from a number of perspectives including the study of everyday information seeking, the study of information needs and uses within the professions and an evolutionary approach to information needs and behaviour (e.g., [Bates 2005](#bat05); [Spink and Cole 2006](#spi06); [Pirolli and Card 1999](#pir99)). Two of these approaches, everyday information seeking and information needs and uses within the professions will be examined in greater depth in this paper. Each has specific aspects which will help to provide a context for deepening our understanding of educators. The discussion will now turn to those perspectives.

### Information needs beyond academia: everyday information seeking in social contexts.

Savolainen ([2008](#sav08)) points out that the study of user needs and information seeking behaviour in non-work contexts has a rather long history, beginning with surveys of citizen information needs as far back as the 1970s. In the 1990s, he coined the phrase "everyday-life information seeking" to describe this field of study. While the focus of the present paper is not on information needs in non-work settings, the rich insight that this research area provides into the social context of information needs, behaviour and practices is central to the argument that will be presented. Let us look briefly at three well-known approaches to characterizing these contextual factors.

Chatman's ([1991](#cha91)) depiction of information seeking in the 'small worlds' of low income, working class populations provides a powerful alternative to the '_systematic information searches that go on in academic or workplace environments_' ([McKenzie 2003](#mck03): 20). Chatman ([1999](#cha99): 213) defined the _small world_ as '_a society in which mutual opinions and concerns are reflected by its members, a world in which language and customs bind its participants to a worldview_'. While much of Chatman's work has focused on the information needs and practices of groups who are on the margins of middle class society, some parts of the model provide valuable insight into other communities. For example, Burnett, Besant and Chatman ([2001](#bur01)) present a model that extends the small world concept to work and social contexts outside of academia, binding the earlier and later analyses through the concept of normative behaviour.

Fisher (formerly Pettigrew) and her collaborators elaborate on the notions of information habits and information grounds ([Pettigrew 1998](#pet98); Fisher _et al._ [2004](#fis04), [2005](#fis05); [Fisher and Naumer 2006](#fis06)). The concept _information grounds_ is built on a social constructionist foundation, focuses on community-based social settings and, like Chatman, on everyday life information sharing. Savolainen ([2009](#sav09): 45) argues that '_Small worlds and information grounds are physical and social spaces that. provide insightful views on the interplay of spatial and contextual factors shaping [everyday information seeking and sharing]_', showing both similarities and differences in his analysis.

McKenzie and her collaborators have taken a different approach to the social nature of information practices within a social constructionist paradigm in a series of different environments. Rooting much of the work in question in the use of discourse analysis, McKenzie ([2003](#mck03): 24) observes that '_The social constructionist paradigm puts emphasis on social practices... rather than on behavior [which] shifts the analysis from cognitive to social_'. In a later work, Prigoda and McKenzie add that moving to a collectivist perspective that '_understands information needs, seeking and use to be a part of or embedded in a cultural, social, or organizational practice_' (quoted from [Talja _et al._ 2005](#tal05b): 86) ([Prigoda and McKenzie 2007](#pri07): 91), extends the analysis beyond the view of individuals in their social context to focus on the process in the group as a whole.

### Information needs beyond academia: information seeking in a work setting.

As was noted at the beginning of this brief history, early studies of user information seeking and needs focused on the needs of academic researchers in the sciences and engineering. However, there has been a substantial literature on information seeking behaviour and practices in a variety of work situations, beginning with Wilson and Streatfield's ([1977](#wil77)) study of the information needs of local authority social services departments. The focus of much of this work has been on identifying information seeking behaviour, preferred information sources and changing practice in the face of increasing electronic information availability and on the emergence and changing role of the Internet over the last ten years. An issue with this research, noted by Hewins ([1990](#hew90): 145) is that most '_do not contribute to .theory of model building [and that] many others can best be described as site-specific, system-specific, or service-specific_'. There are, however, two approaches within this research area that propose models of more general applicability.

In a series of papers, Leckie and her collaborators ([Leckie and Pettigrew 1996](#lec96b); [Leckie _et al._ 1996](#lec96a)) discuss the extension of interest to the work of non-academic professionals. The model described by Leckie and Pettigrew ([1996](#lec96b)) is based on role theory, which Biddle ([1979](#bid79)) points out is widely used within the social sciences. Their model proposes that information needs emerge from the roles (e.g., service provider, manager) that professionals undertake as part of their daily work. Within these roles they identify specific tasks. Information seeking, they argue, is related to the '_enactment of a particular role and its associated tasks_' ([Leckie and Pettigrew 1996](#lec96b)) while the characteristics of that information need are influenced by a number of variables such as individual demographics and context, as well as knowledge and availability of sources.

Two features of the literature on work outside the academy should be noted. First, there is a preponderance of studies aimed at health care professionals: physicians, nurses, veterinarians, community health care workers and midwives, for example. In this subset of the literature, there is a substantial overlap between the information related behaviour (or sometimes practices) of the health care professional and the recipient (e.g., between a midwife and a pregnant woman). As a result, many of the more recent papers identify themselves as papers related to everyday-life information seeking. Examples of this include papers by McKenzie ([2002](#mck02), [2003](#mck03)) and by Fisher (formerly Pettigrew) and her collaborators (cf., Pettigrew, [1998](#pet98), [1999](#pet99); [Fisher _et al._ 2005](#fis05)). Second, in delineating this literature as the '_information seeking of professionals_' as Leckie and Pettigrew do, they limit the scope of coverage to individuals in

> service-oriented occupations having a theoretical knowledge base, requiring extensive postsecondary education, having a self-governing association and adhering to internally developed codes of ethics or other statements of principle ([Leckie _et al._ 1996](#lec96a): 162).

They go on to list included professional groups, which they identify as '_ministers, doctors, lawyers, computer scientists, administrators and social workers, to mention a few_' ([Leckie and Pettigrew 1996](#lec96b)). By so doing, they implicitly exclude studies of the technical information needs of other groups whose work may not meet the criteria above (e.g., people in business and retail, politicians, police officers, farmers, janitors, artists, actors and many others) which may provide additional breadth to a theory of work-oriented information needs.

A final model for the social context of information in work settings is drawn from a theory about the situated nature of learning ([Lave and Wenger 1991](#lav91)). It has been adapted to the study of workplace practice by Brown and Duguid ([1991](#bro91)). _Communities of practice_ has been used as a concept that enables us to represent better how professionals work together to exchange information and expertise to solve problems (e.g., [Cox 2005](#cox05)) and has become central to the study of implicit knowledge sharing within knowledge management circles ([Blair 2002](#bla02)). Four key components of this theory of social participation as a process of learning and knowing are: (1) meaning, (2) practice, (3) community and (4) identity ([Wenger 1998](#wen98)).

Having briefly identified a number of theoretical frameworks that might be used to describe and explain the behaviour of working professionals, this review will now turn to the literature on information needs and behaviour in the target group for this research, educators.

### Information needs and behaviour among educators.

In the literature reviewed earlier in this paper, few models have focused on the work of professionals in the field of education. This is noteworthy, given that _Education, training and library occupations_ is the largest sub-group among _Professional and related occupations_ according to Table 603 of the 2008 edition of U.S. Statistical Abstracts, larger even than _Healthcare practitioner and technical occupations_, groups that have received a good deal of study. Educators are quite interesting because their clients range from individuals in pre-school through those in post-secondary education and because the group contains both practitioners and academics. Of equal interest is the range of information sources used in this broadly defined community. Our attention will now turn to a review of the literature on information needs, uses and behaviour among educators.

### Educators and literature use.

In line with other studies that focus on literature use in professional groups, we find a set of studies which looks at information habits among educators. One of the earliest in-depth studies of educators ([Line _et al._ 1971](#lin71)) was part of a broader study of information requirements in the social sciences (INFROSS) in the UK. It looked most intensively at lecturers in colleges of education but also explored the information needs of a smaller group of schoolteachers. The researchers compared information needs for college lecturers engaged in teaching with those whose responsibilities included both teaching and research, noting both the kinds of information sought and the extent to which they used the primary and secondary literature and sought help from library and information services. They found substantial differences in the types and amount of information needed. Lecturers involved in research needed a larger number of references from the literature. Lecturers involved in teaching were interested only in a small number of specific pieces of information, since they had only a limited amount of time and motivation for information gathering. A smaller study described in the report also compared the information related behaviour of these academics with that of a small group of primary and secondary schoolteachers. Like the teaching lecturers, they were interested only in resources that required minimal effort to gather and that were directly relevant to their immediate needs.

More recent studies have continued the practice of looking at information use among educators. Rudner and his colleagues ([2002](#rud02)) studied the use of two relatively new online education journals that focus on education policy and assessment by North American teachers drawn from the population who provide instruction from kindergarten through the end of high school (often referred to as the K-12 period). They found the most frequent users were graduate students and faculty. Most of the use (~66%) was for class assignments and research reports, with less than nine per cent used for finding teaching resources. This pattern is very like that of academic populations from other social science and humanities disciplines. Given the content of the journals, it is not surprising that slightly more than half of the teacher population who accessed the resources identified themselves as teachers and administrators rather than as teachers alone. However, the use even by this population was comparatively low.

Other studies have focused on the use of secondary literature sources. Rudner and colleagues ([Rudner 2000](#rud00); [Hertzberg and Rudner 1999](#her99)) looked at the use of the ERIC database, a key resource for educators. The leading user groups were graduate students, researchers, undergraduates and college professors, in that order. K-12 teachers accounted for only 19% of the searches and K-12 administrators a mere 3.5%. They observed that research report preparation and class assignments accounted for 70% of the items retrieved. Lesson planning and classroom management together totalled less than eight per cent. These differences become more noteworthy when the relative numbers of individuals in each of these groups is taken into account. A slightly more contemporary estimate ([McDowell 2001](#mcd01)) suggested that the number of K-12 teachers in the United States was 3,000,000\. Other statistics, quoted by Rudner, estimated that there were 986,000 education undergraduates, 604,000 education graduate students and 89,000 professors in American colleges of education at approximately the same time period. Thus, practitioners (the largest group) used ERIC much less frequently than college level students and professors. It is unlikely that teachers do not need information in order to be effective. Why then is their usage of the standard education literature so low? One possible explanation is that the richness of the ERIC database and the complexity of the search system are overwhelming to many teachers. When Rudner ([2000](#rud00); [Hertzberg and Rudner 1999](#her99)) examined search results from ERIC, they found that searches were not very successful, in part because the queries were very unsophisticated. Overly simplistic searches are an especially difficult problem in a large database like ERIC. In such databases, infrequent users lack the ability to create precise queries that bring them relevant documents and that do not eliminate many potentially interesting documents. This may in turn decrease the likelihood that these individuals will continue to turn to this source.

### Educators and Internet-based sources.

Along with the growth of general Internet use since 1993, studies of educators have also focused on the use of resources available through the internet. The internet is a heterogeneous information resource. Our discussion will look first at studies of general Internet use, then at the use of specialized educator resources and finally at the use of digital libraries.

Relatively early in this period, Honey and McMillan ([1996](#hon96)) conducted a series of interviews with experienced educators who used computers and the Internet for a range of instructional activities. The participants said that they found the databases, reference materials and discussion groups less beneficial than they had hoped and often not appropriate to the needs of educators. They expressed concern about the need for coordinated training and support for teachers and for the development of instructional tools. This theme is echoed throughout the period. In discussing literacy instruction in K-12 classrooms, Karchmer ([2001](#kar01)) identified several reasons why pre-service teachers might not use Internet resources in their classroom, including both the belief that educational technology was irrelevant given their own childhood and classroom expectations and the lack of training in Internet use in their teacher education and staff development experiences. Carlson and Reidy ([2004](#car04)) studied the use of digital resources among a group of high school science, technology, engineering and mathematics teachers. They found that while all of the respondents use Websites or electronic documents for background information during curriculum planning, 63% spend less than 25% of their instructional time incorporating Web-based resources into their instruction.

As late as 2007, [Williams and Coles](#wil07) surveyed a broad sample of educators in the United Kingdom about their confidence about seeking and using both general and research oriented information online. Teachers were relatively confident about finding and using general information. However, this varied with school level and size, with secondary and middle school teachers and those in larger schools expressing a greater degree of confidence in their information-related abilities than those in primary and smaller schools. In a follow-on interview study mentioned in the same article, the authors note that '_a number of interviewees admitted to problems .in defining a search strategy or knowing where to start_' ([Williams and Coles 2007](#wil07): 195). In the list of sources used for subject-related research ([Williams and Coles 2007](#wil07), Table 1), the Internet was only the seventh source to be rated as being regularly used, following such other sources as discussions with colleagues, professional magazines and mainstream newspapers.

While the findings from activities like the Speak Up National Research Project ([2009](#spe09)), suggest that students themselves are '_early adopters and adapters of new technologies_', it is noteworthy that the teachers surveyed still see access to computer resources and to the Internet as obstacles to adoption and state that their desired hardware choices are laptops, interactive whiteboards and computer projection devices, rather than the mobile devices that form the top choices of their students. This potential lag is echoed in the results of a study by Sveum ([2010](#sve10)) who found that freshmen and even senior education majors showed marginal computing and Internet technology skills and knowledge. Probert ([2009](#pro09)) suggested that she and others have found that teachers within the New Zealand schools were frequently not explicitly teaching information literacy skills even when they themselves were familiar with the concept. Thus, it can be argued the need for information literacy skills encompasses both traditional formal sources and the Internet.

As suggested by the studies discussed in the preceding paragraph, a number of efforts have been made to develop instructionally appropriate Websites and digital collections within this period in an effort to provide materials that can be used quickly and effectively in instruction. This overview will focus on a small number of studies that have been done examining the use of selected sites and services. One of the first large-scale efforts to provide a wide range of resources for educators in the Web is known as the Gateway to Educational Materials or GEM. In a related effort at Syracuse University, one of the institutions involved in the creation of GEM, we find the development of a digital reference source for primary and secondary education ([Lankes 2003](#lan03)). The success of GEM was evaluated in a set of studies by Fitzgerald, Lovin and Branch ([2003](#fit03)). GEM was built on carefully selected educational resources available on the Internet and designed to help teachers find and use these resources. However, the results of the evaluation showed many of the features noted above: most participants lacked sufficient search skills to effectively locate resources within the Gateway and many required training and support from information professionals, including school media specialists.

A number of studies have looked at the development of digital libraries that can support instruction. Digital libraries are especially interesting educational resources since they often contain either or both rich multi-media objects and representations of source documents that make significant cultural objects available to a broad audience. Kuhlthau ([1997](#kuh97): 708) pointed out that this makes them a desirable part of the educational experience from a constructivist approach to '_information age learning environments_'. In a series of studies, Recker and her associates ([Recker _et al._ 2004](#rec04), [2005](#rec05); [Recker 2006](#rec06)) have explored the development of _learning objects_ to be used within the National Science Digital Library. They found that participants were enthusiastic about creating and using small elements of information resources (such as individual PowerPoint slides, rather than a whole presentation) to enrich their lessons but that usage was lower than had been expected. Barriers to use included previously mentioned problems with access to technology and information literacy. Perrault ([2007](#per07)) studied how biology teachers perceived their online search processes and the influences that these had on the way they executed their instructional planning. Her findings were based on both a survey and a follow-on interview study of biology teachers in New York State. They showed that these teachers were using more and a greater variety of current and multi-modal online instructional tools than they had in the period before the Internet. However, they also indicated that teachers were not using key educational online resources, specifically digital libraries, online periodical databases and electronic discussion lists, citing a lack of time to find and use resources as a critical factor limiting that use. This is especially disappointing given the number of well-funded projects designed to make scientific resources available for instructional use.

A second content focus within digitized collections is on materials of historic and cultural importance. Pattuelli ([2008](#pat08)) conducted a study of teachers' information seeking behaviour and practices in the use of the primary source documents available through the _Documenting the American South (DocSouth)_ collection. She looked at the information seeking goals of a group of middle and high school social studies teachers and reports on both usability studies and a series of focus groups. While these teachers are strongly committed to discovery-based learning, they experienced frustrations in trying to incorporate these materials into their students' learning activities. They showed a general lack of interest in pre-arranged educational content, preferring to construct their own exercises but recognized the burden that such activities place on their time, in a context in which they are required to meet the burden that standardized testing places upon their classroom activities. They also spoke of general difficulties locating materials, noting the frustrations associated with the long lists of unrelated materials they find when using keyword searching on Google, their primary search tool.

### What have we learned about information seeking among educators?

It is particularly important that we know about information seeking among educators, not only because of their role as professionals whose work intimately involves information, but also because they are shapers of the social practices associated with information seeking that they inculcate in their students ([Lundh and Limberg 2008](#lun08); [Tuominen _et al._ 2005](#tuo05)). What then has been learned about teachers' information needs, seeking and behaviour in this literature review?

The data suggest that the primary users of the formal literature in education are academics, which could be expected because it is the academy that provides the richest access to databases and their associated journal literature. Even in this audience, however, there is evidence of a low level of use, due primarily to a lack of search skills. The availability of information on the Internet might be thought to level access differences. It does appear to ignite interest and enthusiasm for Internet resources among educators, as does the availability of specialized educator-oriented Websites that contain curriculum resources and digital libraries that provide both interpretation and primary source materials. However, the observed limitations on the utility of these resources because of low levels of search skills should not be ignored by programmes that train preservice educators and by professional development suppliers.

A final note: if these findings are viewed from the perspective of the theoretical frameworks that introduced this literature review, it is interesting to note that the predominant approach is the _traditional_ resource-oriented one found in the early domain-analytic studies: researchers tended to choose an information resource and then to study the pattern of use associated with that resource. Only a small number of studies, limited for the most part to the study of specialized educational information resources and digital libraries, have focused on the social context of use, looking to identify the way in which information needs and use relate to instructional needs. It remains an open question as to whether the information needs that practicing teachers experience are those examined in these studies. It is the purpose of this research project to gather data on this open question.

## The studies

### Research questions.

Two research questions are addressed in these studies:

*   What information do teachers need to do their jobs?
*   Are their needs the same or different from those of the academic community and those shown in the literature surveyed above?

The groups studied come from a community of reading specialists who use a teaching method called Reading Recovery. As we shall see in the discussion that follows, Reading Recovery is a focused programme within the early literacy community, a focus that will be shown to influence their information needs. The study method is non-directive: participants were not asked to report on or to evaluate the information systems they used at the time. Rather they were asked to identify the types of information that would be useful to them and to rate the importance of the information types that emerged from the group discussion.

### Reading Recovery: an introduction.

Reading Recovery is a method for teaching young children experiencing reading difficulties that helps many of them to achieve grade-level-appropriate reading performance. Begun by Dr. Marie Clay in New Zealand over 30 years ago, Reading Recovery is widely practiced in North America, Australia and New Zealand, as well as in the United Kingdom. Reading Recovery is a specialized one-on-one intervention programme and works within affiliated school districts. It is taught to highly qualified, experienced elementary school teachers by a group called Teacher Leaders. Teacher leaders are Reading Recovery teachers who have succeeded in a post-graduate course at established university training centres by faculty members who are referred to as Trainers. Teacher Leaders "are leaders in their local districts where they teach children, train Reading Recovery teachers, maintain contact with past trainees, analyze and report student outcomes, educate local educators, advocate for what cannot be compromised and communicate with the public." [Reading Recovery... 2011](#read).

Teacher leaders become the key link in the process whereby Reading Recovery teachers are trained. Teacher training is carried out as a professional development activity within a local area and involves a full academic year of professional development activities under a registered teacher leader. The teacher thus trained then must follow up with a prescribed set of additional professional development activities. Reading Recovery training integrates both theoretical and practical activities, designed to support reflective practice within the individual teacher and in their training group. This research project studied the expressed information needs of groups of Reading Recovery teachers and Reading Recovery teacher leaders.

### Method

Two focus groups were used to collect data on the research questions. One group was composed of Reading Recovery teachers, the second of Reading Recovery teacher leaders. Focus groups were chosen as the desired data gathering methodology because this is a known and widely favoured method for gaining information about opinions and attitudes about products and services ([Rea and Parker 1997](#rea97)). In particular, they are frequently used to derive basic information about perceptions about habits and usage, especially when the researcher is looking for preliminary data to help understand how and why services are or are not used ([Greenbaum 1993](#gre93)). They have also been frequently used as a method of choice within the study of information seeking and use ([Young and Von Saggern 2001](#you01)).

Participants were invited to attend through a randomly selected e-mail solicitation from the individuals registered for the two conferences identified below. The first fifteen individuals who responded to the solicitation were invited to attend. All participants were compensated for their time.

Thirteen teachers who were attending the National Conference of the Reading Recovery Council of North America in 2005 met for two hours to discuss issues concerning their information needs. After a round of questions designed to introduce the group members to each other, the group was asked to take a few minutes to create jot lists containing the types of information that they need in order to help them with their work. The researcher who acted as focus group facilitator then polled the group for situations that '_cause you to go and look for information of any kind_'. Suggestions were written on flip charts until all ideas had been solicited. Consistent with good focus group methodology, all flip chart content used the exact words of the participants, agreed to by each speaker if any modifications were made for clarification. The flip charts were put up on the walls of the meeting room. Group members were each allocated eight points and were asked to go to the flip charts and to assign eight points to the suggestions recorded there. Using a variant on a plurality voting system, they were allowed to distribute their votes either evenly to their top eight information needs or to give multiple votes to any of those items, based on their beliefs about the importance of those items. Because of technical constraints and as an added measure to protect the privacy of the participants, the sessions were not audio-recorded.

A second group was composed of thirteen teacher leader volunteers who were attending the 2005 North American Leadership Academy and Teacher Leader Institute which is the primary annual professional development activity for teacher leaders. The same focus group method and voting system was used for both groups.

## Results

### Teacher focus group.

The items that were generated in the teachers' focus group discussion appear as items in the first column of Table 1\. In the second column the number of votes associated with each item is given. The participants identified 30 different types of needed information. In Table 1, the list is divided into three major content groupings for ease of interpretation.

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Expressed information needs for teacher focus group and number of votes for each need.**</caption>

<tbody>

<tr>

<th>Information need</th>

<th>No. of votes</th>

</tr>

<tr>

<td colspan="2">**Commnication**</td>

</tr>

<tr>

<td>Research about the value of what we do</td>

<td align="center">3</td>

</tr>

<tr>

<td>Ways to communicate with administration</td>

<td align="center">1</td>

</tr>

<tr>

<td>Ways to gain access to policymakers</td>

<td align="center">1</td>

</tr>

<tr>

<td>Ways to communicate with policymakers</td>

<td align="center">1</td>

</tr>

<tr>

<td>Ways to communicate with parents</td>

<td align="center">12</td>

</tr>

<tr>

<td>Communicating techniques among teachers</td>

<td align="center">1</td>

</tr>

<tr>

<td>Discussion forum</td>

<td align="center">4</td>

</tr>

<tr>

<td>"Ask an expert" service</td>

<td align="center">12</td>

</tr>

<tr>

<td colspan="2">**Specific skill or resource related**</td>

</tr>

<tr>

<td colspan="2">_Skills_</td>

</tr>

<tr>

<td>Ways to approach teaching (techniques, strategies, skills)</td>

<td align="center">0</td>

</tr>

<tr>

<td>Ways to help struggling students/ways to teach</td>

<td align="center">15</td>

</tr>

<tr>

<td>Help for children with special needs (vision, hearing, etc.)</td>

<td align="center">0</td>

</tr>

<tr>

<td>Video clips of classroom activities/instructional methods</td>

<td align="center">13</td>

</tr>

<tr>

<td>Behaviour of model teachers</td>

<td align="center">0</td>

</tr>

<tr>

<td colspan="2">_Resources_</td>

</tr>

<tr>

<td>Enrichment activities (tested)</td>

<td align="center">4</td>

</tr>

<tr>

<td>Support for thematic units (resources, activities)</td>

<td align="center">2</td>

</tr>

<tr>

<td>Lesson plans</td>

<td align="center">0</td>

</tr>

<tr>

<td>Strategic skill sheets</td>

<td align="center">2</td>

</tr>

<tr>

<td>Links to good resources</td>

<td align="center">7</td>

</tr>

<tr>

<td>Child-friendly sites</td>

<td align="center">2</td>

</tr>

<tr>

<td>"Safe" sites for kids</td>

<td align="center">0</td>

</tr>

<tr>

<td>Book recommendations (from teachers)</td>

<td align="center">7</td>

</tr>

<tr>

<td colspan="2">**Personal professional development**</td>

</tr>

<tr>

<td>Information in research articles</td>

<td align="center">6</td>

</tr>

<tr>

<td>Works by Reading Recovery trainers and affiliates</td>

<td align="center">5</td>

</tr>

<tr>

<td>Theses</td>

<td align="center">0</td>

</tr>

<tr>

<td>Grant information availability</td>

<td align="center">2</td>

</tr>

<tr>

<td>Model reports</td>

<td align="center">1</td>

</tr>

<tr>

<td>Reports on successful implemented projects</td>

<td align="center">2</td>

</tr>

<tr>

<td>'Closing the Gap' information</td>

<td align="center">2</td>

</tr>

<tr>

<td>Standards for reporting</td>

<td align="center">0</td>

</tr>

<tr>

<td>Methods for teaching standards</td>

<td align="center">1</td>

</tr>

</tbody>

</table>

The first content set reflects an expressed need for communication support. The group expressed the need to get information about the value of what they do in providing a programme aimed at developing successful readers. Specifically they wanted to get that information in a form that it could be shared with administrators, policy makers and parents. They also felt the need for ways to communicate their skills more effectively to the larger community of classroom teachers. They suggested that they would benefit from communication tools that would enable them to share experiences through discussion forums and to be able to ask questions of experts in the field.

The second content set concerned the need for support for the acquisition of specific, instruction-related skills and for resources that support their teaching. The list included some expected items (lesson plans, support for thematic units, enrichment activities and book and Website recommendations). However, it also included requests for some classroom support materials that are not necessarily text or literature based. These include materials designed to more effectively communicate information about specific skills or techniques. The group was very enthusiastic about the potential use of video clips that show instructional techniques in a way that is direct and that fosters skill transfer more effectively than text-based description. They were also interested in tools that could help reach out to special needs students in their classrooms, providing alternative ways to access learning materials.

The third content set centred on materials which support the continued professional development of early literacy teachers. Three quite different professional goals could be identified in the information requested. First, they expressed the need for continued information about new research in their professional field, including research articles, works by experts in the field and new theses and dissertations. Second, they wanted to be able to get information that would support their own research work. They requested information about grants, including both availability and support materials to aid them in grant preparation. Third, they were interested in getting information that would help them in their own data collection, analysis and reporting activities.

While we see many expected information sources listed, the numeric weightings assigned in the course of the voting activity also shows some surprises. Resources that have often been the focus of efforts to create tools to aid teachers like lesson plans, support for thematic units, strategic skill sheets and safe Websites for children received very few votes (0-2 per item). Participants appeared to be much more interested in focused information sources (e.g., ways to help struggling students) than in getting general updates on new teaching methods. Alternative media (video clips) were more highly prized than traditional print sources. Print is clearly still of at least moderate interest. The group was somewhat interested in book recommendations (although they wanted recommendations from other teachers rather than from book editors or librarians) and research articles both from the education literature and specifically from their own more narrowly defined professional group, along with a request for a discussion forum among themselves and, more strongly, for asking questions of experts in the field. Of equal interest to their need to communicate within their own group is their expressed need to communicate out to parents. Taken together, these information needs demonstrate a sense that this is a practitioner community rather than a simple collection of unrelated educators.

### Teacher leader focus group.

Teacher leaders form an interesting group as educators: they commonly begin their work in the field of education as classroom teachers, but, in becoming teacher leaders, have taken on the additional task of teaching not only primary school students but also teachers. Seeing how this additional responsibility will affect their information needs and practices is a motivating factor in this part of the data collection.

The items generated in the teacher leaders' focus group discussion appear as items in the first column of Table 2\. In the second column the number of votes associated with each item is given. The group identified twenty-four different types of information needs. Below, we see the list divided into the same three major content groups used in the analysis of the teachers' focus group for ease of interpretation and comparison.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Expressed information needs for teacher leader focus group and number of votes for each need.**</caption>

<tbody>

<tr>

<th>Information need</th>

<th>No. of votes</th>

</tr>

<tr>

<td colspan="2">**Commnication**</td>

</tr>

<tr>

<td>Research findings, for others (other teachers, policy makers)</td>

<td align="center">11</td>

</tr>

<tr>

<td>Methods to report to parents that relate to changing models from educators</td>

<td align="center">0</td>

</tr>

<tr>

<td colspan="2">**Specific skill- or resource-related**</td>

</tr>

<tr>

<td colspan="2">_Leading teachers_</td>

</tr>

<tr>

<td>New programmes (Pros and cons)</td>

<td align="center">10</td>

</tr>

<tr>

<td>Needs for staff development programmes and activities that have sustained effects</td>

<td align="center">12</td>

</tr>

<tr>

<td>Ways to identify "experts" for speakers, consultants, e.g., like Consumer Reports' information or peer-reviewed</td>

<td align="center">5</td>

</tr>

<tr>

<td colspan="2">_On the job_</td>

</tr>

<tr>

<td>Ways to help special needs kids and kids in special circumstances: information related to the special need or circumstance</td>

<td align="center">7</td>

</tr>

<tr>

<td>Levels: how does levelling work? How do different levelling systems compare (needed for teachers, administrators and parents)</td>

<td align="center">0</td>

</tr>

<tr>

<td>Language delayed kids</td>

<td align="center">0</td>

</tr>

<tr>

<td>Materials in different languages</td>

<td align="center">1</td>

</tr>

<tr>

<td colspan="2">_For parents_</td>

</tr>

<tr>

<td>Parent activities and literacy activities for families; ideas from other districts</td>

<td align="center">3</td>

</tr>

<tr>

<td>Ways to help parents with emergent literacy (pre-kindergarden)</td>

<td align="center">2</td>

</tr>

<tr>

<td>Ways for parents to help children even if the parents cannot read</td>

<td align="center">1</td>

</tr>

<tr>

<td>Ways to help inexperienced parents</td>

<td align="center">0</td>

</tr>

<tr>

<td>Supporting parents: available information for parents that is up to date, "good" sellers, publicly available in libraries</td>

<td align="center">6</td>

</tr>

<tr>

<td colspan="2">_Recommended books for others_</td>

</tr>

<tr>

<td>Recommended reading for teachers, administrators</td>

<td align="center">0</td>

</tr>

<tr>

<td>Recommended books for classroom literacy libraries</td>

<td align="center">2</td>

</tr>

<tr>

<td>To help teachers work with English language learners: for kids</td>

<td align="center">1</td>

</tr>

<tr>

<td>Lists of other books appropriate for kids at different levels; also categorized by interest topic</td>

<td align="center">8</td>

</tr>

<tr>

<td>Children's books that reflect different social situations that are or are not "traditional"</td>

<td align="center">2</td>

</tr>

<tr>

<td>Out of print books (RR Booklist) or soon to be out of print (publishers' information)</td>

<td align="center">4</td>

</tr>

<tr>

<td colspan="2">**Personal professional development**</td>

</tr>

<tr>

<td>Recommended professional books ("best for topic")</td>

<td align="center">11</td>

</tr>

<tr>

<td>Recommended **recent** books</td>

<td align="center">2</td>

</tr>

<tr>

<td>Dissertations (especially Reading Recovery related)</td>

<td align="center">9</td>

</tr>

<tr>

<td>Definitions of "research based", "research", justification for practice standards; how to evaluate research</td>

<td align="center">4</td>

</tr>

<tr>

<td>Glossary</td>

<td align="center">2</td>

</tr>

</tbody>

</table>

The Communication-related group for the teacher leaders includes outreach to several of the groups also shown in the teachers' focus group. Specifically, teacher leaders were interested in research findings that they could use as communication vehicles directed towards policy makers as well as other teachers and parents.

The second group again focuses specific skill and resource related information. In this group, we see four content sub-groups, three related to skills acquisition and one related to recommended readings. The first centres on information needs that derive from the teacher leaders' role in providing information to support the professional development of the teachers they are responsible for. Two items related to the identification of effective new programmes, especially those with sustained effects, were highly rated. Teacher leaders' responsibilities in support of professional development activities led to a new set of information types. This type would be comprised of lists of existing staff development programmes, including speakers and other consultants. The teacher leaders expressed a need to go beyond advertising copy, aiming at reviews by peers or some other unbiased third-party sources; a need that could potentially be supported by social networking applications.

The second group is further subdivided into specific content needs related to other groups with whom the teacher leader works. The group identified a set of practical, on-the-job information needs associated with their work with students with special needs. The specific kinds of information are derived from the students' personal and family situations and from their linguistic and socio-economic communities of origin. While listed, these items received few votes.

Like teachers, the teacher leaders also identified the need for a variety of materials to support the role that parents play in helping their at-risk readers. This included an interest in suggestion for parent-child activities and for freely available information sources. They also discussed the need for ways to deal better with 'special need' parents, i.e., those who be inexperienced or even those who are themselves not literate.

The next and last subgroup within this category includes the types of book recommendations they proposed. They sought recommendations for books and readings designed for audiences other than themselves; for classroom teachers and administrators, in particular. The teacher leaders were also interested in recommendations for children's books in different languages and content areas both for themselves and for their students. They suggested that it would be useful if the books could be organized not only by level but by content type. Resources for parents were also of interest.

The final group reflects information needs and sources related to the professional development of the individual teacher leader. They wanted access to high quality information, including new, highly rated professional works and access to dissertations in their research area. Several of the teacher leaders were actively engaged in research projects, often associated with work towards advanced degrees. They expressed an interest in materials that provide guidance by defining and evaluating the rapidly evolving technical vocabulary of the field, such as a glossary or FAQ file.

To aid in understanding the implications of the votes for the teacher leader group, the data in Table 2 were extracted and are presented in Table 3\. The number of votes per item is aggregated into categories that bring together the data to aid interpretation and the distribution within sub-categories for the specific skill- or resource-related category is revealed. As Table 3 shows, there are similar patterns across all categories, with the largest number of votes being associated with a small number of items and a large number of items receiving proportionately fewer votes.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: The distribution of votes by category for teacher leaders**</caption>

<tbody>

<tr>

<th rowspan="2" valign="middle">Type of information need</th>

<th colspan="4">Number of votes per category</th>

</tr>

<tr>

<th>0-1</th>

<th>2-5</th>

<th>6-9</th>

<th>10+</th>

</tr>

<tr>

<td>**Communication**</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">1</td>

</tr>

<tr>

<td>**Specific skill or resource related**</td>

<td align="center">6</td>

<td align="center">6</td>

<td align="center">3</td>

<td align="center">2</td>

</tr>

<tr>

<td>Leading teachers</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">2</td>

</tr>

<tr>

<td>On the job</td>

<td align="center">3</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

</tr>

<tr>

<td>For parents</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">1</td>

<td align="center">0</td>

</tr>

<tr>

<td>Recommended books for others</td>

<td align="center">2</td>

<td align="center">3</td>

<td align="center">1</td>

<td align="center">0</td>

</tr>

<tr>

<td>**Personal professional development**</td>

<td align="center">0</td>

<td align="center">3</td>

<td align="center">1</td>

<td align="center">1</td>

</tr>

</tbody>

</table>

These data are consonant with the knowledge that the job responsibilities associated with this group are in support of professional development. The votes for the personal professional development category are more heavily weighted towards the larger number of votes than is the equivalent item in the teachers group. Within the skill and resource category, we see that votes for those related to _leading teachers_ are similarly weighted towards the high vote categories.

## Discussion

### Relating these results to the literature on information needs and behaviour among educators

The overwhelming impression from the literature on educators' information needs and behaviour is that educators experience difficulties finding relevant information in databases and on the Internet. It can be argued this conclusion derives from the way this literature focuses on resource use rather than assessing information needs. When asked directly to identify needs, the teachers and teacher leaders in this study did not request instruction on how to search or even more user-friendly search systems. While there were requests for research articles, it should be noted that these groups do not customarily have access to the research-rich resources of the academic in education departments or even of university students. Indeed, many of the requests for traditional media were for books for themselves and for others. They were particularly concerned with resources generated by their own intellectual community both as the creator of the information and as the source of recommendations rather than for materials from the peer-reviewed literature.

There was surprisingly little interest in freely available digital resources in selected Internet sites and digital libraries. While lesson plans, support for thematic units, strategic skill sheets and safe Websites for children were all mentioned, they received very few votes as important sources. Several factors might cause this effect. These groups focus on early literacy instruction for a young student population (from kindergarden to grade 2). The digital library studies mentioned in the literature review (e.g., [Recker 2006](#rec06); [Perrault 2007](#per07); [Pattuelli 2008](#pat08)) looked at older groups' use of collections of materials related to science and history. However, those same studies may also provide insight into why these kinds of resources are not sought by the teacher groups in the present study. A theme that can be seen in a number of the studies reviewed is the importance of the match between the desired information unit sought by a lesson planner and that of the resource. The need for individual items of information, rather than broadly-based lesson plans, is noted in the work of Recker ([2006](#rec06)), Perrault ([2007](#per07)) and Pattuelli ([2008](#pat08))

A final factor that brings together the present study and previous research is the agreement over the kinds of preferred information sources. Rather than look to peer-reviewed sources or the Internet, both the Reading Recovery teachers and teacher leaders along with the teachers studied by Williams and Coles ([2007](#wil07)) look to colleagues, to recommendations from peers and others in their professional specialty as preferred information sources.

### Relating these results to frameworks that theorise about the user in context in library and information science

The role of theory in any discipline is at least two-fold. It serves as a mechanism for bringing together different research findings and for allowing us to predict future research findings. Second, it provides a framework for understanding, guiding our interpretation of the literature. These data present an interesting case study when viewed from the perspective of different approaches to the ways social context is defined and studied as the role of the user is conceptualized within the library and information science literature. The four theoretical frameworks introduced early in this paper present us with different views of what these data mean and how they should be interpreted. These differing interpretations suggest the importance of looking critically at theoretical frameworks and the role they play in the development of our literature. Specifically, we will now look at the varying insights provided about the data collected here from four perspectives on the social context of information seeking: disciplinarity and resource use, role expectations, information worlds amd information grounds, and communities of practice.

### Disciplinarity and resource use as a context model for these data

This paper began with a look at the earliest approach to looking at the social context of the user, namely at the effect of scholarly discipline as a major differentiating feature among users ([Leckie 2005](#lec05)). As this viewpoint developed, the approach evolved into the study of the use of various information resources (journal, databases, e-journals, etc.) and the ways disciplines and technical communities differ in their usage of these resources. This approach has been used within the discipline of education that is the focus of this study and is reviewed earlier in this paper. The limits of this theoretical framework are explored in the discussion of the relationship between the literature and the present results. To reiterate, the resource use approach selects a resource and studies its use. Reliable data is generated. A literature is evolved. However, there is little focus on the value that users place on the resources outside the studies themselves. As noted above, the teachers recognized the potential for some frequently studied information resources (e.g., lesson plans, digital libraries, the research literature in general) but did not value those resources as highly as they did more tightly focused resources from within their own communities and less traditional information sources, like video clips, an ask-an-expert service and internally generated programme evaluations.

### Role theory in work settings as a context model for these data

Biddle's ([1979](#bid79): 56) work on role theory points out that, "the role concept centres on behaviors that are characteristic of persons in a context". In exploring the concept of role in the context of information seeking in professional work, Leckie and her collaborators (e.g., [Leckie _et al._ 1996](#lec96a)) identified four significant themes. First, it is necessary to look at the broader working context of professional practice and not only the provision of service or expertise. Second, the individuals' work must be examined in depth. Third, professional work involves complex responsibilities drawn from different dimensions (e.g., technical, managerial, etc.). Finally, a variety of forces affect the ease with which professionals can find needed information, resulting in complexity, unpredictability and frustration in the information seeking process.

Let us see how this relates to the observed information needs in our two groups of educators. These two groups, while alike in many ways, differ in the way their activities reflect the part of their work practice that separates them. Teachers who work in the schools have as their primary practice the imparting of instruction to children. Teacher leaders, working in their role as teacher trainers, deal primarily with teachers, rather than children. Reading Recovery teachers look for methods of improving their teaching skills. Teacher leaders look for programmes and experts or consultants that can be used to improve teacher training. Both groups look outside their immediate work practice, seeking materials to aid them in communicating with the broader community of parent groups, other teachers, administrators and policy makers. The teacher group spoke not only of resources to support everyday needs but also for information to aid in data gathering, report generation and grant-writing to support the development of innovative school programmes, showing the multi-dimensional aspects of their work. In addition, data drawn from parts of the focus group not discussed in this analysis included discussions concerning the lack of access to the formal literature, to data on evaluation from peers rather than vendors and information on government programmes, resources which would, in theory, provide a way to improve instructional programmes but which would also make the process less frustrating and unpredictable.

### Small worlds and information grounds as a context model for these data

As Savolainen ([2009](#sav09)) argues, the context models provided by Chatman ([1991](#cha91)) as _small worlds_ and by Fisher and her associates (e.g., [Fisher and Naumer 2006](#fis06)) as _information grounds_, can be compared along social and spatial dimensions. Both approaches have become important models for the role of context in everyday life information seeking. As argued in the introductory literature review, however, they are also used as models for user behaviour and information seeking in situations involving coherent occupations (janitors, booksellers, nurses, beauty shop operators). In some important ways, there are similarities between these groups and the teachers and teacher leaders that are the subjects of the present analysis. Like the inhabitants of small worlds, the teachers' group looks preferentially to others within their community. They express an interest in works by Reading Recovery trainers and associated researchers, including theses and, in the case of teacher leaders, Reading Recovery-related dissertations. They seek input from other Reading Recovery professionals through discussion boards, ask-an-expert services and video clips that show good teaching practice. In many ways they constitute '_local and often small-scale communities.in [which] everyday information seeking and sharing is oriented by generally recognized norms based on beliefs shared by the members of the community_' ([Savolainen 2009](#sav09): 40).

The training programme for Reading Recovery teachers reflects a social and spatial environment like information grounds. Teachers are trained in groups that come from spatially coherent school districts. Groups are trained together in an environment that includes both theoretical and hands-on instruction. The bond between teachers and teacher leaders most often continues beyond the initial training session as teacher leaders, resident in local school districts, provide continuing education and support for past trainees. Reading Recovery teachers must attend state and national Reading Recovery conferences as part of their commitment to the programme. The atmosphere '_fosters the spontaneous and serendipitous sharing of information_' ([Pettigrew 1999](#pet99): 811), like an information ground, rather than as part of a more formal and less social information support system.

### Communities of practice as a context model for these data

The community of practice model has been used most extensively to provide a deeper dimension to the analysis of the activities involved in information sharing and learning (especially apprenticeship) within organizations (e.g., [Wenger 1998](#wen98); [Brown and Duguid 1991](#bro91)). While Reading Recovery practitioners do not practice within this kind of institution, they are not only a professional association concerned with teaching and learning an approach to literacy instruction but also a group that has a strong sense of community. According to Wenger ([1998](#wen98)), two features that characterize a community of practice are that they reflect theories of social practice and theories of identity.

Theories of social practice, Wenger states,

> address the production and reproduction of specific ways of engaging with the world. They are concerned with everyday life and real-life settings, but with an emphasis on the social systems of shared resources by which groups organize and coordinate their activities, mutual relationships and interpretation of the world ([Wenger 1988](#wen98): 13).

Both teachers and teacher leaders operate within a working educational environment but also seek information that validates and extends their approach to education. They seek research that supports the value of their work and research by Reading Recovery educators and in new theses and dissertations focusing on their practice. They ask for discussion boards and FAQs to help members of the community to more effectively share ideas. The teachers look for instructional materials incorporated in video clips that teach skills that this community believes to be critical in aiding at-risk readers to overcome reading difficulties. Through these, they look to organize and coordinate their approach to literacy instruction and to develop a common practice and interpretation of learner needs.

Further, Wenger states, a community of practice exhibits a theory of identity. By this, he argues that such a community is

> concerned with the social formation of the person...and the creation and use of markers of membership such as...social categories. They...attempt to understand the person as formed through complex relations of mutual constitution between individuals and groups ([Wenger 1988](#wen98): 13).

The process by which teachers become Reading Recovery specialists is not simply one in which skills are learned but also one in which teachers come to exhibit the behaviour and attitudes appropriate to this approach to early literacy instruction. Reading Recovery as a programme is adopted by school districts. Reading Recovery teachers spend part of their days in one-on-one instruction with students who experience reading difficulty rather than in a regular classroom setting. Thus, the emphasis seen in the data from both teachers and teacher leaders about the importance of ways to communicate outward, to administrators and policy makers, to other teachers and to parents signifies the extent to which these literacy specialists see themselves as a coherent group with a mission.

## Conclusions

Historically, it has been very common to use disciplinarity and resource use as a framework for theorizing about information needs. Much of the literature on the information needs of educators has taken this form. While it is true that much of this literature has a common focus and thus could provide a rich set of comparative data, both the user populations and the resource types studied tend to be narrowly defined, focusing on academics as users and on the use of the formal information sources as resources. This inherently limits the generality of this framework and excludes features of widespread import. The data from this study show awareness of the kinds of resources that would be the focus using this framework, but they also indicate that these resource types are only a small portion of the kinds of information needed by the group studied. It would be difficult to promote this framework as one that substantially helped us to understand information needs in a broad way.

While role theory is used broadly in the social sciences, its impact on theories of information needs, seeking and use has been more constrained. The data from the present study can be described quite well using the role theoretic framework. While many of the information needs expressed relate to the professional practice (teaching children, teaching teachers) of these individuals, they also encourage us to look beyond this focused behaviour and to look in depth at the actual work of the educator. In addition to looking to develop specific skills and to identify specific resources, both groups are constantly looking for general information about educational innovations, both within and outside of Reading Recovery. In addition, they are looking beyond their own needs, for appropriate resources for other teachers, administrators and principals and for their students' parents. Thus, as a theoretical framework, role theory does a good job of accounting for many more of the expressed information needs seen in this data collection than did the disciplinarity and resource use framework. However, as a side effect of this more in-depth exploration, the possibility of using these data in other contexts, to build a theory of information needs, seeking and use is itself diminished because the behaviour in question is intrinsically tied to the context being studied.

At first glance, the model of information seeking and use provided by the small worlds and information grounds approach does not seem to directly apply to the data at hand. However, as the discussion points out, parallels are close at hand. What from the disciplinary and role theory perspectives seem to be work related information needs appear to be community focused when seen from the small worlds' and information grounds' perspectives. While the community is founded in the instructional context of learning to become a Reading Recovery teacher, members of this group reach out to each other and direct their attention to the support of this teaching method both in their dependence on the informational sources of this community and in their outreach to others in the broader educational community. Thus, we see the information needs and information seeking behaviour from a quite different point of view when our theoretical framework is that provided by the small worlds and information grounds theorists.

A similar point can be made for looking at these data from a community of practice framework. Seen from this framework, the community-building aspects of the observed information needs and behaviour are also an important part of data interpretation. To this, a community of practice framework adds interpretative richness by looking at how these communal activities shape the identity of the individuals involved. Through their training and through practice, Reading Recovery teachers and teacher leaders develop a strong sense of mission that pervades their professional work and colours their personal lives. Viewing the data from a community of practice perspective leads us to think more deeply about the social nature of the information behaviour and about how an information needs analysis can cast light on the characterization of a group.

What can be learned from this analysis? Clearly, the same data set can be described from multiple theoretical perspectives. The interpretation of what has been learned varies substantially. Does this imply that any of the frameworks are wrong or inappropriate? This is perhaps a matter for each of us to judge. Looking at data from multiple theoretical perspectives, however, tells us different things about the information needs of a group and about the implications that such an analysis can provide. A discussion thread that appeared on the JESSE listserv in April of 2011 as this paper was being written asked whether dissertation-level research demands the adoption of a theoretical framework. The thread argued about what this meant as well as about the decision itself, with most agreeing that some sort of theoretical foundation is necessary for the science to proceed. What is troubling from the perspective of this paper is the assumption that one finds a theoretical framework that is already extant (or develops a new theoretical framework) without explicitly and critically examining what multiple frameworks can provide. Such an examination would encourage the field to examine theoretical constructs analytically and to assess how they serve to bring us to a deeper understanding of our data. Such analyses have been done. Cox's ([2005](#cox05)) paper looked critically at a series of models of communities of practice. More recently, Savolainen's ([2009](#sav09)) detailed comparison of the concepts of small worlds and of information grounds provide yet another model for deepening our understanding of theoretical frameworks. The data described in this paper add yet another way to look at how different theoretical frameworks reveal different facets of any set of data. I would argue for the necessity of looking at different views of data to provide us with a basis for comparing and testing theories, with the goal of exploring the nuances of our theoretical assumptions rather than accepting them if we are to advance as a science.

## Acknowledgements

The focus groups used for data collection were funded by a grant from The Verizon Foundation to the Reading Recovery Council of North America to aid in the development of the Center for Early Literacy Information. The author would like to thank Brenda Dervin, Kimberly Black. Jinx Watson and Martha Deaderick for their insights and support, Dania Bilal for pointing to the need for a theoretical framework for this paper and Reading Recovery's teachers and teacher leaders who work so hard in support of early literacy.

## About the author

Lorraine Normore was an Assistant Professor in the School of Information Sciences, University of Tennessee from 2006 through December 2011\. The work described in this paper was carried out while working as the Digital Librarian for the Reading Recovery Council of North America. She received her Master's degree in Library Science from the University of Toronto, Canada and her Ph.D. in Experimental Psychology from the Ohio State University, USA. She can be contacted at: [normore@utk.edu](mailto:normore@utk.edu) or at [cnormore@aol.com](mailto:cnormore@aol.com).

#### References

*   Allen, T.J. (1969). Information needs and uses. _Annual Review of Information Science and Technology_, **4**, 3-29.
*   Bates, M.J. (2005). [Information and knowledge: an evolutionary framework for information science](http://www.webcitation.org/62zv9kK8Z). _Information Research_, **10**(4), paper 239\. Retrieved 6 November, 2011 from http://InformationR.net/ir/10-4/paper239.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/62zv9kK8Z)
*   Belkin, N.J. (2005). Anomalous state of knowledge. In Karen E. Fisher, Sanda Erdelez & Lynne (E.F.) McKechnie (Eds.). _Theories of information behavior_. (pp. 44-48). Medford, NJ: Information Today.
*   Biddle, B.J. (1979). _Role theory: expectations, identities and behaviors_. New York, NY: Academic Press.
*   Blair, D.C. (2002). Knowledge management: hype, hope or help? _Journal of the American Society for Information Science and Technology_, **53**(12), 1019-1028.
*   Brown, J.S. & Duguid, P. (1991). Organizational learning and communities-of-practice: toward a unified view of working, learning and innovating. _Organization Science_, **2**(1), 40-57.
*   Burnett, G., Besant, M. & Chatman, E.A. (2001). Small worlds: normative behavior in virtual communities and feminist bookselling. _Journal of the American Society for Information Science and Technology_, **52**(7), 536-547.
*   Carlson, B. & Reidy, S. (2004). Effective access: teachers' use of digital resources (research in progress). _OCLC Systems & Services_, **20**(2), 65-70.
*   Chatman, E.A. (1991). Life in a small world: applicability of gratification theory to information-seeking behavior. _Journal of the American Society for Information Science_, **42**(6), 438-449.
*   Chatman, E.A. (1999). A theory of life in the round. _Journal of the American Society for Information Science_, **50**(3) 207-217.
*   Cox, A. (2005). What are communities of practice? A comparative review of four seminal works. _Journal of Information Science_, **31**(6), 527-540.
*   Crane, D. (1971). Information needs and uses, _Annual Review of Information Science and Technology_, **6**, 3-39.
*   Dervin, B. (2005). What methodology does to theory: sense-making methodology as exemplar. In Karen E. Fisher, Sanda Erdelez & Lynne (E.F.) McKechnie (Eds.). _Theories of information behavior_. (pp. 25-29). Medford, NJ: Information Today.
*   Fisher, K.E., Marcoux, E., Miller, L.S., Sanchez, A. & Cunningham, E. (2004). [Information behavior of migrant Hispanic farm workers and their families in the Pacific Northwest.](http://www.webcitation.org/62zvgUWCw) _Information Research_, **10**(1), paper 199 Retrieved 6 November, 2011 from http://InformationR.net/ir/10-1/paper199.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/62zvgUWCw)
*   Fisher, K., Naumer, C., Durrance, J., Stromski, L. & Christiansen, T. (2005). [Something old, something new: preliminary findings from an exploratory study about people's information habits and information grounds.](http://www.webcitation.org/62zwIs22b) _Information Research_, **10**(2), paper 223 Retrieved 6 November, 2011 http://InformationR.net/ir/10-2/paper223 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/62zwIs22b)
*   Fisher, K.E. & Naumer, C.M. (2006). Information grounds: theoretical basis and empirical findings on information flow in social settings. In: A. Spink & C. Cole (eds.). _New directions in human information behavior_. (pp. 93-111). Dordrecht, The Netherlands: Springer.
*   Fitzgerald, M.A., Lovin, V. & Branch, R.M. (2003). The Gateway to Educational Materials: an evaluation of an online resource for teachers and an exploration of user behavior. _Journal of Technology and Teacher Education_, **11**(1) 21-51.
*   Fry, J. (2006). Scholarly research and information practices: a domain analytic approach. _Information Processing & Management_, **42**(1) 299-316.
*   Greenbaum, T.L. (1993). _The handbook for focus group research_. 2nd ed. New York: NY: Lexington Books.
*   Herner, S. & Herner, M. (1967). Information needs and uses in science and technology, _Annual Review of Information Science and Technology_, **2** 1-34.
*   Hertzberg, S. & Rudner, L. (1999). [The quality of researchers' searches of the ERIC database.](http://www.webcitation.org/62zwi3tcs) _Education Policy Analysis Archives_, **7**(25) Retrieved 6 November 2011 from http://epaa.asu.edu/ojs/article/download/560/683\. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/62zwi3tcs)
*   Hewins, E.T. (1990). Information need and use studies. _Annual Review of Information Science and Technology_, **25** 145-172.
*   Hjørland, B. & Albrechtsen, H. (1995). Toward a new horizon in information science: domain-analysis. _Journal of the American Society for Information Science_, **46**(6), 400-425.
*   Honey, M. & McMillan, K. (1996). [Case studies of K-12 educators' use of the internet: exploring the relationship between metaphor and practice](http://www.webcitation.org/62zwi3tcs). New York, NY: Center for Children & Technology. (CCT Reports Issue no. 5). Retrieved 12 December 2010, from http://cct.edc.org/admin/publications/report/EducatorCaseStudy96.pdf. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/62zwi3tcs)
*   Ingwersen, P. (1996). Cognitive perspectives of information retrieval interaction: elements of a cognitive IR theory. _Journal of Documentation_, **52**(1), 3-50.
*   Karchmer, R.A. (2001). The journey ahead: thirteen teachers report how the internet influences literacy and literacy instruction in their K-12 classrooms. _Reading Research Quarterly_, **36**(4), 442-466.
*   Kuhlthau, C.C. (1997). Learning in digital libraries: an information search process approach. _Library Trends_, **45**(4), 708-724.
*   Kuhlthau, C.C. (2005). Kuhlthau's information search process. In Karen E. Fisher, Sanda Erdelez & Lynne (E.F.) McKechnie (Eds.). _Theories of information behavior_. (pp. 230-234). Medford, NJ: Information Today.
*   Kuhlthau, C.C. (2008). From information to meaning: confronting challenges of the twenty-first century. _Libri_, **58**(2), 66-73.
*   Lankes, R.D. (2003). [Current state of digital reference in primary and secondary education.](http://www.webcitation.org/62zxGS2V0) _D-Lib Magazine_, **9**(2). Retrieved 6 November, 2011 from http://www.dlib.org/dlib/february03/lankes/02lankes.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/62zxGS2V0).
*   Lave, J. & Wenger, E. (1991). _Situated learning: legitimate peripheral participation_. Cambridge: Cambridge University Press.
*   Leckie, G.J. (2005). General model of the information seeking of professionals. In Karen E. Fisher, Sanda Erdelez & Lynne (E.F.) McKechnie (Eds.). _Theories of information behavior_. (pp. 158-163). Medford, NJ: Information Today.
*   Leckie, G.J., Pettigrew, K.E. & Sylvain, C. (1996). Modeling the information seeking of professionals: a general model derived from research on engineers, health care professionals and lawyers. _Library Quarterly_, **66**(2) 161-193.
*   Leckie, G.J. & Pettigrew, K.E. (1997). [A general model of the information seeking of professionals: role theory through the back door?](http://www.webcitation.org/62zxS7Bub) In P. Vakkari, R. Savolainen & B. Dervin, (Eds.). _Information seeking in context: proceedings of the 1st international conference on information seeking in context, Tampere, Finland 1996_. (pp. 99-110). London: Taylor Graham Publishing. Retrieved 6 November, 2011 from http://informationr.net/isic/ISIC1996/ISIC96_index.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/62zxS7Bub).
*   Line, M.B., Brittain, J.M. & Cranmer, F.A. (1971). Information requirements of College of Education lecturers and schoolteachers. Bath , UK: Bath University of Technology, University Library. (Investigation into Information Requirements of the Social Sciences, Research Report, no. 3\. )
*   Lipetz, B.-A. (1970). Information needs and uses, _Annual Review of Information Science and Technology_, **5**, 3-32.
*   Lundh, A. & Limberg, L. (2008). Information practices in elementary school. _Libri_, **58**(2), 92-101.
*   McDowell, L. (2001). _[Early estimates of public elementary and secondary education statistics: school year 2000-2001](http://www.webcitation.org/62zzoVEzA)_. Washington: U.S. Dept. of Education 2001\. Retrieved 11 September 2011 from http://nces.ed.gov/pubs2001/2001331.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/62zzoVEzA).
*   McKenzie, P.J. (2002). Communication barriers and information-seeking counter-strategies in accounts of practitioner-patient encounters. _Library & Information Science Research_, **24**(1), 31-47.
*   McKenzie, P.J. (2003). A model of information practices in accounts of everyday-life information seeking. _Journal of Documentation_, **59**(1) 19-40.
*   Menzel, H. (1966). Information needs and uses in science and technology, _Annual Review of Information Science and Technology_, **1**, 41-69.
*   Nahl, D. & Bilal, D., (Eds.) (2007). _Information and emotion: the emergent affective paradigm in information behavior research and theory_. Medford, N.J.: Information Today.
*   Paisley, W.J. (1968). Information needs and uses, _Annual Review of Information Science and Technology_, **3** 1-30.
*   Pattuelli, M.C. (2008). Teachers' perspectives and contextual dimensions to guide the design of N.C. history learning objects and ontology. _Information Processing &Management_, **44**(2), 635-646.
*   Perrault, A.M. (2007). [An exploratory study of biology teachers' online information seeking practices.](http://www.webcitation.org/63FIPsDgH) _School Library Media Research_, **10**. Retrieved 23 February 2010 from http://www.ala.org/ala/mgrps/divs/aasl/aaslpubsandjournals/slmrb/slmrcontents/volume10/perrault_biologyteachers.cfm (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/63FIPsDgH)
*   Pettigrew, K.E. (1998). [Agents of information: the role of community health nurses in linking the elderly with local resources by providing human services information](http://www.webcitation.org/6301HpX4s). In Thomas D Wilson & David K. Allen, (Eds.). _Exploring the contexts of information behaviour: proceedings of the 2nd international conference on research in information needs, seeking and use in different contexts, 3/15 August, 1998, Sheffield, UK._ (pp. 257-276). London: Taylor Graham. Retrieved 6 November, 2011 from http://informationr.net/isic/ISIC1998/ISIC98_index.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6301HpX4s).
*   Pettigrew, K.E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behavior among attendees at community clinics. _Information Processing & Management_, **35**(6), 801-817.
*   Pirolli, P. & Card, S. (1999). Information foraging. _Psychological Review_ **106**(4), 643-675.
*   Prigoda, E. & McKenzie, P.J. (2007). Purls of wisdom: a collectivist study of human information behaviour in a public library knitting group. _Journal of Documentation_, **63**(1), 90-114.
*   Probert, E. (2009). Information literacy skills: teacher understandings and practice. _Computers & Education_, **53**(1) 24-33.
*   Rea, L.M. & Parker, R.A. (1997). _Designing and conducting survey research: a comprehensive guide_. San Francisco, CA: Jossey-Bass.
*   Reading Recovery Council of North America. (2011). _[Reading Recovery professional development](http://www.webcitation.org/6302PPAfH)_. Worthington, OH: RRCNA. Retrieved 6 November, 2011 from http://www.rrcna.org/development/development/index.asp (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6302PPAfH)
*   Recker, M. (2006). [Perspectives on teachers as digital library users: consumers, contributors and designers](http://www.webcitation.org/6302UX7T3). _D-Lib Magazine_, **12**(9). Retrieved 6 November 2011 from http://www.dlib.org/dlib/september06/recker/09recker.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6302UX7T3).
*   Recker, M., Dorward, J., Daws
on, D., Mao, X., Liu, Y., Palmer, B. _et al._ (2005). Teaching, designing and sharing: a context for learning objects. _Interdisciplinary Journal of Knowledge and Learning Objects_, **1** 197-216.
*   Recker, M.M., Dorward, J. & Nelson, L.M. (2004). Discovery and use of online learning resources: case study findings. _Educational Technology & Society_, **7**(2), 93-104.
*   Rudner, L. (2000). [Who is going to mine digital library resources? And how?](http://www.webcitation.org/6302etwoq) _D-Lib Magazine_, **6**(5) 10 p. Retrieved 6 November 2011 from http://www.dlib.org/dlib/may00/rudner/05rudner.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6302cD4rT).
*   Rudner, L.M., Miller-Whitehead, M. & Gellmann, J.S. (2002). [Who is reading on-line education journals? Why? And what are they reading?](http://www.webcitation.org/6302jIRLY) _D-Lib Magazine_, **8**(12) 13 p. Retrieved 6 November 2011 from http://www.dlib.org/dlib/december02/rudner/12rudner.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6302jIRLY).
*   Savolainen, R. (2008). _Everyday information practices: a social phenomenological perspective_. Lanham, MD: Scarecrow Press.
*   Savolainen, R. (2009). Small world and information grounds as contexts of information seeking and sharing. _Library & Information Science Research_, **31**(1), 38-45.
*   Speak UP National Research Project. (2009). _Selected national findings: Speak Up 2008 for students, teachers, parents and administrators_. Irvine, CA: Project Tomorrow. Retrieved 2 November 2011 from http://www.tomorrow.org/docs/SU08_selected%20national_findings_complete.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6302rVg0c).
*   Spink, A. & Cole, C. (2006). Integrations and further research. In Amanda Spink & Charles Cole (Eds.). _New directions in human information behavior_. (pp. 231-237). Dordrecht, The Netherlands: Springer.
*   Sveum, E.C. (2010). _[A comparative study of University of Wisconsin-Stout freshmen and senior education major's computing and internet technology skills/knowledge and associated learning experiences](http://www.webcitation.org/63035h0fg)_. Stout, WI: University of Wisconsin-Stout, Graduate School. Retrieved 6 November, 2011 from http://www.eric.ed.gov/PDFS/ED511370.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/63035h0fg)
*   Talja, S. (2005). The domain analytic approach to scholars' information practices. In Karen E. Fisher, Sanda Erdelez & Lynne (E.F.) McKechnie (Eds.). _Theories of information behavior_. (pp. 123-127). Medford, NJ: Information Today..
*   Talja, S., Tuominen, K. & Savolainen, R. (2005). "Isms" in information science: constructivism, collectivism and constructionism. _Journal of Documentation_, **61**(1), 79-101.
*   Tuominen, K., Savolainen, R. & Talja, S. (2005). Information literacy as a sociotechnical practice. _Library Quarterly_, **75**(3), 329-345.
*   Wenger, E. (1998). _Communities of practice: learning, meaning and identity_. New York, NY: Cambridge University Press.
*   Williams, D. & Coles, L. (2007). Teachers' approaches to finding and using research evidence: an information literacy perspective. _Educational Research_, **49**(2) 185-206.
*   Wilson, T.D. (1981). [On user studies and information needs](http://www.webcitation.org/6303NaZi9). _Journal of Documentation_, **37**(1), 3-15\. Retrieved 6 November, 2011 from http://informationr.net/tdw/publ/papers/1981infoneeds.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6303NaZi9)
*   Wilson, T.D. & Streatfield, D.R. (1977). Information needs in local authority social services departments: an interim report on project INISS. _Journal of Documentation_, **33**(4) 277-293.
*   Young, N.J. & Von Saggern, M. (2001). General information seeking in changing times: a focus group study. _Reference & User Services Quarterly_, **41**(2) 159-169.