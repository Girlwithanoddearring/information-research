#### vol. 16 no. 4, December 2011

# Information tactics of immigrants in urban environments

#### [Jessica Lingel](#author)  
School of Communication and Information, Rutgers University, 4 Huntington St., New Brunswick, NJ, USA 08901

#### Abstract

> **Introduction.** This project seeks to contribute to research on everyday human information behaviour by addressing the information practices of immigrants (here called migrational individuals) engaged in learning about new urban environments.  
> **Method.** Two qualitative approaches are used: semi-structured, in-depth interviews and participatory mapping (a methodology involving analysis of maps produced by interviewees). The twelve participants interviewed in this project were migrational individuals recruited from an English language learning and acculturation centre in New York, NY.  
> **Analysis.** Using Certeau's construct of tactics as a theoretical frame, interviews were transcribed and coded with NVIVO software. Analysis focused on practices used by migrational individuals in order to become familiar with New York, sources of surprise and instances of being lost in the city, and technologies and resources that were (or were not) useful in learning about daily life as a recent arrival to an urban environment.  
> **Results.** Main findings from analysis of interviews include: a detailed account of multiple information resources used in everyday life information seeking, the extent to which personal narrative and biography (such as work history) shapes interpretations of surroundings, and the deliberate use of wandering to become familiar with new environments. These findings are theorized in terms of everyday life information seeking. The term information tactics is suggested as particularly salient for understanding daily practices of navigating unfamiliar city space.  
> **Conclusions.** Through discussion of migrational individuals' information practices, possible developments for public libraries and acculturation programmes seeking to provide improved services for the immigrant community are suggested. With both practical and theoretical implications, this project provides in-depth perspectives on an understudied group within library and information science research.

## Introduction

This paper reports findings from a project that investigated the information practices of what are here called migrational individuals, an inclusive term that refers to people in a process of movement, including refugees, exchange students, visitors and immigrants. As travelers, newcomers and strangers, migrational individuals experience cities from a position of otherness, not only in terms of being ethnically or culturally different from natives, but in being unfamiliar with how a particular urban space is organized, and the customs and resources endemic to a host environment. Using accounts from migrational individuals in New York City, this paper provides a theoretical conceptualization of how information is acquired among city newcomers in order to become familiar with a new urban environment. Exploring the information practices of immigrants in the context of learning about a host environment allows for increased understanding of human information behaviour in an understudied, even marginalized community. At the same time, this research contributes to analysis of daily information practices that are so embedded within native routines and habits as to be entirely obscured. In other words, with thick descriptions from outsiders learning about city space, it may be possible to identify information practices used regularly but typically unrecognized by city natives in the daily navigations of urban life. The term information practices is here used to refer not only to the process of locating sources for information, but also the ways in which information is valued, shared and interpreted ([Savolainen 2007](#sav07))

## Theoretical frameworks and previous work

To examine the processes of urban migration is functionally tantamount to examining experiences of being new in city spaces. This involves not only physical presence in an unfamiliar place, but also processes of acquiring and managing information. This section begins with a discussion of theory relevant to the scope of this study's emphasis on daily information practices before moving on to address relevant work on immigration in library and information science research.

A central theoretical framework for this project is taken from de Certeau's work ([1984](#deC84)) on the relationship of individuals to structures of culture and politics, particularly in the context of cities. de Certeau's central project in _The Practice of Everyday Life_ is to identify ways of managing the influences and reach of institutions and institutional control. It is a point of fascination for de Certeau to understand how individuals maneuver through everyday life in ways that are individual rather than institutional, improvised rather than indoctrinated. For the purposes of this paper, the most useful of de Certeau's constructs is the notion of tactics, or the daily practices of dealing with the restrictions and apparatuses of dominant cultural institutions. To illustrate the division between tactics and strategies, de Certeau describes the ways in which residents of urban spaces create singular methods (or tactics) of navigating space in ways that are individual, playful, mischievous and bear little resemblance to the planned, official discourses (or strategies) for organizing cities ([de Certeau 1984](#deC84): 93). When the terms _tactics_ and _tactical_ are used in this paper, they are meant in the specific, de Certeauian sense of individualized maneuvering as a way of coping with institutional infrastructure and organization.

To understand tactics in terms of information, Savolainen's work ([1995](#sav95)) on everyday life information seeking offers an instrumental understanding of information practices in the context of the everyday. The core concept of everyday life information seeking involves,

> the acquisition of various informational (both cognitive and expressive) elements which people employ to orient themselves in daily life or to solve problems not directly connected with the performance of occupational tasks ([Savolainen 1995](#sav95): 266-267).

Critical to Savolainen's work was his contribution to a change in human information behaviour research from user studies situated in a professional or academic context to personal or leisure-based information practices. Such an approach is particularly useful for analysis of the immigrant community ([Caidi _et al._ 2010](#cai10): 501), in that unlike temporary travellers whose experience of a new urban environment is intended to be extraordinary, the immigrant community seeks to make a host environment ordinary, to gain knowledge of and even ownership over new environments such that they begin to feel like home.

Because migrational individuals are by definition outsiders, studying their experiences in an information context contributes to information behaviour research on marginalized groups. Immigrants are here positioned as marginalized first because as a group they tend to be poorer and possess lower levels of formal education than the native population ([Portes and Rumbaut 1990](#por90): 248). It is important to point out the problematic nature of making generalized statements about immigrant populations, especially when differences between nationality, circumstances of immigration and length of stay in the host environment are taken into account.

The economic, social and cultural status of immigrants in the United States through an H-1B (non-immigrant) work visa is bound to be remarkably different from those of an illegal immigrant working on a seasonal basis. Also, even when immigrants possess sufficient levels of education, this may not translate to better employment; Komito and Bates ([2011](#kom11)) found that Polish immigrants in Ireland were underemployed rather than under-educated. With these caveats, the assessment that the immigration population as a whole tends to be poorer and have less formal education than the native population is particularly true of recent arrivals, who comprise the interview pool in this paper.

A second type of marginalization stems from the fact that the migrational experience is often one of vulnerability and uncertainty, particularly in terms of access to and use of information. Some important work has been done on information practices in an everyday life context for marginalized and underserved communities, including research on health information of HIV and AIDS patients ([Veinot 2009](#vei09), [2010](#vei10)), use of technology by urban youth ([Agosto and Hughes-Hassell 2005](#ago05), [2006](#ago06), [2009](#ago09)), and information needs of intimate partner violence survivors ([Westbrook 2008](#wes08)). Perhaps the most influential has been Chatman's work investigating how surrounding social structures influence the acquisition, use and dissemination of information in marginalized or disadvantaged communities, such as female prison inmates ([1999](#cha99)), elderly women in a retirement center ([1996](#cha96)) and janitors at a university ([1991](#cha91)). Among Chatman's insights on the information practices of disadvantaged and minority populations was the argument that social context has a direct and highly influential impact on the ways that information is used, produced, circulated and hidden ([1999](#cha99)). Chatman's research relied on and advocated situating information within the particular social world of the people using it, calling her approach life in the round ([1999](#cha99)). In the context of migrational experiences, taking a life in the round approach to information practices requires accounting for spatial-temporal elements surrounding information, and the affordance not only of acquiring information itself, but in becoming adept (or tactical) in information practices related to urban life.

There has been specific research within library and information science scholarship on immigrants and information use. Much of this work focuses on specific services and programmes that libraries are or should be offering to the immigrant community (e.g., [Burke 2008](#bur08)), tools that immigrants use to maintain ties with their home countries (e.g., [Komito and Bates 2011](#kom11)) or the information networks that inform the immigration process (e.g., [Amit and Riss 2007](#ami07)). Fisher and her colleagues have provided some insightful work on the information use of migrant workers in the Pacific Northwest ([Fisher 2004](#fis04)) and on immigrants in Queens, New York ([Fisher _et al._ 2004](#fis042)). Both of these projects were centered on the information practices of immigrants related to a particular information institution, namely a local library with English language and acculturation services.

In contrast to research focusing on the use of a particular information resource, this project examines the information practices of migrational individuals upon arrival in a host environment, while taking into account how prior information and exposure to representations of the host environment play out post-migration. Although the extent to which libraries and other information institutions could provide much-needed services to immigrant communities has been recognized ([Burke 2008](#bur08); [Public Library Quarterly 2009](#plq09)), research on the information needs of immigrants and the tactics for acquiring information is still limited ([Caidi _et al._ 2010](#cai10): 521). To address the issue of migrational information practices, particularly in urban systems, it is necessary to understand both the social factors and tactical choices at work in maneuvering through unfamiliar environments. This project seeks to address these phenomenon using qualitative interviews in order to gain a complex, holistic understanding of migrational experiences. Research questions guiding this project include:

*   RQ1: What are the information practices used by migrational individuals to learn about new neighbourhoods in a host environment?
*   RQ2: How can these individual (and informational) tactics be used to understand the phenomenology of urban spaces among newcomers?

## Methods

This project uses a qualitative approach, drawing on Corbin and Strauss's ([2008](#cor08)) grounded theory methods to collect and analyse data. Interviews took place during the summer of 2010 at the International Center in New York, an organization that provides English language and acculturation programmes to refugees, immigrants, exchange students and other foreigners. In total, twelve interviews were conducted. Although additional interviews would have provided increased measures of reliability, this number is in keeping with guidelines for attaining saturation in qualitative research ([Guest _et al._ 2006](#gue06)) and reflects the exploratory nature of this project. Table one presents information on interview participants, including age, sex, nationality (although one participant declined to specify his native country, preferring to state only that he was from Africa) and neighbourhood of residence in New York. Half of the participants were from South Korea, while the remaining interviewees are from six other countries.

On the one hand, this means that experiences of Korean immigrants are over-represented in the analysis that follows. (This may simply reflect the increasing number of Asian immigrants to New York City ([Semple 2011](#sem11)).) At the same time, the decision to include a number of nationalities in this study rather than focusing on a single nationality was a deliberate one. As Todd ([1999](#tod99)) has argued in his research on youth and information seeking, heterogeneity of backgrounds allows one to '_maximize portrayals and to identify possible theoretical boundaries of change, rather than providing just one cultural perspective_' ([Todd 1999](#tod99): 12). Also, there is a risk in generalizing the experience of any particular migrational individual or individuals as representative of a country as a whole. In other words, claiming the accounts of an individual (or a small number of individuals) as representative of a nation as a whole obscures important differences that may exist between members of the same country, such as those based on race, class and sex. The aim of this project is to identify theoretical boundaries around the phenomenology of new urban spaces and to begin to map processes of information practices in urban environments in a way that considers, but does not take as its primary focus, nationality.

To recruit for interviews, members of International Center in New York were approached at the organization's lounge, where members and volunteers are encouraged to engage in informal conversations. Members were invited to participate after an initial determination of basic conversation skills in English, at which point the research agenda and confidentiality was explained and consent obtained. Interviews were semi-structured and ranged from thirty to sixty minutes. All interviews were conducted in English; by virtue of being members at the Center, all participants were non-native English speakers. When participants are quoted throughout this paper, minimal attempts are made to alter their language to conform to standard written English.

During interviews, participants were asked to describe how they gained information about the Center, their local place of residence and about experiences in getting to know New York. Drawing on the critical incident technique ([Flanagan 1954](#fla54)), participants were also asked to describe a time when they had been lost in New York City, and whether or not they had then developed a technique for avoiding similar experiences. As a whole, these questions were intended to gain insight into specific processes of tactics for managing different aspects of everyday urban life. Interviews were transcribed and individually coded using NVIVO software. Out of the twelve interviews, two participants (P3, P4) asked not to be recorded and, in those cases, extensive notes were taken during and after the discussion, which were then coded. Using an open coding method ([Corbin & Strauss 2008](#cor08): 59), themes were developed related to tactics of managing newness in city spaces. Although the coding process was guided by an interest in information practices, other themes were allowed to emerge over the course of the researcher immersing herself in interview data.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Demographic information of participants**</caption>

<tbody>

<tr>

<th colspan="5" valign="middle">Participant Details</th>

</tr>

<tr>

<th></th>

<th>Sex</th>

<th>Age</th>

<th>Nationality</th>

<th>Location in NYC</th>

</tr>

<tr>

<td align="center">P1</td>

<td align="center">M</td>

<td align="center">32</td>

<td align="center">Korean</td>

<td align="center">Elmhurst</td>

</tr>

<tr>

<td align="center">P2</td>

<td align="center">M</td>

<td align="center">28</td>

<td align="center">Cuban</td>

<td align="center">ABC City</td>

</tr>

<tr>

<td align="center">P3</td>

<td align="center">F</td>

<td align="center">38</td>

<td align="center">Ukraine</td>

<td align="center">Sheepshead Bay</td>

</tr>

<tr>

<td align="center">P4</td>

<td align="center">F</td>

<td align="center">31</td>

<td align="center">Dominican Republic</td>

<td align="center">Yonkers</td>

</tr>

<tr>

<td align="center">P5</td>

<td align="center">M</td>

<td align="center">35</td>

<td align="center">African</td>

<td align="center">Jamaica</td>

</tr>

<tr>

<td align="center">P6</td>

<td align="center">M</td>

<td align="center">33</td>

<td align="center">Bangladesh</td>

<td align="center">Bronx</td>

</tr>

<tr>

<td align="center">P7</td>

<td align="center">F</td>

<td align="center">28</td>

<td align="center">Korean</td>

<td align="center">Astoria</td>

</tr>

<tr>

<td align="center">P8</td>

<td align="center">F</td>

<td align="center">25</td>

<td align="center">Korean</td>

<td align="center">Baybridge</td>

</tr>

<tr>

<td align="center">P9</td>

<td align="center">F</td>

<td align="center">23</td>

<td align="center">Korean</td>

<td align="center">Midtown</td>

</tr>

<tr>

<td align="center">P10</td>

<td align="center">F</td>

<td align="center">33</td>

<td align="center">Korean</td>

<td align="center">Kew Gardens</td>

</tr>

<tr>

<td align="center">P11</td>

<td align="center">M</td>

<td align="center">24</td>

<td align="center">Korean</td>

<td align="center">Jackson Heights</td>

</tr>

<tr>

<td align="center">P12</td>

<td align="center">F</td>

<td align="center">32</td>

<td align="center">Korean</td>

<td align="center">Flushing</td>

</tr>

</tbody>

</table>

In addition to answering questions during interviews, participants were asked to draw maps of their neighbourhoods, using a methodology called participatory mapping ([Singer _et al._ 2000](#sin00)). Participatory mapping aims to provide researchers with tangible diagrams of an individual perspective on a resource, concept or place. In a de Certeauian frame, participatory mapping provides a depiction of individual tactics for navigating a neighbourhood, rather than institutional strategies for understanding city space. Because it was impossible to know ahead of time where participants lived, pre-formatted maps could not be used, and interviewees were instead asked to draw their neighbourhoods on blank pieces of paper with colored pencils. No set definition of neighbourhood was provided to participants; instead, they were encouraged to depict whatever their conception of the term neighbourhood indicated. After participants drew maps, they were asked to point out five places that they went to in a given week, and to label those places on their maps. In some cases, participants who had drawn maps on a very small scale (depicting only a block or two) were unable to point out five places on their maps, in which case they were asked to describe places without labelling them on their maps. Although twelve interviews were conducted (with participants from six different countries), one participant (P5) declined to draw a map and thus analysis is based on twelve interviews and eleven maps. Maps were used as an aid to interview questions and a source of data in themselves. As with interview transcripts, themes were allowed to emerge from analysis of maps, guided by an interest in developing an understanding of the information practices of migrational individuals in terms of learning about a host environment. Maps were also examined throughout the process of coding interviews in order to gain additional insights into the information practices of participants used to navigate urban environments.

## Findings

This section outlines three main themes developed from analysis of interviews and participatory maps, beginning with a discussion of the reported sources of information among participants, and then addressing how personal history shapes perceptions of city space. Finally, the importance of wandering as a tactic for acquiring information about urban spaces is discussed.

### Information resources for everyday (migrational) life

Throughout the course of interviews, participants identified a number of information resources in the course of discussions about acculturation in New York City, summarized in table two. Two sources were notable in their overwhelming popularity: All participants referred to using friends and the Internet to find information, where the former category, it should be noted, excludes neighbours and friends from school or the International Center in New York. A typical instance of relying on accounts from other migrational individuals from the same country was P1's description of learning about his neighbourhood in Queens:

> my friend, she moved to American nine years already. So she knows everything about America or New York City. So she teaching me about American information, New York City information.

This comment suggests the extensive confidence placed in friends and acquaintances who both share a country of origin and have lived in the host environment.

Friends used by participants as sources for information were not necessarily local; in some cases participants recounted having obtained information about New York before leaving their native country. For example, P6 described learning about Bangladeshi food available in Jackson Heights before he moved to New York, and recounted being told that in New York City,

> Jackson Heights is like an Indian place ? Bangladeshi people or Indian people go there, find special items.

Similarly, P12 described choosing Flushing for grocery shopping as a result of conversations in her native Korea, before she had even left for the United States:

> my friend [told me] you can buy Korean food [in Flushing], it's cheaper than Manhattan.

In these examples, information acquired before moving is given a certain weight and credence even after arriving in New York. This suggests that participants placed value in sources with whom there is a personal connection, even if those sources have limited or indirect access to the object of inquiry, which in this case was New York.

<table width="80%" border="0" cellspacing="0" cellpadding="3" align="center" style="border-right: #ffffff solid; border-top: #ffffff solid; font-size: smaller; border-left: #ffffff solid; border-bottom: #ffffff solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 2: Information resources among migrational individuals**</caption>

<tbody>

<tr>

<td align="center">![Information Resources Among Migrational Individuals](p500fig1.png)</td>

</tr>

</tbody>

</table>

The Internet was unanimously referenced as a way of locating information for everything from hours of operation at the Center (P1, P3, P4, P7, P8, P9, P10, P11) to finding theatres with foreign movies (P2) to apartment hunting (P1, P3, P4, P8, P9, P10, P11). Google was the most frequently cited source of information, where P2's description of using Google to find a route to a location for the first time is representative:

> in the beginning I search the Internet, the Google, Google map, the address, the directions. I write, I write in my notebook or cell phone. Now, it's no problem.

While Google enjoyed frequent use across participants, in some cases, there was a link between nationality and a specific website. For example, among Korean participants, the website HeyKorean was almost unanimously described (by five out of six Korean interviewees) as a particularly useful resource for tasks such as finding apartments, places of worship and entertainment. In most instances, references to the Internet involved a fairly straightforward process of wanting a particular fact and searching for it online, as in the case of needing to know the Center's hours of operation and then looking for it online (Google was the search engine referenced most frequently for this type of search).

In some cases, however, the Internet served as a highly personalized and tactical information practice. Asked to describe a time when she had been lost in New York, P10 described needing directions in the street and looking for people with smart phones to ask for assistance:

> whenever I ask a route, I saw who have iphone. They use their iphone and they teach me.

In this case, making decisions about whom to ask for help was based not on shared ethnicity, age or sex (although these factors likely played a role) but on gaining access to the Internet. In this tactic for managing lostness, technology effectively trumped numerous social and demographic aspects. P4 described using Google translator to locate words in English in the particular context of having been repeatedly frustrated at grocery stores in New York because she was unfamiliar with the English names of certain foods. For this participant, using Google translator became a regular part of preparation for going to the grocery store, largely because she disliked having to ask for help from strangers and because she wanted to avoid repeated trips to the store as a result of not being able to locate needed items. As a resource, the Internet here is used indirectly as a source of information about New York _per se_, and more as a method of coping with different kinds of lostness. For P10, online access served as a signifier of potential assistance, and for P4 it served as a means of pro-active coping. In both cases, participants were attempting to deal with the vulnerability of non-fluency stemming not only from a lack of English speaking abilities, but also in terms of gaining a sense of familiarity in an urban environment. Thus the Internet served both highly-generalized information practices shared across the participant pool, such as Google searches, and highly-individualized practices, such as personalized routines for grocery shopping.

### Personal history as a cartographic lens

A second theme that emerged from interviews was the way in which personal experiences shaped perceptions of host environments. P1, for example, had been a mechanic in Korea. Asked about his current neighbourhood in Queens, he mentioned the number of Korean-owned garages, and also commented on what he felt was the surprisingly poor condition of roads in New York. Interestingly, this participant's map (see Figure 1) included street names and was drawn to a scale that would much more easily than other participatory maps, allow for driving directions. Similarly, a Korean exchange student (P9) studying fashion referred to the salons and beauty supply stores she had seen in her three weeks in New York. The same participant also commented with disappointment about the lack of _fashionable_ New Yorkers, saying

> I thought that New Yorkers were fashionable. I just watched the New York on television, [but here] it wasn't, only a few of them.

P6, a doctor from Bangladesh, noted the hospitals and medical libraries he had thus far located in the city and mentioned that

> one thing that was surprising to me was the obesity. This was not my experience in my imagination.

In each case, the narrative used to describe New York reflected the participants' personal realities and knowledge, demonstrating how information is filtered into a frame of individual history: a former mechanic noticed road conditions; a fashion student noticed the trendiness (or lack thereof) among city residents; a doctor observed health issues. Analysis of maps yielded similar results; the same participant who was disappointed in the lack of high fashion New Yorkers had only been in the city three weeks. Staying with a sister in Midtown, her neighbourhood map (see Figure 2) is notable for the number of tourist attractions depicted, including Carnegie Hall and Central Park. This suggests that her present knowledge of the city was largely one of a tourist aware of sight-seeing stops, rather than the more mundane locations depicted on most participants' maps (laundromats, neighbourhood parks and grocery stores figured heavily). This pattern of framing host environments in terms of personal history is initially an intuitive one, but implies that viewing a new space through a lens of prior experiences is a process that involves not only access to and use of information sources, but an individual interpretation of surroundings drawing on facets of identity such as occupation and education. Also, these comments from participants show that in addition to what people notice about a city, how people describe it is framed in terms of personal narrative.

<div align="center">![Figure 1: Participatory map from P1](p500fig2.png)</div>

<div align="center">  
**Figure 1: Participatory map from P1**</div>

<div align="center">![Figure 2: Participatory map from P9](p500fig3.png)</div>

<div align="center">  
**Figure 2: Participatory map from P9**</div>

### Wandering as an information practice

Finally, several interviewees referenced wandering as a way of becoming familiar with a neighbourhood. Asked to point out places on the map he had drawn of his neighbourhood, P1 explained:

> I walk on the street, and then I found here and here, here, here. And then, everywhere I found myself.

These remarks suggest the extent to which a sense of ownership over or comfort with one's surroundings can take place through wandering. Similarly, P6 described walking through his neighbourhood in the Bronx:

> Whenever I go to new place, I always like to walk anywhere ? just to wandering. To look for something, to look for anything, which I like. Anything.

Some participants thus devised a form of acquiring information about their neighbourhoods completely outside of established information grounds, using informal, unstructured walks to acquire a kind of personal fluency in their neighbourhoods. It is important here to separate wandering and lostness, where the difference depends precisely on intentionality. The participant quotes included here suggest the extent to which submitting oneself to an unknown environment deliberately as part of a process of self-education can be both pleasant and useful. This is in marked distinction from Lynch's ([1960](#lyn60)) characterizations of being lost as a moment of panic and disturbance:

> let the mishap of disorientation once occur, and the sense of anxiety and even terror that accompanies it reveals to us how closely it is linked to our sense of balance and well-being ([Lynch 1960](#lyn60): 4).

In contrast, wandering is here used as a fundamental process of acquiring familiarity. This is not to say that recent immigrants do not experience moments of displacement or confusion. Participants were asked to describe moments of being lost that showed (sometimes quite movingly) a sense of being at a complete loss as to what to do or where to go for help. Wandering, however, was discussed by some participants as a distinct kind of lostness that is both deliberate and constructive, and was referred to by some participants as a useful information practice in terms of becoming acquainted with one's urban surroundings in a way that de Certeau would describe as tactical.

## Discussion

After working through the interview material to identify some of the information practices of migrational individuals, key findings can be summarized in the following way:

*   Information practices used to learn about new neighbourhoods are both diverse and the source of highly personalized improvisation, in response to individualized experiences as newcomers.
*   Perceptions of city space are shaped by personal narrative and biography, evidenced not only in what migrational individuals know about a city, but how they talk about it.
*   Wandering can be utilized as a tactical way of familiarizing oneself with urban surroundings.

The first theme relates directly to the first research question, which sought to identify the practices used by migrational individuals to learn about new city spaces. Research question 2 asked how these practices can be used to understand the phenomenology of urban spaces among newcomers, which relates to the last two themes of personal narrative and wandering. To address these themes examining the literature of urban studies and human information behaviour theory is instructive. This discussion begins by drawing on research from immigrant studies before moving to analysis of information practices and urban space.

### Information and the migrational individual

To understand the above findings in terms of what it means to be migrational, turning to immigration studies literature is particularly instructive. In their work on immigration in the United States, Portes and Rumbaut ([1990](#por90)) have written that the concentration of economic support within a particular immigrant group (called an ethnic enclave) provide critical forms of resources and support. The roles played by ethnic enclaves among the migrational individuals interviewed here was somewhat inconclusive. Information from one's country of origin was referenced but there were no responses of having strong ties along lines of nationality. Some participants stated that having people from their same ethnic group was unimportant in choosing a place to live (P1, P2, P4, P12). Nearly half of the participants were Korean, but none of the participants lived in the same neighbourhood and in fact resided in three different boroughs, underscoring the difficulty of generalizing from the experience of one migrational individual to another based solely on one factor, such as nationality. This is not to contradict Portes and Rumbaut in their claims about the importance of ethnic enclaves, only to point out that what constitutes ethnic enclaves may be a complex question. Does the fact that most Korean interviewees used the Website [HeyKorean](http://www.heykorean.com/) to find information about New York justify calling that an (online) ethnic enclave? It has been suggested that with increased access to tools of communication, immigrants may have an easier time maintaining native customs and identities ([Portes and Rumbaut 1990](#por90): 133; [Komito and Bates 2011](#kom11)). In some cases, the rich information sources and sense of community gained online may not only contribute to this eliding of assimilation, but also could constitute a kind of ethnic enclave.

To bridge immigration and information studies requires accounting for how information circulates within communities, where Levitt's work ([2001](#lev01)) on immigrants from the Dominican Republic who immigrated to Boston provides some useful terminology. According to Levitt, social remittances refer to the circuits of exchange between home and host environments, including the transmission of trends, ideas, customs and habits ([Levitt 2001](#lev01): 54). Levitt's innovation is to expand on what has become a mainstay of political discourses on immigration, economic remittances, to include other forms of tradeable assets. For immigrants interviewed in this project, social remittances included information traded back and forth between home and host countries about where to live, but also about what to expect. P6, for example, did not have to search on his own for Bangladeshi food and could instead simply follow the advice of friends from home who directed him to Jackson Heights. In other cases, migrational individuals in New York served as the sender rather than receiver of social remittances. When asked what questions his friends and relatives in Cuba had about New York, P2 listed:

> What is it like? Is it like the movies? What do people do?

Here, experience in a host environment comes with the ability to shape others' ideas on New York, illustrating the process of social remittances as information. Although Levitt was mostly interested in using the term to refer to the kinds of social capital gained through access to and awareness of trends, customs and fashions of a host environment, social remittances should also be thought of as including the affordances of having access to information about new environments.

### Information and the city

In terms of the second theme of analysis, that migrational individuals' perceptions of their neighbourhoods are shaped by personal history, this finding underscores the ways that a city's imageability of a city is determined by individual history. In his study on the urban landscapes of Boston, Jersey City and Los Angeles, Lynch ([1960](#lyn60)) used the term _imageability_ to refer to '_that quality in a physical object which gives it a high probability of evoking a strong image in any given observer_' ([Lynch 1960](#lyn60): 9). Given the instances in which participants' perceptions of their neighbourhoods were inflected with individual history, the imageability of a city also draws on personal narrative. Lynch's study focused on understanding how long-term residents of a city think of its shapes, streets and structures, and his text does not provide personal history of his participants beyond very limited demographic information. Lynch's description of the limitations of his research included an acknowledgement of class bias, because he purposefully sought to include '_middle-class, professional and managerial_' ([Lynch 1960](#lyn60): 150) people as his participants. Partially because of this, the study was '_confined to images as they exist and one point of time. We would understand them far better if we knew how they develop: how does a stranger build an image of a new city?_' ([Lynch 1960](#lyn60): 157-158). To an extent, this project addresses some of this process of strangers building images of a new city, and goes a step further than Lynch in looking to account for ways that one's personal history shapes the mental cartography of surrounding space, as well as how information practices play into this internal cartography. By gathering narratives from migrational individuals in New York City, it is possible to understand on a granular level how participants develop tactics for learning about new urban environments.

Related to Lynch's comment on understanding imageability as it develops is the question of the extent to which mental maps of a city are malleable. If it is assumed that conceptual maps of cities can be thought of as a way of organizing one's surroundings, it may be that as immigrants become increasingly acculturated, their conceptual maps of city space become less individualized and more homogenous. Portes and Rumbaut ([1990](#por90)) have argued that most immigrants adopt habits, culture and ethics of their host environments, sometimes with starkly negative consequences ([Lynch 1960](#lyn60): 189). In de Certeau's ([1984](#deC84)) terminology, the process of acculturation is one of being increasingly embedded within structures and apparatuses of hegemonic culture. As newcomers, migrational individuals perceive space around them through a lens inflected with personal experience, and descriptions of their neighbourhoods reflect their particular histories and identities. As time progresses, however, these personal histories may be less and less evident in describing one's neighbourhood and ultimately in their perceptions of the city as a whole. Further research, perhaps taking a longitudinal approach to information practices, would be required to address this analysis thoroughly.

Turning to the third theme of interview findings, references to wandering can be positioned as mechanisms of spatial familiarization, attempting to gain a sense of comfort with one's surroundings. Wandering in this sense is paradoxically purposive (in that it is an effort to learn about one's neighbourhood) and directionless (in that there is no explicit destination marking the culmination of these movements). Phenomenologically, wandering serves as a practices that is deliberated and, at least among participants in this study, individual. In this way, wandering acts as a process of immersing oneself in what initially is experienced as unordered environmental stimuli in order to identify (haltingly, perhaps even randomly) patterns of meaning. Thus wandering may be an important process of learning about a host environment and acquiring a sense of one's neighbourhood ? in de Certeau's ([1984](#deC84)) terms wandering allows a newcomer not only to make sense of a neighbourhood but to begin to find ways to make do, to make maps that are individual, personal and poached.

## Professional and research implications for library and information science

From this analysis, there are several implications to be drawn for library and information science, both in the profession and in scholarship. Regarding the former, it is interesting to note that only one participant (P6) described using the library in conversations about information practices, and in this lone case, the library served more as a place of study (for his upcoming medical license exams) than as a site of information seeking. Library and information science studies that have used the library as a starting point for investigation of information practices of immigrants will by design find that the library figures prominently in their results. In contrast, this study took place outside of the library, drawing on a population without predetermined connections to a particular information institution. Although as a sample size this pool of participants is too small to make generalizable claims about library use, there are some indications from these findings that libraries in urban areas may not be reaching a large portion of immigrants. Keeping in mind that members of the International Center in New York are a self-selected group of migrational individuals looking for some degree of acculturation (at the very least, increased proficiency in English), it is perhaps particularly striking that even among this group, there was almost no mention of using the library in discussions of learning about new urban environments. This suggests that although libraries stand to offer a great deal of resources to this population, there is room for expanded outreach. With a better understanding of local information grounds already in use by immigrant communities, including outreach to some of the more popular English language education programmes, libraries could more effectively publicize their services and increase participation in programmes related to and useful for migrational individuals.

In terms of implications for human information behaviour research, there has been a significant push in user studies to refocus HIB scholarship from information seeking to information use ([Dervin 1992](#der92)). Fisher _et al._ ([2004](#fis04)) have been staunch advocates of this view, using it to buttress their work on information grounds. Although these theoretical structures are insightful, a more apt positioning for the individuals studied here is less about use _per se_, and (following [Chatman 1999](#cha99)) more about information context. Taking de Certeau's position outlining individual methods of dealing with institutional power, the term information tactics can be proposed to refer to individually-devised methods of searching for, acquiring and using information. As a term, information tactics draws on the concept of how information is used in the specific context by developing individual approaches to manage institutional strategies. Looking for people with iphones when in need of directions, making shopping lists with Google translator and wandering through one's neighbourhood are all instances of information tactics. Giving consideration to information in terms of use, as well as personal, spatial-temporal context, information tactics captures the everyday methods of navigating official, planned infrastructure. Put another way, focusing on information tactics within human information behaviour research directs attention to how information is used tactically and individually, while taking into account why it is used in that particular way.

## Limitations and further work

Inherent to the design of this project are several limiting factors. A larger pool of participants would allow for the confirmation, extension and complication of themes developed in this analysis. In addition, although this study sought out a nationally-heterogeneous participant pool, the fact that half of the participants in this project were from the same country (South Korea) effectively weighted interview responses toward a single nationality. Further research could either strive towards greater representation or focus solely on the experiences of a particular nationality. This latter approach would lend itself to taking into account facets of culturally-specific identity that informs community-based construction of cities, institutions and information. In particular, focusing research on specific nationalities could allow for a comparative study of information tactics used by particular groups of immigrants. This could be particularly useful for institutions and organizations looking to serve a specific ethnic or national community. It is argued here that the migrational experience of new urban spaces involves a process of information acculturation that has the potential to homogenize individual ordering of mental space. In order to account for this process of acculturation, a longitudinal approach to the immigration experience would enable analysis of information practices as they develop over time.

## Conclusion

Bowker and Star ([1999](#bow99)) have argued that the infrastructure of institutions is invisible to those most familiar with them. In contrast, by their very newness, newcomers are more likely to be able to identify the infrastructure of institutions by virtue of not having been natively attuned to the functions and roles of institutions. It is in this sense that this research aims to contribute to discourses on human information behaviour and information use. On the one hand, it is hoped that with better understanding of migrational information tactics it may be possible to provide improved resources to city newcomers. One reason for pursuing the first reseach question was to identify and document practices of migrational individuals in urban areas. With thick description and diverse perspectives, a complex picture of migrational experiences emerges, with practical implications for information professionals interested in providing acculturation services. On the other, however, it is neither assumed nor desired that in addressing information tactics of migrational individuals, the tactics themselves will disappear. Thus research question 2 centred on exploring migrational experiences from a phenomenological perspective, that is, as a process or set of processes of navigating new spaces, framed here through a focus on information. These information tactics are an essential part of modern life, for long-term residents of a small town as well as newcomers to big cities. The analysis in this paper has focused on immigrants in urban environments because it is assumed that in identifying the information tactics of city strangers, it is possible to see deeply embedded and routinely obscured information tactics of city natives. Just as Lynch ([1960](#lyn60)) suggested that the best way to understand a city's imageability is to investigate the experiences of new residents, the information tactics of strangers can prove illuminating in trying to understand information tactics as practices that take place daily, in ways that are deeply entrenched, taken for granted and hidden, but nevertheless form an essential part of navigating one's environment in everyday life.

## Acknowledgements

The author gives heartfelt thanks to the International Center in New York members and programme administrators for their assistance with this paper. As well, the author thanks anonymous reviewers for their thoughtful and useful comments.

## About the author

Jessica Lingel is a PhD candidate in the School of Communication and Information at Rutgers University. She has an MLIS from Pratt Institute and an MA from New York University. Her research interests include information practices of marginalized communities, social media technologies and intersections of post modern theory and librarianship. She can be contacted at [jlingel@eden.rutgers.edu](mailto:jlingel@eden.rutgers.edu)

#### References

*   Agosto, D. E. & Hughes-Hassell, S. (2006). Toward a model of the everyday life information needs of urban teenagers, part 1: Theoretical model. _Journal of the American Society for Information Science and Technology_, **57**(10), 1394-1403.
*   Agosto, D. E. & Hughes-Hassell, S. (2009). Making sense of an information world: The everyday-life information behaviour of preteens. _Library Quarterly_, **79**(3), 301-341.
*   Agosto, D. E. & Hughes-Hassell, S. (2005). People, places, and questions: An investigation of the everyday life information-seeking behaviours of urban young adults. _Library & Information Science Research_, **27**, 141-163.
*   Amit, K., & Riss, I. (2007). The role of social networks in the immigration decision-making process: the case of North American immigration to Israel. _Immigrants & Minorities_, **25**(3), 290-313.
*   Bowker, G. C., & Star, S. L. (1999). _Sorting things out: classification and its consequences_. Cambridge, MA: MIT Press.
*   Burke, S. (2008). Use of public libraries by immigrants. _Reference & User Services Quarterly_, **48**(2), 164-174.
*   Caidi, N., Allard, D., & Quirke, L. (2010). The information practices of immigrants. _Annual Review of Information Science and Technology_, **44**, 493-531.
*   Chatman, E. A. (1991). Life in a small world: applicability of gratification theory to information-seeking behaviour. _Journal of the American Society for Information Science_, **42**(6), 438-449.
*   Chatman, E.A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science and Technology_, **47**(3), 193-206.
*   Chatman, E. A. (1999). A theory of life in the round. _Journal of the American Society for Information Science_, **50**(3), 207-217.
*   Corbin, J.M. & Strauss, A. L. (2008). _Basics of qualitative research: techniques and procedures for developing grounded theory_. Los Angeles, CA: Sage Publications.
*   De Certeau, M. (1984). _The practice of everyday life, Vol. 1_. Berkeley, CA: University of California.
*   Dervin, B. (1992). From the mind's eye of the user: The sense-making qualitative-quantitative methodology, in Glazier, J. and Powell, R. (Eds.), _Qualitative research in information management_. (pp. 61-84). Englewood, CO: Libraries Unlimited.
*   Fisher, K. (2004). Information behaviour of migrant Hispanic farm workers and their families in the Pacific Northwest. _Information Research_, **10**(1), paper 199\. Retrieved 22 November, 2011 from http://informationr.net/ir/10-1/paper199.html
*   Fisher, K., Durrance, J. & Bouch Hinton, M. (2004). Information grounds and the use of need-based services by immigrants in Queens, New York: A context-based, outcome evaluation approach. _Journal of the American Society for Information Science and Technology_, **55**(8), 754-766.
*   Fisher, K., Landry, C., & Naumer, C. (2007). Social spaces, casual interactions, meaningful exchanges: "Information ground" characteristics based on the college student experience. _Information Research_, **12**(2).
*   Flanagan, J. (1954). The critical incident technique. _Psychological Bulletin_, **51**(4).
*   Guest, G., Bunce, A. & Johnson, L. (2006). How many interviews are enough? An experiment with data saturation and variability. _Field Methods_, **18**(1), 59-82.
*   Komito, L. & Bates, J. (2011). Migrants' information practices and use of social media in Ireland: networks and community. In _Proceedings of the 2011 iConference (iConference '11)._ (pp. 289-295). New York, NY: ACM Press.
*   Levitt, P. (2001). _The transnational villagers_. Berkeley, CA: University of California Press.
*   Lynch, K. (1960). _The image of the city_. Cambridge, MA: Technology Press.
*   Portes, A., & Rumbaut, R. G. (1990). _Immigrant America: a portrait_. Berkeley, CA: University of California Press.
*   _Public Library Quarterly_. (2009). Library services for immigrants, an abridged version. _Public Library Quarterly_, **28**(2), 120-126.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of "way of life." _Library and Information Science Research_ **17**,(3), 259-294.
*   Savolainen, R. (2007). Information behaviour and information practice: reviewing the "umbrella concepts" of information-seeking studies. _Library Quarterly_, **77**(2), 109-132.
*   Semple, K. (2011, June 24). Asian New Yorkers seek power to match numbers. _New York Times_. Retrieved 24 June, 2011 from http://www.nytimes.com/2011/06/24/nyregion/asian-new-yorkers-asian-new-yorkers-seek-power-to-match-surging-numbers.html
*   Singer, M., Stopka, T., Siano, C., Springer, K., Barton, G., Khoshnood, K. _et al._ (2000). The social geography of AIDS and hepatitis risk: qualitative approaches for assessing local differences in sterile-syringe access among injection drug users. _American Journal of Public Health_, **90**(7), 1049-1056.
*   Todd, R. (1999). Utilization of heroin information by adolescent girls in Australia: a cognitive analysis. _Journal of the American Society for Information Science_, **50**(1), 10-23.
*   Veinot, T.C. (2010). A multilevel model of HIV/AIDS information/help network development. _Journal of Documentation_, **66**(6), 875-905.
*   Veinot, T.C. (2009). Interactive acquisition and sharing: Understanding the dynamics of HIV/AIDS information networks. _Journal of the American Society for Information Science and Technology_, **60**(11), 2313-2332.
*   Westbrook, L. (2008). Understanding crisis information needs in context: the case of intimate partner violence survivors. _Library Quarterly_, **78**(3), 237-261.