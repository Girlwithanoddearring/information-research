#### vol. 16 no. 4, December 2011

# Newspapers and their fear of channel spill-over: evidence for Europe

#### [Val?rie-Anne Bleyen](#authors), [Leo Van Hove](#authors)  
ECON Research Group, Vrije Universiteit Brussel (Free University of Brussels), Belgium

#### Abstract

> **Introduction.** Newspapers that have a Website with substantial content need to be sensitive to the danger that their print channel might be cannibalised by the online channel.  
> **Method.** We visited the Websites of all eighty-two national newspapers in eight Western European countries and listed their relevant characteristics. We posit that newspapers' use of _mixed bundling_ of print and online subscriptions and especially how they price such a bundle is an indicator of their fear of channel spill-over.  
> **Analysis.** We use binary logit and ordinary least squares regression analyses to explain newspapers' fear of channel spill-over by means of newspaper-, country- and market-specific variables.  
> **Results.** We find that newspapers have a tendency to do what other newspapers do. We find, for example, that newspapers will grant a higher discount for their bundle when the average discount granted by competitors is high. We also find that financial newspapers are more likely to opt for mixed bundling.  
> **Conclusions.** We find strong indications that online newspapers exhibit mimicking behaviour. However, these results are often difficult to disentangle from country effects such as the relative importance of advertising.

## Introduction

Newspapers that have a Website with substantial content need to make several crucial strategic choices. On the highest level, they need to decide whether to monetise their online content directly (by charging for it) or indirectly (by trying to attract advertising revenues). Newspapers that decide to charge also need to decide for which type and what proportion of content they will charge, in what packages this content will be made available (subscriptions, pay-per-view or both) and, crucially, how much to charge for each access option. When pondering these choices, newspapers also need to take into account that an online offering that overlaps with the print offering and/or is attractively priced might very well cannibalise the traditional newspaper. Well-balanced pricing can obviously help in stemming such channel spill-over, as will differentiating the content across the two channels, but bundling also becomes significant. Indeed, many newspapers offer a package that contains the print as well as the online version at a special price.

In a series of related papers, we examine several of the above decisions. We do this by means of a unique, self-constructed database with data on eighty-two national daily newspapers from eight Western European countries. In Bleyen and Van Hove ([2010b](#bley10b)), we analyse how these newspapers tackle the high-level, _free or fee_ dilemma. In Bleyen and Van Hove ([20010a](#bley10a)), we analyse why some newspaper Websites rely on subscriptions while others (also) offer pay-per-view. In this paper, we try to gauge and explain newspapers' fear of cannibalisation. Specifically, we analyse to what extent newspapers bundle their online and print subscriptions and how they price the bundle. The underlying thinking is that newspapers that offer a bundle and, particularly those that offer a bundle at a substantial discount, have a higher fear of channel spill-over.

On the newspaper level, we find that financial newspapers are more likely to opt for _mixed bundling_ and that the price of the online subscription has a positive impact on the percentage discount offered for the bundle. Conversely, a higher importance of advertising revenues in a given country lowers the discount. Intriguingly, this is just about our only result on the country level. At least part of the explanation for the inter country differences seems to lie more on the market level. We find clear indications of mimicking behaviour for both the decision to opt for _mixed bundling_ as well as for the level of the discount granted for a bundle containing the print and Web subscriptions, but not for the bundle containing the print and site subscriptions.

Besides its use of original data, the paper's main contributions are twofold. Firstly, it proposes novel (albeit imperfect) measures of newspapers' fear of channel spill-over and, secondly, it not only confirms the existence of inter-organizational imitation amongst online newspapers first revealed in Bleyen and Van Hove ([2010a](#bley10a), [2010b](#bley10b)), but actually strengthens the case, as the mimicking results presented in this paper are less open to criticism.

The remainder of the paper is structured as follows. The next Section summarises what the literature has to say about the issue of channel spill-over in the specific case of newspapers. It also provides selected anecdotal evidence on how established players have tackled it. The second Section develops our hypotheses. Section three describes how the data was collected and discusses the set-up of the analysis. This is followed by a presentation of the regression results. The final Section summarises our results, discusses the limitations of our analysis and offers suggestions for further research.

## Newspapers and channel spill-over

As pointed out in the Introduction, when a newspaper builds a Website with substantial content, it has to realise that there is always the danger of cannibalisation of the offline by the online channel ([Choi _et al._ 1997](#choi97)). Newspapers, therefore, should analyse the costs associated with each channel, as well as the nature of the demand. Where the latter is concerned, the key question is whether readers perceive the online version as a complement to or a substitute for the print version. In the first case, the online version will add to company profits by encouraging sales of the offline product. In the substitute case, revenue erosion is a danger and costs should be recovered through online fees and/or advertising. A problem in this respect is that per-reader revenue remains far smaller on the Web than for print ([Knowledge@Wharton 2009](#knowl09)).

Below we first summarise the academic evidence on whether online news and news on paper are complements, independent goods or substitutes. We limit ourselves to newspapers here; the working paper version of the present article also discusses evidence for magazines. The second subsection discusses what newspapers might do to stem the possible channel spill-over.

### How real is the cannibalisation threat?

Overall, it is '_widely assumed that the Internet is cannibalistic_' ([Porter 2001: 73](#porter01)). For the case of newspapers too, several authors argue that offering digital content will cannibalise print sales. However, the empirical evidence is mixed. Part of the explanation is that the studies use different methods and cover different time periods and/or countries. Also, in some cases, the online content is charged for, whereas in other cases it is _gratis_.

Several of the studies that point towards a complementary relationship rely on surveys. Chyi and Lasorsa ([2002](#chyi02)) find that online readers of six newspapers in Austin, Texas, were more, not less, inclined to read the same paper's print version. Chyi ([2004](#chyi04)) later conducted a similar survey for the top seven newspapers in Hong Kong. She finds, again, that there is significant overlap between online and print readership and that the relative number of users of print media among Web users are not reduced.

Turning to the Netherlands, de Waal _et al_. ([2005](#dewa05)) find that of those consulting an online newspaper, 88% also read a printed newspaper and 42% of those reading printed newspapers also consulted a newspaper site. In the case of Australia, Nguyen and Western ([2006](#nguy06)) find that users of Internet news use traditional media (such as TV, radio and newspapers) more frequently than non-users. For newspapers, the mean use figures are 3.99 times a week for users of online news versus 3.47 for non-users. Note, however, that unlike Chyi and Lasorsa ([2002](#chyi02)) and Chyi ([2004](#chyi04)), the analysis by de Waal _et al_. ([2005](#dewa05)) and Nguyen and Western ([2006](#nguy06)) is not on the level of individual newspapers.

The final survey in our overview relates to Spain. Flavian and Gurrea ([2007](#flav07)) find that people are more inclined to consult a digital newspaper to search for up-to-date, breaking news. Conversely, reading as entertainment or out of habit has a negative impact on the use of the digital medium. This again suggests that the print and online channels are complementary, provided that the two products are sufficiently differentiated.

Pauwels and Dans ([2001](#pauw01)) use a different approach and apply cross sectional and time series analyses to twelve Spanish newspapers in the period 1998-1999\. They find that circulation increases Website visits. However, they do not study the opposite impact (from Website visits on print circulation) and thus cannot provide conclusive evidence as to the degree of cannibalisation. Deleersnyder _et al_. ([2002](#dele02)), for their part, use data for 1990-2001 to analyse how the addition of a (mostly free) Internet channel, at different points during that period, affects the longer-run performance of eighty-five British and Dutch newspapers. They conclude that, at least at that point in time, the threat of cannibalisation is largely exaggerated: few newspapers in their sample experienced a significant drop in either print circulation or advertising revenues. For example, only nine newspapers experienced a significant reduction in their circulation income, compared to fifty-eight that did not. On analysing the product similarity of the print and online versions for these two groups, Deleersnyder _et al_. conclude, predictably, that cannibalisation will more likely take place when the Internet channel closely resembles the entrenched channel (and is thus, too much of a substitute).

Switching to the evidence that points towards a substitute relationship, Filistrucchi ([2005](#fili05)) uses monthly data on four leading Italian newspapers between 1976 and 2001 and finds that, in the short run, establishing a Website reduces a newspaper's print sales by approximately 3%. The long run impact is more drastic, at 26%.

Gentzkow ([2007](#gentz07)), for his part, uses consumer survey and media consumption data for the Washington DC market for the period 2000-2003 and finds that the online version of the _Washington Post_ reduced the Post's print readership by 27,000 daily readers or 1.5%. The estimated $33.2 million in revenue generated by the _Washingtonpost.com_ in 2002, came at a cost of $5.5 million in lost print profits. Taking into account the cost of operating the online division (estimated at $45 million a year), the result is a net loss (of roughly $17 million). However, between 2002 and 2004 online revenues almost doubled, resulting in a net _gain_ of $11.5 million in 2004\. Hence, Gentzkow ([2007: 740](#gentz07)) concludes that '_the initial losses of the online paper may have been an investment in anticipation of future market growth_'.

To sum up, the evidence regarding the danger of cannibalisation of newspapers' print by their online channel is mixed. A problem is also that several studies take a one-sided view. As mentioned, Pauwels and Dans ([2001](#pauw01)) disregard the impact of Website visits on print. Conversely, with the exception of Gentzkow ([2007](#gentz07)), papers that study the impact on offline revenues do not take into account possible online revenue streams.

### How could newspapers stem channel spill-over?

As documented above, the academic literature is not very conclusive as to the presence and magnitude of the cannibalisation threat. However, it does show that the free or fee nature of the Internet channel matters, as does content overlap. This generates two straightforward recommendations for newspapers wishing to avoid possible cannibalisation. The first is having a different version of the content across the two channels so that they become more separate offerings; the second is to give significant thought to pricing.

However, bundling or '_the practice of selling two or more distinct goods together for a single price_' ([Adams and Yellen 1976: 475](#adam76)), also enters into play. For instance, newspapers can offer a subscription that includes both the print and the online edition. When these are not made available for sale separately, this amounts to _pure bundling_ or _tying_, which obviously precludes cannibalisation altogether. In the other case, the approach is referred to as _mixed bundling_. The bundle can then be sold at a discount, which can be set according to the perceived threat of channel spill-over.

For a sample of ten German newspaper and magazine publishers, Stahl _et al_. ([2004](#stahl04)) analyse which bundling (and splitting) strategy maximises demand for paid online content and thus revenues. They find that publishers offering the same information bundle online and offline generate only 46% of the revenues generated by suppliers who only offer their unbundled single articles, and merely 8% of the revenues obtained by competitors who offer rebundled information goods (e.g., dossiers on a specific topic). In their model, Venkatesh and Chatterjee ([2006](#venk06)) find that the relative Web preference (measured as the ratio of consumers' reservation price for the online version to that for its print counterpart) matters too. When the Web preference is moderate to high, it is best to set high online prices and offer a wide product line (i.e., print + online bundle + unbundled online _modules_), rather than offering independent products. When the Web preference is low, publishers benefit from offering discounted online modules to complement the high priced print version.

Turning to some anecdotal evidence, the case of the _Arkansas Democrat Gazette_, a regional American newspaper, provides a salient illustration of the link between pricing and channel spill-over. The _Democrat Gazette_ places its online content behind a pay wall and, as a result, had only 3,500 online subscribers in 2009, compared to approximately 170,000 daily print readers. Walter Husmann, the paper's publisher, admits that the online subscription '_does not justify itself as a revenue stream_' ([The Economist 2009](#econ09)). According to _The Economist_, the _Gazette's_ pay wall is more of a '_revenue dam, intended to stop the flow of readers (and, thus, advertisers) away from print. Since 2002, when the paper began charging online, its average daily circulation has dropped by less than 1% a year ? rather better than most_' ([The Economist 2009](#econ09)).

The _Financial Times's_ approach to pricing is also interesting. For years, much of the online news was for paying subscribers only. However, in mid-October 2007, _FT.com_ pioneered an innovative charging system whereby occasional visitors could read five articles a month for free, after which they were asked to register. Once they clicked on thirty stories in a thirty day period, they were asked to subscribe. (In the meantime, the numbers have been lowered to one and ten, respectively.) The _Financial Times_ believed that partially opening up its online material would increase traffic to the site and thus also advertising revenues, without endangering online reader revenues. In a change in policy, _The New York Times_ announced that, starting in early 2011, it too will be introducing a _metered_ subscription service similar to _FT.com_ ([P?rez-Pe?a 2010](#perez10)). Subscribers to the print edition will continue to receive unlimited free access to the site ([P?rez-Pe?a 2010](#perez10)).

Continuing with the issue of bundling, it is interesting to observe that several established information providers have repeatedly altered their policies. _The Wall Street Journal_, for example, has different policies for different editions. Print subscribers to _The Wall Street Journal Europe_ and _The Wall Street Journal Asia_ are given complimentary online access. But subscribers to U.S. edition have to pay an additional fee to have _WSJ Online_ access included with their subscriptions. An interesting experiment is what one could call _daily bundling_. From January 2007, print copies of _The Wall Street Journal Europe_ contained coupons that gave buyers of single copies a day's free online access. The practice was discontinued in January 2009 (although if briefly resurfaced in May 2010).

To conclude, these and other real-life cases show that even established media outlets are uncertain about the optimal extent of bundling and, what to charge for it.

## Theory and hypotheses

We now present the theoretical underpinnings of our analysis and develop our hypotheses. In this paper we set out to gauge and explain newspapers' fear of cannibalisation. As explained above, newspapers that are afraid that readers will substitute the online version for the paper version will obviously have a higher propensity to charge for their online content. So, in principle, one could use the (relative) price of the online content as an indicator of channel-spill-over fear. But the decision to charge is clearly also driven by other considerations, such as the trade-off between reader and advertising revenues, to name but one ([Bleyen and Van Hove 2010b](#bley10b)). This is why we thought more clear-cut indications of channel-spill-over fear might be found in newspapers' _meta bundling_ behaviour; that is, in the way they bundle (or not) their print and online subscriptions. We use the term _meta bundling_ because subscriptions are bundles in their own right. Indeed, as is explained in Table 1 (below), under both pure and mixed bundling, newspapers actually offer a _bundle of bundles_.

More specifically, a crucial starting point of the paper was that newspapers that offer a meta-bundle and particularly those that offer such a bundle at a substantial discount, have a higher fear of channel spill-over. As explained above, the level of the discount can be set according to the perceived threat of channel spill-over. In what follows, we therefore try to explain two strategic decisions that online newspapers have to make (D<sub>1</sub> and D<sub>2</sub>). The first is a straightforward binary decision, namely the choice between a _mixed bundling_ and a _no bundling_ strategy. The second decision that we study is how newspapers that opt for mixed bundling set the level of the discount. In explaining the first decision we used binary logistic regression analysis and we tried an extensive set of explanatory variables, grouped into three broad categories: newspaper-specific, country specific and market specific. A similar set-up is used for D<sub>2</sub>, albeit with a continuous dependent variable and ordinary least squares regressions. In other words, our framework is of the following general form:

D<sub>i</sub> = a<sub>i</sub> + ?<sub>i</sub> newspaper + ?<sub>i</sub> country + d<sub>i</sub> market + e<sub>i</sub>, with i = 1, 2

We now introduce our hypotheses as they related to the category of explanatory variables.

### Newspaper-level hypotheses

Where newspaper characteristics are concerned, there is not much literature to guide us in our hypothesis development. However, an observation that is both obvious and important is that newspapers that have to decide whether to offer a bundle of subscriptions all have some sort of online subscription. Hence, they clearly value reader revenues (as opposed to advertising revenues) and especially revenues from regular readers. Building on this, if a high price for the print subscription goes in tandem with a higher reliance on reader revenues (which we do not know for certain) then expensive newspapers might be more afraid to lose these revenues. An indication in this direction is that in our sample, the average price of the print subscription _per country_, which is a variant on AVGP (the average such price _per market_), is negatively correlated with ADVERT-CONTRIB, the contribution of offline advertising to daily newspapers' revenues; the Pearson r is -0.494<sup>***</sup> (N = 72). Hence, our first hypothesis reads:

> **Hypothesis 1**: More expensive newspapers are more likely to bundle print and online subscriptions (D<sub>1</sub>) and to offer a higher discount for this bundle (D<sub>2</sub>).

Building on this, it is interesting to observe that financial and quality newspapers are more expensive than other newspapers. In our sample, the average price of an annual print subscription for a financial newspaper is ?354, compared to only ?241 for other newspapers (N = 48; Levene's _t_ = -3.42, _p_ <0.01). Quality newspapers also command a higher than average price offline. In our sample, the average print subscription for a quality newspaper costs ?284, compared to ?240 for other newspapers (N = 57; Levene's _t_ = -2.61, _p_ <0.05). Hence our following two hypotheses read:

> **Hypotheses 2 and 3**: Financial and quality newspapers are more likely to bundle print and online subscriptions (D<sub>1</sub>) and to offer a higher discount for this bundle (D<sub>2</sub>).

Looking for evidence to underpin these hypotheses, it is tempting, but admittedly speculative, to link Flavian and Gurrea's ([2007](#flav07)) finding that (Spanish) consumers are more inclined to consult a newspaper site for breaking news with the observation often advanced by media commentators who suggest that financial newspapers have information that is more time- and business critical. A recent survey carried out by PricewaterhouseCoopers ([2009](#price09)) in cooperation with the World Association of Newspapers reinforces this concept. The survey estimated readers' willingness to pay for content on various platforms. It involved more than 4,900 consumers in seven countries, of which we cover four. It was shown that if the willingness to pay for high-quality content on paper is set at 100%, respondents were only willing to pay a maximum of 62% for the same type of news online. But, interestingly, respondents preferring financial content expressed a relatively high willingness to pay: for a high-quality online newspaper with a focus on finance they were prepared to pay, on average, as much as 97% of what they would pay for high quality general news on paper. However, the survey did not examine the issue of whether the higher willingness to pay for the online content of financial newspapers implied a reduced interest in their print content; in other words, whether it implied (stronger) channel spill-over.

### Country-level hypotheses

Over the past decade or so newspapers moved to the fee model when online advertising expenditure was down and moved back to the free model when the market revived. The rationale is obvious: higher (anticipated) advertising revenues lower the need to monetise content directly. A lower reliance on reader revenues should also reduce newspapers' fear of channel spill-over, provided that they think they can reap similar advertising revenues online and offline.

Unfortunately, newspaper-level data documenting online advertising revenues are not publicly available. However, we do have _country_-level data about the relative importance of advertising (see the Section on data collection for details). Hence, our (second-best) hypothesis is of the following form:

> **Hypothesis 4**: The higher the relative importance of (offline) advertising for newspapers in a given country, the less likely newspapers with online subscriptions are to opt for mixed bundling of print and online subscriptions (D<sub>1</sub>) and the less likely they are to apply a high discount (D<sub>2</sub>).

Still on the country level, we also wanted to take into account the possible impact of Internet penetration and/or usage. However, we had no clear assumption about the sign of the impact. Potentially, higher Internet usage in a given country may lead to a cannibalisation threat, or at least the perception of one. Conversely, in periods when the online advertising market is healthy, more online visitors should bring higher advertising revenues, so newspapers might be less concerned about channel spill-over. Hypothesis 5 is thus deliberately phrased vaguely:

> **Hypothesis 5**: Internet penetration and/or usage have an impact on a newspaper's online strategy (D<sub>1-2</sub>).

### Market-level hypotheses

Turning to market-specific variables, there is substantial anecdotal evidence that newspapers monitor each other's strategic decisions. For example, in an article about the _The New York Times_'s recent announcement that it will introduce a _metered_ model in 2011, _NYT_ journalist Richard P?rez-Pe?a points out that any such '_changes are sure to be closely watched by publishers and other purveyors of online content_' ([P?rez-Pe?a 2010](#perez10)). Commenting on the same announcement, Gordon Crovitz, a co-founder of _Journalism Online_ (a separate, multi-publisher content platform), said the _NYT_ move '_would embolden other publishers to follow it_' ([P?rez Pe?a 2010](#perez10)).

We looked, therefore. into the literature on inter-organizational imitation. In a recent survey article, Ordanini _et al_. ([2008](#ordan08)) distinguish six different theoretical streams, each focussing on a different driver of imitation. In macro-micro order of appearance, firms copy: to gain legitimacy for their behaviour (neo-institutional theory); because they believe that other firms have superior information (information cascades models); to limit rivalry (Industrial organization); to let others bear the costs and risks of experimentation (organizational learning theory); to acquire missing capabilities and/or enhance existing resources (the resource-based view of strategic management); or to avoid search, evaluation and selection costs when pondering different strategies (decision-making theory).

Initially, we were inclined to frame the copycat behaviour of online newspapers primarily in the strategic interaction perspective of the Industrial organization literature. Indeed, a newspaper that, in a market where all other sites are free, is the first to introduce, say, an online subscription might suffer reduced profits. If competing sites also have subscriptions, this danger is smaller. Statements by publishers that can be framed into this strategic interaction view abound. For example, _News Corporation_'s Rupert Murdoch announced on August 5, 2009, that he planned to charge for all his online newspapers by the summer of 2010\. Commenting on this move, one U.K. newspaper executive said: '_Murdoch is either going to make the market or steer a lot of users our way. There is very little benefit for anyone being the first mover_' ([Financial Times 2009](#financial09)). However, as is explained in Bleyen and Van Hove ([2010b](#bley10b)), the alternative perspectives mentioned above are also potentially relevant, with the exception of the resourced-based view of strategic management (which mainly analyses imitation from the point of view of the target firm). But regardless of the theoretical framework that is deemed most relevant, the general form of the hypothesis is very similar:

> **Hypothesis 6**: When making strategic decisions, online newspapers are more likely to do what their competitors do (D<sub>1-2</sub>).

## Data collection and methodology

In this Section we explain how we have collected our data and how we have operationalised the above hypotheses. During the period June-July 2006, we visited the Websites of all national newspapers with a daily circulation in eight Western European countries and listed all relevant characteristics. More specifically, our exploration included six sites in Luxembourg, eight sites in both the Netherlands and Spain, nine in Belgium, ten each in Germany and the United Kingdom, eleven in France and a total of twenty in Italy. In selecting these newspapers, we mainly relied on the list of national daily paid-for newspapers in the report of the World Association of Newspapers ([2006](#world06)). For more specific data we contacted the national co ordinating organizations, with the exception of Spain, for which we relied on the portal site [www.spain-newspaper.com](http://www.spain-newspaper.com). The list of examined newspapers is available from the authors.

Regional and local newspapers were not included because of their specific orientation and generally smaller market share. National newspapers aimed at an extremely specific audience also fall outside the scope of our research. In France, for example, _Bilto_, _La Gazette des Courses_ and _Matin Courses_ are intended for fans of horse betting and _Quoti_ is a newspaper for children. Freely distributed newspapers such as the _Metro_ are also excluded. Such _freesheets_ typically only generate revenue through advertising and thus do not have to make the strategic decisions analysed in this paper. As for the choice of countries, we were limited by our linguistic capacities.

We now clarify the definitions of the variables used in our analysis. Descriptive statistics for all variables, together with data sources, are available in Tables [1a](#table1a) and [1b](#table1b) in the Appendix.

### Dependent variables

As explained, the first decision that we analyse in this paper, D<sub>1</sub>, relates to our newspapers' meta-bundling behaviour; that is, the way in which they bundle their online subscriptions. In principle, we discern three possibilities, as set out below. In practice only one newspaper in our sample opted for pure bundling. We excluded this newspaper, thus turning D<sub>1</sub> into a binary decision for which we use binomial logit analysis.

> #### Tying or pure bundling
> 
> There is a pay wall but there is no separate online subscription. Either it can only be purchased as part of a package that also contains the print subscription _or_ only print subscribers have access to the _online_ facilities (at no additional cost). Importantly, since we focus on the access to online content, we disregard whether the print version is available separately or not.
> 
> #### Mixed bundling
> 
> The print and online versions are offered for sale individually, but can also be bought as a bundle. When print subscribers get free access to the online news, we also consider this to be mixed bundling (albeit with a very large _discount_) on condition that the online version is also available as a stand-alone product. Otherwise the newspaper will show up in the first category.
> 
> #### No bundling
> 
> The online subscription is only available as a stand-alone product. Print subscribers pay the full price. To be clear: newspapers that do not offer any online subscription formula are excluded from our analysis.
> 
> Note that the term _online subscriptions_ is used in a broad sense here; that is, as encompassing both site subscriptions as well as so-called PDF subscriptions.
> 
> Note that the precise content of an online subscription can vary widely from one site to another. This is why we decided to split _online subscriptions_ into site and PDF subscriptions. A site subscription gives the reader access to the site for a certain period of time. It may or may not include access to a searchable archive, a PDF version of current and/or past editions of the paper, columns, etc. However, it is also possible that the site subscription does not include the PDF version and that readers who want it have to take a separate PDF subscription. This is not true the other way around: a PDF subscription only includes a digital version of the paper of the day (or of the past couple of days).

Apart from our newspapers' meta-bundling behaviour, we also looked at the prices they charge for their subscriptions. We first calculated ratios between the price of the online and print subscriptions. We have done this both, albeit separately, for site and PDF subscriptions (see above for definitions). The underlying assumption is that, for a given breadth of online content (which we have not measured), the higher the ratio, the more the attractiveness of the online channel is limited, which may indicate a higher fear of cannibalisation. However, none of our regressions with these rough indicators yielded meaningful results. We therefore examined newspapers that apply mixed bundling separately and calculated the percentage discount:

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td style="text-align:center;">(price of print subscription + price of online subscription) - price of bundle

* * *

price of print subscription + price of online subscription</td>

</tr>

</tbody>

</table>

In this expression, _online subscription_ alternatively means PDF (or whatever format is adopted) and site subscription. Clearly, the higher the discount, the lower the probability that a print subscriber will _defect_ - that is, opt for the online version alone.

In our view, this is the better (though by no means perfect) indicator of channel spill-over of the three that we investigate. Importantly, we were unable to control for the breadth and nature of the content that is offered online, whereas our newspapers may well have invested in differentiating their online from their print content. This undermines to some extent the assumption underlying our second indicator that the more expensive the online version is compared to the print version, the more a newspaper fears cannibalisation. Indeed, a newspaper with differentiated online content can have both less reason to fear channel spill-over as well as more room to charge a high price for its online content. Thus, there need not be a direct correspondence between the price that a newspaper charges for its online content and its fear of channel spill-over. Our third indicator, the discount for a print+online-bundle, is also about pricing of online and offline content, but is _not_ invalidated by this criticism, on the contrary.

<div align="center">![content overlap and channel differentiation](p498fig1.png)</div>

<div align="center">  
**Figure 1: Content overlap and differentiation between channels**</div>

Consider, in Figure 1, a newspaper that charges ?200 a year for a print subscription. Suppose, first, that this newspaper does not offer any of its print content online and only has dedicated content on its Website (Scenario 1). Such a newspaper clearly has no reason to fear channel spill-over, as the Website is fully complementary to the printed paper. As a result, it does not need to offer a discount for print subscribers and has ample room to charge for access to the site, say by means of a site subscription. Suppose that our newspaper charges ?50 a year for the online content, with print subscribers paying the full price.

Let us then turn to Scenario 2 and suppose that, besides offering original online content, the newspaper starts to also replicate part of its print content online; say half, which would be worth ?100 in print. This obviously increases the danger of channel spill-over, especially if one assumes, to keep matters simple, that readers exhibit no print (nor Web) preference; in other words, readers are assumed to be indifferent as to the channel through which they access a specific piece of content. The newspaper can react to this danger by charging (more) for its site subscription (to a maximum of ?100), while offering a discount of, say, ?100 for print subscribers (for whom only the online only content has real value added).

The crux of this example is that while the price of the site subscription (?50 initially, ?150 in the second scenario) is driven both by content differentiation _and_ by channel spill-over fear (thus, by both _Online-only content_ and the _Overlap_), the level of the discount (zero initially, ?100 in the second scenario) is only driven by latter (see Figure 1). Hence, any ratio of online and offline prices will only be a good indicator of cannibalisation fear in the absence of online-only content, whereas the percentage discount does not suffer from this handicap. In other words, using the discount rather than the ratio makes it possible to eliminate the disturbing influence of dedicated online content.

This said, our explanation and, in particular, our numerical example, might raise the following interrelated questions: (1) is the discount not just an emanation of _fair pricing_ (as it would be unfair to make print subscribers pay twice for the same content) and (2) given the numbers in Scenario 2, why would the newspaper fear cannibalisation at all? If the newspaper duplicates ?100 worth of its print content online, would it not be indifferent to getting paid ?100 for that content in print or online? Might it not even prefer the latter (because of lower production and distribution costs)?

Giving print subscribers the duplicated content for free would indeed seem to be a prerequisite for being able to charge them (?50 in our example) for the exclusive online content. Not many print readers will be willing to pay, on top of their print subscription, the full price of the online subscription just to get access to ?50 worth of online-only content. However, jumping to the second question, there are definitely reasons why our newspaper might prefer its print subscribers _not_ to opt for the online subscription. With the numbers in Scenario 2, for every print subscriber who thinks that paying ?150 for the online subscription is a better offer than paying ?200 for the print subscription, the newspaper loses ?50 in reader revenues. Hence, it has every reason to make the bundle attractive.

Secondly, in our example we assume that the willingness to pay for a given piece of content is the same whether it is presented on paper or online. There is no certainty about this. As mentioned above, a recent survey by PricewaterhouseCoopers ([2009](#price09)) estimated the willingness to pay for high-quality online content at only 62% of the willingness to pay for the same type of news on paper. Thus, it is possible that our newspaper, when it offers ?100 worth of its print content online may not be able to charge ?100 for that content online, thus further increasing the losses.

An interesting illustration is the implementation of a pay wall, on July 2, 2010, by _The Times_ in the U.K. at ?1 for a day's access and ?2 for a week. Sensible online readers would thus pay ?2 a week or about ?8.50 a month. A reader of the print version who buys _The Times_ every day of the week and _The Sunday Times_ on Sunday spends ?8.50 _a week_ ([Beehivecity 2010](#beeh10)).

Finally and perhaps most importantly, newspapers have to optimise more than one revenue stream. John Hartigan, CEO of _News Ltd_, _News Corporation_'s Australian brand, points out that, broadening the perspective to include advertising revenues, '_an online reader generates about 10 per cent of the revenue we can make from a newspaper reader. So, for every reader we lose from the paper we need to pick up 10 online_' ([WAN-IFRA 2010: 18](#wanifra10)). Group M, the media buying agency of media group WPP, states that '_the bulk of news surfers are _useless tourists_ who not only pay nothing but have little advertising potential_' ([Gapper 2010](#gapp10)). If a migration of readers to the online channel results in lower overall advertising revenues, this is clearly an important reason to fear such migration.

In short, there are clear reasons why, with the numbers in Scenario 2, our newspaper might fear cannibalisation of its print version and want to make its bundle attractive by offering a discount that goes further than just _fair pricing_. In our example, the newspaper might lower the price of the bundle to, say, ?230\. It could then be argued that the best measure of fear of cannibalisation is the difference between the fair price (?250) and the real price (?230). From this perspective, a problem is that we have not measured the overlap between the online and print content and thus cannot determine whether the prices and discounts that we observe are driven only by fair pricing. Also, risk attitudes may differ across publishers.

### Explanatory variables

As explained above, on the newspaper level we mainly look at the _genre_ and _type_ of newspaper. In both cases, we contacted the national co-ordinating organizations in order to obtain a certified classification. Where genre is concerned, we initially distinguished four categories, namely _general_, _sports_, _financial_ and _other_ (religious, political, etc.), but ended up using only a dummy variable for FINANCIAL (as this gave the best results). For type, most professional organizations simply distinguish _quality_ from _popular_ newspapers. Only the Newspaper Society in the U.K. has a third, _mid-market_ category. In the construction of our dummy variable for QUALITY, the mid-market papers (which number only two) were placed under _popular_. Another variable on the newspaper level is the price of the annual print subscription. We use both the unscaled price, denoted as P(PRINT) and a scaled variable, P(PRINT)/AVGP, with AVGP the average price of a print subscription in the relevant market. (What we understand by _market_ and how it differs from _country_ is explained below.) The benefit of using the scaled variable is that it allows for separation of newspaper- and market-specific effects.

Where advertising is concerned, in the absence of newspaper-level data we used country-level data. Specifically, we tried three alternatives: the _contribution of offline advertising to daily newspapers' revenues per country_ (ADVERT CONTRIB); _newspapers' share in total advertising expenditure_ in the country (ADVERT-%EXP-NP); and _advertising revenues generated by paid-for dailies per country_ (in ?, scaled by GDP per capita) (ADVERT-?REV-NP). In a bolder attempt, we also introduced the _share of the Internet in total advertising expenditure_ in the country (ADVERT-%EXP-NET) and the _level of advertising expenditure on the Internet per country_ (in ?, scaled by GDP per capita) (ADVERT ?EXP-NET). The underlying intuition for the introduction of the latter variables was that perhaps newspaper sites have a lower cannibalisation fear because they _think_ they will be able to capture part of the bigger Internet advertising pie, but do not necessarily succeed. (In 2007, the bulk of the rapid increase in online advertising revenue went to portals, search engines and aggregators such as Google News ([Isaacson 2009](#isaac09)).) Also on the country level, we included _Internet access_ as a percentage of the population (%INTERNET) and _mean weekly hours spent on Internet_ (MEANHOURSNET).

Finally, we devoted special attention to the mimicking variables because the different theories of inter-organizational imitation discussed above require, at least to a certain extent, different operational measures. A useful distinction is the classification into _frequency-based_ imitation, where the most widespread behaviour is copied; _trait-based_ imitation, where the behaviour of specific groups of firms is copied larger firms or firms in the same strategic group); and _outcome-based_ imitation, where the best performing players are copied ([Haunschild and Miner 1997](#hauns97); [Ordanini _et al._ 2008](#ordan08)). Since we have no information on the success of the strategic decisions taken by our newspapers, we cannot test for outcome-based imitation and hence focus on frequency- and trait-based imitation, at least for D<sub>1</sub>.

In terms of operationalisation, frequency-based imitation implies that it matters whether a technology or a decision is heavily diffused in the market or not. Therefore, we tallied _how many_ newspapers in a given country follow a specific strategy and divided that number by the total number of newspapers in that country. The goal of this was to test, across countries, whether the local _popularity_ of a strategy affects newspapers' behaviour. However, we also wanted to give a trait-based flavour to some of our mimicking variables. Indeed, Terlaak and Gong ([2008: 847](#terlaak08)) point out that, from an organizational learning perspective, it may make sense for an organization to look at '_referent organizations that are _comparable__'. Since one of our assumptions in Bleyen and Van Hove ([2010b](#bley10b)) was that quality newspapers might very well have an easier time charging for content, we singled out _type_ as a relevant trait and explicitly allowed for the possibility that our newspapers' reference group could be limited to newspapers in the same segment (quality versus popular). In other words, the assumption is that quality newspapers will only look at what the other quality newspapers in the same country do. But this choice of reference group can be framed equally well in an industrial organization perspective. As Ordanini _et al_. ([2008: 384](#ordan08), emphasis added) put it: '_., the best way to reduce competition is to copy the decisions of _**similar**_ players_'.

Overall, our mimicking variables differ in three dimensions (which build on one another), so that the variables number eight?(2<sup>3</sup>) in total. In a first dimension, as an alternative to computing relative numbers of organizations, we also use market shares of the target firms, the additional assumption being that larger newspapers exert more influence on others. From their organizational learning perspective, Terlaak and Gong point out that firm size 'can be a proxy that is indicative of the referent firms' capabilities to identify superior practices' ([2008: 846](#terlaak08)), so that it may make sense to copy larger organizations. But, needless to say, looking at market shares makes obvious sense in an Industrial organization perspective too (since larger newspapers have more market power). A second dimension of our mimicking variables is related to the question of whether the referent firm itself should be included or not in the calculations (of either numbers or market shares). We have tried both options and will discuss the ramifications when presenting specific results below. In a third dimension, we allowed for the possibility that our newspapers' reference group could be all newspapers in the country _or_ only those in the same segment (either quality or popular), as explained above.

These three dimensions are reflected in our notations for the mimicking variables in D<sub>1</sub>. The notations consist of four parts, with the first part indicating whether we use relative numbers (#) or market shares (%), followed by an abbreviation of the strategy concerned (that is, MIXED for mixed bundling), ALL or COMP (for competitors) to specify whether the newspaper itself is included or not and MARKET or SEGM (for quality or popular) to indicate the level of analysis. For example, #MIXED-COMP-SEGM refers to the relative number of competitors in the same market segment that have opted for mixed bundling.

For our second decision (where we try to explain the level of the bundle discount) we needed different mimicking variables. In a first alternative we simply computed the average discount granted by competitors in the same country (AVDISC-COMP-MARKET). In a second alternative, we included the newspaper itself (AVDISC-ALL-MARKET). Also, as mentioned above, from both the organizational Learning and Industrial organization perspectives it is possible that bigger firms (that is, firms with a higher market share) exert more pressure on others. This is why, in a third and fourth alternative, we calculated a _weighted_ average of the discount that all newspapers/competitors with a PDF or site subscription offer (WAVDISC-ALL-MARKET and WAVDISC-COMP-MARKET, respectively). Unfortunately, as will become clear in the next Section, the number of observations was too low to also perform these calculations on the segment level in a meaningful way. In other words, we were unable to test for trait-based imitation in D<sub>2</sub>.

Importantly, in constructing all the above variables, we have assumed that newspaper markets are national, with one exception. For the case of Belgium we have assumed that language defines the market borders and that Dutch or French language newspapers only look at their Dutch or French language counterparts. This focus on local competitors is clearly industrial organization-inspired, but is also compatible with the information cascades theory.

## Empirical results

We now present our regression results, decision by decision. In each subsection, the results are preceded by selected descriptive results, in order to give the reader a feel for the data?set. As mentioned, for D<sub>1</sub> (the choice between mixed and no bundling) we used binary logit analysis. In D<sub>2</sub> (the discount applied under mixed bundling), the dependent variable is of a continuous nature and we use ordinary least squares.

### Meta-bundling

Our descriptive results show that pure bundling is not popular as a meta-bundling strategy: only one (Belgian) newspaper (1.2% of all newspapers) applies it. Twenty-seven newspapers (32.9%) opt for _mixed bundling_ and twenty-nine (35.4%) choose _no bundling_. On a per country basis, the differences are particularly salient. In the U.K., for example, 70% of the newspapers fall into the category _no bundling_ and only 10% choose _mixed bundling_. By contrast, in Germany, _mixed bundling_ strategies (60%) have a much stronger presence than _no bundling_ (10%).

In Table 2, we attempt to explain our newspapers' choice between mixed and no bundling. An obvious but important remark is that we only consider newspapers that have online subscriptions, since the other Websites, including those that are completely free, simply have no bundling decisions to make. Recall that we have also excluded the one newspaper that opts for pure bundling.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2 - The choice between no and mixed bundling**</caption>

<tbody>

<tr>

<th rowspan="3">VARIABLES</th>

<th colspan="2">Model I</th>

<th colspan="2">Model II</th>

<th colspan="2">Model III</th>

<th colspan="2">Model IV</th>

<th colspan="2">Model V</th>

<th colspan="2">Model VI</th>

<th colspan="2">Model VII</th>

</tr>

<tr>

<td>B</td>

<td>Exp (B)</td>

<td>B</td>

<td>Exp (B)</td>

<td>B</td>

<td>Exp (B)</td>

<td>B</td>

<td>Exp (B)</td>

<td>B</td>

<td>Exp (B)</td>

<td>B</td>

<td>Exp (B)</td>

<td>B</td>

<td>Exp (B)</td>

</tr>

<tr>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

</tr>

<tr>

<th colspan="15" align="left">Newspaper-specific</th>

</tr>

<tr>

<td rowspan="2">P(PRINT)</td>

<td>0.013<sup>**</sup></td>

<td>1.013</td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="4"> </td>

</tr>

<tr>

<td>0.006</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">P(PRINT)/AVGP</td>

<td colspan="2" rowspan="4"> </td>

<td>1.112</td>

<td>3.041</td>

</tr>

<tr>

<td>1.660</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">FINANCIAL</td>

<td colspan="2" rowspan="2"> </td>

<td>1.834<sup>*</sup></td>

<td>6.259</td>

<td>2.211<sup>**</sup></td>

<td>9.126</td>

<td>1.832<sup>**</sup></td>

<td>6.245</td>

<td>2.134<sup>**</sup></td>

<td>8.449</td>

<td>2.620<sup>**</sup></td>

<td>13.737</td>

</tr>

<tr>

<td>1.030</td>

<td> </td>

<td>0.961</td>

<td> </td>

<td>0.907</td>

<td> </td>

<td>0.962</td>

<td> </td>

<td>1.091</td>

<td> </td>

</tr>

<tr>

<th colspan="15" align="left">Country-specific</th>

</tr>

<tr>

<td rowspan="2">AVGP</td>

<td colspan="2" rowspan="2"> </td>

<td>0.022<sup>**</sup></td>

<td>1.022</td>

<td>0.050<sup>**</sup></td>

<td>1.052</td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="4"> </td>

</tr>

<tr>

<td>0.010</td>

<td> </td>

<td>023</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">%INTERNET</td>

<td>0.110<sup>*</sup></td>

<td>1.116</td>

<td>0.167<sup>**</sup></td>

<td>0.292<sup>**</sup></td>

<td>0.292<sup>**</sup></td>

<td>1.340</td>

<td>0.021</td>

<td>1.021</td>

<td>0.028</td>

<td>1.028</td>

<td>0.048</td>

<td>1.049</td>

</tr>

<tr>

<td>0.058</td>

<td> </td>

<td>0.083</td>

<td>0.119</td>

<td>0.119</td>

<td> </td>

<td>0.039</td>

<td> </td>

<td>0.038</td>

<td> </td>

<td>0.043</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">MEANHOURSNET</td>

<td colspan="2" rowspan="6"> </td>

<td colspan="2" rowspan="6"> </td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="4"> </td>

<td>-0.341</td>

<td>0.711</td>

</tr>

<tr>

<td>0.634</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">ADVERT-CONTRIB</td>

<td>0.002</td>

<td>1.002</td>

<td>0.010</td>

<td>0.990</td>

<td>0.037</td>

<td>1.038</td>

</tr>

<tr>

<td>0.072</td>

<td> </td>

<td>0.075</td>

<td> </td>

<td>0.227</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">ADVERT-?REV-NP</td>

<td>0.000</td>

<td>1.000</td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

<td>0.000</td>

<td>1.000</td>

<td colspan="2" rowspan="2"> </td>

</tr>

<tr>

<td>0.000</td>

<td> </td>

<td>000</td>

<td> </td>

</tr>

<tr>

<th colspan="15" align="left">Market-specific</th>

</tr>

<tr>

<td rowspan="2">#MIXED-COMP-MARKET</td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="4"> </td>

<td>0.026<sup>**</sup></td>

<td>1.026</td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="6"> </td>

</tr>

<tr>

<td>0.012</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">#MIXED-COMP-SEGM</td>

<td colspan="2" rowspan="6"> </td>

<td>0.014</td>

<td>1.014</td>

</tr>

<tr>

<td>0.010</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">%MIXED-COMP-MARKET</td>

<td>-0.010</td>

<td>0.990</td>

<td>-0.021</td>

<td>0.979</td>

<td>-0.094</td>

<td>0.911</td>

<td colspan="2" rowspan="4"> </td>

<td>0.035<sup>**</sup></td>

<td>1.035</td>

</tr>

<tr>

<td>0.019</td>

<td> </td>

<td>0.023</td>

<td> </td>

<td>0.057</td>

<td> </td>

<td>0.016</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">%MIXED-COMP-SEGM</td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

<td>0.037<sup>**</sup></td>

<td>1.038</td>

</tr>

<tr>

<td>0.018</td>

<td> </td>

</tr>

<tr>

<th rowspan="2" align="left">Intercept</th>

<td>-8.213<sup>**</sup></td>

<td>0.000</td>

<td>-14.133<sup>**</sup></td>

<td>0.000</td>

<td>-25.661<sup>**</sup></td>

<td>0.000</td>

<td>-2.980</td>

<td>0.051</td>

<td>-2.980</td>

<td>0.051</td>

<td>-3.332</td>

<td>0.036</td>

<td>-2.913</td>

<td>0.054</td>

</tr>

<tr>

<td>3.409</td>

<td> </td>

<td>5.934</td>

<td> </td>

<td>10.265</td>

<td> </td>

<td>4.448</td>

<td> </td>

<td>4.448</td>

<td> </td>

<td>2.210</td>

<td> </td>

<td>11.005</td>

<td> </td>

</tr>

<tr>

<th align="left">Nagelkerke R Square</th>

<td colspan="2">0.316</td>

<td colspan="2">0.310</td>

<td colspan="2">0.488</td>

<td colspan="2">0.288</td>

<td colspan="2">0.221</td>

<td colspan="2">0.323</td>

<td colspan="2">0.486</td>

</tr>

<tr>

<th align="left">Cox & Snell R Square</th>

<td colspan="2">0.233</td>

<td colspan="2">0.230</td>

<td colspan="2">0.366</td>

<td colspan="2">0.215</td>

<td colspan="2">165</td>

<td colspan="2">0.241</td>

<td colspan="2">0.353</td>

</tr>

<tr>

<th align="left">-2 Log Likelihood</th>

<td colspan="2">46.290</td>

<td colspan="2">45.723</td>

<td colspan="2">34.418</td>

<td colspan="2">52.278</td>

<td colspan="2">55.126</td>

<td colspan="2">49.396</td>

<td colspan="2">29.337</td>

</tr>

<tr>

<th align="left">No. of observations</th>

<td colspan="2">43</td>

<td colspan="2">42</td>

<td colspan="2">37</td>

<td colspan="2">46</td>

<td colspan="2">46</td>

<td colspan="2">45</td>

<td colspan="2">34</td>

</tr>

<tr>

<th align="left">Chi-squared</th>

<td colspan="2">11.423</td>

<td colspan="2">10.969</td>

<td colspan="2">16.848</td>

<td colspan="2">11.143</td>

<td colspan="2">8.295</td>

<td colspan="2">12.430</td>

<td colspan="2">14.812</td>

</tr>

<tr>

<th align="left">Overall significance</th>

<td colspan="2">0.010</td>

<td colspan="2">0.027</td>

<td colspan="2">0.005</td>

<td colspan="2">0.025</td>

<td colspan="2">0.081</td>

<td colspan="2">0.014</td>

<td colspan="2">0.005</td>

</tr>

<tr>

<td colspan="15">Note: Superscripts <sup>*</sup>, <sup>**</sup> and <sup>***</sup> indicate that the coefficient is significantly different from zero at the 10%, 5% and 1% level, respectively.</td>

</tr>

</tbody>

</table>

On the newspaper level, two variables stand out as having a significant impact, namely the unscaled _price of a print subscription_ and, in particular, whether the newspaper is a financial newspaper. These two results are linked because FINANCIAL on the one hand and P(PRINT) and P(PRINT)/AVGP on the other are strongly correlated. However, P(PRINT) in Model I primarily seems to capture a country effect, because when it is _decomposed_, in Model?II, into P(PRINT)/AVGP and AVGP, the latter is significant but the first is not. _Financial_ newspapers are more eager to adopt mixed bundling and thus appear to have, all else being equal, a greater fear of channel spill-over (Models III-VII), in line with Hypothesis 2\. On the other hand, the quality dummy is never significant. Hypothesis 3 (probably not coincidentally the hypothesis that was least well underpinned) is thus not supported. As an aside, if a recent survey by the Boston Consulting Group ([2009](#bcg09)) is to be believed, by adopting mixed bundling financial newspapers cater to the wants of their readers. According to the survey, 52% of U.S. consumers of business news would be interested in a bundled print-and-online subscription, compared with just 35% of consumers of news for '_young and personal use_'.

Leaving the newspaper level, the prime observation in Table 2 is that there is indeed a country/market effect, which, interestingly, is alternatively captured by the (average) price of a print subscription - P(PRINT) and AVGP in Models I-III, alongside %INTERNET - _or_ by our mimicking variables (Models IV, VI and VII). Reassuringly, the models remain completely intact when the newspaper itself is also included in the calculations of the mimicking variables; that is, when #MIXED-COMP-MARKET is replaced by #MIXED-_ALL_-MARKET and so on (models not reported). Stronger still, the mimicking variable in Model V is now also significant, at the 1% level. In Models IV and VI, the significance of the mimicking variable jumps from the 5% to the 1% level. Mimicking variables that consider all newspapers with a specific strategy are difficult to justify in the information cascades, organizational learning and decision-making frameworks that posit that firms try to capture the experience of _other_ organizations. However, as explained in Bleyen and Van Hove ([2010b](#bley10b)), mimicking variables of the ...-ALL-... type can be justified from an industrial organization or an neo-institutional theory perspective.

This said, a potential problem with our analysis in Table 2 is that when the ...-COMP-... mimicking variables are introduced, in Models?IV-VII, none of the variables on the country level remain significant. The technical explanation is that AVGP is strongly correlated with all four mimicking variables; the Pearson correlations lie between .392<sup>***</sup> and as much as .693<sup>***</sup>. The analytical question is whether what we find in models IV, VI and VII in Table 2 (and for Models IV-VII with ...-ALL-... mimicking variables) is really mimicking behaviour or whether the variables might simply capture a country effect that is not (adequately) captured by our country-specific variables. For some of our mimicking variables, this danger is real because in practice they act as sophisticated country dummies. Indeed, the value for #MIXED-ALL-MARKET is the same for all newspapers in a given country. Suppose that there are ten national newspapers in country _i_ and that six of them opt for mixed bundling and the remaining four for _no bundling_. In such a setting, the value for #MIXED-ALL-MARKET equals 6/10 for _all_ newspapers in the country. However, this is (more or less progressively) less the case and hence less of a problem for the mimicking variables that exclude the newspaper itself (...-COMP-...), that count the (relative) number of newspapers in the same segment (...-SEGM), that exclude the newspaper itself _and_ are situated on the segment level(...-COMP-SEGM), that consider market shares instead of numbers (%...) and so on. From this perspective, the fact that seven out of the eight mimicking variables yield significant results does seem to indicate that there is indeed mimicry among online newspapers. A drawback is that it is not possible to discriminate between the possible drivers for such behaviour mentioned in the methodology Section. Whereas the evidence in Bleyen and Van Hove ([2010b](#bley10b)) primarily seems to point in the direction of strategic interaction (and hence the Industrial organization framework), here the explanation might equally well lie in, say, the taken-for-grantedness of the Neo-Institutional Theory, which can be considered to mean that a new product, process or service is simply not questioned anymore and generally adopted ([Aldrich & Fiol 1994](#aldr94); [Haunschild & Chandler 2008](#hauns08)).

In Table 2 the number of observations varies because of missing values for some of the variables. We have therefore repeated our regressions with overlapping sets. In putting together these overlapping sets, we ignored MEANHOURSNET (and thus Model VII), as this variable would have reduced the number of observations too drastically. The results with overlapping sets are not so reassuring: in Models I, IV and VI none of the variables remain significant. In Model II, the significance of %INTERNET drops to 10%. (Conversely, in Model V, %INTERNET becomes significant at the 5% level and so does ADVERT-CONTRIB, but with a positive sign.) The explanation is that the number of observations drops to a mere thirty-three, which is obviously problematic in a setting with eight countries and nine markets.

### Pricing of online subscriptions

As explained above, when gauging a newspaper's fear of channel spill-over, one might, in principle, also want to look at how the prices of its online and print subscriptions compare. In our sample, the inter-country differences in such ratios were again striking, but our regressions (which are not reported here) either yielded virtually no results or the results were not consistent. With hindsight, this is perhaps not surprising. As stressed earlier, we cannot control for the degree of differentiation of the online offering. There may also be cost differences. Also, in our regressions, newspapers that opt for either mixed or no bundling were intermingled, whereas we assume in the previous subsection that the former have a higher fear of cannibalisation.

In a second step, we therefore examined newspapers that apply mixed bundling separately. As outlined earlier, we also constructed another indicator and now look at the discount that is granted for the bundle. There are twenty-seven newspapers that apply mixed bundling. Within this set of twenty-seven, newspapers with a PDF versus a site subscription proved to be two distinct groups. For one, our descriptive results show that our newspapers see PDF and site subscriptions as alternatives: only three newspapers offer both, compared to thirty-three that have a PDF subscription (but no site subscription) and twenty that have the latter (but not the former). Moreover, the three newspapers that offer both all opt for _no bundling_ so that they do not appear here. As a result, there is no overlap at all between the two groups. Stronger still, not only are we talking about different newspapers, there are also strong country schisms: all French and Italian newspapers that opt for mixed bundling have site subscriptions (and no PDF subscriptions), whereas all German, Luxembourgian and Spanish newspapers that engage in mixed bundling are in the other group. Finally, even though the average discount for a print+PDF-bundle (29%) is not significantly different from that for a print+site-bundle (32%), PDF and site subscriptions are fundamentally different products. Hence, we decided to examine them separately, which is in line with our approach for the price ratios in the first part of this subsection, but obviously comes at a cost in terms of the number of observations. Another implication is that we needed separate mimicking variables. As explained above, in both cases we tried unweighted and weighted average discounts and also experimented with including the newspaper itself. (In two cases, the value of AVDISC-ALL-MARKET and WAVDISC-ALL-MARKET would have been identical to the discount offered by the one newspaper that practices mixed bundling for the relevant type of subscription in the relevant country. These observations were removed from the dataset.)

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: x-small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3 - The discount for (print+online)-bundle**</caption>

<tbody>

<tr>

<th rowspan="4" align="left">VARIABLES</th>

<th colspan="4" align="left">SITE SUBSCRIPTION</th>

<th colspan="10" align="left">PDF SUBSCRIPTION</th>

</tr>

<tr>

<th colspan="2" align="left">Model I</th>

<th colspan="2" align="left">Model II</th>

<th colspan="2" align="left">Model III</th>

<th colspan="2" align="left">Model IV</th>

<th colspan="2" align="left">Model V</th>

<th colspan="2" align="left">Model VI</th>

<th colspan="2" align="left">Model VII</th>

</tr>

<tr>

<td>B</td>

<td>t</td>

<td>B</td>

<td>T</td>

<td>B</td>

<td>t</td>

<td>B</td>

<td>t</td>

<td>B</td>

<td>t</td>

<td>B</td>

<td>t</td>

<td>B</td>

<td>t</td>

</tr>

<tr>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

<td>(SE)</td>

<td> </td>

</tr>

<tr>

<th colspan="15" align="left">Newspaper-specific</th>

</tr>

<tr>

<td rowspan="2">QUALITY</td>

<td>-4.713</td>

<td>-.647</td>

<td>-6.288</td>

<td>-.760</td>

<td colspan="2" rowspan="8"> </td>

<td colspan="2" rowspan="6"> </td>

<td colspan="2" rowspan="8"> </td>

<td colspan="2" rowspan="6"> </td>

<td colspan="2" rowspan="2"> </td>

</tr>

<tr>

<td>7.281</td>

<td> </td>

<td>8.276</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">FINANCIAL</td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

<td>-4.314</td>

<td>-1.237</td>

</tr>

<tr>

<td>3.487</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">P(SITE)/AVGP</td>

<td>15.425<sup>***</sup></td>

<td>4.258</td>

<td>14.464<sup>***</sup></td>

<td>3.533</td>

<td colspan="2" rowspan="2"> </td>

</tr>

<tr>

<td>3.622</td>

<td> </td>

<td>4.094</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">P(PDF)</td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="4"> </td>

<td>0.108<sup>***</sup></td>

<td>3.819</td>

<td>0.104<sup>***</sup></td>

<td>3.779</td>

<td>0.102<sup>***</sup></td>

<td>5.008</td>

</tr>

<tr>

<td>0.028</td>

<td> </td>

<td>0.028</td>

<td> </td>

<td>0.020</td>

<td> </td>

</tr>

<tr>

<td rowspan="2">P(PDF)/AVGP</td>

<td>18.358<sup>**</sup></td>

<td>2.984</td>

<td colspan="2" rowspan="2"></td>

<td>17.670<sup>**</sup></td>

<td>2.981</td>

<td colspan="2" rowspan="2"> </td>

<td colspan="2" rowspan="2"> </td>

</tr>

<tr>

<td>6.152</td>

<td> </td>

<td>5.927</td>

<td> </td>

</tr>

<tr>

<th colspan="15" align="left">Country-specific</th>

</tr>

<tr>

<td rowspan="2">%INTERNET</td>

<td colspan="2" rowspan="6"> </td>

<td colspan="2" rowspan="4"> </td>

<td colspan="2" rowspan="8"> </td>

<td colspan="2" rowspan="8"> </td>

<td colspan="2" rowspan="8"> </td>

<td colspan="2" rowspan="8"> </td>

<td>-.072</td>

<td>-.331</td>

</tr>

<tr>

<td>0.217</td>

<td></td>

</tr>

<tr>

<td rowspan="2">ADVERT-CONTRIB</td>

<td>-1.310<sup>**</sup></td>

<td>-2.659</td>

</tr>

<tr>

<td>0.493</td>

<td></td>

</tr>

<tr>

<td rowspan="2">ADVERT-%EXP-NET</td>

<td>-4.037<sup>**</sup></td>

<td>-2.878</td>

<td colspan="2" rowspan="6"></td>

</tr>

<tr>

<td>1.403</td>

<td></td>

</tr>

<tr>

<td rowspan="2">ADVERT-?EXP-NET</td>

<td>-.001<sup>***</sup></td>

<td>-3.658</td>

<td colspan="2" rowspan="4"></td>

</tr>

<tr>

<td>0.000</td>

<td></td>

</tr>

<tr>

<td rowspan="2">ADVERT-?REV-NP</td>

<td colspan="2" rowspan="2"></td>

<td>-4.463E-6</td>

<td>-.170</td>

<td>-5.035E-5<sup>*</sup></td>

<td>-1.918</td>

<td>-3.070E-7</td>

<td>-.012</td>

<td>-4.491E-5</td>

<td>-1.757</td>

</tr>

<tr>

<td>0.000</td>

<td></td>

<td>0.000</td>

<td></td>

<td>0.000</td>

<td></td>

<td>0.000</td>

<td></td>

</tr>

<tr>

<th colspan="15" align="left">Market-specific</th>

</tr>

<tr>

<td rowspan="2">AVDISC(PDF)-COMP-MARKET</td>

<td colspan="2" rowspan="4"></td>

<td colspan="2" rowspan="4"></td>

<td>0.833<sup>***</sup></td>

<td>3.752</td>

<td>0.750<sup>***</sup></td>

<td>3.882</td>

<td colspan="2" rowspan="2"></td>

<td colspan="2" rowspan="2"></td>

<td colspan="2" rowspan="4"></td>

</tr>

<tr>

<td>0.222</td>

<td></td>

<td>0.193</td>

<td></td>

</tr>

<tr>

<td rowspan="2">WAVDISC(PDF)-COMP-MARKET</td>

<td colspan="2" rowspan="2"></td>

<td colspan="2" rowspan="2"></td>

<td>0.857<sup>***</sup></td>

<td>3.974</td>

<td>0.771<sup>***</sup></td>

<td>4.068</td>

</tr>

<tr>

<td>0.216</td>

<td></td>

<td>0.190</td>

<td></td>

</tr>

<tr>

<th rowspan="2" align="left">Intercept</th>

<td>24.996<sup>***</sup></td>

<td>3.256</td>

<td>33.298<sup>***</sup></td>

<td>3.399</td>

<td>-11.962</td>

<td>-1.276</td>

<td>-4.039</td>

<td>-.588</td>

<td>-12.141</td>

<td>-1.353</td>

<td>-4.342</td>

<td>-.653</td>

<td>84.562<sup>**</sup></td>

<td>2.612</td>

</tr>

<tr>

<td>7.676</td>

<td> </td>

<td>9.796</td>

<td> </td>

<td>9.372</td>

<td> </td>

<td>6.868</td>

<td> </td>

<td>8.972</td>

<td> </td>

<td>6.652</td>

<td> </td>

<td>32.380</td>

<td> </td>

</tr>

<tr>

<th align="left">R Square</th>

<td>0.751</td>

<td>0.682</td>

<td>0.741</td>

<td>0.806</td>

<td>0.759</td>

<td>0.818</td>

<td>0.861</td>

</tr>

<tr>

<th align="left">Adjusted R Square</th>

<td>0.676</td>

<td>0.586</td>

<td>0.643</td>

<td>0.733</td>

<td>0.669</td>

<td>0.749</td>

<td>0.768</td>

</tr>

<tr>

<th align="left">S.E. of the Estimate</th>

<td>6.92303</td>

<td>7.82899</td>

<td>7.74144</td>

<td>6.69772</td>

<td>7.45644</td>

<td>6.49197</td>

<td>5.07324</td>

</tr>

<tr>

<th align="left">No. of observations</th>

<td>14</td>

<td>14</td>

<td>12</td>

<td>12</td>

<td>12</td>

<td>12</td>

<td>11</td>

</tr>

<tr>

<th align="left">F</th>

<td>10.059</td>

<td>7.139</td>

<td>7.611</td>

<td>11.064</td>

<td>8.412</td>

<td>11.948</td>

<td>9.279</td>

</tr>

<tr>

<th align="left">Overall significance</th>

<td>0.002</td>

<td>0.008</td>

<td>0.010</td>

<td>0.003</td>

<td colspan="2">0.007</td>

<td colspan="2">0.003</td>

<td colspan="2">0.010</td>

</tr>

<tr>

<td colspan="15">Note: Superscripts <sup>*</sup>, <sup>**</sup> and <sup>***</sup> indicate that the coefficient is significantly different from zero at the 10%, 5% and 1% level, respectively.</td>

</tr>

</tbody>

</table>

As can be seen in Table 3, the only newspaper-level variable that proved significant was the _price of the online subscription_. We find that the higher the price of the site or PDF subscription (in Models I-II and III-VII, respectively), the higher the percentage discount for the respective bundle. However, this does not really confirm Hypothesis?1, which posits that newspapers that are more expensive _offline_ are more likely to offer a higher discount. Prices for print and online subscriptions _are_ correlated, but when P(SITE)/AVGP, P(PDF) and P(PDF)/AVGP are replaced by P(PRINT) or P(PRINT)/AVGP, the latter are not significant in any of the models. This being so, the results for P(SITE)/AVGP, P(PDF) and P(PDF)/AVGP do not really come as a surprise, since high-priced online content can in itself also be an indication of cannibalisation fear, just like a high discount (the dependent variable). That the QUALITY variable is not significant is only normal if one looks at the numbers: only one (two) out of the fourteen (thirteen) newspapers with a site (PDF) subscription that appear in Table 3 is (are) a popular newspaper. For FINANCIAL, the corresponding numbers are four out of ten for site and three out of eleven for PDF subscriptions.

Turning to the market level, the advertising variables always appear with a negative sign (in line with Hypothesis 4) and are significant in four of the seven models. Continuing our analysis, overall the models show that mimicking behaviour is difficult to distinguish from a possible non-imitative process where newspapers simply respond identically to a common driver. Indeed, the bulk of the models in Table 3 are _either/or_, in the sense that the impact of advertising and indications of mimicking behaviour do not show up together in the same model - with the exception of Model IV. Note that, at least where the PDF subscriptions are concerned, the advertising variables are strongly correlated with the mimicking variable: for example, the Pearson correlation between ADVERT-CONTRIB, ADVERT-%EXP-NET and ADVERT-?EXP-NET on the one hand and AVDISC(PDF)-COMP-MARKET (WAVDISC(PDF)-COMP-MARKET) on the other hand amount to 0.695<sup>**</sup> (0.721<sup>***</sup>), -0.615<sup>**</sup> (-0.647<sup>**</sup>) and even 0.936<sup>***</sup> (0.936<sup>***</sup>) respectively. Only ADVERT-?REV-NP does not suffer from multicollinearity (at -0.003) and this is, not coincidentally, the variable that appears in Model IV. But allowing for this caveat, we do find unambiguously in Models III to VI that the higher the (weighted) _discount granted by other newspapers_ in the same country or region that offer a bundle containing a PDF subscription, the higher the discount offered by the newspaper itself. Broadening the mimicking variables in Models III-IV to the average discount offered by competitors that have a PDF subscription _or a site subscription_ yields virtually identical results (not reported in Table 3). Also, Models III-VI remain intact when the ...-COMP-... mimicking variables are replaced by ...-ALL-... variables. In Models IV and VI, the advertising variable now even becomes significant (at the 5%, resp. 10% level); and in Models III and V, P(PDF)/AVGP is now significant at the 1% level. We find no evidence of mimicking for site subscriptions.

## Concluding remarks and discussion

This paper deals with an important strategic concern for newspapers, namely channel spill-over between the print and online version. In the previous Section, we presented our findings decision by decision. Here we use Table 4 to look at the impact of variables across decisions. We also compare our results with those obtained in Bleyen and Van Hove ([2010a](#bley10a), [2010b](#bley10b)), all the while keeping in mind the different nature of the decisions.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4 - An overview of the results and hypotheses**  
(Notes: The first column shows the explanatory variables used in the analyses, with variables that were constructed as alternatives grouped together. The remaining columns indicate, per decision, the sign and significance of the variables and whether or not the hypothesis is supported. If variables were tested but proved insignificant, this is shown as a shaded area. When the results only occurred in _one_ model and/or were insignificant in alternative models, suggesting that the outcome is not very stable, they are put between brackets. Whenever variables were not tested _explicitly_, this is displayed as a blank area.)</caption>

<tbody>

<tr>

<th rowspan="3" align="left">D<sub>1-2</sub></th>

<th colspan="2" rowspan="2">MIXED BUNDLING</th>

<th colspan="3">DISCOUNT</th>

</tr>

<tr>

<th>SITE</th>

<th>PDF</th>

<td> </td>

</tr>

<tr>

<th>Sign.</th>

<th>Validation</th>

<th>Sign.</th>

<th>Sign.</th>

<th>Validation</th>

</tr>

<tr>

<th colspan="6" style="background-color: #99f5fb" align="left">Newspaper-specific</th>

</tr>

<tr>

<td align="center">P(PRINT)</td>

<td align="center">(+<sup>**</sup>)</td>

<td rowspan="2" align="center"><s>H1</s></td>

<td rowspan="2" style="background-color: #99f5fb"> </td>

<td rowspan="2" style="background-color: #99f5fb"> </td>

<td rowspan="2" align="center"><s>H1</s></td>

</tr>

<tr>

<td align="center">P(PRINT)/AVGP</td>

<td style="background-color: #99f5fb"> </td>

</tr>

<tr>

<td align="center">P(SITE)/AVGP</td>

<td colspan="2" rowspan="3"> </td>

<td align="center">+<sup>***</sup></td>

<td> </td>

<td rowspan="3" align="center">(H1)</td>

</tr>

<tr>

<td align="center">P(PDF)</td>

<td rowspan="2"> </td>

<td align="center">+<sup>***</sup></td>

</tr>

<tr>

<td align="center">P(PDF)/AVGP</td>

<td align="center">+<sup>**</sup></td>

</tr>

<tr>

<td colspan="6"> </td>

</tr>

<tr>

<td align="center">QUALITY</td>

<td style="background-color: #99f5fb"> </td>

<td align="center"><s>H3</s></td>

<td style="background-color: #99f5fb"> </td>

<td style="background-color: #99f5fb"> </td>

<td align="center"><s>H3</s></td>

</tr>

<tr>

<td colspan="6"></td>

</tr>

<tr>

<td align="center">FINANCIAL</td>

<td align="center">+<sup>**/*</sup></td>

<td align="center">**H2**</td>

<td style="background-color: #99f5fb"> </td>

<td style="background-color: #99f5fb"> </td>

<td align="center"><s>H2</s></td>

</tr>

<tr>

<th colspan="6" style="background-color: #99f5fb" align="left">Country-specific</th>

</tr>

<tr>

<td align="center">AVGP(PRINT)</td>

<td align="center">+<sup>**</sup></td>

<td align="center"><s>H1</s></td>

<td style="background-color: #99f5fb"> </td>

<td style="background-color: #99f5fb"> </td>

<td align="center"><s>H1</s></td>

</tr>

<tr>

<td colspan="6"> </td>

</tr>

<tr>

<td align="center">%INTERNET</td>

<td align="center">+<sup>**/*</sup></td>

<td rowspan="2" align="center">**H5**</td>

<td rowspan="2" style="background-color: #99f5fb"> </td>

<td rowspan="2" style="background-color: #99f5fb"> </td>

<td rowspan="2" align="center"><s>H5</s></td>

</tr>

<tr>

<td align="center">MEANHOURSNET</td>

<td style="background-color: #99f5fb"> </td>

</tr>

<tr>

<td colspan="6"> </td>

</tr>

<tr>

<td align="center">ADVERT-CONTRIB</td>

<td style="background-color: #99f5fb"> </td>

<td rowspan="4" align="center"><s>H4</s></td>

<td style="background-color: #99f5fb"> </td>

<td align="center">-<sup>**</sup></td>

<td rowspan="4" align="center">H4</td>

</tr>

<tr>

<td align="center">ADVERT-%EXP-NET</td>

<td> </td>

<td align="center">-<sup>**</sup></td>

<td style="background-color: #99f5fb"> </td>

</tr>

<tr>

<td align="center">ADVERT-?REV-NP</td>

<td style="background-color: #99f5fb"> </td>

<td style="background-color: #99f5fb"> </td>

<td align="center">-<sup>*</sup></td>

</tr>

<tr>

<td align="center">ADVERT-?EXP-NET</td>

<td> </td>

<td align="center">-<sup>***</sup></td>

<td style="background-color: #99f5fb"> </td>

</tr>

<tr>

<th colspan="6" style="background-color: #99f5fb" align="center">Market-specific</th>

</tr>

<tr>

<td align="center">#MIXED-ALL-MARKET</td>

<td align="center">+<sup>***</sup></td>

<td rowspan="8" align="center">**H6**</td>

<td colspan="3" rowspan="8"> </td>

</tr>

<tr>

<td align="center">#MIXED-ALL-SEGM</td>

<td align="center">+<sup>***</sup></td>

</tr>

<tr>

<td align="center">%MIXED-ALL-MARKET</td>

<td align="center">+<sup>***</sup></td>

</tr>

<tr>

<td align="center">%MIXED-ALL-SEGM</td>

<td align="center">+<sup>**</sup></td>

</tr>

<tr>

<td align="center">#MIXED-COMP-MARKET</td>

<td align="center">+<sup>**</sup></td>

</tr>

<tr>

<td align="center">#MIXED-COMP-SEGM</td>

<td style="background-color: #99f5fb"></td>

</tr>

<tr>

<td align="center">%MIXED-COMP-MARKET</td>

<td align="center">(+<sup>**</sup>)</td>

</tr>

<tr>

<td align="center">%MIXED-COMP-SEGM</td>

<td align="center">+<sup>**</sup></td>

</tr>

<tr>

<td align="center">AVDISC(...)-ALL-MARKET</td>

<td colspan="2" rowspan="4"> </td>

<td style="background-color: #99f5fb"> </td>

<td align="center">+<sup>***</sup></td>

<td rowspan="4" align="center">**H6 (PDF)**</td>

</tr>

<tr>

<td align="center">WAVDISC(...)-ALL-MARKET</td>

<td style="background-color: #99f5fb"> </td>

<td align="center">+<sup>***</sup></td>

</tr>

<tr>

<td align="center">AVDISC(...)-COMP-MARKET</td>

<td style="background-color: #99f5fb"> </td>

<td align="center">+<sup>***</sup></td>

</tr>

<tr>

<td align="center">WAVDISC(...)-COMP-MARKET</td>

<td style="background-color: #99f5fb"> </td>

<td align="center">+<sup>***</sup></td>

</tr>

</tbody>

</table>

To start with the individual characteristics, the most important results is that, in line with Hypothesis 2, _financial_ newspapers are more likely to opt for a mixed bundling strategy. In Bleyen and Van Hove ([2010b](#bley10b)) we find that they are also more likely to charge for their archive. Conversely, FINANCIAL does not appear to have an impact on the bundling discount (this paper), nor on the high-level _free or fee_-choice ([Bleyen & Van Hove 2010b](#bley10b)) or on the decision to offer subscriptions and/or pay-per-view ([Bleyen & Van Hove 2010a](#bley10a)). Contrary to expectations (in Hypothesis 3), QUALITY is not significant at all for the two bundling decisions. In particular for D<sub>2</sub>, this may be because of the low number of popular newspapers in the dataset. In bigger and more heterogeneous data sets, QUALITY does matter: we find that quality newspapers are clearly more likely to have a _fee_ Website ([Bleyen and Van Hove 2010b](#bley10b)) and site subscriptions ([Bleyen and Van Hove 2010a](#bley10a)). For pay-per-view, PDF subscriptions and a paid-for archive, the evidence is less overwhelming. A final result on the newspaper level is that Hypothesis 1 is not really supported: in Table 2, P(PRINT) primarily seems to capture a country effect; in Table 3, we do find a significant impact of prices, but only for the prices of the _online_ subscriptions.

On the country level, we have some results for %INTERNET for D<sub>1</sub>(but not for D<sub>2</sub>) and for the impact of advertising revenues for D<sub>2</sub>(but not for D<sub>1</sub>). This relatively low incidence of country-specific variables is even more outspoken for the _free or fee_ and archive decisions analysed in Bleyen and Van Hove ([2010b](#bley10b)). Conversely, in Bleyen and Van Hove ([2010a](#bley10a)), we do find a significant impact of advertising. We find that the higher the relative importance of (offline) advertising, the lower the probability that newspapers have a site subscription or pay-per-view, apparently because they aim for the same type of revenues on- and offline. For PDF subscriptions, the advertising variables have a positive impact, perhaps because the PDF version comprises the same advertisements as its paper counterpart. However, a problem with these results, which is in fact a returning problem with just about all our country-level results, also in this paper, is that the impact of the relative importance of advertising on the one hand and mimicking behaviour on the other hand are just about impossible to disentangle as the two types of variables almost never appear together in a model (because they are strongly correlated). What we call mimicking behaviour might thus be the result of several newspapers in a given country making the same move because they are influenced by a common driver. Disentangling the two explanations is clearly a challenge for future research.

Apart from this caveat, we do find strong indications of mimicking behaviour in the present paper: for D<sub>1</sub>, seven out of the eight mimicking variables prove significant, at the 5% to 1% level; for D<sub>2</sub>, we find that the discount offered for the print+PDF-bundle is positively influenced by the (weighted) average discount set by competing newspapers, but find no such effect for the print+site-bundle. Compared to the mimicking results in our other papers, these results are less open to criticism; see in particular the discussion in Bleyen and Van Hove ([2010b](#bley10b)).?

In the latter paper, we also tried to determine in which of the mimicking theories briefly discussed in this paper our findings could be framed. Our analysis provided support to the importance of the neo-institutional theory and industrial organization perspectives, with the anecdotal evidence suggesting that the framework of industrial organization is the more important of these two concepts. As already mentioned, for D<sub>1</sub>, discriminating between mimicking theories is not possible because just about all mimicking variables are significant. However, since D<sub>2</sub> is not about adoption of new practices or technologies but all about pricing, the rivalry-based imitation of the industrial organization stream would again seem to be the most obvious framework.

An important remark is also that when interpreting our results, one has to take into account that the bundle discount would seem to be a better measure of newspapers' fear of channel spill-over than their use of mixed bundling. We find, for example, that financial newspapers are more likely to opt for a mixed bundling strategy but that FINANCIAL does _not_ affect the bundling discount. If the second is the better indicator of the two, then one should not jump to conclusions as to the reason of the different bundling behaviour of financial newspapers.

Finally, even though we yet have to discover salient drawbacks to using the bundle discount as an indicator of channel-spill-over fear, it is clear that there is always room for improvement. In particular content analysis would seem to impose itself as the next step in our research endeavours, as this would allow to gauge the overlap between the print and online offerings and in this way would make it possible to quantify the substitute or complement nature of the content.

## Acknowledgements

Steve Brown (The Newspaper Society, U.K.), Marc Thiltgen (TNS-Ilres, Luxembourg), Sabine Ozil (SPQN, France), Marlene Blonk (Cebuco, the Netherlands), Tamara van Langenhof (Vlaamse Dagbladpers, Belgium), Federico Megna (FIEG, Italy), Heike Laumer (BDZV, Germany) and Maribel Matallanas (AEDE, Spain) are gratefully acknowledged for providing data, as well as all the newspapers that helped to resolve more detailed and newspaper-specific questions.

We thank the Editor and an anonymous referee for providing valuable guidance on an earlier version of this paper. We also thank Robert G. Picard and past and present colleagues Ralf Caers, Bruno Heyndels, Marc Jegers and Ellen Loix for their constructive comments.

## About the authors

Valerie-Anne Bleyen obtained her PhD from the Vrije Universiteit Brussel (Free University of Brussels) in 2010.? She is currently a researcher at the Center for Studies on Media, Information and Telecommunication (SMIT) of the same university. She can be contacted at [vbleyen@vub.ac.be](mailto:vbleyen@vub.ac.be)

Leo Van Hove is a professor of Economics at the Vrije Universiteit Brussel (Free University of Brussels), where he teaches courses in monetary economics, economics of information, and e-commerce. He can be contacted at [Leo.Van.Hove@vub.ac.be](mailto:Leo.Van.Hove@vub.ac.be).

#### References

*   Adams, W.J. & Yellen, J.L. (1976). Commodity bundling and the burden of monopoly. _Quarterly Journal of Economics_, **90**(3), 475-498.
*   Aldrich, H.E. & Fiol, M.C. (1994). Fools rush in: the institutional context of industry creation. _Academy of Management Review_, **19**, 645-670.
*   Beehivecity (2010, July 18). [Times paywall: more analysis of the data](http://www.webcitation.org/5sDqpmdMv). Retrieved 17 November, 2011 from http://www.beehivecity.com/newspapers/times-paywall-more-analysis-of-the-data191807/ (Archived by WebCite? at http://www.webcitation.org/5sDqpmdMv)
*   Bleyen, V.-A. & Van Hove, L. (2010a). To bundle or not to bundle? How Western European newspapers package their online content. _Journal of Media Economics_, **23**(3), 117-142.
*   Bleyen, V.-A. & Van Hove, L. (2010b). _Free or fee? How Western European newspapers monetise their online content_. Brussels: Vrije Universiteit Brussel. (Working paper)
*   Boston Consulting Group (2009, November 16). [Willingness to pay for news online: key findings from an international survey](http://www.webcitation.org/5nJSGzdRQ). Retrieved 19 November, 2009 from http://www.bcg.com/media/PressReleaseDetails.aspx?id=tcm:12-35297 (Archived by WebCite? at http://www.webcitation.org/5nJSGzdRQ)
*   Choi, S.Y., Stahl, D.O. & Whinston, A.B. (1997). _The economics of electronic commerce: the essential economics of doing business in the electronic marketplace._ Indianapolis, IN: MacMillan Technical Publishing.
*   Chyi, H.I. (2004). Re-examining the market relation between online and print newspapers: the case of Hong Kong. In L. Xiaoming (Ed.), _Internet newspapers - the making of a mainstream medium._ (pp. 193-207). Mahwah, NJ: Lawrence Erlbaum Associates.
*   Chyi, H.I. & Lasorsa, D.L. (2002). An explorative study on the market relation between online and print newspapers. _The Journal of Media Economics_, **15**(2), 91-106.
*   De Waal, E., Sch?nbach, K. & Lauf, E. (2005). Online newspapers: a substitute or complement for print newspapers and other information channels? _Communications: The European Journal of Communication Research_, **30**(1), 55-72.
*   Deleersnyder, B., Geyskens, I., Gielens, K. & Dekimpe, M.G. (2002). How cannibalistic is the Internet channel? A study of the newspaper industry in the United Kingdom and the Netherlands. _International Journal of Research in Marketing_ , **19**(4), 337-348.
*   The Economist (2009, August 27). _Now pay up_. Retrieved 27 August, 2009 from http://www.economist.com/businessfinance/displayStory.cfm?story_id=14327327 (No longer freely available.)
*   Filistrucchi, L. (2005). _The impact of Internet on the market for daily newspapers in Italy_. San Domenico, Italy: European University Institute. (Economics Working Papers ECO2005/12)
*   Financial Times (2009, August 7). [_Rivals sceptical of Murdoch's charging plan._](http://www.webcitation.org/5nJSh6s1h) Retrieved 7 August, 2009 from http://uk.biz.yahoo.com/07082009/399/rivals-sceptical-murdoch-s-charging-plan.html (Archived by WebCite? at http://www.webcitation.org/5nJSh6s1h)
*   Flavian, C. & Gurrea, R. (2007). Duality of newspaper distribution channels: an analysis of readers' motivations. _International Revue of Retail, Distribution and Consumer Research_, **17**(1), 63-78.
*   Gapper, J. (2010, May 26). _Murdoch has to become an elitist_. Retrieved 17 November, 2011 from http://www.ft.com/cms/s/0/ea7dfefa-68f4-11df-910b-00144feab49a.html#axzz1e00TBZIm (No longer freely available.)
*   Gentzkow, M. (2007). Valuing new goods in a model with complementarity: online newspapers. _American Economic Review_, **97**(3), 713-744.
*   Haunschild, P.R. & Chandler, D. (2008). Institutional-level learning: learning as a source of institutional change. In K. Sahlin-Andersson, R. Greenwood, C. Oliver & R. Suddaby (Eds.), _The Sage handbook of organizational institutionalism_ (pp. 624-649). London: Sage Publications.
*   Haunschild, P.R. & Miner, A.S. (1997). Modes of interorganizational imitations: the effects of outcome salience and uncertainty. _Administrative Science Quarterly_, **42**(3) 472-500.
*   Isaacson, W. (2009, February 5). [_How to save your newspaper_](http://www.webcitation.org/5nJX5xUF7). Retrieved 17 November, 2011 from http://www.time.com/time/business/article/0%2C8599%2C1877191-1%2C00.html (Archived by WebCite? at http://www.webcitation.org/5nJX5xUF7)
*   Knowledge@Wharton (2009, January 7). [_Urgent deadline for newspapers: find a new business plan before you vanish_](http://www.webcitation.org/5nJXR2yWB). Retrieved 9 January, 2009 from http://knowledge.wharton.upenn.edu/article.cfm?articleid=2130 (Archived by WebCite? at http://www.webcitation.org/5nJXR2yWB)
*   Mediaweek (2010, August 16). [_Times loses 1.2 million readers_](http://www.webcitation.org/63GrwhLz1). Retrieved 17 Novembere, 2011 from http://www.mediaweek.co.uk/news/1022312/Times-loses-12-million-readers/ (Archived by WebCite? at http://www.webcitation.org/63GrwhLz1)
*   The New York Times Company (2010, January 20). [_The New York Times announces plans for a metered model for NYTimes.com in 2011_](http://www.webcitation.org/5nJXWicr4). Retrieved 21 January, 2010 from http://phx.corporate-ir.net/phoenix.zhtml?c=105317&p=irol-pressArticle&ID=1377114&highlight (Archived by WebCite? at http://www.webcitation.org/5nJXWicr4)
*   Nguyen, A. & Western, M. (2006). [The complementary relationship between the Internet and traditional mass media: the case of online news and information](http://www.webcitation.org/5nJXcFnkq). _Information Research_, **11**(3), paper 259\. Retrieved 17 November, 2011 from http://informationr.net/ir/11-3/paper259.html (Archived by WebCite? at http://www.webcitation.org/5nJXcFnkq)
*   Ordanini, A., Rubera, G. & DeFillippi, R. (2008). The many moods of inter-organizational imitation: a critical review. _International Journal of Management Reviews_, **10**(4), 375-398.
*   Pauwels, K. & Dans, E. (2001). Internet marketing the news: leveraging brand equity from marketplace to marketspace. _Journal of Brand Management_, **8**(4), 303-331.
*   P?rez- Pe?a, R. (2010, January 20). [_The Times to charge for frequent access to its web site_](http://www.webcitation.org/5nJXhP8t2). Retrieved January 21, 2010 from http://www.nytimes.com/2010/01/21/business/media/21times.html?hp&emc=na (Archived by WebCite? at http://www.webcitation.org/5nJXhP8t2)
*   Porter, M. (2001). Strategy and the Internet. _Harvard Business Review_, **79**(3), 62-78.
*   PricewaterhouseCoopers (2009). [_Moving into multiple business models - outlook for newspaper publishing in the digital age_](http://www.webcitation.org/5nJYOQF4h). Retrieved November 12, 2009 from Pwc.com. (Archived by WebCite? at http://www.webcitation.org/5nJYOQF4h)
*   Stahl, F., Sch?fer, M.-F. & Mass, W. (2004). Strategies for selling paid content on newspaper and magazine web sites: an empirical analysis of bundling and splitting of news and magazine articles. _The International Journal on Media Management_, **6**(1 & 2), 59-66.
*   Terlaak, A. & Gong, Y. (2008). Vicarious learning and inferential accuracy in adoption processes. _Academy of Management Review_, **33**(4), 846-868.
*   Venkatesh, R. & Chatterjee, R. (2006). Bundling, unbundling, and pricing of multiform products: the case of magazine content. _Journal of Interactive Marketing_, **20**(2), 21-40.
*   The World Association of Newspapers (2006). _Trends in newsrooms 2006_. Paris: World Editors Forum. Retrieved 12 July, 2006 from Worldeditorsforum.org. (No longer available.)
*   The World Association of Newspapers and News Publishers. (2010). _[New revenue models for newspaper companies: executive summary](http://www.webcitation.org/63MfSPgw5)_. Paris: World Association of Newspapers and News Publishers. (Strategy Report 9(2)). Retrieved 16 November, 2011 from http://www.wan-press.org/IMG/pdf/executive_summary_9.2.pdf (Archived by WebCite? at http://www.webcitation.org/63MfSPgw5)

## Appendices

<table id="table1a" width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th colspan="5">D<sub>1</sub> - The decision to offer no or mixed bundling (Mixed = 1/No = 0)</th>

</tr>

<tr>

<th align="center">Country</th>

<th align="center">Obs</th>

<th align="center">Mixed</th>

<th align="center">Mean</th>

<td align="center">Std. Dev. Across Countries</td>

</tr>

<tr>

<td align="center">BE</td>

<td align="center">6</td>

<td align="center">4</td>

<td align="center">0.6667</td>

<td rowspan="9" align="center">0.25868</td>

</tr>

<tr>

<td align="center">NL</td>

<td align="center">5</td>

<td align="center">4</td>

<td align="center">0.8</td>

</tr>

<tr>

<td align="center">LUX</td>

<td align="center">4</td>

<td align="center">2</td>

<td align="center">0.5</td>

</tr>

<tr>

<td align="center">FR</td>

<td align="center">7</td>

<td align="center">5</td>

<td align="center">0.7143</td>

</tr>

<tr>

<td align="center">DE</td>

<td align="center">7</td>

<td align="center">6</td>

<td align="center">0.8571</td>

</tr>

<tr>

<td align="center">IT</td>

<td align="center">12</td>

<td align="center">3</td>

<td align="center">0.25</td>

</tr>

<tr>

<td align="center">ES</td>

<td align="center">7</td>

<td align="center">2</td>

<td align="center">0.2857</td>

</tr>

<tr>

<td align="center">UK</td>

<td align="center">8</td>

<td align="center">1</td>

<td align="center">0.125</td>

</tr>

<tr>

<td align="center">Total</td>

<td align="center">56</td>

<td align="center">27</td>

<td align="center">0.4821</td>

</tr>

<tr>

<td align="center">N.A.</td>

<td align="center">26</td>

<td align="center">-</td>

<td align="center">0.3171</td>

<td align="center">-</td>

</tr>

</tbody>

</table>

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1a - Summary statistics of the explained variables**  
Sources: all data come directly from the newspaper Websites (see Section on data collection), unless indicated otherwise.</caption>

<tbody>

<tr>

<th nowrap="nowrap" colspan="7" align="center">D<sub>2</sub> - The discount when purchasing a bundle of online and print subscription (%)</th>

</tr>

<tr>

<th>Country</th>

<th>Obs</th>

<th>Mean</th>

<th>Std. Dev.</th>

<th>Min</th>

<th>Max</th>

<th>Std. Dev. Across Countries</th>

</tr>

<tr>

<td align="center">BE</td>

<td align="center">4</td>

<td align="center">27.7425</td>

<td align="center">16.79092</td>

<td align="center">14.24</td>

<td align="center">50</td>

<td rowspan="9" align="center">9.71633</td>

</tr>

<tr>

<td align="center">NL</td>

<td align="center">4</td>

<td align="center">32.745</td>

<td align="center">11.89705</td>

<td align="center">24.15</td>

<td align="center">50</td>

</tr>

<tr>

<td align="center">LUX</td>

<td align="center">2</td>

<td align="center">46.97</td>

<td align="center">0.12728</td>

<td align="center">46.88</td>

<td align="center">47.06</td>

</tr>

<tr>

<td align="center">FR</td>

<td align="center">5</td>

<td align="center">31.162</td>

<td align="center">9.05924</td>

<td align="center">15.9</td>

<td align="center">39.07</td>

</tr>

<tr>

<td align="center">DE</td>

<td align="center">6</td>

<td align="center">30.325</td>

<td align="center">11.46983</td>

<td align="center">11.16</td>

<td align="center">41.16</td>

</tr>

<tr>

<td align="center">IT</td>

<td align="center">3</td>

<td align="center">34.2067</td>

<td align="center">13.74002</td>

<td align="center">25</td>

<td align="center">50</td>

</tr>

<tr>

<td align="center">ES</td>

<td align="center">2</td>

<td align="center">21.8</td>

<td align="center">4.97803</td>

<td align="center">18.28</td>

<td align="center">25.32</td>

</tr>

<tr>

<td align="center">UK</td>

<td align="center">1</td>

<td align="center">10.75</td>

<td align="center">-</td>

<td align="center">10.75</td>

<td align="center">10.75</td>

</tr>

<tr>

<td align="center">Total</td>

<td align="center">27</td>

<td align="center">30.7637</td>

<td align="center">12.14514</td>

<td align="center">10.75</td>

<td align="center">50</td>

</tr>

</tbody>

</table>

<table id="table1b" width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1b - Explanatory variable definitions and summary statistics**  
Sources: all data come directly from the newspaper Websites (see main text), unless indicated otherwise.</caption>

<tbody>

<tr>

<th align="center">VARIABLE ABBREVIATION</th>

<th align="center">VARIABLE</th>

<th align="center">DESCRIPTION</th>

<th align="center">% OF TOTAL SAMPLE (N=82)</th>

</tr>

<tr>

<th colspan="4" align="left">Newspaper-specific</th>

</tr>

<tr>

<td align="center">P(PRINT)</td>

<td align="center">Price annual print subscription  
_(Missing data)_ </td>

<td align="center">The price of an annual print subscription (in ?).  
(Mean = 265.7310; St. Dev. = 85.98075)</td>

<td align="center">74.4  
(25.6)</td>

</tr>

<tr>

<td align="center">P(PRINT)/AVGP</td>

<td align="center">Price annual print subscription - scaled per country/region  
_(Missing data)_ </td>

<td align="center">The price of an annual print subscription (in ?), divided by the average price per country/region.  
(Mean = 1.0000; St. Dev. =0.22683)</td>

<td align="center">73.2  
(26.8)</td>

</tr>

<tr>

<td align="center">P(SITE)</td>

<td align="center">Price annual site subscription  
_(Missing data)_ </td>

<td align="center">The price of an annual site subscription (in ?).  
(Mean = 161.9143; St. Dev. = 96.73345)</td>

<td align="center">28.0  
(72.0)</td>

</tr>

<tr>

<td align="center">P(SITE)/AVGP</td>

<td align="center">Price annual site subscription - scaled per country/region  
_(Missing data)_ </td>

<td align="center">The price of an annual site subscription (in ?), divided by the average price per country/region.  
(Mean = 1.0000; St. Dev. =0.45003)</td>

<td align="center">28.0  
(72.0)</td>

</tr>

<tr>

<td align="center">RATIO(SITE/PRINT)</td>

<td align="center">The ratio of the annual site/PRINT subscription  
_(Missing data)_ </td>

<td align="center">The price of the annual site subscription, divided by the price of the annual print subscription (in ?).  
(Mean = 58.9941; St. Dev. = 29.80026)</td>

<td align="center">26.8  
(73.2)</td>

</tr>

<tr>

<td align="center">P(PDF)</td>

<td align="center">Price annual PDF subscription  
_(Missing data)_ </td>

<td align="center">The price of an annual PDF subscription (in ?).  
(Mean = 150.2276; St. Dev. = 68.99444)</td>

<td align="center">35.4  
(64.6)</td>

</tr>

<tr>

<td align="center">P(PDF)/AVGP</td>

<td align="center">Price annual PDF subscription - scaled per country/region  
_(Missing data)_ </td>

<td align="center">The price of an annual PDF subscription (in ?), divided by the average price per country/region.  
(Mean = 6.6033; St. Dev. = 30.32906)</td>

<td align="center">35.4  
(64.6)</td>

</tr>

<tr>

<td align="center">RATIO(PDF/PRINT)</td>

<td align="center">The ratio of the annual PDF/PRINT subscription  
_(Missing data)_ </td>

<td align="center">The price of the annual PDF subscription, divided by the price of the annual print subscription (in ?).  
(Mean = 51.9289; St. Dev. = 20.19829)</td>

<td align="center">28.0  
(72.0)</td>

</tr>

<tr>

<td align="center">ONLINEREACH</td>

<td align="center">Online daily reach per million of users (3 months average)  
_(Missing data)_ </td>

<td align="center">Reach measures the number of users and is expressed as the percentage of all Internet users who visit a given site, averaged over the specified time period (August-September-October 2006). <u>Source</u>: [http://www.alexa.com](http://www.alexa.com/).  
(Mean = 394.4461; St. Dev. = 639.94937)</td>

<td align="center">96.3  
(3.7)</td>

</tr>

<tr>

<td align="center">QUALITY</td>

<td align="center">Quality newspaper?</td>

<td align="center">Equals 1 if this is a quality newspaper.</td>

<td align="center">68.3</td>

</tr>

<tr>

<td align="center">POPULAR</td>

<td align="center">Popular newspaper?</td>

<td align="center">Equals 1 if this is a popular newspaper.</td>

<td align="center">24.4</td>

</tr>

<tr>

<td align="center"> </td>

<td align="center">_(Missing data)_ </td>

<td align="center"><u>Source</u>: We contacted the national coordinating organizations or the newspapers themselves in case no information could be obtained on the higher level.</td>

<td align="center">(7.3)</td>

</tr>

<tr>

<td align="center">FINANCIAL</td>

<td align="center">Financial newspaper?  
_(Missing data)_ </td>

<td align="center">Equals 1 if newspaper is financial.<u>Source</u>: We contacted the national coordinating organizations or the newspapers themselves in case no information could be obtained on the higher level.</td>

<td align="center">14.6  
(18.3)</td>

</tr>

<tr>

<th colspan="4" align="left">Country-specific</th>

</tr>

<tr>

<td align="center">AVGP(PRINT)</td>

<td align="center">The average price of an annual print subscription per country/region  
_(Missing data)_ </td>

<td align="center">The average price of an annual print subscription per country/region (in ?).  
(Mean = 259.5219; St. Dev. = 55.41845)</td>

<td align="center">87.8  
(12.2)</td>

</tr>

<tr>

<td align="center">AVGP(SITE)</td>

<td align="center">The average price of an annual site subscription per country/region  
_(Missing data)_ </td>

<td align="center">The average price of an annual site subscription per country/region (in ?).  
(Mean = 150.2333; St. Dev. = 48.12118)</td>

<td align="center">80.5  
(19.5)</td>

</tr>

<tr>

<td align="center">AVGP(PDF)</td>

<td align="center">The average price of an annual PDF subscription per country/region </td>

<td align="center">The average price of an annual PDF subscription per country/region (in ?).  
(Mean = 145.3756; St. Dev. = 36.47752)</td>

<td align="center">100</td>

</tr>

<tr>

<td align="center">ADVERT-CONTRIB</td>

<td align="center">Contribution of advertising to daily newspapers' revenues (%)</td>

<td align="center">The contribution of advertising (as opposed to copy sales) to daily newspapers' revenues (in %). <u>Source</u>: Report of the World Association of Newspapers 2006  
(Mean = 51.128; St. Dev. = 9.70788)</td>

<td align="center">100</td>

</tr>

<tr>

<td align="center">ADVERT-%EXP-NP</td>

<td align="center">Advertising expenditure shares newspapers (%)</td>

<td align="center">The advertising expenditure on newspapers (as opposed to magazines, TV, radio, cinema, outdoor and Internet) (in %). <u>Source</u>: Report of the World Association of Newspapers 2006  
(Mean = 29.7659; St. Dev. = 11.0601)</td>

<td align="center">100</td>

</tr>

<tr>

<td align="center">ADVERT-%EXP-NET</td>

<td align="center">Advertising expenditures shares Internet (%)</td>

<td align="center">The advertising expenditure on Internet (as opposed to newspapers, magazines, TV, radio, cinema and outdoor) (in %). <u>Source</u>: Report of the World Association of Newspapers 2006  
(Mean = 2.5061; St. Dev. = 1.98023)</td>

<td align="center">100</td>

</tr>

<tr>

<td align="center">ADVERT-?EXP-NET</td>

<td align="center">Advertising expenditures on Internet (?)  
_(Missing data)_ </td>

<td align="center">The Internet advertising expenditure in ? per country, scaled by dividing by GDP per capita. <u>Source</u>: Report of the World Association of Newspapers 2006  
(Mean = 12574.3882; St. Dev. = 1.55330E4)</td>

<td align="center">92.7  
(7.3)</td>

</tr>

<tr>

<td align="center">ADVERT-?REV-NP</td>

<td align="center">Advertising revenues (?)</td>

<td align="center">The advertising revenues for the paid-for dailies in each country (in ?), scaled by dividing by GDP per capita. <u>Source</u>: Report of the World Association of Newspapers 2006  
(Mean = 74746.5327; St. Dev. = 5.39780E4)</td>

<td align="center">100</td>

</tr>

<tr>

<td align="center">%INTERNET</td>

<td align="center">Internet penetration</td>

<td align="center">The Internet access in a given country, measured as a % of population. <u>Source</u>: Internet World Stats - Usage and Population Statistics, available at [http://www.internetworldstats.com/stats9.htm#eu](http://www.internetworldstats.com/stats9.htm#eu).  
(Mean = 52.3963; St. Dev. = 8.68196)</td>

<td align="center">100</td>

</tr>

<tr>

<td align="center">MEANHOURSNET</td>

<td align="center">Internet consumption  
_(Missing data)_ </td>

<td align="center">Mean weekly hours spent on Internet per country. <u>Source</u>: Report of the World Association of Newspapers 2006  
(Mean = 3.6441; St. Dev. =0.94253)</td>

<td align="center">72.0  
(28.0)</td>

</tr>

<tr>

<th colspan="4" align="left">Market-specific</th>

</tr>

<tr>

<td align="center">#MIXED-COMP-MARKET</td>

<td align="center">Relative number of mixed bundling competitors in the relevant market (%)  
_(Missing data)_ </td>

<td align="center">The number of other newspapers in the same country/region that adopt mixed bundling, divided by the total number of newspapers in the same country/region that apply either mixed or no bundling (in %).  
(Mean = 48.2139; St. Dev. = 29.05623)</td>

<td align="center">68.3  
(31.7)</td>

</tr>

<tr>

<td align="center">#MIXED-COMP-SEGM</td>

<td align="center">Relative number of mixed bundling competitors in the relevant market and market segment (%)  
_(Missing data)_ </td>

<td align="center">The number of other newspapers in the same country/region _and_ market segment as the newspaper itself (either quality or popular) that adopt mixed bundling, divided by the total number of newspapers in the same country/region and segment that apply either mixed or no bundling (in %).  
(Mean = 44.7112; St. Dev. = 36.17382)</td>

<td align="center">63.4  
(36.6)</td>

</tr>

<tr>

<td align="center">%MIXED-COMP-MARKET</td>

<td align="center">Market share of mixed bundling competitors in the relevant market (%)  
_(Missing data)_ </td>

<td align="center">The sum of the market shares of the other newspapers in the same country/region that adopt mixed bundling (in %).  
(Mean = 27.9652; St. Dev. = 26.72401)</td>

<td align="center">67.1  
(32.9)</td>

</tr>

<tr>

<td align="center">%MIXED-COMP-SEGM</td>

<td align="center">Market share of mixed bundling competitors in the relevant market and market segment (%)  
_(Missing data)_ </td>

<td align="center">The sum of the market shares of the other newspapers in the same country/region _and_ market segment (either quality or popular) that adopt mixed bundling (in %).  
(Mean = 18.7686; St. Dev. = 25.86244)</td>

<td align="center">62.2  
(37.8)</td>

</tr>

<tr>

<td align="center">AVDISC(SITE)-COMP-MARKET</td>

<td align="center">The average discount granted by competitors with a site subscription (%)  
_(Missing data)_ </td>

<td align="center">The average discount given by the other newspapers in the country/region that bundle their site and print subscription (in %).  
(Mean = 33.8200; St. Dev. = 6.61113)</td>

<td align="center">15.9  
(84.1)</td>

</tr>

<tr>

<td align="center">AVDISC(PDF)-COMP-MARKET</td>

<td align="center">The average discount granted by competitors with a PDF subscription (%)  
_(Missing data)_ </td>

<td align="center">The average discount given by the other newspapers in the country/region that bundle their PDF and print subscription (in %).  
(Mean = 29.0867; St. Dev. = 10.51569)</td>

<td align="center">14.6  
(85.4)</td>

</tr>

<tr>

<td align="center">WAVDISC(SITE)-COMP-MARKET</td>

<td align="center">The weighted average of the discount granted by competitors with a site subscription (%) - related to market shares  
_(Missing data)_ </td>

<td align="center">The weighted discount is calculated as  
D<sub>Y</sub> = (D<sub>COMP1</sub> * MARKSHARE<sub>COMP1</sub>+ ... + D<sub>COMPi</sub>* MARKSHARE<sub>COMPi</sub>)/  
MARKSHARE<sub>COMP1</sub> + ... + MARKSHARE<sub>COMPi</sub>  
(Mean = 31.857500; St. Dev. = 8.3523444)</td>

<td align="center">14.6  
(85.4)</td>

</tr>

<tr>

<td align="center">WAVDISC(PDF)-COMP-MARKET</td>

<td align="center">The weighted average of the discount granted by competitors with a PDF subscription (%) - related to market shares  
_(Missing data)_ </td>

<td align="center">The weighted discount is calculated as  
D<sub>Y</sub> = (D<sub>COMP1</sub> * MARKSHARE<sub>COMP1</sub>+ ... + D<sub>COMPi</sub>* MARKSHARE<sub>COMPi</sub>)/  
MARKSHARE<sub>COMP1</sub> + ... + MARKSHARE<sub>COMPi</sub>  
(Mean = 28.678333; St. Dev. = 10.4355344)</td>

<td align="center">14.6  
(85.4)</td>

</tr>

<tr>

<td align="center">RATIO-COMP-SITE</td>

<td align="center">The average ratio SITE/PRINT subscription of competitors  
_(Missing data)_ </td>

<td align="center">The average ratio of the annual SITE subscription divided by the annual print subscription, adopted by the competitors in the same country/region.  
(Mean = 61.9520; St. Dev. = 17.38923)</td>

<td align="center">24.4  
(75.6)</td>

</tr>

<tr>

<td align="center">RATIO-COMP-PDF</td>

<td align="center">The average ratio PDF/PRINT subscription of competitors  
_(Missing data)_ </td>

<td align="center">The average ratio of the annual PDF subscription divided by the annual print subscription, adopted by the competitors in the same country/region.  
(Mean = 51.4330; St. Dev. = 13.79970)</td>

<td align="center">24.4  
(75.6)</td>

</tr>

</tbody>

</table>