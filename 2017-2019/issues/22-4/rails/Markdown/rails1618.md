<header>

#### vol.22 no. 4, December, 2017

</header>

Proceedings to RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016.

<article>

# Practising humour: knowledge sharing and humour in the workplace

## [Dean Leith](#author) and [Hilary Yerbury](#author)

> **Introduction** Multi-disciplinary teams created to develop more sustainable ways of working are a focus for investigation into the practices of knowledge sharing. This study, taking a practice theoretical approach, explores the extent to which humour is associated with knowledge sharing and the roles it may play in the practice.  
> **Method.** Nine meetings of a multi-disciplinary project team in local government established to model new sustainable work practices were observed, audio-recorded and subsequently transcribed.  
> **Analysis.** Content analysis of the transcriptions of the meetings was carried out, using emergent coding. The frequency, types and themes of humour used by the team were identified and considered in the context of information activities.  
> **Results.** Among the fifty-two instances of humour, the three most frequent types were witticisms, put-downs and self-denigration. The themes of creativity, exercising control and superiority to others emerged. Humour was mostly used in discussions of administrative aspects of the project, including agenda setting, evaluation and reporting to other council committees.  
> **Conclusion.** The use of humour demonstrated a paradox between needing to act as a creative and innovative team to model a new way of working and continuing to work in the traditional reporting and communication structures of the traditional workplace.

<section>

## Introduction

Recent initiatives implementing placed-based planning in the government sector emphasise the importance of bringing together different perspectives, interests and expertise through innovative processes. There is a shift in focus towards sustainability of work processes and sustainability of the environment ([Untaru,2002](#unt02)). Outcomes-focused teams may replace the traditional ‘professional silos’ ([Mant, 2008](#man08)) in these new work processes and developing a group identity is an important step in these collaborative endeavours ([Cheng and Daniels, 2003](#che03)). Knowledge sharing is central to the success of a range of initiatives undertaken by Australian national, state and local governments as they work towards sustainability ([Cundill, 2010](#cun10)). Such organisations encompass a range of different disciplines and specialist knowledge including finance, engineering, communications and urban horticulture as they come to terms with issues and develop the ability to change ways of working that depend on the sharing of this specialist knowledge ([Blackmore, 2007](#bla07)). The success of knowledge sharing practices in this context often determines the extent to which an organisation meets its community’s expectations. The use of humour has often been considered a significant element in the successful workings of a collaborative project. Although humour has been found to be inappropriate, misunderstood and deliberately offensive ([Plester, 2014](#ple14)), Tracy, Myers and Scott ([2006](#tra06)) consider humour to have positive workplace impacts such as bridging hierarchical gaps and fostering a collegial working climate ([Cooper, 2008](#coo08)).

The present study extends previous research on knowledge sharing and emotion ([Leith and Yerbury, 2015](#lei15)) by focusing on the role of humour as a component of knowledge sharing practice. The research takes a practice theoretical approach that emphasises the importance of ‘sayings, doings and tasks’ ([Schatzki, 2002](#sch02)) and the context of the practices under study. Knowledge sharing activities may be seen as practices in themselves as well as components of larger practices. Practices are not individual activities but are formed, interwoven and sanctioned through an intra-group discursive process that may include educational, workplace or religious discourse ([Lloyd, 2010](#llo10)). Schatzki ([2006](#sch06)) contends that practices are not only constituted by ‘how to do things’ and by rules, but also by teleo-affective features which structure emotions that are acceptable or prescribed for the participant, both in practice and in a specific context. This study seeks to investigate selected emotional elements of knowledge sharing practice, namely conversational humour and joking, in a specific organisational context, that of an Australian local government workplace.

## Review of the literature

There is a sizeable literature on the use of humour in organisational contexts. A number of researchers such as Tracey _et al._, ([2006](#tra06)), Plester ([2014](#ple14)), and Kim and Plester ([2014](#kim14)) point to previous research which identifies _superiority, relief or incongruity_ as the primary motivations behind the use of humour. _Superiority_ theories suggest that individuals use humour so they may feel superior to, and distanced from, others. Examples include situations where managers may use humour toward workers of a lower status in order to reinforce their higher status in the organisation. _Relief theories_ concern the use of humour as physical or emotional relief due to tension or boredom. _Incongruity theories_ emphasise humour as a way of forging consistency between an internal cognitive framework and the workplace environment. Billig ([2005](#bil05)), however, identifies a darker and _‘more shameful’_ side of humour. He claims that ridicule plays a central role in the use of social humour and is often used to gain social power. Indeed, disciplinary and malicious motives behind humour are not often consciously considered and the cruelty and lack of sympathy inherent in laughter targeted at others may not be acknowledged even to oneself ([Plester, 2014](#ple14)).

While many theories concern individual and psychological motivations behind humour, a sociocultural approach to the study of humour in the context of knowledge sharing practice must consider the potential functions of humour as well as the inherently social and cultural context of workplace practices. Studies indicate that humour is often ambiguous. Grugulis ([2002](#gru02)), for example, found that constructing jokes by juxtaposing two different frames of reference provided a glimpse of alternative (and shared) perceptions of reality and that the ambiguity of humour enabled contentious statements to be made without fear of recrimination. Thus, it is not surprising that humour is such a complex phenomenon that can be manifested in many different ways. Dynel ([2009](#dyn09)) offers a useful approach to the definition of humour by distinguishing between non-verbal humour, which arises from pictures or body language, and verbal humour that is produced by language and text. She then divides the latter into _canned jokes_ which are discourse units consisting of a set-up and a punch line which engender surprise or incongruity, and _conversational humour_ which is spontaneous or pre-constructed interactional humour, different from jokes.

Fine and Wood ([2010](#fin10)) make a similar distinction and go further to suggest that _conversational humour_, which they term _jocularity_ or _humorous talk_, must be an embedded part of an ongoing relationship as well as being interactive and referential; that is, that the parties have common references. Dynel’s linguistic approach ([2009](#dyn09)) categorises conversational humour as witticisms, retorts, teasing, banter, put-downs, self-denigrating humour and anecdotes. Witticisms are defined as a clever or humorous textual unit interwoven into a conversational exchange; retorts are similar to witticisms but with a view to amusing the hearer; teasing is a jocular utterance performing a variety of pragmatic functions such as mock challenges or imitation; and banter is longer exchanges of repartee, where two or more participants take part in a humorous interchange ([Dynel, 2009](#dyn09)). More negative humour emerges from put-downs which are disparaging or abusive remarks to others and self-denigration which is self-disparaging, self-mockery or self-directed joking. Anecdotes are humorous narratives derived from personal experience or other people’s lives ([Dynel, 2009](#dyn09)).

From a socio-cultural perspective, humour can be used to subvert power structures and control, and to construct oppositional identities or, alternatively, to assist managers in increasing staff motivation, stimulating creativity or boosting productivity ([Butler, 2015](#but15)). Humour can act as a co-operative face-oriented discourse in a range of contexts, but also as a repressive discourse of manipulative superiors. In asymmetrical contexts and in impolite manifestations of the less powerful to those in power it can subvert repressive or coercive discourse ([Holmes, 2000](#hol00)). It can also create insiders and outsiders, as Charman ([2013](#cha13)) found in a study of police officers, ambulance drivers, and members of the fire service. Tracy _et al._ ([2006](#tra06)) define humour as an unfolding, collaborative and interactional practice that can play a key part in socialising newcomers, building knowledge and constituting the organisational process. Such an approach emphasises the relationship between humour and knowledge building as well as sharing knowledge. Tracy _et al._ ([2006](#tra06)) apply a sense-making approach to a study of human service workers. Although their study reinforces some results of previous studies with respect to motivations, importantly it suggests that humour can assist to alter and evolve understandings of work roles and allow organisational members to interpret situations, develop shared understandings and build knowledge that could be referenced in future situations. They claim that this sense-making process, manifest in everyday practice and talk, generates organisational learning about work duties, clients, other employee groups and one’s self ([Tracey _et al._, 2006](#tra06)).

## Methodology

This investigation focuses on a collaborative, multi-disciplinary work team involved in an initiative coordinated by a local council in Australia and is concerned with intra-agency knowledge sharing. It is based on the practices of a project team initiated by the council's management to transform the policy, planning and work practices from a very traditional siloed structure and approach to a more collaborative workplace, championing place-based planning. The project team included members from disciplines as diverse as engineering, urban horticulture and community services and is identified here by the pseudonym Avenir Project Team (APT). The research study was approved by the Human Research Ethics Committee of the University of Technology Sydney and approval for the researcher to attend project team meetings for this research was granted by the APT project manager and endorsed by the organisation’s management. Data were collected between June 2014 and May 2015.

### Data collection

One researcher attended, observed and audio-recorded nine project team meetings of the Avenir Project Team. These recordings were transcribed, with annotations on tone of voice and other conversational features such as talking over each other. The researcher also took notes on participants’ non-verbal behaviours at all meetings. Instances of laughter identified in the transcripts were taken as the cue that conversational humour was being used and identified as an instance. Where conversational humour was attempted in the opinion of the researchers but no laughter ensued, this was also identified and tagged as an instance of humour.

### Analysis

There were three aspects to the analysis. Firstly, the meeting transcripts were analysed by each researcher for instances of humour using Dynel’s ([2009](#dyn09)) categories of conversational humour. Next, the transcripts were analysed using both _a priori_ and emergent coding to identify the theme or purpose of the humour within the broader context of knowledge sharing. A number of categories emerged. The examples in each category were then matched against the categorisations in the broader literature on humour. Finally, the context of these instances of conversational humour was analysed according to the four activity categories suggested by Lloyd’s ([2010](#llo10)) study. Although Lloyd had linked them to information literacy practice, these activity categories were deemed relevant and appropriate to the knowledge sharing practices of this group ([Leith and Yerbury, 2015](#lei15)). The four activity categories are: _influencing_ defined as advocacy, negotiation, convincing, engaging and attempting to change colleagues’ opinions and practices; _sharing_ defined as sharing professional or disciplinary expertise or knowledge/information on rules and ways of working; _information work_ defined as work aimed at production and reproduction of collective organisational knowledge, such as the development of agendas and evaluation criteria, the creation of communications to the general public and the writing of reports and grant proposals; and _coupling_ defined as facilitating awareness of where information/knowledge is and access strategies to get it, that is, connecting people with information.

## Findings

### Instances of conversational humour

Laughter occurred at all of the nine Avenir Project Team meetings attended by the researcher, however, the amount and type of humour varied from meeting to meeting without any apparent pattern. Using Dynel’s ([2009](#dyn09)) categories of conversational humour, instances in each category were recorded as outlined in Table 1 below:

<table><caption>Table 1: Categories of humour</caption>

<tbody>

<tr>

<th>Humour category  
(Dynel 2009)</th>

<th>Instances of humour with laughter</th>

<th>Instances of humour without laughter</th>

<th>

**Totals**</th>

</tr>

<tr>

<td>Put-down</td>

<td>16</td>

<td>4</td>

<td>

**20**</td>

</tr>

<tr>

<td>Witticism</td>

<td>12</td>

<td></td>

<td>

**12**</td>

</tr>

<tr>

<td>Self-denigrating</td>

<td>10</td>

<td></td>

<td>

**10**</td>

</tr>

<tr>

<td>Teasing</td>

<td>4</td>

<td>2</td>

<td>

**6**</td>

</tr>

<tr>

<td>Banter</td>

<td>4</td>

<td></td>

<td>

**4**</td>

</tr>

<tr>

<td>Totals:</td>

<td>

**46**</td>

<td>

**6**</td>

<td>

**52**</td>

</tr>

</tbody>

</table>

The most frequent humour type was put-downs, closely followed by witticisms and self-denigration. Teasing and banter were less frequent. Witticisms, self-denigration and banter always resulted in some laughter whereas put-downs and teasing occasionally was not followed by laughter.

### Key themes

The key themes that emerged from instances of humour are _creativity, exercising control_ and _superiority to others_. While exercising control and superiority to others may need no definition, creativity is about bringing new approaches to existing processes and procedures. This theme was introduced in the second meeting when Felicity, the team convenor, summarised the team’s role: _‘creativity and problem-solving, because that’s what we do here, solve problems, and we need to be <u>creative</u> about how we do it, for example, <u>pink</u> speed humps’._ In meeting four, there was a discussion about how to encourage residents to actually read the notices sent by mail from the council and Abby, picking up on a current concern, joked: _‘We could say they are flood affected …’_ to which Genevieve promptly adds: _‘Yeah, but the tree we planted [outside your house] is going to eliminate it.’_ Also, from meeting four, Lydia showed that creativity was a whimsical thread running through the meetings when she says: _‘Remember the first meeting when we put cake on the evaluation form for the site visit? … Then we had to evaluate how much cake we had?’_ Felicity’s interjection showed that the team was expected to be trend setters for the Council: _‘And even now, no one else has done it!’._

_Exercising control_ is often achieved through self-denigration, where some team members draw attention to their individuality or their oversights to seek attention or control others. Chris joked in his very broad Scots accent that people might not understand what he is saying because: _‘my accent’s a wee bit thick’_ (meeting one); Jane, who in meeting two had been jokingly dubbed as _Manager of <u>Integrated Creativity</u>_, responded in meeting five when discussing her personal marketing technique: _‘Well … I don’t just want a logo Genevieve, I need a whole new brand!’_. Self-denigration was often used by Felicity, the team convenor, as she controlled the team and its tasks. She put herself down about her approach to managing meetings and delegation: _‘X has the responsibility for compiling the report’_, then, realising her oversight: _‘even though she might not know it… cause I haven’t told her yet’_ (meeting four); and in reviewing the actions in meeting five: _‘oh I haven’t done that –no, that’s a total lie – um, I have reviewed the objectives but I haven’t called the … meeting – I just need to adjust that – um … Incomplete!’._ She then used a fake voice to acknowledge the next action item is also _‘INCOMPLETE!’_. At other times Felicity, as the meeting convenor, banters and jokes to cajole her colleagues and to keep the agenda firmly on track.

The third theme emerging from the data is _superiority to others_, through the opposition of insiders and outsiders. The team seem to reinforce their own identity through creating a distance between the members and other stakeholders both inside and outside the council. The _Traffic Committee_ was mocked in several exchanges: _‘We are going to try and bring creativity into what we are doing … a project … that we can apply a creativity lens to … like the Traffic Committee… Excellent! Yeah, that’s the target isn’t it, the Traffic Committee?’_. They also cast some Not In My Back Yard (NIMBY) residents as outsiders: _‘That doesn’t seem to outweigh [the issue of] parking, though … they might be supportive about the [tree] project but just not in front of <u>their</u> house’_ (meeting three). A discussion about an innovation, of which the team was justifiably proud, showed not only the sense of being a solid group, but also a team better than other teams: _‘Let’s be first and put in for an award?’_ and _‘Yes, that’s the only reason we do things, to win!’._

### Humour and information activities

Analysing the instances of humour in the context of the information activities adapted from Lloyd ([2010](#llo10)) reveals an interesting pattern. A previous report on this research study ([Leith and Yerbury, 2015](#lei15)) had shown that _sharing_ was the most commonly occurring activity during the meetings and _information work_ was the second least commonly occurring. Interestingly though, the majority of the instances of humour (36 out of 52) took place while participants were engaged in information work such as the development of agendas and evaluation criteria, the creation of communications to the general public and the writing of reports and grant proposals. During _sharing_ activities (sharing professional or disciplinary expertise and/or knowledge or information about rules and ways of working), there were only fifteen instances of humour and in _coupling_ (facilitating awareness of where information or knowledge is and access strategies to get it, i.e. connecting people with information), only one humour instance was recorded. No instances of humour occurred during _influencing_ activity (advocacy or negotiation and sharing of collective organisational meaning – convincing, engaging, attempting to change colleagues’ opinions and practices).

Those instances of humour during _sharing_ activities were more likely to be aimed at building a sense of the collective; of making a group of insiders. Dan (meeting four) introduces himself as: _‘I’m Dan … I do trees’_ to which Felicity replies: _‘Just call him the Lorax, he speaks for the trees’._ In a discussion about a potential conflict between vehicles and pedestrians on a street they are designing, Nick says quite seriously: _‘And most people wouldn’t gamble against a vehicle’ to which Genevieve quickly quips: ‘Yeah! I would … you have got to enforce your rights as a pedestrian!’._ The one instance of humour in a _coupling_ activity involved Kevin explaining that he had been on leave and so had not followed up on a report members of the group had been referred to. Felicity exaggerates: _‘Oh! Hopeless … really, you need to show a little more dedication’._ Genevieve picks this up and takes the irony a step further: _‘You could have come in on Sunday to read it!’_ (meeting eight). This example also reflected the expectation of team cohesion which was a regular source of humour. While discussing possible methods of delivering their sustainability messages to the rest of the organisation, Genevieve jokes about dressing up in _‘cult uniforms’_ and writing a team song to be sung at key management meetings such as Traffic Committee meetings.

Much of the humour during _information work_ activity was involved in managing the agenda of the team and with interactions with other committees or other organisations. Chris was overly enthusiastic in reporting on his recent trip _‘when I was overseas earlier in the year’_ and was eventually cut off by the convener. Ian, whose turn it was to speak next, mocks Chris’ verbosity and mention of his overseas trip: _‘Yes, very quickly, in the remaining seconds … yes … during my <u>recent trip overseas</u> …’_ (meeting one). Felicity noted how difficult it could be to get agreement on terminology in a report: _‘Everyone is complaining so much, so I just stopped calling them demonstration projects and I haven’t called them anything! I think that in one of my last emails I just called them ‘spotlight’ or ‘focus’ projects’_ (meeting three).

These examples of humour, which are sometimes banter and sometimes closer to overt teasing, are inserted into discussions of organisational practices. They include the example from meeting four about the establishment of evaluation criteria (_‘Then we had to evaluate how much cake we had’_), where the banter went on: _‘When do we do that [cake] expectation session? … Mine are dashed, completely dashed!’._ Here it is clear that team members are joking about the criteria involved in their traditional evaluation process. Genevieve (meeting eight) shows her understanding of the processes of _information work_ when she says during a discussion of a proposed model of community consultation which is different from those used previously by the Council: _‘I think on page 6 you have explained it –the level of engagement – more broadly. It’s called the Felicity Smith model!’._ Felicity responds with tongue firmly in cheek: _‘Yes, apparently it’s widely used in the best circles’._ In meeting eight, the joking about the Mayor’s decision to discontinue staff Christmas gifts permeates a lengthy exchange about the outcome of a meeting with senior management, with team members anticipating the outcome:

> Felicity: They were very happy with [the work of the team] but we are not going to give you MONEY! We just want to see on-ground works … works and works.  
> Genevieve: But no Christmas present …  
> Felicity: But no Christmas present!  
> Genevieve (laughing): ‘And – no budget!  
> Felicity: No, I did get the budget.

Felicity then explains the processes she and the senior managers went through before an agreement was made for the continuation of the project: _‘Do the work, but don’t take away [any resident] parking was the big message I took away from that meeting’._ Participants roll their eyes and giggle.

## Discussion

The findings of the study indicate that humour is used in a number of ways by members of this team in their meetings, as suggested in the literature. Dynel’s categories of conversational humour were appropriate for identifying the types of humour used and thus for highlighting complexities in the interactions of the group. Whilst witticisms and banter can be seen to lighten the mood of the team ([Charman, 2013](#cha13); [Fine and Wood, 2010,](#fin10) p.173), putdowns and self-denigration clearly operate as the exercise of power ([Grugulus, 2002](#gru02)).

Humour is important in forging a team from a group of individuals who have volunteered to take on involvement in this new way of working. The findings presented here confirm Fine and Wood’s argument ([2010](#fin10)) that conversational humour must be an integral part of social interaction, being interactive; as evidenced by the bantering and extended use of witticisms, and referential; as evidenced through the use of shared experiences, such as the cancelling by the Mayor of Christmas gifts. Felicity, the team convenor, is the most active in initiating instances of humour and she also makes greater use of the range of categories of humour than others. Laughter, as Jane one of the team members states when she quips: _‘Laughter and alcohol, they are the two ingredients to creativity! That’s how we get our brains to open!_, is key to the establishment of sound social relationships and productive work relationships ([Butler, 2015](#but15)) and Felicity and Genevieve clearly do their best to get the team laughing, with witticisms and banter. Team members are also aware of the importance of creating a cohesive group, and use humour, as Holmes ([2006](#hol06)) indicates, to reinforce a co-operative, face to face discourse.

There are relatively few examples of laughter at the expense of an immediate team member. Rather, put-downs, referred to by Charman as _‘combative humour’_, are usually directed at outsiders, in this case, those who oppose or obstruct group activities ([Charman, 2013](#cha13), p.164). Billig ([2005](#bil05)) claims that ridicule is an important element of social humour and so it is in the meetings of this team. Much of the ridicule here is directed outside the team, at the residents or at committees, giving that _sense of superiority_ of this team over others with whom they are engaged, and creating the notion of insiders and outsiders, and thereby also strengthening team cohesion.

The emphasis on _creativity_, and the conscious focus on innovation, essential components of place-based planning ([Reed _et al._, 2014](#ree14); [Untaru, 2002](#unt02)), are important aspects of the team’s practice. As part of the process of breaking down professional silos, cross-disciplinary teams are expected to develop work practices that challenge the old ways of doing things ([Manring and Moore, 2006](#man06)). The team’s members have acknowledged their role as _creative problem solvers_ with the remit to change work practices across the organisation by modelling this creative approach. Thus it is not surprising that there are many references to approaches that are far removed from conventional bureaucratic practices, such as evaluating the meeting by the amount of cake eaten or proposing that speed humps be painted pink.

While overall there was very little emphasis on _information work_ across all meetings compared to the other categories of information activities, still, the majority of the instances of humour (36 of 52 instances) are related to this sort of activity, such as agenda items, reporting, setting evaluation criteria and so on. Most instances would take only a few seconds from a meeting lasting more than an hour, but collectively they shed interesting light on the relationship between team members and the documenting of the team’s collective knowledge in reports or funding proposals. For this team, sharing their specialist knowledge to solve problems of environmental sustainability was their focus; their ability to come up with creative solutions was embedded as part of their collective identity. As a group, they did not seem to demonstrate concern for the administrative aspects related to the project. Through their joking about establishing evaluation criteria, working with committees or about communicating with residents, it would almost have seemed that they had little regard for these aspects of their collaborative project.

Yet, it would be a mistake to consider their conversational humour in that way. Rather, the use of humour relating to reporting relationships, writing reports and proposals, communicating with residents and other Council committees, provides deeper insights into the workings of the team. At one level, the team is expected to act outside the norms. It has _creativity_ through the innovation of work practices as its key objective and is expected to help change practices throughout the wider Council. At another level however, the team is also required to work within the existing Council’s reporting and communication structures. Thus, some of the instances of humour, for example, the suggestion to dress up or develop a team song, can be seen as a conscious effort to acknowledge their responsibility and make fun of it at the same time.

These are people who know what they are expected to do and who have been nominated into this team because they have good technical knowledge and a sound knowledge of the workings of the organisation. Their mockery of established processes and jokes about the conventions of administrative practice can be seen as parody, a form of humour which requires an excellent understanding of the original form, from which to create an exaggerated and intentionally funny imitation. This understanding of the expectations of innovation and of the requirements of conventions of communication create a paradoxical situation, which can be sensed in Patrice’s question about what the team needs to bring about broader changes in work practices in the organisation. When she is unable to sustain the preceding joking banter, she sighs and snaps, _‘Yeah, yeah, and what do we need to do it?’_ This serious comment brings the team back to earth and emphasises the huge challenges in achieving sustainable change in the broader organisation.

## Conclusion

On the surface, the findings of this study are similar to those in many other studies of humour. Humour can be used for a variety of purposes, from creating a sense of cohesion to softening exercises of power to oiling the wheels of social interactions. However, the use of a practice approach and, in particular, Lloyd’s ([2010](#llo10)) information activities, has been able to extend the findings beyond previous studies. The emphasis on the use of humour in _information work_, an information activity not strongly represented in the practices of this team, suggests that humour is used to manage the paradox of needing to forge new ways of working, while still needing to deal with the old, established ways of the organisation.

The team’s use of conversational humour to address a paradoxical situation reflects Grugulis’ ([2002](#gru02)) contention that constructing jokes by juxtaposing two different frames of reference provides a glimpse of alternative (and shared) perceptions of reality. It also supports Tracy’s _et al._ ([2006](#tra06)) finding that humour can assist to alter and evolve understandings of work roles and allow organisational members to interpret situations, develop shared understandings and build knowledge that could be referenced in future situations.

This study has implications for the practice of knowledge-based organisational change relevant to managers. It has highlighted that it is important for team members to be experts in the traditional administrative practices of an organisation and to be able to use this knowledge to make fun of and subvert established approaches as they explore new ways of working. Although references to administrative practices are infrequent in the meetings, the significance for the team in meeting their goals of organisational change is marked by these uses of humour.

The study also has implications for research into knowledge sharing in situations of organisational change. Its use of the practice theoretical approach has placed a focus on the context within which activities occur; the consequence of this is that the put-downs and banter which make up the majority of the instances of humour can be seen not merely as expressions of conversational humour, helping the conduct of a meeting. Instead, they can be seen as a further expression of expertise, in this case of organisational practices, and of attempts to explore how this expertise might be used to introduce innovative processes. The use of practice theory offers opportunities for exploring well-researched topics in new ways.

## Acknowledgements

The authors would like to thank the anonymous reviewers and editor for their helpful comments and suggestions.

</section>

<section>

## <a id="author"></a>About the authors

**Dean Leith** is a final year doctoral student at the University of Technology Sydney undertaking research into knowledge sharing from a practice theoretical perspective. He holds an MLib from the University of New South Wales and has broad professional experience in the practice of information management in the higher education, government and corporate sectors. He can be contacted at [dean.v.leith@student.uts.edu.au](mailto:dean.v.leith@student.uts.edu.au)

**Hilary Yerbury** is Adjunct Professor in Information Studies at the University of Technology Sydney. Her background in European social and political cultures, information management, anthropology and development studies have given her a broad-based approach to the use of information in everyday decision-making and in social change. She has extensive experience in working with young people on development issues. She can be contacted at [Hilary.Yerbury@uts.edu.au](mailto:hilary.yerbury@uts.edu.au)

</section>

<section>

## References

<ul>
<li id="bil05"> Billig, M. (2005). <em>Laughter and ridicule: towards a social critique of humour. </em>London: Sage.</li>
<li id="bla07"> Blackmore, C. (2007). What kinds of knowledge, knowing and learning are required for addressing resource dilemmas? a rhetorical overview. <em>Environmental Science and Policy, 10,</em> 512-525.</li>
<li id="but15"> Butler, N. (2012). Joking aside: theorizing laughter in organisations. <em>Culture and Organisation, 21</em>(1), 42-58.</li>
<li id="cha13"> Charman, S. (2013). Sharing a laugh: the role of humour in relationships between police officers and ambulance staff. <em>International Journal of Sociology and Social Policy, 33</em>(3/4), 152-166.</li>
<li id="che03"> Cheng A. &amp; Daniels, S. (2003). Examining the interaction between geographic scale and ways of knowing in ecosystem management: a case study of place-based collaborative planning. <em>Forestry Sciences, 49</em>(6), 841-854.</li>
<li id="coo08"> Cooper, C. (2008). Elucidating the bonds of workplace humour: a relational process model. <em>Human Relations, 61</em>(8), 1087-1115.</li>
<li id="cun10"> Cundill, G. (2010). Monitoring social learning processes in adaptive co-management: three case studies from South Africa. <em>Ecology and Society, 15,</em> 28-47. </li>
<li id="dyn09"> Dynel, M. (2009). Beyond a joke: types of conversational humour. <em>Language and Linguistics Compass, 3</em>(5), 1284-1299.</li>
<li id="fin10"> Fine, G. A. &amp; Wood, C. (2010). Accounting for jokes: jocular performance in a critical age. <em>Western Folklore, 69</em>(3-4), 299-321.</li>
<li id="gru02"> Grugulis, I. (2002). Nothing serious? Candidates use humour in management training. <em>Human Relations, 55</em>(4), 387-406.</li>
<li id="hol00"> Holmes, J. (2000). Politeness, power and provocation: how humour functions in the workplace. <em>Discourse Studies, 2</em>(2), 159-185.</li> 
<li id="hol06"> Holmes, J. (2006). Sharing a laugh: pragmatic aspects of humour and gender in the workplace. <em>Journal of Pragmatics, 38,</em> 26-50.</li> 
<li id="kim14"> Kim, H. &amp; Plester, B. (2014). <em><a href="http://www.webcitation.org/6sPGH5cjo">Ironing out the differences: the role of humour in workplace relationships.</a></em> Paper presented at the Australasian Humour Studies Network 20th Annual Colloquium, Wellington, New Zealand. 
Retrieved from http://sydney.edu.au/<wbr>humourstudies/docs/AHSN_Conference_2014.pdf
(Archived by WebCite&reg; at http://www.webcitation.org/<wbr>6sPGH5cjo)</li>					
<li id="lei15"> Leith, D. &amp; Yerbury, H. (2015). Organizational knowledge sharing, information literacy and sustainability: two case studies from local government. In S.  Kurbanoglu, J. Boustany, S. Špiranec, E. Grassian, D. Mizrachi and L. Roy, (Eds.), <em>Information Literacy: Moving Towards Sustainability: Third European Conference on Information Literacy (ECIL), Tallinn, Estonia, 19-22 October 2015 </em>(pp. 13-21). Cham: Springer.</li>
<li id="llo10"> Lloyd, A. (2010). Framing information literacy as information practice: site ontology and practice theory. <em>Journal of Documentation, 6</em>(2), 245-258.</li>
<li id="man06"> Manring, S. &amp; Moore, S. (2006). Creating and managing a virtual inter-organisational learning network for greener production: a conceptual model and case study. <em>Journal of Cleaner Production, 14,</em> 891-899.</li>
<li id="man08"> Mant, J. (2008). Place management as a core role in government. <em>Journal of Place Management Development, 1,</em> 100-108.</li>
<li id="ple14"> Plester, B.A. (2014). <em><a href="http://www.webcitation.org/6sPGH5cjo">When a joke is not a joke: The dark side of organisational humour.</a></em> Paper presented at the  Australasian Humour Studies Network 20th Annual Colloquium, Wellington, New Zealand.
Retrieved from http://sydney.edu.au/<wbr>humourstudies/docs/AHSN_Conference_2014.pdf</a> (Archived by WebCite&reg; at http://www.webcitation.org/<wbr>6sPGH5cjo)</li>
<li id="ree14">  Reed, M., Godmaire, H, Abernethy, P. &amp; Guertin, M-A. (2014). Building a community of practice for sustainability: strengthening learning and collective action of Canadian biosphere reserves through a national partnership.  <em>Journal of Environmental Management, 145, </em>230-239. </li>
<li id="sch02"> Schatzki, T. (2002). <em> The site of the social: a philosophical account of the constitution of social life and change.</em> Philadelphia, PA: Pennsylvania State University Press. </li>
<li id="sch06"> Schatzki, T. (2006). On organizations as they happen. <em>Organization Studies, 27</em>(12), 1863-1873. </li>
<li id="tra06"> Tracy, S.J., Myers, K.K. &amp; Scott, C.W. (2006). Cracking jokes and crafting selves: sense-making and identity management among human service workers. <em>Communication Monographs, 73</em>(3), 283-308. </li>
<li id="unt02"> Untaru, S. (2002). Place-based planning for NSW local government; the Warringah local environmental plan 2000. <em>Australian Planner, 39</em>(2), 83–89. </li>
</ul>

</section>

</article>