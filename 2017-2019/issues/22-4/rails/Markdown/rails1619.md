<header>

#### vol.22 no. 4, December, 2017

</header>

Proceedings of RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016\.

<article>

# The interface between indigenous knowledge and libraries: the need for non-Māori librarians to make sense of mātauranga Māori in their professional lives

## [Kathryn Oxborrow](#author), [Anne Goulding](#author) and [Spencer Lilley](#author)

> **Introduction.** This paper outlines the context of research in progress, investigating how non-indigenous librarians in Aotearoa New Zealand make sense of indigenous knowledge in their professional lives. It presents evidence of developments in the information and library environments which make it imperative that non-Māori librarians engage appropriately with mātauranga Māori (Māori knowledge).  
> **Method.** An analysis of recent developments driving or inhibiting engagement with mātauranga Māori is presented alongside a review and synthesis of previous work relevant to the topic.  
> **Results.** The analysis suggests that there are a number of specific issues at the interface between indigenous knowledge and libraries which make it a particularly pressing issue for libraries and librarians both in Aotearoa New Zealand and internationally. Recent developments in the field including those around appropriate metadata and ownership protocols suggest that it is an area of growing importance in the profession. The issues identified and discussed in this paper form the contextual background for an interview-based study, currently in progress, incorporating elements of Dervin’s sense-making methodology.  
> **Conclusion.** The many ways that librarians may encounter indigenous knowledge, and the national and international interest in the topic, highlight the issue of non-indigenous librarians’ engagement with indigenous knowledge as an important one for research.

<section>

## Introduction

This paper presents the context of research in progress, investigating how non-indigenous librarians in Aotearoa New Zealand make sense of indigenous knowledge in their professional lives. The paper highlights why this issue is of particular importance to librarians, given recent developments in our field.

Aotearoa New Zealand is an island nation in the South West Pacific with a population of around 4.7 million ([Statistics New Zealand, 2017](#sta17)). The indigenous Māori population arrived in Aotearoa before 1300 CE ([Royal, 2005](#roy05)). European immigrants began arriving in the early nineteenth century ([Royal, 2005](#roy05)). In 1840, the British Crown and over 500 Māori chiefs signed Te Tiriti o Waitangi-The Treaty of Waitangi, which was designed to mediate the relationship between the two sets of people ([Orange, 2012](#ora12)). After the Treaty was signed, wide-scale colonisation of Aotearoa New Zealand took place including the introduction of western political, legal, social and cultural structures. Over the years, several breaches of the Treaty occurred which led to Māori being dispossessed of much of their land ([Orange, 2012](#ora12)). In the 2013 census, 14.9% of New Zealanders identified as Māori ([Statistics New Zealand, 2013](#sta13)).

Libraries and information organisations in Aotearoa New Zealand are predominantly founded on western cultural ideals. In many countries, including Aotearoa New Zealand, libraries and professional education for librarians are a British or American import ([Carroll, Kerr, Musa and Afzal, 2013](#car13)).The first public library in Aotearoa New Zealand was established by a group of settlers in Wellington in 1841 (New Zealand Ministry for Culture and Heritage, 2014), based on a western understanding of knowledge and information. Thus, libraries in Aotearoa New Zealand have their roots in a worldview that emphasises the importance of the individual, with the role of the library to encourage individual betterment (e.g. [Finks, 1989](#fin89), p. 354). In the western understanding, knowledge is a discrete entity which can be owned by an individual (as expressed in intellectual property rights and legislations; see, for example, [World Intellectual Property Organization, 2011](#wor11)). European New Zealand culture typically transmits knowledge through written media and literacy, and librarians are traditionally seen as guardians or gatekeepers of that knowledge. The majority of librarians in Aotearoa New Zealand are operating with this worldview as their reference point, and may therefore face challenges in engaging with Māori culture, which is very different. One of the most significant differences between western and indigenous cultures is the way in which they conceptualise knowledge and information. Mātauranga Māori, or Māori knowledge, has a more holistic, all-encompassing perspective as explained further below.

There is an increasing need for librarians in Aotearoa New Zealand to make sense of mātauranga Māori to operate equitably and effectively in a bicultural context. Although there is a lack of empirical evidence, anecdotal accounts raise concerns that variability in librarians’ engagement with mātauranga Māori will limit the ability of the profession to meet its bicultural commitments. While institutions can have a commitment to these values, proper change will only occur when there is strong personal commitment from individual librarians. A number of studies demonstrating the need for improvement in a variety of areas related to mātauranga Māori indicate poor understanding of the nature and applicability of mātauranga Māori within the profession, however (e.g. [Evans, 2011](#eva11); [Hayes, 2012](#hay12); [Tuhou, 2011](#tuh11)). This research in progress will contribute to the body of knowledge in this area by investigating the ways in which non-Māori librarians in Aotearoa New Zealand are currently making sense of mātauranga Māori.

The first section of this paper will highlight aspects of indigenous knowledge and mātauranga Māori as defined by various Māori scholars. The majority of the paper will identify issues within the profession of librarianship that make it particularly important for librarians to be aware of and engage well with indigenous knowledge, and will highlight professional imperatives, both international, and local to Aotearoa New Zealand that make indigenous knowledge an important area of professional engagement for librarians.

</section>

<section>

## Notes on terminology

_Aotearoa New Zealand_ is used to refer to New Zealand, reflecting the bicultural nature of the country. Where specific reference is made to the western or European element of New Zealand culture, the term _European New Zealand_ is used.

The term _bicultural_ in the context of Aotearoa New Zealand _‘…refers to two distinct peoples living in one nation, but retaining their respective individual languages, identity, culture, traditions, educational systems, social services and businesses within the one economy’_ (Philips, 2006, as cited in [Szekely and Barnett, 2007](#sze07), p. 16). However, there is an increasing expectation that Māori will be able to access tailored services that recognise specific cultural needs.

The term _western_ is used as defined by Zimmerman ([2015](#zim15)): _‘…the culture of European countries as well as those that have been heavily influenced by European immigration…’_

Definitions of Māori words and phrases are taken from Moorfield ([2011](#moo11)) unless otherwise stated.

</section>

<section>

## Indigenous knowledge

Indigenous knowledge refers to the knowledge systems of indigenous peoples, which are different from western knowledge frameworks. The Alaska Native Science Commission ([n.d.](#aland)) highlight a number of differences between traditional indigenous knowledge systems and western ones. Western knowledge is only secular, whereas indigenous knowledge also incorporates the sacred or spiritual; western knowledge has an emphasis on written knowledge, whereas indigenous knowledge has more of an emphasis on oral and visual transmission; indigenous knowledge is more holistic whereas western knowledge is more segmented ([Alaska Native Science Commission, n.d.](#aland)). Of course, indigenous knowledge and western knowledge are both generic terms; indigenous groups and academic communities have their own unique bodies of knowledge. This has been simplified for the purposes of comparison.

</section>

<section>

## Mātauranga Māori

Mātauranga Māori is a type of indigenous knowledge. In this section we have attempted to represent the different ways in which Māori scholars define mātauranga Māori without distorting the original meanings of the authors, to highlight key aspects. A limitation of literature highlighted here is that it is restricted to resources in English, though the majority of the authors mentioned refer to sources in Te Reo Māori (Māori language).

The most basic definition of mātauranga Māori is Māori knowledge (e.g. [Mead, 2012](#mea12), p. 9; [Procter and Black, 2014](#pro14), p. 90) but the concept has a much more nuanced meaning than that. One of the main contrasts with a western understanding of knowledge is the holistic nature of mātauranga Māori in that it incorporates cultural knowledge such as tikanga (the customary system of values and practices that have developed over time and are deeply embedded in the social context) and its complexities are better understood in the context of Te Reo Māori. This is reflected in the definition of mātauranga Māori given by Mead ([2012](#mea12), p. 9) _‘Māori knowledge complete with its values and attitudes.’_

According to Royal ([2009](#roya09)), the term mātauranga Māori as commonly used today is a relatively modern phenomenon. He believes this results from a transition on the part of Māori people from being totally immersed in Māori knowledge and not having externalised it as a concept, to having a different view whereby having been exposed to and immersed in western knowledge, the all-encompassing concept of mātauranga Māori now sits in contrast with this other knowledge ([Royal, 2009](#roya09)). Royal describes the word mātauranga as being just one aspect of Māori concepts of knowledge on a spectrum between internalised and externalised knowledge, with mātauranga being on the externalised end of the spectrum, and connected to books and schooling.

One of the key aspects highlighted by Mead ([2012](#mea12), p. 11;see also [Edwards, 2012](#edw12), p. 46) is that mātauranga Māori is not a relic of the pre-colonial era (as some might argue), but is instead an evolving body of knowledge, constantly being added to by new generations of Māori. Procter and Black ([2014](#pro14), p. 90) echo this, stating that _‘mātauranga Māori bridges both traditional and contemporary Māori knowledge and philosophies’_. However, Mead also agrees with Royal ([2009](#roy09)) that Māori are now no longer immersed in their cultural knowledge in day-to-day life: _‘In today’s society there is no longer a close dynamic relationship between the knowledge system and the daily lives of the people’_ ([Mead, 2012](#mea12), p. 12).

Royal ([2009](#roy09)) states that Te Reo Māori (Māori language) is fundamentally important to the rejuvenation of mātauranga Māori. Experts in linguistics agree that a language is more than just words, it is a conveyor of culture (e.g. [Royal-Tangaere, 1997](#roy97); [Zuckermann, 2014](#zuc14)).It represents a world view and shows what is important to a particular group of people. This is conveyed clearly in the following quote from the Waitangi Tribunal on claim WAI11 relating to Māori language:

> The language is the embodiment of the particular spiritual and mental concepts of the Māori, more closely related to oriental tradition than to our western ways. It offers a particular world view which, while not challenging our social structure, highlights alternatives for development. Its emphasis on holistic thinking, group development, family relationships and the spiritual dimension of life is not inappropriate in a nuclear age. Without the language this new dimension of life from which New Zealand as a whole may profit would be lost to us. ([Waitangi Tribunal 1986](#wai86), p. 17)

One thing that comes through clearly in much of the literature on mātauranga Māori is that the term refers to Māori knowledge in a generic sense. This is problematic because the pan-Māori approach has its roots in colonisation rather than being the way that Māori see themselves ([Smith, 1996](#smi96)). Iwi, hapū, and whānau (tribes, subtribes and family groups) also have their own specific knowledge, sometimes referred to as mātauranga ā-iwi or tribal knowledge (e.g. [Doherty, 2012](#doh12), [2014](#doh14)).

</section>

<section>

## Indigenous knowledge and libraries

The collection, preservation and dissemination of information and knowledge is the core business of libraries. It follows, therefore, that librarians have a duty to work with indigenous knowledge in a way that is culturally appropriate. While all public organisations can promote culturally appropriate engagement with indigenous peoples, such as by providing culturally-aware customer service, there are several specific issues that arise in the interface between indigenous knowledge and libraries. In recognition of this, several professional groups in the library sector have compiled lists of protocols for engaging with indigenous communities and their knowledge. The International Federation of Library Associations and Institutions (IFLA) Indigenous Matters Section ([n.d. a](#intnda)) listed these:

*   Aboriginal and Torres Strait Islander Library and Information Resource Network ([2010](#abo10)) The Aboriginal and Torres Strait Islander Protocols for Libraries, Archives and Information Services.
*   American Library Association ([2009](#ame11)) Principles, Librarianship and Traditional Cultural Expressions: Nurturing Understanding and Respect (draft)
*   Assembly of Alaska Native Educators ([2000](#ass00)) Guidelines for respecting cultural knowledge.
*   United Nations Commission on Human Rights, Sub-Commission of Prevention of Discrimination and Protection of Minorities, Working Group on Indigenous Populations ([1993](#uni93)) Mataatua Declaration on Cultural and Intellectual Property Rights of Indigenous Peoples.
*   National and State Libraries Australasia ([2014](#nat14)) National Policy Framework for Aboriginal and Torres Strait Islander Library Services and Collections
*   First Archivist Circle ([2007](#fir07)) Protocols for Native American Archival Materials.

(The IFLA page is no longer active as of November 2017.)

The American Library Association publication has been superseded by the American Library Association Presidential Traditional Cultural Expressions Task Force’s Presidential Traditional Cultural Expressions Task Force Report: Final Report ([2011](#ame11)), and the National and State Libraries Australasia policy framework was superseded in 2014 by the National Position Statement for Aboriginal and Torres Strait Islander Library Services and Collections.

These documents highlight a number of key considerations for libraries and librarians in relation to indigenous people and materials. Key themes identified through their analysis are stakeholder involvement, awareness of materials which portray indigenous people in inaccurate and/or stereotypical ways, cataloguing and classification of indigenous materials, access and restriction of access, professional learning and development of library staff, recruitment of indigenous library staff, repatriation and digital repatriation and other digitisation projects, creating a welcoming environment, ownership, copyright and intellectual property. The wider research project is investigating how engaging with these and other issues at the interface between indigenous knowledge and libraries helps non-Māori librarians to make sense of mātauranga Māori. The next section considers selected issues in more depth.

</section>

<section>

## Issues at the interface of indigenous knowledge and libraries

### Cataloguing and classification

The deficiencies of western classification systems such as the Library of Congress subject headings in classifying indigenous concepts are widely documented (e.g. [Duarte and Belarde-Lewis, 2015](#dua15); [Gilman, 2006](#gil06)). In their discussion of mainstream classification in relation to indigenous peoples in North America, Webster and Doyle ([2008](#web08)) summarise the issues thus:

> Classification systems carry systemic biases; they reflect the values and perspectives of their makers. In the case of Native American/Aboriginal topics, the [library and information studies] literature cites the following issues as problematic in mainstream library cataloguing practices: marginalization; historicization; omission; lack of specificity; failure to organize materials in effective ways; lack of relevance; and lack of recognition of the sovereignty of American Indian nations. (p. 191)

Webster and Doyle ([2008](#web08)) also note that this issue affects reference services too, since it influences the accuracy and efficiency of information retrieval by librarians and/or end-users.

In 2015 the journal _Cataloging and Classification Quarterly_ published a special issue focusing on the organisation of indigenous knowledge ([Metoyer and Doyle, 2015](#met15)), highlighting growing interest in this topic. One development of particular note has been the creation and application of specialised indigenous classification schemes. _Ngā Ūpoko Tukutuku Māori Subject Headings_ (e.g. [Paranihi, 2013](#par13)) is one of a number of examples of this.

</section>

<section>

### Misrepresentation of indigenous peoples and cultures in literature

Libraries frequently contain materials about indigenous people or cultures that have been written by non-indigenous authors. Authors often bring their own cultures and worldviews to bear on their observations or depictions of other cultures, and this has resulted in the existence of many texts that are inaccurate and often offensive to those from the culture being described. A good example of this is early anthropological texts, such as the works of Elsdon Best (e.g. [1923](#bes23)), but there are also cases when indigenous peoples and cultures are portrayed in fiction in stereotypical ways, even in new books (there are several reviews of such texts on Debbie Reese’s [_American Indians in Children’s Literature_](http://americanindiansinchildrensliterature.blogspot.com) blog for example). Librarians have a role to play in highlighting the problems in such texts.

</section>

<section>

### Repatriation and digital repatriation

There are numerous examples of documents and objects containing indigenous knowledge being acquired by western cultural institutions such as libraries, in some cases without the originating community’s knowledge or consent. The Mataatua Declaration on Cultural and Intellectual Property Rights of Indigenous Peoples ([Working Group on Indigenous Populations of the United Nations Commission on Human Rights, 1993](#uni93)) suggests that all cultural institutions holding indigenous materials should offer to return them to their historical owners. In practice, however, some materials cannot be traced back to a particular community, or libraries may not be able to return them for other reasons. One approach to this issue is digital repatriation, where a digital copy of the material is returned to the original owners, whilst the original remains in the library. Digital repatriation is not a single uniform technique, and each project is different depending on the needs of the community and the types of digital objects ([Bell, Christen and Turin, 2013](#bel13)).

</section>

<section>

### Access and restriction of access

While freedom of access to information is held up as a virtue in western librarianship, this fails to take into account the knowledge storage and transmission systems of indigenous cultures in which some information may be restricted to a certain group, for example a particular tribe or holders of a certain position within a community. Cullen ([1996](#cul96)) points out that the ideal of universal open access is a relatively new one in western librarianship, but that now it has been embraced, it is expected that the knowledge of non-western cultures should also be universally available. Since material that was originally intended to be restricted may have already been made available to the public, the role of librarians may be to consult with the original owners of the knowledge about how to proceed, particularly with the increasing focus on cultural and intellectual property rights.

</section>

<section>

### Indigenous languages

There are numerous references to the importance of language revitalisation in the preservation and generation of indigenous knowledge (e.g. [Royal, 2009](#roy09)) and libraries have a significant role to play in this process. The following is a statement from the International Indigenous Librarians Forum 2001:

> The indigenous librarians of this forum recognise the importance of language in relation to cultural identity and will inspire progress within our professions, whilst advocating for self-determination and control of indigenous knowledge. (International Indigenous Librarians Forum, 2001, as quoted in [International Indigenous Librarians Forum, 2009](#int09))

There is a lack of explicit reference to language in the protocols listed above. This may be due to the fact that language is considered a fundamental part of indigenous knowledge (e.g. [Royal, 2009](#roy09)). There are many ways in which libraries can play a role in the revitalisation of indigenous languages. In a review of forty case studies of libraries working with indigenous populations, Roy ([2013](#roy13)) highlights eight ways in which libraries support indigenous language revitalisation:

*   Giving the library a name in the local indigenous language.
*   Incorporating indigenous worldviews in strategic planning documents, expressed in terms from the indigenous language.
*   Building collections in indigenous languages.
*   Bilingual signage.
*   Providing facilities to enable communities to create their own language resources.
*   Observation of cultural protocols.
*   Running programmes to raise awareness of indigenous languages and what the library is doing to support them.
*   Acknowledging the importance of indigenous languages and providing support for language education.

Another aspect, which Roy did not mention in her case studies, is the role of libraries with collections of unpublished material. Libraries which hold such documentary material can also play a role when their holdings include materials in indigenous languages, such as correspondence and word lists, which can be very useful for communities in efforts at language revitalisation (e.g. [Rice, 2013](#ric13)). Having some knowledge of local indigenous language(s) creates numerous opportunities to engage with indigenous communities and their knowledge.

The breadth and depth of these issues demonstrates that engagement with mātauranga Māori or other types of indigenous knowledge is not a straightforward process for librarians. As well as the issues discussed above, there are a number of key external drivers encouraging librarians to engage with indigenous knowledge. The next section highlights these drivers first internationally and then in the local context of Aotearoa New Zealand.

</section>

<section>

## International imperatives for librarians to engage with indigenous knowledge

The profession of librarianship centres on information and knowledge, and so there is considerable scope for interaction and engagement with indigenous peoples and their knowledge. The issue of library services for indigenous populations is one that is being approached in various ways in postcolonial contexts beyond Aotearoa New Zealand. In 2008 a special interest group focusing on indigenous matters was set up in IFLA ([Roy, 2009](#roy09), p. 45). The special interest group is now a section, the purpose of which is to _‘support the provision of culturally responsive and effective services to indigenous communities throughout the world’_ ([International Federation of Library Associations and Institutions Indigenous Matters Section, n.d. b](#intndb)). In 2012, IFLA added the element Awareness of Indigenous Knowledge Paradigms to its list of core elements for library and information education programmes ([Smith, Hallam and Ghosh, 2012](#smi12)).

The United Nations Declaration on the Rights of Indigenous Peoples ([United Nations, 2007](#uni07)), which the New Zealand Government officially endorsed in 2010 ([Sharples, 2010](#sha10)), specifically references the rights of indigenous peoples in areas relevant to libraries such as the practice of culture including literature, the revitalisation of native languages and repatriation of sacred objects. Article 13(1) is particularly pertinent to the work of libraries:

> Indigenous peoples have the right to revitalize, use, develop and transmit to future generations their histories, languages, oral traditions, philosophies, writing systems and literatures, and to designate and retain their own names for communities, places and persons. ([United Nations, 2007](#uni07), p. 7)

Additionally there are specific mentions of indigenous people’s right to education (Article 14) and media (Article 16) in their languages, and libraries also have a key role to play in these areas.

The wider research will explore whether these international imperatives play a significant role in individual librarians’ engagement with mātauranga Māori. The role of national-level factors will also be considered.

</section>

<section>

## Local imperatives for librarians to engage with mātauranga Māori

</section>

<section>

### Te Tiriti o Waitangi-The Treaty of Waitangi

Te Tiriti o Waitangi-The Treaty of Waitangi is the founding document of modern New Zealand and is the basis of bicultural relations in Aotearoa New Zealand. It centres on the principles of active protection, tribal self-regulation, redress and consultation ([Hayward, 2007](#hay07)). Libraries have a role to play particularly in the active protection of Māori taonga (anything considered to be of value including socially or culturally valuable objects, resources, phenomenon, ideas and techniques) such as written materials and Te Reo Māori and the knowledge contained within these.

</section>

<section>

### LIANZA and its professional registration scheme

The library and information profession in Aotearoa New Zealand is one of the professions which has demonstrated a commitment to engaging with mātauranga Māori from a relatively early stage in comparison with many other professions in the country. The first mention of library services for Māori in the librarianship literature is in 1962 when the _Māori library services committee_ was formed to recommend strategies to libraries to help them engage with Māori ([Lilley, 2013](#lil13)).

In 1990 the Library and Information Association of New Zealand Aotearoa (LIANZA) first expressed an ongoing commitment to biculturalism and the provision of equitable services to Māori:

> The Association recognises the Treaty of Waitangi, its fundamental role in the definition of Aotearoa, and will work for its fulfilment in all aspects of the Association’s concerns…The Treaty of Waitangi places the Māori in a different constitutional position from that of any other ethnic group in Aotearoa. Librarians therefore have a special responsibility to ensure that the needs and aspirations of the Māori people are recognised in their activities. (New Zealand Library Association Futures Group, 1990, p. 7, as quoted in [Szekely and Barnett, 2007](#sze07), p. 20).

Professional registration was introduced by LIANZA in 2007 ([Millen, 2010](#mil10)). This was established to act as a benchmark for professional learning and development both within the library and information profession in Aotearoa New Zealand and also to be compatible with other Anglophone countries with similar schemes such at the United Kingdom and Australia ([Library and Information Association of New Zealand Aotearoa Taskforce on Professional Registration, 2005](#lib05)).

To become registered, candidates must demonstrate professional learning and development across four professional domains (Knowledge, practice, communication and leadership) and eleven bodies of knowledge. Body of knowledge eleven is _‘Awareness of indigenous knowledge paradigms, which in the New Zealand context refers to Māori’_ ([Library and Information Association of New Zealand Aotearoa, 2013](#lib13), p. 9). The content of body of knowledge eleven was compiled by Hinureina Mangan and Marie Waaka of Te Wānanga o Raukawa, a Māori tertiary institution ([Reweti, 2013](#rew13)). The other ten bodies of knowledge are based on IFLA’s _Guidelines for Professional Library and Information Management Education Programmes_ ([Library and Information Association of New Zealand Aotearoa Taskforce on Professional Registration, 2005](#lib05)).

Professional registration includes mandatory revalidation. Every three years, registrants must submit a reflective journal detailing their professional learning and development which must cover at least three of the four domains and all bodies of knowledge, including body of knowledge eleven. If candidates do not revalidate their registration, it lapses ([Library and Information Association of New Zealand Aotearoa, 2016](#lib16)). The inclusion of body of knowledge eleven demonstrates the commitment of the largest professional association for library and information professionals in Aotearoa New Zealand to the principles of biculturalism. The extent to which individual librarians are similarly committed to learning about indigenous knowledge paradigms and applying that knowledge in the workplace is a focus of this research.

</section>

<section>

### Te Rōpū Whakahau’s Mātauranga Māori in New Zealand Libraries course

Te Rōpū Whakahau was established in 1992, first as a special interest group of LIANZA and then, from 1996, as an independent association for Māori in libraries and information management ([Lilley, 2013](#lil13)).

In 2010 Te Rōpū Whakahau began offering the _Mātauranga Māori in New Zealand Libraries_ course ([Lilley, 2013](#lil13)). The one-day course takes place in a wharenui (Māori meeting house) and begins with a pōwhiri (welcome ceremony). The course addresses key issues for librarians in relation to mātauranga Māori such as the structure and diversity of Māori knowledge frameworks, Māori research methodologies as a lens for understanding the role of libraries in a Māori context, and thinking about practical ways to apply learning about mātauranga Māori to the participant’s own context ([Te Rōpū Whakahau, 2009](#ter09)).

This course is one of the few structured learning opportunities available to library and information professionals in Aotearoa New Zealand in the area of indigenous knowledge and libraries. The wider research will investigate how this and other learning opportunities help non-Māori librarians to make sense of mātauranga Māori in their professional lives.

</section>

<section>

## Issues regarding mātauranga Māori persist in libraries

While there is tangible commitment to biculturalism and mātauranga Māori from the library and information profession in Aotearoa New Zealand as represented by LIANZA and other professional groups, there are still a number of issues of concern related to library professionals’ engagement with these topics. Not every professional librarian is a member of LIANZA, and not every LIANZA member is required to subscribe to its commitment to biculturalism or to take part in its professional registration scheme. This means that while one set of literature may paint a mainly positive picture in a theoretical sense, another highlights the ongoing issues on the ground. Recent research highlights the fact that while steps are being made in the right direction, the profession in Aotearoa New Zealand still has the potential to make a lot more progress in the area of engagement with mātauranga Māori.

Tuhou ([2011](#tuh11)), for example, identified a number of barriers which prevented Māori tertiary students from engaging with the university library. Many of these were physical, with one group likening the atmosphere of the library to a prison, but staff were also a factor. Tuhou recommends cultural awareness training for staff to help them engage appropriately with Māori students, and for all staff to have the skills and confidence to answer reference questions asked by Māori students. The important role of language in indigenous culture is noted above but Evans ([2011](#eva11)) found that although there are increasing levels of bilingual signage in public libraries, English is privileged over Māori in terms of placement and emphasis. She observed that Māori collections were labelled with signage only in English surprisingly often. In another study of public libraries, Hayes ([2012](#hay12)) investigated the incorporation of kaupapa Māori (which Hayes interprets as _‘Māori knowledge frameworks and value systems and the incorporation of a Māori worldview’_ p. 6) in public libraries in Aotearoa New Zealand. He found that incorporation of Māori concepts within bicultural strategy and practice was variable across the libraries he investigated, the community’s demographic profile and location playing a large part in the variability. Significantly, none of his interviewees felt that their libraries were operating in a genuinely bicultural way.

The importance of the potential impact of librarians engaging with this issue is highlighted by Irwin and Katene ([1989](#irw89)), in a study highlighting the dearth of Māori tribal-specific information in libraries and the difficulties experienced by Māori trying to find that information. Irwin and Katene ([1989](#irw89)) emphasise the role to be played by libraries in partnering with Māori to alleviate some of the social disadvantages they face. They argue that knowledge is power and, therefore, access to knowledge is potential power. Social statistics at the time indicated that Māori were disempowered, and the authors argued that one possible reason for this is denial of access to knowledge. _‘Librarians are in a position of power where they can provide open access to knowledge, or they can deny this’_ ([Irwin and Katene, 1989](#irw89), pp. 23-4).While there has been some improvement in the intervening years, statistics show that Māori still experience greater levels of social disadvantage than New Zealanders of European descent (e.g. lower levels of educational achievement, [Statistics New Zealand 2015a](#sta15a); higher levels of incarceration, [New Zealand Department of Corrections, 2016](#new16); lower life expectancy, [Statistics New Zealand, 2015b](#sta15b)). Irwin and Katene suggest that libraries have an important role to play in enabling Māori to improve their conditions through access to their own cultural knowledge. This, alongside the many ways that librarians may encounter indigenous knowledge, and the national and international interest in the topic, highlights the issue of non-indigenous librarians’ engagement with indigenous knowledge as an important one for research.

</section>

<section>

## Non-Māori librarians’ response to these issues

The evidence above presents a compelling picture of the pressing issues around indigenous knowledge and librarianship and the need for non-indigenous librarians to engage with indigenous knowledge in order to address these. Despite this, there has been surprisingly little research on how non-indigenous librarians attempt to engage with indigenous knowledge in their work.

The first stage of the wider research project will consist of interviews with non-Māori librarians to explore their journeys of engaging with mātauranga Māori. These interviews will be based on the micro-moment timeline interview format, part of the sense-making methodology (e.g. [Dervin, 2003](#der03)), asking participants to share their stories of learning about and engaging with mātauranga Māori as a timeline, and then discussing specific incidents in more detail. From this we will build an understanding of motivations, contextual influences and challenges that participants have encountered.

The analysis of the interview data will also be used to inspire questions for focus groups of Māori librarians, to gauge their perceptions of their non-Māori peers’ processes of engagement with mātauranga Māori. It is hoped that this study will lead to more research in this important area and also produce tangible outcomes for the profession in Aotearoa New Zealand.

</section>

<section>

## Conclusion

This paper has presented an argument for the need for research in the area of librarians’ understanding of and engagement with indigenous knowledge, specifically mātauranga Māori in the context of Aotearoa New Zealand. There are many ways that librarians may need to know how to work well with indigenous knowledge, for example in relation to cataloguing and classification. Developments local to Aotearoa, such as the introduction of LIANZA’s professional registration scheme, and internationally, such as the activities of IFLAs Indigenous Matters section, also highlight issues around indigenous knowledge as important for non-indigenous librarians to engage with, and a key area for research. The next stages of this project, interviews and focus groups, will begin to address this, and encourage further research into this important topic, leading to recommendations on how to help non-Māori librarians engage more effectively with indigenous knowledge in their professional lives.

</section>

<section>

## <a id="author"></a>About the authors

**Kathryn Oxborrow** is a PhD student in the School of Information Management at Victoria University of Wellington, New Zealand, where she is researching how non-indigenous librarians make sense of indigenous knowledge in their lives and work. Kathryn has been working in and around libraries for over ten years and is currently a Reference Librarian at the University of Otago, Wellington. Kathryn is originally from the UK and has lived in Aotearoa New Zealand since 2010\. She can be contacted at [Kathryn.Oxborrow@vuw.ac.nz](mailto:kathryn.oxborrow@vuw.ac.nz)  
**Dr Anne Goulding** is Professor of Library and Information Management at the School of Information Management, Victoria University of Wellington, New Zealand. Anne’s teaching and research interests lie primarily in the area of the management of library and information services and her main focus is on the management of public libraries. She can be contacted at [Anne.Goulding@vuw.ac.nz](mailto:anne.goulding@vuw.ac.nz)  
**Dr Spencer Lilley** is a Senior Lecturer in Te Pūtahi a Toi, the School of Māori Art, Knowledge and Education at Massey University in New Zealand. His tribal affiliations are to Te Atiawa, Muaūpoko and Ngāpuhi. His research interests focus on indigenous information behaviour, Māori information literacy issues and professional and cultural development issues for Māori library and information management staff. Dr Lilley is an Honorary Life Member of Te Rōpū Whakahau (Māori in libraries and information management) and is a Fellow and former President of the Library and Information Association of New Zealand Aotearoa. He can be contacted at [S.C.Lilley@massey.ac.nz](mailto:s.c.lilley@massey.ac.nz)

</section>

<section>

## References

<ul>
<li id="abo10"> Aboriginal and Torres Strait Islander Information and Resource Network (2010). <em><a href="http://www.webcitation.org/moA3NQco">The Aboriginal and Torres Strait Islander protocols for libraries, archives and information services.</a></em> Retrieved from http://atsilirn.aiatsis.gov.au/protocols.php. (Archived by WebCite® at http://www.webcitation.org/moA3NQco)</li>
<li id="aland"> Alaska Native Science Commission (n.d.). <em><a href="http://www.webcitation.org/mo9FUHsZ">What is traditional knowledge?</a></em> Retrieved from http://www.nativescience.org/issues/tk.htm. (Archived by WebCite® at http://www.webcitation.org/mo9FUHsZ)</li>		
<li id="ame11"> American Library Association Presidential Traditional Cultural Expressions Task Force (2011). <em>Presidential Traditional Cultural Expressions Task Force report: final report.</em> Washington, DC: American Library Association.</li>				
<li id="ass00"> Assembly of Alaska Native Educators (2000). <em><a href="http://www.webcitation.org/moBkPPUd">Guidelines for respecting cultural knowledge.</a></em> Alaska Native Knowledge Network.
Retrieved from http://ankn.uaf.edu/Publications/Knowledge.html. (Archived by WebCite® at http://www.webcitation.org/moBkPPUd)</li>
<li id="bel13"> Bell, J.A., Christen, K., &amp; Turin, M. (2013). Introduction: after the return. <em>Museum Anthropology Review, 7</em>(1-2), 1-21. </li>
<li id="bes23"> Best, E. (1923). The Maori school of learning: its objects, methods, and ceremonial. Wellington, New Zealand: A. R. Shearer, Government printer. </li>
<li id="car13"> Carroll, M., Kerr, P., Musa, A.I., &amp; Afzal, W. (2013). Commonwealth of uncertainty: how British and American professional models of library practice have shaped LIS Education in selected former British colonies and dominions. <em>IFLA Journal, 39</em>(2), 121-133. </li>
<li id="cul96"> Cullen, R. (1996). The impact of a national bicultural policy on librarianship in New Zealand: a decade of change, challenges and opportunities. <em>Asian Libraries, 5</em>(2), 12-20. </li>
<li id="der03"> Dervin, B. (2003). Sense-making’s journey from metatheory to methodology to method: an example using information seeking and use as a research focus. In B. Dervin, L. Foreman-Wernet &amp; E. Lauterbach (Eds.). <em>Sense-making methodology reader: selected writings of Brenda Dervin </em>(pp.251-268). Cresskill, NJ: Hampton Press. </li>
<li id="doh12"> Doherty, W. (2012). <a href="http://www.webcitation.org/6mo8C5gam">Ranga Framework: He Raranga Kaupapa.</a> In Haemata Ltd, T. Black, D. Bean, W. Collings, and W. Nuku (Eds.) <em>Conversations on mātauranga Māori</em> (pp. 15-36).Wellington, New Zealand: New Zealand Qualifications Authority. Retrieved from http://www.nzqa.govt.nz/maori/te-rautaki-maori/conversations-on-matauranga-maor/ (Archived by WebCite® at http://www.webcitation.org/6mo8C5gam)	</li>			
<li id="doh14"> Doherty, W. (2014). <a href="http://www.webcitation.org/mo9OGgq2">Mātauranga ā-Iwi as it applies to Tūhoe: Te Mātauranga o Tūhoe.</a> In H. Murphy, C. Buchanan, W. Nuku, &amp; B. Ngaia (Eds.) <em> Enhancing Mātauranga Māori and Global Indigenous Knowledge</em> (pp. 29-46).
Retrieved from http://www.nzqa.govt.nz/maori/te-rautaki-maori/enhancing-matauranga-maori-and-global-indigenous-knowledge/
(Archived by WebCite® at http://www.webcitation.org/mo9OGgq2)	</li>			 
<li id="dua15"> Duarte, M.E. &amp; Belarde-Lewis, M. (2015). Imagining: creating spaces for indigenous ontologies. <em>Cataloging and Classification Quarterly, 53</em>(5-6), 677-702. </li> 
<li id="edw12"> Edwards, S. (2012). <a href="http://www.webcitation.org/mo8C5gam">Nā te Mātauranga Māori, Ka Ora Tonu te Ao Māori.</a> In Haemata Ltd, T. Black, D. Bean, W. Collings, and W. Nuku(Eds.) <em>Conversations on mātauranga Māori </em>(pp. 37-58). 
Retrieved from http://www.nzqa.govt.nz/maori/te-rautaki-maori/conversations-on-matauranga-maori/ (Archived by WebCite® at http://www.webcitation.org/mo8C5gam)	</li>								
<li id="eva11"> Evans, E.R. (2011). <em>An investigation into the extent and application of bilingual signage in New Zealand public libraries.</em> Unpublished master’s research project, Victoria University of Wellington, Wellington, New Zealand.</li> 	
<li id="fin89"> Finks, L.W. (1989). What do we stand for? Values without shame. <em>American Libraries, 20</em>(4), 352–356. </li> 
<li id="fir07"> First Archivist Circle (2007). <em><a href="http://www.webcitation.org/moAetbnF">Protocols for Native American archival materials.</a></em> Retrieved from http://www2.nau.edu/libnap-p/protocols.html (Archived by WebCite® at http://www.webcitation.org/moAetbnF)	</li>								
<li id="gil06"> Gilman, I. (2006). <a href="http://www.webcitation.org/GuhQTYO">From marginalization to accessibility: classification of indigenous materials.</a> <em>Faculty Scholarship (Pacific University Library),</em> paper 6. Retrieved from http://commons.pacificu.edu/libfac/6 (Archived by WebCite® at http://www.webcitation.org/GuhQTYO)	</li>				
<li id="hay12"> Hayes, L. (2012). <em>Kaupapa Māori in New Zealand public libraries.</em> Unpublished master’s research project, Victoria University of Wellington, Wellington, New Zealand.</li> 
<li id="hay07"> Hayward, J. (2007). Appendix: The principles of the Treaty of Waitangi. In Waitangi Tribunal (2007) <em>National Overview volume ii </em>(pp. 475-494). Wellington, New Zealand: GP Publications. </li>					
<li id="intnda"> International Federation of Library Associations and Institutions Indigenous Matters Section (n.d. a). <em><a href="https://web.archive.org/web/20160410102521/http://www.ifla.org/indigenous-matters/protocols">Key protocols.</a></em> Retrieved from https://web.archive.org/web/20160410102521/http://www.ifla.org/indigenous-matters/protocols (Archived by WebCite® at http://www.webcitation.org/mo7nbbQ4)	</li>				
<li id="intndb"> International Federation of Library Associations and Institutions Indigenous Matters Section (n.d. b). <em><a href="http://www.webcitation.org/6nH3TW74W">About the Indigenous Matters Section.</a></em> Retrieved from http://www.ifla.org/about-indigenous-matters (Archived by WebCite® at http://www.webcitation.org/6nH3TW74W)	</li>	
<li id="int08"> International Federation of Library Associations and Institutions/United Nations Educational Scientific and Cultural Organization (2008). <em><a href="http://www.webcitation.org/mrky0DTi">IFLA/UNESCO Multicultural Library Manifesto.</a></em> Retrieved from http://www.ifla.org/node/8976 (Archived by WebCite® at http://www.webcitation.org/mrky0DTi)	</li>								
<li id="int09">International Indigenous Librarians Forum(2009).<em><a href="http://www.webcitation.org/6nTPfZn0D">International Indigenous Librarians' Forum 1999 –2009 background &amp; outcomes from past fora.</a></em> Retrieved from https://archive.trw.org.nz/iilf2009_outcomes.php. (Archived by WebCite® at http://www.webcitation.org/6nTPfZn0D)	</li>			
<li id="irw89"> Irwin, K. &amp; Katene, W. (1989). <em>Maori people and the library: a bibliography of Ngati Kahungunu and Te Waka o Takitimu resources.</em> Wellington, New Zealand: He Parekereke, Department of Education, Victoria University of Wellington. </li> 
<li id="lib13"> Library and Information Association of New Zealand Aotearoa (2013). LIANZA Profession Registration Board Professional Practice Domains and Bodies of Knowledge December 2013. Wellington, New Zealand: Library and Information Wellington, New Zealand: Library and Information Association of New Zealand Aotearoa.</li>
<li id="lib14">Library and Information Association of New Zealand Aotearoa (2014).<em><a href="http://www.webcitation.org/6mrjFTzKL">Rules of the New Zealand Library Association (incorporated).</a></em> Wellington, New Zealand: Library and Information Association of New Zealand Aotearoa. Retrieved from http://www.lianza.org.nz/about-us/governing-documents/rules-lianza (Archived by WebCite® at http://www.webcitation.org/6mrjFTzKL)	</li>
<li id="lib16"> Library and Information Association of New Zealand Aotearoa (2016). <em><a href="http://www.webcitation.org/6nFUXiSR0">Code of practice.</a></em> Retrieved from http://www.lianza.org.nz/about-us/governing-documents/code-practice (Archived by WebCite® at http://www.webcitation.org/6nFUXiSR0)	</li>			
<li id="lib05">Library and Information Association of New Zealand Aotearoa Taskforce on Professional Registration (2005). <em>Professional future for the New Zealand Library and Information profession: Discussion Document.</em> Wellington, New Zealand: Library and Information Association of New Zealand Aotearoa. </li>					
<li id="lil13"> Lilley, S. (2013). <em>Te Rōpū Whakahau: waiho i te toipoto, kaua i te toiroa, celebrating 20 years 1992-2012. </em>Palmerston North, New Zealand: Te Rōpū Whakahau. </li> 	
<li id="mea12"> Mead, H.M. (2012). <a href="http://www.webcitation.org/6mo8C5gam">Understanding Mātauranga Māori.</a> In Haemata Ltd, T. Black, D. Bean, W. Collings, and W. Nuku(Eds.) <em>Conversations on mātauranga Māori</em> (pp. 9-14).
Retrieved from http://www.nzqa.govt.nz/maori/te-rautaki-maori/conversations-on-matauranga-maori/ (Archived by WebCite® at http://www.webcitation.org/6mo8C5gam)	</li>			
<li id="met15"> Metoyer, C.A. &amp; Doyle, A.M. (Eds.). (2015). <em>Cataloging and Classification Quarterly, 53</em>(5-6). </li>					
<li id="mil10"> Millen, J. (2010). Te Rau Herenga, a century of library life in Aotearoa: the New Zealand Library Association &amp; LIANZA, 1910-2010. Wellington, New Zealand: Library and Information Association of New Zealand Aotearoa.	
</li><li id="min14"> Ministry of Culture and Heritage (2014). <em><a href="http://www.webcitation.org/6mo8MHZiL">The first public library.</a></em> Retrieved from http://www.nzhistory.net.nz/media/photo/the-first-public-library (Archived by WebCite® at http://www.webcitation.org/6mo8MHZiL)	</li>			
<li id="moo11"> Moorfield, J.C. (2011). <em>Te Aka Māori-English, English-Māori Dictionary and Index</em> (3rd ed.). Auckland, New Zealand: Pearson. </li>					
<li id="nat14"> National and State Libraries Australasia (2014). <em>National position statement for Aboriginal and Torres Strait Islander library services and collections.</em> Melbourne, Australia: National and State Libraries Australasia.</li> 	
<li id="new16"> New Zealand Department of Corrections (2016). <em><a href="http://www.webcitation.org/6mrk2Tmbn">Prison facts and statistics: September 2016.</a></em> Retrieved from http://www.corrections.govt.nz/resources/research_and_statistics/quarterly_prison_statistics/prison_stats_september_2016.html (Archived by WebCite® at http://www.webcitation.org/6mrk2Tmbn)	</li>			 
<li id="ora12"> Orange, C. (2012). <a href="http://www.webcitation.org/6tpAnsD8d">Treaty of Waitangi.</a> In <em>Te Ara: the Encyclopedia of New Zealand. </em> Retrieved from https://teara.govt.nz/en/treaty-of-waitangi (Archived by WebCite® at http://www.webcitation.org/6tpAnsD8d)	</li>			
<li id="par13"> Paranihi, J. (2013). <a href="http://www.webcitation.org/6mrhPuzd0">Ngā Ūpoko Tukutuku/Māori Subject Headings and Iwi-Hapū Names projects.</a> In L. Roy and A. Frydman (Eds.) <em>Library services to indigenous populations: case studies</em> (pp. 105-107). Retrieved from  http://www.ifla.org/publications/library-services-to-indigenous-populations-case-studies (Archived by WebCite® at http://www.webcitation.org/6mrhPuzd0)	</li>	
<li id="pro14"> Procter, J. &amp; Black, H. (2014). <a href="http://www.webcitation.org/6mo9OGgq2">Mātauranga ā-Iwi: He Haerenga Mōrearea.</a> In H. Murphy, C. Buchanan, W. Nuku, &amp; B. Ngaia (Eds.) <em>Enhancing Mātauranga Māori and Global Indigenous Knowledge</em> (pp. 87-100). Retrieved from http://www.nzqa.govt.nz/maori/te-rautaki-maori/enhancing-matauranga-maori-and-global-indigenous-knowledge/ (Archived by WebCite® at http://www.webcitation.org/6mo9OGgq2)	</li>		
<li id="rew13"> Reweti, A. (2013). <a href="http://www.webcitation.org/6mrhPuzd0">Library and Information Association of New Zealand Aotearoa (LIANZA).</a> In L. Roy and A. Frydman (Eds.) <em>Library services to indigenous populations: case studies </em> (pp. 96-98). Retrieved from http://www.ifla.org/publications/library-services-to-indigenous-populations-case-studies (Archived by WebCite® at http://www.webcitation.org/6mrhPuzd0)	</li>			
<li id="ric13"> Rice, D. (2013). <em><a href="http://www.webcitation.org/6mridwMqP">Treasure trove of Indigenous language documents unearthed at NSW State Library.</a></em> Sydney, Australia: Australian Broadcasting Corporation. Retrieved from http://www.abc.net.au/news/2013-08-26/early-indigenous-language-documents-unearthed-at-nsw-library/4912960 (Archived by WebCite® at http://www.webcitation.org/6mridwMqP)	</li>				
<li id="roy09"> Roy, L. (2009). <em>Indigenous matters in library and information science: an evolving ecology. Focus on International Library and Information Work, 40</em>(2), 44-48. </li> 
<li id="roy13"> Roy, L. (2013). The role of tribal libraries and archives in the preservation of indigenous cultural identity through supporting native language revitalization. <em>International Preservation News,</em> 61, 8–11. </li>					
<li id="roy05"> Royal, T.A.C. (2005). <a href="http://www.webcitation.org/6tpAdnee5">Māori.</a> In <em>Te Ara: the Encyclopedia of New Zealand.</em> Retrieved from https://teara.govt.nz/en/maor 	 (Archived by WebCite® at http://www.webcitation.org/6tpAdnee5)	</li>				
<li id="roya09"> Royal, T.A.C. (2009, September 9). <em><a href="http://www.webcitation.org/6mo9Turco">Te Kaimangā: towards a new vision of mātauranga Māori</a></em> [Video file].  Video posted to http://mediacentre.maramatanga.ac.nz/content/te-kaimānga-towards-new-vision-mātauranga-māori (Archived by WebCite® at http://www.webcitation.org/6mo9Turco</li>
<li id="roy97"> Royal-Tangaere, A. (1997). <em>Te puawaitanga o te reo Māori: ka hua te hāo te pōtiki i roto i te whānau: ko tēnei te tāhuhu o te kōhanga reo.</em> Wellington, New Zealand: Te Rūnanga o Aotearoa mō te Rangahau i te Mātauranga. </li>					
<li id="sha10"> Sharples, P. (2010). <em><a href="http://www.webcitation.org/6mrl4OhZd">Supporting UN Declaration restores NZ’s mana.</a></em> Retrieved from https://www.beehive.govt.nz/release/supporting-un-declaration-restores-nz039s-mana (Archived by WebCite® at http://www.webcitation.org/6mrl4OhZd)	</li>			
<li id="smi96"> Smith, L.T. (1996). <a href="http://www.webcitation.org/6nGuOYJmt">Kaupapa Māori research: some Kaupapa Māori principles.</a> In L. Pihama &amp; K. Southey (Eds.),<em> Kaupapa Rangahau: a reader.</em> Hamilton, New Zealand: University of Waikato. Retrieved from http://researchcommons.waikato.ac.nz/handle/10289/9531 (Archived by WebCite® at http://www.webcitation.org/6nGuOYJmt)	</li>			
<li id="smi12"> Smith, K., Hallam, G., &amp; Ghosh, S.B. (2012). <em><a href="http://www.webcitation.org/6swP3BlPR">Guidelines for professional library/information educational programs, 2012.</a></em> Retrieved from https://www.ifla.org/publications/guidelines-for-professional-libraryinformation-educational-programs-2012 (Archived by WebCite® at http://www.webcitation.org/6swP3BlPR)	</li>			
<li id="sta13"> Statistics New Zealand (2013). <em><a href="http://www.webcitation.org/6uAQooBVD">2013 Census ethnic group profiles: Māori.</a></em> Retrieved from http://stats.govt.nz/Census/2013-census/profile-and-summary-reports/ethnic-profiles.aspx?request_value=24705&amp;parent_id=24704&amp;tabname=# (Archived by WebCite® at http://www.webcitation.org/6uAQooBVD)	</li>		
<li id="sta15a"> Statistics New Zealand (2015a). <em><a href="http://www.webcitation.org/6mrjhvhK7">Educational attainment of adults aged 25 to 34 years.</a></em> Retrieved from http://www.stats.govt.nz/browse_for_stats/snapshots-of-nz/nz-social-indicators/Home/Education/ed-attainment-adults.aspx (Archived by WebCite® at http://www.webcitation.org/6mrjhvhK7)	</li>			
<li id="sta15b"> Statistics New Zealand (2015b). <em><a href="http://www.webcitation.org/6mrjkEG23">Life expectancy.</a></em> Retrieved from http://www.stats.govt.nz/browse_for_stats/snapshots-of-nz/nz-social-indicators/Home/Health/life-expectancy.aspx (Archived by WebCite® at http://www.webcitation.org/6mrjkEG23)	</li>					
<li id="sta17"> Statistics New Zealand (2017). <em><a href="http://www.webcitation.org/6tpAPqwUw">National population estimates at 30 June 2017.</a></em> Retrieved from http://m.stats.govt.nz/browse_for_stats/population/estimates_and_projections/NationalPopulationEstimates_HOTPAt30Jun17.aspx (Archived by WebCite® at http://www.webcitation.org/6tpAPqwUw)	</li>			
<li id="sze07"> Szekely, C. &amp; Barnett, J. (2007). The Treaty of Waitangi and Māori in New Zealand libraries. In A. Fields and R. Young (Eds.) <em> Informing New Zealand: libraries, archives and records: Hei puna whakamōhio mō Aotearoa: whare pukapuka, pūranga kōrero, whare taonga</em> (pp. 15-24). Lower Hutt, New Zealand: Open Polytechnic of New Zealand. </li> 
<li id="ter88"> Te Rōpū Takawaenga (1988). Submission to the Joint Advisory Committee on the proposed course prescription for the Diploma in Librarianship 1990. <em>Library Life, 116,</em> 3. </li>					
<li id="ter09"> Te Rōpū Whakahau (2009). <em><a href="http://www.webcitation.org/6mrkhz8pj">Mātauranga Māori within New Zealand libraries.</a></em> Retrieved from https://www.trw.org.nz/matauranga-maori/ (Archived by WebCite® at http://www.webcitation.org/6mrkhz8pj)	</li>			 	
<li id="tuh11"> Tuhou, T. (2011). <em>Barriers to Māori usage of university libraries: an exploratory study in Aotearoa New Zealand.</em> Unpublished master’s research project, Victoria University of Wellington, Wellington, New Zealand. </li> 				
<li id="uni93"> United Nations Commission on Human Rights, Sub-Commission of Prevention of Discrimination and Protection of Minorities, Working Group on Indigenous Populations (1993). <em><a href="http://www.webcitation.org/6mriS1YAZ">The Mataatua Declaration on cultural and intellectual property rights of indigenous peoples.</a></em> Whakatane, New Zealand: United Nations.  Retrieved from http://www.wipo.int/tk/en/databases/creative_heritage/indigenous/link0002.html
(Archived by WebCite® at http://www.webcitation.org/6mriS1YAZ)	</li>			
<li> United Nations (2007). <em><a href="http://www.webcitation.org/6moC5vaXV">Declaration on the Rights of Indigenous Peoples.</a></em> New York, NY: United Nations. Retrieved from http://www.ohchr.org/EN/Issues/IPeoples/Pages/Declaration.aspx (Archived by WebCite® at http://www.webcitation.org/6moC5vaXV)	</li>		
<li id="wai86"> Waitangi Tribunal (1986). <em><a href="http://www.webcitation.org/6mo9mQu58">Report of the Waitangi Tribunal on the Te Reo Maori claim.</a></em> Wellington, New Zealand: Government Printer. Retrieved from https://forms.justice.govt.nz/search/WT/reports/reportSummary.html?reportId=wt_DOC_68482156 (Archived by WebCite® at http://www.webcitation.org/6mo9mQu58)	</li>		
<li id="web08"> Webster, K., &amp; Doyle, A. (2008). Don’t class me in Antiquities! Giving voice to Native American Materials. In K.R. Roberto (Ed.)<em>, Radical cataloging: Essays at the front</em> (pp. 189-197). Jefferson, NC: McFarland. </li>					
<li id="wor11"> World Intellectual Property Organization (2011). <em><a href="http://www.webcitation.org/6nCMSsxX4">What is intellectual property?</a></em> Retrieved from http://www.wipo.int/about-ip/en/  (Archived by WebCite® at http://www.webcitation.org/6nCMSsxX4)	</li>			
<li id="zim15"> Zimmerman, K.A. (2015). <em><a href="http://www.webcitation.org/6nCPVdN3y">What is culture?: Definition of culture.</a></em> Retrieved from http://www.livescience.com/21478-what-is-culture-definition-of-culture.html
(Archived by WebCite® at http://www.webcitation.org/6nCPVdN3y)	</li>			
<li id="zuc14"> Zuckermann, G. (2014). <a href="http://www.webcitation.org/6mo9OGgq2">Historical and moral arguments for language reclamation.</a> In H. Murphy, C. Buchanan, W. Nuku, &amp; B. Ngaia (Eds.) <em> Enhancing mātauranga Māori and global indigenous knowledge</em> (pp. 182-195). Retrieved from http://www.nzqa.govt.nz/maori/te-rautaki-maori/enhancing-matauranga-maori-and-global-indigenous-knowledge/ (Archived by WebCite® at http://www.webcitation.org/6mo9OGgq2)</li>						
</ul>

</section>

</article>