<header>

#### vol. 22 no. 4, December, 2017

Proceedings of RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016.

</header>

<article>

# Editorial: selected papers from RAILS: Research Applications, Information and Library Studies 2016

#### [Brenda Chawner](#author), Conference Chair

<section>

This supplement contains a selection of papers presented at Hono Tangata: Rangahaua kia mārama: Bridging the gap: from research to practice in information studies, the 12th RAILS (Research Applications, Information and Library Studies) Conference.

RAILS 2016 was held at Victoria University of Wellington, New Zealand, from 6–8 December 2016, in association with the Australasian Information Educators’ Symposium (AIES). RAILS is the main Australasian conference for library and information studies researchers, and was first held in 2004 at Queensland University of Technology. Since then the conference has been held annually, hosted by a different Australian information studies programme each time.

For the first ten years, the conference took place on a single day, but from 2014 it was expanded to include AIES and a doctoral workshop. Because the conference site was hosted by a different institution each year, access to earlier papers is limited, and only papers subsequently published as journal articles are available. This is the first time selected papers presented at the conference have been published as a collection, marking a significant milestone in the conference’s evolution.

The 2016 conference was the first one held outside Australia. Its theme was intentionally broad, to encourage people to submit papers on a range of topics in information studies. The conference had an international scope, with presentations by people not only from Australia and New Zealand, but also from the United States, the United Kingdom, Canada, and Qatar.

The conference began with a keynote address by Associate Professor Nadia Caidi from the University of Toronto, _Diversity by design: the way forward_. Papers presented at the conference covered a range of topics, including the relevance of research to practice, differences in publishing patterns between practitioners and researchers, open access, indigenous knowledge, access to information, and archives and recordkeeping. The conference programme also included three panel discussions. The first asked, _What research topics should be explored in the Australasian library and information profession?_ and was led by Helen Partridge, Lisa Given and Damian Lodge. The next focused on information experience as a research object: _What to research? How to research? Where to apply?_, led by Elham Sayyad Abdi, Kate Davis and Helen Partridge. The final panel discussion considered the conference theme in relation to the changing role of academic librarians in connecting research with new audiences, and was led by Richard Hayman and Wade Kelly.

This supplement includes twenty papers presented at AIES and RAILS. Although the keynote speech is not included, Ray Lewis published [a detailed summary](http://www.infotoday.com/it/mar17/Lewis--RAILS-Conference-Embraces-Diversity.shtml) of Associate Professor Caidi’s talk, complemented by an interview, in _Information Today_.

I would like to thank everyone who submitted papers to the conference, members of the conference committee who reviewed papers, my co-conference organisers Gillian Oliver and Anne Goulding, and the _Information Research_ reviewers who provided feedback to improve the published versions of the papers. I should also acknowledge the conference delegates, who came to Wellington just three weeks after the magnitude 7.8 Kaikoura earthquake affected large parts of central New Zealand, and the Victoria University of Wellington staff who ensured that the conference venue was available when the conference started.

Finally, this supplement would not have happened without the support of Amanda Cossham, _Information Research_ Editor, Australasia/S.E. Asia. Amanda coordinated the second round of reviews and provided feedback to authors. She also worked closely with the volunteer copyeditors, whose behind-the-scenes contribution improved the quality of the papers. HTML conversion was done by Nikoletta Soós, who did this work carefully with good attention to detail.

## <a id="author"></a>About the author

Brenda Chawner is a senior lecturer in the School of Information Studies, Victoria University of Wellington, and Chair of the 2016 RAILS Conference. She can be contacted at [brenda.chawner@vuw.ac.nz](mailto:brenda.chawner@vuw.ac.nz)

</section>

</article>P