<header>

#### vol. 22 no. 4, December, 2017

</header>

Proceedings of RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016.

<article>

# Partnerships or parallel lines? The contributions of practitioners and academics to library and information research

## [Bruce White](#author) and [Amanda Cossham](#author)

> **Introduction.** Academic libraries are seeking to extend their roles beyond traditional document provision and knowledge discovery into such areas as strategic research support, research data management and scholarly publishing. Consequently, more academic librarians will need to develop a robust understanding of the scholarly research process as experienced by academics. This paper examines a sample of recent articles in established library and information studies (LIS) journals to identify the comparative contributions of librarians (practitioners) and academics and the extent of collaboration between these groups, based on jointly written articles.  
> **Method.** Journals were drawn from the Scimago Library and Information Sciences list and an algorithm was used to identify authors of individual papers as practising librarians or library and information studies (LIS) academics, or both (joint authorship). Keywords were extracted and aggregated by author category to identify the subject interests of these two groups.  
> **Analysis.** Keywords were manually clustered, and filtered using Excel, to determine the topics of most interest to the different groups of authors.  
> **Results.** Analysis of keywords highlights the different spheres of interest each group has, and questions the degree of alignment between the scholarly literature and the widely-recognised preoccupations of the field. The results indicate striking differences as well as broad areas of common interest.  
> **Conclusion.** Currently, the two groups (practitioners and academics) are running on parallel, or even diverging, lines in their respective approaches to the practice of research within their shared discipline.

<section>

## Introduction

Academic libraries are seeking to extend their roles beyond traditional document provision and knowledge discovery into such areas as strategic research support, research data management and scholarly publishing ([Auckland, 2012](#auc12)). If this transition is to succeed, librarians will need to develop a robust understanding of the scholarly research process as experienced by academics. This process includes developing research projects, applying for research grants, formulating research questions and robust research methods, applying for ethics approvals, conducting the research, analysis of quantitative and qualitative data, and publishing the results through appropriate scholarly channels ([Auckland, 2012](#auc12); [McBain, Culshaw and Walkley Hall, 2013](#mcb13); [Perkins and Slowik, 2013](#per13)). Other aspects of scholarly publishing that will benefit librarians include peer reviewing and serving on editorial advisory boards for journals and conferences, which will expose them to many different types of research and methodologies, as well as to issues of quality and validity. Some academic librarians, especially those who are on a tenure track in United States universities, may already be experienced in the scholarly research process. Others are not, despite having significant professional knowledge and experience. Direct experience of the scholarly research process provides a richer and more nuanced understanding of their own professional discipline and of the research professionals whom they seek to support.

In addition, a defining feature of sound professional practice is the use of research-derived evidence as the basis of decision-making (_cf._ [Abbott, 1998](#abb98); [Pemberton, 1998](#pem98)). It follows that, as advocates for the importance of information, librarians (as a profession) would have a strong sense of the importance of credentialed information and would consider it a particular duty to make use of the published literature of their discipline. Multiple channels for the publication of research are available to authors in this discipline whether they are practising librarians or library and information studies (LIS) academics (faculty) or researchers.

Questions exist, however, about the degree to which the professional practice of librarians is in fact evidence-based ([Murray, 2016](#mur16); see also [Booth, 2011](#boo11); [Genoni, Haddow and Ritchie, 2004](#gen04)), and, in turn, whether the published research of librarianship arises out of, and contributes back to, the practical and philosophical concerns of those working in the field. That is, how much research do practitioners (academic librarians in this case) actually conduct, and how well are they using the research that is published by academics (library and information studies faculty)? We do not claim that all research needs to be _practical_ or that _theoretical_ work has no place in an applied discipline such as this: what the practitioner often needs is not only advice on how to do things but also a broader consideration about why a particular course of action would result in an outcome better aligned with organisational and professional goals. Rather, we observe that all research begins with questions and those that originate from professional activity are more likely to result in real-world outcomes and impacts that will benefit the profession.

Academic librarians would benefit from the support of the library and information studies academic community to foster their individual development and ensure the quality of practitioner research. However, the forces motivating academics to participate in research are very different from those affecting librarians, who face a number of barriers to participation as well as a paucity of incentives ([Galbraith, Garrison and Hales, 2014](#gal14); [Sassen and Wahl, 2014](#sas14)). Furthermore, if a _culture gap_ exists between _pragmatic_ practitioners and _theoretical_ LIS researchers in terms of the reading and uptake of research ([Genoni _et al._, 2004](#gen04)) then this can be expected to manifest when it comes to research orientation and collaborations between members of the two groups.

The questions addressed in this paper are:

*   What are the comparative contributions of practitioners and academics to the published journal article literature?
*   What do practitioners and academics research and write about, and how similar are the research areas that each group focuses on?
*   To what extent do practitioners and academics collaborate, and what are the most likely topics that they address jointly?

The paper provides a brief review of the literature, explains the method and bibliometric analysis of the data set, and presents some findings before drawing conclusions about the current state of practitioner and academic collaboration. Although the discipline is library and information studies, and the journals include some archival and recordkeeping areas, the focus is predominantly on librarianship.

</section>

<section>

## Literature review

The literature reviewed as a background for this paper was drawn predominantly from the last fifteen years. The intention was to examine the current situation in librarianship (as a sub-category of library and information studies) rather than to provide a broad survey of changes over time. The bulk of the available literature is US-focused, and also academic-library focused. While this is perhaps not surprising, given the requirement for many US academic librarians to conduct research to achieve tenure and faculty status, less is known about what happens in other types of library, which is probably indicative of a lack of research by librarians more generally.

The gap between research and practice in library and information studies is widely acknowledged ([Booth, 2003](#boo03); [Chang, 2016](#cha16); [Haddow and Klobas, 2004](#had04); [Joint, 2005](#joi05)). According to Chang ([2016](#cha16)), in a survey of the research-practice divides, _‘few studies have focused on research collaborations among practitioners and researchers in LIS from the perspective of research–practice divides’_ (p. 535). Chang identifies four different types of studies in the library and information science literature: causes of the research practice divides, the means of easing such divides, how practitioners conduct research, and the characteristics of library and information studies articles. The current research fits into the last type.

Although the value of research to a profession is a core assumption, Powell, Baker and Mika ([2002](#pow02)) found that practitioners are less engaged with research than is ideal for the profession. Haddow and Klobas ([2004](#had04)) note that research should inform practice, while practice should both benefit from research and raise additional questions for research. They suggest that that for many disciplines, _‘practice does not benefit from research because communication between research and practice is flawed’_ (p. 30), and they identify eleven gaps between practice and research in library and information studies, along with ways to bridge these gaps.

Wiegand ([2016](#wie16)) bluntly suggests that research is shortchanging libraries, especially public libraries:

> Where is the LIS research community? Why aren’t members of that community conducting longitudinal studies evaluating library activities like the impact of summer reading programs on student reading levels as they move from one grade to another? Where’s the LIS research that identifies the community effects of programs like film festivals, book clubs, children’s story hours, English as a second language classes, literacy tutoring, art exhibits and musical presentations that thousands of public libraries have routinely been hosting for generations? Where are the LIS researchers to perform similar evaluation studies on the multiple community effects of library reading and library as place in all types of libraries across the country and over time that take into account demographic variables like race, age, gender, sexual orientation, class, etc.?

Various solutions have been offered for bridging the research-practice gap. Booth ([2003](#boo03)) considers the role of evidence-based librarianship, while Ponti ([2012](#pon12)) suggests that collaborative research projects involving library and information studies academics and practitioners, using a commons-based peer production form of work, would be effective. Chu ([2007](#chu07)) identified the areas that might be most of use to practitioners, but used a thematic approach that frames approaches to research, rather than a topical breakdown. Walkley Hall ([2015](#wal15)) and McBain _et al._ ([2013](#mcb13)) provide examples of how to foster a culture of research practice in an academic library where tenure and faculty status are not at issue, as is the case in Australia and New Zealand. These include formal management recognition and support for practitioner research and an increased emphasis on the value of professional reflection within an organisational culture traditionally focussed on pragmatic _doers_.

Another way of overcoming the gap between research and practice is for academics (researchers) and practitioners (librarians) to collaborate. Research on academic-practitioner collaboration in library and information studies typically focuses on teaching rather than on research practices, and on the development of information literacy skills for students within the framework of their courses (e.g., [Hrycaj and Russo, 2007](#hry07); [Pham, 2016](#pha16); [Pham and Tanner, 2014](#pha14); [Yousef, 2010](#you10)). These collaborations can take many different forms, and are not limited to library and information studies programmes, but apply to any discipline taught by an institution of higher education. Additionally, collaboration may be between practitioners and library and information studies academics; between practitioners and other disciplines’ academics, especially when the practitioners are subject specialists in their own right; and between practitioners and/or academics and other parts of the wider profession (for example, professional associations, commercial providers, consultants, independent researchers, and research groups).

Collaboration can be identified through co-authored articles, although this is not infallible. Chang ([2016](#cha16)) notes the problem of finding out exactly who authors are, which was also a consideration for the current research. We note too that journals have standard forms for expressing authors’ affiliations which may mean that an author’s parent body is the only one named. In these cases it is not possible to determine whether an author is an academic faculty member, a librarian, a student, or a different type of staff member. Thus, research may in fact be a collaboration between different groups, but this may not necessarily be obvious from the information available within the article.

The motivation of and barriers faced by practitioners impact significantly on collaboration for research ([Clapton, 2010](#cla10)), as does the value given to research itself ([Perkins and Slowik, 2013](#per13)). The different subcultures of faculty and library also play a part in engagement with research ([Walters, 2016](#walt16)), and these subcultures may also influence collaboration as well. The problem of the status of librarians relative to the status of academics still arises ([Pham, 2016](#pha16), sections 5.4.2 Professional divide [Australia] and 6.4.2 Professional divide [Vietnam]), in addition to the question of whether librarians have status as faculty members. The issue, as noted by Galbraith _et al._ ([2016](#gal16)) and Pham, is also around how the status of librarians might affect the relationship they have with academics, and therefore the potential for collaborative research and publication. Tenure and faculty status is often given as a reason why US academic librarians research and publish ([Galbraith, _et al._, 2016](#gal16)) but this may be less relevant in other parts of the world where tenure is not open to academic librarians.

Chang ([2016](#cha16)) concludes that in the six journals they studied and between 1995 and 2014, there was a trend towards co-authored articles by academics (or researchers) and practitioners, and a decrease in the divide between research and practice in library and information studies. However, collaborations of this sort are still much less common than sole authorship or authorship contained within each group (practitioners or academics). Hildreth and Aytac ([2007](#hil07)) note that _‘mixed authorship’_ occurred in less than 10% of the sample of articles they studied, which were drawn from twenty-three library and information studies journals weighted towards practitioner research. However, Finlay, Ni, Tsou and Sugimoto ([2013](#fin13)) found that fewer than 3% of articles in their study were collaborations between librarians and non-librarians, and moreover, articles authored by non-librarians or by non-librarians and librarians had higher citation rates than those solely by librarians. Chang ([2015](#cha15)) evaluated authorship of open access library and information science journals, and found that over half the authors worked in libraries. This is much higher than in other studies, such as Finlay _et al._ ([2013](#fin13)) and Walters and Wilder ([2015](#walt15)), and may be a significant feature of open access journals. Chang’s definition of collaboration included that between librarians, as well as between librarians and other library and information studies researchers.

Many authors write about the advantages of research for librarians, academic librarians and academic libraries more generally. For example, Clapton ([2010](#cla10)) identifies motivations (such as improving the practice in an organisation, personal interest, raising one’s profile, and career progression) and barriers (such as lack of time, no requirement to publish, and lack of skills or knowledge) for practitioners. Perkins and Slowik ([2013](#per13)) identify many values and benefits and their findings support other research. Benefits may include fulfilling tenure-track requirements, better relationships with faculty, improved services, the application of research to daily practices, professional development of librarians, and an improved knowledge of research more widely (p. 153). Advantages also accrue to the profession as a whole.

But there are also disadvantages, including the large commitment of time and effort, time taken away from helping patrons and other library duties, and lack of tenure track positions ([Perkins and Slowik, 2013, p. 145](#per13)). Tenopir ([2015, p. 4](#ten15)) identifies five reasons why librarians should do their own research, and while bringing in grant money and building towards promotion and/or tenure (reasons 4 and 5) will not apply to every type of librarian, her other three reasons: improving practices, partnering with and understanding the needs of researchers, and collaborating with librarians in different environments, are relevant across the profession.

Walters and Wilder ([2015](#walt15)) examine the contributions to library and information science journals from 2007 to 2012 and analyse the distribution of authors in terms of their contributions, their country of origin, and where they publish: just forty research libraries account for 32% of all published articles authored by librarians and thirty-eight of these libraries are based in the US, although a number of the most prolific librarian authors are based in Europe and Asia. Finlay _et al._ ([2013, p. 413](#fin13)) note that _‘The decrease in the number of librarian-authored journal articles is notable’_ and their data indicate that this decrease amounted to 15% between 2002-6 and 2007-11\. While this cannot be seen as positive for the profession as a whole, they suggest that librarians may now be using social media as their preferred channels for _‘service-oriented literature’_, avoiding the peer-review process and delays between writing and publication. They also used a title keyword analysis to compare the topics of librarian and non-librarian contributions and concluded that _‘Librarian-authored journal articles feature a greater emphasis on library services, while non-librarian-authored journal articles display a focus on information seeking, use, retrieval, and informatics’_ (p. 413).

</section>

<section>

## Method

We selected information and library studies journals from the top two quartiles of the Scimago Library and Information Sciences category (http://www.scimagojr.com/journalrank.php?category=3309), and removed those titles with a purely information science emphasis that would be unlikely to publish articles with an emphasis on the professional practice of librarianship. This resulted in forty-seven journal titles that were then searched for on the Scopus database for the years 2013 to 2015\. In total, 4313 research articles were found (excluding reviews, letters and editorials) and their full bibliographic details along with the author affiliation, author keyword and citation count fields were downloaded to an Excel spreadsheet. We subsequently discovered that Scopus did not contain keyword data for nine of the forty-seven selected journals which reduced the effective sample size to thirty-nine journals and 3723 articles, which is still substantially larger than comparable studies (for example, [Chang, 2016](#cha16); [Hildreth and Aytec, 2007](#hil07)). Appendix 1 contains a list of the journal titles analysed and the nine titles without keywords that were removed from the analysis.

For each article, affiliation details of the authors were placed in separate columns and these were then examined by a search formula to determine whether the author was a librarian or an academic or fell into neither category. Because of a high degree of consistency in Scopus affiliation data the following signals were used to allocate the designation Librarian:

*   The words _Library_ or _Libraries_ followed by a comma;
*   The word _Librarian_;
*   The following phrases with appropriate truncations and variant spellings: “_Library at_”, “_Library of_”, “_Library Department_”, “_Public Library_”, “_University Library_”, “_Information Center_”.

The following were used to allocate the designation Academic:

*   “_School of_”, “_Department of_”, “_Information Science_”, “_Information Studies_”, “_Library Studies_”, “_Library Science_”, “_Faculty of_”, “_College of_”, “_Center for_”.

Our definition of an Academic was not restricted to individuals working within the discipline of library studies although the use of specific library studies terms in the formula will probably have weighted the output in that direction. The Scopus affiliation data did not always allow of a formulaic attribution (for example the entire affiliation data for one item was _Queens College, CUNY, United States_) and in these cases the authors were designated as Uncategorised.

There were a significant number of instances in which the formula returned a positive result for both categories, for example _Lane Medical Library, Stanford University School of Medicine_ which contains both the word _Library_ followed by a comma and the phrase “_School of_”. In this case it is clear that the primary designation relates to the library that is situated within the institution so the individual designated a Librarian. On the other hand the designation _Department of Library, Information Science at Indiana University_ returns a positive for both librarian (“_Library_” followed by a comma) and _Academic_ _(“Department of_”) but clearly refers to an academic. All cases in which a double positive was returned were examined and a manual determination was made. If there was genuine ambiguity, either no designation was assigned or they were assigned Dual status. The latter form a small but interesting group that will be considered further in a later stage of the study. Once the task of assigning author designations was completed it was then possible to assign each article to one of four categories: those with at least one _Librarian_ author, those with at least one _Academic_ author, those with at least one author from each of these categories (Collaborations) and those with no assigned authors (Uncategorised).

When the manual corrections were completed, a random sample of 100 articles was checked manually against the original documents on their publishers’ websites to determine whether the formula had assigned authors to the correct categories. Of that sample, only one author had been assigned to the wrong category, but we discovered that many of the Uncategorised authors could have been assigned to specific categories if the whole exercise had been carried out manually and additional data from the full article had been taken into account. Scopus does not always capture the full author affiliation that is recorded in the published article so that some cases such as _Stanford University Libraries_ or _Stanford University School of Medicine_ in the original document simply appear as _Stanford University_ in Scopus. While this points to a limitation in the use of Scopus data for a study of this kind, in that not all authors can be automatically assigned to categories, the low percentage of wrong designations means that the data indicated clearly the existence of two distinct groups, one consisting of Librarians and the other of Academics, and that valid claims can be made about the differences between these groups as they can be inferred from the data.

In addition, the algorithm did not take account of non-English organisational names and roles, nor of different roles within higher education institutions (e.g., Office of the Vice Provost (Teaching and Learning)), nor did it seek to categorise authors from other kinds of organisations, which included research groups (e.g., CIBER Research), industry or professional organisations (e.g., CILIP, Dublin Core Metadata Initiative), commercial organisations (e.g., CAVAL, Serials Solutions), and assorted others (e.g., Derbyshire County Council, California Men’s Colony). If an article had no authors that fell within either of categories _Librarian_ or _Academic_ it was classed as Uncategorised which reduced the effective sample size to 2781.

<table><caption>Table 1: Assigned articles by author</caption>

<tbody>

<tr>

<th>Assigned items authored by</th>

<th>No.</th>

<th>Percentage</th>

</tr>

<tr>

<td>Librarians (Category L)</td>

<td>1496</td>

<td>54%</td>

</tr>

<tr>

<td>Academics (Category A)</td>

<td>1446</td>

<td>52%</td>

</tr>

<tr>

<td>Dual status Librarian/Academic (Category D)</td>

<td>63</td>

<td>2%</td>

</tr>

<tr>

<td>Collaborations</td>

<td>224</td>

<td>8%</td>

</tr>

<tr>

<td>Total minus collaborations</td>

<td>2781</td>

<td></td>

</tr>

</tbody>

</table>

NB: An individual article could belong to more than one category (i.e. the collaborations) so the percentages are not cumulative and equal more than 100%.

Analysis was conducted on the keywords (single words and phrases) assigned to the articles. Two types of clustering were undertaken, the first a conceptual or manual approach drawing together similar or identical subjects with differing terminology and the second an automated approach with filters in Excel, which involved using individual words such as _model_ or _health_ as conceptual markers within keyword phrases. An analysis of the journals that the two authorship groups published in yielded further evidence of their tribal differences.

## Findings and analysis

The analysis of the keywords revealed interesting results both in terms of authors’ keyword use in general and of the different preoccupations and emphases of the two groups (Librarians and Academics).

*   5992 occur only once
*   820 occur only twice
*   327 occur only three times
*   235 occur ten times or more

There were many minor variations (e.g., _e-book_, _e-books_, _eBook_, _electronic book_, _electronic books_; _social networking sites_, _social network sites_, _SNS_, _SNSs_, _social networking websites_ (and combinations that included the initialism); _H-index_, _h index_, _h indices_). There were variations in the types of keyword with combined terms (e.g., _Effectiveness of e-government policy_) and individual keywords (e.g., _Age_; _Demographics_; _Open access_; _Perceptions_). In addition, some articles had both high level terms as well as specific ones (e.g., _Social media_, _Facebook_, and _Twitter_ were on the same article), while others duplicated terms (e.g., _RDA_ and _Resource description and access_). The wide variation in keywords is unlikely to be helpful to the end searcher who must perforce anticipate all variations and variants for any topic. If the aim of keywords is to increase findability and quickly scope an article for a potential reader, there is room for improvement on a journal-by-journal basis.

Keywords were clustered manually (see Table 2) and automatically (see examples in Tables 5 and 6) to provide more effective topical analysis of the data. Even when this clustering had been completed, there were still only 260 keywords or keyword clusters that occurred ten times or more (up from the 235 unclustered keywords).

<table><caption>Table 2: Top 20 topics for each type of author (manually clustered keywords)</caption>

<tbody>

<tr>

<th></th>

<th>Librarians</th>

<th></th>

<th>Academics</th>

</tr>

<tr>

<td>1</td>

<td style="background-color:#FFCCFF">Academic libraries, academic library</td>

<td>1</td>

<td style="background-color:#FFCCFF">Academic libraries, academic library</td>

</tr>

<tr>

<td>2</td>

<td style="background-color:#FFCC66">Information literacy/literacies (IL)</td>

<td>2</td>

<td style="background-color:#FFFF66">Social media</td>

</tr>

<tr>

<td>3=</td>

<td style="background-color:#99CCCC">Libraries, library</td>

<td>3</td>

<td>Public libraries/library</td>

</tr>

<tr>

<td>3=</td>

<td>Assessment</td>

<td>4=</td>

<td style="background-color:#FFCC66">Information literacy/literacies (IL)</td>

</tr>

<tr>

<td>5</td>

<td>Electronic resources/resource, e-resource/s</td>

<td>4=</td>

<td style="background-color:#99CCCC">Libraries, library</td>

</tr>

<tr>

<td>6</td>

<td style="background-color:#66CC99">Collaboration/s</td>

<td>6</td>

<td style="background-color:#66FFFF">Digital libraries/library</td>

</tr>

<tr>

<td>7=</td>

<td style="background-color:#CC6666">University libraries/library</td>

<td>7</td>

<td>E-government, Electronic government</td>

</tr>

<tr>

<td>7=</td>

<td>Library instruction</td>

<td>8=</td>

<td>User/s studies, User/s study</td>

</tr>

<tr>

<td>9</td>

<td style="background-color:#A9A9A9">E-books/book, eBook/s, electronic book/s</td>

<td>8=</td>

<td>Information retrieval</td>

</tr>

<tr>

<td>10</td>

<td style="background-color:#D3D3D3">Open access, OA</td>

<td>10</td>

<td style="background-color:#D3D3D3">Open access, OA</td>

</tr>

<tr>

<td>11</td>

<td>Collection development</td>

<td>11</td>

<td>Citation analysis, citations analysis</td>

</tr>

<tr>

<td>12=</td>

<td style="background-color:0099FF">Librarians, librarian</td>

<td>12=</td>

<td style="background-color:#66CC99">Collaboration/s</td>

</tr>

<tr>

<td>12=</td>

<td>Collection/s management</td>

<td>12=</td>

<td style="background-color:0099FF">Librarians, librarian</td>

</tr>

<tr>

<td>14</td>

<td>Metadata</td>

<td>12=</td>

<td>Information seeking behavio(u)r/s, information seeking, etc</td>

</tr>

<tr>

<td>15</td>

<td>interlibrary loan/s, Inter-library loan/s, ILL</td>

<td>15</td>

<td>Scholarly communication/s</td>

</tr>

<tr>

<td>16</td>

<td>Institutional repository/ies/(IR)</td>

<td>16</td>

<td>Internet</td>

</tr>

<tr>

<td>17=</td>

<td style="background-color:#66FFFF">Digital libraries/library</td>

<td>17=</td>

<td style="background-color:#CC6666">University libraries/library</td>

</tr>

<tr>

<td>17=</td>

<td>Linked data, Linked open data</td>

<td>17=</td>

<td>Bibliometrics, bibliometric</td>

</tr>

<tr>

<td>19</td>

<td>Library services/service</td>

<td>17=</td>

<td>Twitter</td>

</tr>

<tr>

<td>20</td>

<td style="background-color:#FFFF66">Social media</td>

<td>20</td>

<td style="background-color:#A9A9A9">E-books/book, eBook/s, electronic book/s</td>

</tr>

</tbody>

</table>

The two main authorship groups, Librarians and Academics, shared some interests, but showed noticeable differences as well. Table 3 provides the top ranked topics for each group. It is surprising to the authors of this research that _Academic libraries_ was the top topic of interest for both groups, while _Public libraries_ was of much more interest to Academics (ranked 3rd) than to Librarians (ranked 23rd). _Assessment_ was also unexpectedly high on the Librarians’ list (3rd equal) but 94th on the Academics’ list. Half of the topics were unique to each group. _Scholarly communication_ was 21st for Librarians followed by Survey(s), while _Information behavio(u)r_ was 21st for the Academics, followed by _Students_.

For both groups, the first ranked topic of _Academic libraries_ had significantly more articles than the second topic

*   Librarians: 229 articles on Academic libraries to 124 on Information literacy
*   Academics: 106 articles on Academic libraries to 62 on Social media.

Across all groups (including Uncategorised), there were 426 articles with the keywords for academic libraries, and an additional 89 with the keywords for university libraries (see Table 3). The top three topics for librarians were the same as the top three topics overall.

<table><caption>Table 3: Top 12 topics overall (clustered) across all groups</caption>

<tbody>

<tr>

<th></th>

<th>Topic (clustered)</th>

<th>Number of articles</th>

</tr>

<tr>

<td>1</td>

<td style="background-color:#FFCCFF">Academic libraries, academic library</td>

<td>426</td>

</tr>

<tr>

<td>2</td>

<td style="background-color:#FFCC66">Information literacy/(IL), Information literacies</td>

<td>232</td>

</tr>

<tr>

<td>3</td>

<td style="background-color:#99CCCC">Libraries, library</td>

<td>159</td>

</tr>

<tr>

<td>4</td>

<td>Electronic resources/resource, e-resource/s</td>

<td>99</td>

</tr>

<tr>

<td>5=</td>

<td style="background-color:#FFFF66">Social media</td>

<td>97</td>

</tr>

<tr>

<td>5=</td>

<td style="background-color:#D3D3D3">Open access, OA</td>

<td>97</td>

</tr>

<tr>

<td>7</td>

<td style="background-color:#66FFFF">Digital libraries/library</td>

<td>96</td>

</tr>

<tr>

<td>8</td>

<td>Public libraries/library</td>

<td>94</td>

</tr>

<tr>

<td>9=</td>

<td style="background-color:#66CC99">Collaboration/s</td>

<td>90</td>

</tr>

<tr>

<td>9=</td>

<td>Assessment</td>

<td>90</td>

</tr>

<tr>

<td>11</td>

<td style="background-color:#CC6666">University libraries/library</td>

<td>89</td>

</tr>

<tr>

<td>12</td>

<td style="background-color:#A9A9A9">E-books/book, eBook/s, electronic book/s</td>

<td>78</td>

</tr>

</tbody>

</table>

Collaborations between Academics and Librarians were infrequent when compared to authorship within each group, and overall there were very few collaborations between these groups. Some collaboration was conducted with the Uncategorised authors and this collaboration will be presented in a later paper. Table 4 shows the keywords and keyword clusters of articles with authors who were both Academics and Librarians (colours have been matched to Table 2). The top ranked topic for collaboration, _Academic libraries_, accounted for 22 articles; the next two topics accounted for 20 and 10 articles respectively, and all other collaborative articles were in single figures. It is notable that the top topics for collaboration matched the top topics of the Librarians (although _Assessment_ dropped from 3rd equal to 10).

<table><caption>Table 4: Top 20 topics for collaborations</caption>

<tbody>

<tr>

<th>Rank</th>

<th>Topic</th>

<th>Librarians' rank</th>

<th>Academics' rank</th>

</tr>

<tr>

<td>1</td>

<td style="background-color:#FFCCFF">Academic libraries, academic library</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>2</td>

<td style="background-color:#FFCC66">Information literacy/(IL), Information literacies</td>

<td>2</td>

<td>4=</td>

</tr>

<tr>

<td>3</td>

<td style="background-color:#66CC99">Collaboration/s</td>

<td>6</td>

<td>12=</td>

</tr>

<tr>

<td>4</td>

<td style="background-color:#99CCCC">Libraries, library</td>

<td>3=</td>

<td>4=</td>

</tr>

<tr>

<td>5</td>

<td>Scholarly communication/s</td>

<td>21</td>

<td>15</td>

</tr>

<tr>

<td>6=</td>

<td style="background-color:#FFFF66">Social media</td>

<td>20</td>

<td>2</td>

</tr>

<tr>

<td>6=</td>

<td style="background-color:#66FFFF">Digital libraries/library</td>

<td>17</td>

<td>6</td>

</tr>

<tr>

<td>6=</td>

<td>User studies, Users studies, User study, User's study</td>

<td>34</td>

<td>8=</td>

</tr>

<tr>

<td>6=</td>

<td style="background-color:#66CC99">bgcolor="#D3D3D3">Open access, OA</td>

<td>10</td>

<td>10</td>

</tr>

<tr>

<td>6=</td>

<td style="background-color:#0099FF">Librarians, librarian</td>

<td>12=</td>

<td>12=</td>

</tr>

<tr>

<td>6=</td>

<td>Information seeking behaviour/s, behavior/s, information-seeking etc</td>

<td>35</td>

<td>12=</td>

</tr>

<tr>

<td>6=</td>

<td style="background-color:#CC6666">University libraries/library</td>

<td>7=</td>

<td>17=</td>

</tr>

<tr>

<td>6=</td>

<td>Library instruction</td>

<td>7=</td>

<td>116</td>

</tr>

<tr>

<td>14</td>

<td>Evidence-based medicine</td>

<td>83</td>

<td>79</td>

</tr>

<tr>

<td>15=</td>

<td>Public libraries</td>

<td>23</td>

<td>3</td>

</tr>

<tr>

<td>15=</td>

<td>Semantic web</td>

<td>46</td>

<td>23</td>

</tr>

<tr>

<td>15=</td>

<td>Surveys, survey</td>

<td>22</td>

<td>30</td>

</tr>

<tr>

<td>15=</td>

<td>Evaluation</td>

<td>39</td>

<td>34</td>

</tr>

<tr>

<td>15=</td>

<td>Search engine/s</td>

<td>223</td>

<td>35</td>

</tr>

<tr>

<td>15=</td>

<td>Medical students</td>

<td>116</td>

<td>91</td>

</tr>

</tbody>

</table>

The other approach we took to keyword analysis was a _keyword within phrase_ approach, using an Excel character-string formula. With a high proportion of keywords (both single words and phrases) occurring on three or fewer occasions many important concepts were hidden from our best efforts at manually clustering. For example, the word _theory_ is rarely used as a keyword on its own, but it occurs in a total of 52 separate phrases ranging from _Activity theory_ to _Unified theory of acceptance and use of technology_. When _theories_ and _theoretical_ are added a further six phrases are found. Most of these theory keyword phrases occur only once in the sample (and only five of them more than twice) but taken together they constitute a significant meta-idea or a cognitive marker that can be used to cluster keywords and quantify their use by librarians and academics. These clusters can then be combined with related ones (e.g., _theory_ or _model_; _assessment_ or _evaluation_) to create substantial idea sets.

<table><caption>Table 5: Example of keyword clustering</caption>

<tbody>

<tr>

<th>Author keywords</th>

<th>Occurrences</th>

<th>Librarians</th>

<th>Academics</th>

<th>Collaborations</th>

</tr>

<tr>

<td>Regulatory model</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>Research model</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>Resource dependency theory</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>Rogers' diffusion of innovation theory</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>role theory</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Selection model</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>Self-efficacy theory</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>Social capital theory</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>Social cognitive theory</td>

<td>2</td>

<td>0</td>

<td>2</td>

<td>0</td>

</tr>

<tr>

<td>Social exchange theory</td>

<td>2</td>

<td>0</td>

<td>2</td>

<td>0</td>

</tr>

<tr>

<td>Totals</td>

<td>12</td>

<td>3</td>

<td>11</td>

<td>2</td>

</tr>

</tbody>

</table>

In Table 5, which combines _theory_ and _model_ terms, the phrase _Research model _occurs once with at least one Librarian and one Academic as authors so it is a collaboration whereas both occurrences of_ Social cognitive theory_ have academic authors only. It is notable that nine of the ten keyword phrases in this example are used by Academics but only three of them by Librarians and that eleven of the twelve occurrences have Academic authors

When these clusters are aggregated it is possible to calculate the percentages of articles using the keyword phrases whose authors are Librarians or Academics, and the percentages which are collaboratively authored. The _medical_ or _health_ cluster, for example, shows a high level of collaboration and a somewhat higher authorship by Librarians (see Figure 1 and Table 6) whereas the cluster shows low collaboration and a high proportion of Academic authors (Table 6).

<figure>

![Medical or health keywords, by author](../rails1605fig1.jpg)

<figcaption>Figure 1: Medical andhealth keywords, by author</figcaption>

</figure>

<table><caption>Table 6: Previous information seeking</caption>

<tbody>

<tr>

<th>Keyword cluster</th>

<th>Occurrences</th>

<th>Exclusively Librarian authors</th>

<th>Exclusively Academic authors</th>

<th>Collaboration</th>

</tr>

<tr>

<td>Medical or health</td>

<td>270</td>

<td>46%</td>

<td>37%</td>

<td>17%</td>

</tr>

<tr>

<td>Collaboration</td>

<td>122</td>

<td>55%</td>

<td>31%</td>

<td>14%</td>

</tr>

<tr>

<td>Evaluation</td>

<td>123</td>

<td>37%</td>

<td>52%</td>

<td>11%</td>

</tr>

<tr>

<td>Assessment</td>

<td>156</td>

<td>72%</td>

<td>19%</td>

<td>9%</td>

</tr>

<tr>

<td>Assessment or evaluation</td>

<td>261</td>

<td>57%</td>

<td>33%</td>

<td>10%</td>

</tr>

<tr>

<td>User</td>

<td>260</td>

<td>42%</td>

<td>49%</td>

<td>9%</td>

</tr>

<tr>

<td>Library services</td>

<td>89</td>

<td>54%</td>

<td>39%</td>

<td>7%</td>

</tr>

<tr>

<td>Public library</td>

<td>110</td>

<td>24%</td>

<td>70%</td>

<td>6%</td>

</tr>

<tr>

<td>Theory or model</td>

<td>169</td>

<td>16%</td>

<td>80%</td>

<td>4%</td>

</tr>

<tr>

<td>Quality</td>

<td>87</td>

<td>32%</td>

<td>65%</td>

<td>3%</td>

</tr>

</tbody>

</table>

Table note: some articles included both evaluation keywords and assessment keywords so the _Assessment_ or _evaluation_ cluster is not the sum of Evaluation plus Assessment.

Some interesting patterns in word usage emerge. We should avoid over-interpreting the meaning of these clusters (for example, the _Assessment or evaluation_ cluster includes _Collection assessment_, _Research assessment_ and _Student assessment_) but the striking differences that emerge from the data are suggestive of underlying differences in the preoccupations of the two groups. Of the articles in the _Assessment or evaluation_ cluster, 72% are authored exclusively by Librarians. When the two keywords (assessment and evaluation) are combined, the resulting cluster clearly shows a greater authorship by Librarians suggestive of a tendency towards consideration of practices and outcomes rather than theories and explanations. It is possible that for Academics, _Assessment_ is something that they consider to be part of the teaching process (students are assessed), whereas for Librarians it is part of the justification of their services and value to their clients. Further investigation is planned, because examining the keywords singly or even in topical clusters does not provide sufficient context for their articles.

Although Farkas ([2013](#far13)) has argued that _‘few libraries can claim to have developed a culture of assessment’_, the results for librarian-authored articles point to considerable interest in the matter. Perkins and Slowik’s ([2013](#per13)) participants wanted more research on _‘the effects of changes in the library’s mission, organization, and role’_ over the next 10 years, along with _‘evidence-based research on user needs’_ and _‘the impact of the economy, budgets, and justification of investments and expenditures’_ (p. 151). Our research suggests that these topics are already being addressed.

One unexpected result was the higher percentage of academics using _Public library_ (Figure 2) but this may be partly explained by our selection of journals from the top two quartiles of Scimago. Although public librarians appear to publish considerably less scholarly research than their university-based colleagues, much of their work appears in titles outside our sample. Another partial explanation could be that academics may see public libraries as a field of study less well-served by its own practitioners, in contrast to academic libraries. However, as Wiegand ([2016](#wie16)) noted, public libraries are not well-served by research generally.

<figure>

![Public library keywords, by author](../rails1605fig2.jpg)

<figcaption>Figure 2: Public library keywords, by author</figcaption>

</figure>

Librarians and Academics do not generally publish in the same journals (see Figure 3). While this is not surprising given the focus of particular journals, it is also a likely indicator of what is read by (or of interest to) the different groups. The implications are two-fold: each group needs to read what the other is writing about to get a full picture of the profession and its concerns, and, if academics want librarians to read their research, they should publish it in the journals that librarians read (and, of course, vice versa).

<figure>

![Percentages of librarian authors and academic authors, by journal](../rails1605fig3.jpg)

<figcaption>

Figure 3: Percentages of librarian authors (blue) and academic authors (green), by journal [[Click for large version](../rails1605fig3.jpg)—opens in new tab.]</figcaption>

</figure>

## Conclusion

This paper has highlighted some of the areas that librarians and academics research and write about, and identified the differences (which are substantial) as well as the similarities. Ten of the top twenty topics (manually clustered) are the same, although ranked differently. Collaboration is not a focus of either group, with only 6% (249 articles) being collaborations between members of these groups. There does not seem to have been much increase in collaboration since the research by Hildreth and Aytac ([2007](#hil07)).

The situation could best be described as parallel or diverging lines, rather than partnerships. This does not seem to be positive for the profession as a whole, because the research done by academics may lack relevance to practitioners, while practitioners do not get the benefit of the thorough research conducted by academics that could improve professional practices. This gap, which exists despite the frequently good intentions of both groups, is unfortunate. When there are so many threats to libraries from alternative information providers, partnership is a necessity to help ensure the survival of the profession. Greater involvement with research by librarians would benefit the profession as a whole. Although librarians greatly outnumber library and information studies academics, they do not outnumber them in this sample of journal articles, where articles by librarians accounted for 40% and articles by academics accounted for 39%. However, Walters ([2016](#walt16)) says that librarians _‘account for 23 percent of the LIS literature and they contribute roughly two-thirds of the articles published in practice oriented-journals’_ (p. 821). We note that journals in our study are from the top two quartiles of the Scimago Library and Information Sciences category, and that data from the bottom two quartiles might change the results by increasing the number of librarian authors. Further research will address this issue.

We suggest that academics need increased understanding of the profession and its practices, needs, interests and pressures to contribute better to its development, value and sustainability, while librarians need increased understanding of the research process across all disciplines, of scholarly publishing, and of research data management. Better understanding of scholarly publishing and the scholarly research process will benefit librarians and their clients, while better understanding of the aspects and areas that are of interest to librarians will enable academics to target their research towards profession-wide concerns. This could be achieved by both groups actively seeking collaboration and partnerships beyond the traditional areas of support for information literacy (in the library or embedded within courses); better focus on the profession’s problems and issues; a greater engagement with research by librarians through reviewing, reading research articles, and developing an understanding of bibliometrics and scientometrics; research support measures that would encourage librarians to conduct more research; and more emphasis in library and information studies qualifications on research as a professional practice.

One advantage of this research is that it covers a wide range of journal titles, rather than the more limited range often found in similar studies. This means that it includes practitioner-focused as well as academic-focused journals. However, the disadvantage is that it covers only three years of articles (2013-2015) although the methodology is capable of further refinement and would be scalable to longer time periods to indicate changing interests and publishing behaviour over time.

The authors of this paper are an academic librarian (Bruce White) and a library and information studies lecturer (Amanda Cossham). They decided to collaborate to bring expertise from both groups to the research process, and to explore in practice the issues that they were researching and writing about. Both are experienced researchers and have close involvement with scholarly publishing. What started as a small research project to explore one aspect of practitioner–academic collaboration has become a much larger research project of which this can now be considered the first stage.

We plan further analysis of the data, including data clean-up of the Uncategorised authors, evaluation of how journals assign keywords, and the research support measures that currently exist for librarians in Australia and New Zealand (focusing initially on academic librarians, and moving on to other types). Data from the bottom two quartiles of the Scimago Information and Library Science category will also be extracted and evaluated to give a fuller picture, and the extent of collaboration in particular journals is also an aspect we intend to follow up.

## Acknowledgements

Thanks to the RAILS 2016 Conference attendees for questions and comments on the presentation, and to the reviewers for their suggestions.

## <a id="author"></a>About the authors

**Bruce White** is Copyright and Open Access Advisor at Massey University, New Zealand. Previously he has been an academic librarian for and his research interests include bibliometrics, open access, collection management and knowledge translation. He can be contacted at [b.d.white@massey.ac.nz](mailto:b.d.white@massey.ac.nz)  
**Dr Amanda Cossham** is a lecturer in Information and Library Studies at the Open Polytechnic of New Zealand, where she teaches cataloguing and classification, social informatics and information literacy. Her research interests include knowledge organisation and continuing professional development. She can be contacted at [amanda.cossham@openpolytechnic.ac.nz](mailto:amanda.cossham@openpolytechnic.ac.nz)

</section>

<section>

## References

<ul>
<li id="abb98">Abbott, A. (1998). Professionalism and the future of librarianship. <em>Library Trends, 46</em>(3), 430–443.</li>
<li id="auc12">Auckland, M. (2012). <em><a href="http://www.webcitation.org/6tNrgnJBV">Reskilling for research: an investigation into the role and skills of subject and liaison librarians required to effectively support the evolving information needs of researchers.</a></em> RLUK: Research Libraries UK. Retrieved from http://www.rluk.ac.uk/wp-content/uploads/2014/02/RLUK-Re-skilling.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tNrgnJBV)</li>
<li id="boo03">Booth, A. (2003). Bridging the research-practice gap? The role of evidence-based librarianship. <em>New Review of Information and Library Research, 9</em>(1), 3–23. </li>
<li id="boo11">Booth, A. (2011). Barriers and facilitators to evidence-based library and information practice: an international perspective. <em>Perspectives in International Librarianship, 1</em>, 1–15. 
</li><li id="cha15">Chang, Y.-W. (2015). Librarians’ contribution to open access journal publishing in library and information science from the perspective of authorship. <em>The Journal of Academic Librarianship, 41</em>, 660–668.</li>
<li id="cha16">Chang, Y.-W. (2016). Characteristics of articles coauthored by researchers and practitioners in library and information science journals. <em>The Journal of Academic Librarianship, 42</em>, 535–541.</li>
<li id="chu07">Chu, F. T. (2007, June). <a href="http://www.webcitation.org/6tNs2TTDj">Bridging the LIS-practitioner gap: some frames for research.</a> <em>Library Philosophy and Practice.</em> Retrieved from http://digitalcommons.unl.edu/libphilprac/. (Archived by WebCite&reg; at http://www.webcitation.org/6tNs2TTDj)</li> 
<li id="cla10">Clapton, J. (2010). <a href="http://www.webcitation.org/6tNsHPC7D"> Library and information science practitioners writing for publication: motivations, barriers and supports</a>. <em>Library and Information Research, 34</em>(106), 7–21. Retrieved from http://www.lirgjournal.org.uk/lir/ojs/index.php/lir (Archived by WebCite&reg; at http://www.webcitation.org/6tNsHPC7D</li>
<li id="far13">Farkas, M. G. (2013). Building and sustaining a culture of assessment: best practices for change leadership. <em>Reference Services Review, 14</em>(1), 13–31.</li> 
<li id="fin13">Finlay, S. C., Ni, C., Tsou, A. &amp; Sugimoto, C. R. (2013). Publish or practice? An examination of librarians’ contributions to research. <em>Portal: Libraries and the Academy, 13</em>(4), 403–421.</li>
<li id="gal14">Galbraith, Q., Smart, E., Smith, S. D. &amp; Reed, M. (2014). <a href="http://www.webcitation.org/6tNsYEF5v">Who publishes in top-tier library science journals? An analysis by faculty status and tenure.</a> <em>College &amp; Research Libraries, 75</em>(5), 724–735. Retrieved from https://doi.org/10.5860/crl.75.5.724 (Archived by WebCite® at http://www.webcitation.org/6tNsYEF5v)</li>
<li id="gal16">Galbraith, Q., Garrison, M., Hales, W. (2016). <a href="http://www.webcitation.org/6tNt7K7Vy">Perceptions of faculty status among academic librarians.</a> <em>College &amp; Research Libraries, 77</em>(5), 582–594. Retrieved from https://doi.org/10.5860/crl.77.5.582  (Archived by WebCite® at http://www.webcitation.org/6tNt7K7Vy)</li>
<li id="gen04">Genoni, P., Haddow, G. &amp; Ritchie, A. (2004). Why don't librarians use research? In A. Booth and A. Brice, (Eds.), <em>Evidence-based practice for information professionals: a handbook</em> (pp. 49–60). London: Facet Publishing.</li>
<li id="had04">Haddow, G. &amp; Klobas, J. E. (2004). Communication of research to practice in library and information science: closing the gap. <em>Library &amp; Information Science Research, 26</em>(1), 26-43.</li>
<li id="hil07">Hildreth, C. R. &amp; Aytac, S. (2007). Recent library practitioner research: a methodological analysis and critique. <em>Journal of Education for Library and Information Science, 48</em>(3), 236–258.</li>
<li id="hry07">Hrycaj, P. &amp; Russo, M. (2007). Reflections on surveys of faculty attitudes toward collaboration with librarians. <em>The Journal of Academic Librarianship, 33</em>(6), 692–696.</li>
<li id="joi05">Joint, N. (2005). Promoting practitioner-researcher collaboration in library and information science. <em>Library Review, 54</em>(5), 289–294. </li>
<li id="mcb13">McBain, I., Culshaw, H. &amp; Walkley Hall, L. (2013). Establishing a culture of research practice in an academic library: an Australian case study. <em>Library Management, 34</em>(6/7), 448–461.</li> 
<li id="mur16">Murray, T. E. (2016). Applying research in special library settings. <em>Journal of Library Administration, 56</em>(4), 479–487.</li> 
<li id="pem98">Pemberton, J. M. (1998). Records management: confronting our professional issues. <em>Records Management Journal, 8</em>(3), 5–18.</li>
<li id="per13">Perkins, G. H. &amp; Slowik, A. J. (2013). The value of research in academic libraries. <em>College &amp; Research Libraries, 74</em>(2), 143–158.</li>
<li id="pha16">Pham, H.T. (2016). <a href="http://www.webcitation.org/6tNtLhUMV">Collaboration between academics and library staff: a comparative study of two universities in Australia and Vietnam.</a> Unpublished doctoral dissertation, Monash University, Melbourne, Australia. Retrieved from http://bit.ly/2hKrU2R (Archived by WebCite&reg; at  http://www.webcitation.org/6tNtLhUMV)</li>
<li id="pha14">Pham, H. T. &amp; Tanner, K. (2014). Collaboration between academics and librarians. <em>Library Review, 63</em>(1), 15–45. </li>
<li id="pon13">Ponti, M. (2013). Peer production for collaboration between academics and practitioners. <em>Journal of Librarianship and Information Science, 45</em>(1), 23–37.</li>
<li id="pow02">Powell, R. R., Baker, L. M. &amp; Mika, J. J. (2002). Library and information science practitioners and research. <em>Library and Information Science Research, 24</em>, 49–72.</li>
<li id="sas14">Sassen, C. &amp; Wahl, D. (2014). Fostering research and publication in academic libraries. <em>College &amp; Research Libraries, 75</em>(4), 458–491.</li> 
<li id="ten15">Tenopir, C. (2015, March). <a href="http://www.webcitation.org/6tNtYIHr6">Librarians do research too! (Library Connect blueprint for success).</a> Retrieved from https://libraryconnect.elsevier.com/articles/librarians-do-research-too-download-free-ebooklet-carol-tenopir-library-connect (Archived by WebCite® at http://www.webcitation.org/6tNtYIHr6)</li>
<li id="wal15">Walkley Hall, L. (2015). Changing the workplace culture at Flinders University Library: From pragmatism to professional reflection. <em>Australian Academic &amp; Research Libraries, 46</em>(1), 29–38.</li> 
<li id="walt16">Walters, W. (2016). The faculty subculture, the librarian subculture, and librarians’ scholarly productivity. <em>Portal: Libraries and the Academy, 16</em>(4), 817-843.</li> 
<li id="walt15">Walters, W. H. &amp; Wilder, E. I. (2015). Worldwide contributors to the literature of library and information science: Top authors, 2007–2012. <em>Scientometrics, 103</em>, 201-237.</li>
<li id="wie16">Wiegand, W. A. (2016, October 17). <a href="http://www.webcitation.org/6tNtmn5cH">Falling short of their profession’s needs.</a> <em>Inside Higher Ed.</em> Retrieved from https://www.insidehighered.com/views/2016/10/17/how-library-and-information-studies-research-shortchanging-libraries-essay (Archived by WebCite® at http://www.webcitation.org/6tNtmn5cH)</li>
<li id="you10">Yousef, A. (2010). <a href="http://www.webcitation.org/6tNu36Lhc">Faculty attitudes toward collaboration with librarians.</a> <em>Library Philosophy and Practice.</em> Retrieved from http://digitalcommons.unl.edu/libphilprac/ (Archived by WebCite&reg; at http://www.webcitation.org/6tNu36Lhc)</li>
</ul>

</section>

<section>

## Appendix 1

### Journal titles with keywords in Scopus

*   Annals of Library and Information Studies
*   Archival Science
*   Aslib Journal of Information Management
*   Australian Academic and Research Libraries
*   Cataloging and Classification Quarterly
*   Collection Building
*   College and Undergraduate Libraries
*   Electronic Library
*   Government Information Quarterly
*   Health information and libraries journal
*   Interlending and Document Supply
*   International Information and Library Review
*   Internet Reference Services Quarterly
*   Journal of Academic Librarianship
*   Journal of Access Services
*   Journal of Documentation
*   Journal of Electronic Resources Librarianship
*   Journal of Library Administration
*   Journal of Library and Information Services in Distance Learning
*   Journal of Library Metadata
*   Journal of the Medical Library Association
*   Journal of Web Librarianship
*   Legal Reference Services Quarterly
*   Library and Information Science Research
*   Library Hi Tech
*   Library Management
*   Library Review
*   Libri
*   Malaysian Journal of Library and Information Science
*   Medical Reference Services Quarterly
*   Music Reference Services Quarterly
*   New Library World
*   New Review of Academic Librarianship
*   Online Information Review
*   Reference Librarian
*   Reference Services Review
*   Serials Librarian
*   Serials Review
*   Technical Services Quarterly

### Journals without keywords in Scopus (removed from dataset)

*   Annals of Library and Information Studies
*   College and Research Libraries
*   Evidence Based Library and Information Practice
*   Information Technology and Libraries
*   Law Library Journal
*   Library Quarterly
*   Library Resources and Technical Services
*   Library Trends
*   Reference and User Services Quarterly

</section>

</article>