#### vol 24 no.1, March 2019

# Access to academic libraries: an indicator of openness?

## [Katie Wilson, Cameron Neylon, Lucy Montgomery](#author), and [Chun-Kai (Karl) Huang](#author)[](#author)

> **Introduction**. Open access to digital research output is increasing, but academic library policies can place restrictions on public access to libraries. This paper reports on a preliminary study to investigate the correlation between academic library access policies and institutional positions of openness to knowledge.  
> **Method**. This primarily qualitative study used document and data analysis to examine the content of library access or use policies of twelve academic institutions in eight countries. The outcomes were statistically correlated with institutional open access publication policies and practices.  
> **Analysis**. We used an automated search tool together with manual searching to retrieve Web-based library access policies, then categorised and counted the levels and conditions of public access. We compared scores for institutional library access feature with open access features and percentages of open access publications.  
> **Results**. Academic library policies may suggest open public access but multi-layered user categories, privileges and fees charged can inhibit such access, with disparities in openness emerging between library policies and institutional open access policies.  
> **Conclusion**. As open access publishing options and mandates expand, physical entry and access to print and electronic resources in academic libraries is contracting. This conflicts with global library and information commitments to open access to knowledge.

<section>

## Introduction

In recent years global library and information communities have reaffirmed commitments to providing access to information and knowledge. The International Federation of Library Associations and Institutions (IFLA) adopted the theme Libraries Driving Access to Knowledge from 2009-2011 ([Lau, Tammaro and Bothma, 2012](#lau12)), confirming IFLA support for the open access movement. IFLA advocated for the United Nations Sustainable Development programme to incorporate access to information, and information and communication technologies, in achieving the UN sustainable goals. In 2015 the United Nations member states made access to information a specific target under Goal 16 of the 2030 Agenda for the UN Sustainable Development Goals. Libraries provide and promote access for people to '_the information they need to live, learn, create, and innovate_' ([International Federation of Library Associations and Institutions, 2017, p. i](#int17)). However, the Lyon Declaration on Access to Information and Development, with over 600 library, education and information signatories, notes that '_Half of the world's population lacks access to information online_' (cited in [International Federation of Library Associations and Institutions, 2018](#int18)).

Academic libraries in many countries have had a tradition of being open to the wider public in order to enable access to scholarly knowledge and research, to fulfill institutional missions, to facilitate good public relations and promote community engagement ([Hang, 2013](#han13)). However, this openness began to contract in the latter part of the twentieth century ([Courtney, 2001](#cou01)). Increased demand, reduced library budgets, the growth of the Internet and commercial electronic publisher licensing restrictions have led libraries to implement multi-layered access policies for external users. Access to electronic research is either through output published as open access, or available in institutional repositories, or behind commercial publisher paywalls accessible only through university library subscriptions. Cancellations of print journals in favour of electronic versions means material once available on academic library shelves may now be inaccessible to many who are not faculty, students or staff. Further, a large proportion of university library collections is only available in a physical format.

Digitisation has created challenges for academic libraries in relation to space and access to material formats. A disconnect now exists between physical access to library buildings in order to consult material in different formats, including print, and electronic access to research information. Publisher license contracts place restrictions on who can use the online databases, full text e-journals and e-books to which libraries subscribe, often limiting access to core library users. Competing demands for study and social space and shelf storage for physical materials within libraries create further tensions ([Mehtonen, 2016](#meh16)). Library acquisition policies have to balance such issues in accommodating the changes from physical to digital collections.

</section>

<section>

## Aims of the study

By their nature, academic library access policies provide insight into the ways in which a university views its role within the knowledge landscape of a wider community. It is arguable that library access policies reflect the extent to which a university views its knowledge resources as assets to be managed on behalf of an exclusive group of staff or students; or as resources most likely to benefit both the community and the institution if they are shared beyond the university. Research is increasingly published online, sometimes exclusively, but academic library collections still include archival and print resources, particularly in the arts and humanities ([Research Information Network, 2009](#res09)). Massachusetts Institute of Technology Libraries ([2016](#mit16)) noted that while almost 90% of the collection budget is spent on electronic books and journals, most of the libraries' holdings are in physical format. Only 8% of the 1.3 million book titles, 1% of archival collections and 38% of theses or dissertations were available online ([Massachusetts Institute of Technology Libraries, 2016](#mit16), pp. 10-11). Further, the retrospective deposit of legacy journal literature into open access institutional repositories is difficult for legal publisher copyright and institutional policy reasons, and because motivating authors to deposit legacy items into institutional repositories is challenging ([University of California Libraries, 2018](#uni18)). In such an environment how do universities and libraries fulfill openness missions through providing access to both physical and digital published collections, extending access to knowledge?

From the 1990s, the capacity of university libraries to engage with the information needs of wider communities has been challenged by shrinking budgets, new complexities associated with the impact of digital technologies, and the subscription models applied by online content vendors ([Karaganis, 2018](#kar18)). Maintaining access to libraries for users other than staff or students has become harder to sustain ([Courtney, 2001](#cou01), [2003](#cou03)). Academic libraries managing the complex demands of commercial licensing, copyright, budget limitations, and security prioritise use and access policies around the information needs of their core communities: students, staff and faculty. Access for unaffiliated users has not disappeared entirely, but often involves membership fees charged to offset additional costs such as electronic resources and associated services.

The key research question that this paper considers is how university library access policies reflect their institutions' positions on open knowledge. It does this by asking the following questions:

> Do academic library Websites include access or use policies that specify conditions of access for unaffiliated members of the public?  
> How do library access policies correlate with institutional open access policies and open access publishing?  
> What do library access policies on Websites suggest to the wider public about institutional openness?

We first review literature regarding changes in academic library access policies and related studies. The second section presents an analysis of access and use policies from twelve academic libraries across four continents, and views them in relation to institutional open access policies and percentages of open access publishing.

</section>

<section>

## Literature review

The literature identifies changes in academic library access policies since the mid-twentieth century, reflecting financial constraints and the changing electronic publishing environment.

### Background

A range of studies has analysed library access policy and Internet documents, conducted online and telephone surveys, or employed a combination of both methods. Several examined the effects of policy changes and restrictions on library and institutional mission statements or intentions. The literature is dominated somewhat by the United States, with its long history of academic library community engagement ([Dunne, 2009](#dun09)) and publication requirements for tenured professional academic librarians. That there is less discussion from other parts of the world suggests open access to academic libraries may not be an issue everywhere. For example, university libraries in Finland are open to all ([Lehto, Toivonen and Iivonen, 2012](#leh12)). It also reflects the predominance of English language and the north Atlantic in the literature. Further, fewer reviews of library access policies appear in the last five to ten years during which the open access publishing landscape has grown and changed rapidly, moving from an extreme position to mainstream policy.

### Terminology

Academic library policies adopt a range of terminology to identify users external to institutions, those who are not registered as faculty, staff or students ([Barsun, 2003](#bar03); [Burclaff and Britz, 2011](#bur11); [Weare and Stephenson, 2012](#wea12)). Terms include alumni, unaffiliated or non-affiliated user, external user, non-institutional borrower, community member, member of the public, visitor and day visitor. They may be one-time visitors with needs such as consulting print or electronic material, seeking study space or computer facilities, or individuals with longer term ongoing or complex research needs who may be required to register as members.

### Opening and closing of library doors

The history of publicly funded university libraries from the mid-twentieth century shows a marked trend in opening libraries to external users. Decisions to make the resources of university libraries available to the wider community reflected awareness of the role of libraries in generating goodwill among the communities that ultimately funded their operations. Publicly funded university libraries also recognised their role as part of the information infrastructure that their local communities depended on, and their capacity to help lessen the impact of budget cuts to public libraries by making their collections available for community use.

However, extending access, services, borrowing privileges and study space to unaffiliated patrons such as high school students, members of the public, local business and industry became difficult for university libraries to sustain, and they began to prioritise servicing their primary users, students and faculty ([Courtney, 2001](#cou01)). Surveys of US academic libraries in the 1990s showed institutions offering a range of options, from continuing to provide equal physical access, restricting access for unaffiliated users, including alumni, charging fees, to implementing 'tiered access policies' ([Burclaff and Britz, 2011](#bur11), p. 3).

Regional, national and international reciprocal borrowing, co-operative and consortial agreements enabling the sharing of print materials among member libraries' users developed in the 1960s and 1970s ([Duy and Larivière, 2013](#duy13)). These schemes grew in the 1980s and 1990s to supplement shortfalls in library budgets as purchase costs increased. Globally such institutional agreements provide reciprocal borrowing, consortial purchasing, and increasingly focus on shared digital resources ([Dong and Zou, 2009](#don09)). Access to shared resources (print and digital) does not extend to users who are external to member institutions, although some academic consortial lending schemes include public libraries.

In addition to agreements between institutions, who bear and share the costs of reciprocal borrowing, academic libraries have implemented tiered arrangements incorporating levels of access to institutional alumni, external organisations, private or individual researchers, and members of the public. This can involve a daily, joining or annual membership fee that provides physical access to libraries, borrowing of print material, access to a subset of electronic resources, but restricted access to collections and services that may conflict with institutional core users' needs ([Weare and Stephenson, 2012](#wea12)). The fee is presumably intended to offset administrative and staff costs of providing access and services to external users, but analysis or evaluation of the economic impact of fees is limited. One small US library removed membership fees and found increased community use but minimal impact in terms of extra cost to the library, although the authors acknowledged economic impact is difficult to assess ([Dole and Hill, 2011](#dol11)). An outcome of fee-charging, intentional or not, is the narrowing of access to information.

In the twenty-first century, policies have moved rapidly from providing open, public onsite access to academic library collections and published research, both online and in print ([Courtney, 2003](#cou03)), to multi-levelled and multi-dimensional access incorporating coalitions and consortia with other research institutions and organisations. In such arrangements the individual, unaffiliated researcher, member of public may have the least access.

### The growth in commercial electronic publishing

For reasons of collection and space management, and to extend teaching and research access to published knowledge, many libraries have moved towards an electronic collection preference purchasing model. Declining physical loan circulation statistics have pushed academic libraries to justify physical shelving space ([Duy and Larivière, 2013](#duy13)). Pre-online, print only materials such as journal and newspaper volumes not available electronically may be discarded to create computer and study spaces and minimise shelf management. Libraries cancelled print serial subscriptions in favour of electronic versions ([Courtney, 2003](#cou03)), reducing access to books and journals published electronically for users external to academic institutions. Libraries now subscribe to online packages through aggregating vendors who impose restrictions based on subscriptions and licensing with full access limited to registered, authenticated institutional members. This knowledge is leased, not owned by the institution. Lorcan Dempsey ([2016](#dem16)) refers to a shift towards 'facilitated collection', where libraries manage or are stewards of external collections and information partnerships (p. 354). Tiered access to electronic resources delineates categories of users and access, for example: all resources for core users; a set for alumni, onsite visitor or guest use; and resources freely available to the public. Ironically, while access to some current research output is expanding through open access online publications, access to knowledge paywalled commercially may be shrinking or narrowing for populations not affiliated with universities or research institutions.

Heather Joseph, Executive Director of the Scholarly Publishing and Academic Research Coalition (SPARC) noted how difficult it had become for external users to access information in research publications, and the financial barriers for academic libraries, 'even the wealthiest private research institution in the United States' ([Joseph, 2008](#jos08), p. 98), to scholarly research. In 2008, Joseph was positive about the growth of public declarations promoting open access to research, government policies and research organisational mandates, with emerging options for repository deposit and open access journal publications. However, over the ten years since Joseph's article, access to research and knowledge has become more complex. Elizabeth Gadd ([2017](#gad17)) noted that self-archiving policies of journal publishers had become more restrictive and complex since 2004\. Publishers' 'paid (gold) open access options over time increased at a similar rate to the volume of self-archiving restrictions' ([Gadd, 2017](#gad17), p. 103). Universities, while adopting open access policies, have left the ownership of copyright in scholarly publications largely to academics, funders and publishers. Gadd suggested universities implementing joint copyright ownership could be more beneficial for universities, academics and open scholarship, although this is at odds with the Budapest Open Access Initiative ([2002](#bud02)) statement regarding authors' control over the integrity of their work. The recent growth of 'shadow libraries' ([Karaganis, 2018](#kar18), p. 1) or 'black open access' ([Björk, 2017](#bjo17), p. 173) through systems such as Sci-Hub and Library Genesis (LibGen) has broadened access to scholarly publications, demonstrating an access need, but in bypassing copyright these systems threaten the role of academic libraries in this space.

### Previous studies

In the 1980s and 1990s, as libraries grappled with appropriate policies to meet changing circumstances, surveys and studies of library access, particularly in the United States, indicated continued support for unaffiliated users ([Burclaff and Britz, 2011](#bur11)). As library access expressed in policies contracted into the 2000s, several studies examined access for particular groups of external users. More recent studies surveyed institutions by questionnaire and interview, and through analysis of Web documents. Some discuss the relationship between library access policies and university and library mission statements, but do not articulate a correlation with open access to knowledge.

In two related articles, Nancy Courtney reviewed ([2001](#cou01)) and surveyed ([2003](#cou03)) the policies and practices of US academic libraries towards 'unaffiliated users...barbarians at the gate' ([2001](#cou01), p. 473) (the terminology reflects changing attitudes to external users). Reviewing literature from 1950 to 2000 Courtney chronicled the debates and the challenges to free, unaffiliated access from library budgets, computerisation and the Internet. She correctly predicted 'the possibility of diminished access' ([2001](#cou01), p. 478) rather than expansion, as a result of the growth in electronic resources. Surveying 841 higher education institutions (with 527 responses), Courtney ([2003](#cou03)) found most academic libraries continued to offer open access for unaffiliated users to buildings and borrowing (with some restrictions), a result similar to a 1965 Association of College and Research Libraries survey. However, Courtney's survey responses showed increasing limitations to electronic resources as a result of print serial cancellations in favour of online with vendor restrictions and campus computing authentication policies. A similar pattern emerged in five studies by the UK Research Information Network ([2009](#res09)), although there was an increase in external use of the research libraries since 2007\.

Recent studies of United States academic and research libraries analysed access policies for unaffiliated users (for example, [Barsun, 2003](#bar03); [Burclaff and Britz, 2011](#bur11); [Weare and Stephenson, 2012](#wea12); [Whitehead, Gutierrez and Miller, 2014](#whi14)). They found wide variations in external access policies and conditions, with differing fee structures. The information for potential public users was often insufficient, particularly in relation to electronic resources. Beyond universities, Esther Roth-Katz ([2012](#rot12)) analysed visitor and use policies of US art museum library Websites and concluded they did not always communicate clearly the missions of art museums to 'serve the public' as set out in the 'Code of Ethics for Museums' ([Roth-Katz, 2012](#rot12), p. 136).

</section>

<section>

## Method

In this study we set out to explore, through Internet-based policy documents and related information, how university library access or use policies reflect and project institutional positions of openness to knowledge. The initial sample for the study was fourteen medium to large research universities in Australia, Brazil, China, Hong Kong, Mexico, Singapore, South Africa, Taiwan, the United Kingdom and the United States. The selection includes universities with a mix of open-access publication policies, institutional repositories, university presses, high profile research output and smaller research output. A range of countries across four continents was selected to provide a spread of languages and practices.

Using qualitative data analysis, we examined the content of Web-based library access policies to identify types and levels of institutional and external library membership. This involved an iterative process of researchers reading and re-reading the documents ([Bowen, 2009](#bow09)). Several reviews of the documents were needed in order to elicit comparative data for each institution. Further manual searching and following Weblinks located more detailed library membership data. This was required because the information and terminology used to describe membership are not standardised and vary geographically and linguistically. With the information gathered we were able to identify patterns in the data, classify library patron type groupings to create a model of user categories, and count these data across institutions.

We started with the broad questions of 1) who had access to the libraries; 2) under what conditions (such as payment or entry requirements); and 3) what services and collections people had access to. The answers to these questions were not provided consistently and the terminology varied. We collected all references to groups of people allowed (or not allowed) access and sought to classify them. We first classified them according to their proximity to the university: were they internal, adjacent (meaning a community with specific links to the university such as alumni, collaborators, or spouses of academics), or public (meaning general or community members without a specific connection to the university). We identified a set of descriptive categories which successfully captured most of the terms that we observed (see below, Figure 1). A similar approach of collecting references to conditions of access and resources was adopted, followed by categorisation. This was less complex as the requirements to gain access generally were applied to multiple categories in a simpler way. Similarly, the actual resources accessible were more consistent across multiple groups and could be categorised broadly.

A similar process of data analysis was used to identify and enumerate the institutional open-access policies and open-access publishing practices. Generally, this information was easier to locate because terminology was more consistent. We added the data to spreadsheets in order to compare across institutions. Using _constant comparison_ within grounded theory (Glaser and Strauss, 1967, cited in [Weare and Stevenson, 2012](#wea12), p. 118), we compared the content from institutional documents and categorised patterns of library access, open access policies and practices. As we worked through these iterative and comparative processes we learned more about the dimensions of library and open access and were able to identify points of difference on institutional openness.

### Document retrieval

To identify and retrieve documents we developed a user-assisted tool to automate the search, retrieval and downloading of library access or use policies, and open-access policy documents from university Websites. The tool consists of a Jupyter notebook supported by a small library of Python code. It uses the Bing search engine API to run a Web search against the URL for a Website the Global Research Identifier Database - GRID (https://www.grid.ac/) has recorded for a specific university. Generally, for English-language universities if the relevant document exists on the university's Web domain it is found in the top five results. The search results are presented to the user, who can select the appropriate results which are downloaded to a data folder. The code and an example notebook are available at [Github](https://github.com/ccat-lab/doc_search) and Zenodo ([Neylon, 2018](#ney18)).

The tool successfully retrieved documents from twelve universities within the sample set including institutions in China, Hong Kong and Taiwan through their English language Web pages. However, searching and document retrieval from Websites of universities in Mexico and Brazil was less successful because of differing language terminology and limited detail on English language pages.

The automated tool retrieved policy documents, and subsequent manual searching to three or four levels of Web pages identified more detail of user categories, fees charged and privileges for twelve universities. Both automated and manual processes required multiple searches using different keywords, reflecting variations in terminology on Web pages according to local or national custom and translated Websites. Terms used in access policy documents include library access policy, library use policy, library rules, access services and access (to libraries). Details of fees charged and privileges appeared in documents labelled with the terms borrow, borrowers, borrowing, members, membership, external users, privileges, admittance, visitor, visiting and services. Searches of the university Websites using the keywords open access policy, open access and open access funds retrieved Web pages and documents regarding open access policies and support for open access publishing. The [Directory of Open Access Repositories](http://v2.sherpa.ac.uk/opendoar/) was the source for the presence of institutional repositories, as well as university Websites, and the [Web of Science](http://www.webofknowledge.com/) database provided numbers of open access publications for each institution. The searches took place from May to July 2018.

</section>

<section>

## Analysis

The content retrieved from Websites included documents related to library access policies and procedures, open access policies, open access information and publishing. From the library access policy documents, we identified and categorised groups of library users and membership together with their privileges, fees charged for external user access and membership and physical access restrictions to library collections and buildings. Tabulated, these data show the extent of their presence across the sample libraries. The existence or absence of an open access policy, statement of institutional support for open access funding, and an open access institutional repository were considered indicative of institutional support for open access publishing. The two datasets (library access and open access) were plotted with percentages of the institutions' 2016 publications with a status of open access. Finally, we applied a statistical analysis to correlate the association between these three variables.

</section>

<section>

## Results

### User categories

Institutional patrons and external users may be granted membership to access libraries, but multiple categories reflect differing levels of eligibility for privileges and services, joining or membership fees charged and restrictions on physical access. Overall the categories can be grouped in three concentric positions indicating their relationship to the core business of the university: the academic community; individuals and organisations who have prior, established relationships adjacent to the university; and the general, unaffiliated public who have specific research or other information needs (see Figure 1):

*   **Core:** faculty, staff, students of an institution.
*   **Adjacent:** retired, former, ex-staff; spouses; alumni; visiting researchers, scholars; reciprocal scheme borrowers; business and/or industry; societies, non-profit organisations; government departments and agencies.
*   **General public:** community or public members, independent or private researchers, commercial researchers, other university students, school students, one-time or day visitors.

Alumni are referred to often in the literature as unaffiliated, but connections with the institutions they graduated from facilitate library membership (usually for a fee), so in this analysis they are considered to be adjacent.

Massachusetts Institute of Technology Libraries ([2016](#mit16)) discussed a similar model, including global scholars and future students.

<figure class="centre">![ Figure 1: Model of library user categories](../figs/p809fig1.jpg)

<figcaption>Figure 1: Model of library user categories</figcaption>

</figure>

Comparing the extent of user types across the libraries shows core categories are consistent, but the extent of adjacent and public category types varied, often geographically (see Figure 2). For example, the category of spouses and family members of staff is more prevalent in the United States.

<figure class="centre">![Figure 2: Categories of library users from library access policy documents](../figs/p809fig2.png)

<figcaption>Figure 2: Categories of library users from library access policy documents</figcaption>

</figure>

A geographic pattern emerged of more structured, granular membership categories in the academic libraries in Australia, South Africa, the United States and the United Kingdom. For example, the analysis shows a small number of libraries use specific user types such as students from other institutions and school students within the category of public to exclude or restrict access. This delineation may reflect the nature and extent of library research material collections, available study space and equipment, the exclusivity or privacy of institutions, the local population size and geographic location and neighbourhood, or the volume of requests for access from organisations and individuals.

### Dimensions of access

The policy documents and statements retrieved did not provide sufficient detail to understand the nuances of access for different types of external users in practice. Digging deeper into documents and Web pages revealed additional dimensions such as extended categories of access and privileges for the public.

### Privileges

From the access policies, membership and privileges data retrieved we extrapolated the following services libraries may extend to external users:

*   Physical access to library buildings and stacks and collections
*   Read and/or consult materials onsite
*   Membership borrowing privileges for external, unaffiliated users
*   Onsite and remote access to subsets of electronic resources
*   Use of onsite computers, Wi-Fi and printing
*   Assistance for external users
*   Access to government documents (for repository libraries)
*   Interlibrary loan

Figure 3 shows the extent of library privileges for external users across the twelve libraries. Variations to privileges included exceptions for certain user types. Eleven libraries did not allow remote access to electronic resources because of licensing restrictions, and onsite access was limited to selected, free resources. In five libraries e-resource access for alumni onsite was a specified list of free and/or negotiated databases. Interlibrary loan was unavailable to external users in all but one library. Physical building entry was available to external users in all twelve libraries, but with conditions. In three libraries, building access required an ID card; one library specified it was not open to the general public for study purposes only, in other words use of desk space or computers; and in at least two libraries external users could not visit certain collections. One library restricted 3D printing for the public. Overseas university students and the general public could not borrow from one library.

<figure class="centre">![Figure 3: Library privileges for external or unaffiliated users](../figs/p809fig3.png)

<figcaption>Figure 3: Library privileges for external or unaffiliated users</figcaption>

</figure>

In addition to privileges in Figure 3, five libraries required independent, unaffiliated researchers to provide documentary evidence that library material they wish to use is not publicly accessible elsewhere, and/or undergo an interview to demonstrate _legitimate research needs_.

### Fees and physical access

The pattern of privileges available for external users was similar across the twelve libraries, with only a few exceptions for certain user categories. Further analysis identified more granular distinctions relating to openness in terms of fees for access, and physical access restrictions. These led to additional questions:

> Are fees charged to all unaffiliated persons  
> Are any members of the public excluded from access?  
> Do restrictions on physical access to libraries exist?

This in-depth focus on access for the general public highlighted three key points of difference that separated the libraries' positions on openness:

*   access or membership is available for the general public or community
*   membership is provided free of charge to the general public or community
*   physical access to library buildings and/or collections is not restricted

Figure 4 shows how the libraries performed in relation to the above points: the number of libraries offering public access; specific types identified within the category of external users, and if fees were charged for each of these categories. The final bar in the chart represents libraries with physical restrictions in place, such as ID-card requirements.

<figure class="centre">![Figure 4: General public membership categories and fees charged; physical access restrictions](../figs/p809fig4.png)

<figcaption>Figure 4: General public membership categories and fees charged; physical access restrictions</figcaption>

</figure>

Some libraries charged multiple fees by category type. For example, two charged separate fees for both annual membership and for day visitors. Two libraries restricted public membership to state or national residents. School students were excluded specifically through under-eighteen restrictions at four libraries, and access was limited to senior high school students only (for a fee) at another.

Only one library was open to the public with no exceptions, did not charge fees and had no physical access restrictions. Another library had the same conditions but restricted membership to members of public aged over eighteen. These two universities had the most open library access policies, suggesting a commitment to knowledge openness. Both are in medium to large publicly funded research universities.

### Open access publishing

To understand the correlation between institutional positions on open access and library access policies we identified the Website presence of open access policies, institutional repositories and the availability of open access publishing funds for researchers at each of the sample institutions. Overall the presence of open access publishing features is slightly higher than library access features for the twelve universities (see Figure 5).

<figure class="centre">![Figure 5: Open access and library access features](../figs/p809fig5.png)

<figcaption>Figure 5: Open access and library access features</figcaption>

</figure>

Supporting open access, nine of the twelve universities maintained institutional repositories for the deposit of research publications and/or data. Seven universities shared on their Websites documents or statements indicating an institutional policy supporting open access publishing. Two universities without institutional repositories and open access policies were in China, where the open knowledge model focuses more on national than institutional open access repositories ([Montgomery and Ren, 2018](#mon18)). Four universities made available funds to contribute to open access processing fees for publications authored or co-authored by their researchers.

In terms of library access, nine universities offered open access to the libraries for all public users, without exceptions. Six offered unrestricted physical access to library buildings. Only three universities did not charge fees for public users to undertake research in their libraries. These limitations on public access to publicly funded knowledge and research through academic libraries appear to send a counter message to their institutional positions on open access publishing.

Extending the analysis, we contrasted the open access and library access policy features with the percentage of 2016 open access publications indexed by the Web of Science for the sample universities. Web of Science ([www.webofknowledge.com/](http://www.webofknowledge.com/)) does not represent all research publications in all disciplines, but provides a measure of open access publications (green, bronze and gold) across the institutions using open access data from the ImpactStory [Unpaywall](https://unpaywall.org/) database.

Each institution received a point for each of the open access policy features (a policy document, statement on open access publishing funds, presence of an institutional repository), and a point for each of the library access policy features (public access, no fees, no physical restrictions), giving a possible score of 0-3 for each university. See Table 1 for results. Although the scores only report on a limited set of features the data reveal some insights about institutional positions.

<table class="center" style="width:60%;"><caption>  
Table 1: Percentage of open access publishing (2016), open access policy features and open library access features scores by institution. Table entries sorted by percentage of open access publications (rounded to whole numbers).</caption>

<tbody>

<tr>

<th>Institution by country</th>

<th>% Open access  
publications 2016</th>

<th>Open access  
policy features</th>

<th>Library access  
policy features</th>

</tr>

<tr>

<td>US3</td>

<td style="text-align:center">50</td>

<td style="text-align:center">3</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>US2</td>

<td style="text-align:center">49</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>South Africa</td>

<td style="text-align:center">46</td>

<td style="text-align:center">3</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td>UK</td>

<td style="text-align:center">45</td>

<td style="text-align:center">3</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td>US 1</td>

<td style="text-align:center">37</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Taiwan</td>

<td style="text-align:center">33</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>China 1</td>

<td style="text-align:center">31</td>

<td style="text-align:center">2</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td>Australia1</td>

<td style="text-align:center">30</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td>Hong Kong</td>

<td style="text-align:center">29</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>Australia2</td>

<td style="text-align:center">26</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td>Singapore</td>

<td style="text-align:center">19</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td>China 2</td>

<td style="text-align:center">13</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2</td>

</tr>

</tbody>

</table>

The libraries in the sample are identified by country in Table 1, with two in Australia, two in China and three in the United States. Nine libraries scored similarly on the open access policy measure, with the Taiwan, China 2 and Australia 2 libraries scoring lower. However, three with the highest percentage of open access publications and open access policy (US 2, South Africa, UK) scored lowest on the library access measure. Overall, library access policy scores are lower than those for open access policy, with only one library reaching a score of three for library access. Two libraries with a high percentage of open access publications (45%, 46%) and a score of three for open access policy features have a zero score for openness in terms of library access features. As in Figure 5 above, this suggests a lack of correlation between open access publishing and library access.

Viewing the correlation between the three data elements for each library provides further understanding of the interaction between open access publishing, open access policies and library access policies (see Figure 6).

<figure class="centre">![Figure 6: Open access and library access features ](../figs/p809fig6.png)

<figcaption>  
Figure 6: Open access and library access features correlated with percentage of 2016 open access publications for each sample institution. Source of open access publications data: Web of Science content indexed to 03 August 2018.</figcaption>

</figure>

Figure 6 plots the scores for the number of open access policy features and the number of open library access features (vertical axis) against the percentage of 2016 open access publications (horizontal axis) for each of the twelve sample libraries. The limited correlation between the three measures, in particular the lower scoring for library access policies suggests the intentions expressed by the two policy actions have not been applied in similar directions, in other words to reach the same ends.

A statistical analysis using Spearman's rank correlation explored the predictive association between each of the variables of open access policy and library access policy scores and the percentage of open access publications for each institution. Open access policy has a positive relationship with open access publications (ρ= 0.74) whereas the library access policies show a negative correlation with the number of open access publications (ρ= -0.33). Although the dataset is small, the analysis confirms open access policies are more predictive of open access publications than library access policies. This indicates that open access policies do have a direct effect on the narrow aspects of public access provided through online availability of formal publications, but are not necessarily associated (within the universities in this sample) with delivering on a broader commitment to public access to knowledge.

## Limitations and future research

This preliminary study has investigated the library access policies of twelve academic libraries in eight countries as a potential indication of institutional openness to knowledge, and correlated the positions with open access policies and percentages of open access publications. Through investigating policies and documents, the study also reviewed the informational messages library Websites portray to potential unaffiliated public or community users in terms of access to resources. However, this is only a small sample study and further research on a larger scale is needed to expand the premise of academic library access as an indicator of openness.

The reasons libraries choose to implement access restrictions can only be surmised. The study does not intend to judge the libraries' access and membership policies, but to point out the impact, intentional or not, policy restrictions can have on access to knowledge and institutional positions on openness. Similarly, reasons for universities adopting measures in support of open access publishing are complex.

Extending the study to include more institutions will enhance the results discussed in this article. Two further options are possible, building on the outcomes of this study. The first is to retrieve on a larger scale relevant documents relating to public access to academic libraries and open access publishing from more countries using a multilingual lexicon of terminology (in development and available in Zenodo) ([Lexicon Contributors, 2018](#lex18)). Reviewing this larger set of documents using textual analysis to identify the presence and location of relevant keywords will enable further and deeper categorisation. The second option is to invite academic libraries to participate in a qualitative online survey around the key messages identified in the discussion and to supplement this analysis. This would include questions such as:

1.  Does your library provide access to materials for the general public and community members?
2.  Are any categories of the public specifically excluded from access?
3.  Does your library charge fees for general public and/or community access?
4.  Does your library have physical access restrictions to buildings and/or specific collections for the unaffiliated, impacting on members of the public who are not registered with the library?

Further research to extend the analysis by including a larger number of institutions will enhance understanding of library access policies in relation to institutional openness.

</section>

<section>

## Discussion

This study aimed to investigate whether and how university library access policies reflect institutional commitments to openness through analysis of library access or use policies on their Websites, and the correlations with institutional open access policies and publications. All twelve Websites included library access policy information, but the extent and detail varied. Some expressed separate policies in terms of brief, broad overviews resembling mission statements. Others embodied policy in a short sentence that preceded information for potential visitors. Generally, libraries indicated their priority was to support the teaching, learning and research needs of faculty, staff and students of the institutions. Access and services for unaffiliated, external visitors and public or community members are secondary and often with restrictions. Only two of the twelve libraries studied provided full open library access to the public, and one of these specified an age restriction of over eighteen.

Requirements of some libraries for unaffiliated researchers to demonstrate a unique research need to be met by the library can present an intellectual barrier through an assumption or expectation of external users' equal intellectual and social access: understanding the structuring of information and associated social norms as well as accessing it physically. '_To achieve physical access, the individual user has to know that the information exists, where it can be found, and how to navigate the institutional structures to reach it_' ([Burnett, _et al._ 2008](#bur08), p. 57).

All libraries specify a range of external users who can apply for membership and join the library, with a scale of annual, monthly, weekly or daily fees. Some libraries have service level agreements extending privileges to other institutions. Changes to access policies and user categories develop in response to particular problems or situations. Campus unrest, theft and damage to material, or security incidents may lead to physical building access restrictions ([Ajayi, 2007](#aja07); [Leuzinger and Marnane, 2004](#leu04)). The security requirement for ID-cards to enter library buildings at some universities was often difficult to locate on Web pages, sometimes buried in a news item if recently implemented. Libraries have valid reasons for enforcing physical restrictions, and most offer options for users in acceptable categories to obtain library or ID-cards enabling entry to the library. High demand for information may lead to the exclusion of the general public from particular collections (for example, law, health), and in peak use periods such as during exams. Influxes of certain population groups, for instance, high school students may result in their exclusion. However, lack of clear notification and policy detail on public Websites with links to access options, inconsistencies in terminology and location of information on Websites can be a deterrent to potential external users with research needs.

Library privileges and access for the general public with no institutional affiliation detailed in policies and practices emerged as a key differential factor and indicator of openness to knowledge within the academic libraries. Accessibility of research is growing through open access scholarly institutional and digital repositories, but this represents only a small proportion of the research output and material physically held in libraries (books, archives, manuscripts, print journals). Limitations on public access to libraries bound up in layered membership and visitor policies can restrict usage of non-open access and older material in which there may still be research interest. For example, an analysis of Sci-Hub readership found a small usage of '_long tail_' scholarly publications dating back to the 1600s ([Greshake, 2017](#gre17)).

### Correlation with open access positions

Globally many universities are embracing the principles and adopting policies of open access to research output, contributing to open scholarship for the wider community. Requirements from governments and funders are now the key driving forces for the implementation of such policies. Within this landscape, a large amount of legacy, pre-open access research output held in academic libraries may be restricted for the public through multi-layered library access policies. As this sample study has found, library access policies do not always correlate with institutional positions on open access to research, expressed through policies, institutional repositories and the number of scholarly open access publications.

Academic libraries today are challenged to maintain public access within the changing landscape of digital, commercial publication and demands for open access to research. The limitations that library access policies can place on openness to knowledge may not be intentional, but they are in tension with institutional statements on knowledge access reflected in open access publication policies and practices. Further research on a larger scale will inform understanding of these divergent positions.

</section>

<section>

## Conclusion

In an era of increasing open access to publicly funded research there appears to be little consideration of how academic library access policies may affect the delivery of institutional aspirations for open access to knowledge in general. Library preferences for purchasing publications in electronic formats with associated licenses limit access and usage to core library users, and decrease the availability of research to external, unaffiliated users, as Courtney ([2003](#cou03)) predicted at the turn of this century. The need for physical access to knowledge through libraries remains, and it is important to expand the focus of open access to beyond the digital.

This study has investigated how academic library access policies reflect openness to the scholarly research and knowledge libraries collect and facilitate access to. The sample libraries in this study emphasised their primary clientele is core users - faculty, students, staff. Some libraries claimed to be open to the wider public as secondary users but in practice the multiple levels of hierarchical access and privileges identified in this study limit the accessibility of knowledge within these libraries. Most affected is the general, unaffiliated public. This is in sharp contrast to the intention behind growing global commitments to open access for formally published digital information and knowledge. Limiting public access to libraries is an apparent outcome of resource challenges, high demand for libraries' extensive and specialised collections, and publisher license restrictions. That public demand for library access exists, as demonstrated by the restrictions identified in this study, is testament to unfulfilled needs for research knowledge from the wider public external to academic and research institutions. While the current focus of governments, funders and institutions is to open access to digital research publications and data, we need to think more broadly about maintaining openness to knowledge as a whole. As universities compete for reputation and students in a global market, rethinking and extending policies of open access to publicly funded knowledge held in libraries will be beneficial.

</section>

<section>

## Acknowledgements

Thanks to Dr. Gaby Haddow (Curtin University) and to the two _Information Research_ reviewers for reviewing the manuscript and for their constructive suggestions.

</section>

<section>

## About the authors

**Katie Wilson** is a Research Fellow in the Centre for Culture and Technology at Curtin University (Perth, Australia). Email: [katie.wilson@curtin.edu.au](mailto:katie.wilson@curtin.edu.au)  
**Cameron Neylon** is Professor of Research Communications in the Centre for Culture and Technology at Curtin University (Perth, Australia). Email: [cameron.neylon@curtin.edu.au](mailto:cameron.neylon@curtin.edu.au)  
**Lucy Montgomery** is Associate Professor in the Centre for Culture and Technology at Curtin University (Perth, Australia). Email: [lucy.montgomery@curtin.edu.au](mailto:lucy.montgomery@curtin.edu.au)  
**Chun-Kai (Karl) Huang** is a senior research fellow at the Centre for Culture and Technology at Curtin University (Perth, Australia). Email: [karl.huang@curtin.edu.au](mailto:karl.huang@curtin.edu.au)

</section>

<section class="refs">

## References

*   Ajayi, N. A. (2007). Closed-access policy as a solution to library crime: perception and view of students. _Libri, 53_(3), 221–225.
*   Barsun, R. (2003). Library web pages and policies toward 'outsiders'. _Public Services Quarterly, 1_(4), 11–27\.
*   Björk, B.-C. (2017). Gold, green, and black open access. _Learned Publishing, 30_(2), 173–175\.
*   Bowen, G. A. (2009). Document analysis as a qualitative research method. _Qualitative Research Journal, 9_(2), 27–40\.
*   Budapest Open Access Initiative. (2002). [_Read the Budapest Open Access Initiative._](http://www.webcitation.org/7662G2Lua) Retrieved from http://www.budapestopenaccessinitiative.org/read (Archived by WebCite® at http://www.webcitation.org/7662G2Lua)
*   Burclaff, N. & Britz, J. (2011). [Alumni access policies in public university libraries.](http://www.webcitation.org/73B24k80D) _Inkanyiso, Journal of Humanities and Social Sciences, 3_(1), 1–12\. Retrieved from https://www.ajol.info/index.php/ijhss/article/view/69496 (Archived by WebCite® at http://www.webcitation.org/73B24k80D)
*   Burnett, G., Jaeger, P. T. & Thompson, K. M. (2008). Normative behavior and information: the social aspects of information access. _Library & Information Science Research, 30_(1), 56–66.
*   Courtney, N. (2001). Barbarians at the gates: a half-century of unaffiliated users in academic libraries. _Journal of Academic Librarianship, 27_(6), 473–480\.
*   Courtney, N. (2003). Unaffiliated users' access to academic libraries: a survey. _Journal of Academic Librarianship, 29_(1), 3–7.
*   Dempsey, L. (2016). Library collections in the life of the user: two directions. _LIBER Quarterly, 26_(4), 338–359\.
*   Dole, W.V. & Hill, J.B. (2011). Community users in North American academic libraries. _New Library World, 112_(3/4), 141–149.
*   Dong, E.X. & Zou, T.J. (2009). Library consortia in China. _LIBRES: Library and Information Science Research, 19_(1), 1–10.
*   Dunne, S. (2009). [Local community engagement: extending the role of the academic library to meet the university's mission: Project report.](http://www.webcitation.org/73B1pTHlY) [Dublin], Ireland: ANLTC. Retrieved from http://doras.dcu.ie/20242/ (Archived by WebCite® at http://www.webcitation.org/73B1pTHlY)
*   Duy, J. C. & Larivière, V. (2013). An analysis of direct reciprocal borrowing among Québec university libraries. _Journal of Access Services 10_(2) 102–119.
*   Gadd, E. (2017). UK university policy approaches towards the copyright ownership of scholarly works and the future of open access. _Aslib Journal of Information Management, 69_(1), 95–114\.
*   Greshake, B. (2017). [Looking into Pandora's box: the content of Sci-Hub and its usage.](http://www.webcitation.org/73B1e4wkH) _F1000Research, 6,_ 541\. Retrieved from https://f1000research.com/articles/6-541/v1 (Archived by WebCite® at http://www.webcitation.org/73B1e4wkH)
*   Hang, T. L. J. (2013). Community engagement: building bridges between university and community by academic libraries in the 21st century. _Libri, 63_(3), 220–231\.
*   International Federation of Library Associations and Institutions (IFLA). (2017). [_Development and access to information 2017_.](http://www.webcitation.org/74En2iDFd) Retrieved from https://da2i.ifla.org/sites/da2i.ifla.org/files/uploads/docs/da2i-2017-full-report.pdf (Archived by WebCite® at http://www.webcitation.org/74En2iDFd)
*   International Federation of Library Associations and Institutions (IFLA). (2018). [_Libraries, development and the United Nations 2030 agenda_.](http://www.webcitation.org/74EnwNBdd) Retrieved from https://www.ifla.org/libraries-development (Archived by WebCite® at http://www.webcitation.org/74EnwNBdd)
*   Joseph, H. (2008). A question of access: evolving policies and practices. _Journal of Library Administration, 48_(1), 95–106\.
*   Karaganis, J. (Ed.). (2018). [_Shadow libraries: access to knowledge in global higher education_](https://mitpress.mit.edu/books/shadow-libraries). Cambridge, MA: The MIT Press, International Development Research Centre. Retrieved from https://mitpress.mit.edu/books/shadow-libraries
*   Lau, J., Tammaro, A.M. & Bothma, T. (2012). _Libraries driving access to knowledge_. Berlin, Germany: De Gruyter Saur
*   Lehto, A., Toivonen, L. & Iivonen, M. (2012). University library premises: the evaluation of customer satisfaction and usage. In J. Lau, A.M. Tammaro, and T. Bothma, _Libraries driving access to knowledge_. Berlin, Boston, MA: De Gruyter Saur.
*   Leuzinger, P. & Marnane, B. (2004). [_'But you're restricting access to information': the ethical and cultural dimensions of installing access gates in the UTS City Campus library (Blake Library)_](http://www.webcitation.org/73B12HEJH). Paper presented at the ALIA Biennial Conference, Queensland, Australia. Retrieved from https://opus.lib.uts.edu.au/bitstream/10453/2674/1/2004000638.pdf (Archived by WebCite® at http://www.webcitation.org/73B12HEJH)
*   Lexicon Contributors. (2018). [Scholarly communications multilingual lexicon v0.2.](http://www.webcitation.org/74j5G1T2x) Retrieved from https://doi.org/10.5281/zenodo.1954813 (Archived by WebCite® at http://www.webcitation.org/74j5G1T2x)
*   Massachusetts Institute of Technology. (2016). [_Institute-wide task force on the future of libraries: preliminary report._](http://www.webcitation.org/73B0rJ3vN)) Cambridge, MA: Massachusetts Institute of Technology. Retrieved from https://future-of-libraries.mit.edu. (Archived by WebCite® at http://www.webcitation.org/73B0rJ3vN)
*   Mehtonen, P. (2016). [The library as a multidimensional space in the digital age.](http://www.webcitation.org/73B0hrh5m) _Information Research, 21_(1), paper memo 6\. Retrieved from http://www.informationr.net/ir/21-1/memo/memo6.html#.W2uubY4c7sA (Archived by WebCite® at http://www.webcitation.org/73B0hrh5m)
*   Montgomery, L. & Ren, X. (2018). [Understanding open knowledge in China: a Chinese approach to openness?](http://www.webcitation.org/73B02azZc) _Cultural Science Journal, 10_(1). Retrieved from https://culturalscience.org/articles/10.5334/csci.106/ (Archived by WebCite® at http://www.webcitation.org/73B02azZc)
*   Neylon, C. (2018). [_ccat-lab/doc_search: initial release for article submission (Version v0.8)_.](http://www.webcitation.org/74J6iE2EZ) Zenodo. Retrieved from http://doi.org/10.5281/zenodo.1438875 (Archived by WebCite® at http://www.webcitation.org/74J6iE2EZ)
*   Research Information Network. (2009). [_Overcoming barriers: research information content_.](http://www.webcitation.org/73B0aE6RB) Retrieved from http://www.rin.ac.uk/system/files/attachments/overcoming_barriers_report.pdf (Archived by WebCite® at http://www.webcitation.org/73B0aE6RB)
*   Roth-Katz, E. (2012). Access and availability: a study of use policies on art museum library websites. _Art Documentation: Bulletin of the Art Libraries Society of North America, 31_(1), 123–140.
*   University of California Libraries. (2018). [_Pathways to open access_.](http://www.webcitation.org/71np5fqFO) Retrieved from https://libraries.universityofcalifornia.edu/groups/files/about/docs/UC-Libraries-Pathways%20to%20OA-Report.pdf (Archived by WebCite® at http://www.webcitation.org/71np5fqFO)
*   Weare, W.H. & Stevenson, M. (2012). Circulation policies for external users: a comparative study of public urban research institutions. _Journal of Access Services, 9_(3), 111–133\.
*   Whitehead, M. L., Gutierrez, L. & Miller, M. (2014). Circulation policies in academic medical libraries: a comparative study of allocation strategies, demographic analysis, service offerings, and implications for practice. _Journal of Access Services, 11_(4), 239–254

</section>