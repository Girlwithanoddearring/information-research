<!doctype html>
<html lang="en">
<head>
<title>Role-specific behaviour patterns in collaborative information seeking</title>
<meta charset="utf-8" />
<link href="../../IRstyle4.css" rel="stylesheet" media="screen" title="serif" />

<!--Enter appropriate data in the content fields-->
<meta name="dcterms.title" content="Role-specific behaviour patterns in collaborative information seeking" />

<meta name="citation_author" content="Elbeshausen, Stefanie" />
<meta name="citation_author" content="Mandl, Thomas" />
<meta name="citation_author" content="Womser-Hacker, Christa" />

<meta name="dcterms.subject" content="Collaborative Information Seeking is a common practice for solving complex tasks. " />
<meta name="description" content="Collaborative Information Seeking is a common practice for solving complex tasks. In distant collaboration scenarios, system support is necessary to carry out the process efficiently. The paper presents research on roles for distant collaborative search to contribute to the efficiency of the teams and raise group awareness. This is done via two field studies and  by applying a mixed methods approach. The analysis shows some noticeable indications for search actions specific for certain groups of participants. The integrated interpretation of the data led to the development of five different role patterns: Facilitator, Observer/Editor, Implementer, Pathfinder and Compiler. The findings can help to support distant collaboration by making it more efficient and effective and raise users’ satisfaction. Further studies are planned to test the findings in quantitative approaches." />
<meta name="keywords" content="collaboration, information seeking behaviour, qualitative research," />

<!--leave the following to be completed by the Editor-->
<meta name="robots" content="all" />
<meta name="dcterms.publisher" content="University of Borås" />
<meta name="dcterms.type" content="text" />
<meta name="dcterms.identifier" content="ISSN-1368-1613" />
<meta name="dcterms.identifier" content="http://InformationR.net/ir/24-1/isic2018/isic1831.html" />
<meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/24-1/infres241.html" />
<meta name="dcterms.format" content="text/html" />  <meta name="dc.language" content="en" />
<meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />
<meta  name="dcterms.issued" content="2019-03-15" />
<meta name="geo.placename" content="global" />

</head>

<body>

    <header>
    <img height="45" alt="header" src="../../mini_logo2.gif"  width="336" /><br />
    <span style="font-size: medium; font-variant: small-caps; font-weight: bold;">published quarterly by the University of Bor&aring;s, Sweden<br /><br />vol. 24  no. 1, March, 2019</span>
    <br /><br />
    <div class="button">
    <ul>
    <li><a href="isic2018.html">Contents</a> |  </li>
    <li><a href="../../iraindex.html">Author index</a> |  </li>
    <li><a href="../../irsindex.html">Subject index</a> |  </li>
    <li><a href="../../search.html">Search</a> |  </li>
    <li><a href="../../index.html">Home</a> </li>
    </ul>
    </div>
    <hr />
    Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 2.
    <hr />
    </header>

    <article>
        <h1>Role-specific behaviour patterns in collaborative information seeking
        </h1>  <br />

        <h2 class="author"><a href="#author">Stefanie Elbeshausen</a>, <a href="#author">Thomas Mandl</a> and <a href="#author">Christa Womser-Hacker</a>
        </h2>

        <br />

        <blockquote>
        <strong>Introduction</strong>.Collaborative Information Seeking is a common practice for solving complex tasks. In distant collaboration scenarios, system support is necessary to carry out the process efficiently.<br />
        <strong>Methods</strong>. The paper presents research on roles for distant collaborative search to contribute to the efficiency of the teams and raise group awareness. This is done via two field studies and  by applying a mixed methods approach.<br />
        <strong>Analysis</strong>. The analysis shows some noticeable indications for search actions specific for certain groups of participants.<br />
        <strong>Results</strong>. The integrated interpretation of the data led to the development of five different role patterns: Facilitator, Observer/Editor, Implementer, Pathfinder and Compiler.<br />
        <strong>Conclusion</strong>The findings can help to support distant collaboration by making it more efficient and effective and raise users’ satisfaction. Further studies are planned to test the findings in quantitative approaches.<br />
        </blockquote>

        <br />

        <section>
            <h2>Introduction</h2>
            <p>Most of us already encountered situations where we had the need to search with others. The beginning of a joint research project, for example, is often marked by a phase of collaborative search to develop a common understanding of the field. As long as the participants have a shared goal and symmetric benefits this form of mutual search is called collaborative information seeking (<a href="#sha10a">Shah, 2010</a>) or short: CIS. The following article describes our research with the goal to enhance the user&rsquo;s efficiency and satisfaction by developing roles for CIS.</p>

            <h3>Background and related work</h3>
            <p>CIS can be defined as the <em>&lsquo;activities that a group or team of people undertakes to identify and resolve a shared information need&rsquo; </em>(<a href="#pol03">Poltrock <em>et al.</em>, 2003</a>, p. 239)<em>. </em>Especially when tasks are too complex to be solved by an individual, it is beneficial to collaborate (<a href="#sha10a">Shah, 2010</a>). Several research projects addressed collaborative search in science, e.g. Hyldeg&aring;rd (<a href="#hyl06">2006</a>), Haseki, Shah and Gonzales-Ibanez (<a href="#has12">2012</a>), Vick, Nagano and Popadiuk (<a href="#vic15">2015</a>) or Leeder and Shah (<a href="#lee16">2016</a>).</p>
            <p>In distant collaboration, the use of systems which help to carry out the process efficiently is necessary. One relevant aspect in the design of those systems is awareness. In the given context especially group awareness is relevant, which refers to <em>&lsquo;the knowledge and perception of behavioral, cognitive, and social context information on a group or its members&rsquo; </em>(<a href="#bod11">Bodemer and Dehler, 2011</a>, p. 1)<em>.</em></p>
            <p>The concept of awareness was investigated in a variety of research studies, e.g. Gutwin and Greenberg (<a href="#gut02">2002</a>), Kirschner, Kreijns, Phielix and Fransen (<a href="#kir15">2015</a>), Miller and Hadwin (<a href="#mil15">2015</a>). Shah and Marchionini (<a href="#sha10b">2010</a>) researched the relevance of awareness for CIS, based on the tool Coagmento (<a href="#sha10a">Shah, 2010</a>). Despite the fact that a variety of systems exist to support CIS they are, according to Morris (<a href="#mor13">2013</a>), underused. One reason could be that relevant aspects of awareness are missing.</p>

            <h3>Roles of users</h3>
            <p>Heinstr&ouml;m (<a href="#hei02">2002</a>) researched the influence of personality on roles for individual Information Seeking (IS). In the context of collaborative search, Pickens, Golovchinsky, Shah, Qvarfordt and Back (<a href="#pic08">2008</a>) introduced roles that either place emphasis on expertise or on the way to perform a search. Soulier, Shah and Tamine (<a href="#sou14">2014</a>) used a so called <em>user-driven system-mediated approach</em> for mining those roles and improving search performance. In an additional study, Tamine and Soulier (<a href="#tam15">2015</a>) researched the impact of prior assigned and fixed roles in contrast to spontaneous roles, which emerge during the search process. Further they concluded that roles might shift during the search sessions. Those roles are suitable for the context of individual search or Collaborative Information Retrieval. In (long term) CIS, where the search is usually embedded in a more comprehensive work-task (<a href="#han16">Hansen and Wid&egrave;n, 2016</a>) roles are needed which include search performance as well as team behaviour.</p>
            <p>While little research was done on roles for collaborative search, other disciplines dealt with this subject, e.g. in the context of collaborative learning (e.g. <a href="#kol06">Kollar, Fischer and Hesse, 2006</a>; <a href="#wei05">Weinberger, Ertl, Fischer and Mandl, 2005</a>). Strijbos, Martens, Jochems and Broers (<a href="#str04">2004</a>) researched the effect of functional roles on group efficiency in small groups and came to the result, that those roles can increase students&rsquo; awareness of collaboration. A frequently used role model which was evaluated in several studies (e.g. <a href="#ari07">Aritzeta, Swailes and Senior, 2007</a>) was developed by Belbin (<a href="#bel10a">2010a</a>; <a href="#bel10b">2010b</a>). He initially developed eight team roles, which are mapped to three categories. <em>Thinking roles (plant </em>and <em>monitor evaluator) </em>are mainly focussed on knowledge specific processes. <em>Social roles (co-ordinator</em>, <em>teamworker </em>and <em>resource investigator) </em>are interested in social aspects. <em>Action roles </em>(<em>shaper</em>, <em>implementer </em>and <em>completer-finisher) </em>push action during collaboration. To be able to measure the role a person occupies, Belbin developed the <em>Team Role Self Perception Inventory </em>(TRSPI).</p>
        </section>

        <section>
            <h2>Methodological approach and experimental process</h2>
            <p>Based on the state of the art, the following research question was developed:</p>
            <p><em>RQ: Which role-specific behaviour patterns can be examined in the context of CIS?</em></p>
            <p>In order to consider as much of the users&rsquo; behaviour as possible, a field study based on a mixed methods approach was selected. It incorporates the recording of the interaction with a search system, a questionnaire and group interviews. The questionnaire is mainly based on the TRSPI-8 instrument (<a href="#bel10a">Belbin 2010a</a>; <a href="#bel10b">2010b</a>). For analysing the search process, the system SearchTeam was used (the tool SearchTeam is available online at https://searchteam.com). In SearchTeam each team owns a SearchSpace to save results, e.g. in form of links, to post comments and to communicate via an integrated chat. It is also possible to generate files for saving information, e.g. according to sub-tasks. The interviews were set up as semi-structured group interviews (<a href="#prz14">Przyborski and Wohlrab-Sahr, 2014</a>, p.126f).</p>
            <p>The experiment was carried out in two studies with n=34 students as participants. The teams with two to four members had to complete complex tasks over a period of two weeks. Both studies took place in Information Science classes while participation in the experiment was on a voluntary basis. After finishing the task and filling out the questionnaire, the teams were asked to attend the interviews.</p>
        </section>

        <section>
            <h2>Analysis</h2>
            <p>The following subsections provide the data analysis for each instrument, followed by an integrated interpretation.</p>

            <h3>Analysis of the TRSPI</h3>
            <p>The analysis of data conducted with the TRSPI follows a standardised procedure. The participants have to distribute points to statements which represent their behaviour best. The highest score represents the primary team role. In rare cases, one person scores the same for two patterns and adopts two primary roles. Due to the fact that the surveyed teams consist of small groups the analysis is based on the categories <em>social roles (SR), thinking roles (TR) </em>and <em>action roles (AR)</em>. In table 1, an overview of the allocation of team role categories in the study, the percentage in relation to the number of test participant and to the number of team roles is given.</p>
            <table class="center" style="width:40%;">
            <caption><br />Table 1: Proportion of role categories identified in the study (Total), in relation to the number of test participants (% TP) and to the number of detected team roles (% TR). One participant scored the same for two role categories. Accordingly, the number of team roles in the study is raised by one compared to the total amount of test participants. </caption>
            <tbody>
            <tr><th>Role Category</th><th>Total</th><th>% TP (n=34)</th><th>% TR (n=35)</th>
            </tr><tr><td>Social role</td><td style='text-align:left;'>10</td><td style='text-align:left;'>29.4</td><td style='text-align:left;'>28.6</td>
            </tr><tr><td>Thinking role</td><td style='text-align:left;'>5</td><td style='text-align:left;'>14.7</td><td style='text-align:left;'>14.3</td>
            </tr><tr><td>Action role</td><td style='text-align:left;'>20</td><td style='text-align:left;'>58.8</td><td style='text-align:left;'>57.1</td>
            </tr></tbody>
            </table>

            <h3>Analysis of the system interaction</h3>
            <p>The analysis of the system interaction was carried out by a quantitative approach and includes all data the teams produced during their interaction with SearchTeam. For each person we counted the amount of generated files and chat messages, the number of posted links as well as the amount of comments. These scores were allocated to the team roles and to the categories SR, AR and TR. An overview is given in table 2.</p>
            <table class="center" style="width:40%;">
            <caption><br />Table 2: Overview of the interaction with the search system used in the first study (Study 1), the second study (Study 2) and for both conditions (Total). Given is the total amount (n) and the mean average (x̄), as well as the mean average system interaction according to the categories Social Roles (SR), Thinking Roles (TR), and Action Roles (AR). </caption>
            <tbody>
            <tr><th></th><th>Links</th><th>Comments</th><th>Files</th><th>Chat Messages</th>
            </tr><tr><td>Study 1 n</td><td style='text-align:left;'>191</td><td style='text-align:left;'>120</td><td style='text-align:left;'>28</td><td style='text-align:left;'>416</td>
            </tr><tr><td>Study 1 x̄</td><td style='text-align:left;'>10.5</td><td style='text-align:left;'>6.3</td><td style='text-align:left;'>1.5</td><td style='text-align:left;'>21.9</td>
            </tr><tr><td>Study 2 n</td><td style='text-align:left;'>155</td><td style='text-align:left;'>73</td><td style='text-align:left;'>22</td><td style='text-align:left;'>54</td>
            </tr><tr><td>Study 2 x̄</td><td style='text-align:left;'>10.3</td><td style='text-align:left;'>4.9</td><td style='text-align:left;'>1.9</td><td style='text-align:left;'>3.6</td>
            </tr><tr><td>Total n</td><td style='text-align:left;'>346</td><td style='text-align:left;'>193</td><td style='text-align:left;'>50</td><td style='text-align:left;'>470</td>
            </tr><tr><td>Total x̄</td><td style='text-align:left;'>10.2</td><td style='text-align:left;'>5.7</td><td style='text-align:left;'>1.5</td><td style='text-align:left;'>13.8</td>
            </tr><tr><td>SR x̄</td><td style='text-align:left;'>7.7</td><td style='text-align:left;'>5.6</td><td style='text-align:left;'>1.4</td><td style='text-align:left;'>3.1</td>
            </tr><tr><td>TR x̄</td><td style='text-align:left;'>8.4</td><td style='text-align:left;'>9.2</td><td style='text-align:left;'>0.2</td><td style='text-align:left;'>8.0</td>
            </tr><tr><td>AR x̄</td><td style='text-align:left;'>12.5</td><td style='text-align:left;'>6.1</td><td style='text-align:left;'>1.8</td><td style='text-align:left;'>20.0</td>
            </tr></tbody>
            </table>

            <h3>Analysis of the group interviews</h3>
            <p>The group interviews were analysed based on the qualitative content analysis (<a href="#may15">Mayring, 2015</a>) regarding collaboration specific categories. One category targeted the issue of roles which were associated with a leading or organizing position. All teams with more than two members, except one team in the first study, reported that one person was responsible for the process. In most of the cases this individual also established the SearchSpace.</p>

            <h3>Interpretation and discussion of “social” related roles in the study</h3>
            <p>Ten participants (29.4%) fall in the category <em>social roles </em>(SR). By looking at the system interactions the notion emerges that those roles do not show striking activities regarding the average number of interactions (table 2). The low amount of chat messages for SR (x̄ = 3.1) is surprising since those role category is mainly associated with social interaction. The mere system interaction could not provide specific patterns for SR but the analysis of the interviews shows some interesting hints. Noticeable are repeated statements showing support of the team. For instance, a test participant in the second study explained to her teammate a certain tool used during the process. In one group a conflict occurred. The individual associated with a SR, highlighted that it was necessary to talk about the situation because it helped to know which attitude towards collaboration the team members had.</p>
            <p>In sum, SR primarily have a supporting function and mainly take the tasks which are still open, e.g. adding links when further results are necessary. The interview data shows that SR have the tendency to smooth conflicts in the team. Accordingly, we choose the term <em>facilitator </em>for this role because it includes both, a supporting as well as a moderating function.</p>

            <h3>Interpretation and discussion of “thinking” related roles in the study</h3>
            <p>Five individuals (14.7%) with a TR could be identified. It is striking that TR never established the SearchSpace or were associated with a leading or organising position in the team. Just one file (x̄ = 0.2) was generated by a TR.</p>
            <p>Noticeable is the number of comments for this role category (x̄ = 9.2) compared to the total amount (x̄ = 6.4). Those comments show that TR review the documents linked by team members and provide statements in regards of their usefulness. Furthermore, the interviews show hints on analysing team specific aspects. A participant in the first study who is associated with a TR mentions that she knows where her teammates have their strengths and weaknesses and accordingly, who is able to solve which sub-task. In the second study, another test subject who takes a TR says that she has the ambition to analyse her teammates&rsquo; approach of working and to adapt to it.</p>
            <p>Based on the mean average system interaction it could be assumed that TR are simply less engaged. But the noticeable number of comments proves that these roles are committed to collaboration. Even if participants with a TR produced fewer results they are valuable for the team because they observe, edit and analyse the results and the team itself. Accordingly, we choose the name <em>observer/editor </em>for this role pattern.</p>

            <h3>Interpretation and discussion of “action” related roles in the study</h3>
            <p>20 participants (58.8%) are associated with an AR. According to differences and similarities in their behaviour, we identified three different role patterns for AR.</p>
            <p>The first one, <em>pathfinder</em>, takes <em>implementer </em>or <em>shaper </em>as primary role. Seven of the eight persons associated with organising or leading the process are related to this role profile. Six of these participants established the SearchSpace for their group. <em>Pathfinder</em>s created 19 (63.3%) of the 30 files in the sample what is, according to the interview data, associated with sub-task organisation. They mainly show action during the beginning of the process.</p>
            <p>The second one, <em>compiler, </em>is based on Belbin&rsquo;s <em>completer-finisher</em>. In contrast to <em>pathfinders</em>, they especially take action during the last process steps. Both <em>compilers </em>we could identify took care for finishing the task on time by handing in the final result. Furthermore, two groups mentioned that the specific members merged the results found by the team and compiled them to a presentation. One <em>compiler </em>said in the interview that she likes to be responsible for organisation and prefers to be the one who takes care for deadlines.</p>
            <p>By analysing the remaining twelve AR, we decided to leave out the ones which reported that they had no organising person. Seven participants in this category were left, whereof six occupied <em>implementer </em>as primary team role. Accordingly, we named this role <em>implementer</em>. <em>Implementer </em>neither established the SearchSpaces, nor handed in the presentation. They do not show noticeable high or low interaction values (table 2) but still take part in all aspects of collaboration and support the search process.</p>
        </section>

        <section>
            <h2>Conclusions</h2>
            <p>We presented a mixed-method approach for analysing collaborative search behaviour with the goal to develop roles. In the given context, we could identify five roles: <em>facilitator</em>, <em>observer/editor</em>, <em>pathfinder</em>, <em>compiler </em>and <em>implementer</em>. Those roles execute different steps and show specific behaviours during collaboration. Findings on <em>pathfinder </em>and <em>compiler </em>indicate that a certain role pattern is not necessarily applied during the whole process: while <em>pathfinder </em>takes a leading position at the beginning of the process, <em>compiler </em>takes it at the end.</p>
            <p>It is necessary to note that the sample for this study was relatively small (n=34). Therefore, we plan to test the statistical evidence with a larger sample in a quantitative study. Further, the roles are limited to the domain of science and the context of student&rsquo;s collaboration. In future studies we plan to survey the applicability to other domains and collaboration contexts.</p>
            <p>Furthermore, it would be interesting to research the influence of the roles on effectiveness, efficiency and satisfaction during CIS. For this purpose, it would be necessary to develop an instrument which allows analysing the roles prior to collaboration and making them transparent to the team.</p>
        </section>

        <section>
            <h2>Acknowledgements</h2>
            <p>We thank the students who spent their time and effort for participation in the experiments and their contribution to science. They helped us to learn a lot and it was a very interesting – and sometimes highly entertaining and enjoyable – process!</p>
        </section>

        <section>
            <h2 id="author">About the authors</h2>
            <p><strong>Stefanie Elbeshausen</strong> is a Postdoctoral Researcher in the Department for Information Science and Natural Language Processing at the University of Hildesheim, Universitaetsplatz 1, 31141 Hildesheim, Germany. She received a doctorate degree from the University of Hildesheim in 2017. Her research interests are in human-computer interaction and information behavior, in particular collaborative information seeking, virtual and augmented reality and distant learning. She can be contacted at <a href="mailto:elbesh@uni-hildesheim.de">elbesh@uni-hildesheim.de</a>.<br />
            <strong>Thomas Mandl</strong> is a Professor in the Department for Information Science and Natural Language Processing at the University of Hildesheim, Universitaetsplatz 1, 31141 Hildesheim, Germany. He received a doctorate degree and a postdoctoral degree from the University of Hildesheim. His research interests include information retrieval, human-computer interaction and internationalization of information technology. He can be contacted at <a href="mailto:mandl@uni-hildesheim.de">mandl@uni-hildesheim.de</a>.<br />
            <strong>Christa Womser-Hacker</strong> is a Professor in the Department for Information Science and Natural Language Processing at the University of Hildesheim, Universitaetsplatz 1, 31141 Hildesheim, Germany. She received a doctorate degree and a postdoctoral degree from the University of Regensburg. Her main research areas are in the fields of cross-language information retrieval, text mining in the area of digital humanities, intercultural human-computer interaction, information (seeking) behavior and patent retrieval. She can be contacted at <a href="mailto:womser@uni-hildesheim.de">womser@uni-hildesheim.de</a>.</p>
        </section>

        <section class="refs">
            <h2>References</h2>
            <ul class="refs">
            <li id="ari07">Aritzeta, A., Swailes, S. &amp; Senior, B. (2007). Belbin&rsquo;s team role model: development, validity and applications for team building. <em>Journal of Management Studies</em>, <em>44</em>(1), 96-118.</li>
            <li id="bel10a">Belbin, R.M. (2010a). <em>Management teams. Why they succeed or fail</em> (3rd. ed.). Oxford: Butterworth-Heinemann.</li>
            <li id="bel10b">Belbin, R.M. (2010b). <em>Team roles at work</em> (2nd ed.). Oxford: Butterworth-Heinemann.</li>
            <li id="bod11">Bodemer, D. &amp; Dehler, J. (2011). Group awareness in CSCL environments. <em>Computers in Human Behavior</em>, <em>27</em>(3), 1043-1045.</li>
            <li id="gut02">Gutwin, C. &amp; Greenberg, S. (2002). A descriptive framework of workspace awareness for real-time groupware. <em>Computer Supported Cooperative Work (CSCW)</em>, <em>11</em>(3-4), 411-446.</li>
            <li id="han16">Hansen, P. &amp; Wid&egrave;n, G. (2016). The embededness of collaborative information seeking in information culture. <em>Journal of Information Science,</em> <em>12</em>(2), 1-13.</li>
            <li id="has12">Haseki, M., Shah, C. &amp; Gonzales-Ibanez, R. (2012). Time as a trigger of interaction and collaboration in research teams: a diary study. In <em>Proceedings of the ACM 2012 Conference on Computer Supported Cooperative Work Companion. CSCW&rsquo;12</em> (pp. 95-98). New York, NY: ACM.</li>
            <li id="hei02">Heinstr&ouml;m, J. (2002). <em>Fast surfers, broad scanners, and deep divers. Personality and information seeking behaviour</em>. &Aring;bo, Finland: &Aring;bo Akademis f&ouml;rlag.</li>
            <li id="hyl06">Hyldeg&aring;rd, J. (2006). <em>Between individual and group &ndash; exploring group members&rsquo;information behavior in context</em>. Copenhagen: Department of Information Studies, Royal School of Library and Information Science.</li>
            <li id="kir15">Kirschner, P.A., Kreijns, K., Phielix, C.&amp; Fransen, J. (2015). Awareness of cognitive and social behaviour in a CSCL environment. <em>Journal of Computer Assisted Learning</em>, <em>31</em>(1), 59&ndash;77. doi: 10.1111/jcal.12084.</li>
            <li id="kol06">Kollar, I., Fischer, F.&amp; Hesse, F.W. (2006). Collaboration scripts &ndash; a conceptual analysis. <em>Educational Psychological Review</em>, <em>18</em>(2), 159&ndash;185.</li>

            <li id="lee16">Leeder, C. &amp; Shah, C. (2016). Collaborative information seeking in student group projects. <em>Aslib Journal of Information Management</em>, <em>68</em>(5), 526&ndash;544. doi: 10.1108/AJIM-12-2015-0190.</li>

            <li id="may15">Mayring, P. (2015). Qualitative Inhaltsanalyse. Grundlagen und Techniken. Weinheim, Germany: Beltz.</li>

            <li id="mil15">Miller, M. &amp; Hadwin, A. (2015). Scripting and awareness tools for regulating collaborative learning: changing the landscape of support in CSCL. <em>Computers in Human Behavior,</em> <em>52</em>, 573&ndash;588. doi:10.1016/j.chb.2015.01.050.</li>

            <li id="mor13">Morris, M.R. (2013). Collaborative search revisited. In <em>Proceedings of the 2013 Conference of Computer Supported Cooperative Work. CSCW&rsquo;13</em> (pp. 1181-1192). San Antonio, TX, New York, NY: ACM.</li>

            <li id="pic08">Pickens, J., Golovchinsky, G., Shah, C., Qvarfordt, P. &amp; Back, M. (2008): Algorithmic mediation for collaborative exploratory search. In <em>Proceedings of the 31st Annual International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR &rsquo;08)</em> (pp. 315&ndash;322). New York, NY: ACM. doi:10.1145/1390334.1390389.</li>

            <li id="pol03">Poltrock, S., Grudin, J., Dumais, S., Fidel, R., Bruce, H. &amp; Pejtersen, A. (2003). Information seeking and sharing in design teams. In <em>Proceedings of the 2003 International ACM SIGGROUP Conference on Supporting Group Work (GROUP &rsquo;03)</em> (pp. 239&ndash; 247). New York, NY: ACM.</li>

            <li id="prz14">Przyborski, A. &amp; Wohlrab-Sahr, M. (2014): Qualitative Sozialforschung. Ein Arbeitsbuch. Oldenbourg, Germany: DeGruyter.</li>

            <li id="sha10a">Shah, C. (2010). A framework for supporting user-centric collaborative information seeking. Unpublished doctoral dissertation, The University of North Carolina, Chapel Hill, USA.</li>

            <li id="sha10b">Shah, C. &amp; Marchionini, G. (2010). Awareness in colla<em>borative information seeking. Journal of the Association for Information Science and Technology,</em> 61(10), 1970-1986.</li>

            <li id="sou14">Soulier, L., Shah, C. &amp; Tamine, L. (2014). User-driven system-mediated collaborative information retrieval. In <em>Proceedings of the 37th International ACM SIGIR Conference on Research &amp; Development in Information Retrieval (SIGIR '14)</em> (pp. 485&ndash;494). Gold Coast, Australia: ACM.</li>

            <li id="str04">Strijbos, J.-W., Martens, R.L., Jochems, W.M.G. &amp; Broers, N.J. (2004). The effect of functional roles on Group efficiency. <em>Small Group Research,</em> <em>35</em>(2), 195&ndash;229</li>

            <li id="tam15">Tamine, L. &amp; Soulier, L. (2015). Understanding the impact of the role factor in collaborative information retrieval. In <em>Proceedings of the 24th ACM International Conference on Information and Knowledge Management (CIKM '15)</em> (pp. 43&ndash;52). Melbourne, Australia: ACM.</li>

            <li id="vic15">Vick, T.E., Nagano, M.S. &amp; Popadiuk, S. (2015). Information culture and its influences in knowledge creation: evidence from university teams engaged in collaborative innovation projects. <em>International Journal of Information Management</em>, <em>35</em>(3), 292&ndash;298. doi:10.1016/j.ijinfomgt.2015.01.010.</li>

            <li id="wei05">Weinberger, A., Ertl, B., Fischer, F. &amp; Mandl, H. (2005). Epistemic and social scripts in computer- supported collaborative learning. <em>Instructional Science</em>, <em>33</em>(1), 1&ndash;30.</li>



            </ul>
        </section>
        <section>
            <hr />
            <h4 style="text-align:center;">How to cite this paper</h4>
            <div class="citing">Elbeshausen, S., Mandl, T. and Womser-Hacker, C. (2019). Role-specific behaviour patterns in collaborative information seeking In <em>Proceedings of ISIC, The Information Behaviour Conference, Krakow, Poland, 9-11 October: Part 2. Information Research, 24</em>(1), paper isic1831. Retrieved from http://InformationR.net/ir/24-1/isic2018/isic1831.html (Archived by WebCite® at http://www.webcitation.org/76lZ902FR)</div>

        </section>

        <hr />

    </article>
    <br />
    <section>

    <table class="footer" style="border-spacing:10px;">
    <tr>
    <td colspan="3" style="text-align:center; background-color: #5E96FD; color: white; font-family: verdana; font-size: small; font-weight: bold;">Find other papers on this subject</td></tr>
    <tr><td style="text-align:center; vertical-align:top;"><form method="get" action="http://scholar.google.com/scholar" target="_blank">
    <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"> <input type="hidden" name="q" value="collaboration, information seeking behavior" /><br />
    <input type="submit" name="sa" value="Scholar Search"  style="font-size: small; font-family: Verdana; font-weight: bold;" /><input type="hidden" name="num" value="100" />  </td>  </tr></table></form></td>
    <td style="vertical-align:top; text-align:center;">  <!-- Search Google --><form method="get" action="http://www.google.com/custom" target="_blank">
    <table class="footer">
    <tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="collaboration, information seeking behavior" /><br />
    <input type="submit" name="sa" value="Google Search" style="font-family: Verdana; font-weight: bold; font-size: small;" /><input type="hidden" name="client" value="pub-5081678983212084" /><input type="hidden" name="forid" value="1" /><input type="hidden" name="ie" value="ISO-8859-1" /><input type="hidden" name="oe" value="ISO-8859-1" /><input type="hidden" name="cof" value="GALT:#0066CC;GL:1;DIV:#999999;VLC:336633;AH:center;BGC:FFFFFF;LGC:FF9900;LC:0066CC;LC:0066CC;T:000000;GFNT:666666;GIMP:666666;FORID:1;" /><input type="hidden" name="hl" value="en" /></td></tr>
    </table></form></td>
    <td style="vertical-align:top; text-align:center;"><form method="get" action="http://www.bing.com" target="_blank">
    <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="collaboration, information seeking behavior" /> <br /><input type="submit" name="sa" value="Bing"  style="font-size: small; font-family: Verdana; font-weight: bold;" /> <input type="hidden" name="num" value="100" /></td></tr>
    </table></form></td></tr>
    </table>

    <div style="text-align:center;">Check for citations, <a href="http://scholar.google.co.uk/scholar?hl=en&amp;q=http://informationr.net/ir/24-1/isic2018/isic1831.html&amp;btnG=Search&amp;as_sdt=2000">using Google Scholar</a></div>
    <br />
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <div class="addthis_inline_share_toolbox" style="text-align:center;"></div>
    <hr />
    </section>

    <table class="footer" style="padding:10px;"><tr><td style="text-align:center; vertical-align:top;">
    <br />
    <div><a href="https://www.digits.net" target="_blank"><img src="https://counter.digits.net/?counter={f5d37ff4-6e5d-a5c4-950f-6ca1aaca51b4}&amp;template=simple" alt="Hit Counter by Digits" /></a></div>
    </td>
    <td class="footer" style="text-align:center; vertical-align:middle;">
    <div>  &copy; the authors, 2019. <br />Last updated: 1 March, 2019 </div></td>

    <td style="text-align:center; vertical-align:middle;">&nbsp;

    </td></tr>  </table>

    <footer>

    <hr />
    <table class="footer"><tr><td>
    <div class="button">
    <ul style="text-align: center;">
    <li><a href="isic2018.html">Contents</a> | </li>
    <li><a href="../../iraindex.html">Author index</a> | </li>
    <li><a href="../../irsindex.html">Subject index</a> | </li>
    <li><a href="../../search.html">Search</a> | </li>
    <li><a href="../../index.html">Home</a></li>
    </ul>
    </div></td></tr></table>
    <hr />
    </footer>
    <script src="http://www.google-analytics.com/urchin.js">  </script>  <script>  _uacct =
    "UA-672528-1"; urchinTracker();
    </script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5046158704890f2e"></script>
</body>
</html>
