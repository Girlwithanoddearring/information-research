<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="style.css">
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
 <meta name="dcterms.title" content="Meaningful reading experiences among elderly: some insights from a small-scale study of Swedish library outreach services" />
<meta name="citation_author" content="Lindberg, Jenny" />
<meta name="citation_author" content="Hedemark, Åse" />
 <meta name="dcterms.subject" content="The aim of this paper is to explore the extent to which concepts of information behaviour have been adopted within other disciplines." />
 <meta name="description" content="The aim of this paper is to explore the extent to which concepts of information behaviour have been adopted within other disciplines, to the extent allowed by quantitative analysis of Web of Science data.  Searches were carried out in Web of Science in each decade from 1960 to the present day and the results analysed by the journals publishing related papers and by the research areas of these journals.  The 'Analyze Results' feature of Web of Science was used to provide quantitative analysis of the results, by journal title and by research area.  While papers on information behaviour appear in more than one hundred disciplinary areas, the distribution is concentrated in a very limited number of areas and is otherwise thinly spread over the remaining disciplines. Scholars in many disciplines have explored the information needs and information behaviour of those working in the discipline, or whom the disciplines serves. However, the concentration of interest is found in the health and medical sciences, computer science and information systems, communication and media studies, and psychology." />
 <meta name="keywords" content="reading experience, outreach service, elderly people, social inclusion, identity-shaping," />
 <meta name="robots" content="all" />
 <meta name="dcterms.publisher" content="University of Borås" />
 <meta name="dcterms.type" content="text" />
 <meta name="dcterms.identifier" content="ISSN-1368-1613" />
 <meta name="dcterms.identifier" content="http://InformationR.net/ir/24-1/isic2018/isic1836.html" />
 <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/24-1/infres241.html" />
 <meta name="dcterms.format" content="text/html" />  
 <meta name="dc.language" content="en" />
 <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />
 <meta  name="dcterms.issued" content="2019-03-15" />
  <meta name="geo.placename" content="global" />
</head>

 <body>
    <h4 id="vol-24-no-1-march-2019">vol 24 no.1, March 2019</h4>
 <h5>
  Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 2.
 </h5>

 <h1>Meaningful reading experiences among elderly: some insights from a small-scale study of Swedish library outreach services
</h1>  

 <h2 class="author"><a href="#author">Jenny Lindberg</a> and <a href="#author">&Aring;se Hedemark</a>
</h2>

 <blockquote>
 <strong>Introduction</strong>.This paper reports on a small-scale empirical study of Swedish elderly citizens, focusing on their everyday use of literature, provided by public library outreach services.<br />
<strong>Methods</strong>. Empirical data was gathered by telephone interviews with six elderly citizens - four women and two men.<br />
<strong>Analysis</strong>. A qualitatively oriented text analysis was conducted. The transcripts were interpreted through an analytical framework based on the concepts reading experience, social inclusion and identity-shaping.<br />
<strong>Results</strong>. The participants’ articulated reading experiences show that reading provides opportunities for both escapism and intellectual stimulation. High quality service and literature is expected and much appreciated.<br />
<strong>Conclusion</strong>. The analysis indicates that the outreach service in focus serves both as a practical link between the library’s collection and the patrons, and as a social link connecting the elderly to society at large. The service plays a crucial role for the ability of the elderly in shaping and upholding their identities as readers.
 </blockquote>

 <section>

 <h2>Introduction</h2>

 <p>Despite an ageing population in the technically developed parts of the world, studies on senior citizens&rsquo; information needs and use are scarce. In library and information science previous research on the elderly tends to focus on health related information seeking and seldom highlights other kinds of activities and resources such as reading and literature. This paper reports on a small scale interview study of six elderly Swedish citizens, describing their everyday use of literature through various media, provided by public library outreach services in an urban setting. The aim of the study is to understand and describe the role of literature and reading, supported by public libraries, as vital parts of the participants&rsquo; everyday life. These activities are interpreted as profoundly meaningful, both upholding and shaping the reading identity of the elderly. The study is part of an evaluation project initiated by the Regional Library of Stockholm.</p>

<p>The following research questions have guided the study.</p>

<p>Q1: How do the elderly describe their <em>reading experiences</em>?</p>
<p>Q2: What qualities do the outreach service bring, according to the elderly?</p>
<p>Q3: How may the insights about elderly&rsquo;s reading experiences through the outreach service be understood in terms of <em>social inclusion </em>and <em>identity-shaping</em>?</p>

</section>
<section>

<h2>Previous research</h2>

<p>The study is informed by previous studies from two different areas within LIS, studies on information needs, seeking and use (INSU) of seniors (e.g. <a href="#cha91">Chatman, 1991</a>; <a href="#luy11">Luyt and Ho Swee, 2011</a>, <a href="#wil97">Williamson, 1997</a>; <a href="#wil09">Williamson and Asla, 2009</a>) on the one hand, and studies on reading practices on the other (e.g. <a href="#dol11">Dolatkhah, 2011</a>; <a href="#mck16">McKechnie, Skjerdingstad, Rothbauer, Oterholm and Persson, 2016</a>; <a href="#ros06">Ross, McKechnie and Rothbauer, 2006</a>). A summary of the results from previous INSU studies of elderly reveals a prime focus on health-related information behaviour (<a href="#cas16">Case and Given, 2016</a>) while studies on personal interests and the activities of everyday life have been paid less attention. Against this backdrop Chatman&rsquo;s (<a href="#cha91">1991</a>) contribution stands out as extra interesting with its focus on information behaviour in an everyday life context, including older people&rsquo;s interests and their use of literature. A certain point is also made by the author as she challenges the occurring stereotype of elderly peoples&rsquo; preference for light-weight media, such as TV-shows. In the media landscape of today, it is sometimes argued that digital media holds a potential as means for developing democratic library services for all citizens, at least in the technologically developed parts of the world. Still this technological potential often seems limited by age and physical impairment for the user group in focus, that is people of the <em>fourth age</em>, beyond retirement (cf. <a href="#wil09">Williamson and Asla, 2009</a>). Reading practices and reading experiences are to some extent explored within LIS beside neighbouring disciplines like literary studies (<a href="#ros78">Rosenblatt, 1978/1994</a>; <a href="#per16">Persson, 2016</a>). Such studies have also proven valuable for this investigation. As the participants not only depend on a specific outreach service but also on different audiovisual devices for reading, we have also turned to studies that highlight the material aspect of reading (cf. <a href="#dol11">Dolatkhah, 2011</a>; <a href="#hyd13">Hyder, 2013</a>).</p>

</section>

<section>

<h2>Theory</h2>

<p>Drawing on different strands of LIS research some of the previously mentioned contributions also provide useful analytical tools for the study at hand. Analysing the elderly&rsquo;s experiences of reading literature and fiction, our approach has been to view the readers as active, meaning-making individuals (cf. <a href="#nie12">Niemel&auml;, Houtari and Kortelainen, 2012</a>). The main theoretical concepts of this study are <em>reading experience, social inclusion </em>and <em>identity-shaping</em>. Reading experience denotes the participants&rsquo; engagement with a printed or recorded book (cf. <a href="#mck16">McKechnie <em>et al.</em>, 2016</a>). It is not the experience as such that is captured in this study; rather it is the informants&rsquo; articulations of their experiences. Accordingly, the aim of this study is to understand the reading experiences as described by the informants &ndash; not to find the <em>true</em> meaning of reading. Inspired by Chatman&acute;s studies of elderly women (<a href="#cha91">1991</a>; <a href="#cha92">1992</a>) we use the concept of <em>social inclusion </em>to explain what motivates the informants&rsquo; use of literature and the library&rsquo;s outreach service. The concept also entails accessibility; including aspects related to physical impairment, poor economy and the lack of digital and technological skills that might inhibit access to both informal and formal sources of information (<a href="#cha91">Chatman, 1991</a>, p. 282). Rosenblatt (<a href="#ros78">1994</a>) and Ross <em>et al.</em> (<a href="#ros06">2006</a>) state that reading is one way of defining who we are, in relation to others. Drawing on the work of Ross and colleagues (<a href="#ros06">2006</a>) we use the concept of <em>identity </em>as a &lsquo;<em>shifting understanding of the self in relation to various social structures and social constraints</em>&rsquo; (p. 115). The elderly constitute a particularly interesting group in this respect since their age allow them to reflect over nearly a lifespan of reading experiences and the construction of different identities in relation to these.</p>

</section>

<section>

<h2>Method and material</h2>

<p>The research questions have been addressed through the analysis of empirical data produced through gathered from telephone interviews with six elderly citizens &ndash; four women and two men. All participants are public library patrons, ordering and receiving books via the specific outreach service called <em>The book is coming </em>(<em>Boken kommer, </em>in Swedish). As mentioned, the study is part of a larger evaluation project and as such it is supported by both focus group interviews with library staff and management, and statistics. However, in this paper we concentrate on the user perspective. The service in focus is conditioned by some kind of impairment hindering patrons from visiting the library in person. The six participants share the trait of relatively high age, spanning from 69 to 89 years, the majority being over 80 and living in single households. The interviews primarily revolved around the specific service organized by the public library but also around literature and reading as such. The interviews lasted on average for 15-30 minutes. Pseudonyms are used to ensure the participant anonymity and all quotations are reported verbatim after being translated into English by the authors. The analysis focuses primarily on the participants articulated experiences, but it also aims at deepening the knowledge about the group&rsquo;s experiences by a conceptual discussion on social inclusion and identity-shaping.</p>

</section>

<section>

<h2>Findings</h2>

<p>The overall findings show that there are two salient themes emerging from the empirical material. The following presentation is structured according to these.</p>

<h3>Human care and comfort</h3>

<p>The reading experiences articulated by the elderly seem to be closely intertwined with the actual outreach service as such, making the reading experiences clearly affected by the elderly's perception of the service. The quote below illustrates one important dimension of the outreach service, namely the experience of being cared for as a human being.</p>

<blockquote>
<p><em>When someone tends to me, I feel needed. [...] This service that I receive, </em>The book is coming<em>, it makes nothing stand in the way. There is only I and Lena (the librarian) and when the man who delivers the books arrives, asking how I am, it is like we care for each other. (Ingegerd)</em></p>
</blockquote>

<p>Several participants are physically impaired in some way, which decreases their chances to meet and interact with other people. The outreach service seems to provide them with an opportunity to stay in touch, to feel included and to feel needed. Nevertheless, the literature provided by the service and the experience of reading also appears to be a comfort and a joy in itself. Tyra states that she enjoys listening to talking books as she sometimes has trouble sleeping. Ingegerd stresses that she &lsquo;<em>would feel naked&rsquo;</em> without her books. In the next excerpt Cecilia describes how reading has a relaxing function and that it enables her to envision other ways of living, a sort of stimulating escapism from the problems and trivialities of everyday life.</p>

<blockquote>
<p><em>It [reading] means a lot. When you are older and your vision is poor, you can&rsquo;t see everything on TV. It is impossible to separate who&rsquo;s who for example. [...]. It [the reading] is a distraction from everything. And you can dream about another way of living. Right now I read about the 1500s and the 1600s...about a woman...I don&rsquo;t like the book but I have to finish it (laughter). (Cecilia)</em></p>
</blockquote>

<p>The statement indicates that reading provides an opportunity for Cecilia to explore the identities (<a href="#ros06">Ross <em>et al.</em>, 2006</a>, p. 115) of historical fictional characters as in her reference to the woman in the novel. Cecilia&rsquo;s way of reasoning about difficulties using different kinds of media reoccurs in the data. Several participants mention the loss of access to visual media due to impaired sight. Ingegerd strongly states that she hates mobile phones and TV. The reason for this is that she feels that those media do not allow for personal interaction in the way she likes it: <em>&lsquo;It&rsquo;s like we don&rsquo;t engage...you just push buttons. One can&rsquo;t interact. One can&rsquo;t talk&rsquo;. </em>Reading seems to function as a channel, giving the elderly a kind of access to society and the outside world, despite physical impairment and/or inabilities to navigate various digital media (<a href="#hyd13">Hyder, 2013</a>). This notion connects to another theme emerging through the analysis, namely the much valued intellectual stimulation through reading.</p>

<h3>Quality and intellectual stimulation</h3>

<p>An important aspect of reading often mentioned by the participants is the value ascribed to literature of a certain quality. The demand for quality may of course be driven by aesthetic interests but it is also clearly associated with the need of staying intellectually alert:</p>

<blockquote>
<p><em>I have this <strong>recorder</strong> for discs, both in the bedroom and in the living room. So I keep one book going in the living room and another one in the bedroom. [I: Aha!] And then it comes to using your brain to recall which book am I into, and where am I in the book? (Cecilia)</em></p>
</blockquote>

<p>Keeping alert in this way is understood as achievable also by reading predominantly literature of quality or by talking with librarians knowledgeable in literature. The following quote illustrates this line of reasoning:</p>

<blockquote>
<p><em>They [the librarians], have to be knowledgeable of literature, as they often are. And they have to be nice when calling you on the phone. [&hellip;] It&rsquo;s important to be really social, I believe. (Mats)</em></p>
</blockquote>

<p>Notably, beside the basic virtues of kindness and respectful manners, the importance of the service providers, for example the librarians, both being knowledgeable of literature and being readers themselves is often put forward by the elderly. Some participants emphasize their own deep interest in literature and reading, not merely as a way of passing time, but largely for intellectual stimulation. Librarians and other members of the library staff are mentioned as appreciated discussion partners within this shared area of interest.</p>

<p><em>&lsquo;[Reading] has sort of developed my mind, and I think I have become so much wiser reading books. (Ingegerd)&rsquo;. </em>As this statement shows, the informants stress the need to sustain their intellectual capabilities and cultivate their interests and hobbies. When asked who else would benefit from the outreach service Holger states firmly: <em>&lsquo;Old men in my age are either interested in a lot or they are not interested in anything&rsquo;</em>. The statement could be interpreted as either you are interested in many things, including the outreach service and books, or you are not really interested in much at all. Holger clearly appears to have shaped an identity as an active and interested person, since he does read and take part of the service. On several occasions the informants reflect back on a lifetime of interest in reading and some of them explicitly identify themselves as readers since childhood. Margareta states: <em>&lsquo;I have always read... well, since I started to read. I am a bookaholic&rsquo;</em>. One exception is Tyra who claims not to have read much in her younger days but has developed the habit in recent years.</p>

</section>

<section>
<h2>Concluding discussion</h2>

<p>How can the <em>reading experiences </em>of the elderly be understood? Reading experiences are described by the informants both as relaxation, a distraction from the everyday trivialities, and as a means to strengthen intellectual capabilities and a chance to interact with others. The qualities informants attribute to the outreach service are strongly related to aspects such as respectfulness, human care and comfort. The data also shows that reading and the service <em>per se </em>provide social inclusion. In this case social inclusion holds two dimensions &ndash; one concerning personal interaction where for example talking to the librarian about books and sharing reading experiences with friends and relatives are mentioned as important. The other dimension relates to the societal level where reading renders a sense of interaction and connection with society at large. Although the elderly may be physically impaired and unable to go far from home, they might reach out and connect with others through reading. The results fit in well with Chatman&rsquo;s (<a href="#cha91">1991</a>) research showing that elderly are indeed, like most people, curious about the larger social world and utilize different sources such as literature, to stay informed. Reading appears to give the participants a clear sense of well-being and of being less isolated. The quality of the outreach service and the literature provided by it could in this sense be understood in terms of <em>social inclusion</em>. As stated by Chatman (<a href="#cha91">1991</a>) the elderly are not indiscriminate users, but rather <em>picky</em>. On a similar note, the findings from this study indicate that the elderly choose literature selectively based on taste and on what they perceive as quality literature. This is motivated by the need to satisfy intellectual curiosity.</p>

<p>In summary, the results show that reading plays a vital part shaping and upholding an identity as a reader, also among the elderly &ndash; reading seems to help define themselves as interested, intellectually alert and actively in contact with the rest of society. Ross and co-authors (<a href="#ros06">2006</a>) point out reading as a way of seeking information about the world. They claim that a person reads to understand how the world works and as a reader you tend to relate the world to your own experiences and how you fit into it (p. 116). Correspondingly, the results of this study indicate that elderly use literature in this way, utilizing books to orient themselves about the world and understand who they are in relation to it. In fact; the elderly stand out as engaged readers and users of library services.</p>

</section>

<section>
<h2>Acknowledgements</h2>

<p>Thanks to the Regional Library of Stockholm who funded the study on which this paper is based. We also wish to thank our reviewers for helpful comments and Dr. Ola Pilerot for proofreading.</p>
</section>

 
<section>

<h2 id="author">About the authors</h2>

<p><strong>Jenny Lindberg </strong>is a Lecturer at the Swedish School of Library and Information Science, University of Bor&aring;s, Sweden. In 2015 she defended her doctoral thesis, where the information seeking and the shaping of professional identities among library and information science students and novice academic librarians were investigated. Her current research activities focus on the professional information practices of librarians and on the development of public library services. She can be contacted at <a href="mailto:jenny.lindberg@hb.se">jenny.lindberg@hb.se</a>.<br />
<strong>&Aring;se Hedemark</strong> is a Senior Lecturer in Library and Information Science, at the Department of Archival Science, Library &amp; Information Science, and Museum &amp; Heritage Studies, Uppsala University. Hedemark completed her PhD in Library and Information Science in 2009 at Uppsala University. Her PhD project investigated the public image of public libraries using discourse analysis. Her current research interest includes children&acute;s literacy practices and information literacies in public libraries and other cultural institutions. She can be contacted at <a href="mailto:ase.hedemark@abm.uu.se">ase.hedemark@abm.uu.se</a>.</p>

</section>
<section class="refs">

<h2>References</h2>
<ul class="refs">
<li id="cha91">Chatman, E. (1991). Channels to a larger social world: older women staying in contact with the Great Society. <em>Library and Science Research</em>, <em>13</em>(3), 281-300.</li>

<li id="cha92">Chatman, E. (1992). <em>The information world of retired women</em>. Westpost, CT: Greenwood Press.</li>

<li id="cas16">Case, D.O. &amp; Given, L.(2016). <em>Looking for information: a survey of research on information seeking, needs, and behavior</em> (4th ed.). Bingley, UK: Emerald.</li>

<li id="dol11">Dolatkhah, M. (2011). <a href="http://urn.kb.se/resolve?urn=urn%3Anbn%3Ase%3Ahb%3Adiva-3599"><em>Det l&auml;sande barnet: minnen av l&auml;spraktiker, 1900&ndash;1940 </em>[<em>The reading child: memories of reading practices 1900&ndash;1940</em>].</a> Unpublished doctoral dissertation, Institutionen Biblioteks- och informationsvetenskap/Biblioteksh&ouml;gskolan, H&ouml;gskolan i Bor&aring;s, Sweden. Retrieved from http://urn.kb.se/resolve?urn=urn:nbn:se:hb:diva-3599 (Archived by WebCite&reg; http://www.webcitation.org/6xwjRBLTq).</li>

<li id="hyd13">Hyder, E. (2013). <em>Reading groups, libraries and social inclusion: experiences of blind and partially sighted people</em>. Farnham, UK: Ashgate.</li>

<li id="luy11">Luyt, B. &amp; Ho Swee, A. (2011). Reading, the library, and the elderly: a Singapore case study. <em>Journal of Librarianship and Information Science</em>, <em>43</em>(4), 204-212.</li>

<li id="mck16">McKechnie, L., Skjerdingstad, K.I., Rothbauer, P.M., Oterholm, K. &amp; Persson, M. (Eds.). (2016). <em>Plotting the reading experience: theory, practice, politics</em>. Waterloo, ON: Wilfrid Laurier University Press.</li>

<li id="nie12">Niemel&auml;, R., Houtari, M-L &amp; Kortelainen, T. (2012). Enactment and use of information and the media among older adults. <em>Library &amp; Information Science Research</em>, <em>34</em>(3), 212-219.</li>

<li id="per16">Persson, M. (2016). The hidden foundations of critical reading. In L. McKechnie, K.I. Skjerdingstad, P.M. Rothbauer, K. Oterholm &amp; M. Persson (Eds.), <em>Plotting the reading experience: theory, practice, politics</em> (pp. 19-36). Waterloo, ON: Wilfrid Laurier University Press.</li>

<li id="ros78">Rosenblatt, L. (1978/1994). <em>The reader, the text, the poem: the transactional theory of the literary work</em>. Carbondale, IL: Southern Illinois University Press.</li>

<li id="ros06">Ross, C.S., McKechnie, L. &amp; Rothbauer, P.M. (2006). <em>Reading matters: what the research reveals about reading, libraries, and community</em>. Westport, CT: Libraries Unlimited.</li>

<li id="wil97">Williamson, K. (1997). The information needs and information-seeking behaviour of older adults: an Australian study. In P. Vakkari, R. Savolainen &amp; B. Dervin (Eds.), <em>Information seeking in context. Proceedings of a meeting in Finland 14-16 August 1996</em> (pp. 337-350). London: Taylor Graham.</li>

<li id="wil09">Williamson, K. &amp; Asla, T (2009). Information behaviour of people in the fourth age: implications for the conceptualization of information literacy. <em>Library and information Science Research</em>, <em>31</em>(2), 76-83.</li>


</ul>
</section>

</body>
 </html>
