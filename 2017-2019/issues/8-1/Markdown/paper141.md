#### Information Research, Vol. 8 No. 1, October 2002

# Understanding knowledge management and information management: the need for an empirical perspective

#### [France Bouthillier](mailto:france.bouthillier@mcgill.ca) and [Kathleen Shearer](mailto:mkshearer@sprint.ca)  
Graduate School of Library and Information Studies  
McGill University  
Montreal, Canada

#### **Abstract**

> Is Knowledge Management (KM) an emerging discipline or just a new label for Information Management (IM)? To provide some answers to this question, the article summarizes empirical evidence of how KM is practiced in several types of organizations demonstrating the variety of organizational approaches that are used and the processes that are involved. Based on an exploratory study of KM practices, the article presents a typology of methodologies that are employed in various organizations to illustrate what may be considered as the particular nature of KM to show potential differences with IM. The first section of the article discusses the concepts associated with the management of information and knowledge. The second part provides a description of the conceptual framework used for the study and a presentation and discussion of the results.

## Introduction

The distinction between Knowledge Management (KM) and Information Management (IM) is far from being well-articulated in the KM literature and this is compounded by the confusion around the concepts of knowledge and information. In fact, there is no consensus regarding the claim that KM is a new field with its own research base, since much of the terminology and techniques used, such as knowledge mapping, seem to have been borrowed from both IM and librarianship ([Koenig, 1997](#koe97)). KM is considered by some as the business salvation and by others as the "emperor's new clothes" ([Martensson, 2000](#mar00)). On the one hand, authors such as Gourlay ([2000](#gou00)) and Beckman ([1999](#bec??)) present KM as an emerging discipline. According to Beckman, the expression was coined for the first time in 1986 by Dr. Karl Wiig who wrote one of the first books on the topic, _Knowledge Management Foundations_, published in 1993. On the other hand, others, such as Broadbent ([1998](#bro98)), Streatfield and Wilson ([1999](#str99)), claim that firms and information professionals have been practicing for years KM-related activities. Streatfield and Wilson ([1999](#str99)) argue that the concept of knowledge is over-simplified in the KM literature, and they seriously question the attempt to manage what people have in their minds. Nevertheless, there is a real interest and enthusiasm in KM as revealed by the increasing number of publications relating to the topic since 1995 ([Mahdjoubi & Harmon, 2001](#mah01)). In addition, the library and information press has suggested for a number of years that it is a burgeoning field of great interest to information professionals, since they possess the necessary skills to work in the field ([Broadbent, 1997](#bro98-2); [Abram, 1997](#abr97); [Chase, 1998](#cha98); [Henczel, 2001](#hen01); [Oxbrow & Abell, 2002](#oxb02)).

In the business community, there is also a strong interest for KM. A survey conducted in 1997 of 200 large US firms revealed that 80% of corporations had knowledge initiatives ([KPMG, 2000](#kpm00)). Technological innovation has been cited as a major reason for the current interest in KM ([Covin & Stivers, 1997](#cov97)). In the high-tech sector, as well as consulting firms, the stakes are particularly high because knowledge is considered as "the only meaningful economic resource" ([Choo, 1998, 2](#cha98)). Private sector organizations are not the only ones embracing KM. The systematic sharing of knowledge is assuming a larger role in all kinds of organizations around the world ([World Bank, 1999](#wor98); [Luen & Al-Hawamdeh, 2001](#lue01)). Some of the recent KM initiatives in the United Kingdom include the creation of the post of knowledge officer at the British Council and the appointment of a Chief Knowledge Officer at NatWest Markets ([Skok, 2000](#sko00)). Claims of the potential benefits of KM abound and range from improving productivity, decision making, customer service and innovation (See [MacMorrow, 2001](#mac01), for a review of these claims).

Although many KM initiatives are documented in the business literature ([Davenport & Prusak, 1998](#dav98a)), what is actually entailed in these initiatives remains vague and ambiguous because there are many interpretations of knowledge management. And, a recent review by Hlupic et al. ([2002](#hlu02)) identified 18 different definitions of KM. Many attempts have been made to define KM from a theoretical perspective ([Choo, 1998](#cho98a); [Srikantaiah & Koenig, 1999](#sri00), [Oluic-Vukovic, 2001](#olu01), Mac Morrow, 2001) and to identify the various types of organizational knowledge ([Nonaka & Takeuchi 1995](#non95), [Boisot 1998](#boi98), [Brown & Duguid 1998](#bro98)). These attempts do not really address the relationships between KM and IM.

Recently, this lack of a clear distinction between information and knowledge has been recognized as a major issue with the KM literature ([Martensson, 2000](#mar00-2); [Tsoukas & Vladimirou, 2000](#tso00); [Kakabadse et al., 2001](#kak01)). Gourlay ([2000](#gou00)) suggests that KM practices focus mainly on knowledge representations not on knowledge _per se,_ making the distinction between KM and IM even more blurred. There is indeed a fine line between KM and IM at both the conceptual and practical levels. In this article, we would like to suggest that in order to discern what is knowledge and its management in the context of organizations, there is a need to examine the methodologies that are used in KM initiatives. Much of the case-studies published in the literature are associated with large consulting firms, who were pioneers in that area, lending credence to the belief that KM simply a marketing ploy for consultants. Our goal is to show that there are many other types of organizations which have developed an interest for KM, and that the information science community needs to take this phenomenon into account.

This article does not seek to review the KM literature nor to close the debate about KM versus IM differences. Our humble purpose is to summarize some empirical evidence about how KM is practiced in various types of organizations, both from the public and private sectors in order to show the variety of organizational approaches that are used and the processes that are involved. The article examines the concepts associated with the management of information and knowledge. Definitions of IM and KM are briefly presented to circumscribe their differences, and to explain the conceptual framework used for the study. And, finally, based on an exploratory study, the article presents a typology of the methodologies that are employed in various organizations, methodologies that are illustrative of the particular nature of KM and the results of this study are discussed.

## Related concepts

New terminologies and disciplines often generate legitimate skepticism. A few years ago, Cronin ([1985, viii](#cro85)) dealt with the question "is there anything new in the field of information management, or is it just a ritzier label for librarianship?" In his book, he attempted to demonstrate that IM was a new interdisciplinary field seeking to address new approaches to the management of information. Not everyone share his enthusiasm. Connell ([1981, 78](#con81)) argued that the idea of information resource management was "an ill-disguised attempt to provide a sinecure for aging data processing managers". Indeed, the management of information, information resources, and knowledge raises a number of conceptual problems. Cronin ([1985, viii)](#cro85) rightly pointed out that "there are as many definitions of information management as there are supporters of the concept… definitions of information abound (and) definitions of management are many and varied". Similarly, Eaton and Bawden ([1991](#eat91)) have questioned the idea that information is a resource that could be easily managed. The same approach has been taken regarding KM, with Yates-Mercer and Bawden ([2002](#yat02)) arguing that these issues are even more applicable to the management of knowledge.

To differentiate the management of information from the management of knowledge, one must examine the distinctions drawn between the related concepts: **data, information, knowledge and intelligence.** Attempts to define these concepts are numerous and produce slightly different results, depending on which discipline is looking at them. Dictionaries define data as factual information (measurements or statistics) used as a basis for reasoning, discussion, or calculation; information as the communication or reception of knowledge or intelligence; knowledge as the condition of knowing something gained through experience or the condition of apprehending truth or fact through reasoning, and intelligence as the ability to understand and to apply knowledge. For Meadow, et al. ([2000](#mea00)), data refer to a "string of elementary symbols, such as digits or letters" ([p.35](#mea00)). As they point out, information "has no universally accepted meaning, but generally it carries the connotation of evaluated, validated or useful data" ([p.35](#mea00)). Knowledge, on the other hand, involves "a higher degree of certainty or validity than information" and "has the characteristic of information shared and agreed upon within a community" ([Meadow, et al. 2000, p.38](#mea00)). Intelligence, for the previous authors, is a form of information but it is also "a measure of reasoning capacity" ([p. 39](#mea00)). As we can see, many conceptual overlaps exist between all these terms.

Wiig ([1999](#wii89)) defines information as facts and data organized to characterize a particular situation and knowledge as a set of truths and beliefs, perspectives and concepts, judgments and expectations, methodologies and know-how. Therefore, information can be seen as data made meaningful by being put into a context and knowledge as data made meaningful through a set of beliefs about the causal relationships between actions and their probable consequences, gained through either inference or experience ([Mitchell, 2000](#mit00)). Knowledge differs from information in that it is predictive and can be used to guide action while information merely is data in context. For example, if the raw data is -10 degrees, then information would be it is -10 degrees outside, and the knowledge would be that -10 degrees is cold and one must dress warmly. In other words, knowledge is closer to action while information could be seen as documentation of any of pieces of knowledge.

As demonstrated by the variety of definitions, it remains unclear what knowledge is and how it can be managed. The KM literature tends to subscribe to fairly inclusive definitions of knowledge and in practice concepts of knowledge and information are often used interchangeably ([Kakabadse et al., 2001](#kak01)). One example of these definitions, penned by Davenport and Prusak ([1998, p.5](#dav98b)) describes "knowledge (as) a fluid mix of framed experience, values, contextual information, and expert insight that provides a framework for evaluating and incorporating new experiences and information". Nonaka and Takeuchi ([1995, p.58](#non95)) argue that " information is a flow of messages, while knowledge is created by that very flow of information anchored in the beliefs and commitments of its holder." These definitions are not very helpful to distinguish information from knowledge – information is basic to knowledge, the latter is more connected to values, belief, and action – and it is not obvious whether individual and organizational knowledge are similar or different.

Organizational knowledge is frequently categorized into typologies. For example, Nonaka and Takeuchi ([1995](#non95)) identify tacit and explicit knowledge; Choo ([1998a](#cho98a)) sees three different types of knowledge (tacit, explicit, and cultural); and Boisot ([1998](#boi98)) describes four types (personal, proprietary, public knowledge and common sense).

Tacit knowledge seems to be the primary concern of KM writers and has been a great deal of discussion in the literature about its nature. Tacit knowledge is defined as action-based, entrained in practice, and therefore cannot be easily explained or described, but is considered to be the fundamental type of knowledge on which organizational knowledge is built ([Nonaka & Takeuchi, 1995](#non95); [Choo 1998a](#cho98a)). Tsoukas and Vladimirou ([2000, 4](#tso00)) argue, however, that "tacit knowledge is not something that can be converted into explicit knowledge", as claimed by Nonaka and Takeuchi ([1995](#non95)) and other authors. Although most KM writers cite Polanyi ([1962](#pol62)), who drew a distinction between tacit and explicit knowledge, they often overlook a part of his writings emphasizing the personal character of knowledge and knowing. For Polanyi, tacit knowledge cannot be expressed because "we know more than we can tell". Therefore we cannot articulate what we know with words because we are not fully conscious of all the knowledge we possess. It resides and remains in the human mind. Polanyi ([1962](#pol62)) illustrates this with the example of a medical student learning how to read X-ray pictures by listening to experts reading them. Exposure to empirical material and specialized language combined with the learning of medical knowledge will enable the student to become an expert, but tacit knowledge remains tacit. For Nonaka and Takeuchi ([1995](#non95)), tacit knowledge can be transmitted through social interactions or socialization, and made explicit through externalization-although they agree with the idea that tacit knowledge is somewhat hidden. These very different perspectives are a reflection of different backgrounds: Polanyi is a philosopher concerned with individual knowledge while Nonaka and Takeuchi are organizational theorists interested in how knowledge circulates in organizations.

Explicit knowledge, unlike tacit knowledge, is defined as knowledge that can be codified and therefore more easily communicated and shared. KM writers view explicit knowledge as structured and conscious and therefore it can be stored in information technology ([Martensson, 2000](#mar00)). This type of knowledge is often equated with information, providing the argument that KM is simply another terminology for IM. However, the concept of information is far from clear-cut. "Information is a vague and elusive concept susceptible of being understood in a variety of ways" ([Gourlay, 2000, p.3](#gou00)). In the IM literature, the distinction between information and information resources is also very innocuous. Both encompass many other terms such as data, words, records, documents and databases ([Eaton & Bawden, 1991](#eat91)), while information resources also include very different types of resources such as human beings (informal sources) and books and newspapers labeled formal sources ([Choo, 1998b](#cho98b)).

Considering that the concepts of both information and knowledge are unsatisfactorily defined and that the notion that tacit knowledge can be transformed into explicit knowledge is troublesome, some authors have suggested that the expression "knowledge management" is perhaps misleading. Gourlay ([2000](#gou00)), for instance, argues that knowledge itself cannot be managed and it is "knowledge representations" that are the actually focus of KM. And, Abram ([1997](#abr97)) wrote that the knowledge environment, or the conditions of its use, are the only dimensions that are manageable. Hlupic et al. ([2002](#hlu02)) citing Marshall and Brady, mention that given the complexity of knowledge, the depiction of types of knowledge, such as tacit and explicit, as mutually exclusive categories might be also misleading and prevent researchers to see the interrelated dimensions involved in the process of knowing.

While these arguments have merit, it is our contention that to consider information as the equivalent of explicit knowledge reveals an inadequate assessment of the qualitative dimensions of the various types of information and knowledge created, used and transferred in organizations. As Kakabadse et al. ([2001, p.140](#kak01)) suggest, "information and data management are important pillars of knowledge management" but the latter "encompasses broader issues and, in particular, creation of processes and behaviours that allow people to transform information into the organization and create and share knowledge." For example, managing employees' files, sales performance reports and financial statements (information management) involves some similar processes as managing explicit knowledge, but also involves some very different processes, such as the sharing of rules, operating procedures (formal and informal) and learning experiences. Similarly, while both IM and KM require a high degree of human involvement, their objectives are often very different. The ultimate goal of IM is to ensure that information is stored and retrievable, while the ultimate purpose of KM is tied more closely to organization outcomes. For instance, organizations often state their KM goals as to facilitate product innovation. Those with an inclusive definition of IM might purport that KM is nothing new, just as, in the past, those with a broad definition of data management claimed that IM was nothing new. However, with IM came the idea that organizations can be assessed according to the "information flow" and analyzed according to the information processes at work. In a similar way, KM has provided a framework for assessing contextual information, and taking into account more informal information exchanges. In that sense, both fields are very complementary.

## Managing information and knowledge: a conceptual framework

Knowledge and information are not static but rather move through organizations in various ways. One way to distinguish between KM and IM is to identify the processes or steps involves in both fields. Strictly speaking, IM focuses on the "plans and activities that need to be performed to control an organization's records" ([Place & Hyslop, 1982](#pla82)). The nuances between IM and Records Managements and Information Resources Management are subtle because, as mentioned earlier, all these terms tend to be used interchangeably. For Wilson ([1989](#wil89)), IM is the management of the information resources of an organization and involves the management of information technology. Choo ([1998b](#cho98b)) proposed a process model of IM. Presented as a cycle, Choo's model of IM entails 5 basic steps: identification of information needs, information acquisition, information organization and storage, information distribution and information use. Each step requires the planning, the organization, the coordination and the control of a number of activities supported by information technology. According to Choo, IM is key for sustaining knowledge creation and application in organizations and should lead to the "intelligent organization". Although his view is not recent, Cronin ([1985](#cro85)) claimed that focus of IM initiatives is often to control systematically recorded information and less on the use of these records (for example, see the [British Columbia Archives, 2002](#bri02)). Although the human element is considered in techniques such as information audit and mapping, IM or IRM programs tend to implement and maintain information systems and place a strong emphasis on information resources and technology. On the other hand, people management is a critical component of KM ([Gourlay, 2000](#gou00)).

Attempts to define KM processes are numerous. Nonaka and Takeuchi ([1995](#non95)) described four knowledge conversion processes: socialization, externalization, combination, and internalization. Each process involves converting one form of knowledge (tacit or explicit) to another form of knowledge (tacit or explicit). This model focuses on the important issue of how knowledge may be created through organizational sharing and is useful for identifying and evaluating certain key activities in the management of knowledge. Hlupic et. al. ([2002](#hlu02)) refer to Ruggles who identified three main types of activities: knowledge generation involving the creation of new ideas and new patterns; knowledge codification, and knowledge transfer, ensuring exchange of knowledge between individuals and departments. Neither of these process models are broad enough to allow for a complete analysis of organizational knowledge flow, omitting several important steps in the knowledge chain, such as acquiring and storing knowledge. Another model, proposed by Oluic-Vukovic ([2001](#olu01)) outlines 5 steps in the knowledge processing chain: gathering; organizing; refining; representing; and disseminating. This model covers more completely the range of activities involved in the organizational knowledge flow. It closely resembles information life-cycle processes suggesting again the interrelated aspects of IM and KM.

To analyze a number of KM initiatives, we needed a framework that could help us to compare the activities involved in these initiatives. We decided that identifying processes or group of activities would help in spite of the conceptual problems mentioned above. To develop a conceptual framework for our study, the processes of the last model have been altered slightly ([Figure 1](#fig1)) based on our reading of the KM literature. First, the "gathering" step has been separated into three different processes, each of which is distinct from the other: discovery, acquisition, and creation of knowledge. Second, the refining and representing processes have been omitted. Refining is not a major enough process in the knowledge flow, but merely one aspect of the knowledge creation step and knowledge representation generally falls within the scope of the storage and organization process. Third, a separate process of knowledge sharing has been added. This process actually replaces knowledge dissemination in the previous model, as sharing seems to be the terminology more commonly used in the KM field. Discovery involves locating internal knowledge within the organization. This process addresses the oft-quoted phrase, "if only we knew what we know". Large, non-hierarchical or geographically dispersed organizations find this knowledge gathering process especially helpful as one part of the organization may not be aware of the knowledge existing in its other parts. Acquisition involves bringing knowledge into an organization from external sources. The creation of new knowledge may be accomplished in several ways. First, internal knowledge may be combined with other internal knowledge to create new knowledge. And secondly, information may be analyzed to create new knowledge. This is adding value to information so that it is able to produce action. One example of this knowledge creation process is competitive intelligence. Technologies are useful at this stage because they can facilitate the creation of new knowledge through the synthesis of data and information captured from diverse sources ([Oluic-Vukovic, 2001](#olu01)).

After knowledge has been gathered, it must be stored and shared. Knowledge sharing involves the transfer of knowledge from one (or more) person to another one (or more). Knowledge sharing is often a major preoccupation with knowledge management and is frequently addressed in the literature. Not only most organizations abandon the idea that all knowledge should be documented, but they should also be ready to implement different methods for sharing different types of knowledge ([Snowden, 1998](#sno98)).

It is our contention that the focus of KM is not on the distribution nor the dissemination of knowledge but on its sharing. Although knowledge can be acquired at the individual level, to be useful it must be shared by a community, often described as a community of practice. For instance, if there is only one person knowing organizational rules and procedures, such rules and procedures would be useless and meaningless. On the other hand, rules and procedures emanate from communities and exist precisely to regulate group activities. Knowledge sharing is then crucial when new employees arrive and others quit. The management of information does not really focus on information sharing and is more oriented toward the control, preservation, and retention of information. One could also argue that the usefulness and the meaningfulness of information do not depend as much on its collective consumption or sharing: its individual consumption and use could be very effective from an organizational point of view. In fact, too much distribution of information can lead to information overload which could paralyze action. Knowledge sharing is perceived, for example, by the World Bank as critical for economic development and as an important next step going beyond the dissemination of information ([MacMorrow, 2001](#mac01)). In the end, the cycle of knowledge management is not complete nor successful if no efforts are made to ensure the use of stored and shared knowledge. On the other hand, the success of an IM project is achieved when the preservation and the retrieval of information is guaranteed while the success of a KM program ultimately depends on the sharing of knowledge ([Martensson, 2000](#mar00)).

<figure>

![Figure 1](../p141fig1.gif)

<figcaption><a id="fig1"></a>Figure 1: Conceptual framework: knowledge management processes</figcaption>

</figure>

## The study

The purpose of the study was to identify general trends in KM practices across several organizational types in order to gain insight into why and how organizations are practicing the management of knowledge. In particular, the goal was to determine six dimensions of KM initiatives:

1.  Stated goals and objectives;
2.  Types of knowledge being managed;
3.  Sources and the consumers of knowledge;
4.  Knowledge processes involved;
5.  Methodologies employed;
6.  And the technology used.

The research questions were the following:

1.  Are the six dimensions similar or different across organizations?
2.  Is there a fundamental difference in KM practices between organizations from the public sector and those from the private sector in terms of focus?
3.  What are the particular methodologies that characterized KM projects?

As an exploratory study, the methodology consisted to conduct a small number of case studies by using published materials on various organizations that undertook KM projects. Twelve cases were identified through a literature search of ABI Inform and LISA as well as an Internet search using the key phrase "knowledge management": 6 private sector organizations, and 6 public sector organizations. The public sector organizations include both governmental and intergovernmental institutions. Three selection criteria were used:

1.  Only the cases for which sufficient descriptive information was available were included;
2.  Only cases for which the information was published after 1998 were retained;
3.  For the private sector, only cases that were not related to the consulting business were considered.

Each case was reviewed and details of each area of interest were extracted and recorded. The data were then compared and analyzed. The case studies are not assumed to be an exhaustive examination of the KM activities of each organization. It is apparent that knowledge management practices are often performed under other designations and were therefore not identified in the literature review. And, presumably, many of these organizations practice KM in other areas not described in the publications used. A comprehensive study of KM practices within any organization would require collecting data from each organization, combined with observation and interviews, and ideally an ethnographic approach. However, there are few organizational initiatives that have been as well documented as KM projects. Organizations practicing KM are, not surprisingly, prone to document and share their experience: this seems to be consistent with the philosophy underlying KM. Therefore, in spite of the limitations of our methodology, it was possible to collect significant amount of information on each case.

The six organizations retained from the private sector are:

1.  Monsanto, a company devoted to agricultural products and chemicals used in consumer products;
2.  Hoffmann-LaRoche, a pharmaceutical company;
3.  Hewlett-Packard, involved in the computer industry;
4.  BP Amoco, active in the petroleum industry;
5.  Case Corporation, producing agricultural and construction equipment;
6.  Buckman Laboratories, another pharmaceutical company.

The six organizations retained from the public sector are:

1.  Canadian International Development Agency (CIDA);
2.  Health Canada;
3.  US Army Corps of Engineers (US Army);
4.  US Department of Navy (US Navy);
5.  World Bank;
6.  World Health Organization (WHO).

## Results and discussion

Our analysis is entirely based on the terminology used in the documentation, terminology that is not always clear in term of conceptual implications. The stated goals and objectives vary greatly from one organization to another but they all have in common the idea of increasing knowledge sharing ([Table 1](#tab1)). For all private sector organizations, the objectives were to facilitate the sharing of employee knowledge throughout the organization. For example, tacit knowledge is shared through communities of practice by making people working together or interacting in the workplace, explicit knowledge is made available through expert systems and by mapping experts and their knowledge resources. Pharmaceutical companies focus clearly on one objective related to time reduction for approving new products. For the public sector institutions, the objectives include sharing knowledge not only within the organization but also outside it with partners and the general public. Other objectives are raised and deal with knowledge creation, discovery, acquisition, application and accumulation ([Table 2](#tab2) for summary of each case numbered 1 to 12).



<table><caption><a id="tab1"></a>

**Table 1\. Comparison of stated goals and objectives**</caption>

<tbody>

<tr>

<th>Private Sector</th>

<th>Stated Goals and Objectives</th>

</tr>

<tr>

<td>Monsanto</td>

<td>

1.  Create and enable a learning and sharing environment
2.  Connect people with other knowledgeable people
3.  Connect people with information
4.  Enable the conversion of information to knowledge
5.  Encapsulate knowledge to facilitate its transfer
6.  Disseminate knowledge

</td>

</tr>

<tr>

<td>Hoffmann-LaRoche</td>

<td>

1.  Increase the speed of new product launches by harnessing the knowledge the company has;
2.  A three-month reduction in the time it takes to receive approvals on major new drugs
3.  Help product teams prototype the knowledge required for their new drug applications
4.  Produce a comprehensive map of the knowledge sources

</td>

</tr>

<tr>

<td>Hewlett-Packard</td>

<td>

1.  Improve knowledge sharing across units
2.  Facilitate knowledge sharing through informal networking
3.  Establish common language and management frameworks for KM
4.  Summarize knowledge across one business units

</td>

</tr>

<tr>

<td>BP Amoco</td>

<td>

1.  Connect individuals within the company to avoid re-inventing the wheel

</td>

</tr>

<tr>

<td>Case Corporation</td>

<td>

1.  Share business intelligence with employees
2.  Create a central repository for what they know about competitors, markets, their industry

</td>

</tr>

<tr>

<td>Buckman Laboratories</td>

<td>

1.  Accelerate the accumulation and dissemination of knowledge in the company (active in 80 countries)
2.  Facilitate the growth in the value of knowledge existing within the company
3.  Provide customers with maximum levels of support through sharing knowledge with customers

</td>

</tr>

<tr>

<th>Public Sector</th>

<th>Stated Goals and Objectives</th>

</tr>

<tr>

<td>CIDA</td>

<td>Support CIDA in becoming more knowledge-based learning organization through the creation, sharing and application of knowledge within CIDA and among CIDA and its partners</td>

</tr>

<tr>

<td>Health Canada</td>

<td>

1.  Use knowledge that resides in the organization to fulfill our mission which is to help people of Canada to maintain and improve their health
2.  Use knowledge for analysis and evidence-based decision making

</td>

</tr>

<tr>

<td>US Army</td>

<td>

1.  Support organization's strategic business plan
2.  Creation and maintain knowledge bases to improve effectiveness
3.  Increase ability of workforce to utilize knowledge bases
4.  Capitalize upon knowledge assets to develop core business processes

</td>

</tr>

<tr>

<td>US Navy</td>

<td>Facilitate the creation and sharing of knowledge for better decision making</td>

</tr>

<tr>

<td>World Bank</td>

<td>

1.  Create global strategy to share knowledge
2.  Increase the ease of accessibility to World Bank knowledge, both internally and externally
3.  Facilitate access to best global thinking and expertise on development

</td>

</tr>

<tr>

<td>WHO</td>

<td>

1.  Provide access to and sharing of the world's store of global health knowledge
2.  Improve and expand use of electronic access to that knowledge

</td>

</tr>

</tbody>

</table>

From the stated objectives, we were able to derive the following KM processes, in addition to knowledge sharing:

*   Knowledge creation through forums or combination of internal knowledge (Monsanto, Hewlett-Packard, Buckman Laboratories, CIDA, and US Army, US Navy, World Bank, WHO).
*   Knowledge discovery is also a part of most KM initiatives (Monsanto, Hoffmann-LaRoche, Hewlett-Packard, Case Corporation, Buckman Laboratories, Health Canada, US Army, US Navy, World Bank, and WHO). This is done mainly through internal search done by knowledge reporters like at Hewlett-Packard.
*   Knowledge acquisition was emphasized less, with only Monsanto for the private sector, and CIDA, Health Canada, and WHO in the public sector including the acquisition of external knowledge. What is meant by external knowledge is not always clear but we could assume that identifying and hiring external experts is an important dimension. It is not surprising to see that in public sector organizations, external knowledge is an area of concern since these agencies have a role to play in collecting knowledge within a country and worldwide (see [Table 2](#tab2) section on Stated goals and objectives). Knowledge acquisition also involves gaining new knowledge through formal activities by individuals. This type of acquisition was addressed only in 2 cases through organizational learning and email alerts (Case Corporation and Buckman Laboratories). It seems, therefore, that formal acquisition of knowledge is not a central aspect of KM initiatives despite the fact that KM and organizational learning are often seen as being closely related.
*   In most cases, knowledge storage was included in the practice of KM. Only BP Amoco and US Army do not use a particular type of databases or support for KM, with the emphasis being placed on sharing and presumably the knowledge is thought to be stored in employee's heads. Knowledge storage becomes an issue mainly when organizations seek to share explicit knowledge. For example, the outcomes of electronic question and answer forums are often saved and archived. Knowledge and expert databases are also implemented for facilitating access to explicit knowledge. One could argue that database can only store data, but knowledge and expert databases is the actual terminology used by organizations. Covin and Stivers (1997) found in their survey that organizations also refer to information systems for storing knowledge. We, then, face another conceptual problem: what is actually a database and an information system in the context of KM?
*   While knowledge needs may have been identified at some point in these cases, it was not apparent in the literature. Presumably it was not an emphasis in any of the organizations we studied. The fact that identification of knowledge needs was not emphasized in the case studies is disconcerting. This process is paramount for directing the rest of the knowledge processes within a KM initiative. This may be an area where information professionals can contribute their information auditing skills to greatly improve KM projects. One could wonder what is the difference between information and knowledge needs. We suggest that knowledge needs would address not only factual information that people should know but the also training and learning experiences that they should have for performing their tasks and responsibilities.
*   In the same vein, the case studies do not reveal any particular way to manage knowledge use, or method for assessing if all KM activities led to appropriate use of knowledge. We face, perhaps, the limitations of any management projects: it is very difficult to control human activities and especially how knowledge is used.

It is interesting to see that in most cases, specific types of knowledge were addressed in KM project. Health knowledge (Health Canada, WHO), development knowledge (World Bank), problem-solving knowledge (CIDA, US Army), employee expertise (Hewlett-Packard, BP Amoco), business or naval intelligence (Case Corporation, US Navy) were central. Although most organizations try to tackle tacit and explicit knowledge, they do it with a specific focus. Interestingly, cultural knowledge ([Choo, 1998a](#cho98a)) was not mentioned anywhere, as if it was not relevant or even considered. Given the enormous focus on organizational culture in the management literature during the 1980s, this is somewhat puzzling to see no attempt to share that type of knowledge. In all cases, employees are the sources of knowledge as well as the main consumers of knowledge. However, some organizations use external sources as mentioned above, and other seek to facilitate the consumption of knowledge by customers (Case Corporation) and partners or external units (CIDA, US Navy, World Bank, WHO).



<table><caption><a id="tab2"></a>

**Table 2: Summary of results**</caption>

<tbody>

<tr>

<th colspan="3">Stated goals and objectives; types of knowledge; and sources and consumers of knowledge</th>

</tr>

<tr>

<td colspan="2">

**Private Sector**

1.  Internal knowledge creation and sharing
2.  Knowledge discovery and sharing internally
3.  Internal knowledge sharing
4.  Internal knowledge sharing of drug approval process
5.  Internal sharing of business intelligence
6.  Knowledge accumulation, creation and sharing internally and externally

</td>

<td>

**Public Sector**

*   Creation of internal knowledge; sharing and application of knowledge internally and externally.
*   Application and sharing of internal and external knowledge
*   Knowledge sharing and application

1.  Creation and sharing of knowledge internally
2.  External knowledge acquisition and sharing internally and externally
3.  External knowledge acquisition and sharing

</td>

</tr>

<tr>

<th colspan="3">Methodologies employed</th>

</tr>

<tr>

<td>

**Private Sector**

1.  Communities of practice; knowledge mapping
2.  Knowledge mapping; question and answer forum; knowledge database; best-practice repository
3.  Question and answer forum; knowledge database; expert database; network news for customers
4.  Expert database
5.  Knowledge database; new information alerts
6.  Question and answer forums; knowledge database; and learning center

</td>

<td width="51%" colspan="2" height="218">

**Public Sector**

*   Various question and answer/sharing experience forums
*   Knowledge mapping; knowledge database
*   Communities of practice; knowledge mapping
*   Knowledge database; collaborative technologies
*   Communities of practice
*   Question and answer forum; knowledge database; new information alerts

</td>

</tr>

<tr>

<th colspan="3">Technologies involved</th>

</tr>

<tr>

<td>

**Private Sector**

1.  Web-based intranet
2.  Web-based intranet
3.  Web-based intranet, Lotus-Notes database
4.  Web-based intranet
5.  Microsoft mail for email news alerts; Verity's client/server-based Topic Enterprise Server and Topic Agents software.
6.  Compuserve bulletin boards

</td>

<td colspan="2">

**Public Sector**

*   Internet and extranet
*   Web-based intranet; Lotus-Notes database and email
*   Web-based intranet, portals
*   Intranet, Teleconferencing, Satellite Broadcasting and Cable TV
*   Intranet
*   Internet, databases, and email

</td>

</tr>

</tbody>

</table>

The particular methodologies used in each organization are summarized in [Table 3](#tab3). The methodologies are overwhelmingly designed to provide or facilitate the sharing of tacit knowledge and attempts to codify tacit knowledge were few. Communities of practice, question and answer forums, and expert databases, all of which facilitate tacit knowledge sharing, were used in eleven of the twelve case studies. A much smaller emphasis was placed on sharing explicit knowledge. The only methodology used for sharing explicit knowledge was the "knowledge database". These contain explicit knowledge such as previous problem solving techniques and best practices that were codified through interviews or reports. The diversity of methodologies reveal the various strategies that are used in KM projects to fit particular organizational needs and to support specific missions. Some strategies are very sophisticated, involving for example, different knowledge agents playing active roles in KM (knowledge stewards and cross-pollinators at Monsanto), or complex expert database (Hoffmann-LaRoche, Hewlett-Packard) while some methodologies are based on simple interactions (forums). It is important however to recognize how these methodologies involve managing information and knowledge as well as handling particular organizational processes (launching products, maintaining customer relations, building networks). All the case studies required the use of particular technologies facilitating exchanges and collaborative work, involving collective consumption and use of knowledge (see summary of technologies in [Table 2](#tab2)). The study found that companies rely heavily on technology to facilitate knowledge sharing. Despite the fact that knowledge culture is widely accepted to be a significant factor in KM, person to person exchanges were not mentioned as being particularly central in any of the strategies. An important trend is the development of subject specific portals via an Intranet. Interestingly, despite the increasing availability of knowledge management software, none of the technologies used was specifically designed for knowledge management applications, but were adapted for use in the initiatives.



<table><caption><a id="tab3"></a>

**Table 3: Comparison of reported methodologies**</caption>

<tbody>

<tr>

<td>

**Private Sector**</td>

<td>

**Reported Methodologies**</td>

</tr>

<tr>

<td>

Monsanto

</td>

<td>

*   Communities of practice involving "knowledge stewards", "topic experts" and "cross-pollinators"
*   Web of knowledge teams that create and maintain a guide to the company's knowledge

</td>

</tr>

<tr>

<td>

Hoffmann-LaRoche

</td>

<td>

Knowledge Map: directory that point people who need knowledge to the places where it can be found. It has 3 parts:

*   Question Tree: for each question that must be answered in order to receive drug approval, there are numerous sub-questions. These questions form the structure of the knowledge map and point users to areas where the knowledge to answer these questions may be attained;
*   Knowledge Links: set of guidelines that instruct knowledge providers as to when and with whom they should be sharing their knowledge;
*   Best-practice repository.

</td>

</tr>

<tr>

<td>

Hewlett-Packard

</td>

<td>

*   "Trainer's Trading Post": discussion database for training topics;
*   Connex: directory of experts with their profiles;
*   Knowledge Links: database of product development knowledge collected through interviews with experts;
*   HP Network News: a dial-up database for HP customers that contains frequently asked questions.

</td>

</tr>

<tr>

<td>

BP Amoco

</td>

<td>

Connect: expert database where staff create their homepage outlining their expertise, affiliations, etc.

</td>

</tr>

<tr>

<td>

Case Corporation

</td>

<td>

*   Real-time news emails to key decision-makers;
*   Case Knowledge Base: knowledge base containing electronic documents. One knowledge gatekeeper is appointed epr continent to add, delete and classify content.

</td>

</tr>

<tr>

<td>

Buckman Laboratories

</td>

<td>

KNetix, The Buckman Knowledge Network including:

*   Open-Place Forums: to allow employees to post messages, questions, requests, e.g. System operators monitor discussions to ensure that all questions are answered and 2 industry experts help providing answers;
*   Customer Information Centers: database containing all the information available at the company about each customer;
*   Learning Center: associates manage their own continuing education through courses offered by the center.

</td>

</tr>

<tr>

<td>

**Public Sector**</td>

<td>

**Reported Methodologies**</td>

</tr>

<tr>

<td>

CIDA

</td>

<td>

*   Partners Forum: to exchange information with partners and to produce information for sharing;
*   Field Representative Forum: provides a platform for exchange on solutions between staff;
*   Strategic Management Information Forum: to share experiences across different programmes;
*   Regional Forum: to allow branches to share knowledge.

</td>

</tr>

<tr>

<td>

Health Canada

</td>

<td>

*   Federal Budget Database: lists health related project outlined in the budget;
*   Orientation Database: a one-stop shop for general corporate information;
*   Knowledge Map: to point to where the knowledge and information resides in the organization.

</td>

</tr>

<tr>

<td>

US Army

</td>

<td>

Creation of Knowledge Communities: to bring together those working on a common set of activities or mission;

*   Creation of Communities of Practice: to bring together people across departmental lines, may cross several mission areas;
*   Knowledge Map: to identify and categorize an organization's content with specific terminology and taxonomy that acts as a guide to the information resources.
*   Knowledge Audit: technique used to create the knowledge map.

</td>

</tr>

<tr>

<td>

US Navy

</td>

<td>

*   Knowledge Home Port-portal: links together 250 databases
*   Virtual Program Office: collaborative technologies combined with intelligent agent for planning, program development, acquisitions, etc.
*   Virtual Naval Hospital: deliver expert medical information to providers and patients.

</td>

</tr>

<tr>

<td>

World Bank

</td>

<td>

*   Communities of practice: e.g. OneWorld Online, a gateway for the public into issues of sustainable development. Links 250 partners.
*   There are hundreds of other small KM initiatives.

</td>

</tr>

<tr>

<td>

WHO

</td>

<td>

Many initiatives are taking place, among them:

*   FluNET: a surveillance system linking 100 National influenza Centers in 82 countries
*   Anti-microbial Resistance Network: use of a software for training and support for the management of anti-microbial efforts.
*   Action Programme on Essential Drugs: to support the programme, Internet is used to exchange expert knowledge worldwide

</td>

</tr>

</tbody>

</table>

In order to illustrate the particular nature of KM, we propose a typology of 8 distinct methodologies. These methodologies, which were identified in the case studies, are categorized according to their particular focus:

### Focus on communication

1.  Communities of Practice - bring people together, often from different departments, to share ideas. This methodology involves the process of sharing tacit knowledge and development of informal networking (e.g.: Monsanto, Hewlett-Packard, US Army, World Bank).
2.  Question and Answer Forums - bring people together, often geographically dispersed, but with similar jobs, usually through email or chat rooms, to solve problems. This methodology involves the sharing of tacit knowledge and also storage of knowledge as the exchanges are usually archived for future use (e.g.: Buckman Laboratories, CIDA)

### Focus on storage and retrieval

1.  Knowledge Mapping - performing an audit to discover the knowledge resources within an organization, as well as the development of a guide for employees describing and providing location information for these knowledge resources. This methodology involves the discovery of tacit knowledge in order to facilitate eventual sharing (e.g.: Monsanto, Hoffmann-LaRoche, Health Canada, US Army).
2.  Expert Databases- similar to mapping of knowledge, this maps experts by identifying knowledge of each expert and providing a guide map to help employees find those experts. This methodology may involve discovery if performed by others and may just facilitate the sharing of tacit knowledge if, as in many cases, it is up to the employees to provide his of her own expert profile (e.g.: Hewlett-Packard, BP Amoco).
3.  Knowledge Databases- explicit knowledge is stored in databases similar to standard document databases. This methodology facilitates the storage and sharing of explicit knowledge (e.g.: Hoffmann-LaRoche, Hewlett-Packard, Case Corporation, Buckman Laboratories, WHO).

### Focus on selected dissemination

1.  News Information Alerts- provide for the distribution of selected information and explicit knowledge (e.g.: Case Corporation, WHO).
2.  Organizational Learning- acquisition of new knowledge by individuals through training, continuing education (e.g.: Buckman Laboratories).

### Focus on action

1.  Virtual collaboration – enable people from various areas to work together (e.g.: US Navy).

This typology helps, in spite of its limitation, to grasp some aspects of the particular nature of KM as opposed to IM. First, the strategies deal with various organizational processes that are intertwined with KM processes. Second, it suggests that knowledge managers are concerned with the dynamic dimension of knowledge, not only with data, information and technology. Finally, this typology may be useful to derive the competencies that should be addressed in a KM curriculum.

Upon embarking on this study, we expected to find several differences in KM practices between public sector and private sector institutions. When we compare the private sector to the public sector, we see that objectives of KM initiatives are more often smaller in scope and more precise in the private sector while public sector initiatives are more organization-wide. Hundreds of KM strategies are currently being practiced and reported in the literature and the identification of trends by looking at just twelve cases may not be possible. However, one difference is significant enough to be noted: private sector organizations use KM to facilitate the internal knowledge sharing process, while public sector organizations seek to share knowledge, acquired either internally or externally, with their external partners or with the general public. This makes sense, since private institutions have a stake in protecting their knowledge resources, while public sector institutions have as their mandates to serve the public.

In almost all cases, more than one knowledge process was managed, but none of KM initiatives attempts to manages all six processes outlined in our conceptual framework. For instance, despite the fact that knowledge creation was closely related to the goals and objectives of the initiatives, no organization focused specifically on the knowledge creation process. This suggests that there is an underlying assumption that knowledge creation, either through adding to existing knowledge or analyzing information to create new knowledge, is the outcome of knowledge sharing. The fact that knowledge sharing, of all processes outlined, was the overwhelming focus for both the objectives and the methods employed suggests that this is the most important process being addressed in knowledge management at the current time. This makes sense, in that other processes such as knowledge acquisition and creation, might be considered to be part of other organizational areas such as training or research & development and knowledge storage and organization is considered a part of information management.

The organizations in the study did not explicitly define internal and external knowledge, and not always the various methodologies that they used. However, the fact that the type of the knowledge targeted in most initiatives was tacit knowledge suggest that most organizations subscribe to the model of Nonaka and Takeuchi ([1995](#non95)), leading to the conclusion that KM focuses mainly on tacit and undocumented knowledge while IM remains presumably responsible for managing information and explicit knowledge. It would be interesting to investigate the state of IM programs where there are KM projects and to see whether KM and IM are somewhat separate and perform fairly unrelated management tasks or whether they are both interrelated as some authors have suggested ([Kakadbase et al., 2001](#kak01)).

## Conclusion

The field of knowledge management is fairly new, and this explains why its research base is still under development. Despite the vagueness of KM, its potential overlaps with IM, and its weak theoretical base, KM is practiced in many organizations. Examining empirical evidence is certainly a valid approach for identifying building blocks of theories and concepts to support the development of new scientific fields. Indeed, scientific knowledge is often rooted in practice: culture and society existed before we had anthropology and sociology. The empirical evidence that was gathered for this study shows that KM involves human/soft and technical/hard aspects ([Hlupic et al., 2002](#hlu02)). KM seems to be made of various organizational practices requiring changes in policies, work routines and organizational structures. More specifically, our exploratory study allows a number of concluding remarks about knowledge management:

1\. Knowledge, in practice, is most often defined as tacit knowledge in spite of the conceptual problems mentioned above. Explicit knowledge was included only in those initiatives where the focus was converting tacit knowledge into explicit knowledge.

2\. Knowledge management, as it is practiced, really means facilitating the sharing of tacit knowledge. Despite the fact that other processes were part of the KM projects, sharing was the primary emphasis of all case studies.

3\. There are slight differences in the practices between private and public sector knowledge management. Private sector organizations use KM for internal knowledge sharing, targeted in specific areas of the organization and the KM initiatives are most often concerned with managing business and administrative knowledge. Public sector organizations use KM for both internal and external knowledge sharing throughout the organization and the KM initiatives are most often concerned with managing product-related knowledge.

4\. And finally, KM practices could benefit from the skills already held by information professionals. These skills include the identification of knowledge needs, helping to distinguish between information and knowledge, and will facilitate a broader and more inclusive KM initiative.

One can claim that the ontological and epistemological aspects of knowledge are still so ill-defined and poorly understood that KM cannot be an emergent discipline. And, indeed, although the concepts of tacit and explicit knowledge, knowledge sharing and knowledge technologies are often used, they are not clearly defined. However, the question remains why large private and public organizations bother to use unclear terminologies? Why do they want to be associated with an ill-defined field? The IM community cannot continue to claim that it has addressed for years the same issues addressed now by KM experts. Dismissing KM as simply a management fad could be a missed opportunity to understand how knowledge is developed, gained and used in organizations, and ultimately in society. New labels can be misleading but they can also force some reflections. The aim of this article is simply to suggest that there is a need to examine why there is such an interest for KM in both the academic, business communities, and governments.

## References

*   <a id="abr97"></a>Abram, S. (1997) "Post information age positioning for special librarians: is knowledge management the answer?" _Information Outlook_, **1**(6), 18-25.
*   <a id="ant00"></a>Antonelli, C., Geuna, A. and Steinmueller, W. E. (2000) "Information and communication technologies and the production, distribution and use of knowledge". _International Journal of Technology Management_, **20**, 72-94.
*   <a id="arg00"></a>Argote, L. and Ingram, P. (2000). "Knowledge transfer: a basis for competitive advantage in firms." _Organizational Behavior & Human Decision Processes_, **82**(1), 150-169.
*   <a id="bec99"></a>Beckman, T.J. (1999) "The current state of knowledge management." In: _Knowledge management handbook,_ edited by J. Liebowitz. pp.1.1-1.22. NY: CRC Press.
*   <a id="ben00"></a>Bennet, A. (2000) "[Knowledge management: unlocking the potential of our intellectual capital.](http://www.chips.navy.mil/archives/00_jan/km.htm)" _Chips Magazine_, Winter. Available at: http://www.chips.navy.mil/archives/00_jan/km.htm [Site visited 20th September 2002]
*   <a id="boi98"></a>Boisot, M. (1998) _Knowledge assets: securing competitive advantage in the information economy_. New York, NY: Oxford University Press.
*   <a target="_blank" id="bri02"></a>British Columbia Archives (2002) _[About information management](http://www.bcarchives.gov.bc.ca/infomgmt/infomgmt.htm)._ Victoria, BC: British Columbia Archives. Available at: http://www.bcarchives.gov.bc.ca/infomgmt/infomgmt.htm [Site visited 21 September 2002]
*   <a id="bro98"></a>Broadbent, M. (1998) "The phenomenon of knowledge management: what does it mean to the information profession. " _Information Outlook_. **2**(5), 23-36.
*   <a id="bro98-2"></a>Brown, J.S. and Duguid, P. (1998) "Organizing knowledge". _California Management Review_. **40**(3), 90-111.
*   <a id="cha98"></a>Chase, R. L. (1998) "Knowledge navigators." _Information Outlook,_ **2**(9), 17-20.
*   <a id="cho98a"></a>Choo, C. W. (1998a) _The knowing organization: how organizations use information to construct meaning, create knowledge, and make decisions_. New York: Oxford University Press.
*   <a id="cho98b"></a>Choo, C.W. (1998b) _Information management for the intelligent orgnization: the art of scanning the environment_. Medford, NJ: Information Today.
*   <a id="col99"></a>Collison, C. (1999) Connecting the new organization: how BP Amoco encourages post-merger collaboration. _Knowledge Management Review_, **7**, 1-4\. Web site [www.km-review.com](http://www.km-review.com/)
*   <a id="con81"></a>Connell, J.J. (1981) "The fallacy of information resource management." _Infosystems_, **28**(5), 78-84.
*   <a id="cor99"></a>Cortada, J.W. and Woods, J.A., _eds._ (1999) _Knowledge management yearbook 1999_. Boston: Butterworth-Heinemann.
*   <a id="cov97"></a>Covin, T.J. and Stivers, B. (1997) "Knowledge management focus in US and Canadian firms" _Creativity and Innnovation Management,_ **6**(3), 140-150.
*   <a id="cro85"></a>Cronin, B. (1985) "Introduction", in: Information management: from strategies to action, edited by B. Cronin.  pp. vii-ix. London: Aslib.
*   <a id="dav96"></a>Davenport, T. (1996) _[Hewlett Packard promotes knowledge management initiatives](http://webcom.com/quantera/HP.html)_. Austin, TX: Quantum Era Communications. Available at: http://webcom.com/quantera/HP.html [Site visited 21 September 2002]
*   <a id="dav98a"></a>Davenport, T. (1998) _[Knowledge management at Hewlett-Packard, early 1996](http://www.bus.utexas.edu/kman/hpcase.htm)._ Austin, TX: University of Texas. Available at: www.bus.utexas.edu/kman/hpcase.htm [Site visited 20th September 2002]
*   <a id="dav98b"></a>Davenport, T. and Prusak, L. (1998) _Working knowledge: how organizations manage what they know._ Cambridge, MA: Harvard Business School Press.
*   <a id="eat91"></a>Eaton, J.J. and Bawden, D. (1991) "What kind of resource is information?" _International Journal of Information Management_, **11**, 156-165.
*   <a id="ell97"></a>Elliott, S. (1997) "Case Corporation's pilot effort proves value of knowledge management." _Knowledge Management in Practice_, **10**, 1-8.
*   <a id="ful99"></a>Fulmer, W.E. (1999) _[Harvard Business School case study on knowledge sharing in Buckman Laboratories](http://www.knowledge-nurture.com/web/bulabdoc.nsf/By+Title/8A906ABDAEFF6A6D8625680800520F59/$FILE/HBS.pdf). Memphis, TN: Buckman Laboratories._ Available at: http://www.knowledge-nurture.com/web/bulabdoc.nsf/By+Title/8A906ABDAEFF6A6D8625680800520F59/$FILE/HBS.pdf [Site visited 24th September 2002]
*   <a id="gou00"></a>Gourlay, S. (2000) "[Frameworks for knowledge: a contribution towards conceptual clarity for knowledge management](http://bprc.warwick.ac.uk/km013.pdf)". Paper delivered at: _Knowledge management: concepts and controversies conference, Warwick University, 10-11 February 2000_. Available at: http://bprc.warwick.ac.uk/km013.pdf [Site visited 21 September 2002]
*   <a id="hea01"></a>Health Canada. (2001) _[Knowledge management: welcome to Health Canada's knowledge management web site!](http://www.hc-sc.gc.ca/iacb-dgiac/km-gs/english/kmhome.htm)_ Ottawa: Health Canada. Available at: http://www.hc-sc.gc.ca/iacb-dgiac/km-gs/english/kmhome.htm [Site visited 24th September 2002]
*   <a id="hen01"></a>Henczel, S. (2001) "The information audit as a first step towards effective knowledge management." _Information Outlook_, **5 (6)**, 48-62.
*   <a id="hlu02"></a>Hlupic, V., Pouloudi, A. and Rzevski, G. (2002) "Towards an integrated approach to knowledge management: 'hard', 'soft', and 'abstract' issues." _Knowledge and Process Management_, **9**(2), 90-102.
*   <a id="hul00"></a>Hull, R , Coombs, R. Peltu, M. (2000) "Knowledge management practices for innovation: an audit tool for improvement." _International Journal of Technology Management_, **20**, 633-656.
*   <a id="jun00"></a>Junnarkar, Bipin. (2000) "[Creating fertile ground for knowledge at Monsanto](http://www.cbi.cgey.com/journal/issue1/features/creati/creating.pdf)". _Perspectives on Business Innovation_. Issue 1\. [Cap Gemini Ernst and Young publication] Available at: http://www.cbi.cgey.com/journal/issue1/features/creati/creating.pdf [Site visited 21st September 2002]
*   <a id="jur00"></a>Jurisica, I. (2000) "[Systematic knowledge management and knowledge discovery.](http://www.asis.org/Bulletin/Oct-00/jurisica.html)" _Bulletin of the American Society for Information Science_, **27** (1), 9-12\. Available at: http://www.asis.org/Bulletin/Oct-00/jurisica.html [Site visited 24th September 2002
*   <a id="kak01"></a>Kakabadse N.K., Kouzmin, A. and Kakabadse A. (2001) "From tacit knowledge to knowledge: leveraging invisible assets." _Knowledge and Process Management_, **8**(3), 137-154.
*   <a id="kno00"></a>Knowledge Inc. (2000). _[The knowledge enterprise](http://knowledgeinc.com/ICAP.html). [Case studies and solutions]_ Austin, TX: Quantum Era Communications. Available at: http://knowledgeinc.com/ICAP.html">http://knowledgeinc.com/ICAP.html [Sited visited 24th September 2002]
*   <a id="koe97"></a>Koenig, M.E.D. (1997) "Intellectual capital and how to leverage it." _The Bottom Line: Managing Library Finances_, **10**(3), 112-118.
*   <a id="kpm00"></a>KPMG Consulting (2000). _[Knowledge management research report](http://www.kpmgconsulting.co.uk/research/othermedia/wf_8519kmreport.pdf)_. London: Atos KPMG Consulting. Available at: http://www.kpmgconsulting.co.uk/research/othermedia/wf_8519kmreport.pdf [Site visited 24th September 2002]
*   <a id="lue01"></a>Luen, T.W. and Al-Hawamdeh, S. (2001) "Knowledge management in the public sector: principles and practices in police work." _Journal of Information Science_, **27**, 311-318.
*   <a id="maa00"></a>Maas, J. (2000) "Common knowledge: how companies thrive by sharing what they know." _Sloan Management Review_, **41**(4), 105-106.
*   <a id="mac01"></a>MacMorrow, N. (2001) "Knowledge management: an introduction", in: _Annual Review of Information Science and Technology,_ edited by M.E. Williams. pp. 381-422\. Medford, NJ: Information Today.
*   <a id="mah01"></a>Mahdjoubi, D. and Harmon, G. (2001) "Knowledge management: a conceptual platform for the sharing of ideas," in: _Proceedings of the 64<sup>th</sup> ASIST Annual Meeting, Washington, DC 2001_. pp. 290-304.  Medford, NJ: Information Today.
*   <a id="mar00"></a>Martensson, M. (2000). "A critical review of knowledge management as a management tool." _Journal of Knowledge Management_, **4**, 204-216.
*   <a id="mar00-2"></a>Martin, B. (2000). _[Knowledge based organizations: emerging trends in local government in Australia](http://www.tlainc.com/articl16.htm)._ Journal of Knowledge Management Practice,**2** October. Available at: http://www.tlainc.com/articl16.htm [Site visited 21 September 2002]
*   <a id="mea00"></a>Meadow, C. T., Boyce, B.R. and Kraft, D.H.(2000) _Text information retrieval systems_, 2<sup>nd</sup> ed. San Diego, CA: Academic Press.
*   <a id="mit00"></a>Mitchell, K D. (2000) "Knowledge management: the next big thing." _Public Manager_, **29 (2)**, 57-60.
*   <a id="mut00"></a>Mutch, A. (2000) "Managing knowledge, experts, agencies and organizations." _Journal of Management Studies_, **37**, 473-475.
*   <a id="non95"></a>Nonaka, I. and Takeuchi, H. (1995) _The knowledge creating company: how Japanese companies create the dynamics of innovation_. New York: Oxford University Press.
*   <a id="olu01"></a>Oluic-Vukovic, V. (2001) "From information to knowledge: some reflections on the origin of the current shifting towards knowledge processing and further perspective." _Journal of the American Society for Information Science and Technology_, **52**, 54-61.
*   <a id="oxb02"></a>Oxbrow, N. and Abell, A. (2002) "Is there life after knowledge management." _Information Outlook_, **6**(4), 20-29.
*   <a id="pla82"></a>Place, I. and Hyslop, D.J. (1982) _Records management: controlling business information._ Reston, VI: Reston.
*   <a id="pol62"></a>Polanyi, M. (1962) _Personal knowledge_. Chicago, IL.: University of the Chicago Press.
*   <a id="san00"></a>Sanderson, S, Nixon, M., Adrian W. and Aron, A. J. (2000) "Adding value to a company's selling activity through knowledge management: a case study." _International Journal of Technology Management_, **20**, 742-751.
*   <a id="sko00"></a>Skok, W. (2000) "Managing knowledge within the London taxi cab service." _Knowledge and Process Management_, **7**(4), 224-232.
*   <a id="sno98"></a>Snowden, D. (1998) "A framework for creating a sustainable knowledge management programme", in: _The knowledge management yearbook 1999-2000_, edited by J.W. Cortada and J.A. Woods. pp. 52-64\. Oxford: Butterworth Heinemann
*   <a id="spe00"></a>Spender, J-C. (2000) "Tacit knowledge in organizations." _Academy of Management Review_, **25**, 443-446.
*   <a id="str99"></a>Streatfield, D. and Wilson, T.D. (1999) "Deconstructing knowledge management." _Aslib Proceedings_, **51**(3), 67-72.
*   <a id="sri00"></a>Srikanntaiah, T.K. and Koenig, M.E.D. (2000) _Knowledge management for the information professional_. Medford, NJ: Information Today.
*   <a id="tan00"></a>Tan, Jeffrey. (2000) "Managing knowledge-how to do it - a practical case study". _British Journal of Administrative Management_, **19**, 12.
*   <a id="uni98"></a>United States. _Department of Agriculture_ (1998). _[Research, Education, and Economics Information System](http://www.reeusda.gov/ree/reeis/reeover.htm)_. Washington, DC: Department of Agriculture. Available at: http://www.reeusda.gov/ree/reeis/reeover.htm [Site visited 21 September 2002]
*   <a id="usa01"></a>United States. _Army. Corps of Engineers_. (2001) _[USACE knowledge management (KM) strategic plan](http://www.usace.army.mil/ci/km/kmstratplan.html)_. Washington, DC: Army Corps of Engineers. Available at: http://www.usace.army.mil/ci/km/kmstratplan.html [Site visited 21 September 2002]
*   <a id="usd99"></a>United States. _Department of the Army_. (1999). _[AKO [Army Knowledge Online]. The Army portal](https://www.us.army.mil/portal/portal_home.jhtml)._. Washington, DC: Department of the Army. Available at: https://www.us.army.mil/portal/portal_home.jhtml [Site visited 24th September 2002 - access beyond the home page requires password]
*   <a id="wii89"></a>Wiig, K.M. (1999) "Introducing knowledge management into the entreprise", in: _Knowledge management handbook_, edited by J. Liebowitz. pp.3.1-3.41\. NY: CRC Press.
*   <a id="wil89"></a>Wilson, T.D. (1989) "Towards an information management curriculum." _Journal of Information Science_, **15**, 203-210.
*   <a id="wor98"></a>World Bank (1998). _What is Knowledge Management?_ Available at: http://www.worldbank.org/ks/html/pubs_pres_what.html [Site visited 17 December 2000 - No longer available 21 September 2002]
*   <a id="yat02"></a>Yates-Mercer, P. and Bawden, D.(2002) "Managing the paradox: the valuation of knowledge and knowledge management." _Journal of Information Science_, **28**, 19-29.