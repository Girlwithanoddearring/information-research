#### Information Research, Vol. 8 No. 1, October 2002

# Knowledge management: another management fad?

#### [Leonard J. Ponzi](mailto:LenPonzi@SolutionsInResearch.com)  
Doctoral Candidate,
College of Information and Computer Science,
Long Island University,
New York, USA

#### [Michael Koenig](mailto:MKoenig@LIU.edu)  
Professor & Dean,
Palmer School of Library and Information Science,
Long Island University,
New York, USA

#### **Abstract**

> Knowledge management is a subject of a growth body of literature. While capturing the interest of practitioners and scholars in the mid-1990s, knowledge management remains a broadly defined concept with faddish characteristics. Based on annual counts of article retrieved from _Science Citation Index_, _Social Science Citation Index_, and _ABI Inform_ referring to three previous recognized management fad, this paper introduces empirical evidence that proposes that a typical management movement generally reveals itself as a fad in approximately five years. In applying this approach and assumption to the case of knowledge management, the findings suggest that knowledge management is at least living longer than typical fads and perhaps is in the process of establishing itself as a new aspect of management. To further the understanding of knowledge management's development, its interdisciplinary activity and breadth are reported and briefly discussed.  
>   
> **_Note:_** This working paper is preliminary data associated with the first author's dissertation research on the emergence of knowledge management.

## Introduction

Starting in 1995 there has been an explosion in the literature surrounding the developing concept of knowledge management. Today, hardly anyone can attend a conference or read a journal without seeing literature referring to the concept. Despite its popularity, the jury is still out as to whether knowledge management will become a significant and permanent component of management, or just another management fad.

The concept has been defined broadly with a number of definitions being touted. For example, [Ponelis and Fair-Wessels](#pon98) (1998) assert that knowledge management is a new dimension of strategic information management. [Davenport and Prusak](#dav98) (1998) claim that knowledge management is the process of capturing, distributing, and effectively using knowledge. [Skyrme](#sky) (1997) suggests that knowledge management is the explicit and systematic management of vital knowledge along with its associated processes of creating, gathering, organizing, diffusing, using, and exploiting that knowledge.

This paper's objective is not to provide another knowledge management definition but to illuminate its current state of development. It uses annual frequency counts of articles on three well-known fads to demonstrate that management fads generally peak in approximately five years. Applying this technique to the concept of knowledge management allows the paper to shed light on the field as a whole.

## Analytical framework for examining management fads

In this section, we present empirical evidence that management fads generally peak in approximately five years. We provide support by applying the simple bibliometric technique of article counting to three well-known management fads.

A management fad can be considered an innovative concept or technique that is promoted as the forefront of management progress and then diffuses very rapidly among early adopters eager to gain a competitive advantage. After organizational leaders come to the realization that the concept has fallen short of its expected benefits, the concept is quickly discontinued or drops back to very modest usage.

The graphing of article-counts annually is a bibliometric technique that determines how many articles have been devoted to a given concept over time. The rationale for this method is that bibliographic records are a relativety objective indicator for measuring discourse popularity. In other words, the higher the article counts, the larger the volume of discussion.

The initial result of the article-counting technique is time-series data that can be charted into a lifecycle ([Abrahamson & Fairchild, 1999](#abr99)). The most well-known lifecycle shape is an S-curve. It depicts an ideal representation for the emergence, growth, maturity, and decline of an idea or product. In reality, however, not all ideas and products exhibit an S-shaped lifecycle ([Rogers, 1995](#rog95)). Our concern is the lifecycles of fads and fashions. As illustrated in Figure 1, fads emerge quickly and are adopted with great zeal, then peak and decline just as fast. Fashions, on the other hand, are fads that briefly show signs of maturity before declining ([Wasson, 1978](#was78)).

<figure>

![Figure 1](../p145fig1.gif)

<figcaption>Figure 1: Fad & fashion lifecycles (Source: [Wasson 1978](#was78))</figcaption>

</figure>

The theory of management fashion primarily draws from the work of Eric Abrahamson. [Abrahamson's](#abr91) (1991, 1996) theory describes the process by which "fashion setters," or fashion evangelists, which are generally consulting firms, management gurus, mass-media publications, and business schools, disseminate beliefs that certain management techniques are at the forefront of management progress.

Once information is published in the form of articles, annual counts can be captured to provide time-series data that can be charted and analyzed. Based on the work of [Abrahamson](#abr91) (1991, 1996) and [Abrahamson & Fairchild](#abr99) (1999), the bibliometric technique of article counting is a reliable analytical approach to begin an analysis of the published literature in order to illuminate and trace the development of a concept.

In recent years, the academic and industry communities have observed numerous management fads - for example, Quality Circles, Total Quality Management, and Business Process Reengineering ([Hilmer & Donaldson, 1996](#hil96)). The Quality Circles movement is graphed below for illustration.

### Quality Circles

In the early 1980s, Quality Circles became of interest to American manufacturers as a competitive tool in response to the quality gap with Japan. This management technique theorized the importance of organizational goals to achieve greater quality and labour productivity.

A literature review shows that between 1980 and 1982, 90% of the Fortune 500 companies had adopted the Quality Circles management approach ([Lawler & Mohrman, 1985](#law85)). Afterwards, a survey conducted by [Castorina and Wood](#cas88) (1988) revealed that more than 80% of the Fortune 500 companies that originally adopted Quality Circle programs in the early 1980s had abandoned them by 1987.

In 1996, Abrahamson created a Quality Circles lifecycle that independently confirms the literature's claims that the Quality Circle movement was indeed a management fad. Retrieving article counts from ABI Inform, Abrahamson graphed a ten-year trend line representing articles that include the phrase 'Quality Circles' in either the title or abstract.

Abrahamson's results revealed that the Quality Circles movement to have a bell-shape pattern. The pattern depicts a rapid growth starting in 1978 and then reversing in 1982 (see Figure 2). By 1986, this measure returned to its pre-popularity levels, which indicates a management fad.

<figure>

![Figure 2](../p145fig2.gif)

<figcaption>Figure 2: The lifecycle of quality circles, 1977-1986 (Source: [Abrahamson](#abr96), 1996)</figcaption>

</figure>

Furthermore, we also observed that Quality Circles' momentum peaked in five years and we wanted to know if this time period was consistent in other management fashions. To test this proposition, we developed lifecycles with an accepted management fad and a management fashion, namely, Total Quality Management and Business Process Reengineering.

### Total Quality Management & Business Process Reengineering

Total Quality Management and Business Process Reengineering were quality movements that became popular in the 1980s and 1990s. To date, neither lifecycle has been charted from a bibliometric perspective.

To capture a broader lifecycle image than Abrahamson's work, article counts were retrieved on March 16, 2002 from three DIALOG files: Science Citation Index (File 34), Social Science Citation Index (File 7), and ABI Inform (File 15). (See [Appendix](#app) for Dialog search strings and commands) These files were selected because of their comprehensive and broad coverage of the academic and industry literature.

After counts were captured annually by querying for each key phrase located in the title, abstract or descriptor fields and duplicates removed, the results were graphed using Microsoft Excel. The assumption made is that retrieved records that included each key search phrase in the mentioned bibliographic fields are representative writings focused on the subject.

The resulting Total Quality Management and Business Process Reengineering lifecycle graphs clearly resemble the bell-shape fashion pattern noted earlier in the Quality Circles movement (see Figure 3 and Figure 4). The graphs strikingly illustrate the way in which these movements grew and fell in popularity as represented in the academic and industry literature.

<figure>

![Figure 3](../p145fig3.gif)

<figcaption>Figure 3: Total Quality Management, 1990-2001</figcaption>

</figure>

<figure>

![Figure 4](../p145fig4.gif)

<figcaption>Figure 4: Business Process Reengineering, 1990-2001</figcaption>

</figure>

When comparing Figures 2, 3, and 4, each management fashion peaked from four to six years after some momentum had started. More specifically, in 1979 Quality Circles appeared to have momentum only to peak in five years. The same holds true for Total Quality Management (starting in the late 1980s and peaking in 1993) as well as Business Process Reengineering (starting in 1991 and peaking in 1995). To this end, it is reasonable to assume that management fads begin to lose popularity in about five years.

The limitations to this assumption are, of course, that this phenomenon has been tested in only three cases and that the article counts were limited to just three databases. The following section discusses the above approach in the context of knowledge management.

### The case of knowledge management

To a large extent, knowledge management is being considered by many as an emerging multidisciplinary field associated with the likes of system engineering, organizational learning, and decision support, to mention a few. Skeptics, on the other hand, are claiming that knowledge management is just another fad like Total Quality Management or Business Process Reengineering. In this section, the article-counting technique is applied to the concept of knowledge management in order to illuminate its current state of development.

Using the same approach employed in the earlier cases, article counts were retrieved from the three DIALOG files i.e., Science Citation Index (File 34), Social Science Citation Index (File 7), and ABI Inform (File 15). The retrieved counts were articles that included the phrase 'knowledge management' in its title, abstract, or descriptor fields. The assumption made is that retrieved records that included 'knowledge management' in these fields represent writings focused on knowledge management.

<figure>

![Figure 5](../p145fig5.gif)

<figcaption>Figure 5: Knowledge management, 1991-2001</figcaption>

</figure>

The results, which are graphed above in Figure 5, suggest that knowledge management has weathered the five-year mark and perhaps is becoming an addition to the management practice. The diagram illustrates that the popularity of Knowledge Management expanded rapidly from 1997 through 1999, contracted in 2000, and then rebounded in 2001\. To explore the growth period of the knowledge management lifecycle further, an additional bibliometric technique was used to reveal of Interdisciplinary Activity.

Interdisciplinary activity indicates the exportation and integration of theories or methods to other disciplines ([Pierce, 1999](#pie99); [Klein, 1996](#kle96)), in our case, to the development of the emerging field of knowledge management. The method ranks journal names of knowledge management source articles from above and then assigns an ISI's Subject Category Code. ISI's codes have been operationalized by ISI and have been assumed as indicators of disciplines ([White, 1996](#whi96)). This study assumed a threshold count of three or greater. In other words, three or more sources articles in ISI-assigned journals needed to occur in order to be included in the analysis. This threshold reduces the number of random occurrences in journals and indicates the concentration of publication activity.

Management

<table><caption> 

**Table 1: Interdisciplinary activity by column percentage, 1996-2001**</caption>

<tbody>

<tr>

<th>Discipline</th>

<th>1996</th>

<th>1997</th>

<th>1998</th>

<th>1999</th>

<th>2000</th>

<th>2001</th>

</tr>

<tr>

<td>Computer Science</td>

<td>35.7%</td>

<td>43.1%</td>

<td>42.0%</td>

<td>38.8%</td>

<td>28.7%</td>

<td>36.2%</td>

</tr>

<tr>

<td>Business</td>

<td>21.4%</td>

<td>16.9%</td>

<td>32.4%</td>

<td>25.6%</td>

<td>18.0%</td>

<td>20.7%</td>

</tr>

<tr>

<td>42.9%</td>

<td>7.7%</td>

<td>5.3%</td>

<td>12.8%</td>

<td>13.2%</td>

<td>17.2%</td>

</tr>

<tr>

<td>Information Science & Library Science</td>

<td> </td>

<td>15.4%</td>

<td>10.6%</td>

<td>7.9%</td>

<td>16.9%</td>

<td>14.2%</td>

</tr>

<tr>

<td>Engineering</td>

<td> </td>

<td>10.8%</td>

<td>4.3%</td>

<td>8.6%</td>

<td>13.6%</td>

<td>7.7%</td>

</tr>

<tr>

<td>Psychology</td>

<td> </td>

<td>6.2%</td>

<td>5.3%</td>

<td>1.7%</td>

<td>1.8%</td>

<td>1.5%</td>

</tr>

<tr>

<td>Multidisciplinary Sciences</td>

<td> </td>

<td> </td>

<td> </td>

<td>2.0%</td>

<td>4.0%</td>

<td> </td>

</tr>

<tr>

<td>Energy & Fuels</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.7%</td>

<td>3.7%</td>

<td>0.7%</td>

</tr>

<tr>

<td>Social Sciences</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1.7%</td>

</tr>

<tr>

<td>Operations Research & Mgt. Science</td>

<td> </td>

<td> </td>

<td> </td>

<td>1.0%</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Planning & Development</td>

<td> </td>

<td> </td>

<td> </td>

<td>1.0%</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Total:</td>

<td>14</td>

<td>65</td>

<td>207</td>

<td>407</td>

<td>272</td>

<td>401</td>

</tr>

<tr>

<td>Interdisciplinary Breadth:</td>

<td>3</td>

<td>6</td>

<td>6</td>

<td>10</td>

<td>8</td>

<td>8</td>

</tr>

</tbody>

</table>

Table 1 is the proportion of disciplinary affiliation of journals over time. In the 1996, interdisciplinary activity appeared mainly in three areas of study, namely, Computer Science, Business, and Management. Through 1999, the number of disciplines, or Interdisciplinary Breadth expanded to 10\. According to [Koenig](#koe00) (2000), this expansion was in response to new developments in technology and to organizations seeking an advantage in an increasingly competitive market.

In 2000, a pullback in popularity occurred that is, the total number of articles dropped by about 30%. Proportionally, Computer Science and Business experienced a decrease while the remaining six disciplines increased. According to [Abrahamson](#abr91) (1991, 1996), swings downward in popularity might be the direct result of shortfalls in realized benefits being experienced by organizations. One such study that indicated knowledge management was coming up short was in 1999, when Bain & Company conducted their well-known survey on management tools and techniques. Bain & Company reported that knowledge management "not only had relatively low utilization but also very low satisfaction scores relative to the average" ([Rigby 2001](#rig01): 145). Finally, while in 2001 the top two disciplines return approximately to 1996 proportions, the breadth of disciplines participating has more than doubled since 1996.

## Summary

This paper provides empirical evidence that management movements generally reveal themselves as fads or fashions within approximately five years after having gained some type of momentum. When applying this general rule of thumb to the popular concept of knowledge management, it appears that knowledge management has initially survived.

It is certainly plausible to hypothesize that if knowledge management does indeed mature into a permanent new component of managerial attention, it will continue to grow and in the process undergo a tweaking phenomenon -- that is, morphing or transforming into clearer, easier understood concept. The 2000 dip in popularity does suggest such a phenomenon.

To examine whether knowledge management indeed has survived and is on its way to becoming a significant and permanent part of management's tool box, will require not only the passage of time, but will also require a somewhat more sophisticated analysis. It is quite plausible that this phenomenon could obscure the continued growth of a movement. In other words, focusing on the appearance of a new title term can distinguish between typical fads and more long-lasting phenomena, but a more detailed analysis, which the authors look forward to conducting, needs to be undertaken to determine whether knowledge management is more than an unusually broad shouldered-fad.

## References

*   <a id="abr91"></a>Abrahamson, E. (1991). "Managerial fad and fashion: the diffusion and rejection of innovations". _Academy of Management Review_. **16**(3):586-612\.

*   <a id="abr96"></a>Abrahamson, E. (1996). "Managerial fashion." _Academy of Management Review_. **21**(1):254-285\.

*   <a id="abr99"></a>Abrahamson, E. & Fairchild, G. (1999). "Management fashion: lifecycles, triggers, and collective learning processes." _Administrative Science Quarterly_. **44**, 708-740\.

*   <a id="cas88"></a>Castorina, P. & Wood, B. (1988). "Circles in the fortune 500: why circles fail." _Journal for Quality and Participation_. **11**, 40-41\.

*   <a id="dav98"></a>Davenport, T. and Prusak, L. (1998). _Working knowledge: how organizations manage what they know_. Boston, MA: Harvard Business School Press.
*   <a id="hil96"></a>Hilmer, F. & Donaldson, L. (1996). _Management redeemed: debunking the fads that undermine corporate performance_. New York: The Free Press.

*   <a id="kle96"></a>Klein, J. (1996). _Crossing boundaries: knowledge, disciplinarities, and interdisciplinarities_. Charlottesville: University Press of Virginia.

*   <a id="koe00"></a>Koenig, M. (2000). "Information service and productivity: a backgrounder", in: _Knowledge management: for the information professional_. Medford: Information Today.

*   <a id="kpm00"></a>KPMG Consulting. (2000). _[Knowledge management research report](http://www.kpmgconsulting.co.uk/research/othermedia/wf_8519kmreport.pdf)_. London: Atos KPMG Consulting. Available at: http://www.kpmgconsulting.co.uk/research/othermedia/wf_8519kmreport.pdf [Site visited 24th September 2002]

*   <a id="law85"></a>Lawler, E. and Morhman, S. (1985). "Quality circles after the fad." _Harvard Business Review_. 63: 65-71\.

*   <a id="pie99"></a>Pierce, S. (1999). "Boundary crossing in research literature as a means of interdisciplinary information transfer". _Journal of the American Society for Information Science_. **50**(3), 271-279\.

*   <a id="pon98"></a>Ponelis, S. and Fair-Wessels, F. 1998\. "Knowledge management: a literature overview". _South Africa Journal of Library Information Science_. **66**(1), 1-10
*   <a id="rig01"></a>Rigby, D. (2001). "Management tools and techniques: a survey". _California Management Review_. **43**(2), 139-160\.

*   <a id="rog95"></a>Rogers, E. (1995). _Diffusion of innovation_. New York: Free Press.

*   <a id="sky97"></a>Skyrme, D. (1997). "[Knowledge Management: making sense of an oxymoron](http://www.skyrme.com/insights/22km.htm)". _Management Insight_ No. 22\. Available at: http://www.skyrme.com/insights/22km.htm. [Site visited 24th September 2002]
*   <a id="was78"></a>Wasson, C. (1978). _Dynamic competitive strategy & product life cycles_. Austin, TX: Austin Press.

*   <a id="whi96"></a>White, H. (1996). "Literature retrieval for interdisciplinary syntheses". _Library Trends_. **15**(2), 239-264\.

### <a id="app"></a>Appendix

B 7,15,34  
? S ((Total Quality Management) or TQM)/de,ti,ab and py=Year  
S1  
? rd  
Record count  

B 7,15,34  
S (Business Process Reengineering) or BPR)/de,ti,ab and py=Year  
S1 ?  
rd  
Record count