#### Vol. 12 No. 1, October 2006

* * *

# Search engines: a first step to finding information: preliminary findings from a study of observed searches.

#### [A.D. Madden](mailto:a.d.madden@shef.ac.uk) , B. Eaglestone , N.J. Ford and M. Whittle  
Department of Information Studies, University of Sheffield, Regent Court, 211 Portobello Street, Sheffield, S1 4DP, UK

#### Abstract

> **Introduction.** This is a working paper which aims to present the preliminary results of a study into the search behaviours of the general public. The paper reports on the findings of the first six months of an eighteen-month data collection excercise.  
> **Methods.** Detailed observations were made of nine volunteers, engaged on a variety of search tasks. Some of the tasks were self-selected, others were set by the researchers. Most tasks however, were designed to enable the volunteers to search within their own areas of interest and expertise.  
> **Analyses.** A set of 'search dimensions' is proposed and qualitative findings based on these are presented. In addition, some initial quantitative findings are discussed.  
> **Result.** Findings to date suggest that the best search strategy is a combination of simplicity and scrutiny. Volunteers who entered a few search terms but then carefully studied the results, appeared to be more successful than those who attempted to be prescriptive and entered a long series of terms.

## Introduction

Much research on Internet search behaviour has been conducted on samples that were convenient to the researcher, such as undergraduate or graduate students at research universities. ([Hargittai & Hinant 2006](#har)). This limited focus brings into question the applicability of any findings to the wider community of Internet users.

One way of overcoming this difficulty is to study the transaction logs of search engines (e.g., [Broder 2002](#bro); [Jansen _et al._ 1998](#jan98), [2000](#jan00b), [2005](#jan05); [Selberg & Etzioni 1995](#sel); [Spink _et al._ 1998](#spi)). However, as Wang _et al._ ([2000](#wan)) note: _'The cognitive and holistic approaches of studying user behaviours require that researchers observe the "real" process as it happens, not merely the outcomes of a process'._

In this paper, we report on the early stages of a study that aims to observe and describe Internet searches by members of the general public. The paper serves three main functions: it presents a preliminary qualitative analysis of data gathered in the early stages of the project; it identifies a number of key factors that appear to be emerging; and it uses these factors to suggest a range of search dimensions that may be of value in measuring and comparing searches.

## Methods

### Theoretical framework

The project employs a combination of qualitative and quantitative approaches. Searches are observed and are subjected to detailed qualitative study. The results of this analysis are used to identify patterns of search behaviour that may be amenable to quantitative analysis. This paper presents the results of the first stage, based on a qualitative analysis of an initial small sample. Other stages of the project will entail large-scale quantitative testing using as wide a range of people as possible to ensure that such patterns are representative of search behaviour in general and are not an artefact arising from a narrow sample.

### Sample

In selecting our sample, we have aimed to recruit volunteers who represent the demography of Sheffield as a whole. By the end of the project, it is anticipated that one hundred volunteers will have completed 400 to 500 observed and recorded searches.

This paper reports on findings of an initial qualitative analysis based on thirty-nine searches by nine volunteers (five women, four men). The volunteers ranged in age from 28 to 77 years (mean = 45). Length of Internet experience averaged about three years.

### Search tasks

One of the aims of the project is to observe as wide a range of search behaviour as possible. In each session therefore, some time is spent watching volunteers search for topics of their choosing and some is spent watching them perform set tasks. Volunteers are asked to explain their actions and describe their thought processes.

Sessions are recorded using [My Screen Recorder](http://www.deskshare.com/msr.aspx) (Deskshare 2006), which creates a record of the volunteer's comments and actions. A back-up record of keystrokes and Websites is also created using [SpectorPro](http://www.spectorsoft.com/index.html)

Volunteers are contacted beforehand to confirm arrangements. When contacted, they are asked to recall occasions when they have tried to find information using a search engine, but have encountered difficulties. They are asked to repeat these searches. After completing self-selected tasks, searchers are given two to three set searches (depending on the time available).

A task is deemed to have been completed, either when a volunteer feels that a satisfactory answer has been obtained, or when s/he wishes to stop searching. In all cases, to reduce pressure on volunteers, the researchers stress that the decision to end a search is the volunteer's and that he or she should not feel obliged to answer the questions.

The set tasks (Table 1) were carefully worded to avoid prompting volunteers with terms that could be of use in formulating a search.

<table><caption>

**Table 1: Search exercises set by researchers**</caption>

<tbody>

<tr>

<td> </td>

<td> </td>

<th>Closed</th>

<th>Open</th>

<th>Simple</th>

<th>Multi-stage</th>

<th>Implicit</th>

<th>Explicit</th>

<th>No. times search performed</th>

</tr>

<tr>

<th>Heads</th>

<td>1) What was written on Neville Chamberlain's piece of paper (see article below).</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

<td>X</td>

<td>6</td>

</tr>

<tr>

<td> </td>

<td>2) You have won a trip to _Saga_. Can you find out anything interesting about the place?</td>

<td>X  
(stage 1)</td>

<td>X  
(stage 2)</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

<td>6</td>

</tr>

<tr>

<th>Tails</th>

<td>1) You've received a postcard from friends who say they are abroad, visiting somewhere called _Map_. Where are they?</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td>3</td>

</tr>

<tr>

<td> </td>

<td>2) There are many opportunities to win things on the Internet. Find some that may be of interest to you.</td>

<td> </td>

<td>X</td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

<td>2</td>

</tr>

<tr>

<th>Additional</th>

<td>3) Find the postcode of the tallest British building outside of London.</td>

<td>XX  
(stages 1 & 2)</td>

<td> </td>

<td> </td>

<td>X  
</td>

<td>X  
</td>

<td> </td>

<td>4  
</td>

</tr>

</tbody>

</table>

The tasks were designed to enable the effect of a number of factors to be taken into consideration. These are as follows:

**Open vs. closed searches:** open searches are those for which many answers may be found. Closed searches, by contrast, have a clear 'right' answer ([Marchionini 1989](#mar)).

**Simple vs. multi-stage searches:** as noted in the introduction, according to the evidence of transaction logs, the majority of searches are simple and consist of a query comprising a small number of search terms, with no subsequent modification to the query. To ensure that volunteers occasionally have to engage in more complex search behaviour, some of the set problems require them to carry out searches which need more than one piece of information. To complete such an exercise, a volunteer has to carry out a chain of linked searches, which may entail a combination of open and closed searches.

**Implicit vs. explicit domain knowledge:** one criticism of much search research is that, where queries are imposed (i.e., they emanate from a source other than the searcher ([Gross 1999](#gro99))), there are implicit assumptions made about the searchers’ domain knowledge ([Madden _et al._ 2006](#mad)). In this study, many of the searches carried out were ones that the volunteers suggested themselves, so no such assumptions are necessary. Those that were imposed were designed to tax the searchers in the event of their chosen searches proving to be simple. All the imposed searches (with one exception) assume only basic knowledge. For the exception, the required knowledge is made explicit by presenting volunteers with the necessary information in a short paragraph ([Appendix](#app)), from which adequate search terms can be derived.

#### Assigning tasks

After searchers have completed the tasks that they bring with them, they are assigned two of the tasks shown in Table 1\. A coin is tossed to decide which ones. If the same search tasks are selected more than three times in a row, future volunteers are given the alternative until balance is restored.

If there is sufficient time available and if the volunteer is willing, an additional search is carried out.

### Data analysis

One of the aims of the project, is the identification of patterns and structures within searches. In order to achieve this aim, it is necessary to identify elements of a search that can be quantified. Some dimensions are introduced and discussed below. However, to focus solely on quantifiable elements of a search would result in much useful qualitative data being ignored. The analysis presented below therefore, draws on both qualitative and quantitative data.

Transcriptions were made of the recordings and quotations were coded. These provided an insight into the level of understanding that volunteers had of their chosen search engine and of the functions they were using. They also help to illuminate the process by which volunteers selected the Web sites they wished to examine more closely.

#### Quantitative analysis

Most of the quantitative data presented below are based on the number of mouse-clicks made in each search. These statistics were easy to obtain from the screen recordings and were used to calculate _search length_ and _search depth_ (discussed below as _Search dimensions_ ).

Preliminary impressions from the recordings however, suggested that the length of time spent perusing results could prove to be a key factor in whether or not a search was successful. Recordings were therefore re-analysed and times between clicks were noted. Occasionally, these timigns were adjusted. Volunteers would often be struck by a thought relating to their search practice and stop searching in order to discuss it. Their remarks, while interesting and often relevant to the project, were digressions from the search being timed. They were therefore measured and deducted from the overall time between clicks. A delaying digression was deemed to have occurred if:

1.  the cursor remained stationary and
2.  the volunteer was talking about a subject unrelated to the search on which they were currently engaged.

Time was only deducted if both these conditions applied.

## Results

### Choice of search engine.

Google was the preferred choice for thirty-three of the thirty-nine searches. All volunteers used it at least once; but three used more than one search engine within a search (Table 3). One searcher attempted the same search on four different search engines.

### User selected searches.

Previous research (e.g., [Bilal 2002](#bil); [Madden _et al._ 2006](#mad)) had suggested that, when asked to select a search topic, most people would choose an open one. This proved to be the case: seventeen of the 39 searches reported on here were selected by the volunteers (Table 2). Of these seventeen, thirteen were open and four were closed. By contrast, the majority of search exercises set by the researchers are closed (Table 1).

<table><caption>

**Table 2: Searches selected by volunteers**</caption>

<tbody>

<tr>

<th>Searcher</th>

<th>Search</th>

<th>Open</th>

<th>Closed</th>

</tr>

<tr>

<td>1</td>

<td>1*: Details of the film 'Summer Storm'.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>2: Why will 'Natasha Kaplinsky' not be on Breakfast TV for the next six months?</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>2</td>

<td>1: The 'Sheffield Property Shop' Website.</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td> </td>

<td>2: Information about Council Housing in Nottingham .</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>3: French property news.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>3</td>

<td>1: The policy of the UK Kennel Club on white boxer dogs.</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td>4</td>

<td>1: Driver for a Canonscan 300 scanner.</td>

<td> </td>

<td>X</td>

</tr>

<tr>

<td> </td>

<td>2: Free audiobook downloads.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>5</td>

<td>1: Information about the 'Sheffield Pals' battalion in WWI.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>6</td>

<td>1: Tropical fish tanks.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>2: Addresses of Bed & Breakfast establishments in York .</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>7</td>

<td>1: Timetables for Vietnamese trains.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>2: Information about Voluntary teaching abroad.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>8</td>

<td>1: Atomic Rooster.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>2: Charlie Poole.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>9</td>

<td>1: Directions for getting to Lincoln Cemetary.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>2: Complaints about the 'DVD giveaway' from the Daily Express newspaper.</td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td colspan="4">

_*Search number. See Table 3 for details of strategies employed to complete these searches._</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 3: Searches classified according to the three modification strategies identified**</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Category of search</th>

</tr>

<tr>

<th>Volunteer</th>

<th>

Simple search  
_(No modification)_</th>

<th>

Syntactic _modification_</th>

<th>

Semantic modification  
_(related concept)_</th>

<th>

Change of  
_search engine_</th>

</tr>

<tr>

<td>1</td>

<td>3*(Chamberlain)</td>

<td>1, 2, 4 (Saga), 5 (Tower)</td>

<td>5 (Tower)</td>

<td> </td>

</tr>

<tr>

<td>2</td>

<td>1, 2, 3, 4 (Chamberlain),  
5 (Saga)</td>

<td>6 (Tower)</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>3</td>

<td> </td>

<td>1, 2, 4 (Prize)</td>

<td> </td>

<td>3 (Map)</td>

</tr>

<tr>

<td>4</td>

<td>4 (Saga)</td>

<td>1, 2</td>

<td>3 (Map)</td>

<td>3 (Map)</td>

</tr>

<tr>

<td>5</td>

<td> </td>

<td>1, 2 (Chamberlain),  
3 (Saga), 4 (Tower)</td>

<td>1, 2 (Chamberlain)</td>

<td>1</td>

</tr>

<tr>

<td>6</td>

<td>1, 3 (Chamberlain),</td>

<td>2</td>

<td>4 (Prize)</td>

<td> </td>

</tr>

<tr>

<td>7</td>

<td>2, 3 (Chamberlain), 4 (Saga)</td>

<td>1,5 (Tower)</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>8</td>

<td>1, 2</td>

<td> </td>

<td> </td>

<td>3 (Chamberlain)</td>

</tr>

<tr>

<td>9</td>

<td>1, 4 (Saga)</td>

<td>2, 3 (Chamberlain)</td>

<td> </td>

<td> </td>

</tr>

<tr>

<th>

No. of searches  
_within category_</th>

<td>16</td>

<td>19</td>

<td>5</td>

<td>4</td>

</tr>

<tr>

<td colspan="5">

_*Search number_</td>

</tr>

</tbody>

</table>

#### Closing searches.

Many of the searches that began as open however, became closed. This happened for one of two reasons.

1.  Occasionally, having begun an open search, a searcher would become particularly interested in specifics of the topic. So, for example, the volunteer who began looking for details of the film 'Summer Storm' later modified her search to find out whether or not any of the lead actors were gay. Similarly, the woman looking for Bed and Breakfast accommodation in York quickly changed her approach and began to look for a particular B&B in York.
2.  Alternatively, searchers would either remember, or deduce the presence of, an online resource that they felt could satisfy their information requirements. An example of this was the volunteer who sought information on the Sheffield Pals battalion. He was reminded, while browsing, that a book about the battalion's history was available online. He then changed his strategy and began to look specifically for the book.

#### Search problems

Most of the volunteers' own searches did not need refining. Although they were asked to carry out searches with which they had previously had difficulties, in general, they encountered few problems when trying to repeat the searches. Those who had difficulties did so for two main reasons. Some were confused by sponsored links. Often however, searchers would enter a large number of terms into the search box with the result that the search was too specific and no useful sites were retrieved. One of the more extreme examples of this was volunteer 4 (Table 2), who began his search for online audio books by entering into Lycos the phrase: _'where can i find Audio books that i can download to my IPOD/pc?'_

This was most clearly illustrated by the searches for the city _Saga_ and for accompanying information. Only three of the six volunteers searching for 'Saga' were successful in locating the city. All three used Google and the search terms they entered were (Volunteer no. is given in parentheses. See Tables 2 and 3):

*   _Saga tourism_ (1), _Saga_ (2), _Saga_ (7).

Having found a suitable link to a site that dealt with the city, these volunteers then stopped using Google and continued their search from the chosen site.

Two of the three unsuccessful volunteers searched for:

*   _saga where is it in the world_ (4); and _SAGA where in the world is it_ (9)

The third began with _Saga_ . However, unlike the other three volunteers who began with short searches, he examined the results for seven seconds then modified his search to _saga 'location'._ He ended his search (unsuccessfully) with:

*   _saga "location of" -museum_ (5)

The fastest of the three searchers who located Saga spent fifteen seconds examining the results. These three were also the only volunteers to succeed in finding the contents of Neville Chamberlain's piece of paper.

#### Use of Boolean operators and quotation marks.

Of the four people searching for the tallest UK tower outside of London, two used Google's 'NOT' operator (i.e., '-') and a third expressed a desire for such a function whilst talking through her choice of search terms:

*   '_I don't think it would listen if I said 'excluding London' would it?_ '

One of the two users of '-' had used it in earlier searches and had also used 'OR'. None of the other volunteers used any Boolean operators, though use of quotation marks was common. However, it was clear that much of this usage was done with little or no understanding of the effects. This is most clearly illustrated by a search for:

*   _postcode "tallest building outside london " -london ._

Not surprisingly, no Web sites were retrieved.

### Search dimensions

Observations of the thirty-nine searches discussed in this paper suggest a number of measures that may be of value in describing and comparing searches in future (Table 5). These are based, in part, on similar measures used by Nichols _et al._ (2004).

#### Search length

The most obvious measurement of a search is the number of clicks that the searcher makes before he or she either concludes or abandons the search (Table 5).

Clicks made using the initial search engine of choice are shown in a separate column from clicks made on other sites, making it possible to calculate the proportion of each search that would be recorded on the search engine's transaction log.

#### Search depth

Maximum depth is defined as the largest number of clicks made on a page found using a search engine. So for example, Volunteer no. 7, looking for information about Saga City, quickly retrieved the City's official Web site. She used nine clicks ( _depth=9_ ) to navigate within and from this Website, without returning to the search engine. By contrast, when she attempted to discover the postcode of Britain 's tallest building outside of London, she examined several sites, one after another, but did not consider any to be helpful. She therefore returned frequently to the search engine. Her search depth on this exercise was just one.

#### Intensity

The time that searchers spent studying Web pages (including pages of retrieved results) differed considerably. At one extreme, searchers would enter a search term, spend a few seconds scanning the resulting hits, then modify the search term. Their searches therefore had a lot of steps, but were carried out quickly. At the other extreme were searchers who would slowly and carefully study each page, before taking any action.

Search intensity is defined as the mean length of time spent studying retrieved material before a decision is made either to continue searching (indicated by a mouse click), or to conclude the search.

## Discussion

#### Comparison with transaction logs

As noted in the introduction, a common way of studying searches by the general public has been through the analysis of log transaction data. Such studies clearly have value and have, for example, revealed patterns in the use of Boolean operators ([Whittle _et al._ 2006](#whi)). However, observations made to date in this study, suggest that such analyses will tend to underestimate the complexity of searches for three reasons, all of which can be inferred from the observations made above.

1.  Searchers occasionally switched search engines rather than modifying their searches (Table 3).
2.  In discussing the closure of searches earlier, it was reported that one volunteer had begun by searching Google for Web sites about a Sheffield battalion, but was reminded of the fact that a book about the battalion was online. He then began to search for the book’s author. It is highly unlikely that a machine analysis of the Google log could be trained to recognise the connection between the two searches. Major semantic modifications involving the search for a related concept were not uncommon (Tables 3,4).
3.  Occasionally, a site retrieved by the search engine became the main search tool (Table 5). The search would then continue using this site, rather than the search engine. At times, such searches lasted for quite a long time; but a study of the search engine’s transaction log would suggest otherwise.

<table><caption>

**Table 4: Changes made to search for related concepts**</caption>

<tbody>

<tr>

<th>Volunteer</th>

<th>Initial search</th>

<th>Related concept</th>

</tr>

<tr>

<td>1</td>

<td>tallest building uk outside london</td>

<td>hilton deansgate Manchester</td>

</tr>

<tr>

<td>4</td>

<td>Where in the world is a place called Map</td>

<td>

microsoft world encl ( _sic_ ) (=encyclopaedia)</td>

</tr>

<tr>

<td>5</td>

<td>

Sheffield batallion ( _sic_ )</td>

<td>

Richard Sparlling ( _sic_ )</td>

</tr>

<tr>

<td> </td>

<td>

neville chamberlin I have in my hand ( _sic_ )</td>

<td>munich pact</td>

</tr>

<tr>

<td>6</td>

<td>lottery</td>

<td>Poker</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 5: Dimensions of observed searches**</caption>

<tbody>

<tr>

<th>Volunteer</th>

<th>Search no.</th>

<th>Length on search engine (On)</th>

<th>Length off search engine (Off)</th>

<th>On (On+Off)*</th>

<th>Depth</th>

<th>Intensity: Mean (s.d.)</th>

</tr>

<tr>

<td>1</td>

<td>1</td>

<td>13</td>

<td>7</td>

<td>0.65</td>

<td>3</td>

<td>19 (23.76)</td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>8</td>

<td>2</td>

<td>0.80</td>

<td>1</td>

<td>11.5 (11.07)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>2</td>

<td>1</td>

<td>0.67</td>

<td>1</td>

<td>15 (13.23)</td>

</tr>

<tr>

<td> </td>

<td>4</td>

<td>13</td>

<td>9</td>

<td>0.59</td>

<td>4</td>

<td>9.68 (10.86)</td>

</tr>

<tr>

<td> </td>

<td>5</td>

<td>19</td>

<td>15</td>

<td>0.56</td>

<td>6</td>

<td>11 (14.48)</td>

</tr>

<tr>

<td colspan="7"> </td>

</tr>

<tr>

<td>2</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>0.5</td>

<td>1</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>1</td>

<td>4</td>

<td>0.2</td>

<td>4</td>

<td>25 (10.37)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>1</td>

<td>1</td>

<td>0.5</td>

<td>1</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>4</td>

<td>9</td>

<td>9</td>

<td>0.5</td>

<td>2</td>

<td>21.11 (17.03)</td>

</tr>

<tr>

<td> </td>

<td>5</td>

<td>1</td>

<td>5</td>

<td>0.17</td>

<td>5</td>

<td>16.67 (11.25)</td>

</tr>

<tr>

<td> </td>

<td>6</td>

<td>6</td>

<td>4</td>

<td>0.6</td>

<td>3</td>

<td>27.5 (34.5)</td>

</tr>

<tr>

<td colspan="7"> </td>

</tr>

<tr>

<td>3</td>

<td>1</td>

<td>14</td>

<td>5</td>

<td>0.74</td>

<td>2</td>

<td>12.36 (10.98)</td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>2</td>

<td>2</td>

<td>0.5</td>

<td>2</td>

<td>13.75 (8.54)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>6</td>

<td>2</td>

<td>0.75</td>

<td>2</td>

<td>10.63 (19.17)</td>

</tr>

<tr>

<td> </td>

<td>4</td>

<td>11</td>

<td>6</td>

<td>0.65</td>

<td>2</td>

<td>12.35 (10.77)</td>

</tr>

<tr>

<td colspan="7"> </td>

</tr>

<tr>

<td>4</td>

<td>1</td>

<td>2</td>

<td>1</td>

<td>0.67</td>

<td>1</td>

<td>43.33 (24.66)</td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>10</td>

<td>8</td>

<td>0.56</td>

<td>2</td>

<td>14.72 (15.1)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>6</td>

<td>5</td>

<td>0.55</td>

<td>3</td>

<td>12.27 (11.26)</td>

</tr>

<tr>

<td> </td>

<td>4</td>

<td>3</td>

<td>1</td>

<td>0.75</td>

<td>1</td>

<td>7.5 (6.45)</td>

</tr>

<tr>

<td colspan="7"> </td>

</tr>

<tr>

<td>5</td>

<td>1</td>

<td>30</td>

<td>14</td>

<td>0.68</td>

<td>6</td>

<td>4.55 (6.36)</td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>22</td>

<td>8</td>

<td>0.73</td>

<td>2</td>

<td>7.67 (9.8)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>8</td>

<td>1</td>

<td>0.89</td>

<td>1</td>

<td>13.33 (16.96)</td>

</tr>

<tr>

<td> </td>

<td>4</td>

<td>24</td>

<td>2</td>

<td>0.92</td>

<td>1</td>

<td>6.73 (12.88)</td>

</tr>

<tr>

<td colspan="7"> </td>

</tr>

<tr>

<td>6</td>

<td>1</td>

<td>5</td>

<td>3</td>

<td>0.63</td>

<td>3</td>

<td>14.38 (13.74)</td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>10</td>

<td>3</td>

<td>0.77</td>

<td>1</td>

<td>10.77 (11.93)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>1</td>

<td>3</td>

<td>0.25</td>

<td>3</td>

<td>42.5 (1.29)</td>

</tr>

<tr>

<td> </td>

<td>4</td>

<td>3</td>

<td>1</td>

<td>0.75</td>

<td>1</td>

<td>17.5 (1.29)</td>

</tr>

<tr>

<td colspan="7"> </td>

</tr>

<tr>

<td>7</td>

<td>1</td>

<td>1</td>

<td>7</td>

<td>0.13</td>

<td>6</td>

<td>15.63 (9.8)</td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>1</td>

<td>3</td>

<td>0.25</td>

<td>3</td>

<td>43.75 (40.29)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>1</td>

<td>4</td>

<td>0.2</td>

<td>4</td>

<td>28.75 (2.74)</td>

</tr>

<tr>

<td> </td>

<td>4</td>

<td>4</td>

<td>10</td>

<td>0.29</td>

<td>9</td>

<td>10.00 (7.03)</td>

</tr>

<tr>

<td> </td>

<td>5</td>

<td>12</td>

<td>4</td>

<td>0.75</td>

<td>1</td>

<td>25.00 (10.63)</td>

</tr>

<tr>

<td colspan="7"> </td>

</tr>

<tr>

<td>8</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>3</td>

<td>2</td>

<td>0.75</td>

<td>1</td>

<td>33.75 (21.36)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>9</td>

<td>4</td>

<td>0.69</td>

<td>4</td>

<td>30.00 (70.71)</td>

</tr>

<tr>

<td colspan="7"> </td>

</tr>

<tr>

<td>9</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>2</td>

<td>3</td>

<td>2</td>

<td>0.6</td>

<td>1</td>

<td>43.00 (31.82)</td>

</tr>

<tr>

<td> </td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>0.6</td>

<td>1</td>

<td>23.00 (28.28)</td>

</tr>

<tr>

<td> </td>

<td>4</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td> </td>

</tr>

<tr>

<td colspan="7">

_*Proportion of search carried out on the search engine. On+ Off = total length of search_.</td>

</tr>

</tbody>

</table>

#### Search length

Many of the studies referred to earlier, which report on the analysis of transaction logs, stress the brevity of searches. However, as Jansen ([2000](#jan00a)) noted in a study involving five search engines, there is often little advantage in using complex queries. Indeed, our study so far appears to endorse this and also suggests that they may, on occasion, be counter-productive. Amongst the searches observed for this study, one common reason for failure has been the use of too many search terms. Some of this was due to inappropriate use of Boolean operators of the kind noted by Jansen _et al._ ([1998](#jan98), [2000](#jan00b)). In other searches however, users were far too prescriptive, which caused the search engine to exclude many potentially useful sites.

#### Refinement and ambiguity

The tools developed to search electronic databases prior to the advent of the World Wide Web searched well-ordered collections of documents, in which considerable human effort had been employed to remove ambiguities and where minimum standards were in place to ensure the appropriateness of included material. A Web search engine has a more difficult task, because many of the search terms that might be used have a wide range of meanings. If a search term has a meaning which is particularly common within the Web-using community, it will be reflected in the ranking of the hits. Often however, more than one meaning is popular and this diversity is apparent within the first page of hits.

Observations to date suggest that, rather than beginning a search with a complex enquiry, a more effective strategy is to begin with a simple search, but then to scrutinize the results closely, so that the range of meanings can be fully appreciated. In other words, it is better to search with intensity rather than at length.

## Conclusion

From the findings of this study so far, it seems that often, the best search strategy is a combination of simplicity and scrutiny. Those volunteers who entered a few search terms but then carefully studied the results, appear to be more successful than those who attempt to be prescriptive and enter a long series of terms. It was also interesting to note the extent to which searching took place away from search engines. It is hardly surprising that the studies of Internet searching have focussed on the use of search engines. However, it is clear from the findings of this study to date that, while search engines have an important role to play in information seeking on the Internet, often, the major part of a search takes place elsewhere.

## Acknowledgements

The authors would like to acknowledge the support of the Arts and Humanities Research Council, which funds this project. We would also like to thank staff at Sheffield's City Learning Centres, the Forum 100 and Sheffield University 's Institute for Lifelong Learning who have put us in touch with potential volunteers. Finally, we would like to express our gratitude to our volunteers for their time.

## References

*   <a id="bil"></a>Bilal, D. (2002). Children's use of the Yahooligans! Web search engine. III. Cognitive and physical behaviors in fully self-generated search tasks. _Journal of the American Society of Information Scientists,_ **53**(13), 1170-1183.
*   <a id="bro"></a>Broder, A (2002). [A taxonomy of Web search](http://www.webcitation.org/5LA2CDBRm). _ACM SIGIR Forum_, **36**(2). Retrieved 23 August, 2006 from http://www.acm.org/sigir/forum/F2002/broder.pdf
*   <a id="des"></a>Department for Education and Skills. (2005) [The Standards Site. City Learning Centres](http://www.webcitation.org/5LA1uAOHJ). Retrieved 8 January 2006 from http://www.standards.dfes.gov.uk/sie/eic/clc/.
*   <a id="gro99"></a>Gross, M. (1999). Imposed queries in the school library media center: a descriptive study, _Library & Information Science Research,_ **21**(4), 501-521\.
*   <a id="gro00"></a>Gross, M. (2001). [Imposed information seeking in school library media centers and public libraries: a common behaviour?](http://informationr.net/ir/6-2/paper100.html) _Information Research_, **6**(2), paper 100\. Retrieved 15 September, 2005 from http://informationr.net/ir/6-2/paper100.html
*   <a id="har"></a>Hargittai, E. & Hinnant, A. (2006). Toward a Social Framework for Information Seeking. In A. Spink & C. Cole (Eds.) _New directions in human information behavior_ (pp. 55-70). New York, NY: Springer.
*   <a id="jan00a"></a>Jansen, B.J. (2000). [The effect of query complexity on Web searching results.](http://informationr.net/ir/6-1/paper87.html) _Information research,_ **6**(1), paper 87\. Retrieved 17 August, 2006 from http://informationr.net/ir/6-1/paper87.html
*   <a id="jan05"></a>Jansen, B.J., Spink, A. & Pedersen, J. (2005) A temporal comparison of AltaVista Web searching. _Journal of the American Society for Information Science and Technology_, **56**(6), 559-570\.
*   <a id="jan98"></a>Jansen, B.J., Spink, A. & Saracevic, T. (1998). Failure analysis in query construction: data and analysis from a large sample of Web queries. In _Proceedings of the third ACM conference on Digital libraries, Pittsburgh, Pennsylvania, United States._ (pp. 289-290). New York, NY: ACM Press.
*   <a id="jan00b"></a>Jansen, B.J., Spink, A. & Saracevic, T. (2000). Real life, real users and real needs: a study and analysis of user queries on the Web. _Information Processing and Management,_ **36**(2), 207-227\.
*   <a id="mad"></a>Madden, A.D., Ford, N.J., Miller, D. & Levy, P. (2006). Children's use of the Internet for information-seeking: what strategies do they use and what factors affect their performance? _Journal of Documentation_ , **62**(6), 744-761\.
*   <a id="mar"></a>Marchionini, G. (1989). Information-seeking strategies of novices using a full-text electronic encyclopedia. _Journal of the American Society for Information Science and Technology_, **40**(1), 54-66
*   <a id="nic"></a>Nicholas, D., Huntington, P., Williams, P. & Dobrowolski, T. (2004). Re-appraising information seeking behaviour in a digital environment: bouncers, checkers, returnees and the like. _Journal of Documentation_, **60** (1) 24-39\.
*   <a id="sel"></a>Selberg, E. & Etzioni, O. (1995). _[Multi-service search and comparison using the MetaCrawler.](http://www.webcitation.org/5Lk0bUfkO) _Paper presented at the Fourth International World Wide Web Conference, December 11-14, 1995, Boston, Massachusetts, USA. Retrieved 8 January, 2007 from http://www.w3.org/Conferences/WWW4/Papers/169/
*   <a id="spi"></a>Spink, A., Bateman, J. & Jansen, B.J. (1999). Searching the Web: a survey of EXCITE users. _Internet Research: Electronic Networking Applications and Policy_ **9** (2) 117-128\.
*   <a id="wan"></a>Wang, P., Hawk, W.B. & Tenopir, C. (2000). Users' interaction with the World Wide Web resources: an exploratory study using a holistic approach. _Information Processing and Management_, **36**(2), 229-251.
*   <a id="whi"></a>Whittle, M., Eaglestone, B., Ford, N., Gillet, V.J. & Madden, A. (2006). Query transformations and their role in Web searching by the general public. _Information Research_ **12**(1), paper 276\. Retrieved 8 January, 2007 from http://informationr.net/ir/12-1/paper276.html

## <a id="app"></a>Appendix

What was written on Neville Chamberlain's piece of paper (see article below)

The Right Honourable Arthur Neville Chamberlain (18 March 1869-9 November 1940) was Prime Minister of the United Kingdom from 1937-1940\. Chamberlain is perhaps one of the most ill-regarded British Prime Ministers of the 20th century, largely due to his policy of appeasement towards Nazi Germany regarding the abandonment of Czechoslovakia to Hitler at Munich in 1938\.

<figure>

![Figure 1](../../12-2/p294fig1.jpg)</figure>

Chamberlain on his return from Munich, waves the infamous piece of paper containing the resolution to commit to peaceful methods signed by both Hitler and himself. He said:  
_'My good friends, for the second time in our history, a British Prime Minister has returned from Germany bringing peace with honour. I believe it is peace for our time.'_

_Extract from Wikipedia_