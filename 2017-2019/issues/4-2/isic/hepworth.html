<!DOCTYPE html>
<html lang="en">

<head>
	<title>University students' information seeking behaviour in a changing learning environment</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="description" content="doctoral workshop paper, Information Seeking in Context">
	<meta name="keywords" content="">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>Investigating methods for understanding user requirements for information products</h1>
	<h3>Mark Hepworth</h3>
	<p>Senior Lecturer, Division of Information Studies<br>
		Nanyang Technological University<br>
		Singapore<br>
		<em>asmark@ntu.edu.sg</em></p>
	<h2>Introduction</h2>
	<p>This research is concerned with methods that can be used for helping to understand people's requirements for
		information products. Two questions are central to this research:</p>
	<ol>
		<li>What kind of data should we try to capture about people and their interaction with information so that we
			can have a detailed understanding of their requirements?</li>
		<li>Having determined what we need to find out about, what research techniques are most appropriate for
			capturing the relevant data?</li>
	</ol>
	<p>To help answer these questions literature from 'user studies' and 'information retrieval' (library and
		information science), human computer interface design, and systems analysis and design were reviewed. This
		resulted in a conceptual framework that indicated the data that need to be captured. This was followed by a
		review of literature from the areas mentioned above as well as research methodology to identify techniques that
		could be applied. Different techniques were evaluated.</p>
	<p>Together this formed a methodology that was applied to a community to see whether a useful understanding of their
		requirements could be derived. The community chosen, students, is one that is relatively well understood and has
		been the focus of research for many years. It was felt that choosing a community where data on their needs
		exists would help to provide an indication of the effectiveness of the methodology. At minimum it should be able
		to derive similar insights to those that have been built up over the years. At best it would provide a more
		detailed understanding of this community in terms of their interaction with information and the implications for
		an information product that would meet their requirements. The application of the methodology therefore enabled
		the research to understand the strengths and weaknesses of the methodology.</p>
	<h2>The stages of the research</h2>
	<p>The following outlines the stages the research has gone through and briefly the findings.</p>
	<h3>Stage 1. What kind of data should we try to capture about people and their interaction with information so that
		we can have a detailed understanding of their requirements?</h3>
	<p>To help answer this question a number of papers were reviewed. The following cites those that had particular
		impact on the researcher: from user studies (<a href="#ref2">Taylor, 1968</a>; <a href="#ref1">Garvey, Nan Lin,
			and Nelson, 1971</a>; <a href="#ref3">Wilson, Streatfield and Mullins, 1979</a>; <a href="#ref3">Wilson,
			1981</a>; <a href="#ref2">Streatfield 1983</a>; <a href="#ref2">Hogeweg De Hart, 1983, 1984</a>; <a
			href="#ref">Dervin and Nilan, 1986</a>; <a href="#ref">Borgman, 1996</a>); from human computer interface
		design (<a href="#ref2">Preece, <em>et al.</em>, 1994</a>; <a href="#ref2">Hill, 1995</a>; <a
			href="#ref2">Hartson and Boehm-Davis, 1995</a>; <a href="#ref2">Shackel 1997</a>), and from systems analysis
		(<a href="#ref">Brown, 1994</a>; <a href="#ref2">Robinson and Prior, 1995</a>; <a href="#ref3">Mehdi
			Sagheb-Tehrani, 1995</a>; <a href="#ref3">Wixen and Ramsey, 1996</a>; <a href="#ref2">Underwood, 1996</a>;
		<a href="#ref2">Tudor and Tudor, 1997</a>; <a href="#ref2">Simonsen and Kensing 1997</a>). This process was
		facilitated by a number of articles published in the 90s that helped to pull together and differentiate between
		various research approaches, (<a href="#ref1">Hewins, 1990</a>; <a href="#ref">Allen, 1991</a>; <a
			href="#ref2">Westbrook, 1993</a>; <a href="#ref">Ellis, 1993</a>; <a href="#ref3">Wilson, 1994</a>; <a
			href="#ref1">Ingwersen, 1996</a>; <a href="#ref">Ellis, 1996</a>). There were found to be a great diversity
		of approaches. Few are specifically related to the specification of user requirements for information products.
	</p>
	<p>However four common themes were identified. First the &quot;<em>sociological</em>&quot; which highlights the
		importance of the social context, (the roles, the tasks), of the respondent. Second the
		&quot;<em>content</em>&quot; area which includes the physical environment and tools that the respondent
		interacts with and are associated with their roles and tasks (such as books, the Internet, etc.). Thirdly the
		&quot;<em>psychological</em>&quot; which emphasises the cognitive and affective domain. Fourthly the
		&quot;<em>behavioural</em>&quot;. <a href="#ref">Bandura's</a> (1986) triadic framework of,</p>
	<ul>
		<li>Behaviour e.g. browsing the shelves or entering search statements;</li>
		<li>Environment e.g. tasks, roles, subject matter, informationsystems, services and products</li>
		<li>Cognition &amp; Personal Factors (thoughts and emotions) e.g. wanting to refine a search and feelings of
			confusion.</li>
	</ul>
	<p>was then adopted to help conceptualise the relationship between these themes identified.</p>
	<p>Bandura's notion of <strong>environment</strong> therefore incorporates both the <em>sociological</em> and the
		<em>content</em> dimensions. When studying the users the <strong>environment</strong> relates to their context
		both in the sociological sense of the tasks and roles they have to perform but also the physical environment
		i.e. the books, the articles, the OPACs, World Wide Web pages etc. that they interact with and get feedback
		from. Feedback that follows and to some extent stimulates <strong>behaviour</strong> that is based on
		experience, knowledge and perception. <strong>Cognitive and personal factors</strong> influence and result in
		behaviour and are related to the environment, background and tasks of the respondents. In this research it
		should be noted that personal factors, such as psychometric data, have not been explored but also have
		implications for the user's requirements. For example people may prefer either a virtual reality or three
		dimentional interface design, the 'rats-eye view', or the more abstract two dimensional &quot;birds-eye
		view&quot; such as used by Windows 95 Explorer software interface (<a href="#ref1">Howlett, 1996</a>).</p>
	<p>User requirements analysis therefore needs to capture the respondent's thoughts as well as their behaviour and
		the materials necessary to undertake information intensive tasks. Limiting one's study to any one dimension
		would result in only a partial picture of the users' requirements and hence would lead to the development of
		products that did not adequately support the user requirements.</p>
	<p>The three dimensions 'environment', 'behaviour' and 'cognitive and personal factors' can therefore taken to be
		fundamental to understanding the potential user of information and hence user requirments analysis and provide
		the researcher with a framework for data gathering.</p>
	<h3>Stage 2. What research techniques are most appropriate for capturing the relevant data?</h3>
	<p>The second stage of the research concerned the identification of techniques to elicit the various dimensions
		identified above. Choice of techniques was therefore driven by the need to gather data on the three dimensions
		outlined above.</p>
	<p>Choice of research techniques depends on the ontology and epistemology of the researcher. In this case the
		researcher was influenced by a number of approaches including ethnographic, which emphasises the importance of
		the respondents local context, and sense-making in that people were perceived as active participants in the
		process and their perceptions and actions relate to and are formed in a dynamic way as they encounter specific
		situations (<a href="#ref">Dervin, 1992, 1994</a>). This implied that data should be gathered while the
		respondents undertake the task and that perceptions need to be captured. It was also assumed that if data on a
		number of people in similar situations are studied generalisations could be made. Partly due to the influence of
		<a href="#ref1">Kuhlthau</a> (1991) and HCI research, respondents were studied over a period of time starting
		from task initiation until they had gathered what they thought was relevant information. This also reflected the
		notion that the objective was to derive user requirements for a product that would support the entire task.</p>
	<p>Qualitative techniques were chosen partly because they are recognised as appropriate for exploratory research
		where variables are not clearly defined and also because of their efficacy in highlighting themes, processes and
		cognition of the respondent.</p>
	<p>To help determine the most appropriate techniques a review of appropriate literature on research methodology was
		conducted, (<a href="#ref2">Patton, 1990</a>; <a href="#ref1">Miles and Huberman,1994</a>; <a
			href="#ref">Churchill, 1995</a>; <a href="#ref2">Nicholas, 1996</a>; <a href="#ref2">Neuman, 1997</a>; <a
			href="#ref3">Zikmund 1997</a>). In addition various equipment (tape, video, forms, Lotus Screencam
		(screen/voice capture software)) and methods including observation, interview, talk-through, task analysis, task
		hierarchy diagrams and the critical incident technique were reviewed.</p>
	<p>Finally a combination of the following were chosen as most appropriate,</p>
	<ul>
		<li>task analysis to capture the respondents perception of the task and sub-tasks; as well as the task
			environment (services, systems etc.),</li>
		<li>talk-through capturing verbalised thoughts while respondents conduct tasks, providing data on the cognitive
			dimension</li>
		<li>observation to capture behavioural data (actions) as well as information about the environment with which
			the respondent is interacting.</li>
	</ul>
	<p>To capture data, forms were designed that sensitized the researcher to collecting data on the three dimensions:
		cognitive and personal factors, behaviour, and the environment.</p>
	<p>Stage one and two therefore resulted in a possible framework for understanding what data should be captured and
		how the data should be gathered.</p>
	<h3>Stage 3. Implementation of the methodology.</h3>
	<p>Fifty Master of Science Information Studies students were divided into six groups. Each group chose one of three
		research topics. Each week the students rotated between being either researchers or respondents. Peer pressure
		and the fact that a small proportion of marks for course assessment were awarded for &quot;participation&quot;
		in the project helped to ensure that they were serious about the task. At the end of semester each group were
		also expected to derive their own solutions for an information product.</p>
	<p>Implementation took the following course.</p>
	<ol>
		<li>An initial task analysis interview was conducted with the respondents. The aim was to capture the users'
			overall perception of the task. The task being to go through the process of gathering material on the topic
			to the point where they would start writing an essay.<br>
			The interview used &quot;What&quot; questions to identify major tasks, &quot;How&quot; questions to identify
			sub-tasks and &quot;goal&quot; questions to identify objectives and outcomes, (<a href="#ref2">Sebillotte,
				1988</a>). Researchers were monitored to try to ensure that they did not depart from this format or
			provoke &quot;correct&quot; responses. Task Hierarchy Diagrams were derived. These diagrams helped to reveal
			aspects of the environment that the respondents expected to interact with including the services and
			products they expected to use and also, to a lesser extent, the associated cognitive and behavioural tasks.
		</li>
		<li>Once respondents started to undertake the assignment the processes, perceptions, actions and objects were
			recorded using a combination of talk through technique and observation. Predefined forms sensitised
			researchers to capturing cognitive, behavioural and environmental data.</li>
	</ol>
	<p>Researchers limited themselves to only asking about the thoughts of the user, &quot;what are you thinking
		now?&quot; and were not expected to ask probing questions, such as &quot;Why&quot; or make any suggestions that
		would lead the respondent in a particular direction.</p>
	<ul>
		<li>This generated eight Task Hierarchy Diagrams and the 52 talk-through and observation forms. A total of 1160
			incidents were identified in the transcripts. An inductive approach was used to categorise these incidents.
		</li>
	</ul>
	<h2>An overview of the respondents' information tasks</h2>
	<p>The respondents having chosen a question to answer</p>
	<ol>
		<li>spend time understanding what the question is about. Respondents &quot;look at the question carefully&quot;,
			&quot;think back to what they have read&quot;, think about &quot;what is expected&quot; and generally try to
			define the topic. They may wish to contact an expert for help.</li>
	</ol>
	<ul>
		<li>After searching and retrieval respondents still returned to and continued the process of topic definition.
			In general respondent's found this aspect of the overall task very difficult.</li>
		<li>As <a href="#ref1">Kuhlthau</a> (1991) has pointed out this stage was associated with confusion and
			trepidation.</li>
	</ul>
	<ol start="3">
		<li>The respondents then started to choose systems and services to search. This was influenced by physical
			location of resources, the perceived subject content, types of material available and also familiarity.</li>
		<li>Search terms and combinations of terms were identified (often with great difficulty) and tried out.
			Depending on the response from the systems and the relevance of material retrieved respondents may narrow
			(&quot;refining&quot;), broaden or &quot;redefine&quot; their searches. Numerous attempts are often made
			using different terms. Different systems, locations, organisations may also be tried.</li>
	</ol>
	<ul>
		<li>Respondents were not clear how the various systems worked. Systems such as Yahoo were chosen because it was
			&quot;memorable&quot;. A great deal of frustration was associated with using these services. Respondent's
			were &quot;exasperated&quot;, &quot;befuddled&quot;, felt &quot;irritation&quot; and &quot;inadequacy&quot;.
		</li>
	</ul>
	<ol start="6">
		<li>After viewing hits, headlines, abstracts, texts relevant information may be identified. Relevance was
			identified by recognising significant terms. Additional or more appropriate terms were identified and
			searches refined and redefined. Useful terms and bibliographic data were noted. Searches became more precise
			with more boolean &quot;anding&quot;. Again iteration, narrowing, broadening, redefinition, choosing
			alternative systems and locations took place.</li>
	</ol>
	<ul>
		<li>Respondents were &quot;excited&quot;, &quot;relieved&quot;, once relevant material was found.</li>
	</ul>
	<ol start="8">
		<li>Once material was found either electronically in full text or via locators, such as library call numbers,
			extensive browsing of the location and the media took place. Shelved material was located via call numbers
			and titles browsed for specific or significant terms.</li>
	</ol>
	<ul>
		<li>In both journals and books contents pages, chapter/article headings and sub-headings, indexes were scanned.
			Introductory sentences and paragraphs, conclusions, citations were also scanned. Other criteria such as
			format and appropriateness of theinformation were considered.</li>
	</ul>
	<ol start="10">
		<li>Respondents captured information but also returned to earlier processes of choosing systems, services,
			defining the topic, refining and redefining the search.</li>
	</ol>
	<h2>Conclusion</h2>
	<p>The conceptual framework (the triadic environmental, behavioral and cognitive dimensions) developed on the basis
		of previous user studies, information retrieval, systems analysis and HCI studies helped the researcher to
		determine what data should be collected. It also helped to identify appropriate research techniques such as the
		talk-through technique to capture cognition. Recognising that respondents are involved in a highly interactive
		and contextually sensitive sense-making process has also influenced the choice of a qualitative and ethnographic
		approach.</p>
	<p>The techniques for data capture were able to be used by relatively inexperienced researchers i.e. the students.
		These techniques served to provide a rich picture of the respondents' user requirements and identified six main
		tasks and sixty-three sub-tasks. There was evident correspondence with previous findings such <a
			href="#ref1">Kuhlthau's</a> (1991); &quot;initiation&quot;, &quot;selection&quot;, &quot;exploration&quot;,
		&quot;formulation&quot;, &quot;collection&quot; (but not &quot;presentation&quot;), as well as the affective
		dimension. <a href="#ref">Eisenberg's and Brown's</a> (1992) categories of information skill, &quot;task
		definition&quot;, &quot;development of information seeking strategies&quot;, &quot;location and access&quot;,
		&quot;information use&quot;, can also be recognised in the tasks and sub-tasks identified.</p>
	<p><a href="#ref3">Ellis’</a> (1993), &quot;starting&quot;, &quot;chaining&quot;, &quot;browsing&quot;,
		&quot;differentiating&quot;, &quot;extracting&quot;, &quot;verification&quot; and &quot;ending&quot; were also
		identified. Chaining however was less apparent perhaps because this tends to be associated with the later stages
		of the research process, which was not studied. This is also true of &quot;verification&quot; and
		&quot;ending&quot; or Ellis’ &quot;monitoring&quot; which were not evident in this study due to the nature of
		the task.</p>
	<p>To develop an information product that meets these needs will require a great deal of work in the areas of,</p>
	<ul>
		<li>
			<p>generating metadata about collections, media, and their information content and the character of
				&quot;texts&quot;.</p>
		</li>
		<li>
			<p>Enabling subject definition and the identification of search terms will also require the development of
				tools that the user can interact with, rather than automatically generating terms in the background.</p>
		</li>
		<li>
			<p>Digitisation of key parts of media such as contents pages, chapter headings, sub-headings and
				introductions is necessary to aid relevance judgements.</p>
		</li>
		<li>
			<p>Cognitive tasks such as being able to broaden or narrow the search will need to be supported.</p>
		</li>
	</ul>
	<p>Some of these features are already evident in evolving information products. However no system or product
		currently supports the full range of requirements identified.</p>
	<p>It should be emphasised that, although the study can be seen to have implications for an information product for
		students, the study did not cover the entire research and report generation process and that due to the
		qualitative nature of the study generalisations and solutions are specific to the community studied. However
		judging from the literature and current digital library and information retrieval solutions some of these
		requirements may be generic and extend beyond the specific community.</p>
	<p>Individual tasks and sub-tasks need further research in terms of the respondent's perception of the task. This
		would involve additional research techniques. The effect of personal factors (such as psychology or knowledge)
		may be significant and may, for example, have impact on the &quot;look and feel&quot; of the interface design as
		well as the undertaking of specific tasks. Different types of question, task and role will also have
		implications for the user requirements.</p>
	<h3>Acknowledgements</h3>
	<p>I would like to thank previous researchers in the areas of user studies, information retrieval and human computer
		interface design who inspired and provided the bedrock for this work. In addition I would like to thank students
		of the Division of Information Studies at Nanyang Technological University, who I hope will be able to apply
		some of these approaches to the development of their own information services and products. I would also like to
		thank my colleagues for their invaluable feedback.</p>
	<h2><a id="ref"></a>References</h2>
	<ul>
		<li>Allen, B. L. (1991). Cognitive Research in Information Science: Implications for Design. In Williams, M.
			(ed.) <em>Annual Review of Information Science and Technology</em> (ARIST), Medford, NJ: Learned
			Information, 26, 3-37</li>
		<li>Bandura, A. (1986). <em>Social foundations of thought and action: A social cognitive theory</em>. Englewood
			Cliffs. NJ: Prentice Hall</li>
		<li>Borgman, C. L. (1996). Why are online catalogs still hard to use? <em>Journal of the American Society for
				Information Science,</em> 47(7), 493-503</li>
		<li>Brown, D. (1994). STUDIO: <em>Structured User-interface Design for Interaction Optimisation</em>. London:
			Prentice Hall</li>
		<li>Churchill, G. A. (1995). <em>Marketing research: methodological foundations</em>. Fort Worth, TX: The Dryden
			Press.</li>
		<li>Dervin, B. (1992). From the mind's eye of the user: The sense-making qualitative-quantitative methodology.
			In J.D. Glazier, R.R. Powell (eds_.), Qualitative research in information management_, 6-84,. Englewood ,
			CO: Libraries Unlimited.</li>
		<li>Dervin, B. (1994). Information – Democracy: An Examination of Underlying Assumptions. <em>Journal of the
				American Society for Information Science</em>. 45(6), 369-385</li>
		<li>Dervin, B. and Nilan, M. S. (1986). Information needs and uses. In Williams, M. E. (ed.) <em>Annual Review
				of Information Science and Technology</em>, Medford, N.J.: Knowledge Industry Publications, Inc., 21,
			3-33</li>
		<li>Eisenberg, M. B. &amp; Brown, M. K. (1992). Current themes regarding library and information skills:
			research supporting and research lacking. <em>School Library Media Quarterly</em>, 20(2), Winter, 103-110.
		</li>
		<li>Ellis, D. (1993). Modelling the information seeking patterns of academic researchers: a grounded theory
			approach. <em>Library Quarterly</em>, 63(1), 469-486</li>
		<li>Ellis, D. (1996). The Dilemma of Measurement in Information Retrieval Research. <em>Journal of the American
				Society for Information Science</em>, 47(1), 123-136</li>
		<li><a id="ref1"></a>Garvey, W. D. Nan Lin and Nelson C. E. (1971). A comparison of scientific communication
			behaviour of social and physical scientists_. International Social Science Journal_, 23(2), 256-272</li>
		<li>Hartson, H. R. and Boehm-Davis, D. (1993). User interface development process and methodologies.
			<em>Behaviour &amp; Information Technology</em>, 12(2), 98-114</li>
		<li>Hewins, E. T. (1990). Information Need and Use Studies, vol. 25 of <em>Annual Review of Information Science
				and Technology</em>, ed. Williams, M. American Society for Information Science: Elsevier Science
			Publications, 142-172</li>
		<li>Hill, S. (1995). <em>A practical introduction to the Human-Computer Interface</em>. London: DP Publications
		</li>
		<li>Hogeweg-De Haart, H. P. (1983). Characteristics of Social Science Information: A Selective Review of the
			Literature. Part I. <em>Social Science Information Studies</em>, 3, 147-164</li>
		<li>Hogeweg-De Haart, H. P. (1984). Characteristics of Social Science Information: A Selective Review of the
			Literature. Part II. <em>Social Science Information Studies</em>, 4, 15-30</li>
		<li>Howelett, V. (1996). <em>Visual interface design for Windows: effective user interface for Windows 95,
				Windows NT and Windows 3.1.</em> New York: John Wiley</li>
		<li>Ingwersen, P. (1996). Cognitive perspectives of information retrieval interaction: elements of cognitive IR
			theory. <em>Journal of Documentation</em>, 52(1), 3-50</li>
		<li>Kuhlthau, C. C. (1991). Inside the search process: Information Seeking from the User's Perspective_. Journal
			of American Society of Information Science_, 42(5), 361-371</li>
		<li>Mehdi Sagheb-Tehrani (1995). Knoweldge acquisition process some issues for further research. In Aamodt, A.
			and Komorowski, J. (eds). Proceedings of the <em>Scandinavian Conference on Artificial Intelligence.</em>
			Trondheim, Norway, May 29-31. Amsterdam: IOS Press. 448-452</li>
		<li>Miles, B. M. Huberman, A. M. (1994_). Qualitative data analysis: an expanded sourcebook._ Thousand Oaks, CA:
			Sage Publications.</li>
		<li>Neuman, W. L. (1997). <em>Social research methods: Qualitative and Quantitative Approaches.</em> Boston:
			Allyn and Bacon</li>
		<li><a id="ref2"></a>Nicholas, D. (1996). <em>Assessing information needs: tools and techniques</em>. London:
			Aslib</li>
		<li>Patton, M. Q. (1990). <em>Qualitative evaluation and research methods</em>. Newbury Park, CA: Sage
			Publications</li>
		<li>Preece, J. Roger, Y. Sharp, H. Beyon, D. Holland, S. &amp; Carey, I. (1994). <em>Human Computer
				Interaction.</em> Wokingham, UK: Addison Wesley Publishing</li>
		<li>Robinson, B. Prior, M. (1995). <em>Systems Analysis Techniques</em>. London: International Thompson Computer
			Press.</li>
		<li>Sebillotte, S. (1988). Hierarchial planning, a method for task analysis: the example of office task
			analysis. <em>Behaviour and Information Technology</em>, 7(3), 275-293</li>
		<li>Shackel, B. (1997). Human-computer interaction-whence and whither? <em>Journal of the American Society for
				Information Science,</em> 48(11), 970-986</li>
		<li>Simonsen, J. Kensing, F. (1997). Using ethnography in contextual design. <em>Communications of the ACM</em>,
			40(7), 82-83, 84, 86, 88</li>
		<li>Streatfield, D. (1983) Moving towards the information user: some research and implications. <em>Social
				Science Information Studies,</em> 3, 223-241</li>
		<li>Tudor, D. J. Tudor, I. J. (1997). <em>A comparison of structured methods</em>. Houndmills, UK: Macmillan
			Press.</li>
		<li>Underwood, P. G. (1996). <em>Soft systems analysis and the management of libraries, information services and
				resource centres</em>. London: Library Association</li>
		<li>Westbrook, L. T. (1993). User Needs: A synthesis and analysis of current theories for the practitioner.
			<em>RQ</em>, 32, 541-549</li>
		<li><a id="ref3"></a>Wilson, T. D. (1981). On user studies and information needs. <em>Journal of
				Documentation</em>, 37(1), 3-15</li>
		<li>Wilson, T. D. (1994). Information Needs and Uses. In Vickery, B. (ed_). Fifty years of information progress:
			A Journal of Documentation Review_. London: Aslib 15-51</li>
		<li>Wilson, T. D. Streatfield, D. R. &amp; Mullins, C. (1979). Information Needs in Local Authority Social
			Services Departments: A Second Report on Project INISS. <em>Journal of Documentation,</em> 35(2), 120-136
		</li>
		<li>Wixon, D. Ramey, J. (1996). <em>Field methods casebook for software design</em>. New York: John Wiley &amp;
			Sons.</li>
		<li>Zikmund, W. G. (1997). <em>Business research methods</em>. Fort Worth, TX: The Dryden Press.</li>
	</ul>
	<p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October 1998</strong><br>
		<em>Investigating methods for understanding user requirements for information products</em>, by [Mark
		Hepworth](MAILTO: asmark@ntu.edu.sg)<br>
		Location: http://InformationR.net/ir/4-2/isic/hepworth.html    © the author, 1998.<br>
		Last updated: 9th September 1998</p>

</body>

</html>