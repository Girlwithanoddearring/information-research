# ![infres](../infres.gif)

## **Volume 4 No 2 October 1998**

#### _Information Research: an electronic journal,_ is published four times a year by Professor T.D. Wilson of the Department of Information Studies, University of Sheffield _in association with_

##### University of North Texas, USA (Regional Editor, [Dr. Amanda Spink](mailto:spink@lis.admin.unt.edu )); University of Tampere, Finland (Regional Editor, [Dr. Reijo Savolainen](mailto:liresa@uta.fi)) and the University of Vilnius, Lithuania (Regional Editor, [Dr. Elena Maceviciute](mailto:elena.maceviciute@kf.vu.lt))

#### ISSN 1368-1613

## Contents

#### [Editorial](editor42.html)

### Refereed Papers

#### [Information contracting tools in a cancer specialist unit: the role of Healthcare Resource Groups (HRGs)](paper52.html), by Carol Marlow, Royal Marsden Hospital, London and Hugh Preston, Department of Information and Library Studies, University of Wales, Aberystwyth

#### [Searching heterogeneous collections on the Web: behaviour of Excite users](paper53.html), by Amanda Spink & Judy Bateman, University of North Texas, Denton, Texas, USA, and Major Bernard. J. Jansen. United States Military Academy, West Point, New York, USA

### Working Papers

#### [Student attitudes towards electronic information resources](paper54.html), by Kathryn Ray & Joan Day, Department of Information and Library Management, University of Northumbria at Newcastle, UK

#### [Doctoral Workshop Papers from "Information Seeking in Context" - an International Conference on Information Needs, Seeking and Use in Different Contexts](docpaps.html)

#### [Reviews](../reviews/reviews.html)

#### [World List of Departments and Schools of Information Studies, Information Management, Information Systems, etc.](http://InformationR.net/wl/)

### If you find Information Research useful, please [sign in](../register.html) and we'll notify you of future issues.

_Information Research_ is designed, maintained and published by by [Professor Tom Wilson.](mailto:t.d.wilson@sheffield.ac.uk) Design and editorial content © T.D. Wilson, 1995-98