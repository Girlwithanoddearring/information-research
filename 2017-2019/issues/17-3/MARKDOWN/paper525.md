#### vol. 17 no. 3, September 2012

# The impact of information behaviour on small business failure

#### [Rita Marcella](#authors) and Laura Illingworth  
Aberdeen Business School, Robert Gordon University, Garthdee Road, Aberdeen, AB10 7QE, United Kingdom

#### Abstract

> **Introduction.** This paper explores the impact of information behaviour on small business failure in the UK.  
> **Method.** The sensitive subject was approached through the use of unstructured telephone interviews with twenty small business owners who had recently closed their business or were going through the closure process.  
> **Analysis.** Interviews were recorded digitally and transcribed. These transcripts were then analysed to identify the important themes and issues emerging.  
> **Results.** There had been little information seeking conducted by business owners both before business set up and at critical points of operation. Where information had been sought, it had been from a limited range of sources and providers, who were frequently regarded by business owners as unprofessional and lacking in expertise. Indeed, many judged the quality of the information provided largely on the basis of their subjective response to the person providing it, rather than any other, potentially more robust criteria. Respondents expressed feelings of loneliness and isolation, and indicated a need for a 'no-strings-attached' networking or mentoring programme. Despite their negative experiences, almost all participants indicated that they might start another business, and were keen to offer practical advice to others considering start-up.  
> **Conclusions.** The findings reinforce the academic literature in highlighting the importance of information use by small businesses at all stages of the business cycle. Further research might explore the construction of a network, aimed at combating isolation through meaningful interaction with sources of expert and trusted advice.

## Introduction

Small or medium-sized enterprises (SMEs) in the UK account for almost 60% of private sector employment and 50% of its turnover ([Department for Business Innovation and Skills 2010](#dep10)) and have suffered disproportionately through the recession ([Guardian 2009](#gua09)), struggling to cope with changing patterns of consumption and inaccessible credit ([Pearce and Michael 2006](#pea06)). Business failure is a not uncommon experience for business owners ([Pratten 2004](#pra04)), indeed the most likely outcome for start-ups, yet there is little understanding of why and how it happens. The lack of research into business failure is attributed to lack of access to robust data and to participants willing to be open about their experience. Such research would, however, yield valuable insights into the necessary conditions for small businesses to survive in economically challenging times and assist in the formulation of Government support strategies (see, for example, [Chittenden _et al._ 1993](#chit93)). Abdelsamad and Kindling endorse this view: 'although failures cannot be completely avoided in a free enterprise system, the failure rate could be reduced if some of its causes are recognised and preventative action is taken' (cited in [Everett and Watson 1998](#eve98): 372).

This research aims to:

*   deepen understanding of how small business owners are coping in the current economic downturn;
*   identify internal and external causes of small business failure in the UK; and
*   explore the influence of information behaviour on small business failure.

The impact of information behaviour was explored at critical points in the life cycle of the business, identifying the lessons the owners have learnt from their experiences of failure, as well as those they may not have consciously recognised as having an informational dimension. This is in line with Koenig (1992), who argues that 'we need more research on the relationship between the information environment and organisational productivity' (cited in [Vaughan 1999](#vau99): 194), and with Duncan (1992), who suggests that such research could help 'resolve the scepticism that information is simply an additional cost that does not yield measurable benefits' (cited in [Vaughan 1999](#vau99): 194).

The research makes an original contribution to small business and information management literature as this is an area that has seldom been studied in recent years, where research subjects have been difficult if not impossible to access.

### Definitions of failure

Much conceptual discussion has focused on the variety of ways in which business failure can be defined. The definition is critical as this may highlight particular outcomes while ignoring others. Failure is generally regarded as the discontinuance of the business due to a lack of adequate financial resources ([Everett and Watson 1998](#eve98)) and '_involves liquidation of insolvent companies and personal bankruptcy_', excluding business discontinuation where the business could have continued to operate ([Burns 2007](#bur07): 340). Fredland and Morris ([1976](#fre76): 7), however, argue that discontinuance of a business can be used as a '_proxy for failure_', as '_discontinuance suggests that resources have been shifted to more profitable opportunities_'. Cochran takes the pragmatic view that '_failure should mean inability to 'make a go of it' whether losses entail one's own capital or someone else's_' ([Cochran 1981](#coc81): 52). Watson and Everett ([1996](#wat96)) identified five categories of failure: ceasing to exist (for any reason); closing or change in ownership; filing for bankruptcy; closing to limit losses; and failing to reach financial goals. Although some close because of a successful sale ([Bates 2005](#bat05)), many do so because the business has become insolvent ([Politis and Gabrielsson 2009](#pol09)).

Longenecker _et al._ ([1999](#lon99)) argue that some definitions focus purely on the reality of ceasing to trade, while others recognise a more human sense of failure to manage a business in a way that will enable it to operate sustainably. This study uses the term failure for any form of closure, either through bankruptcy, liquidation, prevention of further losses, to re-start another business, and/or due to personal choice (such as early retirement). It does not, therefore, presuppose any actual personal failure on the part of the owner or manager, although the research team's primary interest is in the extent to which the business could have continued to operate profitably had the owner or manager had better access to information.

### The impact of information use in business

The effective use of information can produce a number of benefits for the small business including reduced administrative costs, more effective collaboration with external parties and increased customer service through greater efficiency ([NetMonkeys 2009](#net09)). However, many in business remain unconvinced of the utility and value of information in their business practice, despite information theorists arguing that there is a '_need to have access to adequate information to increase and sustain their [business] competitiveness_' (see, for example, [Chiware 2007](#chiw07): 136). One barrier for small businesses is the lack of staff, time and resources to find and manage information ([Lybaert 1998](#lyb98)). Rowley ([2004](#row04): 5) found that '_small business organisations are notorious for their slowness to invest in and adopt, even the most basic uses of information technology, let alone appreciate the strategic impact that effective information systems can have on the success of a business_'. Chiware ([2007](#chiw07): 136) argues that access to information '_provides the enterprises with a competitive edge in running their business ... a major constraint to developing a dynamic SMME [small, medium and micro enterprise] sector is the accessibility of validated relevant information_'.

Lybaert ([1998](#lyb98): 171) attributes the failure of small business to appreciate the contribution of good information access to the fact that the impact of information access on business success has not been demonstrated convincingly: the '_explanation for the differences in [SME] performance is very complicated in view of the many influences on success. One of the factors that is frequently referred to is the use of adequate information and a sound information system_'. She highlights the importance of the small business owner understanding how information can help them to be successful, through findings which show that SME owners who use more information achieve better results and are, interestingly, more optimistic about the future: '_entrepreneurs value information to arm themselves for the future_' ([Lybaert 1998](#lyb98): 188). Rather than owners' attitudes to information, the present research seeks to explore the more subtle impact that poor information access may have had on actual performance, whether or not the entrepreneur is aware that this is the case. It also seeks to explore links between information behaviour and failure rather than success: the difficulty lies in demonstrating absolutely impact on success, while the current research suggests that impact on failure may conversely allow a more concrete conceptualisation and demonstration of impact.

This hypothesis is supported by earlier literature where, despite general academic consensus that access to information does contribute to success, owners and managers of small businesses remain reluctant to research and resource information. In a Canadian study, one third of business owners did not actively seek information; while most small business owners turned to their colleagues, clients and suppliers for information and advice ([Bouthillier 2003](#bou03)). Johnson and Kuehn ([1987](#john87)) and Vaughan ([1999](#vau99)) agree that small business managers rely more on personal contacts and informal sources of advice. Bouthillier ([2003](#bou03): 126) recognised, however, that informal sources, though convenient, '_do not always provide efficient access to the strategic and innovation information that is needed to remain in business_'. As Rowley argues: '_an armoury of formal and informal channels must be enlisted [by the business owner]_' ([Rowley 2004](#row04): 4).

Although the academic literature provides some evidence of the importance of information, both formal and informal, there has been little recent research into the information behaviour of UK small business owners. This is of particular interest during the current economic downturn, while financial challenges continue to predominate. In such challenging times, where it is ever harder for the small business to be successful, does information become more or less valuable?

## Research methods

One of the issues associated with investigating the impact of effective information use on business success is that studies have tended to focus on asking owners or managers for their views, when these individuals may not be best placed to judge. How can someone who has failed to acquire information which might have helped them know that the lack of that information was a critical factor in their business failing? Equally, when studying business success it is frequently difficult to isolate information behaviour, or indeed any other single factor, as being more or less influential. Typically the owner or manager is more inclined to attribute the predominant influence to personal leadership traits. This study will hope to illuminate our understanding of this complex area by analysing the impact of information incidents on the story of a business failure, by listening to twenty business owners' accounts of the life cycle of their business, from inception to closure. By asking participants to tell the story of their business, the researchers avoid making a priori assumptions about the role of information but can analyse these stories to identify the points at which information might have had an influential role, positive or negative.

Robert Gordon University had conducted an earlier, web-based survey of members of the UK Federation of Small Businesses (FSB). Of the 6,289 survey respondents, 26% envisaged closure in the near future (see Figure 1).

<figure>![Figure 1: Survey results](p525fig1.jpg)

<figcaption>Figure 1: Survey results</figcaption>

</figure>

Of those contemplating closure, 131 expressed willingness to participate in a follow-up interview. These formed the target population for the current research, from which the team randomly selected twenty interviewees covering a variety of business sectors and geographic locations across the UK (see Figure 2).

<figure>![Figure 2: Location of survey respondents](p525fig2.jpg)

<figcaption>Figure 1: Location of survey respondents</figcaption>

</figure>

It is widely recognised that it is very difficult for researchers to access owners of closed businesses, as they are virtually impossible to trace ([Everett and Watson 1998](#eve98); [Stokes and Blackburn 2002](#sto02); [Harada 2007](#har07)). As a result the interviews forming the basis of the current research provide a unique resource.

Interview questions were designed to be open ended, to encourage free, non-predetermined responses and to elicit narratives or stories about the participants' personal experiences of business failure. The use of storytelling as a qualitative method is increasingly being recognised in both social science and entrepreneurship research (see, for example, [Steyaert and Bouwen 1997](#ste97); [Rae 2000](#rae00)), with a greater focus on human and cultural rather than economic aspects of entrepreneurship ([Hill and McGowan 1999](#hil99)). Indeed, Denning ([2005](#den05)) believes that any research that does not recognize the value of storytelling or narrative approaches as a way of understanding organisational performance and culture will not provide a full or true account of that organisation.

Narratives are held to be particularly useful when conducting research on a sensitive subject such as business failure. For some business owners, failure may be an experience of which to be ashamed and which they are unwilling to discuss ([Johansson 2004](#joha04)). Respondents were therefore asked non-judgmental questions in a way that would encourage extended narrative response. For example, interviewees were asked 'Can you take me through the steps you took to start up your business?' and 'Can you tell us about a point when you realised that your business might be in danger of closing down?'

The researchers believe that this storytelling approach was particularly successful. All respondents were open and spoke very freely about their experiences, requiring few prompts or clarifications, and typically interviewees exceeded expectations in terms of the scale, depth and detail of data revealed. Some expressed pleasure and satisfaction at being able to talk about their experiences of business failure and hoped that these could be valuable to others. The research team presented the broad findings to the Institute for Small Business and Entrepreneurship Conference in 2009 ([Marcella and Illingworth 2009](#marc09a)). The present paper explores further the information behaviour aspects of the research findings.

All interviews were recorded and transcribed fully before qualitative analysis and coding was carried out. This analysis was conducted twice with each of the authors working independently to ensure that all inferences drawn were valid. Although respondents were coded in industry sectors, these codes were not designed to enable comparison across sectors as the numbers involved were too small to enable such conclusions to be drawn.

## Results

The results of the study will be discussed in relation to significant stages in the life cycle of business operation: before set up, at critical points during business operation, and during business closure.

### Information gathering before set up

The first stage of business practice where access to and use of reliable information might be deemed valuable is the point at which the individual was thinking about and preparing for the set up of their business. There are conceivably numerous categories of information which might be useful at this stage, such as: the best legal basis on which to set up the business; how best to access finance; where to locate; which suppliers to use; whether to rent or buy property; whether to employ staff and on what terms; what the market is and/or could be; and whether there are likely or predictable changes in the external environment which might impact on business success. And what typically did our respondents do? Very little, it would appear.

Many of the small business owners admitted to conducting no research at all at this stage, with some not even talking to their banks about variant funding options or conducting any investigation of the potential market:

> _I didn't do any of the demographic bits. I just went in blind._ (Respondent H, pet shop)

Indeed, this same interviewee had no knowledge before set up of the basic business realities such as the differential nature of business contracts, which had subsequently led to serious financial concerns:

> _I didn't realise all the other little bits. Why do you have to have business electric? Why do you have to have business water rate?_

Many of the business owners had operated on the misguided but apparently widespread assumption that if you have been a consumer of something you will know how to provide it and tended to describe what they did as intuitive:

> _The research I did was more into what would sell ... I haven't run a cafe before ... I think we were marginally under-researched._ (Respondent T, wholesale tea business and tea house)

A final group of business owners had sought external advice before setting up their business, typically from accountants, bank managers and business organisations; but they reported that they had met with varying degrees of success in terms of the quality and reliability of that information. One respondent had sought advice from an accountant friend:

> _We sat down and looked at how much we were likely to need and what the turnover was likely to be and he basically said it wouldn't work, so we had another wee look at it and tweaked a few things and he said that'll work._ (Respondent B, construction company)

This quote illustrates a surprisingly common lack of factual objectivity or realism in the informational base upon which decisions were made.

Another respondent had utilised a Business Link start up scheme but dismissed their advisors as

> _a bunch of jumped up guys who were in positions where they felt they know best ... they kind of look down on you a little bit._ (Respondent G, design consultancy)

Typically, respondents tended to judge information quality on their subjective reaction to the person providing it, rather than on any more analytic or robust model. This was found to be the case at all stages of the business cycle and may partially explain the preference, often cited in the literature, for informal sources of information and advice.

Generally, the results indicate that few participants conducted any form of research before starting their business, and any advice sought by business owners was not always felt to be helpful. Clearly the drive to get started overrode any perceived need for planning amongst this group of respondents. The overwhelming response was that such pre-planning and information gathering in support of critical decision making was felt to be unnecessary at the time. Respondents tended to have been over-confident in their belief that they knew best and that their instincts, even if unproven, were sound. As research has shown that without self-confidence most people would not start a business, then clearly confidence is an important trait in the business owner. However, this research would suggest that, if untempered by an awareness of the limits of one's knowledge, then many start-up decisions will inevitably be flawed.

### Information behaviour at critical points of business operation

When asked to talk about the critical points at which they came to realise that their business might fail, many respondents noted the speed and unpredictability with which the economic downturn had hit and the unexpected severity of its impact:

> _It all happened extremely quickly._ (Respondent D, construction company)

> _It's happened on a huge scale._ (Respondent S, construction company)

As a result, they had little in reserve to counteract the impact of the recession. One interviewee, who had been in business for a number of years, expressed a common theme:

> _I've been through some tough times in my life but this is like nothing else really. It's like looking down a gun barrel with a maniac at the other end._ (Respondent S, construction company)

It is undeniable that the downturn was unpredictable in scale of impact: no one was advising businesses to proceed with prudence in early 2008.

In line with findings in other industry sectors, some respondents found that the economic downturn freed time for business owners to be creative, innovative and to re-think how they do business:

> _One thing about recession I've learned … it is an opportunity._ (Respondent K, estate agent)

> _The best ideas come out ... because people are having to be innovative._ (Respondent T, wholesale tea business and tea house)

Equally, the economic downturn forces business owners to re-evaluate their cost base and to look for new markets. One interviewee, forced to reduce her overheads, commented:

> _The downturn and the financial crisis has actually worked in my favour._ (Respondent I, health and beauty products)

Many interviewees faced decision making dilemmas at these crucial junctures, '_before you lose everything you've got_' (Respondent S, construction company). Respondent O, the owner of a printing business, asked himself if there was any point in continuing '_if I'm just borrowing myself into a hole?_', in line with Ucbasaran _et al._ ([2009](#ucb10)), who found this a common reason for many businesses closing. However, respondents frequently did not have access to the kind of data that would help them to make the best decision. In difficult times businesses turn to banks for extra funding to ameliorate cash flow reductions, but also to innovate or improve their business, through, for example, increased marketing. However, as soon as the recession hit the UK it became increasingly difficult to access such funds.

There was confusion around the availability of business loans, with mixed messages being received from the media, government and the banks:

> _The bank told me when I asked about a start up loan, that the business had to be started up before I could apply for a start up loan._ (Respondent F, maternity wear shop)

There was also confusion about the financial help available to small business owners directly from government sources through grants. Business owners were particularly uncertain about what to expect from banks and this may of course relate to the current recession being attributed widely to the earlier actions of financial institutions, as well as to the rapidly changing policies of many institutions as the recession progressed. In difficult and changing times, it is particularly vital to know what the best terms are likely to be in businesses with a small (and ever tightening) margin. There were uncertainties at the most basic level amongst participants, often at highly critical points such as loan negotiation; for example, many were confused about the basis for small business loan interest rates and that these were based on LIBOR (London Interbank Offered Rate, calculated daily) rather than Bank of England rates (much quoted in the media), which tend to be significantly lower.

Seeking information from banks had proven troublesome for a number of respondents. For example, Respondent C, the owner of a needle craft business, noted: '_whenever I tried to get in touch ... it was always on voicemail and calls are never returned_'. By the time she spoke to her dedicated manager the '_figures were dreadful and that was it really_'. Respondents had also encountered poor responses from advisers, suppliers, government agencies, liquidation officers and mortgage providers. One felt let down by his local council who had decided to introduce a city centre refurbishment scheme:

> _The council didn't make people really aware of how it was going to disrupt the town centre._ (Respondent E, restaurant)

For many interviewees, changes in the market and bank policies since the economic downturn had led to their feeling '_terribly vulnerable_'. Uncertainty and insecurity were commonly expressed themes, particularly amongst business owners who had been forced into administration in very stressful circumstances, where the handling of the situation by mortgage lenders, banks and insolvency practitioners had created further frustration and stress, particularly in terms of lack of communication and unwillingness to engage in dialogue.

Overall, respondents lacked access to independent, fair and objective information from financial institutions at critical points for their businesses. Interestingly, the International Financial Reporting Standards (IFRS) Foundation are introducing new minimum benchmarks, with an emphasis on the meaningful information that should be provided and the clarity of the message for consumers ([Wood and Hutchinson 2010](#woo10)), indicating that there is acceptance in the financial sector that communications and information transmission were fatally flawed both before and during the recession.

### The complexity of the information landscape

In addition to financial uncertainties, the vast majority of respondents identified problems with the amount and credibility of information and advice available to small business owners. Said and Hughey ([1977](#sai77)) believed that 75% of failures could be avoided if sound advice is available and accepted. One of the most prevalent challenges for respondents was not knowing whom to ask for information or business advice:

> _There was a lack of support in as much as I didn't know who to turn to._ (Respondent O, printing business)

> _People will talk to you about what they are prepared to offer but not about the alternatives._ (Respondent Q, caravan park)

In line with previous research ([Vaughan 1999](#vau99), [Bouthillier 2003](#bou03)), respondents had been directed to sources of advice, but the information received was frequently flawed, resulting in poor decisions being made by the small business owner:

> _We took advice from friends and colleagues and they put us in touch with professional people and what we've ended up with is the wrong finance package._ (Respondent L, hotel)

In many cases, respondents reported receiving information from apparently authoritative sources that was deficient:

> _Their [advisors'] answers aren't practical and you never see anybody and you're always talking to people on the phone, you never get them to come here and get a feel for what's going on._ (Respondent O, printing business)

Equally, respondents were inundated with information, largely unsolicited, from organisations and individuals providing misleading or incorrect information designed to sell services rather than information:

> _The amount of rip-off people, the leeches that seem to know that you've just opened a new business._ (Respondent H, pet shop)

Those that were in the process of bankruptcy or had been put into administration seem to have been particularly poorly informed about the process and the options open to them, with the information being either non-existent or very confusing. Indeed, respondents were frequently given contradictory advice:

> _I've been asking about bankruptcy and closing down and I've spoken to a solicitor and an insolvency practitioner and they're both giving me completely different advice._ (Respondent C, needle craft business)

Having too much advice and particularly conflicting advice can be as frustrating as having too little. Some respondents complained that there was too much information, especially on the Internet, and that this information was often contradictory and confusing:

> _If anything there was too much information around._ (Respondent R, fabric wholesaler)

> _It basically confused me even more … you get little tit-bits out of each person._ (Respondent T, wholesale tea business and tea house)

Others were confused about the myriad of support agencies which seemed to exist.

Some respondents found the professional advice available disappointing and lacking in expertise, leading to it being disregarded in decision making:

> _I talked to a few advisors but I came to the conclusion that they all had different views and so you had therefore to ... make the decision yourself._ (Respondent T, wholesale tea business and tea house)

Business Link, an organisation created to support business owners, in particular, were the subject of a number of complaints. For example, one said:

> _I didn't think they were that good ... they make out they're interested in small business but I don't think they are._ (Respondent P, carpentry business)

This same respondent also complained about his experience with a leading business organisation which he felt had not

> _done anything for me. They gave advice on a legal matter which basically said there's nothing you can do about it. It would cost me more money than I've lost._

Although, if this advice did save cost, it may have been in fact very valuable. Respondents typically reacted negatively when given information that they found unpalatable, suggesting again an emotive rather than rational response.

This research has identified a complex variety of ways in which information might be deemed poor quality. Much of the existing research into information behaviour has focused on the criteria upon which the quality of information can be evaluated. There may also be merit in examining the converse, that is what factors render information deficient, which might include not just the inherent qualities of the information but also how it is received, the extent of conflicting information and the attributes of the information provider.

### The isolation of the small business owner

Respondents spoke feelingly about how lonely the experience of being a small business owner had been:

> _It's quite a lonely place, entrepreneurialism._ (Respondent T, wholesale tea business and tea house)

However some believed this loneliness was due to the lack of networking and support available:

> _I think we need a self help group. Somebody else to speak to that's been through it as well, because it is very lonely._ (Respondent C, needle craft business)

> _When you are one person doing everything that's the hardest because you get into a rut of doing the same thing ... It would be good to have some sort of club, where we could network with a few other people ... a network of support._ (Respondent I, health and beauty products)

And yet in reality there are a multitude of business networks of which respondents were unaware, suggesting that, though the concept might be sound, unless connections can be made that are meaningful to business owners then these networks will never achieve the success enjoyed by social networking.

One of the greatest challenges surrounds the capture of true expertise and experience. There was little agreement amongst respondents as to what form of expertise would be most helpful. Some favoured introducing a network providing access to mentors with business acumen; impartial individuals able to advise on business plans, with real-life experience and specifically not recent graduates or theorists. As one interviewee noted:

> _There's not enough grey heads … What I'd like to see is an older, wiser head to give some stability and credibility, like a Harvey Jones type of guy._ (Respondent S, construction company)

Although this same respondent also expressed a degree of caution, stating that he would only take advice '_if there weren't too many strings attached_'. Another interviewee demonstrated a subtle suspicion of government agencies, preferring a network

> _that wasn't government led, maybe government funded, but not government organised._ (Respondent T, wholesale tea business and tea house)

However, in a highly competitive commercial world there are barriers to networking:

> _The only person who would understand my business is a competitor ... I can't talk to competitors, you can't even socialise [with them] because you always get talking about business._ (Respondent S, construction company)

In some cases networking might verge on illegality, indicative of cartels and price fixing. Worries about intellectual property and commercial confidentiality were also expressed. However, in line with Deakins and Freel ([1998](#dea98): 150), who conclude '_without existing contacts it is important that entrepreneurs learn to network quickly in their industry at an early stage_', one participant had found the experience of sharing business plans beneficial:

> _Everybody chewed the fat, and we had a sort of brainstorming session … It was great, we all learned from each other._ (Respondent B, construction company)

### Post-failure business activity

Table 1 outlines the number of interviewees who had closed their business by the time of the interviews. A surprisingly high proportion (87%) would start another business.

<table class="center"><caption>  
**Table 1: Reasons for failure and number of owners who would start another business**</caption>

<tbody>

<tr>

<th>Main reason for business closing '_in the next 3 months_'</th>

<th>Total number of businesses</th>

<th>Number of businesses that closed by 31st May 2009</th>

<th>Number of owners of closed businesses who would start another</th>

</tr>

<tr>

<td>Recession</td>

<td align="center">15</td>

<td align="center">12</td>

<td align="center">9</td>

</tr>

<tr>

<td>Liquidation or administration</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">2</td>

</tr>

<tr>

<td>Early retirement</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">2</td>

</tr>

<tr>

<td>Issues with bank</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>**Totals**</td>

<td align="center">**20**</td>

<td align="center">**16**</td>

<td align="center">**13**</td>

</tr>

</tbody>

</table>

When interviewees were asked if they would consider starting another business, responses were positive, with most expressing optimism and self-belief:

> _Oh, tomorrow if I could!_ (Respondent O, printing business)

> _How boring would it be to have one business for the whole of your life?_ (Respondent T, wholesale tea business and tea house)

It can be difficult to understand why these individuals have such confidence and willingness to embark on another project so soon after failure and have such faith in their capacity to succeed. A common thread through a number of interviews was the participants' unfaltering faith in their personal acumen, although the validity of this belief may be questionable: '_strong self-confidence, in extremis, becomes 'delusional' behaviour evidenced by an excessive optimism_' ([Burns 2007](#bur07): 330).

Many respondents, in line with Stokes and Blackburn ([2002](#sto02)), had simply enjoyed being in business:

> _It is the most rewarding and fun thing that you'll ever do ... and here I am a person who has lost upwards of a hundred thousand pounds on the business._ (Respondent T, wholesale tea business and tea house)

The innate optimism of entrepreneurs (in particular serial entrepreneurs), regardless of previous experiences, has also been documented by Ucbasaran _et al._ ([2009](#ucb10)). This feeling of optimism was not universal, however. There were a small number who felt it unlikely that they would start another business:

> _Certainly not at the moment. I've still got a very bitter taste in my mouth._ (Respondent C, needle craft business)

> _No, I cannot afford for us to lose any more, I have got to have a regular income._ (Respondent M, tiling training school)

Respondents typically blamed external factors almost entirely for their business failing. Though the recession has been influential, this finding may also reflect the intrinsic confidence of those who have started up a business and a natural reluctance to recognise a personal contribution. Very few respondents recognised internal problems or took personal responsibility for their business failure. Only one respondent identified personal failings:

> _In all honesty I think I'm probably the wrong sort of temperament to have done something like this, 'cause if anything I'm too cautious, I don't gamble._ (Respondent C, needle craft business)

Otherwise owners attributed failure to 'bad timing' and believed their business would have survived in a more benign economic climate.

### Lessons learned from failure

Deakins and Freel ([1998](#dea98): 146) argue that '_the learning process in SMEs is a crucial part of the evolution of SMEs_'. From the current findings, it is clear that, although participants had few reservations about starting another business and did not acknowledge error, they had learnt from their experiences of failure. They were prepared to agree that they would do some things differently in their next venture, including moving to higher footfall retail locations, looking for better financing and borrowing less:

> _I'd be a lot more cautious before I started a business and I'd make sure it was more solid._ (Respondent L, hotel)

When asked if they could provide some advice for future small business owners, the initial response of many was simply '_don't!_' and '_go and get a job with a decent pension_'. It is particularly interesting that this is the general response despite interviewees' willingness to start another business themselves. When prompted further, interviewees did have advice to give, listed in Table 2 in order of frequency cited.

<table class="center" style="width:90%"><caption>  
**Table 2: Respondents' advice for new small business owners**</caption>

<tbody>

<tr>

<th>Advice</th>

<th>Quotes</th>

</tr>

<tr>

<td>Do your research</td>

<td>_They need definitely to research it. Is there a real need for that sort of business in the area? What level of competition would they face?_ (Respondent P, carpentry business)</td>

</tr>

<tr>

<td>Have plenty of capital</td>

<td>_Make sure you've got plenty of capital. It's going to cost them twice as much as they think._ (Respondent K, estate agent)  
_Don't underestimate the amount of cash you need. Do a proper cashflow and plan for a rainy day._ (Respondent R, fabric wholesaler)</td>

</tr>

<tr>

<td>Do not borrow more money than you need</td>

<td>_Always make sure that you don't borrow more than you need to and that your borrowings are well within your affordability._ (Respondent O, printing business)</td>

</tr>

<tr>

<td>Plan for the unexpected</td>

<td>_Go in with your eyes open, know what the downfalls are going to be ... and make sure you're prepared for them._ (Respondent B, construction company)</td>

</tr>

<tr>

<td>Speak to other small business owners</td>

<td>_Start to mix with other business people who can guide and give hints._ (Respondent I, health and beauty products)</td>

</tr>

<tr>

<td>Seek advice from business agencies</td>

<td>_For younger people out there, there are associations like the FSB who can guide you._ (Respondent A, jewellery shop)</td>

</tr>

<tr>

<td>Stick to a product or sector that you know</td>

<td>_We were trying to do something a little different, but now ... I would say ... stick to what people know ... don't try to be innovative and adventurous, stick to what's popular._ (Respondent E, restaurant)</td>

</tr>

<tr>

<td>Be prepared to work hard</td>

<td>_It's going to take up a lot of their time and energy._ (Respondent I, health and beauty products)</td>

</tr>

<tr>

<td>Be likeable</td>

<td>_People have got to like you as well. You've got to be able to sell._ (Respondent R, fabric wholesaler)</td>

</tr>

</tbody>

</table>

The importance of doing research or deepening understanding, through all stages of the business cycle but particularly before set up, was the most frequently cited piece of advice that would be given to new business owners, highlighting the importance of good access to reliable and relevant information in business start up.

It is also interesting to note, however, that a further four points in the list relate to information behaviour - seek advice, speak to other owners, stick to a field you know and plan for the unexpected - further outlining from the interviewees' perspective how gathering information, both written and verbal, formal and informal, from banks, mortgage lenders, financial advisors, other business owners and friends can all help a business become more successful. Indeed, even in terms of finance, much of the advice was about finding out what the costs of finance would be, what capital would be needed, and where to go to get the best deals.

## Conclusions

The majority of respondents had carried out little research before setting up their business. This was manifest both in terms of the business sector and markets associated with that sector, and also with regard to exploring the options open to them in terms of managing and financing the business. References to '_just going in blind_' or '_just going ahead_' support the general picture of a lack of analysis both at start up and throughout the business life cycle. Where advice or information had been sought it had been from a limited range of sources, typically the Internet, or from a limited range of providers, such as Business Link and professional advisers. There was a tendency throughout to rely on what others were saying, often interested parties such as franchise agents or inexpert parties such as '_friends_', rather than testing a range of sources to get an impartial view. Those that had approached professional bodies for information or advice felt that the information received was inappropriate, unprofessional and lacking in expertise. Where the advice providers could be categorised notionally as independent and expert, such as Business Link, very grave reservations were expressed by respondents about the depth of expertise and quality of advice provided. Many judged the quality of the information provided on the basis of their subjective response to the person providing it, rather than on the value, reliability and usefulness of the information itself. There was little sense that information might be evaluated on any other, and potentially more robust, criteria. However, this research does not demonstrate an overwhelming reliance on informal sources, and therefore challenges some of the assumptions of previous research.

In terms of financial decision making, many respondents had made the wrong choice and/or were still uncertain about the best route forward for them. Often they were confused about their options or unclear about what the implications of a particular course of action would be further down the road. Participants generally evinced a lack of trust in financial institutions and were concerned about a lack of '_independence_' in the agencies and organisations to which they might turn for help. Very frequently they simply did not know where to turn, and expressed the view that those agencies and organisations ostensibly there to help them might have another agenda - '_leeches_' - or were insufficiently '_practical_' and lacked experience and understanding of the business context. These respondents tended to feel isolated, that there was no one to whom they might turn, with many expressing a desire for a networking or mentoring programme where there were '_no strings attached_' and no other agenda.

Clearly articulated throughout the results is the hugely damaging impact of the recession on the majority of respondents, most of whom identified lack of funds as a very significant causal factor in failure, whether as a result of inability to access additional loan facilities or from the drop in turnover, with little capacity to decrease costs commensurately. The suddenness with which the financial crisis hit and the volatility with which lending policy and spending patterns varied, meant that changes in the external environment with which they might otherwise have coped proved catastrophic for many. The unpredictability and scale of the downturn meant that the experience '_was like nothing else_' and so they had little resource on which to draw, internally or externally, in order to understand how to survive the current crisis. Interestingly, these findings contrast with those of another study undertaken by the authors ([Marcella, Williams and Tourish 2009](#marc09b)) which suggest that in the ever volatile energy sector industry leaders are far better prepared to cope with rapid fluctuations between '_boom and bust_'. There were some small business owners in the present study who had managed to survive and who had identified positive benefits and new competitive opportunities in the recession, but these were very much in the minority.

Although many of the respondents were facing very significant financial losses as a result of their business failing, almost all were positive about starting another business, most commonly with an enhanced understanding of market conditions, of their business location and of the true costs of operation. They expressed enjoyment on a personal level in their experience of running their own business, with few exceptions, in line with the findings of previous studies. However, counter-intuitively, they were much more expressive of the negative aspects of their experience when asked what advice they would give to others considering start-up. This is an interesting finding as it would suggest that the respondents are much more confident about their own capacities than they are about those of others. It indicates a perhaps greater level of belief in the learning they have achieved from failure than was manifest in their description of their experience, while it raises questions about the extent to which such belief is a fair assessment or delusional. Equally interestingly, four of the top tips proffered by respondents relate to research and information gathering. '_Do your research_' they counsel others, despite (or perhaps because of) the fact that so few had done so themselves. If this finding indicates that, in starting a new business after failure, small business owners do more research and plan more effectively then it would support the widely held American belief that failure is a valuable lesson (see, for example, [Martin 2005](#mart05)).

The findings of this study reinforce the academic literature in highlighting the importance of information use by SMEs at all stages of the business cycle, from starting in business, surviving the early stages and encountering unstable market conditions. Respondents recognise that the information used should be sourced from professional and reliable bodies and individuals and support must be objective and appropriate to the business context. Business owners demonstrated little capacity at present to assess the merits of conflicting information in an analytic rather than emotional manner. The notion of a band of experienced business people who have survived difficult times would appear to be appealing to participants and the potential to learn from the experience of those in particularly volatile industry sectors might be investigated further. The lack of satisfaction expressed with existing agencies would indicate that a fresh approach to the provision of business advice and support is required, one that really recognises and responds to the practical needs of the business owners. The authors propose to consider, through further analysis of the data set, the many ways in which information might be deemed flawed by the business owner, both in terms of the information itself and the source from which it emanates and to consider how these findings might underpin the construction of a network to combat isolation through meaningful interaction with sources of expert and trusted advice.

## About the authors

**Rita Marcella** is Dean of the Aberdeen Business School, Robert Gordon University, UK. She can be contacted at r.c.marcella@rgu.ac.uk  
**Laura Illingworth** was formerly a Research Assistant at the Aberdeen Business School, Robert Gordon University, UK.

#### References

*   Bates, T. (2005). Analysis of young, small firms that have closed: delineating successful from unsuccessful closures. _Journal of Business Venturing_, **20**(3), 343-358.
*   Bouthillier, F. (2003). _[Access to information for small business managers: examination of some strategies and values.](http://www.webcitation.org/669e7PGVN)_ Paper presented at the annual conference of the Canadian Association for Information Science, Halifax, Nova Scotia, 30 May-1 June. Retrieved 14 March, 2012 from http://www.cais-acsi.ca/proceedings/2003/bouthillier_2003.pdf (Archived by WebCite® at http://www.webcitation.org/669e7PGVN)
*   Burns, P. (2007). _Entrepreneurship and small business._ London: Palgrave and Macmillan.
*   Chittenden, F., Robertson, M. & Watkins, D. (1993). _Small firms: recession and recovery._ London: Paul Chapman Publishing.
*   Chiware, E.R.T. (2007). Designing and implementing business information services in the SMME sector in a developing country: the case for Namibia. _IFLA Journal_, **33**(2), 136-44.
*   Cochran, A.B. (1981). Small business mortality rates: a review of the literature. _Journal of Small Business Management_, **19**(4), 50-59.
*   Deakins, D. & Freel, M. (1998). Entrepreneurial learning and the growth process in SMEs. _The Learning Organization_, **5**(3), 144-155.
*   Denning, S. (2005). The role of narrative in organizations. In J.S. Brown, S. Denning, K. Groh & L. Prusack (Eds.), _Storytelling in organizations: why storytelling is transforming 21st century organizations and management_ (pp. 165-182). Oxford: Elsevier Butterworth-Heinemann.
*   Department for Business Innovation & Skills (2010). _[Backing small business.](http://www.webcitation.org/669fL7kkH)_ London: Department for Business Innovation & Skills. Retrieved 14 March, 2012 from http://www.bis.gov.uk/assets/biscore/enterprise/docs/b/10-1243-backing-small-business.pdf (Archived by WebCite® at http://www.webcitation.org/669fL7kkH)>
*   Everett, J. & Watson, J. (1998). Small business failure and external risk factors. _Small Business Economics_, **11**(4), 371-390.
*   Fredland, J.E. & Morris, C.E. (1976). A cross section analysis of small business failure. _American Journal of Small Business_, **1**(1), 7-18.
*   Guardian (2009, March 18). [Britain's recession will last the longest, IMF predicts.](http://www.webcitation.org/669forQKd) Retrieved 14 March, 2010 from http://www.guardian.co.uk/business/2009/mar/18/britains-recession-to-last-longest (Archived by WebCite® at http://www.webcitation.org/669forQKd).
*   Harada, N. (2007). Which firms exit and why? An analysis of small firm exits in Japan. _Small Business Economics_, **29**(4), 401-414.
*   Hill, J. & McGowan, P. (1999). Small business and enterprise development: questions about research methodology. _International Journal of Entrepreneurial Behaviour and Research_, **5**(1), 5-18.
*   Johansson, A.W. (2004). Narrating the entrepreneur. _International Small Business Journal_, **22**(3), 273-293.
*   Johnson, L. & Kuehn, R. (1987). The small business owner/manager's search for external information. _Journal of Small Business Management_, **25**(3), 53-60.
*   Longenecker, C.O., Simonetti, J.L. & Sharkey, T.W. (1999). Why organisations fail: the view from the front-line. _Management Decision_, **37**(6), 503-513.
*   Lybaert, N. (1998). The information use in a SME: its importance and some elements of influence. _Small Business Economics_, **10**(2), 171-191.
*   Marcella, R. & Illingworth, L. (2009). _Small business failure: the causes and consequences._ Paper presented at the 32nd Annual Conference of the Institute for Small Business and Entrepreneurship, Liverpool, UK, 4 November.
*   Marcella, R., Williams, D. & Tourish, N. (2009). Riding the rapids through recession. Aberdeen Business Journal, **5**, 16-23
*   Martin, N. (2005). The role of history and culture in developing bankruptcy and insolvency systems: the perils of legal transplantation. _Boston College International and Comparative Law Review_, **28**(1), 1-78.
*   NetMonkeys (2009). _[White paper: the importance of information management.](http://www.webcitation.org/669fZGRNI)_ Retrieved 14 March, 2012 from http://www.netmonkeys.co.uk/whitepapers/information-management.pdf (Archived by WebCite® at http://www.webcitation.org/669fZGRNI).
*   Pearce, J.A. & Michael, S.C. (2006). Strategies to prevent economic recessions from causing business failure. _Business Horizons_, **49**(3), 201-209.
*   Politis, D. & Gabrielsson, J. (2009). Entrepreneurs' attitudes towards failures: an experiential learning approach. _International Journal of Entrepreneurial Behaviour and Research_, **15**(4), 364-383.
*   Pratten, J.D. (2004). Examining the possible causes of business failure in British public houses. _International Journal of Contemporary Hospitality_, **16**(4), 246-252.
*   Rae, D. (2000). Understanding entrepreneurial learning: a question of how? _International Journal of Entrepreneurial Behaviour and Research_, **6**(3), 145-159.
*   Rowley, J.E. (2004). _Strategic management information systems and techniques._ Manchester and Oxford: National Computing Centre and Blackwell Publishers.
*   Said, K.E. & Hughey, K.J. (1977). Managerial problems of the small firm. _Journal of Small Business Management_, **15**(1), 37-42.
*   Steyaert, C. & Bouwen, R. (1997). Telling stories of entrepreneurship - towards a narrative contextual epistemology for entrepreneurial studies. In R. Donckels & A. Mietten (Eds.), _Entrepreneurship and SME research_ (pp. 47-62). Aldershot, UK: Ashgate.
*   Stokes, D. & Blackburn, R. (2002). Learning the hard way: the lessons of owner-managers who have closed their businesses. _Journal of Small Business and Enterprise Development_, **9**(1), 17-27
*   Ucbasaran, D., Westhead, P., Wright, M. & Flores, M. (2010). The nature of entrepreneurial experience, business failure and comparative optimism. _Journal of Business Venturing_, **25**(6), 541-555
*   Vaughan, L.Q. (1999). The contribution of information to business success: a LISREL model analysis of manufacturers in Shanghai. _Information Processing and Management_, **35**(2), 193-208.
*   Watson, J. & Everett, J.E. (1996). Do small businesses have high failure rates? Evidence from Australian retailers. _Journal of Small Business Management_, **34**(4), 45-62.
*   Wood, D. & Hutchinson, A. (2010). [The future of financial reporting.](http://issuu.com/therobertgordonuniversity/docs/rgu-aberdeen-business-journal-2010) _Aberdeen Business Journal_, (No. 5), 12-15\. Retrieved 20 August, 2012 from http://issuu.com/therobertgordonuniversity/docs/rgu-aberdeen-business-journal-2010