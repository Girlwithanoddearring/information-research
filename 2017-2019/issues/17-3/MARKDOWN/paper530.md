#### vol. 17 no. 3, September 2012

# Teacher trainees’ information acquisition in lesson planning

#### [Mikko Tanni](#author)  
School of Information Sciences, FIN-33014 University of Tampere, Finland

#### Abstract

> **Introduction.** Few studies have addressed teacher trainees’ information acquisition in the context of a task. The paper reports findings from an empirical study on teacher trainees’ use of information (seeking) channels and sources, and their modes of information acquisition in lesson planning.  
> **Method.** Semi-structured (retrospectively held) individual interviews and supporting classroom observations were conducted.  
> **Analysis.** Inductive thematic analysis was applied to the transcribed interviews.  
> **Findings.** The trainees used various information channels for a single lesson plan, mainly their personal collections and the Web. The information sources acquired comprised documentary and interpersonal sources, various media and contents. Their information acquisition encompasses a continuum of modes beginning with purposeful and goal-orientated, and ending up with accidental and passive modes of information acquisition.  
> **Conclusions.** Trainees’ information acquisition for lesson plans cannot be described only in terms of their present activities or goal-orientated information seeking behaviour. The use of personal collections of information sources, gathered in the past in anticipation of future lessons, constitutes an essential characteristic of their information acquisition. Trainees’ modes of information acquisition reflect continuity across work tasks, which calls for capturing across task perspectives in information seeking models.

## Introduction

Teachers’ information acquisition has received limited attention in information seeking and retrieval studies. Particularly lacking is research that scrutinises teachers’ tasks. A fundamental teacher’s task is lesson planning. Teachers plan lessons to transform a curriculum or a syllabus, institutional expectations and their educational conceptions into practical guidelines for the classroom ([John 1991](#joh91), [1994](#joh94), [2006](#joh06); [Jones and Smith 1997](#jon97)). Lesson planning is a core issue in teacher training because of the challenge it poses to beginning teachers.

The goal of the present study is to describe teacher trainees’ information (seeking) channels and sources and their ways of acquiring information in lesson planning. The task is used as a framework for analysing access to information sources. According to Byström and Hansen ([2005](#bys05)), and Vakkari ([2001](#vak01), [2003](#vak03)) the premises of task-based information seeking and retrieval studies are that tasks trigger (more or less) purposeful information seeking to the accomplishment of a goal, and that information seeking is dependent on and interesting only in connection to the primary goal of the task. A task is a sequence of actions focusing on a particular item of work involving a purpose, a practical goal (i.e., the result or outcome of the task), implementation methods and requirements. Task-based research typically takes a single task into focus, explaining changes in the task performer’s activities in terms of the stage of the task.

The paper reports selected findings from a research project scrutinising teacher trainees' (hereafter, simply, 'trainees') information behaviour in lesson planning. The project was carried out in co-operation with the Unit for Pedagogical Studies in Subject Teacher Education, at the University of Tampere, Finland, in the academic year of 2007–2008\. The pilot study ([Tanni _et al._ 2008](#tan08)) provided preliminary results concerning the teacher trainees’ use of information channels and sources, and other characteristics of their information seeking behaviour.

The paper is organised as follows. The first section lays down the conceptual framework: information channels, sources, information acquisition and the key characteristics of the task. Research questions are presented at the end of the section. The second section elaborates the case, data collection and analysis methods. The third and the fourth sections, respectively, present and discuss the findings. The final section presents the conclusions.

## Core concepts and related research

Information sources are physical entities in a variety of media providing data or signs, which may become information when perceived ([Ingwersen and Järvelin 2005](#ing05): 386). Information sources are therefore not synonymous with information contents. According to Byström ([1999](#bys99): 31–32), the information seeking channel is the intermediary, which guides the information seeker to the source. The information channels are enabled by but not synonymous with various technologies. Information channels, like sources, may be informal (i.e., interpersonal) or formal (i.e., documentary) and, like sources, exclude the information seeker. There is no absolute difference between sources and channels: particularly interpersonal information channels may turn into sources and vice versa. ([Byström 1999](#bys99): 31–32; [Leckie _et al._ 1996](#lec96).) Direct observation is a unique information channel, for it does not provide access to information sources as representations of authors’ interpretations of the world but sensory data only (see [Serola 2009](#ser09): 70; [Ingwersen and Järvelin 2005](#ing05): 47–54).

Byström and Hansen ([2005](#bys05): 1055) elaborate the distinction between the two concepts by arguing that ‘_[i]nformation channels are used to become aware of and locate appropriate information sources that contain, or are expected to contain, the actual information sought for_‘. They also conceptualise information seeking as a type of sub-task performed in support of the task proper, emphasising that information seeking is meaningful only in relation to the latter. However, information acquisition may and typically does involve referrential information that is relevant only to the performance of a sub-task and not directly to the task proper. It is therefore useful to discern between information channels and sources on the basis of task levels.

Sonnenwald ([1999](#son99)) has argued that human information behaviour takes place in a context of a variety of related situations, wherein an _information horizon_ of information resources is available: social networks (e.g., colleagues, experts, librarians and other information brokers), documents (e.g., books, Websites and broadcast media), observation in the world and information retrieval tools. Leckie _et al._ ([1996](#lec96)) have argued similarly that professionals’ _awareness of information_ and their perceptions about the process of information seeking determine their choice of information channels and sources. Many empirical studies have concluded that professionals prefer to choose information channels and consult sources they are familiar with ([Byström and Hansen 2005](#bys05); [Ellis and Haugan 1997](#ell97); [Leckie _et al._ 1996](#lec96)).

McKenzie ([2003](#mck03)) has presented an empirically derived two-dimensional model capturing a whole spectrum of _information practices_ beyond the purposeful activity invoked by the term information seeking. The first dimension discerns between four modes of information practices: _active seeking_ for a specific goal, _active scanning_ of a subject without a specific concern, _non-directed monitoring_ with no intent other than to become generally informed, and _by proxy_ when informed by others on their initiative. The second dimension discerns between the two stages of _making connections_ and _interacting with sources_; for example, between seeking contact with or being contacted by an information source and asking questions from or being told by an information source.

Ellis and Haugan ([1997](#ell97)) have presented an empirically derived model based on behavioural _patterns_ in information seeking. _Surveying_ is an aim for an initial overview of the topic, often beginning with an established _starting point_. Surveying known people or documents from earlier projects is called _consulting_. Following referential links between information sources by starting from known sources is called _chaining_. A sub-aspect of chaining is _verifying_ specific pieces of knowledge. _Monitoring_ is an aim to maintain awareness of developments in a field by regularly following particular known information sources. Both surveying and monitoring can manifest themselves as behaviour such as _browsing_. ([Ellis and Haugan 1997](#ell97).) The patterns of surveying and monitoring are open for reading as a goal (to gain an overview and to stay up to date) and browsing and chaining, for example, as the behavioural means to achieve the ends.

Kuhlthau ([2004](#kuh04)) has presented an influential _information search process_ model explaining changes in information seeking behaviour in terms of task-performers’ mental states. Vakkari ([2001](#vak01): 58) has elaborated the model and confirmed that information seeking depends ‘systematically on the stage of the task performance process and the mental model of the searcher’. Task-based research, using Kuhlthau’s model as the framework, takes a single present task performance process and an individual’s cognition as the points of departure.

Challenging models based on Kuhlthau's, the literature attests for variation in goal-orientation, problem-specificity or direction in information acquisition. It is possible to discover information even when not actively seeking information. Serendipitous or accidental forms of information discovery can be placed on a continuum where they oppose the most purposeful, goal-orientated or -directed modes of information acquisition ([Erdelez 1997](#erd97); [Foster and Ford 2003](#fos03); [McKenzie 2003](#mck03)).

In the present paper, the term information _acquisition_ is preferred, for it does not imply activity in the way the term _seeking_ does (see [Erdelez 1997](#mck03); [McKenzie 2003](#mck03)). The term _mode_ is used of the way or the manner in which information is acquired. The _modes of information acquisition_ are abstractions of information behaviour. They should not be understood as conscious information seeking strategies, but more of features or characteristics that such strategies might have. The modes are not about patterns either, because patterns imply consecutive steps of action, which are not implied in the present paper.

Some modes of information acquisition, accidental information encountering and monitoring particularly, presume a conception of a _time horizon_ extending beyond present activities: past information needs coming to mind and information perceived as being valuable for anticipated uses. Social contexts may affect the ways in which individuals perceive time horizons encouraging the adoption of either narrower or broader views. The anticipation of future tasks is a reason and a requirement for keeping personal collections of information sources at hand. A degree of contextual and situational continuity is required to be able to anticipate future needs for information and exploit the personal collection. ([Bruce 2005](#bru05); [Byström and Hansen 2005](#bys05); [Erdelez 1997](#erd97); [Foster and Ford 2003](#fos03); [Leckie _et al._ 1996](#lec96); [Savolainen 2006](#sav06); see [Sonnenwald 1999](#son99) for more about context and situation.)

In teacher training lesson planning is both a learning task and an authentic work task at a real school (see [Kim and Roth 2011](#kim11)). In a typical training scenario, the mentoring teacher in the training school assigns a topic, but leaves the teacher trainee with a degree of freedom, within curricular restrictions, to carry out the task. Teacher training institutions and school-based mentors might offer instructional templates for teacher trainees, laying out the structural elements of the task in various levels of detail and rigidity. Lessons are planned consecutively and in parallel over the training period. Both short and long term planning takes place ([Jones and Smith 1997](#jon97); [Perrault 2007](#per07)). Trainees consider a complexity of factors in lesson planning: the curriculum, subject content, learning assignments and activities, classroom control, pupils’ and students’ age, abilities and learning, and suitable resources ([John 1991](#joh91), [1994](#joh94), [2006](#joh06); [Jones and Smith 1997](#jon97)). As novices, trainees need contextual information about pupils’ and students’ abilities, individual needs and classroom behaviour, which is typically available only over interpersonal information channels (see [John 1991](#joh91), [1994](#joh94); [Kim and Roth 2011](#kim11)). With experience, trainees learn means to cope with information overload and seek information efficiently for lessons plans ([Tanni _et al._ 2008](#tan08)). The context and the situation therefore add predictability and stability to trainees' perspective on the task.

Few studies have addressed trainees' information acquisition in lesson planning. Summers _et al._ ([1983](#sum83)) surveyed teachers’ use of information sources in general. Small _et al._ ([1998](#sma98)) surveyed teachers’ use of information sources in instructional planning, which covers both daily lesson planning and long-term unit planning. Perrault ([2007](#per07)) surveyed and interviewed teachers about their use of _online resources_ (i.e., Internet-based services) in instructional planning. Merchant and Hepworth ([2002](#mer02)) interviewed teachers about their use of information sources in lesson planning. The studies give a general picture of the variety of information channels and sources used by teachers in general or in instructional planning. The studies do not discern between information channels and sources, preferring the latter term, or between referential information and actual information used in the task proper. It is also noteworthy that the surveys questioned teachers on their use of given (i.e., pre-defined) information channels and sources, perhaps missing some options.

The literature gives some ideas on the ways teachers acquire information for lesson plans. Small _et al._ ([1998](#sma98)) suggest that teachers, by using a variety of information sources in instructional planning, pick _nuggets_ of information from several sources over evolving searches. Merchant and Hepworth ([2002](#mer02)) and Perrault ([2007](#per07)) point out that teachers seek information for a variety of purposes, which suggests variation in the ways information is sought. The pilot study ([Tanni _et al._ 2008](#tan08)) demonstrated that teacher trainees, facing the topical breadth of history and social sciences curricula, expressed substantial variation in the specificity of search goals, depending on how much they knew about the topic and useful information sources in advance. The trainees explained between returning to information sources on the basis of past experiences and exploring broad topics while still learning about them.

The paper will address the issues brought up in the literature review, mainly the trainees' information horizons or awareness of information and their ways of acquiring information in the context of lesson planning. The following research questions are addressed in the present study:

1.  What information channels and sources do trainees use in lesson planning?
2.  What are trainees' modes of information acquisition in lesson planning?

Both of these questions are addressed by using the task-based viewpoint (a) in the analysis of information channels and sources and (b) to characterise lesson planning as a task with the trainees' modes of information acquisition.

## Data, sampling and analysis methods

A total of twenty-three subject teacher trainees, seventeen women and six men, most in their late twenties or early thirties, were recruited in the academic year of 2007–2008, with the help of two teacher educators. The participants had majored – or were in the process of majoring – in history, social sciences, psychology and philosophy.

In Finland, subject teachers teach in basic education (grades 7–9), general upper secondary education, in liberal adult education institutions or as teachers of core subjects in vocational institutions. In the teacher’s pedagogical studies each trainee plans and delivers approximately thirty training lessons under the supervision of a mentoring teacher (hereafter, simply 'mentor') at a school. The mentor and the trainee meet and agree on a general topic. The mentor typically does not give strict teaching assignments with explicit requirements for the training lessons. Instead, s/he assigns a topic leaving the trainees the freedom to plan the lessons according to their own discretion: outline a schedule for the lesson, select appropriate teaching methods, acquire information resources, and plan exercises (if any), but not necessarily in this order. Teacher educators at the university give guidance and practical instructions. The mentors have a variety of practices of their own, but they do often request a schedule of the lesson in advance, observe the training lessons on the spot and give feedback.

Individual interviews were the primary data collection method. Many respondents were unwilling to be interviewed or observed at their homes or just preferred to be interviewed at their training schools, which ruled out interviews and direct observations of information behaviour during lesson planning activities. The retrospective interviews posed limits on the kind of data that could be collected on lesson planning as a process. A semi-structured interview guide (see [Appendix](#app)) was used, but the questions were rephrased if clarification was needed. The researcher attended a single training lesson for each trainee and observed what information resources the trainee presented to the class. After the lesson, the researcher interviewed the trainee concerning the lesson. The observations were exploited to particularise interview questions, to provide common ground for discussion and to ensure that all information resources used in the class were dealt with. If the parties had not been able to agree on the schedule, the interviewee was prompted to choose a lesson in his or her best memory for the interview; practically the latest lesson. About two-thirds of the lessons were observed. The first interview was held in December 2007 and the last in April 2008\. The interviews were recorded with a laptop computer and resulted in approximately seven-and-a-half hours of audio data. The median length of a single interview was approximately seventeen minutes and the average twenty minutes. The recordings were transcribed by a professional service.

The transcripts in Finnish were analysed thematically, using the HyperResearch software package. Thematic analysis is a process of encoding (i.e., classifying or categorising) qualitative information with explicit codes. A theme, at minimum, is an observation and a description of a pattern on the manifest level of data and, at maximum, an interpretation and an explanation of the pattern on the latent level ([Boyatzis 1998](#boy98): 4, 16–17). A code captures the essence of thematic observations as a label and a description, thereby providing a link between data and the researcher’s ideas about the data ([Boyatzis 1998](#boy98): 11, 31–32, 48–49). Each code covers ‘the most basic segment, or element, of the raw data or information that can be assessed in a meaningful way regarding the phenomenon’ ([Boyatzis 1998](#boy98): 63–65). In the present study, codes were assigned to units of data between a few words to a few sentences. The unit of analysis (or the case in HyperResearch), the entity being analysed ([Boyatzis 1998](#boy98): 62–63) and encoded, was the individual teacher trainee, represented by the corresponding interview transcript.

The code development followed the hybrid approach described by Boyatzis ([1998](#boy98): 51–53): previous research was consulted for ideas, but codes were developed inductively from the data instead of being derived from a theory or adopted from a previous study. The following ideas proved essential to make sense of the data. The task-based view helped to discern between referential information and information directly applicable to the task. Therefore reading lists and intermediaries making recommendations for literature, for example, were not considered as information channels or sources in the findings. It also proved very helpful to follow McKenzie ([2003](#mck03)) and discern between the stages of (1) seeking or making connections with, or being contacted by, information sources directly or by referral, and (2) acting, or being acted on, in established connections with information sources. The notion proved essential to analyse situations where getting access to information sources was so trivial that the respondents did not elaborate on it: these included, for example, accounts on monitoring a daily newspaper at home or consulting the mentoring teacher at the school.

The principle of constant comparisons (see [Lincoln and Guba 1985](#lin85): 339–344) was followed through the analysis. The data was read through with the research questions in mind and an initial set of themes was produced based on perceived (dis)similarities in the data. The data was read through again case-by-case and coded for the presence of the emerging themes. The themes were then retrieved code-by-code and checked for consistency within each theme and for differences across the themes. Reading within a theme emphasised the differences in the theme; reading across the themes emphasised the similarities in each theme. The codes were written to ensure the maximum of differentiation in the data: a pattern of data was coded only as one of the several related themes. The codes were refined in further constant comparisons between the thematic units of data and corresponding code descriptions. The descriptions were reformulated and themes were split or new ones added. The codes, thus, gradually tested against the data, became more discriminating and consistent. The process was concluded when the properties of the revised codes were crystallised and stabilised. Anomalies are discussed in the next section.

## Findings

The findings are presented as tables and elaborated in the text. The scores represent the number of respondents (i.e., units of analysis) with the corresponding theme coded present at least once. Therefore, the maximum number of respondents (i.e., n) associated with the theme is twenty-three. The codes were considered either being present or absent in the unit of analysis, and the frequencies of occurrences were not considered. The individual respondents are identified in the text as the letter C (as in case) and a pseudo-random number when quoted.

### Trainees' use of information channels and information sources

Table 1 summarises the information channels and the associated sources used by the trainees. The information channels are organised by the number of associated respondents and discussed accordingly. The information sources are discussed along with the first channel they were associated. Information sources accessed over the task were coded according to the information channel used at the time and not according to the channel used originally. Websites, online library catalogues or people acting as intermediaries offering referential information, were not considered as information sources under the criterion that an information source should directly support the lesson planning task.

<table class="center"><caption>  
Table 1: Information channels and sources</caption>

<tbody>

<tr>

<th style="width:25%;">Information channels</th>

<th>n</th>

<th>%</th>

<th>Information sources</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>Personal collection</td>

<td style="text-align:center">21</td>

<td style="text-align:center">91</td>

<td>Textbooks</td>

<td style="text-align:center">15</td>

<td style="text-align:center">65</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Books (non-fiction)</td>

<td style="text-align:center">10</td>

<td style="text-align:center">43</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Personal files</td>

<td style="text-align:center">7</td>

<td style="text-align:center">30</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Periodicals</td>

<td style="text-align:center">3</td>

<td style="text-align:center">13</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Educational materials</td>

<td style="text-align:center">3</td>

<td style="text-align:center">13</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Notes</td>

<td style="text-align:center">3</td>

<td style="text-align:center">13</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Personal photographs</td>

<td style="text-align:center">2</td>

<td style="text-align:center">9</td>

</tr>

<tr>

<td>Web</td>

<td style="text-align:center">21</td>

<td style="text-align:center">91</td>

<td>Websites</td>

<td style="text-align:center">15</td>

<td style="text-align:center">65</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Unspecified Web</td>

<td style="text-align:center">13</td>

<td style="text-align:center">57</td>

</tr>

<tr>

<td>Interpersonal resources</td>

<td style="text-align:center">10</td>

<td style="text-align:center">43</td>

<td>Mentoring teacher</td>

<td style="text-align:center">6</td>

<td style="text-align:center">26</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>People</td>

<td style="text-align:center">3</td>

<td style="text-align:center">13</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Textbooks</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Books (non-fiction)</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Videos (recording)</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td>Libraries</td>

<td style="text-align:center">10</td>

<td style="text-align:center">43</td>

<td>Books (non-fiction)</td>

<td style="text-align:center">10</td>

<td style="text-align:center">43</td>

</tr>

<tr>

<td>Observation</td>

<td></td>

<td></td>

<td></td>

<td style="text-align:center">6</td>

<td style="text-align:center">26</td>

</tr>

<tr>

<td>Exhibitions</td>

<td style="text-align:center">2</td>

<td style="text-align:center">9</td>

<td>Unknown</td>

<td style="text-align:center">2</td>

<td style="text-align:center">9</td>

</tr>

<tr>

<td>Unknown</td>

<td style="text-align:center">2</td>

<td style="text-align:center">9</td>

<td>Books (non-fiction)</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td>Classic novels</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

</tr>

</tbody>

</table>

**Personal collection** is an information channel providing access to information sources employed by the trainees when engaging in the task. The personal collection retains information sources stored (e.g., saved, bought or loaned) earlier. Unconditional access to the information sources is a characteristic: a familiar book in the library or a Website might become inaccessible for reasons beyond the trainee’s personal influence. The personal collection covers both the printed information objects such as books and the digital documents saved on the trainees' personal computers. The personal collection provided access to various different types of information sources.

**Textbooks** are those deployed by the school, textbooks from alternative book series, and the trainees' old textbooks from high school. In other words, books written for the purpose of teaching pupils of a particular grade. **Books** comprise nonfictional and academic monographs but excludes textbooks and fiction, which came up in the data rarely. Non-fiction comprised, for example, general works on history of philosophy, philosophical dictionaries, theological literature, art books, handbooks, geographical guidebooks, encyclopaedias, professional literature (on social work) and university textbooks. **Classic novels** comprise fictional literature used by a single teacher trainee as an information source. The information channel used could not be identified.

**Personal files** cover a kind of scrapbook of information items acquired from various information sources and stored in physical folders or on personal computers. The analytic principle is that an information item such as a comic strip can be viewed as the information content sought for or, as a cutting, the medium carrying that information. The origin of the items stored in the personal files was often disregarded in the trainees' accounts. When accessed from the personal collection the information items become information sources regardless from where they were originally acquired. The information items stored in personal files included a set of PowerPoint slides, a report on an unspecified study, an unspecified article, a comic strip, a newspaper cutting, and a wholly unelaborated set of _materials_. One teacher trainee said that she stored her children’s school art and exams in folders. While it would be possible to consider the children as an interpersonal resource (i.e., an information channel) enabling access to the schoolwork (an information source), the art and the exams were accessed from the personal collection during lesson planning.

**Periodicals** include (printed) newspapers and magazines but exclude academic journals, which the trainees did not use. The theme captures the accounts where periodicals were accessed specifically for use with the present lesson. The cuttings taken earlier from newspapers and magazines were considered personal files. Noteworthy, the periodicals were accessed from the personal collections and not, for example, from libraries. **Educational materials** comprise (printed) teacher’s guides typically supplied with textbooks and other instructional material. The guides offer pedagogic instruction and guidelines on how to plan teaching. **Notes** consist of the trainees' own notes from lectures at the university and associated handouts typically distributed at the same lectures. **Personal photographs** are photographs taken by the trainees. The theme does not cover photographs as pictorial information sought from and carried by (other) information sources, such as textbooks and Websites; these would have been considered as personal files if stored in personal collections.

**Web** is an information channel providing access to information sources available on the _World Wide Web_. The theme is not concerned with full-text or reference databases available through the Web, because the contents of such databases are not generally accessible or the items are not immediately available over the Web. **Websites** cover specific and usually named interrelated collections of documents on the Web. These include organisational Websites and larger entities such as Wikipedia, the most often named Website. **Unspecified Web** covers the trainees' accounts where they spoke of the whole Web as an information source, calling it _Net_, _Google_ or even _computer_, thereby leaving individual information sources on the Web unspecified and unnamed. Some trainees said that they could not remember which Websites they had accessed. Others cited search engines (i.e., _Google_) as information sources, in their words. Particularly images sought through _Google Images_ were spoken of separately from the information sources they were originally published in.

**Interpersonal resources** comprise the trainees' mediated or immediate, synchronous or asynchronous contacts with other people as (informal) information sources or as information channels to (formal) information sources. The people acting as interpersonal resources often also had the roles of intermediaries, referring to information sources available through other information channels but were not considered as information channels then. The **Mentoring teacher** (or mentor) assigned by the school to guide and tutor trainees was the person consulted most often as an information source. The **People** consulted as information sources included the method teacher at the university, a co-worker from a previous workplace, teacher colleagues, a researcher (at the university) and one teacher trainee’s spouse. Fellow trainees observed in their training classes were not coded as people in this theme (_cf._ below). The **Video** refers to a documentary on a medium (either a VHS tape or a DVD disc) handed by the mentoring teacher to a teacher trainee.

**Libraries** encompass school, public and university libraries as accessed on the spot. The online library catalogues available through Web interfaces were the only type of database accessed by the teacher trainees. However, checking up online catalogues for references caught from Websites or hints received about relevant items, as useful as such referential information might have been to the teacher trainees, did not establish access to the items in the library. Such referential information was not directly useful in the performance of the task proper. The trainees had to physically visit the libraries anyway to hunt down the items, making the library the information channel.

**Observation** constituted an information channel where the trainees attended fellow trainees', and in one case a mentoring teacher’s, lessons. Observation can be thought of as an information channel, enabling access to lessons as a kind of information source for information about teachers’ didactic practices and students’ behaviour. However, the teachers were directly observed in teaching and not communicated with. Consequently, the information acquired from the lessons was of the trainees' own making and did not involve representations, people or documents. The lessons – and the teachers and the pupils involved – were objects being observed and not information sources in the proper sense of the concept. It is of course possible that the trainees wrote down memos of their observations and later accessed them as information sources from their personal collections, but the data did not indicate that.

**Exhibitions** comprise public displays such as fairs and museum centres offering printed supplementary materials free of charge. As a type of information channel, exhibitions required a physical visit on the spot whilst the lesson was planned. (It is reasonable to assume that the materials gathered from exhibitions are typically stored in personal collections for future use.) The types of information sources acquired from exhibitions remained unknown. **Unknown** covers the data associated with the visits but lacking the detail to identify the type of information sources in question. The researcher supposes that these materials were brochures.

### trainees' modes of information acquisition

Table 2 summarises six modes of the trainees' information acquisition. The modes are organised to illustrate a continuum beginning with the most goal-oriented and ending up with the most serendipitous mode of information acquisition.

<table class="center" style="width:50%;"><caption>  
Table 2: Modes of information acquisition</caption>

<tbody>

<tr>

<th>Mode</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>Seeking known-items</td>

<td style="text-align:center">18</td>

<td style="text-align:center">78</td>

</tr>

<tr>

<td>Seeking with direction</td>

<td style="text-align:center">10</td>

<td style="text-align:center">43</td>

</tr>

<tr>

<td>Surveying the topic</td>

<td style="text-align:center">18</td>

<td style="text-align:center">78</td>

</tr>

<tr>

<td>Monitoring</td>

<td style="text-align:center">5</td>

<td style="text-align:center">22</td>

</tr>

<tr>

<td>Accidental encounters</td>

<td style="text-align:center">3</td>

<td style="text-align:center">13</td>

</tr>

<tr>

<td>Being given information</td>

<td style="text-align:center">5</td>

<td style="text-align:center">22</td>

</tr>

</tbody>

</table>

**Seeking known-items** occurred when the trainees returned to familiar information sources at known locations for information contents or items they were familiar with. Many trainees described seeking access by recall explicitly. One trainee articulated the core sense of the theme aptly:

> I had originally found [the Website] for a completely different purpose... and I familiarised myself with [the Website] quite carefully at that time. Then, when planning this lesson, it _occurred_ to me, that I could use [the Website]. (C5)

A selection of quotations elaborate variation within the theme:

> I wanted [a photograph] of the old Parliament [building], and I _remembered_ that it’s in the book. (C15)  
>   
> I’ve had that [Calvin and Hobbes strip] for a very long time, and I _recalled_ it when I started [planning the lesson]. (C22)  
>   
> I’d been to [a museum] then at that time and I _recalled_... that [a display item] is associated with [the topic of the present lesson], and I _remembered_ taking photos of it, and I got the photos from my blog.’ (C19)  
>   
> [I used] lecture notes and handouts from courses, if I could _remember_ that I’ve been to a course [on the topic]. (C1)  
>   
> I originally had [a book] as required reading on a course... at my freshman year and I’ve often _returned_ to the book that way. (C8)

The accounts show how the trainees recalled both information sources and items in information sources they had accessed in the past, before the task initiation. The trainee referring to his blog had recently taken a field trip with his class.

Some trainees expressed being familiar with relevant literature in general. Consider the following examples:

> The lessons I have taught are topically quite **familiar** to me, and I already have quite clear conceptions from which books I’ll go to get the information. (C11)  
>   
> In fact, I [already] _knew_ quite a few of those books. (C2)  
>   
> [I was] thinking of the professional literature, which I was _familiar_ with, that I’ve used. (C7)

Some trainees' accounts on access by recall were implicit. For example, a few explained that they had recently planned lessons on similar topics and therefore already held relevant information sources. Others referred to their own course books from the university, their own textbooks from high school and training materials from courses they had been on. The theme was not coded as present if the trainees cited accessing known information sources such as their mentors to request references to unknown information sources (_cf._ below).

**Seeking with direction** occurred when the trainees did have clear ideas of what they were seeking (_cf._ below) but could not access relevant information sources or items in information sources by recall (_cf._ above). The trainees often expressed specificity by describing search goals of a factual nature. The following quotations are illustrative:

> I went to the computer to search **when** the fortification work finally ended. (C14)  
>   
> There’s a filosofia.fi-portal, which I visited pertaining to **Kant’s Theory of Knowledge.** (C1)  
>   
> I went to the Web to see how the **names** of certain [political] parties are translated into English for they are certainly not in dictionaries. (C23)

The trainees also cited seeking specific information (content) items. One elaborated her thinking aptly:

> I [thought], there must be **a picture of the** [floor of the] **old parliament** [building] as there’s one of the current... I was thinking, that these pictures must exist... I thought, that the parliament has been photographed through decades. (C23)

Other items sought by the trainees and articulated as specific search goals include pictures of paintings, a historical document, photographs of President Kekkonen and selected topical politicians.

The trainees also conducted directed searches to access specific information sources. The following quotations exemplify the aspect:

> I **checked** [from the library OPAC] **what books they have there** on the spot. (C22)  
>   
> I had to go to the library and choose **the latest [of an author]**. (C14)

The former quotation may also be conceived as verifying and the latter as an example of chaining by the author’s name. Yet another quote illustrates directed browsing from a selected starting point:

> I **browsed through** last year’s matriculation examinations for questions concerning [the topic of the lesson]. As the year 2006 was the 100th anniversary year of Parliament of Finland, I thought I would quite likely **find a question on this topic** and I did. (C23)

Some trainees' accounts referred to meetings with their mentoring teachers. The following example raises an interesting point:

> I **asked** the [mentoring] teacher **if I could photocopy these diagrams**, and she thought that, "if you copy a page or two, it won’t matter". (C8)

The trainee described asking the mentor a direct question about a copyright issue concerning a diagram that she had found from a book. The trainee was obviously familiar with the mentoring teacher as an information source. The direct nature of the question asked, however, suggests that the trainee was not verifying something she had known.

**Surveying the topic** occurred when the trainees conducted sweeping searches without expressing direction towards specific goals or pieces of information. In a broad sense, the trainees described topical surveying as ‘_Googling_’: entering general topical search-keys in _Google_, browsing search-results and following links forward. The following quotations illustrate the idea:

> One usually _**Googles**_ and only then searches, I mean, **looks at what comes out of it**, and then follows the links forward. (C15)  
> **It doesn’t matter, what the words you search with are**... the information I’m searching for, it comes, kind of, from somewhere, that actual [information]. (C14)  
> Quite often... I’m **skimming through** information and I’m usually on the Web quite a lot, kind of delving there, and therein really many wrong tracks may come, but sometimes the wrong tracks are quite worthwhile, you might find somewhere into those deep Web search engines or Websites about [a particular topic]. (C17)

A trainee discussed a starting point of a broad topical survey:

> **First read the [textbook] chapter** [on] that topic. Look for these key words... and gather them up, and then, from that basis, begin to develop [the search] forward, for example, by seeking information on the Internet, _Wikipedia_, _Google_. And then from [the Finnish history network] and others, where are these original documents... And if you can’t find information on the Web, then it’s time for a visit to the library. Then you’ll search books with the key words. (C19)

Topical surveying was not limited to the Web. A teacher trainee, for example, conducted a survey in the library OPAC. Some trainees described surveying within their personal collections or specific domains:

> I have shelves full of [academic literature on the topic] at home and **I try to dig out** the in depth material from there if it’s not on the Web. (C21)  
>   
> We **went through a reading list** with [the mentoring teacher]... there’s quite a number of books on it, which one can look through, and that was it. (C2)

The former quotation illustrates that the literature in the personal collection, while limited in scope, is not necessarily accessible by recall. Some trainees' descriptions of their negotiations with their mentoring teachers took the form of topical surveys:

> We first met the mentoring teacher, who gave some _tips on how [the topic] could be approached_. We, then, asked questions, if we could do this or that, and the teacher then gave her own comments... what are issues that should be more closely dealt with. (C8)  
>   
> At first I chatted with my mentoring teacher a little about **what [the lesson] could cover**. (C2)

Here, the trainees, rather than directly inquiring about a specific issue (cf. above), consulted the mentor concerning the overall topic of the lesson.

**Monitoring** occurred (or had occurred) when the trainees kept watch (or had kept watch) on certain information sources or places for information to use in the present (or a future) lesson plan. A teacher trainee described the mode as follows:

> ‘**We were watching** all along what’s for example in newspapers, what’s in there, utilised and... gathered, and **we both had materials already from newspapers** before this. (C7)

The quotation also illustrates how fluidly the trainees moved over the time dimension in their accounts by referring to their present and past monitoring activities. The trainees alluded to monitoring for prospective lesson plans:

> Nowadays, I read newspapers with scissors in my hands as **I’m collecting all kinds of articles**, it’s surely what’s so important in our societal subjects, snatch everything you can. (C8)  
>   
> There **at the Educa-fair**, if there was anything relating to the things I was teaching, **I gathered all** of them for I thought that I might need them some day. (C8)

The latter quotation also describes monitoring a place or a context for information sources rather than a source for information. Monitoring was the only mode of information acquisition that the trainees explicitly associated with gathering information to their personal collections to use it later.

**Accidental encounters** occurred when the trainees serendipitously found (1) information sources in situations where they were not seeking contact with information sources or (2) valuable information in information sources already at their hands. (The passive form in the theme’s name is intentional.) Accidental encounters are often subject to the other modes of information acquisition, serving other ongoing tasks. This quotation illuminates both aspects:

> [A dissertation] _accidentally caught my eye_ in the library, as I was there digging in books for my masters thesis, and I then browsed it, and the diagram **happened to be there**. (C8)

Another trainee (C9) explained how she was on a Website of the TV show <cite>Big Brother</cite>, hosted by a tabloid, when an unrelated item in the news feed ‘_caught her eye’_, and she picked up the news article for use in the lesson. It is noteworthy how a leisure time activity turned into a work related outcome. A quotation illustrates an accidental encounter when surveying another topic:

> I was there [in the library] hanging around in front of the shelves, and, as this was a handbook of world history... I started browsing it and spotted [the picture]. I was **seeking information on [the topic]** at the same time but I was looking at **other history books there as well... and it caught my eye**. It was just **a lucky accident** that I found the picture. (C22)

Thus, the trainee encountered information in a situation where the encounter was likely but not expected.

A quotation illustrates how the temporal dimension raised confusion about how to code the data:

> [I] **encountered the diagram accidentally** in [a book], it was **a book given as a tip** to us by the mentoring teacher... It’s a seminal work, and [the diagram] was there in that book. I actually **bought the book for myself**. (C8)

The teacher trainee stated that an accidental encounter occurred. On the other hand, the account explicitly reads that the book had been bought from recommendation, meaning that the teacher trainee had been given information about a useful information source (cf. below). However, the teacher trainee had been referred to the book and not to the diagram, and the recommendation must have occurred in the past and not in the confines of the present task. Further, as the teacher trainee expressed owning the book, the occurrence could have been coded as seeking known-items (cf. above). However, the expression used suggests that the diagram was not purposefully sought by recall even though the book containing it was accessed from the personal collection.

The theme was not coded present superficially on any expression suggesting accidental encounters with information. While a trainee (C22), in a casual manner, used the expression ‘by accident’ concerning a book found in the library, the data showed that she had been purposefully surveying books on the topic from a library OPAC.

**Being given information** occurred when the trainees, without actively asking for help, were informed by information sources or directed to information sources by intermediaries (practically, mentors) working on their behalf. The following quotations elaborate three aspects of passive information acquisition:

> I **got a tip** from the mentoring teacher, that [after plays] write on [the blackboard] a lot. (C4)  
>   
> That mentoring teacher, as she knew that I had to re-plan my lesson, she said that, as it is always so laborious to plan lessons, **she has a video**... "if you want to show it". (C22)  
>   
> I had books that were actually **recommended** by the mentoring teacher. (C6)

The trainees were, thus, being (1) informed about (i.e., told), (2) handed information sources or (3) referred to information sources. As above, the trainees probably were already in contact with their mentors, and it is likely that they did not receive information completely serendipitously.

### Summary of the findings

The first research question was concerned with trainees' use of information channels and sources in lesson planning. The trainees used a repertoire of six information channels for the single lesson plan: the personal collection, the Web, interpersonal resources, libraries, observation and exhibitions. The information sources used for the lesson plan were of a broad scope as well, comprising both documentary and interpersonal sources, various media and content: general non-fiction, personal and educational.

The second research question was concerned with trainees' modes of information acquisition. The findings were conceptualised as a continuum beginning with the most goal-oriented and ending up with the most serendipitous mode. The trainees' modes of information acquisition were _Seeking known-items_, _Seeking with direction_, _Surveying the topic_, _Monitoring_, _Accidental encounters_ and _Being given information_.

## Discussion

The findings corroborate the pilot study ([Tanni _et al._ 2008](#tan08)) concerning trainees' use of information channels and sources, refining the role of the personal collection particularly. A short glance at the earlier studies gives an idea how the present findings relate. The earlier studies have identified (typically implicitly) the following information channels: (a) meetings, including workshops, courses, seminars and conventions; (b) conversations (i.e., interpersonal resources); (c) libraries, including school, district, university and public libraries; (d) personal collections; (e) radio and television; (f) computer retrieval or databases; (g) digital libraries; (h) the Web, (i) bulletin boards and mailing lists ([Summers _et al._ 1983](#sum83); [Small _et al._ 1998](#sma98); [Merchant and Hepworth 2002](#mer02); [Perrault 2007](#per07)). The information sources identified in the earlier studies are: (a) personal notes and files; (b) educational journals, newsletters and magazines; (c) curriculum materials; (d) people, including colleagues, experts and librarians; (e) textbooks, including college and university level textbooks; (f) books; (g) research reports and theses; (h) Websites; and (i) videos ([Summers _et al._ 1983](#sum83); [Small _et al._ 1998](#sma98); [Merchant and Hepworth 2002](#mer02); [Perrault 2007](#per07)). Summers _et al._ ([1983](#sum83)) considered abstracts, indexes and bibliographies as information sources as well. In the present study, these were considered as search aids or tools, which do not contribute to the task proper.

Some of the information channels are characteristic and others incidental to lesson planning as a teacher’s work task. The use of personal collections and interpersonal resources is typical to professional work ([Leckie _et al._ 1996](#lec96)). The personal collection performed a major role in the trainees' information acquisition by providing access to information sources of which some had been acquired in the past through external information channels while others were personal by origin. The use of personal collections suggests that trainees are planning for the long term at the end of their training year. The use of interpersonal resources such as colleagues is typical to teachers (see [Kim and Roth 2011](#kim11)), and in the case of trainees these include the mentoring teacher as well. Observation on the other hand has not been identified as a teacher’s information channel in previous information seeking and retrieval studies. It is characteristic to teacher training, for trainees are required to comment other student teachers’ teaching. The information channel provides access to contextual information unavailable from documentary information sources. Visits to exhibitions are a rather incidental teacher’s information channel in lesson planning, its availability being dependent on the timing of field trips or teachers’ own leisure time interests. Both observation and visits do not seem to be purposively carried out for individual lesson plans, although the information acquired thereof may influence any ongoing lesson planning processes. The trainees' allusions to these two information channels suggest that different types of tasks can emerge simultaneously from teachers’ work roles (see [Leckie _et al._ 1996](#lec96)) and trigger information acquisition over individual work tasks.

Research using the Leckie _et al._ ([1996](#lec96)) model is prolific, but it typically does not address professionals in the role of educator, to which the present findings relate. An example of related research is the Borgman _et al._ ([2005](#bor05)) study on a faculty of geography seeking information in the roles of researchers and teachers. The geographers were engaged in both active and passive modes of information acquisition, particularly in continual scanning (cf. _Monitoring_) of their information environment, in both roles. The roles were mutually reinforcing, leading to serendipitous encounters with relevant information (sources). ([Borgman _et al._ 2005](#bor05).) The present findings also suggest that some trainees, too, were not only thinking between various lesson plans in their information acquisition, but also switching between the roles of the practicing teacher and the student teacher, still working on their Masters' theses.

The trainees did not explicitly cite interpersonal resources, observations or exhibitions as information channels (see [Appendix](#app), question 5c), which their responses across the data nevertheless indicated. The findings may thus correspond with, but are not synonymous with, the conceptions the trainees may have regarding the channels. The number of trainees associated with the themes in Table 1 is thus likely to be lower than the actual number exploiting these information channels and the related sources. The interviews focused on a single lesson: the values might even out if the use of information channels and sources was considered over the whole training year. Most trainees observed their fellow trainees' training classes, at least if they were assigned to teach the same class later. It is also important to notice that the low number of trainees using personal photographs as information sources does not represent the trainees who sought photographs as pictorial information from other information sources. This paper is not concerned with the type of information the trainees obtained from information sources.

These findings corroborate and refine the pilot study ([Tanni _et al._ 2008](#tan08)), which suggested that trainees' search goals vary substantially depending on how much they know about the topic and useful information sources in advance. The present findings show a broader picture of trainees' information acquisition extending beyond the present lesson plans and active or goal-orientated information seeking. The modes reflect a core characteristic of lesson planning: the task was being iterated over the training period, meaning that the trainees could capitalise on their earlier efforts, when acquiring information for the present lesson plans, and predict future tasks. Seeking known-items is an obvious example of the continuity. Merchant and Hepworth ([2002](#mer02)) make a point about the use of familiar information sources such as textbooks as starting points before turning to sources in libraries or the Web. Erdelez ([1997](#erd97)) and Foster and Ford ([2003](#fos03)) point out that recognising value in accidentally encountered information sources requires past experiences or sense of contexts and situations where such information was or would be valuable. The present findings also show that the trainees moved across different problem areas or topics when accidentally encountering information (see [Erdelez 1997](#erd97)). Clearly, teacher training and lesson planning are contexts and situations fostering accidental encounters with information sources. By monitoring, the trainees were not only seeking information for the present lesson plan but also explicitly preparing for future ones, thereby making _Monitoring_ the only mode explicitly associated with the development of the personal collection. While teachers’ monitoring is not explicitly discussed in the literature, the choice of information channels and sources (i.e., mass media) itself suggests that teachers monitor information sources (see [Small _et al._ 1998](#sma98)).

McKenzie ([2003](#mck03)) notes that models based on studies conducted in academic or workplace contexts tend to describe only active information seeking for present needs and, therefore, do not capture information acquisition for several inter-related present tasks, let alone for any future ones. Such models therefore do not recognise the whole variety of information acquisition. ([McKenzie 2003](#mck03).) Models derived from Kuhlthau’s information search process are apt for describing systematic information seeking over a series of stages in the confines of a single, present task. The process is based on school assignments, which typically are performed in relatively unfamiliar problem areas in isolation of other similar tasks. Task-based research, focusing on single tasks in isolation, does not consider the past or future tasks: personal collections, anticipation of information needs, and, consequently, make sense of accidental encounters or monitoring as modes of information acquisition. Furthermore, the information search process describing learning driven information seeking from the cognitive point of view fails to make sense of accidental encounters and passive information acquisition (i.e., _Being given information_). Lesson planning represents a task type that is iterative and invokes anticipative information acquisition on a time horizon extending beyond a single task. Continuity across individual tasks is an interesting characteristic discerning between types of tasks and the models able to describe them (see [Ingwersen and Järvelin 2005](#ing05): 75).

## Conclusions

Trainees use a broad selection of information channels and sources in lesson planning. trainees actively seek information for present lesson plans from external information sources, but the entirety of their information acquisition encompasses information sources acquired in the past and stored in the personal collection as well. The use of the personal information collection over the course of the task invokes the issue of personal information management, which, although not addressed in this paper, is clearly an important point to consider in task-based studies.

Trainees' information acquisition cannot be described only in terms of their present activities or goal-orientated information seeking behaviour. They access familiar information sources by recall, implying that past experiences of using the sources are relevant to present lesson plans. trainees anticipate and prepare for future tasks by monitoring information sources or places. Accidental encounters of information sources imply connections across time and topics. Therefore, continuity is an important characteristic of lesson planning as a task. It implies time as a central dimension of trainees' information acquisition. Future research on teachers’ tasks should consider continuity across related individual tasks.

## Acknowledgements

I am particularly grateful to the anonymous referees for their constructive comments and suggestions for literature.

## About the author

**Mikko Tanni** can be contacted at: [mikko.tanni@uta.fi](mailto:mikko.tanni@uta.fi).

#### References

*   Borgman, C.L., Smart, L.J., Millwood, K.A., Finley, J.R., Champeny, L., Gilliland, A.J. & Leazer, G.H. (2005). Comparing faculty information seeking in teaching and research: implications for the design of digital libraries. _Journal of the American Society for Information Science & Technology_, **56**(6), 636-657.
*   Boyatzis, R.E. (1998). _Transforming qualitative information: thematic analysis and code development._ Thousand Oaks, CA: Sage.
*   Bruce, H. (2005). [Personal, anticipated information need.](http://www.webcitation.org/6AM4QMaeL) _Information Research_, **10**(3) paper 232\. Retrieved 26 August, 2012 from http://informationr.net/ir/10-3/paper232.html (Archived by WebCite® at http://www.webcitation.org/6AM4QMaeL).
*   Byström, K. (1999). _Task complexity, information types and information sources: examination of relationships._ Unpublished doctoral dissertation, University of Tampere, Tampere, Finland.
*   Byström, K. & Hansen, P. (2005). Conceptual framework for tasks in information studies. _Journal of the American Society for Information Science and Technology_, **56**(10), 1050-1061.
*   Ellis, D. & Haugan, M. (1997). Modelling the information seeking patterns of engineers and research scientists in an industrial environment. _Journal of Documentation_, **53**(4), 384-403.
*   Erdelez, S. (1997). Information encountering: a conceptual framework for accidental information discovery. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts_ (pp. 412-421). London: Taylor Graham.
*   Foster, A. & Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation_, **59**(3), 321-340.
*   Ingwersen, P. & Järvelin, K. (2005). _The turn: integration of information seeking and retrieval in context._ Dordrecht, The Netherlands: Springer.
*   John, P.D. (1991). A qualitative study of British student teachers' lesson planning perspectives. _Journal of Education for Teaching_, **17**(3), 301-320.
*   John, P.D. (1994). The integration of research validated knowledge with practice: lesson planning and the student history teacher. _Cambridge Journal of Education_, **24**(1), 33-47.
*   John, P.D. (2006). Lesson planning and the student teacher: re-thinking the dominant model. _Journal of Curriculum Studies_, **38**(4), 483-498.
*   Jones, K. & Smith, K. (1997). [_Student teachers learning to plan mathematics lessons._](http://www.webcitation.org/6AP4MFD3k) Paper presented at the Annual Conference of the Association of Mathematics Education Teachers (AMET1997), Leicester, 15-17th May. Retrieved 27 August, 2012 from http://eprints.soton.ac.uk/41312/1/Jones_Smith_AMET_conference_1997.pdf (Archived by WebCite® at http://www.webcitation.org/6AP4MFD3k).
*   Kim, K. & Roth, G. (2011). [Novice teachers and their acquisition of work-related information](http://www.webcitation.org/6APYZSp4p). _Current Issues in Education_, **14**(1). Retrieved 11 January, 2012 from http://cie.asu.edu/ojs/index.php/cieatasu/article/view/268/152 (Archived by WebCite® at http://www.webcitation.org/6APYZSp4p)
*   Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services._ (2nd. ed.). Westport, CT: Libraries Unlimited.
*   Leckie, G. J., Pettigrew, K.E., & Sylvain, C. (1996). Modelling the information seeking of professionals: a general model derived from research on engineers, health care professionals and lawyers. _Library Quarterly_, **66**(2), 161–193.
*   Lincoln, Y.S. & Guba, E. G. (1985). _Naturalistic inquiry._ London: Sage.
*   McKenzie, P. (2003). A model of information practices in accounts of every-day life information seeking. _Journal of Documentation_, **73**(3), 261-288.
*   Merchant, L. & Hepworth, M. (2002). Information literacy of teachers and pupils in secondary schools. _Journal of Librarianship and Information Science_, **34**(2), 81-89.
*   Perrault, A.M. (2007). [An exploratory study of biology teachers’ online information seeking practices.](http://www.webcitation.org/6AMM6zB3O) _School Library Media Research_, 10\. Retrieved 19 July, 2011 from http://www.eric.ed.gov/PDFS/EJ851701.pdf (Archived by WebCite® at http://www.webcitation.org/6AMM6zB3O).
*   Savolainen, R. (2006). Time as a context of information seeking. _Library & Information Science Research_, **28**(1), 110-127.
*   Serola, S. (2009). [_Kaupunkisuunnittelijoiden työtehtävät, tiedontarpeet ja tiedonhankinta_](http://www.webcitation.org/6AMMSZbjv), [City planners work tasks, information needs and data acquisition] Tampere, Finland: Tampere University Press. (Acta Electronica Universitatis Tamperensis 809). Retrieved 9 February, 2011 from http://acta.uta.fi/pdf/978-951-44-7603-7.pdf (Archived by WebCite® at http://www.webcitation.org/6AMMSZbjv).
*   Small, R.V., Sutton, S., Eisenberg, M., Miwa, M. & Urfels, C. (1998). An investigation of PreK-12 educators’ information needs and search behaviours on the Internet (Report No. IR 019 040). In N.J. Maushak, C. Schlosser, T.N. Lloyd & M. Simonson, (Eds.), _Proceedings of Selected Research and Development Presentations at the National Convention of the Association for Educational Communications and Technology (AECT), St. Louis, Missouri, February 18-22, 1998_, (pp. 401-414). Washington, DC: Association for Educational Communications and Technology. (ERIC Document Reproduction Service No. ED 423 862).
*   Sonnenwald, D.H. (1999). Evolving perspectives of human information behaviour: contexts, situations, social networks and information horizons. In T.D. Wilson, & D. Allen (Eds.), _Exploring the contexts of information behaviour: Proceedings of the 2nd International Conference on Research in Information Needs, Seeking and Use in Different Contexts_, 13–15 August 1998, Sheffield, UK (pp. 176–190). London: Taylor Graham.
*   Summers, E.G., Matheson, J. & Conry, R. (1983). The effect of personal, professional, and psychological attributes, and information seeking behavior on the use of information sources by educators. _Journal of the American Society for Information Science_, **34**(1), 75-85.
*   Tanni, M., Sormunen, E. & Syvänen, A. (2008). [Prospective history teachers’ information behaviour in lesson planning](http://www.webcitation.org/6AMMig0mq). _Information Research_, **13**(4) paper 374\. Retrieved 27 August, 2012 from http://informationr.net/ir/13-4/paper374.html (Archived by WebCite® at http://www.webcitation.org/6AMMig0mq).
*   Vakkari, P. (2001). A theory of the task-based information retrieval process: a summary and generalisation of a longitudinal study. _Journal of Documentation_, **57**(1), 44-60.
*   Vakkari, P. (2003). Task-based information searching. _Annual Review of Information Science and Technology_, **37**, 413-464.

## Appendices

### The Interview Schedule

1.  What learning goals did you set for the pupils in the lesson plan?
2.  What issues did you consider important to take into account when planning this lesson?
3.  Could you describe in general how the lesson was planned?
4.  For what purposes did you seek information when planning the lesson?
5.  When seeking information for [the purpose],
    1.  at what stage of lesson planning did this occur
    2.  from what sources did you seek information
    3.  where did you access these sources
    4.  in which order did you seek information from these sources
    5.  how did you seek access to these information sources
    6.  on what grounds did you assess the suitability of the information sources you did find
    7.  did you adapt the original materials and if you did how / what did you learn from this information?
6.  What kind of lesson planning related work approaches do you have?
7.  Do you store or save documents sought for lesson plans for future needs?
8.  How do you organise the stored materials?
9.  How much time did it take to plan the lesson?
10.  Were you satisfied with the materials that you found for the lesson plan?
11.  What did you learn about information seeking and use when planning this lesson?

The classroom observations were exploited to elaborate the question 4, if necessary. The questions from 5a to 5g were iterated for and adapted to each purpose articulated by the interviewee. There were two variations of the question 5g, because the teacher trainees’ use of information depended on the purpose for which it was sought (see question 4).