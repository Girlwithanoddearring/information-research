#### vol. 17 no. 3, September 2012

# Activity theory as a theoretical framework in the study of information practices in molecular medicine

#### [Annikki Roos](#author)  
Hanken School of Economics, Helsinki, Finland and Terkko - Meilahti Campus Library, Helsinki University Library, University of Helsinki

#### Abstract

> **Introduction.** It is widely agreed that studies in information science should be situated in a broad or holistic context. Some researchers have highlighted the potential of the cultural historical activity theory in this respect. In this study we have used the activity theory framework to test how well activity theory contributed to the understanding of the information practices of two research groups in molecular medicine.  
> **Method.** The paper is a qualitative study where activity theory is implemented as a method to study contextual issues in information practices.  
> **Results.** In this study the research work in molecular medicine is the central activity. It is studied in the web of neighbouring activity systems, like information services, which is understood as an instrument-producing activity.  
> **Conclusions.** The activity theoretical framework appeared to be useful in providing a holistic approach for the study of information practices in research work on the domain of molecular medicine. The hierarchical structure of activities helped to situate information practices as a tool, which mediates between the subject and the object of the molecular medicine research work. It came apparent that information practices belong to the lower level of activities in the system.

## Introduction

It is widely agreed that studies in information science should be situated in a broad or holistic context (see for example [Ingwersen and Järvelin 2005](#ing05)). Wilson ([2006](#wil06), [2008](#wil08)) and Allen _et al._ [(2011)](#all11) have highlighted the potential of the cultural historical activity theory (or simply activity theory) in this respect. According to Wilson ([2006](#wil06)), the context of information needs and information uses is understood more deeply using the activity theoretical base. Activity theory enables the discovery of contradictions that affect information practices. Activity theory might also provide an overarching paradigm which might combine different problem areas (information seeking, information retrieval) of information studies ([Wilson 2006](#wil06)). This article will set the material from a previous study ([Roos _et al._ 2008](#roo08)) in the theoretical framework of activity theory and test how that will contribute to the understanding of the information practices of a research group in molecular medicine.

The article starts with descriptions of central concepts for information practices, followed by basic concepts and principles of activity theory. Some examples of the use of activity theory in information science are described. After that the domain of molecular medicine is introduced and the activity theory framework is applied as a method to understand research work in molecular medicine. In the later part of the article information practices in molecular medicine are examined with the help of the activity theory framework and the contribution of this method discussed.

## Background

Information related practices, or information practices, are understood to comprise needs, seeking, management, giving, and using information in context. ([Palmer and Cragin 2009](#pal09)). In this study, _information practices_ is used instead of the concept of information behaviour. The reason for this is to try to emphasise the contextual factors of information seeking, use and sharing and consider information seekers in their social context (compare. [Fisher _et al._ 2005](#fis05) and [Savolainen 2007](#sav07)). As Palmer and Cragin ([2009](#pal09)) conclude, understanding the nature of information practices and their relation to the production of knowledge is important for theoretical as well as practical work in information science.

In the early stages of information seeking research (1960-1985), the studies concentrated mainly on the information systems viewpoint: systems needs. People involved were considered as passive users of institutions or systems. Since 1985 the theoretical understanding has expanded and the interactive nature of the situations or context, work tasks, information seeking and information systems is understood to be included. There has been an alteration from systems-oriented to actor-oriented research. This means that individuals are analysed in concrete situations. Because of the emphasis on individuals, some researchers have claimed a more social or holistic approach to the research ([Ingwersen and Järvelin 2005](#ing05)).

Several critical voices have been raised towards the situation in information research. They are expressed by researchers from both the information seeking and information retrieval communities. Hjørland formed the radical opinion about the situation in information research that it has always been in crisis ([Hjørland 1997: 3](#hjo97)). He believes that the user studies' perspective has often been about users' relationship to the system of information sources and notes that retrieval research typically adopts a very atomistic perspective in studying the _match_ between a query and a document. He assumes that the solution could be a critical analysis of the positivistic and idealistic assumptions about knowledge and science. Hjørland's alternative view of knowledge is _realistic_ and he understands it as a tool that is a historically and culturally developed _product_. He is also of the opinion that this view, based on activity theoretical framework, could unite user studies and information retrieval studies in information science ([Hjørland, 1997](#hjo97)).

Hjørland and Albrechtsen have promoted the idea of domain analysis as a new paradigm in information science. They have stated that the best way to understand information could be to study ‘_the knowledge-domains as thought or discourse communities, which are parts of society's division of labor_' ([Hjørland and Albrechtsen 1995: 400](#hjo95)). Knowledge organization, language, communication forms and even information systems are reflections of the objects of the work in these domains and their role in the society. Because of this, it would be necessary to consider, for example, an individual person's information needs from this perspective ([Hjørland and Albrechtsen 1995](#hjo95))

An approach where information seeking research is directed towards both tasks and technology is presented by Ingwersen and Järvelin ([2005](#ing05)). The authors emphasise the need for more empirical research that could empirically answer research questions on characteristics of contexts and task situations, actors, information, seeking processes, sources, systems and use of information. It would be fruitful from the point of view of the design of information retrieval systems to conduct studies where the features of systems and features of tasks and information seeking process are related ([Ingwersen and Järvelin 2005](#ing05)).

Activity theory is a framework that provides clarity to the concept of _context_ in information science research ([Allen _et al._ 2011](#all11)). It may be helpful in several ways: in guiding the development of research instruments, providing a framework for the analysis of data, understanding of user behaviour that can help in the design of systems, and providing understanding of the wider contextual issues that affect systems use and usability ([Wilson 2008](#wil08)).

In this article the activity theoretical framework (described below in _Activity theory and its applications_) is used to understand the information practices of researchers in the molecular medicine research environment. Molecular medicine is an interdisciplinary scientific field that utilizes molecular and genetic techniques in the study of the biological processes and mechanisms of diseases. Its aims are to provide new approaches to the diagnosis, prevention and treatment of disorders and diseases.

The principles of activity theory are studied and applied in order to gain a deeper understanding of the ways in which the context of a domain such as molecular medicine might affect how researchers use and seek information.

The main research questions are:

*   Is activity theory a useful theoretical frame for studying the contextual issues of information practices in molecular medicine?
*   What are the possible benefits of activity theory framework to the research of information practices?

## Method

This is a qualitative study where activity theory is used as a theoretical framework. The present study uses qualitative methods in re-analysing the data which was originally collected in a survey conducted in 2007 ([Roos _et al._ 2008](#roo08)). The information gathered from the survey questionnaire was enriched by semi-structured interviews and observations of the work practices of two research groups in molecular medicine. The groups consisted of a responsible, senior researcher, PhD students and graduate students. Group A was doing research on a disorder which was known to have a polygenetic background and Group B's research objects were diseases which were known to be monogenic, caused by mutations in a single gene.

These two groups of one research institute in Finland were chosen as subjects because of their diverse research objects. It was presupposed that the methods and tools of research would differ considerably as a consequence, and that was intended to give a holistic picture of the research work of the domain of molecular medicine.

Activity theory is a philosophical and multidisciplinary framework that can be used to examine human activity or practice. It was initiated in Russia by Lev Vygotsky at the beginning of the 19th century as a psychological theory of human consciousness, thinking and learning ([Miettinen _et al._ 2009](#mie09)). Activity theory provides a philosophical framework for studying different forms of practices and actions as developmental processes interlinking individual and social levels ([Spasser 1999](#spa99)).

Different models or tendencies of activity theory exist. The general activity theory, sometimes called Scandinavian activity theory is based to the concepts of Vygotsky and Leontiev ([1978](#leo78)), and has been enhanced mainly by Engeström ([1987](#eng87), [1995](#eng95), [1999](#eng99)). The alternative model of the activity theory is called the systemic-structural activity theory, which has been developed by Bedny and colleagues. Although these models are closely related, there are some conceptual and other differences which Wilson ([2006](#wil06)) identifies (see also [Bedny and Karwowski 2007](#bed07)). This study will mainly use the approach of the general activity theory.

## Activity theory and its applications

From the activity theoretical perspective, the activity of any subject is a purposeful interaction of the subject with the world. It is a process in which mutual transformations between the subject and the object are achieved. The subject and the object of an activity transform each other. In activity theory, this central process is called internalization. Through the analysis of activities, it is possible to understand both the subject and the object of the activity ([Kaptelinin and Nardi 2006: 31](#kap06); [Kuutti 1995](#kuu95)).

Engeström's well-known triangular model of the complex activity system is presented in Figure 1.

<figure>![](p526fig1.gif)

<figcaption>Figure 1: A complex model of an activity system ([Engeström, 2001](#eng01))</figcaption>

</figure>

The model is systemic and all the elements are related to each other. According to Kuutti ([1995](#kuu95)) it contains mutual relationships between subject, object and community. There are always various artefacts included in the activities. Tools and signs, Rules and Division of labour involve artefacts which have a mediating role. The relationship between subject and object is mediated by tools (like instruments or programs) and signs (e.g., language), the relationship between subject and community is mediated by rules (for example laws) and the relationship between object and community is mediated by the division of labour. The mediating terms are also historically formed and open to change ([Kuutti 1995](#kuu95)).

In the activity system the object is transformed into outcomes through a hierarchical process of activity. The hierarchy was developed by Leont'ev ([1978](#leo78)) and it distinguishes between activity, actions and operations and relates these terms to motives, goals and the conditions under which the activity is performed.

Activity, a collective phenomenon with a shared object and motive, can be divided to actions according to the division of work. Actions are conducted by individuals or a collective using a physical, or intellectual or abstract tool. For example, an abstract tool can be knowledge or skills ([Mursu _et al._ 2007](#mur07)). Activity systems are networked and interact with other activity systems. Even though individual or group actions are goal-directed and might be independent, they are understandable only against the background of the whole activity system. According to activity theory the prime unit of analysis should be the activity system ([Engeström 2001](#eng01)).

Contradictions have a central role in the development and change of activities. Contradictions mean historically accumulated structural tensions within and between activity systems. These tensions might be caused by adopting a new element such as a technology from outside the system. Contradictions might not produce disturbances and conflicts exclusively, but may also produce innovative solutions to change the activities. When the object and motive are reconceptualised to involve a radically wider horizon of possibilities than in the previous mode of the activity, an expansive transformation happens ([Engeström 2001](#eng01)). According to Kaptelinin and Nardi ([2006: 71](#nar06)) all practice is a result of certain historical developments under certain conditions. The development is both an object of study and general research methodology in activity theory.

The area where activity theory is applied is very broad, ranging from psychology and educational development to organization studies. The most interesting applications of activity theory in respect to the present study are work studies, information systems development and human-computer interaction (e.g., [Wilson 2008](#wil08)). The usefulness of activity theory for human computer interaction has been proposed by (among others) by Kuutti ([1991](#kuu91)), Kaptelinin _et al._ ([1995](#kap95)) and Nardi ([1996](#nar96)). According to Kaptelinin and Nardi ([2006](#kap06)) the use of activity theory meant a turn towards contextual approaches in human-computer interaction.

### Activity theory in information science

There are few researchers in information science who have used activity theory as a theoretical frame; Hjørland and Wilson belong to the pioneers. Wilson ([2006](#wil06), [2008](#wil08)) has made a thorough analysis of the situation and is among the main proponents for activity theory in information science, demonstrating the potential of activity theory to offer a conceptual framework and a new methodological instrument. He noted that use of the framework can draw the researcher's attention to the aspects of the context of activity that otherwise might be missed. According to Wilson the concept of _tool_ is particularly helpful in information searching ([2006](#wil06), [2008](#wil08)). Hjørland ([1997](#hjo97)) urges a nonidealistic, _realistic_ or pragmatic view of knowledge as a base for information seeking research, and concludes that human knowledge is object-oriented and this fact determines information seeking.

According to Wilson ([2006](#wil06)), the hierarchy of activity is very valuable in research into information seeking behaviour (see Figure 2).

<figure>![](p526fig2.gif)

<figcaption>Figure 2: Activity, actions and operations ([Wilson 2006](#wil06))</figcaption>

</figure>

The distinction between activity, actions and operations could give a broader perspective to the role of information behaviour in human life. The role of information seeking could be more realistic when it is not understood as an activity, but a set of actions that support some higher level activity. Wilson suggests also that a re-examination of previous studies in the light of activity theory might be useful in providing insights that could have been missing in previous analysis ([2006](#wil06), [2008](#wil08)).

Spasser ([1999](#spa99)) proposes that activity theory provides a conceptual framework that will facilitate the continual improvement of practice and the transferability and cumulation of knowledge in information science. He considers activity theory to be especially applicable with regard to information systems development, use and evaluation ([Spasser 1999](#spa99)) and he has applied activity theory to the study of digital library evaluation ([Spasser 2002](#spa02)).

Widén-Wulff and Davenport ([2007](#wid07)) have used activity theory in their study of information sharing and creation of knowledge in two organizations. They used data which was collected without activity theory–analysis in mind. According to them activity theory was useful because it forced them to clarify the terminology and to expand the horizons within which they observed and explored behaviour ([Widén-Wulff and Davenport 2007](#wid07)).

Allen _et al._ ([2011](#all11)) argue that one of the greatest benefits of activity theory in the study of information practices is that it takes into account context. It concentrates on the cultural and technical mediation of human activity instead of an isolated human being. On the other hand, the hierarchical structure of the activity (activity-action-operations) provides the opportunity to reduce activity into its components, the deconstruction of an activity is possible and makes the analysis of information practices fruitful. In addition, activity theory provides a theoretical lens which helps in explaining activity mediation and this may improve the development of practical solutions, like the design or development of systems ([Allen _et al._ 2011](#all11)).

Despite the examples above, the situation in the research of information practices has often been quite similar to the one described by Kaptelinin and Nardi ([2006](#kap06)) in human-computer interaction: studies have focused on the usage of a system or a tool, as if the use of the system or tool would have its own purpose. If the theoretical frame of activity theory is applied, the focus will be extended to a meaningful context of the subject's interaction with the world, including the social context. In the following section, the activity theoretical lens will be used in the analyses of information practices in two research groups of molecular medicine. The aim is to connect information practices to other object-oriented activities and their social context.

## Research work in molecular medicine

### Molecular medicine

To be able to understand information practices of researchers in molecular medicine, the nature of the domain and its historical development need to be comprehended.

Molecular medicine is an interdisciplinary, applied scientific field which uses molecular and genetic techniques in the study of the biological processes and the mechanisms of human diseases. It is understood mainly as one sub-discipline of biomedicine but is also connected closely to biochemistry, cell biology, molecular biology and molecular genetics. Molecular medicine is data intensive, producing and processing a huge amount of all kinds of data. Dependence on expensive equipment and technology of acquiring and analysing data is characteristic of this domain. This is why the variety of methods and data analysing tools used in research work is broad ([Roos _et al._ 2008](#roo08)). Because the task in molecular medicine seems to be quite unified, results are produced in standardized ways and are clearly linked to theoretical goals. The reporting systems are also stable, standardized and hierarchically ordered (see [Hedlund and Roos 2007](#hed07)). This means that it is easier to co-ordinate and control work procedures, the channels of communication, and interconnected goals across the domain. The mutual dependence in the domain of molecular medicine is quite high (see [Whitley 2000](#whi00)) and the control of and competition for reputation in the field is challenging.

The human genome was mapped and sequenced in the Human Genome Project between 1990 and 2003\. According to Collins _et al._ ([2003](#col03)) the Project has generated massive and complex databases which have transformed the study of virtually all life processes. Advances in genetics, comparative genomics, biochemistry and bioinformatics have made it possible to analyse and study the health and diseases of organisms on a molecular level. The increased understanding of the interactions between the entire genome and nongenomic factors has caused a change from the era of genetic medicine (i.e., knowledge about single genes is used to improve the diagnosis and treatment of single-gene disorders) to genomic medicine. This has meant a significant change from small, individual laboratories to broad international research collaboration. Genomics, the study of whole genomes ([Pohlhaus and Cook-Deegan 2008](#poh08)), has become central to the research field of biomedicine ([Collins _et al._ 2003](#col03); [Feero _et al._ 2010](#fee10); [Kellenberger 2004](#kel04)).

From the activity theoretical perspective, a fundamental, expansive change has occurred in the activity systems of molecular medicine research. The development of the physical tools, like technology, has been huge and rapid. It has changed the community involved, the rules mediating the relationship between the actor and the community and also the division of labour. For example, the development of effective and appropriate genomics methods highlights new ethical, legal and social questions. Totally new professions such as bioinformaticians have been born. The role of small, individual laboratories has diminished and the importance of international collaboration has increased as genome research has emerged.

### Research work and information practices

One of the major tasks of molecular medicine is to identify genetic factors that cause a certain disease or increase an individual's risk of a disease (see [Runge and Patterson 2006](#run06)). When studying biological processes and the mechanisms of human diseases, molecular medicine uses molecular and genetic techniques and tools such as bioinformatics or animal models.

MacMullen and Denn ([2005](#mac05)) have analysed the problems in molecular biology and grouped them into three general classes: problems related to the structure of protein molecules, the function of genes and proteins, and communication (i.e., information flow through DNA, RNA, protein transcription and translation). The problems of structure, function and communication are interrelated. Tran _et al._ ([2004](#tra04)) have applied task analysis when studying researchers' work in genomics and proteomics. The four major categories of work tasks were gene analysis, protein analysis, biostatistical analysis, and literature searching.

Denn and MacMullen ([2002](#den02)) have also analysed the experimental cycle of molecular biology and the role and possibilities of information science in it. According to them, the experimental cycle includes the following steps: problem/question, experimental design, sample preparation, data acquisition, data transformation, analysis and comparison, modelling and simulation and replication or duplication. There is likely to be a broad range of information science activities, each at different stages of the research process of the molecular biology. Denn and MacMullen classify them into three categories: the development of new tools and methods for managing, integrating and visualizing data, the application of tools and methods for integration, inference and discovery, and theoretical approaches to biological information.

According to Roos _et al._ ([2008](#roo08)) the information environment in molecular medicine is varied. It consists of huge amounts of data which is stored in several types of databases, searched through different information retrieval systems and used by diverse analysis tools. Research and bibliographic databases as well as full-text articles are used seamlessly so that data located (or constructed) in one are used to access the other. Bioinformatics database portals are very popular probably because many of them enable the interlaced use of different databases ([Buetow 2005](#bue05); [Roos _et al._ 2008](#roo08)).

Fry ([2006](#fry06)) has applied Whitley's theory ([2000](#whi00)) in the study of information practices in different domains. Her findings show that in fields like molecular medicine, which have high degree of mutual dependence, low degree of task uncertainty and which co-ordinate and control reporting channels, researchers work willingly together with field-based digital information resources. This fits well with the observation made by Roos _et al._ ([2008](#roo08)) that bioinformatics portals are very popular among the researchers in molecular medicine. In other fields, such as the social sciences, where the degree of mutual dependence is low, the degree of task uncertainty is high and communication channels are less controlled, field-based digital resources are not integrated into the epistemic and social structures of the field in the same way.

### The activity system in molecular medicine

The following analysis is based on the survey conducted in the 2007 ([Roos _et al._ 2008](#roo08)). The information collected via the survey was improved with interviews and the observation of work practices in two research groups. These groups were chosen on the basis that they had diverse research objects and so would together give a holistic picture of the research work in this domain. In total, seven interviews were conducted, during which researchers were asked to describe the research process, how it proceeds and which stages it includes. Special attention was paid to information intensive stages and the problems researchers experienced during these stages. Both senior researchers and research students were interviewed.

According to the interviews and the observation of the work practices, the main objects (or activity on the highest level) of the research work were twofold: a) which gene or genes cause the specified disorder or disease, and, b) what is the molecular mechanism behind the disease in the gene. The outcome of such research could be to help in preventing or treating the disorder caused by the mutations. This activity system could be called research work in molecular medicine. As an example, the elements of the activity system are represented in a rough, non-exclusive way in the Table 1.

<table class="center" style="width:90%"><caption>  
Table 1: An example of the elements in the activity system of molecular medicine</caption>

<tbody>

<tr>

<th style="width:30%">Element</th>

<th style="width:70%">An example of the element</th>

</tr>

<tr>

<td>Subject or actor</td>

<td>The research group</td>

</tr>

<tr>

<td>Outcome</td>

<td>Prevent a disease (e.g. breast cancer) and find treatment for it</td>

</tr>

<tr>

<td>Object/motive</td>

<td>The study of the biological processes and the mechanisms of the human disease</td>

</tr>

<tr>

<td>Tools</td>

<td>Molecular and genetic techniques. Previous research results. Research data and databanks. Tools and applications</td>

</tr>

<tr>

<td>Rules</td>

<td>Rules for scientific work and communication in the domain, incl. ethical rules. Mechanisms for financing. Organizational rules</td>

</tr>

<tr>

<td>Community</td>

<td>Global scientific community, financing agencies, publishers, research institutes, universities, research groups, teams, individual researchers with different roles</td>

</tr>

<tr>

<td>Division of labour</td>

<td>Laboratory work, scientific computing, administrative work, browsing and searching information and data, analysing results, publishing results etc.</td>

</tr>

</tbody>

</table>

It is evident that the activity system of research work in molecular medicine does not sit in isolation but is connected to various simultaneous activity systems which have different objects. Examples of these are the activity system of information services in the library, the activity system of education in the university, or the activity system of management and administration of the university or research institute. Thus, following Engeström ([1987](#eng87)), we see the activity system as part of an embedded web of activity systems. When we apply this to the present study and the activity system of research work in molecular medicine (Figure 3), we find that the research work appears as the central activity and the prevention or treatment of the diseases as the object-activity. According to Engeström, the object-activity includes activities where the objects and outcomes of the central activity system are embedded. The central activity system is surrounded also with other ‘_neighbour activities_', which are instrument-producing activity, subject-producing activity and rule-producing activity ([Engeström 1987: 89](#eng87), [1995: 63](#eng95)).

From this same viewpoint and depicted in Figure 3, education could be understood as a subject-producing activity. Some members of the research groups were involved in this activity and had as their main objective to finish their studies and get a PhD in genetics. This objective might to some extent be contradictory to the objective of the central activity system of research. Management could be an example of the rule-producing activity. The responsible researcher had to be at the same time also an administrative manager and in charge in financing. The situation was challenging when some of the researchers belonged to different organizations or to two or even more institutions concurrently, each of which had their own infrastructure and at least some differing objectives. Information services or bioinformatics could be comprehended as examples of instrument producing activities. This web of activity systems is presented in Figure 3.

<figure>![](p526fig3.gif)

<figcaption>Figure 3: The web of activity systems in the research work of molecular medicine (see [Engeström 1987: 89](#eng87), [1995: 63](#eng95))</figcaption>

</figure>

The process in the activity system where the object is transformed to outcomes is hierarchical and actually very complicated. Activities are influenced by other activities, they change constantly and there is always a web of connected activity systems that have different objects. At every level of the activities, the processes of change are interactive ([Allen _et al._ 2011](#all11); [Hasan and Gould 2001](#has01)).

Based on the interviews and observations, the activity system of research work in molecular medicine could, for example, consist of the following actions: inventing the research idea, planning the laboratory implementation, laboratory work, the analysis of the results, the reporting of the results. The chain of actions is described roughly and non-exclusively in the Figure 4 below. Our intention was to highlight the nature of the research work as a long-lasting process but the result is not very successful. Wilson ([2006](#wil06)) has pointed to the static nature of the diagrammatic representations of activities (see above). This becomes evident in this study when the research process is the target of the analysis. One possibility to highlight the process nature of the activity is to describe its normal flow of actions. This is the approach in the following figure.

<figure>![](p526fig4.gif)

<figcaption>Figure 4: The chain of actions in the research work of molecular medicine</figcaption>

</figure>

At every stage of the process, the outcome of the activity system is extremely significant for the development of the next stage. Each action has a differing object and the subject might also differ. For example in the idea creation stage, the role of the principal investigator or the group leader is central as is her/his knowledge, skills and networks in the research community. Financial challenges might be the directing elements (rules, community) especially for the experiment planning. The role of the mediating tools in actions is central. They can modify the existing actions and open possibilities for new ones ([Allen _et al._ 2011](#all11)). Information technology has been very important for the development of the domain of molecular medicine. The mediating role of the technological development has caused an expansive change in the community, the division of labour and the role of other tools in the domain.

## Activity theory as a basis for understanding researchers' information practices in molecular medicine

Applying the principles of activity theory to the analysis of information practices in molecular medicine means that information practices will not be taken out of context; otherwise they would be seen as isolated entities that cannot be properly understood or developed.

The web of activity systems described in Figure 3 and the chain of actions in Figure 4 help to put information practices in their broader context. From previous research we know that certain actions in the research work are more information intensive than others (see for example [Palmer and Cragin 2009](#pal09)). According to the data collected by interviewing the researchers, idea creation, analysis (setting the results in the context) and reporting of the results were the most important actions in relation to information practices. The main information-intensive actions and the most relevant information practices related to them are presented in Table 2.

<table class="center"><caption>  
Table 2: The main information intensive actions in the research work of molecular medicine and related information practices  
</caption>

<tbody>

<tr>

<th>Action</th>

<th colspan="1">Information practices</th>

</tr>

<tr>

<td>Research idea creation</td>

<td>

*   Reading
*   Discussions with colleagues
*   Conferences

</td>

</tr>

<tr>

<td>Analysis of results</td>

<td>

*   Searching facts from bioinformatics databases, using analysis tools
*   Reading literature

</td>

</tr>

<tr>

<td>Reporting results</td>

<td>

*   Reading literature: results from previous studies
*   Searching references from PubMed and OMIM (Online Mendelian Inheritance in Man)
*   Searching facts from bioinformatics databases

</td>

</tr>

</tbody>

</table>

Reading the literature, discussions with colleagues and conferences were the most important information practices in the action of idea creation. Reading and searching the literature was considered as the most critical but also the most time-consuming and difficult in the following actions: idea creation, analysis and reporting. The huge amount of published literature (i.e., articles in the domain) was perceived as a big problem and the need to filter out the irrelevant and evaluate the relevant information was expressed as a challenge. One fourth of the survey respondents named library catalogues or portals as a finding tool for the literature, one third named Google, Google Scholar or both. [PubMed](http://www.ncbi.nlm.nih.gov/pubmed) was without doubt the most important single literature search tool for the researchers. Some of the interviewees expressed difficulties in choosing the search terms in PubMed, and there was a significant difference between the two research groups in this respect. Those researchers who were studying a disease which had a multi genetic background reported that they had difficulties in choosing the search terms, while researchers of monogenic diseases did not express similar problems.

Searching the literature from PubMed was named by both research teams as an important part of the reporting of results action. Various databases and tools are very important in the analysis of results action. For example, [UCSC Genome Browser](http://genome.ucsc.edu) and [similar services](http://www.ncbi.nlm.nih.gov/genome/) provided by the [National Center for Biotechnology Information](http://www.ncbi.nlm.nih.gov/) are frequently used. Some analysis or other tools were not used because of the lack of knowledge or ability to use them. Numerous tools, web services and databases were well known, but the interviewees expressed little interest in them because they lacked the time to get to know and use them. In conclusion, it could be said that information practices were experienced as necessary but difficult and time-consuming throughout the research work.

From the activity theoretical perspective, information practices of researchers could be considered mainly as one tool that mediates between the subject and the object of the activity system of research work. Information services could be regarded as one neighbouring activity, i.e., they are an instrument-producing activity system for the central activity of research work in molecular medicine (see Figure 3). Whitley's ([2000](#whi00)) theory of the intellectual and social organization of academic fields could be understood as one explaining component of the activity theory element rules.

The hierarchical aspect of activities in activity theory has been pointed out as one possible major benefit of implementation of activity theory to information science ([Wilson 2008](#wil08)). When the activity is divided into three hierarchical levels across the whole process, the path from activities, actions and operations to outcome becomes more understandable and clearer. It also becomes easier to analyse the problems and hindrances affecting the activity and to try to find solutions to them.

In the following figure (Figure 5) one example of the hierarchy of information related practices in molecular medicine is represented. In this example, the activity generating motive is to understand the function of a certain gene. This motive produces the activity of researching the function of a gene and determines the goal of finding all known facts about the gene. The goal results in numerous parallel actions, an example being to search existing information about the gene from different sources. Searching the [Online Mendelian Inheritance in Man](http://www.ncbi.nlm.nih.gov/omim) is an example of an operation which is affected by the conditions like the availability of the tool or knowledge to use it. The motive (understand the function of the gene) rather than the goal explains the emergence of all the actions, such as searching existing information (see [Engeström 1999](#eng99)).

<figure>![Figure 5: An example of the hierarchy of information related practices in molecular medicine](p526fig5.png)

<figcaption>Figure 5: An example of the hierarchy of information related practices in molecular medicine</figcaption>

</figure>

From this perspective, it seems evident that information practices belong to the lower level of activities in the activity system of research work in molecular medicine. This does not necessarily mean that the significance of information practices to the outcome of the activity should be low. This example also clarifies the importance of relationships and dependencies between different elements and levels of activities. A simplified example: when information specialists advise researchers in the use of a particular search device the advice is more efficient if they are acquainted with the higher level goals of the researchers. If the technical functionality of the search device is the main focus of this action, it might be more difficult for researchers to find real benefit from the guidance.

Researchers are not just using a certain tool or device; they are actors who make choices consciously. It became evident that the researchers used familiarity as one of the main grounds to choose to use a certain tool, for example, the Genome Browser. When the information environment is as manifold as in molecular biology and medicine this is a very sensible way to proceed.

Another important consequence from the adaption of principles of activity theory is that the focus of the research into information practices should be more about problems and contradictions. Researchers in one of the target groups noted particularly the problems they had choosing search terms that enabled them to keep track of the relevant information in the large amount of published material. The number of available databases and tools in the domain is huge. Researchers were aware of this but were able to use only a small number of them. It appeared also that at least some of them had problems using even the more familiar tools. Lack of time was also stated as a reason for not studying to use new tools which were known to be available.

## Discussion

Typically, research in information science has concentrated either on one aspect of the activity system (mostly in the category of tools) or on the functionality of one kind of tool (information use, searching for information, etc.). The relationship between the subject and the tools has also been a focus. The complexity of the whole activity system as well as the objective of the activity has often been left out of the analysis. As described earlier ([Järvelin and Ingwersen 2004](#jar04)), using a system or a tool appears as a purpose in its own right in information science. The research object of information science is presented in the triangle form in Figure 6.

<figure>![Figure 6: Research object of information science and the triangle of activity theory](p526fig6.png)

<figcaption>Figure 6: Research object of information science and the triangle of activity theory</figcaption>

</figure>

The first principle of activity theory, that the activity system is taken as the prime object of analysis, implies that ‘_the scope of analysis needs to be extended from tasks to a meaningful context of subject's interaction with the world including the social context_' ([Kaptelinin and Nardi 2006: 34](#kap06)). Activity theory seems to provide a holistic approach to studying information practices in the context.

According to the activity theoretical framework, information practices are understood as tools which have a mediating role between the outcome and the subject in the net of activity systems of research work in molecular medicine. In the hierarchy of activities, information practices belong to lower level of actions and operations and do not have a meaning in themselves, but rather should be understood as one tool among others. In this case it seemed that the activity system of local information services did not provide much support to the researchers when, for example, only a quarter of the researchers used library-related services as a tool in the literature searching actions. This could be an indication of a contradiction between the separate activity systems of research work in molecular medicine and information services provided by the library. An explanation could be that the structure and hierarchy of the activity system of research work has not been understood well enough in the planning of local information services, and because of this, objects of these systems might be at least partly contradicting. If the tools are designed in isolation, out of the context of the net of activity systems, there is a risk that these tools will be impractical and even useless in the research work context.

When planning the information services to researchers it is essential to be aware of hierarchy of actions, objects and outcomes of activity systems in the research process and to understand the rules subjects follow when interacting with the molecular medicine community. The study of the hierarchy of activities in this case helped to situate information practices in the activity system of the research process, i.e., to see them in context. It became apparent that collaboration between members of these activity systems would be needed if the tools and systems were to support the activities of the researchers suitably.

As stated earlier in this study information practices were considered to be necessary but hard to do and time-consuming. They belonged to lower level of activities in the net of activity systems of research work in molecular medicine. On this basis, it might be possible to claim that in this case there seemed to exist almost a lack of interest to the information practices.

The activity theoretical viewpoint also raises the role of other activity systems where some members of the research groups are involved simultaneously. An example of this is the system of higher education. Different actors, such as senior researchers and PhD or graduate students may have different needs for information because they have different (if similar) motives and roles in the various activity systems. The viewpoint could be useful when planning the training of information resources or services to these researcher groups, for example.

This case had made it clear that from the perspective of information practices, molecular medicine is not a unified domain. Research directed to multi genetic diseases, monogenic diseases or examination of the genome have more or less different information-related problems.

Activity theory-based analysis helps in highlighting and systematizing various factors that exist in the research work. It would be profitable if attention could be given to all elements and their relations and interconnections when researching information practices. This might be helpful in planning information services to researchers or research groups because it would help in being active or at least in reacting in a timely way to the changes which seem to be forthcoming in some areas of the activity system.

The concepts of activity theory might be abstract or even vague. The boundary between an activity and an action for example is not always clear in practice. It might depend on the viewpoint whether certain activities in the research work are understood as actions or tools which mediate between the actor and the object of the research work.

## Limitations of the study

The most notable limitation of this study is that the analysis and conclusions are based on a limited number of researchers and research groups in single research institute. The examination is also on a quite general level and a more detailed inquiry might have given results that would have been practically more useful. However the understanding of information practices in molecular medicine has been deepened by this case study.

Historicity is one of the principles in activity theory. Deeper historical perspective and analysis might have been needed in this study. This limitation might lead to deficiencies in the analysis of problems and potential in the activity system of molecular medicine.

It also became evident that the activity theoretical model used in this study offered a relatively static model for the analyses of processes.

## Conclusions

The activity theoretical framework seems to be useful in providing a holistic approach for the study of information practices in research work in the domain of molecular medicine. It helps in pointing out the complex nature of the process where researchers work towards achieving particular goals. Activity theory helps to understand more deeply the domain of molecular medicine and the structure of research work when compared to the previous study of Roos _et al._ ([2008](#roo08)). The most important benefit in using this framework for information science might be that it puts information practices in their context, as a mediating tool in the research process, and helps to structure them; thus it highlights important elements which otherwise might have been missed. It focuses attention on factors like the object and the hierarchy of activities in the research work. Activity theory shows the true importance of how the historical development of the domain has affected information practices and especially the development of the information environment.

The activity system of research work in molecular medicine consists of various embedded actions and related activities. Research work was presented in this study as a central activity with education, management and (for example) information services as neigbour activities. In the research work activity system, the most information intensive actions were the creation of the research idea, the analysis of the results and the reporting of results. There also existed other simultaneous activities which had at least partly differing and sometimes even contradicting objects.

In the hierarchy of activities, information practices belonged to the lower level, i.e., to actions and operations. From the point of view of researchers, these activities and operations seemed not to be significant on their own, only in relation to the main activity. Information practices have their place in the hierarchy of other object oriented activities of researchers.

It appeared that the activity system of information services did not provide much support to the researchers when, for example, only a quarter of the researchers used library-related services as a literature searching tool. This could be an indication of a contradiction between the separate activity systems of research work in molecular medicine and information services provided by the library. An explanation for this could be that the activity system of research work is not understood well enough when information services are being constructed and that the object of these systems might be partly contradicting.

Another benefit from the use of the activity theory was that it helped us to understand that researchers who work in the same domain and even in the same field might have various objectives and motives because of other simultaneous activities. Every field also has its own history and context which have an impact on the use of and need for specific tools. These differences need to be understood and applied when information services, systems or tools are designed for these actors.

In the end it became obvious that in the light of the activity theory the term user should be replaced by the term actor in the information environment. According to activity theory, activities are conscious, motivated and dynamic. _Actor_ in information environment takes into account the interactive nature of the process where the actor transforms the goals to outcomes better than _user_.

In practice, activity theory might be useful when it directs information practitioners to think about the objective of researchers' activities. It seems evident that information practitioners need to cooperate with the actors in the research work in order to develop tools and practices that support as effectively as possible the main objective of the research work. It would be even more advantageous if other related activity systems were to be included in this work. This might lead information system scientists and information scientists together with researchers in molecular medicine to combine their efforts and skills for the design of information systems and their interfaces. A real collaboration between members of these activity systems would be needed if the tools and systems are to support the activities of the researchers suitably. More interdisciplinary research using the frame of activity theory could be conducted to attain this goal. It might also be able to test the usefulness of the expansive work development method in the research of information practices.

## Acknowledgements

I want to thank warmly Dr. Turid Hedlund for commenting and supporting this work. I am also grateful to the anonymous referees for their comments and suggestions. Thanks to Amanda Crossham for an irreplaceable work in copyediting.

## About the author

Annikki Roos is the Library Director at the Terkko Meilahti Campus Library in the Unversity of Helsinki, Finland. She can be contacted at: [annikki.roos[at]helsinki.fi](mailto:annikki.roos@helsinki.fi).

#### References

*   Allen, D., Karanasios, S. & Slavova, M. (2011). Working with activity theory: context, technology, and information behavior. _Journal of the American Society for Information Science and Technology_, **62**(4), 776-788.
*   Bedny, G. & Karwowski, W. (2007). _A systemic-structural theory of activity. Applications to human performance and work design_. Boca Raton, FL: CRC Press.
*   Buetow, K.H. (2005). Cyberinfrastructure: e\empowering a “Third Way” in biomedical research. _Science_, **308**(5723), 821-824.
*   Collins, F.S., Green, E.D., Guttmacher, A.E. & Guyer, M.S. (2003). [A vision for the future of genomics research.](http://www.webcitation.org/69GqtnLZX) _Nature_, **422**(6934), 835-847\. Retrieved 23 August, 2012 from http://www.nature.com/nature/journal/v422/n6934/full/nature01626.html (Archived by WebCite® at http://www.webcitation.org/69GqtnLZX)
*   Denn, S.O. & MacMullen, W.J. (2002). The ambiguous bioinformatics domain: a conceptual map of information science applications for molecular biology. _Proceedings of the American Society for Information Science & Technology_, **39**(1), 556-558.
*   Engeström, Y. (1987). _Learning by expanding: an activity-theoretical approach to developmental research_. Helsinki: Orienta-Konsultit.
*   Engeström, Y. (1995). _Kehittävä työntutkimus: perusteita, tuloksia ja haasteita_. [Developmental work research: results and challenges] Helsinki: Painatuskeskus.
*   Engeström, Y. (1999). Innovative learning in work teams: analysing cycles of knowledge creation in practice. In Y. Engeström, R. Miettinen & R.-L. Punamäki (Eds.), _Perspectives on activity theory_ (pp. 377-404). Cambridge: Cambridge University Press.
*   Engeström, Y. (2001). Expansive learning at work: toward an activity theoretical reconceptualization. _Journal of Education and Work_, **14**(1), 133-156.
*   Feero, W.G., Guttmacher, A.E. & Collins, F.S. (2010). [Genomic medicine: an updated primer](http://www.webcitation.org/69GrMSRs1). _New England Journal of Medicine_, **362**(21), 2001-2011\. Retrieved 23 August, 2012 from http://www.nejm.org/doi/full/10.1056/NEJMra0907175 (Archived by WebCite® at http://www.webcitation.org/69GrMSRs1)
*   Fisher, K.E., Erdelez, S. & McKechnie, L. (Eds.). (2005). _Theories of information behavior_. Medford, NJ: Information Today.
*   Fry, J. (2006). Scholarly research and information practices: a domain analytic approach. _Information Processing & Management_, **42**(1), 299-316.
*   Hasan, H. & Gould, E. (2001). Support for the sense-making activity of managers. _Decision Support Systems_, **31**(1), 71-86.
*   Hedlund, T. & Roos, A. (2007). Open access publishing as a discipline specific way of scientific communication: the case of biomedical research in Finland. _Advances in Librarianship and Organization_, **25**, 113-131.
*   Hjørland, B. (1997). _Information seeking and subject representation: an activity-theoretical approach to information science_. Westport, CT: Greenwood Press.
*   Hjørland, B., & Albrechtsen, H. (1995). Toward a new horizon in information science: domain-analysis. _Journal of the American Society for Information Science_, **46**(6), 400–425
*   Ingwersen, P. & Järvelin, K. (2005). _The turn: integration of information seeking and retrieval in context_. Dordrecht, The Netherlands: Springer.
*   Järvelin, K., & Ingwersen, P. (2004). [Information seeking research needs extension towards tasks and technology](http://www.webcitation.org/69GrlcZ5Y). _Information Research_, **10**(1), paper 212\. (Archived by WebCite® at http://www.webcitation.org/69GrlcZ5Y)
*   Kaptelinin, V., Kuutti, K. & Bannon, L. (1995). Activity theory: basic concepts and applications: a summary of the tutorial given at the east west HCI95 conference. In Brad Blumenthal, Juri Gornostaev & Claus Unger, (Eds.). _Human-computer interaction_ (pp. 189-201). Berlin/Heidelberg: Springer. (Lecture notes in computer science, 1015/1995).
*   Kaptelinin, V. & Nardi, B.A. (2006). _Acting with technology_. Cambridge, MA: MIT Press.
*   Kellenberger, E. (2004). [The evolution of molecular biology](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1299088/) . _EMBO reports_, **5**(6), 546-549.
*   Kuutti, K. (1995). Activity theory as a potential framework for human-computer interaction research. In B.A. Nardi (Ed.), _Context and consciousness_ (pp. 17-44). Cambridge, MA: MIT Press.
*   Kuutti, K. (1991). Activity theory and its applications to information systems research and development. In H-E. Nissen, H. K. Klein and R. Hirscheim (eds.), _Information systems research: contemporary approaches and emergent traditions_, (pp. 529-549). Amsterdam: Elsevier.
*   Leont'ev, A. N. (1978). _[Activity, consciousness, and personality](http://www.webcitation.org/5IWYmOhTm)_. Retrieved 23 August, 2012 from http://www.marxists.org/archive/leontev/works/1978/index.htm (Archived by WebCite® at http://www.webcitation.org/5IWYmOhTm)
*   MacMullen, W.J. & Denn, S.O. (2005). Information problems in molecular biology and bioinformatics. _Journal of the American Society for Information Science and Technology_, **56**(5), 447-456.
*   Miettinen, R., Samra-Fredericks, D. & Yanow, D. (2009). Re-turn to practice: an introductory essay. _Organization Studies_, **30**(12), 1309-1327.
*   Mursu, A., Luukkonen, I., Toivanen, M. & Korpela, M. (2007). [Activity theory in information systems research and practice: theoretical underpinnings for an information systems development model](http://www.webcitation.org/5um42oLJv). _Information Research_, **12**(3), paper 311\. Retrieved 23 August, 2012 from http://informationr.net/ir/12-3/paper311.html (Archived by WebCite® at http://www.webcitation.org/5um42oLJv)
*   Nardi, B.A. (1996). _Context and consciousness: activity theory and human-computer interaction_. Cambridge, MA: MIT Press.
*   Palmer, C.L. & Cragin, M.H. (2009). Scholarship and disciplinary practices. _Annual Review of Information Science and Technology_, **42**, 163-212.
*   Pohlhaus, J. & Cook-Deegan, R. (2008). [Genomics research: world survey of public funding](http://www.webcitation.org/69Gtgnwm9). _BMC Genomics_, **9**(1), 472\. Retrieved 23 August, 2012 from http://www.biomedcentral.com/1471-2164/9/472/ (Archived by WebCite® at http://www.webcitation.org/69Gtgnwm9)
*   Runge, M.S. & Patterson, C. (2006). _Principles of molecular medicine_. Totowa, NJ: Humana Press.
*   Roos, A., Kumpulainen, S., Järvelin, K. & Hedlund, T. (2008). [The information environment of researchers in molecular medicine](http://www.webcitation.org/5xieZtodg). _Information Research_, **13**(3), paper 353\. Retrieved 23 August, 2012 from http://informationr.net/ir/17-3/paper353.html (Archived by WebCite® at http://www.webcitation.org/5xieZtodg)
*   Savolainen, R. (2007). Information behavior and information practice: reviewing the "umbrella concepts" of information-seeking studies. _Library Quarterly_, **77**(2), 109-132.
*   Spasser, M. (1999). Informing information science: the case for activity theory. _Journal of the American Society for Information Science_, **50**(12), 1136-1138.
*   Spasser, M.A. (2002). Realist activity theory for digital library evaluation: conceptual framework and case study. _Computer Supported Cooperative Works_, **11**(1/2), 81-110.
*   Tran, D., Dubay, C., Gorman, P. & Hersh, W. (2004). Applying task analysis to describe and facilitate bioinformatics tasks. In M. Fieschi, E. Coiera & Y.-C.J. Li (Eds.) _MEDINFO 2004: Proceedings of the 11th World Congress on Medical Information_, (pp. 818-822). Amsterdam, IOS Press.
*   Whitley, R. (2000). _The intellectual and social organization of the sciences_ (Vol. 2). Oxford: Oxford University Press.
*   Widén-Wulff, G. & Davenport, E. (2007). [Information sharing and organizational knowledge production in two Finnish firms: an exploration using activity theory](http://www.webcitation.org/5t0ZfnNVv). _Information Research_, **12**(3), paper 310\. Retrieved 23 August, 2012 from http://informationr.net/ir/12-3/paper310.html (Archived by WebCite® at http://www.webcitation.org/5t0ZfnNVv)
*   Wilson, T.D. (2008). Activity theory and information seeking. _Annual Review of Information Science and Technology_, **42**, 119-161.
*   Wilson, T.D. (2006). [A re-examination of information seeking behaviour in the context of activity theory](http://www.webcitation.org/5IWYdH5fH). _Information Research_, **11**(4), paper 260\. Retrieved 23 August, 2012 from http://informationr.net/ir/11-4/paper260.html (Archived by WebCite® at http://www.webcitation.org/5IWYdH5fH)