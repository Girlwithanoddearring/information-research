#### vol. 21 no. 1, March, 2016

# Effective techniques for the promotion of library services and resources

[Zhixian Yi](#author)

#### Abstract

> **Introduction.** This study examines how Australian academic librarians perceive techniques for promoting services and resources, and the factors affecting the perceptions regarding effectiveness of techniques used.  
> **Method.** Data were collected from an online survey that was sent to 400 academic librarians in thirty-seven Australian universities. The response rate was 57.5%.  
> **Analysis.** The qualitative data were analysed using content analysis. The collected quantitative and qualitative data were analysed using descriptive and inferential statistics (ordinal regressions).  
> **Results.** Librarians used a variety of techniques to promote services and resources. Demographic variables, human capital variables and library variables were significant predictors of perceptions of the effective promotion techniques used. However, this study indicates that other independent variables such as number of different library professional positions and years involved in all library services made no difference.  
> **Conclusions.** This study provides a better understanding of academic librarians' attitudes and views towards techniques for promoting services and resources. Librarians may use the results to reflect on the effectiveness of these techniques, to balance the weight of the factors' influences and to better understand various promotion techniques. This will enable them to promote library services and resources more effectively in the future.

## Introduction

In this digital age, academic libraries are facing a variety of challenges such as ongoing budget cuts, application of new information technologies, changing internal and external environments, and changing demands of research and teaching. A managerial tool assisting libraries to face challenges now and in the future is effective promotion and marketing. Today, academic libraries are no longer the only choice for students, faculty, staff and other clients to go to for information. To attract clients, generate non-user awareness, and raise awareness of available services and resources, libraries need to find ways to promote services and resources to clients as effectively as possible. As such, promotional approaches are used to convey the availability and value of services and resources to target markets and should be designed to cause library users and non-users to act ([Helinsky, 2008](#hel08); [Webreck Alman, 2007](#web07)). These approaches must be developed in such a way that they gain user attention, providing a reason for the library's services and resources to be selected over those of competing services ([Chartered Institute of Marketing, 2009](#cha09)).

### Promotional approaches

Promotional tools that can be used by academic libraries to promote their services and resources include: digital media, such as the library's Website, e-mail lists, blogs and podcasts; print materials, such as posters, handouts and giveaways; events such as orientation tours and workshops; and other tools such as library publications, contests, brochures, direct mail, Web 2.0 applications and displays ([Fisher and Pride, 2006](#fis06); [Mathews, 2009](#mat09); [Webreck Alman, 2007](#web07)). A good Website helps to bring services and resources together in a unique way, because it is a direct link between the library and its specific users (for example students and academics) and the services it is seeking to promote. It also provides a channel for communication with target clients. Some media can be cross functional; for example, traditional tools, such as flyers, brochures and posters, can be used to promote events and programmes, which are promotional tools in themselves. Newsletters can introduce new developments as well as highlight current services. Target audiences can be easily and effectively targeted through e-mail lists and the Internet. These services are cost effective, as they require little investment in resources and reach the intended client directly.

This study examined how Australian academic librarians perceive techniques used to promote their services and resources and the factors influencing their perceptions of the particular approaches used, to provide a better understanding of their attitudes and views towards these techniques. Librarians may use the results to reflect on the effectiveness of these techniques, to balance the weight of the factors' influences and the better to understand various promotion techniques. This will enable them to promote academic library services and resources more effectively in the future.

### Literature review

Information technologies change rapidly. Information products and services are in a multiplicity of formats in libraries. For libraries and information services to stay viable in the current climate, it is important that they adopt marketing strategies to help meet organizational mission,goals and objectives. Marketing has long been associated with the selling of a product in order to make a profit, but was extended to the non-profit sector including libraries in the 1960s ([Enache, 2008](#ena08), p. 477).

For libraries, marketing is about a set of activities including understanding client needs, determining market niches, identifying products and services, building client relationships and creating '_marketing mix_' ([de Saez, 2002](#des02); [Potter, 2012](#pot12); [Rowley, 2003](#row03); [Welch, 2006](#wel06)). Unlike traditional marketing that is organization-focused and for a specific product, libraries and information agencies are client-focused organizations focusing on clients and meeting their needs, and need to adopt services marketing ([Welch, 2006](#wel06), p. 14). . Services are the intangible products that libraries now have to promote in order to compete with external competitors. Libraries face numerous challenges such as restricted funding and increasing user expectations, as they identify, develop, deliver and monitor service offerings that are superior to their competitors. However, these challenges offer opportunities to provide better services for users by redefining customer relationships through the use of marketing strategies to build and strengthen ongoing relationships with customers ([Rowley, 2003](#row03), p. 14). Relationship marketing is relevant to services marketing and creates much value because it builds a viable and long-term relationship with the clients that seek to use resources and services so that the customers are retained ([Rowley, 2003](#row03)).

The marketing mix refers to a set of variables that can be used by a library to promote its services and resources to users ([de Saez, 2002](#des02); [Lancaster and Reynolds, 1995](#lan95); [Welch, 2006](#wel06)). The marketing mix is traditionally referred to as the 4 Ps: price, product, promotion and place; however the fifth P, people, is now commonly included. Although the marketing mix was developed for imparting the advantages of a tangible product, with the focus on product marketing, the literature agrees on the importance of applying this focus to service promotion. As the need for promotion of services has grown and is now more widely recognised, the marketing mix has been refined and adapted to include services, not just products ([Mollel, 2013](#mol13)). One of the key marketing mix strategies is effective promotion.

A comprehensive literature search has shown increasing interest in the necessity of appropriately promoting library services and resources, as well as the critical need to do this to maintain visibility.The literature agrees that marketing and promotion are often used interchangeably; however, they are quite different, with promotion being a subset of marketing, as outlined above in the marketing mix ([Mollel, 2013](#mol13); [Germano 2010](#ger10); [Seddon, 1990](#sed90)). Promotion, in a library setting, can therefore refer to the methods used to provide information to users about the library's services and resources, ensuring that users are aware of the services and resources available. These methods include approaches such as print and online advertising (for example bookmarks, brochures, booklets, mailouts and Website announcements), electronic methods (for example e-mail, digital signage, screen-savers and alerts), and merchandise for giveaways and static marketing collateral (for example, pens, fridge magnets and water bottles).

A variety of techniques has been used to promote library services and resources. Nkanga ([2002](#nka02), p. 309) found that promotional techniques such as personal contacts, circulars, memos, telephone calls, meetings, direct mailing, displays, talks, newsletters, library tours and leaflets were widely used. The tools used for promotion were reviewed and the promotion activities of the studied department's information products were described by Cummings ([1994](#cum94)). Many forms of techniques such as a combination of outreach programmes, holding lectures, changing library exhibits, library tours, classroom instruction, one-on-one appointments, library homepage, online catalogue and one-on-one training were suggested to promote library services and resources ([Dodsworth, 1998](#dod98), p. 321-332). Bhat ([1998](#bha98)) outlined the marketing approaches used in the British Council Network in India and found that techniques such as mailouts, personal visits, presentation at institutions, brochures, leaflets, newsletters, extension activities, cultivating the press and media advertisements were used to promote library and information services in order to increase the customer base (pp. 32-33). Jackson ([2001](#jac01), p. 46) considered how to promote the interfaces, how to choose the right time to promote the service, and found that a variety of promotional methods such as business cards, leaflets, e-mails, letters, newsletters, personal contact, meetings, phone calls and interactive presentation were used to attract users. Ashcroft ([2002](#ash02)) examined the promotion and evaluation of electronic journals in academic library collection and found that the methods of promoting e-journals included seminars for students, A-Z list of e-journal titles shown on the library web pages, leaflets, advertisements, and e-mail alerts (p. 151). Adeloye ([2003](#ade03), p. 17) presented a number of practical ideas such as the use of promotional techniques including brochures, library guides and exhibitions. The internet was used to promote library services and the techniques used to promote library Website were circulation expire alert, live digital reference desk, cross search and library portals ([Ju, 2006](#ju06), p. 341-342).

To keep pace with evolving information technologies, librarians use a group of software applications including blogs, wikis and podcasting, media-sharing tools such as YouTube and Flickr, and social networking services such as Twitter and Facebook ([Hinchliffe and Leon, 2011](#hin11); [Moulaison and Corrado, 2011](#mou11); [Yi, 2014](#yi14)) to market their services and resources with mixed success. Blogs and wikis, as well as social networking and information sharing sites such as Facebook, Flickr and YouTube, create new types of content. Information professionals use tools such as RSS (Really Simple Syndication), tagging and bookmarking as a means of promotion. YouTube is a video sharing site which allows people around the world to communicate and interact, making it a distribution point for user-created content ([YouTube, 2015](#you15)). Kho's paper ([2011](#kho11)), which explores social media use for customer engagement, substantiates the successful use of YouTube to market the library's collection. YouTube also enables users to embed videos in other Web 2.0 tools, such as Facebook, blogs and wikis. Flickr is a photo sharing website which allows users to store, sort, search and post photographs and to create discussion groups. Besides posting materials for promotion purposes, information professionals can post photos of the organization and staff to provide a virtual tour of their agency. Twitter is social networking tool that is becoming increasingly popular, because of its ability to showcase interesting events, features or news in 140 characters or less. Several papers have explored how Twitter is being used in libraries to market their services and resources by actually using the social networking site to provide the service ([Fields, 2010](#fie10); [Milstein, 2009](#mil09); [Rodzvilla, 2010](#rod10); [Stuart 2010](#stu10)). A favorite networking site for people of all ages to keep in touch is Facebook. A broad study by Hendrix, Chiarella, Hasman, Murphy and Zafron ([2009](#hen09)) on the use of Facebook by more than seventy librarians found that most libraries used the social networking site to predominantly market their services. A study by Garcia-Milian, Norton and Tennant ([2012](#gar12)) suggests that the more _likes_ a library's Facebook page has, the greater the potential for engagement with users through this medium.

These techniques continue to be used, and in addition, the literature reports on various approaches taken to promoting library services and resources, with mixed results on effectiveness. For example, Adeyoyin ([2005](#ade05)) and Germano ([2010](#ger10)) both believe that comprehensive marketing campaigns are required, used in conjunction with promotional materials; however, they did not go into details about the types of approaches that can be used, or the inherent success factor of such approaches. Campbell and Gibson ([2005](#cam05)) conducted a study confirming this: a year long marketing plan using any of the promotional approaches listed earlier had success in raising awareness of the library, however lack of a library-wide plan coordinating all promotional efforts created a disjointed effort, which had an impact on further achievements. A more recent survey of academic librarians on using many of the approaches listed was conducted (Polger and Okamoto, 2013), however, the authors do not explain which promotion technique was effective and what factors influenced the techniques used. Further studies of international academic libraries found that users were not being updated regularly, despite the knowledge that improving awareness of library services and resources can lead to increased use ([Garoufallou, Zafeiriou, Siatri and Balapanidou 2013](#gar13); [Kaba, 2011](#kab11)).

Increases in circulation, account registrations and e-book access became apparent after the creation of displays, physical signs and use of the library Website to promote resources (<a href="">Jones, McCandless, Kiblinger, Giles and McCabe, 2011</a>; [McGeachin and Ramirez, 2005](#mcg05)). Empey and Black ([2005](#emp05)), although unable to determine heightened awareness resulting from their efforts, used anecdotal evidence to assess positive changes in user attitudes. The perception is that these small changes made significant improvements in staff and user engagement, as well as increased use statistics.

A common theme that emerges with the use of promotional materials and various approaches is thate-mail, e-mail alerts (distinct from e-mails due to their short or thematic nature), signage (both print and digital) and use of library Websites (announcements and screensavers) are the techniques that librarians mostly use for promotion in the libraries.This has been mainly due to relatively low investment of both funding and staff time ([Jones _et al._, 2011](#jon11); [Kratzert, Richey and Wasserman, 2001](#kra01); [Verostek, 2005](#ver05); [Vilelle, 2005](#vil05)). The literature is compelling in its discussion regarding the importance of promoting library services and resources in order to maintain visibility in competitive environments. However, effectiveness of promotional tools used has not been studied in great depth, particularly in relation to how effective Australian academic librarians find these techniques.

## Study framework and hypotheses

This study is focused on the relationship between the effective techniques that Australian academic librarians used to promote library services and resources and three kinds of predictors: demographics, human capital, and library variables. Demographics refer to age and sex. Human capital is '_an amalgam of factors such as education, experience, training, intelligence, energy, work habits, trustworthiness and initiative that affect the value of a worker's marginal product_' ([Frank and Bernanke, 2007](#fra07)). For this study, human capital consists of years at present position, education level, number of different positions, years of service, the formal study of marketing and attendance at a marketing workshop in the last five years. Library variables are made up of the number of library branches, staff, and clients

The results of a pilot study ([Yi, Lodge and McCausland, 2013](#yi13)) demonstrated that education level, years in present positions, and number of library branches were significant predictors, while other independent variables were not significant. It was hypothesised that there are significant relationships between education levels, years in present positions or number of library branches and the techniques used to promote library services and resources. It was also hypothesised that there are no significant relationships between the techniques used and the predictors except for education level, years in present positions and number of library branches. One of the purposes of this study is to test the above hypotheses using ordinal regressions.

## Methods

### Population and sample

The research respondents of this study were recruited from libraries in the thirty-seven Australian universities. According to the 2011 statistics of the Council of Australian University Librarians, there were 1,470 professional staff in these universities. After obtaining seven hundred academic librarians' names and e-mail addresses from Websites, random sampling was used to recruit the respondents. Four hundred randomly selected respondents were invited to complete an online survey. Following a pilot study, four hundred librarians were sent a survey by an introductory e-mail to complete and return within fifteen days. In order to obtain more responses, they were also sent reminder e-mails. All participation was strictly voluntary. During the study, all of the respondents worked and lived in Australia. The number of librarians who completed and returned surveys was 230 (57.5%).

### Survey design

In the first section, survey questions focused on three kinds of predictors: demographics (sex and age); human capital (education level, years at present position, years of service, number of different positions, formal study of marketing and workshop attendance on marketing in the last five years); and library variables (number of library branches, staff, and patrons). One of the main survey questions asked how effective the techniques used to promote library services and resources were, as perceived by the respondents. The techniques and approaches listed included:

*   digital media (catalogues, e-mails, library Website, online advertising, social media, Webcasts and Website announcements);
*   print materials (advertisements, booklets, brochures, direct mail, flyers, giveaways, leaflets, newsletters, print advertising and published guides); and
*   events (classroom instruction, exhibits or displays, face-to-face events, library tours, one-to-one conversations, open houses, phone, training sessions and workshops).

For the _Other_ option, respondents could write their free comments on techniques they felt were effective in promoting services and resources.

### Variables and measurements

The dependent variables were the techniques and approaches listed in the _Survey design_ section. Dependent variables were measured using ordinal variables with the rating scales: _ineffective_, _somewhat effective_, _effective, more effective_ and _most effective_. Demographics, human capital, and library variables were three kinds of independent variables.

#### Data analysis and analytical strategies

Descriptive content analysis ([Sarantakos, 2005](#sar05)) was used to analyse the data collected from open-ended responses. Descriptive statistics (percentages) and inferential statistics (ordinal regression) were used to analyse the quantitative data. The main method of this study’s analysis was ordinal regression, which was used to determine the relationships between a dependent variable with multiple categories and more than two independent variables. Dependent variables should be ordinal ones. However, independent variables can be categorical and continuous variables.

In the tables, thresholds only stand for the response variables in the ordered regressions and the threshold coefficients are not usually explained in the findings. Log likelihood determines whether the independent variables included in a model offer the date a good fit. -2 log likelihood is '_the product of -2 and the log likelihood of the null model and fitted final mode_l' and '_the likelihood of the model is used to test of whether all predictors’ regression coefficients in the model are simultaneously zero and in tests of nested models_' ([University..., 2016](#uni16)). Model c2 is used to test statistical significance between the dependent variable and a set of predictors. Pseudo R<sup>2</sup> is '_a measure used in logistic and probit regression to represent the proportion of error variance controlled by the model_' ([Vogt, 2005](#vog05), p. 252).

The asterisks indicate the significance level of these results, with a single asterisk marking results significant to the 0.05 level (meaning the probability of the value occurring by chance was less than or equal to 5%), a double asterisk marking results significant to the 0.01 level (meaning the probability of the value occurring by chance was less than or equal 1%), and a triple asterisk marking results significant to the 0.001 level (meaning the probability of the value occurring by chance was less than or equal to 0.1%).

## Findings

In this study, 71.7% (165) of 230 respondents successfully answered the question on the effectiveness of techniques used to promote library services and resources. The final analysis did not include sixty-five incomplete questionnaires.

### Descriptive results

Figure 1 below shows the descriptive results of the techniques used to promote library services and resources.

In terms of which techniques used were considered effective, 42.4% thought that e-mails were _effective_. Twenty-nine point one percent of respondents thought that library Website was an _effective_ promotion technique, with 39.4% of respondents perceiving that library Website was a _more effective_ technique, and 19.4% a _most effective_ technique. Thirty-nine point four percent of respondents perceived that online advertising was an _effective_ promotion technique, with 38.8% of respondents thinking that use of social media was an _effective_ technique, 25.5% _more effective_, and 10.3 % _most effective_. Webcasts were perceived as _effective_ by 33.3% of respondents Thirty-two point one of respondents perceived that Website announcements were _effective_ and 32.7 % of respondents reported that Website announcements were _more effective_. However, only 6.7 % of respondents thought that Website announcements were _most effective_.

Advertisements were perceived as _effective_ by 38.8% of respondents. Brochures were reported as _effective by 41.2% of respondents._ Flyers were perceived to be _effectively_ and _more effectively_ used to promote library services and resources by 34.5% and 14.5% of respondents. Thirty-two point seven per cent of respondents thought that giveaways were _effective_ and 25.5% of respondents thought that giveaways were a _more_ _effective_ technique. Newsletters were thought _effective_ by 34.5%, and 32.1% respondents thought that print advertising was _effective_. Published guides were perceived to be _effectively_ and _more effectively_ used to promote library services and resources by 33.9% and 13.9% of respondents.

Classroom instruction was seen as an _effective_ technique by 26.7% of respondents, 40.6% a more _effective_ technique, and 14.5% a _most effective_ technique. Forty-two point four percent of respondents perceived that exhibits or displays were thought an _effective_ promotion technique by 42.4% of respondents and 41.2% perceived face-to-face events as _effective_, 30.9% _more effective_, and 10.3% _most effective_. Library tours were thought an _effective_ technique by 36.4%, 30.3% thought that library tours were _more effective_ and 7.9% of respondents reported that library tours were _most effective_. Twenty-seven point three percent of respondents thought that One-to-one conversations were seen as _effective_ by 27.3%, _more effective_ by 31.5%, and _most effective_ by 27.3% of respondents. Training sessions were seen as _effective_ by 29.1%, _more effective_ by 46.7% and _most effective_ by 13.9% of respondents. Workshops were perceived to be _effectively_, _more effectively_ and _most effectively_ used to promote library services and resources by 30.3%, 39.4% and 12.1% of respondents, respectively.

<figure class="centre">![Figure 1: Percentage effectiveness of the techniques used](p702fig1.png)

<figcaption>  
Figure 1: Percentage effectiveness of the techniques used to promote services and resources</figcaption>

</figure>

Booklets and newsletters were perceived to be somewhat effectively used to promote services and resources. Direct mail was perceived to be ineffective by 36.4% of respondents. The techniques of events such as one-to-one conversations, classroom instruction, training sessions, face-to-face events and workshops and digital media such as library website and social media were perceived to be perceived to be most effectively used to promote services and resources. All the techniques were perceived to be used by all respondents. Thirteen free comments did not include a true _Other_ promotion technique.

### Dependent and independent variables

Table 1 shows the percentages, medians and ranges of the variables. The dependent variables were the techniques used to promote library services and resources. They were ordinal variables. Sex, formally studying marketing and attending a workshop on marketing in the last five years were nominal variables. The ordinal variables included age and education level. Age is an ordinal variable because age comprised ten categories (24 or under, 25-29 years, 30-34 years, and on up to > 64 years). The other independent variables were continuous.

<table class="center" style="width:auto;"><caption>  
Table 1: Descriptive statistics of variables used in the analysis</caption>

<tbody>

<tr>

<th>Variables</th>

<th>Median</th>

<th>SDev</th>

<th> </th>

<th>Median</th>

<th>SDev</th>

</tr>

<tr>

<td colspan="6">**_Dependent variables_**</td>

</tr>

<tr>

<td>Catalogues</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

<td>E-mails</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Library Website</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

<td>Online advertising</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Social media</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

<td>Webcasts</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Website announcements</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

<td>Advertisements</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Booklets</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

<td>Brochures</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Direct mail</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

<td>Flyers</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Giveaways</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

<td>Leaflets</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Newsletters</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

<td>Print advertising</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Published guides</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

<td>Classroom instruction</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Exhibits or displays</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

<td>Face-to-face events</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Library tours</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

<td>One-to-one conversations</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Open houses</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

<td>Phone</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Training sessions</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

<td>Workshops</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td colspan="6">_**Independent variables**_</td>

</tr>

<tr>

<td>Male</td>

<td style="text-align:center;">27.3%</td>

<td style="text-align:center;">—</td>

<td>Age (10-point scale)</td>

<td style="text-align:center;">7</td>

<td style="text-align:center;">9</td>

</tr>

<tr>

<td>Education level</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">5</td>

<td>Years in present position</td>

<td style="text-align:center;">6.4</td>

<td style="text-align:center;">5.5</td>

</tr>

<tr>

<td>Years involved in all library services</td>

<td style="text-align:center;">21.6</td>

<td style="text-align:center;">10.9</td>

<td>Number of different library professional positions</td>

<td style="text-align:center;">5.7</td>

<td style="text-align:center;">3.5</td>

</tr>

<tr>

<td>Number of staff</td>

<td style="text-align:center;">98.4</td>

<td style="text-align:center;">61.9</td>

<td>Number of library branches</td>

<td style="text-align:center;">4.7</td>

<td style="text-align:center;">3.2</td>

</tr>

<tr>

<td>Number of total population</td>

<td style="text-align:center;">30236.9</td>

<td style="text-align:center;">17859.1</td>

<td>Formally studying marketing</td>

<td style="text-align:center;">15.2%</td>

<td style="text-align:center;">—</td>

</tr>

<tr>

<td>Attended a workshop on marketing in the last five years</td>

<td style="text-align:center;">35.8%</td>

<td style="text-align:center;">—</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

### Results of ordinal regressions

Tables 2, 3 and 4 below show the ordinal regression estimates predicting the effective approaches used to promote services and resources. Three tables are used because there are twenty-six independent variables used in the analysis and the split is to enable the readers to understand the results as clearly as possible. The results are reported according to three groups of techniques used to promote library services and resources: Table 2 covers digital media (catalogues, e-mails, library Website, online advertising, social media, Webcasts and Website announcements); Table 3 concerns print materials (advertisements, booklets, brochures, direct mail, flyers, giveaways, leaflets, newsletters, print advertising and published guides); and Table 4 covers events (classroom instruction, exhibits or displays, face-to-face events, library tours, one-to-one conversations, open houses, phone, training sessions and workshops).

The results show that independent variables (number of staff, attending a workshop on marketing in the last five years, number of library branches, formally studying marketing, number of total patrons, age, sex, years of present position, and education level) significantly influenced the predictors of this study. These results may come from respondents who had the characteristics of these significant predictors and were more or less likely to perceive that their chosen promotional techniques were most effective to their given situations.

As demonstrated in Table 2 below (click for a large image), the predictor of formally studying marketing was detected to be positively and very significantly related to the e-mail technique used. Those who formally studied marketing had a higher likelihood than their counterparts to respond that e-mails were most effective. This is not consistent with the hypothesis. This might imply that librarians formally studying marketing could use these tools further to promote their services and resources and were therefore more receptive to using these tools. <span>[![Table 2](p702tab2thmb.png)](p702tab2.png)</span>The predictors of years of present position and formally studying marketing were detected to be positively and significantly related to the use of online advertising. Those with more years of present position or formally studying marketing were more likely than their counterparts to respond that online advertising was a most effective marketing technique. It might suggest that librarians with more years of present position or formally studying marketing could use this technique more and get more effective results of this technique used. Men were significantly and negatively related to the use of Webcasts. Male librarians were less likely than female librarians to perceive that Webcasts were most effective. This does not support the second hypothesis. It implies that male librarians could get more effective results of using other promotion techniques and female librarians could use these techniques and obtain more effective promotion results.

The relationship between number of library branches and Website announcements was detected to be significant and positive. Those who worked in libraries with more branches were more likely than their counterparts to respond that Website announcements were a most effective promotion technique. This is consistent with the first hypothesis. It suggests that librarians who worked in libraries with more branches used this technique more and the promotion results of using this technique might be more effective.

Table 3 below (click for a large image) shows that the results demonstrate that the independent variables of age, education level, years of present position, number of staff, number of library branches, formally studying marketing and attending a workshop on marketing in the last five years significantly influenced the predictors. Age was detected to be significantly and negatively related to the giveaways used. Those who were older were less likely than their counterparts to perceive that giveaways were a most effective promotion technique. This does not support the second hypothesis. It might imply that younger librarians may get more effective results while using giveaways to promote services and resources. Age was also detected to be significantly and negatively related with print advertising used. <span>[![Table 3](p702tab3thmb.png)](p702tab3.png)</span>Those who were older were less likely than younger respondents to perceive that print advertising was a most effective promotion technique. This does not support the second hypothesis. It implies that younger librarians could get more effective results by using this technique.

There was a significant and negative relationship between education level and the use of published guides. Those with more education were less likely than their counterparts to respond that published guides were a most effective promotion technique. This supports the first hypothesis. It might imply that librarians with more education could use a variety of other promotion techniques and get more effective results.

Number of staff was detected to be positively and significantly associated with the probability of booklets being used. Librarians who worked in libraries with more staff were more likely than their counterparts to report that booklets were most effectively used to promote services and resources. This rejects the hypothesis. The relationship between booklets and attending a workshop on marketing in the last five years was detected to be significant and negative. Those who attended more workshops on marketing in the last five years were less likely than their counterparts to respond that booklets were most effective promotion technique. This is not consistent with the hypothesis. It may suggest that the librarians attending the marketing workshops may be more receptive to using other techniques to promote services and resources.

The relationships between years in present position or number of library branches and giveaways used were detected to be significant and positive. Those who worked at present position for more years or in the libraries with more branches were more likely than their counterparts to perceive that giveaways were most effective. This is consistent with the first hypothesis. It implies that librarians with more years of present position or with more branches might use giveaways more and obtain added effective promotion results. There was a significant and positive relationship between formally studying marketing and leaflets used. Those who formally studied marketing were more likely than their counterparts to respond that leaflets were most effective. This does not support the second hypothesis. It implies that those who formally studied marketing might obtain more effective results while using leaflets to promote services and resources.

Table 4 below (click for large image) demonstrates that the predictors of number of staff, number of library branches, number of total patrons, formally studying marketing and attending a workshop on marketing in the last five years were significant factors influencing respondents' perceptions of effective techniques used to promote services and resources.

<span>[![table 4](p702tab4thmb.png)](p702tab4.png)</span>

Number of staff and number of total clients were detected to be significantly and positively related with open houses used. Those who worked in libraries with more staff and clients were more likely than their counterparts to perceive that open houses were most effective. This does not support the second hypothesis. It implies that librarians with more staff and clients could get more effective results of using this technique.

A positive and significant relationship between number of library branches and classroom instruction was detected. Those who worked in libraries with more branches were more likely than their counterparts to respond that classroom instruction was a most effective promotion technique. This is consistent with the first hypothesis. The relationships between number of total patrons and exhibits or displays were significant and negative. Those who worked in libraries with more patrons were less likely than their counterparts to perceive that exhibits or displays were most effective techniques. This does not support the second hypothesis. It suggests that librarians with more patrons might use a variety of effective techniques while promoting services and resources.

A positive and significant relationship between formally studying marketing and face-to-face events was detected. Those who formally studied marketing were more likely than their counterparts to respond that face-to-face events were a most effective promotion technique. This is not consistent with the second hypothesis. It may indicate that face-to-face events might be more effectively used in libraries with staff who formally studied marketing.

There was a significant and positive relationship between attending a workshop on marketing in the last five years and phone use was detected to be significant and positive. Those who attended more workshops on marketing in the last five years were more likely than their counterparts to respond that phone was a most effective promotion technique. This is not consistent with the second hypothesis. It suggests that librarians who did not attend more workshops in the last five years might not use this technique more and the promotion results of using this technique might not be more effective.

## Discussion

Currently, academic libraries face numerous challenges such as ongoing budget cuts and the changing demands of learning, teaching and research. They have undergone a comprehensive and wide-reaching transformation in how they create, use and maintain scholarly materials, and have become providers of electronic resources as well as places in which physical collections can be accessed and used. This study has confirmed that rigorous and disciplined marketing and promotion approaches which will ensure that scarce resources and services match customer needs for the greatest impact ([Roberts and Rowley, 2004](#rob04), p.128) have been adopted in the libraries. These systematic approaches will improve visibility of the library, which will help to promote the value of the libraries to their parent organizations. This study also confirmed that the effective ways to promote library services and resources to clients have been widely used in order to attract clients, generate non-user awareness and raise awareness of available services and resources, and demographics, human capital and library variables play a significant role in academic librarians' perceptions of effective promotion techniques used.

The descriptive results of this study show that librarians perceived that most of tools in three categories of techniques (digital media, print materials and events) were effectively, more effectively or most effectively used to promote library services and resources, and that librarians in practice used a variety of effective techniques.

The high percentages of digital media techniques used indicate that library Websites and social media were the most effective promotion techniques. These findings reflect the points made in the studies by Garoufallou _et al._ ([2013](#gar13)), Khan and Bhatti ([2012](#kha12)), and Vasileiou and Rowley ([2011](#vas11)). A library Website is a representation of the library and an access portal to services, resources, and demonstrates the values of the library. It promotes easy to follow tabs to the library customer questions, allows users to have access to quick information via links to accessible information and caters for multi-levels of users. Earlier research by Fang ([2007](#fan07)) noted that Google analytics showed that the library Website was regularly used and received numerous hits.

The high percentages for the social media techniques used indicate that the era when libraries were considered to be only a depository of information has passed and current libraries as interactive hubs where users gather to seek and share information and find entertainment have come into being. Social media (including Facebook, Twitter and YouTube) as the tools, platforms and applications that enable customers to connect, communicate and collaborate with others online are now often used by libraries in their promotion campaigns because social media can support user-generated content that can be distributed among the participants to view, share, and improve. As found by Polger and Okamoto ([2013](#pol13), p. 250), '_a large percentage of libraries (70%) are using social media to reach library users and non-users_'.

With regard to the event promotion techniques used, the high percentages demonstrate that one-to-one conversations, classroom instruction, training sessions, workshops and face-to-face events were the most effective promotion techniques. These promotion techniques used suggest the importance and effectiveness of 'human touch' and individual and group interactions. Academic libraries are expanding their provision of online information resources at an escalating rate. E-books, e-journals and streaming videos are replacing print books, journals and DVDs. Changes and innovations in accessibility of library services and resources necessitate the provision of more one-to-one conversations, classroom instruction, training sessions, workshops and face-to-face events for library users to know how to access and use services and resources effectively in their learning, teaching, or research.

Combining the determinants of demographics and human capital, the statistically significant factors influencing librarians' perceptions of the effective promotion techniques used were arrived at in this study. The significant factors were age, gender, education level, years of present position, formally studying marketing and attending a workshop on marketing in the last five years. This conceptual model also identified library characteristics associated with the effective techniques used. The likelihood that a promotion technique is most effective was significantly determined by number of staff, number of library branches and number of total patrons.

This study indicates that the use of Webcasts was significantly impacted by gender and the predictor of age significantly impacted giveaways and print advertising used. The significant impact of education level on the techniques used was confined to published guides used. The use of giveaways and online advertising was significantly impacted by years of present position. The predictor of formally studying marketing significantly impacted the use of e-mail, leaflets, online advertising and face-to-face events. The use of booklets and phone was significantly impacted by the predictor of attending a workshop on marketing in the last five years. Number of library branches had a significant impact on the use of classroom instruction, giveaways and Website announcements. Number of staff significantly impacted the use of booklets and open houses. The use of exhibits or displays and open houses was significantly impacted by number of total clients. These findings reveal a significant impact of demographics, human capital and library characteristics on the likelihood of effective promotion techniques used and suggest that, in practice, more techniques can be considered to help attain the effectiveness of promotion techniques used.

The results will allow a better understanding of librarians' perceptions of effective techniques used to promote services and resources. Librarians may use these results to reflect on various choices of promotion strategies and to balance the weight of these influences. While promoting services and resources, librarians need to have a good understanding of their own demographic characteristics and human capital. In general, this will give them a better idea of what techniques they would be more likely to use in a given situation. For instance, those who formally studied marketing were found to have a higher likelihood than their counterparts to respond that e-mails, leaflets, online advertising and face-to-face events were most effective promotion techniques. This implies that librarians formally studying marketing might obtain more effective results while using these techniques, could use these techniques further, and were more receptive to using these tools. Those who did not formally study marketing could consider the effectiveness of these techniques used while promoting services and resources and attend the workshops on how to use these techniques more effectively. Therefore, it is important for them to know the range of approaches, the approaches that are likely to be successful, approaches that best suit their libraries and the factors influencing the techniques used. Their choices of effective techniques depend on their own decisions and situations. Reviewing the characteristics of the situation and their favoured techniques will help librarians adjust promotion behaviours to meet the needs of the situation.

The findings of this study will help librarians analyse what library characteristics there are in their libraries, reflect on different options of promotion techniques, and balance the weight of library factors that significantly influence the techniques used, as shown by both quantitative and qualitative data analyses. The effective promotion techniques chosen depend on the situation. As the situation evolves, so should the techniques used. If students of library and information science are educated in these techniques and how to appropriately use them, they will have a better understanding of library promotion. This knowledge will enable them to understand various promotion techniques. The classes may be given in schools. Librarians may have workshops on this for future librarians.

This study further confirm the finding obtained in the pilot study by Yi _et al._ (2013, p. 593) that the predictors of education level, years of present positions and number of library branches were significant.

## Conclusion

This exploratory study contributes to the body of knowledge about various techniques used to promote library services and resources and the library marketing literature by examining in great depth the effectiveness of promotional tools used and the factors influencing the effective promotion techniques used. It compares the current findings with those of existing empirical studies of library promotion techniques used and regards the roles of demographics, human capital and library characteristics. It has practical implications for how to effectively promote future services and resources and contributes to future researchers wanting to explore library promotional techniques.

This study found that demographics, human capital and library variables played a significant role in academic librarians' perceptions of effective promotion techniques used. Demographic variables such as age and sex, human capital variables such as education level, years in present position, formally studying marketing and attending a workshop on marketing in the last five years and library variables such as number of staff, number of library branches and number of total patrons were significant predictors of perceptions of the effectiveness of the use of promotion techniques, but this study indicates that other independent variables such as number of different library professional positions and years involved in all library services made no difference.

This study has demonstrated that librarians actually use a variety of effective techniques to promote services and resources and provided examples of how demographics, human capital, and library characteristics relate to the perceptions of effective promotion techniques used. All information professionals can benefit from knowing how demographics, human capital, and library characteristics influence librarians' perceptions of the effective techniques used. For example, information professionals might reflect on how they generally promote services and resources through these techniques. This reflection may also bring the understanding that this casual interaction may still lead positive interactions. How to use effective techniques to promote services and resources may be taught in schools as well as in work places. Schools of library and information studies may offer specific modules related to librarians' perceptions of effective promotion techniques used to better prepare students as effective marketers in the future, while libraries or professional associations may provide related training programmes and workshops for information professionals to further their learning.

There were some limitations for this study. Firstly, the survey results may differ from the views of independent observers. Accurate responses were also dependent on respondent willingness and ability and there was potential for misinterpretation of survey questions as well as personal bias. Finally, data were collected from librarians perceiving effective techniques used to promote services and resources at only one point in time. Accordingly, the results of the study might lack generalisability.

Academic librarians are facing a variety of challenges and obstacles such as '_juggling many responsibilities at once_', '_lack of funding/budgetary issues_', '_lack of time_' and '_lack of staffing/resources_' ([Polger and Okamoto, 2013](#pol13), p. 247). To meet challenges, overcome obstacles and win over competitors, librarians play a key role in effectively promoting services and resources and, as such, the role of academic librarians is crucial to ensure this happens. This study confirmed that some librarians promoted services and resources using a variety of effective promotion techniques such as advertisements, face-to-face events, giveaways, library Website, and library tours, while others used different, but still effective, promotion techniques such as social media, workshops, and published guides to promote services and resources. Future research will focus on how often librarians use these techniques and other effective techniques for promoting services and resources and the influencing factors, and on more promotion-related questions such as what to promote and how to evaluate promotional activities in the digital age.

## Acknowledgements

This paper would not have been possible without the time release provided by a Charles Sturt University Faculty of Education Small Grant.

## About the author

Zhixian (George) Yi is a lecturer in the School of Information Studies at Charles Sturt University (CSU), Australia. He received a doctorate in information and library sciences and a PhD minor in educational leadership from Texas Woman's University, United States. He was awarded his MLS from Southern Connecticut State University, United States. His research interests concentrate on three main areas: management and leadership, marketing techniques, and information use and needs. He can be contacted at [gyi@csu.edu.au](mailto:gyi@csu.edu.au)

#### References

*   Adeloye, A. (2003). How to market yourself and your library organization: a solo librarian's guide. _The Bottom Line, 16_(1), 15-18.
*   Adeyoyin, S.A. (2005). Strategic planning for marketing library services. _Library Management, 26_(8/9), 494-507.
*   Ashcroft, L. (2002). Issues in developing, managing and marketing electronic journals collections. _Collection Building, 21_(4), 147-154.
*   Bhat, M. I. (1998). [Marketing of library and information services at British Council library network in India](http://www.webcitation.org/6fLL3QDFI). _DESlDOC Bulletin of lnformation Technology, 18_(3), 29-33\. Retrieved from http://www.gndec.ac.in/~librarian/sveri/dbit1803029.pdf (Archived by WebCite® at http://www.webcitation.org/6fLL3QDFI)
*   Campbell, J., & Gibson, S. (2005). Implementing an action plan. _College & Undergraduate Libraries, 12_(1/2), 153-164.
*   Chartered Institute of Marketing. (2009). Marketing and the 7 Ps: a brief summary of marketing and how it works. Maindenhead, UK: Chartered Institute of Marketing. Retrieved from http://www.cim.co.uk/files/7ps.pdf
*   Cummings, S. (1994). Marketing and promotion of information products. _Quarterly Bulletin of the International Association of Agricultural Information Specialists, 39_(1/2), 69-75.
*   de Saez, E.E. (2002). _Marketing concepts for libraries and information services_ (2nd ed.). London: Facet Publishing.
*   Dodsworth, E. (1998). Marketing academic libraries: a necessary plan. _Journal of Academic Librarianship, 24_(4), 320-322.
*   Empey, H., & Black, N. E. (2005). Marketing the academic library. _College & Undergraduate Libraries, 12_(1/2), 19-33.
*   Enache, I. (2008). The theoretical fundamentals of library marketing. _Philobiblon, 13_, 477-490.
*   Fang, W. (2007). Using Google analytics for improving library Website content and design: a case study. _Library Philosophy and Practice, 9_(2). Retrieved from http://unllib.unl.edu/LPP/fang.htm
*   Fields, E. (2010). A unique twitter use for reference services. _Library Hi Tech News, 27_(6), 14-15.
*   Fisher, P. H., & Pride, M. M. (2006). _Blueprint for your library marketing plan: a guide to help you survive and thrive_. Chicago, IL: American Library Association.
*   Frank, R. H. & Bernanke, B. S. (2007). _Principles of microeconomics_. New York, NY: McGraw-Hill/Irwin.
*   Garcia-Milian, R., Norton, H., & Tennant, M. (2012). The presence of academic health sciences libraries on Facebook: the relationship between content and library popularity. _Medical Reference Services Quarterly, 31_(2).
*   Garoufallou, E., Zafeiriou, G., Siatri, R., & Balapanidou, E. (2013). Marketing applications in Greek academic library services. _Library Management, 34_(8/9), 632-649.
*   Germano, M. A. (2010). Narrative-based library marketing: selling your library's value during tough economic times. _The Bottom Line: Managing Library Finances, 23_(1), 5 - 17.
*   Helinsky, Z. (2008). _A short-cut to marketing the librar_y. Oxford: Chandos Publishing.
*   Hendrix, D., Chiarella, D., Hasman, L., Murphy, S. & Zafron, ML. (2009). Use of Facebook in academic health sciences libraries. _Journal of the Medical Library Association, 97_(1), 44-47.
*   Hinchliffe, L.J., & Leon, R. (2011). Innovation as a framework for adopting Web 2.0 marketing approaches. In D. Gupta & R. Savard. (Eds.), _Marketing libraries in a Web 2.0 world_ (pp. 58-65). Berlin: De Gruyter Saur.
*   Jackson, M. (2001). Marketing the HyLife project. _Library Management, 22_(1/2), 43-49.
*   Jones, D.Y., McCandless, M., Kiblinger, K., Giles, K., & McCabe, J. (2011). Simple marketing techniques and space planning to increase circulation. _Collection Management, 36_(2), 107-118.
*   Ju, W. (2006). Marketing and service promotion practices in the LCAS. _Library Management, 27_(6/7), 336-343.
*   Kaba, A. (2011). Marketing information resources and services on the web: current status of academic libraries in the United Arab Emirates. _Information Development, 27_(58), 58-65.
*   Khan, S. A., & Bhatti, R. (2012). [Application of social media in marketing of library andinformation services: a case study from Pakistan](http://www.webcitation.org/6fLKoMowZ). _Webology, 9_(1). Retrieved from http://www.webology.org/2012/v9n1/a93.html (Archived by WebCite® at http://www.webcitation.org/6fLKoMowZ)
*   Kho, N. D. (2011). Social media in libraries: keys to deeper engagement. _Information Today, 28_(6), 1, 31-32.
*   Kratzert, M., Richey, D., & Wasserman, C. (2001). Tips and snags of cyber-reference. _College and Undergraduate Libraries, 8_, 73-82.
*   Lancaster, G., & Reynolds P. (1995), _Marketing_. New York, NY: Heinemann.
*   McGeachin, R. B., & Ramirez, D. (2005). Collaborating with students to develop an advertising campaign. _College & Undergraduate Libraries, 12_(1/2), 139-152.
*   Mathews, B. (2009). _Marketing today's academic library: a bold new approach to communicating with students_. Chicago, IL: American Library Association.
*   Milstein, S. (2009). Twitter FOR libraries (and librarians). _Computers in Libraries, 29_(5), 17-18.
*   Mollel, M. M. (2013). Marketing mix for librarians and information professionals. _Infopreneurship Journal, 1_(1), 10-28.
*   Moulaison, H.L., & Corrado, E.M. (2011). Staying free from 'corporate marketing machines' library policy for Web 2.0 tools. In D. Gupta & R. Savard. (Eds.), _Marketing libraries in a Web 2.0 world_ (pp. 43-55). Berlin: De Gruyter Saur.
*   Nkanga, N. A. (2002). Marketing information services in Botswana: an exploratory study of selected information providing institutions in Gaborone. _Library Management, 23_(6/7), 302-313.
*   Polger, M. A., & Okamoto, K. (2013). Who's spinning the library? Responsibilities of academic librarians who promote. _Library Management, 34_(3), 236-253.
*   Potter, N. (2012). _The library marketing toolkit_.London: Facet Publishing.
*   Roberts, S., & Rowley, J. (2004). _Managing information services_. London: Facet Publishing.
*   Rodzvilla, J. (2010). New title tweets. _Computers in Libraries, 30_(5), 27-30.
*   Rowley, J. (2003). Information marketing: seven questions. _Library Management, 24_(1/2), 13-19.
*   Sarantakos, S. (2005). _Social research_. New York, NY: Palgrave Macmillan.
*   Seddon, S. (1990). Marketing library and information services. _Library Management, 11_(6), 35-39.
*   Stuart, D. (2010). What are libraries doing on twitter? _Online, 34_(1), 45-47.
*   University of California Los Angeles. _Institute for Digital Research and Education_. (2016). _[Annotated SPSS output. Ordered logistic regression](http://www.webcitation.org/6fSeUuNAb)_. Los Angeles, CA: University of California Los Angeles. Retrieved from http://www.ats.ucla.edu/stat/spss/output/ologit.htm (Archived by WebCite® at http://www.webcitation.org/6fSeUuNAb)
*   Vasileiou, M., & Rowley, J. (2011). Marketing and promotion of e-books in academic libraries. _Journal of Documentation, 67_(4), 624-643.
*   Verostek, J. M. (2005). Affordable, effective, and realistic marketing. _College & Undergraduate Libraries, 12_(1/2), 119-138.
*   Vilelle, L. (2005). Marketing virtual reference. _College & Undergraduate Libraries, 12_(1/2), 65-79.
*   Vogt, W. P. (2005). _Dictionary of statistics and methodology: a nontechnical guide for the social sciences_. (3rd ed.). Thousand Oaks, CA: Sage Publications.
*   Webreck Alman, S. (2007). _Crash course in marketing for libraries_. Westport, CT: Libraries Unlimited.
*   Welch, L. (2006). _The other 51 weeks: a marketing handbook for librarians_. Wagga Wagga, NSW, Australia: Centre for Information Studies, Charles Sturt University.
*   Yi, Z., Lodge, D., & McCausland, S. (2013). Australian academic librarians' perceptions of marketing services and resources. _Library Management, 34_(8/9), 585-602.
*   Yi, Z. (2014). Australian academic librarians' perceptions of effective Web 2.0 tools used to market services and resources. _The Journal of Academic Librarianship, 40_(3/4), 220-227.
*   YouTube, 2015\. _[About YouTube](http://www.webcitation.org/6fLJOqSK6)_. Retrieved from https://www.youtube.com/yt/about/ (Archived by WebCite® at http://www.webcitation.org/6fLJOqSK6)