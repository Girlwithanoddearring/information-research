#### vol. 21 no. 1, March, 2016

# Empathetic communication among discourse participants in virtual communities of people who suffer from mental illnesses

[Nava Rothschild](#author) and [Noa Aharony](#author)

#### Abstract

> **Introduction**. The current study attempts to explore the contribution of participation in closed Internet communities for people with mental illnesses as a channel for forming social relationships.  
> **Method**. A case study approach was adopted: the research population consisted of discussions that took place in two virtual communities for people with mental illnesses in Israel.  
> **Analysis**. Content analyses were carried out to revel the types of support that exchange between the participants in the discourse at the communities.  
> **Results**. There is no conclusive answer to the question as to whether social links that exist in computer mediated communication channels can fill people's diverse social needs. Results showed that many people try to form social relationships by means of participation in computer-mediated support communities. However, the large majority of participants in this discourse do so only in the context of crises they experience because of the mental disorder. A number of kinds of expressions of social support and empathetic processes were found in the virtual discourse and his nature is different from empathetic processes that exist in face-to-face communication.  
> **Conclusions**. Additional research needs to be done to suit the virtual communication channels to the social needs of socially excluded populations.

## Introduction

The World Health Organization ([2015](#wor15b)) defines mental disorders as: '_disorders that are generally characterised by a combination of abnormal thoughts, perceptions, emotions, behavior and relationships with others_'. According to the World Health Organization's estimate ([2015](#wor15)), approximately 25% of the population of the Western world will suffer from a mental disorder at least once in their lives and about 15% of the population suffer from a mental disorder at any given time. According to the same estimate, 1%–2% of the population suffer from severe, long-term mental illnesses that cause a significant decline in functioning and severe disability. The World Health Organization graded mental illnesses in a similar place to that of heart diseases and cancer in the index of the overall burden of diseases. Mental illnesses are also among the ten main factors that lead to disability in the world ([Aviram, 2006](#avi06); [Levy, 2010](#lev10); [Israel Ministry of Health, 2013](#isr13)).

Social relationships are important for people's mental welfare. The concept of social support is understood by most people intuitively as help from others in difficult life situations. Cobb ([1976](#cob76), p. 300) defines social support as the entirety of the social connections that individuals have with the social frameworks (people or groups) that advance their feeling of being loved and valued and members of a network of human connections and mutual obligations. The social network of individuals in the general population consists of approximately forty people, social networks of people with severe mental illness range in size from four to ten people. These social networks tend to be less reciprocal, to have fewer family members, and to consist of service providers such as group home staff and caseworkers ([Bradshaw and Haddock, 1998](#bra98); [Maya, Becker, McCrone and Thornicroft, 1998](#may98)). Perese and Wolf ([2005](#per05)) found that more than half of people with mental illnesses report isolation, compared to a third of the general population. In other studies a close link was found between mental disorders and feelings of isolation ([McManus, Meltzer, Brugha, Bebbington and Jenkins, 2009](#mcm09); [Meltzer, _et al._, 2013](#mel13)). The present study examines the contribution of participation in Internet closed virtual communities for people with mental illnesses as a channel for forming social relationships.

The study focuses on the following subjects: who are the participants in the virtual communities? Do social relationships develop between the members of the community? Can these relationships be significant and provide social support and empathy for people with mental illnesses?

## Review of the literature: virtual support communities

The term _virtual community_ is used to describe people who meet and form ties in a virtual environment. Internet support groups, mainly those that dealt with medical problems, gained momentum at the beginning of the 1990s. Virtual communities were formed because of a field of interest shared by the members of the community or because of a similar life story ([Uden-Kraan, Drossaert, Taal, Shaw, Seydel and Van De Laar, 2008](#ude08)). The number of people who devote time to participation in virtual communities is increasing; they do so with the aim of creating friendships, developing relationships, and receiving, or indeed giving, emotional support. Social relationships in a virtual community can help deprived populations greatly, as they make it possible to remain in contact with family members and friends despite mobility difficulties, lack of resources or similar ([Pistrang, Barker and Humphreys, 2008](#pis08)). Often the people who join virtual support groups do so after they find themselves in a stressful situation, for example when contending with severe illness ([Komito, 2011](#kom11)). Reducing geographical gaps, twenty-four hour availability, a reasonable price, anonymity and an increasing number of Internet users have all caused an increase in the number of participants in virtual communities ([Pfeil and Zaphiris, 2007](#pfe07)). The participants in these groups expect to receive _knowledge as a result of experience_, as opposed to the _professional knowledge_ that is provided by professionals ([Uden-Kraan _et al._, 2008](#ude08), p. 413).

Emotional communication is defined as understanding, expressing and sharing emotions or moods between two or more people ([Derks, Fischer and Bos, 2008](#der08)). The main difference between research of transference of emotions in computer-mediated communication and in face-to-face communication is that in computer-mediated communication the main means of emotional expression is almost entirely based on expressing emotions textually. In the past, researchers ([Lee and Wagner, 2002](#lee02); [Rice and Love, 1987](#ric87)) maintained that transference of emotions by computer-mediated communication is adversely affected by absence of the visual aspect that is important in order to decode other people's emotions. Honeycutt ([2005](#hon05)) also maintained that computer-mediated communication is characterised by a lack of social norms or social control that makes it difficult to form significant relationships between people. However other researchers maintain that the absence of the visual hints that exist in face-to-face communication, social labelling and the influence of social norms that operate in face-to face communication has a restraining effect, and thus the computer-mediated form of communication makes it possible to form more impartial relationships ([Derks, _et al._, 2008](#der08); [Evers, Fischer, Rodriquez Mosquera and Manstead, 2005](#eve05); [Manstead and Fischer, 2001](#man01)).

People find ways to communicate emotions in computer-mediated communication systems in a way that is similar to face-to-face communication. An example of this is the use of emoticons such as smiley faces that are used to add a visual aspect to the message's content. Research by Pornsakulvanich, Haridakis and Rubin ([2008](#por08)) found that people's degree of satisfaction with the relationships that they carry out by means of computer-mediated communication depends on their character, their motivation to form a relationship and the convenience of using virtual tools. Derks, _et al._ ([2008](#der08)) reached the conclusion that emotional communication using computer-mediated channels is very similar in the degree of its effectiveness to face-to-face communication. According to them, satisfactory relationships can also be built in virtual communication channels.

Griffiths, Calear and Banfield ([2009](#gri09a)) found that dimensions such as self-disclosure that are rarely found in face-to-face communication are much more widespread in computer-mediated communication channels. This difference is explained as part of the perceived anonymity that is enabled by communication by means of a computer. Users can write and read messages whenever they wish, and invest as much time and effort as they feel like in this activity. Participants can also focus on writing the message and its content without having to listen to another person at the same time, as this is not a face-to-face discussion. As participants can read other participants' messages a long time after they appear, these do not distract them from their day-to-day activities. This feature can improve the quality of the psychological support, as it makes it possible to invest much time and effort in giving a supportive answer and, correspondingly, to receive such replies ([Walther and Boyd, 2002](#wal02)). Often people who turn to virtual support communities do so because they are unable to find the kind of support they need in other ways ([Coulson, Buchanan and Aubeeluck, 2007](#cou07); [Walther and Boyd, 2002](#wal02); [Winefield, Coventry, Lewis and Harvey, 2003](#win03)). Komito ([2011](#kom11)), who examined the nature of relationships that develop between Filipino and Polish migrants in Ireland through computer-mediated communication channels, found that participation in virtual communities of migrants contributed to the feeling of belonging and widened the social network of these minority groups and also helped the migrants to remain in touch with friends in their countries of origin.

## Virtual support communities for people with mental illnesses

The support groups for people with mental illnesses were established by people with mental illnesses or by professionals in the field of mental health. Their aim is to provide information and support for other people who also experience mental illnesses. There is not much existing professional literature on the subject and different opinions are expressed in it. In his study, Houston ([2002](#hou02)) examined five virtual support communities for people suffering from depression; he found that most of the participants in these communities suffer from isolation and have very limited social networks. According to his research findings, participation in a virtual community did not increase the participants' social networks; however, they experienced a reduction in the symptoms of depression. A third of the participants in the community studied by Houston ([2002](#hou02)), reported a preference for computer-mediated communication, because of concern about the prejudices that accompany disclosure of mental illness. Computer-mediated communication channels enable the participants to remain anonymous and thus help them to be more open about their illness. It also emerged from the findings that most of the people with mental illnesses who were active in the virtual community in a given period were undergoing an episode of depression and were socially isolated. The participants reported receiving significant social support through participating in the virtual community; they attributed a high level of importance to these support communities and used the information they accumulated there in their relations with the professionals who treat them.

On the other hand, the findings of another study ([Kaplan, Salzer, Solomon, Brusilovskiy and Cousounis, 2011](#kap11)) do not support the opinion that participation in virtual support communities managed by the people with mental illnesses themselves does indeed benefit them. According to these findings, the participants did not feel better about themselves as a consequence of participation in groups. In the opinion of the researchers, the lack of a professional group manager made it difficult to create a feeling of community, and this may be the reason for the lack of practical benefit to the participants from the community. The researchers added that the virtual support communities are not available to the entire community of people with mentally illnesses but only to a limited group. This is because participation in the communities necessitates a certain level of functioning and environmental stability and communication using a computerised system, and this is not always possible for people who are mentally ill.

Although Smithson _et al._ ([2011](#smi11)) and Whitlock, Power and Eckenrode ([2006](#whi06)) found in studies of teenagers who suffer from the syndrome of self-injury, that their participation in forums is perceived as a good channel for receiving information, empowerment and creating a feeling of community, between people suffering from the syndrome. On the other hand, they expressed concern that participation in communities may encourage self-injury behaviour among teenagers, in consequence of exposure to the content in the forum. Similar concern was also expressed by Gavin, Rodham and Poyer ([2008](#gav08)), who were concerned about participation by women who suffer from anorexia in closed forums of women with this illness. As in these forums, the eating disorder is presented as normal and normative, and the participants frequently encourage each other not to seek help from professionals.

Vayreda and Antaki ([2009](#vay09)) examined online forums of people with bipolar syndrome and found that the participants evaluate the advice given in the forum as good, as it is necessary to receive permission from the forum managers to be exposed to the discourse in the forum and this permission is given only after presenting the necessary medical authorization. The certainty that not just any casual browser can be exposed to the content in the forum induces a feeling of security and encourages disclosure. Salem, Bogat and Reid ([1997](#sal97)) also found in their study of support groups on the subject of depression that participants who frequently write in a group are significantly more likely to provide psychological support to other group members, or to respond individually to any request for support that arises, than participants who do not write in the group. However these participants tend to avoid self-disclosure and refrain from asking for personal support from the group.

It is to be noted that there are professionals who are worried about the possibility that their patients will participate in computer-mediated support groups without medical supervision. According to them, participation in groups is liable to harm people with mental illnesses and increase their social isolation, as participation in virtual communities may be used as an excuse for people with mental illnesses to remain at home and not meet other people. Professionals are also concerned about the patient's exposure to dangerous, unprofessional advice, and that other participants are liable to exploit people with mental illnesses as a consequence of disclosing their personal story. There is also a possibility that depressed patients will decide to neglect their medical treatment as they will decide to manage with participation in virtual support communities only ([Kaplan _et al._, 2011](#kap11)).

## Empathy

A main variant in the present study is empathy. Empathy is described by Kohut ([1981](#koh81), p. 128) as the ability to put oneself in another person's place, temporarily, not through full identification and connection. As a consequence of seeing the other person's difficulties and feelings, and the ability to escape that feeling people may respond with emotional support, i.e., the ability to feel the other without relinquishing the self. This is an emotional process that also has cognitive aspects. Levenson and Ruef ([1992](#lev92)) defined empathy as the ability to know what another person is feeling, feel what another person is feeling and respond with compassion to the other person's troubles. According to Davis ([2004](#dav04)), empathy involves transference of another person's experience to within one's own experience and reactions. He maintained that empathy is a psychological process that, at least theoretically, joins together the separate units of the self and the other. When examining the term in this way one can relate to empathy as a deep social phenomenon.

Two people or more are involved in the empathetic process; one side gives the empathetic message and the other side receives it. As people have common ground and experiences, they can indirectly experience emotions that another person feels, when they imagine they are in the same situation. The empathetic process involves two stages that take place in the empathiser: understanding – understanding the other’s situation, a feeling and perception of similarity; in this stage the empathiser identifies situations that s/he has experienced in the past in the story of the person seeking empathy that enable the empathiser to understand that person’s troubles better. The second stage is the stage of action, when the empathiser takes empathetic action towards the person in need ([Elliott, Bohart, Watson and Greenberg, 2011](#ell11); [Håkansson and Montgomery, 2003](#hak03); [Shamay-Tsoory, 2009](#sha09)). Empathisers are motivated to act for the benefit of the person receiving empathy because of their understanding that this person is like them. According to another hypothesis, empathy is derived from altruistic motivation to help others ([Pfeil and Zaphiris, 2007](#pfe07)).

According to findings in previous studies ([Pfeil and Zaphiris, 2007](#pfe07); [Preece and Ghozati, 2001](#pre01)), expressions of empathy exist in computer-mediated communication. This phenomenon is particularly interesting, because empathy, like other emotions, is transmitted to others mainly by non-verbal means, whereas communication in virtual communities consists almost entirely of verbal expression and written messages, and it is difficult to transmit physical gestures or vocal messages. The present study examines whether characteristics of empathy exist in virtual communities of people with mental illnesses in Israel.

## The research questions

1.  What motivates people with mental illnesses to join virtual support communities, and what characterises them?
2.  What kinds of social support are exchanged in these communities? Emotional support? Physical support? Support by providing information?
3.  Is empathy expressed in the virtual support communities for people with mental illnesses?

## Methods

The study was based on the constructivist qualitative research tradition that is characterised by a holistic attitude to phenomena. The aim of this paradigm is to understand the phenomenon and the context in which it takes place as a whole. According to the constructivist paradigm there is no one reality that can be researched, but reality is a result of personal structuring ([Shkedi, 2003](#shk03)). To achieve this aim, a case study methodology was used. Focusing on the aspects relevant to our research question ([Creswell, Hanson, Plano and Morales, 2007](#cre07)), a content analysis was performed. The research corpus was studied using a content analysis technique that is a systematic method of comparison. This is a series of question, codes by means of which the coder seeks to examine the research materials, and to which s/he receives answers from within a pre-determined series of alternatives ([Bauer and Aarts, 2000](#bau00a)).

The research population consisted of discussions that took place in two virtual communities for people with mental illnesses in Israel: Community One and Community Two. The choice of the communities that participated in the study were based on the manner in which discourse in the community was managed and led; i.e., the community was managed by the participants themselves. The community managers were also people who have mental difficulties. The chosen communities had no professional body that moderates discourse.

In the course of gathering the data, passive observation of the activity in the two virtual support communities that were chosen for the study took place. Using passive observation, the researcher approached the experience of participation in a virtual community and got to know the participants and the topics discussed in the community. In accordance with the instructions of the researcher's institution ethics committee, permission was received from the community managers to carry out passive observations of the activity in the community for academic research purposes. Data from our research would not include details that could link the participants in the discussions to the findings. The sample for the study included all online posts in the communities during September and October 2011 and during January and February 2012\. September and October were chosen as they are difficult months for people in the Northern Hemisphere with mental illnesses (the holiday period in Israel emphasises the isolation they feel) and January and February represent routine everyday activity. A total of 240 threads, consisting of 1,385 posts, were analysed in the study. The corpus also fulfils the three criteria presented by Bauer and Aarts ([2000](#bau00a)), as essential for constructing a qualitative research corpus:

*   Relevance: gathering data relevant to the research question.
*   Homogeneity: the kind of data should be uniform, and two kinds of data collection should not to be mixed in one corpus.
*   Synchronicity: the data should be relevant to the period being studied.

The data were analysed in three stages using content analysis techniques (see figure 1) ([Ayalon and Sabar-Ben Yehoshua, 2010](#aya10)).

<figure class="centre">![Figure1: Data analysis](p701fig1.png)

<figcaption>Figure 1: The conceptual framework</figcaption>

</figure>

In the first stage the research corpus in each community and each period was divided at thread level into nine main categories: threads that deal with introducing oneself, medical topics and legal rights, problems related to the National Insurance Institute and the Ministry of Health, personal stories, experiences of difficulties, problems at work, community building and miscellaneous topics. The aim of this stage was to carry out an initial grouping of the discussions according to the subject of the conversation; this stage is parallel to the open coding stage described by Ayalon and Sabar-Ben Yehoshua ([2010](#aya10)).

In the second stage, the corpus was analysed at post level using six categories: self-disclosure, economic support, in-depth support, building a community, medical-legal facts and miscellaneous information. This stage is parallel to the axis coding stage as demonstrated by Ayalon and Sabar-Ben Yehoshua ([2010](#aya10)), in which the main categories were defined precisely according to the research questions and the materials that were collected, and the entry conditions for each main category were defined.

The third stage was the stage of in-depth analysis in which the posts were dissected into smaller units of meaning (statements) and each statement was classified in a system of themes that was constructed from the content that emerged during analysis. At the end of the process the final category tree was constructed; this consisted of eighty-three categories that fully expressed the content of the messages exchanged in the communities in light of the research questions. This stage is similar to the stage of directed coding described by Ayalon and Sabar-BenYehoshua ([2010](#aya10)), in which the stage of coding the data and matching the categories within the main categories continues until all the existing data easily match the existing system of groups and repeats begin to be discovered.

The research corpus was studied using a content analysis technique that is a systematic method of comparison ([Bauer and Aarts, 2000](#bau00a)).

To create limits to the study the researchers relied on previous studies ([Pfiel and Zaphiris, 2009](#pfe09); [Siriaraya, Tang, Ang, Pfeil and Zaphiris, 2011](#sir11)) that determined a lower limit, i.e., analysis of 200 messages as a point that saturation is reached, in order to exhaust the research field. In the present study 1,385 messages were analysed, at least fifty from each community for each period.

A test of the coding’s reliability was carried out in two stages. In the first stage an internal reliability test was carried out: the researcher coded about 10% of the data again, after a period of at least two weeks, with the aim of testing the coding system’s reliability, consistency and internal stability. In the second stage 10% of the messages underwent further coding by a colleague. The degree of agreement between the coders was 94.6% in coding the main subjects and 85.7% in coding the statements to the system of themes that was formulated for the study.

## Findings

About 72% of the participants in discourse were women and about 28% of the participants were men. During the study fifty-five members participated in Community One of whom twenty-four writers were active in both periods. This group included eight writers who wrote more than fifteen posts in each period and who belong to the group of most significant contributors to the community.

Thirty-one members participated in Community Two during the study of whom eleven members were active in both periods. Of these members, six wrote more than ten posts in each period and were the significant core group around which the activity in took place.

The research corpus was divided into main categories according to the threads' opening posts. It was found that 80% of the opening posts in the threads included content that expresses an experience of difficulty. This was usually connected to mental illness and a request for support from the community. These posts were sorted into main categories: personal story, experience of difficulty, problems with officialdom, problems at work and advice on medication. It was found that 20% of the remaining posts related to topics not directly connected to the writer's situation or problems and contained messages related to the experience of participation in the community or other topics such as current events, humour or similar. These messages were sorted into the categories of community building and miscellaneous.

<table class="center" style="width:99%;"><caption>  
Table 1: The number of threads included in each main category</caption>

<tbody>

<tr>

<th colspan="2">Community 1</th>

<th colspan="2">Community 2</th>

</tr>

<tr>

<th>Main category</th>

<th>No. of threads</th>

<th>Main category</th>

<th>No. of threads</th>

</tr>

<tr>

<td>Experience of difficulties</td>

<td style="text-align:center;">33</td>

<td>Experience of difficulties</td>

<td style="text-align:center;">32</td>

</tr>

<tr>

<td>Personal story</td>

<td style="text-align:center;">32</td>

<td>Personal story</td>

<td style="text-align:center;">26</td>

</tr>

<tr>

<td>Problems with officialdom</td>

<td style="text-align:center;">18</td>

<td>Community building</td>

<td style="text-align:center;">16</td>

</tr>

<tr>

<td>Advice on medication</td>

<td style="text-align:center;">17</td>

<td>Miscellaneous</td>

<td style="text-align:center;">11</td>

</tr>

<tr>

<td>Miscellaneous</td>

<td style="text-align:center;">16</td>

<td>Introducing oneself</td>

<td style="text-align:center;">10</td>

</tr>

<tr>

<td>Introducing oneself</td>

<td style="text-align:center;">9</td>

<td>Problems with officialdom</td>

<td style="text-align:center;">5</td>

</tr>

<tr>

<td>Problems at work</td>

<td style="text-align:center;">8</td>

<td>Advice on medication</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td>Community building</td>

<td style="text-align:center;">4</td>

<td>Problems at work</td>

<td style="text-align:center;">—</td>

</tr>

<tr>

<td>Total threads</td>

<td style="text-align:center;">137</td>

<td> </td>

<td style="text-align:center;">103</td>

</tr>

</tbody>

</table>

Below are a number of examples of opening posts.

The _personal story_ category refers to a situation where a person reveals his/her general story, and _experience of difficulties_ category addresses a situation where a person reports about a present difficulty and seeks for assistance. These two main categories were accompanied by deep emotional disclosure:

> It doesn't seem that there is any prospect of a future. I'm in an employment centre, I live in a hostel. I'm not satisfied with my life, I want to live it but I don't have any way to do so. What I want more than anything is to get to know people, mainly friends.

The _problems at work_, _problems with officialdom_ and _advice on medication_ main categories deal with difficulties people with mental illnesses encounter to which a factual, less emotional solution, such as help in choosing practical courses of action or similar, can be given:

‘

> Who can provide information on hospitalization and what the dangers are, where can information be found about the procedure of voluntary hospitalization?

The topic of _community building_ contains threads on the feelings of the participants in the discussion about the experience of participation in the community; in this case the writer comments on the unique support that can be received through participation in the community from other members who experience similar problems:

> In my opinion the idea that people with experience in the field of mental illnesses help other people with mental illness is good, and there is better containing ability and understanding for one another.

What motivates people to join discourse in communities?

During the research, fourteen new members joined activity in the communities. Of the people who joined, 57% did so following crisis situations experienced as a consequence of their mental disorder. Their joining posts included the medical diagnosis of the disorder they suffer from, and a more specific mention of the problem that caused them to turn to the community at that specific time. In these posts, when social relationships have not yet been established with the community, support is usually sought through requests for information, mainly about medication, treatment and rights to which they are legally entitled. For example:

> I'm new here... I've suffered from depression recently and I hardly function... I wanted to ask what you recommend doing in this situation? Could medicinal treatment help me?

Twenty-nine per cent of the new participants joined because they wished to form social ties with people like themselves through the community. The way the new participants who belong to this group present themselves is different. They address all the members of the community in a pleasant tone that invites contact.

> I'm new in the community... I woke up early today, so good morning, world. Is anyone here?

Another reason for joining the community was a feeling of isolation and about 14% of the opening posts of new members in the community contain expressions of isolation. In the first post, the participant tells the community about feelings of isolation that s/he is experiencing and connects them to the mental disorder from which s/he suffers.

The first research question also related to the different patterns of activity adopted by people who join the discussion.

The findings show that not everyone who joined the community continued to participate in discourse or build interpersonal relationships. It was found that a small core of active writers wrote the majority of posts in the communities. In Community One twelve out of thirty-one participants wrote 478 posts; 80% of all the posts written in both periods. And in Community Two ten participants out of fifty-five community members wrote 544 posts; 69% of all the posts that were written in the community in the two research periods.

During the research, it was possible to characterise the activity patterns of two types of participants in the communities: participants who mainly sought support and participants who mainly gave support to others. To examine this hypothesis we used statistical tools to help characterise the patterns of activity of the writers in the communities. We classified each participant according to an additional category, named the _support giving index_; the index is built from a five-point scale: _only asks for support, mainly asks for support, asks for and gives support equally, mainly gives support_ and _only gives support_ (determining the index on the scale for each writer was carried out qualitatively by the researcher by means of critical examination of all the messages written by that writer).

It was found in the light of the content analysis that most opening posts are requests for support. Therefore, the number of opening posts written by each participant and the number of posts within threads started by that participant were counted and compared to the number of posts written by the participant in threads begun by other people. In the analysis, we used the Pearson coefficient, which measures the relation between two variables in a quantitative index, and we also examined which of the writers mainly give support compared to writers who mainly seek support, relative to the number of posts they wrote.

<figure class="centre">![Figure 2: relation between the number of posts and the support scale](p701fig2.png)

<figcaption>Figure 2: The relation between the number of posts and the support scale</figcaption>

</figure>

Figure 2 presents the relation between the number of posts the participants wrote and their attribution as a receiver or giver of support. It can be seen that most of the people who wrote few posts were classified as receivers of support while most of the people who wrote many posts were classified as givers of support.

In addition, it was found that writers who wrote many opening posts and continuing posts in threads they began mainly sought support (r = 0.683, p < 0.001) and writers who wrote many posts in threads begun by others (r = 0.956, p < 0.001) mainly gave support.

According to these findings, the communities were built on a number of significant writers who constitute the core of the community and a secondary circle of casual writers who usually write when they need support from the community. The participants in the secondary circle had two kinds of customary behaviour; most of them begin a new thread that presents their difficulty and they participate only in the thread that deals with the topic they raised. Others react to threads begun by other people near their own thread (during the same period); it follows that these participants were interested in discourse in the community only in periods when they needed help, whereas in other periods when they received support from other sources they do not participate much in the community. By comparison, the regular participants wrote less opening posts on average, they respond more to other people's requests for help and they were the people who provided most of the support in the community.

### What kinds of social support are exchanged in the communities?

To characterise the kinds of support exchanged in the community, the messages in the threads were classified according to a system of six categories that express the different kinds of support. Table 2 sums up the number of messages that were classified in each main category of the different kinds of help.

<table class="center" style="width:99%;"><caption>  
Table 2: Distribution of the main categories of the themes in the two communities in each period of research</caption>

<tbody>

<tr>

<th colspan="2">Community 1</th>

<th colspan="2">Community 2</th>

</tr>

<tr>

<th>Main category</th>

<th>No. of messages</th>

<th>Main category</th>

<th>No. of messages</th>

</tr>

<tr>

<td>Intensive support</td>

<td style="text-align:center;">280</td>

<td>Intensive support</td>

<td style="text-align:center;">205</td>

</tr>

<tr>

<td>Self-disclosure</td>

<td style="text-align:center;">141</td>

<td>General support</td>

<td style="text-align:center;">167</td>

</tr>

<tr>

<td>Medical information</td>

<td style="text-align:center;">135</td>

<td>Self-disclosure</td>

<td style="text-align:center;">106</td>

</tr>

<tr>

<td>General support</td>

<td style="text-align:center;">118</td>

<td>Community building</td>

<td style="text-align:center;">72</td>

</tr>

<tr>

<td>Community building</td>

<td style="text-align:center;">78</td>

<td>Medical information</td>

<td style="text-align:center;">56</td>

</tr>

<tr>

<td>Miscellaneous</td>

<td style="text-align:center;">61</td>

<td>Miscellaneous</td>

<td style="text-align:center;">52</td>

</tr>

</tbody>

</table>

Below are examples of messages included in the various main categories.

The category of _intensive support_ includes expressions of support aimed at a specific person who shares a difficulty that s/he faces; this category included expressions of containing, compassion and empathy as well as practical suggestions on how to cope with the difficulty:

> You are not a failure. You have a lot of good qualities, try to see them.

In the category of _self-disclosure_, support was given by means of similar life experience and reflection on how to cope with the situation, and in this way, the writer wants to strengthen the person asking for support:

> My path was not too different and the pain tripped us up the whole time. You fight. You try. First of all, allow yourself to love yourself. To open your heart. To forgive. Yourself... You're not a failure. You have many good things in you, try to see them.

The category of _general support_ contained common expressions of support such as '_welcome_', '_you have reached a warm house_'.

The main categories of _general support_, _intensive support_ and _self-disclosure_ include categories expressing different kinds of support that was focused specifically on the person seeking support:

*   Support by means of exchanging information: participants who asked for information on their illness, its symptoms and the rights that they are entitled to as people with mentally illnesses. In _Community One_ 9.26% of the messages contained requests for information from the community, compared to 3.34% of the messages in _Community Two_.
*   A direct request for support: according to analysis of the opening posts of the threads, 11.5% of the opening posts contained a direct request for support.
*   Psychological backing: messages related to giving psychological backing were divided into three categories:
    *   Light psychological backing: expressions of backing that were addressed to a specific person or to the entire community. This is general backing that is not focused on a specific problem presented by the writer. This kind of backing includes general encouragement, expressions of affection, general reinforcement and support, advice that was addressed to all members of the community and expressions of empathy at a low level. In _Community Two_ 14.37% of the posts contained messages of this kind. In _Community One_ 24.87% of the posts contained messages of light backing.
    *   Intensive psychological backing: expressions of backing that were addressed specifically to another person following a request for backing or deep self-disclosure by the writer. This category includes posts that contain empathetic messages. In _Community One_ 34.10% of the posts contain messages with expressions of deep backing for others and in _Community Two_ 32.24% contained expressions of this kind.
    *   Support by means of acceptance and containment of the writer's situation: many writers felt sufficiently confident to share their diagnoses, their mental condition at a given time and the difficulties they contended with as a consequence of their mental illness with the members of the community. In _Community One_ 15% of the messages included content that involved disclosure of personal information and in _Community Two_ 17.13% of the posts included content that involved disclosure of personal information. These data confirm the hypothesis assumption about the writers' feelings of security in personal disclosure of the difficulties they experience in consequence of their mental illness.
    *   Support by carrying out a physical act for another person: support of this kind was not particularly common, but it indicates the quality of the social relationships that were formed in the community, when people were prepared to give their time and make an effort to help others. This reflects the person's degree of commitment to the relationship. In the entire research period there were a total of eight posts that contained messages about a physical act that one of the participants did for another member of the community.

The community building main category contains categories that express support given to all the participants in the community. It also contains categories that examined whether participation in the communities provides the members in the community with the social support system they need.

_The feeling of belonging to the group:_ participants' messages that showed a wish to create or strengthen social relationships by means of participation in discourse: in Community One 18.14% of the posts contain messages that related to maintaining and reinforcing social relationships within the community and in Community Two 26.21% of the messages contained themes related to feelings about participation in the community. This finding reinforces the idea relating to the quality of the social relationships that are formed in the community, and the great importance that the participants in Community Two attribute to formation and maintenance of interpersonal relationships by means of participation in discourse in the community.

_Support by means of forming a social community_: one of the motives for joining a community is the hope that through participation in discourse in the community the participants will be able to form social relationships with other people similar to themselves: 18.14% of the posts in Community One and 26.21% of the posts in Community Two were classified in this category. From analysis of the messages that were classified in this category, we found that the participants in the discourse are divided in their opinions as to whether forming relationships through participation in online communities does indeed provide satisfactory social support for the members of the community:

> If your friends cannot provide you with this support, help you, you need to look for another source of support somewhere else... The community is also a good source of support but it can't replace a hug and a friend.

In contrast some people saw participation in the community as an excellent channel to fill social needs:

> I like being in the community, as I feel that in my everyday life there is hardly any room for the illness, however it is part of me, and in the community I allow room for this part.

### Is empathy expressed in the virtual support communities?

The aim of the third research question was to identify and characterise the patterns of empathy that appear in virtual support communities of people with mental illnesses. The idea was that if expressions of empathetic processes are found among the participants in the discourse we can maintain that significant relationships are indeed formed between the participants.

In discussions in the communities, we found varied expressions of empathy as defined above. Thirty nine per cent of all the threads that were investigated contained empathetic messages. The means of transferring the empathetic messages in computer-mediated communication was different from empathy that was given in face-to-face communication. The difference was derived from the different qualities of this communication channel. In the current study a number of forms of empathetic communication between the participants were found.

_Situations in which empathetic discourse between two members of the community exist_, when the person seeking empathy makes a direct request to receive empathy from a specific person, or when the words of the person asking for empathy deeply affected another person in the community and consequently s/he reacts in an empathetic manner:

> Thank you for saying I helped you. If I can also help now, I will be glad to do so... The wish to end the suffering is so familiar to me... Despite many years of balance and a relatively good feeling, it is still there somehow. I want to say to you that there is hope and that it’s worth hanging onto life because there are good things and good people around.

_Situations in which the participant shared the difficulties s/he feels with the group_ and the responses of all the group’s members combined to form an empathetic and reinforcing message. In one of the discussions a writer shared her difficulties as a consequence of depression and the members of the community sent her various messages of support; together all these messages that appear in one thread form a strong, empathetic message of support through identification with the situation:

> Like you I hardly sleep. I'm dead scared. I can't find a framework. I have suicidal thoughts every day.

_Support and empathy as a consequence of a similar life experience:_

> I completely understand you; I too experienced something like that a few years ago. Day hospitalization in a certain place I was in greatly helped me to get back on my feet.

_Giving a feeling of closeness and belonging to the group:_

> Don’t panic, we are with you – tell us later how you are getting on.

_Support as a consequence of familiarity:_

> I don't know why you collapsed but – and this is an important "but" – you have had impressive successes in the past. If you've succeeded once, there's no reason you can't succeed again.

_Support as a consequence of closeness:_

It's not fair to tell me that you want me to come and on the other hand to write that you want to die. Just like that – me and death in the same breath. Let's do a deal – I'm prepared to enter the group and write to you every day, even on difficult days when I don't want to do anything, on condition that you’ll write back and tell me everything that's on your mind.

Focused empathy:

> If I were in your place I would make sure I received psychological support and I would carry out the task. You mustn't give up on yourself, on the contrary. You're on the right path to free yourself of your difficulties. That's why you're in treatment, and we love you and you are very dear to us. I'm sure that everyone here feels great empathy towards you. Continue on the path you are treading, slowly but surely – I send you a giant hug.

## Discussion

This study analysed the discourse that took place in two closed virtual communities of people with mental illnesses in two defined research periods. Using qualitative content analysis techniques we sought to examine and describe the social relationships that developed between the participants in the communities' discourse. The analysis was carried out according to the case study approach.

In the research literature we found that many researchers have examined discourse in forums and communities that are managed by professionals who treat people with mental illnesses ([Houston, 2002](#hou02); [Kaplan _et al._, 2011](#kap11)). However, we found few studies that analysed discourse in empathetic virtual communities that are managed by the people with mental illnesses themselves with no supervising element ([Smithson _et al._, 2011](#smi11); [Whitlock, Power and Eckenrode, 2006](#whi06)). In communities that are managed by professionals the discourse is not equal and it mostly consists of questions that the people with mental illnesses address to the professionals. We maintain that in this situation it is harder to create a feeling of community and equal social relationships between the participants. The importance of the present study is derived from investigation of social discourse in virtual communities of people with mental illnesses that are managed by the people with mental illnesses themselves with no involvement by professionals. In the study we attempted to examine whether it is possible to form social relationships when the discourse in the communities has an equal basis.

### The participants in the virtual community of people with mental illnesses in Israel

The reasons for joining the virtual community can be found from the opening posts published by new participants in the communities. Coulson, Buchanan and Aubeeluck ([2007](#cou07)) found that most of the people who turn to virtual support communities do so as they cannot find the kind of support they need in other ways. The results of the current study show that the main reason for joining the communities is a crisis or difficulty experienced by the participants at a given moment that is attributed to the mental disorder from which they suffer. These new participants tended to focus on the thread in which they wrote the opening post and they respond mainly to writers who relate to the problem they are contending with at the time. This finding is similar to findings related to the circumstances in which people join forums on medical topics, as reported by Burke, Kraut and Joyce ([2010](#bur10)). A further reason for joining communities is the feeling of isolation that accompanies mental disorders. These new participants hope that by joining the community they will be able to form social relationships with other people like them – relationships they have difficulty forming in other ways. Turner, Grube and Meyers ([2001](#tur01)), who maintained in their study that many of the people who turn to virtual support groups do so when they feel their offline social relationships are shallow and inadequate, also reported a similar finding.

The results of this study show that only a few of the people who join activity in the communities continue with this activity over time. These writers were the significant core and they ensure the community remains continuously active. They are people who have succeeded in making the transition from a writer seeking a solution to a momentary crisis to writers who provide support in a variety of ways for other writers; they are people who have even succeeded in forming deep interpersonal relationships among themselves, who give a wide variety of expressions of giving and receiving messages of support. Expressions of deep personal acquaintance can be found in their writing; they are able to understand what the other writers are saying as part of a continuum of on-going ties, not from the narrow viewpoint of a momentary problem taken out of context. This finding reinforces the findings of the study carried out by Salem _et al._ ([1997](#sal97)) who maintained that writers who frequently write in a community are more likely to give messages of support to other people in the community than writers who participate less in the community discourse.

### The kinds of social support exchanged in the communities

It emerges from the study that expressions of various kinds of support processes can be found in the discourse that takes place in the communities, similarly to the kinds of support that were reported in the studies of Uden-Kraan _et al._ ([2008](#ude08)) and Pistrang, Barker and Humphreys ([2008](#pis08)), which categorised the kinds of support that are given in support communities that deal with coping with various illnesses. The present study found:

*   Exchange of information: ?in the study by Griffiths _et al._ ([2009](#gri09a)) the rate of the exchange of information was significantly higher than was found in our research. This is because that research was carried out in communities in which the community manager is a professional or other authority. The discourse mainly consisted of the participants' questions with answers given by the community manager who was perceived as knowledgeable in the field.
*   A direct request for support: the findings reinforce the findings of Griffiths _et al._ ([2009](#gri09b)), which reported a similar percentage of direct requests for support on the part of participants in the discourse.
*   Psychological support: the findings show that participants in virtual discourse feel the environment is safe for them to share personal information, in contrast with previous studies that dealt with the level of disclosure of difficulties related to mental disorders in face-to-face communication. Those studies showed that most of the participants in discourse prefer to hide the information about their mental condition ([Doron, Slonim-Nevo and Ronen, 2008](#dor08); [Shtruch, Shereshevsky, Naon, Daniel and Fischman, 2009](#sht09)). It seems that the advantages of virtual communication that ensure relative anonymity, which makes it possible to form ties with other people like themselves and ensures that the information will not be revealed to others, contribute to the feeling of security that people contending with a mental disorder feel, and enable greater disclosure than in face-to-face channels.

### Expressions of empathy in the virtual support communities for people with mental illnesses

Previous studies ([Pfeil and Zaphiris, 2007](#pfe07); [Preece and Ghoza, 2001](#pre01); [Siriaraya _et al._, 2011](#sir11)) found that despite the limitations and the lack of physical contact, which is of great importance when transmitting emotions between people, expressions of empathy can be found in discourse conducted by means of virtual communication channels. In our study we found many expressions of empathetic processes that are created by the participants in discourse in virtual communities. These empathetic processes are a very important aspect of forming and establishing ties that exist by means of computer-mediated systems, like social relationships that are based on face-to-face communication. However the nature of the process is different from empathetic processes that take place in face-to-face communication because of the difference in the nature of the communication medium. Thus a situation is created where an empathetic process takes place between an individual who seeks help and a great number of members who respond to that person, and characteristics of a general empathetic process can be seen in one collection of their responses.

## Conclusion

There is no conclusive answer to the question as to whether social links that exist in computer mediated communication channels can fill people's diverse social needs. In this study we have attempted to contribute something to the existing body of knowledge on the subject of whether it is possible for people who are part of populations that have difficulties in forming social relationships in the physical world to form such relationships though computer-mediated communication channels. As we showed in the study, discourse in virtual communities does indeed create deep relationships between people who otherwise might not meet or choose to form interpersonal relationships because of obstacles such as physical limitations, prohibitions or social norms.

According to the participants in this discourse, the medium still has limitations that make it difficult for it to be a communication channel that fulfils all the social needs of members of socially excluded populations. It seems there is room to develop virtual tools that will meet this need more precisely. However, for many people who feel isolated the online discourse does indeed enable formation of interpersonal relationships that they have not managed to form in other ways. Therefore it is important to continue to research the characteristics of communication in virtual channels and to fit them better to social needs in general and those of exceptional populations in particular.

### The limitations of the research:

*   The limitation of generalization and representativeness: as a strategy of case study was chosen for the research, there is no aspiration to apply the findings generally to the entire population, but rather to reflect the events in a particular environment at a defined time.
*   The limitation of non-reversibility: in qualitative research, after meaning has been extracted from the data it is impossible to return to the raw data and the interpretation is univalent as it was found and presented by the researcher.
*   When building the corpus for research, in the time between the two periods that were sampled, there was a significant drop in the extent of the activity of one of the communities that was researched (this community is no longer active). The process of decline in the community’s activity was not recorded while gathering the data. It may be that this had effects on the discourse and the results of the research that it is impossible to weigh in presentation of the findings.

In future research, we would suggest investigating whether the number of participants in the community can predict the strength of the links in the community. Are the social ties in the community stronger in a smaller community? Does the intimacy contribute to the strength of the ties in the community? We would also investigate whether there is a link between the mental disorder from which the writer suffers and the chance that s/he will succeed in forming personal relationships by means of participation in the discourse of virtual communities.

## About the authors

**Noa Aharony** received her Ph.D. in 2003 from the School of Education at Bar-Ilan University (Israel). She is Senior Lecturer and head of the Information Science Department at Bar-Ilan University, Ramat-Gan, Israel, 5290002\. Her research interests are in education for library and information science, information literacy, technological innovations and the information science community, and Web 2.0\. She can be contacted at [Noa.Aharony@biu.ac.il](mailto:Noa.Aharony@biu.ac.il) ORCID No. [orcid.org/0000-0003-1440-3305](http://orcid.org/0000-0003-1440-3305)  
**Nava Rothschild** received her master's degree from the Department of Information Science, with a specialization in social Information Science at Bar-Ilan University. She is an independent information specialist. This article was written following the thesis done as part of a university degree. She can be contacted at [rotnava@gmail.com](mailto:rotnava@gmail.com).

#### References

*   Aviram, U. (2006). _Liqrat sherutei nefesh qehilatiim: gormim me'akhvim vegormim meafsherim shinui bemediniut uvesherutim livriut hanefesh (Niyar emda, kvutzat hamehqar bemediniut hevratit)_ [Towards community mental health services: delaying factors and factors enabling change in mental health policy and services (Position paper, the research group in social policy)]. Jerusalem, Israel: School of Social Work, Hebrew University. Retrieved 2011 from http://www.sw.huji.ac.il/upload/mentalhealth_pdf.pdf.
*   Ayalon, Y. & Sabar-Ben Yehoshua, N. (2010). Tahalih nituah tokhen lefi teoriah me'ugenet basadeh [The process of content analysis according to grounded theory]. In L. Kacen & M. Krumer-Nevo (eds.), _Nituah netunim bemehqar eikhutani_ (pp. 382–359). Beer Sheba, Israel: Ben-Gurion University Press.
*   Babic, D. (2010). Stigma and mental illness. _Materia Socio Medica, 22_(1), 43-46.
*   Bauer, M.W. & Aarts, B. (2000). Corpus construction: a principle for qualitative data collection. In: M. W. Bauer & G.D. Gaskell (Eds.), _Qualitative researching with text, image and sound. A practical handbook for social research_, (pp. 19–37). London: Sage Publications.
*   Bauer, M.W. & Gaskell, G. (2000). _Qualitative researching with text, image and sound: a practical handbook._ London: Sage Publications.
*   Berry, C., Gerry, L., Hayward, M. & Chandler, R. (2010). Expectations and illusions: a position paper on the relationship between mental health practitioners and social exclusion. _Journal of Psychiatric and Mental Health Nursing, 17_(5), 411-421.
*   Boardman, J. (2010). [Concepts of social exclusion](http://www.webcitation.org/6fSu9Q0qL). In Jed Boardman, Alan Currie, Helen Killaspy and Gillian Mezey, (Eds.). _Social inclusion and mental health._ London: RCPsych Publications. (pp. 10-21). Retrieved from http://www.rcpsych.ac.uk/files/samplechapter/SocialinclusionSC.pdf (Archived by WebCite® at http://www.webcitation.org/6fSu9Q0qL)
*   Bradshaw, T. & Haddock, G. (1998). Is befriending by trained volunteers of value to people suffering from long-term mental illness? _Journal of Advanced Nursing, 27_(4), 713–720.
*   Burke, M., Kraut, R. E. & Joyce, E. (2010). Membership claims and requests: some newcomer socialization strategies in online communities. _Small Group Research, 4_(1), 4-40.
*   Clement, S., Schauman, O., Graham, T., Maggioni, F., Evans-Lacko, S. & Bezborodovs, N. (2014). What is the impact of mental health-related stigma on help-seeking? A systematic review of quantitative and qualitative studies. _Psychological Medicine, 26_(2), 1-17.
*   Cobb, S. (1976). Social support as moderator of life stress. _Psychosomatic Medicine, 38_(5), 300-331.
*   Coulson, N. S., Buchanan, H. & Aubeeluck, A. (2007). Social support in cyberspace: a content analysis of communication within a Huntington's disease online support group. _Patient Education and Counseling, 68_(2), 173-178.
*   Creswell, J. W., Hanson, W. E., Plano, V. L. & Morales, A. (2007). Qualitative research designs: selection and implementation. _The Counseling Psychologist, 35_(2), 236-264.
*   Davis, M. H. (2004). Empathy: negotiating the border between self and other. In L. Z. Tiedens & C. W. Leach (Eds.), _The social life of emotions_, (pp. 19-42). New York, NY: Cambridge University Press.
*   Derks, D., Fischer, A. H. & Bos, A. E. (2008). The role of emotion in computer-mediated communication: a review. _Computers in Human Behavior, 24_(3), 766-785.
*   Doron, I., Slonim-Nevo, V. & Ronen, Y. (2008). Al zekhuyot adam veal hadarah hevratit [On human rights and social exclusion]. In: Y. Ronen, I. Doron & V. Slonim-Nevo (Eds.). _Hadarah hevratit vezekhuyot adam beYisrael_ [Human rights and social exclusion in Israel], (pp. 7–19). Tel Aviv, Israel: Ramot.
*   Elliott, R., Bohart, A. C., Watson, J. C. & Greenberg, L. S. (2011). Empathy. _Psychotherapy, 48_(1), 43-49.
*   Evers, C., Fischer, A. H., Rodriquez Mosquera, P. M. & Manstead, A. A. (2005). Anger and social appraisal: a "spicy" sex difference? _Emotion, 5_(3), 258-266.
*   Gavin, J., Rodham, K. & Poyer, H. (2008). The presentation of "Pro-Anorexia" in online group interactions. _Qualitative Health Research, 18_(3), 325-333.
*   Griffiths, K. M., Calear, A. L. & Banfield, M. (2009). [Systematic review on internet support groups (ISGs) and depression (1): do ISGs reduce depressive symptoms?](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2802256/) _Journal of Medical Internet Research, 11_(3), e40\. Retrieved from http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2802256/ [Archiving not allowed].
*   Griffiths, K. M., Calear, A. L., Banfield, M. & Tam, A. (2009). [Systematic review on internet support groups (ISGs) and depression (2): what is known about depression ISGs?](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2802257/) _Journal of Medical Internet Research, 11_(3), e41\. Retrieved from http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2802257/ [Archiving not allowed].
*   Håkansson, J. & Montgomery, H. (2003). Empathy as an interpersonal phenomenon. _Journal of Social and Personal Relationships, 20_(3), 267-284.
*   Honeycutt, C. (2005). Hazing as a process of boundary maintenance in an online community. _Journal of Computer-Mediated Communication, 10_(2), 00-00\. Retrieved from http://onlinelibrary.wiley.com/doi/10.1111/j.1083-6101.2005.tb00240.x/full
*   Houston, T. K. (2002). Internet support groups for depression: a 1-year prospective cohort study. _American Journal of Psychiatry, 159_(12), 2062-2068.
*   Israel. _Central Bureau of Statistics_. (2010). _Haseqer hahevrati 2010_ [The social survey 2010]. Jerusalem, Israel: Central Bureau of Statistics. Retrieved from http://www.cbs.gov.il/reader/cw_usr_view_SHTML?ID=569
*   Israel. _Ministry of Health_. (2013). _[Psychiatric care: admissions, discharges, beds, inpatients and day-care patients.](http://www.webcitation.org/getfile.php?fileid=d5ea4848e10a7eda43e82c54f1c0254af6776a24)_. Retrieved from http://www.cbs.gov.il/reader/shnaton/templ_shnaton.html?num_tab=st06_11&CYear=2012 (Archived by WebCite® at http://www.webcitation.org/getfile.php?fileid=d5ea4848e10a7eda43e82c54f1c0254af6776a24)
*   Kaplan, K., Salzer, M. S., Solomon, P., Brusilovskiy, E. & Cousounis, P. (2011). Internet peer support for individuals with psychiatric disabilities: a randomized controlled trial. _Social Science & Medicine, 72_(1), 54-62.
*   Kohut, H. (1981). On empathy. _International Journal of Psychoanalytic Self-Psychology, 5_(2), 122-131.
*   Komito, L. (2011). Social media and migration: virtual community 2.0\. _Journal of the American Society for Information Science and Technology, 62_(6), 1075-1086.
*   Lee, V. & Wagner, H. (2002). The effect of social presence on the facial and verbal expression of emotion and the interrelationships among emotion components. _Journal of Nonverbal Behavior, 26_(1), 3-25.
*   Levenson, R. W. & Ruef, A. M. (1992). Empathy: a physiological substrate. _Journal of Personality and Social Psychology, 63_(2), 234-246.
*   Levy, S. (2010). _[Ishpuz briut hanefesh beYisrael](http://www.webcitation.org/6fSuVYrdu)_ [Mental health hospitalization in Israel]. Jerusalem, Israel: Knesset Research and Information Center. Retrieved from: http://www.knesset.gov.il/mmm/data/pdf/m02428.pdf. (Archived by WebCite® at http://www.webcitation.org/6fSuVYrdu)
*   McManus, S., Meltzer, H., Brugha, T., Bebbington, P. & Jenkins, R. (2009). _Psychiatric morbidity in England (2007): results of a household survey_. London: National Centre for Social Research.
*   Manstead, A.S. & Fischer, A.H. (2001). Social appraisal: the social world as object of and in?uence on appraisal processes. In K. R. Scherer, A. Schorr, & T. Johnstone, (Eds.). _Appraisal processes in emotion: theory, methods, research_, (pp. 221-232). Oxford: Oxford University Press.
*   Martinez, A.G., Piff, P.K., Mendoza-Denton, R. & Hinshaw, S.P. (2011). The power of a label: mental illness diagnoses, ascribed humanity, and social rejection. _Journal of Social and Clinical Psychology, 30_(1), 1-23.
*   Maya, A., Becker, T., McCrone, P. & Thornicroft, G. (1998). Social networks and mental health service utilization—a literature review. _International Journal of Social Psychiatry, 44_(4), 248–266.
*   Meltzer, H., Bebbington, P., Dennis, M.S., Jenkins, R., Mcmanus, S. & Brugha, T.S. (2013). Feelings of loneliness among adults with mental disorder. _Social Psychiatry and Psychiatric Epidemiology, 48_(1), 5-13.
*   Perese, E. F. & Wolf, M. (2005). Combating loneliness among persons with severe mental illness: social network interventions' characteristics, effectiveness, and applicability. _Issues in Mental Health Nursing, 26_(6), 591-609.
*   Pfeil, U. & Zaphiris, P. (2007). Patterns of empathy in online communication. In Bo Begole & Stephen Payne (Eds.) _Proceedings of the SIGCHI Conference on Human Factors in Computing Systems_ (pp. 919-928). New York, NY: ACM.
*   Pfeil, U. & Zaphiris, P. (2009). Investigating social network patterns within an empathic online community for older people. _Computers in Human Behavior, 25_(5), 1139-1155.
*   Pistrang, N., Barker, C. & Humphreys, K. (2008). Mutual help groups for mental health problems: a review of effectiveness studies. _American Journal of Community Psychology, 42_(1-2), 110-121.
*   Pornsakulvanich, V., Haridakis, P. & Rubin, A. M. (2008). The influence of dispositions and internet motivation on online communication satisfaction and relationship closeness. _Computers in Human Behavior, 24_(5), 2292-2310.
*   Preece, J. & Ghozati, K. (2001). Observations and explorations of empathy online. In R. E. Rice & J. E. Katz (Eds.), _The Internet and health communication: experiences and expectations_, (pp. 237-260). Thousand oaks, CA: Sage Publications.
*   Rice, R. E. & Love, G. (1987). Electronic emotion: socio emotional content in a computer-mediated communication network. _Communication Research, 14_(1), 85-108.
*   Salem, D. A., Bogat, G. A. & Reid, C. (1997). Mutual help goes on-line. _Journal of Community Psychology, 25_(2), 189-207.
*   Shamay-Tsoory, S. (2009). Empathic processing: its cognitive and affective dimensions and neuroanatomical basis. In J. Decety & W. J. Ickes (Eds.), _The social neuroscience of empathy_, (pp. 215-232). Cambridge, MA: MIT Press.
*   Shkedi, A. (2003). _Milim hamenasot laga’at: meqhar aikhutani – teoria veyisum_ [Words that attempt to touch: qualitative research theory and practice]. Tel Aviv, Israel: Tel Aviv University.
*   Shtruch, N., Shereshevsky, Y., Naon, D., Daniel, N. & Fischman, N. (2009). _[Anashim im be'ayot nafshiot qashot beYisrael: reiya meshulevet shel ma'arakhot vesherutim.](http://www.webcitation.org/6fSudtwfJ)_ [People with severe mental disabilities in Israel: an integrated view of the service system]. Jerusalem, Israel: Meyers-JDC-Brookdale Institute and the Israel Ministry of Health. Retrieved from http://brookdaleheb.jdc.org.il/_Uploads/PublicationsFiles/549-09-SevereMentalDisorders-ES-HEB.pdf (Archived by WebCite® at http://www.webcitation.org/6fSudtwfJ)
*   Siriaraya, P., Tang, C., Ang, C. S., Pfeil, U. & Zaphiris, P. (2011). A comparison of empathic communication pattern for teenagers and older people in online support community. _Behavior & Information Technology, 30_(5), 617-628.
*   Smithson, J., Sharkey, S., Hewis, E., Jones, R. B., Emmens, T., Ford, T. & Owens, C. (2011). Membership and boundary maintenance on an online self-harm forum. _Qualitative Health Research, 21_(11), 1567-1575.
*   Tidy, C. (2010.). Social isolation: how to help patients be less lonely’. _Patient_. Retrieved from http://www.patient.co.uk/doctor/social-isolation-how-to-help-patients-be-less-lonely
*   Turner, J. W., Grube, J. A. & Meyers, J. (2001). Developing an optimal match within online communities: an exploration of CMC support communities and traditional support. _Journal of Communication, 51_(2), 231-251.
*   Uden-Kraan, C.F., Drossaert, C.H., Taal, E., Shaw, B.R., Seydel, E.R. & Van De Laar, M.A. (2008). Empowering processes and outcomes of participation in online support groups for patients with breast cancer, arthritis, or fibromyalgia. _Qualitative Health Research, 18_(3), 405-417.
*   Vayreda, A. & Antaki, C. (2009). Social support and unsolicited advice in a bipolar disorder online forum. _Qualitative Health Research, 19_(7), 931-942.
*   Walther, J.B. & Boyd, S. (2002). Attraction to computer-mediated social support. In C. A. Lin & D. J. Atkin (Eds.), _Communication technology and society: audience adoption and uses_, (pp. 153-188). Creskill, NJ: Hampton Press.
*   Whitlock, J.L., Powers, J.L. & Eckenrode, J. (2006). The virtual cutting edge: the Internet and adolescent self-injury. _Developmental Psychology, 42_(3), 407-417.
*   Winefield, H.R., Coventry, B.J., Lewis, M. & Harvey, E.J. (2003). Attitudes of patients with breast cancer toward support groups. _Journal of Psychosocial Oncology, 21_(2), 39-54.
*   World Health Organization. (2015). _[Suicide](http://www.webcitation.org/6fSundjm1)._ Geneva, Switzerland: World Health Organization. Retrieved from http://www.who.int/mediacentre/factsheets/fs398/en/ (Archived by WebCite® at http://www.webcitation.org/6fSundjm1)
*   World Health Organization. (2015). _[Mental disorders](http://www.webcitation.org/6fSuvjJfR)_. Geneva, Switzerland: World Health Organization. (Fact sheet N°396). Retrieved from http://www.who.int/mediacentre/factsheets/fs396/en/ (Archived by WebCite® at http://www.webcitation.org/6fSuvjJfR)