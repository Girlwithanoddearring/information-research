#### Volume 13 No 4 December, 2008

## Proceedings of the 7th conference on Information Seeking in Context, Vilnius, September 2008

#### Edited by Elena Macevičiūtė and T.D. Wilson

#### Bonnie Nardi  
[Keynote address. Mixed realities: information spaces then and now](paper354.html)

#### Carol C. Kuhlthau, Jannica Heinström and Ross J. Todd  
[The 'information search process' revisited: is the model still useful?](paper355.html)

#### Jennifer M. Berryman  
[Influences on the judgement of enough information](paper356.html)

#### Noa Fink-Shamit and Judit Bar-Ilan  
[Information quality assessment on the Web: an expression of behaviour](paper357.html)

#### Allen Foster, Christine Urquhart and Janet Turner  
[Validating coding for a theoretical model of information behaviour](paper358.html)

#### Snunith Shoham and Sarah Kaufman Strauss  
[Immigrants' information needs: their role in the absorption process](paper359.html)

#### Ina Fourie  
[Information needs and information behaviour of patients and family members in a cancer palliative care setting](paper360.html)

#### Pertti Vakkari  
[Keynote address. Trends and approaches in information behaviour research](paper361.html)

#### Reijo Savolainen  
[Autonomous, controlled and half-hearted. Unemployed people's motivations to seek information about jobs](paper362.html)

#### Karen Nowé, Elena Macevičiūtė and T.D. Wilson  
[Tensions and contradictions in the information behaviour of Board members of a voluntary organization](paper363.html)

#### Nei-Ching Yeh  
[The social construction of the world of gay and lesbian people and their information behaviour](paper364.html)

#### Kirsty Williamson  
[Where information is paramount: a mixed methods multi-disciplinary investigation of Australian online investors](paper365.html)

#### Shunsaku Tamura, Makiko Miwa, Mika Koshizuka, Nozomi Ikeya, Seiichi Saito, Yumiko Kasai, Yasunori Saito and Norihisa Awamura  
[Satisfaction and perception of usefulness among users of business information services in Japan](paper366.html)

#### Eric Thivant and Laïd Bouzidi  
[Analysis of information sources representation for financial product design: new perspectives for information seeking and use behaviour.](paper367.html)

#### Kristina Eriksson-Backa  
[Access to health information: perception of barriers among elderly in a language minority](paper368.html)

#### Isto Huvila  
[The information condition: information resources and use in labour, work and action](paper369.html)

#### Lennart Björneborn  
[Serendipity dimensions and users' information behaviour in the physical library interface](paper370.html)

#### Elena Macevičiūtė and T.D. Wilson  
[Information behaviour in research network building by relocated scholars in Swedish higher education: a report on a pilot project](paper371.html)

#### Birgitta Olander  
[Scholarly communication in transition: computer scientists' information behaviour over twenty years](paper372.html)

#### Spencer C. Lilley  
[Information barriers and Maori secondary school students](paper373.html)

#### Mikko Tanni, Eero Sormunen and Antti Syvänen  
[Prospective history teachers' information behaviour in lesson planning](paper374.html)

#### Lynne (E.F.) McKechnie, Heidi Julien, Shelagh K. Genuis and Tami Oliphant  
[Communicating research findings to library and information science practitioners: a study of ISIC papers from 1996 to 2000](paper375.html)

#### Makiko Miwa and Hideaki Takahashi  
[Knowledge acquisition and modification in students' exploratory Web search processes while career planning](paper376.html)

#### Brendan Luyt, Chia Zuhaila Bte Chia Zainal, Olivia Victoria Petines Mayo, and Tan Siow Yun  
[Young people's perceptions and usage of Wikipedia](paper377.html)

#### David K. Allen, T.D. Wilson, Alastair W.T. Norman and Charles Knight  
[Information on the move: the use of mobile information systems by UK Police Forces](paper378.html)

#### Jeonghyun Kim  
[Perceived difficulty as a determinant of Web search success](paper379.html)

#### Jela Steinerová  
[Seeking relevance in academic information use](paper380.html)

#### Theresa Dirndorfer Anderson and Jo Orsatti  
[Rhythms of being at ISIC - understanding the place of the ISIC conferences in information seeking research](paper381.html)

#### J.V. Rodríguez Jr., F.J. Martínez and J.V. Rodríguez  
[Analysis of the similarity of the responses of Web search engines to user queries: a user perspective](paper382.html)




