#### vol. 13 no. 4, December, 2008

# Analysis of information sources representation for financial product design: new perspectives for information seeking and use behaviour.

#### [Eric Thivant](mailto:Thivant@univ-lyon3.fr) and [Laïd Bouzidi](mailto:bouzidi@univ-lyon3.fr)  
University Lyon 3, IAE, CR MAGELLAN, SICOMOR, Lyon, France

#### Abstract

> **Introduction.** Information sources play a key role for information seeking and use behaviour. We analyse the relationship between information sources and information seeking and use behaviour.  
> **Method.** Within a social psychology framework, we use social representation method and have interviewed seventeen financiers, using a qualitative method that underlines the relationship between information sources, professional contexts and strategies.  
> **Analysis.** The data were analysed using the APRIL resemblance analysis software package to establish the linkages between information sources in the perceptions of the financiers.  
> **Results.** Thus, this study demonstrates that the analysis of information sources in details with the professional context can help us to explain information specific practices.  
> **Conclusions.** These findings have implications for developing new information systems and better understanding information seeking and use behaviour.


## Introduction

This exploratory study proposes a critical analysis of financial product design in order to better understand information seeking behaviour in the light of recent flawed financial product design. Indeed, the current international financial crisis, which began with the US sub-prime residential mortgages problem and has continued with a French derivatives products problem, reveals to us the problem of increasing financial sophistication. This situation should be carefully analysed and it causes us to question the information seeking and use behaviour for financial product development and about the information sources used. This information seeking and use behaviour will rely on specific information sources (economic, financial and marketing reports for example), and the choice of these information sources and the strategies used can explain the actual information seeking and use behaviour in financial product design.

To deal with this problem, we have developed a working hypothesis: 'The analysis of a user's "preferential information sources" can help us to understand their information seeking and use behaviour'. We will test this hypothesis within a specific theoretical framework, such as psycho-social theory, and demonstrate that this theoretical and methodological framework will help us to understand the process of information seeking and use behaviour. In our case, we need first to understand better the relationship between use of existing information sources by a financier and then to associate the sources with the financiers information seeking and use behaviour.

Thus, first, in our present analysis, after discussing the theoretical and methodological frameworks, we will present the psycho-social approaches and more particularly the social representation methodology we will use. Secondly, we will demonstrate our hypothesis with an exploratory study using one aspect of this framework, applied to the financial sector. Thirdly, we will show the result obtained by this approach and its limits, so as to understand better professional information seeking and use behaviour. Consequently, this study wishes to determine the information seeking and use steps for an engineering activity, and more specially financial engineering, within a specific framework and to show that we need more hybrid approaches if we wish to investigate this problem further.

## Choice of the theoretical and methodological framework

### Presentation of existing theoretical frameworks

According to Bates ([2005](#bat05)), there are at least thirteen meta-theories or approaches that have been used more or less easily in information science:

*   historical approach;
*   constructivist approach;
*   constructionist or discourse-analytic approach;
*   philosophical approach;
*   critical approach;
*   ethnographic approach;
*   socio-cognitive approach;
*   cognitive approach;
*   bibliometric approach;
*   physical approach (information theory);
*   engineering approach;
*   user-centred design approach; and
*   evolutionary approach.

But from our point of view, this overview of meta-theories is not complete. For example, some sociological approaches and psycho-sociological approaches are not present here. Moreover some relationships within approaches seem to be difficult to explain. According to Bates ( [2005](#bat05)), Vygotsky, Kelly, Dewey and even Garfinkel (ethno-methodology approach) use nearly the same 'constructivist' approach, but Vygotsky develops a more socio-cognitive approach than Kelly, who uses more psycho-cognitive approach. Also all the relationships between approaches are not very clear. As Bates ([2005](#bat05): 14) recognizes, '_each of the meta-theories above is part philosophy and some part methodology_'. So we should use these frameworks cautiously in our own studies. Of course some approaches are not very easy to classify, such as the sense-making approach, although this could be considered as a socio-cognitive or an informational approach.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: A breakdown of theoretical and methodological frameworks**</caption>

<tbody>

<tr>

<th>Type of approaches</th>

<th>Approaches used by researchers</th>

<th>Authors cited</th>

</tr>

<tr>

<td valign="top">Ethno-methodology approaches</td>

<td valign="top">Grounded theory</td>

<td valign="top">B. Glaser. A. Strauss</td>

</tr>

<tr>

<td valign="top">Psycho-social approaches</td>

<td valign="top">Social representations</td>

<td valign="top">S. Moscovici, D. Jodelet, and W. Doise</td>

</tr>

<tr>

<td valign="top">Psycho-cognitive approaches</td>

<td valign="top">Personal construct</td>

<td valign="top">G. Kelly</td>

</tr>

<tr>

<td valign="top">Constructivism  
Information Science approach</td>

<td valign="top">Sense-making</td>

<td valign="top">B. Dervin</td>

</tr>

<tr>

<td valign="top" rowspan="2">Sociological approaches</td>

<td valign="top">Socio-technical theory</td>

<td valign="top">E. Mumford, R. Bostrom</td>

</tr>

<tr>

<td valign="top">Actor network theory (sociology of translation)</td>

<td valign="top">B. Latour, Callon</td>

</tr>

<tr>

<td valign="top" rowspan="2">Socio-cognitive approach</td>

<td valign="top">Cultural-historical activity theory</td>

<td valign="top">L. Vygotsky, S. Rubinshtein, A. Luria, A. Leont'ev</td>

</tr>

<tr>

<td valign="top">Work centred framework</td>

<td valign="top">A. Pejtersen, R. Fidel</td>

</tr>

</tbody>

</table>

If we look carefully at sense-making for example Tidline said, '_Sense-making is associated with a shift in research emphasis from information sources to information users_' ( [Tidline 2005](#tid05): 113). This analysis is correct, but we should be aware that taking into account the information sources used can help us in our analysis of information seeking and use behaviour.

### Presentation of the social psychology approach and social representation methodology

Researchers in the information science(s) field have used different theoretical and methodological frameworks for decades (see Kuhlthau ([1996](#kuh96)), Ellis ([1993](#ell93), [1997](#ell97)), Cheuk ([1998](#che98)), Fabritius ([1998](#fab98)) and Hjörland ([1997](#hjo97))), in order to understand information seeking and use behaviour and decompose the information seeking activity into several parts. But some theoretical frameworks are not generally used, although they are integrated in the constructivism paradigm. As Allen and Wilson ([2003](#all03)) note, 'Methodology is prior to method and more fundamental, it provides the philosophical groundwork for methods'.

Social representation is a mental and perceptive elaboration process of the reality, which transforms social objects (persons, contexts, situations) into symbolic categories (values, beliefs, ideologies) and give them a cognitive status in our ordinary life. This allows us to focus on our own behaviour inside social interaction in order to understand all aspect of our ordinary life (see Piaget ([2001](#pia01)), Moscovici ([1976](#mos76)), Herzlich ([1969](#her69)), Jodelet ([1984](#jod84))). These social representations are pre-scientific knowledge, called 'common sense' knowledge. Representations are produced and become known in the collective culture. These social representation depend on language used, communication means (interpersonal, institutional and media) and society (social link, ideological context, social inscription and conceptual). As Jodelet said ([1984](#jod84)), representation is practical knowledge, which helps a subject to conceptualize an object and to use it for future action, on the world and on others. Also, '_Une représentation est "générée collectivement" et "partagée par les individus d'un même groupe_"' [A representation is generated collectively and shared by individuals of the same group.] ([Jodelet 1991](#jod91)).

However, in our case and given the formulated hypothesis, the use of these notions supposes some limitations. That is why, even though we can not speak about social representations because the studied object concerns only a number limited of the population (essentially the asset management and marketing services), the financier's representation on this subject exist, because they are concerned by this crucial subject. Besides, methodologies developed by this current framework can be reused here. But we shall describe only representations shared with profession's members.

<div align="center">![Figure 1 : The space of social representation studies](p367fig1.png)</div>

<div align="center">  
**Figure 1 : The space of social representation studies (Source: Jodelet 1991)**</div>

The elaboration of these representations begins with Thêmata, which can be defined as a set of first key ideas, archetypes, deeply embedded in the collective memory of a group. They allow one to emphasise what makes sense in this group. These Thêmata allow us to explain where the representations come from. Following this, for Moscovici and Vignaux ([1994](#mos94b)), thematic structuralization allows one to make _objects_ relevant in the consciousness of this group.

Social representations, in this structural perspective, are elaborated and developed as a result of two additional cognitive processes, the objectivation and the ancrage (or anchoring). The _objectivation process_ ([Rouquette 1994](#rou94)) is a process of information simplification relative to the object and the 'anchoring process' is the social anchoring of the representation. It allows this representation to be attached to something already existing, which is shared by individuals belonging to the same group ([Guimelli 1994](#gui94)).

Finally, according to Abric ([1994](#abr94)), representations are organized with a central core and a peripheral system. The central core, corresponding to the stable part of the representation, is shared collectively by the group ([Moliner 1994](#mol94)). The central core plays a structuring role by generating and by organizing representations. Then, the peripheral elements appear. They are closer to the real object but although they are dependent on the central core, they can also alter the central core. Abric ([1994](#abr94)) summarizes the characteristics of the central system and the peripheral system of a representation: (see Table 2).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Central and peripheral system of a representation**</caption>

<tbody>

<tr>

<th>Central system</th>

<th>Peripheral system</th>

</tr>

<tr>

<td>Linked to collective memory and group's history</td>

<td>Allow experiences accumulation and individual history</td>

</tr>

<tr>

<td>Consensus defines the homogeneity of the group</td>

<td valign="top">Supports group heterogeneity</td>

</tr>

<tr>

<td>Stable  
Coherent and rigid</td>

<td>Flexible  
Supports inconsistency</td>

</tr>

<tr>

<td>Stand up to change</td>

<td>Evolutionary</td>

</tr>

<tr>

<td>Less sensible to immediate context</td>

<td>Sensible to immediate context</td>

</tr>

<tr>

<td valign="top">Functions:  
Produce representation meaning.  
Specify his organization.</td>

<td>Functions:  
Allow adaptation to actual reality.  
Allow content differentiation.  
Protect central system.</td>

</tr>

</tbody>

</table>

## Data collection methods and approaches

### Approaches used

The objective of this research is to understand the information seeking and use behaviour of professional financiers and their relationship with information sources for financial product design. So we analyse the financiers' representation of this subject and try to find recurrent themes or key ideas that can influence their representations (central core and peripheral system).

Abric ([1994](#abr94): 78) advises us to use a multi-method approach. The collection of representations can be realized by methods, either inquiry methods (conversations, questionnaires, inductive boards, drawings and graphic supports, monographic approach), or associative methods (free association, associative cards). Afterwards, an analysis of classic resemblance will allow us to know the degree of structuralization of their representations (other methods can be used, such as the constitution of pairs of words or hierarchical organization of items). Finally, some techniques allow us to be sure of the centrality of the elements of the central system (questioning techniques, inductive methods by ambiguous scenarios, or the cognitive basic schemas method ([Guimelli 1994](#gui94): 83).

We use the analysis of resemblance approach ([Flament 1994](#fla94): 1995). In order to simplify, the resemblance analysis allows us to study different associations between lexical items that are part of the subjects' representation. This resemblance analysis is an indicator of the way a subject organizes representation for the design of the financial products.

### Data collection method

We interviewed seventeen persons, in different levels of responsibility and in eight banks of different kinds mainly between 2000-2003\. In order to know their representations, we use the constructivist paradigm and chose specific tools for our analysis already used as a part of the current trend of social representations (see [Jodelet 1991](#jod91)).

A questionnaire was drafted which consisted of two parts: the first part of the questionnaire included fifteen open questions, which allowed us to verify the importance of information items used by the financial services, and to specify more precisely the searching and monitoring behaviour (as Bates defined in 2002) of financiers. The second part of the questionnaire (the most significant) uses the methodology of social representations in order to obtain a map of information items necessary for the design of financial products. This method describes representations shared among financiers and helps us to think about practices shared by the designers. However, we cannot use the term social representation because our sample size is too small, unlike Roussiau ([1998](#rou98)) who worked on the representations of money with a bigger sample.

### Terminology of the financiers

The first interest of this research is to better define the terminology used by the financial intermediary. We want to establish links between terms or information items and we want to obtain a terminological network's cartography. This global vision of items cartography in relation with others (by adding answers), allows us to obtain a collective and synchronic representation of the 'design of financial product' phenomenon, for the financial intermediaries. The main question is the following one: Where do the ideas come from in order to create a new financial product?

In this question, the financiers connect two categories of different items. On one side, we have the information sources: Official meetings, Financial press, Public general press, Market Studies, Confidential letters, Mailing & News Bulletin, Internal working groups, Internet and Personal contacts. On the other side, we have a group representing the deep-seated motives for designing a new financial product: Rules and laws, Fiscal policy, Competition, Customers satisfaction, Shareholders' demand, Economical Situation, Commercial, Bank profitability.

Every financier lists his preferences for different information items, and we can make the synthesis and obtain a collective lexical organization. Here are the various reserved items:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Information sources used by a financier in product design**</caption>

<tbody>

<tr>

<th>Information sources</th>

<th>Motives</th>

</tr>

<tr>

<td>1\. Financial product design</td>

<td>1\. Fiscal policy</td>

</tr>

<tr>

<td>2\. General public press</td>

<td>2\. Market studies and marketing reports</td>

</tr>

<tr>

<td>3\. Confidential letters</td>

<td>3\. Rules and laws</td>

</tr>

<tr>

<td>4\. Official meetings</td>

<td>4\. Customers satisfaction</td>

</tr>

<tr>

<td>5\. Mailing and news bulletins</td>

<td>5\. Bank profitability</td>

</tr>

<tr>

<td>6\. Financial press</td>

<td>6\. Shareholders' demand</td>

</tr>

<tr>

<td>7\. Internal working groups</td>

<td>7\. Competition</td>

</tr>

<tr>

<td>8\. Personal contacts</td>

<td>8\. Economic situation</td>

</tr>

<tr>

<td>9\. Internet</td>

<td>9\. Salespersons</td>

</tr>

</tbody>

</table>

In order to synthesize the links between these items, we employed resemblance analysis, using the APRIL software developed by CNRS ([Silem _et al._ 1987](#sil87): 127; [Silem _et al._ 1984](#sil84)).

## The main findings of this research: financiers' information sources

With this specific methodology and our sample, we can build a personal typology from the various information sources used by our financiers and we can obtain a more complete representation of every proposed item. Indeed, in this activity, two types of information dominate. There is the information for action and the information for constraint which oblige the designer to modify his design project, but which is bound to this design.

### The central system of the financier's representation

In this central system we find two different kind of central information: _information for action_, i.e.l information which allows us to act strategically (for example following customers needs, the competition or the shareholders' demand); and _information for constraint_, i.e., information which allows us not to commit malpractices as a rule or as a fiscal system.

#### Information for actions 'that are detonators of the financial products process design'.

Marketing studies are key information for designers who want to make sure of the interest before introducing a new financial product. Moreover, the existence of very strong links between the item's _Market studies_ and the item's _Financial product design_ is demonstrated here (see below Figure 1). But other information, such as _Customer satisfaction_, is important also.

<div align="center">![Figure 2: Word graph Market studies and marketing report](p367fig2.png)</div>

<div align="center">  
**Figure 2: Word graph _Market studies and marketing report_ (by class)**</div>

<div align="center">![Figure 3: Word graph Customer satisfaction](p367fig3.png)</div>

<div align="center">  
**Figure 3: Word graph _Customer satisfaction_ (by class)**</div>

We should note that the link between items _Customer satisfaction_, _Market studies_ and _Financial product design_ are very close (see Figures 2 and 3). _Fiscal policy_ should not be ignored for the financial product design (see Figures 4 and 5).

<div align="center">![Figure 4: Word graph Rules, regulations and laws](p367fig4.png)</div>

<div align="center">  
**Figure 4: Word graph _Rules, regulations and laws_ (by number of answers)**</div>

<div align="center">![Figure 5: Fiscal policy](p367fig5.png)</div>

<div align="center">  
**Figure 5: _Fiscal policy_ (by number of answers)**</div>

_Competition_ and _Economic situation_ should be taken into account too (see Figures 6 and 7).

<div align="center">![Figure 6: Word graph Competition](p367fig6.png)</div>

<div align="center">  
**Figure 6: Word graph _Competition_ (by class)**</div>

<div align="center">![Figure 7: Economic situation](p367fig7.png)</div>

<div align="center">  
**Figure 7: _Economic situation_ (by number of answers)**</div>

#### Information for constraint

This kind of information is important, but it does not always result from a direct and active search by the financier. This kind of information helps financiers to build new products in conjunction with other services of the establishment. We distinguish two kind of information, effective constraint information which blocks or changes the initial design project (for example internal working group, bank profitability and sellers), and the non-effective constraint information, which constitutes real challenges (documentation services, computer services, etc.). For example, new software should be developed in order to deal with these sophisticated financial products. Moreover, it's difficult to explain all the details of a new financial product (see the

The design of a new financial product is a collective project and the Internal working group is essential for validating the design of the coquille (or shell) and the internal rules of the product (see Figure 8).

<div align="center">![Figure 8: Word graph Internal working group](p367fig8.png)</div>

<div align="center">  
**Figure 8: Word graph _Internal working group_ (by class)**</div>

We should not neglect the financial factor (_Bank profitability_, see Figure 9) and the human factors (_Salespersons_, see Figure 10) which have an impact upon _Financial product design_.

<div align="center">![Figure 9: Word graph Bank profitability](p367fig9.png)</div>

<div align="center">  
**Figure 9: Word graph _Bank profitability_ (by number of answers)**</div>

<div align="center">![Figure 10: Word graph Salespersons](p367fig10.png)</div>

<div align="center">  
**Figure 10: Word graph _Salespersons_ (by number of answers)**</div>

### Peripheral system of the financier's representation

We observe that information sources are not used directly to create new financial products (the item Financial product design is not very often linked with other direct information sources). Only three information sources can be considered important: _Market studies_ (already described above) which can be considered as an information source; _General public press_, which gives us a state of the world (see Figure 11) and _Personal contacts_, which show us the importance of interpersonal communication (see Figure 12).

<div align="center">![Figure 11 : Word graph General public press ](p367fig11.png)</div>

<div align="center">  
**Figure 11 : Word graph _General public press_ (by number of answers)**</div>

<div align="center">![Figure 12: Word graph Personal contacts](p367fig12.png)</div>

<div align="center">  
**Figure 12: Word graph _Personal contacts_ (by number of answers)**</div>

Other information sources are used with more caution. They are considered as secondary sources, and they are less relevant sources of information than financial publications, mailings, confidential letters and (public) internet.

## Final results

This research underlines financiers' information priorities and choices. It allows us to extend our research into specific information sources that are dominant in the process of the design of financial products. By observing the results of this inquiry, we notice some interesting results, but these need to be augmented by further investigation.

This study shows us the importance of elaborated information, used for actions. We see the importance of using marketing information to bring about customer satisfaction, in order to conform to the new financial pulling and to respond to the actions of competitor. This information for action is directly balanced by constraining information such as commercial ideas, working group proposals inside the Bank or the Bank's profitability (see Figure 13). This constraining information is more or less taken into account.

<div align="center">![Figure 13: Global word graph Financial product design](p367fig13.png)</div>

<div align="center">  
**Figure 13: Global word graph _Financial product design_ (by class)**</div>

If we continue our investigation, we discover the importance of the General public press and Personal contacts for financial product design (see Figure 14).

<div align="center">![Figure 14: Global word graph Financial product design with more details.](p367fig14.png)</div>

<div align="center">  
**Figure 14: Global word graph _Financial product design_ with more details.**</div>

This graph shows us the importance of information resulting from networks (networks of commerce, of customers) and modern communication means (financial publications and the general public press).

This study also pushes aside traditional sources of information. They are not only the sources of financial or specialized information that dominate in the design of new financial products, but they are also the sources of non-specialized information and the marketing reports. Financial information is considered by the profession only as a mirror of itself and a light on the current situation in their daily lives. Moreover, we find that the oral or non-written documents play a more important role than the electronic document in the financial product design. For example how financial managers can explain sophisticated products such as collateralised mortgage obligations (CMO), collateralized debt obligations (CDO), collateralized synthetic obligations, CDO (CDo of CDO), sometimes detailed with 100 pages of documentation, to sales and marketing persons and customers.

This study has the benefit of enlightening our understanding of this profession using a new perspective. Of course, this research was only exploratory and gives an outline of the reality at the moment, in a synchronous way, like a cliche (the phenomenon of the Internet modifies each information source and it has not been made particularly visible here). However, the work allows us to present the usefulness of the 'representations' concepts in the financial sector for designing financial products and services. It contributes to the analysis of this design process

# Conclusion

In summary, this study shows us that this social-psychological approach can be used in order to understand information seeking and use behaviour and the information sources used. Even though our data sample was not significant, this methodology helps us to identify the separate information sources used by financiers. These information sources are distinctly different and indicate the diversity of search strategy. Inside the central system of the financier's representation, a direct and active search is used in order to find information for action and a more passive and undirected search is used for information for constraint (for definition see Bates 2002).

Thus, we have demonstrated our hypothesis, i.e., the deep analysis of professional and preferential information sources can help us to specify information seeking and use behaviour. Indeed, this study was interested mainly in studying how the designers of new financial products see their world and build new connections with information and, in a general way, how financiers think about the design of new financial products within their establishments.

Nevertheless as Henneron, Metzger, Palermiti and Polity said,

> Dans le cas bien particulier d'un opérateur engagé dans une activité professionnelle, nous nous interrogeons sur la place qu'occupent les processus informationnels dans cette activité et sur la manière dont les acteurs s'informent. [In the specific case of a trader engaged in a business, we question the role of information processes in this activity and how players inform themselves.] (Henneron _et al._1999: 13-14),

We should develop other frameworks in order to take into account the 'information which the user looks for and uses in his activity'. This study allows us to show the importance of elaborated information, (working group prefer to use their internal marketing report and customers' satisfaction survey )., but it does not allow us at this stage to model completely this search activity.

In order to model this complete activity, it would be necessary to determine more exactly professional representations for action, as well as the designers' deep motivations. It would also be necessary to analyse the socio-cognitive and informative process of design and decision, in order the better to understand reasoning, practices and steps of information seeking and use behaviour.

## Acknowledgements

## References

*   <a id="all03" name="all03"></a>Allen, D.K. & Wilson, T.D. (2003). Information overload: context and causes. New Review of Information Behaviour Research, 4(1), 31-44.
*   <a id="abr94b" name="abr94b"></a>Abric, J.-C. (1994). L'organisation interne des représentations sociales. [The internal organization of social representations.] In Guimelli, C. (Ed.), _Structures et transformations des représentations sociales_. (pp. 73-84). Paris: Delachaux et Niestlé.
*   <a id="abr94" name="abr94"></a>Abric, J.-C. (1994). Méthodologie de recueil des représentations socials. [Methodology for the collection of social representations.] In J.C. Abric (Ed.). _Pratiques sociales et représentations._ (pp. 59-82). Paris: P.U.F.
*   <a id="bat02" name="bat02"></a>Bates, M. J. (2002). Towards an integrated model of information seeking and searching. _The New Review of Information Behaviour Research_, **3**, 1-16).
*   <a id="bat05" name="bat05"></a>Bates, M. J. (2005). An introduction to metatheories, theories and models. In K. Fisher, S. Erdelez & L.E.F. McKechnie (Eds.), _Theories of information behaviour_ (pp. 1-24). Medford, NJ: Information Today.
*   <a id="che98" name="che98"></a>Cheuk, B. W.-Y. (1998). Exploring information literacy in the workplace: a qualitative study of engineers using the sense-making approach. _International Forum on Information and Documentation_, **23**(2), 30-38.
*   <a id="der99" name="der99"></a>Dervin, B. (1999). On studying information seeking methodologically: the implications of connecting metatheory to method. _Information Processing and Management_, **35**(6), 727-750.
*   <a id="der03" name="der03"></a>Dervin, B. (2003). _A sense-making methodology primer: what is methodological about sense-making?_ Paper presented at the meeting of the International Communication Association, San Diego, CA., USA/
*   <a id="doi95" name="doi95"></a>Doise, W., Clemence, A. & Lorenzi Cioldi, F. (1995). _Représentations sociales et analyses de données._ [Social representations and data analysis.] Grenoble, France: Presses Universitaires de Grenoble.
*   <a id="ell93" name="ell93"></a>Ellis, D., Cox, D. & Hall, K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences. _Journal of Documentation_, **49**(4), 356-369.
*   <a id="ell97" name="ell97"></a>Ellis, D. & Haugan, M. (1997). Modelling the information seeking pattern of engineers and research scientists in an industrial environment. _Journal of Documentation_, **53**(4), 384-403.
*   <a id="fab98" name="fab98"></a>Fabritus, H. (1998). [Information seeking in the newsroom: application of the cognitive framework for analysis of the work context](http://www.webcitation.org/5ctfXAWlC). _Information Research_, **4**(2), Retrieved 30 May, 2008 from http://informationr.net/ir/4-2/isic/fabritiu.html (Archived by WebCite® at http://www.webcitation.org/5ctfXAWlC)
*   <a id="fis05" name="fis05"></a>Fisher, K., Erdelez, S. & Mckechnie, L.E.F. (Eds.). (2005). _Theories of information behavior._ Medford, NJ: Information Today.
*   <a id="fla94" name="fla94"></a>Flament, C. (1994). Structure, dynamique et transformation sans rupture d'une représentation sociale. [Structure, dynamics and seamless transformation of a social representation.] In J.C. Abric (Ed.), _Pratiques sociales et représentations_. (pp. 37-58). Paris, PUF.
*   <a id="gui94" name="gui94"></a>Guimelli, C. (1994). La fonction d'infirmière pratiques et representations sociales. [The function of nursing practices and social representations] In J.C. Abric (Ed.). _Pratiques sociales et représentations._ (pp. 83-107). Paris : PUF.
*   <a id="gui94b" name="gui94b"></a>Guimelli, C. (1994). Présentation de l'ouvrage. [Abstract] In C. Guimelli (Ed.), _Structures et transformations des représentations sociales_ (pp.11-24). Paris: Delachaux et Niestlé.
*   <a id="gri87" name="gri87"></a>Grize J.-B., Vergès P., Silem A. (1987). _Salariés face aux nouvelles technologies: vers une approche socio-logique des représentations sociales_. [Employees face new technologies: towards a socio-logical approach to social representations.] Paris: Éd. du CNRS.
*   <a id="hen99" name="hen99"></a>Henneron G., Metzger J.-P., Palermiti R. & Polity Y. (1999). _[Activité et information: vers un système informatique d'accompagnement et d'assistance.](http://www.webcitation.org/5ctge0Fdc)_ [Activity and information: towards an informatics system for support and assistance] Grenoble, France: Agence Rhône-Alpes en Sciences Sociales et Humaines. Retrieved 8 December, 2008 from http://www.iut2.upmf-grenoble.fr/RI3/Mise_jour_06/Arassh_99.pdf. (Archived by WebCite® at http://www.webcitation.org/5ctge0Fdc)
*   <a id="her69" name="her69"></a>Herzlich, C. (1969). _Santé et maladie, analyse d'une représentation._ [Health and disease, analysis of a representation.] Paris: Mouton.
*   <a id="hjo97" name="hjo97"></a>Hjørland, B. (1997). _Information seeking and subject representation: an activity-theoretical approach to information science._ Wesport, CT: Greenwood.
*   <a id="hou98" name="hou98"></a>Houde, O. (1998). Introduction à la psychologie cognitive. [Introduction to cognitive psychology.] In O. Houde, D. Keyser, O. Koenig (Eds.), _Vocabulaire des sciences cognitives_. Vendôme, France: PUF.
*   <a id="kuh93" name="kuh93"></a>Kuhlthau, C.C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation_, **49**(4), 339-355.
*   <a id="jar03" name="jar03"></a>Järvelin, K. & Wilson, T.D. (2003). [On conceptual models for information seeking and retrieval research.](http://www.webcitation.org/5cth2UT0N) _Information Research_, **9**(1), paper 163\. Retrieved 30 May, 2008 from http://InformationR.net/ir/9-1/paper163.html (Archived by WebCite® at http://www.webcitation.org/5cth2UT0N)
*   <a id="jod84" name="jod84"></a>Jodelet, D. (1984). Réflexions sur la notion de représentation sociale en psychologie sociale. [Reflections on the idea of social representation in social psychology.] _Communication Information_, **6**(2/3), 15-41.
*   <a id="jod91" name="jod91"></a>Jodelet, D. (Ed.). (1991). _Les représentations sociales._ [Social representations.] Vendôme, France: PUF.
*   <a id="kel63" name="kel63"></a>Kelly, G.A. (1963). _A theory of personality, the psychology of personal constructs_. New York, NY: Norton & Company.
*   <a id="lec05" name="lec05"></a>Leckie, G. J. (2005). General model of the information seeking of professionals. In K. Fisher, S. Erdelez & L.E.F. McKechnie (Eds.), _Theories of information behaviour_ (pp. 158-163). Medford, NJ: Information Today.
*   <a id="man98" name="man98"></a>Mannoni, P. (1998). _Les représentations sociales._ [Social representations.] Vendôme, France: PUF.
*   <a id="mol94" name="mol94"></a>Moliner, P. (1994). Les méthodes de repérage et d'identification du noyau. [Methods for the location and identification of the core.] In C. Guimelli (Ed.), _Structures et transformations des représentations sociales_ (pp. 199-224). Paris: Delachaux et Niestlé
*   <a id="mos76" name="mos76"></a>Moscovici, S. (1976). _La psychanalyse, son image et son public._ (2nd ed.) [Psycholanalysis, its image and its public.] Paris: Presses Universitaires de France.
*   <a id="mos94b" name="mos94b"></a>Moscovici, S. & Vignaux, G. (1994). Le concept de thêmata. [The concept of thêmata.] In C. Guimelli (Ed.), _Structures et transformations des représentations sociales._ (pp. 25-72) Paris: Delachaux et Niestlé.
*   <a id="pej98" name="pej98"></a>Pejtersen, A. & Fidel, R. (1998). _A framework for work centered evaluation and design: a case study of IR on the Web_. Paper presented at the Multimedia Information Retrieval Applications (MIRA) Workshop, Grenoble, France.
*   <a id="pia01" name="pia01"></a>Piaget, J. (2001). _Psychologie de l'intelligence._ [Psychology of intelligence.] Saint-Amand, France: Armand Collin.
*   <a id="rou98" name="rou98"></a>Roussiau, N. (1998). Représentation sociale de l'argent. In C.Roland-Levy & P. Adair (Eds), _Psychologie economique_. [The social representaton of money.] (pp. 69-79). Paris: Economica
*   <a id="sil84" name="sil84"></a>Silem A., Glady M. & Martinez G. (1984). Stratégies de communication internes et défis des nouvelles technologies dans l'entreprise. [Internal communication strategies and challenges of new technologies in the enterprise.] Rapport Final pour l'action concertée "vie sociale dans l'entreprise "
*   <a id="tid05" name="tid05"></a>Tidline, T.J. (2005) Dervin's sense-making. In K. Fisher, S. Erdelez & L.E.F. McKechnie (Eds.), _Theories of information behaviour._ (pp. 113-117). Medford, NJ: Information Today.
*   <a id="wil99" name="wil99"></a>Wilson, T.D. (1999) Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-269.


