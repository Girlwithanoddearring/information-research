#### vol. 13 no. 4, December, 2008

# Serendipity dimensions and users' information behaviour in the physical library interface

#### [Lennart Björneborn](mailto:lb@db.dk)

Royal School of Library and Information Science, Copenhagen, Denmark

#### Abstract

> **Introduction.** Outlines an exploratory study concerned with the types of information behaviour users employ to find materials in a public library. Special focus was on what dimensions in the physical library may affect possibilities for serendipity. The overall aim of the study was to develop a conceptual framework including models to describe users' interaction with library spaces.  
> **Method**. The study took place at two Danish public libraries during 10 months in 2006\. Naturalistic observation of users' information behaviour was supplemented with qualitative interviews with 113 users including think-aloud sessions with eleven users.  
> **Analysis.** Data from observations and interviews were transcribed and analysed in an iterative process of categorization and condensation.  
> **Results**. Observations and interviews in the study resulted in a model of different ways of finding library materials using and combining different types of convergent (goal-directed) and divergent (explorative) information behaviour. Ten dimensions in the physical library that may affect possibilities for serendipity were identified in the study. The paper introduces a conceptual framework suggesting that libraries can be viewed as integrative interfaces comprising all contact surfaces and mediation flows between users and library resources, whether human, physical or digital.  
> **Conclusion**. The typology of convergent and divergent information behaviour and the identified serendipity dimensions may have implications for how the integrative interface of public libraries could be designed to facilitate both forms of behaviour.

## Introduction

This paper outlines an exploratory study concerned with what types of information behaviour users employ in order to find materials in a public library: How do users find what they find? Special focus was on identifying dimensions in the physical library that may affect possibilities for serendipity, that is, when users find materials and information not planned for (e.g., [Foster and Ford 2003](#Foster); [Van Andel 1994](#VanAndel)). An underlying incentive in the study was a realization that in order to design physical spaces in public libraries that may attract users and prevent _library bypass_, it is important to know more about library users' information behaviour.

The overall aim of the study was to develop a conceptual framework including models to describe users' interaction with library interfaces. In this framework, elaborated in a section further below, the library is viewed as an integrative interface comprising all contact surfaces and mediation flows between users and available library resources. These information resources, whether human, physical or digital, can be looked upon as an integrated whole; as supplementary and supportive parts.

Users' information behaviour in public libraries has been investigated in different ways over the past decades. There have been surveys of browsing behaviour in public libraries (e.g., [Goodall 1989](#Goodall)) and studies of social activity and information behaviour in public libraries (e.g., [Given and Leckie 2003](#Given); [Leckie and Given 2005](#Leckie)). There has also been increasing attention on how to improve the design and user-friendliness of physical libraries (e.g., [Buschman and Leckie 2007](#Buschman); [Lushington 2002](#Lushington)). However, Leckie and Given ([2005](#Leckie)) point to the need for more studies on how information seeking is carried out in physical libraries. Further, there is a lack of studies of dimensions in physical libraries affecting possibilities for serendipity (hereafter: serendipity dimensions).

With this as background, the two main research questions in the study were:

1.  What types of information behaviour do users employ to find materials in public libraries?
2.  What dimensions in physical public libraries may affect possibilities for serendipity?

## Methodology

Two Danish public libraries were selected as cases in the study. The Copenhagen Main Library, Hovedbiblioteket (hereafter HB), which dates from the early 1990s, has five floors, about 760,000 yearly visitors and an annual circulation of approximately 1.2 million items in 2006\. As a contrast a new-built Copenhagen suburban branch library in Vanløse (VB) has one big room, around 220,000 yearly visitors and an annual circulation of approximately 300,000 items in 2006.

The empirical data were collected by two researchers during a ten month period in 2006 employing methods including:

*   naturalistic observation of users' information behaviour in the physical public library; and
*   qualitative interviews and think-aloud sessions with library users (What did they intend to find? What did they actually find? How did they find it?)

In the two libraries, 113 users were interviewed (HB 73 and VB 40). The interviewees were strategically selected on different weekdays and hours during ten months in order to include both sexes and covering all ages (over 15 years). Users were asked for a short interview about their information behaviour when looking for materials in the library or at the lending automat (self-issue machine). Users were unobtrusively observed before being interviewed. Further, there were naturalistic observations of specific spots in the library and whole library sections. About 10% of the interviewees (HB 7 and VB 4) walked through the library with one of the researchers and gave think-aloud comments and reflections on what triggered their attention and information behaviour. Data from observations and interviews were transcribed and analysed in an iterative process of categorizing and condensing.

Focus in the study was not on generating generalizable results but on employing the methods in an exploratory way in order to identify phenomena and generate hypotheses with regard to serendipity dimensions and users' information behaviour in the physical library interface.

## Results and discussion

Results in the study are discussed in four subsections below. The first subsection presents a model of identified types and combinations of users' convergent (goal-directed) and divergent (explorative) information behaviour in the study. The next subsection outlines ten identified dimensions in physical library interfaces that may affect possibilities for serendipity. In order to put the serendipity dimensions and the library users' information behaviour into a broader and more holistic context, the third subsection introduces a conceptual framework suggesting that libraries can be viewed as integrative interfaces comprising all contact surfaces and mediation flows between users and available library resources, whether human, physical or digital. The last subsection discusses how physical and digital library interfaces provide different affordances, i.e. usage potentials, for user interaction and serendipity.

### Convergent and divergent information behaviour

Analysis of interviews with users at the two investigated libraries showed how different users' information behaviour can be from library visit to library visit and how interests, needs and behaviour can be influenced by life circumstances; both crucial ones like unemployment, divorces and births and everyday ones like rainy weather, an upcoming holiday or lack of energy or time.

In the study, a pattern emerged where interviewed users in their own words differentiated between on the one hand _goal-directed_ searches for materials for work tasks, studies, hobbies, etc. and on the other hand _enjoyable_ and _pleasurable_ browsing for materials for inspiration, experience and relaxation.

Inspired by Ford's ([1999](#Ford)) discussion on convergent and divergent information processing in information retrieval systems, the terms _convergent_ and _divergent_ were used in the study to conceptualize the two main types of information behaviour; convergent goal-directed behaviour and divergent explorative behaviour, see Table 1\. (The differentiation between _information recovery_ and _information discovery_ in Table 1 is inspired by Garfield ([1986](#Garfield1986)) using the terms as complementary aspects in citation indexes. In this context, Garfield ([2004](#Garfield2004)) interestingly talks about _systematic serendipity_ as an essential outcome of such indexes.)

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="5" cellspacing="0" width="75%"><caption align="bottom">**Table 1: _Ideal-type_ aspects of convergent and divergent information behaviour**</caption>

<tbody>

<tr>

<th colspan="2" width="100%">![small figure](p370fig2.jpg)</th>

</tr>

<tr>

<th>Convergent information behaviour</th>

<th>Divergent information behaviour</th>

</tr>

<tr>

<td valign="top">⇒ 'left brain' ⇒ goal-directed, focused, rational ![converge](p370fig4.jpg) ⇒ e.g., Boolean searches, known items ⇒ conscious, explicit information needs ⇒ problems, work tasks ⇒ 'information recovery'</td>

<td valign="top">⇒ 'right brain' ⇒ explorative, impulsive, intuitive ![diverge](p370fig6.jpg) ⇒ e.g., browsing, serendipity ⇒ subconscious, implicit information needs ⇒ interest space, curiosity, pleasure ⇒ 'information discovery'</td>

</tr>

</tbody>

</table>

Table 1 outlines dichotomous _ideal-type_ aspects of convergent and divergent information behaviour. _Convergent_ information behaviour is goal-directed, focused and rational (<span style="font-style: italic;">left brain</span>), e.g., by applying Boolean search strings in known-item searches. This behaviour meets conscious, explicit information needs typically based on specific problems and work tasks. In contrast, _divergent_ information behaviour is explorative, impulsive, intuitive (<span style="font-style: italic;">right brain</span>), e.g., when browsing and experiencing serendipity. This behaviour may reflect more subconscious, implicit and muddled ([Ingwersen 1992](#Ingwersen)) information needs driven by pleasure, curiosity and the user's _interest space_ (Figure 1).

<div align="center">![Figure 1: A user encounters potentially triggering items B, C, D - matched by user's interest space 'iceberg' in user's head) - while moving through an information space searching for item A](p370fig8.jpg)</div>

<div align="center">**Figure 1: A user encounters potentially triggering items B, C, D - matched by user's interest space 'iceberg' in user's head) - while moving through an information space searching for item A**</div>

The user's interest space is simplistically illustrated in Fig. 1 by an _iceberg_ in the user's head. The top of the _iceberg_ covers a small part conscious needs (A). Under the _water_ is the larger part of the _iceberg_ containing the user's subconscious interests (B-D). These latent interests, that may be small or big, can be triggered when the user moves through an information space (library, Web, city, etc.) and encounters options and opportunities afforded by this space.

In real life, the two ideal types of behaviour in Table 1 are mixed, supplement and succeed each other as alluded to in the _behavioural pulse_ at the top of Table 1\. In the study, observations and interviews revealed combinations of known-item searches and browsing, i.e., combinations of convergent and divergent information behaviour. For example, we interviewed users, who started with a convergent search in the online catalogue from their computer at home to check whether and where specific materials were available in the physical library. Subsequently, visiting the physical library, users also browsed for other materials both close to the located material and further away in other sections of the library. Digital and physical library spaces thus supplement each other.

The study showed how convergent, goal-directed behaviour can interplay with divergent, explorative behaviour and how they can alternate at the same library visit. Convergent information behaviour may thus identify central points of information that subsequently function as points of departure for more divergent behaviour. The reverse case was also found in the study, when serendipitously encountered information lead to a need for more focused and convergent search strategies. For example, an interviewee looked for bound volumes of music by the Danish composer Bent Fabricius-Bjerre. On the same shelf he also found sheet music by Bette Midler probably alphabetically misshelved by another user. The user picked up this material as well and then searched goal-directedly for CDs with Midler's music

When users move through an information space they may thus change direction and behaviour several times as their information needs and interests may develop or get triggered depending on options and affordances encountered on their way through an information space. This finding is related to research on multi-tasking information behaviour and information task switching ([Spink 2004](#Spink)) and how information seeking may be a _bit-at-a-time_ activity resembling berry-picking ([Bates 1989](#Bates)).

Convergent and divergent information behaviour have parallels to shopping behaviour where buying necessary commodities for daily housekeeping can alternate with pleasant shopping and impulse-driven purchases ([Underhill 1999](#Underhill)). Other parallels to shopping behaviour are the problems that library users may experience by the sheer number of possible choices. In the study, we observed users in obvious self-negotiations trying to decide '_Will I, won't I_'; walking back and forth to found materials, examining, putting back, walking away, returning, looking again, before deciding.

<div align="center">![Figure 2\. Identified ways of finding library materials](p370fig10.jpg)</div>

<div align="center">**Figure 2: Identified ways of finding library materials using and combining different types of convergent and divergent information behaviour, _cf._ Table 2.**</div>

Figure 2 and Table 2 show different ways of finding library materials using and combining different types of convergent, goal-directed information behaviour and divergent, explorative behaviour identified in the study. The figure includes overlaps, i.e., combinations, between the different ways of finding materials in the library (see Table 2). For example, the overlap between _favourite spot_, _substitute_, _supplement_ and _systematic browsing_ corresponds to users who do not find the desired material in their favourite library spot and replace it with a substitute finding at the same spot and supplement this substitute finding with systematic browsing for other materials at the same favourite spot in the library. A typical example of a favourite spot in the study was shelves with detective stories visited by users every time they visited the library.

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">**Table 2: Identified finding types, i.e., ways of finding library materials**</caption>

<tbody>

<tr>

<th>Finding types</th>

<th>Explanation</th>

</tr>

<tr>

<td>Planned finding</td>

<td>Users find what they planned to find, known-item searches, by convergent, goal-directed behaviour.</td>

</tr>

<tr>

<td>Favourite spot finding</td>

<td>Some users goal-directedly go to the same few favourite spots every time they visit the library.</td>

</tr>

<tr>

<td>Substitute finding</td>

<td>Sometimes users do not find the planned materials and replace them with similar materials as substitutes.</td>

</tr>

<tr>

<td>Supplement finding</td>

<td>Users may supplement planned or substitute findings with findings through different types of browsing.</td>

</tr>

<tr>

<td>Systematic browsing</td>

<td>Users move systematically through library sections and scan displayed library materials.</td>

</tr>

<tr>

<td>Impulsive browsing</td>

<td>Users move more unsystematically through the library with impulse-driven behaviour including incidental encounters of materials.</td>

</tr>

<tr>

<td>Incidental encounters</td>

<td>Occasional discoveries of interesting-looking but unplanned materials on the user's way through the library.</td>

</tr>

</tbody>

</table>

Research on browsing behaviour has revealed different types of browsing (e.g., [Chang and Rice 1993](#Chang)). The two types of browsing in Table 2 should thus be seen as two ends of a continuum with intermediary types as indicated by the blurred transition between the two types in Figure 2.

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="50%"><caption align="bottom">**Table 3: Distribution of how interviewed users at Copenhagen Main Library (HB) and Vanløse Library (VB) found materials**</caption>

<tbody>

<tr>

<th>Finding types</th>

<th>HB</th>

<th>%</th>

<th>VB</th>

<th>%</th>

</tr>

<tr>

<td>Only convergent</td>

<td align="center">37</td>

<td align="center">50.7</td>

<td align="center">19</td>

<td align="center">47.5</td>

</tr>

<tr>

<td style="text-indent: 10px;">including planned</td>

<td align="center">(23)</td>

<td align="center">(31.5)</td>

<td align="center">(10)</td>

<td align="center">(25.0)</td>

</tr>

<tr>

<td style="text-indent: 10px;">including favourite</td>

<td align="center">(10)</td>

<td align="center">(13.7)</td>

<td align="center">(4)</td>

<td align="center">(10.0)</td>

</tr>

<tr>

<td style="text-indent: 10px;">including substitute</td>

<td align="center">(4)</td>

<td align="center">(5.5)</td>

<td align="center">(5)</td>

<td align="center">(12.5)</td>

</tr>

<tr>

<td>Supplement</td>

<td align="center">29</td>

<td align="center">39.7</td>

<td align="center">16</td>

<td align="center">40.0</td>

</tr>

<tr>

<td style="text-indent: 10px;">including planned</td>

<td align="center">(25)</td>

<td align="center">(34.2)</td>

<td align="center">(13)</td>

<td align="center">(32.5)</td>

</tr>

<tr>

<td style="text-indent: 10px;">including favourite</td>

<td align="center">(1)</td>

<td align="center">(1.4)</td>

<td align="center">(0)</td>

<td align="center">0.0</td>

</tr>

<tr>

<td style="text-indent: 10px;">including substitute</td>

<td align="center">(3)</td>

<td align="center">(4.1)</td>

<td align="center">(3)</td>

<td align="center">(7.5)</td>

</tr>

<tr>

<td>Only divergent</td>

<td align="center">7</td>

<td align="center">9.6</td>

<td align="center">5</td>

<td align="center">12.5</td>

</tr>

<tr>

<td></td>

<td align="center">73</td>

<td align="center"> </td>

<td align="center">40</td>

<td></td>

</tr>

</tbody>

</table>

As shown in Table 3, around 51% of the 73 interviewed users at the main library (HB) found materials by engaging in only convergent, goal-directed behaviour (including substitutes and favourite spots). Supplementary findings constituted 40% and interviewees with only divergent, explorative behaviour were 10%. Corresponding numbers for the 40 interviewees at VB were 48%, 40% and 12%. The exploratory study thus showed that about 50% of the interviewees at both libraries were goal-directed only and the other about 50% were open to unplanned discoveries. The small strategic and non-random sample means this finding is not generalizable. It would be interesting to validate in a larger and representative study. However, this finding points to the importance of libraries to meet both convergent and divergent information behaviour of users when designing library spaces.

### Dimensions affecting serendipity

How can the design of library spaces support users' convergent and divergent information behaviour? Libraries have developed a wide range of tools to support users' _convergent_ information behaviour, for example, library catalogues, classification systems, shelves, etc. In this study, the focus was on what dimensions in the physical library may correspondingly support users' _divergent_ information behaviour, or more specifically, affect affordances for serendipity when users find materials and information not planned for.

Analysis of observations, interviews and think-aloud sessions in the study suggests that the following ten dimensions in the physical library may trigger users' interest spaces and affect affordances for serendipity:

_Unhampered access._ Crucial for stimulating affordances for serendipity is the users' unhampered and direct access to information resources, whether human, physical or digital in the library. If information resources are hidden behind counters, in closed stacks, in endless Web lists, etc., possibilities for serendipity are restricted.

_Diversity_ is another crucial serendipity dimension in the library: How rich is the diversity of topics, genres, resources and activities that users may encounter during a library visit? Not many other places in society contain so many topics in so relatively small an area as public libraries. The topical diversity spans the whole knowledge universe of mankind and may thus trigger the diversity of individuals' interest spaces (see Figure 1) that may lead to serendipitous findings.

_Display._ How the library chooses to display the rich diversity of information resources is a third crucial serendipity dimension. Library users may hurry past heavily packed shelves without discovering the potentially intriguing but concealed knowledge universes close by. Curiosity-teasing display may thus trigger users' interest spaces. Many persons interviewed in the study found unplanned materials in the library because the materials were displayed, typically by book covers on tilted shelves. As one interviewee expressed it, he noticed _eye temptations_ and _show off_ books on the shelves. Stack orientation and display in relation to walking directions are also important in this context.

_Contrasts_. Diversity and display cannot stand alone as serendipity dimensions. If our attention is to be drawn to the manifold topics and resources there must be contrasts, variation and eye-catching differentiation that make things stand out from what otherwise would just be a visual cacophony ([Underhill 1999](#Underhill)). It is thus important that the display of information resources includes differentiated contrasts in the shape of lighting, colour schemes, etc., that may trigger users' senses and create varied sense impressions. Contrasts can also be achieved by humorous details in unexpected parts of the library or by contrasting quiet zones and display zones.

_Pointers._ Signage, cross-references, maps and other distinct markers and pointers are important tools to trigger users' interest spaces and draw their attention to both planned and unplanned findings. For example, a father and his three-year-old boy in the study found interesting materials in the children's section due to the topical icons on the shelves.

_Imperfection_. Allowing imperfect 'cracks' and 'loopholes' in the controlled library interface may open up for serendipity. For example, as earlier mentioned, an interviewee looked for bound volumes of music by a Danish composer, and also found music by Bette Midler, probably mis-shelved by another user. Materials left on tables and other behavioural traces left behind by users are other examples of imperfect 'cracks' in the library interface

_Cross contacts._ Possibilities for serendipity may emerge through cross contacts in the shape of contact surfaces across topics, genres, materials, people and library spaces. An example of cross contacts across genres could be by simply displaying jazz biographies together with jazz CDs. Cross contacts across topics happen, for example, when the placing of shelves and other display devices (including trolleys with returned books) bring disparate topics close together. Moving just a few steps across a gangway may take the user topically far. Serendipity is thus stimulated when topical disparity is combined with physical proximity.

_Multi-reachability,_ is a term used in the study to denote how well the physical library interface affords multiple possible routes for users to move and reach from one place to another in the physical or digital library space. For example, physical aisles or digital links can provide multiple pathways through physical and digital library interfaces. _Multi-reachability_ affects freedom of movement and possibilities for serendipity. The more different access routes that users can choose to move along in the library space, the more different resources and topics the users may meet – and the more affordances are present in this space to trigger users' interest spaces. Interestingly, Foster and Ford ([2003](#Foster)) refer to a study suggesting that the move to digital libraries might jeopardize serendipity by reducing the number of available paths to reach a given set of materials.

_Explorability_ is related to _multi-reachability_. In the context of the present study, explorability is defined as how well the library interface invites users to movement, exploration and browsing. That is, how well the lay-out of the library spaces invites users to be curious and to explore sidetracks and byroads of the library.

_Stopability_ counterpointing <span style="font-style: italic;">explorability,</span> here means that the library interface invites users to stop, touch, sit and relevance-assess materials including serendipitous findings. In the physical library it could be seating possibilities close to shelves, etc. and extra spaces on shelves, tables etc. for users to put carried things down and have their hands free to grasp and examine found materials.

Summing up, public libraries should stimulate and support or at least not counteract serendipity dimensions as listed in Table 4:

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">**Table 4: Identified dimensions in library interface for stimulating serendipitous findings**</caption>

<tbody>

<tr>

<th>Serendipity dimension</th>

<th>Explanation</th>

</tr>

<tr>

<td>Unhampered access</td>

<td>Unhampered direct access to information resources</td>

</tr>

<tr>

<td>Diversity</td>

<td>Rich and dense variety of topics, genres, resources, activities, sections</td>

</tr>

<tr>

<td>Display</td>

<td>Curiosity-teasing mediation of information resources</td>

</tr>

<tr>

<td>Contrasts</td>

<td>Eye-catching differentiation including quiet zones and display zones</td>

</tr>

<tr>

<td>Pointers</td>

<td>Distinct signage, maps, markers, etc., may trigger users' interest spaces</td>

</tr>

<tr>

<td>Imperfection</td>

<td>Imperfect 'cracks' and 'loopholes' in library interfaces</td>

</tr>

<tr>

<td>Cross contacts</td>

<td>Contact surfaces across different topics, genres, resources, activities, sections</td>

</tr>

<tr>

<td>Multi-reachability</td>

<td>Many different access routes across library interfaces</td>

</tr>

<tr>

<td>Explorability</td>

<td>Library interface invites users to move, explore and browse</td>

</tr>

<tr>

<td>Stopability</td>

<td>Library interface invites users to stop, touch and assess found materials</td>

</tr>

</tbody>

</table>

This is not an exhaustive list, as there may be additional dimensions that a more extensive investigation would have identified. The listed serendipity dimensions overlap each other. For example, a pointer such as a sign also functions as a contrast. Imperfections such as materials left on a table may also function as cross-contacts if these materials reflect mixed topics from different users.

In the library's mediation policy for how human, physical and digital information resources should be displayed and mediated to users there should be a balance between the different serendipity dimensions in order to avoid overkill, that could have the opposite effect on users so they do not open up for serendipitous findings.

In the present study, the focus was on <span style="font-style: italic;">extrinsic</span> serendipity dimensions in the physical information space. In a more extensive study it would be interesting to also investigate <span style="font-style: italic;">intrinsic</span> serendipity dimensions, that is, what _user_ dimensions (such as, energy, mood, personality, interest space, intentions etc.) that may make users receptive to serendipitous findings in the interplay with information spaces (see, for example Erdelez' ([1997](#Erdelez)) research on information encountering and super encounters).

### Integrative library interface

In order to put the serendipity dimensions and the library users' information behaviour into a broader and more holistic context, a conceptual framework was developed in the study. In this framework the library constitutes an _integrative interface_ comprising the totality of all contact surfaces, access points and mediation flows between users and available library resources, whether human, physical or digital.

The term _interface_ is normally used for the means by which users interact with computers. However, in recent years, physical spaces as a user interface has emerged as a research area in computer science on pervasive computing and augmented reality (e.g., [Ishii and Ullmer 1997](#Ishii); [Wellner _et al._ 2003](#Wellner)) concerned with how physical spaces can be augmented with ubiquitous digital devices and functionalities.

The adjective <span style="font-style: italic;">integrative</span> refers to the various human, physical and digital parts of the library interface being looked upon as an integrated whole; as supplementary and supportive parts for one another. For example, face-to-face interactions between users and librarians are important components in the library interface. A library Web page with user-to-user reading suggestions and the corresponding shelf in the physical library created to display the same books can be perceived as parts of the same integrative interface. This approach opens up for discussing usability issues also in the physical interface, and not only in digital interfaces.

Human library resources comprise staff, users, invited lecturers and other human players in the library. Physical library resources comprise physical materials (books, journals, CDs, etc.) as well as physical display devices (shelves, tables, etc.). Digital library resources comprise digital materials (by screen, download) and digital display devices (online catalogue, databases, Web site, etc.).

Figure 3 shows a library user who interacts with the integrative interface of a library in the shape of all access points to the human, physical and digital resources integrated in the library. Between the resources are mediation flows facilitated by the integrative interface as outlined in Table 5.

<div align="center">![Figure 3: User – enclosed (broken line circle) by socio-cognitive context](p370fig12.jpg)</div>

<div align="center">**Figure 3: User – enclosed (broken line circle) by socio-cognitive context (interests, tasks, problems, needs, intentions etc.) – interacts (double arrows) with library's integrative interface comprising all available contact surfaces with human (hum), physical (phys) and digital (dig) library resources. Between resources are mediation flows (single arrows) supported by the integrative interface,** _see_ Table 5.</div>

<table style="border: medium solid rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">**Table 5: Mediation flows between human, physical and digital information resources in integrative library interface** (Figure 3)</caption>

<tbody>

<tr>

<th>Mediation flow</th>

<th>Explanation and examples</th>

</tr>

<tr>

<td>human to human</td>

<td>Staff or user points to or communicates with another person (e.g., librarian, shelver, knowledgeable user)</td>

</tr>

<tr>

<td>human to physical</td>

<td>Staff or user points to physical display or material (e.g., map, shelf, book, CD)</td>

</tr>

<tr>

<td>human to digital</td>

<td>Staff or user points to digital display or material (e.g., database, web site, PDF file)</td>

</tr>

<tr>

<td>physical to human</td>

<td>Physical display or material points to human resource (e.g., poster 'Book a librarian for personal guidance')</td>

</tr>

<tr>

<td>physical to physical</td>

<td>Physical display or material points to other physical display or material (e.g., map with library floors; shelf sign; shelf with book covers)</td>

</tr>

<tr>

<td>physical to digital</td>

<td>Physical display or material points to digital display or material (e.g., poster refers to web page)</td>

</tr>

<tr>

<td>digital to human</td>

<td>Digital display or material points to human resource (e.g., web page with email links to user group)</td>

</tr>

<tr>

<td>digital to physical</td>

<td>Digital display or material points to physical display or material (e.g., web page with map of physical library; catalogue records with shelf class marks)</td>

</tr>

<tr>

<td>digital to digital</td>

<td>Digital display or material points to other digital display or material (e.g., links between web pages)</td>

</tr>

</tbody>

</table>

The mediation flows in Figure 3 and Table 5 can be combined into longer chains and loops. The crucial point is how users may recognize a red line in the library design and lay-out across the different library spaces and how these mediation flows are interconnected and supplement each other.

An interesting finding in the study is the subtlety and diversity of user-to-user mediation flows in the library interface ('human to human' in Figure 3 and Table 5). This is consisent with the evolving Library 2.0 approach about user-created information sharing in digital and physical library spaces (e.g., [Casey and Savastinuk 2006](#Casey); [Farkas 2007](#Farkas)). For example, observed users in the study looked through shelves and trolleys holding newly returned materials or materials awaiting re-shelving after use. Similarly, materials left on tables were examined by other users. In this <span style="font-style: italic;">indirect</span> user-to-user mediation and social navigation ([Dieberger _et al._ 2000](#Dieberger)), users thus explore and exploit other users' behavioural traces that may lead to serendipitous findings. Another example was the Copenhagen Main Library having shelves with user-to-user-recommended fiction books (suggestions submitted to the library home page). The same library also provided _flags_ with smileys and exclamations (_Shock_, _Yawn_, _Sob_, etc.) for users to attach to books they like or dislike.

### Physical versus digital affordance spaces

As earlier noted, a crucial serendipity dimension deals with users' unhampered and direct access to human, physical and digital library resources. An important difference between physical and digital spaces in the library interface is that only in physical library spaces is it possible for users to have direct, face-to-face access to human information resources (staff and other users) and direct, tangible access to physical resources. This is an important strength of physical library spaces compared with digital library spaces where all resources are digitally mediated.

Physical and digital library spaces may supplement each other, as noted earlier; for example, when users employ convergent information behaviour in a digital part of the library interface, for instance, as earlier noted, when locating a book in the online catalogue and then supplement it with divergent browsing behaviour in a physical part of the library interface.

Direct access in physical library spaces is related to physical tangibility, or the _graspability_ (see [Ishii and Ullmer 1997](#Ishii)) of information materials. In the study, we could see how users' physicality, senses and mobility affect users' information behaviour when seeking, finding and relevance-assessing materials in the library. Interviewed users explained that it meant much to be able to touch and hold the materials. For example, an interviewee in his twenties stated that he never searched for fiction in the online catalogue but '_walked around and fingered_' at the shelves.

Physical and digital information spaces may thus provide different _affordances_ ([Norman 1999](#Norman); [Sadler and Given 2007](#Sadler)) that users may perceive and use for interaction. The term _affordances_ covers usage potentials that the interface (see Figure 3) of the information space affords (i.e., provides, offers) to users for interaction, usage and experience. Affordances may be _intended_ or _unintended_ by designers of the information spaces. Thus there may be a discrepancy between users' perceived affordances and designers' intended affordances ([Sadler and Given 2007](#Sadler)). Users may even perceive affordances not intended by designers. For example, an observed adult user in the study hid Harry Potter books in a library section with books on European Union legislation. Here the user obviously thought that no other users would discover the hidden books.

In this study, the focus was on affordances for serendipity in physical parts of the library interface. It would also be interesting to investigate affordances for serendipity in digital library spaces and see if all the ten identified serendipity dimensions (as hypothesized) would be present also in digital library spaces or whether physical and digital spaces may afford some mutually exclusive serendipity dimensions.

## Conclusion

Observations and interviews in the study resulted in a model of different ways of finding library materials using and combining different types of _convergent_ (goal-directed) information behaviour and _divergent_ (explorative) behaviour. The investigated information behaviour was composite and situational with changes in users' behaviour between visits and during visits as the users' interest spaces get triggered in the library. Divergent information behaviour with browsing and serendipitous findings plays an important role in the physical library as indicated in the study.

Ten different serendipity dimensions in the physical library that may affect serendipity were identified in the study: unhampered access, diversity, display, contrasts, pointers, imperfection, cross contacts, multi-reachability, explorability and _stopability_. In order to put the serendipity dimensions and library users' information behaviour into a broader and more holistic context, the paper introduced a conceptual framework suggesting that libraries can be viewed as _integrative interfaces_ comprising all contact surfaces and mediation flows between users and library resources. These information resources, whether human, physical or digital, can be looked upon as an integrated whole; as supplementary and supportive parts for one another. Furthermore, the integrative library interface can be designed to invite users with different _interest spaces_ to interact with the diversity of human, physical and digital information resources available in the library.

One key realization in the study is that we are physical beings who like to interact with physical materials and other physical beings in physical spaces; physical library interfaces should thus be better designed with intended affordances to facilitate this interaction. Only in the physical library is it possible for users to have direct, face-to-face access to human information resources and direct, tangible access to physical resources. This is an important difference between physical and digital library spaces. The physical library should thus be seen as an indispensable part of the whole integrative library interface.

Mediation (or _affordance_) policies of libraries should include how human, physical and digital information resources should be mediated, displyed and supplement each other in the library's integrative interface. If integrated into these policies, the presented typology of convergent and divergent information behaviour and identified serendipity dimensions could serve as means to design the library interface to afford and facilitate both convergent and divergent information behaviour, supporting users to explore, exploit and expand affordances embedded in the library's integrative interface.

## Acknowledgements

The study was supported by funding from the Danish Ministry of Culture, _KuM no. TAKT 2005-059_ and _2006-094_.

## References

*   <a id="Bates" name="Bates">Bates, M.J. (1989). The design of browsing and berrypicking techniques for the online search interface. _Online Review,_ **13**(5), 407-42.</a>
*   <a id="Buschman" name="Buschman">Buschman, J.E. & Leckie, G.J. (eds.). (2007). _The library as place: history, community and culture_. Westport, CT: Libraries Unlimited.</a>
*   <a id="Casey" name="Casey">Casey, M.E. & Savastinuk, L.C. (2006).</a> [Library 2.0: service for the next-generation library](http://www.webcitation.org/5d2VH8VMP). _Library Journal_, **131**(14), 40\. Retrieved 11 December 2008 from http://www.libraryjournal.com/article/CA6365200.html (Archived by WebCite® at http://www.webcitation.org/5d2VH8VMP)
*   <a id="Chang" name="Chang">Chang, S. & Rice, R.E. (1993). Browsing: a multidimensional framework. _Annual Review of Information Science and Technology,_ **28**, 231-276.</a>
*   <a id="Dieberger" name="Dieberger">Dieberger, A, Dourish, P., Höök, K., Resnick, P. & Wexelblatt, A. (2000). Social navigation: techniques for building more usable systems. _Interactions_, **7**(6), 36-45.</a>
*   <a id="Erdelez" name="Erdelez">Erdelez, S. (1997). Information encountering: a conceptual framework for accidental information discovery. In: Vakkari, Savolainen & Dervin (eds.). _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts. 14-16 August, 1996, Tampere, Finland,_ (pp. 412-421). London: Taylor Graham.</a>
*   <a id="Farkas" name="Farkas">Farkas, M.G. (2007). _Social software in libraries: building collaboration, communication and community online_. Medford, NJ: Information Today.</a>
*   <a id="Ford" name="Ford">Ford, N. (1999). Information retrieval and creativity: towards support for the original thinker. _Journal of Documentation_, **55**(5), 528-542.</a>
*   <a id="Foster" name="Foster">Foster, A. & Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation_, **59**(3), 321-340.</a>
*   <a id="Garfield1986" name="Garfield1986">Garfield, E. (1986, October 20).</a>[The metaphor-science connection.](http://www.webcitation.org/5d2VZtlpA) _Current Comments_, **42**, 3-10\. Retrieved 13 December, 2008 from http://www.garfield.library.upenn.edu/essays/v9p316y1986.pdf . (Archived by WebCite® at http://www.webcitation.org/5d2VZtlpA)
*   <a id="Garfield2004" name="Garfield2004">Garfield, E. (2004).</a> [Systematic serendipity: finding the undiscovered answers to science questions](http://www.webcitation.org/5d2Ve0Axn). Presentation at the _Medical ignorance collaboratory,_ University of Arizona, Tucson. Retrieved 11 December 2008 from http://www.garfield.library.upenn.edu/papers/az072004.pdf (Archived by WebCite® at http://www.webcitation.org/5d2Ve0Axn)
*   <a id="Given" name="Given">Given, L.M. & Leckie, G.J. (2003). 'Sweeping' the library: mapping the social activity space of the public library. _Library & Information Science Research_, **25**(4), 365-385.</a>
*   <a id="Goodall" name="Goodall">Goodall, D.L. (1989). _Browsing in public libraries_. Loughborough, U.K.: Loughborough University of Technology, Library and Information Statistics Unit. (LISU Occasional Paper no. 1)</a>
*   <a id="Ingwersen" name="Ingwersen">Ingwersen, P. (1992). _Information retrieval interaction_. London: Taylor Graham.</a>
*   <a id="Ishii" name="Ishii">Ishii, H. & Ullmer, B. (1997). Tangible bits: towards seamless interfaces between people, bits and atoms. In _Human factors in computing systems: CHI 97 conference proceedings: 22-27 March, 1997, Atlanta, Georgia_. (pp. 234-241). New York, NY: ACM Press.</a>
*   <a id="Leckie" name="Leckie">Leckie, G.J. & Given, L.M. (2005). Understanding information seeking: the public library context. _Advances in Librarianship_, **29** 1-72.</a>
*   <a id="Lushington" name="Lushington">Lushington, N. (2002). _Libraries designed for users: a 21st century guide_. New York, NY: Neal-Schuman.</a>
*   <a id="Norman" name="Norman">Norman, D. (1999). Affordance, conventions and design. _Interactions_, **6**(3), 38-42.</a>
*   <a id="Sadler" name="Sadler">Sadler, E. & Given, L.M. (2007). Affordance theory: a framework for graduate students' information behaviour. _Journal of Documentation_, **63**(1), 115-141.</a>
*   <a id="Spink" name="Spink">Spink, A. (2004). Multitasking information behaviour and information task switching: an exploratory study. _Journal of Documentation_, **60**(4), 336-351.</a>
*   <a id="Underhill" name="Underhill">Underhill, P. (1999). _Why we buy: the science of shopping_. New York, NY: Simon & Schuster.</a>
*   <a id="VanAndel" name="VanAndel">Van Andel, P. (1994). Anatomy of the unsought finding. Serendipity: origin, history, domains, traditions, appearances, patterns and programmability. _British Journal for the Philosophy of Science_, **45**(2), 631-648</a>
*   <a id="Wellner" name="Wellner">Wellner, P., Mackay, W. & Gold, R. (2003). Special issue on computer augmented environments: back to the real world. _Communications of the ACM_, **36**(7), 24-26.</a>