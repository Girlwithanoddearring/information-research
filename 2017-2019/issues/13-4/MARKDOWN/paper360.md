#### vol. 13 no. 4, December, 2008

# Information needs and information behaviour of patients and their family members in a cancer palliative care setting: an exploratory study of an existential context from different perspectives

#### [Ina Fourie](mailto:ina.fourie@up.ac.za)  
Associate professor, Department of Information Science, University of Pretoria, Pretoria 0002, South Africa

#### Abstract

> **Introduction.** Very little is known about information needs and information behaviour in cancer palliative care: this paper addresses that gap.  
> **Method.** An exploratory study with a purposive, convenience sample of patients and their family members in a cancer palliative care setting in South Africa was conducted. Individual semi-structured interviews were held with patients, family members and healthcare professionals.  
> **Analysis.** An interpretative approach was followed. Transcribed data were analysed to identify aspects of information behaviour that may shed light on gaps in our understanding of the cancer palliative care context, as well as differences in the perceptions of oncologists, nurses, patients and family members.  
> **Results.** Even if there is no cure, patients and family members express different levels of, and fluctuating manifestations of, information need. Although factors influencing information behaviour were noted, the complexity, and interwoven nature thereof make it difficult to use these to predict information needs and information behaviour.  
> **Conclusions.** More attention should be paid to unexpressed and dormant information needs. A re-analysis of data and the literature should be used to explore models from information seeking and information behaviour as well as from other disciplines that might support a further study of the manifestations of information behaviour.

## Introduction

In-spite of increasing survival rates and life-expectancy cancer is still strongly associated with pain, fear, changes in physical appearance and death. The complexity of the cancer journey, the many individualised nuances, and the ever-fluctuating variety of needs that manifest in the process of trying to make sense of this life-threatening situation have often been noted ([Friedrichsen 2002](#fri02); [Friis _at al._ 2003: 167](#fri03); [Johnson 1997](#joh97); [Lichter 1987](#lic87); [Williamson and Manaszewicz 2002: 12](#wil02)). The many models and theories of information seeking and information behaviour (for example those reported in reviews by [Case 2006](#cas06), [2007](#cas07) and [Courtright 2007](#cou07) as well as [Fisher _et al._ 2005](#fis05)) pointing out the importance of context in information needs and information seeking amongst other things have been noted. Frustrations with information provision, information sources, expressions of anxiety, etc. however still feature strongly, implying that in spite of _some_ understanding there is still a gap when offering information-related support and/or a gap in our research focus ([Gattellari _et al._ 2002: 503](#gat02); [Hopkinson and Corner 2006](#hop06); [Williamson and Manaszewicz 2002](#wil02)).

In addition there is an urgency to meet with the information needs and information seeking behaviour of patients in cancer contexts. More patients are diagnosed with cancer ([Jimbo _et al._ 2006](#jim06)). Healthcare professionals (working with serious staff shortages) are under pressure to offer _total care_, to enable patients to participate in informed decision-making, and to support patients in using information resources such as the Internet ([Johnson _et al._ 2001: 335](#joh01); [Lichter 1987](#lic87); [Zanchetta and Moura 2006: 806](#zan06)). This is especially important since more patients are reported to turn to the Internet ([Dickerson _et al._ 2006](#dic06); [Newnham _et al._ 2006](#new06); [Williamson and Manaszewicz 2002](#wil02)).

These and reported frustrations should stimulate research to offer support for information behaviour. In addition contradictory reports on patients' interest in informed decision-making, the need to acquire information and the impact of demographic factors (for example age and sex) should alert us to the very individual and fluctuating nature of information needs and information seeking already noted in cancer contexts ([Beaver and Booth 2007;](#) [Friis _et al._ 2003: 169](#fri03); [Ramanadhan and Viswanath 2006: 131](#ram06); [Williamson 2005](#wil05)). According to Van der Molen ([1999: 242](#van99)) unless people are specifically asked about their information needs in areas other than their illness and treatment, healthcare professionals are likely to remain ignorant of them.

Considering these as well as the increase in interest in interventions such as portals, Websites, e-mail support groups and health communication, it seems timely, therefore, to consider information behaviour in cancer palliative care contexts: the last phase in cancer, which is associated with terms such as _terminally ill, dying, end-of-life_ and _hospice care,_ and of which very little is known ([Friis _et al._ 2003: 162](#fri03); [Kutner _et al._ 1999](#kut99); [Meier _et al._ 2007](#mei07); [Stiefel 2006](#sti06); [Street and Ottman 2007](#str07); [Williamson 2005](#wil05)). Zanchetta and Moura ([2006](#zan06)) remark: '_Among patients with end-stage cancer, motivation to seek in-depth information, including possibilities of wellness and hope, remains only partially understood_'. Although there are opinions that patients may require less information than caregivers towards the end ([Parker _et al._ 2007: 81-82](#par07)), there are also contradictory claims: '_By contrast, patients in advanced stages needed more information overall. This may be partly attributed to the reluctance of doctors to disclose bad news to patients with unfavourable prognosis_' ([Iconomou _et al._ 2002: 319](#ico02)). Milberg _et al._ ([2008: 58](#mil08)) add: '_Palliative care does not end with the death of the patient, and many palliative care services offer specific follow-up services for the bereaved_'.

Against this background an exploratory study with patients in cancer palliative care was conducted to collect data that can point to new ways of considering information needs and information behaviour and shed light on contradictory results.

## Clarification of concepts

_Healthcare professionals_ will be used as encompassing term for oncologists, doctors and nursing staff.

### Human information behaviour

From reported research there seems to be a focus on information needs and information seeking, including active as well as passive forms of information seeking and influencing behaviour ([Ankem 2006](#ank06); [Clayton _et al._ 2005](#cla05); [Edgar _et al._ 2002](#edg02); [Fukui 2004](#fuk04); [Johnson 1997](#joh97); [Rose 1999](#ros99); [Voogt _et al._ 2005](#voo05)). Considering dissatisfaction with information provision and information content ([Williamson and Manaszewicz 2003](#wil03)), inadequate awareness of the different domains ([Jepson _et al._ 2007: 890](#jep07)), gaps reported in the knowledge of patients in palliative care ([Gattellari _et al._ 2002: 503](#gat02)) and the avoidance of information ([Case _et al._ 2005](#cas05)) this article takes a wider view in determining the context for data analysis in accepting Wilson's ([1999: 249](#wil99)) definition of human information behaviour as, '_the totality of human behavior in relation to sources and channels of information, including both active and passive information-seeking and information use_'.

Information behaviour encapsulates information seeking, information searching and information retrieval, as well as communication and use of information. It encompasses ignorance of the need for information and avoidance of information, as well as active information seeking by using a variety of media and resources once an information need has been recognised. Although note is taken of debate on the concept of information behaviour as discussed by Courtright ([2007](#cou07)), this will not be further explored in this article. The complexity of human information behaviour as has been noted by many researchers e.g., Wilson ([1999](#wil99)) should, however, raise awareness of the complexity of its manifestations in a life-threatening disease context (i.e., cancer) and specifically cancer palliative care where patients (and family) may acknowledge the implications of poor prognosis: '_[Information] will help me to die with dignity - since one has come such a long way with the cancer_', or they may be holding on to some form of hope: '_Really, really, all I want to know is what it means to be called a survivor_'. In fact patients in palliative care face a life-ending context. And even though acknowledging no cure they may not explicitly express their information needs in terms of dying.

### Information needs

In palliative care contexts, physical, psychological, emotional, spiritual and financial information needs were noted amongst others ([Baker 2004](#bak04)). An information need can refer to the gap between what we know and what we need to know, or to an anomalous state of knowledge ([Belkin _et al._ 1982](#bel82)). To bridge the gap, actions of information seeking are undertaken ([Dervin 1999](#der99)), if the gap is recognised. The meaning of _information needs_ is seldom clarified in research reports. For example, Friis _et al._ ([2003: 169](#fri03)) also note that patients and healthcare professionals may attach different meanings to _information_ (and therefore information needs), for example relating information to personal and psychosocial aspects of the illness (patients) versus relating it to general biomedical aspects (doctors). For the purposes of this article an explanation by Wilson in 1981 will be accepted that information needs are often accounted for by other needs (i.e., primary needs), in addition to the sense-making approach by Dervin (as cited in [Case 2007: 75](#cas07)): '_… a state that arises within a person, suggesting some kind of gap that requires filling_'. This is wider than Case's ([2007: 5](#cas07)) explanation of an information need as '_…a recognition that your knowledge is inadequate to satisfy a goal that you have_' and may allow for information needs not being recognized by the patients and family members, but which seems evident to healthcare professionals from the actions of patients and family members such as their anxiety and body language.

### Palliative care

Palliative care can refer to either care required since the time of diagnosis with a life-threatening disease such as cancer, or to the care of terminally ill patients to reduce pain but without curing the cause. For purposes of this paper and for selection of participants, the World Health Organisation's (WHO) definition of palliative care was accepted, namely,

> ...an approach that improves the quality of life of patients and their families facing the problems associated with life-threatening illness, through the prevention and relief of suffering by means of early identification and impeccable assessment and treatment of pain and other problems, physical, psychosocial and spiritual ([World Health Organization n.d](#wor)).

In context of this interpretation, patients with very poor prognosis were selected.

## Exploratory study

The exploratory study, using purposive, convenience sampling, was conducted at a cancer palliative care setting in Pretoria, the administrative capital of South Africa. Since differences have been noted in the information needs of patients and their families (i.e., those needing information) ([Clayton _et al._ 2005](#cla05); [Friedrichsen and Milberg 2006](#fri06); [Johnson 1997](#joh97); [Kirk _et al._ 2004](#kir04); [Lichter 1987](#lic87); [Stiefel 2006](#sti06)), as well as the perceptions of healthcare professionals (i.e., the providers of information interventions) ([Van der Molen 1999: 241](#van99); [Williamson and Manaszewicz 2002](#wil02)), participants were selected to offer different perspectives. They included:

*   in- as well as out-patients treated by a private medical oncology centre and when possible, family members;
*   treating oncologists and a doctor specialising in palliative care;
*   nursing staff from the medical oncology centre and oncology hospital wards.

Permission for the study was obtained from the University's Faculty Ethics Committee, the treating oncology centre, matron of the treating hospital and the unit manager of the oncology wards. All participants signed a participation form of consent agreeing to interviews being tape-recorded. If participants did not want to continue or if the interview caused distress it would be stopped and continued later; if possible. For each patient the treating oncologist or doctor as well as the oncology ward unit manager (in case of in-patients) also signed consent forms to approach patients and their family members. The interviews with patients and their families were conducted by Wilna Burgers, an oncology social worker with a Masters degree in Medical Social Work. She is a staff member of the Mary Potter Oncology Centre, Pretoria, South Africa. Wilna had access to the patients' records and also works closely with the healthcare professionals.

### Participating sample group

For exploratory purposes a relatively small sample (in total twenty-five) was used. Twelve more people were approached as patient/family participants: eight were too sick, one forgot about the appointment, one was too busy and two did not feel like participating.

#### Patients and family members

Most patients were hospitalised (i.e., were in-patients) at the time of the interviews and when possible family members were included. Participating patients were identified by the sister-in-charge at the oncology centre, treating oncologists or doctor and the oncology social worker who had access to their medical records. Since it would be difficult for terminally ill patients to participate we mostly used the broader, WHO, interpretation of palliative care to select participants associated with poor prognosis. In one incident only the patient's wife and son participated since the patient himself was too ill (passing away within two weeks of the interview). Two other participants also passed away within a month of the interviews without having mentioned how very ill they were or describing themselves in the context of palliative care.

To put participants in context, Table 1 represents some cancer related information and demographic detail on the patients and Table 2 on the family members.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 1: Selected demographic and cancer data for patients**</caption>

<tbody>

<tr>

<th valign="middle">Sex</th>

<th>Type of cancer</th>

<th>Occupation</th>

<th>Language</th>

<th>Age</th>

<th>Qualification</th>

</tr>

<tr>

<td>Female</td>

<td>Breast & bone metastasis (no cure)</td>

<td>Nurse</td>

<td>Afrikaans</td>

<td>36 - 45</td>

<td>Post school</td>

</tr>

<tr>

<td>Female</td>

<td>Inflammation breast cancer (no remission)</td>

<td>Was a secretary</td>

<td>Afrikaans</td>

<td>36 - 45</td>

<td>Post school</td>

</tr>

<tr>

<td>Female</td>

<td>Ovary cancer & liver metastasis (no cure)</td>

<td>Housewife, was active in local organisations</td>

<td>English</td>

<td>> 75</td>

<td>High school not completed</td>

</tr>

<tr>

<td>Male</td>

<td>Testicle cancer & metastasis to lymph nodes (no cure)</td>

<td>Electronic engineer</td>

<td>Afrikaans - (German background)</td>

<td>26 - 35</td>

<td>Post school</td>

</tr>

<tr>

<td>Male</td>

<td>Differentiated non-Hodgkin lymphoma (passed away within two weeks)</td>

<td>Pensioner</td>

<td>English (Indian)</td>

<td>55 - 65</td>

<td>High school completed</td>

</tr>

<tr>

<td>Male</td>

<td>Lung cancer (passed away within a month)</td>

<td>Administrative</td>

<td>Afrikaans</td>

<td>36 - 45</td>

<td>High school completed</td>

</tr>

<tr>

<td>Male</td>

<td>Multiple myeloma (passed away within a month)  
_Patient was too ill to participate. His wife and son, however, participated._</td>

<td>Depot manager</td>

<td>Afrikaans</td>

<td>36 - 45</td>

<td>Post school technical diploma</td>

</tr>

<tr>

<td>Male</td>

<td>Acute Myeloid Leukaemia  
(prognosis good - falling within the wider interpretation of palliative care)  
_The wife sat in on the interview and participated_</td>

<td>Business man</td>

<td>Afrikaans</td>

<td>36 - 45</td>

<td>Post school</td>

</tr>

</tbody>

</table>

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 2: Selected demographic data for family members**</caption>

<tbody>

<tr>

<th valign="middle">Sex</th>

<th>Relationship</th>

<th>Occupation</th>

<th>Language</th>

<th>Age</th>

<th>Qualifications</th>

</tr>

<tr>

<td>Female</td>

<td>Wife</td>

<td>Housewife</td>

<td>Afrikaans</td>

<td>46 - 55</td>

<td>High school completed</td>

</tr>

<tr>

<td>Female</td>

<td>Wife</td>

<td>Administrative position</td>

<td>Afrikaans</td>

<td>36 - 45</td>

<td>High school completed</td>

</tr>

<tr>

<td>Female</td>

<td>Wife</td>

<td>Afrikaans</td>

<td>Business woman</td>

<td>36 - 45</td>

<td>High school completed</td>

</tr>

<tr>

<td>Male</td>

<td>Son</td>

<td>English (Afrikaans background)</td>

<td>Manager at hamburger enterprise</td>

<td>26 - 35</td>

<td>High school not completed</td>

</tr>

</tbody>

</table>

#### Nursing staff

Eight nursing staff members from the oncology hospital wards and the treating oncology medical centre were interviewed. They were considered representative of the staff in terms of years of experience and the nature of contact with patients and their families.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 3: Professional experience of nursing staff**</caption>

<tbody>

<tr>

<th valign="middle">Sex</th>

<th>Language</th>

<th>Rank</th>

<th>Professional function</th>

</tr>

<tr>

<td>8 females</td>

<td>7 Afrikaans  
1 English</td>

<td>1 oncology ward unit manager  
1 sister-in-charge  
5 sisters  
1 staff nurse</td>

<td>3 administrating treatment to in-patients  
3 treating patients in hospital  
1 sister-in-charge  
1 oncology ward unit manager</td>

</tr>

</tbody>

</table>

#### Oncologists and a doctor

Four treating oncologists and a doctor specialising in palliative care were interviewed from the oncology centre.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 4: Professional experience of oncologists and doctor**</caption>

<tbody>

<tr>

<th valign="middle">Sex</th>

<th>Language</th>

<th>Profession</th>

</tr>

<tr>

<td>2 male  
3 female</td>

<td>3 Afrikaans  
2 English</td>

<td>4 oncologists (1 with experience in hospice care)  
1 doctor specializing in palliative care</td>

</tr>

</tbody>

</table>

### Methods for data collection

Research reports on information needs and information seeking in cancer contexts mostly focus on quantitative studies using questionnaires that have been widely used and tested in the biomedical and healthcare fields (e.g., Toronto Information Needs Questionnaire-Breast Cancer and Patient Learning Needs Scale and Information Preference Cards ([Ankem 2006](#ank06))). Williamson and Manaszewicz ([2002](#wil02)) express concern that this may be a reason why the same answers are often found. Noting some good examples of qualitative research, namely Baker ([2004](#bak04)), Friis _et al._ ([2003](#fri03)), Kvåle ([2007](#kv07)) and Rose ([1999](#ros99)), the following methods were selected for data collection.

For patients and family members semi-structured individual interviews were conducted by an oncology social worker according to a questionnaire. The following applied:

*   The questionnaire was completed by the interviewer;
*   The interview was tape recorded with consent (free transcription; interviews were in Afrikaans or English depending on participants' language of preference);
*   The emotional state of the participant during the interview (e.g., apathy, loneliness) was observed by the social worker;
*   Rapport was established with participants at the beginning of the interview (not taped);
*   Post discussion with participants followed the interview (not taped);
*   Opportunities for follow-up sessions were explored (the interviews opened numerous opportunities);
*   If patients became emotional the recording was temporarily stopped;
*   Participants could at any time end the interview or withdraw;
*   The interviews lasted between 45 - 60 minutes.

For the healthcare professionals (oncologists, doctor & nursing staff) individual interviews were conducted by the main researcher with the social worker sitting in. The following applied:

*   The interviews were conducted according to an interview guideline, with encouragement to elaborate on answers;
*   The interviews were tape recorded with consent (free transcription; interviews were in Afrikaans or English depending on participants' language of preference);
*   The interviews lasted between thirty to sixty minutes (one fifteen-minute interview with an oncologist was followed up by a thirty-minute interview to allow for further elaboration).

In-spite of perceptions that cancer patients may not always want to talk about their difficult feelings ([Kvåle 2007](#kv07)), patients and their families (although sometimes nervous or emotional), were all willing to answer questions. One participant, in-spite of the experienced skills of the interviewer, could not be kept to the questionnaire format: she only wanted to share as much as possible of her story and experiences in her own words to help the researchers!

In compiling the questionnaires the standardised questionnaires and findings reported by other studies were noted (e.g., Baker ([2004](#bak04)), Cox _et al._ ([2006](#cox06)), Friedrichsen and Milberg ([2006](#fri06)), Friis _et al._ ([2003](#fri03)), Kirk _at al._ ([2004](#kir04)), Lichter ([1987](#lic87)) and Johnson ([1997](#joh97))). Models of information seeking and information behaviour pointing to the importance of demographic factors, barriers, importance of anxiety, personal networking, motivation or activating mechanisms for information seeking, etc. e.g., Wilson's 1981 and 1986 models ([Wilson 1999](#wil99)) and the comprehensive model of information seeking proposed by Johnson ([1997](#joh97)), were also noted and incorporated in the questionnaire. The intention with a detailed questionnaire was to allow the oncology social worker with no background in Information Seeking or Information Science to use her expertise in counselling cancer patients and their families to conduct interviews allowing participants to reflect on their information needs and information seeking within the wider interpretation of information behaviour.

Apart from demographic and personal detail, the questionnaire for patients and their family members covered the following broad categories: the information required, willingness to ask for information, preferred methods and resources of finding information, perceptions of the information needed by other parties (e.g., family members' perception of the information needed by patients), awareness of available information sources and reliance on the Internet. The interview schedule for oncologists, doctors and nursing staff included questions on: differences noted in information needs in different cancer contexts (e.g., palliative or pediatric cancer care); the responsibility of medical and healthcare professionals to meet with the information needs of cancer patients and their families; opinions on the use of the Internet as cancer information source and perceptions on its use by patients and their families; suggestions on the improvement of access to information for patients and their families; perceptions on the information needs and information behaviour of patients and their families; and perceptions on the impact of the medical history of the patients.

For the exploratory study demographic data was collected to contextualise patients and their families rather than to generalise correlations with information needs and information behaviour.

Although very time intensive, the methods provided information-rich data, which will need to go through several phases of analysis complemented by supporting literature analyses.

### Building on existing research and models

Numerous models of information seeking, information searching and to a lesser extent information behaviour (also pointed out by Case _et al._ 2005: 356) have been noted (e.g., as discussed in [Case 2006](#cas06), [2007](#cas07), [Courtright 2007](#cou07), [Fisher _et al._ 2005](#fis05) and [Järvelin and Wilson 2003](#j03)). These range from models focusing on the emotional experiences and thoughts during the seeking process and information seeking as iterative and variable process over time ([Kuhlthau 1991](#kuh91), [2004](#kuh04)), to the stages or phases of information seeking (e.g., [Wilson 1999](#wil99)). In the cancer context Johnson ([1997](#joh97)) proposed the comprehensive model with a strong focus on influencing behaviour such as promoting cancer screening. Research reports from the biomedical and healthcare literature acknowledge findings and theories in the field e.g., testing the monitoring/blunting theory of Miller ([1995](#)). (This theory was later questioned by Williamson and Manaszewicz ([2002](#wil02))).

There is, however, little if any acknowledgement of the models and theories from information science and information seeking in the biomedical and healthcare literature. Exceptions are books by Lichter ([1987](#lic87)) and Stiefel ([2006](#sti06)) which are unfortunately not heavily cited in the journal literature. Although the 1981 and 1996 models of Wilson ([1999](#wil99)) and the Johnson ([1997](#joh97)) model, as well as subsequent studies reported by Case _et al._ ([2005](#cas05)), Johnson ([2003](#joh03)) and Johnson _et al._ ([2001](#joh01)) certainly hold much potential for understanding cancer-related information seeking it was decided to _note_ support for these models from the preliminary findings, but, for the moment, to focus on gaps in our understanding, with the intention to relate the gaps noted and existing models more explicitly to findings in the follow-up phases of the project. The most important shortcomings of the models seemed to be the lack of explicit reference to unawareness of information needs (in the 1996 model Wilson only mentions passive information seeking [[Wilson 1999](#wil99)]) as well as a lack of acknowledgement of ignorance on the value of information. Furthermore the complexity of information behaviour is not linked to difficulty in making predictions for individual contexts. The strengths of these models when compared to research reports in the cancer information-seeking contexts include reference to self-efficacy, and beliefs and salience as activating mechanisms (e.g., [Johnson 1997](#joh97), [Wilson 1999](#wil99)). Within the limitations for article length such models will not be further explored.

Some of the theory and findings noted that are important for this study include Williamson's ([2005](#wil05)) article on _one size does not fit all,_ the need and value for social networking ([Johnson 1997](#joh97)) and considering the _whole person_ in context ([Williamson and Manaszewicz 2002](#wil02); and explored in more detail in the review by [Courtright 2007](#cou07)). There is also Beaver and Witham's ([2007: 24](#beaw)) report on the continued need for access to information and support, Rose's ([1999](#ros99)) qualitative analysis of the information needs of informal carers of terminally ill patients and Barnoy, Bar-Tal and Zisser's ([2006](#bar06)) report on information coping styles (monitoring vs blunting) finding that role and not information seeking style was the main factors that influenced behaviour. With regard to cancer information seeking and especially palliative care it seems as if research focuses on expressed information needs, the need for disclosure of information on the disease and prognosis, communication with doctors and healthcare professionals and how they share information, satisfaction with information, patients' understanding of the information and barriers to their understanding of the information ([Friedrichsen and Milberg 2006](#fri06); [Kirk _et al._ 2004](#kir04)).

### Analysis of data

The data collection methods provided information-rich data. Linked to a substantial review of the literature (only partially reported in the preceding section) an interpretative approach was followed. This method is also reported by Williamson and Manaszewicz ([2002](#wil02)) and Williamson ([2005](#wil05)) and explained by Pickard ([2007](#pic07)) and O'Donoghue ([2007](#odo07)). After transcribing the interviews and several readings, key themes were identified. Looking for potential gaps, only a few themes are covered in this paper. These include the need or desire for information (i.e., when they require information, how they deal with information, preference for information resources, type of information desired, reasons for needing information, willingness to ask for information, perceptions of the information needs of other role players in the cancer context and reliance on the Internet), as well as the factors such as age, culture and type of cancer that seem to influence information behaviour. The article reports on the first round of analysis. Input from patients, family members and the healthcare professionals are considered.

### Selected findings

The exploratory study confirmed the complexity of information behaviour in cancer palliative care contexts, the need for information, the diversity of information needs and information behaviour of participants, as well as the differences between people sharing the same cancer context (patient vs. spouse; son vs. wife). Healthcare professionals' perceptions seemed to differ depending on their role (oncologist vs. nurse), nature of contact (treatment in wards vs. chemotherapy in oncology centre) and time that can be spent with patients and their families. Hospital nurses have much more contact with the patients and family: '_We rely heavily on our nurses… for better or worse… the sisters have much more time with the patient_'. Their perceptions on meeting the information needs of patients and their families and how they use the Internet also differed from each other, as well as the actual reports by the patients and their families. This article will only report on some of the findings. Differences in expressed information needs by patients and family members when queried and perceptions of such needs expressed by healthcare professionals when queried, need to be further pursued to ensure that gaps in our awareness of potential information needs are filled.

The study confirmed the need to recognise the individual nature of information needs and manifestations of information behaviour (also noted by [Williamson and Manaszewicz 2002](#wil02)) and the fact that patients and their families do not explicitly define their cancer situation in terms of the WHO definition of palliative care: for them it was more about the fact that they have cancer and need to carry on. They seemed to be either unaware of a poor prognosis (although oncologists confirmed that patients are informed), or deny it because of, for example religious beliefs: '_I have received a word from the scriptures that he will be cured_'; and '_We must only cure the disease_'. Even when accepting the prognosis a patient still grasped for hope: '_Dr said that I will never go into remission again... before that I really believed that I will be cured… really, really all I want to know is what it means to be called a survivor_'. Changes in information behaviour expressed during the interviews were also noted, similar to changes in information behaviour mentioned by Cox _et al._ ([2006: 268](#cox06)) and Friis _et al._ ([2003: 165](#fri03)).

#### Information needed or desired

##### Aspects on which information is required

Many of the information needs noted in the literature for various contexts of cancer (e.g., [Lichter 1987](#lic87)) and more specifically on palliative care ([Docherty _et al._ 2008](#doc08); [Friedrichsen 2002](#fri02)) were mentioned by participants. For example wanting to know about the type of cancer, the treatment (what to expect, the side effects), medication, prognosis, life expectancy ('_I need to know when the end will come_'; son), assurance that the patient will be cured, what will happen next and why things are happening. Although there are overlaps, each patient's expressed needs are also very individual. In the same context a wife and a son expressed very different information needs: the son desired factual information on the disease, prognosis and life expectancy while his mother, although explaining that she preferred no information, admitted not knowing how to react. (She thus reflected a dormant information need, where a primary need [to know what to do], was not interpreted as a secondary need for information.) Healthcare professionals mentioned the need for information on pain management, home care and dealing with problems such as diarrhea and fever.

It seems as if needs expressed by patients and family members and the perceptions of healthcare professionals need to be supplemented with an audit of information needs noted in the literature, as well as an extension of the survey to gain a more holistic picture of potential information needs to expect and provide for.

##### Ability to express information needs and ability to understand information

A strong need for information to be tailor-made according to individual needs and circumstances such as coping with a combination of cancer and another life-threatening disease was noted. A need for individualisation of information and a personal relationship with healthcare providers is also noted by Docherty _et al._ ([2008: 166](#doc08)), stressing the impression that provision of information brochures and leaflets (mentioned by healthcare professionals) are not sufficient to meet the information needs of patients and their families. This is confirmed by patient's and their family member's comments on their ability to understand information and put that information to use: '_Not in language accessible to a guy who sells hamburgers for a living_'; '_I do not have biology_', and coming from a nurse: '_Regardless of educational level, some do not understand the biology/physiology of the disease_'. Some of the oncologists on the other hand, however, explained that the information provided by Websites for cancer patients, is very well written and easy to understand.

Healthcare professionals (especially the nurses) noted that not all information needs are expressed. For example patients not asking about sexual well-being (unlike in the study reported by Cox _et al._ ([2006](#cox06))) or non-verbal expressions of needs ('_You can feel the emotional need_'; '_Emotional needs… you can feel it… in which phase of bereavement he is…_' ). The latter is a primary need that may be met (at least partially) by either patients and their families or healthcare professionals having information on dealing with emotional needs. A patient noted: '_Psychosocial and emotional information very important, but very much neglected_'.

Healthcare professionals noted that '_Patients do not remember_' and, therefore, information need to be repeated; only one patient, however, mentioned forgetfulness due to the treatment. According to healthcare professionals patients also do not distinguish between types of cancer when seeking information. For them '_cancer is cancer_'. This is a viewpoint which often leads to increased anxieties if finding information on the wrong type or phase of cancer.

It seems as if patients and family members are not always aware of information needs, how to express these and if they receive information or come across information through Internet searches, the information is not always accessible in terms of the language used and level of presentation. Although healthcare professionals are aware of some unexpressed information needs and patients' and their families' inability to understand information, the need for tailor-made information and the need to cater for a diversity of information needs and learning styles as well as information seeking styles did not feature strongly in their response. Many of the problems concerning the expression of information needs and problems in understanding and interpreting the literature are also confirmed in the literature ([Docherty _et al._ 2008](#doc08)). Lichter ([1987: 96](#lic87)) notes: '_They may be overwhelmed by anxiety yet be unable to formulate their fears even to themselves_'. Regardless of our knowledge of shortcomings in the awareness and expression of information needs, the problem continues to exist.

##### Level and amount of information

Diversity in the level and amount of information required as mentioned in the literature (e.g., [Van der Molen 1999](#van99)) has also been confirmed by this study. Some participants wanted no information: '_I do not want to know about the side-effects…_. '_[Information] not for me_'. '_I do not want to hear it in words_'. For others, information does not matter: '_For me it [information] does not matter_', while others wanted as much as possible: '_The more I know the better_'. Diversity was also noted in the responses of individual participants. When she learned that the cancer was spreading: '_O goodness… I want to know more…_'. She, however, does not want to know everything: '_I am not yet feeling like reading about those who did not make it_'. Such responses reconfirmed the complexity of cancer related information behaviour and the difficulty in predicting behaviour for a specific cancer context.

Healthcare professionals noted that patients who are dying do not want a lot of information, though their family may experience an increased need for information. '_Some do not want to talk… in denial'_; '_Pick up vibe if patient want to know more or not_'. Their experience seems to be that patients who have come some way with cancer before moving into palliative care have fewer questions than patients who immediately moved to palliative care. There are also patients and family members with lots of questions: '_Some are grilling me with questions_'.

These findings confirmed the difficulty to predict information needs in a palliative care context.

##### Effect of information

A diversity of responses was also noted on the effect of information. Some respondents reported that information reassured them: '_I received information on the side effects of chemotherapy. I was prepared… when I did not experience all the side effects… I felt I am doing well_', while information meant increased anxiety for others: '_At the moment information is confusing me… it actually makes me afraid_'; '_It is very difficult - I now have the information, but it does not bring that comfort_'; '_Things do not always go according to plan - [I then] experience increased anxiety_'. Healthcare professionals pointed out that anxiety is often increased by well-meaning family members and friends exploring the Internet finding information on different types of cancer and different stages than those applying to the patient.

Findings confirmed the diversity of effects information may have and therefore also implied the difficulty in predicting how, when and to whom information should be provided. Again a systematic review and audit of effects of information on patients in palliative care may help to deepen our understanding of information behaviour.

##### Time when information is required

Diversity between the needs of patients and family members with regard to the time when information is required and healthcare professionals' perceptions on this was also noted. Some patients and family members would only ask for information when actually required e.g., on pain control: '_Do not want to plan ahead_', while others need information to plan ahead: '_I want to know where I am going… I want to see the long term picture_'. This particular participant did not want to leave planning to her family when she gets too ill to make her own decisions: '_I do not want to leave everything for the family_'. She considered herself a bigger picture person. Some healthcare professionals noted that patients in palliative care want less information while their families want more. Others noted that shortly before dying patients seem to have a lot of questions on what is going to happen and sometimes they need information with regard to concerns on e.g., the family being left with inadequate financial support. According to healthcare professionals the need for information also depends on the levels of anxiety being experienced.

##### Role and responsibility in dealing with information

Different roles in dealing with information were reported. In some instances a family member will act between the doctor and patient as well as other family members: '_I explain to my mother (wife of patient)… she understands even less than me_'; '_My son has Internet access - he researches everything_', while others preferred to explain to their family members because '_Information will pull them down_'. A close synergy in sharing information was also clear from some responses: '_My wife read to me when I was too tired_'. It would be interesting to do further research on the actual dynamics of accepting responsibility to seeking information, as well as in shifting the responsibility.

Nurses especially were sensitive to family dynamics and noted that an increase in questions and a need for information may arise in contexts where there are many unresolved issues in families. One of the oncologists also prefers to deal with the patient and with only one family member as spoke person for the rest of the family.

##### Preferences for information sources

Although there were no reports on not trusting the doctor, even patients considering the doctor as their sole information source: '_I mostly relied on the doctor_'; '_At time of diagnosis the doctor provided information_' admitted using other sources. '_I was not interested in hearing anything from anybody but him_'; this patient, however, also explained that she used the Internet to make social contact with other patients. Others reported trusting the doctor, but also having a need to consult other sources, even if it is only to realise that the doctor is on the right track and well-informed: '_Doctor the most important… but I ask everybody... a guy needs to ask everywhere_'. Even when reporting to receive no information from the doctor: '_Dr did not tell me anything_', a patient claimed to be quite content, because she had a strong social network and friendship circle. In other cases such social networks included experts in the cancer context such as a pathologist friend: '_She explained to me from day one_'. According to Mills and Davidson

> It is important to note that patients do not develop coping strategies through receiving information from one or two sources, rather they tend to use a combination of sources, which vary with time in order to satisfy their information needs. ([Mills and Davidson 2002: 375](#mil02))

Clayton _et al._ ([2005](#cla05)) also note diversity in the provision of information regarding prognosis and end-of-life care.

Some of the oncologists noted the value multimedia methods such as videos and drawings, as well as booklets may have on caring for cancer patients who are dying. The value of a Website (i.e., portal) was also noted, but with immediate reference to the tremendous cost implications:

> There is much one would like…, but everything gets down to affordability… need a library… including books, videos, DVDs and Internet access… need people who can advice patients… you actually need a team that can offer information… a power effort [which involves more than input from a single treating medical clinic or institution].

Healthcare professionals also stressed the need for patients to talk to other patients and the impact a strong support network may have on the information needs of patients and their family members. It seems, therefore, as if further research on multimedia approaches, including serious gaming as well as social networking and family dynamics in information seeking might be useful.

##### Type of information

The type of information used or needed by patients and their families seemed very diverse ranging from emotional information to only factual information. A patient needing information on emotional support and coping did not necessarily recognise it as an information need: '_I am a human being… I do not understand what I have to do… I am scared that I will do the wrong thing_'. A male patient remarked: '_I read a few books on the meaning of life_', while another explained that he moved between factual, emotional and religious information depending on where he found himself in terms of his disease. There were also comments on the neglecting of psychosocial and emotional information: '_Psychosocial and emotional information very important, but very much neglected_'. One respondent made it clear that he was only interested in factual information. He also took responsibility to explain things to his mother who admitted not knowing how to react emotionally, totally relying on her son and not asking questions due to her low self-esteem. It appears as if she actually needs information on emotional issues.

Healthcare professionals were aware of the diversity of type of information required. How they address such information needs, however, did not feature strongly in their responses and would require a re-analysis of the collected data to pick up on possible finer nuances of dealing with information needs. Although they acknowledge emotional needs, they did not elaborate on how to full-fill these needs. It seems as if patients are more reluctant to share emotional needs with the oncologists or doctor. In contrast the nurses are very sensitive for body language expressing emotional needs: '_Even if it is non verbal attention… holding a hand… not always factual information that is necessary_'. If there is a better understanding of information needs, it might be possible to advise nurses, for example, to play a stronger role in meeting the information needs of patients.

##### Reasons for seeking information

A diversity of reasons for seeking information was reported including planning ahead: '_I need information to make a difference to the immediate future; do not want to be caught unprepared_', verifying information and considering different angles, as a form of hope in being in contact with survivors (one patient accessed [Hope from IBC Website](http://www.ibcresearch.org/) to make contact with a survivor for emotional support: '_Really, really, all I want to know is what it means to be called a survivor_'). Interestingly enough patients did not necessarily seek information to participate in decision-making: '_Does not necessarily influence my reaction to the doctor's decision… do not always want to argue with the doctor - at least you know he is clued up_'.

Healthcare professionals noted patients with poor prognosis often spent more time searching for information especially on the Internet. Others noted that some patients will keep on asking questions until they hear what they want to hear.

##### Willingness to ask for information

Patients differed from admitting to never asking for information: '_It is part of my low self-esteem_', to always asking information when required: '_If it comes to my health, then I ask_'. Others were hesitant to ask: '_I do not feel comfortable to ask. I get the impression they think you do not know what you are talking about_'. And sometimes they had no idea whom to ask.

Healthcare professionals confirmed that some patients and family members are reluctant to ask and sometimes they do not at all wish to discuss the issue.

Considering an earlier finding that patients and their families do not always realise information needs, a lack of willingness to ask for information need to be foreseen when planning for the provision of information: information should be available for those who do not feel like asking for information or starting their own search efforts, in case they change their mind and also to raise awareness of potential information needs. Friedrichsen also confirms that:

> These studies suggest that patients do want to talk honestly about the seriousness of their disease, but this does not mean that they are prepared to take the initiative… There seems to be a discrepancy between preferences [wanting to know] and reality. ([Friedrichsen 2002: 8-9](#fri02))

In continuing the research it seems worthwhile to consider the value for patients to first have information they can read and study in their own time at their own pace and then to offer opportunities for personal contact and interaction and support in contextualising the information to their own circumstances. In addressing this issue in further research it might be useful to draw on the literature of adult and distance learning e.g., as discussed by [Knowles _et al._ (2005)](#kno05) and Moore ([2007](#moo07)).

##### Need for other people to be informed about the disease and prognosis

Although some patients and their families felt that the whole family, relatives and friends should know about their disease and prognosis, others felt that some family members will not be able to deal with a poor prognosis: '_My mother should not know about the prognosis - she will not do well with it_' ; her father was informed about the [poor] prognosis, however. Others ensured that their (young) children are informed and argued: '_If other people are informed then they also know…_'.

The oncologists and doctor confirmed that they will respect a patient's preference not to have family members informed, but will hardly ever even consider to keep the patient uninformed.

##### Using the Internet

Most patients and their families admitted searching the Internet or at least having contact with somebody searching and sharing information with them. The following non-use of the Internet, however, needs to be noted: '_My daughter does not have time to search_' (the patient died within two weeks of the interview); '_My husband is an engineer. He does not like the Internet_' (coming from a 78 year old patient) and '_I sit on my hands_', i.e., to keep herself from using the Internet. In spite of being a qualified nurse she had been warned against using the Internet; although this participant showed a very strong desire to participate in the decision-making process and to plan ahead she took the warning seriously.

Although some of the oncologists encouraged their patients to search on the Internet and offered Web addresses, there was also concern for the unreliable information patients found on the Internet and their inability to contextualise such information. Some of the oncologists especially seem to underestimate the needs of patients and families and their use of the Internet. '_They (patients in palliative care) do not look on the Internet - they go by word of mouth_'. The growing interest of cancer patients in searching the Internet is, however, also confirmed by Meier _et al._ ([2007](#mei07)).

#### Range of factors influencing the information behaviour of patients and their family

Similar to findings reported in the literature, there is no agreement on the factors influencing information needs and information behaviour ([Friis _et al._ 2003](#fri03); [Lichter 1987](#lic87); [Thomson and Hoffman-Goetz 2007](#tho07); [Van der Molen 2000: 49](#van00)). Not only do individual patients and family members differ in influences noted, but there also is a difference between the perceptions of nurses, oncologists and doctors. Only a few factors are noted here.

##### Factors noted by patients

Patients noted that the availability of friends: '_From the first day lots of friends around me_' made it easier to cope and that they had less need for information. They also noted support from a religious group, too much information all at once, lack of time for explanation and the impact of their profession: '_Based on my profession - very comfortable in using the Internet as an information source_' (engineer) while another participant had negative experiences because of her profession: '_They think you know… but you don't know; you are the patient… If I have another chance, I will not mention my profession_' (nurse). Both patients and family noted a lack of time to ask for explanations as a barrier: '_Cannot ask everything from doctor… I know others are waiting to have a quick chance of seeing him_'.

##### Factors noted by family members

When discussing factors influencing their information needs and information behaviour, family members noted the difficulty of dealing with medical terminology and the complexity of information given by the doctor: '_They talk on their level… way above our heads_', and uncertainty of whom to ask, or contradictory explanations: '_You get five nurses and five different explanations_'.

##### Factors noted by oncologists and doctor

The oncologists and the doctor (who specialises in palliative care) mentioned the following influencing factors: age: '_Older patients [have] more understanding … they understand life comes to an end_', severity of prognosis, time from diagnosis before moving to palliative care: '_Depend on whether they immediately went into palliative care…_', and how poor prognosis is communicated to a patient (one of the oncologists felt that his body language may increase patients' anxiety). This confirms a remark by Friedrichsen:

> The experience of receiving and providing information about discontinuing tumour specific treatment is like crossing a border, where patients experience behaviour of the physician and the words they express of great significance. ([Friedrichsen 2002](#fri02))

(None of these factors was, however, noted by the patients or family.) The oncologists and doctor mostly felt that the type of cancer does not have an impact, but rather the socio-economic group, educational level and culture.

##### Factors noted by nurses

When questioned about factors influencing information needs and information behaviour, nurses pointed out the stage of the disease (e.g., more questions at time of diagnosis), type of treatment (e.g., chemotherapy), an inability to apply the information to their personal situation, anxiety (they do not take in the information and requires lots of repetition: '_It depends on their fears_') as well as other relationships: '_It depends on their relationship with their family_'. Supporting personal networks is especially important in South Africa with family members often living abroad. Some argued that the needs of patients with breast cancer will differ and that patients with a visual cancer such as a neck or eye tumour may be too shy to interact with healthcare professionals. (This could be explored in more detail in a follow-up phase.) The feeling often, however, was that the '_type of cancer does not have an influence. Patient specific… not cancer specific_'. Nurses also mentioned socio-economic groups, educational level and probably culture as influencing factors. In general nurses seem to be more alert to the information needs and information behaviour of patients and family members.

There is some overlap between the factors noted by oncologists and nurses. These should, however, be plotted against the factors noted by patients and family members to get an overall picture of factors potentially influencing information needs and information behaviour. In addition it should be supplemented by an audit of information needs and influencing factors reported in the subject literature.

#### Importance of self-identity and coping style

Self-identity and coping style as influencing factors in addition to role have often been mentioned as influencing factors ([Van der Molen 1999](#van99)). Neither of these nor personality were explicitly noted by healthcare professionals, except for one oncologist: '_It depends on their personality… people who like to be in control… would like to know in advance what will happen_'. Without being asked, a number of patients and family members, however, stressed their self-identity, personality type or coping style: '_This is unfortunately how I am… I am too scared to ask… part of my low self-esteem_'; '_I look at the bigger picture'…'think in boxes and squares_' (son of a patient); '_I like to prepare myself for the bigger picture; I am a bigger picture person. I want to know everything now… prepare, finish_' (female patient) (considered in the context of the specific interviews the two participants had different perceptions of what _bigger picture_ entails); '_I always was happy-go-lucky_' and '_I want to live life fully with quality_'.

It seems that in addition to looking at other ways for deepening our understanding of information needs and information behaviour in cancer palliative care contexts, it might be worthwhile to explore the impact of self-identity and coping styles, while also noting contradictory findings about the latter e.g., Miller ([1995](#mil95)) vs Williamson and Manaszewics ([2002](#wil02)).

#### Using different perceptions to identify and understand information needs

<div>![Figure 1: Triad data collection filling gaps in understanding information needs](p360fig1.png)</div>

From the preceding discussion it seems as if our understanding of all facets of the information needs and information behaviour of patients and their families in cancer palliative care contexts need to be further developed by supplementing information needs and information behaviour reported by patients and family members as well as by other role players in the particular context (e.g., oncologists, doctors, nurses). This can be done by means of an audit of information needs reported in the subject literature as well as mapping the information needs reported by patients and family members to needs noted by healthcare professionals. More interviews with all role players in the cancer palliative care context would also be necessary to identify a wider spectrum of potential information needs. It is also essential to pursue different ways of data collection to ensure that gaps in our awareness of potential information needs are filled.

Figure 1 depicts how a triad of participants from different contexts (i.e., patients, family members, healthcare professionals) can be used to fill gaps in our understanding of information needs and information behaviour in a context where information is needed to make sense of not only a life-threatening disease, but also a life-ending, existential context. These would cover the various aspects discussed in this article, as well as others that might be identified in follow-up analysis, as well as detailed systematic literature reviews to supplement what we know about the information needs, information behaviour and the factors influencing these in cancer palliative care contexts.

## Discussion

Due to the complex nature of the existential context of the exploratory project the findings mentioned in the preceding sections should be seen as a preliminary analysis only that need to be further subjected to rigorous re-analysis of the information rich collection of data (only some aspects are explored in this article) as well as the collection of additional data from various contexts such as involving a larger group of patients and family members and other role players in the cancer palliative care context such as dieticians and social workers. Before doing this, the literature should, however, be explored in more depth, as well as more breadth. The latter would require the identification of fields that may direct the analysis and the collection of data for a second phase, such as literature on death and dying ([Glaser and Strauss 2005](#gla05)), patient education and communication ([Moore and Spiegel 2004](#moo04)), as well as the learning sciences and cognitive sciences ([Sawyer 2005](#saw05)) and the link between information seeking, learning and learning theories (e.g., as explored by [Kuhlthau 2004](#kuh04)). Systematic reviews of various issues identified in the exploratory project would also be useful. These include:

*   Dormant information needs, unawareness of information needs and difficulty in expressing information needs.
*   Reasons for information needs and information seeking, whether there are any links to sense-making and the nature of such links.
*   Links between information and coping in palliative care as well as cancer contexts (seen separately and in combination).
*   Models of information seeking and information behaviour as well as health communication and health care that may shed light.
*   Theories that may shed light on information behaviour in existential contexts.
*   Role of personality, self-identity and coping style.

Such reviews should be used to explore new ways of deepening understanding of information behaviour as well as pointing out shortcomings in the existing research. In the literature review for the exploratory project it was noted that there is for example very little evidence of studies from the biomedical and healthcare disciplines following a wider interpretation of information behaviour as explained by Wilson ([1999: 249](#wil99)). Studies are mostly limited to information needs and information seeking in a much narrower sense.

Although there are some overlaps, cancer palliative care seems to be marked by very individual expressions of information needs and experiences of information behaviour. This applies to patients and family members, as well as the perceptions of healthcare professionals and is in line with the individual nature of information needs noted by Van der Molen ([1999](#van99)) and Williamson and Manaszewicz ([2002](#wil02)). Patients fluctuate in their specific context or situatedness in terms of what they need, when they need it and how they need it. In the same context members of a family would express different information needs and different information behaviour (e.g., differences between a mother and son where the farther was a patient). At times it seemed as if the healthcare professionals could not be talking about the same patients we interviewed and as if the oncologists and doctor (in the same setup) were not treating the same body of patients as the nurses. The needs, experiences and perceptions they expressed based on the question prompts of the interviewers were very different, but also with some overlap. Each interview offered new data for reflection and it seemed as if we could continue _ad infinitum_ to collect new data to improve our understanding. Although patients' experiences of the healthcare corps were very different, they never expressed doubt in their competency or judgment. '_The doctor knows it all_' captures the feedback from patients and family members well. Although they trusted the doctor and healthcare professionals, they still turned, however, to a number of other resources and often to the Internet. They still expressed frustrations.

The complexity and diversity of information behaviour in an existential context became clearer with each interview. Although this might be true, the purpose of an exploratory study is to gain _some_ understanding that can be used to explore the importance of the research problem, to direct further research and to offer preliminary guidelines for implementation in praxis. Although the intention of research is to find ways to predict information needs and information behaviour, this is not possible at this stage of the exploratory project. The findings noted in the article, may however help to strengthen awareness for the need to cater for all such needs and preferences, on all levels of detail and complexity required, since all of these may potentially manifest at some or other time for some or other individual. Such awareness is especially important when considering the wealth of authoritative, information available via the Internet, which might easily be made available to patients.

On a practical level the diversity and fluctuation of information behaviour and difficulties to predict it should be noted. It is suggested that facets of information behaviour be viewed on continuums where information needs and information behaviour may be plotted at different continuum points pending patients' and families' awareness of their situatedness and their reaction (i.e., their behaviour) at a particular point in time. Although it does not seem possible at this stage to predict where individuals would find them on a spectrum in terms of information needs and information behaviour, it seems safe to assume that all of these spectrums and fluctuating movement on spectrums will manifest in cancer palliative care contexts offering support to a (often large) number of people from diverse backgrounds, cancer locations (i.e., types of cancer), supporting systems, cultural groups, etc. In other words, when considering information provision and communication to a large population of patients and family members (e.g., by means of a portal) it might be worthwhile to allow for all spectrums and points on the continuum: somewhere there would be somebody that fits a particular continuum point. Table 5 reflects a more detailed view of information needs, information behaviour and influencing factors seen on continuums of various spectra with different continuum points that can be used in information provision on a practical level. The spectra and continuum points need to be further developed as explained in an earlier section.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 5: Viewing information needs, information behaviour and influencing factors on continuums of various spectra with different continuum points as noted from research**</caption>

<tbody>

<tr>

<th>Information needs</th>

<th> </th>

</tr>

<tr>

<td>Desire for information</td>

<td>As much as possible ................................. Nothing</td>

</tr>

<tr>

<td>Point in time when required</td>

<td>Immediately ..... When needed ..................... Never</td>

</tr>

<tr>

<td>Type of information</td>

<td>Factual ...... Different combinations............Emotional</td>

</tr>

<tr>

<td>Reasons for needing information</td>

<td>To cope.....To make decisions....................To verify</td>

</tr>

<tr>

<td>Etc.</td>

<td> </td>

</tr>

<tr>

<th>Manifestations of information behaviour</th>

<th> </th>

</tr>

<tr>

<td>Information sources</td>

<td>Doctor only ... Doctor & family ... Internet ... Everybody</td>

</tr>

<tr>

<td>Willingness to ask</td>

<td>Always .... When necessary ........................... Never</td>

</tr>

<tr>

<td>Responsibility for seeking information</td>

<td>Patient .... Family member ..................... Joined effort</td>

</tr>

<tr>

<td>Using the Internet</td>

<td>Never .... Now & then ............................. Constantly</td>

</tr>

<tr>

<td>Having other people informed</td>

<td>Nobody else .... Some people .................... Everybody</td>

</tr>

<tr>

<td>Etc.</td>

<td> </td>

</tr>

<tr>

<th>Influencing factors</th>

<th> </th>

</tr>

<tr>

<td>Age</td>

<td>Young need more information ..................................</td>

</tr>

<tr>

<td>Time since diagnosis</td>

<td>Immediately after diagnosis ..... With poor prognosis ...</td>

</tr>

<tr>

<td>Self-identity</td>

<td>Encourage info. seeking .... Inhibit info. seeking .........</td>

</tr>

<tr>

<td>Time for explanations</td>

<td>Too little time ......................................................</td>

</tr>

<tr>

<td>Religion</td>

<td>Relied on religion and religious group (less questions) ...</td>

</tr>

<tr>

<td>Etc.</td>

<td> </td>

</tr>

</tbody>

</table>

For the provision of information on a practical level, it also seems worthwhile to analyse literature on independent and adult learning as well as distance learning ([Knowles _et al._ 2005](#kno07); [Moore 2007](#moo07)) for ideas on how information needs and information behaviour noted in this exploratory study can be addressed in patient education and information communication to patients and family members. This is in addition to what might be offered by literature from the learning sciences ([Sawyer 2005](#saw05)) and learning theories (e.g., as explored by [Kuhlthau 2004](#kuh04)). The need for information to be contextualised and the need for human intervention, as clearly pointed out by this project, however, should also be considered for this route of pursuing the research on a practical level. Even when information is provided, patients and their families find it difficult to interpret and apply the information in their context: they found it difficult to contextualise information. In the data collected there was, however, very little explicit reference to sense-making. Needs for information was mentioned with regard to preparation for treatment, and knowing about the disease and prognosis, but not specifically with regard to making sense of their situation, i.e., making sense of dealing and coping with cancer and dying. The data therefore need to be re-analysed for implicit indications of a need for sense-making, which has been suggested by ([Dervin 1999](#der99)) as an important instigator of information seeking.

The oncology social worker who conducted the interviews with patients and family members noted that the interviews opened opportunities to reach out to them, to make them aware of gaps they may have in dealing with their situation and to make them aware of a human being with the expertise and training that can help them to contextualise information and to make sense of the information in their specific context.

Although patients and their families expressed information needs, they also reflected unexpressed and dormant information needs. It became clear that most patients and their families do not necessarily interpret their context in terms of gaps that can/need to be filled by information. From this exploratory study, it appears that patients and their families are either not aware of the fact that they are in palliative care or either do not explicitly acknowledge it. Even when acknowledging that there is no cure, they expressed different levels of information need and very individual, as well as fluctuating manifestations of information needs and information behaviour were noted.

Healthcare professionals have different opinions on the extent to which patients in palliative care need information and the scope of information needed. When analysing the responses of healthcare professionals it also seems as if they are not always sensitive to the fact that there may by many unexpressed and dormant information needs and that there is considerable differences between their perceptions of the information needs and information behaviour of patients and their families and how patients and their families describe their own experiences and information needs. Taking the different perceptions, expressions and experiences in combination may, however, _add_ to gaining an overall picture of information behaviour in a particular context.

Although factors influencing information behaviour were noted, the complexity and interwoven nature thereof make it difficult to use these to predict information needs and information behaviour in cancer palliative care contexts. It is also difficult to shed light on contradictory perceptions. A few gaps in our understanding of information needs and information behaviour can, however, be identified at this stage and need to be pursued in follow-up projects, namely  

*   Gap in understanding the scope of fluctuation in human information behaviour in cancer palliative care contexts.
*   Gap in understanding the spectrum of information needs in cancer palliative care contexts.
*   Gap in understanding the breadth and unpredictability of manifestations of information behaviour in cancer palliative care contexts.
*   Gap in understanding contrasting findings in variables that impact on information behaviour in cancer palliative care contexts, especially coping style and self-identity.
*   Gap in understanding diversity of information behaviour in the same incident of cancer (i.e., differences between patients and various family members facing the same incident).

Apart from what has been noted in the preceding paragraphs, it is suggested that further research could also focus on:  

*   Understanding the differences in the perceptions of healthcare professionals on the information needs and information behaviour of patients and family members in cancer palliative care contexts with specific reference to the impact of their role (as seen by their institution/employer), their perception of their role, the nature of their contact with patients and family members and the time they spent with them.
*   Ways of dealing with dormant and unexpressed information needs.
*   Effect if patients and family members do not have information in contrast to reported effects of information on patients and family members.
*   Awareness of emotional information needs and how to deal with these.

Although very time-consuming, the qualitative research approach based on semi-structured interviews directed by a questionnaire for the patients and family members and by an interview schedule for healthcare professionals proofed very valuable to collect information rich data that certainly need to be revisited following an extended review of the subject literature. It proofed particularly valuable to have the interviews with the patients and family members conducted by a trained medical/oncology social worker. She was very comfortable in talking to patients and family members. She knew how to let them talk with the minimum of interruptions and also how to interrupt interviews if participants got too emotional. She noted that the interviews offered excellent opportunities to make contact with patients and family members and to create a space for follow-up work with them. Another approach might be to seek in-depth understanding of information behaviour in individual contexts, such as investigating information behaviour of all role players in a specific case since diagnosis in a palliative phase using the more limited interpretation of palliative care linked to no cure and death and involving input from all involved in supporting the patient and family. Although very valuable this would require an extremely sensitive approach to data collection and the full support of a social worker and the rest of the oncology support team.

## Conclusion

Information needs and information behaviour in an existential context is extremely complex and requires a lot more work. We need to draw on a wide variety of methods and resources, ongoing reflection and extensive literature reviews.

This exploratory study can only touch on some aspects that might shape our understanding. In deepening our understanding of cancer palliative care contexts, the following seems to be the most import for a first round of pursuing the research. More attention should be paid to unexpressed and dormant information needs (e.g., through information audits, extensive literature reviews and repeated data analysis) as well as collecting perceptions from all role players in the situation (e.g., dieticians, social workers and more patients and family participants). Noting the diversity and fluctuation of information behaviour and difficulties to predict it, facets of information behaviour need to be viewed on continuums where information needs and information behaviour may be plotted at different continuum points pending patients' and families' awareness of their situatedness and their response at a particular point in time. Patient reported information needs should be supplemented by often contrasting perceptions of their information needs and information behaviour as seen by oncology healthcare professionals. A re-analysis of the data and the literature should be used to explore models from information seeking as well as from other disciplines that might support a further study of manifestations of information behaviour, as well as other aspects note in this article.

## Acknowledgements

My appreciation to colleagues attending the ISIC2008 conference in Vilnius, 17-19 September 2008, for offering suggestions on my paper that could be incorporated into this article (e.g., reading the work of [Glaser and Strauss 2005](#gla05)).

## References

Ankem, K. (2006). Factors influencing information needs among cancer patients: a meta-analysis. _Library and Information Science Research,_ **28**(1),7-23\.

Baker, L.M. (2004). Information needs at the end of life: a content analysis of one person's story. _Journal of the Medical Library Association,_ **92**(1), 78-82\.

Barnoy, S., Bar-Tal, Y., & Zisser, B. (2006). Correspondence in informational coping styles: how important is it for cancer patients and their spouses? _Personality and Individual Differences,_ **41**(1), 105-115\.

Beaver, K. & Booth, K. (2007). Information needs and decision-making preferences: comparing findings for gynaecological, breast and colorectal cancer. _European Journal of Oncology Nursing,_ **11**(5), 409-416\.

Beaver, K. & Witham, G. (2007). Information needs of the informal carers of women treated for breast cancer. _European Journal of Oncology Nursing,_ **11**(1),16-25\.

Belkin, N.J., Oddy, R.N., & Brooks, H.M. (1982). ASK for information retrieval. Part I. _Journal of Documentation,_ **38**(2), 61-71\.

Case, D.O. (2006). Information behaviour. _Annual Review of Information Science and Technology,_ **40**, 293-328\.

Case, D.O. (2007). _Looking for information: a survey of research on information seeking, needs and behaviour._ 2nd ed. Amsterdam: Elsevier.

Case, D.O. andrews, J.E., Johnson, J.D., & Allard, S.L. (2005). Avoiding versus seeking: the relationship of information seeking to avoidance, blunting, coping, dissonance and related concepts. _Journal of the Medical Library Association,_ **93**(3), 353-362\.

Clayton, J.M., Butow, P.N., & Tattersall, M.H.N. (2005). The needs of terminally ill cancer patients versus those of caregivers for information regarding prognosis and end-of-life issues. _Cancer,_ **103**(9), 1957-1964.

Courtright, C. (2007). Context in information behaviour research. _Annual Review of Information Science and Technology,_ **41**, 273-306.

Cox, A., Jenkins, V., Catt, S., Langridge, C., & Fallowfield, L. (2006). Information needs and experiences: an audit of UK cancer patients. _European Journal of Oncology Nursing,_ **10**(4), 263-272.

Dervin, B. (1999). On studying information seeking methodologically: the implications of connecting metatheory to method. _Information Processing and Management,_ **35**(6), 727-750\.

Dickerson, S.S., Boehmke, M., Ogle, C., & Brown, J.K. (2006). Seeking and managing hope: patients' experiences using the Internet for cancer care. _Oncology Nursing Forum,_ **33**(1), E8-E17\.

Docherty, A., Owens, A., Asadi-Lari, M., Petchey, R., Williams, J., & Carter, Y.H. (2008). Knowledge and information needs of informal caregivers in palliative care: a qualitative systematic review. _Palliative Medicine,_ **22**(2),153-171\.

Edgar, L., Greenberg, A., & Remmer, J. (2002). Providing Internet lessons to oncology patients and family members: a shared project. _Psycho-Oncology,_ **11**(5), 439-446.

Fisher, K., Erdelez, S., & McKechnie, L. (eds). (2005). _Theories of information behavior._ Medford, NJ: Information Today.

Friedrichsen, M. (2002). _[Crossing the border: different ways cancer patients, family members and physicians experience information in the transition to the later palliative phase.](http://liu.diva-portal.org/smash/get/diva2:21214/FULLTEXT01)_ Linköping, Sweden: Linköping University, Department of Biomedcidne and Surgery, Palliative Research Unit. (University Dissertation No. 727.) Retrieved 14 December, 2008 from http://liu.diva-portal.org/smash/get/diva2:21214/FULLTEXT01 (Archived by WebCite® at http://www.webcitation.org/5d7KoORJS)

Friedrichsen, M., & Milberg, A. (2006). Concerns about losing control when breaking bad news to terminally ill patients with cancer: physicians' perspective. _Journal of Palliative Medicine,_ **9**(3), 673-682\.

Friis, L.S., Elverdam, B., & Schmidt, K.G. (2003). The patient's perspective: a qualitative study of acute myeloid leukaemia patients' need for information and their information-seeking behaviour. _Support Cancer Care_ , **11**(3), 162-170.

Fukui S. (2004). Information needs and the related variables of Japanese family caregivers of terminally ill cancer patients. _Nursing and Health Sciences,_ **6**(1), 29-36.

Gattellari, M., Voigt, K.J., Butow, P.N., & Tattersall, M.H.N. (2002). When the treatment goal is not cure: are cancer patients equipped to make informed decisions? _Journal of Clinical Oncology,_ **20**(2), 503-513.

Glaser, B.G. & Strauss, A.L. (2005). _Awareness of dying._ Edison, NJ: Transaction Publishers.

Hopkinson, J., & Corner, J. (2006). Helping patients with advanced cancer live with concerns about eating: a challenge for palliative care professionals. _Journal of Pain and Symptom Management,_ **31**(4), 293-305.

Iconomou, G., Viha, A., Koutras, A., Vagenakis, A.G., & Kalofonos, H.P. (2002). Information needs and awareness of diagnosis in patients with cancer receiving chemotherapy. _Palliative Medicine,_ **16**(4), 315-321.

Järvelin, K., & Wilson, T.D. (2003). [On conceptual models for information seeking and retrieval research](http://informationr.net/ir/9-1/paper163.html). _Information Research,_ **9**(1), paper 163\. Retrieved May 8, 2008 from http://InformationR.net/ir/9-1/paper163.html (Archived by WebCite® at http://www.webcitation.org/5d7KJTTUC)

Jepson, R.G., Hewison, J., Thompson, A. & Weller, D. (2007). Patient perspectives on information and choice in cancer screening: a qualitative study in the UK. _Social Science & Medicine,_ **65**(5), 890-899.

Jimbo, M., Nease, D.E., Ruffin, M.T., & Rana, G.K. (2006). [Information technology and cancer prevention.](http://caonline.amcancersoc.org/cgi/content/full/56/1/26) _CA: a Cancer Journal for Clinicians,_ **56**(1), 26-35 Retrieved May 8, 2008 from http://caonline.amcancersoc.org/cgi/content/full/56/1/26 . (Archived by WebCite at http://www.webcitation.org/5d7K7WZVH)

Johnson, J.D. (1997). _Cancer-related information seeking._ Cresskill, NJ: Hampton Press.

Johnson, J.D. (2003). On contexts of information seeking. _Information Processing & Management,_ **39**(5), 735-760.

Johnson, J.D. andrews, J.E., & Allard, S. (2001). A model for understanding and affecting cancer genetics information seeking. _Library & Information Science Research,_ **23**(4), 335-349.

Kirk, P., Kirk, I., & Kristjanson, L.J. (2004). What do patients receiving palliative care for cancer and their families want to be told? A Canadian and Australian qualitative study. _British Medical Journal_ (International Edition), **328**(7452), 1343-1347\. Retrieved 14 December, 2008 from http://www.bmj.com/cgi/content/full/328/7452/1343 . (Archived by WebCite® at http://www.webcitation.org/5d7K0bhqs)

Knowles, M.S., Holton, E.F., & Swanson, R.A. (2005). _The adult learner: the definitive classic in adult education in human resource development._ 6th ed. Amsterdam: Elsevier.

Kuhlthau, C.C. (1991). Inside the search process: information seeking from the users' perspective. _Journal of the American Society for Information Science,_ **42**(5), 361-371.

Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services._ 2nd edition. Westport, CT. Libraries Unlimited.

Kutner, J.S., Steiner, J.F., Corbett, K.K., Jahnigen, D.W., & Barton, P.L. (1999). Information needs in terminal illness. _Social Science & Medicine,_ **48**(10), 1341-1352.

Kvåle, K. (2007). Do cancer patients always want to talk about difficult emotions? A qualitative study of cancer inpatients communication needs. _European Journal of Oncology Nursing,_ **11**(4), 320-327.

Lichter, I. (1987). _Communication in cancer care._ Edinburgh: Churchill Livingstone.

Meier, A., Lyons, E.J., Frydman, G.,Forlenza, M.J., & Rimer, B.K. (2007). How cancer survivors provide support on cancer-related Internet mailing lists. _Journal of Medical Internet Research,_ **9**(2), E2.

Milberg, A., Olsson, E-C., Jakobson, M., Olsson, M., & Friedrichsen, M. (2008). Family members' perceived needs for bereavement follow-up. _Journal of Pain and Symptom Management,_ **35**(1), 58-69.

Miller, S.M. (1995). Monitoring versus blunting styles of coping with cancer influence the information patients want and need about their disease. _Cancer,_ **76**(2), 167-177.

Mills, M.E., & Davidson, R. (2002). Cancer patients' sources of information: use and quality issues. _Psycho-oncology,_ **11**(5), 371-378.

Moore, M.G. (2007). _Handbook of distance education._ 2nd ed. London: Routledge.

Moore, R.J. & Spiegel, D. (eds.) (2004). _Cancer, culture and communication._ New York, NY: Kluwer Academic.

Newnham, G.M., Burns, W.I., Snyder, R.D., Dowling, A.J., Ranieri, N.F., Gray, E.L., et al. (2006). Information from the Internet: attitudes of Australian oncology patients. _Internal Medicine Journal,_ **36**(11), 718-723.

O'Donoghue, T. (2007). _Planning your qualitative research project: an introduction to interpretivist research in education._ London: Routledge.

Parker, S.M., Clayton, J.M., Hancock, K., Walder, S., Butow, P.N., Carrick, S., et al. (2007). A systematic review of prognostic/end-of-life communication with adults in the advanced stages of a life-limiting illness: patient/caregiver preferences for the content, style and timing of information. _Journal of Pain and Symptom Management,_ **34**(1), 81-93.

Pickard, A.J. (2007). _Research methods in information._ London: Facet Publishing.

Ramanadhan, S., & Viswanath, K. (2006). Health and the information nonseeker: a profile. _Health Communication,_ **20**(2), 131-139\.