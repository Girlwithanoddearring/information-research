####  vol. 13 no. 4, December, 2008

# Rhythms of “being” at ISIC - understanding the place of the ISIC conferences in information seeking research

#### [Theresa Dirndorfer Anderson](mailto:anderdorfer@gmail.com) and [Jo Orsatti](mailto:jo.orsatti@yahoo.com.au)  
Information and Knowledge Management Program, Faculty of Arts and Social Sciences, University of Technology, Sydney, Broadway, NSW 2007, Australia

####  Abstract

> **Introduction**. The paper reports on findings from a project merging exploring the professional practice of academics, research students and practitioners within the ISIC community, drawn from fieldwork at the 2006 Information Seeking in Context (ISIC 2006) conference in Sydney, Australia.  
> **Method**. The project used diverse ethnographic and unobtrusive techniques to locate and describe the range of activities taking place during the 2006 ISIC conference.  
> **Analysis**. Both authors names and conference titles were collected from all conferences and mapped to see if core themes could be identified. Themes were compared to the topics of interest elicited from two conference surveys distributed at ISIC 2006.  
> **Results**. People attend ISIC conferences because of a desire to connect with researchers, not necessarily because of specific research (areas). However, the interests of ISIC 2006 participants fall well within the core themes and clusters characterising ISIC papers since 1996\.  
> **Conclusions**. The project contributes to a fuller understanding of the interlacing of research and information practices and ISIC's contribution to information behaviour research.


## Introduction

Research conferences are information-intensive sites of activity and an essential component in the professional practice of academics, research students and practitioners in any field. Such conferences enable the convergence of and interplay between formal and informal communication channels as well as mediated and personal information exchanges. Taking part in a conference like the biennial Information Seeking in Context conferences (ISIC), for instance, can be framed as both a research and an information practice. As a site of information practice, the conference can be examined in terms of participants' purposive information seeking, incidental or serendipitous encounters with information and formal and informal information exchanges. As a site of research practice, the conference can be examined in terms of its contribution to learning, scholarly communication and engaging with research and researchers in the field. The findings discussed in this paper are drawn from a project merging exploration of these two practices within the ISIC community. As one of the participants in the ISIC 2006 conference in Sydney reported, ISIC conferences are the '_.primary learning arena for [me]: keeping informed of current research, collegial exchange of ideas_' (Phil (respondents are assigned a pseudonym)). In response to the question What does ISIC mean to you?, he remarked that ISIC is the '_primary mechanism for engaging with key researchers in the information behaviour field._' Conference activity is an information behaviour, which, as Phil's remarks illustrate, is a critical component of being a researcher or practitioner in the field. The overlap of the formal and the informal aspects of social interactions, knowledge production and information gathering that are part of scholarly communication practice are particularly evident at a conference. As such, conferences provide an opportunity to study the interconnections between and within these practices.

Our paper explores the specific identity of ISIC 2006 within the context of the wider ISIC series. It examines the topics that (according to the informants in our project) seemed to bring people to the conference. We are interested here in exploring the appeal and the practice that revealed itself at ISIC 2006\. We are also interested in the trajectories arising from earlier conferences in the ISIC series that might be considered _core business_ for ISIC. This is, in our view, the beginning of a historical exploration of the practice of research within the ISIC community. Looking at what has been presented at the conferences since the first event in 1996 helps us to form a picture of the information about the field on offer. What is the thematic landscape in which attendees move at an ISIC conference? How do the day-to-day choices of conference participants interact with that landscape? What makes ISIC _ISIC_ for attendees?

## Project background and related literature

Conferences renew, inform and inspire us. They expose us to the latest research and provide the networking opportunities that connect us to professional colleagues and friends. The project from which the findings in this paper are reported explores the information and communication exchanges at a research conference and the role they play in academic work practices. The project is part of our ongoing exploration of research as a scholarly practice and the practice of research. Conferences provide forums for many concurrent social and academic activities. Our goal is to explore the rhythms of ISIC across the conference series as well as the rhythms present in the day to day activity taking place at a particular ISIC. The project was also envisaged as a way of creating a research narrative depicting the evolution of the conference since the first ISIC in 1996 and its contribution to developments in information behaviour research. The conference that took place in Sydney, Australia in July 2006 presented an opportunity to begin examining these activities within a specific community (information behaviour researchers).

At ISIC 2006, we piloted a range of techniques, which can contribute to a contextual understanding of the dynamics of this important component of scholarly communication. After providing some background about the first phase of the project and some of the research instruments used to study conference activity at ISIC 2006, this paper discusses the outcome of analysis from some of the survey data and what it suggests about the role that ISIC conferences play in the research and information seeking activities of the researchers and practitioners who attend them.

### Key themes under investigation

Conference activity has long played a prominent role in the information seeking of researchers. Olander's ([2007](#OLA2007)) longitudinal investigation of computer scientists, for instance, reported that '_carefully selected conferences and journals_' served as the main strategies for monitoring one's research area, practices that had not changed in any essential way in the twenty years between the first and second wave of her interviews with the scientists. Digital media may have changed the way we access journals and participate in a conference. Nevertheless, conferences remain a critical component of individual scholarship. In many ways an international research conference like ISIC is a site where all forms of scholarly communication interact: formal and informal modes of knowledge exchange, print and electronic media, grey literature and formal publications ([Borgman and Furner 2001](#BOR2001)).

Research activity is information intensive; no more so than at a conference. There are several aspects of conference activity and academic life that underpin this project. This section presents key themes informing the discussion presented in this paper.

#### Theme 1: knowledge sharing in an academic life

There has been a growing recognition of the need to examine the information behaviour of academics in context (e.g., [Ellis _et al._ 1993](#ELD1993); [Ocholla 1996](#OCH1996); [Selden 2001](#SEL2001); [Talja 2002](#TAL2002)). Meadows ([1990](#MED1990)) and Sandstrom ([2001](#SAN2001)) emphasise academic research as a form of social interaction and suggest that academic information seeking can be viewed as a communal activity within a socio-ecological system. Researchers like Talja ([2002](#TAL2002)) and Selden ([2001](#SEL2001)) further build on this exploration of the social practice associated with academic work. Such knowledge sharing plays an important role within the evolution of an academic's research activity. Anderson ([2006](#ATD2006)) found, for example, that conference attendances at timely points in a project or conversations with colleagues had noticeable impact on her informants' own research and information seeking practices.

The research described in this paper is part of a project that seeks to build on those findings. It also seeks to contribute to discussions by Talja ([2002](#TAL2002)) and Fry and Talja ([2004](#FRY2004)) about social sharing in academic communities and the role of conference papers in academic work ( [Drott 1995](#DMC1995)). In her examination of the context of academic information seeking, Talja ([2002](#TAL2002): 155) concludes:

> Scholars' social networks not only affect their choices of information seeking strategies; rather these networks are often the place where information is sought, interpreted, used and created.

At a research conference, the networks that facilitate these information practices manifest themselves in both the presentation of formal papers and the informal exchanges taking place during the course of the event.

#### Theme 2: Informal exchanges of information

This project builds on Paisley and Parker ([1968](#PAI1968)), whose study represents one of the earliest examinations of the role of conferences (in their paper referred to as scientific conventions) in the use of information. In the opening paragraph of that paper, Paisley and Parker note that, in contrast to the '_substantial artifacts of an information system_' like books and journals: '_The informal exchanges of information at a convention leave no record - except in the minds of scientists themselves_' ([Paisley and Parker 1968](#PAI1968): 65). Their work examines the '_scientific information flow_' through these informal exchanges, focusing on the way attendees at a social science convention acquire and use the information obtained there. While their study is forty years old, the questions explored continue to have relevance for a study of conference activity in our time. For this reason, one of the data collection tools for our project involved a revised version of the Paisley and Parker questionnaire. Our modifications are discussed further [below](#resdes).

#### Theme 3: Information encounters and serendipitous discoveries

A further thread relates to the significance of unanticipated or unplanned encounters with people and information that a person might experience while engaged in research. The qualities of serendipitous and unstructured encounters with information play a critical role in the hard sciences as well as in qualitative research ([Fine and Deegan 1996](#FIG1996); [Ford 1999](#FOR1999); [Konecki 2008](#KON2008)). These interactions and serendipitous encounters occur not only in the context of engagement with information, but in encounters with colleagues. The heady mix of people and ideas within the intensive setting of a research conference affords a valuable opportunity for studying this thread. Anderson ([2006](#ATD2006)) found that her informants used conference attendance as a way to kick-start their thinking about information gathering on particular topics as well as to stay current on the latest thinking in research areas of interest to them. Such practice differs from deliberate information seeking, however. These connection building opportunities can arise simply by attending and being exposed to the ideas that are circulating at a conference.

#### Theme 4: Intertwining research and information practices in scholarly life

A common feature through all these themes is the acknowledgment that conference activity be viewed as both a research and information practice. Knowledge production in the university of the 21st century is increasingly seen as a highly creative, multidisciplinary undertaking. Research practice, according to Noam ([1997](#NOA1997)) consists primarily of three elements: to create knowledge and standards; to preserve information; and to pass it on to others. It is a knowledge generation process whereby information is transformed into meaning by a researcher for further communication within a community. Every scholar is a '_link in the information chain_' ([Herman 2001](#HER2001): 393). Working with information thus is critically intertwined with research work; research work is laced with behaviour we might identify as information seeking. Studying the information practices of academics has to go hand in hand with studying academic work life.

Exploring research and information practices in the fullest possible context allows us to understand the complexity and diversity involved in both information seeking and academic work. This principle motivated our approach to the data collection (discussed below). Furthermore, as was indicated above, academic work has both strong social and individual characteristics. Both the people and information brought together at a research conference shape the experience. The aim of our project is to undertake an intense, fine-grained observation of this very complex and dynamic environment.

## <a id="resdes" name="resdes"></a>The “Day in the life of a conference ” Project

The project takes a _bricolage_ approach ([Kincheloe 2001](#KIN2001); [Lincoln 2001](#LIN2001); [Kincheloe 2005](#KIN2005)) using diverse ethnographic and unobtrusive techniques to locate and describe the range of activities taking place during ISIC 2006\. The field work forms the core of research narratives that follow the analytical approach reported in Anderson ([2006](#ATD2006)).These narratives involve two streams of collection (both of which are ongoing): one associated with collecting the stories of researchers with connections to ISIC since its beginnings in 1996 and the other associated with the activities of participants at ISIC conferences in 2006 and then in 2008.

Exploring the Web of relationships between conference participants and the processes by which knowledge is exchanged and research is produced requires an emergent approach to the research design in terms of both the gathering and the analysis of fieldwork ([Denzin 1989](#DNK1989); [Erlandson _et al._ 1993](#EDA1993)) inform our work, though the project draws on a range of methods and approaches to investigate work practices in context. The work is also informed by ethnographic principles for examining organisations and media use in relation to work ([Harper 2000](#HAR2000); [Hirsch and Gellner 2001](#HIR2001)). Another key theoretical orientation involves creative analytic practices typical of narrative research (e.g., [Clandinin and Connelly 2000](#CLA2000); [Richardson 2000](#RIC2000)). This emergent approach is integrated with a bibliometric analysis of ISIC conference citations ([Gläser and Laudel 2001](#GLA2001); [Cronin and Shaw 2002](#CRO2002)) to lend further support to the historical narrative crafted from the ethnographic elements of the fieldwork.

Two questionnaires were distributed (one before the conference, another at the conference) to help us build a composite picture of the ISIC community and the general information behaviour of conference participants. (See [Appendices](#AppA) for copies of these two research instruments.) The questions in the first survey instrument were designed to provide a better understanding of how the Conference fits into respondents' overall research work. The second survey instrument posed conference-specific questions. The questions for both these instruments adapted and extended Paisley and Parker's ([1968](#PAI1968)) questionnaire, distributed at the 1966 meeting of an interdisciplinary behavioural science convention. We chose to use their questionnaire as a starting point for the design of our own as it struck us as an under-used, but appropriate, instrument for our purposes. Their work remains influential ([Bates 2004](#BMJ2004)) and suggests the types of characteristics and information behaviour that are reportable and relevant to any study of information exchanges at a conference.

Unlike the earlier instrument used by Paisley and Parker, ours was divided into two parts, with the first part being electronically distributed to conference participants prior to the conference. In this way, responses to this first questionnaire could be used to identify key informants to involve in some of the other data gathering approaches. These included activity diaries and critical incident interviews ([Barry 1997b](#BCA1997a)), blogging and online discussions on a dedicated Website adapting the _directives_ of the autobiographical diary method ([Black and Crann 2002](#BLA2002)), photo-voice ([Wang and Burris 1997](#WAN1997)) and structured observations ([Gonzalez and Mark 2004](#GON2004)).

The second questionnaire (distributed throughout the conference) invited participants to share their impressions of ISIC 2006 with us. The date and time of each response was recorded so we could contextualise comments in relation to the conference programme. As with the pre-conference questionnaire, respondents had the option to remain anonymous. This two-page instrument adapted some of the questions from Paisley and Parker, which asked how respondents thought they might use information they heard or collected at the conference. Our survey form attempted to tabularise these questions in order to reduce their size in comparison with the original survey. Doing so meant responses could be collected quickly during coffee breaks and other intervals between conference sessions. The main focus of our instrument, however, was a series of four open-ended questions about respondents' impressions and interests at the conference followed by four quick response questions that invited them to share with us impressions, interests and attention-grabbers at various points throughout the conference. A contrast to the lengthier pre-conference questionnaire, this survey form resembled market research clipboard surveys. Our intention was to collect conference _sound grabs_ that could help us build a picture of the way information circulated at the conference and its potential significance for participants. To this end, a team of research assistants wandered through the conference inviting participants to respond to some of these questions orally if they chose to do so.

Along with data gathering techniques that directly involved participants, unobtrusive spatial analysis techniques ([Given 2003](#GIV2003)) were used to map the physical layout of conference venues and to record the ways people made use of various formal and informal spaces associated with the conference.

## Rhythms of activity at ISIC 2006

In this paper we focus on our analysis on questions posed in both survey instruments exploring what attracted attendees to the ISIC 2006 conference. The particular pre-conference survey questions we discuss relate to the programme listed [on the Website](http://www.webcitation.org/5d2opfXMN) and to whether respondents were interested in following research in particular areas at the conference. The two questions posed in the conference survey are: _What brings you to ISIC?_ and, _What does ISIC mean to you?_ We used the reasons people gave for attending the Conference and any particular areas of interest they were looking for as the starting point for our analysis.

In both sets of questions, respondents commented that they attended the Conference to learn about the _'latest research in the field'_ and to hear from and meet with researchers and practitioners. Such comments are consistent with earlier research about the role that conference attendance plays in scholarly life. Here the intertwining of research and information activity is at its most connected: what they want or hope to find at the conference will (as many of the informants themselves said) shape what information they will pursue and what and how they will use it. Using data collected at the Conference, we sought to explore how participants were defining _the field_. The diversity of responses prompted us to examine them further and construct a grounded taxonomy of their stated research interests. To identify the information made available to conference attendees through the formal programme, we also began charting both the topics and authors of the conference presentations. Examining _the field_ in terms of both survey respondents and the Conference programme series helped us to develop a fuller picture of the information landscape, which participants find themselves in at an ISIC conference.

## What is the ISIC landscape?

Examining the ISIC landscape began with the collation of all papers presented at ISIC conferences from the first in 1996, through to the most recent in 2006\. Two hundred and ten papers were presented at these conferences. Both authors names and conference paper titles were collected from all conferences as well as keywords from the 2004 and 2006 conference papers found in the metadata of issues of the _Information Research_ journal in which these papers were published.

### Mapping conference topics

To complete a keyword mapping across all conferences, keywords were extracted from the titles of papers presented at conferences from 1996 to 2002 as no publication metadata keywords are available for these earlier conferences. Title and keyword approaches were used to maintain as much as possible the representations of the papers as they were presented by their authors. For the 1996 to 2002 conferences minimal interpretation was attempted in the identification of keywords. A comparison of author-provided keywords for the 2004 and 2006 conference papers suggested that for the most part authors were using similar terminology within the two forms of representation. However it was evident in this analysis that author-provided keywords did tend to include a higher instance of broader categorisations for their own work, for example more instances of terms such as _information behaviour_ appeared than in the titles of the papers themselves. This is consistent with findings of Whittaker _et al._ ([1989](#WHI1989)) whose comparative study of title keywords and subject descriptors found that keywords and subject descriptors are comparable. However, keywords have a tendency to highlight _original_ aspects of the author's contribution whereas the subject descriptors tend to emphasise placement of the paper in relationship to other publications within the field.

Once keywords were controlled and categorised for variations (such as Web and World Wide Web), 341 separate keywords were identified across all conferences. Of these 256 (75% of total keywords) appear only once. The unique keywords demonstrate a very heterogeneous group, which has a tendency to be combined with central themes and introduces variations of context, methodology or specific concepts. At the top end of the distribution the terms appearing do coincide, unsurprisingly, with the central themes of the conferences series. In order of most to least frequent the terms are: _information seeking_ (18% of total keywords), _information behaviour_ (10%), _information use_ (7%), _information needs_ (6%), _information seeking behaviour_ (5%). Of the total papers presented within the conference series 128 (60%) of papers contain at least one of these theme keywords either in the title or in the author-provided keywords. There is no indication that author-provided keywords from the 2004 and 2006 use these broad categories more frequently than earlier titles with the exception of the term information behaviour whose usage increases from 1 in the 1996 conference, to 5 in the 2002 conference to 10 and 12 in the 2004 and 2006 conferences respectively.

Further examination of these five theme keywords revealed that 24 (11%) papers combined more than one keyword indicating themes. The term _information seeking_ was combined an equal number of times with _information behaviour_ and _information use_. To examine the extent of combination with terms not indicating theme a further analysis was completed assigning all non-theme terms to the categories context, concept and method. Figure 1, below, provides a visualisation of the subjects across the conferences. For ease of viewing, the figure only shows the subjects that appear more than three times within the keywords. The most frequently used combination with a theme was the introduction of a context (93 instances), for example information seeking of security analysts. Introduction of concepts in combination with a theme was less frequent at 61 instances and methods were least frequently introduced at 31 instances. This analysis indicates that the most frequently occurring pattern for publication in the conference series is papers that address a theme of ISIC and then introduce a context in which the theme is examined. This is unsurprising in some respects but does indicate that selection criteria and presentation of papers is closely aligned with the central aims of the conference series.

<div align="center">![Figure 1: Mapping of conference paper keywords](p381fig1.jpg)</div>

<div align="center">  
**Figure 1: Subjects appearing more than three times within the keywords across all ISIC conferences**</div>

### Mapping conference authors

In addition to exploring the paper titles, we also generated a list of all authors appearing in the ISIC programmes since 1996\. Author names were standardised to control for name variations (for example Carol C. Kuhlthau and Carol Kuhlthau, were included as the same author).

Between 1996 and 2006 (six ISIC conferences) there have been 232 presenters and co-authors. Of these 180 (78%) have contributed one paper, so the heavy majority of presenters are not repeat presenters though this figure does not address attendances (i.e., where the presenter has attended but not presented). Correspondingly, there is a small number of members who have presented at more than one conference. Only four authors have presented at five conferences, with six presenters presenting three and four times. The 2006 conference had the clear majority of first time presenters, (51 presenters) with the next highest being 33 single time presenters in 1998 and 2000, who of course have not presented at an ISIC conference since this time.

Figure 2 is a visualisation of the patterns of contribution across the ISIC conference series for contributors of more than one paper. In the centre of the visualisation are authors who have contributed presentations to five of the six ISIC conferences. As can be seen from the visualisation there are a number of authors who do not contribute in a continuous or sequential pattern but rather will have gap years in which they do not present, though only one author (author 53) has presented at only the 1996 and the 2006 conferences and no others.

<div align="center">![Figure 2: Mapping conference presenters with conference events](p381fig2.jpg)</div>

<div align="center">  
**Figure 2: Author participation patterns across the conferences.**</div>

## A slice of ISIC 2006

Our attention now turns to ISIC 2006, where our project was able, through its diverse data collection techniques, to collect impressions of ISIC from some of the conference participants. In many ways the field of information seeking research is defined by the ISIC conference series. This paper discusses how Conference participants were defining that field and their reasons for attending the Conference. Responses to questionnaires distributed before and during the conference suggested that ISIC is viewed by many as their core conference for hearing about the latest research. Unsurprisingly, they used many different terms to describe and label the conference, or more specifically, the content of papers being presented. To explore this aspect of our survey responses, we tapped into a small part of the rich field materials collected before and during the three day conference. Exploring these responses helps us better understand what _information seeking in context_ meant for these participants in ISIC 2006.

With the permission of the ISIC Programme Committee, all conference registrants received an e-mail message shortly before the conference, introducing the project and inviting them to complete a pre-conference survey. We received a total of 53 responses to invitations sent to the 90 participants registered at the time with 39 respondents (73%) reporting they had never attended an ISIC conference before the Sydney event. It is interesting that 12 of the 14 respondents who **had** been to an ISIC conference prior to the one in Sydney had attended the 2004 conference held in Dublin. (By the first day of the conference a total of 111 people had registered).

The section of the survey we focus on in this paper had a slightly lower response rate, with 49 usable responses. Of these responses, we found that 85% (N=49) reported that the conference themes were specifically related to their own work. These respondents described themselves as researchers, practitioners and/or educators, reflective of the conference's appeal to research and practice communities.

One of the questions asked: _Are you interested in following research in particular area/s at ISIC? If yes, please specify which areas._ Although a large proportion of these survey respondents were attending their first ISIC conference, we found it interesting that 73 % (N=49) of **all** respondents replied YES to this question. Thirty-five of those respondents went on to specify areas of interest to them. We decided to focus our initial analysis on responses to this question and the topics supplied by these respondents.

The 35 respondents to this part of the survey supplied topics of interest very similar to the core topics identified in our mapping of the ISIC landscape: _information behaviour_ (31%), _information seeking_ (23%), _information use_ (14%) and _information seeking behaviour_ (11%) were the most common terms. In their responses, there was a tendency to offer very specific interests within these broad categories that fell within groupings similar to the _concept_ and _context_ clusters identified in the papers. These participants were particularly interested in organizational, health and schools or education settings. For example, one respondent stated topic interests as: '_barriers/inducements to information seeking_' and another wrote '_information seeking in the school library environment_'.

To extend our analysis of the pre-conference survey responses, we examined the 25 written responses to the conference survey's two opening questions (_What brings you to ISIC? What does ISIC mean to you?_). Like the pre-conference survey, we found the majority of respondents described the ISIC conference as their primary conference for information behaviour and related fields. This was the case for students, early-career and established scholars and information professionals.

## Discussion

Mindful that not everyone attending the ISIC conferences presents a paper and that not all information exchanges are connected to formal presentation of a conference paper, charting the ISIC landscape does provide a sense of what participants are hearing at the conference and what, through the formal programme at least, is on offer. It also provides a useful reference point for exploring the responses collected during ISIC 2006 about attendees' topics of interest.

### What information is on offer at an ISIC conference?

Our mapping of the ISIC landscape offers some indicators that ISIC does indeed have core business, as represented in the five themes listed earlier. Analysis of this aspect of the mapping is in the early stages but is helping to shed light on the themes on offer at ISIC conferences. It is worth noting the high incidents of papers falling within the context cluster. Potential relationships between these clusters and the five core themes need to be examined further to identify thematic trends and mergers.

While it is evident from comments collected at ISIC 2006 that many participants have attended two or more ISIC conferences, the large number of one-time presenters suggests that the core business of the ISIC community indicated by mapping the conference papers is delivered by a fluid mix of authors. These figures may speak to a very small and dedicated core of contributors and may represent some interesting patterns of engagement with the community or section of presentations. Further research is planned to identify the relationship between attendance and contribution as well as the role presenters feel the ISIC conferences take in their careers. The youth of the conference series may be a contributing factor in the low instance of repeat presenters.

### What are participants seeking at ISIC conferences?

Thematically speaking, the interests of our slice of ISIC 2006 participants fall well within the core themes and clusters characterising the ISIC landscape since 1996\. A matrix of a comparison between their topics and keywords from the analysis of conference papers confirmed a good alliance between the broad areas of interest cited by ISIC 2006 participants and the topics presented at the conference. In both surveys, participants described ISIC as their primary conference for keeping informed about research in their field. There is no indication that they were coming for very specific information but rather, as Phil described it, to keep up with what is happening in the field. An interesting artifact of the self-selection of our pre-conference survey respondents was that the majority of them were first-timers. As such, we gained some insight into reasons someone might decide to attend an ISIC conference for the first time. Interestingly, responses to the conference survey were collected from first-time and regular ISIC participants and garnered similar responses in terms of thematic interests of participants.

Comments in both surveys suggest that locating the 2006 event in the Asia-Pacific attracted attendees and presenters who have followed the ISIC literature for some time but been unable to attend earlier conferences (most often, the reason provided being the cost and duration of travel to Europe). Preliminary scans of authors at previous conferences and anecdotal information about the participants at them suggest that each ISIC conference attracts people from the host institution's region. It may also help explain the high author turnover uncovered in the mapping exercise. In this way, ISIC takes on a local flavour that also contributes to its character.

There were indications of professional as well as geographic diversity within the ISIC community. Responses in both surveys were provided by researchers and practitioners ranging from students and early-career through to senior researchers and seasoned professionals. Mary, a research student in the early stages of a PhD described ISIC as a place to '_.meet people, talk, sounding board for ideas_'. Her comments were not all that different from Sarah, a mid-career academic who described ISIC as:

> .the heart of our domain. ISIC is a unique venue for people who study people and information - nurturing and creative environment shared history, don't have to explain; go back and question traditional assumptions; cutting edge.

When asked about what brings them to ISIC, most respondents mentioned the value of meeting with people with similar research interests. It is not just about presenters at a conference, however, but about meeting and networking with other participants, as Ellen (a senior information behaviour researcher) observed:

> ISIC is a primary venue for me. The focus means that most papers are relevant to me. It allows me to discuss work of selected researchers I've followed for years. It allows me to get feedback on my own research and it helps me work with my own (and others') doctoral students.

Phil commented that dialogue at the conference '.affirms the value of the community'. Repeatedly in both surveys, respondents used terms like '_community_', '_collegial_', '_network_', '_meeting place_' and even '_family_' to describe the ISIC conference. When asked what ISIC means to her, Viv responded:

> Although ISIC has meant nothing to me before this, I now use it as a place where I will have something in common with all the researchers.

People are attracted to the ISIC conferences because of people. Three of the pre-conference survey respondents, for instance, listed authors whose work they follow rather than topics when answering our question about areas of interest at ISIC 2006\.

## Conclusion and lessons learned

Conferences and the information they can make available to us serve essential functions in scholarly and research practice, contributing to the monitoring of developments in the field and nurturing the personal networks that support and sustain an individual's research and personal development. The mapping of both the ISIC landscape and our survey respondents' comments about their interests helps us to understand what the field means in the ISIC conference series. By exploring what attracted our respondents to ISIC 2006 and what ISIC means to them, we also begin to get a picture of the many facets of ISIC and the role it plays in the lives of researchers, practitioners and educators who attend. ISIC is described as a conference, a community, a theme, a research focus. Our study of the ISIC conference and responses of participants in the Sydney event illustrate the role this particular conference series can and does play in the research and information practices of researchers and practitioners who identify with the field of information behaviour in its many manifestations. In this way, the project contributes to a fuller understanding not only of the interlacing of research and information practices but ISIC's contribution to the shaping of information behaviour research.

The combination of data collection tools used for our ISIC 2006 field work has provided a very rich collection of material to tap into within our project. The data gathering process was, however, labour intensive and owes a great deal to the enthusiastic team of research assistants who worked at the event. As the results reported here illustrate, the approach is serving us well. Capturing the context of a fast-paced three day conference is an intense field work experience. Resources (time, effort, assistants) permitting, attempting such a suite of data gathering activities helps provide rich context for research questions that evolve through the analysis of a fast-paced special event like a research conference. In this way, we are able to collect slices of activity within a conference and across a conference series.

This paper introduces some of the patterns and dynamics of topics and contributing authors that have emerged from the slices collected over ten years of ISIC conferences and analysed in our project thus far. In this way, we begin to understand the rhythms of research activity associated with this conference series. We will continue to work with the material collected at ISIC 2006 to better understand some of the rhythms of activity that take place within a single conference event. Taking advantage of the diverse sources of information about conference activity to extend the analysis introduced in this paper, longitudinal work will also continue to explore the research practices of some ISIC participants and examine in greater detail how information at ISIC 2006 is used by some of our project participant (authors and attendees). In future work we also intend to examine more fully the comments made in both surveys indicating that many people attend ISIC conferences because of a desire to connect to researchers, not necessarily specific research (areas). Finally, we are also continuing with the work started at ISIC 2006: collecting personal recollections of ISIC conferences from people who have been active within the community since the 1996 event in Tampere, Finland. In this way we intend to extend our initial exploration of the formal program to begin unpacking, at the formal as well as informal levels, ways information is generated through an ISIC conference.

## Acknowledgements

The authors wish to thank the student volunteers who were so essential to the collection of data at the ISIC2006 conference in Sydney. Their talents, dedication and enthusiasm are very much appreciated.

## References

*   <a id="ATD2006" name="ATD2006"></a>Anderson, T. D. (2006). Studying human judgments of relevance: interaction in context. In Ruthven, I., Borlund, P., Ingwersen, P., Belkin, N.J., Tombros, A. & Vakkari, P. (eds.) Information Interaction in Context. _Proceedings of the first international IIiX Symposium_, (pp. 7-23). (ACM International Conference Proceedings Series). New York, NY: ACM Press,
*   <a id="BCA1997a" name="BCA1997a"></a>Barry, C.A. (1997a). Information skills for an electronic world: training doctoral research students. _Journal of Information Science_, **23**(3), 225-238\.
*   <a id="BCA1997b" name="BCA1997b"></a>Barry, C.A. (1997b). The research activity timeline: a qualitative tool for information research. _Library and Information Science Research_, **19**,(2), 153-179\.
*   <a id="BMJ2004" name="BMJ2004"></a>Bates, M.J. (2004). [Information science at the University of California at Berkeley in the 1960s: a memoir of student days.](http://www.webcitation.org/5d2qzcITs) _Library Trends_, **52**,(4), 683-701\. Retrieved 13 December, 2008 from http://www.gseis.ucla.edu/faculty/bates/articles/Berkeley.html (Archived by WebCite® at http://www.webcitation.org/5d2qzcITs)
*   <a id="BLA2002" name="BLA2002"></a>Black, A. & Crann, M. (2002). In the public eye: a mass observation of the public library. _Journal of Librarianship and Information Science_, **34**(3), 145-157\.
*   <a id="BOR2001" name="BOR2001"></a>Borgman, C.L. & Furner, J. (2001). Scholarly communication and bibliometrics. _Annual Review of Information Science and Technology_, **36**, 3-72\.
*   <a id="CLA2000" name="CLA2000"></a>Clandinin, D.J. & Connelly, F.M. (2000). _Narrative inquiry: experience and story in qualitative research_. San Francisco, CA: Jossey-Bass
*   <a id="CRO2002" name="CRO2002"></a>Cronin, B. & Shaw, D. (2002). Identity-creators and image-makers: using citation analysis and thick description to put authors in their place. _Scientometrics_, **54**,(1), 31-49\.
*   <a id="DNK1989" name="DNK1989"></a>Denzin, N.K. (1989). _The research act_. Englewood Cliff, NJ: Prentice-Hall
*   <a id="DMC1995" name="DMC1995"></a>Drott, M.C. (1995). Reexamining the role of conference papers in scholarly communication. _Journal of the American Society for Information Science_, **46**(4), 299-305\.
*   <a id="ELD1993" name="ELD1993"></a>Ellis, D., Cox, D. & Hall, K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences. _Journal of Documentation_, **49**(4), 356-69\.
*   <a id="EDA1993" name="EDA1993"></a>Erlandson, D.A., Harris, E.L., Skipper, B.L. & Allen, S.D. (1993). _Doing naturalistic inquiry. A guide to methods_. Newbury Park, CA: Sage
*   <a id="FIG1996" name="FIG1996"></a>Fine, G. & Deegan, J. (1996). [Three principles of serendip: insight, chance and discovery in qualitative research.](http://www.webcitation.org/5d2wcAu39) _Qualitative Studies In Education_, **9**(4). Retrieved 10 December 2008 from http://www.ul.ie/~philos/vol2/deegan.html (Archived by WebCite® at http://www.webcitation.org/5d2wcAu39)
*   <a id="FOR1999" name="FOR1999"></a>Ford, N. (1999). Information retrieval and creativity: towards support for the original thinker. _Journal of Documentation_, **55**(5), 528-42\.
*   <a id="FRY2004" name="FRY2004"></a>Fry, J. & Talja, S. (2004). The cultural shaping of scholarly communication: explaining e-journal use within and across academic fields. _Proceedings of the American Society for Information Science and Technology_, **41**(1), 20-30.
*   <a id="GIV2003" name="GIV2003"></a>Given, L.M. (2003). "Sweeping" the library: mapping the social activity space of the public library. _Library and Information Science Research_, **25**(4), 365-85\.
*   <a id="GLA2001" name="GLA2001"></a>Gläser, J. & Laudel, G. (2001). Integrating scientometric indicators into sociological studies: methodical and methodological problems. _Scientometrics_, **52**(3), 411-34\.
*   <a id="GON2004" name="GON2004"></a>Gonzalez, V.M. & Mark, G. (2004). ["Constant, constant, multi-tasking craziness": managing multiple working spheres.](http://www.webcitation.org/5d2xHdoc4) In _Proceedings of the SIGCHI conference on Human factors in computing systems, Vienna, Austria_. (pp. 113-120). New York, NY: ACM Press. Retrieved 13 December, 2008 from http://www.ics.uci.edu/~gmark/CHI2004.pdf (Archived by WebCite® at http://www.webcitation.org/5d2xHdoc4)
*   <a id="HAR2000" name="HAR2000"></a>Harper, R.H.R. (2000). The organisation of ethnography. _Computer Supported Cooperative Work (CSCW)_, **9**(2), 239-264.
*   <a id="HER2001" name="HER2001"></a>Herman, E. (2001). End-users in academia: meeting the information needs of university researchers in an electronic age: Part 1\. _Aslib proceedings_, **53**(9), 387-401.
*   <a id="HIR2001" name="HIR2001"></a>Hirsch, E. & Gellner, D.N. (2001). Introduction: ethnography of organizations and organizations of ethnography. In D.N. Gellner & E. Hirsch (eds.). _Inside organizations: anthropologists at work_, (pp.1-15) Oxford and New York, NY: Berg.
*   <a id="KIN2001" name="KIN2001"></a>Kincheloe, J.L. (2001). Describing the bricolage: conceptualising a new rigor in qualitative research. _Qualitative Inquiry_, **7**(6), 679-92.
*   <a id="KIN2005" name="KIN2005"></a>Kincheloe, J.L. (2005). On to the next level: continuing the conceptualization of the bricolage._Qualitative Inquiry_, **11**(3), 323-50.
*   <a id="KON2008" name="KON2008"></a>Konecki, K.T. (2008). [Grounded theory and serendipity: natural history of a research.](http://www.webcitation.org/5d2y1TDkn) _Qualitative Sociology Review_, **4**(1), 171-188\. Retrieved 13 December, 2008 from http://bit.ly/Z0L4 (Archived by WebCite® at http://www.webcitation.org/5d2y1TDkn)
*   <a id="LIN2001" name="LIN2001"></a>Lincoln, Y.S. (2001). An emerging new bricoleur: promises and possibilities - a reaction to Joe Kincheloe's "Describing the bricolage". _Qualitative Inquiry_, **7**(6), 693-96\.
*   <a id="MCI2003" name="MCI2003"></a>McIntyre, A. (2003). Through the eyes of women: photovoice and participatory research as tools for recognising place. _Gender, Place and Culture_, **10**(1), 47-65\.
*   <a id="MED1990" name="MED1990"></a>Meadows, J. (1990). General overview: keynote address. In M. Feeney and K. Merry (Eds.), _Information technology and the research process: proceedings of a conference held at Cranfield Institute of Technology, UK, 18-21 July 1989_ (pp. 1-13.). London: Bowker-Saur.
*   <a id="MOS2006" name="MOS2006"></a>Moss, P. (2006). Emergent methods in feminist research. In S. Hesse-Biber (Ed.), _Handbook of feminist research._ (Ch. 13). Thousand Oaks, CA: Sage.
*   <a id="NOA1997" name="NOA1997"></a>Noam, E. (1997). [_Electronics and the future of the research library. An essay prepared for the 1997 ACRL National Conference Choosing Our Futures._](http://www.webcitation.org/5d2yNCbgx) Chicago, IL: Association of College and Research Libraries. Retrieved at 10 December 2008 from at: http://archive.ala.org/acrl/invited/noam.html (Archived by WebCite® at http://www.webcitation.org/5d2yNCbgx)
*   <a id="OCH1996" name="OCH1996"></a>Ocholla, D.N. (1996). Information-seeking behaviour by academics: a preliminary study. _International Information and Library Review_, **28**(4), 345-58\.
*   <a id="OLA2007" name="OLA2007"></a>Olander, B. (2007). [Information interaction among computer scientists. A longitudinal study.](http://InformationR.net/ir/12-4/colis/colis14.html) _Information Research_, **12**(4) paper colis14\. Retrieved 10 December 2008 from http://InformationR.net/ir/12-4/colis/colis14.html (Archived by WebCite® at http://www.webcitation.org/5d2yS4UEE)
*   <a id="PAI1968" name="PAI1968"></a>Paisley, W.J. & Parker, E.B. (1968). The AAPOR Conference as a communication medium. _Public Opinion Quarterly_, **32**(1), 65-73.
*   <a id="RIC2000" name="RIC2000"></a>Richardson, L. (2000). Writing: a method of inquiry. In N.K. Denzin & Y.S. Lincoln (eds.), _Handbook of qualitative research_, 2nd ed., (pp. 923-48.) Thousand Oaks, CA: Sage.
*   <a id="SAN2001" name="SAN2001"></a>Sandstrom, P.E. (2001). Scholarly communication as a socioecological system. _Scientometrics_, **51**(3), 573-605\.
*   <a id="SEL2001" name="SEL2001"></a>Selden, L. (2001). Academic information seeking—careers and capital types. _New Review of Information Behaviour Research_, **1**(2), 195-215\.
*   <a id="TAL2002" name="TAL2002"></a>Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. _New Review of Information Behaviour Research_, **3**, 143-59\.
*   <a id="WAN1997" name="WAN1997"></a>Wang, C.C. & Burris, M.A. (1997). Photovoice: concept, methodology and use for participatory needs assessment. _Health Education & Behavior_, **24**(3), 369-87\.
*   <a id="WHI1989" name="WHI1989"></a>Whittaker, J., Courtial, J.P. & Law, J., (1989). Creativity and conformity in science: titles, keywords and co-word analysis. _Social Studies of Science_, **19**(3), 473-496.



## Appendix A: Pre-Conference Survey

### About this questionnaire

Welcome, 

This survey questionnaire has been devised as part of a research project exploring the information and communication exchanges that take place at conferences like ISIC. Responses to this questionnaire will be used to build up a composite picture of the ISIC community and the general information behaviour of conference participants. The responses you provide will help us to gain a better understanding of how ISIC fits into the overall research work of conference participants. The final question invites you to participate in further study at the conference itself.

As a token of appreciation for making the time to participate in this research project, everyone completing this questionnaire will be receive an entry in a prize raffle, with prizes to be drawn at the forthcoming ISIC conference in Sydney. If you provide your contact details at the end of this survey, you will be entered automatically into this draw.

Please read the Information Sheet and Consent Form before completing the survey. These documents were also attached to the e-mail inviting you to participate. NOTE that sending a response acknowledges that you have read the sheet and that you are giving consent as documented in the consent form.

Please answer these questions in relation to your professional and research interests. The survey should take between 15 and 20 minutes to complete and must be completed in one session. A number of questions are mandatory. These questions are marked with an *.

Thank you for completing the survey. Your comments will be kept confidential.



### Questions about your professional and research interests

<table width="900" border="0" cellspacing="0" cellpadding="0">

<tbody>

<tr>

<td>1\. Have you attended a previous ISIC conference? *  
</td>

</tr>

<tr>

<td>

<form method="post" action=""><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</form>

</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>2\. If yes, please select as many of the following options as apply.  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>

<form method="post" action=""><input type="checkbox" name="Tampere 1996" value=" "> Tampere 1996  
<input type="checkbox" name="Sheffield 1998" value=" "> Sheffield 1998  
<input type="checkbox" name="Goteborg 2000" value=" "> Goteborg 2000  
<input type="checkbox" name="Lisbon 2002" value=" "> Lisbon 2002  
<input type="checkbox" name="Dublin 2004" value=" "> Dublin 2004  
</form>

</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>3\. How many other (non-ISIC) conferences have you attended since September 2004? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>

<form method="post" action=""><input type="text" size="20" name="Many"></form>

</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>4\. What were the names of the conferences you attended?</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>

<form method="post" action=""><input type="text" size="20" name="Many"></form>

</td>

</tr>

<tr>

<td>5\. I would describe myself as: (Please select as many options as apply)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>

<form method="post" action=""><input type="checkbox" name="Researcher " value=" "> Researcher  
<input type="checkbox" name="Practitioner" value=" "> Practitioner  
<input type="checkbox" name="Lecturer/Teacher" value=" "> Lecturer/Teacher  
<input type="checkbox" name="Student" value=" "> Student  
Other: <input type="text" size="20" name="Many">  
</form>

</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>6\. What is your main reason for attending ISIC2006? (Please select only ONE option)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name="" value=" "> Conference themes are specifically related to my own work  
<input type="checkbox" name="" value=" "> Conference themes are marginally related to my own work  
<input type="checkbox" name="" value=" "> Conference themes are related to my general interests  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>7\. If none of the options in the previous question apply, please provide a brief explanation.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td>8\. How important do you consider your attendance at an ISIC conference(s) for you research and/or professional interests? * (Please select only ONE option)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name="" value=" "> Not at all important  
<input type="checkbox" name="" value=" "> Not very important  
<input type="checkbox" name="" value=" "> Fairly important  
<input type="checkbox" name="" value=" ">Very important  
<input type="checkbox" name="" value=" "> Essential  
</td>

</tr>

</tbody>

</table>

### Questions about the ISIC Program

<table width="900" border="0" cellspacing="0" cellpadding="0">

<tbody>

<tr>

<td>1\. What sessions are you particularly interested in attending? * (Please select as many options as apply)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name="" value=" "> Keynote: Brenda Dervin  
<input type="checkbox" name="" value=" "> Keynote: Chun Wei Choo  
<input type="checkbox" name="" value=" "> Rural Communities  
<input type="checkbox" name="" value=" ">Uncertainty  
<input type="checkbox" name="" value=" "> Essential  
<input type="checkbox" name="" value=" "> Information behaviour in school settings  
<input type="checkbox" name="" value=" "> Information sharing in industrial settings  
<input type="checkbox" name="" value=" "> Health information seeking  
<input type="checkbox" name="" value=" "> Information use environments  
<input type="checkbox" name="" value=" "> Conceptualising information behaviour  
<input type="checkbox" name="" value=" "> Study, work and play  
<input type="checkbox" name="" value=" "> Information in educational environments  
<input type="checkbox" name="" value=" ">Locating and organising in electronic environments  
<input type="checkbox" name="" value=" "> Panel: Whither information seeking behaviour research  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>2\. Are you interested in following the work of a particular researcher/s at ISIC? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>3\. If yes, please specify which researchers.</td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>4\. Are you interested in following research in particular area/s at ISIC? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>5\. If yes, please specify which areas.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

</tbody>

</table>

### Questions about information you receive in relation to your professional and research interests

<table width="900" border="0" cellspacing="0" cellpadding="0">

<tbody>

<tr>

<td>1\. What journals (print or electronic) do you regularly scan or read? * Please list titles.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>2\. What Websites or e-communities (e.g., blogs, listservs, discussions) do you visit? * Please list names of sites/communities.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>3\. Do you actively participate in any of these? * If so, which ones?</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>4\. If yes, which groups do you actively participate in?</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>5\. Is there anything else you actively monitor in relation to your research/practice? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>6\. If yes, please specify.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>7\. Are you a reviewer for any publications? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>8\. If yes, please list publications.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>9\. Are you a reviewer for any conferences? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>10\. If yes, please list conferences.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>11\. Do you provide feedback (informally review) for colleagues preparing papers or reports? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>12\. If yes, in what topic areas?</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>13\. How many colleagues do you provide with such feedback?</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>14\. Which of the following options describes these colleagues? (Please select as many options as apply)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name="Researcher " value=" "> Junior colleague  
<input type="checkbox" name="Practitioner" value=" "> Peer  
<input type="checkbox" name="Lecturer/Teacher" value=" "> Senior colleague  
<input type="checkbox" name="Student" value=" "> Current PhD supervisor  
<input type="checkbox" name="Student" value=" "> Former PhD supervisor  
<input type="checkbox" name="Student" value=" "> Mentor  
Other: <input type="text" size="20" name="Many">  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>15\. Is there anything else that you get sent to read? (for example, by particular organisations or people, or in a particular context)*</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>16\. If yes, please specify.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>17\. Are there any other strategies you use to become informed? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>18\. If yes, please specify.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

</tbody>

</table>

### Questions about the information you produce

<table width="900" border="0" cellspacing="0" cellpadding="0">

<tbody>

<tr>

<td>1\. Are you preparing or have you in the past six months worked on any of the following? (Please select as many options as apply)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name=" " value=" "> Thesis  
<input type="checkbox" name="" value=" "> Report  
<input type="checkbox" name="" value=" "> Journal article (online or print)  
<input type="checkbox" name="" value=" "> Conference paper  
<input type="checkbox" name="" value=" "> Conference short paper/poster  
<input type="checkbox" name="" value=" "> Book  
<input type="checkbox" name="" value=" "> Book chapter  
<input type="checkbox" name="" value=" "> Blog  
Other: <input type="text" size="20" name="Many">  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>2\. Do you request feedback (informal review) from colleagues regarding papers or reports you are preparing? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>3\. If yes, in what topic areas?</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>4\. Which of the following options describe these colleagues? (Please select as many options as apply)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name="Researcher " value=" "> Junior colleague  
<input type="checkbox" name="Practitioner" value=" "> Peer  
<input type="checkbox" name="Lecturer/Teacher" value=" "> Senior colleague  
<input type="checkbox" name="Student" value=" "> Current PhD supervisor  
<input type="checkbox" name="Student" value=" "> Former PhD supervisor  
<input type="checkbox" name="Student" value=" "> Mentor  
Other: <input type="text" size="20" name="Many">  
</td>

</tr>

</tbody>

</table>

### Questions about you

Your responses to the following questions will help us build a picture of the general characteristics of ISIC2006 conference participants.

<table width="900" border="0" cellspacing="0" cellpadding="0">

<tbody>

<tr>

<td>1\. What is your main occupation at present? * (Please select only ONE item)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name="Researcher " value=" "> Information practitioner  
<input type="checkbox" name="Practitioner" value=" ">Practitioner in some other field  
<input type="checkbox" name="Lecturer/Teacher" value=" "> Undergraduate student  
<input type="checkbox" name="Student" value=" "> Postgraduate student  
<input type="checkbox" name="Student" value=" "> PhD student  
<input type="checkbox" name="Student" value=" "> Teacher/professor in the information studies/systems field  
<input type="checkbox" name="Student" value=" "> Teacher/professor in a different field  
<input type="checkbox" name="Student" value=" "> Research at a commercial institution  
Other: <input type="text" size="20" name="Many">  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>2\. Where are you geographically located? * (Please select only ONE item)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name="Researcher " value=" "> Africa  
<input type="checkbox" name="Practitioner" value=" "> Asia  
<input type="checkbox" name="Lecturer/Teacher" value=" "> Australia or New Zealand  
<input type="checkbox" name="Student" value=" "> Canada  
<input type="checkbox" name="Student" value=" "> Europe  
<input type="checkbox" name="Student" value=" "> Oceania  
<input type="checkbox" name="Student" value=" "> South America  
<input type="checkbox" name="Student" value=" "> United Kingdom  
<input type="checkbox" name="Student" value=" "> United States  
Other: <input type="text" size="20" name="Many">  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>3\. What is the highest level of study you have achieved to date? * (Please select only ONE item)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name="Researcher " value=" "> Undergraduate degree (BA)  
<input type="checkbox" name="Practitioner" value=" "> Undergraduate degree (BSc)  
<input type="checkbox" name="Lecturer/Teacher" value=" "> Master of Arts (Coursework)  
<input type="checkbox" name="Student" value=" "> Master of Arts (Research)  
<input type="checkbox" name="Student" value=" "> Master of Science (Coursework)  
<input type="checkbox" name="Student" value=" "> Master of Science (Research)  
<input type="checkbox" name="Student" value=" "> PhD  
Other: <input type="text" size="20" name="Many">  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>4\. How do you describe your field(s) of research and/or specific area/s of interest? * If more than one, please list and number each area.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>5\. How long have you been involved in each of these field(s)? * Please specify. (You can refer to the numbers in previous question to save time).</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>6\. Has your field of interest changed over your career? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Mr/Ms" value="Yes"> Yes  
<input type="radio" name="Mr/Ms" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>7\. If yes, please describe how.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>8\. Would you be interested in participating in the next stage of this research project at the ISIC conference in July 2006? *</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="radio" name="Yes" value="Yes"> Yes  
<input type="radio" name="Yes" value="No"> No  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>9\. Please indicate if you are interested in any of the following options. (Please select as many options as apply)</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="checkbox" name=" " value=" "> Discussion on a conference blog about past, present and future ISIC activities  
<input type="checkbox" name="" value=" "> Follow-up e-mail correspondence prior to the ISIC2006 conference  
<input type="checkbox" name="" value=" ">Follow-up telephone interview prior to the ISIC2006 conference  
<input type="checkbox" name="" value=" "> In-depth face to face interview during the ISIC2006 conference  
<input type="checkbox" name="" value=" "> Follow-up survey questionnaire during the ISIC2006 conference  
<input type="checkbox" name="" value=" "> Ethnographic observation of your participation at ISIC (one day or part of one day)  
<input type="checkbox" name="" value=" "> Taking photographs during ISIC and discussing with a researcher (photovoice)  
<input type="checkbox" name="" value=" "> Keeping a blog leading up to and during the ISIC2006 conference  
<input type="checkbox" name="" value=" "> Keeping a diary of your conference activities (one day or part of one day) and discussing with a researcher  
</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td>10\. Please provide your contact details (name, e-mail and/or telephone) if you have indicated you wish to participate in the next stage of the research project AND/OR if you wish to go into the draw for the raffle.</td>

</tr>

<tr>

<td> </td>

</tr>

<tr>

<td><input type="text" size="20" name="Many"></td>

</tr>

</tbody>

</table>

## <a id="AppB" name="AppB"></a>Appendix B: Conference Survey Questions under discussion in this Paper<sup>[2](#Anote2)</sup>

What brings you to ISIC? What does ISIC mean to you?

What are you hearing that grabs your attention?

What is missing? What would you like to see/have seen?

What makes ISIC _ISIC_ for you?

**The following statements refer to things you have heard or discussed at any time during the conference:**

The most interesting information

The most useful information in terms of your current activities or interests

The most affirming information, in terms of your own work or ideas

The most unexpected or surprising information

The following question asks you to identify if you have made connections to any of the following types of information. For each connection you have made, please identify where that connection was made (in terms of grid options below), ranking the impact of each on a 10 point scale (10= Significant impact; 0=no impact)<sup>[3](#Anote3)</sup>

<table width="90%" cellspacing="" cellpadding="1" border="2" frame="box">

<tbody>

<tr>

<td width="100%" colspan="8" align="center">**Types of information**</td>

</tr>

<tr>

<td></td>

<td>information on someone else's work that is similar to my own</td>

<td>information on someone else's work that is very similiar to my own</td>

<td>information on ideas for new research projects</td>

<td>information on specific methods or procedures</td>

<td>information on findings</td>

<td>information on research applications</td>

<td>Comments on my own work</td>

</tr>

<tr>

<td>From presentation of a paper directly</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>From discussion after presentation</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Spoke to presenter just after their presentation</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Spoke to another delegate about the material</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>From a poster session</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>From abstract in the conference program</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Spoke to participant informally elsewhere @ conference</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

</tbody>

</table>

Do you wish to share any details about the most meaningful connection you have made to a person or idea at this conference?

Thank you for completing and returning the questionnaire - your comments will be kept in confidence.

## Notes to the Appendices

1.  <a id="Anote1" name="Anote1"></a>The instructions and questions presented here were part of a pre-conference survey distributed electronically to all registered ISIC 2006 participants in early July 2006.
2.  <a id="Anote2" name="Anote2"></a>These questions were the basis for clipboard surveys, interview grabs and a 2-page conference survey participants were invited to complete throughout the three-day event. This paper draws from the responses to the printed survey. Further analysis will examine remaining aspects of this phase of the fieldwork.
3.  <a id="Anote3" name="Anote3"></a>The graph is a variation of the approach taken by Paisley & Parker ([1968](#PAI1968)) survey.



