#### vol. 13 no. 4, December, 2008

# Mixed realities: information spaces then and now

#### [Bonnie Nardi](mailto:...)

Department of Informatics, University of California, Irvine, Donald Bren Hall 5042, Irvine, CA 92697-3440, USA

#### Abstract

> **Introduction.** The paper is based on a transcript of a presentation given to ISIC2008\. It concentrates on the findings from research into the gaming communities engaged in World of Warcraft.  
> **Methods.** The research was carried out using the anthropological approach by playing the game, interviewing the players and analysing the game documentation and Internet resources.  
> **Findings.** The mixed realities formed by the gaming communities from the physical spaces and virtual realities are similar to earlier mixed realities and can be comparable to libraries, dormitories, homes, etc. Information becomes a tool for community building and players are creating different tools not only for gathering and accessing information but also for sharing and communal usage.  
> **Conclusions.** Video games as information spaces are moving to mixed reality spaces that fuse the virtual and physical. They accustom us to the constant monitoring that creates information and the culture that endorses freely sharing it.

## Introduction

I'd like to talk this morning about some of my research on information spaces as mixed realities, discussing trends in the evolution of information spaces as I have observed them in my research on online multiplayer video games.

I have been studying a game called [World of Warcraft](http://www.worldofwarcraft.com/index.xml) (Figure 1). I am not a typical video game player, I became interested in these games because I have heard the undergraduate students in the Department of Informatics at the University of California, Irvine, talking of them frequently and with great enthusiasm. Since I study social life on the Internet (I study blogging and instant messaging and video, etc.), I thought that there is a whole area of social interaction I know nothing about so I need to do this investigation. I got an account, started to play and got drawn into this whole world. It turns out that, in the USA, video games generate more revenue even than film. So, this is really an important medium.

<div align="center">![Logo for the World of Warcraft](p354fig1.png)</div>

<div align="center">**Figure 1: Logo for the World of Warcraft**</div>

## World of Warcraft

_World of Warcraft_ is the most popular multiplayer game produced by [Blizzard Entertainment](http://www.blizzard.com/us/). It is a commercial product. There are more than ten million players worldwide. As an anthropologist I find it very interesting that it is so cross-cultural, it seems to transcend our cultural boundaries. The game is now available in English, two versions in Chinese, Korean, French, German, Spanish and, recently, in Russian. It is truly a global phenomenon: half are in China, the rest scattered between America, Europe, then Australia, New Zealand, Taiwan and so on. No one really knows the demographics precisely, but I would judge that the modal player is a male in his twenties, but there is a sizeable minority of women players as well as players of all ages. Nick Yee ([2005](#yee)) suggests that about 25% of the players in North America are female. It is a much smaller proportion in China; we did some counts in Internet cafés and I think it is about 10% female players, but I think you have to be very careful about these numbers. I've played with many people in their thirties and forties, but typically, it tends to be a young person's game. However, it is much more open to older players than some of the other, more violent, video games such as [Grand Theft Auto.](http://www.rockstargames.com/IV/)

_World of Warcraft_ is, of course, an entertainment application, but virtual worlds are becoming increasingly important in corporate setting as well. I have research collaborations with Intel, IBM, and Agilent to study these worlds. They are interested in them because of the possibilities for remote collaboration. These are enormous global corporations and they find it difficult to get all of their employees to collaborate across cultural and international boundaries. The experiments are taking place to see whether working within these virtual worlds takes us beyond e-mail, video-conferencing, beyond our current tools. I think that we are far from that, but it makes an interesting research area. Some of these companies are interested also in next generation user interfaces; what is it like to play inside video-games? My life in _World of Warcraft_ is explored in a forthcoming book ([Nardi forthcoming](#nar))

Most of my research was done on North American servers because I live in the USA, but around five million players of the game live in China. So, I got curious about what it was like to play there, whether there were cultural differences. Therefore, I went to China in the summer of 2007 to study how the game is played there. A couple of students from Peking University and a graduate student from my own university helped me.

<div align="center">![Figure 2: Interface of the World of Warcraft game](p354fig2.png)</div>

<div align="center">**Figure 2: Interface of the _World of Warcraft_ game**</div>

I will try to explain what it is like to play this game. I can remember my own experience of feeling completely lost and having no clue of what it is all about. The basics are that you create an animated character and you move your character around in a three-dimensional space (Figure 2). There are many objects that your characters have, you can acquire, trade and share them with others (Figure 3).

<div align="center">![Figure 3: Objects that players can acquire in the World of Warcraft](p354fig3.png)</div>

<div align="center">**Figure 3: Objects that players can acquire in the _World of Warcraft_**</div>

_World of Warcraft_ is a very complex information space. I was amazed how much I had to learn to be able to play. Here are some details about what the players have to learn: first, you have your character and it is of a particular type. There are nine different types. A character has certain abilities and you need to learn dozens of the abilities that every character has to play the game effectively. As it is a social game, and you are playing with other people, you also need to know a little about the abilities of their characters. In the backpack you carry some of your items: all kinds of potions, weapons, armour and so forth that you acquire to equip your character effectively. Your character is of a particular type, but you can customize that type and you have talents. You have to know how to arrange the talents so that your character plays well. You slay monsters and each monster behaves differently. You have to learn how they behave to be able to defeat them. There is an enormous geography: you go out into the world that contains deserts and jungles, forests, mountains, and rivers. It is very elaborate and very exciting, but you have to know where to go to accomplish your tasks in the game. Every character can have professions, like herbalism, blacksmithing, or mining, following the medieval theme. You have to learn how to practice your professions. It is a competitive game and there are many tactics and strategies that you have to pick up: how to win, how to form good groups, run a guild, play well, etc. As a player inside _World of Warcraft_ you are in a constant state of learning, and you are always in need of new information.

## Methods

For online methodology for studying this game, as a trained anthropologist, I have used typical anthropological methods: immersive participant-observation fieldwork. That means creating characters, joining two North American guilds, spending a lot of time in the game, and doing a lot of interviews. My students and I have conducted over 150 interviews both online and offline, and most of them offline. I am still an old-fashioned anthropologist and I like to sit down with people and have a long conversation. Sometimes there is a moment in the game when it is really perfect to do a short interview. _World of Warcraft_ has in-game functions for collecting, recording the chats that people engage in. Thus, it was very easy to collect thousands of pages of chat logs. Easy to collect and hard to analyse them, but a very good information source about the game. There is an enormous amount of player-created information about the game. People who get completely bored with their jobs get very excited creating spreadsheets and charts, guides and facts, forums and blogs and wikis all about _World of Warcraft_. Some of it looks so professional you could turn it into your boss. I also immersed myself in these information sources.

## Mixed reality spaces

To put the work into a broader context I would like to quote from a psychologist and philosopher Brian Vandenberg who said:

> Human development is not simply the acquisition of information about a given world. It involves becoming grounded in an uncertain world. ([Vandenberg 1998](#van): 298)

So I take this to mean that information is crucial to human life because in an uncertain world, community is necessary and information is necessary for creating and sustaining community.

Communities can have spaces and spaces come in many forms and configurations. There are at least three different types of spaces that we can think about. First, there are physical spaces like this incredible room [_Ed. the lecture theatre in Vilnius University Library_], digital spaces like online video games and what I call mixed reality spaces, which fuse the physical and the digital. I would like to go back in time to my old library: this is the Alta Branch of Cleveland Public Library System (Cleveland, Ohio), which was funded by an American philanthropist John D. Rockefeller in 1914 (Figure 4). I remember trundling off to that library to get a card when I was four years old. Though I could not read, I loved books so much that my cousins agreed to take me there. This has a lot of fond memories for me. This library is, of course, a physical space grounded in specific physical geography of Cleveland, Ohio. That was my first public community information space.

<div align="left">![Figure 4: Alta branch, Cleveland Public Library System, Cleveland, Ohio.](p354fig4.png)</div>

<div align="right">![Figure 5: Night Elf priest](p354fig5.png)</div>

The most recent one is the _World of Warcraft_ space. The picture shows my character: Night Elf priest (Figure 5). There is a picture of my guild: forty people standing around a monster that we have just slain (Figure 6), the people take screenshots of these moments and post them to guild websites. This is a moment of solidarity in our guild showing what it is like to be inside of this completely virtual world.

<div align="center">![Figure 6: After slaying the monster](p354fig6.png)</div>

<div align="center">**Figure 6: After slaying the monster**</div>

When I went to China in 2007 I found Internet cafés. There the people play _World of Warcraft_ and other computer games in China. They bring together the physical and the virtual into classic mixed reality space. Why do people go to Internet cafés in China? There are hundreds thousands of these cafés over China. They are very accessible. Many people lack good computers at home. They may have some computation at home, but it may not be good enough to play a game like _World of Warcraft_, which requires really decent equipment. Cafés have very good equipment and have lots of software loaded on the computers. So, it is a very congenial place to go to get the kind of equipment that many people simply do not have at home.

The other reason is because there are people there. I want to talk about finding information in the context of the Internet café. There are several quotes from the interviews we have done in Beijing.

> Chen said: If there is an empty seat next to a World of Warcraft player, I go over there to sit next to him, even though it is in a really crowded area. We look at each other's equipment and have a conversation about it. Sometimes we exchange seats with other people so that we [World of Warcraft players] can sit closer to each other.

This sounds like a fairly off hand comment about looking at each other's equipment, but in the _World of Warcraft_ one of main goals of the game is to acquire better equipment so that you can make your character more powerful. So, having an opportunity to sit next to someone and look at their equipment and talk to them about it is a really important information finding opportunity.

> Liu said: So I sit next to [more experienced players] and talk to them while they are playing and see how they are playing the game.

So, in the Internet café people have physical proximity to others for finding information about the virtual game. This is a part of becoming '_grounded in an uncertain world_' where you need to acquire a lot of information.

Internet cafés as mixed reality spaces create bonds of community that people find satisfying. Shaowzeng is a young player we were talking to. He had quite good equipment in his apartment but he liked playing in Internet cafes, because as he said:

> Shaowzeng: Home has no atmosphere.
> 
> Lee said: I enjoy playing at the cafe because there are more people, it's more exciting. Most of the guild activities are at night, so the people all show up late in the Internet café. I enjoy the atmosphere of people playing around me.
> 
> Yang said: The people here are nice, we play together, they all live around here. We know each other from playing the game.

So people play the virtual game, and at the same time they play with physically present others who maybe live in the same neighbourhood.

I think Internet cafés may be the most important examples of mixed reality spaces that I can think of. They are definitely a global phenomenon, they are found everywhere. I mention the following countries because we have some interesting research about them: China, Korea, Australia, Canada, UK, Jordan, Iraq. But there are other mixed reality spaces as well, including libraries, classrooms, after-school clubs, student dormitories, LAN parties. Mixed reality spaces also include homes. This is a transcript conducted by my undergraduate student using instant messaging service, so this is a very exact transcript (names changed).

> Instant Message transcript
> 
> [14:07] Dan: What do you like about the games you play, you already said you like making friends and what else?  
> [14:07] Mrs. Pain: Hmmm.  
> [14:08] Mrs. Pain: And its not just about making friends cause I do have a very busy social life and many friends.  
> [14:08] Mrs. Pain: I think its kinda an escape.  
> [14:09] Dan: Escape from what?  
> [14:09] Mrs. Pain: From the kids.  
> [14:09] Mrs. Pain: My busy life.  
> [14:09] Mrs. Pain: Time to relax and have fun.  
> [14:09] Dan: So kind of like a stress release?  
> [14:09] Mrs. Pain: Ya.  
> [14:09] Mrs. Pain: Time to get away and have fun with some friends.  
> [14:09] Mrs. Pain: But I am still home for my kids.  
> [14:10] Mrs. Pain: And I am in the same room with my husband who is a gamer.

I think that in the Internet cafés, libraries, dormitories, homes and so forth, we are developing a constellation of mixed reality spaces, even if we give more time to computers, we are not just receding into the purely virtual.

Might we see this as kind of a '_back to the future_' phenomenon? I was really struck by the fact that the Internet cafe looks and feels very much like my old library. I have put together these pictures that show this relation. In one of the pictures you see the picture taken in Beijing of an Internet cafe (Figure 8). The other one I have found on the internet. It is my old Alta Branch Library, Cleveland, Ohio from 1903 (Figure 7), even before it was funded by John Rockefeller. Some citizens really wanted to have a library of their own and they got together and put it in the basement of an old settlement house (community centre) in Cleveland. These two pictures look very similar to me. We have people coming together in social space, sitting together at long tables, sitting about the same distance from one another, they have information sources in front of them (whether computers in Beijing or books and magazines in the library). I was not there in 1903, but I can imagine that there was a lot of quite congenial conversation going on, really quietly, just as in the Internet café in Beijing. The people are talking and communicating but things are kept quiet because people are focusing on their games or the movies they are watching or whatever they are doing with their computers. It seems to me that these information spaces in some very interesting way transcend time and space.

<div align="center">![Figures 7, 8: Alta branch library 1903 and Beijing Internet cafe, 2007](p354fig7.png)</div>

There is another picture of Cleveland library taken in 1953 and the picture that we took in an Internet cafe in Beijing. Again their similarities are striking. In the library we have a group of students sitting together around the table with their books and we have a librarian standing over them. She is an authority, and an expert, and a helper in the library. In the Internet café in Beijing we have the players sitting together with their computers playing the game. There is a café manager standing. The Internet café managers in China are very knowledgeable about computers and software. They can recommend games to you; they can tell you how to play them. Very much like librarians they are the authority and the expert and the help. So, this is all the part of the configuration of the mixed reality, information space.

<div align="center">![Figure 9, 10: Cleveland library in 1952 and Beijing Internet cafe, 2007](p354fig9.png)</div>

This is the final couple of pictures that also struck me as being similar. In the first picture we see the entrance to the Internet café in Beijing: beautiful Chinese colours very warm and inviting. It is difficult to see in the old photograph, but it is a very beautiful old building with lovely brick in Cleveland. In both cases we see a stairways leading up to the doors, there is a signage showing what the activity is in these information spaces. In both cases I find them very congenial, inviting and welcoming.

<div align="center">![Figure 11, 12: Entrance to Beijing Internet cafe; Entrance to Alta Branch Library](p354fig11.png)</div>

Mixed reality information spaces minimize information search time, and maximize results by providing a diverse set of information resources, both technological and human. I would like to quote from a computer scientist Agre who has captured what is important about these spaces:

> As long as we persist in opposing so-called virtual communities to the face-to-face communities of the mythical opposite extreme, we miss the ways in which real communities.employ a whole ecology of media as they think together about the matters that concern them. ([Agre 1999](#agr))

This is a good way to say that we need to be aware of these spaces that are out there and think how we can make them better and help.

I would like to compare mixed reality spaces to another important trend in information spaces, namely, mobile (cell) phones. Mobile phones are good because they are ubiquitous and cheap. But they are peer-to-peer, hence less generative of community; they do not generate the community in the same ways as mixed reality spaces. It also seems that they are less able to bridge the digital divide in information usage because of the small screen format. In the Internet cafés people in China have the sophisticated equipment and the large screen format. In Internet cafés people play games, watch movies, visit visually rich websites, look at photos, and having a big screen is very important for those kinds of activities. It seems that mixed reality spaces provide something that we are not getting with mobile phones.

## Information search and sharing in the game community

Now I move the talk to the discussion of information search that can be accomplished by the digital capture of human activity and shared in a community. I am moving away from the mixed reality issues and focus on more purely virtual kinds of information issues.

I call this _effortless search_. I would like to give three examples from _World of Warcraft_ looking at player-created software modifications that can be plugged into the game. _World of Warcraft_ is a commercial product that you load on your machine and play. But there is an application programme interface that enables anybody to write a small bit of software that can be plugged into the game. Players enjoy doing it and there are thousands of these modifications (or _mods_ as players call them). They are created, maintained, distributed on the Internet for free by players, but, occasionally, Blizzard Entertainment will identify a particularly useful _mod_ and incorporate it into the game. Those who produce the _mods_ appear to have no issues with intellectual property, the _mods_ are regarded as communal property. I will talk in more detail about three of these modifications that are quite popular to give a sense of how they handle information.

### Gatherer

The first one is called _Gatherer_. This is a map of a part of the world: an application that helps you to practice your profession. If you are a miner or a herbalist, your task is to go out into the geography and collect herbs or minerals (copper and tin). The tiny yellow and green dots show the resources. When you extract some from the landscape it disappears from the map and the game will regenerate it over time. When you collected something today, tomorrow you want to do something more, but you may have forgotten where you have been. The application records player actions and where he has been and presents on a map. You have to go where the green or yellow dots are to collect your herbs or find minerals. This is quite straightforward. What I find interesting about this player created application is that your guild-mates see the same information on their maps. This is a very communal kind of information. I do not have to go to the place where the minerals are; I just look at the map and see that Tom already has been there and I know where to go. This becomes a shared community resource. _Gatherer_ also has option to inform guild-mates of a player's activity when they gather an herb or find a mineral. You see one little line saying that Tom has just picked a flower. I get a sense of his activity even when I am not playing with him. This application provides a social awareness mechanism which is very congenial for building community and a guild.

<div align="center">![Figure 13: Gatherer map](p354fig13.png)</div>

<div align="center">**Figure 13: _Gatherer_ map**</div>

Thus, this application provides social information about others' activities, as well as information that helps players do tasks such as finding herbs, but also builds community through awareness information.

### Auctioneer

The second player-created program is called _Auctioneer_. In _World of Warcraft_ you have a backpack where you collect various things (Figure 3). You can buy and sell these things. If you need _game gold_ you go to the _Auction House_ to sell something. You need to know how much to charge, the price for what you are selling. The application _Auctioneer_ monitors transactions at the _Auction House_ over time and provides you with information about what the reasonable price might be for your item. What is very interesting is that players do not even have to load or run this modification. They can do a simple query and _Auctioneer_ will query the databases of their guild-mates. If I type in '? Mystical Mantle of the Whale', a piece of equipment that I want to sell, the _Auctioneer_ software will go and look at the databases that have this application running and will tell me what I should charge for it. This is a very communitarian way of dealing with information. This is also an effortless search based on automatic logging of traces of human activity in a bounded community.

### Recount

The third modification is called _Recount_. It monitors and records player performance. It allows players to compare their performance. _World of Warcraft_ is a competitive game and people always want to know whether they are doing better than others. The little chart shows how much damage was done in slaying the monster. _Magiemae_ is in the first place followed by others. Again, only one person needs to do the recording and that person can share with everyone.

<div align="center">![Figure 14: Recount table](p354fig14.png)</div>

<div align="center">**Figure 14: _Recount_ table**</div>

## Privacy

There is constant recording and sharing of transactions and activities that yields useful information, effortlessly accessed, but it does have a side effect of reducing privacy.

<div align="center">![Figure 15: Scenes from a small town in Ohio](p354fig15.png)</div>

This is another way in which virtual information spaces take us back to the future. They are like small towns where there is little privacy and everyone knows what everyone else is doing. This is s picture of a small town in Ohio where I have spent many summers and where everyone knows everything about everybody. That is how a lot of the Internet feels like to me.

In the vastness of the Internet we are carving out small social spaces (like guilds) where information is contextualized by community produced and disseminated freely, with few constraints.

## Conclusion

In conclusion, video games as information spaces:

*   are moving to mixed reality spaces that fuse the virtual and physical;
*   accustom us to the constant monitoring that creates information and the culture that endorses freely sharing it;
*   many of the applications that record and share information are player-created modifications, so this is not a corporate America, this is a part of the gaming culture; and
*   Internet culture is moving toward monitoring, easy access to information, effortless community search but also less privacy.

Information is the creation and sustenance of community in an uncertain world. We are creating communitarian spaces that freely create, distribute, and share information; much as in the past.

## Acknowledgements

Thanks to students Silvia Lindtner, He Jing, Wenjing Liang, Yong Ming Kow, Nick DiGiuseppe, Trina Choontanom, Justin Harris, Stella Ly who helped with this research, and to Professor Elena Maceviciute for her transcription of my words.

## References

Agre, P.A. (1999). [Life after cyberspace](http://www.webcitation.org/5cnTMvd1R). _EASST Review_, **18**(3) Retrieved 4 November, 2008 from http://www.easst.net/review/sept1999/agre (Archived by WebCite® at http://www.webcitation.org/5cnTMvd1R)

Nardi, B. (forthcoming). _My life as Night Elf Priest: an anthropological account of World of Warcraft_.

Vanderberg B. (1998). Real or not real: a vital developmental dichotomy. In O.N. Saracho & B. Spodek _Multiple perspectives on play in early childhood education_, (pp. 295-306). Albany, NY: SUNY Press.

Yee, N. (2005). _[WoW gender-bending.](http://www.webcitation.org/5cnTbyOOl)_ Retrieved 3 December, 2008 from http://www.nickyee.com/daedalus/archives/001369.php (Archived by WebCite® at http://www.webcitation.org/5cnTbyOOl)