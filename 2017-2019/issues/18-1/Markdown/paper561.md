<header>

#### vol. 18 no. 1, March, 2013

</header>

<article>

# An information need for emotional cues: unpacking the role of emotions in sense making

#### [Natalya Godbold](#author)  
University of Technology Sydney, Australia

#### Abstract

> **Introduction.** The contribution of emotions to collective sense making in virtual communities is explored, by examining implicit and explicit emotional support.  
> **Theoretical approach.** Informational processes are considered from a social constructionist perspective informed by sense-making and ethnomethodology. Theories of normative behaviour and practice theory are used to explore the interplay between social and individual contributions to emotional sense making.  
> **Analysis.** A single 'typical' thread is translated into thematic charts to display particular emotional elements.  
> **Results.** Emotional support emerges by development of consensuses in tones and mirroring of vocabulary and ideas. Explicitly supportive statements are not the main source of support: additionally, interactions between tones and ideas enable substantial shifts in meanings. Thus, emotional cues are an intrinsic element in the informational processes observed.  
> **Practical implications.** Emotional information is an information need for contributors to discussion boards, because of its relevance during the negotiation of meaning in interactions. Tone emerges as an important emotional guide.  
> **Conclusions.** Emphasising factual information during one-off interactions is problematic if people are also looking for emotional sense making cues. Information providers should consider ways to enable access to emotional information for clients and support the need for time in which emotional understanding may develop.

<section>

## Introduction

In online renal discussion groups, people help each other through reporting experiences of kidney failure. This paper explores how contributors provide emotional support to each other, answering Veinot's ([2009](#vei09)) call for further investigation into the 'affective dimensions' of collaborative information behaviour in the context of illness. Here, I discuss emotions both as a form of information and as an intrinsic element in the processes of collaborative sense making that take place online.

## Background

When information studies researchers examine emotions in information behaviour, they commonly consider the emotional states of individuals in relation to other factors (such as situations, individual psychology, tasks or goals); emotions are seen to affect the situation-individual-task-goal and vice versa ([Dervin and Reinhard 2007](#der07)). In particular, emotions are often seen as an element which interrupts or hampers clear thinking during information behaviour ([Kim _et al._ 2007](#kim); [Kuhlthau 2004](#kuh); [Mellon 1986](#mel); [Nahl 2005](#nah); [Savolainen 1995](#sav)); occasionally, emotions are seen to prompt or support particular activities or goals ([Kuhlthau 2004](#kuh); [Nahl 2005](#nah); [Savolainen 1995](#sav)). Either way, they are factors which modify and are modified by the information behaviour of individuals: the emotions are separate from the information work ([Dervin and Reinhard 2007](#der07)).

For researchers interested in social aspects of information, emotions are more closely involved with information; information sharing is seen to thrive in environments where people feel supported and relaxed, able to mix serious information exchange with relaxed chit chat ([Pettigrew 1999](#pet)). In these situations, information sharing and emotions are aspects of information behaviour which are intertwined during the development of shared understandings ([Veinot 2009](#vei09); [Veinot _et al._ 2011](#vei11)).

Occasionally, a different kind of perspective emerges, in which emotions modify understanding of what is seen to be information. For example, feelings of hopelessness or a desire for security may colour the individual's choice of potentially informative sources. ([Chatman 2000](#cha)). Such perspectives view emotions as '_conceptually linked to information_' ([Dervin and Reinhard 2007](#der07): 78), identifying affect as a component of sense-making ([Olsson 2010](#ols)). In relation to her own work Dervin explicitly positions emotions as '_information bearers in a confluence of sense-making elements_' ([Dervin and Reinhard 2007](#der07): 79). Emotions and information have been drawn together ([Colombetti and Torrance 2009](#col)); emotions are _informational_. It is this perspective on emotions which I explore in the analysis to follow.

### Theoretical perspective

Key theoretical influences of this study are from Sense-Making ([Dervin 1999](#der99)) and ethnomethodology ([Garfinkel 1967](#gar)).

Dervin's descriptions of sense-making (unless referring to Dervin, I omit the hyphen) suggest an ongoing process of approximations, during which the human attempts to reconcile inevitable epistemological-ontological gaps between their understanding and their experiences. By describing the human who makes sense as a 'body-mind-heart-spirit' ([Dervin 1999](#der99): 730), it was Dervin who brought my attention to the importance of emotions in the bumbling processes of making sense. She also emphasized that making sense '_is accomplished by verbings_' ([2001/2003](#der01): 238-239). Her emphasis on verbs focuses attention on processes or activities, which bring about change and which are themselves transitory, rather than nouns, which are stable. From this perspective, information is not _object_ but _process_ ([Buckland 1991](#buc)); more specifically, a process of relating ([Egan and Shera](#ega) 1952: 132; [Hjørland 2007](#hjo); [Karamuftuoglu 2009](#kar); [Sundin and Johannisson 2005](#sun)). Viewing information and knowledge as processes or relationships foregrounds the connection-making they involve. Here, I explore ways in which emotions are involved in the processes of connection-making that constitute knowing or being informed.

Ethnomethodology directs attention to interplays between social and individual sense making, the _methods_ by which people (_ethno_) make sense of and conduct themselves in social situations. Viewing society as a '_system of rule governed activities_' and exploring how people maintain those rules ([Garfinkel 1967](#gar): 74), Garfinkel looked for ways in which the social structures of everyday activities are enacted, rather than assuming that people follow rules, they are instead assumed to recreate them ([Rawls 2002](#raw)).

Ethnomethodology dovetails well with Dervin's sense-making methodology, in that both conceive individuals as sense making theorists who actively design their moves. For both, meaning is contextual and developed by participants in ways particular to each unique situation and both require that the researcher pay attention to context and the involvement of context in people's interpretation of meaning. But while Dervin's approach is to interview individual participants, ethnomethodological research commonly involves ethnographic immersion of the researcher in the field of study, with attention paid to the interactions of people in shared situations: collective rather than individual sense making.

### Setting of the study

As a treatment for kidney failure, options for dialysis commonly involve two large needles in the arm for several 4-8 hour treatments a week, or a permanent tube in the belly. Common experiences of dialysis include physical symptoms such as fatigue, cramping, nausea, vomiting and thirst ([Lee _et al._ 2007](#lee); [O'Sullivan and McCarthy 2009](#osu); [Waters 2008](#wat)). Psychological symptoms include anger, grief, anxiety, depression and uncertainty ([Bellou _et al._ 2006](#bel); [Kring and Crane 2009](#kri); [Polaschek 2005](#pol); [Wong _et al._ 2009](#won)). Dialysis also has social effects for example, on family and work ([Tong _et al._ 2009](#ton); [White _et al._ 1999](#whi)). As such, chronic kidney failure requires ongoing sense making.

Online discussion groups are internet forums where people discuss ideas by typing posts, usually a paragraph or two long. Posts are gathered into threads which maintain the conversational order in which they appeared and remain on semi-permanent display. Online gatherings such as discussion groups may function as communities ([Hampton and Wellman 2003](#ham); [Ley 2007](#ley)), to which members say they turn for information exchange and emotional support ([Armstrong and Powell 2009](#arm); [Bacon _et al._ 2000](#bac); [Bonniface and Green 2007](#bon); [Sharf 1997](#sha)). Social relationships online are strengthened by activities such as sharing or _gifting_ of information, resources and ideas ([Krasnova _et al._ 2010](#kra); [Skågeby 2010](#ska)), while virtual communities also exhibit normative behaviour, limiting the range of attitudes or emotions which contributors might feel they can manifest ([Burnett _et al._ 2001](#bur)).

Despite the established function of virtual communities for providing support, Chuan and Yang ([2010](#chu)) found that explicit emotional support was rare in forums. Meanwhile, commenting on the use of Twitter during campus shootings, Heverin and Zach note unanswered, emotional tweets which they describe as a '_talking cure_', proposing that their purpose was merely to express feelings without the expectation of a response ([2011](#hev)). If direct emotional support is rare and such posts are left apparently unanswered, how does emotional support work in virtual communities?

I spent almost two years observing and contributing to discussions in three online renal discussion groups. Making myself known to the groups both as a researcher and as the wife of a kidney transplant recipient, I aimed to develop my understanding of how people in these communities interacted with each other to make sense of their shared concerns. Though I asked no research questions, my status as a researcher remained visible to members via my signature and profile. Consent to join the groups was negotiated with moderators, while consent to use posts was requested by showing proposed writing to contributors, obtaining feedback and permission on a case by case basis. Ethics approval for the project was obtained from the University of Technology, Sydney, before commencement.

Discussions in the three groups were characterised by ongoing exchanges in which ideas were sustained and developed, involving detailed, personal narratives while the meanings of events and situations were teased out. Such interactions enabled my investigation into how sense is developed in communities and how individuals draw from and contribute to communal sense making (also explored in [Godbold, in press](#god)).

In January 2012 the combined membership of the three groups I studied was 6,580\. However, _total membership_ is a problematic figure, as there is no way to know how many people joined and are no longer active: some read but never post, some will have lost interest and some are deceased. In May 2011, posts made during a week of discussions were copied into a database, to create a sample with which to contextualise the study and its populations. Consent was obtained for the use of contributions from 147 people who made 1,193 posts, of which 787 posts contained content written by the contributor about renal failure. Table 1 (below) shows the relative contribution of each group during that week.

<table><caption>  
Table 1: Sources of data</caption>

<tbody>

<tr>

<th rowspan="2">Source</th>

<th rowspan="2">Total membership since inception</th>

<th colspan="4">During week in May 2011</th>

</tr>

<tr>

<th>No. of contributors</th>

<th>No. of threads</th>

<th>No. of posts</th>

<th>No. of posts about renal failure*</th>

</tr>

<tr>

<td>Group1</td>

<td>6104</td>

<td>120</td>

<td>170</td>

<td>874</td>

<td>649</td>

</tr>

<tr>

<td>Group2</td>

<td>404</td>

<td>17</td>

<td>40</td>

<td>272</td>

<td>92</td>

</tr>

<tr>

<td>Group3</td>

<td>72</td>

<td>14</td>

<td>11</td>

<td>47</td>

<td>46</td>

</tr>

<tr>

<td>Total</td>

<td></td>

<td>147</td>

<td>221</td>

<td>1193</td>

<td>787</td>

</tr>

<tr>

<td colspan="6">* Excludes posts which reproduce information from other sites, without commentary.</td>

</tr>

</tbody>

</table>

Most contributors were from first world English speaking countries, predominantly from the USA (60%). Contributors were mainly renal patients (74%) and people living with patients (18%) but included some health professionals (2%) and other interested parties. 58% of contributors were female and the average age was 45 years.

However the findings reported here are not based solely on data from that week. In line with ethnomethodological approaches to research, the data presented here are _typical_ interactions. They were produced while I was a member and are analysed in light of my understanding of the norms of the groups. To develop familiarity with interactions, I read an average of 400 posts a week for almost 1.5 years in 2009-10\. I also explored the archives going back to 2005\. I then analysed themes in the sample described above, after which I undertook a close examination of seven threads, one of which I present here.

Because the patterns I wish to describe occur in most threads, 'any thread' will display them, while presenting its own particular deviations. In the coming analysis, I use a single thread, entitled 'I AM BORG', to demonstrate common patterns in the interactions online. After presenting my analysis of this particular thread, I will discuss how it is representative of (and different from) other interactions in the groups by comparing it with data from my other analyses.

I will demonstrate how a range of emotional cues formed part of the sense making processes of people discussing renal failure in online discussion boards. In particular, by observing the timing of tones in posts, I will demonstrate processes by which people steered each other toward and away from possible perspectives, as they developed consensuses of feelings in threads.

## I AM BORG

'I AM BORG' was a discussion about the normal development of a fistula for renal dialysis. A renal fistula is an enlarged vein created surgically by joining a vein and an artery, commonly located in the arm. The enlarged vein makes it easier and safer to repeatedly insert needles for dialysis. This thread was initiated by a person who had not begun dialysis and had a relatively new fistula. I have reproduced text from the thread without changing spelling or punctuation.

> Topic: I AM BORG! *  
>   
> Post No. 1  
> Person 1  
> Just a quick question as I need a bit of reassurance. I just got out of the shower and I happened to notice how pronounced my veins are in my left arm and up into my shoulder and onto my chest. My fistula is in my upper left arm. It looks as if someone has taken a thin paint brush and has painted light blue lines all up my arm and the lines are in 3D. Is this normal?  
>   
> Post No. 2  
> Person 2  
> First, make sure you don't have two pairs of glasses on. My veins are definitely more pronounced on my fistula arm.  
>   
> Post No. 3  
> Person 3  
> It is totally normal [nickname]. They get more pronounced since they are rerouting the blood due to the fistula creation. The main fistula vein will develop even more as it is used and mine is quite large. As hard as it is to do, I try to befriend it and view it as my lifeline which of course it is when we're on D [i.e., dialysis]. Mine shows no sign of going anywhere despite transplant almost two years ago. ![emoticon1](../emoticon1.jpg) [This animated emoticon 'hugs' the recipient.]  
>   
> Post No. 4  
> Person 4  
> Yup, normal. I've still got a place below my collarbone on the left side that everyone thinks is bruised since there are so many extra veins just below the surface.  
>   
> \* The Borg is an alien civilisation depicted in the TV series _Star Trek_. They are warlike and actively invade other species. They then assimilate prisoners by adding machine parts (including a robotic eye) and connecting their conciousness to the collective Borg mind.

These are the first four of twenty-three posts in this thread.

### Thematic analysis

I conducted a thematic analysis of threads, identifying tones as I saw them used locally. In this thread I identified five principal tones: positive, playful, worried, reassuring and unconcerned. Positive posts were those which presented an optimistic perspective, such as befriending one's fistula and remembering that it allows the treatments that keep you alive. Playful posts appeared to me to make jokes, such as the opening comment in the thread: 'I AM BORG' or the response, '_make sure you don't have two pairs of glasses on_'. I coded posts as worried when they said they were worried, or even if there was a chance the person was worried, such as when Person A asked, '_Is this normal_', which could be a worried question. Not all posts said that '_it was normal_'; those that did specifically address the question of it _all being quite normal_, I coded as reassuring. 'Unconcerned' was a tone which I used to code posts which cheerfully discussed their fistulae or experiences. Many such posts were brief (e.g. Post No. 2 and No. 4), suggesting that the contributors' attention did not rest long on the issue. In all three online groups, people frequently comment that they care for or worry about other members who are like 'family'. So lack of attention here conveys the message that there is little to worry about, particularly when brevity is coupled with playfulness as in Post No. 2\. These posts were informative for their lack of worry and lack of detail.

The chart at Figure 1 below represents the analysis of tones in this thread. The discussion begins at the left hand side and runs across in chronological order to the right. Hence, the column marked '1' represents the first post in the thread. Different tones are listed in the first column at the far left. When a tone appears in a post, the row is coloured in the column for that post. Where consent was not obtained to include a particular post, the column has been blanked out.

</section>

<figure>

![Representation of emotional tones](../p561fig1.jpg)

<figcaption>Figure 1: Representation of emotional tones in 'I AM BORG'</figcaption>

</figure>

<section>

#### Development of emotional consensuses

The resulting chart (Figure 1) displays how playful, humorous, unconcerned tones (rows 4 and 6) dominated the thread. In this section I will explore how the _reiteration_ of tones creates a kind of emotional consensus, an overall feeling for how most contributors felt about the topic at hand.

As I described earlier, Person 1 opened the thread with tones which were both humorous and worried. Looking at Row 5, only one other post echoed worried tones back to her: Post No. 7 balanced having been worried (acknowledging that being worried is reasonable) with now being confident about her fistula. Meanwhile, all other posts responded to Person 1's call for reassurance with unconcerned tones. The overall impression of the thread was created by reiteration of tones, sometimes developing ideas but often simply echoing them and reinforcing their effect. By each post mirroring the unconcerned, often playful tones of the others, emotional consensus emerged. This is not to suggest that everyone agreed. Typically, in threads, as people take up content and tone and move them in ways relevant to their own experiences, nuances emerge with an accompanying complexity of tones. Lone voices are frequent and in most threads, more than one consensus is likely. Even in 'I AM BORG', one could argue that a collection of consensuses, including playfulness, positivity and an acknowledgement that the situation is serious, are each manifested to different degrees by the various contributors. Importantly, being worried appeared not to be a consensus.

#### Shifting tones

Once a number of posts appeared in a thread, it was common to see an individual repost and shift their tone to match emergent consensuses. For example Person1 implied a sense of worry about her fistula (row 5 in Figure 1), but received three positive, unconcerned replies (Post No. 2-Post No. 4). When she next posted her tone shifted to positivity and unconcern, matching theirs (Post No. 5).

#### Shifting meanings

Tones worked together with vocabulary, to achieve shifts in the meaning of ideas. The following quotes are taken from a bit later in the discussion:

> Post No. 7  
>   
> Person 6  
> [...]  
> I have one of those lovely psuedoanyerisms* near my elbow. From time to time it will pulsate and I can actually see it pulsate. It freaked me out the first time I saw it, so I kinda did what you did and went to twitter. Within a few minutes, I was told by several people that it was normal.  
>   
> [...]  
>   
> Post No. 9  
> Person 1  
> [quoting Post No. 7]  
> I think I have a pseudoanuerysm, too... sort of a knot adjacent to my scar that I can see pulsate. Creepy.  
>   
> Post No. 10  
> Person 8  
> It's always something you can scare kids with, though.![emoticon 2](../emoticon2.jpg) [This animated emoticon makes a 'cheers' motion with the beer steins]  
>   
> [...]  
>   
> Post No. 12  
> Person 9  
> Captains Log; Stardate 29th December 2010.** The collective have decided you cannot be BORG for although you share the group ideal you do not have any tubes protruding from any body parts... LIKE ME ! ![emoticon 3](../emoticon3.jpg) I am therefore BORG but you may be Klingon ?![emoticon 3](emoticon3.jpg) [This animated emoticon 'rolls on floor laughing' whilst slapping the ground.]  
>   
> 
> * * *
> 
> \* An aneurysm is an abnormal swelling in the wall of an artery; in a pseudoaneurysm the swelling of the artery is contained by clotted blood rather than the artery wall ([Martin 2010](#mar)).  
>   
> ** References in this post are to the Star Trek series: narration in the show is accomplished by voice-overs in which the Captain of the 'Enterprise' reads from his log. The Klingon are an alien, humanoid race with large, furrowed foreheads.

The rest of the thread was a series of jokes about the advantages of being cyborg.

The point here is how the conception of fistulas changed and how tones supported those shifts in meaning. The original post presented fistulas as a bit scary, as alien or _Borg_. Many of the early replies, with their reassuring, cheerful tones, presented an alternative view of fistulas as useful and medically practical. Now, we see fistulas as a bit weirdly fascinating. Person 6 described the way her '_lovely_' pseudoaneurysm '_pulsates_' (lines 30-31). She acknowledged the idea of being worried, but her tone suggested a kind of detached enjoyment of the lovely, pulsating swelling. In reply, Person 1 picked up on both the vocabulary and the idea of the pulsating pseudoaneurysm, as well as the bemused, fascinated tone. Next fistulas were presented as fun and finally, as a part of the dialysis identity. One's identity as dialysis patient become related to one's potential identity as borg and the borg identity emerged as something desired, something with positive outcomes. The playful development of the borg theme was co-developed with the idea of strangeness as good. In combination with the cheerful tone, both being cyborg and being an outsider became sources of pride. Emotional tones were used to select particular perspectives or reinforce them. These analyses demonstrate incremental tonal shifts achieved substantial shifts in meaning: how content and tone were interrelated.

#### Explicit versus implicit emotional support

The next chart (Figure 3) shows a different view of the thread, created by marking examples of _emotional support, experience, explanations_ and _humour_. It was created by making the rows one square thicker whenever a sentence containing that particular theme appeared in a post. The top row, _experience_, was marked whenever a sentence described a lived experience ('_Mine is like this..._', '_I thought this..._', '_we do it this way_' and the like). _Explanation_ shows instances of general information not explicitly embedded in experience (such as, '_The fistula will enlarge as it matures_', '_the whole point is to enlarge your veins..._'). _Explicit support_ was reserved for sentences which directly express understanding or reassurance ('_I feel the same_', '_don't worry_', or in this instance, the reassuring hug in Post No. 3). Meanwhile _implicit support_ shows where people described having the same experiences or having felt the same way. This is a wide view of support; such mirroring of experiences suggests comprehension of feelings (sympathy) without the use of directly compassionate expression. _Humour_ includes jokes, statements marked with laughing emoticons and references to the importance of humour. Through humour, contributors demonstrated ways of dealing positively and cheerfully with difficulties such as stigma and fear of medical complications. [This use of humour to release negative emotions and enable discussion of otherwise disturbing ideas is a matter for more detailed discussion - see for example, Freud ([1976](#fre)).]

</section>

<figure>

![I AM BORG': summary of key elements](../p561fig3.jpg)

<figcaption>Figure 3: 'I AM BORG': summary of key elements</figcaption>

</figure>

<section>

As noted earlier, the literature suggests people frequent discussion boards for information and support. The analysis above reveals the possible danger of taking such findings superficially by assuming that 'support' might manifest as directly supportive statements. _Explicit support_ is the least frequently appearing theme. Even the row for _implicit support_ does not reflect the full picture of support that took place in the thread. By contrast, _unconcerned tone_ is a steady guiding presence throughout the thread. Meanwhile, the humorous turn commencing at Post No. 10 coincides with people exploring the negative side of having a fistula, reconciling the disturbing look of it and changing the meaning of being cyborg. In this way a variety of emotional guidance and informational cues from tone can be found throughout the thread. This thread is unusual for its _extreme_ lack of explicit support: many have more than this. Nonetheless, most threads have less explicit support than any other element.

One could also assume that by _information_, people mean facts, such as how things work or what one must do or should expect. Such information might be located in descriptions of _experiences_ and general _explanations_. But Figure 3 shows that these elements are balanced by instances of emotional information such as explicit and implicit support and the ongoing supportive tones provided throughout the thread, confirming the point that cognitive and emotional informational processes are both significantly involved in making sense.

## Findings

I have used a single thread to demonstrate what I observed to be common to discussions in all three boards:

*   emotional consensus formed through repetition of similar tones;
*   emotional guidance provided through tones and demonstrated by shifts in tone;
*   gradual iterative changes in meanings, supported by tone; and
*   indirect emotional support embedded in non-emotive language.

### How representative is 'I AM BORG'?

'I AM BORG' is one of seven threads which were analysed in detail by creating charts like those shown in this paper. The seven threads were a purposive sample, four of which were chosen to find threads which might not display the patterns described here ([Silverman 2006](#sil)). Two of the seven threads were not on the topic of renal failure, while another was characterised by non-harmonious interactions. Yet the patterns which I have summarised above were all noted in each of the seven threads.

Meanwhile in an earlier phase of this study, I had coded each post from the week in May 2011 to themes including _experience_, _explanations_ and _explicit support_. Multiple codes were possible for each post, but in this initial coding, I did not register how often a theme recurred within posts. (Thematic codes were created during iterations of coding, and are an outcome of my meaning making as I read and re-read the data ([Charmaz, 2006](#cha)).). This showed _if a post contained a theme_, but not _how often_. Threads were then analysed to establish frequencies with which various themes were noted in each thread. The analysis included 82 threads comprised of 588 posts. (Threads were included in the analysis if three or more successive posts were added during the week, composed by contributors, on the topic of renal failure. New members introduce themselves with an _Introduction_ thread, and are welcomed into the community with encouraging replies. Five _introduction_ threads were excluded, as they contain significantly more explicit support (such as '_Welcome!_') than other threads.)

Figure 4 below compares the appearance of themes in the seven closely coded threads, with the larger sample. It shows how for each of these sets of threads, _explicit support_ appeared in threads least often and lived _experience_ the most. These brief comparisons also suggest that, in terms of the relative incidence of these themes, the set of seven threads is representative of the larger sample.

<figure>

![Frequency of themes within posts](../p561fig4.jpg)

<figcaption>Figure 4: Frequency of themes within posts (themes counted once for each sentence)</figcaption>

</figure>

Meanwhile in Figure 5 below, 'I AM BORG' emerges as a fair representative of the appearance of themes in the seven threads analysed. Figure 5 displays the incidence in threads of sentences demonstrating the themes _lived experience_, _implicit support, explanation_ and _explicit support_, showing that the percentage incidence for each theme is similar between 'I AM BORG' and the set of seven threads. Figure 5 also strengthens the assertion that posts were comprised far more of _lived experience_ than _explanation_ and that _explicit support_ appeared least often in every thread.

<figure>

![Appearances of themes in posts](../p561fig5.jpg)

<figcaption>Figure 5: Appearances of themes in posts (themes counted once for each post)</figcaption>

</figure>

In both analyses, 'I AM BORG' differs from other threads by having less frequent _explicit support_ than the norm - (1.6%) compared to the sample of all seven threads (8.0%). This may be because contributors viewed the situation without concern. Other threads, in which there is a matter of concern, contain more instances of _explicit support_ such as '_hang in there_' or '_you can do this_'.

## Discussion

Making sense involves not just getting _ideas_ straight _in your head_ but also being able to '_feel_ all right' about yourself and your situation. In 'I AM BORG', posts demonstrate a range of contextualised emotional reactions, which serve as guides for the reader. As such, the stories, anecdotes, exclamations and facts that comprise posts do not only convey what might be called cognitive elements, things that you might think, but simultaneously they convey emotional elements, things that you might feel. 'Informing' necessarily occurs on a range of dimensions.

In her work on social norms, Chatman suggests that people are guided by norms to the extent that they will stop seeking or ignore information, if they feel that it may contravene the norms of their social world ([2000](#cha)). From this perspective, norms could be seen to _trap_ the individual or leave them with restricted options. The detail of interactions here allows us to see a dynamic between the weight of existing consensuses and the capacity of each new post to add to, detract from or shift the direction of the thread in new directions. People benefit from a sense of the combined sense making of others (by taking on their tones) but also have the option to modify norms (using different tones, as _counter-conduct_; [Foucault 2007](#fou)). Some practice theorists emphasise this ongoing interplay between expectations and enactment, as an essential part of all practices ([Gherardi 2009b](#ghe9b)). Gherardi directs attention towards '_how practices are reproduced and in being reproduced change over time - intentionally and unintentionally_' ([2009a: 124](#ghe9a)). This includes practices or ideas which are disruptive or contrary and which may, as they do sometimes here, change or contrast with the consensus. In this sense, norms can seen as oppressive, as guiding and as flexible and re-workable.

Instead of providing each other with bits of emotional information, the interactions I have unpacked here reveal how tones, feelings, ideas and meanings combine to form shared sense making online, worked up in individual posts and passed forward to the group. The image of information as '_obtained and processed_' does little justice to the situational embeddedness of intertwined informational processes. In addition, this paper shifts attention from emotions perceived in terms of psychological profiles (e.g., optimistic, determined, extrovert) which may act as intervening variables affecting the onset of information behaviour (e.g., [Wilson 1997](#wil) raised the relevance of such factors, in a useful model of information behaviour). Instead, much has been learned here by paying attention to the changing emotions interleaved throughout interactions. These are _collaborated emotions_ in dialogue with individually owned feelings, built up by contributors in mutually recognisable ways regardless of their various individual psychological profiles.

### Implications for professional practice

In these threads, guidance by tones was not a one-off event. The overall tone of a thread emerged post by post, by repetition of particular tones combined. It is not until consensus emerged that individuals changed their tones to match. It would appear that emotional sense making involves repetition and the recognition of patterns, developing familiarity. Health professionals may expect that patients will need no more than the facts, but perhaps they do need more; not because they need more accurate information, but because they need time for the tones of the conversation to provide necessary emotional cues. This highlights problems with one-off information provision such as single, typically rushed appointments with a doctor or specialist, if they do not allow for repetition or allow time for the demonstration and uptake of emotional understandings.

When dealing with threatening situations such as loss of good health or loss of key beliefs, people may be seeking not only for what to think but also for how to feel. Meanwhile doctors who aim to provide factual information with professional detachment may unintentionally impoverish patient-doctor interactions by removing useful emotional cues, such as calm, cheer, urgency or concern. This raises questions about how information professionals could respond to patients' possible need for emotional information and support. Most useful is the potential for further development and support of peer-based information networks (noted by [Veinot 2009](#vei09)). Information professionals could facilitate connections between people with similar experiences, by enabling access to people whose emotional cues have the authority of experience. This could be done by helping people to set up local discussion groups, join existing groups and contribute to them and by considering ways to increase the useability and usefulness of discussion board software and archictecture.

## Conclusion

This paper contributes to relational conceptions of sense making, by demonstrating its emotional as well as conceptual dimensions. Rather than treating emotions as attributes of individuals which affect information behaviours but are separate from them, I have shown how collaborated emotions are intrinsic to the processes by which informing takes place, affecting the meanings we derive. Emotional cues emerge as an information need for contributors to the discussion boards, because of their relevance to the negotiation of meaning in interactions.

Emotional information can be, but is not restricted to direct expressions of support or understanding. The evidence in this analysis located emotional information in the mirroring of vocabulary and experience and in the judicious use of humour. Most saliently, however, tone was an important emotional guide.

In the field of internet research, emotional support has already been established as an equal partner with information seeking for members of virtual communities. Information studies professionals need to take that partnership seriously, with an understanding of the nuances of emotional support revealed here. We should consider where and how emotional cues may already be available in various systems of information provision and how they may usefully be extended.

## Acknowledgements

The author gratefully acknowledges the support of IHateDialysis, Australian Dialysis Buddies and KidneyKorner and the knowledge and generosity of the members of those forums, as well as the advice and support of her supervisors Michael Olsson and Rick Iedema. She is also thankful for the critical input of Hilary Yerbury and Marie Manidis, the last of whom provided inspiration for the charts used in this analysis.

This research was supported by an APA Scholarship from the Australian Commonwealth Government.

## About the author

**Natalya Godbold** is a doctoral candidate in the Department of Information and Knowledge Management, Faculty of Arts and Sciences, University of Technology Sydney, Australia. She received her Bachelor of Science from the University of Queensland, Australia and her Master of Library and Information Science from the University of Technology Sydney, Australia. She can be contacted at: [ngodbold@gmail.com](ngodbold@gmail.com)

</section>

<section>

## References

<ul>
    <li id="arm">Armstrong, N. &amp; Powell, J. (2009). Patient perspectives on health advice posted on
        Internet discussion boards: a qualitative study. <em>Health Expectations</em>,
        <strong>12</strong>(3), 313-320.
    </li>
    <li id="bac">Bacon, E. S., Condon, E. H. &amp; Fernsler, J. I. (2000). Young widow's experience with an
        Internet self-help group. <em>Journal of Psychosocial Nursing &amp; Mental Health Services</em>,
        <strong>38</strong>(7), 24.
    </li>
    <li id="buc">Buckland, M. K. (1991). Information as thing. <em>Journal of the American Society for
            Information Science</em>, <strong>42</strong>(5), 351-360.
    </li>
    <li id="bur">Burnett, G., Besant, M. &amp; Chatman, E. A. (2001). Small worlds: normative behavior in
        virtual communities and feminist bookselling.<em>Journal of the American Society for Information
            Science and Technology</em>, <strong>52</strong>(7), 536-547.
    </li>
    <li id="cha">Chatman, E. A. (2000). Framing social life in theory and research. <em>New Review of
            Information Behaviour Research</em>, <strong>1</strong>(1), 3-17.
    </li>
    <li id="chu">Chuang, K. Y. &amp; Yang, C. C. (2010). Helping you to help me: exploring supportive
        interaction in online health community. <em>Proceedings of the American Society for Information
            Science and Technology</em>, <strong>47</strong>(1), 1-10.
    </li>
    <li id="col">Colombetti, G. &amp; Torrance, S. (2009). Emotion and ethics: an inter-(en)active approach.
        <em>Phenomenology and the Cognitive Sciences</em>, <strong>8</strong>(4), 505-526.
    </li>
    <li id="der99">Dervin, B. (1999). On studying information seeking methodologically: the implications of
        connecting metatheory to method. <em>Information Processing &amp; Management</em>,
        <strong>35</strong>(6), 727-750.
    </li>
    <li id="der01">Dervin, B. &amp; Frenette, M. (2001/2003). Sense-making methodology: communicating
        communicatively with campaign audiences. In B. Dervin, L. Foreeman-Wernet &amp; E. Lauterbach
        (Eds.), <em>Sense-making methodology reader: selected writings of Brenda Dervin</em> (pp. 233-249).
        Cresskill, NJ: Hampton Press.
    </li>
    <li id="der07">Dervin, B. &amp; Reinhard, C. D. (2007). How emotional dimensions of situated information
        seeking relate to user evaluations of help from sources: an exemplar study informed by sense-making
        methodology. In D. Nahl &amp; D. Bilal (Eds.), <em>Information and emotion: the emergent affective
            paradigm in information behavior research and theory</em> (pp. 51-84). Medford, NJ: Information
        Today.
    </li>
    <li id="ega">Egan, M. E. &amp; Shera, J. H. (1952). Foundations of a theory of bibliography. <em>Library
            Quarterly</em>, <strong>22</strong>(2), 125-137.
    </li>
    <li id="fou">Foucault, M. (2007). Security, territory, population: lectures at the Collège de France
        1977-1978. Basingstoke, UK: Palgrave Macmillan.
    </li>
    <li id="fre">Freud, S. (1976). <em>Jokes and their relation to the unconscious</em> (J. Strachey,
        Trans.). Harmondsworth, UK: Penguin.
    </li>
    <li id="gar">Garfinkel, H. (1967). <em>Studies in ethnomethodology</em>. Cambridge: Polity Press
    </li>
    <li id="ghe9a">Gherardi, S. (2009a). Introduction: the critical power of the 'practice lens'.
        <em>Management Learning</em>, <strong>40</strong>(2), 115-128.
    </li>
    <li id="ghe9b">Gherardi, S. (2009b). Knowing and learning in practice-based studies: an introduction.
        <em>Learning Organization</em>, <strong>16</strong>(5), 352-359.
    </li>
    <li id="god">Godbold, N. (In press). Usefully messy: how people use rich descriptions to make sense in
        online renal discussion groups. In G. Widén &amp; K. Holmberg (Eds.), <em>Social information
            research [working title]</em>. Amsterdam: Elsevier Academic Press.
    </li>
    <li id="ham">Hampton, K. &amp; Wellman, B. (2003). Neighboring in netville: how the internet supports
        community and social capital in a wired suburb. <em>City &amp; Community</em>,
        <strong>2</strong>(4), 277-311.
    </li>
    <li id="hjo">Hjørland, B. (2007). Information: objective or subjective/situational? <em>Journal of the
            American Society for Information Science and Technology</em>, <strong>58</strong>(10),
        1448-1456.
    </li>
    <li id="kar">Karamuftuoglu, M. (2009). Situating logic and information in information science.
        <em>Journal of the American Society for Information Science and Technology</em>,
        <strong>60</strong>(10), 2019-2031.
    </li>
    <li id="kim">Kim, K., Lustria, M., Burke, D. &amp; Kwon, N. (2007). <a
            href="http://www.webcitation.org/6Eg18rguK">Predictors of cancer information overload: findings
            from a national survey</a>. <em>Information Research</em>, <strong>12</strong>(4) Retrieved 27
        April 2012 from http://informationr.net/ir/12-4/paper326.html (Archived by WebCite® at
        http://www.webcitation.org/6Eg18rguK)
    </li>
    <li id="kra">Krasnova, H., Spiekermann, S., Koroleva, K. &amp; Hildebrand, T. (2010). Online social
        networks: why we disclose. <em>Journal of Information Technology</em>, <strong>25</strong>(2),
        109-125.
    </li>
    <li id="kuh">Kuhlthau, C. C. (2004). <em>Seeking meaning : a process approach to library and information
            services</em> (2nd. ed.). Westport, CT: Libraries Unlimited.
    </li>
    <li id="lee">Lee, B., Lin, C., Chaboyer, W., Chiang, C. &amp; Hung, C. (2007). The fatigue experience of
        haemodialysis patients in Taiwan. <em>Journal of Clinical Nursing</em>, <strong>16</strong>(2),
        407-413.
    </li>
    <li id="ley">Ley, B. L. (2007). Vive les roses!: the architecture of commitment in an online pregnancy
        and mothering group. <em>Journal of Computer-Mediated Communication</em>, <strong>12</strong>(4),
        1388-1408.
    </li>
    <li id="mar">Martin, E. A. (Ed.). (2010). <em>Concise medical dictionary</em> (8 ed.). Oxford; New York,
        NY: Oxford University Press.
    </li>
    <li id="mel">Mellon, C. (1986). Library anxiety: a grounded theory and its development. <em>College
            &amp; Research Libraries</em>, <strong>47</strong>(2) 160-165.
    </li>
    <li id="nah">Nahl, D. (2005). Affective load. In K. E. Fisher, S. Erdelez &amp; L. McKechnie (Eds.),
        <em>Theories of information Behavior</em> (pp. 39-43). Medford, NJ: Information Today.
    </li>
    <li id="ols">Olsson, M. (2010). All the world's a stage: making sense of Shakespeare. <em>Proceedings of
            the American Society for Information Science and Technology</em>, <strong>47</strong>(1), 1-10.
    </li>
    <li id="pet">Pettigrew, K. E. (1999). Waiting for chiropody: Contextual results from an ethnographic
        study of the information behaviour among attendees at community clinics. <em>Information Processing
            and Management</em>, <strong>35</strong>(6), 801-817.
    </li>
    <li id="pol">Polaschek, N. (2005). Haemodialysing at home: the client experience of self-treatment.
        <em>EDTNA/ERCA Journal of Renal Care</em>, <strong>31</strong>(1) 27-30.
    </li>
    <li id="raw">Rawls, A. W. (2002). Editor's introduction. In H. Garfinkel &amp; A. W. Rawls (Eds.),
        <em>Ethnomethodology's program: working out Durkheim's aphorism</em> (pp. 1-64). Oxford: Rowman
        &amp; Littlefield
    </li>
    <li id="sav">Savolainen, R. (1995). Everyday life information seeking: approaching information seeking
        in the context of "way of life". <em>Library and Information Science Research</em>,
        <strong>17</strong>(3),259-294.
    </li>
    <li id="sha">Sharf, B. F. (1997). Communicating breast cancer on-line: support and empowerment on the
        Internet. <em>Women &amp; Health</em>, <strong>26</strong>(1), 65-84.
    </li>
    <li id="sil">Silverman, D. (2006). <em>Interpreting qualitative data: methods for analysing talk, text
            and interaction</em> (3rd. ed.). Thousand Oaks, CA: Sage Publications.
    </li>
    <li id="ska">Skågeby, J. (2010). Gift-giving as a conceptual framework: framing social behavior in
        online networks. <em>Journal of Information Technology</em>, <strong>25</strong>(2), 170-177.
    </li>
    <li id="sun">Sundin, O. &amp; Johannisson, J. (2005). Pragmatism, neo-pragmatism and sociocultural
        theory: Communicative participation as a perspective in LIS. <em>Journal of Documentation</em>,
        <strong>61</strong>(1) 23-43.
    </li>
    <li id="ton">Tong, A., Sainsbury, P., Chadban, S., Walker, R. G., Harris, D. C., Carter, S. M., <em>et
            al.</em> (2009). Patients' experiences and perspectives of living with CKD. <em>American Journal
            of Kidney Diseases</em>, <strong>53</strong>(4), 689-700.
    </li>
    <li id="vei09">Veinot, T. C. (2009). Interactive acquisition and sharing: understanding the dynamics of
        HIV/AIDS information networks. <em>Journal of the American Society for Information Science and
            Technology</em>, <strong>60</strong>(11), 2313-2332.
    </li>
    <li id="wat">Waters, A. L. (2008). An ethnography of a children's renal unit: experiences of children
        and young people with long-term renal illness. <em>Journal of Clinical Nursing</em>,
        <strong>17</strong>(23), 3103-3114.
    </li>
    <li id="whi">White, Y., Grenyer, B. F. S. &amp; Grenyer, B. (1999). The biopsychosocial impact of
        end-stage renal disease: the experience of dialysis patients and their partners. <em>Journal of
            Advanced Nursing</em>, <strong>30</strong>(6) 1312-1320.
    </li>
    <li id="wil">Wilson, T. D. (1997). Information behaviour: an interdisciplinary perspective.
        <em>Information Processing &amp; Management</em>, <strong>33</strong>(4), 551-572.
    </li>
    <li id="won">Wong, J., Eakin, J., Migram, P., Cafazzo, J. A., Halifax, N. V. D. &amp; Chan, C. T.
        (2009). Patients' experiences with learning a complex medical device for the self-administration of
        nocturnal home hemodialysis. <em>Nephrology Nursing Journal</em>, <strong>36</strong>(1) 27-33.
    </li>
</ul>

</section>

</article>