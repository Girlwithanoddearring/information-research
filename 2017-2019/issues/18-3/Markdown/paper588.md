<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

# Measuring patients' preferences and priorities for information in chronic kidney disease

#### [Paula Ormandy](#authors)  
School of Nursing, Midwifery & Social Work, University of Salford  
#### [Claire Hulme](#authors)  
Academic Unit of Health Economics, Leeds Institute of Health Sciences, University of Leeds

<section>

#### Abstract

> **Objective**. To evaluate an information needs questionnaire that uses a Thurstone’s paired comparison approach to measure the information needs of patients with chronic kidney disease.  
> **Methods**. A two-phased, cross-sectional survey that sequentially develops and tests the questionnaire. Semi-structured interviews (n=20) generated information needs items for inclusion in the questionnaire. These items were paired using Ross’s matrix. The questionnaire was used in interviews in phase two (n=89) and paired comparison analysis was undertaken.  
> **Results**. A majority of patients ranked highly self-care information, how to manage their own condition, better control their diet and fluid intake, and understand blood results. 70.79% of patients agreed all items in the questionnaire to be relevant; there was an acceptable degree of reliability for data scalability (R2 =0.6175); agreement found between patients (Kendall’s coefficient 0.06, p<0.001); and all patients’ responses were deemed consistent (circular triads >30). Despite this, there was not a good fit between data and Case V and Case III models (Mosteller χ<sup>2</sup>=52.21, p=0.003, χ<sup>2</sup>=49.49 p<0.001 respectively).  
> **Conclusion**. Using this approach it is possible to identify and measure the strengths of preferences for information of chronic kidney disease patients. Further testing with a larger sample to examine the internal consistency and scalability of the data is required.

## Introduction

Key policy drivers worldwide focus on the need to optimise the role of patients in the management of their care; focusing services on patient needs and preferences to facilitate choice; and providing information to help them make those choices. Over the past decade there has been an increased interest in how patients’ information needs are identified and measured reflecting these health policy initiatives ([Rutten _et al._ 2005](#rut05); [Ormandy 2010](#orm10)). Indeed without robust evidence both education and information provision for patients will be based on inferred rather than actual patient information needs ([Jenkins _et al._ 2001](#jen01)).

Quantitative measurement of information needs has employed summated and differential scales to obtain accurate, representative findings, at minimal cost and with the least measurement error ([Mesters _et al._ 2001](#mes01)). Use of such scales adds breadth to the research and targeting of large samples ([McDowell 2006](#mcd06)). Large numbers, in turn, provide an opportunity to identify information needs of distinct groups and how demographic characteristics can influence both the need and type of information ([Luker _et al._ 1995](#luk95); [Degner _et al._ 1997](#deg97); [Wallberg _et al._ 2000](#wal00); [Brownall _et al._ 2004](#bro04); [Kumar _et al._ 2004](#kum04)).

Summated scales such as the Likert scale are easy to develop and use a subject-centred approach to scale responses at different points along a continuum. One of the first summated scales to focus on information needs was the information needs styles questionnaire ([Cassileth _et al._ 1980](#cas80)). The questionnaire, generated for cancer patients in the UK, has since been modified by others within the same field ([Fallowfield _et al._ 1995](#fal95); [Meredith _et al._ 1996](#mer96); [Kumar _et al._ 2004](#kum04)). Within nephrology modified questionnaires have been used to measure the information needs of patients with chronic kidney disease ([Orsino _et al._ 2003](#ors03); [Fine _et al._ 2005](#fin05), [2007](#fin07)). Items within the developed questionnaires ([Cassileth _et al._ 1980](#cas80); [Fallowfield _et al._ 1995](#fal95); [Meredith _et al._ 1996](#mer96); [Fine _et al._ 2005](#fin05)) have face-validity, but criterion-related validity has not been tested and internal consistency scales are not provided making it difficult to assess reliability ([Pinquart and Duberstein 2004](#pin04)).

Studies, predominantly in oncology, have used differential scales to measure patients’ priorities and preferences for information ([Luker _et al._ 1995](#luk95); [Degner _et al._ 1998](#deg98); [Beaver _et al._ 1999](#bea99); [Wallberg _et al._ 2000](#wal00); [Caress _et al._ 2002](#car02); [Brownall _et al._ 2004](#bro04); [Beaver and Booth 2007](#bea07)). Degner _et al._ ([1998](#deg98)) were one of the first to develop and apply Thurstone’s paired comparisons approach ([Thurstone 1974](#thu74)). Using a set of items that possess some attribute in varying degrees an individual makes a preference judgement regarding the importance of an item when compared with another. The approach allows not only for the items to be ranked in terms of importance but for distance between items to be measured. This approach was piloted in the UK and Canada with breast cancer patients ([Luker _et al._ 1995](#luk95), [1996](#luk95); [Bilodeau and Degner 1996](#bil96); [Degner _et al._ 1997](#deg97), [1998](#deg98)). These studies identified nine core information needs by way of literature review. The core needs were ordered into 36 pairs, with one item compared against another and respondents asked to choose the most important in each pairing.

Evidence from information needs research with chronic kidney disease patients is limited; no study to date has tested and applied differential measurement within this context; yet the advantages of a paired comparison approach, not only to rank information items but also to measure the distance between items giving a clearer idea of the significance of certain information needs over others is confirmed by the work of Degner _et al._ ([1998](#deg98)). Indeed the ceiling effect whereby patients identify wanting to know everything within a summated scale is avoided.

The aim of this paper is to evaluate an information needs questionnaire, which uses a differential scale, Thurstone’s paired comparison approach, to measure the information needs of patients with chronic kidney disease. The paper presents the findings of a two phased cross-sectional survey study reporting the sequential development and testing of the questionnaire.

## Methods

In a study carried out in 2006-2007, information needs items were identified in the first phase using semi-structured patient interviews. Patients were identified from a clinical network in the Northwest of England which encompassed: hospital-based haemodialysis; home-based haemodialysis; peritoneal dialysis (use of an abdominal catheter); transplanted and a pre-dialysis patient population. Twenty interviews were carried out using a purposive sampling frame targeting patients of different ages, gender and representative of different treatment modalities (Table 1). Ethical approval for the study was obtained from the Local Research Ethics Committee, University of Salford, and Trust Research and Development Committee.

<table><caption>Table 1: Phase one sample characteristics</caption>

<tbody>

<tr>

<th>Sex</th>

<th>(N)</th>

<th>Age (yrs)</th>

<th>Age Groups</th>

<th colspan="2">(N)</th>

</tr>

<tr>

<td>Male</td>

<td>11</td>

<td>Mean 52.55</td>

<td>18 - <40</td>

<td colspan="2">4</td>

</tr>

<tr>

<td>Female</td>

<td>9</td>

<td>Median 48.50</td>

<td>40 - &lt;60</td>

<td colspan="2">9</td>

</tr>

<tr>

<td colspan="2"> </td>

<td>Range 29 - 81</td>

<td>Over 60</td>

<td colspan="2">7</td>

</tr>

<tr>

<th>Modality group</th>

<th>(N)</th>

<th>Time on renal replacement therapy</th>

<th>(N)</th>

<th>Ethnic group</th>

<th>(N)</th>

</tr>

<tr>

<td>Pre-dialysis</td>

<td>5</td>

<td>Less than 1 year</td>

<td>6</td>

<td>White</td>

<td>20</td>

</tr>

<tr>

<td>Hospital haemodialysis</td>

<td>8</td>

<td>1 year to less than 5 years</td>

<td>6</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>Peritoneal dialysis</td>

<td>7</td>

<td>More than 5 years</td>

<td>3</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>No experience</td>

<td>5</td>

<td colspan="2"> </td>

<td colspan="2"> </td>

</tr>

<tr>

<th>Cause of chronic kidney disease</th>

<th>(N)</th>

<th colspan="2">Socio-economic group</th>

<th colspan="2">(N)</th>

</tr>

<tr>

<td>Glomerulonephritis or sclerosis (I)</td>

<td>3</td>

<td colspan="2">Professional</td>

<td colspan="2">8</td>

</tr>

<tr>

<td>Pyelonephritis (II)</td>

<td>4</td>

<td colspan="2">Associate professional and technical</td>

<td colspan="2">2</td>

</tr>

<tr>

<td>Polycystic kidneys (adult) (III)</td>

<td>2</td>

<td colspan="2">Administrative and secretarial</td>

<td colspan="2">4</td>

</tr>

<tr>

<td>Diabetes (VI)</td>

<td>3</td>

<td colspan="2">Skilled trade</td>

<td colspan="2">2</td>

</tr>

<tr>

<td>Miscellaneous (VII)</td>

<td>7</td>

<td colspan="2">Process, plant and machine operatives</td>

<td colspan="2">2</td>

</tr>

<tr>

<td>Unknown (VIII)</td>

<td>1</td>

<td colspan="2">Elementary occupations</td>

<td colspan="2">1</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td colspan="2">Unclassified (in education or never worked)</td>

<td colspan="2">1</td>

</tr>

</tbody>

</table>

Core information items were elicited using a combination of content and thematic analyses ([Miles and Huberman 1994](#mil94)). After sixteen interviews thirty-one broad themes were identified, a further four interviews generated no new themes, thus data collection was considered saturated ([Straus and Corbin 1998](#str98)). Independent analysis of a sub-set of five interview transcripts confirmed the themes.

Four fundamental analytical concepts, direct (expressed need), indirect (unexpressed need), coincidental and information deficit underpinned identification and extraction of information needs clarified by descriptor statements ([Williamson 1998](#wil98); [Nicholas 2000](#nic00); [Shenton and Dixon 2004](#she04)). Phrases containing multiple meanings were grouped under each pertinent theme. The initial thirty-one theme framework was merged into twelve core themes with forty-five sub-themes (Table 2).

<table><caption>Table 2: Information need themes and sub-themes</caption>

<tbody>

<tr>

<td>

**Theme 1**: Chronic kidney disease, progression of the disease, what why when not working, what to expect in the future

– Not too much information – too soon  
– Cause of kidney disease  
– What the kidneys actually do?  
– What to expect - what will happen?  
– Prognosis and future</td>

<td>

**Theme 2**: Physical symptoms as a result of renal replacement therapy and disease, what to expect and information, altered body image or sexual health

– Physical symptoms – side effects from renal replacement therapy or disease – what to expect  
– What to do if experiencing symptoms  
– Sexual health  
– Altered body image</td>

</tr>

<tr>

<td>

**Theme 3**: Renal replacement therapy (options, advantages and disadvantages of different treatments, practicalities, access, shifts, transplantation issues)

– Different treatment options for dialysis  
– What does the treatment involve – how effective?  
– When will I start?  
– Transplant options</td>

<td>

**Theme 4**: Practical aspects of renal replacement therapy  
– Practicalities of having renal replacement therapy

– How to do it?  
– Amount of stock  
– Transport issues (hospital haemodialysis)  
– Access for dialysis (fistula/Tenchkoff catheter)  
– Changes in treatment regime  
– Dialysis long-term  
– Listed and waiting for a transplant</td>

</tr>

<tr>

<td>

**Theme 5**: Complications and side effects of renal replacement therapy and disease, what to expect and information

– Don’t want to know possible complications  
– What to do if experiencing complication; how to recognise a complication; what to expect?  
– How to avoid complications?  
– Chance of getting a complication</td>

<td>

**Theme 6**: Medication information and possible side effects  
– Side effects of medication

– Why am I on this medication; what is it for?</td>

</tr>

<tr>

<td>

**Theme 7**: Family and lifestyle issues and information

– Impact of renal replacement therapy and chronic kidney disease on your lifestyle  
– Fitting dialysis round your life  
– Holidays and travel</td>

<td>

**Theme 8**: Work and financial related issues and information

– Able to continue working  
– Fitting dialysis round work  
– Impact on ability to work, career progression and self-esteem  
– Financial implications  
</td>

</tr>

<tr>

<td>

**Theme 9**: Diet and fluid restrictions, what and why

– Diet and fluid restriction information for different renal replacement therapy  
– Fitting the diet restrictions alongside your lifestyle</td>

<td>

**Theme 10**: Tests, investigations and blood results

– What should my blood results be; what can I do about it?</td>

</tr>

<tr>

<td>

**Theme 11**: Psychological issues, coping, feeling down and fed up  
– How to cope, normality, staying positive

– Peer comparison  
– Adapting to the shock of needing dialysis  
– Threat to survival  
– Denial: deal with it when it happens  
– Feeling depressed; discussing emotions</td>

<td>

**Theme 12**: Other patient experiences; talking to other patients  
– Other patients experiences

– Opportunity to talk to other patients</td>

</tr>

</tbody>

</table>

The thematic information items generated were compared and contrasted with existing research evidence ([Ormandy 2008](#orm08)). Using an expert group (clinicians, researchers and chronic kidney disease patient) data items were merged and reduced. Nine core information need themes emerged. New theme labels were created and agreed, ensuring language and meaning of the theme remained transparent.

1.  Information about what is chronic (long-term) kidney disease, what is the cause, how will it progress, what is the future.
2.  Information about how the kidney disease may affect me, physically or in other ways, how to recognise symptoms and what to expect.
3.  Information about the different treatment options, the advantages and disadvantages of each treatment, what the different treatments look like (such as machines etc) (Haemodialysis, peritoneal dialysis, transplant, automated peritoneal dialysis).
4.  Information about the practical issues of starting or changing treatment, what will happen to me and what can I expect (such as having a fistula, or peritoneal catheter, the frequency and length of time of treatment sessions or exchanges, fluid restrictions, base weight, ordering stock, using different strength bags, to up to date information on treatment changes).
5.  Information about what complications or side effects may occur as a result of the treatment or medication I’m taking.
6.  Information about ways in which I can manage and influence my own condition such as food restrictions, medication, how to keep my blood tests results stable or improve them.
7.  Information about the ways in which the kidney disease and the treatment may affect my daily life, social activities, work opportunities and financial situation (benefits and allowances available).
8.  Information from other patients about what it can be like living with chronic kidney disease and receiving regular treatment (such as practical tips on what I can do).
9.  Information about how to cope with and adjust to chronic kidney disease and who can provide support if I need it.

A summary of the nine core items (above) was sent to all participants for verification to establish content and face validity. Based on their feedback the items were re-worded and taken forward as the content of the paired comparison component of the questionnaire, within which the optimal method of pairing the nine items was determined using Ross’s matrix ([Ross 1974](#ros74)) to prevent presentational bias. The number of pairs of items was determined by the formula [n (n-1)/2] (n=number of core items) ([Degner _et al._ 1998](#deg98)).

In phase two the questionniare was used in interviews with patients identified from the same managed clinical network. Of the 386 patients identified, all were invited to take part in the study and eighty-nine were recruited (23%) (Table 3). The sample was representative of the wider population. No statistically significant differences were found between age (p=0.131), treatment mode (p=0.502), and time on renal replacement therapy (p=0.885). The proportions of male (59.6%) patients were comparable to the population (male 57.5%).

<table>

<tbody>

<tr>

<th>Sex</th>

<th>N (%)</th>

<th>Age (yrs)</th>

<th>Age groups</th>

<th colspan="2">N (%)</th>

</tr>

<tr>

<td>Male</td>

<td>53 (59.6%)</td>

<td>Mean 56.67</td>

<td>18 - under 40</td>

<td colspan="2">10 (11.2%)</td>

</tr>

<tr>

<td>Female</td>

<td>36 (40.4%)</td>

<td>Median 59.00</td>

<td>40 - under 60</td>

<td colspan="2">39 (43.8%)</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Range 25 - 83</td>

<td>Over 60</td>

<td colspan="2">40 (44.9%)</td>

</tr>

<tr>

<th>Treatment mode</th>

<th>N (%)</th>

<th>Time on renal replacement therapy</th>

<th>(N)</th>

<th>Time since diagnosis</th>

<th>(N)</th>

</tr>

<tr>

<td>Pre-dialysis</td>

<td>23 (25.8%)</td>

<td>Less than 1 year</td>

<td>16</td>

<td>Less than 2 years</td>

<td>21</td>

</tr>

<tr>

<td>Hospital dialysis</td>

<td>38 (42.7%)</td>

<td>1 - less than 2 years</td>

<td>19</td>

<td>2 - 10 years</td>

<td>27</td>

</tr>

<tr>

<td>Peritoneal dialysis</td>

<td>28 (31.5%)</td>

<td>2 - 5 years</td>

<td>17</td>

<td>10 - less than 20 years</td>

<td>28</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>More than 5 years</td>

<td>14</td>

<td>More than 20 years</td>

<td>13</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>No experience of renal replacement therapy</td>

<td>23</td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

<table><caption>Table 3: Phase two sample characteristics</caption>

<tbody>

<tr>

<th>Ethnic group</th>

<th>(N)</th>

<th>Co-morbid conditions</th>

<th>(N)</th>

</tr>

<tr>

<td>White</td>

<td>83</td>

<td>No</td>

<td>45</td>

</tr>

<tr>

<td>Black Caribbean</td>

<td>1</td>

<td>Yes</td>

<td>44</td>

</tr>

<tr>

<td>Pakistani</td>

<td>2</td>

<td>

**Groups**:</td>

<td> </td>

</tr>

<tr>

<td>Indian</td>

<td>1</td>

<td>1 – Diseases causing chronic kidney disease</td>

<td>11</td>

</tr>

<tr>

<td>Chinese</td>

<td>1</td>

<td>2 – Diseases unrelated to chronic kidney disease</td>

<td>14</td>

</tr>

<tr>

<td>Not disclosed</td>

<td>1</td>

<td>3 - Cardiovascular disease</td>

<td>24</td>

</tr>

<tr>

<th>Current situation</th>

<th>(N)</th>

<th>Cause of chronic kidney disease</th>

<th>(N)</th>

</tr>

<tr>

<td>Full-time employment</td>

<td>14</td>

<td>Glomerulonephritis or sclerosis (I)</td>

<td>8</td>

</tr>

<tr>

<td>Part-time employment</td>

<td>8</td>

<td>Pyelonephritis (II)</td>

<td>11</td>

</tr>

<tr>

<td>Unable to work due to ill health</td>

<td>27</td>

<td>Polycystic kidneys (adult) (III)</td>

<td>9</td>

</tr>

<tr>

<td>Unemployed</td>

<td>4</td>

<td>Hypertension (IV)</td>

<td>9</td>

</tr>

<tr>

<td>Full-time education</td>

<td>1</td>

<td>Renal vascular disease (V)</td>

<td>0</td>

</tr>

<tr>

<td>Retired</td>

<td>35</td>

<td>Diabetes (VI)</td>

<td>14</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Miscellaneous (VII)</td>

<td>20</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Unknown (VIII)</td>

<td>18</td>

</tr>

<tr>

<th>Education qualification</th>

<th>(N)</th>

<th>Socio-economic group</th>

<th>(N)</th>

</tr>

<tr>

<td>No formal qualifications or secondary education only</td>

<td>28</td>

<td>Managers and senior officials</td>

<td>10</td>

</tr>

<tr>

<td>Pre-University, technical and skilled trade qualifications</td>

<td>20</td>

<td>Professional</td>

<td>18</td>

</tr>

<tr>

<td>College or university first or higher degree; professional qualification</td>

<td>36</td>

<td>Associate professional and technical</td>

<td>15</td>

</tr>

<tr>

<td>Other</td>

<td>5</td>

<td>Administrative and secretarial</td>

<td>15</td>

</tr>

<tr>

<td rowspan="6"> </td>

<td rowspan="6"> </td>

<td>Skilled trade</td>

<td>11</td>

</tr>

<tr>

<td>Personal service</td>

<td>2</td>

</tr>

<tr>

<td>Sales and customer service operators</td>

<td>1</td>

</tr>

<tr>

<td>Process, plant and machine operatives</td>

<td>9</td>

</tr>

<tr>

<td>Elementary occupations</td>

<td>4</td>

</tr>

<tr>

<td>Unclassified (in education or never worked)</td>

<td>4</td>

</tr>

</tbody>

</table>

Within the interviews the nine core items were explained to clarify understanding and patients identified their current top-priority information need item. For the paired items, patients were asked if they could have information on only one of the two items which item s/he would choose. Other pairs on the same page were hidden to prevent distraction and to focus selection. On completion of the paired comparisons each patient was asked if the information needs identified were relevant to them and whether important information needs had been omitted. Duration of the face-to-face interviews was 45-60 minutes, with the setting determined by the patient: home, hospital, or workplace.

Demographic data were collected and ethnicity coded using the UK Renal Registry taxonomy ([Ansell _et al._ 2007](#ans07)). Current occupations were classified using the Standard Occupation Classification ([2000](#uk00)). Patients not working were classified using previous occupation.

A consecutive sample of the first ten patients was used as a pilot. Patient feedback on the questionnaire indicated items were clear, relevant and easy to understand and no revisions were made to the instrument.

Analysis was carried out using the Statistical Package for the Social Sciences (SPSS) and the Statistical Analysis Software package, using Thurstone’s paired comparison analysis ([Sloan _et al._ 1994](#slo94)). The analysis is based on Case V and III of Thurstone’s _law of comparative judgment_. The difference between the two cases being that; Case V stringently assumes that there is no correlation among different rankings an individual gives, whereas the more lenient Case III allows for and estimates differences among individual item variability. The basic underlying assumption of the _law_ is that when two stimuli are presented together they could be ranked in terms of some attribute, the attribute in this study being perceived importance ([McIver and Carmines 1981](#mci81)). Each item will vary in terms of the attribute when investigated, although an individual may vary in their judgment of an item from one instance to the next, but overall there will be a frequent occurring response ([Luker _et al._ 1995](#luk95)). Agreement between ratings of core information items was measured using Kendall’s coefficient of agreement for paired comparisons ([Edwards 1974](#edw74)). Kendall’s coefficient of consistency measured consistency and logic in patients’ answers. Logical comparisons of items were determined when an individual preferred item A over item B, then item B over item C, and then logically chose item A over item C. When item C was selected over item A then an inconsistent comparison had been made, referred to as a circular triad (a mismatch of the comparative judgement between items) ([Edwards 1974](#edw74)). Sloan _et al._ ([1994](#slo94)) suggest a maximum of thirty circular triads before individuals are considered inconsistent in their ratings.

The Mosteller χ<sup>2</sup> test of internal consistency, a goodness of fit test, assessed how well the data fit the underlying assumptions of Thurstone’s scaling Case V and Case III models was calculated. Gulliksen and Tukey’s (R2) measure of reliability that calculated the scalability of the data, the extent to which the Thurstone scale scores account for the variability of the individuals’ responses. The higher the R2 score the more scalable the data ([Sloan _et al._ 1994](#slo94)). Descriptive statistics were used to analyse rank ordering of items and satisfaction with information giving. Independent-samples t-tests and one-way analysis of variance were used to compare the mean scale scores for the core items by subgroups on each of the demographic variables in turn.

In addition the _averaged preferred proportions_ test was used to compare sub-groups and their comparative judgements. When significant differences are identified between the test scale values of items it indicates that the groups selected items differently and had contrasting preferences. The Bonferroni correction was used to protect against Type I error; p-values of less than p=0.0055 indicate that the test value for each item is found to be significantly different for each group.

## Results

### Priorities and preferences

Core information needs are presented in Figure 1 in rank order based on the priorities determined for the whole sample (n=89). Scale values highlight the priority patients gave to items by rank order, the higher the value the more important this information need is compared with the others. Those with a negative scale value indicate that this item was preferred by less than 50% of the sample.

Items ranked highest comprise of information needs of recognising symptoms (item 2, scale value 0.134) and complications (item 5, scale value 0.192) and, most importantly, what they themselves can do about it (item 6, scale value 0.355). Lower ranked items were concerned with psychological rather than physical aspects, including experiences of other patients (item 8, scale value –0.44) and adapting and coping with a chronic illness (item 9, scale value –0.178). A significant difference (p<0.001) was found in the distance between the top three ranked items 6, 5 and 2, and the bottom item 8, as well as the top ranked item 6 and item 9 (second to the bottom).

<figure>

![Scale 1 - priority information needs (whole sample)](../p588fig1.png)

<figcaption>Figure 1: Scale 1 - priority information needs (whole sample).</figcaption>

</figure>

When asked to identify the most important current information need 22.5% identified item 6, receiving information to enable self-management of their condition. However, items 4 and 1 were both identified by 20.2% of respondents as the most important current information need. The least number of patients (2.2%) selected item 8, information regarding other patients’ experiences as the most important (Table 4). The current most important item (6) selected by the highest number of patients and the item (8) selected by the least number of patients corresponds with the highest and lowest ranked items determined by the paired-comparison approach.

<table><caption>Table 4: Current most important information need</caption>

<tbody>

<tr>

<th rowspan="2">Item no.</th>

<th rowspan="2">Item descriptor</th>

<th colspan="2">Patients</th>

</tr>

<tr>

<th>N</th>

<th>%</th>

</tr>

<tr>

<td>6</td>

<td>Self–management, understanding blood results, tests, diet and medication to improve condition</td>

<td>20</td>

<td>22.5</td>

</tr>

<tr>

<td>4</td>

<td>Practical issues for all types of renal replacement therapy</td>

<td>18</td>

<td>20.2</td>

</tr>

<tr>

<td>1</td>

<td>What is the cause of chronic kidney disease, progression, future</td>

<td>18</td>

<td>20.2</td>

</tr>

<tr>

<td>3</td>

<td>Different treatment options, the advantages and disadvantages</td>

<td>10</td>

<td>11.2</td>

</tr>

<tr>

<td>2</td>

<td>Physical effects of chronic kidney disease, recognise symptoms, what to expect</td>

<td>7</td>

<td>7.9</td>

</tr>

<tr>

<td>5</td>

<td>Complications or side effects from treatment or medication</td>

<td>6</td>

<td>6.7</td>

</tr>

<tr>

<td>7</td>

<td>Affects on daily life, social activities, work and finances</td>

<td>4</td>

<td>4.5</td>

</tr>

<tr>

<td>9</td>

<td>How to cope and adjust, who can provide support</td>

<td>4</td>

<td>4.5</td>

</tr>

<tr>

<td>8</td>

<td>Other patients' experiences of chronic kidney disease and treatment</td>

<td>2</td>

<td>2.2</td>

</tr>

<tr>

<td colspan="2">Total</td>

<td>89</td>

<td>100</td>

</tr>

</tbody>

</table>

### Questionnaire reliability, validity and fit

Kendall’s coefficient of agreement (Kendall's u) (0.06, p=<0.001) indicated agreement amongst participants. None of the participants had >30 circular triads (range 28-0, mean 9.29 (SD 7.045), median 7.00) therefore it was appropriate to conclude participants demonstrated consistency in their responses. Gulliksen and Tukey’s R2=0.6175, indicated an acceptable degree of reliability with just under two thirds of variance accounted for by scale scores.

The Mosteller chi-squared test of internal consistency determines how well the data fits the underlying assumptions of the Thurstone scaling Case V and/or Case III statistical model (Sloan _et al._ 1994). For the data to fit the Case V model the chi-squared and Mosteller p-value are used. A non-significant result indicates the scale values fit the observed data. In this study of eighty-nine participants, the χ<sup>2</sup>=52.21 (with 28.00 degrees of freedom) and p= <0.05, produced a significant result. This indicated that the expected scale values were not a particularly good fit with the model, despite the Gulliksen and Tukey’s measure indicated an acceptable degree of reliability.

Comparable results were observed when the data was tested for goodness of fit against the Case III scaling model. Although the fit was marginally better the χ<sup>2</sup>=49.49 (with 20.00 degrees of freedom) and p=<0.05, produced a significant result, indicative that the expected scale values were not a good fit with this particular model either. The Gulliksen and Tukeys measure of reliability (R2) was higher (R2=0.6352) for the Case III model, which again indicated an acceptable degree of reliability with again just less than two thirds of variance accounted for by scale scores.

The lack of fit with both scale models could indicate the sample size is not large enough to overcome individual inconsistencies or more likely that confusion existed amongst individuals in making the comparisons. To explore this further a sub-set of individuals who had circular triads of >16 (p=<0.90) and were thus considered to be less consistent in their responses were excluded from the analysis (n=16). The result of which then produced a lower Mosteller χ<sup>2</sup>=31.791 and non-significant p-value (p=0.283) indicative of improved cohesiveness, a better fit between the data and the model.

Similar results were found within the sub-group analysis where the data, for the majority of sub-groups, fit the model with smaller sample sizes producing non-significant results (p=<0.05). Those sub-groups where the model did not fit as well (groups such as males, hospital dialysis group, those with lower educational qualifications, and those with no co-morbidity) contained participants with a higher number of circular triads (≥16) (Table 5).

<table><caption>Table 5: Overview of statistical results (Case V)</caption>

<tbody>

<tr>

<th rowspan="2">Sample group</th>

<th rowspan="2">Mosteller χ<sup>2</sup></th>

<th rowspan="2">Mosteller  
p-value</th>

<th rowspan="2">Kendall’s u</th>

<th rowspan="2">Kendall’s  
p-value</th>

<th colspan="3">Circular triads</th>

<th rowspan="2">R2</th>

</tr>

<tr>

<th>Mean</th>

<th>Median</th>

<th>SD</th>

</tr>

<tr>

<td>Whole sample (n=89)</td>

<td>52.21</td>

<td>

**0.0036**</td>

<td>0.064</td>

<td>

**0.00**</td>

<td>9.29</td>

<td>7.00</td>

<td>7.04</td>

<td>0.61</td>

</tr>

<tr>

<td>Sample with circular triads <16 (n=73)</td>

<td>31.79</td>

<td>0.283</td>

<td>0.078</td>

<td>

**0.00**</td>

<td>6.80</td>

<td>6.00</td>

<td>4.77</td>

<td>0.75</td>

</tr>

<tr>

<td>Males (n=53)</td>

<td>44.15</td>

<td>

_0.027_</td>

<td>0.062</td>

<td>

**0.00**</td>

<td>9.75</td>

<td>9.00</td>

<td>6.76</td>

<td>0.52</td>

</tr>

<tr>

<td>Females (n=36)</td>

<td>25.79</td>

<td>0.585</td>

<td>0.054</td>

<td>

**0.00**</td>

<td>8.61</td>

<td>6.00</td>

<td>7.49</td>

<td>0.57</td>

</tr>

<tr>

<td>Under 50 yrs (n=28)</td>

<td>35.69</td>

<td>0.151</td>

<td>0.031</td>

<td>

**0.00**</td>

<td>9.92</td>

<td>6.50</td>

<td>8.74</td>

<td>0.24</td>

</tr>

<tr>

<td>Over 50 to under 60 (n=21)</td>

<td>20.13</td>

<td>0.859</td>

<td>0.015</td>

<td>0.97</td>

<td>8.61</td>

<td>6.00</td>

<td>7.34</td>

<td>0.34</td>

</tr>

<tr>

<td>Over 60 years (n=40)</td>

<td>39.44</td>

<td>0.074</td>

<td>0.161</td>

<td>

**0.00**</td>

<td>9.20</td>

<td>9.00</td>

<td>5.56</td>

<td>0.74</td>

</tr>

<tr>

<td>Haemodialysis (hospital-based) (n=38)</td>

<td>38.12</td>

<td>0.096</td>

<td>0.075</td>

<td>

**0.00**</td>

<td>10.63</td>

<td>10.00</td>

<td>8.15</td>

<td>0.50</td>

</tr>

<tr>

<td>Peritoneal dialysis (n=28)</td>

<td>36.72</td>

<td>0.124</td>

<td>0.089</td>

<td>

**0.00**</td>

<td>6.82</td>

<td>6.00</td>

<td>5.73</td>

<td>0.53</td>

</tr>

<tr>

<td>Pre-dialysis (stage 5) (n=25)</td>

<td>16.50</td>

<td>0.957</td>

<td>0.04</td>

<td>

**0.00**</td>

<td>10.12</td>

<td>11.00</td>

<td>6.12</td>

<td>0.60</td>

</tr>

<tr>

<td>Higher educated (n=36)</td>

<td>28.58</td>

<td>0.434</td>

<td>0.065</td>

<td>

**0.00**</td>

<td>7.80</td>

<td>6.50</td>

<td>6.37</td>

<td>0.59</td>

</tr>

<tr>

<td>Lower educated (n=28)</td>

<td>51.82</td>

<td>

**0.004**</td>

<td>0.106</td>

<td>

**0.00**</td>

<td>10.75</td>

<td>8.00</td>

<td>8.68</td>

<td>0.36</td>

</tr>

<tr>

<td>Employed (n=22)</td>

<td>9.31</td>

<td>0.999</td>

<td>0.036</td>

<td>

**0.00**</td>

<td>5.90</td>

<td>4.00</td>

<td>5.22</td>

<td>0.74</td>

</tr>

<tr>

<td>Unable to work - ill health (n=27)</td>

<td>38.59</td>

<td>0.087</td>

<td>0.031</td>

<td>

**0.00**</td>

<td>11.03</td>

<td>11.00</td>

<td>8.82</td>

<td>0.19</td>

</tr>

<tr>

<td>Retired (n=35)</td>

<td>34.72</td>

<td>0.178</td>

<td>0.188</td>

<td>

**0.00**</td>

<td>10.11</td>

<td>11.00</td>

<td>5.99</td>

<td>0.79</td>

</tr>

<tr>

<td>No co-morbidity (n=45)</td>

<td>48.66</td>

<td>

**0.009**</td>

<td>0.064</td>

<td>

**0.00**</td>

<td>9.82</td>

<td>7.00</td>

<td>8.12</td>

<td>0.44</td>

</tr>

<tr>

<td>Co-morbid condition (n=44)</td>

<td>26.21</td>

<td>0.562</td>

<td>0.064</td>

<td>

**0.00**</td>

<td>8.75</td>

<td>7.50</td>

<td>5.78</td>

<td>0.64</td>

</tr>

<tr>

<td>Less than 2 years receiving renal replacement therapy (n=35)</td>

<td>26.95</td>

<td>0.521</td>

<td>0.061</td>

<td>

**0.00**</td>

<td>8.06</td>

<td>7.00</td>

<td>6.23</td>

<td>0.57</td>

</tr>

<tr>

<td>More than 2 years receiving renal replacement therapy (n=31)</td>

<td>40.12</td>

<td>0.065</td>

<td>0.084</td>

<td>

**0.00**</td>

<td>10.19</td>

<td>9.00</td>

<td>8.49</td>

<td>0.49</td>

</tr>

<tr>

<td>Less than 2 years since diagnosis (n=21)</td>

<td>30.56</td>

<td>0.337</td>

<td>0.038</td>

<td>

**0.00**</td>

<td>9.48</td>

<td>9.00</td>

<td>7.03</td>

<td>0.29</td>

</tr>

<tr>

<td>2-10 years since diagnosis (n=27)</td>

<td>27.82</td>

<td>0.474</td>

<td>0.072</td>

<td>

**0.00**</td>

<td>10.77</td>

<td>11.0</td>

<td>7.95</td>

<td>0.53</td>

</tr>

<tr>

<td>More than 10 years since diagnosis (n=41)</td>

<td>30.12</td>

<td>0.358</td>

<td>0.074</td>

<td>

**0.00**</td>

<td>8.21</td>

<td>6.00</td>

<td>6.39</td>

<td>0.64</td>

</tr>

<tr>

<td colspan="9">

**Values in bold** - significant at 1% level; _Values in italic_ - significant at 5% level</td>

</tr>

</tbody>

</table>

### Sub-group analysis

There were subtle differences in the order of scale items between demographic groups (Table 6). Closer scrutiny using the averaged preferred proportions test indicated no significant differences between information need priorities by gender, time on therapy, education level or co-morbid condition. However, significant differences were observed between; age groups, treatment, time since diagnosis, and current work situation.

<table><caption>Table 6: Demographic differences in Thurstone (Case V) scale profiles</caption>

<tbody>

<tr>

<th rowspan="2">Group</th>

<th colspan="9">Scale items</th>

</tr>

<tr>

<th colspan="4">High rank</th>

<th colspan="5">Low rank</th>

</tr>

<tr>

<td rowspan="2">Whole sample  
(N=89)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**4**</td>

<td>

**7**</td>

<td>

**1**</td>

<td>

**3**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.35</td>

<td>0.19</td>

<td>0.13</td>

<td>0.04</td>

<td>0.02</td>

<td>-0.02</td>

<td>-0.12</td>

<td>-0.17</td>

<td>-0.44</td>

</tr>

<tr>

<th colspan="10">Sex</th>

</tr>

<tr>

<td rowspan="2">Male (N=53)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**4**</td>

<td>

**1**</td>

<td>

**7**</td>

<td>

**3**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.35</td>

<td>0.19</td>

<td>0.11</td>

<td>0.04</td>

<td>0.02</td>

<td>0.02</td>

<td>-0.13</td>

<td>-0.16</td>

<td>-0.44</td>

</tr>

<tr>

<td rowspan="2">Female (N=36)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**4**</td>

<td>

**7**</td>

<td>

**1**</td>

<td>

**3**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.35</td>

<td>0.20</td>

<td>0.17</td>

<td>0.05</td>

<td>0.02</td>

<td>-0.07</td>

<td>-0.10</td>

<td>-0.18</td>

<td>-0.44</td>

</tr>

<tr>

<th colspan="10">Age</th>

</tr>

<tr>

<td rowspan="2">More than 60 years (N=40)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**4**</td>

<td>

**1**</td>

<td>

**3**</td>

<td>

**7**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.55</td>

<td>0.27</td>

<td>0.19</td>

<td>0.17</td>

<td>-0.11</td>

<td>-0.04</td>

<td>-0.06</td>

<td>-0.11</td>

<td>-0.95</td>

</tr>

<tr>

<td rowspan="2">Over 50- under 60 years (N=21)</td>

<td>

**1**</td>

<td>

**5**</td>

<td>

**6**</td>

<td>

**2**</td>

<td>

**7**</td>

<td>

**8**</td>

<td>

**4**</td>

<td>

**3**</td>

<td>

**9**</td>

</tr>

<tr>

<td>0.25</td>

<td>0.19</td>

<td>0.18</td>

<td>0.07</td>

<td>-0.02</td>

<td>-0.12</td>

<td>-0.12</td>

<td>-0.21</td>

<td>-0.23</td>

</tr>

<tr>

<td rowspan="2">Under 50 years (N=28)</td>

<td>

**6**</td>

<td>

**7**</td>

<td>

**2**</td>

<td>

**5**</td>

<td>

**4**</td>

<td>

**8**</td>

<td>

**3**</td>

<td>

**9**</td>

<td>

**1**</td>

</tr>

<tr>

<td>0.24</td>

<td>0.16</td>

<td>0.12 5</td>

<td>0.11</td>

<td>0.02</td>

<td>-0.11</td>

<td>-0.16</td>

<td>-0.17</td>

<td>-0.20</td>

</tr>

<tr>

<th colspan="10">Treatment mode</th>

</tr>

<tr>

<td rowspan="2">Pre-dialysis (N=23)</td>

<td>

**6**</td>

<td>

**4**</td>

<td>

**5**</td>

<td>

**7**</td>

<td>

**2**</td>

<td>

**3**</td>

<td>

**8**</td>

<td>

**9**</td>

<td>

**1**</td>

</tr>

<tr>

<td>0.43</td>

<td>0.30</td>

<td>0.07</td>

<td>0.06</td>

<td>-0.01</td>

<td>-0.03</td>

<td>-0.27</td>

<td>-0.27</td>

<td>-0.28</td>

</tr>

<tr>

<td rowspan="2">Hospital dialysis (N=38)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**1**</td>

<td>

**7**</td>

<td>

**4**</td>

<td>

**3**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.38</td>

<td>0.19</td>

<td>0.13</td>

<td>0.08</td>

<td>0.06</td>

<td>0.04</td>

<td>-0.15</td>

<td>-0.18</td>

<td>-0.54</td>

</tr>

<tr>

<td rowspan="2">Peritoneal dialysis (N=28)</td>

<td>

**5**</td>

<td>

**6**</td>

<td>

**2**</td>

<td>

**1**</td>

<td>

**7**</td>

<td>

**9**</td>

<td>

**3**</td>

<td>

**4**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.33</td>

<td>0.27</td>

<td>0.26</td>

<td>0.07</td>

<td>-0.06</td>

<td>-0.07</td>

<td>-0.15</td>

<td>-0.17</td>

<td>-0.47</td>

</tr>

<tr>

<th colspan="10">Time since diagnosis</th>

</tr>

<tr>

<td rowspan="2">Less than 2 years (N=21)</td>

<td>

**6**</td>

<td>

**2**</td>

<td>

**7**</td>

<td>

**1**</td>

<td>

**5**</td>

<td>

**3**</td>

<td>

**4**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.35</td>

<td>0.09</td>

<td>0.09</td>

<td>0.06</td>

<td>0.05</td>

<td>-0.05</td>

<td>-0.05</td>

<td>-0.17</td>

<td>-0.38</td>

</tr>

<tr>

<td rowspan="2">More than 2 years to under 10 years (N=27)</td>

<td>

**5**</td>

<td>

**6**</td>

<td>

**2**</td>

<td>

**3**</td>

<td>

**1**</td>

<td>

**4**</td>

<td>

**7**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.30</td>

<td>0.22</td>

<td>0.10</td>

<td>0.08</td>

<td>0.08</td>

<td>0.04</td>

<td>-0.07</td>

<td>-0.09</td>

<td>-0.65</td>

</tr>

<tr>

<td rowspan="2">More than 10 years (N=41)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**4**</td>

<td>

**7**</td>

<td>

**1**</td>

<td>

**9**</td>

<td>

**3**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.44</td>

<td>0.20</td>

<td>0.18</td>

<td>0.10</td>

<td>0.05</td>

<td>-0.12</td>

<td>-0.22</td>

<td>-0.29</td>

<td>-0.35</td>

</tr>

<tr>

<th colspan="10">Time on renal replacement therapy</th>

</tr>

<tr>

<td rowspan="2">Less than 2 years (N=35)</td>

<td>

**6**</td>

<td>

**2**</td>

<td>

**5**</td>

<td>

**1**</td>

<td>

**7**</td>

<td>

**4**</td>

<td>

**9**</td>

<td>

**3**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.35</td>

<td>0.21</td>

<td>0.13</td>

<td>0.10</td>

<td>0.03</td>

<td>-0.06</td>

<td>-0.08</td>

<td>-0.17</td>

<td>-0.49</td>

</tr>

<tr>

<td rowspan="2">2 years or more (N=31)</td>

<td>

**5**</td>

<td>

**6**</td>

<td>

**2**</td>

<td>

**1**</td>

<td>

**7**</td>

<td>

**4**</td>

<td>

**3**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.39</td>

<td>0.29</td>

<td>0.19</td>

<td>0.08</td>

<td>-0.03</td>

<td>-0.05</td>

<td>-0.13</td>

<td>-0.22</td>

<td>-0.53</td>

</tr>

<tr>

<th colspan="10">Current situation</th>

</tr>

<tr>

<td rowspan="2">Employed (N=22)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**7**</td>

<td>

**2**</td>

<td>

**8**</td>

<td>

**4**</td>

<td>

**1**</td>

<td>

**9**</td>

<td>

**3**</td>

</tr>

<tr>

<td>0.40</td>

<td>0.29</td>

<td>0.09</td>

<td>0.08</td>

<td>-0.09</td>

<td>-0.10</td>

<td>-0.11</td>

<td>-0.11 3</td>

<td>-0.46</td>

</tr>

<tr>

<td rowspan="2">Unable to work (N=27)</td>

<td>

**2**</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**1**</td>

<td>

**7**</td>

<td>

**8**</td>

<td>

**4**</td>

<td>

**3**</td>

<td>

**9**</td>

</tr>

<tr>

<td>0.22</td>

<td>0.19</td>

<td>0.16</td>

<td>0.04</td>

<td>0.01</td>

<td>-0.10</td>

<td>-0.15</td>

<td>-0.16</td>

<td>-0.20</td>

</tr>

<tr>

<td rowspan="2">Retired (N=35)</td>

<td>

**6**</td>

<td>

**4**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**3**</td>

<td>

**1**</td>

<td>

**7**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.62</td>

<td>0.31</td>

<td>0.21</td>

<td>0.09</td>

<td>0.08</td>

<td>-0.03</td>

<td>-0.04</td>

<td>-0.11</td>

<td>-1.14</td>

</tr>

<tr>

<th colspan="10">Education level</th>

</tr>

<tr>

<td rowspan="2">O level or less (None) (N=28)</td>

<td>

**6**</td>

<td>

**4**</td>

<td>

**2**</td>

<td>

**5**</td>

<td>

**1**</td>

<td>

**3**</td>

<td>

**7**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.37</td>

<td>0.21</td>

<td>0.18</td>

<td>0.09</td>

<td>0.07</td>

<td>0.04</td>

<td>-0.06</td>

<td>-0.32</td>

<td>-0.57</td>

</tr>

<tr>

<td rowspan="2">Degree or higher (N=36)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**7**</td>

<td>

**2**</td>

<td>

**4**</td>

<td>

**9**</td>

<td>

**1**</td>

<td>

**3**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.31</td>

<td>0.24</td>

<td>0.14</td>

<td>0.13</td>

<td>-0.01</td>

<td>-0.06</td>

<td>-0.07</td>

<td>-0.17</td>

<td>0.51</td>

</tr>

<tr>

<th colspan="10">Co-morbidity</th>

</tr>

<tr>

<td rowspan="2">No co-morbidity (N= 45)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**7**</td>

<td>

**1**</td>

<td>

**4**</td>

<td>

**9**</td>

<td>

**3**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.36</td>

<td>0.22</td>

<td>0.11</td>

<td>0.03</td>

<td>0.01</td>

<td>-0.07</td>

<td>-0.08</td>

<td>-0.18</td>

<td>-0.41</td>

</tr>

<tr>

<td rowspan="2">Co-morbidity (N=44)</td>

<td>

**6**</td>

<td>

**5**</td>

<td>

**2**</td>

<td>

**4**</td>

<td>

**7**</td>

<td>

**1**</td>

<td>

**3**</td>

<td>

**9**</td>

<td>

**8**</td>

</tr>

<tr>

<td>0.34</td>

<td>0.17</td>

<td>0.16</td>

<td>0.15</td>

<td>0.02</td>

<td>-0.04</td>

<td>-0.06</td>

<td>-0.26</td>

<td>-0.47</td>

</tr>

</tbody>

</table>

For patients aged under fifty years of age there was a significant difference (t-test, p=<0.05) between the distance in the top-ranked item 6 (scale value 0.24) and the lowest item 1 (scale value –0.20) (Table 7, Scale 2). Information about managing their condition (item 6) and the impact upon their lifestyle (item 7) were of a greater priority to younger patients than information about the cause of kidney disease and the future (item 1). In contrast, patients between the ages of 50 and 60 years ranked item 1 (scale value 0.25) the highest. The rank order of core items by patients over 60 years of age showed a significant difference (t-test, p=<0.05) between the lowest ranked item 8 (scale value -0.95) and the distance between all the other core items. The experiences of other patients were considerably lower in priority to older patients, compared with patients in lower age groups.

Patients in the pre-dialysis group (waiting to start treatment) typically rated information about the practicalities of therapy (item 4) higher than patients in the treatment groups (hospital dialysis and peritoneal dialysis) (Scale 4) (Table 7, Scale 3). Indeed, there were significant differences recorded between the distance of item 4 (scale value 0.30) compared with the lower ranked items 1, 8 and 9 (scale values –0.28, -0.27 and –0.27 respectively), for this group of patients causing this item to stand out. Of further interest was the low scale value assigned to item 1 (information about the cause of kidney disease and the future) by the pre-dialysis group compared with the higher priority given to this information need by both the hospital dialysis and peritoneal dialysis patients. For the hospital dialysis and peritoneal groups the top three ranked items were the same. Although for the peritoneal dialysis group the highest ranked item changed from item 6 to item 5 signifying that information about complications and side effects was slightly more important.

Patients who had been diagnosed less than two years previously gave a lower scale value (-0.05) to item 4 (practicalities of different treatments) for which there was a significant difference noted between this and the top-ranked item 6 (scale value 0.35) (Table 7, Scale 4). Those patients who had been diagnosed between 2-10 years gave the experiences of other patients (item 8) such low priority (scale value –0.35) that significant differences were shown with the distances between this particular item and the other core items. In addition for patients who had been diagnosed over ten years previously, item 3 (different treatment options) was ranked lower (scale value –0.29) resulting in a significant difference between this and the top-ranked item 6 (scale value 0.44).

Although it would appear from the findings that the information needs of patients change as time progresses from diagnosis, in particular, that information about the practicalities of renal replacement therapy increases in importance, it must be viewed with caution. On closer examination of the sub-sample groups, for example, of those who had been diagnosed for less than two years, 52% were unable to work and 71% were receiving replacement therapy. These two factors most probably influence the low priority given to the information about the practicalities of therapy not necessarily the time since diagnosis.

Within the employment sub-analysis (employed, unable to work due to ill health and retired) there were significant differences with regard to the distance and importance placed on specific items (Table 7, Scale 5). Patients in full or part-time employment rated item 6 (scale value 0.40) and item 5 (scale value 0.29) as the first and second most important information needs but then rated item 7 (information regarding the impact of the disease on their lifestyle) as third highest (scale value 0.09). The top-ranked item for those patients unable to work due to ill health was item 2 (physical affects and symptoms) with a scale value of 0.22\.

<table><caption>Table 7: Sub-group analysis Scales 2-5</caption>

<tbody>

<tr>

<th colspan="3">Scale 2: Age</th>

<th colspan="3">Scale 3: Treatment group</th>

<th colspan="3">Scale 4: Time since diagnosis</th>

<th colspan="3">Scale 5: Current work situation</th>

</tr>

<tr>

<th>Under 50 (n=28)</th>

<th>50 to 60 (n=21)</th>

<th>Over 60 (n=40)</th>

<th>Pre-dialysis (n=23)</th>

<th>Hospital dialysis (n=38)</th>

<th>Peritoneal dialysis (n=28)</th>

<th>Less than two years (n=21)</th>

<th>Two to ten years (n=27)</th>

<th>Over ten years (n=41)</th>

<th>Employed (n=22)</th>

<th>Unable to work (n=27)</th>

<th>Retired (n=35)</th>

</tr>

<tr>

<td colspan="3">higher priority</td>

<td colspan="3">higher priority</td>

<td colspan="3">higher priority</td>

<td colspan="3">higher priority</td>

</tr>

<tr>

<td>

**6**  
(0.24)</td>

<td>

**1**  
(0.25)</td>

<td>

**6**  
(0.55)</td>

<td>

**6**  
(0.43)</td>

<td>

**6**  
(0.38)</td>

<td>

**5**  
(0.33)</td>

<td>

**6**  
(0.35)</td>

<td>

**5**  
(0.30)</td>

<td>

**6**  
(0.44)</td>

<td>

**6**  
(0.40)</td>

<td>

**2**  
(0.22)</td>

<td>

**6**  
(0.62)</td>

</tr>

<tr>

<td>

**7**  
(0.16)</td>

<td>

**5**  
(0.19)</td>

<td>

**5**  
(0.27)</td>

<td>

**4**  
(0.30)</td>

<td>

**5**  
(0.19)</td>

<td>

**6**  
(0.27)</td>

<td>

**2**  
(0.09)</td>

<td>

**6**  
(0.22)</td>

<td>

**5**  
(0.20)</td>

<td>

**5**  
(0.29)</td>

<td>

**6**  
(0.19)</td>

<td>

**4**  
(0.31)</td>

</tr>

<tr>

<td>

**2**  
(0.12)</td>

<td>

**6**  
(0.18)</td>

<td>

**2**  
(0.19)</td>

<td>

**5**  
(0.07)</td>

<td>

**2**  
(0.13)</td>

<td>

**2**  
(0.26)</td>

<td>

**7**  
(0.09)</td>

<td>

**2**  
(0.10)</td>

<td>

**2**  
(0.18)</td>

<td>

**7**  
(0.09)</td>

<td>

**5**  
(0.16)</td>

<td>

**5**  
(0.21)</td>

</tr>

<tr>

<td>

**5**  
(0.11)</td>

<td>

**2**  
(0.07)</td>

<td>

**4**  
(0.17)</td>

<td>

**7**  
(0.06)</td>

<td>

**1**  
(0.08)</td>

<td>

**1**  
(0.07)</td>

<td>

**1**  
(0.06)</td>

<td>

**2**  
(0.08)</td>

<td>

**4**  
(0.10)</td>

<td>

**2**  
(0.08)</td>

<td>

**1**  
(0.04)</td>

<td>

**2**  
(0.09)</td>

</tr>

<tr>

<td>

**4**  
(0.02)</td>

<td>

**7**  
(-0.02)</td>

<td>

**1**  
(-0.11)</td>

<td>

**2**  
(-0.01)</td>

<td>

**7**  
(0.06)</td>

<td>

**7**  
(-0.06)</td>

<td>

**5**  
(0.05)</td>

<td>

**1**  
(0.08)</td>

<td>

**7**  
(0.05)</td>

<td>

**8**  
(-0.09)</td>

<td>

**7**  
(0.01)</td>

<td>

**3**  
(-0.08)</td>

</tr>

<tr>

<td>

**8**  
(-0.11)</td>

<td>

**8**  
(-0.12)</td>

<td>

**3**  
(-0.04)</td>

<td>

**3**  
(-0.03)</td>

<td>

**4**  
(0.04)</td>

<td>

**9**  
(-0.07)</td>

<td>

**3**  
(-0.05)</td>

<td>

**4**  
(0.04)</td>

<td>

**1**  
(-0.12)</td>

<td>

**4**  
(-0.10)</td>

<td>

**8**  
(-0.10)</td>

<td>

**1**  
(-0.03)</td>

</tr>

<tr>

<td>

**3**  
(-0.16)</td>

<td>

**4**  
(-0.12)</td>

<td>

**7**  
(-0.06)</td>

<td>

**8**  
(-0.27)</td>

<td>

**3**  
(-0.15)</td>

<td>

**3**  
(-0.15)</td>

<td>

**4**  
(-0.05)</td>

<td>

**7**  
(-0.07)</td>

<td>

**9**  
(-0.22)</td>

<td>

**1**  
(-0.11)</td>

<td>

**4**  
(-0.15)</td>

<td>

**7**  
(-0.04)</td>

</tr>

<tr>

<td>

**9**  
(-0.17)</td>

<td>

**3**  
(-0.21)</td>

<td>

**9**  
(-0.11)</td>

<td>

**9**  
(-0.27)</td>

<td>

**9**  
(-0.18)</td>

<td>

**4**  
(-0.17)</td>

<td>

**9**  
(-0.17)</td>

<td>

**9**  
(-0.09)</td>

<td>

**3**  
(-0.29)</td>

<td>

**9**  
(-0.11)</td>

<td>

**3**  
(-0.16)</td>

<td>

**9**  
(-0.11)</td>

</tr>

<tr>

<td>

**1**  
(-0.20)</td>

<td>

**9**  
(-0.23)</td>

<td>

**8**  
(-0.95)</td>

<td>

**1**  
(-0.28)</td>

<td>

**8**  
(-0.54)</td>

<td>

**8**  
(-0.47)</td>

<td>

**8**  
(-0.38)</td>

<td>

**8**  
(-0.65)</td>

<td>

**8**  
(-0.35)</td>

<td>

**3**  
(-0.46)</td>

<td>

**9**  
(-0.20)</td>

<td>

**8**  
(-1.14)</td>

</tr>

<tr>

<td colspan="3">lower priority</td>

<td colspan="3">lower priority</td>

<td colspan="3">lower priority</td>

<td colspan="3">lower priority</td>

</tr>

</tbody>

</table>

## Implications for practice

### High priority information needs

Patients prioritised information about managing their own disease, understanding and recognising physical symptoms, complications and side effects as the most important. Gathering information that increases knowledge about the disease, through symptom management and overseeing aspects of treatment facilitates survival ([Curtin and Mapes 2001](#cur01)).

The highest ranked information need, managing their own condition through controlling their diet and/or fluid intake and to a lesser degree understanding their blood results, was important to patients because it was concerned with aspects of their care for which they have direct control. Fostering and encouraging patients’ self care skills and increasing independence is a key driver within UK national policy ([Department of Health 2004](#uk04)), particularly to free up the availability of in-centre services for those patients for which self-care is not possible. Self-management information is crucial to patients at all stages of the disease, particularly those patients who are deciding which replacement therapy to choose. Targeted education about self-care has been shown to increase the number of patients who go on to choose peritoneal dialysis, home dialysis or self-care dialysis and remain independent ([Piccoli _et al._ 2000](#pic00); [Manns _et al._ 2005](#man05)). To identify that both patients and professionals are working towards the same priorities, is not only reassuring but also increases the possibility of developing a renal service that meets the needs of both groups.

Diet and fluid restrictions, have a cosiderable impact on the quality of life of patients ([Bass _et al._ 1999](#bas99)) and information that could help them minimise the effects was considered a high priority ([Groome _et al._ 1994](#gro94); [Schatell _et al._ 2003a](#sch03a), [2003b](#sch03a); [Harwood _et al._ 2005](#har05)). Patients attending out patient clinic have blood taken to monitor their disease and treatment stability. Interpreting the blood results, what would be normal for them and what action could be taken to make improvements was important information to patients ([Coupe 1998](#cou98); [Schatell _et al._ 2003a](#sch03a)). As _Renal PatientView_ ([Renal Association 2005](#ren05)) (a system by which patients can access their electronic records including their blood results) becomes more established throughout the UK then information and education that increases their understanding and knowledge of what these mean, is crucial.

Information that enables a patient to recognise a physical symptom, complication and/or side effect and understand its cause was ranked closely together in second place. In this sample it was interesting to note the high priority given to these topics for patients who had been on renal replacement therapy for many years, signifying an unmet or continuous need for information need and/or an increase in physical complications as a result of the disease progression. In practice information about physical symptoms and possible complications must be formalised and be continued over time to prevent patients having to learn from experience in a disorganised and unsystematic manner.

### Medium priority information needs

A number of information needs clustered in the middle of the scale with minimal distance between them, indicating comparable levels of importance for the whole sample. These included the cause of chronic kidney disease and the future, practical issues regarding replacement therapy and different treatment options, the affects of the disease and treatment on coping with, and adapting to life with ,the disease.

Of interest was the importance given to the information need about what chronic kidney disease is and the cause, an important theme for those who had been on renal replacement therapy for many years and had not received appropriate information. Clarifying at the outset that the disease can be diagnosed without ever establishing the cause would help to allay these patients' concerns, and go some way to satisfying or reducing the significance of this need over time.

A key aspect of theme 7 (information about the effects on daily life, social activities, work and finances) was the affect the disease has on employment. As shown in other studies ([Whitaker and Albee 1996](#whi96); [Orsino _et al._ 2003](#ors03)), sustaining a career and continuing to work was important to those who were the family breadwinner and/or single with no family to rely on for financial support. The early provision of targeted information addressing employment issues and realistic career advice could reduce the number of patients starting therapy being forced to make a major lifestyle change, by giving up work, becoming dependent on social security benefits, managing a reduced standard of living or having unrealised life goals and low self-esteem.

### Lower priority information needs

Comments made by patients during the interviews intimated that although information on other patients’ experiences was not considered particularly important there was a degree of pertinent information to be gleaned from others’ experiences. This has been highlighted by patients in other studies ([Juhnke and Curtin 2000](#juh00)). Tweed and Ceaser ([2005](#twe05)) suggest that patients receive reassurance and experience reduced feelings of isolation when they compare themselves with others. Two patients prioritised information from other patients as their most important current information need, suggesting it cannot be dismissed.

## Discussion and conclusion

### Measuring information needs

The instrument is easy to complete and useful in identifying the information needs and priorities of a group of patients and within sub-groups. The paired comparison approach enabled the distance as well as the rank order of items to be measured which was invaluable when interpreting the data providing a deeper understanding than would have been achieved using a simple Likert scale. It was clear patients found it easier to choose between two items at a time than trying to choose one most important item from the list of nine.

The approach was, however, cumbersome given the time required to develop and ensure item accuracy. Administering the instrument using face-to-face interviews was labour intensive. Therefore, as a tool for use in clinical practice, it would be both time consuming and of limited benefit to the individual patient. Nonetheless, compiled within a condensed format (a topic guide), the nine core information needs could be used to initiate patient discussion and draw out more specific individual information needs, or used as an evidence base for deriving the content of education programmes.

Core information need items were relevant to patients and the reliability of data scalability and agreement between participants was acceptable. Overall 70.79% of patients agreed all items to be relevant; there was an acceptable degree of reliability for data scalability (R2 =0.6175); a degree of agreement found between patients (Kendall’s coefficient 0.06); and patient’s responses were consistent. Despite this, there was not a good fit between data and Case V and Case III models (Mosteller χ<sup>2</sup> =52.21, p=0.003, χ<sup>2</sup>=49.49 p=<0.001 respectively).

The lack of fit to the model is likely to have been distorted by inconsistent comparative judgements of a small number of patients. Although the numbers of circular triads, from this group of patients, did not exceed the recommended maximum they clearly influenced data reliability. What is interesting to note is that from the studies using this method, with similar numbers of circular triads only Wallberg _et al._ ([2000](#wal00)) report the results of the Mostellar χ<sup>2</sup> or Gulliksen and Tukey’s measure of scalability. If it is assumed that more than sixteen circular triads are indicative of inconsistent judgements then the data suggests that patients found it problematic to decide between particular items although when examined there was no pattern to the items involved. These inconsistencies may signify that many of the themes were personally relevant to the patient and the confusion was created when two or more were of comparable priority (clustering around the middle of the scale is indicative of this). When the inconsistent judgements were extracted, the data fitted the model. It is possible that within a larger sample the inconsistent judgements may be less prominent. Alternatively the solution may be much simpler in as much as some respondents may be unclear of their own priorities.

In phase one an information topic identified from one study Groome _et al._ ([1994](#gro94)), indicated that patients required information about the availability and quality of nursing and physician care and to a lesser degree what facilities were available. This item was not included because of a lack of corroborating evidence either the literature or the patient interviews. However, six patients suggested information about clinic visits, their consultant and service delivery were missing information needs on the questionnaire, albeit not as important as the other core items listed. The majority of these patients had been re-located to a satellite unit and, as a result, expressed concerns about their reduced contact with the medical team and inappropriate clinic visits. Concern about missing items that are important to a minority has been expressed previously ([Degner _et al._ 1998](#deg98)) and the issue cannot be ignored. Whilst in this study the missing item appears context specific, future testing of the instrument within this patient population should consider inclusion of this information need.

The importance of qualitative methods for identifying information needs cannot be underestimated. Indeed face-validity of many questionnaires using differential scales has been determined using preliminary patient interviews or focus groups to agree and confirm the content ([Luker _et al._ 1995](#luk95); [Degner _et al._ 1997](#deg97); [Fallowfield _et al._ 1995](#fal95); [Hepworth and Harrison 2004](#hep04); [Fine _et al._ 2005](#fin05); [Hylands _et al._ 2006](#hyl06)). Failure to involve patients at the outset in instrument design draws doubt on whether the tool can successfully address the needs of the patient and brings into the spotlight whether patients’ information preferences are determined by professionals and not grounded in what patients themselves consider important ([Scott and Thompson 2003](#sco03)).

The study is limited by the relatively small sample size in phase two. Only eighty-nine patients were recruited, just under half the anticipated number, which may have introduced selection bias. A contributory reason for this shortfall was problems identifying the population; the shifting status of patients in terms of treatment mode made eligibility assessment difficult. Where status could not be confirmed potential recruits were lost to the study. Despite this, only one quarter of patients contacted chose to be involved, maybe because they valued their time when not on dialysis and were not prepared to encroach on it. Hines _et al._ ([1997a](#hin97a), [1997b](#hin97b)) recruited 197 hospital-based dialysis patients by interviewing them whilst receiving therapy. Although initially this did not seem appropriate, some participants in this study preferred to meet during treatment, indicating it was a better use of their time. This additional strategy could possibly have increased the sample size and should be considered as viable method in future research.

A further weakness of the study sample was failure to recruit patients from ethnic minority groups, despite offering translation services. However, this reflected a wider problem of low prevalence rates within the local population signifying ethnic minority patients at risk of chronic kidney disease were not being identified and referred to the service ([Greater Manchester Renal Strategy Group 2008](#gre08)).

### Conclusion

In summary, the study has shown that, using the paired comparison approach , it is not only possible to identify the information needs but also measure the strengths of preferences for information of patients. Such needs exist on an individual level and, when patients are grouped, there is consensus about what is most important. However, further testing of the reliability and validity of the questionnaire is required.

## Acknowledgements

We are very grateful and would like to recognise the valuable contribution of the wider research team Dennis Crane (patient representative), Mrs Jane Macdonald, Professor Donal O’Donoghue, and Professor Ann-Louise Caress. The study received funding from the British Renal Society and we thank them for their support. The analysis could not have been performed without access to the SAS computer program developed in Canada by Sloan et al. (1994) used to produce profiles of information needs based on Thurstone’s law of comparative judgment.

## <a id="authors"></a>About the authors

Professor **Paula Ormandy** is a health service researcher, leading the research programme for long-term conditions with specific research interests in chronic kidney disease, patient information needs, self care and self-management, and the use of digital media in health. She holds a BSc (Hons) in Nursing Studies, and MSc in Practitioner Research, Manchester Metropolitan University and a PhD in Nursing from the University of Salford; she is a Registered General Nurse. Paula can be contacted at: [p.ormandy@salford.ac.uk](mailto:p.ormandy@salford.ac.uk)  
Professor **Claire Hulme** is Director of the Academic Unit of Health Economics in the Leeds Institute of Health Sciences, University of Leeds. She holds a BSc (Hons.) in Economics, MA in Higher Education Practice and Research, and PhD in Health Economics from the University of Salford. Claire's research interests are in the economic evaluation of community programmes spanning the health and social sectors and economic evaluation alongside clinical trials. She can be contacted at [c.t.hulme@leeds.ac.uk](mailto:c.t.hulme@leeds.ac.uk)

</section>

<section>

## References

<ul>
<li id="ans07">Ansell, D., Feehally, J., Feest, T.G., Tomson, C., Williams, A.J. &amp; Warwick, G. (2007).
<em><a href="http://www.webcitation.org/6ILuiKWhv">UK Renal Registry report: the tenth annual
report.</a></em> Bristol, UK: Renal Registry. Retrieved 24 July, 2013 from
http://www.renalreg.com/Reports/2007.html (Archived by WebCite&reg; at
http://www.webcitation.org/6ILuiKWhv)</li>
<li id="bas99">Bass, E.B., Jenckes, M.W., Fink, N.E., Cagney, K.A., Wu, A.W., Sadler, J.H. <em>et al.</em>
(1999). Use of focus groups to identify concerns about dialysis. <em>Medical Decision Making</em>,
<strong>19</strong>(3), 287-95.</li>
<li id="bea07">Beaver, K. &amp; Booth, K. (2007). Information needs and decision-making preferences.
Comparing findings for gynaecological, breast and colorectal cancer. <em>European Journal of Oncology
Nursing</em>, <strong>11</strong>(5), 409-416.</li>
<li id="bea99">Beaver, K., Bogg, J. &amp; Luker, K.A. (1999). Decision-making preferences and information
needs: a comparison of colorectal and breast cancer. <em>Health Expectations</em>, <strong>2</strong>(4),
266-276.</li>
<li id="bil96">Bilodeau, B. &amp; Degner, L.F. (1996). Profiles of information needs of women recently
diagnosed with cancer: a pilot study. <em>Cancer Nursing Forum</em>, <strong>23</strong>(4), 691-696.</li>
<li id="bro04">Browall, M., Carlsson, M. &amp; Horvath, G. (2004). Information needs of women with recently
diagnosed ovarian cancer – a longitudinal study. <em>European Oncology Nursing Society</em>,
<strong>8</strong>(3), 200-207.</li>
<li id="car02">Caress, A-L., Luker, K., Woodcock, A. &amp; Beaver, K. (2002). An exploratory study of
priority information needs in adult asthma patients. <em>Patient Education and Counseling</em>,
<strong>47</strong>(4), 319-327.</li>
<li id="cas80">Cassileth, B.R., Zupkis, R.V., Sutton-Smith, K. &amp; March, V. (1980). Information and
participation preferences amongst cancer patients. <em>Annals of Internal Medicine</em>,
<strong>92</strong>(6), 832-836. </li>
<li id="cou98">Coupe D. (1998). Making decisions about dialysis options: an audit of patients' views.
<em>European Dialysis Transplant Nurses Association/ European Renal Care Association Journal</em>,
<strong>24</strong>(1), 25-26, 31. </li>
<li id="cur01">Curtin, R.B. &amp; Mapes, D.L. (2001). Health care management strategies of long-term
dialysis survivors. <em>Nephrology Nursing Journal</em>, <strong>28</strong>(4), 385-92; discussion 393-4.
</li>
<li id="deg97">Degner, L.F., Kristjanson, L.J., Bowman, D., Sloan, J.A., Carriere, K.C., O’Neill, J. <em>et
al.</em> (1997). Information needs and decisional preferences in women with breast cancer. <em>Journal
of the American Medical Association</em>, <strong>277</strong>(18), 1485-1492.</li>
<li id="deg98">Degner, L.F., Davison, B.J., Sloan, J.A. &amp; Mueller, B. (1998). Development of a scale to
measure the information needs in cancer care. <em>Journal of Nursing Measurement</em>,
<strong>6</strong>(2), 137-153.</li>
<li id="edw74">Edwards, A.L. (1974). Circular triads, the coefficient of consistence, and the coefficient of
agreement. In G.M. Maranell, (Ed.) <em>Scaling: a sourcebook for behavioral scientists.</em> (pp. 98-105).
Chicago, IL: Aldine Publishing Company.</li>
<li id="fal95">Fallowfield, L., Ford, S., Lewis, S. (1995). No news is not good news: information
preferences of patients with cancer. <em>Psychological Oncology</em>, <strong>4</strong>(3), 197-202.
</li>
<li id="fin05">Fine, A., Fontaine, B., Krausher, M.M. &amp; Rich, B.R. (2005). Nephrologists should
voluntarily divulge survival data to potential dialysis patients: a questionnaire study. <em>Peritoneal
Dialysis International</em>, <strong>25</strong>(3), 269-273.</li>
<li id="fin07">Fine, A., Fontaine, B., Kraushar, M.M. &amp; Plamondon, J. (2007). Patients with chronic
kidney disease. Stages 3 and 4 demand survival information on dialysis. <em>Peritoneal Dialysis
International</em>, <strong>27</strong>(5), 589-591.</li>
<li id="gre08">Greater Manchester Renal Strategy Group (2008). <em><a
href="http://www.webcitation.org/6ILwWBD4w">Strategic framework for kidney care in Greater Manchester
2008-2013</a></em>. Oldham, UK: Greater Manchester Kidney Care Network. Retrieved 24 July, 2013 from
http://bit.ly/16lKBiK (Archived by WebCite&reg; at http://www.webcitation.org/6ILwWBD4w)</li>
<li id="gro94">Groome, P.A., Hutchinson, T.A. &amp; Tousignant, P. (1994). Content of a decision analysis
for treatment choice in end-stage renal disease: who should be consulted? <em>Medical Decision
Making</em>, <strong>14</strong>(1), 91-7.</li>
<li id="har05">Harwood, L., Locking-Cusolito, H., Spittal, J., Wilson, B. &amp; White, S. (2005). Preparing
for hemodialysis: patient stressors and responses. <em>Nephrology Nursing Journal</em>,
<strong>32</strong>(3), 295-302.</li>
<li id="hep04">Hepworth, M. &amp; Harrision, J. (2004). A survey of the information needs of people with
multiple sclerosis. <em>Health Informatics Journal,</em> <strong>10</strong>(1), 49-69.</li>
<li id="hin97a">Hines, S.C., Badzek, L. &amp; Moss, A.H. (1997). Informed consent among chronically ill
elderly. Assessing its (in)adequacy and predictors. <em>Journal of Applied Communication Research</em>,
<strong>25</strong>(3), 151-169.</li>
<li id="hin97b">Hines, S.C., Babrow, A.S., Badzek, L. &amp; Moss, A.H. (1997). Communication and problematic
integration in end-of-life decisions. Dialysis decisions among the elderly. <em>Health Communication</em>,
<strong>9</strong>(3), 199-217. </li>
<li id="hyl06">Hyland, M.E., Jones, R.C.M. &amp; Hanney, K.E. (2006). The lung information needs
questionnaire: development, preliminary validation and findings. <em>Respiratory Medicine</em>,
<strong>100</strong>(10), 1807-1816. </li>
<li id="jen01">Jenkins, V., Fallowfield, L., Saul, J. (2001). Information needs of patients with cancer:
results from a large study in UK cancer centres. <em>British Journal of Cancer</em>,
<strong>84</strong>(1), 48-51. </li>
<li id="juh00">Juhnke, J. &amp; Curtin, R.B. (2000). New study identifies ESRD patient education needs.
<em>Nephrology News &amp; Issues</em>, <strong>14</strong>(6), 38-39.</li>
<li id="kum04">Kumar, D.M., Symonds, R.P., Sundar, S., Ibrahim, K., Savelyich, B.S.P. &amp; Miller, E.
(2004). Information needs of Asian and White British cancer patients and their families in Leicestershire:
a cross-sectional survey. <em>British Journal of Cancer</em>, <strong>90</strong>(8), 1471-1478.</li>
<li id="luk95">Luker, K.A., Beaver, K., Leinster, S.J., Owens, R.G., Degner, L.F. &amp; Sloan, J.A. (1995).
The information needs of women newly diagnosed with breast cancer. <em>Journal of Advanced Nursing</em>,
<strong>22</strong>(1), 134-141.</li>
<li id="luk96">Luker, K.A., Beaver, K. Leinster, S.J. &amp; Owens, R.G. (1996). Information needs and
sources of information for women with breast cancer: a follow-up study. <em>Journal of Advanced
Nursing</em>, <strong>23</strong>(3), 487-495.</li>
<li id="mcd06">McDowell I. (2006). <em>Measuring health: a guide to rating scales and questionnaires.</em>
Oxford: Oxford University Press. </li>
<li id="mci81">McIver, J.P. &amp; Carmines, E.G. (1981). <em>Unidimensional scaling</em>. Thousand Oaks, CA:
Sage Publications Inc.</li>
<li id="man05">Manns, B.J., Taub K., Vanderstraeten, C., Jones, H., Mills, C., Visser, M. <em>et al.</em>
(2005). The impact of education on chronic kidney disease patients’ plans to initiate dialysis with
self-care dialysis: a randomized trial. <em>Kidney International</em>, <strong>68</strong>(4), 1777-1783.
</li>
<li id="mer96">Meredith, C., Symonds, P., Webster, L., Lamont, D., Pyper, E., Gillis, C.R. <em>et al.</em>
(1996). <a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2352093/pdf/bmj00560-0036.pdf">Information
needs of cancer patients in west Scotland: cross sectional survey of patients’ views.</a> <em>British
Medical Journal</em>, <strong>313</strong>(7059), 724-726. Retrieved 24 July, 2013 from
http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2352093/pdf/bmj00560-0036.pdf</li>
<li id="mes01">Mesters, I., van den Borne, B., De Boer, M. &amp; Pruyn, J. (2001). Measuring information
needs among cancer patients. <em>Patient Education and Counseling</em>, <strong>43</strong>(3), 253-262.
</li>
<li id="mil94">Miles, M.B., Huberman, A.M. (1994). <em>Qualitative data analysis: an expanded
sourcebook</em>. (2nd. ed.) Thousand Oaks, CA: Sage Publications.</li>
<li id="nic00">Nicholas, D. (2000). <em>Assessing information needs: tools, techniques and concepts for the
Internet age</em>. (2nd. ed.). London: Aslib. </li>
<li id="orm08">Ormandy, P. (2008). Information topics important to chronic kidney disease patients: a
systematic review. <em>Journal of Renal Care</em>, <strong>34</strong>(1), 19-27.</li>
<li id="orm10">Ormandy P. (2010). Defining information need in health – assimilating complex theories from
information science. <em>Health Expectations</em>, <strong>14</strong>(1),92-104.</li>
<li id="ors03">Orsino, A., Cameron, J.I., Seidl, M., Mendelssohn, D. &amp; Stewart, D.E. (2003). Medical
decision-making and information needs in end-stage renal disease patients. <em>General Hospital
Psychiatry</em>, <strong>25</strong>(5), 324-31.</li>
<li id="pic00">Piccoli, G., Mezza, E., Iadarola, A.M., Bechis, F., Anania, P., Vischi, M. <em>et al.</em>
(2000). <a href="http://www.webcitation.org/6IM0DTPsv">Education as a clinical tool for self-dialysis.</a>
<em>Advances in Peritoneal Dialysis</em>, <strong>16</strong>, 186-190. Retrieved 24 July, 2013 from
http://www.advancesinpd.com/adv00/Education00.html (Archived by WebCite&reg; at
http://www.webcitation.org/6IM0DTPsv)</li>
<li id="pin04">Pinquart, M. &amp; Duberstein, P.R. (2004). Information needs and decision-making processes
in older cancer patients. <em>Critical Reviews in Oncology/Hematology,</em> <strong>51</strong>(1), 69-80.
</li>
<li id="ren05">Renal Association. <em>Renal Information Exchange Group</em>. (2005). <em><a
href="http://www.webcitation.org/6IM0XDH6f">Renal PatientView</a></em>. Retrieved 24 July, 2013 from
http://www.renal.org/whatwedo/JointActivitiesSection/RIXGSection/RenalPatientView.aspx (Archived by
WebCite&reg; at http://www.webcitation.org/6IM0XDH6f)</li>
<li id="ros74">Ross, R.T. (1974). Optimal orders in the method of paired comparisons. In G.M. Maranell
(Eds.) <em>Scaling: a sourcebook for behavioral scientists.</em> (pp. 106-109). Chicago, IL: Aldine
Publishing Company.</li>
<li id="rut05">Rutten, L.J.F., Arora, N.K., Bakos, A.D., Aziz, N. &amp; Rowland, J. (2005). Information
needs and sources of information among cancer patients: a systemtatic review of research (1980-2003).
<em>Patient Education and Counseling</em>, <strong>57</strong>(3), 250-261.</li>
<li id="sch03a">Schatell, D., Ellstrom-Calder, A., Alt, P.S. &amp; Garland, J.S. (2003a). Survey of CKD
patients reveals significant gaps in knowledge about kidney disease. Part 1. <em>Nephrology News &amp;
Issues</em>, <strong>17</strong>(5), 23-25.</li>
<li id="sch03b">Schatell, D., Ellstrom-Calder, A., Alt, P.S. &amp; Garland, J.S. (2003b). Survey of CKD
patients reveals significant gaps in knowledge about kidney disease. Part 2. <em>Nephrology News &amp;
Issues</em>, <strong>17</strong>(6), 17-19.</li>
<li id="sco03">Scott, J.T. &amp; Thompson, D.R. (2003). Assessing the patient information needs of
post-myocardial infarction patients: a systematic review. <em>Patient Education and Counseling</em>,
<strong>50</strong>(2), 167-177. </li>
<li id="she04">Shenton, A.K. &amp; Dixon, P. (2004). The nature of information needs and strategies for
their investigation in youngsters. <em>Library &amp; Information Science Research</em>,
<strong>26</strong>(3), 296-310.</li>
<li id="slo94">Sloan, J.A., Doig, W. &amp; Yeung, A.A. (1994). Manual to carry out Thurstone Scaling and
related analytic procedures. Winnipeg, Canada: University of Manitoba, Manitoba Nursing Research
Institute. (Technical Report #11).</li>
<li id="str98">Straus, A.L. &amp; Corbin, J. (1998). <em>Basics of qualitative research: grounded theory
procedures and techniques</em>. (2nd. ed.). London: Sage Publications.</li>
<li id="thu74">Thurstone, L.L. (1974). A law of comparative judgment. In G.M. Maranell (Eds.) <em>Scaling: a
sourcebook for behavioral scientists</em>. (pp 81-92). Chicago, IL: Aldine Publishing Company. </li>
<li id="twe05">Tweed, A.E. &amp; Ceaser K. (2005) Renal nursing. Renal replacement therapy choices for
pre-dialysis renal patients. <em>British Journal of Nursing</em>, <strong>14</strong>(12), 659-664.</li>
<li id="uk04">U.K. <em>Department of Health</em>. (2004). <em><a
href="http://www.webcitation.org/6ILvnNvm4">National service framework for renal services. Part one:
dialysis and transplantation.</a></em> London, Department of Health. Retrieved 24 July, 2013 from
http://bit.ly/1eSILp0 (Archived by WebCite&reg; at http://www.webcitation.org/6ILvnNvm4)</li>
<li id="uk00">U.K. <em>Office of National Statistics</em>. (2000). <em><a
href="http://www.webcitation.org/6IM30y48G">Standard occupational classification 2000</a></em>.
Newport, UK: Office of National Statistics. Retrieved 24 July, 2013 from http://bit.ly/18CA2pr (Archived
by WebCite&reg; at http://www.webcitation.org/6IM30y48G)</li>
<li id="wal00">Wallberg, B., Michelson, H., Nystedt, M., Bolund, C., Degner, L.F. &amp; Wilking, N. (2000).
Information needs and preferences for participation in treatment decisions among Swedish breast cancer
patients. <em>Acta Oncologica</em>, <strong>39</strong>(4), 467-476.</li>
<li id="whi96">Whittaker, A.A. &amp; Albee, B.J. (1996). Factors influencing patient selection of dialysis
treatment modality. <em>American Nephrology Nurses Association Journal</em>, <strong>23</strong>(4),
369-75; discussion 376-377.</li>
<li id="wil98">Williamson, K. (1998). Discovered by chance: the role of incidental information acquisition
in an ecological model of information use. <em>Library &amp; Information Science Research,</em>
<strong>20</strong>(1), 23-40.</li>
</ul>

</section>

</article>