<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

# The information seeking behaviour of oil and gas industry workers in the context of health, safety and emergency response: a discussion of the value of models of information behaviour

#### [Rita Marcella](#authors), [Tracy Pirie](#authors) and [Hayley Rowlands](#authors)  
Robert Gordon University, Garthdee Road, Aberdeen, AB10 7QE

#### Abstract

> **Introduction.** We explore the application of models of information seeking behaviour to the findings of a study of the information needs of oil and gas workers in relation to health, safety and emergency response.  
> **Method.** Primary data were collected through a Web-based survey of oil and gas workers and in-depth semi-structured interviews with eleven individuals working in four case-study companies. The critical incident formed the basis for the first part of the interview.  
> **Analysis.** Initial analysis of the data was undertaken by statistical analysis of the survey findings, qualitative coding of extended responses to both the survey and interview instruments, and further _post hoc_ analysis of findings against themes identified in four key information behaviour models.  
> **Results.** Information behaviour models help researchers to make sense of the data from applied, practice-based studies of information need and use in industry. Without such theoretical underpinning, applied research lacks depth and fails to contribute to the development of knowledge.  
> **Conclusions.** The research demonstrates the validity of some of the core principles of the investigated models. The findings also identify a number of other aspects of information search which could be further explored in model generation.

## Introduction

The critical nature of health and safety in the oil and gas industry continues to dominate the media, as illustrated by the high profile Deepwater Horizon disaster in 2010, where a subsequent investigation concluded that "BP focused too much on the little details of personal worker safety instead of the big systemic hazards that led to the 2010 Gulf of Mexico oil spill", noting in particular the "difference between worker safety and making sure the entire rig and well are safe" ([The Guardian 2012](#gua12)). Many of the concerns noted by the panel focused on incompatible systems and poor communications creating a barrier between operator and contractor.

In 2011, a team at Aberdeen Business School conducted a research project, sponsored by AVEVA, which explored the role of information systems in enhancing health, safety and emergency (HSE) response in the oil and gas industry. This resulted in an industry report which was launched at simultaneous events in Aberdeen, UK and Houston, Texas ([Marcella and Pirie 2011](#mar11)). Whilst the research identified interesting and useful perspectives on the role of information systems, or safety management systems in enhancing health and safety information, it also uncovered insights into the information seeking behaviour of oil and gas employees in the context of health and safety information. These insights form the focus of the present paper which seeks to consider the research findings in light of influential theoretical approaches to information seeking.

## Literature review

Previous research into safety management systems in the oil and gas industry has focused on the design and implementation of systems, their effectiveness and reliability, and the organizational culture necessary for such systems to have impact. There has been little research into information behaviour and use in the context of organizational information needs and the interaction between the individual and organization through these systems.

### Safety management systems

The oil and gas industry employs complex and multiple systems to store, retrieve, create and communicate information relating to health and safety. As such, Reason _et al._ ([1998](#rea98)) conclude that consideration should be given to the needs and capacities of end users when safety management systems processes and guidance are developed. Comprehensible and usable information communication is particularly significant in light of Reason's ([1998](#rea1998)) sentiment that a safe culture is an informed culture.

Given the challenge of operating in a dynamic and highly competitive industry, it is unsurprising that Dijkhuizen and Henriquez ([2011](#dij11)) highlight that not every accident will be fully investigated because of a lack of time and that, therefore, organizations across the oil and gas industry should share data for subsequent analysis of direct and root causes. They suggest that a standard classification should be used to help feed analysis back into safety management and to guide priority setting. Mearns _et al._ ([2003](#mea03)) also present a case for greater cross-industry collaboration between organizations and argue that the benefits of sharing information outweigh any perceived threats in being open about incidents that have occurred. They suggest that confidential reporting is the key to understanding the role of human factors in accidents. Meanwhile, Groeneweg _et al._ ([2010](#gro10)) believe that investigations should explore fully the role of information as part of incidents.

Baram ([2010](#bar10)) concludes that better analysis of information and wider collaboration is vital to improvement in health and safety performance and that the industry must create a trusting environment to build a positive safety culture rather than focusing on compliance. Understanding how individuals use and think about information in a health and safety context will aid the creation of a positive safety culture which encourages incident reporting.

Bottani _et al._ ([2009](#bot09)) argue that adopters of safety management systems exhibit higher performance levels across a variety of safety indicators, suggesting that the mere fact of having a system will lead to enhanced safety. To be effective, safety management systems require input and interaction by multiple agents. Beard and Santos-Reyes ([2003](#bea03)) identify two types of knowledge required to underpin effective systems: (i) procedures, processes and operations; and (ii) technical, human and organizational aspects. However, even those with automated safety management systems face challenges as a result of the complexity and variety of standards, organizational complexity and cumbersome documentation. Davidson _et al._ ([2006](#dav06)) argue that standardising and simplifying processes and documentation would allow for better staff understanding of personal roles and responsibilities regarding health and safety.

Commentators highlight the importance of _near miss_ reporting to identify potential risks. However, there are two areas of fallibility: the person and the system ([Reason 2000](#rea00)) where it is possible to seek either a systemic organizational or psychological cause of failure. Reason categorises failures into two types: active failures (unsafe acts carried out by people in direct contact with the system) and latent failures (error provoking conditions, which may take a long time to emerge). Both may be dependent on access to reliable information.

Research has also been carried out into the extent to which systems can help both proactively to predict accidents and reactively to investigate their cause. In 2008 Aas created a classification framework to form the basis of a 'formal knowledge transfer system', which can also be used to predict and prevent future accidents and analyse the effectiveness of risk mitigations ([Aas 2008: 275](#aas08)). Johnsen _et al._ ([2010](#joh10)) conclude that the use of proactive risk indicators, identified through sources such as interviews, the literature and Health and Safety Executive guidelines, can make organizations more resilient. However, there is also evidence to suggest that information may exist but not be available in a usable form. Brazier and Pacitti ([2008](#bra08)) found that poor handovers have contributed to major accidents. The face-to-face shift handover might be enhanced by creating a structured logging environment, with log books and handover reports. This allows easier sharing of information, to improve decision making and priority setting and leaves an audit trail (see also [Cavaye and Poutchkovsky 2004](#cav04)).

Endsley ([2000: 4](#end00)) explores the information dimensions of situational awareness theory, arguing that

> the problem with today's systems is not a lack of information, but finding what is needed when it is needed... this is because there is a huge gap between the tons of data being produced and disseminated and people's ability to find the bits that are needed and process them [in support of decision making].

Saracevic recognised this challenge which led to his theory of information consolidation ([Saracevic and Wood 1981](#sar81)). The added dimension of the situational aspects of information need formulated by Endsley ([2000: 4](#end00)) emphasises the importance of systems supporting '_the operator's ability to get the needed information under dynamic operational constraints_'.

The _elements_ ([Endsley 2000: 4](#end00)) of situational awareness vary widely between domains but the components can be generalised as perception, comprehension, projection or prediction and temporal aspects. The first stage, perception, which is described as fundamental, refers to the perception of cues and information. Comprehension is the second stage of situation awareness which involves the combination, interpretation, storage and retention of information to derive meaning and significance in relation to goals and tasks. Stage three, projection, is the ability to forecast future situations to allow for timely decision making. Finally the temporal aspects of situation awareness are considered, as how much time is available until an event occurs will influence action to be taken, and impact on tasks and goals. The dynamic aspect of real-world situations is highlighted, with the need for situation awareness to evolve as situations change and new information is available, and which is a fundamental ability when dealing with a critical or emergency incident.

### The communication of health and safety information

As staff are expected to deal with a significant amount of health and safety information in the workplace, the capacity of end-users must be considered when implementing safety management systems, and information must be displayed and communicated to them in a way which is comprehensible and usable. Fairbairn _et al._ ([2006](#fai06)) describe the typical _safety case_ as a huge document, in which information is difficult to find and extract and which often means little to the workforce. These documents also date quickly. Fairbairn _et al._ ([2006](#fai06)) believe that presenting the safety case in a graphic and dynamic electronic format makes it easier to understand, allows information to be tailored to job role and responsibilities and, generally, improves communication. Similarly, Campbell _et al._ ([2010](#cam10)) argue that a pictographic and standardised representation of health and safety information is better suited to its effective communication to multicultural workforces. If such visualisations are realistic they can also enhance perceptions of safety ([Frohlich 2010](#fro10)). Dalzell ([2004](#dal04)) also argues that staff must have access to sufficient information to do their job safely, concluding that knowledge is the greatest risk reducer.

Brazier and Sedgwick ([2011](#bra11)) argue that over-reliance on oral communication may undermine safety as there are too many opportunities for miscommunication. They propose that communications between control rooms and senior management be improved by recording information electronically, a form also easy to use, update and search. Use of a single integrated system would also ensure that the format, structure and content of handovers are standardised, improving the safety culture and enabling the historical analysis of information.

Johnsen _et al._ ([2008](#joh08)) note that collaborative digital integrated systems improve communication between onshore and offshore personnel, optimising decision making; however, companies are cautioned to be vigilant about security risks, the potential lack of shared understanding and the need for staff training in system use. Other benefits of an integrated system identified by Kubala and Carey ([2002](#kub02)) include greater consistency of implementation and a heightened capacity to provide multilingual and multicultural information and resources.

It is widely acknowledged that information dissemination could be better designed to engage the workforce. Lopez ([2012](#lop12)) proposes greater use of new forms of social media as an effective way of engaging and informing staff about the health and safety agenda. While Tamarra and Macwan ([2012](#tam12)) believe that the inclusion of a daily health and safety message in the footer of every e-mail sent in an organization ensures that the health and safety culture permeates the workforce. Flin ([2008](#fli08)) recommends storytelling as a good way of involving staff in learning about health and safety.

### Organizational factors

In terms of organizational culture and the effectiveness of safety management systems, Reiman and Rollenhagen ([2011](#rei11)) believe that a sound safety culture requires a holistic human and system alignment; while Antonsen _et al._ ([2007](#ant07)) conclude that a trusting, encouraging, participatory environment is essential, if the workforce is to make informed health and safety decisions. However, Reason _et al._ ([2001](#rea01)) believe that what they call the _'vulnerable system syndrome'_ has three elements: blame, denial and pursuit of specific indicators, each of which may undermine the production of good and reliable intelligence. Cohan ([2002](#coh02)) also warns of _'information myopia'_ as a common feature of the lead up to critical incidents, where a poor organizational culture impacts on honesty and information sharing.

Choo ([2009](#cho09)) discusses the information barriers which prevent organizations from noting and acting upon warning signals, including: _epistemic blind spots_ (selective use of information, use of information which confirms beliefs, reinterpretation of information to meet beliefs); _risk denial_ (values, norms and priorities influence the use of information so that no action is taken); and _structural impediments_ (the supply and flow of information affects the organization's ability to detect, mitigate and recover from failures).

> First, in early warning, it is difficult to discern signals about a real threat from random noise. Second, even when signals are detected, policy makers have to weigh the costs and consequences of possible false alarms and misses in deciding whether to [move to] active warning. ([Choo 2009: 1071](#cho09)).

Choo also notes that information is rarely complete or conclusive and difficult decisions remain, reinforcing the need for an open and honest reporting culture to maximise the value of workforce intelligence.

Mearns and Yule ([2009](#mea09)) find management commitment to safety a critical factor in influencing levels of safety behaviour, in contrast with the widely held view that frontline staff need to take more responsibility for safety ([Marcella and Pirie 2011](#mar11)). To reduce disparity between management and workforce perceptions of health and safety effectiveness, Hovden _et al._ ([2008](#hov08)) recommend the involvement of safety representatives in gathering employee opinions and assessments.

Digital systems are today considered essential for effective operation in offshore or any remote operating environment ([Maher _et al._ 2011](#mah11)). Haight ([2006](#hai06)), however, concludes that a balance is needed between human and system capacity to maximise a safe culture; while Moulton and Forrest ([2005](#mou05)) suggest that automation can lead to 'out-of-loop' syndrome where the workforce becomes detached from the process and more experienced staff are discouraged from passing on tacit knowledge to those less experienced. Folb _et al._ ([2010](#fol10)) caution that, as in other domains, the effectiveness of health and safety information systems is undermined by time constraints leading inevitably to the phenomenon of _satisficing_, described as "accepting less than full information as adequate" ([Folb _et al._ 2010: 6](#fol10)).

### Models of information seeking behaviour

As illustrated by the literature presented above, much of the research regarding health and safety information has focused on systems, processes and organizational context, with little focus on the role of the individual and their information seeking behaviour. The present paper attempts to place health and safety information in a theoretical context by considering data on information behaviour collected from a practical study, designed to feed back into industry practice, in terms of their relevance to models of information seeking behaviour. Relating practice to theory in this way will help us to explore the contribution such models can make to develop an understanding of the information practices demonstrated in the earlier study.

Four models of information seeking behaviour will be considered: Kuhlthau ([1991](#kul91)); Leckie, Pettigrew and Sylvain ([1996](#lec96)); Ellis and Haugan ([1997](#ell97)) and Wilson ([1996](#wil96)). Case ([2006: 120](#cas06)) states '_theories and models are simplified versions of reality, yet models typically make their content more concrete through a diagram of some sort.a model describes a relationship among concepts but is tied more closely to the real world_'. This relationship with the real world is a major attraction in using models to understand the information seeking behaviour of oil and gas employees in relation to health and safety information. This study was carried out with participation from health and safety managers, senior managers and engineers currently working in the oil and gas industry.

### Kuhlthau's information search process

Kuhlthau ([1991](#kul91)) sets out a general model from the user's perspective of an information search process which considers not only information actions and tasks, but also the affective dimension in the feelings and thoughts that individuals experience during this process. She describes her model as "the user's constructive activity of finding meaning from information in order to extend his or her state of knowledge on a particular problem or topic" ([Kuhlthau 1991: 361](#kul91)).

There are six stages to the information search process; _initiation_ (an awareness of lack of knowledge or understanding, recognising the need for information); _selection_ (identification and selection of a general topic or approach); _exploration_ (finding information on a general topic to extend individual understanding); _formulation_ (forming a focus for the search through information found in _exploration_); _collection_ (gathering of information related to the focused topic); and _presentation_ (completing the search and preparing to present or otherwise use the information collected).

### Ellis and Haugan's general behavioural model of information seeking patterns

Ellis and Haugan ([1997](#ell97)) carried out research into the information seeking patterns of research workers in an oil and gas company in the context of their '_general behavioural model [which] can be considered to be quite robust in relation to the information seeking patterns of scientists, engineers and social scientists in both academic and an industrial research environment_' ([1997: 402](#ell97)).

Eight activities are identified, but no sequence is given in which these occur. _Surveying_ is the process of gathering an overview of literature within a new subject, or identifying key people in the field as potential sources of information by speaking to colleagues, consulting internal documents, etc. _Chaining_ is following the connections or references between sources of information to identify new sources, again through literature or personal contacts. _Monitoring_ involves maintaining an awareness of developments formally or informally. _Browsing_ involves casually consulting primary and secondary sources. _Distinguishing_ also happens between sources to identify the information best suited to the individual information need, whilst considering available time and the extent of the information need. _Filtering_ is using specific criteria or mechanisms which ensure information is relevant and precise, whilst _extracting_ is working through these sources to identify material of interest to the searcher. Finally _ending_ involves checking sources for new information since the search was begun, and clarifying any queries or questions.

### Leckie, Pettigrew and Sylvain's information seeking behaviour of professionals

Leckie _et al._ propose a general model of the information seeking behaviour of professionals (e.g. doctors, lawyers, engineers), recognising that each will encounter a variety of work related information needs and arguing that,

> while it is true that the provision of various types of service or expertise to their clients is the overarching activity that links all professionals, they work within specific environments that differ greatly in organizational structure, mission, goals and social culture. ([1996](#lec96): 178).

There are six components in this model:

_Work roles and associated tasks_: professional life is complicated and various roles are assumed in day-to-day work. Specific tasks are associated with each of these roles which influence the context of an information need.

_Characteristics of information need_: roles and tasks are influenced by factors such as searcher demographics and the context, frequency, importance and complexity of the search.

_Awareness_: the searcher's level of awareness of sources and/or content influences information seeking behaviour, as does familiarity with sources, trustworthiness, packaging, cost, quality and accessibility.

_Sources_: sources of information in professional contexts are varied and can include colleagues, librarians, handbooks, journals and personal knowledge and experience. They may be formal or informal, internal or external and oral or written sources.

_Outcomes_: the outcomes of information seeking behaviour ideally are that the information need is met and the instigating task is accomplished. If not, further information seeking behaviour can take place to satisfy the information need.

### Wilson's global model of information behaviour

To fully understand Wilson's ([1996](#wil96)) global model of information behaviour, his earlier ([Wilson 1981](#wil81)) model must first be explored. The 1981 model was an outline of areas associated with information seeking behaviour, emanating from a perceived need by an information user. To satisfy the need, formal or informal information sources are consulted, leading to either success or failure in finding relevant information. If successful, the information found will be used to satisfy the user's need; if unsuccessful, then the search process will begin again.

Wilson later revised this model, drawing on research from fields other than information science, such as decision making, psychology and consumer research ([Wilson 1996](#wil96)). The later model describes a cyclical process which begins with the person in context, followed by the context of the information need. An activating mechanism instigates the need for information, and intervening variables influence the line of information behaviour taken by the individual. Another activating mechanism dictates the need to find information and then information seeking behaviour occurs. Wilson proposes four categories of information seeking behaviour: passive attention, passive search, active search, and ongoing search. Information found is then processed and used, or deemed unsuitable to fulfil the information need and the information seeking process starts again.

The Wilson model synthesises the recognition of contextual and individual factors which impact on the information seeking process, whilst acknowledging the significance of volition on the part of the searcher.

In 2008, Wilson commented on the growing disconnection between research by practitioners (to improve practice) and academic research (grounded on theory), commenting that the

> consequence of a disconnection between practice and academe is the danger that the division may become so complete that both sets of players simply ignore what is happening in the other 'world'...some may believe that the position has already been reached that professional education and research are irrelevant to practice ([Wilson 2008: 462](#wil08)).

Wilson suggests that action research should be applied to future research into information seeking behaviour to help bridge this gap. Whilst the project from which this paper draws its data was not designed as an action research project, the data gathered draws from the real life experiences of practitioners. In particular the critical incident case approach used in the interviews allows the researcher to explore the behaviour of practitioners in an information seeking activity with minimal prompting and a focus on what actually happened.

## Research methods

The aim of the current paper is to explore data from commissioned research into the role of information systems in the management of health and safety in the oil and gas industry, to consider the extent to which these data can be explained by existing models of information behaviour and to identify any emerging findings which add to our theoretical perspectives on information behaviour.

The research project was conducted in two stages, in a mix of quantitative and qualitative approaches: an online survey and in-depth interviews using critical incidents as a focus in four case-study companies.

The research team were part of Aberdeen Business School at Robert Gordon University, with expertise in information management and information behaviour. This influenced the design of the survey and, although the focus was on information systems, ensured that theoretical perspectives on information seeking behaviour were considered in design of the instruments. All survey questions and interview schedules were designed in consultation with the commissioning company AVEVA, to reflect industry perspectives, and each part of the study was piloted at an appropriate stage with industry representatives. The interview schedule was piloted internally amongst AVEVA personnel, which led to a diverse array of individuals contributing to the design of the research instrument. On reflection whilst the process of collaborative research design can be challenging, with industry-led research focusing on broad lessons and academic research focusing on what may be perceived as small detail, a collaborative approach was felt to be an effective exercise in ensuring the research instrument was well designed. In the authors' experience the outcomes from a robustly designed instrument have been positive, ensuring effective output for both parties.

The online survey was conducted between April and May 2011 using Survey Monkey (survey questions are provided in Appendix 1). Participants were invited to complete the online survey by e-mail, which was distributed to organizational industry contacts and the LinkedIn 'Oil and Gas Professionals' group. Participation was also promoted through press releases. There were 374 responses to the online survey from operating companies, contractors, service companies and suppliers. Respondents tended to be health and safety managers, senior managers and engineers. Over 72% of participants described themselves as able to exert major influence on health and safety decision making and direction, indicating a relatively senior level of respondents. Responding companies operated worldwide, with only 29% focused on the UK Continental Shelf.

In the second stage of the research, four companies were enlisted to participate in in-depth follow-up interviews: these were an operator, a contractor, a manufacturer and a logistics company (interview questions are provided in Appendix 2). Participating companies were identified through industry contacts, LinkedIn and from the survey. A single critical incident to be discussed was identified by the companies before interviewing began, and all incidents chosen were unexpected and unpredictable. The incidents selected were regarded as sufficiently substantial to have potentially caused a detrimental impact on the organization, but were not of such stature as to jeopardise company anonymity. The duration of incidents ranged from a week to years, and interviewees were key individuals instrumental in dealing with the incidents and able to comment critically on the impact information behaviour had had on their company's capacity to respond.

Interviews were carried out face-to-face or by telephone and lasted between forty minutes and two hours. All interviews were recorded with oral permission by interview respondents. The interview schedule was long and covered lots of eventualities; therefore, some questions were superfluous and truncated, were not needed, or further probing was required depending on the circumstance and receptiveness of the interviewee. The interviews were subsequently transcribed verbatim and recurring themes were coded by the research team, in an iterative process. Subsequent further analysis for the present paper has been informed by the models of information behaviour discussed above.

Interviews were carried out with eleven individuals encouraging reflection on the critical incident identified. Interviewees held a variety of roles in relation to these, including investigator, supervisor, lead response, project manager, team leader, safety advisor, duty manager, data analyst and participant. The research team sought participation from a variety of organizational levels to reflect the differing perspectives within an emergency response team dealing with a critical incident.

The critical incident case study technique was chosen for the interviews, as this allows a _storytelling_ approach, with interview participants telling the research team about an incident in their own words. The critical incident technique is an approach first proposed by Flanagan ([1954](#fla54)) and subsequently found valuable as part of behavioural research in relation to information behaviour (see, for example, [Sonnenwald _et al_. 2001](#son01) and [Urquhart _et al._ 2003](#urq03)). The critical incident formed the early focus of the interview, followed by questions about safety information in the second half. This avoided dictating which topics should be discussed during the critical incident part of the interview, gave focus to the interview for participants, encouraged free and open discourse on a familiar topic, and enhanced analysis of the key themes. On reflection, the research team believe this to be a sound approach to the research project and believe the results of this study reinforce the value of critical incidents as a focus for storytelling and narrative enquiry in future information behaviour research. It enables participants to describe in real life terms episodes of information behaviour where participants may have little understanding of (or interest in) the information domain on a more conceptual level.

To understand the data from the research project better, in the context of information seeking behaviour, the components of the models were categorised into three general themes: pre-search, searching, and information use and application.

<table><caption>Table 1: Components of information behaviour models</caption>

<tbody>

<tr>

<th>Information behaviour model</th>

<th>Pre-search</th>

<th>Searching</th>

<th>Information use and application</th>

</tr>

<tr>

<td>

[Kuhlthau (1991)](#kuh91)</td>

<td>Initiation, selection, exploration and formulation</td>

<td>Collection</td>

<td>Presentation</td>

</tr>

<tr>

<td>  

[Leckie, Pettigrew and Sylvain (1996)](#lec96)</td>

<td>Work role and associated tasks, characteristics of information need</td>

<td>Awareness and sources</td>

<td>Outcomes</td>

</tr>

<tr>

<td>

[Ellis and Haugan (1997)](#ell97)</td>

<td>Surveying</td>

<td>Chaining, monitoring, browsing, distinguishing and filtering</td>

<td>Extracting and ending</td>

</tr>

<tr>

<td>

[Wilson (1996)](#wil96)</td>

<td>Person in context, context of information need and activating mechanism</td>

<td>Intervening variables, activating mechanism and information seeking behaviour</td>

<td>Information processing and use</td>

</tr>

</tbody>

</table>

The discussion which follows presents the results of the commissioned research in three broad sections:

1.  in terms of what the results of the research tell us about information behaviour in relation to systems, the company context and the individual;
2.  in terms of what the results of the interviews tell us about individual information behaviour (pre-searching, searching and information use); and
3.  a discussion of the applicability of the information behaviour models to the observed information seeking environment.

## The systems

Safety management systems provide the core for managing the metrics applied by companies to safety, with 92% of questionnaire respondents reporting their company had an information system to support health and safety. However, despite the widespread use of safety management systems throughout the responding companies, over a quarter of respondents felt there were difficulties in accessing information to help them operate safely; almost 30% of respondents felt there were deficiencies in the reporting of and provision of health and safety information; and over 35% were aware of instances where they or colleagues had not recorded information about near misses. Paradoxically, while respondents were confident they were sharing information with others, 24% felt that relevant information was not being shared with them and over 45% reported instances where information was not shared with other parties.

Tools being developed to improve safety information included developing dynamic forms of safety communications (for example, toolbox talks, e-mails and notice boards), improving company handbooks, increased training, increased emphasis on competency assessment and strengthening safety leadership.

All companies had incident recording and reporting systems although concerns were expressed about the communication of safety lessons in a comprehensible and dynamic manner. The methods most commonly used to provide employees with health and safety information were safety bulletins, safety meetings, newsletters, site walk-arounds by management, verbal direction, toolbox talks, seminars, company handbooks, e-mail, company intranets, training packages, management systems and notice boards.

Interviewees were comfortable they had process safety information to support organizational metrics, and that this information was current and robust. There were issues, however, as to whether the correct types of information were being recorded.

A complex array of information sources is used to carry out roles safely including: company intranets, intranet gateways to external resources, the UK Health and Safety Executive Website, Institution of Occupational Safety and Health materials, competitor Websites, customer guidelines, trade documents, government Websites and documents, information related to external audits, information shared with companies and competitors, management meetings, company health and safety reports, statistical reports from external agencies, integrated management systems, oral briefings from health, safety and environment teams, maintenance management systems, incident recording systems, accounting systems and verbal communication with key individuals.

Interviewees reported that the majority of information required was available through a company database, although it was recognised that third party information was integrated on an ad-hoc basis, explaining the previously reported uncertainty around the reliability of some information.

Many thought an integrated system collocating the variety of sources, systems and data, would be desirable, if challenging to produce. Integration would enhance consistency of information provision, reduce reliance on cascade procedures and ensure updated information is available in one location. However some respondents noted breakdowns in communication channels closer to the front line, as, for example, when information is communicated verbally at potentially weak points such as shift handover.

The ease with which respondents can communicate with the systems or people that could help respond to an incident varied. Some found internal communication to be easier than external; in one case all communications were mediated through a third party. Other agendas may come into play, as a result of concerns around liability, competitive advantage, market impact and reputation, and these tend to be heightened in critical incident situations. Some interviewees reported a reluctance to share information. Such caution may impede an organization's ability to respond to the incident in the quickest and most effective manner, encourage duplication of effort, result in differing conclusions being drawn and lead to confusion about responsibilities.

Research participants acknowledged that information gaps existed during a critical incident, which were sometimes unpredictable and inevitable. It was however felt that such gaps placed additional pressure on staff not just at the outset of an incident but throughout its duration, as ill-informed early decisions affected everything that followed. This is in line with Endsley's ([2000: 5](#end00)) theory of situation awareness, which states the perception of cues is fundamental as 'the odds of forming an incorrect picture of the situation increase dramatically' if cues are missed.

While much might be done to enhance information dissemination to staff to support ongoing safety performance, it is particularly important that critical incident information handling procedures be drawn up, given the heightened importance of getting the right information quickly to the right people, to minimise collateral damage. All of the pressures of everyday information handling are exacerbated when dealing with a potentially catastrophic incident, yet individuals rely, arguably to an even greater extent, on sources that are trusted, familiar and physically accessible.

Whilst safety management systems have improved communications across the oil and gas industry and its supply chain, respondents continue to report barriers to open communication and transparent reporting of the causes of incidents, undermining the potential of such shared systems to aid in the identification of performance indicators which robustly assess safety performance and ensure that lessons are being learned from incidents and near misses.

## The company context

Safety management systems are used by the oil and gas industry to monitor safety performance and improvements in safety, as well as to raise safety awareness generally and encourage a positive safety culture. Drivers include changes in regulation and legislation, customer demands, changes in industry perceptions, increased focus on procedural compliance and increased media coverage of high profile industry incidents. Respondents sought to foster better personal attitudes and individual safety behaviour, as well as encouraging personal responsibility for safety.

It was felt by companies involved in the research that they provided a good level of structured guidance and training for employees on accessing or searching for information. This was done through the use of staff handbooks and training programmes. There was, however, evidence of confusion amongst respondents relating to the myriad of regulatory standards and compliance regimes in which they operated. Equally an individual might have to deal with a variety of differing client expectations. Respondents noted particular challenges in dealing with environmental regulations.

The industry as a whole also faces the twin challenge of an often highly experienced workforce, with very fixed views; in contrast to new entrants with new ideas but little real life experience. This tension is felt to impact on the success of safety enhancement initiatives to encourage behavioural change.

## The information user

### Pre-searching

Respondents reported that the diverse workforce, in age range, experience, culture and literacy levels, reduced the effectiveness of training packages which tended to be designed for office-based staff or new employees.

Just over half of respondents reported a need for better systems to record audit improvements and over 40% of respondents reported a need to improve the quality of safety information available. Clearer information was required about fundamentals, such as what procedures should be followed if an incident occurs. Respondents felt that company handbooks could be improved by clearly highlighting the most reliable indicators of heightened risk. Respondents also reiterated the need for more user friendly health and safety information systems, greater pictorial representation of information, and improved offsite access to systems. More generally, there were calls for an overall simplification of documentation.

To overcome these issues respondents felt that employees should have a more proactive role in determining the information which could highlight potential risks and in identifying the information required in the event of an emergency or when dealing with a health and safety incident. Although challenging, such user consultation at the design stage will inevitably result in better designed information systems.

### Searching

Critical incidents are dynamic high pressure situations, when a variety of information is required from multiple sources to enhance situation awareness and make decisions quickly; information needs can change and develop as the incident unfolds. Delays in sourcing information can potentially escalate the incident but also heighten risks for others involved. Overall, interviewees highlighted the need for information to be readily available in one physical/virtual location when dealing with an incident, so the whole emergency response team has access to information relevant to where the incident was occurring.

Time was identified as a significantly influential factor when dealing with an incident as decisions often need to be made instantaneously or conversely be made over a period of months, depending on the nature of the incident. It was felt where time is a critical factor there is a greater chance of full data not being resourced to make a decision, again aligning with situation awareness theory.

The variety of types of information required when responding to a critical incident might include: information from people involved in the incident, the location of the incident, what machinery, equipment or chemicals were involved, customer, contractor and supplier information and policies, company data about the specific job, data recorded on machinery or equipment, incident statements, third party assessments of products and equipment as well as the location of affected equipment, where employees and contractors were located at the time of the incident, information about how the company wishes to proceed, information on the logistics of moving employees, the current state of operations and occupational health advice. There was also a need to verify the validity of information accessed during a critical incident.

While 80% of questionnaire respondents felt the systems they used supported them in assessing and improving safety, more than 30% had never received training on how to access the information they needed to operate safely. Sources used by employees for safety information include team meetings, informal networks and external experts. There was also a tendency to surf the Internet in the hope of finding relevant and reliable information. When dealing with a critical incident, interviewees were guided in finding the information they required by: training in dealing with specific situations; seeking guidance from senior managers; approaching trade bodies; and generic searching on the Internet. When working as a team during a critical incident, employees liaised and shared information via telephone, e-mail, video conferencing or face to face meetings.

The survey revealed profound barriers in terms of user access to information. Over 25% of respondents find it a challenge to access information they feel necessary to perform their roles safely. The challenges faced included: systems failure, procedures not covering specific circumstances, filing issues, data not existing, lack of communication infrastructure in-country, overly complex systems, poorly indexed systems, restricted access areas in systems, and information overload. Over 40% also reported difficulties in knowing how to search for relevant safety information, which could be related to the access issues reported above or to deficiencies in training. This could also be explained by around 25% who felt unsure about what safety information they actually had access to and the circumstances in which it should be used.

In all of the critical incidents, respondents reported having to consult external sources of information, including third party transport agencies, vendors, the emergency services, customers and suppliers, environmental agencies, trade bodies, standards agencies, government bodies, contractors, logistics experts and offshore installations. The usefulness of these sources was determined by experience, industry codes and company procedures or driven by the nature of the incident. A number also reported relying on '_gut instinct_'.

Some interviewees had been guided by senior management to external sources of information, and team and management discussions would take place concerning the trustworthiness of external sources, which are more likely to be subjected to some form of verification. Interviewees regarded internal information as more likely to have been previously subject to quality assurance. Others noted that they had no way of assuring themselves that the information they were using was fit for purpose. Where people were aware of uncertainty over the status of information they tended to escalate concerns about data reliability to senior management teams flagging its questionable status and seeking advice.

It is interesting to note the high level of trust demonstrated in, and lack of verification of, internal information sources in light of the evidence of the survey that respondents tend to be aware of misreporting of data in internal systems.

### Information use

Despite awareness of gaps in the sharing of knowledge, respondents reported feeling pressurised and overwhelmed by the sheer amount of information, frequently duplicated or contradictory, with which they must seek to engage. This is exemplified in the number of standards with which they must comply. This increase in pressure and feeling of being overwhelmed can lead to important information being missed or incorrectly prioritised, reducing situation awareness. The scale of information available may not only undermine an individual's capacity to process but also induce complacency, leading to valuable sources being ignored.

Respondents measured the trustworthiness of the information available on the basis of somewhat subjective criteria such as character judgement when receiving verbal accounts from employees, appraisal of the track record of companies with whom they were dealing, the interviewee's personal interpretation of the data, value judgements as to the reputation of external agencies such as environmental bodies, and whether or not the interviewee regarded the information as '_just a fact_'. Interviewees also reported issues with inaccurate manufacturer's data and paperwork, fragmentary information from third parties and poor quality information provided by suppliers. In terms of determining whether information was trustworthy, interviewees confirmed the preference for internal sources.

Frequently, the validity of information used in decision making was only tested after its use and indeed after the incident was over. Respondents tended to focus on the immediate information required to take the next steps rather than the information which would enable them to form a longer term view.

There were perceived information barriers regarding the nature of information, including the effective translation of information for foreign workers, the development of media to make instruction more visually explicit, the introduction of explicit procedures for how to access, process and apply information in work roles and a need to trend health and safety issues more effectively. It was also felt that disparate systems and a lack of resources to deal with health and safety information helped to create information barriers.

Where gaps in information existed they often related to: information not being available on what corrective actions were expected of an organization in the early stages of an incident; logistical information; and information on how the incident would affect key equipment. It was also suggested that where information is changed in daily operations these are not always propagated to associated references, exacerbating the potential for error during an incident. For some incidents there was felt to be a need for all information that might be required to be available in a single widely accessible digital form.

In some cases during a critical incident interviewees reported they had _no_ access to information which would have aided them to assess the impact of their decisions. For others the systems in place did not allow for the creation of reliable statistical models to support their decision making. In some cases there were issues with accessibility of required information which resulted in delays in first step responses to an incident, and others identified a challenge accessing the highly specialist information sought.

As with most information behaviour, individuals tend to place great trust in informal sources, such as peers, and the ubiquitous recourse of surfing the Internet. With information being sought and disseminated through numerous channels and by a variety of means, a great concern for health and safety managers is that messages are not always being received, internalised and operated upon, but rather getting lost in the _white noise_. This lack of comprehension may have negative consequences on the ability to derive meaning and significance from information, therefore reducing situation awareness and increasing the risk of incidents occurring.

## Conclusions: the applicability of information models to the research findings

In data collected from a real world complex business environment, the stages identified in the information seeking behaviour models can be observed throughout. However, the reality of an information seeking episode such as those illustrated in the critical incidents on which the current study draws, is that the stages merge and overlap as information of multiple types is required at different points in a process which is non-linear and where strands overlap and interact. The situation may involve multiple players and systems and can become chaotic as a result of time and other pressures.

Situation awareness has become an important conceptual part of the process of system design for particular operator fields of engagement such as air flight: however, it can be readily seen how such an approach would have value for the design of health, safety and emergency response information systems. The authors have not found evidence of such use to date, but future research might valuably explore the identification of data, with user consultation, which would best enhance situation awareness. It would also be valuable to explore the role of systems in effective communication during emergency response, and how data may be best aggregated for team access to support situation awareness throughout the critical incident.

Kuhlthau ([1991](#kul91)) presents broad categories of stages in information seeking behaviour all of which are relevant to the current study, but more significantly for the current research her observations on the influence of feelings and emotions on information behaviour are confirmed in the present study. Respondents reported feelings of frustration and being overwhelmed as a result of the information gaps and barriers they encountered, while feeling under intense pressure as a result of the time constraints in which they operated. Respondents noted uncertainty, anxiety and confusion in describing their experience of critical incidents. They relied on 'gut instinct', experience and familiar and trusted resources very readily in circumstances where the use of invalid information might impact on the outcomes of a catastrophic event. Perhaps the focus on critical incidents heightens the significance of the affective: however the present authors would argue that the affective domain is always relevant to a degree.

Ellis and Haugan ([1997](#ell97)) give no sequence to the stages of their information model, and focus on the variety of actions which comprise information seeking behaviour. The present study highlights this complex process of information seeking and use in a real life situation, and indicates that information seeking does not happen in a systematic manner, especially when dealing with an emergency situation. Information may be subject to other constraints, such as physical location and time, which impact fundamentally on an abstract conceptualisation of the information search process.

Leckie _et al._ ([1996](#lec96)) provide a model in a work related context, and highlight the differences between individual organizations and the effect this can have on information behaviour. The data presented here confirms this finding as the systems and procedures organizations have in place can influence the information seeking of their employees. Another dimension of the organizational context is the company's culture and bigger picture agenda, where, while the rhetoric may be about openness and sharing, in actuality there is active constraint on information sharing, and this constraint is likely to be more rigorously enforced in a critical situation.

Wilson ([1996](#wil96)) also considers the '_person in context_' and the intervening variables which can influence information behaviour. This is manifested in the time constraints placed on an information need, especially when dealing with an emergency situation, but also by the external pressures from other organizations and channels of communication between concerned parties. This notion of the individual actor as part of a complex team of players comes across very strongly in the current research. Also significant from the Wilson model ([1996](#wil96)) in the context of the current research is the distinction between the passive and active searcher. In the current research much of the organizational strategy in design of safety systems is fuelled by a view of a company seeking to engage with a passive recipient of information, rather than seeing the user as an active information searcher. This is a fundamental flaw in existing system design evidenced by the present study.

What is also evidenced by the present research is that the real world information seeking environment is a complex, fluid and rapidly changing environment and to seek to distil this world into a one-dimensional model may be unhelpful. Whilst researchers such as Leckie _et al._ ([1996](#lec96)) and Wilson ([1996](#wil96)) have sought to convey complexity, it remains a challenge to fully represent the real world environment in a model which allows for comparison and contrast and, therefore, deeper understanding of information behaviour. There are hypothetically limitless variables which can and do influence information behaviour which must be understood, and both these models go some of the way towards achieving this. It may be interesting for future research to consider alternative scientific approaches to modelling which are capable of illustrating complexity in a dynamic way. A major benefit of creating a model is that in its generation, the researcher must provide routes through complexity and codify our sense of the limitless in a meaningful way.

Another level of complexity that is demonstrated in the present research is that of the actor dealing with multiple simultaneous information needs; an emergency response team may have several issues with which to deal, all of which require different types of information from different sources, with varying impacts on the incident outcome. Calls for integrated systems and clearly presented information reflect how complex a task this can be. In a world of uncertainty, as is characteristic of a critical incident, users look for certainty and clarity, indeed that is arguably one of the chief motivators in any information seeking situation. One of the most interesting findings of the current research is the lack of verification, analysis or filtering that takes place and the degree of subjectivity that was observed amongst respondents. Information behaviour models are, to an extent, rooted in a rationalist view of the world, while the study provides evidence that intuition rather than analysis may be a relatively common mode of dealing with complexity.

Information behaviour models are highly useful devices to assist researchers in conceptualising our understanding of information behaviour. We believe that an understanding of these models helps in the design of effective research whether for a real life practical purpose or in more abstract conceptualisation. While the industries commissioning research may desire a more pragmatic outcome, without a deep understanding of the theoretical base upon which they can draw, those carrying out the research will merely skim the surface in their research and are highly unlikely to add to knowledge in any significant way. The latter is surely the aim of all serious researchers. The experience of undertaking the research described in this paper has indicated that it is possible to design research which not only satisfies industry needs in a way that can be understood and applied, but also to draw deeper learning from the findings.

While industry-led research may be focused on the headlines and broad lessons to be drawn, inevitably the academic researcher is more interested in what may seem the small detail. The authors believe that the most significant findings in their research relate to the information challenges, barriers and gaps and their impact on effective and open information sharing within companies and across the industry and on the capacity of that industry to operate safely and deal with emergencies. The industry audience however was interested in the next big idea: sometimes it is the nexus between these two expectations where the next big idea actually evolves.

## Acknowledgements

The authors would like to thank AVEVA for sponsoring the research project. Thank you to the referees and copy-editor (Jo Elliot) for assisting with the publication of this article. A special thank you our colleague Graeme Baxter for his assitance with the project.

## <a id="authors"></a>About the authors

**Rita Marcella** is Dean of Faculty at Aberdeen Business School, Robert Gordon University. Her research interests include: the provision of, need for, and use of government, parliamentary and citizenship information; Internet use by political parties and electoral candidates; freedom of information; business information provision; information behavior and information systems in health and safety management. She can be contacted at [r.c.marcella@rgu.ac.uk](mailto:r.c.marcella@rgu.ac.uk)  
**Tracy Pirie** is a lecturer in the Department of Communication, Marketing and Media, Aberdeen Business School. She was previously a Research Assistant for the same department, and can be contacted at [t.a.pirie@rgu.ac.uk](mailto:t.a.pirie@rgu.ac.uk)  
**Hayley Rowlands** is a research assistant at Aberdeen Business School, where she has worked on several projects relating to the oil and gas industry. She holds a degree in Historical Studies from The University of Glasgow and an MSc in Information and Library Studies. She can be contacted at [h.rowlands1@rgu.ac.uk](mailto:h.rowlands1@rgu.ac.uk)

<section>

## References

<ul>
<li id="aas08">Aas, A. L. (2008). <em>The human factors assessment and classification system (HFACS) for the oil and gas industry</em>. Paper presented at the International Petroleum Technology Conference, 3-5 December 2008, Kuala Lumpur, Malaysia.
</li>
<li id="ant07">Antonsen, S., Ramstand, L. &amp; Kongsvik, T. (2007). Unlocking the organization: action research as a means of improving organizational safety. <em>Safety Science Monitor</em>, <strong>11</strong>(1), 1-10.
</li>
<li id="bea03">Beard, A. N. &amp; Santos-Reys, J. (2003). A safety management system model with application to fire safety offshore. <em>The Geneva Papers on Risk and Insurance</em>, <strong>28</strong>(3), 413-425.
</li>
<li id="bot09">Bottani, E., Monica, L. &amp; Vignali, G. (2009). Safety management systems: performance differences between adopters and non-adopters. <em>Safety Science</em>, <strong>47</strong>(2), 155-162.
</li>
<li id="bar10">Baram, M. (2010). <em><a href="http://www.webcitation.org/6IZb46tZb">Preventing accidents in offshore oil and gas operations: the U.S. approach and some contrasting features of the Norwegian approach</a></em>. Boston, MA: Boston University, School of Law. (Working paper No. 09-43). Retrieved 7 January, 2013 from http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1705812 (Archived by WebCite® at http://www.webcitation.org/6IZb46tZb)
</li>
<li id="bra08">Brazier, A. &amp; Pacitti, B. (2008). Improving shift handover and maximising its value to the business. <em>Loss Prevention Bulletin</em>, <strong>204</strong>(1), 21-28.
</li>
<li id="bra11">Brazier, A. &amp; Sedgwick, M. (2011.) <em>The link between safety and shift handover</em>. Paper presented at SPE Offshore Europe Oil and Gas Conference and Exhibition, 6–8 September 2011, Aberdeen, UK
</li>
<li id="cam10">Campbell , H., Duguay, A. &amp; Lecoindre, C. (2010). <em>Crossing barriers in language and comprehension to improve worker safety</em>. Paper presented at the Abu Dhabi International Petroleum Exhibition and Conference, 1-4 November, 2010, Abu Dhabi, Dubai.
</li>
<li id="cas06">Case, D. O. (2006). <em>Looking for information</em>. (2nd. ed.). London: Academic Press.
</li>
<li id="cav04">Cavaye, N. &amp; Poutchkovsky, A. (2004). <em>Design, benefits and use of an interactive oil spill contingency plan (i-OSCP)</em>. Paper presented at the Seventh SPE International Conference on Health, Safety and Environment in Oil and Gas Exploration and Production, 29-31 March 2004, Calgary, Alberta.
</li>
<li id="cho09">Choo, C. W. (2009). Information use and early warning effectiveness: perspectives and prospects. <em>Journal of the American Society for Information Science and Technology</em>, <strong>60</strong>(5), 1071-1082.
</li>
<li id="coh02">Cohan, J. A. (2002). "I didn't know" and "I was only doing my job". Has corporate governance careened out of control? A case study of Enron's information myopia. <em>Journal of Business Ethics</em>, <strong>40</strong>(3), 275-299.
</li>
<li id="dal04">Dalzell, G. A. (2004). <em>The body or the smoking gun?</em>. Paper presented at the Seventh SPE International Conference on Health, Safety and Environment in Oil and Gas Exploration and Production, 29-31 March 2004, Calgary, Alberta.
</li>
<li id="dav06">Davidson, S., Provost, E. &amp; Baatz, A. (2006). <em>Global HSE standards for global GSE risks: standardise, simplify, and share</em>. Paper presented at the SPE International Conference on Health, Safety, and Environment in Oil and Gas Exploration and Production, 2-4 April, 2006, Abu Dhabi, Dubai.
</li>
<li id="dij11">Dijkhuizen, B. C. J. &amp; Henriquez, L. R. (2011). <em>Mirror of E&amp;P Safety Performance 2005-2009</em>. Paper presented at the SPE European Health, Safety and Environmental Conference in Oil and Gas Exploration and Production, 22-24 February, 2011, Vienna, Austria.
</li>
<li id="ell97">Ellis, D. &amp; Haugan, M. (1997). Modelling the information seeking patterns of engineers and research scientists in an industrial environment. <em>Journal of Documentation</em>, <strong>53</strong>(4), 384-401.
</li>
<li id="end00">Endsley, M. (2000). Theoretical underpinnings of situation awareness: a critical review. In M. R. Endsley and D. J. Garland (Eds.), <em>Situation Awareness Analysis and Measurement</em> (pp. 3-32). Mahwah, NJ: Lawrence Erlbaum Associates.
</li>
<li id="fai06">Fairbairn, L., Pegram, A. &amp; Nishapati, M. (2006). <em>"E-Safety Case": improving communication and application of HSE data</em>. Paper presented at the SPE International Conference on Health, Safety and Environment in Oil and Gas Exploration and Production, 2-4 April, 2006, Abu Dhabi, Dubai.
</li>
<li id="fla54">Flanagan, J. C. (1954). The critical incident technique. <em>Psychological Bulletin</em>, <strong>51</strong>(4), 327-358.
</li>
<li id="fli08">Flin, R. (2008). <em>Enhancing decision making in critical operations</em>. Paper presented at the SPE Conference on Health, Safety and Environment in Oil and Gas Exploration and Production, 15-17 April, 2008, Nice, France.
</li>
<li id="fol10">Folb, B. L., Detlefsen, E. G., Quinn, S. C., Barron, G. &amp; Trauth, J. M. (2010). Information practices of disaster preparedness professionals in multidisciplinary groups. <em>Proceedings of the American Society for Information Science and Technology</em>, <strong>47</strong>(1), 1-9.
</li>
<li id="fro10">Frohlich, P., Schatz, R., Leitner, P., Mantler, S. &amp; Baldauf, M. (2010). Evaluating realistic visualizations for safety-related in-car information systems. In <em>Proceedings of the SIGCHI Conference on Human Factors in Computing Systems, Atlanta, GA, USA – April 10 - 15, 2010</em>, (pp. 3847-3852). New York, NY: ACM Press.
</li>
<li id="gro10">Groeneweg, J., Van Schaardenburgh-Verhoeve, K. N. R., Corver, S. C. &amp; Lancioni, G. E. (2010). <em>Widening the scope of accident investigations</em>. Paper presented at the SPE International Conference on Health, Safety and Environment in Oil and Gas Production, 12-14 April, 2010, Rio de Janeiro, Brazil.
</li>
<li id="gua12">Guardian, The. (2012). <a href="http://www.Webcitation.org/6Gw5lpoTd">BP 'missed big hazards' before Gulf oil spill</a>. <em>The Guardian</em>. Retrieved 7 January, 2013 from http://www.guardian.co.uk/business/2012/jul/24/bp-missed-hazards-deepwater-horizon (Archived by WebCite® at http://www.Webcitation.org/6Gw5lpoTd)
</li>
<li id="hai06">Haight, J. M. (2006). <em>Do automated control systems really reduce human errors and incidents?</em> Paper presented at the ASSE Professional Development Conference and Exposition, June 11-14, 2006, Seattle, Washington.
</li>
<li id="hov08">Hovden, J., Lie, T., Karlsen, J. E. &amp; Alteren, B. (2008). The safety representative under pressure. A study of occupational health and safety management in the Norwegian oil and gas industry. <em>Safety Science</em>, <strong>46</strong>, 493-509. [Powerpoint presentation retrieved 2 August, 2012 from http://www.docstoc.com/docs/128174269/session-23-hovden-the-safety-representative-under-pressure
</li>
<li id="joh08">Johnsen, S.O., Gran, B.A., Jaatun, M.G., Larsen, S. &amp; Thunem, A.P.J. (2008). <a href="http://www.Webcitation.org/6Gw6RzrMd"><em>Safety, security and resilience in integrated operations</em></a>. Trondheim, Norway: SINTEF. Retrieved 7 January, 2013 from http://www.sintef.no/upload/Teknologi_og_samfunn/Sikkerhet%20og%20p%C3%A5litelighet/Rapporter/SINTEF%20A10353.pdf (Archived by WebCite® at http://www.Webcitation.org/6Gw6RzrMd)
</li>
<li id="joh10">Johnsen, S. O., Okstad, E., Aas, A. L. &amp; Skramstad, T. (2010). <em>Proactive indicators of risk in remote operations of oil and gas fields</em>. Paper presented at the SPE International Conference on Health, Safety and Environment in Oil and Gas Exploration and Production, 12-14 April, 2010, Rio de Janeiro, Brazil.
</li>
<li id="kub02">Kubala, G. &amp; Carey, A. (2002). <em>The EnviroHub: an intranet tool for culture change and information management</em>. Paper presented at the SPE International Conference on Health, Safety and Environment in Oil and Gas Exploration and Production, 20-22 March 2002, Kuala Lumpur, Malaysia.
</li>
<li id="kuh91">Kuhlthau, C. C. (1991). Inside the search process: information seeking from the user's perspective. <em>Journal of the American Society for Information Science</em>, <strong>42</strong>(5), 361-371.
</li>
<li id="lec96">Leckie, G. J., Pettigrew, K. E. &amp; Sylvain, C. (1996). Modeling the information seeking of professionals: a general model derived from research on engineers, health care professionals and lawyers. <em>Library Quarterly</em>, <strong>66</strong>(2), 161-193.
</li>
<li id="lop12">Lopez, J. C. (2012). <em>Communicating HSE to a new generation</em>. Paper presented at the SPE Middle East Health, Safety, Security and Environment Conference and Exhibition, 2-4 April, 2012, Abu Dhabi, Dubai.
</li>
<li id="mah11">Maher, S. T., Cheek, C. D., Brawley-Roehl, E. M. &amp; Long, G. D. (2011). <em>Paradigm shift in the regulatory application of safety management systems to offshore facilities</em>. Paper presented at the American Institute of Chemical Engineers 2011 Spring Meeting, 7th Global Congress on Process Safety, 13-17 March, 2011, Chicago, Illinois.
</li>
<li id="mar11">Marcella, R. &amp; Pirie, T. (2011). <em><a href="http://www.aveva.com/~/media/Aveva/English/Resources/White_Papers/Info%20Mgmt/RGU_HS_Information_Gap_Report-2011.ashx">The health and safety information gap</a></em>. Retrieved 7 January, 2013 from http://www.aveva.com/~/media/Aveva/English/Resources/White_Papers/Info%20Mgmt/RGU_HS_Information_Gap_Report-2011.ashx (Registration necessary).
</li>
<li id="mea03">Mearns, K., Whitaker, S., Flin, R., Gordon, R. &amp; O'Connor, P. (2003). <a href="http://www.Webcitation.org/6Gw7OLh9u"><em>Factoring the human into safety: translating research into practice Vol. 1.</em></a> Bootle, UK: Health and Safety Executive. (Research Report 062). Retrieved 7 January, 2013 from http://www.hse.gov.uk/research/rrpdf/rr059.pdf (Archived by WebCite® at http://www.Webcitation.org/6Gw7OLh9u)
</li>
<li id="mea09">Mearns, K. &amp; Yule, S. (2009). The role of national culture in determining safety performance: challenges for the global oil and gas industry. <em>Safety Science</em>, <strong>47</strong>(6), 777-785.
</li>
<li id="mou05">Moulton, B. &amp; Forrest, Y. (2005). Accidents will happen: safety-critical knowledge and automated control systems. <em>New Technology, Work and Employment</em>, <strong>20</strong>(2), 102-114.
</li>
<li id="rea1998">Reason, J. (1998). Achieving a safe culture: theory and practice. <em>Work &amp; Stress</em>, <strong>12</strong>(3), 293-306.
</li>
<li id="rea98">Reason, J., Parker, D. &amp; Lawton, R. (1998). Organizational controls and safety: the varieties of rule-related behaviour. <em>Journal of Occupational and Organizational Psychology</em>, <strong>71</strong>(4), 289-304.
</li>
<li id="rea00">Reason, J. (2000). Human error: models and management. <em>British Medical Journal</em>, <strong>320</strong>(7237), 768-770.
</li>
<li id="rea01">Reason, J., Carthey, J. &amp; De Leval, M. R. (2001). Diagnosing "vulnerable system syndrome": an essential prerequisite to effective risk management. <em>Quality in Health Care</em>, <strong>10</strong>(Supplement II), ii21-ii25.
</li>
<li id="rei11">Reiman, T. &amp; Rollenhagen, C. (2011). Human and organizational biases affecting the management of safety. <em>Reliability Engineering and Systems Safety</em>, <strong>96</strong>(10), 1263-1274.
</li>
<li id="sar81">Saracevic, T. &amp; Wood, J. B. (1981) <em>Consolidation of information: a handbook on evaluation, restructuring and repackaging of scientific and technical information</em>. Paris: UNESCO.
</li>
<li id="son01">Sonnenwald, D. H., Wildemuth, B. M. &amp; Harmon, G. L. (2001). A research method using the concept of information horizons: an example from a study of lower socio-economic students' information seeking behaviour. <em>New Review of Information Behaviour Research</em>, <strong>2</strong>, 65-86.
</li>
<li id="tam12">Tamarra, R. &amp; Macwan, N. (2012). <em>Sharing of HSE lessons through e-messaging tool</em>. Paper presented at the SPE Middle East Health, Safety, Security and Environment Conference and Exhibition, 2-4 April, 2012, Abu Dhabi, Dubai.
</li>
<li id="urq03">Urquhart, C., Light, A., Thomas, R., Barker, A., Yeoman, A., Cooper, J. <em>et al.</em> (2003). Critical incident technique and explication interviewing in studies of information behaviour. <em>Library &amp; Information Science Research</em>, <strong>25</strong>(1), 63-88.
</li>
<li id="wil81">Wilson, T. D. (1981). <a href="http://www.webcitation.org/6IZfy1FLs">On user studies and information needs</a>. <em>Journal of Documentation</em>, <strong>37</strong>(1), 3-15. Retrieved 2 August, 2013 from http://informationr.net/tdw/publ/papers/1981infoneeds.html (Archived by WebCite® at http://www.webcitation.org/6IZfy1FLs)
</li>
<li id="wil96">Wilson, T. D. (1996). <a href="http://www.webcitation.org/6IZg63b70">Models in information behaviour research.</a> <em>Journal of Documentation</em>, <strong>55</strong>(3), 249-270. Retrieved 2 August, 2012 from http://informationr.net/tdw/publ/papers/1999JDoc.html (Archived by WebCite® at http://www.webcitation.org/6IZg63b70)
</li>
<li id="wil08">Wilson, T. D. (2008). The information user: past, present and future. <em>Journal of Information Science</em>, <strong>34</strong>(4), 457-464. Retrieved 2 August, 2013 from http://informationr.net/tdw/publ/papers/2008PastPresentFuture.pdf Retrieved 4 August, 2013 from http://informationr.net/tdw/publ/papers/2008PastPresentFuture.pdf
</li>
</ul>

</section>

</article>

* * *

## Appendices

**Appendix 1, Survey Monkey questionnaire; health and safety information**

_General information: In this section please provide general information about you and your organization._

1.  What is your job title?  

2.  In which of the following functions does your role sit?
    a.  Senior management
    b.  Engineering
    c.  Supply chain
    d.  Production
    e.  Human resources
    f.  Health and safety
    g.  Operations
    h.  Finance
    i.  IT
    j.  Other (if other please specify)

3.  What type of company do you work for?
    a.  Operator
    b.  Service company
    c.  Contractors
    d.  Suppliers
    e.  Other (if other please specify)  

4.  Where are you primarily based:
    a.  Office
    b.  Field
    c.  Other (if other please specify)

5.  Please indicate in which of these global regions your organization operates? (please tick all that apply)
    a.  Africa
    b.  Arctic
    c.  Asia
    d.  Europe
    e.  Non EU European (e.g. Russia)
    f.  North America
    g.  Oceania
    h.  South America
    i.  UKCS
    j.  Other (if other please specify)

6.  Please indicate in which of these global regions you would normally work? (please tick all that apply)
    a.  Africa
    b.  Arctic
    c.  Asia
    d.  Europe
    e.  Non EU European (e.g. Russia)
    f.  North America
    g.  Oceania
    h.  South America
    i.  UKCS
    j.  Other (if other please specify)

_Health and safety: This section explores various aspects of your organization's health and safety._

7.  How important do you believe health and safety is:
    a.  To your organization:
        i.  Very important
        ii.  Important
        iii.  Neither
        iv.  Unimportant
        v.  Very unimportant
    b.  In your part of the organization:
        i.  Very important
        ii.  Important
        iii.  Neither
        iv.  Unimportant
        v.  Very unimportant

8.  Does your organization have any information systems in place to support health and safety?
    a.  Yes
    b.  No
    d.  Don't know

9.  Does your organization operate a consistent and uniform health and safety management system across all operations?
    a.  Yes
    b.  No
    c.  Don't know

10.  Is your organization's health and safety system part of a company wide integrated computer system (Enterprise Resource Planning)?
    a.  Yes
    b.  No
    c.  Don't know

11.  Please indicate the approach that exists across the health and safety system used in your organization:
    a.  Each site operates a separate system
    b.  A mixture of corporate and local systems
    c.  A single corporate system is used
    d.  Don't know
    e.  Other (if other please specify)

12.  How committed are you personally to safety in your role at your organization?
    a.  Very committed
    b.  Committed
    c.  Neither
    d.  Uncommitted
    e.  Very uncommitted

13.  To what degree do you influence decisions and direction on safety?
    a.  Major influence
    b.  Minor influence
    c.  No influence

14.  In the last 2 years do you believe the importance of health and safety in your organization has:
    a.  Increased
    b.  Decreased
    c.  Stayed the same
    d.  Don't know

15.  If you believe the importance of health and safety in your organization has increased in the last 2 years, what do you see as being the single main factor?
    a.  Changes in regulations
    b.  Changes in corporate culture
    c.  Changes in management
    d.  organizational changes
    e.  In reaction to an incident
    f.  Improving a poor safety record
    g.  Other (if other please specify)

16.  If you believe the importance of health and safety in your organization has decreased, or stayed the same, what do you see as being the single main factor?
    a.  No changes in regulation
    b.  Lack of change in corporate culture
    c.  Lack of change in management
    d.  Lack of organizational changes
    e.  Other (if other please specify)

17.  In the last 2 years do you believe the safety performance of your organization has:
    a.  Improved significantly
    b.  Improved slightly
    c.  Stayed the same
    d.  Decreased slightly
    e.  Decreased significantly

18.  If you believe your organization's safety performance has improved, what tools are you aware have been used? (please tick all that apply)
    a.  Adopted new standards
    b.  Increased safety training
    c.  Improve sharing of information with contractors
    d.  Development of competency based assessments
    e.  Enhanced technical training
    f.  Focusing on safety behaviours
    g.  Increased safety communications internally
    h.  Increased senior management championing of safety issues
    i.  Updated operational equipment
    j.  New company procedures (e.g. safety paperwork)
    k.  Not aware of tools being used
    l.  Other (if other please specify)

19.  If you believe your organization's safety performance has stayed the same or not improved, what do you think are the reasons for this? (please tick all that apply)
    a.  Lack of management direction
    b.  No need for increase, we are good enough
    c.  Industry standards do not support current industry needs
    d.  No money for increased focus
    e.  Not enough time
    f.  Unable to make any new progress due to lack of systems/processes
    g.  Don't know
    h.  Other (if other please specify)

20.  Which of these tools are you aware of your organization using to measure safety? (please tick all that apply)
    a.  Number of fatalities
    b.  Number of recordable injuries
    c.  Lost work time injuries
    d.  Number of incidents
    e.  Number of high potential incidents
    f.  Employee assessments
    g.  Behavioural/company culture
    h.  Employee retention
    i.  Level of employee competence
    j.  Employee feedback
    k.  Review of team performance
    l.  Audits
    m.  Competency assessments
    n.  Safety not measured
    o.  Don't know
    p.  Other (if other please specify)

21.  What are your organization's current safety priorities? (please tick all that apply)
    a.  Changing employee safety behaviours
    b.  Changing safety culture of organization
    c.  Meeting regulatory compliance
    d.  Addressing new/changed regulations
    e.  Improving information systems
    f.  Improving the quality of information
    g.  Improving access to information
    h.  Improving employees awareness of information available
    i.  Recording and auditing improvements
    j.  Demonstrating employee competency
    k.  Don't know

22.  In what areas of safety would you most like to see improvements in your organization? (500 characters max)

23.  What types of information would you require to demonstrate to stakeholders where your organization's safety performance is now? (please tick all that apply)
    a.  Number of fatalities
    b.  Number of recordable injuries
    c.  Lost work time injuries
    d.  Number of incidents
    e.  Employee assessments
    f.  Level of employee competence
    g.  Employee feedback
    h.  Review of team performance
    i.  Audits
    j.  Competency assessments
    k.  Don't know
    l.  Other (if other please specify)

24.  What types of information do you require to assess how safety performance can be improved in the future? (please tick all that apply)
    a.  Number of fatalities
    b.  Number of recordable injuries
    c.  Lost work time injuries
    d.  Number of incidents
    e.  Employee assessments
    f.  Level of employee competence
    g.  Employee feedback
    h.  Review of team performance
    i.  Audits
    j.  Competency assessments
    k.  Don't know
    l.  Other (if other please specify)

25.  How motivated is your organization to improve safety to:
    a.  Meet regulatory requirements:
        i.  Very motivated
        ii.  Motivated
        iii.  Neither
        iv.  Unmotivated
        v.  Very unmotivated
    b.  Minimising non-compliance penalties:
        i.  Very motivated
        ii.  Motivated
        iii.  Neither
        iv.  Unmotivated
        v.  Very unmotivated
    c.  Protect corporate reputation:
        i.  Very motivated
        ii.  Motivated
        iii.  Neither
        iv.  Unmotivated
        v.  Very unmotivated
    d.  Meet clients requirements:
        i.  Very motivated
        ii.  Motivated
        iii.  Neither
        iv.  Unmotivated
        v.  Very unmotivated
    e.  Ensure all staff are competent in their roles:
        i.  Very motivated
        ii.  Motivated
        iii.  Neither
        iv.  Unmotivated
        v.  Very unmotivated
    f.  Ensure all employees are safe at work:
        i.  Very motivated
        ii.  Motivated
        iii.  Neither
        iv.  Unmotivated
        v.  Very unmotivated
    g.  Other (please specify area and level of motivation)

26.  What tools does your organization use to measure improvements in safety? (500 characters max)  

27.  What challenges does your organization face in improving safety? (please tick all that apply)
    a.  Lack of resources
    b.  Lack of time to train staff
    c.  Inadequate training
    d.  Aligning to international standards
    e.  Developing a culture of personal responsibility
    f.  Lack of communication internally
    g.  Lack of communication/information sharing across the supply chain
    h.  Lack of personnel competency
    i.  Tendency to focus on productivity over safety
    j.  Support from senior management
    k.  Variations in standards/procedures internally
    l.  Limited sharing of best practice across the industry
    m.  Human behaviours
    n.  Missing or poor quality of information (e.g. process safety, operations procedures)
    o.  Budget constraints
    p.  Constant shift in standards
    q.  Permit to work
    r.  Shift handover
    s.  Operating procedures
    t.  Management of change
    u.  Operational readiness as a result of commissioning activities
    v.  Inspection and maintenance
    w.  Don't know
    x.  Other (if other please specify)

28.  What types of information would be required to enable a better understanding of these challenges? (500 characters max)  

29.  What tools does your organization use to address any of the challenges identified above? (please tick all that apply)
    a.  Training
    b.  Testing competencies
    c.  Improved information systems
    d.  Reward and recognition
    e.  Improved leadership
    f.  Carrying out audits
    g.  Improved safety communication
    h.  Increased consultation with the wider industry/key stakeholder
    i.  Adopting new/improved standards
    j.  Increased management support
    k.  Don't know
    l.  Other (if other please specify)

_Information: This section investigates the use and flow of information systems within your organization and with external bodies._

30.  Do you have responsibility for information systems in your organization?
    a.  Yes
    b.  No
31.  Can information on health and safety be accessed by staff, contractors/suppliers or both to:
    a.  Assess safety of the environment/job
    b.  Respond to an emergency
    c.  Improve safety environment/performance
    d.  To carry out a task safely (e.g. working at heights)
32.  How suitable are the systems used to report on health and safety across your organization for the following:
    a.  To assess safety of job/environment:
        i.  Very suitable
        ii.  Suitable
        iii.  Neither
        iv.  Not suitable
        v.  Very unsuitable
        vi.  Don't know
    b.  To respond to an emergency
        i.  Very suitable
        ii.  Suitable
        iii.  Neither
        iv.  Not suitable
        v.  Very unsuitable
        vi.  Don't know
    c.  To improve safety environment/performance
        i.  Very suitable
        ii.  Suitable
        iii.  Neither
        iv.  Not suitable
        v.  Very unsuitable
        vi.  Don't know
33.  Have you received any type of training on how to access the information you require to operate safely in your role?
    a.  Yes
        i.  If yes, what type and how often?
    b.  No
34.  How does your organization communicate with you regarding access to the systems/information you require in order to carry out your role safely?
    a.  Company intranet
    b.  E-mail
    c.  Newsletter
    d.  Safety meetings
    e.  They don't communicate information
    f.  Don't know
    g.  Other (if other please specify)

35.  How do you access information so that you can operate safely in your role?
    a.  Specialist IT based information systems
    b.  Company intranet
    c.  Informally through colleagues
    d.  Team meetings
    e.  Industry expert sites (e.g. OGP)
    f.  Other (of other please specify)

36.  What types of information do you access in order to:
    a.  Assess safety
        i.  Reports
        ii.  Incident statistics
        iii.  Standards or regulations
        iv.  Technical specifications
        v.  Contractor information
        vi.  Do not access information
        vii.  Other (if other please specify)
    b.  Respond to an emergency
        i.  Reports
        ii.  Incident statistics
        iii.  Standards or regulations
        iv.  Technical specifications
        v.  Contractor information
        vi.  Do not access information
        vii.  Other (if other please specify)
    c.  Improve safety
        i.  Reports
        ii.  Incident statistics
        iii.  Standards or regulations
        iv.  Technical specifications
        v.  Contractor information
        vi.  Do not access information
        vii.  Other (if other please specify)

37.  How confident are you that the information you access is up to date and suitable for its purpose?
    a.  Very confident
    b.  Confident
    c.  Neither
    d.  Not confident
    e.  Seriously concerned

38.  How easy is it for you to find the information that you require to perform safely in your role?
    a.  Very easy
    b.  Easy
    c.  Variable
    e.  Difficult
    f.  Very difficult

39.  Have you ever been unable to access information related to performing your role safely which you know is available in your organization?
    a.  Yes
        i.  If yes, please specify why you could not access the information
    b.  No

40.  Where information is available, have you ever not understood how to search for the relevant information?
    a.  Yes
    b.  No

41.  When accessing safety information you require, is it clear what the purpose of this information is (what it can be used for)?
    a.  Yes, it is clear
    b.  No, it is not clear
    c.  Not always clear

42.  Are you provided with clear instruction on how to present/record information related to safety in your role?
    a.  Yes
    b.  No
    c.  Not always

43.  Do you feel that your corporate information standards are visible to those that need to adhere to them?
    a.  Yes
    b.  No
    c.  Don't know

44.  Have there been any incidences where you/your colleagues have not recorded information related to safety (e.g. near misses)?
    a.  Yes
        i.  If yes, please specify information and reasons why
    b.  No
45.  Do you/your colleagues share safety related information with operators/contractors/suppliers you work with?
    a.  Yes
    b.  No
    c.  Not always
    d.  N/A
46.  Do operators/contractors/suppliers share safety related information with you/your colleagues?
    a.  Yes
    b.  No
    c.  Not always
    d.  N/A
47.  What are the roadblocks to sharing information between operators/contractors or between disciplines? (500 characters max)  

48.  How important is it for safety purposes for you other operators/contractors/suppliers to be able to share information with each other?
    a.  Very important
    b.  Important
    c.  Neither
    d.  Unimportant
    e.  Very unimportant

49.  When sharing information between operators/contractors are you aware of gaps in the information?
    a.  Yes
        i.  If you have answered yes, please specify the information gaps
    b.  No
    c.  Don't know
50.  Does your current information system allow the sharing of lessons learned from incidences/near misses across the organization?
    a.  Yes
    b.  No
        i.  If you have answered no, please specify what the barriers are to sharing this type of information
    c.  Don't know
51.  What other types of information, not already made available, do you require to improve your safety performance within the organization? (500 characters max)  

52.  How could the current information available and its delivery be improved upon? (500 characters max)  

53.  Any other comments? (500 characters max)

* * *

**Appendix 2, Interview Guide; Health and safety Information**

The aim of this study is to ascertain your interaction with information related to health and safety aspects of your operation, both on an ongoing basis, and in relation to a critical incident which has been previously agreed by your organization to be used as a focus for this interview.

The interview is expected to take approximately 1 hour; this varies on how much each participant has to say on the subject. For purposes of transcription and analysis of the key themes, we would like the opportunity to record this interview. All of your responses will be kept strictly confidential, and in any publications arising from the research yours and your company's anonymity are guaranteed. Are you happy for me to go ahead and record this interview?

<u>Organization Overview</u>

Could you please tell me a little bit about your organization and your position?

*   What global regions do you operate in?
*   How many staff do you employ?
*   Length of service in company/oil industry?

How important is safety as part of your company's strategy?

*   Has its importance become greater or less in recent years?
*   What has influenced that change for the company?
*   What types of challenges exist in improving safety in your organization?
*   What types of tools are used/could be used to address these challenges?

<u>Critical incident, has been agreed in advance, refer to it here</u>

<u>Stage 1, incident awareness</u>

I would like you to talk me through the incident from your first awareness that there was a problem through to your disengagement from the incident:

*   What was your role within the incident?
*   Was the incident totally unexpected?
*   Should your organization's information system have given you the ability to identify this incident before it occurred? Were there early warnings that something was going wrong? Was there information available which was ignored that may have prevented the incident if action had been taken?
*   If your system couldn't have made you aware, then what is it that the system couldn't tell you that you needed to know?
*   Generally speaking do your information systems allow you to identify potential problems? What aspect of the system allows this?
*   Had there been any breakdown in communication prior to the incident which meant that those who might have taken action were unaware there was an issue? Please describe? What specific aspects of the systems highlighted the incident for you/should have identified the incident for you?
*   How did you seek to find out more about what was actually happening? Which systems and/or people did you approach, both internally and externally? How did you know where to go to find out more? What happened during the first reactive stage?

<u>Stage 2, taking action</u>

*   Was it easy to communicate with the systems or people necessary at this stage? What problems did you encounter at this stage?
*   How quickly did you have to start making decisions about how to deal with the incident?
*   Were you part of a team or working alone?
*   If a team response was necessary how did you liaise with others on the team and agree how to proceed?
*   What did you need/most want to know as you proceeded in order to take action or make decisions in handling this incident?
*   How did you test the robustness of decisions being made at that stage? Did you have access to data about what the impact of decisions was likely to be?
*   Did you also have to consult external sources for advice/help? Who what were they? How did you identify external sources of helpful data?
*   What kind of data would have been useful that you didn't have?
*   Did the systems your organization has in place give you the ability to create reliable statistical models to support your decision making in this incident? How do you feel this impacted on you/the incident as a whole?
*   Did these systems change as a result of the incident? What can it do better now, if it has been changed?
*   Can you give an example of a decision in this situation that might have been improved if you had had better access to information?
*   Did you share/receive information from contractors/suppliers related to this incident? Why/not? How did the information impact on/help the situation?
*   How accessible did you find the specific information you required, was it easy to find, were there any delays in getting this information and if so how did this impact the overall outcome of the incident?
*   Did you feel that you had sufficient time to access/review/understand and apply the information during the incident?
*   Could the information you reviewed in relation to this incident have been presented in a different/better way/format that could have impacted differently on the incident, how?
*   Can you give me an example of formats/types of information that you felt were particularly helpful/relevant in helping you to deal with the incident? Which was the most valuable, and why in particular were these so beneficial?
*   Was there any information you received that was not particularly helpful/relevant? What was the least valuable information you received and why?
*   Did you trust the sources of information?
*   Can you give any examples of a trusted source, and outline why you trusted this?
*   Can you give me any examples of information that you regarded as less reliable, and why it was less so?
*   What actions were taken/outcomes if you did not trust the information?
*   How did you decide which information was trustworthy? (e.g. was it because you had used it before/validated it etc?)
*   Were you required to validate the information?
*   Was there a status attached to the information which verified it was fit for the purpose of safety analysis?
*   When dealing with this incident, in what ways could your management systems have made it easier/more efficient for you to access the different types of information you require to proceed?
*   Did you find any gaps in the information that you required to deal with this incident? If so what where they?
*   Where you found gaps in the information, how critical a role do you feel that the lack/incorrectness/incompleteness of this information played in this critical incident?
*   Was there any point at which you felt at a complete loss, or you didn't know what was going on? If so please describe and what did you do?
*   Did you feel well supported throughout? What would have helped?
*   How important was it to communicate to others what was happening as you sought to deal with the incident? Who needed to know what? How did you decide what information to release?
*   Was the company's communication department involved in this? Did they actively seek information from you about what was happening and what could be released?
*   Were you subject to an OSHA/HSE audit as a result of this incident, if so were you able to quickly and efficiently provide them with the information they requested? If not, what problems did you encounter and why? What about if you were subject to a general ad hoc visit on a general basis?
*   Did your organization use this incident as a means of identifying lessons learned regarding information access? If so, in what way?
*   Were any changes made to how information was gathered/stored/presented?
*   Were any changes made to processes in response to critical incidents?

<u>Safety information</u>

*   Are you comfortable that you can assess if your key safety processes and procedures comply with current regulatory requirements and standards? Please specify.
*   Thinking of safety information are you challenged by changing regulations and standards? Please specify.
*   Does your organization provide suitable guidance/training for all employees on how to access information so that they understand how/where to search for the information?
*   How does our organization communicate with you/other employees on where to find systems/information related to safety?
*   Does your organization use any tool/applications to assess Safety Culture? If yes, please specify.
*   If you do use a tool/application to measure Safety Culture, does it include an assessment of Process Safety Information?
*   Would you say that your emergency response plan is supported by easily accessible and up-to-date information to support the plan (e.g. P&IDS, Start-up and shutdown procedures, etc.)? If not, what is your key challenge in this regard?
*   Do you have the Process Safety Information to support both Leading and Lagging Safety Indicators? If either presents a challenge, briefly state which and why. Where you have this information do you feel it is up to date? If not then can you give an indication of how much is outdated, e.g. percentage wise?
*   In terms of health and safety information as a whole, what general kinds of information/systems do you use/access, to carry out your role safely?
*   Do you feel that you have access to all of the information you require for your daily decision making and to collaborate with colleagues? If not what is missing? What is the impact of any missing information, how can the organizations overcome this?
*   How integrated do you feel that the different systems/types of information you use are, particularly in the area of process safety management/safety procedures?
*   Do the systems you use allow you to check what standards you need to comply with in different safety situations, both domestically and internationally? Where do you go to find out about the different standards that you need to comply with?
*   How suitable are the systems you use for health and safety information in allowing you/contractors/suppliers to:
    *   Assess safety of the environment/job
    *   Respond to an emergency
    *   Improve safety environment/job
*   Do you deal with accessing Management of Change (MOC) and PHA (Process Hazard Assessment) information associated with any equipment or system (i.e. asset)? If so is previous information on these readily accessible to you? If not what can't you access and why?
    *   How many MOCs are issued monthly and what is their typical backlog?
    *   How many of these are incorrectly classified?
*   How many work permits are issued monthly and how many of these are not completed properly or correctly?
*   Can you easily access the information you require to raise a Hot Working permit?
*   Have you ever found it challenging to gather information for an incident Investigation, or an internal/external audit? If so please state why, and what the main challenges are.
*   Are all types of incidents recorded in your organization? Are there specific types of incidents that would generally be recorded in the industry but are not within your reporting systems?
*   What are the most common non-conformances you find as a result of safety audits?
*   Has your organization ever been cited for any OSHA/HSE or EPA (Environmental Protection Agency) related non-conformances, if so can you share the nature of this with me?
*   How confident are you that the safety information you access in relation to your role is up to date and relevant for its purpose? Do you trust this information in your decision making?
*   Are you able to validate the quality of information at any point in time and identify what/if anything is missing or inconsistent? Any specific examples?
*   Have you experienced any safety related shutdowns that could be attributed to information availability or quality? If so can you please discuss?
*   When comparing information across different systems are there any gaps that you have identified that should not be there? What are these gaps in the information? How could they impact on the organizations? How/do you overcome these gaps?
*   Where there is a change to information can you be sure that this is propagated to all associated references?
*   Are you involved in the sharing of shift handover logs and lessons learned from shift to shift, across the entire operating environment? Examples?
*   Do you have any specific examples where information failures have caused an incident in relation to permit to work/during shift change/in operational readiness/during plant upsets or abnormal operation/ during management of change?
*   Do you feel there are any issues with the amount of information that you are required to deal with in relation to health and safety in the organization (e.g. information overload). If yes (and it's negative) then what would you say is the impact of the amount of information on those that are using it, and how the use the information (i.e. does information get ignored or missed). Do you have any specific examples to highlight this?
*   With the ageing demographics of the oil and gas workers is knowledge retention an issue for your organization, how do you go about capturing the knowledge that these workers have before they leave to retire, do you have specific systems in place to do this. What does this whole issue mean for your organization, what problems do you encounter as a result of this?
*   In what ways could a management system make it easier for you to access health and safety related information?
*   What do you feel are the major information barriers for your company to make improvements in safety, and how could this be improved on?
*   What do you think the impact of that improvement would give the company and your role?
*   Have you been aware of any incidences of general safety data not being logged, attempts to cover up due to issues of trust/blame culture?
*   How critical a role do you feel information plays in safety decisions?
*   Are the information systems utilised in your organization helping to drive safety?
*   Do you feel that there is merit in being able to access safety related information from other disciplines/contractors you work with?
*   Does your safety information system allow you to share lessons learnt across your enterprise? If this is a challenge, please specify. Please give specific examples.
*   What, if any, cost would you say your organization incurs on a yearly/monthly basis as a result of incidents and fines for non-compliance?
*   Are your information systems capable of being used internationally across different cultures? If not what are the major problems?
*   Which KPIs do you have in place to measure the performance of your HSE?
*   How long does it typically take you to get the necessary data to compute these?

<u>General and future development information management</u>

*   Do you know if your organization participates in the Global Reporting Initiative? If so do you know what the main challenges are in providing the necessary information to complete the report?
*   At present what level do you think your company's safety information systems are now? Deficient, capable of improvement, adequate, outstanding?
*   How do you think your company could improve these systems in the future?
*   What does an ideal information management system look like to you?
*   What would you say are the three key benefits of a focused/efficient information management system to a company/contractor operating in the oil and gas industry?
*   What one main benefit do you feel your organization could leverage from a better information system?
*   What else might be done overall to improve safety information systems in the oil and gas industry?
*   Do you have any other comments you would like to make with the use of information/systems in your organization in relation to health and safety?
*   Thank you very much for your participation in this research. If we need to clarify anything from the interview can we get back to you?