<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Bibliographic records in an online environment

#### [Amanda F. Cossham](#author)  
Caulfield School of Information Technology, Monash University, Melbourne, Australia and  
Open Polytechnic of New Zealand, School of Information Science and Technology, Private Bag 31914, Lower Hutt 5040, New Zealand

#### Abstract

> **Introduction**. The IFLA functional requirements for bibliographic records model has had a major impact on cataloguing principles and practices over the past fifteen years. This paper evaluates the model in the light of changes in the wider information environment (especially to information resources and retrieval) and in information seeking behaviour online, rather than in terms of library catalogues per se.  
> **Approach**. Using a critical analytic approach, it reviews a range of literature across library and information science, and analyses the implications of the changing information environment and information behaviour on usefulness of the model.  
> **Conclusion**. The paper argues that although the functional requirements for bibliographic records model may be useful in terms of thinking about the bibliographic universe as constituted when the model was first developed, there have been major changes in the form, format, nature, publishing, and relationships of information resources, along with significant developments in users' information seeking behaviour, understanding and expectations. The model may no longer be sufficient as a theoretical and conceptual basis for cataloguing rules and hence for library catalogues, nor for understanding the bibliographic universe.

<section>

## Introduction

In 1997, an IFLA Study Group issued a report that aimed 'to provide a clearly defined, structured framework for relating the data that are recorded in bibliographic records to the needs of the users of those records' ([IFLA Study Group on the Functional Requirements for Bibliographic Records 1998: Section 2.1.](#ifla98)). There was considerable interest in the report and the functional requirements for bibliographic records model (commonly known as FRBR), and interest increased following the model's adoption as the theoretical underpinning for Resource Description and Access (RDA), the new cataloguing code developed to replace the Anglo-American Cataloguing Rules (AACR or AACR2R) that was released in 2010\. The principles of the model are also included in the Italian cataloguing code Regole italiane di catalogazione (REICAT) of 2009, although this code does not follow the model's definitions and terminology closely ([Petrucciani 2009](#petrucciani09), [2012](#petrucciani12)). Resource Description and Access is of considerable significance for cataloguing librarians and library catalogue development, and the subject of widespread discussion; it is one of the main tools (to date) for implementing the functional requirements determined by the IFLA model.

However, the functional requirements for bibliographic records model was developed during the 1990s when the World Wide Web was in its infancy. Searching in library catalogues was still somewhat limited (for example, Borgman's 'Why are library catalogs still hard to use?' was published in [1996](#borgman96)) and 'search' was not generally a part of someone's everyday life, unless they were an information professional.

Over the fifteen or so years since the model's publication there have been a great many changes in the information landscape; arguably more than at any time since Gutenberg. Driven in no small part by what the Internet (and especially the World Wide Web) makes possible, there have been:

*   new formats of information resources,
*   more complex information resources with more relationships linking them to other resources,
*   new and more flexible ways of publishing,
*   new ways of writing and creating information resources and knowledge,
*   new forms of 'publication', such as recombinations or feedback on blog posts,
*   new ideas about the access, use and relationships between information resources,
*   new and improved search and discovery tools,
*   competition from tools and services outside libraries,
*   increased user familiarity with searching,
*   changes in users' behaviour online, including radically different forms and patterns of engagement,
*   changes in users' understanding of knowledge, of information resources.

Library catalogues now compete in an expanding information environment, with direct competition from search engines, online booksellers, digital libraries and archives (e.g., Project Gutenberg http://www.gutenberg.org/wiki/Main_Page or HathiTrust http://www.hathitrust.org/community), or social media collaborations such as LibraryThing http://www.librarything.com/ (noting that LibraryThing also works with libraries). Internet-based search tools provide powerful search functions that often (but certainly not always) obviate the need for structured metadata such as that in library catalogues.

In the light of these developments it is worth asking how useful the functional requirements for bibliographic records model is in 2013 and in the future. Does it provide a sound underpinning for twenty-first century catalogues? Does it adequately reflect and encompass a _bibliographic universe_ that is increasingly being determined by the Internet? Indeed, does the term bibliographic universe continue to have relevance? Are the model's user tasks of find, identify, select and obtain appropriate for information seeking today?

This paper is not intended as a critique of the model _per se_, as this has been done by other writers. Smiraglia ([2012](#smiraglia12)) provides a concise summary of research into concerns about the model. Rather, the focus is on the model in relation to changes in the wider information environment, and to information users and their information behaviour.

## The functional requirements for bibliographic records model

The functional requirements for bibliographic records model is an entity-relationship model of the bibliographic universe. Based on a long tradition of cataloguing codes, principles and practices, and theory, it was developed by a group of cataloguing experts and derived through '_a logical analysis of the data that are typically reflected in bibliographic records_' (IFLA Study Group 1998: Section 1.2). Thus, it drew on what existed in library catalogues and bibliographic records at the time, and can therefore be said to have _bibliographic warrant_ (cf. Hulme, 1911). It is one of the more sophisticated models in cataloguing, and as it exists independently of any particular cataloguing code, has the potential to be implemented in many different ways.

The model determines the entities, attributes and relationships of interest to users of bibliographic records, and sets out 'user tasks' that are performed by users when searching library catalogues. Bibliographic records (i.e., catalogue records) are the building blocks of library catalogues and represent the resources owned or subscribed to by the library. Alongside these records are others for creators and authors, and for the subject matter of the resources. These are dealt with in two expansions of the main model: functional requirements for authority data (FRAD, published in 2009), and functional requirements for subject authority records (FRSAR, approved in 2010 and published in 2011). These expansions are outside the scope of this paper. A mapping of the model to the CIDOC conceptual reference model (CRM) was completed in 2006 with a full version published in 2009 and version 2.0 in 2012 (International Working Group on FRBR and CIDOC CRM Harmonisation 2012). This is known as FRBRoo and provides an object-oriented definition and mapping of FRBRer, the original entity-relationship model.

The functional requirements for bibliographic records model is a major development for cataloguing, providing a detailed conceptual approach, instead of one that is case-based as earlier developments were (see, for example, Bianchini and Guerrini 2009; Carlyle 2006; Denton 2007; IFLA FRBR Review Group 2010; IFLA Meeting of Experts 2009; Le Boeuf 2005; Zhang and Salaba 2009a). The IFLA Study Group's (1998) report was a re-statement of many of the principles and relationships seen in earlier cataloguing codes and writings, but re-stated at a higher theoretical level, less tied to the physical existence of works in a digital age.

The model defines three groups of entities (see Figure 1):

*   Group 1 entities are Work, Expression, Manifestation, and Item. They represent the products of intellectual or artistic endeavour.
*   Group 2 entities are person and corporate body, responsible for the intellectual or artistic content, the physical production and dissemination, or the custodianship of Group 1 entities
*   Group 3 entities are subjects of intellectual or artistic endeavour; i.e., Group 1 and Group 2 entities along with concepts, objects, events and places (IFLA Study Group 1998: Section 3.1 Overview).

<figure>

![Figure 1: Group 1, 2 and 3 entities and relationships](../pC42figure1.png)

<figcaption>

Figure 1: Group 1, 2 and 3 entities and relationships ([Denton n.d.](#denton))</figcaption>

</figure>

A great deal has been written about the model, so much so that the FRBR Bibliography ([IFLA FRBR Review Group 2010](#ifla10)) is no longer being updated. An initial understanding can be gained from the following: Tillett ([2004](#tillett04)) provides a basic introduction and overview; Carlyle ([2006](#carlyle06)) has an in-depth analysis of the model and discusses issues around modelling abstractions; and Zhang and Salaba ([2009a](#zhang09a)) look at the implementation of the model in libraries and other cultural heritage institutions.

## The bibliographic universe

One of the problems faced in discussing the functional requirements for bibliographic records model is the concept _bibliographic universe_. '_The model ... represents, as far as possible, a "generalized" view of the bibliographic universe …_' ([IFLA Study Group 1998: Section 1.3](#ifla98)). The model deals with '_information resources [that] exist within a given “universe” (e.g., within the totality of available information resources, within the published output of a particular country, within the holdings of a particular library or group of libraries, etc.)' (Section 2.2), and can also be considered 'the universe of entities described in bibliographic records_' (Section 1.2).

Bibliographic universe is rarely defined and more usually scoped, as it is by the IFLA Study Group ([1998](#ifla98)). Eleven definitions dating from 1968 to 2011 were compared and three aspects stand out from these (see Appendix 1 for all eleven definitions). According to these definitions, the bibliographic universe has most often been considered to be:

*   all recorded knowledge (writings and recorded sayings), documents, or
*   those things found in libraries, or,
*   entities in bibliographic databases, in bibliographic records.

The first of these aspects is the most common in the definitions, and is a good conceptual definition, but not a good operational one, because in practice the bibliographic universe is a concept discussed only the library literature. In addition, notions of 'recorded knowledge' and 'documents' are changing, as are the physical collections of such materials. The term bibliographic universe is familiar to cataloguing librarians; less widely used by other librarians; and almost never used outside the discipline or profession. It is not a concept found in archival or record-keeping literature, for example, despite the definition implying that it includes the materials in these collections (and more).

This can be contrasted with the IFLA Study Group's statement that users of bibliographic records include '_readers, students, researchers, library staff, publishers, distribution agents, retailers, information brokers, administrators of intellectual property rights, etc._' ([1998: Section 2.2](#ifla98)). Research into applications of the model claims it will be useful for a wide range of cultural heritage resources (see Zhang and Salaba ([2009a](#zhang09a): Chapter 4) for an overview of this research). This may be correct for some types of resources, but can also be considered somewhat arrogant on the part of the library profession. Petrucciani ([2012](#petrucciani12): 609) notes that other communities such as archivists or those involved with art objects '_will also have their own specific needs and would like to develop their own professional tools (and usually have them already). The development of professional tools by one community for another (or for many others) is usually ineffective and sounds unfair_'.

As it is possible to talk of an information universe or documentary universe (or even an information multiverse), perhaps what is required is an operational definition that includes the concepts of and relationships between documents (textual or non-textual) that are deemed significant, collected, and organised. Talking of a bibliographic universe may not be relevant in the twenty-first century, given the phenomenal changes in the nature of information resources.

## User tasks

Another problem with the model lies in its user tasks. The model aimed to clarify the functions of bibliographic records in the context of tasks that are performed by users when searching library catalogues:

*   find materials that correspond to the user's stated search criteria …;
*   identify an entity (e.g., to confirm that the document described in a record corresponds to the document sought … or to distinguish between two texts or recordings that have the same title);
*   select an entity that is appropriate to the user's needs …;
*   obtain access to the entity described ([IFLA Study Group 1998: Section 2.2](#ifla98)).

These tasks are directly descended from the 'objects of the catalog' outlined by Cutter in his _Rules for a Dictionary Catalog_ ([1904: 12](#cutter04)). Borgman ([1996](#borgman96): 495) notes that Cutter's objects were based on 'a rational, positivistic approach, and not on direct study of how people formulate questions and seek information'. Svenonius ([2000: 18-20](#svenonius00)) suggests that a fifth objective of navigate should be added to these four, where navigate is about moving through the bibliographic universe, as expressed in a database, to find works related to a given work. She bases this on what research into information seeking behaviour tells us users want to do, and on the fact that '_the bibliographic codes of rules used to organise documents assume its existence_' ([Svenonius 2000](#svenonius00): 19).

There has been an enormous amount of research into information behaviour, including information seeking, published since the model was developed as well as prior to it. There are, for example, seventy-two theories of information behaviour documented in Fisher _et al._'s _Theories of information behavior_ ([Fisher _et al._ 2005](#fisher05)). This research needs to be taken into account when considering the user tasks. However, such research does not seem to be reflected in the IFLA Study Group ([1998](#ifla98)) report, and research into the functional requirements model, as well as practitioner discussions on the large international electronic mailing lists such as AUTOCAT, RDA-L, RDA Australia, and FRBR-L, does not pay much attention to information behaviour research either, even when discussing the user tasks.

Weinheimer, a frequent contributor to these lists, emphasises that we do not actually know what it is that library catalogue users want to do, and that we should ask them, in order to ensure that we provide the kinds of approaches and services that they require. (Weinheimer's collected list posts can be found on his blog [First Thus](http://blog.jweinheimer.net/)). There is limited support for this perspective from other contributors who argue that they do, in fact, know what users want, and that the user tasks are accurate. This lack of attention to information behaviour research, as well as to information seeking, information retrieval and findability research, is not encouraging and is unlikely to promote or facilitate the effective development of library catalogues.

A compilation of reports on digital information seeking behaviour is available ([Connaway and Dickey 2010](#svenonius00)), but the reports are limited to English language and have an Anglo-American focus. There is also a substantial body of research into library catalogue use and information seeking in other languages and countries (see, for example, [Bade 2012](#bade12) or [Ibekwe-SanJuan 2012](#ibekwe12)).

## Users and library catalogues

The functional requirements for bibliographic records model user tasks need to be reconsidered and re-weighted in the light of what information behaviour research tells us users are actually doing. It is not clear whether the model adequately provides for current and future functional requirements for bibliographic records. The model and its user tasks will determine the functions of the catalogue records when implemented through cataloguing codes. The tension between user needs on the one hand, and cataloguing codes and functionality on the other have been identified by several authors (for example, [Bade 2012](#bade12); [Hoffman 2009](#hoffman09); [Revelli 2012](#revelli12); [Bianchini 2010](#bianchini10)). While it is not the intention here to discuss failings or deficiencies of library catalogues themselves, it is difficult to avoid some mention of them, as this is where users will encounter the model.

Library and information science is ostensibly user-focused, but library cataloguing can be somewhat lacking in this respect ([Hoffman 2009](#hoffman09)). Library catalogue users are generally familiar with online Web searching, and research shows users '_still like Google_' ([Fast and Campbell 2005](#fast05)), and that they '_don't think, [they] click_' ([Novotny 2004](#novotny04)) when using library catalogues and databases. Hoffman ([2009](#hoffman09)) asks who is responsible for meeting library catalogue users' needs and what the _right way_ is to do this. An OCLC report on what users want from online catalogues found that '_The end user's experience of the delivery of wanted items is as important, if not more important, than his or her discovery experience_' ([Calhoun _et al._ 2009: v.](#calhoun09)). Sadeh ([2007](#sadeh07)), Merčun and Žumer ([2008) and Žumer (](#merc08)[2008](#zumerc08)) investigated the changes in approach that are needed for this _new generation_ of library catalogue users. It is easy to see users moving further and further towards Google-style interfaces, interactive result lists, keyword access and instant availability, and away from the formal approaches offered and facilitated by library catalogues.

The persistent use of the initialism FRBR has provided an overly-convenient handle for the model, and its meaning has become somewhat diminished in the process. It can be argued that the model is both too complex and too simple: too complex for many resources, and too simple for others. It may not be sophisticated enough to encompass the developing new forms of information resource and the relationships between them.

There are limited numbers of information resources that exist in more than one manifestation and require a complex description and relationship structure such as that implied by the model. Bennett _et al._ ([2003](#bennett03)) estimate that 78% of works in WorldCat (the largest bibliographic database in the world) have only one manifestation. Smiraglia ([2008](#smiraglia08)) notes that the majority of works exist in only one instantiation (where instantiation refers to multiple iterations of an informative object over time) but that a substantial proportion '_generate instantiation networks through mutation and derivation_' and that this could be as high as two-thirds of all works ([Smiraglia 2008: 10](#smiraglia08)). His figure for WorldCat is 30%, and he notes that there is likely to be more instantiation in specialised collections than in large collections such as OCLC's WorldCat.

## User testing

As discussed, the functional requirements for bibliographic records model states its focus on users and presents defined user tasks. However, there was no widespread user testing or user evaluation of the model during its development or before publication. Development was instead determined by the collective expert knowledge of the working group and associated commentators and experts. This was done '_for both expediency and practicality, given the international scope of the study and the expected timeframe_' ([Madison 2005](#madison05): 29). While this is understandable, it is perhaps a little unfortunate. Zhang and Salaba ([2009a](#zhang09a), [2009b](#zhang09b)) used the Delphi method to identify research needs around the model. They noted that user research was one of the '_top issues for FRBR development_' ([2009a: 251-2](#zhang09a)) and that the Library of Congress Working Group on the Future of Bibliographic Control ([2008: 29](#library08)) called for more '_FRBR-related_' user testing before further development of Resource Description and Access.

On the other hand, many studies have evaluated the model in relation to existing library catalogues to determine whether it is useful in terms of the desired functionality of bibliographic records and what advantages it might offer (for example, [Ayres 2005](#ayres05); [Bennett _et al._ 2003](#bennett03); [Rajapatirana and Missingham 2005](#rajapatirana05); [Taylor 2007](#taylor07); [Zhang and Salaba 2007](#zhang07), [2009a](#zhang09a)).

Pisanski and Žumer  ([2007](#pisanski07), [2008](#pisanski08), [2010a](#pisanski10a), [2010b](#pisanski10b), [2010c](#pisanski10c), [2012](#pisanski12)) were the first to explore the extent to which the model reflects the user's understanding of the relationships between bibliographic entities found in a library catalogue. Their most recent research indicates that it is a recognisable model of the bibliographic universe from the perspective of the academic library catalogue user, with no other alternative model being obviously preferred ([Pisanski and Žumer  2012](#pisanski12)). They used variations of the model developed from an earlier phase of their research to explore users' understandings, but note that these deal only with textual parts of the bibliographic universe and that further research is needed for other types of resource. Zhang and Salaba's ([2012](#zhang12)) article on users' understanding of '_FRBR-based catalogs_' draws on research from 2007 and more recent research involving academic library users.

Much of this user testing seems to start from the assumption that the model is an appropriate and useful representation of the bibliographic universe. Indeed, as the model is the underpinning of Resource Description and Access, it is a given, at least for the moment, in terms of library catalogue development. Testing then compares users' understanding with the model's depiction of the entities, attributes and relationships of the bibliographic universe. This is not necessarily an ideal approach, given the changing information landscape and resources, and the changes over the past fifteen or so years in users' understanding of and engagement with information resources. The model may be right, in the sense that it will usefully depict the bibliographic universe circa 1997 and the entities within it ([Carlyle 2006](#carlyle06); [Westbrook 2006](#westbrook06)), but it is not necessarily the best possible model for the development of library catalogues, and it is not the only way of considering the information resources that libraries deal with.

Despite this user testing, therefore, the concern is whether the model adequately encompasses the wide range of information resources that are found in library catalogues (not just textual resources); whether user testing in academic libraries is sufficiently representative of users generally; to what extent the users' information behaviours are determined by what they know of library catalogues already; and the extent to which research from 2007 can be considered current when the information landscape and users' engagement online is changing so rapidly.

## What are the alternatives?

There are alternatives to the functional requirements model. Nicholas Carr's ([2010](#carr10)) contention in _The Shallows_ is that the Internet has altered the ways our brains work, making us less inclined to deep reading, more inclined to a constant stream of information through which we dip and skim. The Internet, he notes, has become the communication and information medium of choice and its existence has changed how we function in the information environment. Documentary forms are also changing: older forms are being supplanted, digital formats are proliferating, patterns of resource creation and generation are shifting, publishing is becoming a faster and more fluid process where authors have a wider range of methods of publication. These points are also made by Eco, Carrière and de Tonnac ([2012](#Eco12): 39 and passim.). '_There's no technological constraint on perpetual editing, and the cost of altering digital text is basically zero. As electronic books push paper ones aside, movable type seems fated to be replaced by movable text_' ([Carr, 2011](#carr11)). This begs the question of how we determine a particular Work when it may be in a state of constant change.

Fattahi ([1997](#fattahi97)) discusses the relevance of cataloguing principles to the online environment in his dissertation and in subsequent publications, including those on superworks ([2010](#fattahi10)). In 2000, Svenonius suggested that the role of the bibliographic record in a digital environment was '_not yet clear. Especially unclear is what exactly a bibliographic record should describe_' ([2000: 64](#svenonius00)). The functional requirements model itself continues to be evaluated and discussed, with a second special issue of Cataloging and Classification Quarterly devoted to it published in 2012\. Current discussion ranges from extensions of the model (in particular, the object-oriented FRBRoo), to evaluations of implementations, to discussions of the model in relation to cataloguing codes and to the Semantic web.

Other, albeit less developed, models consider the entities in the bibliographic universe. A substantial body of research examines notions of works, texts and documents and their inter-relationships (e.g., [Beard 2008](#beard08); [Francke 2005](#francke05); [Gunnarsson 2004](#gunnarsson04); [Nunberg 1996](#nunberg96); [Skare 2009](#skare09); [Smiraglia 2001](#smiraglia01)), and textual criticism and scholarly editing (e.g., [Tanselle 1989](#tanselle89), [2001](#tanselle01); [Eggert 2009](#eggert09)). There are also distinct bodies of research into the specific needs of music (notably by Vellucci) and audio-visual materials (notably by Yee).

There are models that provide different ways of looking at the bibliographic universe or parts of it, such as those of Taniguchi ([2002](#taniguchi02), [2003](#taniguchi03)) on expression-level entities, and Murray and Tillett ([2011](#murray11)). The latter suggest four principles on which to base 'cultural heritage resource description': observations, complementarity, graphs and exemplars ([Murray and Tillett 2011](#murray11): 171). They incorporate the functional requirements model into graphs of relationships between cultural heritage resource descriptions, noting that

> the things of interest in cultural heritage institutions keep changing and may require redefinition, aggregation, disaggregation, and re-aggregation. E-R [entity-relationship] and OO [object oriented] modelling as usually practiced are not designed to manage the degree and kind of changes that take place under those circumstances ([Murray and Tillett 2011](#murray11): 173).

The functional requirements for bibliographic records model does not cover aggregate works well, and arguably does not adequately encompass, or allow for, the variety of manifestations and the relationships between these. Nor does it enable instantiation to be documented clearly. Murray and Tillett explain how the advent of electronic then digital communications media requires more complex resource descriptions of more complex resources. As an example, they note that a work could be

> a monograph… reprinted, and reedited; translated…; supplemented by illustrations from multiple artists; excerpted and adapted as plays, an opera, comic books, and cartoon series; multimedia mashups… and has been the subject of dissertations, monographs, journal articles, etc. ([Murray and Tillett 2011](#murray11): 178).

The move to library linked data as part of the development of the semantic web could also be included as an alternative, as the level of relationship provided for in the resource description framework (RDF) and the structure of the metadata, which is not tied to the notion of a catalogue record, may provide for a better approach. We would, in this case, probably talk about maps of relationships, rather than fixed models.

These alternatives may help us to understand other ways of conceptualising the broader information landscape. We could also ask how useful a fixed conceptual model is in a dynamic information environment where both resources and users are changing. The codex is no longer the main form of information resource. Carr (in [Callil _et al._ 2011: 163](#callil11)) notes that the 'very form of a book seems fated to change as the written word shifts to a new means of production and distribution'. Will such works or texts or documents require a different approach to that of the functional requirements for bibliographic records model? This is a good example of the changes identified by Murray and Tillett: works are no longer self-contained, and even if they were never entirely so, the potential range of relationships that could now exist between a work and any other works is increasing significantly. It is beyond our ability to maintain and update these relationships; it is also questionable whether we should do so. We can anticipate the way information resources might be accessed up to a point, but not comprehensively. It has not been possible to document definitively the more constrained sets of relationships that exist between (largely) printed works, and certainly not within the parameters set by the library catalogue.

We expect the functional requirements for bibliographic records model to do too much. It is only one way of considering the bibliographic universe. It is possible that discussions about knowledge organisation, including classification and genre, are going to provide solutions of more use to users than detailed description of information resources and their relationships. Le Boeuf ([2012: 359](#boeuf12)), who has written extensively on the model, suggests that 'FRBR is the problem, FRBRoo [the object-oriented version] is the solution (or part of it)'. Smiraglia ([2012: 364](#smiraglia12)) notes the 'issues of adherence to a now obsolete entity-relationship model' and suggests that 'the community must begin to think of FRBR as a form of knowledge organization system, rather than as a set of rules for resource description'.

## Conclusion

The IFLA functional requirements for bibliographic records model has been dominant in discussion about cataloguing for the past fifteen or so years, and provides a sophisticated, complex, logical and detailed approach that has provided the basis for developing cataloguing codes. But in the light of changes in the information landscape, in information resources, and in information users and their engagement with digital information resources and information behaviour, we should reconsider the model to see whether it remains a useful way of thinking about the bibliographic universe in the twenty-first century. It is only one possible way of considering the abstraction that is the bibliographic universe, and that universe is changing. There are alternative conceptual models and theories that could be developed. Petrucciani ([2012: 604](#petrucciani12)) notes that '_The step-by-step progression from work to expression to manifestation that the FRBR study depicts–or seems to imply ... is less adequate to the more complex and interrelated phenomena of contemporary cultural production and publishing_'.

There is ongoing debate about what the library catalogue user actually wants the catalogue to do, and whether the model as expressed and implemented via Resource Description and Access will facilitate the user tasks of find, identify, select and obtain. The user tasks themselves need to be re-evaluated in the light of research into information seeking behaviour in the online environment.

We need to ask whether the model determines the functions of bibliographic records such that, in future, this will enable users' needs to be met, and whether the user tasks are sufficiently accurate and comprehensive for the kinds of online information seeking behaviour which users have developed. In particular, we should consider whether the bibliographic universe that is mapped in the model will continue as a recognisable phenomenon and as a valid way of thinking about information resources in the face of ongoing change. Our responses to these questions will have an impact on how we develop library catalogues in the twenty-first century and manage the valuable bibliographic data contained within them.

## Acknowledgements

Thanks go to Dr Steve Wright, Dr Kerry Tanner and Adjunct Associate Prof. Graeme Johanson, Caulfield School of Information Technology, to Emeritus Professor Don Schauder and Cherryl Schauder, and to Dr Peta Wellstead for critical feedback on drafts of this article. Thanks also to the anonymous reviewers for their comments.

## <a id="author"></a>About the author

Amanda Cossham is a PhD student in the Caulfield School of Information Technology, Monash University, Melbourne, Australia, and a lecturer in Information and Library Studies, the Open Polytechnic of New Zealand, Wellington, New Zealand. She can be contacted at [amanda.cossham@openpolytechnic.ac.nz](mailto:amanda.cossham@openpolytechnic.ac.nz)

</section>

<section>

## References

<ul>
<li id="ayres05">Ayres, M.-L. (2005). Case studies in implementing Functional Requirements for Bibliographic Records (FRBR): AustLit and MusicAustralia. <em>Australian Library Journal</em>, <strong>54</strong>(1)
</li>
<li id="bade12">Bade, D. (2012). IT, that obscure object of desire: on French anthropology, museum visitors, airplane cockpits, RDA, and the next generation catalog. <em>Cataloging &amp; Classification Quarterly</em>, 50(4), 316-334
</li>
<li id="beard08">Beard, D. (2008). From work to text to document. <em>Archival Science</em>, <strong>8</strong>(3), 217-226
</li>
<li id="bennett03">Bennett, R., Lavoie, B.F., and O'Neill, E.T. (2003). The concept of a work in WorldCat: an application of FRBR. <em>Library Collections, Acquisitions &amp; Technical Services</em>, <strong>27</strong>, 45-59
</li>
<li id="bianchini10">Bianchini, C. (2010). Futuri scenari: RDA, REICAT e la granularità dei cataloghi [Future scenarios: RDA, REICAT and the granularity of catalogues]. BollettinoAIB: Rivista italiana di biblioteconomia e scienze dell'informatzione, <strong>50</strong>(3), 219-238
</li>
<li id="bianchini09">Bianchini, C., and Guerrini, M. (2009). From bibliographic models to cataloging rules: remarks on FRBR, ICP, ISBD, and RDA and the relationships between them. <em>Cataloging &amp; Classification Quarterly</em>, <strong>47</strong>(2), 105-124
</li>
<li id="borgman96">Borgman, C.L. (1996). Why are online catalogs still hard to use? <em>Journal of the American Society for Information Science</em>, <strong>47</strong>(7), 493-503
</li>
<li id="calhoun09">Calhoun, K., Cantrell, J., Gallagher, P. &amp; Hawk, J. (2009). Online catalogs: what users and librarians actually want. Retrieved from http://www.oclc.org/reports/onlinecatalogs/default.htm
</li>
<li id="callil11">Callil, C., Carr, N., Davis, J., Haddon, M., Morrison, B., Parks, T., ... Barzillai, M. (2011). Stop what you're doing and read this! <em>London: Vintage Books</em>
</li>
<li id="carlyle06">Carlyle, A. (2006). Understanding FRBR as a conceptual model: FRBR and the bibliographic universe. <em>Library Resources and Technical Services</em>, <strong>50</strong>(4), 264-273
</li>
<li id="carr10">Carr, N. (2010). The shallows: how the internet is changing the way we think, read and remember. <em>London: Atlantic Books</em>
</li>
<li id="carr11">Carr, N. (2011, December 31). Books that are never done being written. <em>The Wall Street Journal</em>. Retrieved from http://online.wsj.com/article/SB10001424052970203893404577098343417771160.html
</li>
<li id="special">Special issue: the FRBR family of models. (2012). <em>Cataloging and Classification Quarterly</em>, <strong>50</strong>(5-7)
</li>
<li id="connaway10">Connaway, L.S. &amp; Dickey, T.J. (2010). The digital information seeker: report of the findings from selected OCLC, RIN and JISC user behaviour projects. Retrieved from http://www.jisc.ac.uk/publications/reports/2010/digitalinformationseekers.aspx
</li>
<li id="cutter04">Cutter, C.A. (1904). Rules for a dictionary catalog (4th ed.). Retrieved from http://digital.library.unt.edu/ark:/67531/metadc1048/m1/1/
</li>
<li id="denton">Denton, W. (n.d.). [FRBR entities]. Retrieved from http://www.frbr.org/files/entity-relationships.png
</li>
<li id="denton07">Denton, W. (2007). FRBR and the history of cataloging. In A. G. Taylor (Ed.), <em>Understanding FRBR: what it is and how it will affect our retrieval tools</em> (pp. 35-57). Westport, CN: Libraries Unlimited
</li>
<li id="eco12">Eco, U. &amp; Carrière, J.-C. (2012). This is not the end of the book: a conversation (curated by J.-P. de Tonnac; P. McLean, Trans.). <em>London: Vintage Books</em>
</li>
<li id="eggert09">Eggert, P. (2009). Securing the past: conservation in art, architecture and literature. <em>Cambridge: Cambridge University Press</em>
</li>
<li id="fast05">Fast, K.V. &amp; Campbell, D.G. (2005). 'I still like Google': university student perceptions of searching OPACs and the Web. Paper presented at <em>2004 Conference of the American Society for Information Science and Technology</em>
</li>
<li id="fattahi97">Fattahi, R. (1997). The relevance of cataloguing principles to the online environment: an historical and analytical study. <em>PhD, University of New South Wales</em>
</li>
<li id="fattahi10">Fattahi, R. (2010). From information to knowledge: superworks and the challenges in the organization and representation of the bibliographic universe: Lectio magistralis in library science. <em>Florence, Italy: Casalini Libri</em>
</li>
<li id="fisher05">Fisher, K.E., Erdelez, S. &amp; McKechnie, L.E.F. (Eds.). (2005). Theories of information behavior. <em>Medford, NJ: Published for the American Society for Information Science and Technology by Information Today</em>
</li>
<li id="francke05">Francke, H. (2005). What's in a name? <em>Contextualizing the document concept. Literary and Linguistic Computing</em>, <strong>20</strong>(1), 61-69
</li>
<li id="gunnarsson04">Gunnarsson, M. (2004). From information to document and back again. Paper prepared for PhD class on information access. Swedish School of Library and Information Science, Högskolan i Borås. Retrieved from http://www.adm.hb.se/~mg/oz/ia_report.pdf
</li>
<li id="hoffman09">Hoffman, G. L. (2009). Meeting users' needs in cataloging: What is the right thing to do? Cataloging &amp; Classification Quarterly, 47(7), 631-641.
</li>
<li id="hulme11">Hulme, E. W. (1911). Principles of book classification. <em>Library Association Record</em>, <strong>13</strong>, 354-358, 389-394, 444-449
</li>
<li id="ibekwe12">Ibekwe-SanJuan, F. (2012). The French conception of information science: 'Une exception française'? <em>Journal of the American Society for Information Science and Technology</em>, <strong>63</strong>(9), 1693-1709
</li>
<li id="ifla10">IFLA FRBR Review Group. (2010). FRBR bibliography. Retrieved from http://www.ifla.org/node/881
</li>
<li id="ifla09">IFLA Meeting of Experts on the International Cataloguing Code. (2009). Statement of international cataloguing principles. Retrieved from http://archive.ifla.org/VII/s13/icp/ICP-2009_en.pdf
</li>
<li id="ifla98">IFLA Study Group on the Functional Requirements for Bibliographic Records (1998). Functional requirements for bibliographic records: Final report. Retrieved from http://www.ifla.org/VII/s13/frbr/
</li>
<li id="international12">International Working Group on FRBR and CIDOC CRM Harmonisation. (2012). FRBR object-oriented definition and mapping to FRBRer (version 2.0) (draft). Retrieved from http://www.cidoc-crm.org/docs/frbr_oo/frbr_docs/FRBRoo_V2.0_draft.pdf
</li>
<li id="boeuf05">Le Boeuf, P. (2005). FRBR: hype or cure-all?: introduction. <em>Cataloging &amp; Classification Quarterly</em>, <strong>39</strong>(3-4), 1-13
</li>
<li id="boeuf12">Le Boeuf, P. (2012). Foreword. <em>Cataloging and Classification Quarterly</em>, <strong>50</strong>(5-7), 355-359
</li>
<li id="library08">Library of Congress Working Group on the Future of Bibliographic Control (2008). On the record. Retrieved from http://www.loc.gov/bibliographic-future/news/lcwg-ontherecord-jan08-final.pdf
</li>
<li id="madison05">Madison, O.M.A. (2005). The origins of the IFLA study on functional requirements for bibliographic records. <em>Cataloging &amp; Classification Quarterly</em>, <strong>39</strong>(3/4), 15-37
</li>
<li id="merc08">Merčun, T. &amp; Žumer, M. (2008). New generation of catalogues for the new generation of users: A comparison of six library catalogues. <em>Program: Electronic Library and Information Systems</em>, <strong>42</strong>(3), 243-261
</li>
<li id="murray11">Murray, R.J. &amp;Tillett, B. (2011). Cataloging theory in search of graph theory and other ivory towers: Object: Cultural heritage resource description networks. <em>Information Technology and Libraries</em>, <strong>30</strong>(4), 170-184
</li>
<li id="novotny04">Novotny, E. (2004). 'I don't think I click': a protocol analysis study of use of a library online catalog in the Internet age. <em>College and Research Libraries</em>, <strong>65</strong>(5), 525-537
</li>
<li id="nunberg96">Nunberg, G. (Ed.). (1996). The future of the book. <em>Berkeley CA: University of California Press</em>
</li>
<li id="petrucciani09">Petrucciani, A. (2009). Every reader his work, every work its title (&amp; author): The new Italian cataloguing code REICAT. Paper presented at <em>World Library and Information Congress: 75th IFLA General Conference and Council</em>, 23-27 August, 2009, Milan, Italy. Retrieved from http://conference.ifla.org/past/ifla75/107-petrucciani-en.pdf
</li>
<li id="petrucciani12">Petrucciani, A. (2012). From the FRBR model to the Italian cataloging code (and vice versa?). <em>Cataloging and Classification Quarterly</em>, <strong>50</strong>(5-7), 603-621
</li>
<li id="pisanski07">Pisanski, J. &amp; Žumer, M. (2007). Mental models and the bibliographic universe. Paper presented at Libraries in the Digital Age (LIDA) 2007, Dubrovnik and Mljet, Croatia. Retrieved from http://www.ffos.hr/lida/datoteke/LIDA2007-Pisanski_Zumer.ppt
</li>
<li id="pisanski08">Pisanski, J. &amp; Žumer, M. (2008). How do non-librarians see the bibliographic universe? In C. Arsenault and J.T. Tennis (Eds.), <em>Culture and Identity in Knowledge Organization: Proceedings of the 10th International ISKO conference</em>, 5-8 August, Montreal, Canada (pp. 131-136). Würzburg, Germany: Ergon Verlag
</li>
<li id="pisanski10a">Pisanski, J. &amp; Žumer, M. (2010a). Intuitiveness of Functional Requirements for Bibliographic Records: A study in a broader context: Presented at Seventh International Conference on Conceptions of Library and Information Science: "Unity in diversity". <em>Information Research</em>, <strong>15</strong>(4), paper colis725
</li>
<li id="pisanski10b">Pisanski, J. &amp; Žumer, M. (2010b). Mental models of the bibliographic universe. Part 1, Mental models of descriptions. <em>Journal of Documentation</em>, <strong>66</strong>(5), 643-668
</li>
<li id="pisanski10c">Pisanski, J. &amp; Žumer, M. (2010c). Mental models of the bibliographic universe. Part 2, Comparison tasks and conclusions. <em>Journal of Documentation</em>, <strong>66</strong>(5), 668-680
</li>
<li id="pisanski12">Pisanski, J. &amp; Žumer, M. (2012). User verification of the FRBR conceptual model. <em>Journal of Documentation</em>, <strong>68</strong>(4), 582-592
</li>
<li id="rajapatirana05">Rajapatirana, B. &amp; Missingham, R. (2005). The Australian National Bibliographic Database and the functional requirements for the bibliographic database [sic] (FRBR). <em>Australian Library Journal</em>, <strong>54</strong>(1)
</li>
<li id="revelli12">Revelli, C. (2012). The catalogue (and the cataloguer): A defenseless entity? <em>JLIS.it Italian Journal of Library and Information Science</em>, <strong>3</strong>(1), 1-11
</li>
<li id="sadeh07">Sadeh, T. (2007). Time for a change: New approaches for a new generation of library users. <em>New Library World</em>, <strong>108</strong>(7/8), 307-316
</li>
<li id="skare09">Skare, R. (2009). The notion of text and the notion of document: what difference does it make? <em>Nordlit</em>, <strong>24</strong>, 323-331
</li>
<li id="smiraglia01">Smiraglia, R. P. (2001). The nature of 'a work': implications for the organization of knowledge. <em>Lanham, MD: Scarecrow Press</em>
</li>
<li id="smiraglia08">Smiraglia, R. P. (2008). A meta-analysis of instantiation as a phenomenon of information objects. <em>Culture del Testo e del Documento</em>, <strong>9</strong>(25), 5-25
</li>
<li id="smiraglia12">Smiraglia, R. P. (2012). Be careful what you wish for: FRBR, some lacunae, a review. <em>Cataloging and Classification Quarterly</em>, <strong>50</strong>(5-7), 360-368
</li>
<li id="svenonius00">Svenonius, E. (2000). The intellectual foundation of information organization. <em>Cambridge, MA: MIT Press</em>
</li>
<li id="taniguchi02">Taniguchi, S. (2002). A conceptual model giving primacy to expression-level bibliographic entity in cataloging. <em>Journal of Documentation</em>, <strong>58</strong>(4), 363-382
</li>
<li id="taniguchi03">Taniguchi, S. (2003). Conceptual modeling of component parts of bibliographic resources in cataloging. <em>Journal of Documentation</em>, <strong>59</strong>(6), 692-708
</li>
<li id="tanner10">Tanner, K. J. (2010). Human capital, organisational system and technologies in knowledge-enabled organisational change: Submitted in fulfilment of the requirements for the degree of Doctor of Philosophy, December 2006, Caulfield School of Information Technology, Faculty of Information Technology, Monash University. <em>Saarbrücken, Germany: LAP LAMBERT Academic Publishing</em>
</li>
<li id="tanselle89">Tanselle, G. T. (1989). A rationale of textual criticism. <em>Philadelphia, PA: University of Pennsylvania Press</em>
</li>
<li id="tanselle01">Tanselle, G. T. (2001). Thoughts on the authenticity of electronic texts. <em>Studies in Bibliography</em>, <strong>54</strong>, 133-136
</li>
<li id="taylor07">Taylor, A. G. (Ed.). (2007). Understanding FRBR: what it is and how it will affect our retrieval tools. <em>Westport, CN: Libraries Unlimited</em>
</li>
<li id="tillett04">Tillett, B. (2004). What is FRBR? A conceptual model for the bibliographic universe. Retrieved from http://www.loc.gov/catdir/cpso/whatfrbr.html
</li>
<li id="westbrook06">Westbrook, L. (2006). Mental models: a theoretical overview and preliminary study. <em>Journal of Information Science</em>, <strong>32</strong>(6), 563-579
</li>
<li id="wilson68">Wilson, P. (1968). Two kinds of power: an essay in bibliographical control. <em>Los Angeles, CA: University of California</em>
</li>
<li id="zhang07">Zhang, Y. &amp; Salaba, A. (2007). Critical issues and challenges facing FRBR research and practice. <em>Bulletin of the American Society for Information Science and Technology</em>, <strong>33</strong>(6), 30-31
</li>
<li id="zhang09a">Zhang, Y. &amp; Salaba, A. (2009a). Implementing FRBR in libraries: key issues and future directions. <em>New York: Neal-Schuman</em>
</li>
<li id="zhang09b">Zhang, Y. &amp; Salaba, A. (2009b). What is next for Functional Requirements for Bibliographic Records? A Delphi study. <em>The Library Quarterly</em>, <strong>79</strong>(2), 233-255
</li>
<li id="zhang12">Zhang, Y. &amp; Salaba, A. (2012). What do users tell us about FRBR-based catalogs? <em>Cataloging &amp; Classification Quarterly</em>, <strong>50</strong>(5-7), 705-723
</li>
</ul>

</section>

</article>