<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Empowering interviews: narrative interviews in the study of information literacy in everyday life settings

#### [Johanna Rivano Eckerdal](mailto:johanna.rivano_eckerdal@kultur.lu.se)  
Lund University Department of Arts and Cultural Sciences,  
hus Josephson, Biskopsgatan 7,  
223 62 Lund, Sweden

#### Abstract

> **Introduction**. This paper presents a way to design and conduct interviews, within a sociocultural perspective, for studying information literacy practices in everyday life.  
> **Methods**. A framework was developed combining a socio-cultural perspective with a narrative interview was developed. Interviewees were invited to participate by talking and using visual tools (deck of cards of information sources, maps, and horizons of information sources). Five young women were interviewed about how they chose contraceptives and how they in the process engaged in information literacy practices.  
> **Analysis**. A qualitative analysis was carried out, using transcripts of interviews, visual tools supplemented with observations, field notes and transcripts of conversations during counselling meetings before the interviews.  
> **Results**. The interviews became “empowering” arenas, providing opportunities for interviewees and interviewer to reflect on and ponder over what it means to choose and use a contraceptive. The information literacy practices of evaluating information sources about contraceptives became a part of a story to tell about what it means to be a young woman, expressing oneself as leading a sexually active life.  
> **Conclusion**. The proposed narrative interview design, in which interviewees are invited to take active part in the interview, proves itself fruitful within a sociocultural perspective.

<section>

## Introduction

Considerable time and effort are put into planning and designing research so that choices concerning theory and method fit with each other and with the research questions. When in progress, and particularly within qualitative approaches, research design most certainly will be adjusted and timetables altered. When research is finally reported, these painstaking and time-consuming stages often boil down to a couple of sentences stripped of meaning that convey their profound impact on the results. One of the exceptions is the discussion in Carey, McKechnie and McKenzie ([2001](#Carey01)) of different ways of gaining access to participants in ELIS research. Interviews stand out as a dominant research method within qualitative approaches, used to gain insight into how users make sense of e.g. information interactions in their everyday life. However, methodological discussions about interviews are scarce (see [Talja, 1999](#Talja99); [McKechnie, Julien, Pecoskie and Dixon, 2006](#McKechnie06) for exceptions). This is particularly surprising in research with a sociocultural perspective on information literacy, as proponents of this theoretical stance have questioned the use of interviewing as a research method ([Säljö, 1997](#Saljo97); [2000](#Saljo00)). In this paper an interview method designed to overcome the shortcomings of traditional interviews is presented and discussed. The paper thus contributes to and advocates methodological discussions in information literacy research and in LIS at large.

Several policy documents have been produced over the years to explain or promote information literacy. Influential ones include the American Library Association’s report ([1989](#American89)) and the Alexandria proclamation ([Garner, 2006](#Garner06)). These emphasize the relevance of information literacy for three arenas: learning during the first schooling years, continuous learning during work-life and possible participation as a citizen and exercising one’s civic rights and obligations ([Lloyd and Williamson, 2008](#Lloyd08)). Information literacy has also become a research area within LIS, mostly focusing on the first two arenas mentioned above: the educational sector (e g [Simmons, 2005](#Simmons05); [Limberg, Alexandersson, Lantz-Andersson and Folkesson, 2008](#Limberg08); [Sundin and Francke, 2009](#Sundin09); [Lupton and Bruce, 2010](#Lupton10)) and work-life ([Tuominen, Savolainen and Talja, 2005](#Tuominen05); [Lloyd, 2006](#Lloyd06); [2009](#Lloyd09); [Moring, 2010](#Moring10)). Research from everyday life settings though scarce is necessary to gain more insight into the complex and ambiguous notion of information literacy. Studies of information literacy in everyday life settings are wanting ([Hultgren, 2009](#Hultgren09); [Aarnitaival, 2010](#Aarnitaival10); [Lloyd, Lipu and Kennan, 2010](#Lloyd10); [Williamson, 2010](#Williamson10)) and young people’s own descriptions of information literacy practices are missing. Giving young women opportunities to tell their stories about choosing contraceptives, what sources they used and how they evaluated them is one way of making such a contribution.

In order to discuss methodological issues, the paper reports on and discusses the design of a study on information literacy in one everyday life arena, that of sexual health in Sweden. Sexual health is of importance for both individuals and society as the use of contraceptives contributes to avoiding unintended pregnancies and sexually transmitted infections, STI:s. A focal point of source criticism was chosen in order to operationalize information literacy, specifically how young women use and evaluate information sources before choosing a contraceptive. The paper thus also contributes to LIS by referring to a study on young people’s evaluation of information concerning their sexual and reproductive health, a missing arena within LIS ([Pierce, 2007: 83](#Pierce07)). However, it is challenging to conduct an empirical study on this topic. What was ethically and practically feasible was to conduct interviews.

The aim of this paper is to discuss how to design and conduct interviews for studying information literacy in everyday life within a sociocultural perspective. Accordingly a theoretical framework is constructed in which this perspective is combined with a narrative interview method. The framework is then used in an interview study on young women’s information literacy practices as expressed in stories about what information sources they used and how they evaluated the sources before making a choice. Several steps were taken to invite the interviewees to become active participants in the interview. These included the use of visual tools mapping the information sources used and a deck of cards with suggestions of information sources that could function as memory triggers during the interview. Choosing a contraceptive can be understood as an expression of the malleability of (sexual) identities in late modernity ([Giddens, 1992](#Giddens92); [Eckerdal, 2011a](#Eckerdal11a)). Thus, telling their stories about choosing a contraceptive is a way of creating and expressing part of their identities. Hence, a secondary aim of the article, closely related to the main aim, is to discuss how the choice of contraceptives becomes a part of their lives, their identities; a story to tell. [[1]](#footnote1)

In the next section a critique of interviewing as a research method, as raised by one of the proponents of the sociocultural perspective on learning is presented. It is followed by the exposition of an interview method drawing on narrative interviews proposed by Mishler ([1986a](#Mishler86a), [1986b](#Mishler86b)) and the narrative interview technique proposed by Jovchelovitch and Bauer ([2000](#Jovchelovitch00)), previously used within LIS by Bates ([2004](#Bates04)), Hultgren ([2009](#Hultgren09)) and Aarnitaival ([2010](#Aarnitaival10)) as well as the visual tools used during the interviews. Empirical examples from the young women’s interview stories are given. The women are engaged in learning and the learning concerns what contraceptive to use and what it means for them: how the contraceptive is chosen, how it is told and understood and how it becomes part of their identities. They learn to be young women choosing to use and continuing using contraceptives. The range of sources available to them to legitimate this use becomes part of their stories of how being a contraceptive user makes sense to them.

## Sociocultural critique of interviews as research method

Using interviewing as a research method raises problems within a sociocultural perspective on information literacy. This perspective brings to the fore an awareness of the situatedness of learning: where and when learning occurs is critical. What is said during an interview is to a high degree shaped by being formulated in the interview situation. What does that specific conversation say about other situations, about the situations that are the topic of the interview? Although interviews are used in sociocultural research on information literacy, they are often combined with other methods such as observations, allowing researchers to participate in the studied practice (e. g. [Lloyd, 2006](#Lloyd06); [Limberg et al., 2008](#Limberg08); [Lloyd, 2009](#Lloyd09); [Sundin and Francke, 2009](#Sundin09); [Moring, 2010](#Moring10)). In a number of studies, mostly within educational science, researchers follow students in the classroom using video to record the situations ([Lantz-Andersson, 2009](#Lantz09); [Lundh, 2011](#Lundh11)).

Roger Säljö, professor of education and educational psychology and advocate of the sociocultural perspective on learning, has made some important comments on interviewing as a research method ([1997](#Saljo97); [2000: 115 pp.](#Saljo00)). He makes us aware of the difference between thinking and talking. We do not necessarily say what we think, but in everyday way of talking (“What are you thinking about, come on tell me!”) and also in research (interviews as ways to come to know what informants think) the difference is often neglected ([2000: 115 pp.](#Saljo00)). It is what people say, the accounting practices, which constitute the unit of analysis ([Säljö, 1997: 178](#Saljo97)). Consequently the distinction between thinking and communicating is important. It is communication that is researchable.

Another comment by Säljö concerns the interview as context for communication ([1997: 173](#Saljo97); [2000: 115](#Saljo00)). If someone agrees to be interviewed, a conversation around the topic of the interview will evolve, it would be rude and offensive not to answer questions once engaged in the conversation. As a consequence Säljö proposes to study and analyse accounts as “attempts at communicating in situated practices”([1997: 188](#Saljo97)). Accounting practices are viewed as cultural tools that people learn to use when engaging in communicative practices ([1997: 182](#Saljo97)). If interviews were recognized as situated communicative practices they would be understandable from a sociocultural perspective on learning (cf [1997: 180](#Saljo97)). In the following an attempt to do so is presented.

## Empowering interviews

The design of an empirical study including interviews must address also the socioculturally oriented methodological critical objections. Contributions meeting objections to interviewing as a research method, while maintaining the interview format, are presented below, prior to outlining the design used in the present study.

### Research interviews as narratives

The American psychology researcher Elliot Mishler ([1986a](#Mishler86a); [1986b](#Mishler86b)) objected to the standard practice of interviewing, and suggested narrative interviewing as a valuable research method. His work has inspired research within LIS. McKenzie ([2002: 32](#McKenzie02)) refers to Mishler, Clark, Ingelfinger and Simon ([1989](#Mishler89)) as an example of literature on practitioner-patient communication and Park ([1994](#Park94)), Barry ([1995](#Barry95)), and Squires ([1997](#Squires97)), draw on Mishler ([1979](#Mishler79)) to strengthen arguments about the need for contextualizing research. Mishler’s alternative approach focuses on the interview as a form of discourse ([1986a: vii](#Mishler86a)). Instead of trying to adapt interviewing to meet criticism about its inexactness and ‘unscientificness’, the so-called weaknesses of the method are appreciated as strengths. Mishler puts forth four important propositions: “(1) interviews are speech events; (2) the discourse of interviews is constructed jointly by interviewers and respondents; (3) analysis and interpretation are based on a theory of discourse and meaning; (4) the meanings of questions and answers are contextually grounded.” ([1986a: ix](#Mishler86a))

According to these propositions there are specific matters to take into consideration. Mishler’s first concern is about creating a correct representation of the speech event. He argues, in 1986, in favour of tape recording interviews, creating a transcript that interprets the sound as text. Today recording is normal procedure. However, to define the act of transcribing as an interpretation, that is an analytical act, is still necessary and important. The transcript is not a representation of reality but an interpretation and a representation of the conversation, thus a transformation ([1986a: 47pp.](#Mishler86a)). Secondly, Mishler draws attention to the interview as a joint construction by interviewer and interviewee. It is possible to focus on how the interaction may change by involving the interviewee in the process. The questions asked and the answers given are negotiated through restatings and reformulations. In the following a longer extract from the interview with Sofia will illustrate this.

Early in the study referred to here it came up that it is not at all self-evident neither what it means to use a contraceptive nor how a contraceptive is understood. When these questions were asked some of the young women were confused. Like in this example from the interview with Sofia:

> Johanna: Mm, what does it mean to you that you are using a contraceptive?  
> Sofia: I don’t think that I understand the question.  
> J: No. Well, yes, you have started to use a contraceptive  
> S: Yes, mm  
> J: does it have, well does it mean something to you that you can use a contraceptive. I’m thinking about perhaps also what you told me about that you actually are experiencing quite some side effects, and you said that you have to have something.  
> S: Yes. You, you, I don’t want to get pregnant.  
> J. No  
> S: So it’s important for me to have something, to have some protection.  
> J: Mm  
> S: Because, things happen and if you don’t have any protection it might, you might end up pregnant or  
> J: Mm  
> S: in one way or another and I’m not. I, I have many are sort of that they don’t really care because it’s just like, If I get pregnant I can always have an abortion (is a very easy way to see it). But I have been in situations myself when I have thought that I have been that. And I have always been kind of sure that I don’t want a baby like when I’m this young  
> J: Mm  
> S: But anyways, it’s a really tough decision to make  
> J: Mm  
> S: And you don’t want to have to do it  
> J: Mm  
> S: unnecessarily. So of course, of course you protect yourself  
> J: Mm  
> S: It’s like, it’s obvious.  
> (Interview with Sofia)

Reformulating my question helped Sofia to start telling about and understanding what role the use of a contraceptive had in her life. Sofia continues that it is a relief and that: ”it doesn’t matter if my partner and I have sex because it’s like, I’m protected and it’s nothing we have to think about. It’s sort of no big deal. It seems very natural.” For Sofia the contraceptive technology opens up a way for her to understand herself as being sexually active without any connection to fertility, something she understands as natural.

When regarding research interviews from a narrative point of view, interview questions should not be viewed as stimuli that trigger typical behaviours of respondents ([Mishler, 1986a: 54pp](#Mishler86a)). By recognising that understandings are created during interviews the importance of having a transcript to follow this unfolding is obvious. In the transcript from the interview with Paula it is possible to follow how her understanding of being a contraceptive user develops in the story she tells:

> Johanna: Perhaps you already talked a bit about that when I asked you about your experiences from the other, from using condoms but anyways if, what does it imply to you to use a contraceptive? What does it mean?  
> Paula: Well, that I am safe from, not being pregnant or, well with condoms it was the diseases  
> J: Mm  
> P: It’s always quite nice to skip that.  
> J: Mm  
> P: and, well. That’s the biggest  
> J: Mm  
> P: That’s what you think about most of all, I think  
> J: Mm  
> P: Skip diseases, (skip being) pregnant  
> J: Yes. And for your partner?  
> P: I don’t know really but I think it’s the same too, it’s absolutely the biggest, not becoming pregnant  
> J: Mm  
> P: Disease (you can always) or, well, cure, part from some, those big like H, HIV and AIDS  
> J: Mm  
> P: But part from that I suppose  
> J: Mm  
> P: this is the biggest. We’re only eighteen you know.  
> J: Yes.  
> P: It’s not the right time  
> J: No, and what do you think that a contraceptive is, what is it, what?  
> P: A personal bodyguard. (Giggle)  
> J: Ok, yes  
> P: It’s a protection  
> J: Mm  
> P: An aid. It’s, yes, that’s it.  
> (Interview with Paula)

Paula’s answer that the contraceptive, in her case an implant, for her is a personal bodyguard comes rather quickly, and she giggles after having said it. It is both metaphor and reality elegantly summing up how she understands herself being a contraceptive user. According to her story she is protected from pregnancy, a circumstance that would make her life different from what she wants it to be right now.

In the standard practice of interviewing in which analysis stands for reporting the statements collected through the interview-device in a correct way, theoretical presuppositions are often not taken into account (cf [Kvale, 1996](#Kvale96)) . In his third proposition, Mishler ([1986a](#Mishler86a)) draws attention to the theoretical basis of the interpretation of the interview, and he also gives examples of different ways to theorize interviews as narratives ([1986a: 66-116](#Mishler86a)). He presents them in three categories. In the first category, called structural analysis, six linguistic units are analysed for the temporal order of events and for understanding how the story relates to the real events that they refer to (ibid: 77). The analysis in the second category aims at finding coherence in the story and using a taxonomy of coherence (ibid: 87). The third category focuses on awareness of the context, specifically on the interview as a context, as part of the analysis (ibid: 96). The analysis is an interpretation in which contextual issues both regarding the interview situation and the role of the interviewer need to be addressed ([Mishler, 1986a: 96](#Mishler86a)).

The final proposition states that meaning is contextually grounded ([Mishler 1986a: 117](#Mishler86a)). The conversation is a joint achievement. Within a sociocultural mode of expression, the research interview is viewed as a situated communicative practice. This proposition allows us to address the question of power inequality in interview situations. The interviewer is invested with the power to lead conversations and to analyse afterwards (ibid: 117). Instead of disguising power issues, Mishler suggests ways to address them by designing interviews that empowers interviewees (ibid: 118). His suggestion is to “encourage them to find and speak in their own ‘voices’ ” (ibid: 118), that is letting them tell their stories; “produce narrative accounts” (ibid: 119). The telling of stories may also have effects outside the interview itself as it may prepare for activities (ibid: 119).

Mishler suggests forms of interviewing in which interviewees are considered to be competent observers of their own experiences ([1986a: 123](#Mishler86a)) and research collaborators (ibid: 126), the interview being a situation in which learning takes place (ibid: 130).

### Narrative interview technique

Jessica Bates uses narrative interviewing, within LIS ([2004](#Bates04)). She suggests that it provides “a particularly useful methodological framework for studies of everyday life information-seeking behaviour” (ibid: 15). Narrative interviewing presents fewer problems than traditional interviewing (ibid: 16). Bates uses a narrative technique outlined by Jovchelovitch and Bauer ([2000](#Jovchelovitch00)). [[2]](#footnote2) Telling stories is a fundamental part of how people communicate ([Jovchelovitch and Bauer, 2000: 58](#Jovchelovitch00)). Stories are constructed along two dimensions, a chronological one showing in which order events occur, and a non-chronological one, showing how events are put together into meaningful wholes: a plot (ibid). This approach fits well with the aim of investigating different information sources used over a period of time and the meaning ascribed to the sources by the young women. When telling a story you follow a narration schema with certain characteristics, for example what is included as relevant in the story. Thus the storytellers’ view of the world is emphasized. It allows for the storyteller to formulate the story in her own words. Focusing on the narration allows informants to have a higher degree of impact on the gathered material than with a more traditional question-answer schema (cf [Mishler, 1986a](#Mishler86a)). By emphasizing a narrative interview method, it is recognized that language is not a neutral but a constitutive tool. Jovchelovitch and Bauer ([2000: 60](#Jovchelovitch00)) stress that the interviewer should be careful in adapting to the language used by the informant. This is in line with Mishler’s awareness of the power relations between participants in an interview.

The narrative interview, as presented by Jovchelovitch and Bauer ([2000](#Jovchelovitch00)), is described in terms of phases shown in Table 1 below. One of the phases focuses on allowing the interviewee to tell the story. At that time it is important to encourage her to continue, without interrupting. A new phase unfolds as the narration is told, in which questions are allowed. When preparing the interview, the interviewer must transform analytical questions into questions that work in an interview situation. The interviewer must adapt to the way the informant is phrasing his or her experiences.

<table><caption>

Table 1\. Basic phases of the narrative interview (adapted from [Jovchelovitch and Bauer, 2000: 62](#Jovchelovitch00))</caption>

<tbody>

<tr>

<th>Phases</th>

<th>Rules</th>

</tr>

<tr>

<td>Preparation</td>

<td>Exploring the field</td>

</tr>

<tr>

<td></td>

<td>Formulating analytical questions</td>

</tr>

<tr>

<td>1 Initiation</td>

<td>Formulating initial topic for narration</td>

</tr>

<tr>

<td></td>

<td>Using visual aids</td>

</tr>

<tr>

<td>2 Main narration</td>

<td>No interruptions</td>

</tr>

<tr>

<td></td>

<td>Only non-verbal encouragement to continue story-telling</td>

</tr>

<tr>

<td></td>

<td>Wait for the coda</td>

</tr>

<tr>

<td>3 Questioning phase</td>

<td>only ‘What happened then?’</td>

</tr>

<tr>

<td></td>

<td>No opinion and attitude questions</td>

</tr>

<tr>

<td></td>

<td>No arguing or contradictions</td>

</tr>

<tr>

<td></td>

<td>No why-questions</td>

</tr>

<tr>

<td></td>

<td>Analytical into interview questions</td>

</tr>

<tr>

<td>4 Concluding talk</td>

<td>Stop recording</td>

</tr>

<tr>

<td></td>

<td>Why-questions allowed</td>

</tr>

<tr>

<td></td>

<td>Memory-protocol immediately after interview</td>

</tr>

</tbody>

</table>

## Design of the study

Both Mishler ([1986a](#Mishler86a)) and Jovchelovitch and Bauer ([2000](#Jovchelovitch00)) address criticism of interviewing. Mishler’s propositions, fitting well with a sociocultural perspective on information literacy, were met in the design of the study referred to here. The interviews are viewed as speech events and the interactions as joint achievements. [[3]](#footnote3) The discourse of the interviews is viewed as jointly constructed by the interviewer and the interviewees. Mishler’s third proposition about theories of discourse and meaning links well with the awareness within a sociocultural perspective on information literacy about both context and language being important in interactions. Contextual awareness is also emphasized in the fourth proposition. In the present study it meant recognizing that the interview is a specific communicative practice, bringing that awareness into both its design and analysis. The drawing of maps and horizons is included in the design to invite interviewees to actively participate and engage. It is also acknowledged that the stories told during the interview might not have been previously told. Their potential for change is thus taken into account and the importance that this telling may have for the narrator’s identity is brought into the analysis as well. A consequence of this recognition is that the material from the interviews should be supplemented with other empirical material: participatory observations at youth centres, field notes and transcripts of conversations during counselling meetings. By doing so a richer picture of the different situations that are talked about during the interviews emerges.

The structure of a narrative interview presented by Jovchelovitch and Bauer ([2000](#Jovchelovitch00)) is, with some modifications, used as a guide to design the interviews. The following structure is used: An initial phase, during which I introduce myself, the topic of the interview and the different aspects of the interview, is followed by a second phase in which the story is told. During this phase no questions are asked, the only interaction being such that encourage the interviewee by saying things like “I see” or “yes”. However, I was sometimes asked by the interviewees to clarify what they were supposed to do and I then answered these questions. A third phase follows, a questioning phase in which a set of questions brought to the interview is used and the story just told is followed up. The final phase is about ending the interview and some small talk before saying good-bye.

During the interviews I followed the four phases in a flexible manner; that is adapting to how the young women told their stories. The stories unfolding during the second phase varied in detail and length and sometimes stories were told as answers to questions asked in the third phase. Questions about what it means to use a contraceptive and what a contraceptive is turned out to be particularly productive in this sense. As interviews are considered to be joint achievements between interviewers and interviewees, the stories told build on and develop from the questions asked as we together tried to make sense of what choosing a contraceptive meant for the interviewees. A story does not only unfold when it fits with certain characteristics, without interruption, but also when the women continue to talk about their views or experiences of something, answering questions asked in the interview situation. It was foreseen, in line with Mishler ([1986](#Mishler86)), that the interview might be the first time the young women told a story about choosing a contraceptive and even the first time for considering it a story relevant to tell. The interview might then be an opportunity for recognizing the variety of information sources about contraceptives and consequently the variety of understandings of contraceptives that are possible. In the analysis the interviews were considered as a whole and no part was treated as more “true” or valid than another.

### Visual tools

To further enhance the interviewees’ participation during the interview’s second phase I lean on a methodological tool included in a participatory design study within Science and Technological Studies. It is about creating a map of a place and the activities taking place there, as described by Elovaara, Igira and Mörtberg ([2006](#Elovaara06)). Jovchelovitch and Bauer ([2000](#Jovchelovitch00)) also suggest the use of visual aids in the initiation phase of the interview (Table 1). Elovaara, Igira and Mörtberg ([2006](#Elovaara06)) organized a workshop in which a group of women jointly created a map of a workplace and the activities taking place there. The idea was “talking by doing”. The group, researchers included, created the physical map and discussed the workplace and its activities at the same time. Artefacts and important relations were also included in the map. By referring to the map the researchers could ask questions and discuss with other participants. I found the idea of creating a physical map in combination with narrative interviewing convincing. The map is created, immediately visible to both researcher and informant. The interviewees are asked to tell the story about how they chose their contraceptive, what sources were used and to draw a map about the different information sources they used before making the choice. The map in this case is not of one physical location. Some young women chose to make a drawing of the different sources they had used while others wrote them down. Anna made a drawing of the information sources she used:

<figure>

![Figure 1](../pC10fig1.png)

<figcaption>Figure 1</figcaption>

</figure>

In addition, the interviewees are asked to draw an information source horizon of their information sources to increase the use of the map during the interview. Drawing information horizon maps is a method developed by Dianne Sonnenwald and her colleagues (e.g. [Sonnenwald, Wildemuth and Harmon, 2001](#Sonnenwald01)). It builds on her theory about information horizons, which are the information resources available to someone in a specific situation. To draw a map of an information horizon is to graphically place oneself in relation to the resources (ibid). Savolainen and Kari further developed the method, calling their version information source horizons ([2004: 418](#Savolainen04)). They presented their interviewees with a model of three zones of relevance (most used, of secondary use and of peripheral use). The interviewees were asked to place the sources they used within the zone that fitted. The obtained material was analysed both quantitatively and qualitatively.

The information source horizon is a powerful tool to use while interviewing as it opens up ways to expand on how the interviewee used the information sources that is being mentioned. Savolainen and Kari’s ([2004](#Savolainen04)) emphasize that the concept is based on a visual metaphor, the consequences of which not to be overlooked. In her study of researchers’ information practices Wiklund ([2007](#Wiklund07)) combined the use of information horizons with creating a timeline of important events and people to capture the interviewees’ social networks. Hultgren ([2009](#Hultgren09)) employed information horizons in her study of young people’s information related activities concerning future studies and work.

In the study referred to here the activity of making maps and horizons is considered as a contribution to enhancing dialog and communication. The interviewees are asked to draw a map as a starting point and then to write or draw on another paper all the information sources they have on the map, but placing the sources in relation to themselves depending on how useful the sources were to them. Accordingly, the way the information horizon may look is in the hands of the interviewees and not of the researcher. In this study no quantitative analysis is made of the horizons. When Mia drew the information source horizon she decided to make two horizons, one representing her at the age 13-16 years old and one representing her present age. When making them she explained how her personal network had changed during the last couple of years:

> M: Eh, then I know that I had my ‘plastic sister’, she was very close [left out part where Mia describes what she means by ‘plastic sister’: her dad’s new wife’s daughter]  
> M: Yes, exactly, she was, she is a year older than me, I have no contact with her now. Because they have divorced and then  
> J: Mm  
> M: But she was a year older and sort of one step ahead of me  
> J: Mm  
> M: So I could go after her, or so  
> J. Mm  
> M: Eh, then, I have to think now, even her mother then, my dad’s wife that is, was very helpful  
> J: Mm  
> M: Shall I write ‘plastic mother ‘too? (laughter)  
> J: Mm  
> M: Because I could talk to her in a completely different way than I can with my mom  
> J: Mm  
> M: precisely, maybe, because there was this little distance  
> (Interview with Mia)

Mia was not a very verbal person. The making of the information source horizon was a trigger for telling the story about her changed social situation which turned out to be important for her decision regarding (non)use of contraceptives.

<figure>

![Figure 2](../pC10fig2.png)

<figcaption>Figure 2</figcaption>

</figure>

Based on the sociocultural view that learning is always happening and Mishler’s basic statement about interview being a joint construction, the interview is considered here as a learning opportunity and not as reporting on a ‘reality’ taking place somewhere else. Thus, the story is not understood as having been told or even been thought of in terms of being a story before, by the interviewees. The design deals with the potential difficulties that the interviewees might find in remembering the various sources used. Therefore a number of interviews are conducted with the sole goal to collect suggestions of possible information sources. Those interviews are not recorded. Large papers are used and filled with the suggested information sources. The interviewees are requested to mark their suggestions with red alerts or green alerts, to clarify whether they consider the sources useful or not. However, they do not need to have used them themselves. A deck of cards, based on the outcome of these interviews, was created, with single information sources written on each card. The deck of cards is offered as inspiration or memory-trigger during the interview in which the interviewees tell their story.

## Discussion and conclusions

In this paper a methodological framework is presented and discussed. The framework combines a sociocultural perspective (e.g. [Säljö, 1997](#Saljo97); [2000](#Saljo00)) with a narrative interview method ([Mishler, 1986a](#Mishler86a); [1986b](#Mishler86b); [Jovchelovitch and Bauer, 2000](#Jovchelovitch00)) in which visual tools are used to engage interviewees as active contributors in the interview situation. Three visual tools are added to increase interviewees’ participation: information source maps, information source horizons and a deck of cards. The paper contributes to LIS by scrutinizing how interviews may be used in spite of the difficulties that interviewing entails in terms of knowledge claims and power issues. The interviews are, according to the methodological framework developed here, recognized as a specific discourse jointly produced during the interview. The interviewees are invited to participate and produce narratives and visual representations while talking with the interviewer.

The empirical material obtained from the interviews, which I have referred to throughout the article, consists of transcripts, information source maps and information source horizons. The visual tools were employed to engage the interviewees more actively in the knowledge produced during the interaction. They were also a means to overcome difficulties the interviewees might have in talking about the topic. It was foreseen that their evaluation and use of information sources when choosing a contraceptive might not be something that the interviewees had thought about in terms of a story to tell. Making the map and the horizon as well as offering the use of the deck of cards with suggested information sources were activities facilitating different starting points for telling a story. Stories were told at different phases of the interview, not only during the phase of the main narration. Consequently, in the analysis no importance attached to when a story was told.

A second aim of the article is to discuss the consequences of the method developed for understanding how the information literacy practices of choosing contraceptives become part of the young women’s identity construction in the story they tell about how they chose a contraceptive. The empirical examples presented demonstrate how contraceptives can be understood as powerful tools for developing sexual identities as people go from being adolescents to grown-ups. The young women’s stories evolve around what information sources they used and how they evaluated them. In telling their stories, the choice of a contraceptive is also a way of creating and expressing part of their identities, an example of what Giddens ([1992](#Giddens92)) calls plastic sexuality, sexuality as malleable. An understanding of sexual intercourse disengaged from fertility as natural can be related to how the young women and the midwives they met for counselling negotiate normality ([Eckerdal, 2012](#Eckerdal12)).

The young women speak about contraceptives as protection against pregnancy and also as prevention from disease. However, only the condom do prevent disease. The young women say that they are protected against pregnancy. In their stories it is out of the question to become pregnant. In relation to their conduct, when choosing to start with birth control, they choose, above all, a protection against abortion (c f [Ekstrand, 2008: 22](#Ekstrand08)). This manifests itself in very different ways. Some of the young women do not believe that they can become pregnant and therefore do not protect themselves. Others say that they are carefully protecting themselves with condoms to keep themselves from getting pregnant. Experiences of having thought themselves pregnant, or of having been pregnant and of having had an abortion or someone close to them, or of a midwife pointing out that at their age they are very fertile; all led to the decision to begin using contraception. The question is not whether to have a child or not, but to avoid abortion. Therefore, they start using contraception. Once in steady relationships they want to use devices other than a condom. The technologies they use on their own bodies are judged to be more secure. Wajcman ([2004](#Wajcman04)) has described two opposing ways of understanding contraceptives: either as liberating or as violating the female body. In the stories told by the young women the contraceptive they have started to use is experienced as liberating: either it works very well or it has the potential for becoming liberating once the side effects are overcome.

Based on these findings it is proposed that the narrative interview design, in which interviewees are invited to take active part in the interview by both talking and drawing maps and horizons of information sources, meets the critique advanced at traditional interviews. In the present study the interviews became “empowering” arenas, offering opportunities for interviewees and interviewer to reflect on and ponder over what it means to choose and use a contraceptive. Information literacy practices of evaluating information sources about contraceptives is here analysed as a part of a story to tell about what it means to be a young woman and to express oneself as leading a sexually active life.

The empirical material obtained consists of maps of information sources, information source horizons and transcripts from five interviews with five women, 18-21 years old. The participants had all been in contact with a midwife at a youth centre in the south of Sweden, for counselling about contraceptives. The author had met all five participants prior to the interviews. Two of them had been individually interviewed and three had taken part in (different) group interviews. The young women were informed that participation was voluntary and that the material would be treated with confidentiality. The pseudonyms Anna, Lisa, Mia, Paula, and Sofia will be used in the article. The study has been reported elsewhere (Rivano Eckerdal, 2011a; 2011b; 2012) and was approved by the Regional Ethical Review Board of South Sweden.

[1]  <a id="footnote1"></a> <small>The empirical material obtained consists of maps of information sources, information source horizons and transcripts from five interviews with five women, 18-21 years old. The participants had all been in contact with a midwife at a youth centre in the south of Sweden, for counselling about contraceptives. The author had met all five participants prior to the interviews. Two of them had been individually interviewed and three had taken part in (different) group interviews. The young women were informed that participation was voluntary and that the material would be treated with confidentiality. The pseudonyms Anna, Lisa, Mia, Paula, and Sofia will be used in the article. The study has been reported elsewhere ([Eckerdal, 2011a](#Eckerdal11a); [2011b](#Eckerdal11b); [2012](#Eckerdal12)) and was approved by the Regional Ethical Review Board of South Sweden.</small>

[2] <a id="footnote2"></a> <small>According to Jovchelovitch and Bauer, Schütze described the technique in an unpublished manuscript in German, published by Jovchelovitch and Bauer in English with some adjustments (2000).</small>

[3] <a id="footnote3"></a> <small>A previous report from the research ([Eckerdal, 2011a](#eckerdal11a)) looked into the meetings between young women and midwives as joint achievements by use of positioning theory, a theory building on Austin’s speech act theory ([1961](#Austin61)).</small>

</section>

<section>

## References
<ul>
<li id="Aarnitaival10">Aarnitaival, S. (2010). Becoming a citizen – becoming information-literate? Immigrants’ experiences of information literacy learning situations in Finland. In A. Lloyd and S. Talja (Eds.), <em>Practising information literacy: bringing theories of practice, learning and information literacy together</em> (pp.301-329). Wagga Wagga: Centre for Information Studies
</li>
<li id="American89">American Library Association. (1989). Information literacy: Final report. <em>Chicago: American Library Association Presidential Committee on Information Literacy</em>
</li>
<li id="Austin61">Austin, J.L. (1961). How to do things with words. <em>Oxford: Clarendon Press</em>
</li>
<li id="Barry95">Barry, C.A. (1995). Critical issues in evaluating the impact of IT on information activity in academic research: developing a qualitative research solution. Library &amp; <em>Information Science Research</em>, <strong>17</strong>(2), 107-134
</li>
<li id="Bates04">Bates, J.A. (2004). Use of narrative interviewing in everyday information behavior research. <em>Library &amp; Information Science Research</em>, <strong>26</strong>(1), 15-28
</li>
<li id="Carey01">Carey, R.F., McKechnie, L. E. F. and McKenzie, P.J. (2001). Gaining access to everyday life information seeking. <em>Library &amp; Information Science Research</em>, <strong>23</strong>(4), 319-334
</li>
<li id="Eckerdal11a">Eckerdal, J.R. (2011a). To jointly negotiate a personal decision: a qualitative study on information literacy practices in midwifery counselling about contraceptives at youth centres in Southern Sweden. <em>Information Research</em>, <strong>16</strong>(1) paper 466
</li>
<li id="Eckerdal11b">Eckerdal, J.R. (2011b). Young women choosing contraceptives: stories about information literacy practices related to evaluation and use of information sources. <em>Dansk biblioteksforskning</em>, <strong>7</strong>(2/3), 19-31
</li>
<li id="Eckerdal12">Eckerdal, J.R. (2012). Information sources at play: the apparatus of knowledge production in contraceptive counselling. <em>Journal of Documentation</em>, <strong>68</strong>(3), 278-298
</li>
<li id="Ekstrand08">Ekstrand, M. (2008). Sexual risk taking: perceptions of contraceptive use, abortion and sexually transmitted infections among adolescents in Sweden. <em>Uppsala: Acta Universitatis Upsaliensis</em>.(University of Uppsala Ph.D. dissertation)
</li>
<li id="Elovaara06">Elovaara, P., Igira, F.J. and Mörtberg, C. (2006). Whose participation? Whose knowledge? <em>Trento: Participatory Design Conference Proceedings</em>
</li>
<li id="Garner06">Garner, S.D. (Ed.). (2006). High level colloquium on information literacy and lifelong learning. Bibliotheca Alexandra, Alexandria, Egypt, November 6-9, 2005. Retrieved July 28, 2013 from http://archive.ifla.org/III/wsis/High-Level-Colloquium.pdf (Archived by WebCite® at http://www.webcitation.org/6IRtliIxr)
</li>
<li id="Giddens92">Giddens, A. (1992). The transformation of intimacy: sexuality, love and eroticism in modern societies. <em>Cambridge: Polity Press</em>
</li>
<li id="Hultgren09">Hultgren, F. (2009). Approaching the future: A study of Swedish school leavers’ information related activities. <em>Göteborg and Borås: Valfrid</em>. (University of Gothenburg Ph.D. dissertation)
</li>
<li id="Jovchelovitch00">Jovchelovitch, S. and Bauer, M.W. (2000). Narrative interviewing. In M. W. Bauer and G. Gaskell (Eds.), <em>Qualitative researching with text, image and sound: a practical handbook</em> (pp. 57- 74). London: Sage
</li>
<li id="Kvale96">Kvale, S. (1996). InterViews: An Introduction to Qualitative Research Interviewing. <em>Sage: Thousand Oaks</em>
</li>
<li id="Lantz09">Lantz-Andersson, A. (2009). Framing in educational practices: learning activity, digital technology and the logic of situated action. <em>Göteborg: Acta Universitatis Gothoburgensis</em> 278. (University of Gothenburg Ph.D. dissertation)
</li>
<li id="Limberg08">Limberg, L., Alexandersson, M., Lantz-Andersson, A. and Folkesson, L. (2008). What matters? Shaping meaningful learning through teaching information literacy. <em>Libri</em>, <strong>58</strong>(2), 82-91
</li>
<li id="Lloyd06">Lloyd, A. (2006) Information literacy landscapes: an emerging picture. <em>Journal of Documentation</em>, <strong>62</strong>(5), 570-583
</li>
<li id="Lloyd09">Lloyd A. (2009). Informing practice: information experiences of ambulance officers in training and on-road practice. <em>Journal of Documentation</em> <strong>65</strong>(2), 396–419
</li>
<li id="Lloyd10">Lloyd, A., Lipu, S. and Kennan, M. (2010). Becoming citizens: examining social inclusion from an information perspective. <em>Australian Academic &amp; Research Libraries</em>, <strong>41</strong>(1), 42-53
</li>
<li id="Lloyd08">Lloyd, A. and Williamson, K. (2008). Towards an understanding of information literacy in context: implications for research. <em>Journal of Librarianship and Information Science</em>, <strong>40</strong>(3), 3-12
</li>
<li id="Lundh11">Lundh, A. (2011). Doing research in primary school: information activities in project-based learning. <em>Göteborg and Borås: Valfrid</em>. (University of Gothenburg Ph.D. dissertation)
</li>
<li id="Lupton10">Lupton, M and Bruce, C. (2010). Window on information literacy worlds: generic, situated and transformative perspectives. In A. Lloyd and S. Talja (Eds.), <em>Practising information literacy: bringing theories of practice, learning and information literacy together</em> (pp.3-27). Wagga Wagga: Centre for Information Studies
</li>
<li id="McKechnie06">McKechnie, L., Julien, H., Pecoskie, J.L. and Dixon, C.M. (2006). The presentation of the information user in reports of information behaviour research. <em>Information Research</em>, <strong>12</strong>(1) paper 278
</li>
<li id="McKenzie02">McKenzie, P.J. (2002) Communication barriers and information-seeking counterstrategies in accounts of practitioner-patient encounters. <em>Library &amp; Information Science Research</em>, <strong>24</strong>(1), 31-47
</li>
<li id="Mishler79">Mishler, E.G. (1979). Meaning in context: is there any other kind? <em>Harvard Educational Review</em>, <strong>49</strong>(19), 1-19
</li>
<li id="Mishler86">Mishler, E.G. (1986a). Research Interviewing: context and narrative. <em>Cambridge, Mass: Harvard University Press</em>
</li>
<li>Mishler, E.G. (1986b). The analysis of interview-narratives. In T.H. Sarbin (Ed.), <em>Narrative psychology: the storied nature of human conduct</em>. New York: Praeger
</li>
<li id="Mishler89">Mishler, E.G., Clark, J.A., Ingelfinger, J., and Simon, M. P. (1989). The language of attentive patient care: a comparison of two medical interviews. <em>Journal of General Internal Medicine</em>, <strong>4</strong>(4), 325-335
</li>
<li id="Moring10">Moring, C. (2010). Learning trajectories: becoming information literate across practices. Position paper presented at the 7th. <em>CoLIS Conference, Information Literacy Research Seminar</em>, London, June 21.-24
</li>
<li id="Park94">Park, T.K. (1994). Toward a theory of user-based relevance: a call for a new paradigm of inquiry. <em>Journal of the American Society for Information Science</em>, <strong>45</strong>(3), 135-141
</li>
<li id="Pierce07">Pierce, J. B. (2007). Research directions for understanding and responding to young adults sexual and reproductive health information needs. In: M.K. Chelton and C. Cool (Eds.), <em>Youth information seeking behaviour II: context, theories, models, and issues</em> (pp.63-91). Lanham Md: Scarecrow Press
</li>
<li id="Savolainen04">Savolainen, R., and Kari, J. (2004). Placing the Internet in information source horizons: a study of information seeking by Internet users in the context of self-development. <em>Library &amp; Information Science Research</em>, <strong>26</strong>(4), 415–433
</li>
<li id="Simmons05">Simmons, H.M. (2005) Librarians as disciplinary discourse mediators: using genre theory to move toward critical information literacy. <em>Portal: Libraries and the Academy</em>, <strong>5</strong>(3), 297-311
</li>
<li id="Sonnenwald01">Sonnenwald, D.H., Wildemuth, B.M. and Harmon, G.T. (2001). A research method to investigate information seeking using the concept of information horizons: an example from a study of lower socio-economic student’s information seeking behaviour. <em>The New Review of Information Behaviour Research</em>, <strong>2</strong>, 65–86
</li>
<li id="Squires97">Squires, D. (1997). Exploring the use of interactive information systems in academic research: borrowing from ethnographic tradition. <em>Education for Information</em>, <strong>15</strong>(4), 323-330
</li>
<li id="Sundin09">Sundin, O. and Francke, H. (2009). In search of credibility: pupils' information practices in learning environments. <em>Information Research</em> <strong>14</strong>(4) paper 418
</li>
<li id="Saljo97">Säljö, R. (1997) Talk as data and practice – A critical look at phenomenographic inquiry and the appeal to experience. <em>Higher Education Research &amp; Development</em>, <strong>16</strong>(2), 173-190
</li>
<li id="Saljo00">Säljö, R. (2000). Lärande i praktiken: ett sociokulturellt perspektiv. [Learning in practice: a sociocultural perspective] <em>Stockholm: Prisma</em>
</li>
<li id="Talja99">Talja, S. (1999). Analyzing qualitative interview data: the discourse analytical method. <em>Library &amp; Information Science Research</em>, <strong>21</strong>(4), 459-477
</li>
<li id="Tuominen05">Tuominen, K., Savolainen, R. and Talja, S. (2005). Information literacy as a sociotechnical practice. <em>Library Quarterly</em>, <strong>75</strong>(3), 329-345
</li>
<li id="Wajcman04">Wajcman, J. (2004). TechnoFeminism. <em>Cambridge: Polity</em>
</li>
<li id="Wiklund07">Wiklund, G. (2007). Interaktion i forskningspraktiken: vårdvetenskapliga forskares sociala nätverk.[Interaction in the research practice: the social networks of caring scientists] <em>Lund: Lunds universitet</em>. (University of Lund licentiate thesis)
</li>
<li id="Williamson10">Williamson, K. (2010). Knowledge building by Australian online investors: The role of information literacy. In A. Lloyd and S. Talja (Eds.), <em>Practising information literacy: bringing theories of practice, learning and information literacy together</em> (pp.251-271). Wagga Wagga: Centre for Information Studies
</li>
</ul>

</section>

</article>