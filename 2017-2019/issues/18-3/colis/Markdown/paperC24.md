<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Disposal of information seeking and retrieval research: replacement with a radical proposition

#### [John M. Budd](mailto:buddj@missouri.edu) and [Ashley Anstaett](mailto:ala8b@mail.missouri.edu)  
School of Information Science and Learning Technologies, University of Missouri, 303 Townsend, Columbia, MO 65211, USA

#### Abstract

> **Introduction**. Research and theory on the topics of information seeking and retrieval have been plagued by some fundamental problems for several decades. Many of the difficulties spring from mechanistic and instrumental thinking and modelling.  
> **Method**. Existing models of information retrieval and information seeking are examined for efficacy in a probabilistic environment. Alternatives to determinism are explored.  
> **Analysis**. The models that have been suggested are investigated for openness to human volition and the nature of questioning. This investigation is the basis for examination of authenticity among the models and potential desiderata in the thinking underpinning the modelling. Conclusions. The only viable solution to the problems of determinism is a complete re-conception of the nature of people who want to be informed—where the desire comes from, how perception affects the process, and outcomes can be assessed.

<section>

## Introduction

The purpose of this paper is to claim that a fundamentally altered conception of informing is needed. One aspect of the field of information studies must be acknowledged at the outset: the majority of studies in library and information science conducted to date consider information a noun. As such, the human interaction with the dynamics that are putatively addressed are limited to the existence of information as a noun. The limitation is akin to humans interacting with law, or medicine, instead of judicial action or advocacy, or healing. A person who needs legal advice is not concerned with statutes, but with the processes of interpreting and applying the statutes. A person with the symptoms of an illness is not like to care about anatomy, but wants a health professional to diagnose and treat the symptoms. In other words, descriptions of abstract nouns mean little to people interested in action; they desire strategies to effect the most advantageous action. A key to this conception is that emphasis is on inform (as verb, or informing as predicative gerund) rather than on information. Before proceeding with this claim, some background on the present state of thought and work is required. Some assumptions provide the foundations of the study of human information behaviour. Many of the assumptions follow cognitive science (although the cognitive science that dates back at least two decades) and psychology. A number of models have been expressed by several individuals, and a shift from noun to verb form is assisted by examination of the models. Some examples offer further background from which to proceed.

## Studies of consciousness

There are a number of theories that attempt to salvage monism against any challenge from, not just dualism, but any phenomenon that may not be readily explainable by reductive physicalism. One of these is Identity theory. This theory is fraught with problems though, some of which are evident in a brief claim by John Smythies ([2009](#smythies09)):

> However, to avoid philosophical confusion, it is better not to say that we perceive what is going on in our brains, or in our private phenomenal spaces, since, by definition, we should say that we perceive only external objects. Sensations are only a part of the process of perception, but we do not perceive sensations. Psychologists, during the course of their experiments, can examine or observe (but not perceive) their own sensations, veridical or hallucinatory, but that is for different purposes. We do not say that we perceive what is going on in our brains. . ., but because we cannot confuse a part of a process with the whole process. Having sensations is the last step in the complex representative chain of perception. Perceiving external physical objects involves the whole chain (p. 41).

A substantive difficulty with the claim is that, while an individual certainly cannot be aware of the inner working of one’s own brain, the perceptual experience is not simply of external objects, but of internal responses to them. According to their theoretical expression, there is actually no such thing as expression as such. Perhaps the most common, and most influential theory of recent time, is what is called the computational, or information-processing theory. The name is, itself, descriptive of the content of the theory. A major proponent is Michael Gazzaniga ([2011](#gazzaniga11)), who has written a considerable amount on the topic. He has mentioned work done by Benjamin Libet about a quarter century ago that demonstrated action preceding conscious decision to act (neurological stimulus occurring before statement of the decision to do something. Therefore, free will is a fiction, and conscious action is information processing. Gazzaniga omits informing readers that Libet, in a later work ([2005](#libet05)) recanted his earlier conclusions and affirmed the strong possibility of free will and the falsehood of information processing. There is much more that can be said about what Walter Freeman calls the standard cognitive programme, but space does not permit further exploration here. Not all neuroscientists agree with the standard programme. Freeman ([2000](#freeman00)), one of the contrarians, says, “Some brain scientists, myself among them, are committed to a different point of view in which the power to choose is an essential and unalienable property of human life. . . . Meaning is a kind of living structure that grows and changes, yet endures” (pp. 4-5, 9).

The vast majority of information seeking and retrieval and human information behaviour research of the last four decades has followed, deliberately or not, what can be called the standard cognitive model, which has tacitly included action by human agents. Modern cognitive science is a relatively recent development, dating back to the establishment of formal associations and academic programmes in the 1960s. The standard cognitive model can be described rather briefly. The major premise holds that the mind is a corollary to behavior that can be identified in the brain. Neural activity that can be identified is the explanatory force governing actions, decisions, and illusions of free will. The standard model is deterministic, and that fact is not accidental. Physical phenomena, it is claimed, can be examined in ways that non- or extra-physical phenomena cannot be. The principle predecessor of the standard cognitive model was customarily some version or other of Cartesian dualism. The argument against Cartesianism, which has been very cogent, has been articulated by a number of people for a number of years (see [Ryle, 1949](#ryle49)).

With the demise of Cartesianism there had to be some replacement, which has been a kind of monism, in which the physical, or the body, is the existent entity. What Descartes presented was essentially a first-person perspective of consciousness, as was expressed by his _cogito_. By the middle part of the twentieth century that first-person view had been replaced, in part as a result of the following of logical positivism and of behaviorism. In the second half of the twentieth century, though, both positivism and behaviorism were discredited as false receptor phenomena (states that are experienced only physically). That the limitation to mere physicality is false may seem intuitive, but it must be examined in detail in order to accept or refute. A purely physical phenomenon can be stated as, “My pain at _t_= my c-fiber stimulation at _t_” (see [Lycan, 1987: 11](#lycan87)). The statement is not untrue, but it is incomplete, perhaps even deceptive. Lycan ([1987](#lycan87)) paraphrases a rather complex assessment of the physical and linguistic complications of the purely physical statement:

> If _a_ and _b_ are “distinguishable” in the sense that we seem to be able to imagine one existing apart from the other, then it is possible that _a ? b_, unless (i) “someone could be, _qualitatively_ speaking, in the same epistemic situation” _vis-à-vis a_ and _b_, and still “in such a situation a _qualitatively_ analogous statement could be false,” or [let us add] (ii) there exists some third alternative explanation of the distinguishability of _a_ and _b_” [emphases in original] (p. 11).

While the statement is complex, the idea behind it is straightforward. There are identifiable and commutable distinctions between the physical states and other internal states that were not admissible by such programmes as positivism and behaviorism. Lycan ([1987](#lycan87)), to his discredit, mistakes Kripke to be a Cartesian, when Kripke’s point is that there are multiple ways of expressing material events and phenomena. Lycan’s materialism is too reductionist to provide a fruitful guide to the study of human information behaviour or information retrieval.

Contemporary work in consciousness stubbornly holds to the computational, information-processing hypothesis. Daniel Bor ([2012](#bor12)) is one of the latest to hearken back to the reductionist explanation of cognition, its origins, and its results. His entire book is an extended argument for the mind being nothing more than the brain. He ([2012](#bor12)) does hedge his bet a bit:

> almost all neuroscientists assume that the brain is a kind of computer, they recognize that it functions in a fundamentally different way from the PCs on our desks. The two main distinctions are whether an event has a cause and effect (essentially a serial architecture), or many causes and effects (a parallel architecture),and whether an event will necessarily cause another (a deterministic framework), or just make it likely that the next event will happen (a probabilistic framework) (p. 22).

There are two fundamental errors in Bor’s positions: (1) not all neuroscientists agree with him (by a long shot), and (2) his list of choices include so many possibilities that scientific inquiry will be busy for decades (at least) resolving the problem. The value of mentioning his stance is the illustration that a reductionist stance remains popular. Space prohibits discussion of other reductionist theories, such as that of eliminative materialism (see Patricia [Churchland, 1986](#churchland86)), but is yet another neuroscience-based approach to the limitation of mind to brain states. The very use of the word “states” presumes objects to be located.

## Information studies

Early work in information science (information studies) tended to draw from the standard cognitive model, whether explicitly or implicitly. In the relatively early stages of the _cognitive turn_ in information studies, Belkin and Robetrson ([1976](#belkin76)) presented a conceptual model of knowledge as defined by the standard cognitive model:

> We should note here that structure should be regarded as a category, rather than a concept; that is, it is of universal applicability (in a sense, everything has structure). This does not affect our argument: all it means is that we have to be careful when defining things in terms of structure. When we consider the term "information" as it has been used in the past, it is difficult to understand what the various uses have in common—that is, what the basic notion conveyed by the term is. In our view, the only basic notion common to most or all uses of information is the idea of structures being changed. We are therefore tempted to define it as follows: _Information is that which is capable of transforming structure_ (p. 198).

Their model may not be the first in the field, but it has been very influential. They also provide a graphic model that captures the standard cognitive model with no ambiguity.

<figure>

![Figure 1: The basic concepts of information science](../pC24fig1.png)

<figcaption>

Figure 1: The basic concepts of information science ([Belkin and Robertson, 1976: 201](#belkin76))</figcaption>

</figure>

In later work Belkin ([1982](#belkin82)) and colleagues attempted to address shortcomings of the efforts in human information behaviour to limit inquiry to user satisfaction with retrieval of materials (omitting “informing”). As they state, “This new approach recognizes that a fundamental element in the IR situation is the development of an information need out of an inqdequate state of knowledge” (p. 62). In their review of the thought on IR work at the time they refer to Gerd Wersig ([1971](#wersig71)), who corrects the misapprehension that individuals have an unfulfilled information need; rather, individuals recognize the need to solve a problem and explore ways to resolve the problem by eliminating an inadequate knowledge base (in other words, to be informed). Belkin and Robertson ([1982](#belkin82)) created a graphic design of their model, in which the elements of ASK can be realized (p. 65).

<figure>

![Figure 2: A cognitive communication system for information retrieval](../pC24fig2.png)

<figcaption>

Figure 2: A cognitive communication system for information retrieval ([Belkin, 1980: 135](#belkin80))</figcaption>

</figure>

<figure>

![Figure 3: Multi-tasking information behaviour and information task switching](../pC24fig3.png)

<figcaption>

Figure 3: Multi-tasking information behaviour and information task switching ([Spink, 2004: 347](#spink04))</figcaption>

</figure>

<figure>

![Figure 4: A model of information behaviour](../pC24fig4.png)

<figcaption>

Figure 4: A model of information behaviour ([Wilson, 2006: 659](#wilson06))</figcaption>

</figure>

Perhaps the most extreme anti-subjective position is articulated by Zbigniew Gackowski ([2010](#gackowski10)). In fact, his admitted purpose is to eliminate subjectivity and reduce information seeking and retrieval processes to objective measures. For example, he says:

> Key questions that the physical view addresses unambiguously are as follows:
> 
> 1.  Is informing limited to cases involving human clients?
> 2.  How should information and data be distinguished?
> 3.  How does informing (between informing entities and entities informed) differ from what takes place within the entity informed—the client?
> 4.  May the qualifier “subjective” be meaningfully applied to information?

_Informing_, _human clients_, and _subjective_ have little meaning in his programme, so he proposes a mechanistic and reductive systems-based model.

## Discussion of the models

There are some consistent features of these models, even as they address somewhat distinct elements of information seeking and retrieval. At the heart of the process is a known needs with a series of measures of purely physical aspects of information. Gackowski’s model clearly expresses his bias. These models are made possible, in part, by the idea, propounded by Buckland ([1991](#buckland91)) of information as thing, as a physical entity that can be studied as such.

<figure>

![Figure 5: Control loop](../pC24fig6.png)

<figcaption>

Figure 5: Control loop ([Gackowski, 2010: 37](#gackowski10))</figcaption>

</figure>

In an information seeking and retrieval task (according to the model), an information seeker has some grasp on what is needed/wanted and what is missing. There is some desideratum that can ultimately be identified and the absence can be resolved through the process indicated by the models. Spinks’s ([2004](#spink04)) model, for example, includes both planning and serendipity. The seeker has a starting point that is identifiable and that forms a relative or comparative point from which to proceed. There may, at times, be a need to depart from the starting point as other possibilities arise and draw the seeker’s attention. There could then be an iterative process of informing and questioning, assisted by functions of multi-tasking. In her model, it is the task switching that is central; resolution of an information seeking problem is not addressed directly.

The objectives of Wilson’s ([2006](#wilson06)) model are different from those of Spinks. There is an initial query expressed by a searcher in the material form of a “need.” What follows explicitly is a putative process of information behavior. That process entails specific subsequent acts, including seeking the resources offered by formal systems or less formal resources, including asking other individuals for information. These actions then result in success of failure, with success leading potentially to use of the information and ultimate satisfaction or non-satisfaction. This linear model leaves much to be desired. For example, there is no relationship between the use of formal systems and less than formal resources, although an information seeker may well choose to employ multiple means of exploration. Perhaps the most important shortcoming of this model is that processes have ending points, including the final decision on satisfaction. Any model that would illustrate such claims graphically should be based on sound empirical evidence. The relation of this model to actual process of information seeking and retrieval is incomplete at best and misleading at worst.

Erdelez attempt illustrates processes that can occur when individuals are open to information resources that may not be directly pertinent (or may be peripheral) to the immediate need at hand. For example, an individual may be searching with a particular goal in mind, but may come across some pertinent information that is not foremost in the seeker’s consciousness. The model suggests ways by which the seeker may alter the searching process through the act of searching and by means of retrieval. It includes elements that the other models do not, including the potential for informing. A drawback is that this still depicted as a linear exercise in consciousness. There is an established pathway of cognitive and conscious reception of potentially informing communication. The pathway of departure is discreet; it does not provide for possible connections between the original task and the one to which the searcher is diverted. Also, there is inadequate explanation of how the searcher is able to return to the original task. What changes in consciousness might be necessary to enable this phenomenon to occur?

## A Different Idea of Consciousness

Explication of the pathways adopted by all of the models adopts an inherent presumption: The knowledge of the creator of the process has first-person knowledge. It was mentioned above that the first-person stance was abandoned some years ago, but the abandonment may have been premature. Lynne Rudder Baker ([2003](#baker03)) presents an argument that revives the existence of first-person knowledge that is different from (and indeed cannot be compared to) third-person knowledge. The example she uses is Timothy McVeigh and his awareness that his execution was swiftly approaching was something only he be conscious of directly. Other people could have awareness of the upcoming event, but they could not share his conscious state; in other words, others cannot have the same conscious of the event that is about to occur to McVeigh as a subject. In effect, Baker is rejecting a claim that persists even today that all things are knowable by means of scientific examination (stance generally referred to as scientism). She expresses the difference between first-person knowledge and first-person understanding as the error of scientism: “Scientism is not itself a scientific claim; it is a philosophical claim about science” (p. 165). The difference is enormous and affects all examination of consciousness. First-person knowledge is fundamentally distinct from other knowledge , sentience, or awareness. Baker ([2003](#baker03)) continues:

There are two kinds of knowledge that have first-person propositions as objects: (1) knowledge of propositions known as self-justifying (e,g., I exist [as thought by me now]); and (2) knowledge of propositions justified for the knower by evidence or observation. . . . What the first-person perspective makes possible is not just knowledge of first-person propositions, but conscious awareness of one’s knowledge. One cannot only know that he* exists (or that he* is going to die in two days), but can also be consciously aware of knowing the things (pp. 169-79).

These are only the first steps towards a complete reconsideration of consciousness and informing within the context of Linformation studies. What follows is rather radical, especially in its almost complete rejection of the conceptions, inquiry, and ideology of information seeking and retrieval to date. The principle re-conception has its foundations in what can be called embodied consciousness (or, by extension, embodied informing). The key to this new approach is the emphasis on what is taken to be knowledge as well. A thoroughgoing analysis would have to include a critique of Karl Popper and followers, as well as everyone who believes that libraries are physical repositories of knowledge. Both positions are false, but the reasons for their shortcomings would require a longer paper. Suffice it to say that the radical position rejects these stances as not merely inadequate, but misguided.

A straightforward starting place is with John Searle’s ([2004](#searle04)) objection to many contemporary ideas about consciousness: “I write out of the conviction that the philosophy of mind is the most important subject in contemporary philosophy and that the standard views—dualism, materialism, behaviorism, functionalism, computationism, eliminativism, epiphenomemalism—are false” (p. 6). What is left is something that is new to research in information studies. Colin McGinn ([1999](#mcginn99)) takes the skepticism a step further: “human phenomenology is not reducible to our physiology, since an alien scientist who did not share our senses would not learn our phenomenology just by delving into our brains” (p.23). McGinn’s observation will be connected to the models and discourse on contemporary seeking and retrieval work momentarily. Understanding of the brain-mind link is greatly overstated at this time. An example of the undeserved optimism is evident in neuroscientist Steven Rose’s ([2005](#rose05)) claim that “the apparently seamless process of observing, evaluation and choosing between images based on past experience involves multiple brain regions, not necessarily connected via simple synaptic pathways. Achieving such coherence is called ‘binding’, and, as I have said, is the central theoretical problem for twenty-first neuroscience” (p. 157). The binding problem is addressed, not just in neuroscience, but in linguistics (more effectively) as well. Rose’s optimism may be admirable, but despite the successes of neuroscience, that answer that he seeks is still elusive. Likewise, a complete understanding of information seeking and retrieval, limited by “information,” remains as intractable as it has for the past four-plus decades.

A challenge that embodied informing has to deal with is what David Chalmers ([2010](#chalmers10)) calls “spatial unity” (p. 500). Briefly stated, the unity in question need not be (indeed rarely is) perception of two things being the same; it is a complex perception of more than one thing being in proximity in some simple or complicated way. Chalmers examines spatial unity from a rather straightforward physical way (or, in the terminology of philosophy of mind, in the phenomenal sense). There is, however, a potentially rich manner of investigation that will be a focus now. To begin, it must be recognized that the phenomenological aspect of perception is physical, not simply analogous to the physical. That said, whereas the customary information seeking and retrieval work tends to be deterministic, the human activities examined here are nondeterministic. Not an easy concept to grasp, examples should be used to clarify it. A teacher probably prepares a lecture or the structure of a class, but the teacher may well be unaware of what she will say until she says it. The nature of consciousness is such that spontaneity is possible. By the same token, the teacher may actually be surprised by thoughts and speech as the class progresses. The listener in the class usually does not act on the basis of representation (connecting the language directly with reality or with signs of a certain sort). The fact that a class is governed by space, time, participants, and other factors does not result in determined results.

Many of these kinds of principles are expressed (defined) by Maurice Merleau-Ponty ([1962](#merleau62)). He claims that “Existence is indeterminate in itself, by reason of its fundamental structure, and in so far as it is the very process whereby the hitherto meaningless takes on meaning” (p. 169). Merleau-Ponty emphasizes that indeterminacy is, in itself, a positive phenomenon. By that he means that logic is not primary; expression is important. Empiricism as it has been practiced since the early twentieth century objectifies phenomena to the extent that phenomena are nothing more than the extension of appearance in the world. The perception by the senses is ignored, so a theoretical articulation defined by objectivism governs inquiry. The perceptual theory of Merleau-Ponty ([1962](#merleau62)) has a completely different foundation. Permanence is not limited to physical presence (in informational terms this is analogous to the absolute presence of informational artifacts that are assessed as visible phenomena only). Merleau-Ponty ([1962](#merleau62)) says that the permanence of an object “is not a permanence of the world, but a permanence on my part” (p. 90). Observation is not solely intellectual; it is physical, sensory. So, consciousness is an extensive act.

The essence of embodiment is summed up by Francisco Varela and colleagues ([1991](#varela91)):

> By using the term embodied we mean to highlight two points: first, that cognition depends upon the kinds of experience that come from having a body with various sensorimotor capacities that are themselves embedded in a more encompassing biological, psychological, and cultural context. By using the term action we mean to emphasize once again that sensory and motor processes, perception and action, are fundamentally inseparable in lived cognition (pp. 172-73).

The brain is absolutely essential for consciousness, and consciousness is absolutely essential for informing. The challenge is to demonstrate precisely what constitutes informing. The stance here is that the discursive practices that typify information seeking and behavior—that is, the structural determinacy of systems—is mistaken. The mistake can be stated in part as the error of contemporary research as a search for nomological structures. Laws, as they are sought by researchers (and as they have been depicted negatively by Searle), do not exist. The work in information studies has, for the most part, ignored the mind-body problem that has plagued philosophers for centuries. The lack of action by information studies researchers is not particularly surprising, since the problem has not received serious attention from neuroscientists. Nonetheless, the work in information seeking and retrieval has assumed that there really is no mind-body problem and that each of us has access to the consciousness of each other. The models depicted above illustrate the assumption: a searcher acts as though the creators of the artifacts they find have consciousnesses too. At an extreme, a searcher may presume that the system itself is conscious and is able to comprehend the search terms just as a human can. Philosophers refer to the mind-body challenge as the “hard problem.” McGinn ([1999](#mcginn99)) summarizes: “I can only be sure that I have consciousness, and the reason for that is that I can be sure that I have consciousness without solving the mind-body problem, because of the unique access I have to my own consciousness” (p. 199).

The conundrum can be translated into information studies research. Once again, the above models, plus the discourse typified by Belkin, et al., Wilson, and Saracevic (see 2007; although Saracevic does admit to affective elements of relevance), and others, contain inherent presumptions of other consciousnesses, other minds. If a search is able to reveal meaning contained within artifacts—without access to those other minds—there is a serious error that seems to be, once again, intractable. A consideration that must underlie information studies inquiry is acceptance that linguistic acceptance determines an individual’s subjectivity and thinking. An indeterminstic premise (as Merleau-Ponty clearly demonstrates) shows that nonlinguistic elements are part of consciousness. The nonlinguistic components are vital to individuals’ ability to perceive both the world as it is and the world as it is represented in the mind. Both kinds of perception help to construct awareness and, so, knowledge.

There is a rather simple answer to the dilemma that exists in information studies: “contrast the fact that brain states themselves may be known by any number of different objective methods. These different methods yield the same objective knowledge of them; regardless of which of these methods is used, exactly the same objective fact is determined. _This is not true for subjective states_” [emphasis added] ([Georgalis, 2006, p.89](#georgalis06)). information studies research emphasizes the first (rather trivial admission) and ignores the latter. There is no mechanism that can “measure” subjective states, judgments, or perceptions (see McGiin above). The most common error attached to Georgalis’s assessment is the assumption of a “third-person” perspective from which individuals’ actions can be examined. In information studies the third person is usually the researcher, who attempts to insert himself or herself into the subject’s consciousness. The insertion may actually confound the subject’s consciousness by diverting it from the essence of one’s own consciousness. Conclusions about searching—including the statement of a question (which may be constrained by limitations or perturbations of perspective), the terms used to represent the question, and the nature of the artifacts retrieved—can pervert the entire process of inquiry. The conclusions, which are commonly applied, omit an enormous wealth of potential understanding of the subject.

Some items remain to be discussed in this radical proposal. One is memory, which is seldom mentioned in information studies literature. For example, Henri Bergson is not mentioned as a “Person” in Library, Information Science & Technology Abstracts with Full Text. Bergson has expressed some of the most sophisticated ideas about memory in the last century. He distinguishes between the automatic memory that allows us to follow traffic laws and other conventional actions, and personal memories, which enable contemplation (for Bergson the integration of memory and action). For him, memory is a component of a very complex consciousness, depicted in part by what he refers to as the “inverted cone.” The cone represents memories that are parts of dynamic systems which enable memories to flow into the perception and action of the present.

<figure>

![Figure 6: Inverted cone](../pC24fig7.png)

<figcaption>Figure 6: Inverted cone ([Bergson, 1911: 169](#bergson11))</figcaption>

</figure>

Bergson’s conception of memory carries with it a theory of space and time that few had previously conceived. Merleau-Ponty ([1962](#merleau62)) acknowledges the uniqueness of Bergson’s idea, remarking that it does have shortcomings, but no difficulties that render it inapplicable to the examination of consciousness. To reiterate, Bergson’s conception includes space as well as time. As is the case with other matters, Bergson’s unity of space and time suggests other scientific programmes, such as quantum mechanics (although that cannot be addressed here (see [Budd, forthcoming](#budd))). That programme is non-deterministic, probabilistic, and opposed to many standard notions of conventional science.

It seems relatively simple to devise models to reflect deterministic processes that are limited and do not embody the entirety of informing (see above). The complex depiction of what has been said in the second part of this paper is much more difficult to depict graphically.

One last point will be mentioned here as a key component of this proposal. The concept of intentionality is one that is mentioned in many contexts by scholars of various backgrounds. One in particular is pertinent to consciousness as defined in this examination. It is John Searle ([1992](#searle92)) who provides a clear definition of intentionality as a complex relation to consciousness. According to Searle, to be able to respond to something, or create a proposition, an individual must have a Network of intentions from which to draw. The selection is extraordinarily complex, not likely to be the same twice, connected in different ways at different times. Moreover, the Network is, itself, fluid and dynamic; as is the case with Bergson’s inverted cone, the Network is recursive and builds upon itself and its own formation. A difference between Searle and Bergson is that Searle has come to believe that memory is, in some ways, a mechanism that helps to generate modes of consciousness. In short, the past helps to create the present. One more matter deserves attention here: along with the Network comes the Background, and Searle ([1992](#searle92)) says that the “application depends on contingently existing biological and cultural facts about human beings” (p. 191). Searle’s stance is echoed more recently by Jesse Printz ([2012](#printz12)), who says, “Culture and history are essential to an understanding of who we are and how with think and act” (p. 365).

<figure>

![Figure 7](../pC24fig8.png)

<figcaption>Figure 7</figcaption>

</figure>

## Alternative model

This model may not be ideal, but it captures some of the vital elements of the radical proposal. Much more work must be done on this topic, and some components were referred to only briefly or obliquely. The primary purpose, as was stated earlier, is to transform research on information seeking and retrieval. It remains to be seen if this occurs. In developing a model such as this, a debt is owed to work done by Hjørland ([2010](#hjorland10)) and Day ([2011](#day11)).

</section>

<section>

## References
<ul>
<li id="baker03">Baker, L.R. (2003). Must science validate all knowledge? In Sanford, A, ed. The nature and limits of human understanding. <em>London: University of Glasgow</em>
</li>
<li id="belkin80">Belkin, N. J. (1980). Anomalous states of knowledge as a basis for information retrieval. Canadian Journal of Information Science, 34, 55-85.
</li>
<li id="belkin82">Belkin, N. J. &amp; Oddy, R. N. (1982). ASK for information retrieval, Part I: Background and theory. Journal of Documentation, 38, 61-71.
</li>
<li id="belkin76">Belkin, N. J. &amp; Robertson, S. E. (1976). Information science and the phenomenon of information. Journal of the American Society for Information Science, 27, 197-204.
</li>
<li id="bergson11">Bergson, H. (1911). Matter and memory, trans. N.M. Paul and W.S. Palmer. <em>London: Allen &amp; Unwin</em>
</li>
<li id="bor12">Bor, D. (2012). The ravenous mind: How the science of consciousness explains our insatiable search for meaning. <em>New York: Basic Books</em>
</li>
<li id="buckland91">Buckland, M.K. (1991). Information as thing. <em>Journal of the American Society of Information Science</em>, <strong>42</strong>, 351-360
</li>
<li id="budd">Budd, J. M. (Forthcoming). Re-Conceiving information studies: A quantum approach. <em>Journal of Documentation</em>
</li>
<li id="chalmers10">Chalmers, D.J. (2010). The character of consciousness. <em>Oxford: Oxford University Press</em>
</li>
<li id="churchland86">Churchland, P.M. (1986). Neurophilosophy. <em>Cambridge, MA: MIT</em> Press
</li>
<li id="day11">Day, R. (2011). Death of the user: Reconceptualizing subjects, objects, and their relations. <em>Journal of the American Society for Information Science and Technology</em>, <strong>62</strong>, 78-88
</li>
<li id="freeman00">Freeman, W.J. (2000). How brains make up their minds. <em>New York: Columbia University Press</em>
</li>
<li id="gackowski10">Gackowski, Z. (2010). Subjectivity dispelled: Physical views of information and informing. <em>Informing Science: The International Journal of an Emerging Transdiscipline</em>, <strong>13</strong>, 35-52
</li>
<li id="gazzaniga11">Gazzaniga, M.S. (2011). Who’s in Charge? Free Will and the Science of the Brain. <em>New York: Harper/Collins</em>
</li>
<li id="georgalis06">Georgalis, N. (2006). The primacy of the subjective: Foundations for a unified theory of mind and language. <em>Cambridge, MA: MIT Press</em>
</li>
<li id="hjorland10">Hjørland, B. (2010). The foundation of the concept of relevance. <em>Journal of the American Society for Information Science and Technology</em>, <strong>61</strong>, 217-237
</li>
<li id="libet05">Libet, B. (2005). Mind Time: The Temporal Factor in Consciousness. <em>Cambridge, MA: Harvard University Press</em>
</li>
<li id="lycan87">Lycan, W. G. (1987). Consciousness. Cambridge, MA: MIT Press.
</li>
<li id="mcginn99">McGinn, C. (1999). The mysterious flame: Conscious Minds in a material world. <em>New York: Basic Books</em>
</li>
<li id="merleau62">Merleau-Ponty, M. (1962). The phenomenology of perception, trans. By C. Smith. <em>London: Routledge</em>
</li>
<li id="papineau02">Papineau, D. (2002). Thinking about consciousness. <em>Oxford: Oxford University Press</em>
</li>
<li id="printz12">Printz, J.J. (2012). Beyond human nature: How culture and experience shape the human mind. <em>New York: Norton</em>
</li>
<li id="rose05">Rose, S. (2005). The future of the brain: The promise and perils of tomorrow’s neuroscience. <em>Oxford: Oxford University Press</em>
</li>
<li id="ryle49">Ryle, G. (1949). The concept of mind. New York, NY: Barnes &amp; Noble.
</li>
<li id="saracevic07">Saracevic, T. (2007). Relevance: A review of the literature and a framework for thinking on the notion in information science. Part II: Nature and manifestations of relevance. <em>Journal of the American Society for Information Science &amp; Technology</em>, <strong>58</strong>, 1915-1933
</li>
<li id="searle04">Searle, J.R. (2004). <em>Mind: A brief introduction</em>. Oxford: Oxford University Press
</li>
<li id="searle92">Searle, J.R. (1992). A rediscovery of the mind. <em>Cambridge, MA: MIT Press</em>
</li>
<li id="smythies09">Smythies, J. (2009). Brain and consciousness: The ghost in the machines. Journal of Scientific Exploration, 23, 37-50.
</li>
<li id="spink04">Spink, A. (2004). Multitasking information behavior and information task switching: An exploratory study. <em>Journal of Documentation</em>, <strong>60</strong>, 336-51
</li>
<li id="varela91">Varela, F.J., Thompson, E., and Rosch, E. (1991). The embodied mind: Cognitive science and human experience. <em>Cambridge, MA: MIT Press</em>
</li>
<li id="wilson06">Wilson, T.D. (1981/2006). On user studies and information needs. <em>Journal of Documentation</em>, <strong>62</strong>, 658-670
</li>
</ul>

</section>

</article>