<!DOCTYPE html>
<html lang="en">

<head>
	<title>Scholarly Communication's Problems: An Analysis</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="dcterms.title" content="Scholarly Communication's Problems: An Analysis">
	<meta name="author" content="Budd, John M.">
	<meta name="dcterms.subject"
		content="This paper represents ongoing research into the dynamics of scholarly communication.">
	<meta name="description"
		content="People have speaking about problems with scholarly communication, and assigning blame, for the last few decades. Conversations have included media, pricing, ownership, technological development, and other features of the system. While these are somewhat legitimate concerns, they miss the fundamental political point of production and consumption. In other words the entire problem should be examined from the standpoint of political economy. It is from that view that a clear that the challenges emerge. This paper explores scholarly communication by means of political economy.">
	<meta name="keywords" content="scholarly communication, political economy, university administration">
	<meta name="robots" content="all">
	<meta name="dcterms.publisher" content="Professor T.D. Wilson">
	<meta name="dcterms.type" content="text">
	<meta name="dcterms.identifier" content="ISSN-1368-1613">
	<meta name="dcterms.identifier" content="http://InformationR.net/ir/18-3/colis/paperS01.html">
	<meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/18-3/infres183.html">
	<meta name="dcterms.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<meta name="dcterms.issued" content="2013-09-15">
	<meta name="geo.placename" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<header>
		<h4 id="vol-18-no-3-september-2013">vol. 18 no. 3, September, 2013</h4>
	</header>
	<article>
		<h2
			id="proceedings-of-the-eighth-international-conference-on-conceptions-of-library-and-information-science-copenhagen-denmark-19-22-august-2013">
			Proceedings of the Eighth International Conference on Conceptions of Library and Information Science,
			Copenhagen, Denmark, 19-22 August, 2013</h2>
		<h2 id="short-papers">Short papers</h2>
		<h1 id="scholarly-communications-problems-an-analysis">Scholarly communication's problems: an analysis</h1>
		<h4 id="john-m-budd"><a href="mailto:buddj@missouri.edu">John M. Budd</a><br>
			School of Information Science and Learning Technologies, University of Missouri.<br>
			303 Townsend Hall, Columbia, MO 65211, USA.</h4>
		<h4 id="abstract">Abstract</h4>
		<blockquote>
			<p><strong>Introduction</strong>. This paper explores scholarly communication by means of the complex
				political economy of commercial publishers, emerging modes of dissemination, and self-interested
				parties.<br>
				<strong>Method</strong>. This is primarily a review of background forces that have influenced scholarly
				communication for many years. It adds examination of the participants in production and decision making,
				including academic administrators.<br>
				<strong>Analysis</strong>. The paper explores production and dissemination of dominant and emergent
				models of communication, with attention to the political economic elements of choice and coercion in the
				communication processes.<br>
				<strong>Conclusions</strong>. Conversations have included media, pricing, ownership, technological
				development, and other features of the system. While these are somewhat legitimate concerns, they miss
				the fundamental political point of the political economy of production and consumption. In other words
				the entire problem should be examined from this standpoint. It is from that view that a clear picture
				that the challenges emerge.</p>
		</blockquote>
		<section>
			<h2 id="introduction">Introduction</h2>
			<p>Is the difficulty with scholarly communication intractable? The answer is no, but reaching that answer
				requires substantial analysis of causes and effects; the causes and effects are, to a considerable
				extent, political. The foundations of the problem will be the focus here. We can begin with a hint of an
				answer: the dilemma is not new; it has been developing over time and by means of various means,
				influences and choices; and it is not insoluble. First, libraries have been experiencing financial
				pressures for the last several decades, especially trying to pay for serials and databases—writ large,
				to encompass aggregators' and publishers' digital access, among other things. The financial challenges
				have not eased, even as several efforts have had some success at providing information at more
				reasonable rates. Open Access may one of the most notable and obvious successes, but institutional
				repositories are also expanding at a relatively brisk rate. Even with the progress, there is still
				concern about the future pricing and control of serials and other informational items. A component of
				the concern is the expansion of publications and other informational items; more is produced each year,
				so access must be provided each year. This is part of the cause. These kinds of challenges remain the
				sources of concern and are constantly discussed in a variety of forums. In fact, it could be said that
				the crux of the problem is one of stasis (causes and effects not changing substantively), of either
				reluctance on either (or both) sides to stand firm on past action or a stubbornness to consider
				alternatives. This paper will suggest some ways to address the problems by means of evolving cooperation
				among the various entities concerned.</p>
			<h2 id="background-to-the-problem">Background to the problem</h2>
			<p>The problem requires elucidation. Boissy and Schatz (<a href="#bois11">2011</a>), speaking from the
				vendor's perspective say, “We are told over and over again that we live in an information economy and in
				an increasingly intellectual and specialized world. Scholars want resources available at their
				fingertips. Users now expect it. We cannot go back; we cannot regress to some earlier state (even if
				should desire to do so). As always, we need to figure out how to pay for it all, and how to preserve
				what we create” (p. 483). There is a presumption that is mentioned but taken for granted. We live in and
				information economy. Does that necessarily mean that every information artefact is a commodity? Should
				every “product” have material value? Is there nothing that exists for, and in, the public interest?
				Another way to state this question is: Can there be research and scholarship that exist for their own
				sakes, for the elucidation of questions as well as for the provision of answers of material benefit? Is
				there a particular political economy (rather than an explicitly financial economy) that complicates
				information production and consumption? That complication includes a necessary acceptance that
				“production” is by no means limited to the commercial preparation of artefacts for sale. Scholars and
				researchers write the works that are then sold by many companies (at substantial profits). The market
				that typifies scholarly communication is not exactly like other traditional dual-party markets. As Vardi
				(<a href="#vard12">2012</a>) observes, “On one side, you have authors, who freely and eagerly provide
				content (‘publish or perish&quot;). On the other side, there are editors and reviewers, who act as
				gatekeepers. They do so for a variety of reasons: sometimes for financial remuneration, but mostly out
				of civic duty and to gain scholarly prestige” (p. 5). To add to Vardi's claim, authors act in
				self-interest.</p>
			<h2 id="the-players">The players</h2>
			<p>Presidents, chancellors, and provosts of colleges and universities establish the criteria for tenure and
				promotion and, thus, are able to exert enormous control over the work of the faculty. [The discussion
				here will concentrate on North American institutions, which act with much greater individual autonomy
				than do institutions of higher learning in Western Europe, which may be bound by governmental
				regulation. Thus, political economy in various countries can vary considerably.] That control is
				economic in nature; faculty are rewarded (with tenure and promotion, with merit raises, etc.) for
				production that is defined in particular ways. Over the last few decades producers, such Elsevier,
				Springer, and Wiley have been the primary determiners of the ecological environment, even though the
				institutions have fed that ecology. A reality that must be accepted is that this ecology has not been a
				cooperative one. Yes, there has been adaptation, but it has largely been a combination of predation and
				defence. For the conclusion to come about, the contingency has to be transformed into actual alternative
				causes and effects. The task here is to establish precisely what is required for the contingent premises
				to become actualized. We can begin with one component of analysis. The players in this complex market
				behave in vastly different ways. The large journal producers, to a considerable extent, have created a
				market by expanding the numbers of journals available and the numbers of pages that are published.</p>
			<p>In other words, the increased volume was not sufficient; there was an entrepreneurial opportunity to
				enhance revenues in ways that exceeded volume. These companies created their own side of the market,
				without consultation with the other players (but then, they do not really require consultation) (see <a
					href="#madr11">Madrick, 2011: 180-86</a>). Libraries are, substantially, reactive to the producers;
				producers presume that libraries are inelastic markets. The prices have been set, and libraries pay
				those prices. That is, of course, as over-simplification. Libraries have formed cooperative consortia,
				and one of the outcomes of the consortia has been more powerful negotiating power with the publishers.
				The journal producers were no longer setting the process alone; they had to work with multiple libraries
				that had millions of dollars at stake. Libraries began to alter the ecology.</p>
			<p>The major premise can be clarified to state that the publishers, as well as the consumers, face a looming
				abyss. The profit margins of the producers are not sustainable, for a few reasons, the principal one
				being the growth of open access publication. If open access bypasses traditional journal publication,
				profits can fall because of a potential decline in net revenue. What has been done (to reduce the
				argument here for space purposes to a single item) concentrates on the dynamics of open access, which
				is, itself, an ideology in its own right. It is characterized primarily by an optimism that may exceed
				the potential. The idea of open access is also characterized by a substantial number of journals that
				offer lower, or even no, cost to users. In keeping with a component with the traditional journal
				ideology, open access journals customarily employ very similar kinds of peer review as a quality control
				mechanism. [As an aside, almost no journals pay referees to review manuscripts, affirming the
				rationality of production.] Suber (<a href="#sub12">2012</a>) offers an exceedingly cogent observation
				about the conflicting ideologies and their rationalities. “Conventional publishers are adapting to the
				digital age in some respects [to retain their causal status]. They're migrating most print journals to
				digital formats and even dropping their print editions. They're incorporating hyperlinks, search
				engines, and alert services. A growing number are [sic] digitizing their back files and integrating
				texts with data” (p. 35). Another static dilemma crops up. As the traditional corporate sources create
				more electronic tolls and items, libraries might lose access to more and more information. Vardi (2012),
				recognizing the increase in open access sources, says that there are more than two players in this
				market. The multiple players complicate the marketplace and can affect pricing and consumption. An
				analysis is needed of readers' usage and preferences of open access content compared with traditional
				content paid for by libraries. A study conducted by <a href="#welltr">the Wellcome Trust</a>, although
				published in 2004, stated, “When readers are required to pay high prices, for accessing research
				outputs, they would be expected to abandon high-priced journals and substitute them from other sources,
				but readers are ‘protected' from these price implications through the library system. The market is, in
				this respect, highly inefficient” (p. 4). The report (<a href="#welltr">2004</a>) added, “A charge to
				authors would potentially act as a disincentive to publication. The extent of the disincentive would
				depend largely upon the opportunities for alternative publication routes” (p. 4)</p>
			<p>For the most part, a library must continue to pay for database access, or the entire back file may be
				lost. An alternative offered by some producers is the possibility to purchase back files, but the price
				is very high. For example, Elsevier enables institutions to purchase back files according to a pricing
				formula:</p>
			<blockquote>
				<p>This programme enables ScienceDirect customers to fill in Backfiles for specific titles from 1995
					onwards that are not included as part of their current subscription entitlement (four-year Backfiles
					plus the current year building). They can be purchased for a one-time fee on a title-by-title basis
					at a 5% of the current catalogue subscription price x the number of years involved. The Backfiles
					titles selected must match all or a subset of the customer's holdings (<a
						href="http://www.info.sciverse.com/sciencedirect/content/backfiles#1994">sciverse.com</a>.
					Archived by WebCite® at http://www.webcitation.org/6I858IM2y).</p>
			</blockquote>
			<p>It may be that few institutions have the wherewithal to make such purchases. The hopes of stasis (part of
				the dominant ecological culture) are rendered extremely problematic. Open access carries its own
				challenges as well. For example, the Public Library of Science (<a
					href="http://www.plos.org/publish/pricing-policy/publication-fees/">PLOS</a>) charges authors
				publication fees. PLOS Biology and PLOS Medicine charge fees of $2,900 each. “Open” access may not
				always be so open. In 2011 PLOS Biology published 128 research articles; at $2,900 per article the
				publication fee resulted in a gross revenue of $371,200. As institutions smaller than the largest
				research universities require more and more publications of their faculty, some open access publications
				may be closed by the pricing to authors. The difficulty is represented graphically by Baveye (2010):</p>
			<figure>
				<img src="pS01figure1.png" alt="Figure 1: Dynamics of publishing">
				<figcaption>Figure 1: Dynamics of publishing (Baveye, 2010: 200)</figcaption>
			</figure>
			<p>The efficacy of open access is a question that requires much more investigation; at this time it may hold
				some promise, but it is not a complete financial solution. An ultimate answer will require a deep
				examination of the full political economy of communication, including the micro-level of individual,
				institutional, and company decisions. The decisions may not optimize a macroeconomic solution to
				scholarly communication in general.</p>
			<h2 id="analysis-and-directions">Analysis and directions</h2>
			<p>The scholarly communication network, most agree, is dysfunctional. As is presented here, the causes of
				dysfunction rest with every one of the players. There is a simplistic cause for the problem, and rests
				with the political economy of the United States (although many players, especially information
				producers, are outside the U.S., factors at work in the United States exert enormous influence). Without
				going into great detail, in large part because of limitations of space, the social structure of the
				United States is based on an ideology of individualism (see <a href="#levi04">Levine, 2004</a>). This
				individualism is fraught, though, and is complicated by varying definitions and appropriations of
				“individualism.”</p>
			<p>“Individualism” features here because the players have tended to look only to their own interests,
				without consideration for a complex cooperative system. For instance, the large companies have
				concentrated on maximizing profit, while researchers have looked to maximizing numbers of publications
				so as to create prestige, both locally on their respective campuses, and nationally (and beyond) in
				their disciplines. Individualism, especially on the part of faculty, is manifest in particular ways in
				contemporary times. Canadian scholar John Ralston Saul (<a href="#saul03">2003</a>) clearly recognized
				the upside-down dynamic when he noted that individualism largely made the economic phenomena possible
				(p. 3). The reverse of the reasonable is exacerbated by the several ranking structures (such as U. S.
				News &amp; World Report) that tend to shift both consciousness and ideology. There is little loyalty to
				the institution and more attention to self-aggrandizement.</p>
			<p>[N.B.: The above phenomenon may have been changing recently since, because of institutional fiscal
				constraints plus the diminishing of tenured and tenure-track positions in favour of non-tenure-track
				positions. This alteration has resulted in a lessening of institutional loyalty to faculty.] The system
				is characterized by individual preference and perceived personal utility. Cooperation is sorely
				challenged in such a structure.</p>
			<p>The economics of the system is not only inefficient, it has wider-ranging effects on entire
				higher-education institutions by rendering libraries ineffective users of available resources, and
				faculty ineffective members of the institution's ideal social culture. The commercial information
				producers, at this point in time, do not have to care about the actions of the other players, since
				those others are only committed to self-interest and individual utility. What is required to alter the
				dispositions of the players? They will not be changed simply by virtue of the beneficence of one of the
				players. Years ago Garrett Hardin (1966) posited the idea of the Tragedy of the commons. Suffice it to
				say here, the idea holds that, if a resource is presumed to be common, but is not realized as mutually
				beneficial for the future, the resource will be overused and all players will lose. It is not a huge
				leap to information world to be a commons that is endangered. Libraries&quot; budgets are stretched to
				breaking points. Faculty are so pressured to publish and acquire external funding for their research
				that they are discouraged from teaching effectively. The longer term of corporate profits are endangered
				by the other factors. University administrators appear to be oblivious to the dilemma. Can the growth in
				both the number and size this state; it has evolved as a result of conscious, semi-conscious, and
				unconscious actions be the variety of players. The decisions were made in the absence of any clear
				understanding of journals continue unabated? A more important question is: Should the growth in both
				number and size of journals continue? From the year 2000 to the present, a total of 1,231 publications
				are classified as duplicate publications in the database MEDLine (cause, or effect?).</p>
			<h2 id="where-do-we-go">Where do we go?</h2>
			<p>As was mentioned earlier, some of the actions undertaken by the parties are less than conscious. This, of
				course, does not apply to all players all the time. The corporate entities are aware of their motives
				and the outcomes. On the other hand, they may not be fully aware of the impact of their actions on the
				other players. Their profit motive is actually a short-term goal; if future developments (including open
				access journals and institutional repositories (see Burns, Lana, and Budd, forthcoming)) are successful
				as another development,, revenues of corporate information sources may decline. The effects of
				repositories could be magnified if large-scale, global initiatives were to develop unified search and
				retrieval mechanisms that would allow a scholar located anywhere to gain access to materials deposited
				in any institutional repository.</p>
			<p>Libraries are well aware of the monetary impact of journal and database prices, as well as the effects on
				monographic purchases. A great deal has been written about the dilemma; a considerable amount of
				empirical investigation has demonstrated that pricing has had a deleterious impact on information
				resources. Librarians are decidedly aware of what corporate actions do to academic communities. While
				there is an awareness of much of the predicament, there is a great deal that remains unknown. For
				example, many librarians are aware of the problem related to the sources of published materials, but
				connections between the growth of literatures and institutional practices may not always be made.</p>
			<p>The administrators of colleges and universities may be least conscious of the systemic difficulties
				(perhaps by obliviousness or by choice). In fact, it may be that there are administrators who are
				unaware of the systemic nature of the problem, perhaps especially the political power held by commercial
				publishers (and their influence over academic decisions). As long as the requirements for tenure and
				promotion continue to increase (that is, if more and more publications and presentations are required of
				faculty), many of the aforementioned challenges will remain. In many instances, as Martin Nowak (<a
					href="#nowa11">2011</a>) states, the customary problem is scarcity; the academic difficulty is just
				the reverse. Faculty members produce more, frequently at the insistence of administrators. A working
				logic may hold that the more content that is produced, the greater the opportunity for knowledge growth.
				Examination of such phenomena as cognitive load theory should be applied to examine that assumption
				(although that is beyond the scope of the investigation here). Another future study can encompass the
				entire evolutionary dynamic, not merely of production, but of learning and knowledge growth. While a
				great amount of empirical work is required to examine the details of the systemic elements, the
				definitions of the fundamental problems are articulated here. In truth, the initial stage of addressing
				this matter should be raising the consciousness of academic administrators, and any promising
				development of evolutionary progress with this group. Little can be done without teleological change
				that embraces the entirety of the scholarly endeavour. Such a tactic would be a beginning.</p>
		</section>
		<section>
			<h2>References</h2>
			<ul>
				<li id="berg01">Bergstrom, CT. &amp; Berstrom TC. (2001). The economics of scholarly journal publishing.
					Retrieved 15 July, 2013 from http://octavia.zoology.washington.edu/publishing/ (Archived by WebCite®
					at http://www.webcitation.org/6I858IM2y)
				</li>
				<li id="bois11">Boissy, R. &amp; Schatz, B. (2011). Scholarly communications from the publisher
					perspective. <em>Journal of Library Administration</em>, <strong>51</strong>, 476-484
				</li>
				<li id="burnip">Burns, CS., Lana, A. &amp; Budd, JM. (Forthcoming). Institutional repositories:
					exploration of cost and value. <em>D-Lib</em>
				</li>
				<li id="levi04">Levine, A. (2004). The American Ideology: A Critique. <em>New York: Routledge</em>
				</li>
				<li id="madr11">Madrick, JG. (2011). Age if greed: The triumph of finance and the decline of America,
					1970 to the present. <em>New York: Free Press</em>
				</li>
				<li id="nowa11">Nowak, NA. (2011). Super collaborators: Altruism, evolution, and why we need each other
					to succeed. <em>New York: Free Press</em>
				</li>
				<li id="rose00">Rosenberg, A. (2000). Darwinism in philosophy, social science and policy. <em>Cambridge:
						Cambridge University Press</em>
				</li>
				<li id="saul03">Saul, JR. (2003). The unconscious civilization. <em>Toronto: Anansi</em>
				</li>
				<li id="sube12">Suber, P. (2012). Open Access. <em>MA: MIT Press</em>
				</li>
				<li id="vard12">Vardi, M. (2012). Predatory scholarly publishing. <em>Communications of the ACM</em>,
					<strong>55</strong>(5)
				</li>
				<li id="welltr">The Wellcome Trust (2004). Cost and Business Models in Scientific Research Publishing.
					<em>Histon: Cambridgeshire</em>
				</li>
			</ul>
		</section>
	</article>
</body>

</html>