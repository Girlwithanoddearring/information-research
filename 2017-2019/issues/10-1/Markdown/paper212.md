#### Vol. 10 No. 1, October 2004

# Information seeking research needs extension towards tasks and technology

#### [Kalervo Järvelin](mailto:Kalervo.Jarvelin@uta.fi)  
Department of Information Studies  
Tampere University, Tampere, Finland  

#### [Peter Ingwersen](mailto:pi@db.dk)  
The Royal School of Librarianship and Information Science  
Copenhagen, Denmark

#### **Abstract**

> This paper discusses the research into information seeking and its directions at a general level. We approach this topic by analysis and argumentation based on past research in the domain. We begin by presenting a general model of information seeking and retrieval which is used to derive nine broad dimensions that are needed to analyze information seeking and retrieval. Past research is then contrasted with the dimensions and shown not to cover the dimensions sufficiently. Based on an analysis of the goals of information seeking research, and a view on human task performance augmentation, it is then shown that information seeking is intimately associated with, and dependent on, other aspects of work; tasks and technology included. This leads to a discussion on design and evaluation frameworks for information seeking and retrieval, based on which two action lines are proposed: information retrieval research needs extension toward more context and information seeking research needs extension towards tasks and technology.

## Introduction

Since the mid-1980s, many theoretical models and frameworks have been proposed for information seeking research (for a review, see [Järvelin & Wilson 2003](#jarvelin03)). Taken together they suggest a perspective covering phenomena from information systems and their design, through information access by various processes to work tasks, seen in the list of the nine dimensions [below](#dims). The focus of theoretical analysis, however, has been in the seeking process: its stages, actors, access strategies, and sources. Work tasks and information (retrieval) systems have received less theoretical attention as foci of modelling and theorizing. ([Ingwersen & Järvelin forthcoming](#ingwersen); [Vakkari 2003](#vakkari03))

Recent empirical findings of information seeking research have provided insight into actor-centred information seeking. Information seeking has been understood as a process in which the actor's understanding of his or her tasks or problems, information needs, relevance criteria, and the available information space evolve. The actors studied have been varied, now including various professional groups and also lay people. These studies have provided rich and realistic descriptions on how people encounter discontinuities or gaps and how they try to make sense of them. ([Ingwersen & Järvelin 2005](#ingwersen))

However, in spite progress in theoretical understanding, empirical studies of information seeking provide only a limited number of empirical answers to research questions that relate characteristics of contexts and situations to characteristics of tasks, actors, information, seeking processes, sources, systems and use of information. It also has remained difficult to apply the findings to information system design. While our understanding of task requirements and effects on information seeking has advanced, the understanding on how to derive and apply design criteria for information (retrieval) systems has not advanced correspondingly. For the most part, information retrieval system design is not informed about the situations and conditions of their use. There is a shortage of studies that relate (information retrieval) system features to features of task and/or seeking processes. Therefore, we argue that information seeking research should be extended towards both tasks and technology. ([Ingwersen & Järvelin forthcoming](#ingwersen))

The organization of this paper is as follows: first, we present a model of information seeking and retrieval that focuses on various players and their interaction in context. We then move into the dimensions that, in our view, are essential to the understanding of information seeking and retrieval. Nine such dimensions are proposed: they are well founded in the literature and are unlikely to be found surprising. We then use these dimensions to analyse the status of knowledge of information seeking and retrieval up to very recent times. It becomes apparent that the current status is unsatisfactory in terms of the connection between information seeking research and the implications for augmenting work tasks and systems design. We conclude that these lacunae need attention.

We begin the discussion on what should be done by looking into the goals of information seeking research. Three types of goals will be discussed and we suggest that much information seeking research is done in isolation, and argue that it rather should be done 'in association'. For this purpose we will propose as task performance augmentation view and try to show how intimately information seeking is association with, and dependent on, other aspects of work.

Taking a practical stance, we then discuss design and evaluation frameworks for information seeking and retrieval. If evaluation sounds too system-driven we may think of empirical investigation into the use, strengths and weaknesses of information access practices. We conclude with a proposal for action lines for information seeking and retrieval research.

Throughout the paper the reader will see us writing about work tasks. We will not keep repeating at every instance '... and daily-life activities and interests as entertainment or cultural interests' but these are generally included.

## General analytical model of information seeking and retrieval

Figure 1 presents a general analytical model of information seeking and retrieval proposed in a forthcoming book ([Ingwersen & Järvelin forthcoming](#ingwersen)). It shows various players and their interaction in context in the field of information seeking and retrieval.
<figure>

![fig1](../p212fig1.jpg)

<figcaption>

**Figure 1: A general analytical model of information seeking and retrieval ([Ingwersen & Järvelin forthcoming](#ingwersen))**</figcaption>
</figure>

We may observe cognitive actors such as information seekers in the middle surrounded by several kinds of contexts. These are formed by the actors' social, organizational and cultural affiliations, information objects, information systems and the interfaces for using them. The context for any node in the diagram consists of all the other nodes.

For example, algorithmic and interactive information retrieval processes do not stand alone, but are special cases of information seeking behaviour. Algorithmic information retrieval, that is, the interaction between information objects and algorithms, arrow (4), has no real meaning without human interaction with information retrieval systems, arrows (2-3). Interactive information retrieval itself functions as a special case of information seeking. Today, all information seeking becomes increasingly technology-driven because progressively more and more informal communication channels, such as mail, become formalized in systems because of the digitalization of communication.

Information seeking behaviour means the acquisition of information from knowledge sources. For instance, one may ask a colleague, through (in)formal channels such as social interaction in context (arrow 1), or through an information system (arrows 2-4).

Secondly, actors operate in a dual context: that of information systems and information spaces surrounding them, and the socio-cultural-organizational context to the right. Over time, the latter context influences and, to a large degree, creates the information object space on the one hand (arrow 6) and the information technology infrastructure (arrow 8) on the other.

In different roles, the actors themselves are not just information seekers but also authors of information objects (arrow 5) and creators of systems (arrow 7).

## Nine broad dimensions

By analysing information seeking and retrieval with the help of this model, a number of dimensions involved in information seeking and retrieval may be observed. We propose nine broad dimensions that interact in information seeking and retrieval processes. They are all found in the literature of information seeking and retrieval but, as far as we know, never put together in any single study. In the following we present them briefly using a work task perspective.

1.  <a id="dims"></a>The work task dimension covers the work task set by the organization, the social organization of work, collaboration between actors and the physical and system environment.
2.  The search task dimension covers necessary seeking and retrieval practices, as understood collectively in organizational practice.
3.  The actor dimension covers the actor's declarative knowledge and procedural skills, and other personal traits, such as motivation and emotions.
4.  The perceived work task dimension covers the actor's perception of the work task: forming the task that is carried out.
5.  The perceived search task dimension covers the actor's perception of the search task including information need types regarding the task and its performance process, and perceived information space.
6.  The document dimension covers document contents and genres and collections in various languages and media, which may contain information relevant to the task as perceived by the actor.
7.  The algorithmic search engine dimension covers the representation of documents or information and information needs. It also covers tools and support for query formulation and methods for matching document and query representations.
8.  The algorithmic interface dimension covers tools for visualization and presentation of information objects, collections and their organization.
9.  The access and interaction dimension covers strategies of information access, interaction between the actor and the interface (both in social and in system contexts).

Each of the dimensions is complex and contains multiple variables. In any single study relevant variables need to be explicated, depending on the goals of the study.

Observing the possible variation along the dimensions, it is obvious that information seeking and retrieval is performed in very diverse work and leisure situations characterized by diverse values on the dimensions. Thus, information seeking and retrieval becomes quite different. In many situations, actors performing their work tasks do not view information seeking and retrieval as separate aspects of their tasks and are ignorant about the conceptualizations of information seeking and retrieval prevailing in the research community. Professionally mediated information retrieval is a notable exception to the contrary. However, due to Web searching, most retrieval nowadays is based on direct end-user searching by the actors themselves. We believe that the actors mostly view information seeking and retrieval instrumentally, not as a goal in itself, and want to complete it quickly.

## Foci of traditional information seeking and retrieval research

The foci of traditional information seeking and retrieval research are illustrated in Figure 2 in the light of the nine dimensions. The columns identify three traditional research areas within information seeking and retrieval.

The face and traffic sign symbols represent the intensity by which the nine dimensions have been investigated or avoided in research. One may see that retrieval systems have been evaluated in traditional information retrieval research only for some limited use scenarios, mostly excluding searchers in the context of their work tasks. However, neither does current information seeking research provide much help in understanding this situation. While the information seeking practices of various actor populations have been investigated, much remains still unexplored. Moreover, the majority of information seeking studies does not look at information retrieval systems at all, or not at the level of system features, interaction and support for query formulation and searching. With this perspective in mind we do not really know how well current information retrieval systems serve their users in various situations.

<figure>

![fig2](../p212fig2.jpg)

<figcaption>

**Figure 2: Foci of traditional information seeking and retrieval research ([Ingwersen & Järvelin forthcoming](#ingwersen))**</figcaption>
</figure>

Both the information seeking and the information retrieval research areas need extension. We will however focus now on Information Seeking in the present paper.

## Goals of information seeking research

Information seeking research, over the years, has often been criticized for serious weaknesses. Herner and Herner ([1967](#herner)), and Brittain ([1975](#brittain)) were among the early critics, who argued that there were conceptual problems in defining information needs and seeking, and several methodological problems. Moreover, the studies were largely seen as useless due to unclear goals and lack of cumulation of findings: they were seen inapplicable for designing of information services. Brittain concluded that only a small part of what had been published in this area had been worth it. Hewins ([1990](#hewins)) summarized that deficits of conceptual frameworks, methodology and theory building have continued to plague the information seeking and retrieval research area. We can thus see that the critics have been around through all the years of information seeking studies. Those working in the area in the 1990s have not been very critical, but we believe that the sentiment has been, and still is, shared by many working in information retrieval. Until the emergence of information seeking research as a strong area for doctoral level research (mainly in the 1990s), the time appears not to have been ripe for theoretical and empirical breakthroughs.

This state of affairs leads one to the conclusion that the motivations for the study of information seeking should be examined. In principle, the motivations, and benefits, may lie in (a) theoretically understanding information seeking in the form of models and theories, (b) empirically describing information seeking in various contexts, and (c) providing support to the design of information systems and information management.

Developing theoretical understanding of a domain is a necessary task for any discipline. An essential issue is the definition of the domain. It should cover a meaningful system of phenomena that supports explanation and understanding. The theoretical understanding of information seeking clearly has advanced in the 1990s as the many models proposed indicate, e.g. Dervin's Sense-making Approach ([1983](#dervin83); [Dervin & Nilan 1986](#derviin86)), Ellis's ([1989](#ellis); [Ellis _et al._ 1993](#ellis93)) information seeking features, Kuhlthau's ([1991](#kuhlthau91)) process model, Wilson and Walsh's ([1996](#wilson96)) model of information behaviour, and Byström and Järvelin's ([1995](#bystrom)) model of task-based information seeking. Taken together they suggest a perspective covering phenomena from information systems and their design, through information access by various processes to work tasks: these are seen in the list of the nine dimensions. The focus of theoretical analysis, however, has been on the seeking process: its stages, actors, access strategies, and sources. Work tasks and information (retrieval) systems have received less theoretical attention as foci of modelling and theorizing (see, e.g., [Vakkari 2003](#vakkari03)).

Developing empirical understanding of phenomena within the domain is also necessary for a discipline. Theoretical understanding must be grounded on observables. Otherwise it turns into speculation. Information seeking phenomena in specific contexts are understood, explained and predicted by having theoretically justified findings on work and search tasks and their context. Again, with a few exceptions, the empirical findings focus on the seeking processes, with less attention to work tasks and information (retrieval) systems. The findings are typically descriptive rather than explanatory. The empirical studies have provided only few answers to research questions relating characteristics of contexts, situations and tasks to characteristics of seeking processes or the rest of the nine dimensions. The process-oriented modern approaches in information seeking, e.g., work based on Kuhlthau's model ([1993](#kuhlthau)) or Ellis's ([1989](#ellis)) _characteristics_ of information seeking behaviour, have covered several empirical domains, such as the social sciences and engineering, and some work task contexts, e.g., student information seeking ([Kuhlthau 1993](#kuhlthau); [Vakkari 2001](#vakkari01)). However, many remain unexplored. On the other hand, this is of course healthy for a research area: there remains something to do.

Supporting information management and information systems design may be the weakest contribution of information seeking so far. This may be understood through the previous slide. Studies in information seeking rarely include information (retrieval) system design features in their study settings. We mean features that the information (retrieval) system designers find relevant and deal with. In such a situation the research results cannot communicate to systems design, because the worlds do not touch. In principle, of course, it may also be the case that information retrieval system designers are busy with the wrong variables or features. While our understanding of work task requirements and effects on information seeking has advanced, the understanding on how to derive and apply design criteria for information (retrieval) systems has not advanced correspondingly.

## Information seeking in isolation

Having its roots in the _user studies_ area of librarianship, information seeking research has come a long way from research that revolved around the users of a single institution, source or channel. However, information seeking research, as such, still seems to be the study of the behaviour that takes place between tasks and information sources and cannot be theoretically justified as an isolated area. Why? From the actors' point-of-view, seeking does not always constitute an independent system, or meaningful system, of activities as a focus of attention. At least for many actors engaged in information seeking, it may be an activity that is not considered—if recognized at all—in isolation. They are just doing their work and might welcome better ways of doing it, but are not used to notice or discuss the seeking component in which the information seeking and retrieval community is interested.

From _user studies_ onwards, the pragmatic goal of improving information access has been one of the goals of information seeking research. Today, however, the context is much broader, encompassing many different ways of acquiring information in very varied situations. We believe that the pragmatic goal continues to be a major goal within information seeking. Therefore, information seeking should be studied in the context of work tasks and technology

## Augmenting task performance

Often, from the actor's point-of-view a proper system for analysing information access may be a system of activities: work task, leisure interests and the social organization of these activities. We may take a look at augmenting task performance. After all, augmenting task performance is an important goal in working life and also in leisure activities. In the former it may be related to effectiveness and efficiency, and in the latter, if not efficiency, then at least quality.

Engelbart ([1962](#engelbart)) of the Stanford Research Institute suggested forty years ago a conceptual framework for augmenting human intellect. In Figure 3 we apply it and present an exemplary means-ends hierarchy focusing on information seeking.

<figure>

![fig3](../p212fig3.jpg)

<figcaption>
 
**Figure 3\. Augmenting task performance ([Ingwersen & Järvelin forthcoming](#ingwersen))**</figcaption>
</figure>

At the top of the figure we have an actor's work task in context. In order to augment the actor's performance, one may improve her tools, her knowledge, her methods and/or her training in the application of the former. Improving knowledge and methods means acquiring new knowledge, which can be done by creating it, remembering it or through information seeking. Information seeking, again, may reach towards education, documents or other sources.

If one chooses documents, these may be documents immediately to hand, documents from colleagues or nearby collections, or documents retrieved from various databases through searching. If one chooses other sources than documents, these may be factual databases, colleagues, etc.

In this figure, information seeking and its manifestations may seem somewhat remote from the work task - with document retrieval even more remote and behind many decisions. Nevertheless, our view is that information seeking and retrieval belongs to a context in real life, such as the work task context. The distance does not make information seeking and retrieval independent of work tasks or other activities; it needs to contribute to them. This sets many requirements on augmentation through information seeking and retrieval if it is going to be useful and used in the long run.

## Information seeking and retrieval affects and is dependent on its context

Next, we demonstrate how information seeking and retrieval affects its context and is dependent on it. In fact, there is complex interaction between all task components. In Figure 4 we consider task goals, the task process proper, information acquisition, the information used in the task, and information systems.

<figure>

![fig4](../p212fig4.jpg)

<figcaption>

**Figure 4: The interaction of task components ([Ingwersen & Järvelin forthcoming](#ingwersen))**</figcaption>
</figure>
Very schematically, the figure shows that all components affect all other components. Whenever one changes, the change requires and/or causes changes in the other. For instance, information systems may affect the information that is available for the task. In some cases this may allow setting more demanding goals for the task and changes to the task process proper. This may bounce back new requirements on information systems: there are constant repercussions. In fact, in modern times of rapid technological change, a dynamic imbalance dominates the scene with only relatively short periods of relatively stable practices. Information seeking and retrieval is intimately connected to task performance indeed. The changes may be classified from simple to very pervasive, the simple ones being just the change of implementation like replacing pen and paper by a pocket calculator without touching anything else, and complex ones such as changes in the ultimate goals of work whereby also the process may totally change, information seeking processes included. Technology may be the cause of such changes but also the recipient of new requirements.

## An information seeking and retrieval design and evaluation framework

We argued that the pragmatic goal of information seeking research is to support information systems design and information management. In history, this meant support to the development of library services while nowadays it means support to the development of diverse, often computerized, information systems. If someone does not wish to consider such practical goals, s/he may replace _design and evaluation of information systems_ in the following by _the analysis of social and organizational practices_, with related _construction of meaning, if not feeling_ in a task or leisure activity setting.

A focus on design and evaluation does not at all exclude purely theoretical or empirical study goals or interests. Evaluation may be seen as the analysis of practices, system use and features from the actor viewpoint. After all, systems design and evaluation is best served by theoretically and empirically well-grounded knowledge.

<figure>

![fig5](../p212fig5.jpg)

<figcaption>
  
**Figure 5: Information seeking and retrieval design and evaluation frameworks ([Ingwersen & Järvelin forthcoming](#ingwersen))**</figcaption>
</figure>

Basically, following the pragmatic goal, we approach information seeking and retrieval design and evaluation as embedded contexts of retrieval, seeking and work tasks and interests. Information retrieval serves the goals of seeking, and information seeking the goals of the work task (interest). The same person symbol in all the three contexts denotes the same or another actor(s) performing the work task, the seeking task and the retrieval task; interpreting the tasks, performing the process and interpreting the outcome, possibly resulting in task reformulation in each context. Exemplary possible evaluation criteria in each context are given the labels A - D. Although not explicit, the nine dimensions discussed above are included in the Figure.

Reading from inside, we start by information retrieval design and evaluation as a special case in information seeking. Traditionally, information retrieval focuses on document and request representations and their matching and also request reformulation. As de-contextualized, information retrieval may be designed and evaluated in its own context, as is done in the computer-science-oriented, laboratory, information retrieval approach. In this confined context the evaluation measures are the traditional ones, recall and precision, or some recently proposed novel measures ([Borlund & Ingwersen 1998](#borlund); [Järvelin & Kekäläinen 2002](#jarvelin02)). In addition, one may assess the system's efficiency along various dimensions during information retrieval interaction, the quality of information (documents) retrieved, and the quality of the search process such as searcher's effort (time), satisfaction, and various types of moves or tactics employed: not so frequent in information retrieval research but important in practice.

However, information retrieval belongs to the searcher's information seeking context. In this context we have a seeking task, a practice of information seeking and some result, all more or less defined at the outset. In this context, information retrieval is but one means of gaining access to required information. This context provides a variety of information sources and systems and communication tools, all with different characteristics. Their use is based on the seeker's discretion and they are used in a concerted way. The design and evaluation of these sources and systems needs to take their joint usability and quality of information and process into account. One may ask what is the contribution of an information retrieval system, or any other system, in the end result of a seeking process, over time, across seeking tasks, and across seekers. Since the knowledge sources and systems are not used in isolation they should not be designed nor evaluated in isolation. They affect each other's utility in context. The evaluation criteria in the information seeking context are different: see at B in Figure 5.

An obvious counter-argument is that there are too many seeking contexts with too many possible combinations of systems, so that the design and evaluation of information retrieval systems becomes unmanageable. Therefore, it is better to stick to the tradition of information retrieval design and evaluation. If one knows no more than one's own unsystematic recollection of personal information retrieval system use, the suggested design and evaluation approach may be of tall order, indeed. However, even limited knowledge on real information seeking and retrieval may reveal typical uses, strengths and weaknesses of various systems, and how their users perceive them. This provides a better basis for design than the de-contextualized standard assumptions and measures. A nice parallel may be observed in the critique of information seeking research by Dervin and Nilan ([1986](#dervin)): _mutatis mutandis_.

Further, information seeking seldom is an end in itself but rather serves a work task. In the work task context we have an assigned and perceived work task, the task process and its outcome, again more or less well defined at the outset. The real impact of information seeking and retrieval is its contribution to the work task process - like effort and time - and the quality of its outcome. Therefore, in the end, information seeking and retrieval should be designed and evaluated for their utility in the work task context.

Again, an obvious counter-argument is that it may be difficult to discern the contribution of a particular system in the work task outcome without carefully designed experiments. Moreover, there are too many work task contexts that are too weakly related to information (retrieval) systems. The design and evaluation of information systems thus becomes unmanageable and cannot learn from all too remote task requirements. Therefore, the counter-argument goes, it is best to close one's eyes and stick to the tradition of design and evaluation. However, even limited knowledge of real work tasks may reveal typical uses, strengths and weaknesses of various practices and systems to be transferred from one context to another, and how their real users perceive them, as was the original idea behind the Cognitive Systems Engineering approach a decade ago ([Rasmussen & Pejtersen 1997](#rasmussen)).

## Conclusion: extending information seeking research

Two action lines are therefore needed. On the one hand, information retrieval research needs to be extended to capture more context. The real issue in information retrieval systems design is not whether its recall-precision performance goes up by a statistically significant percentage. Rather, it is whether it helps the actor solve the search task more effectively or efficiently. To achieve this it is necessary to learn how the actors can be helped. It certainly is possible to do more than just allow a window for entering two keywords and try to capitalize on that. Only by this line of action one may approach real information retrieval engineering. Information systems engineering allows one to specify necessary information system features by looking at analyses of information retrieval systems use in terms of tasks, users, documents and access requirements.

On the other hand, current information seeking research needs to be extended both towards task context and the information systems context. We appreciate the efforts so far exploring information seeking in diverse task and actor contexts but also think that the diversity of contexts is far from exhausted. For example, the Web is a popular target for many information seeking investigations. However, it is not a single coherent unit but appears quite different for different actors, tasks, and domains. Therefore, much research is needed that explores information seeking in various task and actor contexts. However, it is not sufficient to analyse just the in-between aspects of seeking activities. These need to be related to the task dimension. It is neither sufficient to perform just job-level analyses; one needs to look into individual tasks, because the latter vary heavily in their requirements for information and typical sources of information even within one job. Also, it becomes crucial to observe general patterns across tasks and contexts.

Moreover, the systems context in information seeking research so far has been limited and often nonexistent. This research should reach towards system and interaction features so that communication with system design is facilitated.

There is demand for studies that seek to bridge the dimensions of information systems and their interfaces, their use and task features. There is also much work to do. Research economy requires one to carefully consider the variability of contexts so that diverse types of contexts can be covered. One needs to carefully choose combinations of variables from various dimensions - in a nutshell, to try to incorporate variables that bridge task performance, information access and information (retrieval) systems. We believe that a further step, similar to the one in scope initiated by the Dervin and Nilan paper ([1986](#dervin)), is needed in information seeking research.

Paying due attention to the goal of augmenting work task performance and lay interests alike and the available information systems environments turn information seeking much closer to disciplines such as information management, information systems, organizational design etc. Information seeking research may lose some of its independence but gain a better ability to communicate across disciplinary boundaries, thereby becoming more relevant in the eyes of the others. We feel that there is much demand for research along these lines.

Note: This paper is a slightly edited version of the keynote address by the first author at the ISIC 2004 Conference in Dublin, Ireland.

## References

*   <a id="borlund"></a>Borlund, P. & Ingwersen, P. (1998). Measures of relative relevance and ranked half-life: performance indicators for interactive information retrieval. In W.B. Croft, A. Moffat, C.J. van Rijsbergen, R. Wilkinson & J. Zobel (Eds.), _Proceedings of the 21st Annual International ACM SIGIR Conference on Research and Development in Information Retrieval_, (pp. 324-331). New York, NY: Association for Computing Machinery.
*   <a id="brittain"></a>Brittain, M. (1975). Information needs and the application of the results of user studies. In A. Debons & W. Cameron (Eds.), _Perspectives in information science._ (pp. 425-447). Leyden, Netherlands: Noordhoff.
*   <a id="bystrom"></a>Byström, K. & Järvelin K. (1995). Task complexity affects information seeking and use. _Information Processing & Management_, **31**(2), 191-213.
*   <a id="dervin"></a>Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33
*   <a id="dervin83"></a>Dervin, B. (1983). _An overview of sense-making research: concepts, methods and results to date._ Paper presented at the International Communications Association Annual Meeting. Dallas, Texas.
*   <a id="dervin86"></a>Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33.
*   <a id="ellis"></a>Ellis, D. (1989). A behavioural approach to information retrieval design. _Journal of Documentation_, **45**(3), 171-212.
*   <a id="ellis93"></a>Ellis, D., Cox, D. & Hall, K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences. _Journal of Documentation_, **49**(4), 356-369.
*   <a id="engelbart"></a>Engelbart, D. (1962). _Augmenting human intellect: a conceptual framework_. Menlo Park, CA: Stanford Research Institute.
*   <a id="herner"></a>Herner, S. & Herner, M. (1967). Information needs and use studies in science and technology. _Annual Review of Information Science and Technology_, **2**, 1-34.
*   <a id="hewins"></a>Hewins, E.T. (1990). Information needs and use studies. _Annual Review of Information Science and Technology_, **25**, 147-172.
*   <a id="ingwersen"></a>Ingwersen, P. & Järvelin, K. (forthcoming). _The turn: integration of information seeking and retrieval in context._ Berlin: Springer/Kluwer.
*   <a id="jarvelin02"></a>Järvelin, K. & Kekäläinen, J. (2002). Cumulated gain-based evaluation of information retrieval techniques. _ACM Transactions on Information Systems_, **20**(4), 422-446.
*   <a id="jarvelin03"></a>Järvelin, K. & Wilson, T.D. (2003). [On conceptual models for information seeking and retrieval research](http://InformationR.net/ir/9-1/paper163.html). _Information Research, 9_(1) paper 163 Retrieved 10 February, 2004 from http://InformationR.net/ir/9-1/paper163.html
*   <a id="kuhlthau91"></a>Kuhlthau, C. C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371.
*   <a id="kuhlthau"></a>Kuhlthau, C.C. (1993). _Seeking meaning._ Norwood, NJ: Ablex.
*   <a id="pejtersen"></a>Pejtersen, A. M. & Rasmussen, J. (1997). Ecological information systems and support of learning: coupling work domain information to user characteristics. In M.G. Helander, T.K. Landauer & P.V. Prabhu (Eds.), _Handbook of human-computer interaction_. (pp. 315-345). Amsterdam: Elsevier.
*   <a id="vakkari01"></a>Vakkari, P. (2001). A theory of the task-based information retrieval process: a summary and generalization of a longitudinal study. _Journal of Documentation_, **57**(1), 44-60.
*   <a id="vakkari03"></a>Vakkari, P. (2003). Task-based information searching. _Annual Review of Information Science and Technology_, **37**, 413-464.
*   <a id="wilson"></a>Wilson, T.D. & Walsh, C. (1996). [Information behaviour: an interdisciplinary perspective.](http://informationr.net/tdw/publ/infbehav/prelims.html) Sheffield: University of Sheffield, Department of Information Studies. (British Library Research and Innovation Report 10) Retrieved 3 October, 2004 from http://informationr.net/tdw/publ/infbehav/prelims.html