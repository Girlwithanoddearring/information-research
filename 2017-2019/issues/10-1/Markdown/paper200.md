#### Vol. 10 No. 1, October 2004

# Field differences in the use and perceived usefulness of scholarly mailing lists

#### [Sanna Talja](mailto:sanna.talja@uta.fi), [Reijo Savolainen](mailto:reijo.savolainen@uta.fi) and [Hanni Maula](mailto:hanni.maula@sanoma.fi)  
Department of Information Studies, University of Tampere  
Tampere, Finland

#### **Abstract**

> Based on a qualitative comparative study across four domains, this paper explores how the use and perceived usefulness of scholarly mailing lists is related to primary search methods, collaboration patterns, loci of critical information, physical proximity of like-minded colleagues, field size, the desirability of sharing information in public or semi-public discussion fora, relevance criteria, the degree of scatter within a field, and book versus article orientation. The findings show the differential role of formal and informal computer-mediated communication across fields. Environmental biologists and nursing scientists saw little value in mailing lists for research purposes. They relied on their local collaborators as sources of support and advice. Historians and literature and cultural studies scholars experienced mailing lists as helpful in monitoring literature and progress of the field.

## Introduction

Electronic communication forums such as mailing lists and discussion groups are frequently approached and described by using metaphors such as "virtual communities", "cyberspace colleges", and "voluntary networks" (Jones, [2003](#jones): 43). These metaphors carry particular sets of assumptions about the function and significance of these forums. The metaphors imply the emergence of new social structures, new kinds of "invisible colleges", and new senses of identity and connectedness triggered by them (Gläser, [2003](#glaser): 39). In addition, such metaphors imply that the enabling technologies determine social outcomes and user behavior, rather than the social and cultural contexts into which they are embedded (Jones, [2003](#jones): 42).

Community and network metaphors are often intrinsically linked to ethnomethodologically informed, in-depth case studies of naturally occurring online communication in non-institutional and non-organizational contexts. CMC is most often approached by focusing on specific online spaces (cf. Liu, [1999](#liu)), user communities (cf. Baym, [1997](#baym)), organizations (cf. Levy & Foster, [1998](#levy)) or disciplines (cf. Brown, [2001](#brown)). The limitation of case studies is that they are often too culture-specific to enable the development of broader explanatory models. In addition, the viewpoints of non-users are often not equally well represented as those of users.

This study adopts a comparative approach to develop a theoretical understanding of the factors affecting the use and perceived usefulness of scholarly mailing lists.<sup>[1](#note1)</sup> The cross-disciplinary approach may seem an obvious choice in the study of mailing lists, since they are usually more academic and professional in comparison to newsgroups, and since earlier research has shown that there are important field differences in scholars' information practices. Few studies have explored field differences in the use of scholarly mailing lists, however (see [Herring 2002](#herring02): 115-117). Studies of scholarly mailing lists have focused on aspects such as the nature of scholarly mailing lists as discussion forums ([Burton 1994](#burton)), topics and contents of communications ([Berman 1996](#berman); [Wildemuth _et al_. 1997](#wildemuth)), membership and message contribution patterns ([Rojo & Ragsdale 1997](#rojo); [Sierpe 2000](#sierpe)), communication norms ([Herring 1999](#herring99)), and perceived usefulness ([Brown 2001](#brown)).

Studies that have looked at field differences in the use of computer-mediated communication ([Walsh & Bayma 1996](#walsh96); [Walsh _et al_. 2000](#walsh00); [Fry 2003](#fry)) have tended to focus on all forms. This paper adopts a more narrow focus, since, from an information-seeking-context viewpoint, personal e-mail and public forums such as mailing lists are likely to serve distinct purposes.

## Earlier studies on scholarly mailing lists

When electronic discussion lists emerged, they stimulated widespread expectations for increased communication and collaboration among scholars that would also result in increased productivity (cf. [Walsh _et al_. 2000](#walsh00)). They were, for instance, expected to:

> *   link those in need of information with the best sources of expertise,
> *   form links between scholars previously unknown to each other,
> *   enable discussion and knowledge sharing between academics and professionals or citizens,
> *   make it easier for individuals and groups to exchange information and ideas, and to cooperate in scholarly and educational endeavours, and
> *   extend the participatory base of "invisible colleges".

When the use of scholarly mailing lists became more common and widespread, it became increasingly clear that few of these visions would be fulfilled (cf. [Finholt 2002](#finholt): 76-77). The experience of building the Comserve electronic forum in communication studies ([Stephen & Harrison 1994](#stephen): 769) revealed a need to protect accomplished scholars from challenges to identity that might occur in encounters with novices or others who can not place them properly within existing disciplinary hierarchies. A number of studies indicate that the adoption of computer-mediated communication tools has not changed the established systems or forums in which scholarly reputations are built and maintained (cf. Star & Ruhleder, [1994](#star); Kling & McKim, [1999](#kling)), or enlarged nonelite scientists' access to "invisible colleges" (Finholt, [2002](#finholt): 75). Rather, computer-mediated communication tools that are not well integrated into these systems tend to remain unused. Empirical studies also indicate that scholarly mailing lists may involve differential benefits for senior and junior and elite and nonelite scientists (cf. Doty _et al_., [1991](#doty)). Without equal benefits from information exchanges, senior scholars may withdraw their participation ([ibid](#doty).).

In their study of the use of academic discussion lists in social sciences and humanities, Rojo and Ragsdale ([1997](#ragsdale)) noted the fragility of users' binding to these forums. This was manifest in transient membership and the general preference for a broadcasting recipient, rather than contributor, role. Messages were not, in general, deemed very important ([ibid](#rojo).). In a study of the use of mailing list by music scholars, Brown ([2001](#brown)) similarly observed a high rate of discontinuance, lack of active participation, and strong evidence for scholars' growing disenchantment with the value of this innovation for research purposes.

The next section reviews studies that explicitly focus on disciplinary differences in the use and uptake of computer-mediated communication.

## Comparative studies

Walsh and Bayma ([1996](#bayma)) examined the uptake and use of computer-mediated communication across four scientific domains, mathematics, physics, chemistry and experimental biology by using in-depth interviews, and found significant differences in the use of computer-mediated communication across disciplines. The use of bulletin boards and distribution lists was common in mathematics and physics, but much less common in experimental biology and chemistry. Mathematicians and physicists used computer-mediated communication for informal communication, while chemists and experimental biologists limited their use of computer-mediated communication to formal communication, to seeking information from online databases. They found the following factors influential in determining patterns of computer-mediated communication use:

> *   size of research field (large/small),
> *   market penetration,
> *   locus of critical information and degree of interdependence between research units, and
> *   technical limitations (Walsh & Bayma, [1996](#walsh): 689).

Researchers in small fields are, according to Walsh and Bayma ([ibid](#bayma)., p. 689), able to filter relevant literature through their knowledge of authors, while in large expansive fields information scanning takes place in the formal domain. They argue that the sharing of information in mailing lists is less desirable in market-penetrated fields such as chemistry and experimental biology. The locus of critical information in mathematics is the like-minded colleague, while in experimental biology, critical information is located primarily in the laboratory. In experimental biology, each research team is able to gather within its lab the set of materials and expertise needed to study a problem ([ibid](#bayma).: 691). Technical limitations mean that photos and drawings, for instance, are more difficult to translate electronically.

Through in-depth interviews, Fry ([2003](#fry)) studied patterns of computer-mediated communication uptake and use across three specialties: high-energy physics, social/cultural geography and corpus-based linguistics. According to Fry ([2003](#fry), pp. 19-20), the factors shaping patterns of computer-mediated communication uptake and use include, for instance:

> *   nature of intellectual territory (extent of interdisciplinarity and multidisciplinarity),
> *   concentrations of critical mass, levels and nature of competition,
> *   character of personal networks (close-knit or loose-knit) and extent of personal network ties,
> *   nature of work organization in formal collaborations (locally/centrally organized), and
> *   physical proximity of personal networks and collaborative activities.

Fry also noticed the differential dependence upon formal and informal communication. The physicists in Fry's study were heavy users of the centralised pre-print archive arXiV.org, while social/cultural geographers rarely used electronic databases to locate literature. Physicists engaged in centrally organized formal collaborations used collaboration-wide newsgroups, but their use of community-wide mailing lists was limited. Social/cultural geographers subscribed to mailing lists, because they were in a marginal position within their parent discipline, and often the sole representatives of their specialty within their departments. Corpus-based linguists considered online networks such as discussion lists to be part of their personal network due to the scarcity of researchers in this domain.

Walsh and Bayma's and Fry's studies focussed on several forms of computer-mediated communication. In a study that explored the patterns of use of e-journals and databases across domains, Talja and Maula ([2003](#talja)) explained field variation by the following interrelated domain factors:

> *   primary search method (directed searching, browsing, linking), <sup>[2](#note2)</sup>
> *   domain size (density of the universe of relevant documents),<sup>[3](#note3)</sup>
> *   degree of scatter within a field (low/high),
> *   primary relevance criteria (topical/paradigmatic), and
> *   book versus article orientation.

Talja and Maula found that although most scholars use a mix of different search strategies, there are clear differences in the relative importance of these methods across fields. Talja's and Maula's study corroborated earlier findings (cf. Green, [2000](#green)) according to which for humanities scholars browsing and chaining are often more effective techniques for identifying relevant literature than directed (descriptor-based searching). The findings supported the hypothesis presented by Bates ([2002](#bates02): 148) according to which scholars in very densely and sparsely populated research areas rely more on browsing and linking to identify relevant documents. Walsh and Bayma ([1996](#bates96)) and Fry ([2003](#fry)) similarly noted the effect of field size on patterns of computer-mediated communication use.

The degree of scatter and degree of interdisciplinarity and multidisciplinarity of a domain also influence information seeking patterns (Bates, [1996](#bates96)) and computer-mediated communication use. Scholars in low scatter fields are served by a small number of highly specialised journals, whereas in high scatter fields, relevant materials are dispersed across several disciplines and published in a large number of different journals ([Packer & Soergel 1979](#packer); Bates, [1996](#bates96)). Scatter is also related to the extent to which a field’s underlying principles are well developed, to the extent to which the literature of the field is well organized in the light of scholars' research interests and problems, and to the breadth and clarity of the demarcations around the subject area (Mote, [1962](#mote)).

Talja and Maula found field differences in information seeking patterns to be related also to differences in relevance criteria, and the nature of the research object. In humanities specialties such as media and cultural studies where research objects and problems could be constructed differently from diverse viewpoins, information seekers commonly attached their search strategies to particular conversations or paradigms. In such fields, the choice of theories or methodological approaches limits or widens the range of materials considered as relevant independently of the topic or phenomenon studied. In natural sciences, research objects are usually more stable and standardised, and searches are more commonly focused on the phenomenon or substance being studied. The distinction between topical and paradigmatic relevance resembles Walsh and Bayma's distinction between "author-based filtering" and "formal scanning", and Fry's "nature of intellectual territory".

Talja's and Maula's study also showed differences in e-journal and database use patterns in fields where books carried the most prestige and were regarded as the most important sources and contributions in comparison to fields where peer-reviewed articles were considered as the major contributions and most important sources. The relative importance of books versus articles may also affect mailing list use patterns, since Talja ([2002](#talja)) found that book-oriented scholars mainly share interpretations of documents, while scholars in fields where peer-reviewed articles are the most important sources share the relevant documents (directly), and information about document retrieval methods.

In summary, the studies reviewed above point to the differential roles of formal and informal communication across fields. These studies identified central factors that may be utilised in an empirically founded, multifaceted explanatory approach. We may thus hypothesise that the following factors are likely to impact the use and perceived usefulness of scholarly mailing lists: primary search methods, collaboration patterns, physical proximity of like-minded colleagues, field size, degree of scatter, loci of critical information, the desirability of sharing information in public or semi-public discussion forums, relevance criteria, and book versus article orientation.

## The empirical study

Based on semi-structured interviews, this study explores the role of scholarly mailing lists across four domains: nursing science, literature and cultural studies, history, and environmental biology. The empirical data was gathered in 2000 by qualitative thematic interviews of forty-four scholars representing two Finnish universities, one having a strong natural sciences orientation, and the other a social sciences and humanities orientation. The data on the role of scholarly mailing lists were gathered as part of a larger project, Academic IT cultures, that focused on scholars' use of networked resources. Scholarly mailing lists were one of the themes explored in the interviews. The interviews were designed to gain detailed qualitative data on scholars' work and information practices in the chosen fields. As argued by Paisley (1968: 4), the use of any specific information source or medium cannot be adequately understood without taking into account all the available sources and channels and their potentially divergent roles and values. The fact that the overall study did not focus solely on mailing lists strengthens, rather than weakens, the credibility of the results, because the viewpoint of non-users is equally well represented as that of users.

The aim of the selection of fields for the study was to enable comparisons between natural sciences, humanities, and social sciences. Environmental biology represents a laboratory science. History and literature are humanities fields, whereas media and cultural studies is an inter- and multidisciplinary field. Nursing science is an interdisciplinary field drawing on medical and social sciences.

Two humanities fields are represented in the study because among humanities scholars it was easiest to find both non-users of electronic networks and researchers involved in digital library and web publishing projects. Individual participants were chosen on the basis of the information given in departmental and individual researchers' homepages to represent differing research orientations and levels of research experience (see Table 1). During the interview series, understandings of field-specific information practices gained from the first interviews were used as grounds of comparison in subsequent interviews. This procedure enabled us to gain a sufficiently reliable picture of practices that were common to a field as well as variations related to topic or specialty. Of the forty-four participants, twenty-two are men and twenty-two women.  

<table><caption>

**Table 1: Profile of the informants**</caption>

<tbody>

<tr>

<th>Discipline</th>

<th>Junior researchers  
(predoctoral)  
</th>

<th>Senior researchers  
(postdoctoral)  
</th>

<th>Totals</th>

</tr>

<tr>

<td>

**Environmental biology**</td>

<td>3</td>

<td>7</td>

<td>10</td>

</tr>

<tr>

<td>

**Nursing science**</td>

<td>9</td>

<td>3</td>

<td>12</td>

</tr>

<tr>

<td>

**History**</td>

<td>3</td>

<td>8</td>

<td>11</td>

</tr>

<tr>

<td>

**Literature and cultural studies**</td>

<td>4</td>

<td>7</td>

<td>11</td>

</tr>

<tr>

<th>Totals</th>

<td>19</td>

<td>25</td>

<td>44</td>

</tr>

</tbody>

</table>

The interviews lasted an average of approximately one and a half hours. They were tape-recorded and transcribed in full for analysis. Data about the use of different types of computer and computer-mediated communication applications were also gathered by a separate short questionnaire. The recorded interview transcripts were coded and analysed thematically and interpreted by conducting comparisons across fields, specialisms and individual responses. The quantitative results are not generalisable in a statistical sense, rather, their role is to illustrate field differences. The limitation of the study is that because the number of respondents in each field is small (as mandated by the qualitative approach and the work involved in analysing qualitative data), it cannot provide reliable information about precise levels of use of mailing lists in the studied fields. Instead, the data enables an understanding of their relative value from the participants' point of view.

The specific reseach questions are:

> *   What are the perceived advantages and disadvantages of mailing lists?
> *   For what purposes are mailing lists used?
> *   What are the reasons for non-use?

## Results

As shown in Table 2, there were clear differences in patterns of mailing list use between the two humanities fields and nursing science and environmental biology.

<table><caption>

**Table 2: Field differences in the subscription of mailing lists**</caption>

<tbody>

<tr>

<th>Discipline</th>

<th>Subscribes  
</th>

<th>Does not subscribe  
</th>

<th>Totals</th>

</tr>

<tr>

<td>

**Environmental biologists**</td>

<td>2</td>

<td>8</td>

<td>10</td>

</tr>

<tr>

<td>

**Nursing scientists**</td>

<td>3</td>

<td>9</td>

<td>12</td>

</tr>

<tr>

<td>

**Literature and cultural studies scholars**</td>

<td>8</td>

<td>3</td>

<td>11</td>

</tr>

<tr>

<td>

**Historians**</td>

<td>7</td>

<td>4</td>

<td>11</td>

</tr>

<tr>

<td>

**Totals**</td>

<td>20</td>

<td>24</td>

<td>44</td>

</tr>

</tbody>

</table>

### Environmental biology

Two environmental biologists subscribed to international mailing lists, but one was considering unsubscribing. Some others in this field had previously subscribed to mailing lists, but had estimated lists to be of limited usefulness in relation to the time and effort they required for handling and deleting incoming mail. One scholar explained that:

> the most useful feature was information about international meetings. That was useful, but the messages started to get so miscellaneous that there was no sense in it any more. (Senior researcher)

The most useful feature of mailing lists for environmental biologists was information about international meetings. Scholars in this field had little appreciation for the communicative, community-building, or networking potentials of mailing lists. Nor did they report having acquired useful information about publications through mailing lists.

> I am a subscriber of the X.X<sup>[4](#note4)</sup> list. Maybe once a week I get a really useful message containing information that I need. In principle it is a really good forum for sharing information, but the fact that some people use it for conversing with each other makes it a bad system and really annoying from time to time. (Junior researcher)

Environmental biologists generally work in groups in which research priorities are set, experiments are planned, research techniques and results are discussed, and information about relevant documents are shared. The kind of collaborations these scholars were heavily involved in were primarily local and national in nature. In this respect, they differed from the kind of close-knit international collaborations typical for, for instance, the high-energy physicists in Fry's ([2003](#fry)) study who used collaboration-wide mailing lists as a means of keeping partners up-to-date about the progression of the project. The local research group and laboratory was the locus of critical information for environmental biologists. The one environmental biologist who did find mailing lists useful differed from her colleagues in one important respect: the colleagues in the research groups she belonged to were situated not in her workplace, but in other institutions. Thus, the lack of physical proximity of colleagues with exactly matching interests explains this "deviant" case among environmental biologists. She also differed from her colleagues in that she described her research interests as "relatively wide" and therefore as requiring also journal scanning, and not solely relying on descriptor-based searching.

Environmental biologists rarely browsed. Peer-reviewed journal articles were the most important sources for them. They searched relevant material and also monitored their field through conducting directed descriptor-based searches in electronic databases and the _Current Contents_ CD-ROM that they described as containing all the core journals of their field. After having located relevant articles in databases, they could approach their authors by requesting copies. This practice could sometimes generate contacts between scholars working with similar topics, but conferences were described as the main avenue for building professional networks and for sharing experiences.

### Nursing science

Three nursing scientists subscribed to national or international mailing lists. All three found the lists to some extent useful for monitoring current discussions, but they read the messages only if they had time to spare:

> Mailing lists are to some extent useful. X.X. helps me in keeping abreast with current discussions, what issues are found as deserving attention, but I mainly look at the headings, and delete the messages, or, if they are interesting, read them. When a lot of messages come from the list I simply delete them all without reading. (Junior researcher)

The subscribers' research was directly related to the issues discussed in the list, and this was the reason for its subscription. After having familiarised themselves with this medium, most nursing scientists had decided they did not need it. Reflecting the general view of nursing scientists, one of the subscribers explained that all information activities should have a clear goal and clear gains, and she was considering quitting the list. There was a clear lack of fit between nursing scientists' information practices and mailing lists as information communication technologies.

Both nursing scientists and environmental biologists place a high value on systematic literature review: using a rigorous non-biased methodology to collect all topically relevant peer-reviewed articles, extracting information from those articles and synthesising the results. Especially clinically oriented nursing scientists have to relate their research to the medical knowledge base. Nursing scientists searched and monitored literature mainly through conducting keyword searches in "official bibliographic databases" such as Cinahl and Medline. As nursing science is an interdisciplinary and high scatter field, scholars in this field preferred to use aggregated e-journal databases that enable searching across fields.

Local research groups and their members were the loci of critical information also for nursing scientists. Although nursing scientists' research topics are more variable than those of environmental biologists, various topics are streamlined to research groups or longitudinal close-knit projects binding together scholars within a nursing specialty (e.g., nursing education, preventive nursing research). Literature, ideas, advice, and support were available to nursing scientists within these groups.

### History and literature and cultural studies

Eight out of the eleven literature and cultural studies scholars interviewed subscribed to mailing lists, and seven out of eleven historians. Humanities scholars generally had a much more positive view of mailing lists and their usefulness than scholars in the other fields studied. The non-subscribers (7/22) were either non-users or low users of electronic communication media. They worked in research areas (e.g., Finnish literature) where access to the Internet and its resources was relatively unimportant because of the scarcity of networked resources directly relevant for their research. Especially scholars specialised in Finnish history and Finnish literature work in sparsely populated research areas (they are searchers of "needles in haystacks", Bates, [2002](#bates02)), and their primary search methods are browsing and linking. For these scholars, mailing lists are valuable in that they amplify and extend these search methods. For instance, one historian said that mailing lists "help me to find my way to relevant Websites".

For senior scholars in sparsely populated research areas, manual browsing through catalogues, archives and books was supplemented by browsing through personal networks in the hope of finding relevant material. Mailing list were seen as valuable because they provide a corresponding opportunity for social browsing and information encountering:

> I have had the opportunity to go to international conferences, so I know the names of the people who are studying these issues and am acquainted with a number of them. Through them I get to know if someone has published something that is worth checking. Also through mailing lists I get information about relevant sources. (Junior scholar, literature and cultural studies)

The scholar cited above also explained that "I do not really use article databases, because I know that there does not exist much literature on my topic, and most of the existing literature represents a different viewpoint, it is social scientific and not my stuff". Information about relevant material received through the mailing list he subscribed to could, in turn, be trusted to represent "the right viewpoint". Lists were thus also a means of social and collaborative literature filtering, and provided access to groups in which epistemic and methodological positions are built and discussed:

> I do not search literature through keyword searches, that would sink me in a swamp. I use old-fashioned methods, looking at the references in other studies, and technology enables me to follow international mailing lists and the discussions going on in them. Lists focussed on a specialty or a specific issue provide me information about new literature. Libraries' databases are not, at least in our field, valid in the sense that you could find what you are looking for by using index terms. (Junior scholar, literature and cultural studies)

Many humanities scholars subscribed to several mailing lists. One media and cultural studies scholar, for example, subscribed to lists in the fields of communication, cultural studies and film studies. Especially media and cultural studies scholars often had a wide range of interests, and they viewed directed subject searches as requiring too narrow and limited a focus.

Mailing lists were not only perceived as useful for encountering information on the scholars own topic. Most historians and many literary scholars subscribed to a national list called "The History Network" that publishes reviews of humanities and social sciences books. This list was seen as providing an opportunity to monitor new literature and the domain more widely:

> Lists provide information that is for the most part useful for my work. Book reviews, etc. Lists are not central for seeking material, but for keeping in touch. (Senior researcher, history)

Humanities scholars also saw lists as useful for keeping in touch with research lines, "what people are doing", or "what is now current". Some humanities scholars said that lists provide ideas and intellectual stimulation, however, they also reported that the need for stimulation and responsiveness to others' ideas varies greatly according to the phase of research. Ideas were welcome in the initiation phase, while during writing and ending phases ([Ellis 1993](#Ellis)) lists could be experienced as a disturbance.

> If you have been away, you may have 300 new e-mails. I pretty soon gave the lists up, although I have resubscribed to some of them and then unsubscribed from them again. It depends on my mood and mental state whether I want to get stimulation from others, or whether I do not want any distraction. (Junior researcher, literature and cultural studies)

While all nursing scientists, environmental biologists, and historians who subscribed to mailing lists preferred the recipient role, some literary and cultural studies doctoral students also participated in the communications and sharing of information concerning publications in the lists. However, also literature and cultural scholars studies saw the provision of information concerning conferences and other incoming events as the primary and most useful function of mailing lists.

Even humanities scholars who did subscribe to mailing lists did not deem their messages very important, and were not committed to them. When busy, they discarded all messages without reading them. Humanities scholars easily joined lists and permanently or temporarily resigned from them if they experienced e-mail or work overload. One indicator of the greater usefulness of mailing lists to humanities scholars in comparison to the other two fields studied, was, however, that many of them had developed systems for managing group mail. For instance, to prevent the overflowing of their regular e-mail, some had acquired a separate e-mail address for list e-mail, and some automatically re-routed list e-mail to a folder. Both practices were designed to enable the rapid effective scanning of list mail at a convenient moment.

History and literature and cultural studies are fields where the range of topics studied is wide. Many historians and literary scholars were the only representatives of their specialist fields in their departments and universities. In the past, conferences and seminars provided these scholars the only opportunity to discuss their work with colleagues with similar interests. Mailing lists were especially important for scholars who were the sole experts in their specialty in their home institution (or their home country). They were experienced as relieving the loneliness of research work, and providing an opportunity to maintain a sense of the international progress of the field.

In general, doctoral students were more avid subscribers of mailing lists than professors. A professor in history expressed the view that mailing lists are invaluable especially for doctoral students not located in their home institutions. He saw that mailing lists provide the geographically dispersed history scholars a place where they can become aware of each other and ongoing projects.

## The differential role and use of formal and informal communication media across fields

This study corroborated earlier findings ([Walsh & Bayma 1996](#walsh)) indicating that in fields with heavy reliance on "formal scanning" and directed subject searches, mailing lists are not perceived as useful sources of materials or advice. Nursing scientists and environmental biologists generally saw little value in mailing lists for research purposes. They placed a strong emphasis on systematic literature reviews, and had little appreciation for discussion, debate, and sharing of information and ideas in public or semi-public forums.

For humanities scholars, sharing interpretations of documents is a central part of research practice. Humanities scholars are often "author-filterers" ([Walsh & Bayma 1996](#walsh)) and conversation-oriented seekers for whom the locus of critical information is the like-minded colleague. History and literary studies scholars subscribed to mailing lists and followed them substantially more often also because they work in fields where the range of topics studied is wide and scholarly communities are geographically dispersed. In comparison, in enviromental biology, scholars in one institution focus on a few topics and research lines. For environmental biologists and nursing scientists local research groups and collaborative projects were the loci of critical information.

Even mailing lists that covered a relatively narrow range of interests had not - in any field studied - created direct contacts between researchers sharing similar interests. All the partipants in this study reported that they had created their personal and international scholarly contacts and networks mainly in conferences and seminars, and that these contacts were later kept up by personal e-mail. Thus, although the discussions going on in scholarly mailing lists may resemble the discussions in conferences (Burton, [1994](#burton)), and in the same way may provide an opportunity for monitoring current research lines, scholars' networks and invisible colleges still rely on face-to-face contacts. Little evidence was found in this study of mailing lists' ability to forge links between scholars. Rather, this study corroborated the findings of earlier studies concerning the fragility of scholars' binding to these forums.

## Conclusion

Contrary to the widespread assumption in computer-mediated communication studies (often leaning on virtual community metaphors), sociability and social presence were not very desirable features in scholarly mailing lists. Scholars were more likely to unsubscribe from high-traffic mailing lists containing discussion and debate than low-traffic lists that were restricted to queries and announcements. Even humanities scholars who experienced mailing lists as useful for keeping updated in current discussions and monitoring literature easily unsubscribed from lists when they experienced information overload. The disadvantages of mailing lists in terms of time required for reading and handling messages, and the challenge for developing an effective personal e-mail management system easily overweighed their benefits. The preferred type of mailing list was a message board providing information about conferences and other events, and new publications.

As a cross-disciplinary comparative study, this study contributed to the understanding of the factors underlying the use and perceived usefulness of scholarly mailing lists. This study filled a gap on existing research on scholarly mailing lists by applying an information seeking in context perspective, which enabled an understanding of the role of mailing lists in the context of various sources and channels available to scholars. In the context of information seeking research, this study contributed to the body of empirical research seeking to identify factors that explain variation in scholars' information practices. Many classic and contemporary works embed scholars' information practices within the overarching context of disciplinary differences, and aim at forming holistic understandings of scholars' work and communication practices. However, not many attempts have been made to develop a more systematic comparative approach for explaining scholars' information practices. Building on earlier studies (Walsh & Bayma, [1996](#walsh); Fry, [2003](#fry); Talja & Maula, [2003](#talja)) the present study applied a multifaceted empirically grounded framework. The framework also provides tools for characterising and comparing the features of sub-domains. For example, in fields such as history there are several specialties where the significance of mailing lists may vary. Comparative studies that enable a deeper understanding of scholarly communities and their information practices can significantly help in designing information systems to meet the needs of those communities.

## <a id="notes"></a>Notes

<a id="note1"></a>

1. Electronic discussion lists - also called distribution lists, mailing lists, or listservs - distribute e-mail messages posted to a listserver (or listserv) to a list of subscribers. Discussion lists, like e-mail, are textual and asynchronous. Their function is to enable the transmission of information to multiple recipients (subscribers) simultaneously and to enable the exchange of ideas on a certain subject area. Typical categories of contents in discussion lists are: a) announcements: information about training courses, initiatives, conferences, seminars and publications, b) queries: requests for information, query responses, and c) discussion of issues (Berman, [1996](#berman); Wildemuth _et al_., [1997](#widemuth)). There are three types of mailing lists: a) moderated lists, where messages are filtered by a person who approves them for distribution, b) digest lists, where the messages are compiled by a list owner or moderator and sent to subscribers periodically, and c) unmoderated lists, where every posting sent to the list goes directly to receivers. Mailing lists may include “from one to many”, “from many to one” and “from many to many” communication (Savolainen, [2001](#savolainen), pp. 69-70). In the case of “from one to many”, a person addresses a question to other participants. In the case of “from many to one”, several participants answer the question. “Many to many” type communication may manifest itself in that several people forming a group approach others to communicate a viewpoint or to seek information.  
<a id="note2"></a>
2. Directed searching means doing subject searches by using databases whose materials have been indexed, catalogued and classified, and which provide elaborate retrieval (e.g., free-text search) capabilities (Bates, [2002](#bates02): 142). Browsing may be conducted either individually or socially through formal or informal collaboration, and it can be either purposive and directed or undirected and random (e.g., information encountering on the web). Linking may involve following the links provided in the web or chaining by following the references of seed documents (Bates, [2002](#bates02)).  
<a id="note3"></a>
3. Domain size refers to the amount of relevant documents available in a search space or domain in relation to all materials in the area (Bates, [2002](#bates02)).  
<a id="note4"></a>
4. The names of lists have been removed to protect the anonymity of participants.

## References

*   <a id="bates96"></a>Bates, M. J. (1996). Learning about the information seeking of interdisciplinary scholars and students, _Library Trends_, **45**(2), 55-164.
*   <a id="bates02"></a>Bates, M. J. (2002). Speculations on browsing, directed searching, and linking in relation to the Bradford distribution. In H. Bruce, R. Fidel, P. Ingwersen & P. Vakkari (Eds.), _Emerging frameworks and methods: proceedings of the fourth international conference on conceptions of library and information science (CoLIS4), July 21-25, Seattle, WA._ (pp. 137-149). Greenwood Village, CO: Libraries Unlimited.
*   <a id="baym"></a>Baym, N. (1997). Interpreting soap operas and creating community: inside an electronic fan culture. In S. Kiesler (Ed.), _Culture of the Internet_ (pp. 103-120). Mahwah, NJ.: Lawrence Erlbaum.
*   <a id="berman"></a>Berman, Y. (1996). Discussion groups in the Internet as sources of information: the case of social work. _Aslib Proceedings_, **48**(2), 31-36.
*   <a id="brown"></a>Brown, D.C. (2001). [The role of computer-mediated communication in the research process of music scholars: an exploratory investigation.](http://informationr.net/ir/6-2/paper99.html) _Information Research, 6_(2), paper 99\. Retrieved 2 February 2004 from http://informationr.net/ir/6-2/paper99.html.
*   <a id="burton"></a>Burton, P. (1994). Electronic mail as an academic discussion forum. _Journal of Documentation_, **50**(2), 99-110.
*   <a id="doty"></a>Doty, P., Bishop, A.P., & McClure, C. R. (1991). Scientific norms and the use of electronic research networks. _ASIS'91: Proceedings of the 54th Annual Meeting of the American Society for Information Science_ 24-38.
*   <a id="ellis"></a>Ellis, D. (1993). Modeling the information-seeking patterns of academic research: a grounded theory approach. _Library Quarterly_, **63**(4), 469-486.
*   <a id="finholt"></a>Finholt, T. (2002). Collaboratories. _Annual Review of Information Science and Technology_, **36**, 73-107.
*   <a id="fry"></a>Fry, J. (2003). _The cultural shaping of scholarly communication within academic specialisms_. PhD thesis, University of Brighton.
*   <a id="glaser"></a>Gläser, J. (2003). What Internet use does and does not change in scientific communities. _Science Studies_ **16**(1), 38-51.
*   <a id="green"></a>Green, R. (2000). Locating sources in humanities scholarship: the efficacy of following bibliographic references. _Library Quarterly_, **70**(2) 201-229.
*   <aside id="herring99">Herring, S.C. (1999). Posting in a different voice: gender and ethics in computer-mediated communication. In P. Mayer (Ed.), _Computer media and communication: a reader_ (pp. 241-265). Oxford: Oxford University Press.</aside>

*   <a id="herring02"></a>Herring, S.C. (2002). Computer-mediated communication on the Internet. _Annual Review of Information Science and Technology_, **36** 109-168.
*   <a id="jones"></a>Jones, Q. (2003). Applying cyber-archaeology. In K. Kuutti, E.H. Karsten, G. Fitzpatrick, P. Dourish, & K. Schmidt (Eds.) _ECSCW 2003: Proceedings of the Eight European Conference on Computer Supported Cooperative Work 14-18 September 2003, Helsinki, Finland_ (pp. 41-60). Amsterdam: Kluwer.
*   <a id="kling"></a>Kling, R. & McKim, G. (2000). Not just a matter of time: field differences and the shaping of electronic media in supporting scientific communication. _Journal of the American Society for Information Science_, **51**(14), 1306-1320.
*   <a id="levy"></a>Levy, P. & Foster, A. (1998) Communicating effectively in the networked organization: using electronic mail in academic libraries. _Journal of Documentation_, **54**(5), 566-583.
*   <a id="liu"></a>Liu, G.Z. (1999). [Virtual community presence in Internet relay chatting](http://www.ascusc.org/jcomputer-mediated%20communication/vol5/issue1/liu.html). _Journal of Computer-Mediated Communication_, **5**(1). Retrieved 15 January 2004 from http://www.ascusc.org/jcomputer-mediated communication/vol5/issue1/liu.html.
*   <a id="mote"></a>Mote, L.J.B. (1962). Reasons for the variation of information needs of scientists, _Journal of Documentation_, **18**(4),169-175.
*   <a id="packer"></a>Packer, K. H. & Soergel, D. (1979). The importance of SDI for current awareness in fields with severe scatter of information. _Journal of the American Society for Information Science_, **30**(3) 125-135.
*   <a id="paisley"></a>Paisley, W. J. (1968). Information needs and uses. _Annual Review of Information Science and Technology_, **3**, 1-30.
*   <a id="rojo"></a>Rojo, A. & Ragsdale, R.G. (1997). A process perspective on participation in scholarly electronic forums. _Science Communication_, **18**(4), 320-341.
*   <a id="savolainen"></a>Savolainen, R. (2001). Living encyclopedia or idle talk? Seeking and providing consumer information in an Internet newsgroup. _Library & Information Science Research_, **23**(1), 67-90.
*   <a id="sierpe"></a>Sierpe, E. (2000). Gender and technological practice in electronic discussion lists: an examination of JESSE, the Library/Information Science Education Forum. _Library & Information Science Research_, **22**(3), 273-289.
*   <a id="star"></a>Star, S. L. & Ruhleder, K. (1996). Steps toward an ecology of infrastructure: design and access for large information spaces. _Information Systems Research_, **7**(1), 111-134.
*   <a id="stephen"></a>Stephen, T. & Harrison, T.M. (1994). Comserve: moving the communication discipline online. _Journal of the American Society for Information Science_, **45**(10), 765-770.
*   <a id="talja02"></a>Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. _New Review of Information Behavior Research_, **3** 143-160.
*   <a id="talja03"></a>Talja, S. & Maula, H. (2003). Reasons for the use and non-use of electronic journals and databases: a domain analytic study in four scholarly disciplines. _Journal of Documentation_, **59**(6), 673-691.
*   <a id="walsh96"></a>Walsh, J.P. & Bayma, T. (1996). Computer networks and scientific work. _Social Studies of Science_, **26**(4), 661-703.
*   <a id="walsh00"></a>Walsh, J.P., Kucker, S. & Maloney, N.G. (2000). Connecting minds: computer-mediated communication and scientific work. _Journal of the American Society for Information Science_, **51**(14), 1295-1305.
*   <a id="wildermuth"></a>Wildemuth, B., Crenshaw, L., Jenniches, W. & Harmes, C.J. (1997). What's everybody talking about: message functions and topics on electronic lists and newsgroups in information and library science. _Journal of Education for Library and Information Science_, **38**(2), 137-156.