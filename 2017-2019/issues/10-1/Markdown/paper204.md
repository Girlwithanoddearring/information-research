#### Vol. 10 No. 1, October 2004

# Discipline, availability of electronic resources and the use of Finnish National Electronic Library - FinELib

<table>

<tbody>

<tr>

<td>

#### Sanna Törmä  
Nokia Corporation  
Visiokatu 6, 33720 Tampere, Finland

</td>

<td> </td>

<td>

#### [Pertti Vakkari](mailto:pertti.vakkari@uta.fi)  
Department of Information Studies  
University of Tampere, Tampere, Finland

</td>

</tr>

</tbody>

</table>

#### **Abstract**

> This study elaborated relations between digital library use by university faculty, users' discipline and the availability of key resources in the Finnish National Electronic Library (FinELib), Finnish national digital library, by using nationwide representative survey data. The results show that the perceived availability of key electronic resources by researchers in FinELib was a stronger predictor of the frequency and purpose of use of its services than users' discipline. Regardless of discipline a good perceived provision of central resources led to a more frequent use of FinELib. The satisfaction with the services did not vary with the discipline, but with the perceived availability of resources.

## Introduction

Several factors have been found to be associated with the use of electronic resources and digital libraries by university faculty. Subject discipline, status, sex and age are typical factors related to the use of electronic libraries in this group ([Tenopir, 2003](#tenopir)). Studies show that the discipline of researchers is connected with the use of literature and libraries in the traditional ([Garvey, 1979](#garvey)) as well as in electronic format ([Abels _et al._ 1996](#abels); [Eason _et al._ 2000](#eason); [Tenopir, 2003](#tenopir)).

The analysis of the associations between the use of electronic resources and libraries and factors explaining this use is typically done variable by variable. These studies identify and name the factors, but relate them to use one by one. They do not inform us how various factors co-vary with each other and with the use of electronic resources. Thus, it is not possible to show how various factors interact in explaining variance in the use of electronic resources; that is, it is not possible to reveal how various factors are jointly associated with use. Although some studies elaborate these relationships (e.g., [Eason _et al._, 2000](#eason); [Talja & Maula 2003](#talja03)), their conclusions are often based on relating use to single, rather than several, variables.

One of the major factors explaining the use of electronic libraries is the scholar's discipline ([Borgman, 2000](#borgman); [Tenopir, 2003](#tenopir)). It seems that representatives of science and medicine use electronic resources more frequently than humanists and social scientists ([Borgman, 2000](#borgman); [Tenopir, 2003](#tenopir)). Also the availability of relevant electronic resources affects how frequently scholars use them. ([Eason _et al._, 2000](#eason); [Abels _et al._, 1996](#abels)). The provision of electronic resources varies among disciplines. They are most available in science and medicine and least in social sciences and humanities ([Borgman, 2000](#borgman); [Kling & McKim, 1999](#kling)). It is evident that it is not only the characteristics of a discipline, but also the availability of electronic resources in the discipline, that explains the use of electronic resources and libraries. Consequently, if the availability of electronic resources in libraries was more even, there would not be such great differences in their use among the disciplines. Some results refer to this ([Eason et al., 2002](#eason); [Abels _et al._, 1996](#abels)), although the conclusions are not based on the elaboration of the relationships among these factors but on a variable-by-variable analysis.

In this paper we analyse how the frequency and type of use of nationwide electronic library, FinELib, is associated with the disciplines of the faculty of Finnish universities and with the perceived availability of the electronic resources relevant to the faculty.

## Earlier results

According to numerous studies (e.g., [Covi, 1999](#covi99); [Eason _et al._, 2000](#eason); [Kling & McKim, 1998](#kling); [Tenopir, 2003](#tenopir)) user's discipline and institutional context strongly affect the use of electronic resources. Electronic resources are typically most widely used in natural and technical sciences. In humanities and social sciences these resources are used less often.

Some studies indicate as well that the availability of electronic resources correlates with the use of these resources. However, there are far fewer of these studies than studies that prove the influence of disciplinary factors.

Abels _et al._ ([1996](#abels)) explored factors that affect the adoption and use of electronic networks and network services by science and engineering faculties in small universities and colleges. They found that the perceived utility of the network services correlated significantly with intensity of use and number of services used. They pointed out that perceptions of utility might be influenced by factors such as academic discipline and task. Relationships between these factors were not elaborated upon.

In the SuperJournal Project, Eason _et al._ ([2000](#eason)) showed that academic users' perceptions of the contents (including both coverage and relevance) of electronic journals provided and ease of use of the system were the most significant factors affecting patterns of use. Users' perceptions of both factors were affected by a range of intervening factors such as discipline and status. They suggest that the disciplinary differences in the use of electronic journals were in part associated with the differences in the coverage of SuperJournal and the varied availability of competitive services. Thus, there is indirect evidence that both scholars' discipline and the availability of relevant material interact in the use of electronic resources in libraries.

In a case study of the patterns of use of electronic networks by researchers in four disciplines Talja & Maula ([2003](#talja03)) found that the provision of material in scholars' research topics had a greater influence on the use of networked resources than their discipline.

In all, although there are studies that suggest that both users' discipline and the availability of resources correlate with the use of electronic resources, there is only scattered empirical evidence based on case studies of how these two factors are related to use.

### Research design

The aim of this study is to analyse how the discipline of university faculty and the availability of material central to the discipline are connected to the frequency and purpose of use of FinELib. We also explore how these two factors are associated with overall satisfaction with the services provided by FinELib. Moreover, we analyse how the use of FinELib is related to the prestige of its services in the users' community and this varies with discipline or the perceived availability of electronic resources.

#### The Finnish National Electronic Library

The [National Electronic Library, FinELib](http://www.lib.helsinki.fi/finelib/english/index.html), is part of the Finnish National Library's online services for libraries. It was established in 1997 and consists of a consortium of universities, polytechnics and research institutes. It offers about 8,200 full-text, online journals, and 120 reference databases, dictionaries and reference books. Each library decides individually which collection of these resources it buys. Thus, the availability of resources differs from library to library mainly according to the disciplines represented in the member institution of the consortium.

#### Data collection

The data used in this study were collected by FinELib as its annual user survey using a Web-based questionnaire. The questionnaire was posted in FinELib's homepage from the 18th to the 29th November, 2002\. It contained questions about the use and perceptions of FinELib as well as other electronic resources and was addressed to people who work or study at the universities. In total, 629 members of faculty and PhD students from Finnish universities filled in the questionnaire.

As the analysis below shows the sample is, with some reservations, representative of all Finnish universities. This kind of nationwide survey on the use of electronic libraries is relatively rare. Earlier studies have typically been smaller-scale case studies. The size and representativeness of the data gives a reliable point of departure for exploring the relations between discipline, availability of resources and use of FinELib.

#### Sample

The population of this study is faculty and PhD students in Finnish universities. The (self-selected) sample consists of 629 faculty members and PhD students who filled in the questionnaire of the annual user survey of FinELib in 2002\. Thus, the sample consists of self-selected volunteers and may be biased towards those who are most active users of digital libraries. The analysis below indicates that the sample is reasonably representative by sex, discipline, occupation and university. However, some later findings hint that it is likely that active users of digital libraries are somewhat over-represented in the data.

Inspection of the demographics of the respondents showed that the sample's breakdown by sex was nearly equivalent to the population. 46% of university faculty and PhD students in Finland were women in 2002 ([Kota database of university statistics](http://www.csc.fi/kota/aihelista1.html)), whereas in this study 45 % of the respondents were women.

<table><caption>

**Table 1: The breakdown of sample and population by discipline (%)**</caption>

<tbody>

<tr>

<th>Discipline</th>

<th>Sample  
(n=629)  
</th>

<th>Population  
(n=35385)*  
</th>

</tr>

<tr>

<td>Engineering</td>

<td>20</td>

<td>25</td>

</tr>

<tr>

<td>Humanities</td>

<td>18</td>

<td>24</td>

</tr>

<tr>

<td>Natural science</td>

<td>24</td>

<td>18</td>

</tr>

<tr>

<td>Medicine and nursing</td>

<td>11</td>

<td>12</td>

</tr>

<tr>

<td>Social science</td>

<td>10</td>

<td>12</td>

</tr>

<tr>

<td>Economics</td>

<td>16</td>

<td>8</td>

</tr>

<tr>

<td>Missing</td>

<td>1</td>

<td>3</td>

</tr>

<tr>

<td>Total</td>

<td>100</td>

<td>100</td>

</tr>

<tr>

<td colspan="3">* Source: Kota database</td>

</tr>

</tbody>

</table>

The sample's breakdown by discipline is presented in Table 1\. Natural sciences and economics are over-represented in the data. In particular, representatives of economics have been very eager to participate in the survey. Humanists and the representatives of engineering are clearly under-represented.

There were differences between the sample's and the population's breakdown by status (Table 2). PhD students were heavily under-represented in the sample. This is probably due to the fact that a large proportion of PhD students are studying part-time and, therefore, not working so intensively on their dissertations. However, we are interested in the use of FinELib by full-time academics, and in this respect the sample is more representative as it appears.

<table><caption>

**Table 2: The breakdown of sample and population by status (%)**</caption>

<tbody>

<tr>

<th>Occupation</th>

<th>Sample  
(629)</th>

<th>Population (N=35385)*</th>

</tr>

<tr>

<td>PhD students</td>

<td>30</td>

<td>62</td>

</tr>

<tr>

<td>Assistants**§**/project researchers</td>

<td>43</td>

<td>22</td>

</tr>

<tr>

<td>Lecturers</td>

<td>16</td>

<td>10</td>

</tr>

<tr>

<td>Professors</td>

<td>11</td>

<td>6</td>

</tr>

<tr>

<td>Total</td>

<td>100</td>

<td>100</td>

</tr>

<tr>

<td colspan="3">* Source: Kota database  
§ full time doctoral student with administrative and teaching duties</td>

</tr>

</tbody>

</table>

If PhD students are excluded, then the proportion of the professional categories in the sample and population are quite even. There were 61% assistants and project researchers in the sample and 58% in the population. The respective figures for lecturers were 23% vs. 26% and for professors 16% vs. 16%. This implies that occupational groups other than doctoral students are represented reliably in the sample. However, there is an under-representation of PhD students in the sample. It is clearly smaller than the figure shown if we take into account only full-time doctoral students. Although we do not have exact figures, a rough estimation of their share is between 30-50% of all PhD students. If this is the case, the sample as a whole would be fairly representative in respect to academic status.

There were respondents from all twenty Finnish universities except from the Academy of Music. Respondents from the University of Helsinki were slightly over-represented in the sample. The relatively small universities, Turku School of Economics and the Swedish School of Economics, were over-represented. This over-representation of those two universities was also reflected in the over-representation of economics in the disciplines. The Universities of Tampere and of Jyväskylä were under-represented.

The sample is somewhat biased towards natural sciences, engineering and economics and consequently towards universities where these disciplines are well represented. Humanities are under-represented in the data. In other respects the sample is fairly representative.  

#### Variables used in the study

The dependent variables in our study were the frequency of use of FinELib resources, satisfaction with FinElib and the purpose of using FinElib resources. The questionnaire did not contain a specific definition of services provided by FinELib. However, several questions focused solely on the use of literature in various forms, such as electronic journals, reference books, dictionaries and reference databases. Thus, electronic resources were operationalised as literature.

A problem with the validity of measurement is whether the interviewees were able to distinguish between the use of resources provided by the FinELib and other electronic resources. At least two factors speak for this: first, FinELib is the major provider of electronic content in terms of volume at Finnish universities. Although users do not always access this provision by the home page of FinELib, but directly through their home library's home page, the likelihood of using FinELib is great. Secondly, the questionnaire listed the major databases and publishers' journal provision (e.g., ScienceDirect) in the disciplinary categories in this study and asked the respondents to indicate which of these they had used. Thus, they were informed about the major literature provided by FinELib in their discipline. We may conclude that the validity of measuring the use of FinELib resources was fairly high.

The independent variables used were the respondent's discipline, the perceived availability of electronic resources in FinELib and the prestige of FinELib in the users' community. Respondents' status, age and sex were also tested but, due to their weak association with use, these variables were not analysed in greater depth. The precise description of the variables used is given in the [Appendix](#app).

#### Dependent variables

The frequency of use of FinELib was measured with a five-point scale ranging from _less than a few times a month_ to _daily_. Satisfaction with FinELib was measured with a four-point scale, ranging from _very unsatisfied_ to _very satisfied_. The use of FinELib for three major purposes was measured by asking whether the respondents had used it for searching for information for a research project, keeping up-to-date with current issues in one's own discipline and searching for information for teaching.

#### Independent variables

The perceived availability of resources was measured by a question: 'Do you find your own discipline's key resources in FinELib?' A five-point scale ranging from _not at all_ to _very well_ was used.

The disciplines were classified in the questionnaire into six broader categories: engineering, humanities, natural sciences, medicine and nursing science, social sciences and economics (Table 3). This categorization corresponds to the official categorization of disciplines by the Ministry of Education.

It is evident that these disciplinary categories are not homogenous in terms of their research culture and literature orientation; e.g., within humanities, psychology and pedagogics may share more features of social sciences than of humanities. This kind of within-group variance may reduce the between-group variance of disciplinary categorization, thereby reducing its explanatory power.

<table><caption>

**Table 3:. Disciplinary categories**</caption>

<tbody>

<tr>

<th>Name</th>

<th>Disciplines</th>

</tr>

<tr>

<td>Humanities</td>

<td>history, folklore, pedagogics, theology, psychology, linguistics, fine arts, music, theatre and dance</td>

</tr>

<tr>

<td>Natural sciences</td>

<td>mathematics, physics, chemistry, agriculture and forestry, dietetics, food industry and home economics</td>

</tr>

<tr>

<td>Economics</td>

<td>economics</td>

</tr>

<tr>

<td>Engineering</td>

<td>engineering, computer science and architecture</td>

</tr>

<tr>

<td>Medicine</td>

<td>medicine, nursing science and physical education</td>

</tr>

<tr>

<td>Social sciences</td>

<td>social sciences, law and administration</td>

</tr>

</tbody>

</table>

## Results

First we will explore the frequency of use of FinElLib between disciplines and between the levels of availability of relevant content for researchers. Thereafter, we will elaborate how these two explaining factors are related to the use of FinELib.

### Discipline and the use of FinELib

When cross-tabulating the frequency of use according to discipline, academic position, sex and discipline, only discipline was statistically significantly associated with the frequency of use. There is a significant difference in the frequency of use of the materials provided by FinELib between the disciplines (p=0.000) (Table 4). About half of the researchers used it at least several times a week, ranging from about two-thirds in natural sciences to one-third in the humanities and social sciences. Academics in economics were almost as active as natural scientists with their share of 57%. Representatives of medicine and engineering were not such active users as economists. In these two latter groups a surprisingly large proportion did not use FinELib at all. One-fifth of the researchers in engineering and one-third in medicine did not access the contents provided by FinELib. A partial explanation of the relatively frequent use by natural scientists and economists is their over-representation in the sample, and of the infrequent use by representatives of engineering is their under-representation. The self-selection of the most active users in the two former groups may have biased the results to some degree.

<table><caption>

**Table 4: The frequency of use of FinELib by discipline (%)**</caption>

<tbody>

<tr>

<th>Frequency of use <sup>1)</sup></th>

<th>Humanities  
(111)</th>

<th>Natural  
Science (151)</th>

<th>Economics  
(101)</th>

<th>Engineering  
(127)</th>

<th>Medicine  
(69)</th>

<th>Social Science  
(65)</th>

<th>Total (624)</th>

</tr>

<tr>

<td>Daily</td>

<td>34</td>

<td>63</td>

<td>57</td>

<td>40</td>

<td>46</td>

<td>35</td>

<td>48</td>

</tr>

<tr>

<td>Weekly</td>

<td>41</td>

<td>27</td>

<td>28</td>

<td>37</td>

<td>22</td>

<td>54</td>

<td>34</td>

</tr>

<tr>

<td>Not using</td>

<td>24</td>

<td>10</td>

<td>15</td>

<td>23</td>

<td>32</td>

<td>11</td>

<td>18</td>

</tr>

<tr>

<td>Total</td>

<td>100</td>

<td>100</td>

<td>100</td>

<td>100</td>

<td>100</td>

<td>100</td>

<td>100</td>

</tr>

<tr>

<td colspan="8">

<sup>1)</sup> Daily = Daily or several times in a week; &nbsp; Weekly = Once a week or less frequently</td>

</tr>

</tbody>

</table>

### Discipline, perceived availability and use

In order to explore the relation between the perceived availability of relevant content in FinELib and its use, the means of the frequency of use were calculated according to the level of availability. The frequency of use was scored as following: daily = 5, several times a week = 4, once a week = 3, some times a month = 2 and less frequently = 1\. Those not using FinELib were excluded in the following analysis.

The availability variable was dichotomized because of its great variation within disciplines. The values _very well_ and _well_ were collapsed into one category and the values _to some degree_, _badly_ and _not at all_ into another category.

An ANOVA showed that the perceived availability of central material in FinELib by scholars was significantly associated with use of its services (F=22.41; p=0.001) (Table 5). The association between discipline and use was significant (F=2.36; p=0.04), but not as strong as in the previous case. The weak availability of central material led to clearly more infrequent use in all disciplines. Thus, it seems that the perceived availability of material central to interests is a stronger predictor of use of an electronic library than the discipline of the user.

Those with a perceived good availability of material used the library on average several times a week, whereas those who did not felt the availability as good used it approximately some times a month (Table 5). Thus, availability of content central to the needs of researchers in a digital library seems significantly to increase their frequency of use of its services.

<table><caption>

**Table 5: The mean of the frequency of use of FinELib materials by the perceived availability of content and discipline (%)**</caption>

<tbody>

<tr>

<th rowspan="2">Dicipline</th>

<th colspan="4">Perceived availability of central content in FinELib</th>

</tr>

<tr>

<th>At least, good</th>

<th>At most, moderate</th>

<th>Total</th>

<th>p.</th>

</tr>

<tr>

<td>Humanities</td>

<td>3.6 (n=33)</td>

<td>2.7 (n=51)</td>

<td>3.1</td>

<td>0.002</td>

</tr>

<tr>

<td>Natural sciences</td>

<td>3.8 (n=109)</td>

<td>3.5 (n=26)</td>

<td>3.7</td>

<td>0.15</td>

</tr>

<tr>

<td>Economics</td>

<td>3.8 (n=77)</td>

<td>3.0 (n=9)</td>

<td>3.7</td>

<td>0.014</td>

</tr>

<tr>

<td>Engineering</td>

<td>3.5 (n=83)</td>

<td>3.1 (n=15)</td>

<td>3.4</td>

<td>0.24</td>

</tr>

<tr>

<td>Medicine</td>

<td>3.8 (n=40)</td>

<td>3.0 (n=7)</td>

<td>3.7</td>

<td>0.05</td>

</tr>

<tr>

<td>Social sciences</td>

<td>3.3 (n=27)</td>

<td>2.9 (n=31)</td>

<td>3.1</td>

<td>0.14</td>

</tr>

<tr>

<td>Total</td>

<td>3.7 (n= 369)</td>

<td>2.9 (n= 139)</td>

<td>3.5</td>

<td>0.001</td>

</tr>

</tbody>

</table>

In each discipline those who found the availability of relevant content in FinELib good used it more often than others. Good availability seemed to lead researchers to use FinELib almost as frequently between the disciplines so that the average frequency was nearly several times a week. The average frequency of use varied more within the group whose members did not perceive the availability of material to be good. The strength of the association between availability and use varied to some extent between the disciplines. Perceived content availability discriminated significantly the use of FinELib in all other disciplines than in natural and social sciences and engineering.

In all, it seems that variation in the perceived provision of central material in electronic form between disciplines explains in part disciplinary differences in the use of FinELib. However, other characteristics of disciplines—such as the nature of the research work and the tradition of using literature ([Covi, 1999](#cov99); [Garvey, 1979](#garvey))—were associated with the use of FinELib although the influence of perceived availability of the material was controlled.

We also explored how satisfaction with the services of FinELib was associated both with the discipline and availability of the material by calculating an ANOVA of the means of _satisfaction with the services of FinELib_. The results showed that the perceived availability of material was significantly associated with the satisfaction with the services (F=19.2; p=0.000), whereas the discipline of the user was not associated with it (F=0.47; p=0.8). There was no interaction between the independent variables (F=0.49; p=0.78). Thus, it is the availability in a digital library of content central to researchers' needs, not the researchers' discipline that influences how satisfied they are with the library services.

### Use and prestige of FinELib

It is plausible that the atmosphere towards services of digital libraries varies between research communities. Some results ([Covi & Kling, 1996](#cov96); [Talja, 2002](#talja)) suggest that in a community with an attitude that favours electronic resources the use of those resources is more frequent than in communities with a less favouring atmosphere. Thus, the variation in the use of digital libraries could be explicable, in part, by the influence of a sub-culture that favours electronic resources.

Next, we explore how the atmosphere towards FinELib in a research community was associated with the use of FinELib and how the differences in atmosphere and in the availability of material were connected to the use of FinELib.

The atmosphere towards FinELib was measured by asking, 'What is your colleagues' position on the services of FinELib?' Answers could be given in a seven-point scale ranging from, _use much, regard as highly important_ to _are unfamiliar with it_. For the analysis, this variable was dichotomized. The values _use much, regard as highly important_ and _use fairly much, regard as important_ were collapsed into a category _high prestige_ and all other values into a category _at most, moderate prestige_.

An ANOVA showed that both the prestige of the services (F=21.3; p=0.000) and the availability of material (F=18.3; p=0.000) were significantly associated with the frequency of use of FinELib services (Table 6). There was some slight interaction between the variables (F=1.9; p=0.17). Thus, the higher the prestige of FinELib was in a research community, the more frequently it was used, and the better the availability in FinELib of material central to researchers' needs, the more frequent its use.

<table><caption>

**Table 6\. The mean of the frequency of use of FinELib materials by the perceived availability of content and the prestige of FinELib in a researcher's community.**</caption>

<tbody>

<tr>

<th rowspan="2">Prestige of FinELib</th>

<th colspan="4">Perceived availability of central content in FinELib</th>

</tr>

<tr>

<th>At least, good</th>

<th>At most, moderate</th>

<th>Total</th>

<th>p.</th>

</tr>

<tr>

<td>High</td>

<td>3.8 (268)</td>

<td>3.4 (41)</td>

<td>3.7 (309)</td>

<td>0.05</td>

</tr>

<tr>

<td>At most moderate</td>

<td>3.4 (100)</td>

<td>2.8 (101)</td>

<td>3.1 (201)</td>

<td>0.000</td>

</tr>

<tr>

<td>Total</td>

<td>3.7 (368)</td>

<td>3.0 (142)</td>

<td>3.5 (510)</td>

<td>0.000</td>

</tr>

<tr>

<td>p.</td>

<td>0.002</td>

<td>0.001</td>

<td>0.000</td>

<td>0.17</td>

</tr>

</tbody>

</table>

The results show that the services of this digital library were used most frequently when both the atmosphere in the community was in favour of its services and material central to needs was experienced as available by the users. The use of FinELib services was as frequent if the prestige of the services was, at most, moderate, but the availability of material good, or if prestige was high and the availability was, at most, moderate. Use was most infrequent if both the prestige and availability were, at most, moderate.

A closer examination of the results revealed that, if the atmosphere was in favour of the services of FinELib, the difference in the use of its services was smaller between those who perceived the material as highly available compared to those who perceived it as at most moderately available (Difference=0.4; p=0.05), than between the same groups when the community was less in favour of the services (Difference=0.6; p=0.000). A positive atmosphere in a community towards the use of digital library services seems to encourage researchers to use its services, especially if the perceived availability in that library of material central to needs is at most moderate.

It may be that, in a positive atmosphere, the exchange of information about digital library services is more active, increasing the awareness in the community about the services and, consequently, increasing use. This hypothesis can be tested by analysing from where the researchers had heard about the FinELib services. If the proportion of those who have heard about FinELib services from colleagues is larger in the groups with a higher FinELib prestige, then the hypothesis is corroborated.

If the prestige of FinELib was high in a community, its members had heard about it more commonly from colleagues than in a community with a less favourable attitude towards its services, regardless of the perceived availability of the key resources. In communities with a high regard for FinELib, 27% of those with good availability of resources, and 26% of those with lesser availability, had heard about FinELib from colleagues. The corresponding figures in communities with lower regard for FinELib, were 16% and 17%. Thus, interaction in a research community that values FinELib's services increases its members' awareness of those services and, consequently, their use.

An ANOVA showed that the prestige of FinELib within a community, (F=21.7; p=0.000), was a stronger predictor of use than users' disciplines (F=3.0;p=0.01). There was no interaction between the variables (F=0.2; p=0.97) Thus, regardless of the discipline, those working in a research community with a positive atmosphere towards digital libraries use its services more frequently than those in a less positive atmosphere. The former used services on average several times a week whereas the latter about once a week. The difference in use within disciplines was significant in all fields other than in medicine (p=.12) and social sciences (p=0.20).

### Discipline, availability and use of FinELib for various purposes

Next we explore in more detail how the perceived availability of material and the discipline of the users were associated with the use of FinELib for various purposes. We selected the three most frequent ways of using the services: searching for information for research projects (92% of all users), keeping up to date in one's own field (68%) and searching for information for teaching (41%). The variable was dichotomous including the options _use_ or _do not use_. We focused on the proportion of those who used the service for the above purpose by calculating the average of use by scoring _use = 1_ and _do not use = 0_.

<table><caption>

**Table 7: The proportion of users who have used FinELib to search for information for a research project by the perceived availability of content and discipline (%)**</caption>

<tbody>

<tr>

<th rowspan="2">Prestige of FinELib</th>

<th colspan="4">Perceived availability of central content in FinELib</th>

</tr>

<tr>

<th>At least, good</th>

<th>At most, moderate</th>

<th>Total</th>

<th>p.</th>

</tr>

<tr>

<td>Humanities  
</td>

<td>97</td>

<td>81</td>

<td>87</td>

<td>0.03</td>

</tr>

<tr>

<td>Natural sciences</td>

<td>97</td>

<td>87</td>

<td>95</td>

<td>0.02</td>

</tr>

<tr>

<td>Economics</td>

<td>95</td>

<td>100</td>

<td>96</td>

<td>0.05</td>

</tr>

<tr>

<td>Engineering</td>

<td>95</td>

<td>82</td>

<td>93</td>

<td>0.05</td>

</tr>

<tr>

<td>Medicine</td>

<td>91</td>

<td>70</td>

<td>87</td>

<td>0.09</td>

</tr>

<tr>

<td>Social sciences</td>

<td>96</td>

<td>84</td>

<td>90</td>

<td>0.14</td>

</tr>

<tr>

<td>Total</td>

<td>96</td>

<td>83</td>

<td>92</td>

<td>0.000</td>

</tr>

</tbody>

</table>

The perceived availability of material central to needs was significantly associated with the extent that FinELib was used for seeking information for research (F=13.4; p=0.000), whereas the discipline of the user was not (F=1.5; p=0.19) (Table 7). Thus, there were no great differences between disciplines in how FinELib was used for seeking material for research. The perceived availability of material seems to explain the difference in this type of use between the disciplines. Good availability leads to a more common use of the library for seeking research material. There is more variance in seeking for research information between disciplines if the library does not seem to provide enough central content for users.

<table><caption>

**Table 8: The proportion of users who have used FinELib for keeping up to date by the perceived availability of content and discipline (%)**</caption>

<tbody>

<tr>

<th rowspan="2">Prestige of FinELib</th>

<th colspan="4">Perceived availability of central content in FinELib</th>

</tr>

<tr>

<th>At least, good</th>

<th>At most, moderate</th>

<th>Total</th>

<th>p.</th>

</tr>

<tr>

<td>Humanities</td>

<td>74</td>

<td>49</td>

<td>53</td>

<td>0.05</td>

</tr>

<tr>

<td>Natural sciences</td>

<td>86</td>

<td>73</td>

<td>84</td>

<td>0.09</td>

</tr>

<tr>

<td>Economics</td>

<td>67</td>

<td>11</td>

<td>61</td>

<td>0.001</td>

</tr>

<tr>

<td>Engineering</td>

<td>69</td>

<td>47</td>

<td>66</td>

<td>0.08</td>

</tr>

<tr>

<td>Medicine</td>

<td>65</td>

<td>50</td>

<td>62</td>

<td>0.38</td>

</tr>

<tr>

<td>Social sciences</td>

<td>78</td>

<td>50</td>

<td>63</td>

<td>0.03</td>

</tr>

<tr>

<td>Total</td>

<td>74</td>

<td>53</td>

<td>68</td>

<td>0.000</td>

</tr>

</tbody>

</table>

Keeping up to date in one's own field by using FinElib services was significantly associated with both the discipline (F=4.656; p=0.000) and the perceived availability of material (F=25.084; p=0.000) (Table 8). There was no interaction effect (F=1.195; p=0.31). The digital library was used for current awareness most in natural sciences (84%), and least in humanities (49%). There is little variation in groups consisting of the remaining disciplines. Controlling for availability reveals, surprisingly, that those social scientists and humanists with good availability of material central to needs used the library second commonly among the disciplines for keeping up to date. This finding corresponds in part to the observation by Eason _et al._ ([2000](#eason)) that enthusiastic SuperJournal users were social scientists with a good journal coverage preferring regular browsing of core journals for keeping up with new information.

<table><caption>

**Table 9: The proportion of the users who have used FinELib for searching information for teaching by the perceived availability of content and discipline (%)**</caption>

<tbody>

<tr>

<th rowspan="2">Discipline</th>

<th colspan="4">Perceived availability of central content in FinELib</th>

</tr>

<tr>

<th>At least, good</th>

<th>At most, moderate</th>

<th>Total</th>

<th>p.</th>

</tr>

<tr>

<td>Humanities</td>

<td>56</td>

<td>33</td>

<td>42</td>

<td>0.04</td>

</tr>

<tr>

<td>Natural sciences</td>

<td>43</td>

<td>20</td>

<td>38</td>

<td>0.02</td>

</tr>

<tr>

<td>Economics</td>

<td>54</td>

<td>11</td>

<td>50</td>

<td>0.01</td>

</tr>

<tr>

<td>Engineering</td>

<td>34</td>

<td>18</td>

<td>31</td>

<td>0.19</td>

</tr>

<tr>

<td>Medicine</td>

<td>58</td>

<td>41</td>

<td>42</td>

<td>0.31</td>

</tr>

<tr>

<td>Social sciences</td>

<td>44</td>

<td>41</td>

<td>42</td>

<td>0.77</td>

</tr>

<tr>

<td>Total</td>

<td>46</td>

<td>30</td>

<td>41</td>

<td>0.000</td>

</tr>

</tbody>

</table>

The perceived availability of material was associated significantly with searching in FinELib for information for teaching (F14.489; p=0.000), whereas user's discipline was less strongly associated (F=1.879; p=0.10) (Table 9). Here also, the availability of material explains to a certain extent the differences between the disciplines in searching for material for teaching in FinELib. Representatives of humanities and medicine used FinELib relatively more commonly for teaching purposes than others, especially when compared with its use for research or keeping up to date. Although the availability of material was perceived as good, academics in engineering and natural sciences used FinELib relatively infrequently for teaching compared with their colleagues in other disciplines and also compared with their other ways of using FinELib.

In all, the perceived availability of material is a significant predictor of the use of FinELib services for research and teaching purposes, whereas keeping up to date by using FinELib is associated also significantly both with availability and discipline.

There were interesting deviations from the typical patterns of digital library use between disciplines (Tenopir 2003). FinELib was used by humanists more commonly for looking for material for teaching and by social scientists for keeping up to date. On the other hand, the use of FinELib for supporting teaching was uncommon in engineering and natural sciences. These patterns may reflect the more general patterns of attitude and practices in various university activities.

## Discussion and conclusions

This study elaborated on relations among digital library use, users' discipline and the perceived availability of central resources in FinELib by using nationwide, representative data. Previous results on the relations between these factors have been scattered and based on rather small case studies. The results of this study can be generalized at least to concern Finland.

The results show that the perceived availability in FinELib of electronic resources central to researchers' needs was a stronger predictor of the frequency of use of its services than users' discipline. Regardless of discipline, a perception that the provision of such resources was good led to more frequent use of FinELib. This result confirms the findings and suggestions by Abels _et al._ ([1996](#abels)), Eason _et al._ ([2000](#eason)) and Talja & Maula ([2003](#talja03)).

The disciplinary categorization used probably produced within-group variance, which may reduce between-group variance thereby reducing the variable's explanatory power. If a more homogenous grouping of disciplines, discipline may turn out to be a stronger predictor of the use of electronic resources than observed in this study. For future studies a more homogenous categorization of disciplines would be recommended. Also, more emphasis should be given to disciplinary characteristics such as the nature of research, that may affect the use of electronic resources. Moreover, a more detailed analysis of the type of use of electronic material would be needed.

Our results also showed that satisfaction with the services of FinELib did not vary with the discipline, but with the perceived availability of the central material. It is understandable that in each discipline the major factor influencing scholars' satisfaction with the services is that they have access to material central to their field of interest.

We found that the perceived availability of central resources was a stronger predictor than discipline in using FinELib for searching for material either for research or for teaching purpose. The discipline of the users had a stronger role when using FinELib for keeping up to date with new information. These findings of the purpose of use are in line, partly, with DLF/CLIR/Outsell study (Tenopir [2003](#tenopir)), which showed that the use of electronic journals varies with the purpose of use (research or teaching).

Our results also indicated that the prestige of digital library services in the users' community was more strongly associated with the frequency of use than users' discipline. Both the perceived availability of material and the prestige of FinELib services in the community were significantly connected to its use. We noticed that a positive atmosphere in a community towards the use of digital library services seemed to encourage researchers to use its services, especially if the perceived availability of the central material in the library was moderate or weaker.

Our study has shown that the characteristics of the community, be it provision of library services or atmosphere in the users' community, influences the use of digital library services. In this respect our study was contextual-analytic, exploring relations between individual and organizational factors. This has been rare in information seeking studies (Vakkari [1997](#vakkari)). However, institutional and organizational structures and arrangements may have a strong influence on the individuals exposed to these structures. Provision and organization of services in digital libraries are central organizational features affecting their use. Talja & Maula [2003](#talja03)) have suggested studying and evaluating the design and organization of digital libraries by linking the features of services to the work practices of those for whom the services are intended.

Contextual analysis provides a valid means for exploring the connections between the features of structure and provision of digital library services with the characteristics of individuals' work tasks and their information searching and use. This would require observing not only perceived features of the services, but also their objective characteristics, and link these with the tasks and information use by the actors.

## Acknowledgements

Warm thanks to Kristiina Hormia-Poutanen, Arja Tuuliniemi and Ari Alkio of FinELib for their help in providing FinELib data for our study.

## References

*   <a id="abels"></a>Abels, E.G.., Liebscher, P. & Denman, D.W. (1996). Factors that influence the use of electronic networks by science and engineering faculty at small institution. Part 1\. queries. _Journal of the American Society for Information Science_, **47**(2), 146-158.
*   <a id="borgman"></a>Borgman, C.L. (2000). _From Gutenberg to the global information infrastructure: access to information in the networked world_. Cambridge, MA: MIT Press.
*   <a id="cov99"></a>Covi, L.M. (1999). Material mastery: situating digital library use in university research practices. _Information Processing and Management_, **35**(3), 293-316.
*   <a id="cov96"></a>Covi, L.M. & Kling, R. (1996). Organizational dimensions of effective digital library use: closed rational and open natural systems model. _Journal of the American Society for Information Science_, **47**(9), 672-689.
*   <a id="eason"></a>Eason, K. Richardson, S. & Yu, L. (2000). Patterns of use of electronic journals. _Journal of Documentation_, **56**(5), 477-504.
*   <a id="garvey"></a>Garvey, W. (1979). _Communication: the essence of science_. Oxford: Pergamon Press.
*   <a id="kling"></a>Kling, R. & McKim, G. (1999). Scholarly communication and the continuum of electronic publishing. _Journal of the American Society for Information Science_, **50**(10), 890-906.
*   <a id="talja02"></a>Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. _New Review of Information Behaviour Research_, **3**, 143-159.
*   <a id="talja03"></a>Talja, S. & Maula, H. (2003). Reasons for the use and non-use of electronic journals and databases: a domain analytic study in four scholarly disciplines. _Journal of Documentation_, **59**(6), 673-691.
*   <a id="tenopir"></a>Tenopir, C. (2003). _[Use and users of electronic library resources: an overview and analysis of recent research studies.](http://www.clir.org/pubs/reports/pub120/contents.html)_ Washington, DC: Council on Library and Information Resources. Retrieved 12 August, 2004 from http://www.clir.org/pubs/reports/pub120/contents.html
*   <a id="vakkar"></a>Vakkari, P. (1997). Information seeking in context. A challenging metatheory. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), Information Seeking in Context: Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts 14-16 August, 1996, Tampere, Finland, (pp. 451-464). London: Taylor Graham.

* * *

## Appendix

### Dependent variables

####   
Frequency of use

How frequently you use FinELib-resources?

Daily  
Several times a week  
Once a week  
Few times a month  
Less frequently

#### Satisfaction with FinELib

How satisfied you are with FinELib services?

Very satisfied  
Quite satisfied  
Cannot tell  
Quite unsatisfied  
Very unsatisfied

#### Purpose

For which purposes you have used FinELib services?

Searching for information for a research project  
Keeping up-to-date of own discipline?s current issues  
Searching information for teaching

####   
Independent variables

Availability of key resources

How well you can find in FinELib key resources in your field?

Very well  
Well  
To some degree  
Badly  
Not at all

#### Prestige of FinELib

What is the position of your colleagues on FinELib services (prestige in your community)?

Use much, regard as highly important  
Use fairly much, regard as important  
Use to some degree, regard as fairly important  
Use little, regard as not important  
Do not use, unable to use  
Not familiar with  
Cannot tell