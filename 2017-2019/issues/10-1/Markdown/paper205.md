#### Vol. 10 No. 1, October 2004

# Seeking information, seeking connections, seeking meaning: genealogists and family historians

#### [Elizabeth Yakel](mailto:yakel@umich.edu)  
School of Information  
University of Michigan  
Ann Arbor, Michigan 48109-1092 USA

#### **Abstract**

> Genealogy and family history are examples of everyday life information seeking and provide a unique example of intensive and extensive use of libraries and archives over time. In spite of the ongoing nature of this activity, genealogists and family historians have rarely been the subject of study in the information seeking literature and therefore the nature of their information problems have not been explored. This article discusses findings from a qualitative study based on twenty-nine in-depth, semi-structured interviews with genealogists and family historians and observations of their personal information management practices. Results indicated that the search for factual information often led to one for orienting information. Finding ancestors in the past was also a means of finding one's own identity in the present. Family history is also an activity without a clear end goal; after the ancestry chart is filled in the search continues for more information about the lives of one's forebears. Thus, family history should be viewed as an ongoing process of seeking meaning. The ultimate need is not a fact or date, but to create a larger narrative, connect with others in the past and in the present, and to find coherence in one's own life.

## Introduction

Family history is an example of everyday life information seeking. It is an information-intensive personal activity that takes place in a non-work environment. According to the PEW Internet and American Life Project, the use of the Internet for hobbies has grown exponentially ([Madden, 2003](#madden)). These data show that 24% of respondents went online to research family history or genealogy. When related activities such as participation in local genealogical societies, family reunions, and research in libraries and archives are taken into account, the number of people engaged in genealogical activities is even greater ([Maritz Research, 2000](#maritz)). Genealogy is an interesting activity to study for several reasons. First, it is one of the few examples of information seeking in everyday life that requires intensive and extensive use of libraries and archives. Second, although genealogists and family historians do seek assistance from librarians and archivists, as a group they have also developed their own social systems and networks to support their information seeking, analysis, and management needs. Third, they have a sense of both collecting and managing information in the present as well as the need to pass on the information in the future. While information needs may be expressed in factual terms, the underlying need is affective.

The terms genealogist and family historian are used very inconsistently and have been used interchangeably. Some consider genealogists to be individuals who conduct genealogical research professionally, on behalf of others and family historians as those who pursue genealogy for themselves on an amateur basis. When doing the literature review for this paper, the term _family history_ resulted in the retrieval of articles by professional historians on the family as a social institution. This article uses these terms differently. 'Genealogy, the study of ancestry and descent, refers more to the actual search for ancestors, while family history, the narrative of the events in your ancestors' lives, denotes the telling of your family's story. Family history is genealogy come alive' ([About.com, 2004](#about.com)). With this definition, either genealogists or family historians can be professional; the distinction between the two is in the expectations surrounding both the process and the products of the activity.

This paper reports on twenty-nine in-depth interviews with genealogists and family historians and selected observations of their personal information management practices. The interviews focused on both information seeking and information management. In general, participants pursuing family history viewed this endeavour as an ongoing process, rather than as a project that could be finished with the completion of an ancestry chart. As a consequence, they have formed strong networks and a social system to support their information needs. This has many implications for the information seeking process. While initially the information need may have been expressed in terms of the search for practical information, such as a grandmother's birth date or place, the ultimate need was for orienting information. I conclude that family history is a form of seeking meaning related to the process of finding coherence as part of the mastery of life.

## Literature Review

Genealogists and family historians have been the subject of articles in a number of different fields including archives, sociology, and library and information science (LIS). Not surprisingly, the perspectives on genealogy vary considerably. The archival and LIS literatures approach the genealogical problem from a managerial perspective, rather than an exploration of information needs (e.g., [Ashton, 1983](#ashton); [Turnbaugh, 1983](#turnbaugh)). When needs are addressed these are presented in a very narrow manner, identifying sources for genealogists, rather than exploring the full dimension of the need ([Boyns, 1999](#boyns); [Jacobsen, 1981](#jacobsen)). The sociological articles examine genealogy as a cultural phenomenon and focus on motivations for, and the underlying meaning of, this activity to individuals or social groups (e.g., [Bear, 2001](#bear); [Lambert, 2002](#lambert02)).

Given the popularity of genealogy and that genealogists and family historians make up one of the largest constituencies of researchers in archives, it is surprising that so little has been written about this group. A literature search of archival journals in the United States revealed approximately ten articles specifically on this topic. Similar searches in the United Kingdom have located even fewer articles ([Boyns, 1999](#ref2)). What writings there are in the archives and LIS fields also document the historically antagonistic relationship between genealogists and these institutions. In the United States, archivists traditionally viewed genealogists as very superficial researchers ([Peckham, 1956](#peckham)). This attitude has changed, although some of this legacy remains and, with it, mutual suspicion between archivists and genealogists. As recently as 1990, Mills outlined steps that could be taken by individuals in both camps to heal this rift. Even before Mills's article some progress had been made in bridging the divide between genealogists and librarians and archivists. Bidlack ([1983](#bidlack)) discussed a healthier relationship between genealogists and libraries and viewed genealogists as closely associated with libraries, assisting in collection development and cataloguing projects as well as being library advocates. Turnbaugh ([1983](#turnbaugh)) commented on a number of projects in which genealogists heavily supported and contributed to the Illinois State Archives. More recently, Redman ([1993](#redman)) and McKay ([2003](#mckay)) have identified co-operation between archivists and genealogists on a number of mutually beneficial projects.

Duff and Johnson ([2003](#duff)) completed the only research study specifically on information seeking by genealogists. This research is also unique because it views the archives from the genealogist's perspective. They studied information seeking patterns in archives and libraries, search strategies, interaction with access systems, and consultations with archivists and librarians. In the end, Duff and Johnson found that genealogists were as likely to work around existing systems as to use them and that, in the search process, genealogists relied more heavily on their own social networks than on information from professionals.

The interactions between institutions and individuals in the genealogical information seeking process have also been discussed in the sociological literature. At its worst, the institution (library, archives, records office) is a threatening bureaucracy challenging identity, belonging, and even the right to seek information about oneself ([Bear, 2001](#bear)). Bear identified the loss Anglo-Indians felt when their history was drawn too closely into a world of documents with biased classifications based on the cultural and economic needs of their colonizers. In turn, these classifications often conflicted with the family narratives.

> The workers who wrote to the railway officials never forgot the poser of the archive to define their public identities and determine the veracity of their personal stories. These workers sought to end the inconsistent ways in which their lives and relationships were enfolded into the classifications of colonial. ([Bear, 2001: 369](#bear))

This creation of narrative was seen as an important aspect of the genealogical endeavor ([Lambert, 2002](#lambert02)).

Narrative requires that people create connections and meaning from disparate information. Previously, Lambert ([1996](#lambert96)) argued that people conduct genealogical research the better to understand their roots and to get to know their ancestors as people. Connecting through time became a major goal for the genealogist. When this happened, Lambert asserted that there is a commingling of temporal orientations. As a result, the genealogist's information seeking goal changed from the collection of names and dates to a more broad-based search for information. This, in essence, is the transformation from genealogist to family historian. Family historians wanted to place their forebears in historical time as a means of personalizing the past.

The evidence concerning how much genealogists and family historians want to connect with others in the present is mixed. Simko and Peters ([1983](#simko)) viewed genealogists as independent and found that almost half of the genealogists they surveyed did not belong to any genealogical society. Dulong ([1986](#dulong)) presented a very different picture of genealogical organizations and argued for their centrality in the research process. The patterns and strength of genealogical networks definitely requires further study given these disparate findings.

Genealogists and family historians, though, do have connections and a distinct role within their families. Lambert ([2002](#lambert02)) has characterized them as _memory workers_ who are pivotal in the process of constructing their families' collective memories. Genealogists and family historians themselves viewed their position of family historian as both an important part of their identity and a necessary role in family life ([Simko & Peters, 1983](#simko)). As such, they were both seekers and creators of meaning. This paper attempts to explore three of the underlying topics in the literature in greater depth: information seeking, the role of social and familial connections and the meaning of memory work for genealogists and family historians.

## Methodology

The initial research questions were:

*   What are the information seeking patterns of genealogists? and
*   How do genealogists manage their research-related information and integrate this into other types of family information (e.g., photographs, birth and death certificates, and artefacts)?

As the research progressed, additional questions arose concerning what engaging in genealogical work meant to participants and how they utilized family and genealogical networks. This research employed interview methodology with an observational component. Subjects participated in an hour-long, semi-structured interview focusing on the activities they engaged in while doing family history. When possible, interviews were conducted in the participant's home. This was done in 50% of the interviews. As a result, information management practices could be viewed first hand. In the other cases participants generally brought along artefacts to the interview, including genealogical charts, scrapbooks, and other types of research data or family heirlooms.

The interview protocol was inclusive (See the [Appendix](#appendix)): questions were asked about the motivation and any triggers for initiating genealogical work, how the respondents learned to seek information and do genealogical research, and how they interpreted, analyzed, and organized research and family heirloom data and artefacts. The questions also defined information seeking activities broadly: that is, including searching the Internet, working in libraries and archives, visiting historic sites related to one's family history, and networking with other genealogists. Managing family history information was also explored in depth and included very practical aspects of information management such as computer applications, organization of information, and filing practices. However, it also encompassed other issues, such as how documents were divided or passed along in families and the creation of family history Websites or books.

Participants were recruited using several different methods. Verbal invitations were announced at genealogical societies in the southeast Michigan area. Additionally, flyers were posted at a local genealogical resource centre and at a senior citizens centre. The criteria for participation were that the individual had at some time been engaged in genealogical or family history research. One goal was to recruit a variety of individuals ranging in age, expertise in genealogical research, and family background. Another goal was to interview people who had different relationships to the multiple social networks of genealogists.

In the end, twenty-nine individuals were interviewed. The average age of participants was sixty-two, within the range thirty-one to eighty-five. Eleven of the participants were men and eighteen women. On average, interviewees had been engaged in genealogical research for eighteen years. One genealogist had been working on family history projects for thirty-three years, one less than a year. Individuals were all of European descent and white. About half had done genealogical research in both the United States and Europe.

All of the interviews were audio taped. The tapes were transcribed and entered into a qualitative data analysis application, Atlas.ti, for coding and analysis ([Barry, 1998](#barry)). Categories for analysis were developed in several ways. Initial categories followed the major sections of the interview guide. These included codes for when people began to pursue genealogy, how they learned to do genealogical research, interactions with others (family historians, family members, archivists, librarians), and organizational methods. Other categories emerged from the interviews themselves. These codes were often at a higher conceptual level than the initial codes. For example, after the family historians noted when they began they often provided a narrative of their journey to genealogical research. Thus, a code emerged for triggering events. It was through these narratives that I began asking whether someone introduced them to genealogy and whether they has thought about identifying another family member to take on the family history mantle. Thus, _passing information on_ was coded. Codes also emerged concerning how participants viewed family history at different points in this pursuit. It was from these that the conception of family history as a process encompassing different levels of information needs and the idea of seeking meaning emerged.

## Seeking information: genealogy as a project to family history as a process

> I've got a lot of irons in the fire. Genealogy is the messiest one because I can't - I can't quite contain it. It's like - has so many arms reaching out for - and you never get done. Never get done. (Genealogist 32, lines 1022-1025)

Savolainen ([1995](#savolainen)) refers to the difference between seeking practical information and seeking orienting information. The transformation from family historian to genealogist means moving from the former to the latter dimension. Genealogists search out discrete facts and dates in census records, vital records in county clerks' offices, and obituaries on microfilm in public libraries. For family historians, the search for this information is over-shadowed by larger information needs concerning connecting and seeking identity. As one participant noted, '...there are lots of satisfactions, intellectual and emotional, in this'. (Genealogist #19, lines 470-471).

Many of the interviewees in this study had long been intrigued by family stories. One participant recounted going to years of family Memorial Day gatherings and listening to stories from other family members (Genealogist #1). Years later some triggering event, usually a birth or death, would signal the formal initiation of genealogical research. Initially, this search was for facts, but in many cases the search broadened out to a more encompassing inquiry for information about one's ancestors' lives or even of the time period in which they lived. One family historian characterized this as a desire to '...know where they lived and what they did. How'd they meet their spouse, how come they never had children, or how come they had so many children' (Genealogist 18, lines 147-148). Others expanded their historical interests even further to the geographic location, time period of particular ancestors, and historical events. When researching a relative who fought in the United States Civil War, one participant referred to reading a diary from another member of his ancestor's unit. 'And we found an account; somebody was in the same company. They had written a diary and so the diary says a lot of men were ill, so he died of measles' (Genealogist 12, lines 427-429). The expanded information seeking served as a means of learning about the historical narrative more broadly and at the same time filling in the personal story of their ancestor's life.

This shift in information seeking also signaled a change in thinking about genealogy in terms of an activity that can be completed to considering it a continuous process. This has many implications for the information seeking. Most significantly, as some questions are answered, more develop as family historians probe deeper and deeper into the lives of their ancestors. This leads them to seek out more diverse types of sources, as in the example of the diary above, and leads to the need to develop more complex search strategies to figure out what type of institution would hold this information.

Learning how to seek genealogical information is a significant step in this process. A number of the archival authors ([Boyns, 1999](#boyns); [Turnbaugh, 1983](#turnbaugh)) noted that their archives had developed workshops for genealogists. This study found that genealogists primarily learned from each other, a fact previously reported by Bidlack ([1983](#bidlack)). The notable exception to this was that a number of interviewees had enrolled in week-long seminars run by the Church of the Latter Day Saints at their facility in Salt Lake City, Utah. None of the family historians saw archives or libraries as a major source of continuing education or development for their genealogical research skills. Genealogical societies, however, were a major venue for education, both one-on-one and through lectures and workshops. Education, though, was only one form of connection provided by genealogical organizations.

## Seeking connections

> You never know who you're sitting next to. It might be a cousin or whatever. (Genealogist 16, lines 204-205)

In addition to seeking a connection to ancestors, genealogy is characterized by making connections with other genealogists and family historians. This is done through a number of mechanisms that have been established to facilitate interactions and research, such as genealogical societies and Internet sites. All participants made use of the Internet and participated in some form of genealogical organization either locally or abroad.

Genealogical use of the Internet included a variety of activities such as research in online resources or databases, seeking information in chat rooms or listservs, finding contact information for libraries of archives, and planning research trips. A number of participants had also set up a Website documenting their family research. The Web has also fostered individual connections. One noted, 'I had one lady virtually hug me over e-mail because I was like, 'Oh yes,' I know who this is. Here's who his parents are and I've got the pension records for his Grandfather' (Genealogist 1, lines 303-305).

Membership in genealogical groups was a given. These ranged from formal, local genealogical societies to informal genealogical study groups or distant parish record societies. Participation was not necessarily face-to-face; a number of genealogists _participated_ from afar through e-mail, phone, or letters. Membership in organizations near a interviewee's residence provided support for the research endeavour through education methods, commiseration, celebration, and camaraderie. Membership in far-flung genealogical societies was essential to identify local researchers (particularly in foreign countries) who would provide research services for a fee. This participation also assured genealogists and family historians that they would receive regular news about discoveries, sources, and perhaps even in locating distant relatives.

Whether this participation was face-to-face or geographically dispersed, identification with the group was strong. The strength of these connections is best demonstrated through two core ethical precepts of genealogists and family historians: information sharing and giving back. Information sharing both within families and with strangers was common among the genealogists and family historians studied. Many saw the provision of information without any guarantee of return as a way of promoting genealogical endeavours. This sharing also extended to formal meetings where information was widely shared and others' discoveries were celebrated. At the gatherings I attended, one of the regular items on the agenda was for people to stand up and announce their _breakthroughs_ or important developments in their research. These were then celebrated by the group.

Almost all of the participants engaged in some _giving back_ activity, such as lecturing on genealogical topics, transcribing records and mounting this information on the Internet, creating inventories of cemetery grave markers, or volunteering at local genealogical resource centres. Extended or geographically dispersed membership did not deter interviewees from _giving back_. One interviewee, who relied on the work of others at a distant genealogical society, noted his desire to give back and discussed his transcription of information from a microfilm into a database that could be posted on the Web (Genealogist 19).

## Seeking meaning: managing records and managing the legacy

> I've decided I'm not going to get a book written so I decided what I'm going to do is I'm just going to start writing up different things that I have discovered, giving the genealogy, the people, the children and that kind of thing, and then some of the stories that I found out. I'm just going to type them up, you know, and put them in plastic in a notebook and then if anybody wants it? (Genealogist 9, lines 683-688)

Information management is a core activity of genealogical work. Family historians and genealogists manage information from a variety of sources. Specialized genealogical software applications, notebooks and loose-leaf binders, and narratives were some of the organizational devices and strategies used to both manage research data and create meaning. The use of different organizational methods signaled different conceptions of genealogical and family history work. Three of these conceptualizations are the story, the archives, and the pedigree chart.

Some genealogists and family historians see themselves as narrators of the family history. This comes out in several ways. A few transcribed family stories. Others saw the narrative form as a means of synthesizing or integrating the very personal and detailed birth, marriage, and death data within a broader framework of the locality, major events, and time period in which their ancestors lived (Genealogist 18). Narratives also ran in families. One participant reported:

> 'My grandmother had written a biography of her life, and when she was 80, she wrote this. And, so then, we gave my dad a blank book and told him to write his....[She went on to say] My son gave me a computer, the one who is interested now, he gave me a book - a book and he said here, you write your story, so, I'm in the process'. (Genealogist 20, lines 32-36 and lines 327-328)

Narration, though, brought with it some literary license. The narrators in this study fell along a continuum. At one end were individuals trying to place facts in narrative form. At the other end, facts were not as important as the overall story:

> 'I am also an artist. I put my albums together anyway I wanted - no chronology, just sort of these collage techniques, and my husband at the time would say, "Oh, but you have to write names and you have to write dates." And I said, "No, this is my album"' (Genealogist 21, lines 289-292).

Others conceptualized themselves more as family archivists. One explicitly proclaimed, 'I'm kind of the archivist' (Genealogist 10, line 127). She discussed filling notebook after notebook, organized by branches of her family, with her research notes, photocopies of birth, death, census records, and original family heirlooms such as birth, death, and wedding announcements. Another noted that this role was widely recognized among family members,

> 'I'm kind of the genealogist, archivist, for the family and so they - if they find anything - and I tell them - if there's ever a time that you don't have space to store it, I will be happy to store it for you - so I have all their family documentation' (Genealogist 3, lines 150-161).

Genealogists and family historians who consider themselves archivists also think about posterity. One discussed her own personal manuscript materials,

> 'Well, I've kept - all my life I've kept every letter that I've received and letters that I write, you know, I keep them on my hard disk. I don't think they would be very interesting to anyone but, I mean I keep those.' (Genealogist 24, lines 704-706).

This quotation demonstrates a view of self as historical being and a desire to connect with the future. It also shows a very broad conception of possible information sources.

The navigator creates a map of familial connections. This is characterized by the pedigree chart, perhaps the most traditional genealogical product. These come in a number of forms and styles. The pedigree chart represents an organizational scheme that provides genealogists with a visualization of what work has been done and what work remains to be completed. Yet, the chart is also the embodiment of connections over time. One interviewee stated in awe,

> 'I like the charts because I can bring it out and I can show you and—what's so odd is that all of these people make me who I am and I often wonder, Do I look like any one of those male relatives on either both sides of the family? I'd love to know. But it's mind boggling—when you see that on a piece of paper, there are all these names running across. It's something else.... When you look at these sheets, it just stymies [me] that all of these people are my relatives that make me what I am today or whatever. I mean I - and all of us have this.' (Genealogist #23, lines 445-453 and lines 773-775)

Narrator, archivist, and navigator are types of family historian, a role that participants assumed very consciously. One interviewee characterized her role and the scope of her work succinctly:

> 'My husband tends to be what I call a genealogist. He finds the name, and he's got the birth date, and the death date, and the marriage date and that's when he's finished with that person. I tend to be a family historian.' (Genealogist 18, lines 144-147).

She continued by listing an array of questions about her ancestors' lives that could not be answered through vital records, such as how spouses met. These queries were typical of the interviewees and illustrate the search for and management of meaning.

A part of seeking meaning was to make sure that meaning, once found, was not lost. Participants took pride in their role as family historian and saw this as an important position to continue. Just as interviewees recounted being identified as the next family historian by some other family member, a number noted their identification of someone else to carry the family legacy and to whom they would give the materials they collected at some point. This intergenerational legacy signified not just the transfer of family photographs and records, but an initiation and an invitation into the genealogical process.

> My mother picked it up and when she died in 1983, I _inherited_ all of her information. And then for the past ten or fifteen years I've been really, really researching with my oldest daughter. And we go to various libraries; we've been to Salt Lake City to the Mormon Library ten or eleven times. (Genealogist 27, lines 15-19)

## Conclusions

This article extends the research on information seeking and genealogists in several ways. First, the exploration of differences in information seeking by genealogists and family historians differs from much of the previous literature on genealogists in archives and LIS by revealing levels of information needs. While libraries and archives have focused on satisfying the information needs of genealogists, few have considered the broader information needs of family historians. Consequently, genealogists and family historians have turned to genealogical groups to support these larger needs through education, celebration, and support.

Second, this study is the first to examine how genealogists make sense of and manage the information they collect. In terms of the personal information management research, this is also one of few studies to examine non-work settings. Identifying, selecting, documenting, and organizing information are integral components of genealogical work. Creating these frameworks was closely tied to how genealogists and family historians conceptualized their role. Narrators, archivists, and navigators were the organizational schemas that emerged from this research.

Finally, genealogy and family history are as much about seeking information as they are about seeking meaning. Self-identification and self-discovery through the role of family historian were an important dimension of the genealogical research process. Family history is more than seeking practical historical information on births, deaths, and marriages. While this information is essential in completing the pedigree chart and creating data points along the lines, the family historian needs to fill in the story between the lines. And, it is this process of connecting—connecting the past with the present and connecting within the present to family and the genealogical community—that has an orienting function for genealogists. It is also this aspect of the process that begins to address their underlying information needs. At its most abstract level, family history is a form of seeking meaning about one's ancestor's lives, one's own life, and the potential connections among people—all essential elements in finding meaning and coherence. As such, genealogy is a means of working towards a _mastery of life_.

## Acknowledgements

The author wishes to acknowledge the contributions of Deborah A. Torres who assisted with the interviews and checked transcriptions and the comments of the anonymous referees which were invaluable.

## References

*   <a id="about.com"></a>About.com (2004). _[Choosing genealogy or family history?](http://genealogy.about.com/library/tips/blfamilyhistory.htm)_ Retrieved 15 June 2004 from http://genealogy.about.com/library/tips/blfamilyhistory.htm
*   <a id="ashton"></a>Ashton, R.J. (1983). A commitment to excellence in genealogy: how the public library became the only tourist attraction in Fort Wayne, Indiana. _Library Trends,_ **32**(1), 89-96.
*   <a id="barry"></a>Barry, C.A. (1998). [Choosing qualitative data analysis software: Atlas/ti and Nudist Compared.](http://www.socresonline.org.uk/3/3/4.html) _Sociological Research Online,_ **7/8**(3). Retrieved 12 February, 2004 from http://www.socresonline.org.uk/3/3/4.html
*   <a id="bear"></a>Bear, L. (2001). Public genealogies: documents, bodies and nations in Anglo-Indian Railway family histories. _Contributions of Indian Sociology (New Series),_ **35**(3), 355-388.
*   <a id="bidlack"></a>Bidlack, R.E. (1983). Genealogy today. _Library Trends,_ **32**(1), 7-23.
*   <a id="boyns"></a>Boyns, R. (1999). Archivists and family historians: local authority record repositories and the Family History User Group. _Journal of the Society of Archivists,_ **20**(1), 61-73.
*   <a id="duff"></a>Duff, W.M. & Johnson, C.A. (2003). Where is the list with all the names? Information-seeking behavior of genealogists. _American Archivist,_ **66**(1), 79-95.
*   <a id="dulong"></a>Dulong, J.P. (1986). _Genealogical groups in a changing environment: from lineage to heritage._ Unpublished doctoral dissertation, Wayne State University, Michigan, USA
*   <a id="jacobsen"></a>Jacobsen: H. (1981). "The world turned upside down": reference priorities and the state archives. _American Archivist,_ **44**(4), 341-345.
*   <a id="lambert96"></a>Lambert, R. D. (1996). The family historian and temporal orientations towards the ancestral past. _Time and Society,_ **5**(2), 115-143.
*   <a id="lambert02"></a>Lambert, R.D. (2002). Reclaiming the past: narrative, rhetoric and the 'convict stain.' _Journal of Sociology,_ **38**(2), 111-127.
*   <a id="mckay"></a>McKay, A.C. (2002). Genealogists and records: preservation, advocacy, and politics. _Archival Issues,_ **27**(1), 23-33.
*   <a id="madden"></a>Madden, M. (2003). _[_America's online pursuits: the changing picture of who's online and what they do. Pew Internet & American Life Project._](http://www.socresonline.org.uk/3/3/4.html)_ Retrieved 12 February, 2004 from http://www.pewinternet.org/reports/toc.asp?Report=106
*   <a id="maritz"></a>Maritz Research (2000). _[_Choosing sixty percent of Americans intrigued by their family roots._](http://www.maritzresearch.com/release.asp?rc=195&p=2&T=P)_ Retrieved 12 February, 2004 from http://www.maritzresearch.com/release.asp?rc=195&p=2&T=P
*   <a id="mills"></a>Mills, E. S. (1990, May). Genealogists and archivists: communicating, cooperating, and coping! _SAA NewsLetter_, 20-24.
*   <a id="peckham"></a>Peckham, H.H. (1956). Aiding the scholar in using manuscript collections. _American Archivist_, **19**(3), 221-228.
*   <a id="redman"></a>Redman, G.R. (1983). Archivists and genealogists: the trend toward peaceful coexistence. _Archival Issues_, **18**(2), 121-132.
*   <a id="savolainen"></a>Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of way of life. _Library & Information Science Research_, **17**(3), 259-294.
*   <a id="simko"></a>Simko, P. & Peters, S.N. (1983) A survey of genealogists at the Newberry Library. _Library Trends,_ **32**(1), 97-109.
*   <a id="turnbaugh"></a>Turnbaugh, R. (1983). The impact of genealogical users on state archives programs. _Library Trends_, **32**(1), 39-49.

## <a id="appendix"></a>Appendix

### Interview guide: information seeking behaviour of family historians

1.  Beginning
    1.  How long have you been doing family history?
    2.  What got you started?
2.  Learning how to do genealogy
    1.  How did you learn how to do genealogy?
    2.  What genealogical how to books do you own or do you rely on heavily?
3.  Information seeking and selection
    1.  How do you decide where to begin?
    2.  What types of information are you looking for?
    3.  Do you search for genealogical information on the Web?
    4.  What web-based genealogical sites do you use?
    5.  What other web-based sources of information do you use?
    6.  How do you select information or decide which information might be relevant for your family research?
4.  Citation practices
    1.  When you collect information do you note the source? How?
5.  Role of social networks in information seeking
    1.  How important are other family historians in assisting you in your research?
    2.  How important are other family members in helping your family research project?
    3.  Do you belong to a genealogical society?
6.  Family documentation
    1.  What types of family history documents or information do you keep? (Probe for form and genre of records - e.g., death records)
    2.  How long have you been collecting family information?
    3.  Where did you get the family documentation?
    4.  Are you aware of other family members that have other documents or photographs?
    5.  Do you share documents or photographs with other family members?
    6.  What are your plans for passing on family materials?
7.  Record-keeping practices
    1.  Do you organize your family history materials?
    2.  Do you have any type of inventory of your materials?
    3.  Would you describe/show the organization to me?
    4.  How aware are you of best practices for genealogical recordkeeping?
8.  Technology
    1.  Do you use a software package to manage your genealogical information?
    2.  What version of this software do you have?
    3.  How did you select this application?
    4.  How would you rate your computer skills?