#### Vol. 10 No. 1, October 2004

# From information behaviour research to the design of information systems: the Cognitive Work Analysis framework

<table>

<tbody>

<tr>

<td>

#### [Raya Fidel](mailto:fidelr@u.washington.edu)  
#### The Information School, University of Washington  
#### Seattle, Washington, USA

</td>

<td> </td>

<td>

#### [Annelise Mark Pejtersen](mailto:AMP@risoe.dk)
#### Risø National Laboratory, Roskilde  
#### Denmark

</td>

</tr>

</tbody>

</table>

#### **Abstract**

> Cognitive Work Analysis is a conceptual framework that makes it possible to analyse the forces that shape human-information interaction. This analysis can then be directly transformed to design requirements for information systems. Its approach is work-centred, rather than user-centred, as it analyses the constraints and goals that shape information behaviour in the work place, regardless of the specific individuals who are involved. Being a holistic approach, it examines simultaneously several dimensions: the environmental, organizational, social, activity, and individual. As a result, applying the framework requires a multi-disciplinary approach. It provides concepts and templates to facilitate an analysis of complex phenomena, without reducing their complexity. As a framework, it is a structure that accommodates any relevant theory, model, or method. Cognitive Work Analysis has proved to be an effective approach to the study of human information behaviour for the purpose of designing information systems.

## Introduction

Human information behaviour is a highly active area of research within Information Science and other fields. Indeed, the significant body of research that has been carried out to date has contributed greatly to our understanding of human-information interaction. Yet, very few studies have generated results that are directly relevant to the design of information systems.

Clearly, information systems would be most effective if their design is informed by an understanding of the human-information interaction of their intended users. Yet, information systems have been designed—and widely used—almost completely unaffected by results of studies in human information behaviour <sup>[1](#footnote1)</sup>. It is important, therefore, to examine how human information behaviour research could inform design. A variety of reasons have probably motivated systems designers to ignore this research, such as pressure to design systems quickly, no obvious relevance of research results to design, and lack of appreciation of _soft_ research. Instead of analysing these reasons, it might be useful to examine how results of human information behaviour research projects can increase their applicability to systems design. This will address a standing concern: bridging the gap between designers and researchers, and increasing the relevance of academic research to the practitioners' work [(Dervin, 2003)](#dervin).

A designer who consults studies in human information behaviour in order to be guided in the design of an information system faces two major challenges. First, the phenomenon under study, human-information interaction, is highly complex. Human information behaviour studies that uncover this complexity usually present it in textual narratives that do not fit the engineering rigour necessary for design. Secondly many studies that have attempted to include implications for systems design describe the phenomenon under investigation, rather than analyse it. To design a system, however, requires an ability to predict behaviour under various changing circumstances. Therefore, describing the current manifestation of a phenomenon is not enough for design.

An analogy can illustrate the second issue. Suppose, for example, that Mary wants to help John to get from his hotel to the beach. She may give him specific directions based either on the route most people take (the descriptive approach), or on the route she thinks is best according to some criteria (the normative approach). These sets of directions, however, may or may not fit John's specific situation. Moreover, even if they are helpful at a particular time, they may not be useful to him later, or to other people. A third possibility is to give him a map. This way John can see the possibilities and choose the one that fits his situation and preferences at the moment he decides to go to the beach (the formative approach). For John's situation, Mary might choose any one of the three options. If it was her responsibility to help the entire hotel clientele year round, however, her best approach would be to create a map that makes it easy for guests to figure out how to get to the beach in the most satisfying route to them because a map, by its very nature, illustrates all the possible routes to the beach. Systems designers require maps as well, maps that point to the possibilities of information behaviour in a particular context. It may not be important for designers to know when a certain person would employ a specific strategy, or what exact circumstances would motivate the person to this strategy selection. Once they can see the possible strategies for people in a particular context, they can design a system that will support such strategies. Clearly, descriptions of a certain behaviour at a certain time cannot serve as the sole basis for design. It requires maps of information behaviour.

This paper discusses briefly these issues and shows how the Cognitive Work Analysis framework addresses them and how it can be used to study human-information interaction for the purpose of systems design. That is, how it creates maps of human-information interaction.

## Cognitive Work Analysis

Cognitive Work Analysis ([Vicente, 1999](#vicente)) is a work-centred conceptual framework developed by Rasmussen, __et al.__, ([1994](#rasmussen1994)). Its purpose is to analyse cognitive work. The framework's theoretical roots are in General Systems Thinking, Adaptive Control Systems, and Gibson's Ecological Psychology, and it is the result of the generalization of experiences from field studies which led to the design of support systems for a variety of modern work domains, such as process plants and libraries. Developed since the 1960s ([Rasmussen, 1986](#rasmussen1986)), it belongs to a set of approaches that together constitute Cognitive Systems Engineering. As Woods ([2003](#woods)) explains, some of the basic foundations of this school of thought are, 'adaptations directed at coping with complexity... and how to make automated and intelligent systems team players'.

Using this framework in Information Science, one analyses cognitive work to inform the design of information systems. In this context, the concept 'information system' refers to any system, whether intellectual or computerized, that facilitates and supports human-information interaction. Thus, a library as a whole could be considered an information system, and so could a reference desk, the Web, an online public access catalogue, or a cataloguing department.

Unlike the common approach to the design of information systems (design and development first and evaluation later), Cognitive Work Analysis first evaluates the system already in place, and then develops recommendations for design. The evaluation is based on the analysis of information behaviour in context. Cognitive Work Analysis has been successfully applied to the evaluation and design of information systems and collaboratories.<sup>[2](#note2)</sup> For example, it guided the development of the first retrieval system for fiction called BookHouse ([Pejtersen 1989;](#pejtersen1989) [Rasmussen _et al._, 1994;](#rasmussen1994) [Pejtersen, 1992](#pejtersen1992)). Based on the analysis of reference interviews in public and school libraries, Pejtersen developed a fiction retrieval system, with a graphical user interface, in which users can look for books by a variety of attributes, such as the subject, historical period, mood, and the cover design. It serves children and adults, as well as library cataloguers. The system also caters to various strategies: users can just browse without any particular attribute in mind, look for a specific book, or look for books that are similar to one they liked. More recently, Cognitive Work Analysis was used to analyse data collected in a study of Web searching by high school students [(Pejtersen & Fidel, 1998;](#pejtersen1998) [Fidel _et al._, 1999](#fidel1999)). In this study, the framework proved to be very powerful in helping to uncover the problems that students experienced when using the Web to search for information, and offered recommendations for designs that can alleviate such problems. Pejtersen and her colleagues have recently completed the COLLATE project that will support multi-institutional collaboration in indexing and retrieval among the national film archives of Germany, Austria, and the Czech Republic ([Albrechtsen _et al._, 2002;](#albrechtsen) [Hertzum _et al._, 2002](#hertzum).

### The dimensions of Cognitive Work Analysis

Cognitive Work Analysis considers people who interact with information as _actors_ involved in their work-related actions, rather than as _users_ of systems. Focusing on information behaviour on the job, Cognitive Work Analysis views human-information interaction in the context of human work activities. It assumes that in order to be able to design systems that work harmoniously with humans, one has to understand:

*   the work actors do,
*   their information behaviour,
*   the context in which they work, and
*   the reasons for their actions

Therefore, Cognitive Work Analysis focuses _simultaneously_ on the task actors perform, the environment in which it is carried out, and the perceptual, cognitive, and ergonomic attributes of the people who do the task. A graphic presentation of the framework is given in Figure 1\. In this presentation, each set of attributes mentioned above is designated with a circle and is considered a dimension for analysis. Thus, each dimension is a host of attributes, factors, or variables, depending on the purpose and method of a study. Because Cognitive Work Analysis investigates information behaviour in context, individual studies create results that are valid for the design of information systems in the context investigated, rather than for the design of general information systems. Results from a variety of studies, however, can be combined together and generalized to inform the design of other information systems.

<figure>

![Figure 1: The dimensions of Cognitive Work Analysis](../p210fig1.gif)

<figcaption>

**Figure 1: The dimensions of Cognitive Work Analysis**</figcaption>

</figure>

To further explain the Cognitive Work Analysis dimensions, consider a project to study the information behaviour of teachers in a public elementary school, with the aim of developing design recommendations for an information system to support the teachers' work. For this project the Cognitive Work Analysis dimensions for analysis would be:

*   **The work environment.** Investigates the environments in which the school operates. Examples of questions: What are the federal, state, and school district regulations under which the school operates? What is the state policy and standards for the school's curriculum? What is the population from which the school can recruit students?
*   **Work-domain analysis.** Studies the work that is done at the school and the school library. Examples of questions: What are the goals of each organization? What are the constraints within which it has to operate? What are the activities in which each organization is involved? What tools and technologies it uses to perform these activities?
*   **Task analysis.** Looks at specific tasks and analyses them with the same questions. Examples of questions: What are a teacher's goals for lessons? What are the constraints a teacher faces in preparing and delivering a lesson? What information sources does a teacher consult?
*   **Organizational analysis.** Examines the management style, the organizational culture, the social conventions, and how roles are allocated. Examples of questions: How does the teacher communicate with the principal? Why was the teacher allocated to teach a course? Who decides whether or not the librarian should give a presentation in a class session? What procedure does this process follow?
*   **Decision analysis.** Provides a more specific analysis of individual decisions. Examples of questions: for a librarian's decision whether certain images would be relevant for a lesson, for instance the issues involved might be: what information does a school librarian need to make this decision? What information sources are available to her? What sources are desirable but not available?
*   **Strategies analysis.** For each task and decision, examines which strategies are possible. Examples of questions: How can a teacher who is looking for an image to use in her lesson find it? For instance, can she ask a colleague to think about an image? Can she browse in a book in the library? Can she go to a site she knows on the Web? Can she search art databases?
*   **User's resources and values analysis.** Identifies characteristics of each group of users. Examples: What is the experience a teacher has in looking for visual information? What is the knowledge a teacher has of the arts requirements standards? What are the most important values a teacher holds? What is the knowledge of a school librarian about art? What is the degree of importance a school librarian attributes to including art in the curriculum?

Although the dimensions are laid out in a certain order, employing them in actual projects follows no fixed sequence. Because of the interdependence among the dimensions, a researcher moves from one dimension to another in an iterative process. The path of this movement is determined by the particular problem at hand and also by pragmatic considerations.

From the perspective of information seeking, one may interpret Figure 1 in a distinct way. Suppose one wishes to analyse information seeking behaviour of a group of people (rather than design an information system). Information seeking behaviour manifests itself by the strategies that people employ (see Figure 1), that is, the methods they use to find information. Clearly, a host of factors external to the behaviour itself influence the selection of strategies. In the systems approach terminology, such factors are called _constraints_, factors that affect information behaviour, but cannot be changed by it ([Churchman, 1979](#churchman)).

The dimensions presented by Cognitive Work Analysis represent the constraints on information seeking, starting with the external environment of the work place to the individual resources and values of the actor. Each dimension creates the constraint for the one nested in it. Thus, the work environment affects how a work place is operating, and this mode of operation shapes the task that an actor performs. The task, in turn, affects the decisions that an actor makes, and these decisions influence seeking behaviour. In addition, the actor's characteristics have an effect on seeking behaviour and so does the social organization of the work place. Cognitive Work Analysis assumes that while one can _describe_ information behaviour without taking these constraints into account, the best way to _analyse_ information behaviour is through an in-depth analysis of these constraints. Work analysis is, therefore, an analysis of the constraints that shape information seeking behaviour.

### Some characteristics of Cognitive Work Analysis

In a way, the dimensions of Cognitive Work Analysis define the context of information seeking, and provide a framework for its analysis. Johnson observed that context has been '_conceived of in terms of constraints and limits on individual action... rather than enablers_' ([Johnson 2003](#johnson): 738) in studies of information seeking. While in every day language the terms _constraints_ and _limits_ are almost synonymous, in the systems approach's vocabulary they are distinct. Constraints are the given parameters within which actors operate, and, as such, they are actually enablers of action because without them action cannot take place. Without the constraint of gravity, for instance, we could not walk or dance and our bodies could not perform their functions. Imagine a work place with no fixed mode of operation, where individuals can carry out any task that comes to their minds at any moment, and decisions that people make are free from the requirements of the tasks they perform. Obviously, such a place would not be functional, and would not _enable_ information seeking.

The focus of Cognitive Work Analysis, therefore, is on analysing the constraints in a particular context, that is, the factors that affect work and information behaviour. The method is to carry out an investigation to identify and study the constraints for a given context, assuming that each context presents its own constraints.

Cognitive Work Analysis has several distinct attributes that are useful for the study of human-information interaction and for the design of information systems. Most importantly, it provides a holistic approach that makes it possible to account for several dimensions simultaneously. In addition, the framework facilitates an in-depth examination of the various dimensions of a context. A study of a particular context, therefore, is a multi-disciplinary examination with the purpose of understanding the interaction between people and information in the work context. These two attributes make the framework a powerful guide for the evaluation and design of information systems for the context under investigation because, in reality, all dimensions—personal, social, and organizational—play a role simultaneously and interdependently.

Lastly, while the framework is based on a set of conceptual and epistemological constructs, it provides a _structure_ for the analysis of human-information interaction, rather than subscribing to specific theories or models. Sanderson explained that, '_The scientific foundations of Cognitive Work Analysis are various—a "conceptual marketplace" as Rasmussen described it"because they have been appropriated to fulfil a practical need_' ([Sanderson 2003: 226](#sanderson)). One can employ a wide variety of theories, methods, or tools that may be deemed helpful for the analysis of a specific situation. This flexibility turns the focus of an investigation to the phenomenon under study, rather than to the testing and verification of models and theories, or to the employment of a particular methodology. At the same time, Cognitive Work Analysis has built-in mechanisms to carry out rigorous and systematic research. It provides several templates to support both analysis and modelling, in addition to the dimensions for analysis. One of these templates, the Means-End Analysis, is discussed later in the paper.

## Dealing with complexity

Human-information interaction is a complex phenomenon because of the variability inherent to human cognitive processes and because of the highly complex environment in which humans operate in the modern world. The Cognitive Work Analysis dimensions (Figure 1) are a first step to dealing with this complexity. They parcel out the investigated phenomenon. They indicate that some attributes are organizational, some determined by the work and subject domains, and others are cultural or individual. Each dimension, however, is also complex. How can these complexities be made compliant with the requirements of the design process?

### Individual complexity and variability

In principle, almost every possible characteristic of an actor, and every element in an actor's life experience, may affect information behaviour. The possibilities are so enormous that human information behaviour is still in the process of uncovering the elements that should be considered for research ([Fidel _et al._, in press](#fidel04)). The number of elements is so great that it is impossible to consider them all, and all their possible combinations, in a single study, or apply them all to the design of an information system.

Cognitive Work Analysis assumes that for the design of information systems, it is impossible to consider all the attributes an individual might have, the variability among individuals, and the transformations they might go through with changing personal situations. Therefore, Cognitive Work Analysis takes a _work-centred_ approach, rather than a _user-centred_ one. The focus of the analysis is not the individual actor but on the work domain and the requirements that it presents to the actors who operate within it.

This approach is facilitated by the Cognitive Work Analysis dimensions. Work analysis along these dimensions lays out the constraints under which actors carry out their tasks, regardless of their individual attributes. In other words, it analyses the context in which actors in a certain organization, performing a particular task, operate. Moreover, Cognitive Work Analysis recognizes that there are certain attributes that are typical to those who operate in a certain context. When Cognitive Work Analysis analyses _actor's resources and values_ (inner circle in Figure 1), it creates a model of the prototypical actor, that is, that of the best example of an actor in the given context. Cognitive Work Analysis recognizes that not all actors are prototypical, and that their individual attributes and histories might affect their interaction with information. However, because the goal of Cognitive Work Analysis is to design information systems for distinct work domains and tasks, regardless of the individuals who are carrying out a task at a certain point in time, it considers the prototypical attributes as most important.

The decision as to which prototypical attributes to analyse may change from one work domain to another. In a study about Web searching behaviour of high school students ([Fidel _et al._, 1999](#fidel1999)), for example, we analysed the students' education, their experience with computers and with retrieving information from the Web, their experience in the subject domain, their educational plans for the future, their preferences with regard to searching the Web, the priority criteria they used to select a search strategy, the performance criteria they employed, and their opinion about their own situation, abilities, and preferences. While varying from one domain to another, several attributes are common to most domains, such as level of expertise and experience with the subject domain, experience and expertise with information systems, and technology, preferences, values, structure of subject domain, and type of training required to carry out the task. Future research on the application of Cognitive Work Analysis to the design of information systems is likely to develop a core set of attributes that would be relevant to most studies.

### Complexity in the environment

Analysing the environment in which human-information interaction takes place might be a daunting task because it is difficult to decide what to include in an analysis and what to leave out. Cognitive Work Analysis assumes that within this richness and complexity there are, 'basic sources of regularity that underlie the responses of the work domain to human actions' ([Vicente, 1999: 47](#vicente)). Work analysis, therefore, focuses on these "basic sources of regularities", which are the constraints that shape the behaviour of the actor. These stable and behaviour-shaping constraints are called _invariants_. Concentrating on the analysis of invariants does not ignore, or reduce, the complexity in the environment as invariants can be highly complex. It merely focuses our attention to the elements in the environment that have significant effect on information behaviour.

The concept of _invariant_ has also been transferred to the design of information systems. Such systems are designed to adapt to the actors' work in the realm of the invariants. At the same time, Cognitive Work Analysis recognizes that situational and individual factors affect information behaviour. But designers cannot predict all the possible ways in which a system can support the work of all actors. As a result, a system that adapts to all possible situational and individual factors is unattainable. Consequently, actors will have to adapt themselves to the system when a situation deviates from that predicted by the invariants. That is, actors will have to behave adaptively as well. It is the task of the designer, therefore, to build systems that support actors when they adapt to the system's behaviour. That is, the goal is to design an information system that is adaptive to the actors' work in the realm of stable, behaviour-shaping constraints, and, at the same time, makes it possible for the actor to adapt when situational and unpredictable factors arise.

A tool to analyse the complexity inherent in the context is the Means-Ends Analysis template which is an abstraction hierarchy. This analysis begins with the most abstract level of analysis: the goal and constraints of a system (Figure 2). To answer how the system is achieving its goals; that is, the means it uses for that purpose, one examines the priorities under which the system operates. To understand how a system can follow its priorities, one looks at the general function the system performs. How are these functions carried out? By performing certain processes. Finally, the processes are completed with the use of certain system resources. When looking at the most concrete level first, the physical resources, one progresses upwards in the analysis, answering _why_ questions; that is, identifying the ends in the means-ends analysis: Why is the system using these resources? To perform these processes. Why is it carrying out these processes? To fulfill these functions. Why these functions? To meet the priorities. Why these priorities? To achieve the goals of the system, given its constraints.

<figure>

![Figure 2: The template that facilitates means-ends analysis. Each row represents a level in the abstraction hierarchy](../p210fig2.gif)

<figcaption>

**Figure 2: The template that facilitates means-ends analysis. Each row represents a level in the abstraction hierarchy**</figcaption>

</figure>

Consider, for example, the school in which the study of high school students searching the Web took place. The school itself had several goals. We selected one of them to illustrate the analysis: to train students to acquire skills in information technology. A partial analysis is presented in Figure 3.

<figure>

![Figure 3: A partial means-ends analysis of a high school](../p210fig3.gif)

<figcaption>

**Figure 3: A partial means-ends analysis of a high school**</figcaption>

</figure>

In this example, the school's goals and constraints are explicitly expressed. The _priorities_ indicate what the school considers is possible to achieve, given the goal and the constraints. One of their priorities is, for instance, to integrate information technology into the curriculum. To attain this priority, however, the school has planned its activities on a general level and decided what general functions to carry out. One of them is _teaching_. This level is also relevant to the design of systems to support the school's activities. The principal may decide, for example, that the school requires a system to support teaching, but no system is required to facilitate policy development or the purchase of technology. Focusing on teaching, then, the analysis continues its breakdown to the means that are employed to perform the general function of teaching (the physical processes), and then the physical resources that are used for each process.

The Means-Ends Analysis preserves complexity, and at the same time makes it possible to analyse the system. It preserves complexity because each level provides a different description of the system _as a whole_, rather than breaking it into isolated parts. In addition, it provides a good mechanism to cope with complexity because each upper level provides the context for the lower one. That is, it reflects how constraints in one level affect the level below.

This template can be used to analyse a work domain, as well as a task or a decision.

## Description versus analysis

Focusing on the analysis of the behaviour-shaping constraints, rather than on the observed behaviour, makes Cognitive Work Analysis particularly useful for the design of information systems. A mere description of an observed behaviour presents various problems for designers. People's information behaviour is informed by the mental models they have on the information world around them, but some of these models can be incomplete or wrong. The design of information systems should not be led by such models. Further, not all people have the same mental model but a designer cannot know which models are complete and correct. In addition, the information systems that are already in place, and their limitations, greatly influence their users' mental models and their information behaviour. As a general rule, however, designers try to create new, or improved, systems, rather than replicating existing ones. Alternatively, by gaining an in-depth understanding of the factors that shape information behaviour, researchers can determine what information behaviour patterns **can** take place, or what strategies **can** be used, independently of how observed actors interact with current systems. This frees the design from its dependence on the capabilities of existing systems and their effectiveness in the process of human-information interaction.

The analysis of goals and behaviour-shaping constraints creates a formative model. Vicente explains that a formative model is, '_A model that describes requirements that must be satisfied so that a system could behave in a new, desired way_' ([Vicente 1999: 7](#vicente)). Unlike a descriptive model, which describes how things are, or a normative, which explains how things should be, a formative model portrays what is possible. The map Mary provides the hotel guests shows constraints: the layout of streets, buildings on the way, direction one is allowed to drive on each street, distance, and so on. Analysis of constraints makes it possible to create maps. Similarly, designers of information systems would need to know _the lay of the land_ of information behaviour. Therefore, studies of human information behaviour that uncover what users need, what is possible for them to do, and what is not possible, would be most useful for systems design.

## Challenges to the application of Cognitive Work Analysis

Because of its built-in flexibility, Cognitive Work Analysis provides no recipes for its deployment. While other research frameworks often instruct researchers what methods to use, and what questions to ask, Cognitive Work Analysis does not subscribe to a set of methods, or research questions. It offers a general approach, and requires the individual researcher to select the appropriate methods and the specific questions to ask, based on the phenomenon that is being investigated.

This presents two major challenges. First, to apply the approach effectively requires some knowledge and experience in human information behaviour research. Novice human information behaviour researchers may encounter difficulties when attempting to use this framework for the first time. Secondly, while guidelines about useful methods and research questions can be developed for a particular work domain, these cannot be automatically generalized to another domain. However, it is likely that establishing a rich tradition of applying Cognitive Work Analysis to the design of information system would generate guidelines that would guide future studies in various domains.

In addition to the knowledge level required from a researcher, carrying out a cognitive work analysis for the purpose of designing an information systems is highly resource demanding. Because the Cognitive Work Analysis approach calls for an in-depth understanding of the constraints and processes in place, a typical study involves an extensive field study in addition to the laboratory experimentation that is needed for the design itself. While not inherently a challenge, such an in-depth approach is not always easy to support in our times of scarce resources for research and preference for fast results.

## Conclusion

The need to create a bridge between the study of human information behaviour and the design of information systems has been voiced in Information Science as well as in other areas, such as Information Systems ([e.g., Johnstone _et al._ 2004](#johnstone)). Cognitive Work Analysis provides one approach to make studies in human-information interaction relevant to systems design.

While addressing the more general area of human-information interaction, Cognitive Work Analysis contributes to the study of information seeking in context in various ways. While it does not identify the specific context-related variables that affect human-information interaction for all actors, it delineates the dimensions that together shape and contribute to this interaction. Moreover, these dimensions have been developed through many empirical studies of human interaction with systems in the work place, and can be used to analyse this interaction and aid in the design of information systems. Through its dimensions, templates, and formative approach, Cognitive Work Analysis has proved highly effective in investigating the complex and dynamic nature of the context and the phenomena that human information behaviour research addresses.

On the spectrum of research approaches, ranging from the reductionist and generalizable approaches, to the holistic and individual ones, Cognitive Work Analysis is placed somewhere in the middle, adapting a holistic approach focusing on the task or function actors perform. While, to date, only a few information systems have been designed based on this approach, they have proved highly effective and had impact on design. Because the development of Cognitive Work Analysis is based on empirical research, future research in human-information interaction will not only result in improving the requirements for the design of additional information systems, it will also further refine the general application of Cognitive Work Analysis to the design of information systems.

## Notes

<a id="footnote1"></a>1\. The phrases _human-information interaction_, _human information behaviour_, and _information behaviour_ represent the same concept in this paper and are used interchangeably.

<a id="note2"></a>_Editor's note_.The term collaboratory has not yet found its way into the Oxford English Dictionary. It appears to have been coined by William Wulf, who defined it as a "...'center without walls,' in which the nation's researchers can perform their research without regard to geographical location - interacting with colleagues, accessing instrumentation, sharing data and computational resources, [and] accessing information in digital libraries". (Wulf W.A. Towards a National Collaboratory. In Joshua Lederberg, and Keith Uncapher, Towards a National Collaboratory: [Unpublished] report of an Invitational Workshop (Rockefeller University, New York City, 13-15 March 1989), p. 3) The term is sometimes used (wrongly it seems) simply to mean a building within which research is carried on by a number of collaborating units.

## References

*   <a id="albrechtsen"></a>Albrechtsen, H., Pejtersen, A.M. and Cleal, B. (2002). Empirical work analysis of collaborative film indexing. In H. Bruce _et al._ (Eds.), _Emerging frameworks and methods: Proceedings of the Fourth International Conference on Conceptions of Library and Information Science._ (pp. 85-108). Greenwood Village, CO: Libraries Unlimited.
*   <a id="churchman"></a>Churchman, C. W. (1979). _The systems approach._ New York: Dell.
*   <a id="dervin"></a>Dervin, B. (2003). [Human studies and user studies: a call for methodological interdisciplinarity.](http://informationr.net/ir/9-1/paper166.html) _Information Research_, **9**(1) paper 166\. Retrieved 15 October, 2003 from http://informationr.net/ir/9-1/paper166.html.
*   <a id="fidel1999"></a>Fidel, R. _et al._ (1999). A visit to the information mall: Web searching behavior of high school students. _Journal of American Society of Information Science_, **50**(1), 24-37.
*   <a id="fidel04"></a>Fidel, R., Pejtersen, A.M., Cleal, B. & Bruce, H. (2004). A multi-dimensional approach to the study of human-information interaction: a case study of collaborative information retrieval. _Journal of the American Society for Information Science and Technology_, **55**(11) 939-953
*   <a id="hertzum"></a>Hertzum, M., _et al._ (2002). An analysis of collaboration in three film archives: a case for collaboratories. In H. Bruce _et al._ (Eds.), _Emerging Frameworks and Methods: Proceedings of the Fourth International Conference on Conceptions of Library and Information Science._ (pp. 69-84). Greenwood Village, CO: Libraries Unlimited.
*   <a id="johnson"></a>Johnson, D.J. (2003). On context of information seeking. _Information Processing & Management_, **39**(5), 735-760.
*   <a id="johnstone"></a>Johnstone, D., Tate, M. and Bonner, M. (2004). [Bringing human information behaviour into information systems research: an application of systems modeling.](http://informationr.net/ir/9-4/paper191.html) _Information Research_, **9**(4) paper 191\. Retrieved 15 July, 2004 from http://InformationR.net/ir/9-4/paper191.html.
*   <a id="pejtersen1985"></a>Pejtersen, A.M. (1985). Implications of users' value perception for the design of a bibliographic retrieval system. In J.C. Agrawal and P. Zunde (Eds.), _Empirical foundations of information and software science._ (pp. 23-37). New York, NY: Plenum.
*   <a id="pejtersen1989"></a>Pejtersen, A.M. (1989). _The BOOK House: modelling user needs and search strategies as a basis for system design._ Roskilde, Denmark: Risø National Laboratory. (Risø report M-2794).
*   <a id="pejtersen1992"></a>Pejtersen, A.M. (1992). The Book House. An icon based database system for fiction retrieval in public libraries. In: B. Cronin (Ed.), _The marketing of library and information services 2._ (pp. 572-591). London: Aslib.
*   <a id="pejtersen1998"></a>Pejtersen, A.M., and Fidel, R. (1998). _[A framework for work-centred evaluation and design: a case study of IR on the Web.](http://www.dcs.gla.ac.uk/mira/workshops/grenoble/fp.pdf)_ Working paper for the MIRA workshop, Grenoble, March 1988\. Retrieved 13 August, 2004 from http://www.dcs.gla.ac.uk/mira/workshops/ grenoble/fp.pdf
*   <a id="rasmussen1986"></a>Rasmussen, J. (1986). _Information processing and human-machine interaction: an approach to Cognitive Engineering._ New York, NY: North-Holland.
*   <a id="rasmussen1990"></a>Rasmussen, J., Pejtersen, A.M., & Schmidt, K. (1990). _Taxonomy for cognitive work analysis._ Roskilde, Denmark: Risø National Laboratory. (Risø report M-2871).
*   <a id="rasmussen1994"></a>Rasmussen, J., Pejtersen, A.M. and Goodstein, L.P. (1994). _Cognitive Systems Engineering._ New York, NY: Wiley.
*   <a id="sanderson"></a>Sanderson, P.M. (2003). Cognitive work analysis. In J. Carroll (Ed.), _HCI models, theories, and frameworks: toward an interdisciplinary science._ (pp. 225-264). San Francisco, CA: Morgan-Kaufmann.
*   <a id="vicente"></a>Vicente, K.J. (1999). _Cognitive work analysis._ Mahwah, NJ: Lawrence Erlbaum.
*   <a id="woods"></a>Woods, D. (2003). Discovering how distributed cognitive systems work. In E. Hollnagel (Ed.), _Handbook of cognitive task design._ (pp. 37-53). Mahwah, NJ: Lawrence Erlbaum.