####Information Research, Vol. 11 No. 3, April 2006



# Use of information sources by cancer patients: results of a systematic review of the research literature

#### [Kalyani Ankem](mailto:ankem@nc.rr.com)  
School of Library and Information Sciences  
North Carolina Central University  
Durham, North Carolina 27707, USA


#### Abstract

> **Objectives.** Existing findings on cancer patients' use of information sources were synthesized to 1) rank the most and least used information sources and the most helpful information sources and to 2) find the impact of patient demographics and situations on use of information sources.  
> **Methods.** To synthesize results found across studies, a systematic review was conducted. Medline and CINAHL were searched to retrieve literature on cancer patients' information source use. The retrieved articles were carefully selected according to predetermined criteria, and several articles were eliminated in a systematic approach.  
> **Analysis.** The twelve articles that met the criteria were systematically analysed by extracting data from articles and summarizing data for the purpose of synthesis to determine the meaning of findings on most used information sources, least used information sources, most helpful information sources, effect of patient characteristics on preference for an information source, and effect of patient situations on preference for an information source.  
> **Results.** In descending order of use, health care professionals, medical pamphlets, and family and friends were most used information sources. Internet and support groups were least used. In descending order of helpfulness, books, health care professionals and medical pamphlets were found to be most helpful information sources. Younger patients used health care professionals and certain forms of written information sources more than older patients.  
> **Conclusions.** The systematic review shows that many areas of cancer patients' information source use have been either neglected or barely analysed. An in-depth understanding of cancer patients' use of information sources and the characteristics in information sources they consider to be helpful is important for developing successful interventions to better inform patients.

* * *

## Introduction

A diagnosis of cancer is a stressful life experience. The nature of the disease requires patients to learn about the illness, make difficult decisions regarding ensuing treatment, and cope with the consequences of the illness. It has been found that having relevant information not only helps cancer patients to understand the disease, but it also facilitates their decision-making and coping ([Cassileth _et al._ 1980](#cassileth80), [Iconomou _et al._ 2002](#iconomou)). Graydon _et al._ ([1997](#graydon)) have postulated five categories for information needs among breast cancer patients: 1) nature of disease, its process and prognosis, 2) cancer treatments, 3) investigative tests, 4) preventive, restorative, and maintenance physical care and 5) patient's or family's psychosocial concerns. Degner _et al._ ([1997](#degner)) developed nine categories of information needs among cancer patients: 1) spread of the disease, 2) likelihood of cure, 3) treatment options, 4) side-effects of treatment, 5) effect on family and friends, 6) risk of disease to family, 7) impact on work, daily activities, and social life, 8) self-care issues and 9) sexual concerns. Similar categories have been developed by other researchers.

Where do these patients get this much-needed information? Only a limited number of studies have examined the information sources that cancer patients use to seek needed information. For example, Rees and Bath ([2000a](#rees)), in a review of the literature on the information behaviour of breast cancer patients and family members from 1988 to 1998, could only find four studies on the use of information sources by breast cancer patients. More studies are certainly required on this topic. Understanding the phenomenon of information source use among cancer patients is important because interventions are needed to ensure that these patients are exposed to a range of information sources and are substantially informed about the management of this complex disease ([Schapira _et al._ 1999](#schapira)).

More recent research indicates that cancer patients, during their illness, have a variety of information sources available to obtain the information needed to learn, decide, adjust and cope. Medical professionals, such as physicians, nurses, chemotherapy and radiotherapy staff, are expected to be primary and important sources of information for the patients. Although different medical professionals possess varying levels of medical knowledge, these sources are generally well-informed and well-placed to provide information to patients while caring for them. In addition to conveying oral information during interactions with patients, health care professionals present their knowledge to consumers through pamphlets, Web sites, medical books and articles in journals. Research shows that patients diagnosed with cancer may supplement the information obtained from these sources with that from various non-medical information sources: family and/or friends, fellow cancer patients, support groups, magazines, television, radio and newspapers.

More recent research ([Mills & Davidson 2002](#mills); [Raupach and Hiller 2002](#raupach)) indicates that cancer patients, during their illness, have a variety of information sources available to obtain the information needed to learn, decide, adjust and cope. Medical professionals, such as physicians, nurses, chemotherapy and radiotherapy staff, are expected to be primary and important sources of information for the patients ([Luker _et al._ 1996](#luker); [Mills and Davidson 2002](#mills)). Although different medical professionals possess varying levels of medical knowledge, these sources are generally well-informed and well-placed to provide information to patients while caring for them. In addition to conveying oral information during interactions with patients, health care professionals present their knowledge to consumers through pamphlets ([Davison _et al._ 2002](#davison)), Web sites ( [Katz _et al._ 2004](#katz)), medical books ([American Cancer Society n.d.](#ameXX)) and articles in journals ([American Medical Association n.d.](#amedXX)). Research shows ( [Carlsson 2000](#carlsson), [Mills and Davidson 2002](#mills), [Raupach and Hiller 2002](#raupach), [Turk-Charles _et al._ 1997](#turk-charles)) that patients diagnosed with cancer may supplement the information obtained from these sources with that from various non-medical information sources: family and/or friends, fellow cancer patients, support groups, magazines, television, radio and newspapers.

Among these sources, the mass media sources—magazines, television, radio, newspapers, books, journals, pamphlets, and Web sites—in particular, allow for considerable passive information reception in addition to active information seeking ([Carlsson 2000](#carlsson)). For instance, there is greater chance of passive information reception from magazines, newspapers, television, and radio. A patient could be watching the news on television and may receive cancer-related information without making a conscious effort to find the information. In comparison, the information seeking from Web sites is often active; that is, the patients have to make a conscious effort to find cancer-related information, as they also would when they read books, refer to narratives (accounts of patient experience), peruse pamphlets, or contact telephone helplines. These alternate modes facilitate greater access.

Mass media sources vary in quality because of the differences in rigour of dissemination processes which they undergo. Content presented through magazines, television, radio, newspapers, and Web sites, especially that produced outside health care settings, may convey misleading and inaccurate information ( [Seidman _et al._ 2003](#seidman)). More reliable are mass media sources, such as medical books, journals, and pamphlets, which are often geared towards specific audiences and are communicated by health care professionals through more rigorous dissemination processes. A caveat is that medical books and journals may be difficult for some patients to read. Even educational pamphlets, which are actually targeted toward patients, have often been criticized for being pitched at the wrong educational level ([Foltz and Sullivan 1996](#folz)). All written materials, however, have the advantage of allowing the patient to review and reference the material in the future.

Interpersonal sources, on the other hand, have the potential to be more effective in communication. For instance, family and friends, although lay sources, are trusted information sources readily available to the patients and are essential in the coping process ([Luker _et al._ 1996](#luker)). Similarly, other cancer patients can provide comfort through sharing personal experiences although these may sometimes be outdated experiences ([Schapira _et al._ 1999](schapira)). Also, support groups, like family and friends, can be instrumental in helping patients cope with the illness ([Edgar _et al._ 2000](#edgar)).

Current research literature indicates that cancer patients, through the stages of diagnosis, treatment, and post-treatment, tend to use a combination of these information sources ([Mills and Davidson 2002](#mills)), both medical and non-medical, including mass media and interpersonal, each of which present different advantages for use; that is, the patients do not simply receive information from one or two sources. In addition, cancer patients' use of these information sources has been known to vary based on individual patient characteristics and the situations patients find themselves in during the illness ([Mills and Davidson 2002](#mills); [Raupach and Hiller 2002](#raupach)). According to the literature, age, gender, and education are patient characteristics which can affect use of information sources by this patient group ( [Bilodeau and Degner 1996](#bil96); [Mills and Davidson 2002](#mills)). Similarly, according to the literature ([Bilodeau and Degner 1996](#bil96); [Raupach and Hiller 2002](#raupach)), these patients' use of information sources can be influenced by situational variables, such as stage of cancer at diagnosis, time passed since being diagnosed with cancer, their preference for a role in decision-making related to illness, and the type of treatment that they are undergoing.

In this study, the author conducted a systematic review of existing studies on information source use by cancer patients. The systematic review, although underutilized in library and information science, is a widely applied method in medicine, psychology, and business among other disciplines to synthesize and summarize existing quantitative data related to a research problem. When mathematical computation is employed to synthesize data, a systematic review is considered a meta-analysis. Mathematical amalgamation, however, is not a requirement for systematic reviews. Systematic reviews can be conducted, as in this study, by summarizing the quantitative results of existing studies and by drawings conclusions about what the results mean as a body of literature.

The method was applied to: 1) gather existing data on use of information sources and variations in use of information sources according to patient demographics and patient situational variables and to, 2) summarize the frequency of use of specific information sources, perceived helpfulness of specific information sources, and relationships between demographic and situational variables and information source use across studies in the literature. Quantitative data representative of the above were gathered from individual studies across the literature. Subsequently, data were summarized to show what the findings related to different research questions from independent studies mean as a body of literature. A synthesis of the results will, as intended, provide guidance for interventions designed to ensure that patients are exposed to a number of information sources set in place to impart much needed information in the management of a complex disease. For instance, the review will tell us whether or not individual characteristics should be taken into consideration in directing patients to information sources, or whether or not situations during the illness should lead to context-based selection of information sources for patients. Do the information sources which are preferred by patients possess certain qualities that we can understand and emulate in information provision? Is there something we can do about important information sources that are underutilized? Most importantly, perhaps we can understand what patients consider helpful and incorporate these qualities in information delivery.

The following research questions guided the probe for gathering and summarizing data:

1.  Which information sources are most used by cancer patients in seeking information?
2.  Which information sources are least used by cancer patients in seeking information?
3.  Which information sources do cancer patients find helpful?
4.  What is the influence of patient demographics, such as age, sex, and education, on cancer patients' choice of information sources?
5.  What is the influence of patient situational variables - stage of illness at diagnosis, time elapsed since diagnosis, role preferred in decision-making regarding illness, and type of treatment being received - on cancer patients' choice of information sources?

## Procedures

A systematic review was conducted to synthesize results from previous studies on the use of information sources by cancer patients. It is the first of its kind in analysing the concepts covered herein. As discussed above, the systematic review as a method is an organized approach where separate results from related studies that already exist on a topic are combined to reach conclusions. When the results are mathematically combined, the systematic review is called a meta-analysis ([Ankem 2005](#ankem)); however, a mathematical amalgamation is not a requirement for a systematic review. In this study, a systematic review was conducted by analysing and presenting the analytics in synthesis; that is, without the actual mathematical combination. A detailed description of systematic review as a research method can be found in an article by Carr ([2002](#carr)). In addition to Carr's article, several applications of the method can be retrieved from the [Cochrane Collaboration](http://www.cochrane.org/index0.htm) database which is dedicated to providing access to systematic reviews. One such review of a different aspect of information provision—methods of information giving—was followed to ensure procedural accuracy in conducting a systematic review ([McPherson _et al._ 2001](#mcpherson)).

This author embarked on a sizeable project to synthesize literature on cancer patients' information needs, information seeking, and information source use. A meta-analysis of factors affecting cancer patients' level of information needs and a systematic review of their need for specific types of information have been completed. In the project, this article is a paper which probes the use of information sources by cancer patients. As in one of the other papers, given the number of studies available and the nature of existing statistical results related to the research questions examined in this paper, a systematic review was deemed to be a more suitable method than a meta-analysis.

To commence review, online databases, Medline and CINAHL, were searched. The first search in Medline and CINAHL was with the subject heading neoplasm (with the explode function) which was combined using the Boolean 'OR' with the search term cancer patients (in the title, abstract, MESH subject heading, and CAS registry and EC number word) to retrieve studies on cancer patients. This search was combined with several separate searches using the following terms: information need(s), information seeking, information seeking behaviour, information source(s), and information resource(s) (in the title, abstract, Mesh subject heading, and CAS registry/EC number word) to retrieve studies only on cancer patients' information needs, seeking, and use. The final search was limited to English language, adults, and the years 1993-2003\. Additionally, the final search in CINAHL was limited to research studies. These final searches produced 196 articles in Medline and 283 articles in CINAHL which included studies conducted in countries other than the United States. The reasons for limiting the literature search to a ten-year period from 1993 to 2003 were (1) the development of new information sources, such as web sites, within the past decade and 2) the consequential change in information behaviour among consumers who want more information.

Based on this author's prior knowledge, Medline and CINAHL are the only databases that point to information seeking studies that were conducted with actual cancer patients and that were published during 1993-2003\. However, searches similar to those conducted in Medline and CINAHL were carried in LISA to find research on the topics of interest published during 1993-2003\. The term cancer patients (in the title, abstract, and descriptors) was combined separately with searches on the terms information need(s), information seeking, information seeking behaviur, information source(s), and information resource(s) (all in the title, abstract, and descriptors). These searches did not produce any publications of studies on information needs, seeking, or use with actual cancer patients. To have an idea of the coverage of research on the topics of interest generally in LISA, the search was broadened to only the term cancer patients (in the title, abstract, and descriptors) to retrieve all studies related to cancer patients published during 1993-2003\. This broader search produced 38 publications. Several of these studies, however, focused on the information sources designed for cancer patients without actually involving cancer patients in the studies. Only three studies included cancer patients as participants, and in two of the three, patients' perceptions of information technology in health care delivery were examined. Only one study ( [Rees and Bath, 2000b](#reesb)) was somewhat related to the topics of interest in this author's project. Rees and Bath's article examines advantages and disadvantages of various information sources as viewed by actual cancer patients participating in the study. It was determined that even this study does not deal directly with information needs, seeking, or use among cancer patients.

The process of identifying results is the most painstaking and time-consuming part of conducting a systematic review. To identify studies on the topic, the titles and abstracts of the retrieved articles were perused. This led to the selection of 110 publications on information needs and information source use. As the present study was part of a larger study, literature on information needs was included in the review at an early stage, but was separated later. In a systematic review, several criteria are established _a priori_ to find specific studies that will provide pertinent data for the review. At this point, the general criteria established for the inclusion of studies were: (1) examination of information needs of cancer patients or information sources used by cancer patients, (2) inclusion of adults as subjects in the study (the age restriction in searching had retrieved a study with 17-year olds), and (3) application of quantitative research methods and statistics: any statistic to convey need for information or use of an information source and inferential statistics, in particular, to convey a relationship between a demographic or situational characteristics and need for information or use of an information source. Demographic characteristics considered relevant were age, education, and sex while situational characteristics considered relevant were: time since diagnosis, stage of cancer, type of treatment received, and patient's preferred role in making illness-related decisions. The above review led to the elimination of thirty-five more papers that dealt with qualitative studies on information needs, evaluation of general communication channels or site specific cancer information-oriented programmes, breast cancer risk, hereditary breast cancer and genetic testing, prevention and screening, symptoms before diagnosis, information needs before breast biopsy, quality of life during cancer, coping during cancer, and impact of a particular information provider, such as the Cancer Information Service (CIS).

Most systematic reviews analyse quantitative results from clinical trials. This is not to say that qualitative results must be excluded as they can be synthesized separately. This study, in the popular approach, is limited to synthesis of quantitative results.

The remaining seventy-five articles were critically read for specific data on the use of information sources by cancer patients and the relationship, if any, between their demographics and situations and their use of a certain information source. From these remaining articles, twelve were selected. All of the papers that dealt solely with information needs were separated for other research studies. One article was removed because the information sources analysed were too few and all the sources covered were within health care which would not allow for meaningful comparison with the range of information sources covered in other papers. To include studies on patient information seeking activities—both for facts and advice—from several information sources that are the focus here, a careful distinction was made to separate and remove studies on patient educational activities which are often about a specific educational programme from a provider perspective.

Also, in the pool of sixty-three papers removed were those that dealt with the following topics, which were not directly relevant to the present systematic review:

*   sources patients would prefer to use (not sources they actually used);
*   number of sources used without specification and description of individual sources used;
*   use of information formats (such as audio or video);
*   preference for presentation style (such as reading versus conversation);
*   barriers to use of information sources by special groups, such as minority or ethnic groups;
*   use of a service offered by a source (such as support, not information, offered by a support group, or medical care, not medical information, offered by physicians);
*   perceived quality of an information source (not perceived helpfulness or usefulness of an information source; satisfaction gained from receiving information from a source, or importance attached to an information source);
*   helpfulness of a service (not information) provided by a source;
*   usefulness of a service (not information) provided by a source;
*   satisfaction gained from receiving a service (not information) from a source;
*   effects of demographics and situations on use of a service (not information) provided by a source;
*   effects of demographics and situations on satisfaction with information received from a source; and
*   effects of demographics and situations on satisfaction with service received from a source.

In the selection of the twelve studies for the systematic review, no distinction was made between types of cancer. The larger pool of studies retrieved through the literature search examined patients diagnosed with heterogeneous types of cancer. The publications selected were all peer-reviewed journal articles authored by researchers in medicine, nursing, public health, pharmacy, psychology, psychiatry, and physical therapy. Sample sizes in individual studies ranged from 75 to 334\. The majority of sample sizes, however, ranged from 105 to 160.

### Measurement of use of information sources, demographics, and situations

A discussion of measurement of variables follows to justify the inclusion of the twelve studies in the systematic review, as it is important in such a review that variables of interest to a research question are comparable across studies. Most questionnaires that captured information source use were designed specifically for a study; thus, they were not standardized instruments but were based on earlier psychometric literature. For instance, Dunn _et al._'s ([1999](#dunn)) instrument was based on work by Cassileth _et al._ ([1985](#Cassileth85)), and Mills and Davidson's ([2002](#mills)) instrument titled Information Sources Questionnaire was based on work by Luker _et al._ ([1996](#luker)). Face or content validity of these instruments is discussed in the articles. Overall, seven instruments in seven different studies were tested, and the researchers ([Biley _et al._ 2001](#biley); [Bosompra _et al._ 2002](#bosompra); [Mills and Davidson 2002](#mills); [Raupach and Hiller 2002](#raupach); [Schapira _et al._ 1999](#schapira); [Steginga _et al._ 2002](#steginga); [Turk-Charles _et al._ 1997](#turk-charles)) report reliability for three of these: the two instruments administered by Bosompra et al. (2001) and Steginga _et al._ (2002) and the instrument administered by Turk-Charles _et al._ ([1997](#turk-charles)) to gather data on non-medical information seeking by cancer patients. The instrument appearing in the Turk-Charles _et al._ study to measure non-medical information seeking was a counterpart to another instrument that they employed to gather data on patients' medical establishment information seeking. The latter of the two instruments employed by Turk-Charles _et al._ was based on the Krantz Health Opinion Survey for which reliability has been widely reported in the literature.

All instruments focused on the frequency of use of specific information sources. Usually, respondents are provided with a list of sources and asked to indicate whether they had used the sources. Turk-Charles _et al._ ([1997](#turk-charles)), instead, tapped into the frequency of use using "...a Likert scale for each type of non-medical information source (newspaper, television, and friends) and medical information sources (physicians and nurses).". The data thus gathered were applied only towards examining a relationship between demographic variables and use of information sources, not for ranking use of information sources.

While a more in-depth approach to the study of use can certainly be facilitated by qualitative research, administering predetermined lists is an acceptable approach in the quantitative research tradition. In a different approach to the common listing of information sources submitted to patients, Steginga _et al._ ([2002](#steginga)), instead of giving a list of information sources, asked the patients to provide information sources that they had used in making treatment decisions. Although quantitative, this approach was deemed different from all others, and the Steginga _et al._ study was included in the systematic review for summarizing most used but not for summarizing least used information sources because it is possible that patients may not think of mentioning an information source used infrequently or used only once.

In tapping the helpfulness of information sources, one approach to measurement was gathering the frequency with which patients attached usefulness to information sources, gained satisfaction through information sources, or attached importance to information sources. Another way to measure this concept was on a Likert scale of helpfulness for various information sources ([Schapira _et al._ 1999](#schapira)). As the above concepts were all considered to be closely identified with helpfulness, the results from the studies examining any of these concepts within the context of information source use were to be included in the systematic review for the purposes of ranking most helpful information sources. Perceived quality of an information source was not considered a similar concept because the idea of quality can be somewhat removed from the relevance that an information source has for an individual. An individual can think of an information source to be of high quality without ever using it or having any intention to use it. Alternatively, an information source thought to be important to an individual has more chance of use, thus, being useful or helpful.

The following is a description of the measurement of variables found in examining relationships between patient demographics and situations and use of information sources. Age was measured either as a categorical variable (age ranges provided for the patient to select from) or a continuous variable (the exact age of the patient). Education and sex were measured as categorical variables. Educational categories were secondary school, upper secondary school, and university degree. Sex was categorized as either male or female. Among situational variables, 'time since diagnosis' was measured as a categorical variable in intervals of time. Raupach and Hiller ([2002](#raupach)) measured time since diagnosis in six-month intervals ranging, in total, from six months to thirty months. The researchers gathered the frequency of use of various information sources for each time period. Certain situational variables—stage of illness, patient preference for role in decision-making, and type of treatment—were discovered to be overlooked in relation to use of information sources in the literature. Actually, of the demographic and situational determinants of information source use, only the relationship of age to information source use was examined in more than one study.

### Data extraction

Before data were extracted and categorized from individual studies for ranking, a list of information sources was developed to collate information sources that were too specific into a broader category that would be sufficiently distinct from other categories and be meaningful for ranking purposes. The eleven categories developed were:

1.  health care professionals;
2.  professionals in areas allied with health care, such as social work and psychology;
3.  broadcast and print journalism media, such as television, radio, newspapers, and magazines;
4.  books and journals;
5.  medical patient educational materials in various formats, including pamphlets;
6.  support groups;
7.  family and/or friends;
8.  other cancer patients;
9.  Internet;
10.  library; and
11.  all other information sources.

The first ten categories were all derived from the literature on cancer patients' information source use. The eleventh category 'other information sources' was included in this systematic review to accommodate information sources infrequently examined in the literature.

Frequency of use was usually represented in percentages in individual studies. In conjunction with the list of eleven information sources mentioned in the previous paragraph, these percentages were employed to arrive at the three most used information sources noted by patients from each study. The procedure was as follows. Taking Schapira _et al._ ([1999](#schapira)) as an example, this study ranked 'urologist' as first in information sources used, so 'urologist' was ranked first in the systematic review as well ( [see Table 1](#tab1)). 'Written pamphlets' was ranked second in information sources used, which fell under our broader 'medical patient educational materials' and was different from 'urologist' which fell under our 'health care professionals,' so the 'written pamphlets' were placed in the second column as second in ranking in this systematic review. 'Radiation oncologist' was ranked third in information sources used, but this fell under our broader 'health care professionals' which had already been ranked first; guided by the fact that one of the health care professionals was ranked first in the study, 'radiation oncologist' was also included as first in ranking in this systematic review. To show the researcher's original ranking in the individual study to the reader, the ranking from the study was entered next to each information source in parentheses. Hence, while 'urologist' and 'radiation oncologist' were entered as first in ranking in this review, the researcher's original ranks (1) and (3), respectively, are included in parentheses next to the items ([Table 1](#tab1)).

The next source in the Schapira _et al._ study was 'family' which fell under our broader 'family and friends' and was different from the other information sources already entered. Therefore, 'family' was entered in the third column for ranking as third in this review (Table 1). The approach allowed the collation of, for instance, 'urologist' and 'radiation oncologist' into 'health care professionals', a more identifiable information source that was distinct from the other information sources, 'medical patient educational materials' and 'family', that were ranked second and third in use in this study. This process was continued until the three most used information sources were extracted from each study for the systematic review. The idea was to synthesize what already exists in the literature in terms of use. Hence, the result will be a reflection of what has already been found, and no presumption is made that a certain combination of information sources, or one source complemented by other sources, is the best in providing information to cancer patients. Also, no distinction was made as to whether facts or advice were received from an information source.

Similar techniques were applied to extract data from articles and to organize data for ranking purposes so that the two least used information sources (by working in the opposite direction of frequency of use and ranking from the bottom-up) and the three most helpful information sources as perceived by cancer patients who participated in the studies could be determined. Again, the idea was not to find that a particular source was less used than generally expected: the intention was to reflect what the literature says about information sources that have been less used by cancer patients. In this case, the aim was to identify two information sources least used by less than 30% of the participants, which was the cutoff point based on percentages of low use in the studies.

For example, pamphlets and the Internet appeared as separate sources, and that is how they were categorized for analysis. The latter was assumed to be general cancer information sites on the Internet and the former a medical pamphlet in print distributed by health care organizations. A support group was thought to be an entity established to assist patients in coping, but was also capable of providing helpful information in any format.

In addition, as Tables 1-3 show, data concerning the type of information sought by patients, the time of information seeking during their illness (if available), cancer diagnosis of participating patients, and statistics of information source use were also extracted from each study. The type of information patients sought and the time element of information seeking, especially, add an important dimension to information source use which will be revisited in the results and conclusion sections in providing suggestions for future research.

To extract data on factors affecting use of information sources, any relationship between demographics and situational variables—age, education, sex, stage of disease, patient's decision-making style, time since diagnosis, and type of treatment received—and use of an information source among cancer patients, if available, was recorded. Relationships between factors and information use represented only in inferential statistics were entered (see Tables 4-5). Again, to provide a context for the use of information sources, data concerning cancer diagnosis of participating patients was extracted and entered. Also, it was deemed important to show the measurement and analysis within individual studies. Therefore, descriptions of instruments administered for gathering data on frequency of information source use (when available) and statistics employed to analyse relationships of patient demographics and/or situations to information source use within studies were also extracted and entered.

The synthesis was guided by the data within each table, which summarizes results from related studies relating to a research question. The three most used information sources extracted from each study are presented in Table 1\. In addition, Table 1 includes the type of information patients needed or sought, the point in time during illness at which they needed or sought the information as covered in the study, and the type of cancer with which the patients were diagnosed. The first two variables, in particular, provide a context for information source use by patients. Tables 2 and 3 are similar, but present, respectively, the lesser used information sources and the most helpful information sources.

Table 4 illustrates the effect of demographics—age, education and sex—on use of an information source. Again, along with pertinent results from each study, the table presents the cancer diagnosis of patients within each study. Also, the instruments administered for measurement and data gathering and the inferential statistic applied to study the effect of demographics on information source use are included. Age is the only demographic to have been analysed in more than one study. The last table, Table 5, is similar to Table 4 but shows the effect of situations on information source use. As can be seen, 'time since diagnosis' is the only variable to have been covered in the literature and in only one study. The type of information patients needed or sought and the time during which they needed or sought the information, which provide context for patients' use of information sources, in studies appearing in Tables 4-5, can be found in earlier tables when the studies provide such data. Both Table 4 and Table 5, summarize results, across studies, pertinent to a research question and illustrate what the results mean as a body of literature.

## Results

### The three most used information sources among cancer patients

As expected, health care professionals were the primary source of information for most cancer patients (Table 1) ([Bosompra _et al._ 2002](#bosompra), [Carlsson 2000](#carlsson), [Mills and Davidson 2002](#mills), [Nair _et al._ 2000](#nair), [Raupach and Hiller 2002](#raupach), [Schapira _et al._ 1999](#schapira), [Steginga _et al._ 2002](#steginga)). Among health care professionals, patients identified the urologist, oncologists, oncologist's staff, primary care physicians, the surgeon, the hospital consultant, nurses, chemotherapy staff, radiotherapy staff and the physical therapist as sources of information they had used often. Although patients participating in different studies ranked different medical professionals with whom they came into contact as the most used information source, when they ranked physicians of any type as an information source, they consistently placed physicians in first place.

<a id="tab1" name="tab1"></a>

<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Three most used information sources among cancer patients**</caption>

<tbody>

<tr>

<th rowspan="2">Study</th>

<th colspan="3">Most used information sources</th>

<th rowspan="2">Information needed and time of need</th>

<th rowspan="2">Type of Cancer</th>

</tr>

<tr>

<th>First</th>

<th>Second</th>

<th>Third</th>

</tr>

<tr>

<td valign="top">Nair _et al._ 2000</td>

<td valign="top">Physician/nurse(1)</td>

<td valign="top">Readings(2)</td>

<td valign="top">Friends/relatives(3)</td>

<td valign="top">Information: chemotherapy side effects</td>

<td valign="top">Ovarian and heterogeneous - not specified</td>

</tr>

<tr>

<td valign="top">Schapira _et al._ 1999</td>

<td valign="top">Urologist(1)  
Radiation oncologist(3)  
General medical(5)</td>

<td valign="top">Written pamphlet(2)</td>

<td valign="top">Family(4)  
Friends with prostate cancer(4)</td>

<td valign="top">Time: during diagnosis and initial treatment</td>

<td valign="top">Prostate cancer</td>

</tr>

<tr>

<td valign="top">Biley _et al._ 2001</td>

<td valign="top">Word of mouth(1)</td>

<td valign="top">Leaflets(2)</td>

<td valign="top">Television(3)</td>

<td valign="top">Information: general</td>

<td valign="top">Not specified</td>

</tr>

<tr>

<td valign="top">Mills and Davidson 2002</td>

<td valign="top">Hospital consultant(1)  
General practitioner(2)  
Chemotherapy/radiotherapy staff(3)  
Ward nursing staff(4)</td>

<td valign="top">Family/friends(5)</td>

<td valign="top">Hospital written information(6)</td>

<td valign="top">Information: general</td>

<td valign="top">Heterogeneous - colorectal, lung, breast, prostate, gynecological, gastric</td>

</tr>

<tr>

<td valign="top">Edgar _et al._ 2000</td>

<td valign="top">Found out myself(1)</td>

<td valign="top">Family/friends(2)</td>

<td valign="top">Nurse(3)  
Note: more, however, learned about cancer support groups from nurses than through family/friends</td>

<td valign="top">Information: psychological support, cancer support services, and complementary therapies  
Time: while completing treatment</td>

<td valign="top">Breast cancer</td>

</tr>

<tr>

<td valign="top">Carlsson 2000</td>

<td valign="top">Health care staff(1)</td>

<td valign="top">Newspapers, magazines(2)  
Television, radio(3)</td>

<td valign="top">Television, radio(3)</td>

<td valign="top">Information: general</td>

<td valign="top">All diagnoses except leukaemia, myeloma, and neuroendocrine tumors</td>

</tr>

<tr>

<td valign="top">Bosompra _et al._ 2002</td>

<td valign="top">Physical therapist(1)  
Oncologist's staff(3)  
Primary care physician(4)  
Radiation oncologist(5)  
Oncologist(6)</td>

<td valign="top">Books(2)</td>

<td valign="top">American Cancer Society(6)</td>

<td valign="top">Information: general  
Time: recently diagnosed and treated</td>

<td valign="top">Breast cancer</td>

</tr>

<tr>

<td valign="top">Steginga _et al._ 2002</td>

<td valign="top">Physician(1)</td>

<td valign="top">Internet(2)</td>

<td valign="top">Family/friends(3)</td>

<td valign="top">Time: during decision-making about treatment</td>

<td valign="top">Prostate cancer</td>

</tr>

<tr>

<td valign="top">Davison _et al._ 2002</td>

<td valign="top">Friend/relative(1)</td>

<td valign="top">Pamphlets from the physician's office(2)</td>

<td valign="top">Other prostate cancer patients(2)</td>

<td valign="top">Time: since diagnosis</td>

<td valign="top">Prostate cancer</td>

</tr>

<tr>

<td valign="top">Raupach and Hiller 2002</td>

<td valign="top">Surgeon(1)  
General practitioner(5)</td>

<td valign="top">Television(2)  
Magazines(3)  
Newspapers(4)</td>

<td valign="top">Books(6)</td>

<td valign="top">Time: following primary treatment</td>

<td valign="top">Breast cancer</td>

</tr>

</tbody>

</table>

After health care professionals, medical pamphlets, which are designed to educate cancer patients and are provided to patients in health care settings were the most frequently used information sources ([Biley _et al._ 2001](#biley), [Davison _et al._ 2002](#davison), [Schapira _et al._ 1999](#schapira)). The ranking was based on patients' reported use of medical pamphlets in three individual studies. In the review these were taken to be more specific than 'readings' (reported in the study by [Nair _et al._ 2000](#nair)), which could have covered more diverse written materials. Similarly, the 'leaflets' reported in the study by Biley _et al._, were taken to be more specific written materials.

In the analysis of the studies, family and friends were ranked third in use as an information source by cancer patients ([Carlsson 2000](#carlsson), [Nair _et al._ 2000](#nair), [Schapira _et al._ 1999](#schapira), [Steginga _et al._ 2002](#steginga)). The studies show that family and friends are a source for information for these patients and not simply a source of support. Thus, overall, in descending order of use, health care professionals, medical pamphlets, and family and friends were found to be the three information sources patients diagnosed with cancer most often consult for information.

The number of patients who received information from health care professionals was rather high, ranging from 86% to 100%. One study ([Bosompra _et al._ 2002](#bosompra)), which had a slightly different emphasis on the time of interaction, reported that 35% received information from health care professionals. The proportion of patients who used pamphlets to get information ranged from 34% to 79%. The third category, those who rely on family and friends for information, followed close behind, with the scores ranging from 46% to 55%. One exception was a study which showed 32% had received information from people in their lives ([Steginga _et al._ 2002](#steginga)).

Table 1 also provides the context for each study that examined the use of information sources by cancer patients. Only five studies examined type of information needed or sought by cancer patients. Of these, researchers of three studies limited themselves to one to three types of information to focus on whereas the researchers of the other two studies categorized all information sought by cancer patients into several types as they conceived them. Details of types of information in all five studies can be found in Table 1\. It is important to note that no study tied the types of information needed or sought to varying information source use; that is, there was no probing of which information source is used for which type of information. As for time during illness, different studies focused on different points ranging from diagnosis to post-treatment. All points of illness, such as diagnosis, pre-treatment, initial stage of treatment, mid stage of treatment, latter stage of treatment, and post-treatment, were never covered in any one study. The use of information sources at different times during illness was examined in only one study, discussed later in this systematic review in the section on the impact of situation on information source use.

### Information sources least used by cancer patients

The Internet was clearly cancer patients' least-used information source (Table 2) ([Carlsson 2000](#carlsson), [Mills and Davidson 2002](#mills), [Raupach and Hiller 2002](#raupach)). The reader must keep in mind that although the literature reviewed was published between 1993 and 2003, the only some of the studies included the Internet as an option on the list provided to participants to probe how frequently it was used by patients. Table 2 shows the year of publication of the articles from which these results were extracted. Secondly, even though the Internet was not as much in use and certainly the Web, in particular, was not in use, by the general public in the early nineties, there were still Internet tools, such as gopher, files on ftp servers, and bulletin boards, which were used by a substantial number of people. By the mid-nineties, Internet use had certainly increased, and by the end of the decade, it had become prevalent. Regardless of these developments, in this review, a more recent article indicates that the elderly cancer patients did not use this information source (Table 2).

<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Less used information sources among cancer patients**</caption>

<tbody>

<tr>

<th>Study</th>

<th>Least used information sources</th>

<th>Less used information source</th>

<th>Information needed and time of need</th>

<th>Type of Cancer</th>

</tr>

<tr>

<td valign="top">Nair _et al._ 2000</td>

<td valign="top">Consent form</td>

<td valign="top"> </td>

<td valign="top">Information: chemotherapy side effects</td>

<td valign="top">Ovarian and heterogeneous - not specified</td>

</tr>

<tr>

<td valign="top">Schapira _et al._ 1999</td>

<td valign="top">Support group(1)</td>

<td valign="top">Religious(2)</td>

<td valign="top">Time: during diagnosis and initial treatment</td>

<td valign="top">Prostate cancer</td>

</tr>

<tr>

<td valign="top">Biley _et al._ 2001</td>

<td valign="top">Computer packages(1)</td>

<td valign="top">Internet(2)</td>

<td valign="top">Information: general</td>

<td valign="top">Not specified</td>

</tr>

<tr>

<td valign="top">Mills and Davidson 2002</td>

<td valign="top">Internet(1)</td>

<td valign="top">Support groups(3)</td>

<td valign="top">Information: general</td>

<td valign="top">Heterogeneous - colorectal, lung, breast, prostate, gynecological, gastric</td>

</tr>

<tr>

<td valign="top">Edgar _et al._ 2000</td>

<td valign="top">Psychiatrist/psychologist(2)  
Social worker(4)</td>

<td valign="top">Cancer support organization(6)</td>

<td valign="top">Information: psychological support, cancer support services, and complementary therapies  
Time: while completing treatment</td>

<td valign="top">Breast cancer</td>

</tr>

<tr>

<td valign="top">Carlsson 2000</td>

<td valign="top">Internet(1)</td>

<td valign="top">Hospital-based telephone helpline(2)</td>

<td valign="top">Information: general</td>

<td valign="top">All diagnoses except leukaemia, myeloma, and neuroendocrine tumors</td>

</tr>

<tr>

<td valign="top">Bosompra _et al._ 2002</td>

<td valign="top">Relative(1)</td>

<td valign="top">Library(2)</td>

<td valign="top">Information: general</td>

<td valign="top">Breast cancer</td>

</tr>

<tr>

<td valign="top">Davison _et al._ 2002</td>

<td valign="top">Newspaper articles(1)</td>

<td valign="top"> </td>

<td valign="top">Time: since diagnosis</td>

<td valign="top">Prostate cancer</td>

</tr>

<tr>

<td valign="top">Raupach and Hiller 2002</td>

<td valign="top">Internet(1)</td>

<td valign="top">Peer support programme volunteer(1)  
Support groups(2)</td>

<td valign="top">Time: following primary treatment</td>

<td valign="top">Breast cancer</td>

</tr>

</tbody>

</table>

Somewhat more in use than the Internet, but still in the bottom rank in use by cancer patients, were support groups ([Edgar _et al._ 2000](#edgar), [Mills and Davidson 2002](#mills), [Raupach and Hiller 2002](#raupach)) which can help patients cope with their illness. Again, this rating is very interesting because support groups were cited as the most helpful information source (see below) by the few cancer patients who reported using them in one study ([Raupach and Hiller 2002](#raupach)).

The number of patients who had used the Internet for information was very low, ranging from 4% to 10%, and the number that turned to support groups for information was higher, ranging from 5% to 28%. This leads us to question why the Internet and support groups, both important information sources, were not being used by cancer patients: this is addressed in the discussion section.

The context in which the individual studies of use of information sources were conducted, is also provided in Table 2\. This has similar data to that presented in Table 1 because many of the same studies were used in analysing the most-used and least-used information sources. Table 2 shows that few studies focused on any particular type of information. Also, although many studies focused on some point in time during illness ranging from pre-treatment through treatment to post-treatment, almost none probed the connection between time periods and information source use.

### The most helpful information sources

Both medical and other cancer-related books were cited as the most helpful (Table 3) ([Carlsson 2000](#carlsson), [Schapira _et al._ 1999](#schapira)). Interestingly, books were not frequently-used information sources, and the most used information sources, the health care professionals, were not cited as most helpful (Table 1 and Table 3). Health care professionals, however, were ranked second in helpfulness ([Luker _et al._ 1996](#luker), [Schapira _et al._ 1999](#schapira)) along with medical pamphlets (Table 3) ([Luker _et al._ 1996](#luker), [Raupach and Hiller 2002](#raupach)). Medical pamphlets were, also, like health care professionals, among highly-used information sources (Table 1). Among health care professionals, those found to be helpful by patients were the radiation oncologist, the nurse, the urologist, the hospital consultant, and the general practitioner, but no one type of health care professional was cited as helpful in more than one study leading to uncertainty as to which health care professional is viewed as the most helpful.

<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Helpfulness of information sources**</caption>

<tbody>

<tr>

<th rowspan="2">Study</th>

<th colspan="3">Most helpful information sources</th>

<th rowspan="2">Information needed and time of need</th>

<th rowspan="2">Type of Cancer</th>

</tr>

<tr>

<th>First</th>

<th>Second</th>

<th>Third</th>

</tr>

<tr>

<td valign="top">Schapira _et al._ 1999</td>

<td valign="top">Books(1)  
Note: The summary score of electronic and print media which included books ranked third in Schapira's categorical ranking.  
Although helpful, books were only used by 32%.</td>

<td valign="top">Radiation oncologist(2)  
Nurse(3)  
Urologist(4)  
Note: The summary score of health professionals which included radiation oncologist, nurse, and urologist ranked first in Schapira's categorical ranking.  
Radiation oncologists were used by 48%, nurses by 21%, and urologist by 100%.</td>

<td valign="top">Support group(5)  
Note: The summary score of patient educational materials which included support groups ranked second in Schapira's categorical ranking.  
Although helpful, support groups were used by only 5%.</td>

<td valign="top">Time: during diagnosis and initial treatment</td>

<td valign="top">Prostate cancer</td>

</tr>

<tr>

<td valign="top">Carlsson 2000</td>

<td valign="top">Medical books(1)</td>

<td valign="top">Television(2)</td>

<td valign="top">Narratives(3)</td>

<td valign="top">Information: general</td>

<td valign="top">All diagnoses except leukaemia, myeloma, and neuroendocrine tumors</td>

</tr>

<tr>

<td valign="top">Luker _et al._ 1996</td>

<td valign="top">Hospital consultant(1)  
Nurse(2)  
Nurses on wards and clinics(4)  
General practitioner(5)</td>

<td valign="top">Leaflets(3)</td>

<td valign="top">Women's magazines(5)</td>

<td valign="top">Time: newly diagnosed stage</td>

<td valign="top">Breast cancer</td>

</tr>

<tr>

<td valign="top">Luker _et al._ 1996  
</td>

<td valign="top">Women's magazines(1)  
Television/radio(2)  
Newspapers(3)</td>

<td valign="top">Hospital consultant(2)  
General practitioner(3)</td>

<td valign="top">Family/friends(4)</td>

<td valign="top">Time: follow-up stage</td>

<td valign="top">Breast cancer</td>

</tr>

<tr>

<td valign="top">Raupach and Hiller 2002  
 - ranked by percentage who found a source helpful of those who used the source</td>

<td valign="top">Support groups(1)</td>

<td valign="top">Brochures(2)</td>

<td valign="top">Surgeon(3)  
Note: Same percentage of those who used Family as an information source were highly satisfied. Only 21, however, used Family as opposed to 111 who used surgeon. Hence, it is ranked third here.</td>

<td valign="top">Time: following primary treatment</td>

<td valign="top">Breast cancer</td>

</tr>

<tr>

<td valign="top">Raupach and Hiller 2002  
 - ranked by percentage of entire sample who found a source helpful</td>

<td valign="top">Surgeon(1)  
General practitioner(3)</td>

<td valign="top">Books(2)</td>

<td valign="top">Other women with breast cancer(4)</td>

<td valign="top">Time: following primary treatment</td>

<td valign="top">Breast cancer</td>

</tr>

</tbody>

</table>

Nearly 68% of the patients in the Carlsson ([2000](#carlsson)) study considered books to be the most important source for information and 32% of patients in the Schapira _et al._ ([1999](#schapira)) study, who had actually used books for information, gave these sources the highest rating of helpfulness (4.4 on a scale of 5). Patients who considered the health care professional and pamphlets also to be useful ranged from 42% to 69%. About 48% of the patients in the Schapira _et al._ ([1999](#schapira)) study, who actually reported to have received information from the radiation oncologist, rated these professionals the second most helpful (4.3 on a scale of 5).

Different information sources were ranked third in helpfulness in the studies included in the review; hence, the information source that was third in helpfulness was unclear and could not be determined.

### Influence of patient characteristics on use of information sources

The systematic review indicates that age may play a role in the use of information sources. Younger patients consulted health care professionals and forms of written materials more than older patients did. Table 4 shows the age groups of patients studied by the three researchers who explored the effect of age on sources patients chose for information.

The three studies conducted by Carlsson ([2000](#carlsson)), Mills and Davidson ([2002](#mills)), and Turk-Charles _et al._ ([1997](#turk-charles)) point to somewhat different results, but when taken together they support the above conclusion. Carlsson's ([2000](#carlsson)) study, which concerns only written sources, found that younger patients used newspapers and narratives more than older patients did. Mills and Davidson ([2002](#mills)) report that younger patients used health care professionals and written sources more. These sources were only part of many other sources that they found younger patients to use more. According to them, younger patients also used other mass media sources, such as television, radio, and the Internet, and interpersonal sources outside of the medical establishment, and most importantly, family and friends and support groups more than older patients did. They go on to say that use by older patients, as their age increased, that is, those in the over-65 group, actually, made poor use of not only written information sources but also television, radio, and support groups.

<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Effect of patient demographics on use of information sources**</caption>

<tbody>

<tr>

<th>Study and demographics</th>

<th>Effect on use of information sources</th>

<th>Type of cancer</th>

<th>Measure</th>

<th>Statistic</th>

</tr>

<tr>

<td>**Age**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td valign="top">Mills and Davidson 2002</td>

<td valign="top">Most of the information sources - hospital consultant, chemotherapy/radiotherapy staff, ward staff, written information, family/friends, TV/radio, support/voluntary groups, and Internet - were used most frequently by younger patients (</= 45) and least frequently by oldest patient group (>/= 65).  
Only two sources - physiotherapists and dieticians - were used most frequently by patients aged 45-64 years.  
Patients aged over 65 years, made far less use of written information, TV/radio, and support/voluntary groups.</td>

<td valign="top">Heterogeneous -colorectal, lung, breast, prostate, gynecological, gastric</td>

<td valign="top">Information Sources Questionnaire (ISQ) designed by researchers for the study</td>

<td align="center" valign="middle">χ<sup style="font-size: x-small;">2</sup></td>

</tr>

<tr>

<td valign="top">Carlsson 2000</td>

<td valign="top">Younger patients (</= 60) used narratives to a greater extent than older patients (>/= 60).  
Younger patients had also read articles about cancer in the newspapers to a significantly greater extent.</td>

<td valign="top">All diagnoses except leukaemia, myeloma, and neuroendocrine tumors</td>

<td valign="top">Designed by researchers specifically for the study</td>

<td align="center" valign="middle">χ<sup style="font-size: x-small;">2</sup></td>

</tr>

<tr>

<td valign="top">Turk-Charles _et al._ 1997</td>

<td valign="top">Younger patients sought more information from medical establishment information sources (physicians and nurses) than older patients. That is, information seeking from medical establishment information sources decreased as age of patients increased. No age differences were found in seeking information from non-medical establishment sources (newspaper, television, and friends).</td>

<td valign="top">Heterogeneous - not specified</td>

<td valign="top">Krantz Health Opinion Survey to collect data on medical establishment information-seeking  
and another measure designed by researchers specifically for the study employed to collect data on non-medical establishment information-seeking</td>

<td align="center" valign="middle">_r_</td>

</tr>

<tr>

<td valign="top">**Education**</td>

<td valign="top"> </td>

<td valign="top"> </td>

<td valign="top"> </td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Carlsson 2000</td>

<td valign="top">Patients with higher level of education (those with upper secondary school or university degree) used Internet, medical books and telephone helpline more than people with lower level of education (those with secondary school education only up to the legally allowed age for discontinuing school).</td>

<td valign="top">All diagnoses except leukaemia, myeloma, and neuroendocrine tumors</td>

<td valign="top">Designed by researchers specifically for the study</td>

<td align="center" valign="middle">χ<sup style="font-size: x-small;">2</sup></td>

</tr>

<tr>

<td valign="top">**Sex**</td>

<td valign="top"> </td>

<td valign="top"> </td>

<td valign="top"> </td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Carlsson 2000</td>

<td valign="top">Women used narratives to a greater extent than men.</td>

<td valign="top">All diagnoses except leukaemia, myeloma, and neuroendocrine tumors</td>

<td valign="top">Designed by researchers specifically for the study</td>

<td align="center" valign="middle">χ<sup style="font-size: x-small;">2</sup></td>

</tr>

</tbody>

</table>

In comparison, Turk-Charles and colleagues found support for only the health care professionals. In a different approach, in which they separated non-medical establishment information seeking (using newspapers, television, and friends) from medical establishment information seeking (using physicians and nurses), they found that there was no difference, between younger and older patients, in use of non-medical information sources—newspapers, television, and friends— ([Turk-Charles _et al._ 1997](#turk-charles)). That is not to say that Turk-Charles and colleagues contradict use of all written sources by younger patients found by the others. Their approach is different, and they did not study written sources available within the medical establishment.

Education and sex were explored in only one study for the impact that they may have on information source use (Table 4). Carlsson ([2000](#carlsson)) found that patients with higher education used the Internet, medical books, and a telephone helpline more than patients with lower levels of education, and women used narratives more than men. Thus, the relationship between education and sex and use of information sources calls for more research.

### Influence of patient situations on use of information sources

Among patient situations deemed relevant to this systematic review, only 'time elapsed since a patient was diagnosed with cancer' was examined for its effect on information source use and in only one study. According to the researchers in this study, the use of the surgeon, the cancer specialist, the breast care nurse, the peer support programme volunteer, books, brochures, and friends decreased with increasing time since diagnosis (Table 5) which spanned different treatment stages and post-treatment ([Raupach and Hiller 2002](#raupach)). Again, more research is called for on the effect of relevant situations, including the effect of time on patients' choice of information sources, as they move through diagnosis, stages of treatment, and post treatment.

<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Effect of patient situations on use of information sources**</caption>

<tbody>

<tr>

<th>Study and situation</th>

<th>Effect on use of information sources</th>

<th>Type of cancer</th>

<th>Measure</th>

<th>Statistic</th>

</tr>

<tr>

<td valign="top">Raupach and Hiller 2002  
Time since diagnosis</td>

<td valign="top">Fewer women received information from the surgeon, cancer specialists, breast care nurse, peer support programme volunteer, books, brochures, and friends with increasing time since diagnosis.</td>

<td valign="top">Breast cancer</td>

<td valign="top">Designed by researchers specifically for the study</td>

<td valign="top">Prevalence rate ratio (PRR)</td>

</tr>

</tbody>

</table>

## Discussion and Conclusions

Cancer patients consulted health care professionals, medical pamphlets, and family and friends most often for information. The use of health care professionals and written materials from the medical establishment was expected. Most striking, is the reliance on family and friends as information sources for information, not just for support. It shows that family and friends are instrumental in the process of managing a complex disease when the patient's ability to comprehend all the information they receive may be impaired due to the stress of illness.

Interestingly, patients were not using the Internet and support groups for information as much as the other sources. The Internet, as an information resource, has existed since the early nineties, and the Web since the mid-nineties, and the support groups are important sources that can be instrumental in coping. It is possible that use of the Internet among elderly cancer patients has increased since the studies cited in this systematic review were conducted. Alternatively, it could be that these elderly patients need assistance using the Internet because it has been developed after the active periods of their lives. Mills and Davidson ([2002](#mills)) report that cancer patients rated the Internet as a high quality source for information. Not all information on the Internet is of high quality, and it is a source where the user must be more critical than when consulting many other sources.

Cancer patients found books, health professionals, and medical pamphlets to be the most helpful. The results do not tell us which particular books or specific health professionals are the most helpful to cancer patients. Information source use was found to vary among cancer patients only in relation to age; that is, younger patients were found to use health care professionals and certain forms (not always specified) of written materials more than older patients did. Time as an influence on choice of information source was dealt with in one study which showed that use of mass-media and interpersonal sources mainly within health care decreased over time.

In addition to the results presented above, this systematic review reveals that many questions of information source use by cancer patients either remain unanswered or require more research. An analysis of reasons behind the low use of the Internet and support groups—if the low use of the Internet still exists—can assist in increasing the awareness of these sources among patients. An in-depth qualitative analysis of the characteristics of the most helpful sources can provide guidance in improving information provision. A finding of the specific written sources that are not used as much, and again, the reasons for less than expected use of both health care professionals and these written information sources by older patients, can be valuable in assisting older patients in making better use of these important sources. Also, an understanding of which sources patients use as time progresses during illness can provide guidance in selecting a channel for reaching patients after they leave the health care environment.

As noted, some areas were found either to be completely neglected or barely analysed in the literature. How patient's choice of information sources is affected by other situations, such as stage of illness at diagnosis, role preferred in decision-making regarding illness, and type of treatment being received requires probing. Also, the types of information cancer patients need or seek and the information source they use for a particular type of information requires analysis to guide selection of information sources based on patient needs. A better understanding of these areas of information source use can assist in developing successful interventions the better to inform cancer patients.

.

## Acknowledgements

The author wishes to thank the editor of Information Research, Professor Tom Wilson, for his close reading of the paper and providing several insightful comments and suggestions. Also, the comments of the anonymous referees were greatly helpful in revising the paper.

## References

*   <a name="ankem" id="ankem"></a>Ankem, K. (2005). Approaches to meta-analysis: a guide for LIS researchers. _Library & Information Science Research,_ **27**(2), 164-176.
*   <a name="ameXX" id="ameXX"></a>American Cancer Society. (n.d.). _[Books for patients, family & friends.](http://www.cancer.org/docroot/PUB/PUB_0.asp?publicationTypeName=Books_for_PFF)_ Retrieved 6 March, 2005 from http://www.cancer.org/docroot/PUB/PUB_0.asp?publicationTypeName=Books_for_PFF
*   <a name="amedXX" id="amedXX"></a>American Medical Association. (n.d.). _[JAMA patient page.](http://jama.ama-assn.org/cgi/collection/patient_page) _Retrieved 6 March, 2005 from http://jama.ama-assn.org/cgi/collection/patient_page
*   <a name="biley" id="biley"></a>Biley, A., Robbe, I. & Laugharne, C. (2001). Sources of health information for people with cancer. _British Journal of Nursing,_ **10**(2), 102-106.
*   <a name="bil96" id="bil96"></a>Bilodeau, B.A., & Degner, L.F. (1996). Information needs, sources of information, and decisional roles in women with breast cancer. _Oncology Nursing Forum_, **23**(4), 691-696.
*   <a name="bosompra" id="bosompra"></a>Bosompra, K., Ashikaga, T., O'Brien, P.J., Nelson, L., Skelly, J. & Beatty, D.J. (2002). Knowledge about preventing and managing lymphedema: a survey of recently diagnosed and treated breast cancer patients. _Patient Education & Counseling,_ **47**(2), 155-163.
*   <a name="carr" id="carr"></a>Carr, AB. (2002). Systematic reviews of the literature: the overview and meta-analysis. _Dental Clinics of North America,_ **46**(1), 79-86.
*   <a name="carlsson" id="carlsson"></a>Carlsson, M. (2000). Cancer patients seeking information from sources outside the health care system. _Supportive Care Cancer,_ **8**(6), 453-457.
*   <a name="cassileth85" id="cassileth85"></a>Cassileth, B.R., Lusk, E.J., Bodenheimer, B.J., Farber, J.M., Jochimsen, P. & Morrin- Taylor B. (1985). Chemotherapeutic toxicity: the relationship between patients' pretreatment expectations and posttreatment results. _American Journal of Clinical Oncology,_ **8**(5), 419-425.
*   <a name="cassileth80" id="cassileth80"></a>Cassileth, B.R., Volckmar, D. & Goodman, R.L. (1980). The effect of experience on radiation therapy patients' desire for information. _International Journal of Radiation Oncology, Biology, Physics,_ **6**(4), 493-496.
*   <a name="davison" id="davison"></a>Davison, B.J., Gleave, M.E., Goldenberg, S.L., Degner, L.F., Hoffart, D. & Berkowitz, J. (2002). Assessing information and decision preferences of men with prostate cancer and their partners. _Cancer Nursing,_ **25**(1), 42-49\.
*   <a name="degner" id="degner"></a>Degner, L.F., Kristjanson, L.J., Bowman, D., Sloan, J.A., Carriere, K.C., O'Neil, J., Bilodeau, B., Watson, P. & Mueller, B. (1997). Information needs and decisional preferences in women with breast cancer. _Journal of the American Medical Association,_ **277**(18), 1485-1492\.
*   <a name="dunn" id="dunn"></a>Dunn, J., Steginga, S.K., Occhipinti, S., McCaffrey, J. & Collins, D.M. (1999). Men's preferences for sources of information about and support for cancer. _Journal of Cancer Education,_ **14**(4), 238-242\.
*   <a name="edgar" id="edgar"></a>Edgar, L., Remmer, J., Rosberger. Z. & Fournier, M.A. (2000). Resource use in women completing treatment for breast cancer. _Psycho-Oncology,_ **9**(5), 428-438\.
*   <a name="foltz" id="foltz"></a>Foltz, A. & Sullivan, J. (1996). Reading level, learning presentation, and desire for information among cancer patients. _Journal of Cancer Education,_ **11**(1), 32-38\.
*   <a name="graydon" id="graydon"></a>Graydon, J., Galloway, S., Palmer-Wickham, S., Harrison, D., Rich-van der Bij, L., West, P., Burlein-Hall, S. & Evans-Boyden, B. (1997). Information needs of women during early treatment for breast Cancer. _Journal of Advanced Nursing,_ **26**(1), 59-64.
*   <a name="iconomou" id="iconomou"></a>Iconomou, G., Viha, A., Koutras, A., Vagenakis, A.G. & Kalofonos, H.P. (2002). Information needs and awareness of diagnosis in patients with cancer receiving chemotherapy: a report from Greece. _Palliative Medicine,_ **16**(14), 315-321.
*   <a name="katz" id="katz"></a>Katz, S.J., Nissan, N., & Moyer, C.A. (2004). Crossing the digital divide: evaluating online communication between patients and their providers. _American Journal of Managed Care_, **10**(9), 593-598.
*   <a name="luker" id="luker"></a>Luker, K.A., Beaver, K., Leinster, S.J. & Glynn Owens, R. (1996). Information needs and sources of information for women with breast cancer: a follow-up study. _Journal of Advanced Nursing,_ **23**(3), 487-495.
*   <a name="mcpherson" id="mcpherson"></a>McPherson, C.J., Higginson, I.J. & Hearn, J. (2001). Effective methods of giving information in cancer: a systematic literature review of randomized controlled trials. _Journal of Public Health Medicine,_ **23**(3), 227-234.
*   <a name="mills" id="mills"></a>Mills, M.E. & Davidson, R. (2002). Cancer patients' sources of information: use and quality issues. _Psycho-Oncology,_ **11**(5), 371-378.
*   <a name="muha" id="muha"></a>Muha, C., Smith, K.S., Baum, S., Ter Maat, J. & Ward, J.A. (1998). The use and selection of sources in information seeking: the Cancer Information Service experience. Part 8\. _Journal of Health Communication,_ **3**(3),(Supplement 1), 109-120.
*   <a name="nair" id="nair"></a>Nair, M.G., Hickok, J.T., Roscoe, J.A. & Morrow, G.R. (2000). Sources of information used by patients to learn about chemotherapy side effects. _Journal of Cancer Education,_ **15**(1), 19-22.
*   <a name="raupach" id="raupach"></a>Raupach, J.C. & Hiller, J.E. (2002). Information and support for women following the primary treatment of breast cancer. _Health Expectations,_ **5**(4), 289-301.
*   <a name="rees" id="rees"></a>Rees, C.E. & Bath, P.A. (2000a). The information needs and source preferences of women with breast cancer and their family members: a review of the literature published between 1988 and 1998\. _Journal of Advanced Nursing,_ **31**(4), 833-841.
*   <a name="reesb" id="reesb"></a>Rees, C.E., & Bath P.A. (2000b). Mass media sources for breast cancer information: their advantages and disadvantages for women with the disease. _Journal of Documentation_, **56**(3), 235-249.
*   <a name="schapira" id="schapira"></a>Schapira, M.M., Meade, C.D., McAuliffe, T.L., Lawrence, W.F. & Nattinger, A.B. (1999). Information sources and professional consultations sought by men newly diagnosed as having prostate cancer. _Journal of Cancer Education,_ **14**(4), 243-247.
*   <a name="seidman" id="seidman"></a>Seidman, J.J., Steinwachs, D., & Rubin, H.R. (2003). [Design and testing of a tool for evaluating the quality of diabetes consumer-information Web sites.](http://www.jmir.org/2003/4/e30/) _Journal of Medical Internet Research_, **5**(4). Retrieved 15 March, 2005 from http://www.jmir.org/2003/4/e30/
*   <a name="steginga" id="steginga"></a>Steginga, S.K., Occhipinti, S., Gardiner, R.A., Yaxley, J. & Heathcote, P. (2002). Making decisions about treatment for localized prostate cancer. _BJU International,_ **89**(3), 255-260.
*   <a name="turk-charles" id="turk-charles"></a>Turk-Charles, S., Meyerowitz, B.E. & Gatz, M. (1997). Age differences in information- seeking among cancer patients. _International Journal of Aging & Human Development,_ **45**(2), 85-98.

