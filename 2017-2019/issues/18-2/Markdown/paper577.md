<header>

#### vol. 18 no. 2, June, 2013

</header>

<article>

# Knowledge management, codification and tacit knowledge

#### [Chris Kimble](#author)  
Euromed Management, 13288 Marseille cedex 9, France, and, CREGOR/MRM, Universite´ Montpellier II, 34095 Montpellier cedex 5, France

<section>

#### Abstract

> **Introduction.** This article returns to a theme addressed in Vol. 8(1) October 2002 of the journal: knowledge management and the problem of managing tacit knowledge.  
> **Method.** The article is primarily a review and analysis of the literature associated with the management of knowledge. In particular, it focuses on the works of a group of economists who have studied the transformation of knowledge into information through the process of codification and the knowledge transaction topography they have developed to describe this process.  
> **Analysis.** The article explores the theoretical and philosophical antecedents of the economists' views. It uses this as a basis for examining the dominant views of knowledge that appear in much of the literature on knowledge management and for performing a critical evaluation of their work.  
> **Results.** The results of the analysis centre upon the question of when is it appropriate to codify knowledge. They present a basic summary of the costs and benefits of codification before looking in more detail at its desirability.  
> **Conclusions.** The conclusions concern the implications of the above for knowledge management and the management of tacit knowledge. They deal with the nature of knowledge management, some of the reasons why knowledge management projects fail to achieve their expectations and the potential problems of codification as a strategy for knowledge management.

## Introduction

This article returns to a theme addressed in [the special issue of Information Research](http://informationr.net/ir/8-1/infres81.html) on knowledge management: the feasibility of knowledge management and in particular the problem of dealing with tacit knowledge in knowledge management. In keeping with the tone of that earlier edition (sub-titled 'Knowledge management - the Emperor's new clothes?') we offer an agnostic's view of knowledge management. We do so by developing some of the themes from an article by Miller ([2002](#mil02)) entitled 'Information has no intrinsic meaning'. In his article Miller argues that knowledge results from the uniquely human capacity of attributing meanings to the messages we receive, that is, we each create our individual versions of knowledge.

In this article, we draw in particular on the works of economists who, within their discipline, have traditionally taken a somewhat different approach. For example, Ancori et al., observe,

> 'To be treated as an economic good, knowledge must be put in a form that allows it to circulate and be exchanged. The main transformation investigated by economists is the transformation of knowledge into information, i.e. the codification of knowledge. The process of codification allows them to treat knowledge-reduced-to-information according to the standard tools of economics.' ([Ancori _et al_. 2000](#anc00): 255-256)

Thus while Miller ([2002](#mil02)) argues that information has no intrinsic meaning and the same piece of information can result in different instances of knowledge, economists such as Ancori _et al_. argue that information can have an economic meaning and, if codified, different instances of knowledge can be treated as though they were pieces of information.

The first part of the article presents a conceptual framework, based on the work of Shannon and Weaver ([1949](#sha49)), which is used to discuss information and knowledge, avoiding the issue of tacit knowledge altogether. This view emphasises the importance of communication, an area that is often neglected or treated as unproblematic in the literature on knowledge management. It treats the semantic content of the message itself as irrelevant and, like Miller's article, leaves the problem of interpreting the semantics of a message to human beings.

Although useful, Shannon and Weaver's ideas are not universally applicable. The next part of the article examines the limits of their applicability and explores some alternative views of knowledge. It begins by looking at the traditional definition of knowledge as justified true belief that underpins the work of many economists and is largely consistent with Shannon and Weaver's theory. This is followed by an examination of the constructivist view of knowledge, which sees truth as, at best, a phenomenon that exists only within the confines of a particular social group. Finally, given the importance of groups to the latter viewpoint, we consider the issue of knowledge as a group phenomenon.

Having laid the foundations for our analysis, we then address the issue of tacit knowledge directly by looking at both Polanyi's ([1966](#pol66)) original formulation of the concept and the notion of tacit knowledge popularised by Nonaka ([1994](#non94)). We follow this with an examination of the issue of codification using a framework created by Cowan _et al_. ([2000](#cow00)) which delineates where, in the conceptual terrain between inarticulable tacit and fully codified explicit knowledge, the codification of knowledge might take place.

The final section addresses the question of the desirability and value of codification. It does this firstly by adopting the cost-benefit approach to the issue favoured by economists and then expanding this to consider the strategic, practical and epistemological problems of codification. Finally, the article concludes with some observations on the reasons why knowledge management projects fail to deliver the expected benefits or quickly fall into disuse.

## Information and knowledge

### Information as a flux

The classic approach of economists is to adopt a view of knowledge that allows it to be considered as a stock that is accumulated through interaction with an information flux.

The idea of an information flux is associated with Shannon and Weaver's ([1949](#sha49)) influential mathematical model of communication. The origins of the theory lie with Shannon's work in Bell Laboratories in the 1940s ([Shannon 1948](#sha48)). Shannon was primarily concerned with the problem of how to make the optimal use of the capacity provided by the company's telephone lines; the central problem was one of '_reproducing at one point, either exactly or approximately, a message selected at another point'_ where '_... the actual message is one selected from a set of possible messages_' ([Shannon 1948](#sha48): 5). According to the theory, the more of a message that was received, the higher would be the probability that the message selected from the set of possible messages was the same as the original.

In the theory, a message is treated as a stream of encoded characters transmitted from a source to a destination via a communication channel. The process begins with the sender who creates a message, which is then encoded and translated into a signal that is transmitted along the communication channel to a receiver, which decodes the message and reconstitutes it for a recipient. In principle, as long as a stable, shared syntax for encoding/decoding exists, the accuracy of the message is guaranteed. The sematic content of the message however is irrelevant to this process. As Shannon noted, 'Frequently the messages have meaning; that is they refer to or are correlated according to some system with certain physical or conceptual entities. These semantic aspects of communication are irrelevant to the engineering problem' ([Shannon 1948](#sha48): 5).

In his theory, the term information had a narrow, specific, technical meaning. Information (equivalent to additional coded symbols being received) was simply something that reduced the uncertainty that the correct message would be selected from a set of possible messages; it did not refer to the meaning of the message, i.e. its semantic content.

Despite Shannon's warning that the application of his ideas was not 'a trivial matter of translating his words to a new domain' ([Shannon 1956](#sha56)), economists argue that the cumulative effect of the flow of messages in an information flux is to reduce uncertainty, where uncertainty is now defined as, '… the difference between the amount of information required to perform the task and the amount of information already possessed by the organization' (Galbraith quoted in [Daft and Lengel 1986](#daf86): 556). In other words, for an economist, the effect of an information flux is to add, in a cumulative fashion, to the sum of what is already known.

### The context dependent nature of information

For a piece of information to be useful, one must know the context in which it will be used; yet, despite all of the advances in information technology, selecting and organizing relevant information is still a major problem. One explanation for this might be the difficulty of dealing with the range of contexts in which the information could be used. However, as Cohendet and Steinmueller point out ([2000](#coh00): 195), this issue could be dealt with simply by encoding the information about how it should be used into the message itself. Nevertheless, the problem continues to exist. Clearly, there is more to its solution than is implied by the above analysis.

Duguid ([2005](#dug05)) points to an underlying problem with this approach. To encode the knowledge about how to use a piece of information into the message itself means that the message must either be entirely self-contained or have access to some other source of information, a 'codebook', that will make its meaning clear. As he notes, such an argument runs the risk of becoming trapped between circularity (with codebooks explaining themselves) and infinite regress (with codebooks explaining codebooks).

If, as implied above, it is not always possible to encode everything required to decode a message into the message itself, how can meaningful communication be achieved? In the case of human agents the answer is simple, as Miller ([2002](#mil02)) suggests, it is the knowledge held within the individual that is used to impart meaning to the message. Human beings are able to learn how to assign meaning to events without needing to refer to a set of pre-programmed instructions. The implication of this is that, in the case of human agents, the decoding of the message needs to take into account not only the context of use, but also the knowledge (cognitive context) of the receiver.

## Contrasting concepts of knowledge

Although Shannon and Weaver's ideas are now more than 60 years old, they continue to remain influential. As Kogut and Zander ([1996](#kog96)) note,

> 'It is not hard to see its applicability to understanding communication as the problem of people knowing the commands and sharing common notions of coding, of the costs and reliability of various channels … [this] is especially appealing in an age in which machine manipulation of symbols has proven to be such a powerful aid to human intelligence.' ([Kogut and Zander 1996](#kog96): 509)

However, although useful as a model of information processing, their ideas are not always applicable. For Carlile ([2002](#car02)), this point is reached when, 'the problem shifts from one of processing more information to understanding … novel conditions or new knowledge that lies outside the current syntax' ([Carlile 2002](#car02): 444). Treating context dependent knowledge as though it were context independent, while legitimate in some circumstances, is not in others. To examine what those circumstances are, we need to look at the notion of knowledge more closely.

### Knowledge as justified true belief

The idea of knowledge as justified true belief is often attributed to Plato. Although there has been much philosophical debate about the validity of this definition ([Gettier 1963](#get63)), it remains one of the most frequently cited definitions of knowledge and acts as the starting point for our discussion.

According to Ancori _et al_. ([2000](#anc00)) most traditional economic theory is based on a realist ontology and a rationalist epistemology similar to that found in the physical sciences. Taken in its strongest form, this position would put forward the argument that there is a knowable reality that is both invariant and universal, that is, it is true at all times and in all places. Consequently, given a set of axioms, new knowledge can be deduced simply through the application of logic and reasoning. Like Shannon and Weaver's theory, the distinction between information and knowledge dissolves, and the problem of how to deal with knowledge at the level of a group, referred to below, all but disappears.

However in contrast to Shannon and Weaver, by linking knowledge to true belief, the semantics of the message become relevant, as this view now implies there is indeed a correct way to interpret information. It also implies that knowledge does not need to be justified by reference to sensory perceptions, and that there is no necessary connection between knowledge and action. If these assumptions are accepted, then learning (i.e. the creation of new knowledge) simply becomes the accumulation of one item of (true) information upon another. In other words, new knowledge is created through the combination (i.e. the union) of existing sets of information (Figure 1). It is this concept that underpins many of the cumulative models of knowledge found in the knowledge management literature, such as Ackoff's ([1989](#ack89)) typology of the progression from data to wisdom.

A variant of this viewpoint takes the same realist ontological stance of a universal and unchanging external reality, but relaxes the assumptions concerning truth claims by adopting an empiricist epistemology. Here the argument is that although there is an external truth, it is not possible to apprehend it directly. Instead, it can only be approached by formulating hypotheses and undertaking a series of controlled experiments to build, incrementally, a more accurate representation of reality. This view however has also been the subject of intense philosophical debate with authors such as Popper ([1959](#pop59)) and Khun ([1962](#kuh62)) pointing to the problem of separating the outcome of experiments from the process that generated them.

### The constructivist's concept of knowledge

For an alternative view of knowledge, we return to the situation outlined in the section on the context dependent nature of information and deal with the case where human agents are involved and everything required to decode a message has not, for whatever reason, been incorporated into the message itself.

The key change between this and the previous position is a shift from realism to what is sometimes termed anti-realism ([Chalmers 2009](#cha09)). For anti-realists, a stable, uncontested external reality does not exist. Stated in its extreme form, the anti-realist position poses many problems and more often reality is defined in terms of what is accepted as truth within the bounds of a particular group or community. Following the work of Berger and Luckmann ([1966](#ber66)), this is sometimes termed the constructivist or social constructivist approach.

In a constructivist approach, an action or message has no fixed meaning; such meaning that it does have only exists within the shared perception of a group of actors that hold common epistemic beliefs. Thus, the actors and their ability to recognise, interpret or ignore events become central to the way in which they organize and reorganize their representations of the world. The notion of a group of actors sharing cognitive structures that exist outside of themselves as individuals is problematic and will be dealt with shortly, but for the moment let us consider how this changes the justified true belief version of knowledge outlined above.

Firstly, knowledge now becomes both an input and an output. Existing knowledge, shaped by membership of a particular group and held in an individual's memory, acts as a filter to give meaning to the incoming information flux (input) and new knowledge is generated as meanings are attributed to events (output). Clearly, the assumption that knowledge does not need to be justified by reference to the perceptions of the recipient no longer holds, even if those perceptions are themselves influenced by membership of some broader community.

Secondly, as we shall see below, the assumption that there is no necessary connection between knowledge and action also no longer holds. Many of the ideas put forward for how the shared cognitive structures that provide the necessary epistemic glue to make the anti-realist position tenable, rely on mechanisms such as learning by doing. For example, Cook and Brown ([1999](#coo99)) argue that knowing something, as opposed to the passive act of simply having knowledge, is only ever realised in the course of applying that knowledge.

### Knowledge at the level of groups and communities

Observing that psychologists tend to focus on individual cognition in laboratory settings, Walsh ([1995](#wal95)) claims the notion of group-level cognition challenges the Western orthodoxy of the individual as an independent locus of social action; a sentiment later echoed by Ancori _et al_. ([2000](#anc00)) in relation to economists. Although the situation has changed somewhat in the intervening years, dealing with the concept of knowledge at the group-level still poses a number of problems.

The notion of knowledge held within an individual is relatively easy to grasp, however the idea of knowledge that is somehow shared between individuals in a group is more difficult to explain. In his exhaustive literature review, Walsh ([1995](#wal95)) provides numerous examples of studies that demonstrate the existence of what he terms 'supra-individual' knowledge structures; mental templates that give complex information environments form and meaning. However, the issue here is not the existence of such structures, nor particularly what those structures are, but with what they mean for our understanding of knowledge and our ability to manage it.

As indicated in the section on the constructivist's concept of knowledge, much of the work in this area has focused on communities of individuals who share a common worldview. Such communities are seen as the key to providing the cognitive common ground needed if one adopts the constructivist's anti-realist stance. Although the terms that are used have nuances that we do not explore here, epistemic communities and communities of practice are frequently cited as examples of communities that perform this role ([Creplet _et al_. 2001](#cre01); [Iverson and Mcphee 2002](#ive02)).

A common feature of both epistemic communities and communities of practice is that they rely heavily on the notion of knowledge being created through a process of learning by doing, an idea that places the link between knowledge and action at centre stage. Both emphasise how boundaries, created through a process of socialisation and centred on some communal activity, lead to a shared worldview that acts as a foundation for communication and collective action ([Wenger 1996](#wen96); [Håkanson 2010](#hak10)). Such groups not only create the basis for a common understanding of what is being done and why, they frequently create their own language to express this understanding to others in the group ([Lesser and Storck 2001](#les01); [Murillo 2008](#mur08)).

Using Walsh's terminology, these groups act as supra-individual knowledge structures, not only in the sense of them being a shared interpretive framework for individuals, but also in the sense of them acting as a form of collective memory ([Nelson and Winter 1982](#nel82); [Barley 1986](#bar86); [Lave and Wenger 1991](#lav91)). If these ideas are accepted, then our view of knowledge at the supra-individual level becomes closer to an intersection of the sets of knowledge held by individuals than the union of sets portrayed in the section on knowledge as justified true belief (Figure 1).

<figure>

![Figure 1: Contrasting views of supra-individual knowledge](../p577fig2.jpg)

<figcaption>Figure 1: Contrasting views of supra-individual knowledge</figcaption>

</figure>

## The problem with tacit knowledge

Until this point, we have looked at information and knowledge without addressing one of the core themes of this article: tacit knowledge. The discussion of tacit knowledge was left until now because it was felt that it would be more useful to explore some of the fundamental ideas behind knowledge and knowledge management before exploring this topic in any detail.

According to Cowan _et al_., the notion of tacit knowledge has, '_drifted far from its original epistemological and psychological moorings, has become unproductively amorphous [and] now obscures more than it clarifies_' ([Cowan _et al_. 2000](#cow00): 213). Tacit knowledge is usually described as knowledge that is either (a) inarticulable, that is, it is impossible to describe in propositional terms, or (b) implicit, that is, articulable but only with some difficulty. It is usually seen as being acquired through direct personal experience of something. Because it is hidden from the outside observer, and possibly even from the holder of the knowledge, it is also seen as being difficult to identify and measure.

In spite of this, tacit knowledge has become an important topic of discussion in the literature. For example, Easterby-Smith and Prieto ([2008](#eas08)) observe, '_Many of the authors in this field consider that the primary challenge [for Knowledge Management] lies in understanding the nature and processes of tacit knowledge as opposed to explicit knowledge_.' ([Easterby-Smith and Prieto 2008](#eas08): 239). In particular, tacit knowledge has been seen by many as a source of sustainable competitive advantage ([Barney 1991](#bar91); [Grant 1996b](#gra96b)) and innovation ([Kogut and Zander 1992](#kog92); [Leonard and Sensiper 1998](#leo98)).

The nature of tacit knowledge continues to be the subject of much critical attention ([Gourlay 2006b](#gou06b)). We will not explore this in detail here, but instead focus on two widely cited but different views of tacit knowledge. The first view is based on Polanyi's original ([1966](#pol66)) notion of the tacit dimension to knowledge. The second, and now probably the dominant view, is attributed to Nonaka and focuses primarily on the difference between tacit and explicit knowledge.

### Tacit knowledge and Polanyi

First , Polanyi was more concerned with knowing as a process than with the particular kind of knowledge that resulted from that process; accordingly, he wrote more about tacit knowing than about tacit knowledge. For Polanyi tacit knowledge was a dimension of knowledge that was located in the mind of an individual; there could be no such thing as objective explicit knowledge, which could exist independently of an individual's tacit knowing: '_The ideal of a strictly explicit knowledge is indeed self-contradictory; deprived of their tacit coefficients, all spoken words, all formulae, all maps and graphs, are strictly meaningless._' ([Polanyi 1969](#pol69): 195).

Many of Polanyi's ideas originated from his opposition to the positivist view of science and his concern to make clear the role that personal commitments and beliefs of scientists play in the practice of science. As Spender notes, '_for Polanyi science was a process of explicating the tacit intuitive understanding that was driven by the subconscious learning of the focused scientist_' ([Spender 1996](#spe96): 50). However although absolute objectivity can never be attained, Polanyi did believe that our tacit awareness connects us to an external reality.

For Polanyi, the tacit dimension to knowledge is inaccessible to the conscious mind. He used examples such as a skilful performer to illustrate this, arguing that the knowledge that underlies their performance is largely tacit in the sense that they would find it difficult or impossible to articulate what they were doing or why. However, although such knowledge may be difficult to articulate in some circumstances, it may be less difficult in others.

Polanyi makes a distinction between focal and subsidiary awareness. He illustrates this using the example of a carpenter hammering a nail into a piece of wood. The carpenter gives his attention to both the nail and hammer, but in a different way. His objective is to use the hammer to drive the nail into the wood; consequently, his attention is focused on what is happening to the nail, although he retains a subsidiary awareness of the hammer, which is used to guide his hand.

In Polanyi's example, the traveller first makes sense of his experiences using his own innate knowledge. He then composes a written (codified) account of his journey, which he sends to his friend. Upon reception, the traveller's friend interprets the letter using his own knowledge and experience to 'decode' the message. Thus, this process is not a simple process of codification and decoding but the intermingling of three distinct sets of knowledge employed in three different contexts; the knowledge of the traveller, the knowledge associated with producing and reading written documents and the knowledge held by the traveller's friend.

For Polanyi, knowing is the act of integrating tacit and explicit knowledge. Although it is possible to make certain aspects of knowledge explicit and encode it; something can only be _known_ when this explicit component is combined with the tacit in the mind of the receiver. In this sense, knowledge is best termed a duality ([Hildreth and Kimble 2002](#hil02)).

### Tacit knowledge and Nonaka

A different view of tacit knowledge can be found in much of the current literature on knowledge management. This view largely depends on the degree to which knowledge is articulable and presents tacit and explicit knowledge as two distinctive categories of knowledge. The intellectual basis for this view is usually attributed to Nonaka's reading of Polanyi.

According to Nonaka, '_Polanyi classified human knowledge into two categories. "Explicit" or codified knowledge refers to knowledge that is transmittable in formal, systematic language. On the other hand, "tacit" knowledge has a personal quality, which makes it hard to formalize and communicate._' ([Nonaka 1994: 16](#non94)). Although this may not be an accurate reflection of Polanyi's views, and was later revised ([Nonaka and von Krogh 2009](#non09)), this hard distinction between tacit and explicit has since been adopted by numerous authors ([Conner and Prahalad 1996](#con96); [O'Dell and Grayson 1998](#ode98); [Ipe 2003](#ipe03); [Feghali and El-Den 2008](#feg08); [Swart and Harvey 2011](#swa11)).

Nonaka first presented his ideas about how knowledge was used in organizations in 1991 and subsequently extended and developed them into what he termed an organizational knowledge creation theory ([Nonaka 1991](#non91), [1994](#non94); [Nonaka and Takeuchi 1995](#non95)). The concept of knowledge conversion, converting one form of knowledge into another, and that of the SECI model, which describes the mechanism through which this conversion takes place, are the key concepts in Nonaka's theory.

Schematically the SECI model consists of a two by two matrix with each cell in the matrix, Socialisation, Externalisation, Combination and Internalisation, representing one phase in a cycle of knowledge conversion. In the socialisation phase, an apprentice acquires tacit knowledge from an expert through working with them on a continuous basis. A transfer of tacit knowledge takes place. In the externalisation phase, the expert (previously the apprentice) is now able to articulate or externalise their freshly acquired knowledge, making it available for others to use. The newly acquired tacit knowledge is transformed into explicit knowledge. In the combination phase, guided by the organization's goals, this knowledge is combined with other explicit knowledge, either at the individual or at the collective level. In the final internalisation phase, the explicit knowledge created in the previous stage is internalised and is transformed into tacit knowledge again. Nonaka argues that this process of conversion and 'amplification' of knowledge can be repeated at different levels within an organization, moving in a growing spiral from an individual to a group level and later on to an organizational or inter-organizational level.

Effectively, this view presents knowledge as a dichotomy ([Hildreth and Kimble 2002](#hil02)) where knowledge is defined as being either tacit or explicit. This view lends itself easily to the notion of the management and codification of knowledge ([Gourlay 2006a](#gou06a)). For example, if knowledge can be separated into its components it becomes easier to conceive of knowledge as an object that can be owned, stored, manipulated and exchanged ([Schultze and Stabell 2004](#sch04)). It also encourages the juxtaposition of the terms tacit and codified knowledge, so that tacit becomes a label for anything that is uncodified ([Cowan _et al_. 2000](#cow00)), making the process of knowledge conversion between tacit and explicit appear to be a simple, one step, process.

In his later work, Nonaka attempted to clarify his views by asserting that, in fact, tacit and explicit knowledge existed on a continuum where, '_The notion of "continuum" refers to knowledge ranging from tacit to explicit and vice versa_.' ([Nonaka and von Krogh 2009: 637](#non09)). Although this might be conceptually different from the popular view of a hard distinction between two different types of knowledge, it remains fundamentally different from Polanyi's view of knowledge as a tacit/explicit duality where tacit and explicit are inextricably intertwined.

## The limits of codification

Although Shannon and Weaver's theories do not deal directly with the problem of managing knowledge, their notion of coding and decoding does provide one approach to dealing with it. What follows below is a description of an approach to dealing with the problem of tacit knowledge that starts from the seemingly more straightforward approach of codification. This is largely based on Cowan _et al_.'s ([2000](#cow00)) topography of knowledge transaction activities.

Cowan _et al_.'s approach to the issue of tacitness is that of a '_sceptical economist_'. By this, they do not necessarily mean that they are sceptical about the existence of tacit knowledge, nor that they are sceptical about its importance for economics, but principally that they have become concerned about the way that the term is used as a piece of fashionable economic jargon. Their goal was to restrict the realm of the tacit and to,

> critically reconsider the ways in which the concepts of tacitness and codification have come to be employed by economists, and to develop a more coherent re-conceptualization of these aspects of knowledge production and distribution activities. ([Cowan _et al_. 2000](#cow00): 213)

The focus of their work is on, '_where various knowledge transactions or activities take place, rather than where knowledge of different sorts may be said to reside._' ([Cowan _et al_. 2000](#cow00): 229).

Since its publication, Cowan _et al_.'s work has been the subject of criticism ([Duguid 2005](#dug05); [Balconi _et al_. 2007](#bal07)), however we have chosen to use it here as it highlights the circumstances in which knowledge might be codified and, in doing so, it throws the topic of tacit knowledge into sharper relief.

### Cowan _et al_.'s topography

Perhaps surprisingly, given that Cowan _et al_. are economists, certain aspects of their approach have more in common with the social constructivist view outlined in the section on the constructivist's concept of knowledge than the realist or rationalist view presented in the section on knowledge as justified true belief. Most of their analysis centres on the availability of a codebook. For Cowan _et al_., a codebook is an 'authorized dictionary' used to decode the content of messages (e.g., stored documents). The codes it contains are not a representation of an underlying truth but a set of standards or rules that have been endorsed by an authority or gained authority through common consent. They concede that some of the codes may come from specialised knowledge that has been acquired elsewhere, and may itself be tacit. They also accept that not everybody will have the knowledge required to interpret the codes properly. Thus, what is codified for one person may be tacit for another and completely incomprehensible for a third.

Their topography starts with a three-way split between knowledge that has already been articulated, and is therefore already codified; knowledge that is as yet unarticulated but in principle could be articulated, and knowledge that is simply inarticulable (Figure 2).

They do not address the issue of tacit knowledge, that is, knowledge that is inarticulable, directly. Their focus is on codification and the use of codebooks; they simply suggest that the third part of the initial branching that corresponds to this should be set aside as '_not very interesting_' ([Cowan _et al_. 2000](#cow00): 230). The branch, labelled _articulated_, is dealt with in a similar fashion. Cowan _et al_. argue that if the knowledge has already been articulated, is recorded and is referred to by the group, then a codebook must exist and must be in use; effectively, the knowledge is already codified and of no further interest.

<figure>

![Figure 2: Topography of knowledge transaction activities](../p577fig1.jpg)

<figcaption>

Figure 2: Topography of knowledge transaction activities (source: Cowan _et al._ 2000)</figcaption>

</figure>

This leaves only the _unarticulated_ branch. One of Cowan _et al_.'s primary concerns was to address the almost mythical status that tacit knowledge had acquired. Their objectives were, first, to explore the option of the codification of tacit knowledge more deeply, and secondly to highlight what they saw as the widespread error of assuming that un-codified knowledge was equivalent to tacit knowledge.

For Cowan _et al_., the unarticulated branch represents a situation where no codebook is apparent. They split this into two sub-cases where,

> 'In one, knowledge is tacit in the normal sense - it has not been recorded either in word or artifact, so no codebook exists. In the other, knowledge may have been recorded, so a codebook exists, but this book may not be referred to by members of the group - or, if it is, references are so rare as to be indiscernible to an outside observer.' ([Cowan _et al_. 2000](#cow00): 231)

Thus, Cowan _et al_. open up a new possibility, one where knowledge has been recorded and so has, at some point, been codified, but where the codebook has been lost so that to a casual observer it appears as if no codebook exists. They cite as an example a group of scientific researchers who work on some topic that is already highly codified but the members of the group know the codebook so well that it has become internalised, '_paradoxically, its existence and contents are matters left tacit among the group unless some dispute or memory problem arises_' ([Cowan _et al_. 2000](#cow00): 323). As Winograd and Flores ([1986](#win86): 149) put it, it has become lost in the depths of obviousness. They argue that many of the studies that show the crucial role of tacit knowledge are in fact examples of this, when researchers have '_placed undue reliance upon an overly casual observational test_' ([Cowan _et al_. 2000](#cow00): 237).

The other branch of this tree, where no codebook exists, poses a different problem. Remember that we are dealing with a situation where, in principle, knowledge is articulable. Cowan _et al_. suggest that this situation can be dealt with by a three-way split, where which branch is followed hinges on the presence or absence of disputes.

The first branch represents a situation where there is _agreement_ about what is being done and the reasons for doing it. Consequently, no disputes occur that might trigger the demand for a more clearly stated set of goals or procedures and the group functions on the basis of a tacit agreement. Cowan _et al_. claim that it is common to find examples of this situation in large organizations.

The remaining two branches occur when there is a dispute and no codebook exists to resolve it; the difference between them hinges on how the dispute is settled. The branch labelled _authority_ concerns the situation where there is some form of procedural authority that can arbitrate between the contending parties and create a suitable codebook. An example of this might be where there is an umbrella-organization or association that can act as a broker between the disputing parties ([Kimble _et al_. 2010](#kim10)). The second branch concerns the situation where _no authority_ exists. Here the basis for the resolution of disputes is more tricky. Cowan _et al_. suggest that the resolution to this type of dispute often relies on, '_personal (and organizational) gurus of one shape or another, including the "new age" cult leaders in whom procedural and personal authority over the conduct of group affairs are fused_' ([Cowan _et al_. 2000](#cow00): 234-235).

## To codify or not?

While it is certainly possible to argue with points of detail and interpretation in Cowan _et al_.'s topography, it does offer a pragmatic approach to answering the question about what should be codified.

> _Any individual or group of agents makes decisions about what kind of knowledge activity to pursue and how it will be carried on. Should the output be codified or remain uncodified? … For an economist, there is a simple one-line answer: the choices will depend on the perceived costs and benefits_. ([Cowan _et al_. 2000](#cow00): 231)

As indicated previously, for an economist, this means focusing exclusively on knowledge that is capable of being codified and converted into information. Below we briefly outline what might be expected from this type of cost-benefit analysis before expanding the discussion to look at the issue of codification from other perspectives. In particular, we will examine the relevance of the cost-benefit approach in the light of the different views of tacit knowledge examined in the section on contrasting concepts of knowledge.

### The benefits of codification

For the most part, economists tend to treat the benefits of codification as a given, although the argument of the low or negligible marginal costs associated with transmission of information is implicit in much of the work. Cohendet and Steinmueller ([2000](#coh00): 202-203) list four benefits of codification.

*   It gives information the properties of a standard commodity in that it can be bought and sold and used to signal the desirability of entering into some form of transaction.
*   It makes it possible to exploit some of the non-standard features of information as a commodity, such as its _stock_ not being reduced by its sale and its low marginal cost of reproduction.
*   It allows the _modularization_ of bodies of knowledge, which facilitates specialisation and allows firms to acquire knowledge at a fixed cost, which in turn facilitates the outsourcing of activities.
*   It directly affects knowledge creation, innovation and economic growth, which has the potential to alter the rate and direction of knowledge generation and distribution dramatically.

Cowan _et al_.'s discussion of the benefits of codification are equally sparse. Their principle contribution being to outline five situations where they claim codification would be particularly beneficial. These are in activities that involve the transfer, recombination, description, memorisation or adaptation of knowledge ([Cowan _et al_. 2000: 243-245](#cow00)).

Similar views on capturing knowledge can be found in the early work on knowledge management, such as Davenport _et al_.'s ([1998](#dav98)) call for a 'yellow pages' database of expertise or O'Dell and Grayson's ([1998](#ode98)) call for the identification and transfer of best practices. The work on technology-based organizational memories ([Walsh and Ungson 1991](#wal91); [Stein and Zwass 1995](#ste95)) is also based on the '_knowing what we know_' approach to knowledge management. On a slightly different tack, DeLong ([2004](#del04)), urges companies to find ways to capture and store the knowledge of their employees to deal with the threat of an ageing workforce. However, in each case, the benefit rests on the same argument: that if knowledge is of value to a company, it should be captured, stored and shared, and codification represents the most cost-effective way to do this.

### The costs of codification

While Cowan _et al_. may have little to say about the benefits of codification, their topography does provide a structure to examine its costs. Perhaps the most obvious cost associated with codification is that of creating the codebook.

If no codebook exists, then one will need to be created; this can involve substantial fixed costs. First, some form of conceptual framework, together with the language needed to describe it, has to be developed. Once this has been done, work can start on producing the codebook itself. The costs of this can be particularly high where there are significant disagreements or differences of opinion.

In the case of an existing codebook, be it either lost or explicit, the costs of producing the codebook are, to a greater or lesser extent, already sunk and codification can be achieved at a much lower marginal cost. Pre-existing standards and a vocabulary of well-defined and commonly understood terms should greatly reduce the time and effort required to produce the final codebook.

However, in both of these cases, the costs of codification do not stop with the production of the codebook. Codifying a new piece of knowledge will add to the content of the existing codebook and will also draw upon those contents. A potentially complex, self-referential situation is created which needs to be managed. This problem can be particularly severe when dealing with a new activity where the contents of the codebook have not yet stabilised. Even with well-established activities, adding new material will introduce new concepts and terminology. Consequently, the cost of maintaining the integrity of an existing codebook will inevitably form part of the on-going costs of codification ([Kimble 2013](#kim13)).

Both Cohendet and Steinmueller ([2000](#coh00)) and Cowan _et al_. ([2000](#cow00)) note the influence that the mechanisms that underlie the construction of a shared context can have on the cost of maintaining a codebook. Cohendet and Steinmueller ([2000](#coh00)) suggest that if a situation is relatively stable, for example producing declarative propositions for some physical process, then investments in a codebook should show increasing returns. If on the other hand, the creation of a shared context relies on constantly shifting social interactions, such as those associated with the building of organizational skills and competencies, then the return on investment is more likely to be constant than increasing.

### The desirability of codification

Having looked at the costs and benefits, we now return to the original question, to codify or not to codify? The focus provided by economists ([Ancori _et al_. 2000](#anc00); [Cohendet and Steinmueller 2000](#coh00); [Cowan _et al_. 2000](#cow00)) tends to be exclusively on codified knowledge and carries with it the implicit assumption that by comparing costs and benefits it will be possible to establish an 'optimum degree of codification'. However, this type of analysis falls short in two areas.

First, it does not consider the strategic implications of codification; secondly, and perhaps more significantly, it places an undue emphasis on the idea of knowledge as a dichotomy. Effectively, it assumes that knowledge can be divided into what is often termed tacit (as yet uncodified) and explicit (already codified) knowledge, and that all that is needed is to decide where the line between the two should be drawn.

Dealing first with the strategic issues, perhaps the most obvious risk is that of making something whose value is based on its scarcity and inimitability easier to share and reproduce. For example, Grant ([1996a](#gra96a)) claims that,

> Sustainability of competitive advantage therefore requires resources which are idiosyncratic (and therefore scarce), and not easily transferable or replicable. These criteria point to knowledge (tacit knowledge in particular) as the most strategically-important resource which firms possess. ([Grant 1996a](#gra96a): 376)

However Schultz and Stabell among others, highlight the apparent contradiction that,

> arises out of knowledge management researchers' recommendations that, in order to manage tacit knowledge, it must be made explicit (e.g., Nonaka and Takeuchi, 1995). However, once the tacit knowledge is explicit, it can be imitated, implying a potential loss of competitive advantage. ([Schultze and Stabell 2004](#sch04): 551)

We will not examine the risks of codification in any more detail here as they have been the subject of discussion elsewhere. Even Cowan _et al_. note that codified knowledge can be a potent carrier of history and that excessive codification can lock an organization into obsolete conceptual schemas. Instead, we will focus on the incommensurability of the cost-benefits approach with the view of knowledge as a duality and ask a more fundamental question, is it even possible to draw a line between what should and what should not be codified?

The assumption that underlies much of the preceding discussion is that there is a choice between what can be rendered explicit and what should be left as tacit. However, most of the tacit knowledge that Grant and others refer to is rooted in undocumented 'know how' embedded in the web of relationships that go to make up the firm. Can we treat this type of knowledge in that way?

Many of the examples that Polanyi referred to involved some form of skill or competency where the rules for performing the activity have been pushed into the unconscious, leaving the conscious mind free to focus on the performance itself. For example, a pianist does not have to think about the layout of the piano, or how to use the pedals, and is free to concentrate on reading the music and its interpretation. It is only when the tacit dimension to knowledge, which concerns how to operate a piano, and the explicit dimension, which concerns how to read music, are brought together that the performance is complete. If this point of view of knowledge as a duality is accepted, then it becomes impossible to deal with the two things independently.

Even if this argument is rejected and, following Cowan _et al_.'s logic, it were possible to extract certain aspects of this form of tacit knowledge and codify them, would this be desirable, or even practical? First, there is every likelihood that the remnants of uncodified, unobserved tacit knowledge would be lost and forgotten. For example, Brown and Duguid ([1991](#bro91)) explain how Xerox's early attempts at knowledge management focused on abstract canonical knowledge, the way the company thought the work should be done, and ignored the value of practice-based, non-canonical knowledge, which later turned out to be of vital importance. Similar examples of potentially valuable knowledge being ignored or undervalued can be found elsewhere ([Alvesson and Karreman 2001](#alv01); [McKinlay 2002](#mck02); [Thompson 2005](#tho05)).

Secondly, the costs of attempting to maintain a codebook for such knowledge would probably prove to be prohibitive. As we noted earlier, in the section on knowledge at the level of groups and communities, communities often create their own private language which not only serves as a useful shorthand for members of the community but can also act as boundary between it and the outside world; in such cases, knowledge really does represent power ([Teigland and Wasko 2003](#tei03); [Kimble _et al_. 2010](#kim10)). Creating a codebook in such circumstances is likely to prove costly, as it will inevitably mean chasing a moving target. As Marshall and Brady ([2001](#mar01)) observe in relation to attempts to specify precise and unambiguous meanings for natural languages, '_Linguistic meaning is never complete and final… It is unstable and open to potentially infinite interpretation and reinterpretation in an unending play of substitution_' ([Marshall and Brady 2001](#mar01): 101).

## Conclusions

The goal of this article was to return to knowledge management and the problem of the management of tacit knowledge in knowledge management. In contrast to many of the articles that tackle this topic, we approached it, not from the viewpoint of what is it that is special about tacit knowledge that means it cannot be codified, but from the viewpoint of the sceptical economists ([Cowan _et al_. 2000](#cow00)) who ask what is it about tacit knowledge that can not be codified?

In taking this approach, we have returned to the theoretical basis for the views of knowledge that appear in the knowledge management literature, and have highlighted the philosophical positions that underpin them. Armed with this knowledge we have critically examined Cowan _et al_.'s analysis of what they believe is open to codification and drawn attention to its shortcomings.

In this final section we will consider what this means for knowledge management.

### Knowledge management or information management?

One of the frequent and potent criticisms levelled at knowledge management is that it is really no more than information management re-badged ([Wilson 2002](#wil02)). For example, Offsey ([1997](#off97)) notes that, '_what many software vendors tout as knowledge management systems are only existing information retrieval engines, groupware systems or document management systems with a new marketing tagline_' ([Offsey 1997](#off97): 113).

For those who take the view of the sceptical economists, this is not criticism at all, as the goal of codifying and storing knowledge is to turn it into information. Perhaps Cowan _et al_.'s attempts to demystify tacit knowledge have some value here by throwing a more honest, and possibly more realistic, light upon what is often termed knowledge management. If we can be clearer about what we mean when we talk about capturing and storing knowledge, we may be one step closer to a more realistic expectation about what knowledge management and knowledge management systems can do.

### Misjudging the costs and benefits of codification

The history of knowledge management systems, particularly large scale knowledge management projects, contains a number of examples of cost overruns and failure to achieve the expected benefits ([Walsham 2001](#wal01)). What is it then that lies at the root of the over-optimistic assumptions that are made about knowledge management?

Clearly, part of the initial growth of knowledge management was driven by commercial gain. Kay, for example describes the '_vendor feeding frenzy_' ([Kay 2003](#kay03): 683), which developed as software vendors attempted to exploit the new and potentially lucrative market for knowledge management. However, the rationale that underpins these assumptions is probably best expressed in terms of Figure 1.

If knowledge can be separated into independent tacit and explicit components, and if these can be specified and constrained to act in such a way that they fulfil the requirements of Cowan _et al_.'s codebook, then the idea of increasing returns through the cumulative growth of knowledge that comes from the union of individual knowledge sets becomes plausible. However, in most cases, the greatest benefits to an organization come from the construction of a shared cognitive common ground between human agents. As we have seen, in this case, the return on investment is more likely to be constant because the growth of knowledge comes from the intersection of sets of individual knowledge. Although attractive, the idea of a cumulative growth of knowledge, be it Nonaka's '_amplification_' of knowledge ([Nonaka 1994](#non94)) or Ackoff's progression from data to wisdom ([Ackoff 1989](#ack89)), is most often likely to prove a fallacy.

If there is a tendency to overestimate the benefits of knowledge management, what then of the costs? As indicated in the preceding discussions, the costs of building and maintaining a codebook, particularly when dealing with a novel or fluid situation, can be high. There are several studies that provide examples of knowledge management systems that are developed, at some cost, and then either fully or partially fall into disuse ([Vaast 2007](#vaa07); [Ali _et al_. 2012](#ali12)). The problems of extensibility and scalability due to continual modifications to the internal content and structure of such systems remain a significant technological challenge. The field of ontological engineering in artificial intelligence claims to offer one solution to this, but so far there are few working real world examples to support this claim. The cost of managing and maintaining systems based on codified knowledge remains significant and recurring ([Kimble 2013](#kim13)).

### Codification and knowledge as a duality

We have argued that it is more accurate, and perhaps more legitimate, to describe knowledge management systems as information management systems and that, by doing so, this might lead to a more realistic expectation of what such systems can achieve. What else might our analysis contribute to our understanding of knowledge management and the management of tacit knowledge?

Cowan _et al_.'s topography is helpful because it focuses on the issue of codification and avoids the _not codified_ = _tacit_ = _uncodifiable_ trap. It is also useful for providing a framework to help us think about the costs and benefits of codification. However, like Nonaka's SECI model, it runs the risk of allowing Polanyi's distinction between the tacit and explicit dimensions of knowledge to segue into a dichotomy, where knowledge is either tacit or explicit, and from there, to lapse into thinking of knowledge as an object that can be stored and manipulated.

Codification is a valid approach to knowledge management, but it is not universally applicable. As we noted in the section on the desirability of codification, it does not sit well when dealing with the sorts of tacit knowledge that are involved in skills and competencies. In such circumstances, it simply does not make sense to attempt to separate tacit and explicit and deal with them separately, because both are needed to complete the picture. Similarly, we noted that the effect of codifying knowledge can lead to knowledge that has not been codified being discounted as '_not very interesting_' ([Cowan _et al_. 2000](#cow00): 230) and lost. The point of knowledge management, at least in so far as it is used to further a set of organizational goals, is to leverage knowledge; separating out, codifying and storing certain aspects of knowledge runs the risk of doing the opposite and producing people who can only follow instructions ([Brown and Duguid 1991](#bro91); [Talbott 1999](#tal99)).

Robert Skidelsky, in his biography of another economist, John Maynard Keynes, quotes Keynes as telling his Cambridge students in 1933 that '_when you adopt perfectly precise language, you are trying to express yourself for the benefit of those who are incapable of thought_' ([Skidelsky 2000](#ski00)). By focusing exclusively on codified knowledge, the advocates of codification may lose sight of the intimate linkages between tacit and explicit knowledge. To do so runs the risk of creating organizations staffed by people that knowledge management has made, in Keynes words, '_incapable of thought_'.

## About the author

Chris Kimble is an Associate Professor of Strategy and Technology Management at Euromed Management Marseille, is affiliated to MRM-CREGOR at Universite´ Montpellier II, and is the Academic Editor for the journal Global Business and Organizational Excellence. Before moving to France, he lectured in the UK on information systems and management at the University of York, and information technology at the University of Newcastle. His research interests are business strategy and the management of the fit between the digital and social worlds. Chris Kimble is the corresponding author and can be contacted at: [chris.kimble@euromed-management.com](mailto:chris.kimble@euromed-management.com)

## References

<ul>
    <li id="ack89">Ackoff, R.L. (1989). From data to wisdom. <em>Journal of Applied Systems Analysis</em>, <strong>16</strong>(1), 3-9.
    </li>
    <li id="ali12">Ali, N.A., Whiddett, D., Tretiakov, A. &amp; Hunter, I. (2012). The use of information technologies for knowledge sharing by secondary healthcare organisations in New Zealand. <em>International Journal of Medical Informatics</em>, <strong>81</strong>(7), 500-506.
    </li>
    <li id="alv01">Alvesson, M. &amp; Karreman, D. (2001). Odd couple: making sense of the curious concept of knowledge management. <em>Journal of Management Studies</em>, <strong>38</strong>(7), 995-1018.
    </li>
    <li id="anc00">Ancori, B., Bureth, A. &amp; Cohendet, P. (2000). The economics of knowledge: the debate about codification and tacit knowledge. <em>Industrial and Corporate Change</em>, <strong>9</strong>(2), 255-287.
    </li>
    <li id="bal07">Balconi, M., Pozzali, A. &amp; Viale, R. (2007). The 'codification debate' revisited: a conceptual framework to analyze the role of tacit knowledge in economics. <em>Industrial and Corporate Change</em>, <strong>16</strong>(5), 823-849.
    </li>
    <li id="bar86">Barley, S.R. (1986). Technology as an occasion for structuring: evidence from observations of CT scanners and the social order of radiology departments. <em>Administrative Science Quarterly</em>, <strong>31</strong>(1), 78-108.
    </li>
    <li id="bar91">Barney, J. (1991). Firm resources and sustained competitive advantage. <em>Journal of Management</em>, <strong>17</strong>(1), 99-120.
    </li>
    <li id="ber66">Berger, P.L. &amp; Luckmann, T. (1966). <em>The social construction of reality: a treatise in the sociology of knowledge</em>. New York, NY: Doubleday.
    </li>
    <li id="bro91">Brown, J.S. &amp; Duguid, P. (1991). Organizational learning and communities of practice: toward a unified view of working, learning, and innovation. <em>Organization Science</em>, <strong>2</strong>(1), 40-57.
    </li>
    <li id="car02">Carlile, P.R. (2002). A pragmatic view of knowledge and boundaries: boundary objects in new product development. <em>Organization Science</em>, <strong>13</strong>(4), 442-455.
    </li>
    <li id="cha09">Chalmers, D. (2009). Ontological anti-realism. In D. Chalmers, D. Manley &amp; R. Wasserman (Eds.), <em>Metametaphysics: new essays on the foundations of ontology</em> (pp. 77-129). Oxford: Oxford University Press.
    </li>
    <li id="coh00">Cohendet, P. &amp; Steinmueller, E.W. (2000). The codification of knowledge: a conceptual and empirical exploration. <em>Industrial and Corporate Change</em>, <strong>9</strong>(2), 195-209.
    </li>
    <li id="con96">Conner, K.R. &amp; Prahalad, C.K. (1996). A resource-based theory of the firm: knowledge versus opportunism. <em>Organization Science</em>, <strong>7</strong>(5), 477-501.
    </li>
    <li id="coo99">Cook, S.D.N. &amp; Brown, J.S. (1999). Bridging epistemologies: the generative dance between organizational knowledge and organizational knowing. <em>Organization Science</em>, <strong>10</strong>(4), 381-400.
    </li>
    <li id="cow00">Cowan, R., David, P.A. &amp; Foray, D. (2000). The explicit economics of knowledge codification and tacitness. <em>Industrial and Corporate Change</em>, <strong>9</strong>(2), 211-253.
    </li>
    <li id="cre01">Creplet, F., Dupouet, O., Kern, F., Mehmanpazir, B. &amp; Munier, F. (2001). Consultants and experts in management consulting firms. <em>Research Policy</em>, <strong>30</strong>(9), 1517-1535.
    </li>
    <li id="daf86">Daft, R.L. &amp; Lengel, R.H. (1986). Organizational information requirements, media richness and structural design. <em>Management Science</em>, <strong>32</strong>(5), 554-571.
    </li>
    <li id="dav98">Davenport, T.H., DeLong, D.W. &amp; Beers, M.C. (1998). Successful knowledge management projects. <em>Sloan Management Review</em>, <strong>39</strong>(2), 43-57.
    </li>
    <li id="del04">DeLong, D.W. (2004). <em>Lost knowledge: confronting the threat of an aging workforce</em>. Oxford: Oxford University Press.
    </li>
    <li id="dug05">Duguid, P. (2005). The art of knowing: social and tacit dimensions of knowledge and the limits of the community of practice. <em>The Information Society</em>, <strong>21</strong>(2), 109-118.
    </li>
    <li id="eas08">Easterby-Smith, M. &amp; Prieto, I.M. (2008). Dynamic capabilities and knowledge management: an integrative role for learning? <em>British Journal of Management</em>, <strong>19</strong>(3), 235-249.
    </li>
    <li id="feg08">Feghali, T. &amp; El-Den, J. (2008). Knowledge transformation among virtually-cooperating group members. <em>Journal of Knowledge Management</em>, <strong>12</strong>(1), 92-105.
    </li>
    <li id="get63">Gettier, E.L. (1963). Is justified true belief knowledge? <em>Analysis</em>, <strong>23</strong>(6), 121-123.
    </li>
    <li id="gou06a">Gourlay, S. (2006a). Conceptualizing knowledge creation: a critique of Nonaka's theory. <em>Journal of Management Studies</em>, <strong>43</strong>(7), 1415-1436.
    </li>
    <li id="gou06b">Gourlay, S. (2006b). Towards conceptual clarity for tacit knowledge: a review of empirical studies. <em>Knowledge Management Research &amp; Practice</em>, <strong>4</strong>(1), 60-69.
    </li>
    <li id="gra96a">Grant, R.M. (1996a). Prospering in dynamically-competitive environments: organizational capability as knowledge integration. <em>Organization Science</em>, <strong>7</strong>(4), 375-387.
    </li>
    <li id="gra96b">Grant, R.M. (1996b). Toward a knowledge-based theory of the firm. <em>Strategic Management Journal</em>, <strong>17</strong>(Special Issue), 109-122.
    </li>
    <li id="hak10">Håkanson, L. (2010). The firm as an epistemic community: the knowledge-based view revisited. <em>Industrial and Corporate Change</em>, <strong>19</strong>(6), 1801-1828.
    </li>
    <li id="hil02">Hildreth, P.M. &amp; Kimble, C. (2002). <a href="http://www.webcitation.org/6FeZnf9Pu">The duality of knowledge</a>. <em>Information Research</em>, <strong>8</strong>(1). Retrieved 12 October, 2012 from http://informationr.net/ir/8-1/paper142.html. (Archived by Webcite at http://www.webcitation.org/6FeZnf9Pu)
    </li>
    <li id="ipe03">Ipe, M. (2003). Knowledge sharing in organizations: a conceptual framework. <em>Human Resource Development Review</em>, <strong>2</strong>(4), 337-359.
    </li>
    <li id="ive02">Iverson, J.O. &amp; Mcphee, R. D. (2002). Knowledge management in communities of practice. <em>Management Communication Quarterly</em>, <strong>16</strong>(2), 259-266.
    </li>
    <li id="kay03">Kay, A.S. (2003). The curious success of knowledge management. In C. W. Holsapple (Ed.), <em>Handbook on knowledge management</em> (Vol. 2, pp. 679-681). Berlin : Springer.
    </li>
    <li id="kim13">Kimble, C. (2013). What cost knowledge management? The example of Infosys. <em>Global Business and Organizational Excellence</em>, <strong>32</strong>(3), 6-14.
    </li>
    <li id="kim10">Kimble, C., Grenier, C. &amp; Goglio-Primard, K. (2010). Innovation and knowledge sharing across professional boundaries: political interplay between boundary objects and brokers. <em>International Journal of Information Management</em>, <strong>30</strong>(5), 437-444.
    </li>
    <li id="kog92">Kogut, B. &amp; Zander, U. (1992). Knowledge of the firm, combinative capabilities, and the replication of technology. <em>Organization Science</em>, <strong>3</strong>(3), 383-397.
    </li>
    <li id="kog96">Kogut, B. &amp; Zander, U. (1996). What firms do? Coordination, identity, and learning. <em>Organization Science</em>, <strong>7</strong>(5), 502-518.
    </li>
    <li id="kuh62">Kuhn, T.S. (1962). <em>The structure of scientific revolutions</em>. Chicago, IL: Chicago University Press.
    </li>
    <li id="lav91">Lave, J. &amp; Wenger, E. (1991). <em>Situated learning: legitimate peripheral participation</em>. Cambridge: Cambridge University Press.
    </li>
    <li id="leo98">Leonard, D. &amp; Sensiper, S. (1998). The role of tacit knowledge in group innovation. <em>California Management Review</em>, <strong>40</strong>(3), 112-131.
    </li>
    <li id="les01">Lesser, E.L. &amp; Storck, J. (2001). Communities of practice and organizational performance. <em>IBM Systems Journal</em>, <strong>40</strong>(4), 831-841.
    </li>
    <li id="mck02">McKinlay, A. (2002). The limits of knowledge management. <em>New Technology, Work and Employment</em>, <strong>17</strong>(2), 76-88.
    </li>
    <li id="mar01">Marshall, N. &amp; Brady, T. (2001). Knowledge management and the politics of knowledge: illustrations from complex products and systems. <em>European Journal of Information Systems</em>, <strong>10</strong>, 99-112.
    </li>
    <li id="mil02">Miller, F.J. (2002). <a href="http://www.webcitation.org/6FeZtDqhD">I = 0 (information has no intrinsic meaning)</a>. <em>Information Research</em>, <strong>8</strong>(1). Retrieved 12 October, 2012 from http://informationr.net/ir/8-1/paper140.html (Archived by Webcite at http://www.webcitation.org/6FeZtDqhD).
    </li>
    <li id="mur08">Murillo, E. (2008). <a href="http://www.webcitation.org/6FeZdSXtM">Searching Usenet for virtual communities of practice: using mixed methods to identify the constructs of Wenger's theory</a>. <em>Information Research</em>, <strong>13</strong>(4). Retrieved 31 January, 2013 from http://informationr.net/ir/13-4/paper386.html (Archived by Webcite at http://www.webcitation.org/6FeZdSXtM).
    </li>
    <li id="nel82">Nelson, R.R. &amp; Winter, S. G. (1982). <em>An evolutionary theory of economic change</em>. Cambridge, MA: Harvard University Press.
    </li>
    <li id="non91">Nonaka, I. (1991). The knowledge-creating company. <em>Harvard Business Review</em>, <strong>69</strong>(6), 96-104.
    </li>
    <li id="non94">Nonaka, I. (1994). A dynamic theory of organizational knowledge creation. <em>Organization Science</em>, <strong>5</strong>(1), 14-37.
    </li>
    <li id="non95">Nonaka, I. &amp; Takeuchi, H. (1995). <em>The knowledge-creating company: how Japanese companies create the dynamics of innovation</em>. Oxford: Oxford University Press.
    </li>
    <li id="non09">Nonaka, I. &amp; von Krogh, G. (2009). Perspective - tacit knowledge and knowledge conversion: controversy and advancement in organizational knowledge creation theory. <em>Organization Science</em>, <strong>20</strong>(3), 635-652.
    </li>
    <li id="ode98">O'Dell, C. &amp; Grayson, C. J. (1998). If only we knew what we know: identification and transfer of internal best practices. <em>California Management Review</em>, <strong>40</strong>(3), 154-174.
    </li>
    <li id="off97">Offsey, S. (1997). Knowledge management: linking people to knowledge for bottom line results. <em>Journal of Knowledge Management</em>, <strong>1</strong>(2), 113-122.
    </li>
    <li id="pol66">Polanyi, M. (1966). <em>The tacit dimension</em>. London: Routledge and Kegan Paul.
    </li>
    <li id="pol69">Polanyi, M. (1969). <em>Knowing and being</em>. Chicago, IL: University of Chicago Press.
    </li>
    <li id="pop59">Popper, K.R. (1959). <em>The logic of scientific discovery</em>. London: Hutchinson.
    </li>
    <li id="sch04">Schultze, U. &amp; Stabell, C. (2004). Knowing what you don't know? Discourses and contradictions in knowledge management research. <em>Journal of Management Studies</em>, <strong>41</strong>(4), 549-573.
    </li>
    <li id="sha48">Shannon, C.E. (1948). <em>A mathematical theory of communication. (No. B-1598)</em>. New York, NY: The American Telephone and Telegraph Company.
    </li>
    <li id="sha56">Shannon, C.E. (1956). The bandwagon (editorial). <em>IEEE Transactions on Information Theory</em>, <strong>2</strong>(1), 3.
    </li>
    <li id="sha49">Shannon, C.E. &amp; Weaver, W. (1949). <em>The mathematical theory of communication</em>. Urbana, IL: The University of Illinois Press.
    </li>
    <li id="ski00">Skidelsky, R. (2000). <a href="http://www.webcitation.org/6FeZj2A62">Ideas and the world</a>. <em>The Economist</em>. Retrieved 13 October, 2012 from http://www.economist.com/node/431717 (Archived by Webcite at http://www.webcitation.org/6FeZj2A62)
    </li>
    <li id="spe96">Spender, J.C. (1996). Making knowledge the basis of a dynamic theory of the firm. <em>Strategic Management Journal</em>, <strong>17</strong>, 45-62.
    </li>
    <li id="ste95">Stein, E.W. &amp; Zwass, V. (1995). Actualizing organizational memory with information systems. <em>Information Systems Research</em>, <strong>6</strong>(2), 85-117.
    </li>
    <li id="swa11">Swart, J. &amp; Harvey, P. (2011). Identifying knowledge boundaries: the case of networked projects. <em>Journal of Knowledge Management</em>, <strong>15</strong>(5), 703-721.
    </li>
    <li id="tal99">Talbott, S.L. (1999). <a href="http://www.webcitation.org/6FeZzyzH9">The great knowledge implosion</a>. <em>NetFuture</em>(84). Retrieved 15 October, 2012 from http://www.netfuture.org/1999/Feb0999_84.html#4 (Archived by Webcite at http://www.webcitation.org/6FeZzyzH9).
    </li>
    <li id="tei03">Teigland, R. &amp; Wasko, M.M. (2003). Integrating knowledge through information trading: examining the relationship between boundary spanning communication and individual performance. <em>Decision Sciences</em>, <strong>34</strong>(2), 261-286.
    </li>
    <li id="tho05">Thompson, M. (2005). Structural and epistemic parameters in communities of practice. <em>Organization Science</em>, <strong>16</strong>(2), 151-164.
    </li>
    <li id="vaa07">Vaast, E. (2007). What goes online comes offline: knowledge management system use in a soft bureaucracy. <em>Organization Studies</em>, <strong>28</strong>(3), 283-306.
    </li>
    <li id="wak95">Walsh, J.P. (1995). Managerial and organizational cognition: notes from a trip down memory lane. <em>Organization Science</em>, <strong>6</strong>(3), 280-321.
    </li>
    <li id="wal91">Walsh, J.P. &amp; Ungson, G. R. (1991). Organizational memory. Academy of management review, <strong>16</strong>(1), 57-91.
    </li>
    <li id="wal01">Walsham, G. (2001). Knowledge management: the benefits and limitations of computer systems. <em>European Management Journal</em>, <strong>19</strong>(6), 599-608.
    </li>
    <li id="wen96">Wenger, E. (1996). Communities of practice: the social fabric of a learning organization. <em>Healthcare Forum Journal</em>, <strong>39</strong>(4), 20-24.
    </li>
    <li id="wil02">Wilson, T.D. (2002). <a href="http://www.webcitation.org/6Fea4TacK">The nonsense of 'knowledge management'</a>. <em>Information Research</em>, <strong>8</strong>(1). Retrieved 16 October, 2012 from http://informationr.net/ir/8-1/paper144.html (Archived by Webcite at http://www.webcitation.org/6Fea4TacK).
    </li>
    <li id="win86">Winograd, T. &amp; Flores, F. (1986). <em>Understanding computer and cognition: a new foundation for design</em>. Norwood, NJ: Ablex Publishing.
    </li>
</ul>

</section>

</article>