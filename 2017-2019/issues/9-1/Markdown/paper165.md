#### Vol. 9 No. 1, October 2003

# Five personality dimensions and their influence on information behaviour

#### [Jannica Heinström](mailto:jheinstr@abo.fi)  
Department of Social and Political Sciences/Information Studies  
Åbo Akademi University  
Tavastgatan 13  
FIN-20500 Åbo  
Finland

#### **Abstract**

> This article emphasize the importance of considering psychological mechanisms for a thorough understanding of users of information services. The focal point is the relation between personality and information seeking which is explored through a quantitative analysis of 305 university students' personality traits and information habits. It is shown that information behaviour could be connected to all the personality dimensions tested in the study - neuroticism, extraversion, openness to experience, competitiveness and conscientiousness. Possible explanations for these relations are discussed. It is concluded that inner traits interact with contextual factors in their final impact on information behaviour.

## Introduction

During the last decades we have seen a growing demand on the capacity to handle information. It is encouraged to become an information literate life-long learner in order to meet the requirements of the fast-paced society. At a certain extent this is something which can be learnt. It is, however, plausible that certain persons match these requirements and adapt to the changing demands with less effort than others. There is certainly not one single personality type which would form the "ideal" information literate citizen. Quite the contrary, different traits may prove useful in different situations. A comprehension of how different traits come into play in information seeking would increase the understanding of users of information services. The aim of this article is to reflect over individual differences in information behaviour with a particular focus on how and why personality traits influence information strategies.

Information seeking has often been compared to a rational problem-solving process, where a gap in knowledge triggers a conscious search for information. This may apply to some situations, but in most cases the information-seeking process is dynamic and changeable. It is dependent on the context and to a large extent on the individual performing it ([Solomon, 2002](#sol02)). Some people may plan and structure their searches, while others gather information in a more flexible and spontaneous fashion. The reasons behind different information approaches may lie in the context, but also be due to the person's inner processes and needs.

The research tradition within LIS (Library and Information Science) has over the last years increasingly focused on users' search behaviour. Within this tradition a particular emphasis has been on the context of information seeking ([Solomon, 2002](#sol02)). It has been acknowledged that the information-seeking process is dependent on task (e.g., [Byström, 2000](#bys00)), discipline (e.g., [Ocholla, 1999](#och99)) or stage of the research process (e.g., [Kuhlthau, 1993](#kuh93)). This research tradition, where the individual is studied as part of the context, has provided a valuable understanding of groups of users in a sociological sense. In order to gain a full understanding of information behaviour, it would be vital to further extend the focus to the user's psychological processes. Allen & Kim ( [2001](#all01)) have highlighted the importance of accounting for both the context and the individual characteristics, as search behaviour is likely to evolve through interaction between the two. Understanding of psychological characteristics can shed light on both variability and patterns in information seeking ( [Wilson, 2000](#wil00)).

One important psychological mechanism which guides behaviour is personality. Everyone has their unique pattern of feelings, thoughts and behaviours, which is formed by a fairly stable combination of personality traits ([Phares, 1991](#pha91)). As personality forms an inclination towards certain characteristic reactions in any given situation, personality traits are likely to influence attitudes and behaviour also in an information-seeking context.

## Psychological factors in information-seeking behaviour

Many psychological mechanisms come to work also in a seemly rational process as information seeking. Wilson's model of information behaviour shows how psychological, demographical, role-related, interpersonal, environmental and source-related characteristics influence the information-seeking process. The decision to seek information is dependent on motivation which may have a cognitive origin or be emotionally based as in the need to reinforce previous values. Before the relevant information is retrieved the searchers must overcome possible barriers, which sometimes are psychological. They must experience the situation as rewarding enough and themselves as competent enough to actually take the final decision to seek information ([Wilson, 1981](#wil81); [Wilson & Walsh, 1996](#wil96)).

The cognitive tradition within LIS has been the traditional stronghold for research with a particular focus on the individual. This tradition emphasises cognitive processes such as thinking, perception, memory, recognition, learning and problem solving ([Ingwersen, 1996](#ing96)). Our earlier knowledge structure influence the way we receive and understand new information. The same piece of information is consequently received differently dependent on the person's pre-understanding of the topic.

Discrepancies between persons perception of knowledge and their actual knowledge are not unusual ([Radecki & Jaccard, 1995](#rad95)). Personal comprehension can be underestimated, perhaps without reason, if compared to knowledgeable peers or a highly regarded authority. On the other hand, the own capacity can also be overestimated by a strong identification with peers and accordingly their knowledge. People with a tendency to overestimate their own knowledge run the risk of seeking too little information and, as a consequence, make their decisions on insufficient grounds ([Radecki & Jaccard, 1995](#rad95)).

Motivation and interest influence the way information is used and critically evaluated (Limberg, 1998). The more interested we are in the topic, the more information we seek about it. Since people have a limited capacity for assimilating new information, particular attention is paid to information which can be related to previous knowledge. The personal frame of reference forms a filter that picks out familiar information ( [Leckie & Pettigrew, 1997](#lec97)). Information that confirms our previous values are particularly welcome and facts that do not correlate with our own views are often ignored ([Radecki & Jaccard, 1995](#rad95)). Information sources likewise tend to be chosen on base of familiarity rather than potential usefulness. This also applies to information professionals like librarians ([Ingwersen, 1982](#ing82)).

Cognitive styles is another aspect of knowledge creation with an influence on information behaviour. Individual differences related to cognitive styles and study approaches come into play in database searches, on the Internet and in virtual environments ([Ford, 2000](#for00); [Ford _et al._, 2001](#for01) ; [Kim, 2001](#kim01)). Students with a holistic learning style are, for instance, more explorative in their searches, while their serialist counterparts build their searches in a narrower stepwise fashion ([Ford _et al._, 2002](#for02)).

In order to get a complete picture of information behaviour, the consideration of affective and conative elements are important in addition to cognitive ones ([Solomon, 1997](#sol97)). The cognitive uncertainty in relation to unfamiliar situations or problems arises as a consequence of one's rational judgements of required knowledge level and progress in work. Affective uncertainty is related to insecurity and pessimism ([Wilson _et al._, 2002](#wil02)). In the use of information systems merely technical skills are not enough, also a positive attitude and self-confidence are needed in order to cope with the systems. Emotional aspects like feelings of frustration, impatience, information overload, resistance to new information and computer aversion may form barriers to the search process ([Nahl, 2001](#nah01)). The feeling of uncertainty, often expressed as anxiety or worry, is particularly strong at the beginning of a search process, when the users become aware of their lacking knowledge about the topic ([Kuhlthau, 1993](#kuh93): 108-111). Although anxiety in connection to intellectual work usually is a temporary state ([Venkula, 1988](#ven88): 48), certain individuals may be particularly vulnerable to feelings of stress and worry in an information-seeking context ([Heinström, 2002](#hei02)).

Although the process of seeking information may involve a wide range of negative experiences of frustration and anxiety, it may also trigger positive responses like excitement and satisfaction ([Solomon, 1997](#sol97) ). A successful search process evoke positive emotions, such as joy, interest and exhiliration, and consequently encourage the searcher to continue and extend the searches ([Nahl, 2001](#nah01)). As topical knowledge is extended, confidence usually grows. The thoughts develop from being vague and confused to being clear and knowledgeable. Information seeking is consequently a cognitive and emotional process of constructing a personal understanding of a topic ([Kuhlthau, 1993](#kuh93): 108-111).

Solomon ([1997](#sol97)) has noted that information seekers are characterised by typical patterns of affective responses and differ from each other in the intensity of their reactions. Can these differences be related to personality traits?

### Personality

_'Personality is that pattern of characteristic thoughts, feelings, and behaviours that distinguishes one person from another and that persists over time and situation'_ ([Phares, 1991](#pha91): 4). It is the sum of biologically based and learnt behaviour which forms the person's unique responses to environmental stimuli ([Ryckman, 1982](#ryc82): 4-5).

The concept of personality must be hypothetically understood ([Ryckman, 1982](#ryc82): 4). No clear neurological ground can be found for it, although attempts have been made to describe the basis of personality in terms of neurophysiology ([Rowe, 1989](#row89)) or cortical dopamine activity ([Pickering & Gray, 2001](#pic02)).

The personality structure is fairly stable and predictable throughout different situations and time ([Phares, 1991](#pha91): 4-7). There are personality traits of different depth and significance. The innermost layer is the basis, while the outermost layer is situation-bound and influenced by, for example, tiredness. A tired person might accordingly behave in a way that is not like his/her true self ( [Cattell, 1950](#cat50)). Dependent on the situation, personality traits may be more or less visible and personality may also develop over time ([Phares, 1991](#pha91): 4-7). The changes which reflect events and feelings during the lifespan only affect the surface and not the core character. Profound changes in personality are usually consequences of major life changes or deliberate effort ([Costa & McCrae, 1992](#cos92): 9).

It is important that the individuals adapt to their circumstances in life at the same time as they retain the feeling of a solid inner core. Some adaptations seem to be general and follow a certain pattern. Sensation seeking is one example of characteristics which diminish over time from adolescence to middle age in all cultures ([Costa & McCrae, 1980](#cos92): 80). Neuroticism and openness to experience tend to decrease over time, while self-esteem, conscientiousness and agreeableness tend to increase ([Neyer, 2000](#ney00)). The expressions of personality are moreover dependent on age and maturity. The same activity level may thus enhance an interest for football at a young age and gardening in later days ([Costa & McCrae, 1980](#cos80): 80).

### Personality theories

Throughout the centuries, personality has been described and measured by a range of theories and models. Some theories (such as those of [Freud, 1996](#fre96) and [Jung, 1986](#jun86)) seek to explain the dynamics of personality as a whole. One of the basic concepts of Freud's theories is the notion of different levels of consciousness. We are aware of the phenomena on the conscious level, able to reach the phenomena on the preconscious level but unaware of the issues on the unconscious level. Our personality and reactions are influenced by all these three levels. Jung extended the unconscious concept to include the collective unconscious and the study of archetypes. We inherit in our brains the collective unconscious, which is a latent memory base of our ancestors. Archetypes are themes which have been part of human life throughout all time and cultures. The persona, anima and animus and the self are some of the archetypes described by Jung. The persona represents the mask and the different roles we play in our lives. Each man has a feminine side, anima, while each woman has a male side, animus. The self is depicted as our true potential and aim of self-actualization. All these aspects influence our behaviour and form the basis of our character.

Besides the psychodynamic theories of personality there are the descriptive ones. The dispositional personality perspective depicts personality as made up by physiologically based traits, which guide behaviour. Traits can be described as tendencies to behave and react in a specific way ([Phares, 1991](#pha91): 254). Personality states, on the other hand, are the results of the combination of traits and situation. Persons with high emotional instability are, for instance, more likely than calm and stable persons to feel anxiety in a threatening evaluation situation. Traits can thus be described as dispositions to states ([Humphreys & Revelle, 1984](#hum84)).

Trait theory has in recent years become more and more popular. The base of personality in this tradition is related to genetics and neurological processes. Research based on studies of twins raised apart has shown that 50% of the central personality traits can be related to genes. Experiences in childhood is another ground for the formation of personality. As experiences are self-selected to a large degree, they can also be guided by genetic disposition. Although genetics seem to influence personality, neither genetic disposition nor environmental influences are deterministic. The individual is unique in his/her character and part of a complex system which makes it impossible to predict reactions with certainty ([Bouchard, 1997](#bou97)).

After 50 years of personality research there is a common agreement in the field that there are five basic dimensions that can be used to describe differences in cognitive, affective and social behaviour. This is the base for the five-factor model of personality ([Revelle & Loftus, 1992](#rev92)). The five dimensions are usually described in the following order of decreasing robustness based on previous personality scales: neuroticism, extraversion, openness to experience, agreeableness and conscientiousness ([Costa & McCrae, 1992](#cos92): 14-16). The dimensions are stable across a lifespan and seem to have a physiological base ([Revelle & Loftus, 1992](#rev92)).

The five-factor model discussion evolved from an analysis of the terms which are used to describe personality. The lexical hypothesis states that there is enough information in natural language to describe differences in personality, as natural basic characteristics are reflected into language ([Goldberg, 1990](#gol90) ). Besides the lexical analysis, additional support for the five-factor model was found in the analysis of personality questionnaires. Almost all of the personality tests existing today measure one or more of the five factors ([McCrae & John, 1992](#mcc92)). The five dimensions are depicted in Table 1.

<table><caption>

**Table 1\. Personality dimensions and the poles of traits they form. Based on Costa & McCrae (1992: 14-16, 49).**</caption>

<tbody>

<tr>

<th>Personality dimension</th>

<th>High level</th>

<th>Low level</th>

</tr>

<tr>

<td>Neuroticism</td>

<td>sensitive, nervous</td>

<td>secure, confident</td>

</tr>

<tr>

<td>Extraversion</td>

<td>outgoing, energetic</td>

<td>shy, withdrawn</td>

</tr>

<tr>

<td>Openness to experience</td>

<td>inventive, curious</td>

<td>cautious, conservative</td>

</tr>

<tr>

<td>Agreeableness</td>

<td>friendly, compassionate</td>

<td>competitive, outspoken</td>

</tr>

<tr>

<td>Conscientiousness</td>

<td>efficient, organized</td>

<td>easy-going, careless</td>

</tr>

</tbody>

</table>

**Neuroticism** is a measure of affect and emotional control. Low levels of neuroticism indicate emotional stability whereas high levels of neuroticism increase the likelihood of experiencing negative emotions. Persons with high levels of neuroticism are reactive and more easily bothered by stimuli in their environment. They more frequently become unstable, worried, temperamental and sad. Resistant persons on the other hand need strong stimuli to be provoked ([Howard & Howard, 1995](#how95)). The term neuroticism does not necessarily refer to any psychiatric defect. A more proper term could be negative affectivity or nervousness ([McCrae & John, 1992](#mcc92)).

The **extraversion-introversion** dimension contrasts an outgoing character with a withdrawn nature. Extraverts tend to be more physically and verbally active whereas introverts are independent, reserved, steady and like being alone. The person in the middle of the dimension likes a mix between social situations and solitude ([Howard & Howard, 1995](#how95)). Extraverts are adventurous, assertive, frank, sociable and talkative. Introverts may be described as quiet, reserved, shy and unsociable ( [Costa & McCrae, 1992](#cos92): 49).

**Openness to experience** is a measure of depth, breadth and variability in a person's imagination and urge for experiences. The factor relates to intellect, openness to new ideas, cultural interests, educational aptitude and creativity as well as an interest in varied sensory and cognitive experiences. People with a high openness to experience have broad interests, are liberal and like novelty. The preservers with low openness to experience are conventional, conservative and prefer familiarity ([Howard & Howard, 1995](#how95)).

The **agreeableness** scale is linked to altruism, nurturance, caring and emotional support versus competitiveness, hostility, indifference, self-centeredness, spitefulness and jealousy ([Howard & Howard, 1995](#how95)). Agreeable people can be described as altruistic, gentle, kind, sympathetic and warm ([Costa & McCrae, 1992](#cos92): 49).

**Conscientiousness** is a measure of goal-directed behaviour and amount of control over impulses. Conscientiousness has been linked to educational achievement and particularly to the will to achieve. The focused person concentrates on a limited number of goals but strives hard to reach them, while the flexible person is more impulsive and easier to persuade from one task to another ([Howard & Howard, 1995](#how95)). The more conscientious a person is, the more competent, dutiful, orderly, responsible and thorough ([Costa & McCrae, 1992](#cos92): 49).

### The influence of personality on information-seeking behaviour

One tradition which has studied personality within LIS has described typical traits of librarians. In interaction with their customers, the librarians' friendliness, patience and helpfulness are vital characteristics in order to enhance approachability ([Hatchard & Crocker, 1990](#hat90)). Afolabi ( [1996](#afo96)) showed that librarians tend to be social, investigative and enterprising to their personality, while LIS students have been described as dominant, relaxed, sensitive and insecure ( [Goulding _et al._, 2000](#gou00)). Librarians tend to score as introvert, sensing, thinking and judging on the Myers-Bryggs scale. This personality type can be described as shy, down-to-earth, logical and organized ([Agada, 1998](#aga98); [Scherdin, 1994](sch94)). Webreck _et al._ ([1985](#wer85)) likewise described librarians as typically introverted and organized, but showed that the age, length of employment and position of the librarians are more decisive for their information richness.

Kernan & Mojena ([1973](#ker73)) studied university students' information utilization in a problem solving process. The results showed that the participants' utilization of information could be grouped into three groups according to their information behaviour and personality. The ritualistic group used a considerable amount of information in their problem solving. They were responsible and persistent but lacked confidence in themselves and others. Their persistent character and doubt of own abilities made them exaggerate the information seeking. The efficacious group obtained average results on both information-seeking amount and personality trait scores. The venturesome group used only 15 % of the available information, which is about a third of the amount used by the ritualistic group. These students were risk-taking, self-confident, dominant and social to their personality. They had a great distaste for routines.

Kirton ([1989](#kir89)) has described two opposite modes of problem solving, decision-making and creativity; the adaptors and the innovators. The adaptors are prone to accept generally recognised theories, policies and paradigms whereas the innovators want to construct their own models and question the present paradigms. The adaptors want to do things better while the innovators want to do things differently. The adaptors tend to be dogmatic, withdrawn, conscientious and anxious while innovators are open to new influences, extraverted and more confident than the adaptors (previous research reviewed in [Kirton, 1989](#kir89): 31). If these personality descriptions would be related to the five-factor model ([Costa & McCrae, 1992](#cos92) ) the trait combination of adaptors could be a combination of nervousness, introversion, conservativeness and conscientiousness while the combination of extraversion, openness to experience and emotional stability would be characteristic for innovators.

Palmer ([1991](#pal91)) tested the influence of personality as measured by the Kirton Adaption-Innovation Inventory ([Kirton, 1989](#kir89)) on the information-seeking behaviour of scientists. She found that innovators usually sought information widely, enthusiastically and used many different sources of information. Adaptors were vulnerable to social pressure and authority, prone to conformity and doubted their abilities. They were in addition more controlled, methodical and systematic in their information seeking.

The 'diffusion of innovation' theory has shown that whereas innovators are eager to become acquainted with new ideas and practices, the large majority is sceptical and cautious of novelties. The eagerness to adapt an innovation can be described on a continuum from innovators to laggards ([Rogers & Scott, 1997](#rog97) ). Risk-taking, charismatic, achievement oriented, enduring, dedicated and confident persons have dispositions which make them likely to be innovators ([Howell & Higgins, 1990](#how90)). Conferences, workshops and Web sites are essential information sources for innovators in their quest for the latest break-throughs ([Jacobsen, 1998](jac98): 3).

Personality differences have been acknowledged as influential on database searches and an important factor to consider in the design of IR systems ([Borgman, 1989](bor89)). Although long experience in database searching usually reduce the influence of personality, shyness and weak self-esteem may initially have a negative impact on search outcome ([Bellardo, 1985](#bel85)). The estimation and expectations of one's own capacities is often more influential on performance than the actual skills one possesses. Explanatory style ([Bandura, 1986](#ban86)) can, consequently, have an impact on information search behaviour. If searchers consider it likely that they will fail the search task, this affects their further actions. They might abandon the search too soon, fail to take notes or key inaccurately. Those who expect to be successful at a search task are more efficient and adaptive than those who doubt their ability to complete the task. The less competence you expect from yourself, the less effort you are willing to use ([Nahl, 1996](#nah96)).

Secure persons have a constructive and positive attitude towards information and appreciate a large recall. The more secure people are, the more actively they seek information. They are more accepting of new information and prepared to possible changes. They have a flexible cognitive structure and are more adjusted to a changing world ([Miculincer, 1997](#mic97)). Finley & Finley ([1996](#fin96)) noted that a positive attitude towards novelty correlates with a positive attitude towards and usage of the Internet. Insecure persons often have difficulties in coping with unpredictability, disorder and ambiguity in the search systems. They have a tendency to finish the search process as soon as possible, which can result in premature decisions based on insufficient information. Insecure persons are less likely to change their views and accept new information they are ([Miculincer, 1997](#mic97)). This insecurity could be linked to neuroticism. Nervousness can indeed be a barrier to information. One study has shown that nervousness formed an obstacle for seeking information for 20% of cancer patients, who would have been in need of information ([Borgers _et al._, 1993](#bor93)).

## The investigation

### Research questions

This study aimed to explore the influence of five personality dimension on information behaviour. The theoretical framework foremost leans on user behaviour research. Wilson's model of information behaviour is particularly relevant for the study, as it recognises the importance of psychological factors ( [Wilson, 1981](#wil81); [Wilson & Walsh, 1996](#wil96)). Information behaviour includes inner processes and outer factors, which influence information seeking and affect the individuals' way of responding to their information need ([Wilson, 1997](#wil97)). In the present study, personality was analysed in relation to the five-factor model, which describes five core personality dimensions ([Costa & McCrae, 1992](#cos92)).

The overall aim of the study was to explore whether information behaviour can be explained by personality.

The specific research questions were the following:

*   How does neuroticism influence information behaviour?
*   How does extraversion influence information behaviour?
*   How does openness to experience influence information behaviour?
*   How does agreeableness influence information behaviour?
*   How does conscientiousness influence information behaviour?

## Method

The present article reports the findings from a larger study which studied the influence of personality and study approach on students' information-seeking behaviour ( [Heinström, 2002](#hei02) ). This article will describe only the results related to personality traits.

The material was collected during January to May 2000\. The respondents were 305 university students who were in the process of writing their Master's thesis at the Åbo Akademi University in Finland. The respondents represented all faculties at the University.

In this study, personality was tested by the NEO Five-Factor Inventory (NEO FFI) which is a short version of the Revised NEO Personality Inventory (NEO PI-R) based on the five-factor model of personality ([Costa & McCrae, 1992](#cos92)). Each of the five personality traits is measured by twelve items, which makes a total of sixty items. The items are statements measured by five-point scales which are formed by two poles from strongly disagree to strongly agree. The scores of the twelve items which measure each trait are summarized and each respondent obtains a raw score of each of the personality traits. The raw scores are then transformed into T scores by using a profile form on the answer sheet. The T scores account for sex differences based on normative samples. Reliability for the scales within the population of the present study was tested with Cronbach alpha and gave the following results; neuroticism (.87), extraversion (.83), openness to experience (.70), agreeableness (.77) and conscientiousness (.81).

The questionnaire which measured information-seeking behaviour was designed for this particular study. It consisted of seventy questions about how the students usually seek information. The questionnaire was in Swedish. The questions started with background information. Then the questions moved on to information-seeking topics like critical evaluation of information, difficulties in relevance judgement, document selection criteria, experience of time pressure as a barrier to information, accidental information discovery and effort used in information seeking.

Study results were measured by a self-assessment question in the information-seeking questionnaire, where the students were asked to state whether they generally got satisfactory, good or excellent marks. The use of self-report instead of more robust measures of grades was based on a belief in peoples capacity to truthfully account for their behaviour. This belief forms the basis of the use of questionnaires in general and was further strengthened by the normal distribution of answers to the questions.

Critical information judgement was measured by seven Likert-scale statements, for example: _What is published in books are facts that can be trusted._ The questions which measured critical thinking were then combined to a critical judgement scale which had a normal distribution of answers.

Difficulties in relevance judgement were measured by the following Likert-scale statement: _Much of what I have read is written in such a way that it is hard to see what is essential_.

In this study document selection was considered in terms of whether the students preferred to retrieve information which confirmed previous ideas or documents which inspired new thinking. This was measured by two questions. In the first question the students were asked whether they preferred documents which confirmed their own thoughts about the subject or documents which gave new ideas. The second question considered whether the respondents preferred material which brings new perspectives to their fields of study or documents which are already recognized and accepted in their fields of study. These questions were summarized into two variables for the further analysis; looking for new views and looking for confirmation of old views.

Time pressure as a barrier to information was measured by the following Likert-scale statement: _Sometimes I simply do not have time to seek information._

Accidental information discovery was measured by the following Likert-scale statement: _Sometimes I come across information even though I am not consciously looking for it_.

How much effort the students were willing to put into their information seeking was measured by ten statements on a Likert-scale. These statements considered, for instance, how much time and money the students were willing to devote to their thesis work. The questions which measured effort in information seeking were combined on an effort scale, which had a normal distribution of answers.

## Results

The final overall response rate was 67 %. 209 women (68 %) and 89 men (29 %) answered the questionnaires. Seven of the respondents chose not to identify their sex. The female majority reflected the general sex structure at Åbo Akademi University, where 61 % of the students are women and 39 % are men (Åbo Akademi in brief 2000, brochure). The age of the respondents varied between 21 and 52 years. The median age was 25 years and the mean age was 28 (0 = 28, sd = 12.58) years.

222 students were at the background stage (developing the research plan and reading background material), 247 students were at the data-collection stage (planning the collection of data, data collection and analysing the data) and 126 at the final stage (interpreting the results and final stage) of their thesis.

The data were tested for normal distribution and the results mainly analysed by correlation analyses. As this article focuses on the influence of personality traits on information seeking, the specific relation between these two variables is the focal point. In the larger analyses, personality traits were used as independent variables and compared, in regression analyses, to the influence of the students' study approach, the disciplines to which they belonged and the stage of the thesis process ([Heinström, 2002](#hei02)). These analyses confirmed the correlation results presented in Table 2\. In this table, only significant results are reported.

<table><caption>

**Table 2\. Pearson correlation analysis of information-seeking behaviour and personality traits**</caption>

<tbody>

<tr>

<th> </th>

<th colspan="5">Personality</th>

</tr>

<tr>

<th>Information seeking behaviour</th>

<th>Neuroticism</th>

<th>Extraversion</th>

<th>Openness</th>

<th>Agreeableness</th>

<th>Conscientiousness</th>

</tr>

<tr>

<td>Relevance difficulties</td>

<td>r=.16  
p=.008</td>

<td>—</td>

<td>r=-.13  
p=.03</td>

<td>r=-.18  
p=.002</td>

<td>r=-.20  
p=.001</td>

</tr>

<tr>

<td>Time pressure as a barrier  
to information</td>

<td>r=.14  
p=.02</td>

<td>r=-.12  
p=.04</td>

<td>—</td>

<td>r=-.18  
p=.002</td>

<td>r=-.15  
p=.01</td>

</tr>

<tr>

<td>Confirmation of previous knowledge</td>

<td>—</td>

<td>r=-.13  
p=.02</td>

<td>r=-.11  
p=.07</td>

<td>—</td>

<td>r=-.14  
p=.01</td>

</tr>

<tr>

<td>Critical information judgement</td>

<td>—</td>

<td>—</td>

<td>r=.23  
p=.0001</td>

<td>r=-.24  
p=.0001</td>

<td>—</td>

</tr>

<tr>

<td>Aiming to acquire new ideas from  
retrieved information</td>

<td>—</td>

<td>r=.17  
p=.007</td>

<td>r=.11  
p=.07</td>

<td>—</td>

<td>r=.14  
p=.01</td>

</tr>

<tr>

<td>Effort</td>

<td>—</td>

<td>—</td>

<td>r=.19  
p=.002</td>

<td>—</td>

<td>r=.18  
p=.003</td>

</tr>

</tbody>

</table>

The correlation analysis revealed that finding it difficult to know whether information is relevant or not was related to a careless (low conscientiousness), competitive (low agreeableness), sensitive (high neuroticism) and conservative (low openness) personality.

Experiencing time pressure was related to an easy-going (low conscientiousness), introverted (low extraversion), sensitive (high neuroticism) and competitive (low agreeableness) personality.

Preferring to retrieve information which confirmed previous knowledge was negatively related to extraversion, openness to experience (although not significantly) and conscientiousness. We can say that an introverted, conservative or easy-going personality prefers information which confirms previous knowledge. There was a non-significant connection between preference for confirming information and high levels of neuroticism.

Critical thinking was related to an open and outspoken (low agreeableness) character.

Aiming to acquire new ideas was typical for outgoing, open and conscientious students. Extraverted students moreover often found useful information by mere chance (r=.14, p=.02) or through informal sources, like teachers and friends (r=.11, p=.06). There was a non-significant connection between openness to experience and accidental information discovery.

Effort was related to conscientiousness and openness to experience. Conscientiousness is related to a general willingness to work hard, which is reflected in information seeking. Openness to experience is linked to curiosity and interest to learn new things, which also trigger use of effort in information seeking. There was a non-significant connection between a tendency to give up database searches if nothing was immediately retrieved and high levels of neuroticism.

The correlation analysis further showed that conscientiousness (r=.15, p=.01) and openness to experience (r=.13, p=.03) were related to good study results, whereas extraversion showed a tendency towards a negative correlation (r=-.10, p=.08) with study results. In other words, it seems as though the efficiency of the conscientious students and the curiosity and willingness to learn of the open students enhance study results.

## Discussion

In the following discussion, the influence of the five personality dimension on the respondents' information behaviour will be explored by presenting each trait dimension in a specific section. It is important to keep in mind that many features of information seeking and more importantly many explaining variables, which influence information seeking fall outside the scope of the article. Although the results showed that personality traits indeed influence information behaviour, the broader scope of influential factors is acknowledged.

### Neuroticism

Neuroticism - the vulnerability to negative emotions - was related to preference for confirming information, feeling that lack of time was a barrier to information retrieval, difficulties with relevance judgement and insecurity in database searching. These connections suggest that negative emotionality may form a barrier to successful information retrieval. This influence seems related to personality inclination as well as to temporary states of anxiety (as previously shown by [Ford _et al._, 2001](#for01)).

As negative emotions consume energy and distract concentration, negative emotionality may be an initial obstacle to successful database searches. If the searchers anticipate similar patterns in future searches this can set off a chain reaction of psychological barriers. Feelings of anxiety tend to enforce an escape reaction to threatening situation where there is a history of failure ([Revelle, 1995](#rev95)). The estimation and expectations of one's own capacities is often more influential on performance than the actual skills one possesses ([Bandura, 1986](#ban86)). Insecurity and doubt of the own abilities may lead to little effort and persistence in information seeking (in line with [Miculincer, 1997](#mic97); [Nahl, 2001](#nah01)). The students with high emotional instability were indeed shown to give up their information searches in databases if they retrieved nothing on their initial queries. The reason for abandoning searches may be doubt of the own abilities and based on a previous history of failure. On other occasions, the rationale behind low persistence in database searches may be a more pragmatic one - lack of time.

The lack of time can be a reality but people may also vary according to how strongly they perceive time pressure and how they act upon it. As the feeling of time pressure is related to stress, it was interesting to note that in particular students with high emotional instability experienced lack of time as a barrier to information. Research has shown that performance by persons with high levels of neuroticism drop in stressful situations ( [Costa & McCrae, 1992](#cos92): 14). Emotionally instable persons are thus more vulnerable to the strainful experience of time limits which reduce their efforts on information seeking.

In addition to lack of time, another common cause for stress in our time is the constant flow of new information. Whether there is a basic insecurity about what is relevant or not, new information can appear too challenging. Students who faced relevance problems were shown to prefer documents which confirmed old knowledge to new thought-provoking documents. Students with high levels of neuroticism are more vulnerable to the strain of many conflicting messages and, accordingly, prefer less confusing information. This is a way of increasing the feeling of control and the confidence of sufficient topical knowledge which is particularly relevant for insecure persons. Previous research has shown that the more secure people are, the more active in information seeking and the more able to accept new information ([Miculincer, 1997](#mic97)). Self-reliance and confidence is linked to an inner security which makes novelty appear less threatening.

At the beginning of the information-seeking process it is common to be reassured by information, which relates to previous knowledge. This reduces the feeling of anxiety which is common at this confusing stage where much of the encountered information is unfamiliar ([Kuhlthau, 1993](#kuh93): 116-117). Kuhlthau has shown that the state of anxiety can be linked to a preference for familiar document content. The trait of anxiety vulnerability, neuroticism, was in the present study linked to preference for documents which confirmed previous ideas. This shows that personality dispositions may explain inclination and preference for certain information content throughout stages.

As the range of negative emotions linked to high levels of neuroticism may appear discouraging in relation to successful information retrieval, it should be pointed out that temporary states of worry and insecurities can in some circumstances enhance performance. Although strong negative emotions can distract the attention on the actual task and in this way hinder performance, it can also be a means of concentration on familiar tasks and, in this way, in fact enhance performance. The hindering or enhancing effect of excitation on performance can be pictured as a U, where there is an optimal level of arousal at the bottom. Arousal that exceeds or goes below this level affects performance negatively ([Crozier, 1997](#cro97): 123-141). It is consequently possible that a certain level of arousal, even related to negative emotions, may in fact increase concentration also on information seeking tasks. One way to attended to more profound insecurities and fears of failure would be increased topical and procedural knowledge, as developed information literacy skills.

### Extraversion

Extraversion was related to informal information retrieval as well as preference for thought-provoking documents over documents which confirmed previous ideas.

Extraverted students have an enthusiastic, active and confident character, which was reflected in their information seeking. These energetic and outgoing students wanted to find much information without being very systematic in their quest for it. Their information strategies were characterised by quick solutions and use of social abilities. Outgoing students often consulted teachers, supervisors and friends as information sources. Supervisors and teachers are good sources for direct guidelines and literature suggestions, while fellow students provide the opportunity for informal feedback and exchange of ideas. The outgoing students particularly welcomed discussions or documents which brought new perspectives to their subject area. Previous research has shown a common resistance to "bother" librarians with information queries ([Onwuegbuzie & Jiao, 1998](#onw98) ). This hesitation is unlikely to restrain outgoing students as social interaction is an important part of their information behaviour.

The extraverts' information-seeking activity does not necessarily mean activity also in information use. A broad collection of information do not evidently lead to deep analysis and learning from the information, at least not as measured in marks. Extraversion was in the present study shown to have a negative impact on marks (a finding in line with [De Raad &Schouwenburg, 1996](#der96)). Extraverted, outgoing students are likely to prefer to devote their time to social activities instead of studies (as shown by [McCown & Johnson, 1991](#mcc91) ).

### Openness to experience

Openness to experience was related to broad information seeking, incidental information acquisition, critical information judgement, preference of thought-provoking documents instead of documents which confirmed previous ideas and use of effort in information seeking. Conservativeness, on the other hand, was related to problems with relevance judgement and preference of documents which confirmed previous ideas instead of thought provoking documents

The results of the present study showed that open students use much effort in their information seeking and prefer to retrieve a wide range of related documents from information searches instead of only a few precise ones. If the key factor behind the extraverted students' active information seeking was an energetic character, intellectual curiosity is the motivating factor behind the open students' broad information seeking. Open people are intellectually curious and like to play with theories ([Costa & McCrae, 1992](#cos92): 15). The higher the involvement and interest, the more complex and profound the need for information tends to be ([Dunn, 1986](#dun86)). While extraverted students often obtained low grades, students with high openness to experience were more successful in their studies (in line with [De Raad & Schouwenburg, 1996](#der96)). This highlights how decisive the motivation behind broad information seeking is for information use and outcome. A need for intellectual analysis is reflected in in-dept exploration of the content which in turn result in a good study outcome.

An invitational and open information attitude is particularly important at the initiation stage. Usually moods change according to information need but there are also tendencies towards stylistic persistence in moods ([Kuhlthau, 1993](#kuh93): 118-119). Open students, who are imaginative, inventive, creative, curious and unconventional ([Costa & McCrae, 1992](#cos92)) show signs of an inherent interest for new ideas. They hunt sources of inspiration in a wide range of sources. Both the characteristics and the search pattern of open students could be compared to innovators (in line with [Jacobsen, 1998](#jac98); [Kirton, 1989](#kir89): 31; [Palmer, 1991](#pal91)).

Open students have an inherent "environmental scanning" in their curiosity and invitational attitude towards life, which explains why they often discovered useful information by chance. Erdelez ([1997](#erd97)) has previously speculated over the possibility that personality traits like curiosity, desire for exploration and numerous interests increase the likelihood of encountering useful information by chance. It was interesting, therefore, to note that students characterized by openness to experience were most open for accidental information discovery. Open persons have the wide interests and desire for exploration suggested by Erdelez. The result of the present study consequently suggests that personality traits indeed increase the likelihood for information encountering.

Open individuals are unconventional and prepared to question authorities ([Costa & McCrae, 1992](#cos92): 15). These characteristics form a good basis for critical evaluation of information. The critical and open students prefer to retrieve a broad range of information rather than a few precise ones right on target. This further increases the awareness of differences in interpretations, viewpoints and content quality. Curious and interested students, with confidence in their capability to critically analyse information, are not afraid of new information content but rather welcome it.

We could see that high openness to experience lead to a broad and invitational information attitude. How would then low openness to experience influence information seeking? The results showed that a conservative character, who wants things to remain as they always have, prefer familiarity to novelty also in information retrieval. The conservative students used the least possible effort in their information seeking and preferred to retrieve only a few precise documents instead of a wide range of closely related documents. A precise search result is less likely to offer new, challenging ideas, which the conservative students want to avoid. This implies a cautious information-seeking attitude which is narrow in content aim as well as conduct. The conservative students, who prefer confirmation of familiar knowledge, can be compared to the adaptors who are reluctant to new ideas and conservative in their character ([Kirton, 1989](#kir89)). Little openness to experience in character is accordingly manifested in little openness to new information.

Conservative students likewise expressed conventionality in their criteria for document choice. They preferred clearly and recently written documents and overviews, which all are 'standard' quality criteria, presumably taught and recommended by teachers and supervisors. Their use of information sources also seemed traditional and conservative as they mostly found information in the most customary sources; printed sources and group sources, like lectures. They did not show any desire to use of more explorative information sources, like mass-media or Internet sources.

### Competitiveness

Competitiveness was related to experiencing lack of time as a barrier to information retrieval, problems with relevance judgement and competence in critical analysis of information.

One of the facets of the competitiveness scale is impatience ([Costa & McCrae, 1992](#cos92) : 49). If students are impatient by character, it is unlikely that they will devote time for information seeking. When they eventually need the information, they can unjustly feel that the lack of time was the barrier, when the actual barrier was that they did not prioritise information seeking in their use of time. It is, nevertheless, important to keep in mind that lack of time simply can be a reality, where the only option is a hasty and superficial information seeking. As confidence in relevance judgement would require time to get acquainted with the topic from various viewpoints, difficulties in knowing whether information was relevant for the topic or not could be related to the impatient character of competitive students.

The present study also showed a link between competitiveness and critical analysis of information. Low levels of agreeableness (competitiveness) are useful in an academic context as low agreeableness forms a base for sceptical and critical thinking ([Costa & McCrae, 1992](#cos92): 15).

### Conscientiousness

Conscientiousness was related to preference of thought-provoking documents instead of documents which confirmed previous ideas and use of effort in information seeking. Carelessness, on the other hand, was related to problems with relevance judgement, feeling that lack of time was a barrier to information retrieval and preference of documents which confirmed previous ideas instead of thought provoking documents

Conscientious students were willing to use effort - time, money and hard work - in order to obtain relevant information. One central feature of conscientiousness is self-control, with a capacity to carry out tasks. Conscientious people are strong-willed and determined to achieve, also academically ([Costa & McCrae, 1992](#cos92): 16). Structure and persistence seems to be related to mastered information seeking with few problems. These students seem to know what they are aiming at and are willing to work hard in order to attain it. All in all, these variables seem to describe an information-seeking behaviour, which is taken seriously. The Kernan and Mojena ([1973](#ker73)) study showed a similar pattern of responsible students who used a large amount of information in their problem solving.

Not only did the conscientious students work hard in order to retrieve useful information. They also put strain in their analysis of it, as they preferred documents by respected authors from acknowledged sources over more easily digested literature. Determination in personality may be a decisive factor in this material preference, as introduction to new ideas always requires analysis and reconsideration. The conscientious students are willing to meet this challenge with their usual willingness to work hard. This persistence may prove useful in an academic context. The conscientious students were indeed shown to obtain good study results (in line with [De Raad & Schouwenburg, 1996](#der96)).

Easygoing students with low levels of conscientiousness often chose their information sources on the basis of easy access with the use of minimum effort and thoroughness. These students can be described as easily distracted, careless, impulsive and hasty ([Costa & McCrae, 1992](#cos92): 49). Information searches by easy-going persons showed signs of this impulsiveness as they were rather gradually developed than being planned. Information content which confirm previous ideas is easier to digest and therefore particularly attractive for an easy-going character. The choice of information source was in this case more guided by a need for quick answers than an intention to obtain information to ponder upon.

## Conclusions

The contribution of the present study lies foremost in the emphasis on the importance of considering individual patterns of information-seeking behaviour. As the results showed that information seeking could be related to personality traits, the study suggests that psychological mechanisms other than cognitive abilities (e.g., [Ingwersen, 1996](#ing96)) and feelings (e.g., [Kuhlthau, 1993](#kuh93)) may be connected to information-seeking behaviour. This illustrates how person dependent the information-seeking process is, and how important it is to acknowledge that each individual has an unique way of seeking information.

Research which focuses on individual differences is based on an assumption that understanding of psychological mechanisms needs to account for variability between individuals as well as the overall levels of performance ([Stankov _et al._, 1995](#sta95): 15). By finding connections between certain personality traits and certain behaviours, such as information seeking, we can gain a deeper understanding of the psychological mechanism, which influence information seeking habits.

The final impact of personality on information seeking is dependent on the unique combination of traits which distinguish each individual. The more traits that incline towards certain information behaviour an individual possesses, the more likely it is that s/he will take on this behaviour. In some cases conflicting inclinations by different traits may neutralize the impact of personality traits, whereas in other situations, a strong personality characteristic may dominate and override other tendencies. This is the case for instance when a foremost conservative but conscientious person overcomes his/her cautious inclination towards confirming information by taking the effort to explore new challenging documents.

The results of the present study point to the importance of examining information-seeking behaviour from a broad perspective, as previously pointed out by Wilson ([1997](#wil97), [2000](#wil00) ). Although regularities and patterns in information-seeking behaviour have been found (for example, by [Kuhlthau, 1993](#kuh93)), there are always exceptions to the general pattern, which are not accounted for within the framework of the models. Personality differences is one factor that can explain these exceptions. Personality disposition is far from deterministic and human reactions can never be predicted with certainty. If we however seek patterns in personal characteristics in addition to contexts, we may add one more dimension to our understanding of information-seeking behaviour. The disposition within the individual interacts with contextual demands. The core personality will remain the same although the way it is expressed and how much it influences behaviour varies according to context. If the situation is motivating enough contextual variables influence search behaviour more, but in less motivating situations as in neutral tasks, such as routines or daily activities, personal characteristics are likely to be more influential ([Allen & Kim, 2001](#all01)).

Information strategies can certainly be learnt and modified, but some tendencies and habits tend to remain regardless of education. The present study has pointed to personality traits as one influential factor on this. If one for instance is impulsive and easy-going to the character, it is understandable that this affects the gathering of information which might be more spontaneous and less structured than a search by a conscientious and methodical person. Habits and preferences can certainly be modified, but this modification is not without limits.

With awareness that there are profound differences in the way people seek information and that these habits may be related to something as stable as personality traits, it is clear that everyone cannot be adapted to the same search routines. Instead search systems and information services must be further developed to better account for individual differences.

## Send your comments on this paper to the journal's discussion list - join [IR-discuss](http://www.jiscmail.ac.uk/lists/IR-DISCUSS.html)

## References

*   <a id="afo96"></a>Afolabi, M. (1996). Holland's typological theory and its implications for librarianship and libraries. _Library Career Development_, **4**(3), 15-21.
*   <a id="aga98"></a>Agada, J. (1998). Profiling librarians with the Myers-Briggs type indicator: studies in self selection and type stability. _Education for Information,_ **16**(1), 57-69.
*   <a id="all01"></a>Allen, B.L. & Kim, K-S. (2001). Person and context in information seeking: interaction between cognitive and task variables. _New Review of Information Behaviour Research._, **2**, 1-16
*   <a id="ban86"></a>Bandura, A. (1986). _Social foundations of thought and action: a social cognitive theory_. Englewood Cliffs, NJ: Prentice Hall
*   <a id="bel85"></a>Bellardo, T. (1985). An investigation of online searcher traits and their relationship to search outcome. _Journal of the American Society for Information Science_, **36**(4), 241-250.
*   <a id="bli96"></a>Blickle, G. (1996). Personality traits, learning strategies, and performance. _European Journal of Personality_, **10**, 337-352.
*   <a id="bor93"></a>Borgers, R., Mullen, P.D., Meertens, R., Rijken, M., Eussen, G., Plagge, I., Visser, A. P. & Blijham, G. H. (1993). The information seeking behaviour of cancer outpatients: a description of the situation. _Patient Education and Counselling_, **22**(1), 35-46.
*   <a id="bor89"></a>Borgman, C. (1989) All users of information retrieval systems are not created equal: an exploration into individual differences. _Information processing and management_, **25**(3), 237-251.
*   <a id="bou97"></a>Bouchard, T.J. (1997). Whenever the twain shall meet. _The Sciences_, **37**(5), 52-57.
*   <a id="bys00"></a>Byström, K. (2000). The effects of task complexity on the relationship between information types acquired and information sources used. _New Review of Information Behaviour Research._, **1** , 85-101.
*   <a id="cat50"></a>Cattell, R.B. (1950). _An introduction to personality study_. London: Hutchinson.
*   <a id="cos80"></a>Costa, P.T. & McCrae, R.R. (1980). _Still stable after all these years: personality as a key to some issues in adulthood and old age_. In P. B. Baltes & O. G. Brim, (Eds.). _Life span development and behaviour_. (3rd. ed.) (pp. 65-102). New York, NY: Academic Press.
*   <a id="cos92"></a>Costa, P.T., & McCrae, R.R. (1992). _NEO PI-R. Professional manual_. Odessa, FL: Psychological Assessment Resources, Inc.
*   <a id="cro97"></a>Crozier, W. R. (1997). _Individual learners. Personality differences in education._ London: Routledge.
*   <a id="der96"></a>De Raad, B. & Schouwenburg, H. C. (1996). _Personality in learning and education: a review._ _European Journal of Personality_, **10**(5), 303-336.
*   <a id="dun86"></a>Dunn, K. (1986). Psychological needs and source linkages in undergraduate information-seeking behaviour. _College and Research Libraries_, **47**(5), 475-481\.
*   <a id="erd97"></a>Erdelez, S. (1997). _Information encountering: a conceptual framework for accidental information discovery._ In P. Vakkari, R. Savolainen and B. Dervin (Eds.). _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts. Tampere, Finland: 14-16 August 1996_. (pp. 412-421). London: Taylor Graham.
*   <a id="fin96"></a>Finley, K. & Finley, T. (1996). The relative role of knowledge and innovativeness in determing librarians' attitudes toward and use of the Internet: a structural equation modelling approach. _Library Quarterly_, **66**(1), 59-83.
*   <a id="for00"></a>Ford, N. (2000). Cognitive styles and virtual environments. _Journal of the American Society for Information Science_, **51**(6), 543-557.
*   <a id="for01"></a>Ford, N., Miller, D. & Moss, N. (2001). The role of individual differences in Internet searching: an empirical study. _Journal of the American Society for Information Science and Technology_, **52**(12), 1049-1066.
*   <a id="for02"></a>Ford, N., Wilson, T. D., Foster, A., Ellis, D. & Spink, A. (2002). Information seeking and mediated searching. Part 4\. Cognitive styles in information seeking. _Journal of the American Society for Information Science and Technology_, **53**(9), 728-735.
*   <a id="fre96"></a>Freud, S. (1996). _Samlade skrifter. 1\. Föreläsningar: orientering i psykoanalysen_.  In D. Titelman, (Ed.). Stockholm: Natur och kultur
*   <a id="gol90"></a>Goldberg, L.R. (1990). An alternative "description of personality": the Big-Five factor structure. _Journal of Personality and Social Psychology_, **59**(6), 1216-1229
*   <a id="gou00"></a>Goulding, A., Bromham, B., Hannabuss, S. & Cramer, D. (2000). Professional characters: the personality of the future information workforce. _Education for Information_, **18**(1), 7-31.
*   <a id="hat90"></a>Hatchard, D.B. & Crocker, C. (1990). Library users - a psychological profile. _Australian Academic and Research Libraries_, **21**(2), 97-105.
*   <a id="hei02"></a>Heinström, J. (2002). _[Fast surfers, broad scanners and deep divers - personality and information seeking behaviour](http://www.abo.fi/~jheinstr/thesis.htm)._ Åbo (Turku): Åbo Akademi University Press. (Doctoral dissertation) Retrieved 3 October, 2003 from http://www.abo.fi/~jheinstr/thesis.htm
*   <a id="how95"></a>Howard, P.J. & Howard, J. M. (1995). _The Big Five quickstart: an introduction to the Five-Factor Model of Personality for human resource professionals_. Charlotte, NC: Centre for Applied Cognitive Studies.
*   <a id="how90"></a>Howell, J. M. & Higgins, C. A. (1990). Champions of technological innovation. _Administrative Science Quarterly_, **35**(2), 317-341.
*   <a id="hum84"></a>Humphreys, M. S. & Revelle, W. (1984). Personality, motivation and performance. A theory of the relationship between individual differences and information processing. _Psychological Review_, **91**(2), 153-184.
*   <a id="ing82"></a>Ingwersen, P. (1982). Search procedures in the library - analysed from the cognitive point of view. _Journal of Documentation_, **38**(3), 165-191.
*   <a id="ing96"></a>Ingwersen, P. (1996). Cognitive perspectives of information retrieval interaction: elements of a cognitive IR theory.  _Journal of Documentation_, **52**(1), 3-50\.
*   <a id="jac98"></a>Jacobsen, D. M. (1998). _[Adoption patterns and characteristics of faculty who integrate computer technology for teaching and learning in higher education.](http://www.ucalgary.ca/~dmjacobs/phd/diss/)_ Unpublished doctoral dissertation, University of Calgary, Department of Educational Psychology, Calgary, Canada. Retrieved 3 October, 2003 from http://www.ucalgary.ca/~dmjacobs/phd/diss/
*   <a id="jun86"></a>Jung, C. G. (1986). _Jaget och det omedvetna._ (2nd. ed.) Stockholm: Wahlström & Widstrand
*   <a id="ker73"></a>Kernan, J. B. & Mojena, R. (1973). Information utilization and personality. _Journal of Communication_, **23**(3), 315-327.
*   <a id="kim01"></a>Kim, K.-S. (2001). Information seeking on the Web: effects of user and task variables. _Library and Information Science Research,_ **23**(3), 233-255.
*   <a id="kir89"></a>Kirton, M. J. (1989). A theory of cognitive style. In M. J. Kirton (Ed.). _Adaptors and innovators. Styles of creativity and problem-solving._ (pp. 1-36). London: Routledge.
*   <a id="kuh93"></a>Kuhlthau, C. C. (1993). _Seeking meaning: a process approach to library and information services._ Norwood, NJ: Ablex.
*   <a id="lec97"></a>Leckie, G. J., & Pettigrew, K.  (1997). A general model of the information-seeking of professionals: role theory through the back door? In P. Vakkari, R. Savolainen and B. Dervin (Eds.). _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts. Tampere, Finland: 14 August 1996-16 August 1996_. (pp. 99-110). London: Taylor Graham.
*   <a id="lim98"></a>Limberg, L. (1998). _Att söka information för att lära_. Gothenburg: Valfrid.
*   <a id="mcc91"></a>McCown, W. G. & Johnson, J. L. (1991). Personality and chronic procrastination by university students during an academic examination period. _Personality and Individual Differences_, **12**(5), 413-415\.
*   <a id="mcc92"></a>McCrae, R. R & John, O. (1992). An introduction to the five-factor model and its applications. _Journal of Personality,_ **60**(2), 174-214\.
*   <a id="mic97"></a>Miculincer, M. (1997). Adult attachement style and information processing: individual differences in curiosity and cognitive closure. _Journal of Personality and Social Psychology_, **72**(5), 1217-1230.
*   <a id="nah96"></a>Nahl, D. (1996). Affective monitoring of Internet learners: perceived self-efficacy and success. In S. Hardin (Ed.). _Proceeding of the 59th ASIS Annual Meeting. Global complexity: information, chaos and control_, **33**, 100-109.
*   <a id="nah01"></a>Nahl, D. (2001).[A conceptual framework for explaining information behavior.](http://www.utpjournals.com/jour.ihtml?lp=simile/issue2/nahlfulltext.html) _Studies in Media and Information Literacy Education_, **1**(2). Retrieved 3 October, 2003 from http://www.utpjournals.com/jour.ihtml?lp=simile/issue2/nahlfulltext.html
*   <a id="ney00"></a>Neyer, F.J. (2000). _How does personality influence social relationships and choices of life patterns?._ Paper presented at the 10<sup>th</sup> European Conference on Personality, July 16-20, 2000, Cracow, Poland.
*   <a id="och99"></a>Ocholla, D. (1999). Insights into information-seeking and communicating behaviour of academics. _International Information & Library Review_, **31**(3), 111-143.
*   <a id="onw98"></a>Onwuegbuzie, A.J. & Jiao, Q.G. (1998). The relationship between library anxiety and learning styles among graduate students: implications for library instruction. _Library and Information Science Research_ , **20**(3), 235-249.
*   <a id="pal91"></a>Palmer, J. (1991). Scientists and information. II. Personal factors in information behaviour. _Journal of Documentation_, **47**(3), 254-275\.
*   <a id="pha91"></a>Phares, E. J. (1991). _Introduction to psychology_. (3rd. ed.) New York: Harper Collins Publishers.
*   <a id="pic01"></a>Pickering, A. D. & Gray, J. A. (2001). Dopamine, appetitive reinforcement, and the neuropsychology of human learning: an individual differences approach. In A. Eliasz & A. Angleitner (Eds.). _Advances in individual differences research_. (pp. 113-149). Lengerich, Germany: PABST Science Publishers.
*   <a id="rad95"></a>Radecki, C. M., and Jaccard, J. (1995). Perceptions of knowledge, actual knowledge and information search behaviour. _Journal of Experimental Social Psychology_, **31**(2), 107-118\.
*   <a id="rev92"></a>Revelle, W. & Loftus, D. (1992). [The implications of arousal effects for the study of affect and memory.](http://pmc.psych.nwu.edu/revelle/publications/rl91/rev_loft_ToC.html) In S.A. Christianson (Ed.) _Handbook of emotion and memory. Research and theory._ (pp. 113-149). Hillsdale, NJ: Lawrence Erlebaum Associates. Retrieved 3 October, 2003 from http://pmc.psych.nwu.edu/revelle/publications/rl91/rev_loft_ToC.html
*   <a id="rog97"></a>Rogers, E. M. & Scott, K. L. (1997). _[The diffusion of innovations model and outreach from the national network of libraries of medicine to native American communities.](http://nnlm.gov/pnr/eval/rogers.html)_ (Draft paper prepared for the National Network of Libraries of Medicine, Pacific Northwest Region, Seattle.) Retrieved 3 October, 2003 from http://nnlm.gov/pnr/eval/rogers.html
*   <a id="row89"></a>Rowe, D. (1989). _Personality theory and behavioural genetics: contribution and issues._ In D. Buss & N. Cantor (Eds.). _Personality psychology. Recent trends and emerging directions_. (pp. 294-307). New York: Springer-Verlag.
*   <a id="ryc82"></a>Ryckman, R. (1982). _Theories of personality_. (2nd/ ed.) Monterey, CA: Brooks/Cole.
*   <a id="sch94"></a>Scherdin, M. (1994). _Vive la difference: exploring librarian personality types using the MBTI_. In M. Scherdin (Ed.). _Discovering librarians: profiles of a profession_. (pp. 125-156). Chicago, IL: Association of College and Research Libraries.
*   <a id="sol97"></a>Solomon, P. (1997). Discovering information behaviour in sense making: III. The person. _Journal of the American Society for Information Science_, **48**(12), 1127-1138.
*   <a id="sol02"></a>Solomon, P. (2002). _Discovering information in context._ In B. Cronin, (Ed.). _Annual Review of Information Science and Technology_, **36**, 229-264.
*   <a id="sta95"></a>Stankov, L., Boyle, G. J., & Cattell, R. B. (1995). _Models and paradigms in intelligence research._ In D. Saklofske & M. Zeidner (Eds.). _International handbook of personality and intelligence._ (pp. 15-43). New York: Plenum.
*   <a id="ven88"></a>Venkula, J. (1988). _Tietämisen taidot._ Tieteelliseen toimintaan harjaantuminen yliopisto-opinnoissa. Philosophica-sarja. Helsinki: Gaudeamus.
*   <a id="web85"></a>Webreck, S., Fine, S. & Woolls, B. (1985). _The effects of personality type and organizational climate on the acquisition and utilization of information: a case study approach involving the professional staff of four academic libraries._ Unpublished doctoral dissertation, University of Pittsburgh, School of Information Science, Pittsburgh, PA.
*   <a id="wil81"></a>Wilson, T. D. (1981). On user studies and information needs. _Journal of Documentation_, **37**(1), 3-15.
*   <a id="wil97"></a>Wilson, T. D. (1997). _Information behaviour: an inter-disciplinary perspective._ In P. Vakkari, R. Savolainen and B. Dervin (Eds.). _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts._ _Tampere, Finland: 14 August 1996-16 August 1996_. (pp. 39-50). London: Taylor Graham,
*   <a id="wil00"></a>Wilson, T. D. (2000). [Human information behavior](http://209.68.25.5/Articles/Vol3/v3n2p49-56.pdf). _Informing Science_, 3 (2), 49-56\. Retrieved 8 October, 2003 from http://209.68.25.5/Articles/Vol3/v3n2p49-56.pdf
*   <a id="wil02"></a>Wilson, T. D., Ford, N., Ellis, D., Foster, A., & Spink, A. (2002). Information seeking and mediated searching. Part 2\. Uncertainty and its correlates. _Journal of the American Society for Information Science and Technology_, **53**(9), 704-715.
*   <a id="wil96"></a>Wilson, T. D., & Walsh, C. (1996). _[Information behaviour: an inter-disciplinary perspective.](http://informationr.net/tdw/publ/infbehav/prelims.html)_ London: British Library Research and Innovation Centre. (BLRIC Report 10). Retrieved 3 October, 2003 from http://informationr.net/tdw/publ/infbehav/prelims.html