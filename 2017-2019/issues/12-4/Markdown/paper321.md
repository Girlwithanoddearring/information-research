#### Vol. 12 No. 4, October, 2007

* * *

# The 'platinum route' to open access: a case study of E-JASL: _The Electronic Journal of Academic and Special Librarianship_

#### [Paul G. Haschak](mailto:PHaschak@usouthal.edu)  
Coordinator of Collection Management, 
University Library, 
University of South Alabama, 
Mobile, Alabama, 
USA

#### Abstract

> **Introduction.** In 1999, with no money and no support from any library organization, the author partnered with the International Consortium for Alternative Academic Publication (ICAAP), later renamed the International Consortium for the Advancement of Academic Publication, to found a new electronic journal, _The Journal of Southern Academic and Special Librarianship_, renamed _E-JASL: The Electronic Journal of Academic and Special Librarianship_ in 2002.  
> **Description.** This case study is based on the author's own experiences founding and developing a professional, independent, permanently archived, peer reviewed, open-access, electronic library journal, employing a scholar-led model of publishing. The author's partnership with the ICAAP is discussed emphasizing the benefits of this collaboration.  
> **Conclusion.** The ICAAP has demonstrated to the world that is possible to form independent scholarly journal publishing projects outside of the commercial mainstream. Also, the ICAAP has shown that there is an alternative to paying commercial publishers hundreds and even thousands of dollars to buy back the scholarly research of our colleagues in academia. The alternative is starting and/or supporting scholarly journal publishing projects that take the 'platinum route' to open-access. Everyone is encouraged to work to make academic research free and freely accessible on the Web for one and all.

“_The Platinum Route is the voluntary, collaborative, no charge model that is usually overlooked in the debates on OA_.” ([Wilson 2007](#wil07))

## Introduction

In the 1990s, a lot of us got excited about the possibilities of exploring and exploiting the potential of existing Web technologies for the development of a new scholarly communication system. We had high hopes that this new system would give scholars and libraries an increased role in the scholarly communication system and provide libraries the necessary financial relief from the predatory practices of some large commercial publishers.

We were further encouraged by articles written by such scholars as Mike Sosteric ([1996](#sos96)), Bernard Hibbits ([1996](#hib96)), and Steve Harnad ([1991](#har91)). These scholars envisioned a future in which an independent, open-access, scholarly publishing model would flourish.

More than a decade has passed and this vision has not been realized.

Frankly, our challenge remains to tap into the true potential of our present day technology and move fearlessly towards an independent, open-access, scholarly publishing model.

## A backdrop to continuing subscription increases.

A serials crisis has plagued libraries for decades, now. Libraries have typically responded by cutting back on the acquisition of new serial and monographic titles. While in the past, this crisis was viewed, chiefly, as a library problem, today, this crisis is viewed more correctly as a crisis in the scholarly communication system itself, and as a threat to access. Its effects are felt not only in academic libraries but also all across campus in college and university classrooms and laboratories.

This crisis continues to be magnified by the pricing practices of large commercial publishers. A relatively small number of commercial publishers now control an ever increasing percentage of serial titles. Mergers and acquisitions within the publishing industry have exacerbated this trend.

In an April 19, 1999 press release by the American Library Association it was reported that 'Fourteen of 26 members of the editorial board of the Journal of Academic Librarianship... tendered their resignations in protest over increased prices imposed by Reed Elsevier, which purchased the journal from JAI Press in October 1998' ([American Library Association 1999](#ala99)).

## The International Consortium for alternative Academic Publication. (ICAAP)

It was in the summer of 1998 that I started seriously thinking about launching my own new, professional, independent, peer reviewed, open-access, electronic library journal. Finding a home for this proposed new electronic journal was a source of real frustration. It was my wish not to lose control of the journal to some library director or University official—or worse. Add to the mix, the fact that I did not have any funding. Regardless, I needed a stable site that would provide a permanent platform for the journal. It was my strong wish to have my journal free and freely accessible on the World Wide Web.

Ms. Elizabeth Dickerson, a colleague of mine at Southeastern Louisiana University who knew of my interests, connected me with Mr. Adam Chandler of the Energy and Environmental Information Resources Center at the University of Louisiana in Lafayette, Louisiana. At the suggestion of Mr. Chandler, I contacted Dr. Mike Sosteric, founder of the ICAAP, at Athabasca University in Athabasca, Alberta, Canada.

After the exchange of many e-mails with Dr. Sosteric, in which he quizzed me about the rationale for the new journal and other issues, I partnered with the International Consortium for alternative Academic Publication (later renamed the [International Consortium for the _Advancement_ of Academic Publication](http://www.icaap.org)), with no money and no support from any library organization or from my own University.

The ICAAP, is described [on its Website](http://www.icaap.org/about.php) as '...a research and development unit within Athabasca University. The ICAAP is devoted to the advancement of electronic scholarly communication. Their mission includes technological support, production, publication, and enhancement of scholarly journals and education resources, with the goals of greater accessibility, recognition and communication within the academic community'.

Lisa Guernsey acknowledges in the Chronicle of Higher Education (October 1998) that the ICAAP was most likely the 'first large-scale project to encourage scholars to publish their work on their own'.

My own experience partnering with the ICAAP has been a very positive one. The great part from my point of view is that the ICAAP services can be customized to each individual's needs. In the early days of the ICAAP, hosting journals and providing final production assistance were keys to their success.

Also, in their early days, the ICAAP explored SGML and XML as their _central technologies_. They actually developed an XML implementation, ICAAP eXtended Markup Language document system (IXML). IXML was both an extension and stripped down version of HTML. In more recent days, the ICAAP has migrated to the Open Journal Systems editorial management system, with IXML now a thing of the past.

The following is a list of current ICAAP services available to scholarly journals (see listing [on their Website](http://www.icaap.org/services.php):

*   Journal setup in OJS (Open Journal Systems)
*   Site layout/design
*   Input of archived issues
*   Article conversion
*   Site management
*   Domain name management
*   Site hosting
*   ISSN registration
*   Technical support
*   Archiving services
*   Inclusion in searchable ICAAP database
*   Comprehensive Web site statistics and user tracking
*   Indexing-submission of your site's Web pages to all major Web services

## The [Journal of Southern Academic and Special Librarianship](http://southernlibrarianship.icaap.org/indexv1.html) (JSASL)

_JSASL_ (ISSN 1525-321X) went live on the Web in June of 1999 with a test issue, which consisted of two features authored by me. They were entitled _A checklist of considerations for writing a book proposal_ and _Drug, pharmaceutical, and pharmacy-related sites_. Our plan was to publish three issues in each volume and, after the first issue, we decided it was important for us to go with a traditional peer-review system. I was not interested in reinventing the wheel, so I did a literature search of articles about peer reviewing and a set of guidelines was adapted from my readings, with input from similar guidelines used by other journals.

<figure>

![JSASL](../p321fig1.jpg)

<figcaption>

Figure 1: Front page of Volume 1 of _JSASL_</figcaption>

</figure>

Our second issue published in October of 1999 was our first peer reviewed issue. It consisted of four articles and one book review. The articles included _Metadata in a digital special library_, _Learning communities_, _INSPEC on FirstSearch_, and _A model job training program for summer youth_. The book review was entitled, _Flyer by night: a cautionary review_.

Our third issue of volume one was published in February of 2000\. It consisted of three articles and one book review. The articles included _Are transaction logs useful?_, _Cataloging expert systems_, and _Subject librarians' relations with faculty at the University of Botswana_. The book review was entitled, _This state has 'Boundries' (sic), but no focus_.

We learned from early prospective authors that it was important to them that our issues be permanently archived. Interestingly enough, we were approached, early on, by the National Library of Canada (now called the Library and Archives Canada) to have all of our articles permanently archived in their [Electronic Publications](http://epe.lac-bac.gc.ca/e-coll-e/inet-loc-e.htm) collection. Our articles are permanently archived also by [Athabasca University](http://southernlibrarianship.icaap.org/).

Originally, _JSASL_ was formed as a regional publication to fill a perceived gap in the literature of academic and special librarianship in the southern United States. It was (and is) a very exciting project, with elements from several of my interests converging all at once in a professional, electronic library journal. I have always had a strong interest in publishing. From November 1992 through June 1995, I edited _[Notes and Tracings](http://www.llaonline.org/fp/files/pubs/newsletters/ntv27_2.pdf)_, the official newsletter of the academic section of the Louisiana Library Association. For a year, I edited a column in the _LLA Bulletin_. By 1998, I already had two reference books published.

I also had a strong interest in the Web. In October 1998 I started as the Web Guide to Track and Field for the Miningco.com (New York, NY), now called [About.com](http://about.com/). For three years, I performed contract work for them which included writing feature articles, publishing a bi-monthly newsletter, maintaining and growing a list of classified links, a bulletin board and a chat room. So, my original interest in developing a stronger professional library literature in my region combined nicely with my strong interests with the World Wide Web and all of its possibilities. Founding _JSASL_, therefore, was a real joy, personally and professionally.

I worked hard to be a driving force in the Journal's growth. I handled the solicitation of articles and organized the peer review process. Mr. Chandler, our Deputy Editor, created the original Website formatting and continued handling the HTML markup for the articles during the first year of publication. At the start of the second year, Mr. Chandler resigned and was replaced by Ms. Jean Caswell, who continues today as the Web Site Coordinator and the Editor-in-Chief.

## E-JASL: The Electronic Journal of Academic and Special Librarianship

In 2002, we realized that we were definitely leaning towards a wider, more international readership. So, we changed our name to _[E-JASL: The Electronic Journal of Academic and Special Librarianship](http://southernlibrarianship.icaap.org/)_ (ISSN 1704-8532), dropping the regional emphasis from our name and from our focus.

<figure>

![EJASL](../p321fig2.jpg)

<figcaption>

Figure 2: Front page of _E-JASL_</figcaption>

</figure>

Our new goal was getting _E-JASL_ indexed and abstracted by major indexing and abstracting services in the field of librarianship. Through promotion of the journal on listservs and great persistence contacting indexing and abstracting services over the span of many years, I was able to convince _LISA: Library and Information Science Abstracts_ and _Library Literature and Information Science_ to retrospectively index _E-JASL_. _LISA_ indexes and abstracts _E-JASL_ beginning with volume number five. _Library Literature and Information Science_ indexes _JSASL/E-JASL_ beginning with volume one.

After the Journal became permanently archived and after we became indexed and abstracted, getting good manuscript submissions just got easier and easier for us. Those early years were a real struggle, though.

In our second year we published only six articles! The articles were entitled _The Americans with Disabilities Act compliance and academic libraries in the Southeastern United States_, _NORDINFO_, _Undergraduate full-text databases_, _A survey of four libraries in Kunming_, _Teaching information literacy in 50 minutes a week_ and _Integrating academic student support services at Loyola University_.

Looking back, we were able to persist by our own sheer will.

To summarize our growth, in the first four years we averaged 7.25 articles published a year. In our fifth and sixth year we averaged 12.5 articles published a year. We anticipate our average for the seventh and eighth years to be 19 articles a year. With the number of articles published continuing to go up, our quality has gone up, too. This year, for the first time, we will reject more manuscripts than we accept. The previous three years we rejected about 20% of the items we received. Prior to that, we rejected manuscripts at about a 10% average a year.

We feel we have made real progress towards reaching out to our international audience. In the first six months of 2007, we have averaged over 26,400 hits per month. In May 2007 we had a high of 30,477 hits.

Countries with the most page views include, in order, the United States, India, and Canada, followed by Great Britain, Japan, Nigeria, China, the Philippines, Germany, Norway, Ireland, South Korea, Australia, Malaysia, Thailand, Spain, South Africa, Ghana, Brazil, Poland, the Netherlands, Kenya, Bangladesh, Singapore, Sweden, Tonga, Hong Kong, Israel, New Zealand, Pakistan, Mexico, Italy, Iran, Russian Federation, Romania, France, Turkey, Vietnam, Barbados, Indonesia, Slovenia, Argentina, Botswana, Greece, Finland, Taiwan, Switzerland, and Saudi Arabia.

## Copyright

We believe that the authors of scholarly articles should retain the copyright of their own scholarly work. From our own [Web Guidelines](http://southernlibrarianship.icaap.org/accept.htm):

> Copyright belongs to the author. As publisher, we are merely providing a process for your intellectual property to be reviewed by and distributed to your peers. We believe this enlightened OPEN SOURCE spirit will become standard practice in the future. The ICAAP is a working example that shows that there really is an alternative to paying publishers hundreds and even thousands of dollars to buy back the work of our colleagues in academia.

## 'Platinum route' to open-access.

I published a speech in _E-JASL_ given by Gentry Holbert in Kiev a few years ago that included this (reputedly) ancient saying: 'May you live in interesting times'. These are truly interesting, changing, and challenging times. I feel that those of us who are involved in starting and developing open-access, electronic publishing projects are really pioneers, of sorts. It is my hope that academic libraries and scholars will expand their roles to include more and more publishing activities that, in the past, were reserved for university presses, scholarly societies, and the commercial presses, only.

It is my firm belief that all scholarly journal articles should be free and freely accessible. There never should be a charge to the readers, the authors, or the institutions for access.

The editors of E-JASL are proud of what they have accomplished and they will continue to take a leadership role in the _platinum route_ to open-access. Hopefully, the patterns of scholarly publishing will change to embrace the platinum route. Each of us has to do what we can to make this a reality. It is my belief that all we need is the collective will to do so!

Library school professors, particularly, need to show more leadership in the open-access movement. If you are not a part of an open-access journal, join a current project or start a project of your own. If you are an author, submit your work to an open-access journal. Be sure to make your work openly accessible. Don't be obscure!

Universities and libraries must become more and more involved in open-access publishing, too. It is time for college and university officials to get off of the sidelines. The ICAAP and Athabasca University example should serve as a model. College and University Presidents, Provosts, and Vice Presidents of Academic Affairs please note what can be done if you have the will.

## Acknowledgements

I am very, very thankful for Ms. Caswell and all of her contributions and I am certainly thankful, also, to Dr. Sosteric for including _JSASL_ in ICAAPs original demonstration group of thirty scholarly journals. I am thankful, too, that our publishing costs have been picked up 100% by the ICAAP and Athabasca University. All and all, I believe that Sosteric has successfully demonstrated to the world that it is possible to form independent scholarly journal projects outside of the commercial mainstream. Furthermore, I am thankful to Mr. Adam Chandler for his Web expertise in those early years and for his concern for quality.

Although I have never met in person either Dr. Sosteric or Mr. Chandler, my long distance collaboration with them has been crucial to the start-up and growth of our successful e-journal.

## References

*   American Library Association (ALA) (1999). [More than half of JAL board resigns over subscription increase](http://www.ala.org/ala/alonlin/currentnews/newsarchive/1999/April1999/morethanhalf.cfm%3E). ALA news release, April 19, 1999\. Retrieved 19 July, 2007 from http://www.ala.org/ala/alonlin/currentnews/newsarchive/1999/April1999/morethanhalf.cfm
*   Guernsey, L. (1998, October 30). Research libraries newsletter examines profits of journal publishers. _Chronicle of Higher Education_. Available from http://chronicle.com/daily/98/10/98103002t.htm (Subscription needed).
*   Harnad, S. (1991). [Post-Gutenberg galaxy: the fourth revolution in the means of production of knowledge](ftp://ftp.princeton.edu/pub/harnad/Harnad/HTML/harnad9/postgutenberg.html). _Public-Access Computer Systems Review_, **2**(1), 39-53\. Retrieved 20 July, 2007 from ftp://ftp.princeton.edu/pub/harnad/Harnad/HTML/harnad9/postgutenberg.html
*   Hibbitts, B. (1996). [Last writes? Re-assessing the Law Review in the age of cyberspace](http://www.firstmonday.org/issues/issue3/hibbitts/). _First Monday_, **1**(3). Retrieved July 20, 2007 from http://www.firstmonday.org/issues/issue3/hibbitts/
*   Holbert, G.L. (2002). [Technology, libraries and the Internet: a comparison of the impact of the printing press and World Wide Web](http://southernlibrarianship.icaap.org/content/v03n01/Holbert_g01.htm) . _E-JASL_, **3**(1-2). Retrieved 23 July, 2007 from http://southernlibrarianship.icaap.org/content/v03n01/Holbert_g01.htm
*   Sosteric, M. (1996). [Electronic journals: the grand information future?](http://www.sociology.org/content/vol002.002/sosteric.html) _Electronic Journal of Sociology_, **2**(2). Retrieved 20 July, 2007 from http://www.sociology.org/content/vol002.002/sosteric.html
*   Sosteric, M. (1999). [ICAAP eXtended markup language: exploiting XML and adding value to the journals production process.](http://www.dlib.org/dlib/february99/02sosteric.html) _D-Lib Magazine_, **5**(2). Retrieved 20 July, 2007 from http://www.dlib.org/dlib/february99/02sosteric.html
*   Wilson, T.D. (2007, April 19). [Re: Bundesrat decision](http://threader.ecs.soton.ac.uk/lists/boaiforum/1078.html) [Msg. 1078]. Message posted to http://threader.ecs.soton.ac.uk/lists/boaiforum/. Retrieved 3 August, 2007 from http://threader.ecs.soton.ac.uk/lists/boaiforum/1078.html