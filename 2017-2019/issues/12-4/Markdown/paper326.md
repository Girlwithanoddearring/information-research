#### Vol. 12 No. 4, October, 2007

* * *

# Predictors of cancer information overload: findings from a national survey

#### [Kyunghye Kim](../kim@ci.fsu.edu), [Mia Liza A. Lustria](../mlustria@ci.fsu.edu), [Darrell Burke](../dburke@ci.fsu.edu),  
College of Information 
Florida State University Tallahassee,  
FL 32306-2100,  
USA  
#### [Nahyun Kwon](../nkwon@chuma1.cas.usf.edu)  
School of Library and Information Science,  
University of South Florida Tampa,  
FL 33620-7800,  
USA

#### Abstract

> **Introduction.** We explore predictors of information overload among cancer information seekers who reported having suffered from information overload. These persons were characterized by socio-demographic characteristics, health status, health information and communication environment and behavioural, cognitive, and affective cancer information seeking.  
> **Method.** A secondary analysis was performed of the 2003 Health Information National Trends Survey conducted by the U.S. National Cancer Institute with 6,369 randomly selected participants. A subset of this dataset, which includes the responses of 3,011 cancer information seekers, was analysed. A bivariate analysis was used to identify factors significantly associated with information overload. These factors were then entered in a logistic regression model to identify predictors of overload.  
> **Results.** Lower socio-economic status, poor health, low media attentiveness and high affective components of information seeking were associated with overload. The strongest predictors were education level and cognitive aspects of information seeking, which indicates that overload is strongly predicted by health information literacy skills. Use of the Internet and high media attentiveness, two factors usually thought to cause overload, were found not to be associated with overload.  
> **Conclusion.** The findings emphasize the importance of health information literacy in coping with information overload and implies the need to design better health information campaigns and delivery systems.

## Introduction

In today's vast and diverse information marketplace, critics report that information overload, a common term for receiving too much information, seems to have become an everyday problem. The health information marketplace is no exception. Information overload is a widespread problem for physicians and patients, for the general public and the health care industry alike. Over 400,000 articles are reportedly entered into biomedical literature each year ([Davis _et al._ 2004](#dav04)). In addition, health and medical information are increasingly becoming available to the general public in numerous formats through various sources and channels.

The problem of information overload is especially dire for cancer information seekers for a variety of reasons. Cancer is a complicated disease of over 200 different types, many of which can be contracted at any age. Cancer is one of the most productive areas of clinical research, resulting in the massive growth of cancer information every year ([Stewart and Kleihues 2003](#ste03)). The ever-growing popularity of the Internet has both ameliorated and exacerbated the problem, as it offers lay people easier access to more cancer information while also increasing their risk of receiving inaccurate or misleading information.

Cancer information overload was evidenced in a recent Health Information National Trends Survey of 6,369 persons, conducted by the [National Cancer Institute](#nci03) (2003). Results of that survey reveal that in 2003, seven in ten Americans, or an estimated 149 million people, felt that there were so many recommendations about cancer information that they were confused. Furthermore, almost half of Americans (46%) had a fatalistic belief that 'everything causes cancer.' Such false beliefs and confusion over cancer information are likely to have negative effects on public health ([Doolittle and Spaulding 2005](#doo05); [Gurmankin and Viswanath 2005](#gur05) ). Overload was even more highly pronounced among the respondents who reported having sought cancer information purposively (n=3,011). Among these cancer information seekers, a great majority (2,171 respondents) reported that they suffered from overload.

For health information professionals, this phenomenon brings important questions to the fore. What are the characteristics of people who suffer from cancer information overload? What factors are significantly associated with overload? The answers to these questions have important implications on how we should deliver health information and assess future information services, especially for people who may not have the skills needed to sort through massive amounts of cancer information.

To examine the factors that lead to overload among cancer information seekers and to propose appropriate interventions for at-risk groups, we need to approach overload using an interdisciplinary perspective. An interdisciplinary approach is required because health information seeking is a very rich and dynamic problem-solving process involving the interplay of cognitive, affective and social events. As such, we propose and are guided by a conceptual framework that illustrates selected antecedents of information seeking in addition to behavioural, cognitive and affective components of information seeking that might relate to perceptions of overload.

We conducted a series of nonparametric tests and a logistic regression analysis to determine which set of factors predicts individuals' perceptions of overload. This exploratory study is one of the first studies seeking to probe the relationship between overload and information seeking variables using a nationally-representative sample. We hope that this empirical investigation on cancer information overload will move us a step closer to designing better information campaigns and information services to assist individuals of varied literacy levels and in meeting their specific health information needs.

## Literature review

### The changing environment of health information seeking

Studies on health information seeking have revealed that higher educated individuals seek information, read more, use professional sources to a larger extent and have less trouble evaluating information ([Ek 2004](#eks04)). Health information seeking has been found to be critical in improving coping skills, reducing stress, improving understanding of the cancer disease process and in promoting social support ([Van Der Molen 1999](#van99)). Researchers have also long recognized that health information seeking is far from being a linear process or a single event. Situational factors such as time pressure for searching information, as well as individual factors such as sex, attitudes and age are known to influence an individual's information needs and seeking behaviour ([Ankem 2006](#ank06); [Illic _et al._ 2005](#ill05); [Pandey _et al._ 2003](#pan03); [Wicks 2004](#wic04)).

The use of the Internet for health and health care has changed the roadmap of health communication and information seeking (see [Eysenbach 2001](#eys01) for a systematic review). Traditionally, health and medical information was mostly obtained from health care professionals and/or friends or family members ([Johnson 1997](#joh97)). Today, health-oriented consumers typically seek out a variety of health and medical information and use various sources of information, including interpersonal sources, mass media and, increasingly, the Internet ([Fox 2005](#fox05)). The growing population of Internet users sees the Web as an important health resource. Recent reports have revealed that close to ninety-five million Americans seek health information online ([Fox 2005](#fox05)) and each day more than twelve-and-a-half million health-related searches are conducted on the Web ([Eysenbach 2003](#eys03)). Furthermore, the Internet has changed health consumers' source selection behaviour, preferences and their channel reliance. For instance, one study revealed that consumers may bypass entirely some of the more traditional sources of health-related information and use the Internet instead ([Case _et al._ 2004](#cas04)).

As Cline and Haynes ([2001](#cli01)) note, potential benefits of online health information seeking include: widespread access to health information, interactivity, tailoring of information, potential to facilitate interpersonal interaction and social support, and potential for anonymity. At the same time, the new communication and information environment poses a serious challenge to health information seekers. Not all reports of clinical studies are credible and many reports are conflicting. A recent study found that the results of fourteen of forty-nine most highly cited medical studies were contradicted or downplayed by later research ([Ioannidis 2005](#ioa05)). The general public, therefore, is in danger of being exposed to conflicting medical reports, which is likely to increase confusion and distrust of health news. Thus, current concerns about the quality of health information sources and the importance of educating the general public about how to evaluate health sources, are well-founded ([Adams and Berg 2004](#ada04) ; [Charnock and Shepperd 2004](#cha04); [Dolan _et al._ 2004](#dol04); [Dutta-Bergman 2004](#dut04); [Morahan-Martin 2004](#mor04)).

The perceived effects of online health information seeking on public health and patient empowerment have urged health science researchers to compare salient characteristics of online information seekers and offline information seekers. Some studies have found that age and socio-economic measures, such as income and education, help predict whether individuals seek health information online or offline ([Cotton and Gupta 2004](#cot04)). This echoes the findings of a Harris Interactive survey ([Taylor 2002](#tay02)) which found that people who go online for health information tend to be younger, better educated and more affluent than the general population. Online health information seekers are also found to be more health-oriented than non-seekers ([Dutta-Bergman 2004](#dut04)). Patients are becoming more proactive and want to become active participants in the process of care and clinical decision making ([Levinson _et al._ 2005](#lev05)). Cancer patients find online information very helpful, although potentially overwhelming in quantity ([Ziebland _et al._ 2004](#zie04)). There is no question that good health decisions depend on good health information ([Medical Library Association 2005](#mla05)). However, individuals who are exposed to too much information may be more likely to become confused and make poor health decisions that can potentially have harmful effects on outcomes ([Eysenbach 2003](#eys03)).

### Information overload

Information overload has been the subject of research since George Miller's famous study ([1956](#mil56)) on how many bits of information cause overload, but there are only a handful of definitions of information overload in the literature. Akin ([1997](#aki97)) reviewed overload research in library studies, consumer science and psychiatry and psychology, and concluded that no commonly-accepted definition for information overload exists. Wilson's definition of overload at the personal level is notable as it highlights some common elements:

> ...a perception on the part of the individual (or observers of that person) that the flow of information associated with work tasks is greater than can be managed effectively and a perception that overload in this sense creates a degree of stress for which his or her coping strategies are ineffective. (Wilson [2001](#wil01): 113).

Definitions of information overload also typically allude to: too much information coming in, ineffective information management, stress (or anxiety) and ambiguity. Information overload might occur not only in work environments but also in day-to-day or general situations, which calls for a definition that is not confined to work tasks. Further, overload resulting from a general impression that there is too much information in a certain area may be different from that resulting from purposeful information seeking. Thus, we define information overload as _a perception of being overwhelmed and, thus, confused by information coming in that might hinder learning or impair users' ability to make informed decisions_.

Information overload has been found to yield profound physical, mental, emotional and social effects ([White and Doman 2000](#whi00)). A British psychologist, Lewis, coined the phrase _information fatigue syndrome_ to refer to a situation in which individuals become physically ill from an overload of information ([Reuters 1996](#reu96)). Information overload has also been thought to result in _analysis paralysis_ ([White and Doman 2000](#whi00)) and in making it increasingly difficult to identify and select relevant information ([Eppler and Mengis 2004](#epp04)). Given that an abundance of health information does not always translate into informed choices ([Ivanitskaya _et al._ 2006](#iva06)), it is not surprising that some researchers argue that the issue of overload calls for a change in the role of information professionals from helping users identify and access all relevant information to “protecting users from information” ([Bawden, _et al_. 1999: 254](#baw99)).

Certainly, the complexity of the information environment, as mentioned earlier, seems to be a significant contributor to overload. Researchers have found the major causes of overload to be: the proliferation of communication and information; comprehensive and widespread automated means of access to information ([Biggs 1989](#big89)); diversity and repetitiveness of information ([Biggs 1989](#big89)); and the changing nature of work, deregulation and globalization ([Wilson 1996](#wil96)). Eppler and Mengis ([2004](#epp04)) proposed a systematic framework highlighting five causes of overload: personal factors (i.e., attitude, qualification, experience, motivation, etc.); nature of information task and process (i.e., complexity, occurrence, time pressure, etc.); task and process parameters; organizational design; and information technology (i.e., use and misuse of information technology, etc.). In a more succinct way, Marcusohn (as cited in [Hall and Walton 2004](#hal04)) proposed that the nature of information, an individual's processing capacity and individual needs and desires, can together explain overload. That is, when carried over into cancer information seeking, overload may then be associated with the complex and massive growth of cancer information and how it is presented and disseminated; an individual's attention to cancer information and ability to read, listen to and understand cancer literature; and situational factors such as one's motivation (including history of cancer in the family, etc.) and efforts to search for cancer information. Currently, however, there is very little evidence about the nature and impact of information overload within the general health setting or more specifically among cancer patients ([Hall and Walton 2004](#hal04); [Tidline 1999](#tid99)).

### Cancer information seeking and overload

Some of the main foci of the cancer information seeking literature are: how people seek health information, what factors affect their health information seeking and source selection behaviour and how these consequently affect their health decision making and eventually their health behaviour ([Turner _et al_. 2006](#tur06)). More recently, the conduct of the Health Information National Trends Survey (hereafter, 'National Trends Survey') and other systematic and empirical research emphasize how information seeking has emerged as an important topic in the health communication field (see [Journal of Health Communication 2006](http://www.webcitation.org/5RLJTsXc2), for example). The National Trends Survey is the first to provide in-depth data from a nationally-representative sample on how the general public utilize both traditional and new media to meet cancer information needs ([National Cancer Institute 2003](#nci03)).

Most of the studies analysing the National Trends Survey dataset have been conducted by researchers in public health, health education, or health communication. They have identified socio-demographic characteristics of, among other categories, cancer information seekers vs. non-seekers ( [Rutten _et al._ 2006](#fin06b)); information seeking preferences and experiences of an underserved population ([Nguyen and Bellamy 2006](#ngu06)); and information seeking patterns for cancer screening utilization by specific types of cancer patients ([Ling _et al._ 2006](#lin06)). Most studies focus on behavioural aspects of health information seeking (i.e., measuring _information seeking_ as whether or not individuals seek health information and if so, how often, etc.), and very few have taken into account the complexity and cognitive aspects of information seeking. These studies all have important implications for public health promotion, patient empowerment and quality of health communication, but they also reveal a good opportunity for health information professionals to contribute more to this interdisciplinary discourse.

One 2003 National Trends Survey study, which included an analysis of overload, found that individuals who paid less attention to, and who are less trusting of, health news, reported feeling overwhelmed by the number of recommendations available ([Gurmankin and Viswanath 2005](#gur05)). This led Gurmankin and Viswanath (2005) to conclude that this group may miss critical cancer information and may not believe important or credible information. Their results provide evidence that many Americans perceive information overload, but largely attributed this to exposure to mass media. The current study proposes that media use (or media attention) is only one factor that might contribute to perceptions of information overload.

A host of studies has suggested that self-discipline and time management skills ([Cohen 2005](#coh05)), as well as information literacy (see [Bawden _et al_. 1999](#baw99), for example), are partial solutions to overload. Information literacy is the ability to access, evaluate and use information from a variety of sources. The Medical Library Association ([2005](#mla05)) defines health information literacy as 'the set of abilities needed to: recognize a health information need; identify likely information sources and use them to retrieve relevant information; assess the quality of the information and its applicability to a specific situation; and analyse, understand and use the information to make good health decisions.' Andrews _et al._ ([2005](#and05)) note that many people lack the requisite health information literacy skills to use health information effectively. Such literacy barriers contribute to disparities in health care, in general.

Cancer information seeking, as explored in information science, shows how people receive and act upon information communicated to them from a variety of available information carriers ([Andrews _et al_. 2005](#and05)), how to assess the information needs and how to deliver health information and services by partnering with other community resources, etc. Few of these studies have investigated how health information seekers actually battle and cope with health information overload, or the factors that might cause this. The dearth of empirical research exploring overload in the context of health underlines a new avenue for health information seeking research and calls for a new conceptual model of health information seeking.

### A conceptual model of health information seeking

To guide the current study, we first developed a conceptual model for examining cancer information seeking and overload that draws upon the National Trends Survey framework (National Cancer Institute [2003](#nci03)), Weinstein's Precaution Adoption Process Model ([Weinstein _et al._ 1998)](#wei98) and the National Center for Research on Evaluation, Standards and Student Testing (CRESST) Model of Problem Solving ([O'Neal and Schacter 1997](#one97)). The present model, as illustrated in Figure 1, incorporates the National Trends Survey framework by highlighting and specifying factors that might affect health information seeking and may lead to overload, including: individual factors, the health communication and information environment, along with behavioural, cognitive and affective elements of problem solving. The model depicts overload as a potential by-product of the cancer information seeking process.

The present model extends Johnson's ([1997](#joh97)) Comprehensive Model of Information Seeking, which stresses how antecedents of information seeking and channel characteristics influence _action_ (that is, information seeking). As in the CRESST Model of Problem Solving, [(O'Neal and Schacter 1997](#one97)), in order to be a successful information seeker, one must: know the content of the problem at hand (domain knowledge); be aware of intellectual and technical tricks (problem-solving strategies and technical tactics); be able to plan and monitor one's progress toward solving the problem (metacognition); and be motivated to perform tasks (effort and confidence in finding information). The present model thus incorporates _cognitive and affective elements_ of information seeking, i.e., content knowledge, problem-solving strategies, and motivation of the problem solving.

Finally, this study proposes information overload as one by-product of the cancer information seeking process. Other potential outcomes are not addressed due to limitations of this secondary data analysis.

<figure>

![Figure 1](../p326fig1.jpg)

<figcaption>

**Figure 1: Conceptual framework depicting the potential predictors of cancer information overload**</figcaption>

</figure>

## Research questions

This study aimed to explore characteristics of cancer information seekers who suffer from overload and to determine the implications of these findings for information professionals using the 2003 National Trends Survey dataset. Since measures of overload are limited in the existing dataset, this study was largely exploratory in nature and did not attempt to define any potential causal relationships or test hypotheses. Instead, as a first step, we sought to identify predictors of overload in order to generate hypotheses for future research. Specifically, we explored how socio-economic status, the richness of one's health information and communication environment and how behavioural, cognitive and affective aspects of cancer information seeking are associated with individuals' perceptions of overload. We addressed the following research questions:

1.  Are socio-demographic characteristics of cancer information seekers (as indicated by education, employment, income and health insurance) associated with perceptions of cancer information overload?
2.  Is health status (as indicated by general health, depression, history of cancer) of health information seekers associated with perceptions of overload?
3.  Is the health communication and information environment (as indicated by quality of provider-patient interaction, media attentiveness and social networks) of health information seekers associated with perceptions of overload?
4.  Are behavioural aspects of cancer information seeking (as indicated by the use of the Internet to seek cancer information and frequency of cancer information seeking) associated with overload?
5.  Are cognitive aspects of cancer information seeking (as indicated by cancer literacy, cancer knowledge, awareness of cancer resources, search expertise, concerns for quality of information and trust in online cancer information) associated with overload?
6.  Are affective aspects of cancer information seeking (as indicated by frustration and confidence in finding information) associated with overload?

## Method

### Health Information National Trends Survey

The National Trends Survey is an ongoing cross-sectional survey of the U.S. civilian, non-institutionalized, adult population, developed by health communication scholars and staff of the National Cancer Institute and conducted by an independent company, Westat. The survey instrument includes validated measures selected from existing surveys and new items, which underwent extensive pre-testing and expert review ([National Cancer Institute 2003](#nci03)). The Institute made the dataset publicly available to encourage researchers to explore the health information seeking behaviours of Americans from a variety of perspectives. Data from the 2002-2003 administration of the National Trends Survey were used for this study. The 148-item survey questionnaire is available at [the Survey Website](http://www.webcitation.org/5RKu4gtQi).

The respondent pool was obtained through random-digit dialling from all telephone exchanges in the United States, with over-sampling of exchanges with high numbers of African Americans and Hispanics ([Rizzo 2003](#riz03)) and females (60%). During the household screening, one adult was sampled within each household and recruited for the extended interview (n=6,369). Of this number, 12% reported having ever been diagnosed with cancer and 63% reported having ever had a close family member who had been diagnosed with cancer. Only 47% of the respondents (n=3,011) who reported that they had looked for information about cancer from any source were selected for inclusion in this study. In other words, this study did not attempt to examine the general perceptions of overload from those who had not purposefully looked for cancer information.

### National Trends Survey measures

The following section outlines items from the National Trends Survey dataset included in the current investigation as guided by the conceptual framework. We recoded some items in the dataset to fit the purpose of our study, which may result in some loss of detail during analysis. Researchers often analyse the data by collapsing original survey answer categories into a smaller number for two reasons: a small number of categories tends to be more effective in detecting the patterns hidden in the raw data and the presentation of the results with fewer categories is more readable and understandable ([Babbie 2004](#bab04); [De Vaus 2002](#dev02)). When collapsing categories, we adopted either substantive recoding or distributional recoding by considering the nature of the variable and the distribution pattern of the data. Substantive recoding refers to a rearrangement of values based on their real meaning and is frequently used in Likert-type scales where both 'strongly agree' and 'agree' are collapsed into one group and 'strongly disagree' and 'disagree' are collapsed into another group. Distributional recoding refers to a rearrangement of values by dividing a distribution into equal proportions, so that each category has similar number of cases. If a researcher intends to compare people with high and low income groups, for example, the researcher could use the median of the distribution as a cut-off value for raw data that were originally reported as either ordinal or interval level data ([De Vaus 2002](#dev02)).

In this study, all multiple-item scales used four-point Likert response formats. In addition, items were recoded as necessary so higher scale scores indicated higher endorsement of variables. We adopted common practices of collapsing categories from the existing National Trends Survey literature, unless otherwise indicated.

#### Cancer information overload

As stated earlier, cancer information overload was operationally defined for this study as _a perception of being overwhelmed and confused by information coming in that might hinder learning or impair users' ability to make informed decisions_. Because of the limitations of secondary data analysis, we measured overload using one item from the National Trends Survey: 'There are so many different recommendations about preventing cancer, it's hard to know which ones to follow.' Respondents were asked to answer the question on a four-point scale, ranging from _strongly agree_ (=1) to _strongly disagree_ (=4). This variable was reverse coded so that higher values indicated greater degrees of overload. The variable was then dichotomized to indicate two groups: those who reported overload (=1) and those who did not ( = 0). This was done to facilitate analysis of groups at risk for information overload.

#### Socio-demographic characteristics

The following socio-demographic characteristics were measured: age (collapsed into 18-34, 35-64 and +65); sex; education (collapsed into >=high school, some college, and college graduate); race (recoded into Hispanic, non-Hispanic white, non-Hispanic black or African American and non-Hispanic other or multiple); health insurance (yes/no); annual household income (recoded into &lt;\$25,000, \$25,000-\$50,000 and &gt;\$50,000); and employment status (collapsed into employed for wages or self-employed, out of work, a homemaker, a student, retired and unable to work).

#### Health communication and information environment

Respondents' health communication and information environment was measured by asking the respondents whether and how much, they were exposed to formal and informal sources and channels of health information. Participants were also asked whether someone had looked for cancer information on their behalf (proxy information seekers) or whether they had looked for information themselves. They were also asked about the quality of their interaction with their providers; their media attentiveness; membership in online health support groups; and e-health communication (whether they used the Internet to communicate with doctors).

The quality of provider–patient interactions was measured using the following: _How often, in the past 12 months, did your provider: (1) listen carefully, (2) explain things understandably; (3) show respect; (4) spend enough time; and (5) involve you in health care decisions?_ Participants were asked to respond using a four-point scale, ranging from _always_ (=1) to _never_ (=4). The composite variable was reverse-coded and dichotomized (high/low) using the mean as a cut-off point and with higher values indicating higher quality.

Media attentiveness was measured using the following: _How much attention do you pay to information about health or medical topics on/in [television, radio, newspapers, magazines and the Internet]?_ Reponses were coded on a four-point scale, ranging from _a lot_ (=1) to _not at all_ (=4). Using the same method that we used to measure quality of provider–patient interaction, we dichotomized the composite variable of media attentiveness (high or low) with higher values indicating higher attentiveness.

Membership in online support groups was measured by asking the respondents whether they have used the Internet in the past twelve months to participate in an online support group for people with a similar health or medical issue (yes/no). E-Health communication was measured by asking the respondents if they have used e-mail or the Internet to communicate with a doctor or a doctor's office (yes or no). Proxy information seeking was measured by asking: _Excluding your doctor or other health care provider, has someone else ever looked for information about cancer for you?_ (yes/no).

#### Health status

As in the study by Rutten _et al._ ([2006](#fin06a)), a set of question items on health status were selected: perceived general health, depression and cancer history. The respondents were asked to rate their health from _excellent_, _very good_, _good_, _fair_, to _poor_. We collapsed the variable into _excellent/very good, good_, and _fair/poor_ for this study.

Depression was measured by asking the respondents the amount of time they experienced the following six feelings: (1) _so sad that nothing could cheer you up_, (2) _nervous_, (3) _restless or fidgety_, (4) _hopeless_, (5) _that everything was an effort_ and (6) _worthless_. Participants responded on a five-point scale, ranging from _all of the time_ (=1) to _none of the time_ (=5). We created a composite variable for depression, reverse-coded it so that higher values indicated greater feelings of depression and created a tertile distribution of _none to low_, _moderate_ and _high_.

Cancer history was measured by combining two questions: (1) if they had ever been diagnosed with cancer (yes/no); and (2) if any of their close family members had ever been diagnosed with cancer (yes/no). Therefore, respondents fell into either of four categories: reported cancer for both self and family; self only; family only; or did not report any personal and immediate experience with cancer.

#### Cancer information seeking

_Looked for cancer information online_  
Online cancer information seeking was measured with one item: _Have you ever visited an Internet web site to learn specifically about cancer?_ (yes/no).

_Frequency of Internet use for information about cancer_  
Frequency of Internet use was measured using the following item: _In the past 12 months, how often have you used the Internet to look for advice or information about cancer?_ The original categories of _once a week_, _once a month_, _every few months_, or _less often_ were used for this study.

_Cancer knowledge_  
To measure knowledge about cancer, we adopted the _cancer knowledge index_ developed by Shim _et al._ ([2006](#shi06)) for their National Trends Survey study. The index consists of six items, including: awareness of the impact of several risk factors for cancer, knowledge about recommended daily allowance for fruits and vegetables and awareness of specific screening tests. A composite variable for cancer knowledge was created by adding up scores. Cancer knowledge was then dichotomized into _high_ and _low_ using the median as a cut-off point.

_Cancer literacy_  
Respondents were asked to assess whether the cancer information they found was too hard to understand using a four-point scale of _strongly agree_ (=1) to _strongly disagree_ (=4). We then dichotomized the variable using the average score as a cut-off point (high/low). Higher values indicated greater cancer literacy. We note that in reality, cancer literacy involves more than just an understanding of health content. However, given the limitations of having to deal with an existing dataset, cancer information literacy was operationalized based on just one item.

_Awareness of cancer resources_  
Five items assessed awareness of the following national cancer resources: (1) NIH; (2) ACS; (3) NCI; (4) CIS; and (5) the 1-800-4-CANCER information line (yes/no). Scores were combined and the resulting variable was dichotomized using the median score as a cut-off point (high/low) with higher values indicating greater awareness of cancer resources.

_Trust in cancer information from the Internet_  
Respondents were asked to assess whether they would trust the information about cancer from the Internet using a four-point scale: _a lot_ (=1) to _not at all_ (=4). The variable was reverse-coded so that higher values indicated greater trust. Trust in online cancer information was then re-categorized into _a lot/some_ and _a little/ not at all_.

_Concern about the quality of information_  
Respondents were asked to indicate how much they agreed with the following question: _You were concerned about the quality of the information about cancer_. A four-point scale of _strongly agree_ to _strongly disagree_ was used to measure this. The variable was reverse-coded so that higher values indicated greater concern about the quality of cancer information. This was then re-categorized into _strongly agree/ somewhat agree_ and _strongly disagree/ somewhat disagree_.

_Search expertise_  
We created a composite variable for search expertise using these two items: _You wanted more information, but did not know where to find it_ and _It took a lot of effort to get the information you needed_. Respondents were asked to indicate agreement with the items using a four-point scale of _strongly agree_ (=1) to _strongly disagree_ (=4). We then reverse-coded the values so that lower scores indicated lower search expertise. We then dichotomized the variable into _high_ and _low_ search expertise using the median as a cut-off point.

_Confidence in finding information_  
Respondents were asked to indicate how confident they were that they could get advice or information about cancer if they needed it. A four-point scale of _very confident_ (=1) to _not at all_ (=4) was used to measure this. The variable was reverse-coded so that higher values indicated greater confidence in finding information. The categories were then collapsed into _very confident_ and _somewhat confident or less_.

_Frustration during the information search process_  
Respondents were asked to indicate their agreement with the statement: _You felt frustrated during your search for the information_, A four-point scale of _strongly agree_ (=1) to _strongly disagree_ (=4) was used to measure this. The variable was reverse-coded so that higher values indicated greater frustration in the information search process. Categories were then collapsed into _strongly agree_, _somewhat agree/disagree_, and _strongly disagree_.

### Statistical analyses

A series of Chi-squared tests were conducted to examine whether significant differences existed between the individuals who reported having experienced cancer information overload and those who did not, in terms of the following variables: socio-demographics, health status, exposure to health communication/information environment and cancer information seeking. We then conducted logistic regression analysis to determine which of these variables could significantly predict the probability of cancer information overload occurring in the sample.

Logistic regression analysis allows how well those variables can predict or explain cancer information overload by assessing _goodness of fit_. For logistic regression analysis, the 'responses of a dichotomous variable should be coded as 0 and 1 and the value of 0 should be assigned to whichever response indicates an absence of the characteristic of interest' ([Pallant 2005: 162](#pal05)). Accordingly, the value of 1 was used to represent respondents who expressed having experienced cancer information overload and the value of 0 was used to represent respondents who had not. Since we were interested in generating (rather than testing) hypotheses, forward stepwise logistic regression analysis was used to discover relationships among the variables ([Tabachnick and Fidell 1996](#tab96)). Forward stepwise procedure also enabled us to specify a group of variables that had the best predictive power. Data were analysed using SPSS version 13.0.

## Results

### Sample background

The majority of the sample population is between 35 and 64 years of age (61%) and female (69%). The racial background of respondents closely resembles the [2005 US Census Bureau](http://factfinder.census.gov/servlet/ACSSAFFFacts?_submenuId=factsheet_1&_sse=on) data, with the majority being non-Hispanic white (76%). About 70% of the sample has some college education or college degrees and some 53% report annual house incomes of $50,000 or lower. About 91% of the respondents have health coverage (such as insurance or Medicare). Employment status shows that 62% are employed for wages or are self-employed, whereas 15% are retired and 9% are homemakers. Almost 62% of the respondents reported that they have a personal or family history of cancer.

### Factors associated with cancer information overload

<table><caption>

**Table 1: Characteristics of health information seekers with cancer information overload and without the overload**
(Note: *p <.05, **p <.01, ***p <.001 )</caption>

<tbody>

<tr>

<th rowspan="2">Variables</th>

<th>CIO</th>

<th>Non-CIO</th>

<th rowspan="2">

χ<sup>2</sup></th>

</tr>

<tr>

<th>

(n=2, 171) Valid %</th>

<th>

(n=736) Valid %</th>

</tr>

<tr>

<th colspan="4">

**_Demographics & socio-economic status_**</th>

</tr>

<tr>

<td colspan="4">

**Age** (n=2,896)</td>

</tr>

<tr>

<td>18-34</td>

<td>24.1</td>

<td>26.2</td>

<td rowspan="3">1.3</td>

</tr>

<tr>

<td>35-64</td>

<td>60.8</td>

<td>59.5</td>

</tr>

<tr>

<td>65</td>

<td>15.1</td>

<td>14.3</td>

</tr>

<tr>

<td colspan="4">

**Sex** (n=2,907)</td>

</tr>

<tr>

<td>Male</td>

<td>30.6</td>

<td>32.6</td>

<td rowspan="2">1.0</td>

</tr>

<tr>

<td>Female</td>

<td>69.4</td>

<td>67.4</td>

</tr>

<tr>

<td colspan="4">

**Race or ethnicity** (n=2,087)</td>

</tr>

<tr>

<td>Hispanic</td>

<td>7.3</td>

<td>8.9</td>

<td rowspan="4">2.1</td>

</tr>

<tr>

<td>Non-Hispanic White</td>

<td>76.2</td>

<td>75.1</td>

</tr>

<tr>

<td>Non-Hispanic Black/African American</td>

<td>10.8</td>

<td>10.9</td>

</tr>

<tr>

<td>Non-Hispanic (other or multiple)</td>

<td>5.7</td>

<td>5.1</td>

</tr>

<tr>

<td colspan="4">

**Education** (n=2,824)</td>

</tr>

<tr>

<td>≤ High school</td>

<td>32.2</td>

<td>21.7</td>

<td rowspan="3">45.3***</td>

</tr>

<tr>

<td>Some college</td>

<td>29.9</td>

<td>26.8</td>

</tr>

<tr>

<td>College graduate</td>

<td>37.9</td>

<td>51.5</td>

</tr>

<tr>

<td colspan="4">

**Health coverage of any type** (n=2,817)</td>

</tr>

<tr>

<td>Yes</td>

<td>90.5</td>

<td>91.7</td>

<td rowspan="2">0.9</td>

</tr>

<tr>

<td>No</td>

<td>9.5</td>

<td>8.3</td>

</tr>

<tr>

<td colspan="4">

**Annual household income** (n=2,632)</td>

</tr>

<tr>

<td>

&lt;\$25,000</td>

<td>24.7</td>

<td>19.2</td>

<td rowspan="3">21.3***</td>

</tr>

<tr>

<td>

\$25,000 - \$50,000</td>

<td>30.7</td>

<td>26</td>

</tr>

<tr>

<td>

&gt;\$50,000</td>

<td>44.6</td>

<td>54.8</td>

</tr>

<tr>

<td colspan="4">

**Employment status** (n=2,108)</td>

</tr>

<tr>

<td>Employed for wages/Self-employed</td>

<td>60.7</td>

<td>64.7</td>

<td rowspan="6">19.2**</td>

</tr>

<tr>

<td>Out of work</td>

<td>5.2</td>

<td>4.4</td>

</tr>

<tr>

<td>A homemaker</td>

<td>8.7</td>

<td>9.4</td>

</tr>

<tr>

<td>A student</td>

<td>4.6</td>

<td>6.2</td>

</tr>

<tr>

<td>Retired</td>

<td>15.5</td>

<td>13.1</td>

</tr>

<tr>

<td>Unable to work</td>

<td>5.5</td>

<td>2.2</td>

</tr>

<tr>

<td colspan="4">

**_Health status_**</td>

</tr>

<tr>

<td colspan="4">

**Perceived general health** (n=2,112)</td>

</tr>

<tr>

<td>Excellent/Very good</td>

<td>44.8</td>

<td>54</td>

<td rowspan="3">24.0***</td>

</tr>

<tr>

<td>Good</td>

<td>32</td>

<td>30.3</td>

</tr>

<tr>

<td>Fair/Poor</td>

<td>23.2</td>

<td>15.8</td>

</tr>

<tr>

<td colspan="4">

**Depression** (n=2,815)</td>

</tr>

<tr>

<td>None or Low</td>

<td>34.7</td>

<td>24.9</td>

<td rowspan="3">30.5***</td>

</tr>

<tr>

<td>Moderate</td>

<td>32</td>

<td>31.9</td>

</tr>

<tr>

<td>High</td>

<td>33.3</td>

<td>43.2</td>

</tr>

<tr>

<td colspan="4">

**Cancer history** (n=2,143)</td>

</tr>

<tr>

<td>Both self and family</td>

<td>11.4</td>

<td>10.5</td>

<td rowspan="4">7.8*</td>

</tr>

<tr>

<td>Self</td>

<td>5.3</td>

<td>3.9</td>

</tr>

<tr>

<td>Family</td>

<td>44.3</td>

<td>49.9</td>

</tr>

<tr>

<td>None</td>

<td>39.1</td>

<td>35.8</td>

</tr>

<tr>

<td colspan="4">

**_Health communication and information environment_**</td>

</tr>

<tr>

<td colspan="4">

**Provider-patient interaction quality** (n=1,937)</td>

</tr>

<tr>

<td>High</td>

<td>58.6</td>

<td>62</td>

<td rowspan="2">2.3</td>

</tr>

<tr>

<td>Low</td>

<td>41.4</td>

<td>38</td>

</tr>

<tr>

<td colspan="4">

**Media attentiveness** (n=2,154)</td>

</tr>

<tr>

<td>High</td>

<td>49.4</td>

<td>56.4</td>

<td rowspan="2">10.5***</td>

</tr>

<tr>

<td>Low</td>

<td>50.6</td>

<td>43.6</td>

</tr>

<tr>

<td colspan="4">

**Having a proxy information seeker** (n=2,899)</td>

</tr>

<tr>

<td>Yes</td>

<td>32.5</td>

<td>27.9</td>

<td rowspan="2">5.4*</td>

</tr>

<tr>

<td>No</td>

<td>67.5</td>

<td>72.1</td>

</tr>

<tr>

<td colspan="4">

**Use of the Internet for seeking health/ medical information** (n=1,494)</td>

</tr>

<tr>

<td>Yes</td>

<td>81.8</td>

<td>81.6</td>

<td rowspan="2">0.1</td>

</tr>

<tr>

<td>No</td>

<td>18.2</td>

<td>18.4</td>

</tr>

<tr>

<td colspan="4">

**Membership in online support group** (n=2,212)</td>

</tr>

<tr>

<td>Yes</td>

<td>4.9</td>

<td>7.1</td>

<td rowspan="2">3.8</td>

</tr>

<tr>

<td>No</td>

<td>95.1</td>

<td>92.9</td>

</tr>

<tr>

<td colspan="4">

**E-health communication** (n=2,211)</td>

</tr>

<tr>

<td>Yes</td>

<td>9.6</td>

<td>10.8</td>

<td rowspan="2">0.6</td>

</tr>

<tr>

<td>No</td>

<td>90.4</td>

<td>89.2</td>

</tr>

<tr>

<td colspan="4">

**_Cancer information seeking—behavioural_**</td>

</tr>

<tr>

<td colspan="4">

**Looked for cancer information online** (n=2,030)</td>

</tr>

<tr>

<td>Yes</td>

<td>62.7</td>

<td>65.3</td>

<td rowspan="2">1.2</td>

</tr>

<tr>

<td>No</td>

<td>37.3</td>

<td>34.7</td>

</tr>

<tr>

<td colspan="4">

**Frequency of using the Internet for cancer information** (n=1,281)</td>

</tr>

<tr>

<td>Once a week</td>

<td>5.6</td>

<td>5.4</td>

<td rowspan="4">4.6</td>

</tr>

<tr>

<td>Once a month</td>

<td>10.4</td>

<td>14</td>

</tr>

<tr>

<td>Every few months</td>

<td>30.3</td>

<td>32.1</td>

</tr>

<tr>

<td>Less often</td>

<td>53.8</td>

<td>48.4</td>

</tr>

<tr>

<td colspan="4">

**_Cancer information seeking—cognitive_**</td>

</tr>

<tr>

<td colspan="4">

**Trust in cancer information from the Internet** (n=2,854)</td>

</tr>

<tr>

<td>A lot or some</td>

<td>75.1</td>

<td>79.2</td>

<td rowspan="2">5.1*</td>

</tr>

<tr>

<td>A little or not at all</td>

<td>24.9</td>

<td>20.8</td>

</tr>

<tr>

<td colspan="4">

**Knowledge about cancer** (n=2,254)</td>

</tr>

<tr>

<td>High</td>

<td>34.3</td>

<td>25.6</td>

<td rowspan="2">15.3***</td>

</tr>

<tr>

<td>Low</td>

<td>65.7</td>

<td>74.4</td>

</tr>

<tr>

<td colspan="4">

**Cancer literacy** (n=2,879)</td>

</tr>

<tr>

<td>High</td>

<td>60.6</td>

<td>81</td>

<td rowspan="2">100.0***</td>

</tr>

<tr>

<td>Low</td>

<td>39.4</td>

<td>19</td>

</tr>

<tr>

<td colspan="4">

**Awareness of cancer resources** (n=2,772)</td>

</tr>

<tr>

<td>High</td>

<td>68.8</td>

<td>72.7</td>

<td rowspan="2">3.7</td>

</tr>

<tr>

<td>Low</td>

<td>31.2</td>

<td>27.3</td>

</tr>

<tr>

<td colspan="4">

**Concerned about quality of the cancer information** (n=2,882)</td>

</tr>

<tr>

<td>Strong or Somewhat</td>

<td>62.1</td>

<td>41.4</td>

<td rowspan="2">95.3***</td>

</tr>

<tr>

<td>A little or Not at all</td>

<td>37.9</td>

<td>58.6</td>

</tr>

<tr>

<td colspan="4">

**Search expertise on cancer information** (=2,848)</td>

</tr>

<tr>

<td>High</td>

<td>43</td>

<td>64.7</td>

<td rowspan="2">101.1***</td>

</tr>

<tr>

<td>Low</td>

<td>57</td>

<td>35.3</td>

</tr>

<tr>

<td colspan="4">

**_Cancer information seeking—affective_**</td>

</tr>

<tr>

<td colspan="4">

**Confidence in finding information** (n=2,901)</td>

</tr>

<tr>

<td>Very confident</td>

<td>64.3</td>

<td>75.8</td>

<td rowspan="2">33.1***</td>

</tr>

<tr>

<td>Somewhat confident or less</td>

<td>35.7</td>

<td>24.2</td>

</tr>

<tr>

<td colspan="4">

**Frustrated during the search for cancer information** (n=2,880)</td>

</tr>

<tr>

<td>Strongly agree</td>

<td>15.8</td>

<td>7.2</td>

<td rowspan="3">76.3***</td>

</tr>

<tr>

<td>Somewhat agree or Disagree</td>

<td>54.3</td>

<td>46.9</td>

</tr>

<tr>

<td>Strongly disagree</td>

<td>29.9</td>

<td>45.9</td>

</tr>

</tbody>

</table>

The results of the Chi-squared analysis are presented in Table 1 and their implications for the research questions are:

*   _Research question 1: Are socio-demographic characteristics of health information seekers (as indicated by education, employment, income and health insurance) associated with their likelihood of suffering from cancer information overload?_  
    As presented in Table 1, overload was found to be significantly related to education (χ<sup>2</sup> (2) = 45.3, p< .001), income (χ<sup>2</sup>(2) = 21.3, p<.001) and employment status (χ<sup>2</sup>(5) = 19.2, p<.01). Individuals who were more likely to suffer from overload were those who did not have a college degree, whose annual household incomes were less than $50,000 and who did not, or were unable to, work. Demographic variables, such as age, gender and race and type of health coverage were not significantly associated with overload. In other words, overload was prevalent across this subset of cancer information seekers, but perception of overload varied according to socio-economic status (education, income and employment status).
*   _Research question 2: Is health status (as indicated by general health, depression, history of cancer) of health information seekers associated with their likelihood of suffering from cancer information overload?_  
    All three health status variables were found to be significantly associated with overload. This was higher among those who perceived they were in less than excellent health (χ<sup>2</sup>(2) = 24.0, p<.001), suggesting that people with greater health needs are more likely to suffer from overload. It is reasonable to posit that negative conditions such as frustration, feelings of being overwhelmed, sickness and confusion would be positively associated with overload. Our findings, however, reveal that people with no to low or moderate depression (χ<sup>2</sup>(2) = 30.5, p<.001) are more likely to perceive overload, compared to people with severe depression.  
    Cancer information seekers with personal history of cancer were more likely to report overload compared to those who only had a family history of cancer (χ<sup>2</sup>(3) = 7.8, p<.05). Cancer information seekers who have no history of cancer, too, were more likely to suffer from overload.
*   _Research question 3: Is the health communication and information environment (as indicated by quality of provider–patient interaction, media attentiveness and social networks) of health information seekers associated with their likelihood of suffering from cancer information overload?_  
    Among the variables composing the _health communication and information environment_, media attentiveness (χ<sup>2</sup>(1) = 10.5, p=.001) and having a proxy information seeker(s) (χ<sup>2</sup>(1) = 5.4, p<.05) were significantly associated with overload. People who paid less attention to media and those who had someone else seek information on their behalf were more likely to suffer from overload. Results also show that people who rely on interpersonal sources for cancer information are more likely to suffer from overload. The quality of provider–patient interaction was not found to be significantly associated with overload. E-health communication (as operationally defined as use of e-mail to communicate with doctors) and membership in an online support group were also not associated with overload.
*   _Research question 4: Are behavioural aspects of cancer information seeking (as indicated by the use of the Internet to seek cancer information and frequency of cancer information seeking) associated with cancer information overload?_  
    While majority of the respondents reported that they used the Internet for seeking health or medical information, neither this nor frequency of cancer information seeking were found to be associated with overload.
*   _Research question 5: Are cognitive aspects of cancer information seeking (as indicated by cancer literacy, cancer knowledge, awareness of cancer resources, search expertise, concerns on quality of information and trust in online cancer information) associated with cancer information overload?_  
    Most of the cognitive variables related to cancer information seeking were found to be associated with overload, except for awareness of cancer resources. Overload was higher among those who: had low cancer knowledge (χ<sup>2</sup>(1) = 15.3, p< .001.); had lower levels of cancer literacy (χ<sup>2</sup>(1) = 100.0, p< .001); had greater trust in online cancer information (χ<sup>2</sup>(1) = 5.8, p<.05.); had greater concern for the quality of cancer information (χ<sup>2</sup>(1) = 95.3, p< .001); or had lesser search expertise (χ<sup>2</sup>(1) = 101.1, p<. 001).
*   _Research question 6: Are affective aspects of cancer information seeking (as indicated by frustration and confidence in finding information) associated with cancer information overload?_  
    Affective aspects of information seeking were found to be associated with overload. A majority of the health information seekers (86.3%) who reported overload also reported that they felt somewhat or strongly frustrated during the information search process (χ<sup>2</sup>(2) = 76.3, p<.001). Perception of overload was significantly lower among those who had greater confidence in finding information (χ<sup>2</sup>(1) = 33.1, p<.001).

To recap, we were able to find significant associations between overload and most of the variables investigated except for behavioural aspects of information seeking (research question 4). The significant associations revealed through the bivariate analysis, however, were not all in the direction anticipated. Table 2 summarizes the significantly associated factors.

<table><caption>

**Table 2: The profile of health information seekers most likely to report cancer information overload**</caption>

<tbody>

<tr>

<th>Socio-demographics</th>

<th>Health status</th>

<th>Health Communication and information environment</th>

<th>Cancer information seeking</th>

</tr>

<tr>

<td>Lower level of education</td>

<td>Less healthy</td>

<td>Lower attention paid to media</td>

<td>Less trust in cancer information from the Internet</td>

</tr>

<tr>

<td>Lower income (less than $50,000)</td>

<td>Depressed none or mildly</td>

<td>Have proxy information seeker(s)</td>

<td>Higher knowledge of cancer</td>

</tr>

<tr>

<td>Out of work, retired or unable to work</td>

<td>Personal history of cancer or No history of cancer</td>

<td rowspan="5"> </td>

<td>Lower level of understanding on cancer literature</td>

</tr>

<tr>

<td rowspan="4"> </td>

<td rowspan="4"> </td>

<td>Strong concern on the quality of the cancer information</td>

</tr>

<tr>

<td>Lower search expertise</td>

</tr>

<tr>

<td>Less confident in finding cancer information</td>

</tr>

<tr>

<td>More frustrated during the search for cancer information</td>

</tr>

</tbody>

</table>

### Predictors of cancer information overload

A logistic regression analysis was conducted to test which of the factors were significant predictors of overload. We found that education, concerns about the quality of information, search expertise and cancer literacy were significant predictors of overload (Model χ<sup>2</sup>(5) = 162.4, p= .000) (see Table 3). Beta values were used in an equation to calculate the probability of a case falling into a specific category and to determine the direction of the relationship (which factors increase or decrease the likelihood of cancer information overload occurrence). A negative beta value indicates that an increase in the predictor variable score would result in a decreased probability of the case recording a score of 1 in the dependent variable. Thus, our study found that respondents with more education, cancer literacy and search expertise were less likely to report overload. Concerns about the quality of the information had a positive beta value, indicating that people who were more concerned about the quality of the cancer information found were more likely to report overload. This predictor was also the most powerful predictor among the factors.

The pseudo R-squared value suggests that about 10% of the variability of overload was explained by the set of predictor variables. The model was also able to classify 74.5% of overload correctly. Thus, in three out of four cases, we can make a fairly accurate prediction that a health information seeker is likely to suffer from overload when s/he has some college education, lower search expertise, greater difficulty in understanding the cancer literature, and is highly concerned about quality of the information. All these variables are highly related to information literacy skills and education.

<table><caption>

**Table 3:Logistic regression analysis of predictors of cancer information overload**  
(Note: ***p &lt;0.001)</caption>

<tbody>

<tr>

<th>Predictors</th>

<th>β</th>

<th>S.E.</th>

<th>Odds ratio</th>

<th>Wald statistics</th>

</tr>

<tr>

<td>Education (some college)</td>

<td>-.48</td>

<td>.12</td>

<td>.62</td>

<td>15.21***</td>

</tr>

<tr>

<td>Concerns on quality of information</td>

<td>.48</td>

<td>.10</td>

<td>1.61</td>

<td>21.03***</td>

</tr>

<tr>

<td>Search expertise</td>

<td>-.45</td>

<td>.11</td>

<td>.64</td>

<td>17.74***</td>

</tr>

<tr>

<td>Cancer literacy</td>

<td>-.59</td>

<td>.12</td>

<td>.55</td>

<td>23.51***</td>

</tr>

<tr>

<td>Constant</td>

<td>1.76</td>

<td>.15</td>

<td>5.81</td>

<td>132.74***</td>

</tr>

<tr>

<td>Model χ<sup>2</sup></td>

<td>162.38***</td>

<td colspan="3" rowspan="2"> </td>

</tr>

<tr>

<td>N</td>

<td>2,449</td>

</tr>

</tbody>

</table>

## Discussion

Interpreting which factors are common among cancer information seekers who report having experienced cancer information overload requires a careful examination of the results. The following discussion summarizes how overload might figure in the whole process of health information seeking and what implications this might have for cancer information seekers and health information professionals.

### Socio-economic status and cancer information overload

Existing research on information overload has found that demographic variables such as sex and age are linked to information overload. Specifically, older adults and females have been found to be more prone to overload (See [Akin 1997](#aki97), for a review). We found, however, that age and sex were not associated with overload; nor were race and health insurance coverage. Differences between findings of earlier studies and this study may be due to differences in the main focus and the research designs used. For example, in his classic research, Miller ([1960](#mil60)) sought to see how his subjects coped with information overload by having them work on video terminals identifying lights, colours, or patterns. In addition, most of the earlier studies focused on work-related information seeking and did not deal much with the everyday context of information seeking, let alone the specific context of _health_ information seeking. These differences in findings stress the critical importance of taking context into account in understanding human information behaviour and information overload.

More significant results were uncovered when we further examined the socio-economic status of the respondents, such as employment, perceived health status, income and education. The status variables were all found to be significantly associated with cancer information overload. Among these variables, level of education was the strongest predictor of overload. In general, those who reported having had lower levels of education (or those who did not earn a college degree) were more likely to suffer from overload. The findings of this study indicate that although overload is often alluded to, either as a chronic condition or as a myth of the Information Age, it is a real phenomenon that occurs in more vulnerable groups in society, specifically in lower literacy and lower income populations. The findings further underline the importance of incorporating information literacy skills in high school or college curricula and for emphasizing these skills throughout education.

People who perceive greater health needs are generally more motivated to find health information ([Fox and Rainie 2002](#fox02)) and have a greater likelihood of being confused by the flood of information. Our analysis supported the findings that people who perceived themselves to have less than excellent health were more susceptible to cancer information overload. Interestingly, the results also showed that people who experienced no depression or lower levels of depression were more likely to report having suffered from overload. The results echo studies on information overload conducted in the psychiatric field, where it has been known to be linked to certain levels of depression [(Klemm and Hardie 2002](#kle02)), although the direction of this relationship has not always been consistent . We can hypothesize that those reporting severe levels of general depression are less likely to report feeling information overload as they might be equally less likely to seek information when they are depressed. The limitations of the current dataset, however, prevent us from making any firm generalizations in this regard. In any case, further examination of the psychological and affective domains of cancer information seeking among cancer patients and how this might be associated with overload, are warranted.

### Health communication and information environment and the Internet

The Internet and mass media have often been blamed for the over-proliferation of health information available to the lay public ([Cline and Haynes 2001](#cli01)). It seems only reasonable to assume that online health information seeking and high media attentiveness would be significantly associated with overload. Our analysis, however, shows that most of the variables related to online health information seeking were not found to be significantly associated with overload. Moreover, while the results also show that health information seekers who belong to online support groups or communicate with doctors by e-mail are more likely to report overload, when compared with off-line health information seekers, these differences were not statistically significant. In other words, it did not seem to matter whether information seekers used the Internet to seek information or support, or how frequently they used the Internet. This can be explained in several ways. Recent studies on the use of and preferences for information sources among health information seekers ([Lustria _et al._ 2006](#lus06), for example) show that there is a discrepancy between the sources health consumers reported to have used (that is, the Internet) and the sources they preferred to use (that is, health care providers). Moreover, health information seekers were inclined to use multiple sources of information for a health issue ([Malone _et al._ 2005](#mal05)), regardless of preferences for one or the other ([Johnson 1997](#joh97)). On average, online information seekers used 3.8 sources, whereas off-line information seekers used 3.4 sources ([Cotton and Gupta 2004](#cot04)). Indeed, previous studies on work-related information seeking have found that _delegation_, or processing information from two or more channels, has been known as a common strategy of coping with overload ([Miller 1960](#mil60)). Based on that, one might hypothesize that overload is more related to the number of health information sources consulted rather than preferences for any one type of source over the other. These issues are not yet clear, suggesting the need for more systematic studies on overload and information source selection and use behaviour.

Low media attentiveness was also found to be significantly associated with overload, which support the importance of media attentiveness. How low media attentiveness contributes to overload, however, remains poorly understood and cannot adequately be measured given the limitations of the current dataset. This deserves further investigation because of rising evidence about how media can critically influence attitudes and health decision making (thus the potential link to information overload) and in some cases portray risky and unhealthy behaviours ([Gonzales _et al_. 2004](#gon04)). In turn, this also underscores the importance of media literacy approaches in health communication and education campaigns.

Results also showed that the quality of patient–provider interaction was not found to be significantly linked to overload. This is an interesting finding considering that most patients have reported that doctors or health care providers are their most preferred source of health information ([Kim _et al._ 2006](#kim06)). It is not unreasonable to assume that the quality of their interaction with their doctors might help patients cope with, if not prevent, information overload. Our results show, however, that this had very little to do with perceptions of overload.

Reliance on proxies, such as family and friends, to seek health information was found to be significantly associated with overload. We propose several explanations for this finding. First, confusion might increase as the number of sources increase. Moreover, particular interpersonal sources may not be in a good position to make any type of relevance judgments, nor is it guaranteed that they can evaluate the quality of the information accurately. In addition, during the information exchange between the proxy and the patient, some information might not be communicated accurately or completely, which may result further in confusion. These assumptions cannot be measured given the limitations of the current analysis but are certainly worth further investigation.

### Affective aspects of information seeking and cancer information overload

The relationship of affect on information seeking and perceptions of overload still needs further exploration. On one hand, the bivariate analysis showed that overload was linked to feelings of frustration with the search process and confidence in finding information. On the other hand, results of the logistic regression showed that these variables were not significant predictors of overload. The findings support previous research that has linked negative affects (such as frustration, anxiety and stress) to overload, but at the same time the current analysis did not find frustration to be a significant predictor of overload. The findings should be interpreted with caution, as there are key differences in how frustration has been measured previously and in the current research. Typically, information overload research has defined frustration in terms of seekers' reactions to large amounts of information. In the National Trends Survey, however, frustration was measured in terms of how respondents felt during the information search process. In keeping with this, Kuhlthau ([2004](#kuh04)) defined frustration as a transitional emotion that might be experienced during the information seeking process that can be resolved when information seekers find information they are seeking or when their feelings of uncertainty are lessened. In that regard, _uncertainty_ might be a key intervening variable that might help explain the relationship of affective aspects of information seeking and overload. Given that information seeking is a process that aims to reduce uncertainty and that overload is one possible consequence of information seeking, future research needs to investigate what role uncertainty might play.

Aside from these, there can be other reasons why health information seekers might feel frustrated. For example, Wicks ([2004](#wic04)), in a study of information seeking related to everyday life tasks by older adults, found that feelings of frustration could be related to a number of factors, including physical disabilities, poor communication with nursing home staff, difficulties with evaluating the information, technology problems, poor library services and difficulties dealing with voluminous information. Further qualitative research on how information seekers feel when they are overloaded with information can offer a richer understanding of how different emotions such as frustration can be linked to overload.

### Cognitive aspects of information seeking and cancer information overload

Results showed that most of the cognitive aspects associated with cancer information seeking were found to be significantly related to overload. In particular, greater cancer knowledge, more concerns about the quality of information, less understanding of information (cancer literacy), and less trust for online sources were all found to be associated with overload. In addition, respondents who reported greater searching expertise were significantly less likely to report overload. This supports earlier studies that have shown that searching expertise is an important skill for coping with too much information ([Van Der Molen 1999](#van99)) and that, in turn, may affect patients' abilities to make informed decisions.

While our study seems to confirm that people who are not adroit at dealing with large amounts of information might suffer from overload, our study also indicates that persons who had more concern about the quality of cancer information and had greater knowledge about cancer were more likely to report overload. The link between higher levels of knowledge and greater concerns about information quality seems logical, but how these variables might be associated with overload is still poorly understood. Current research on cancer information seeking has suggested that patients tend to be more active information seekers and there is some evidence that their overall disease management skills, including information seeking skills, may develop as they go through different stages of cancer ([Frosch and Kaplan 1999](#fro99)). Perhaps a closer examination of how the information needs and seeking skills of cancer patients evolve during the cancer continuum could reveal potential intervening variables that might better explain the occurrence of overload.

Our findings likewise emphasize the importance of health information literacy in combating overload. Health (information) literacy is operationally defined in Healthy People 2010 ([2000](hea00)) as _the degree to which individuals have the capacity to obtain, process and understand basic health information and services for appropriate health decisions_. The terms _health literacy_ and _health information literacy_ are used interchangeably in the literature. Earlier definitions of health literacy narrowly focused on functional literacy and emphasized the ability to read and understand medical instructions, including the ability to read and comprehend prescription bottles, appointment slips and the other essential health-related materials required to function as a patient ([American Medical Association…, 1999)](#ama99). Later definitions have emphasized the need to go beyond assessing readability of health information and improving reading levels and include efforts to improve patients' abilities to evaluate and act upon health information ([Nutbeam 2000](#nut00)).

Researchers have found that groups with the highest prevalence of chronic disease and the greatest need for health care experience more difficulty reading and understanding the medical information ([American Medical Association … 1999](#ama99)). In particular, cancer patients with poor health literacy have demonstrated difficulties with both written and oral communication, which, in turn, have been found to impair communication and discussion of cancer treatment options, including routine procedures and clinical trials ([Davis _et al_. 2004](#dav04)). Health literacy is also known to be related to multiple aspects of health, including health knowledge, health status, use of health services and disease management skills. Inadequate health literacy may be an important barrier to patients' understanding of their diagnoses and treatment and to receiving high-quality care ([Williams _et al._ 1995](#wil95)).

An earlier study showed, however, that 'high literacy levels does not guarantee that a person will respond in a desired way to health education and communication activities' ([Nutbeam 1999: 52](#nut99)). Nutbeam ([2000](#nut00)) argued that patients need to develop a more complex set of skills in order to successfully navigate health care settings. This includes not only the ability to read medical instructions, but also the ability to extract and apply health information to a variety of circumstances and settings, as well as the ability to evaluate the quality of health information. So, while an increasing number of Americans have been able to harness technology to improve their health knowledge and make more informed decisions, a majority of those in most need of this information and health interventions remain at risk. Recent studies suggest that advances in health information technologies have widened the disparities especially among sicker, older and more vulnerable groups ([Parker _et al._ 2003](#par03)). Cline and Haynes ([2001](#cli01)) noted that inequitable access is further exacerbated by limited navigational skills. Moreover, meagre information-evaluation skills contribute to disparities among health consumers of varied educational backgrounds, reinforcing the _need for quality standards and widespread criteria for evaluating health information_ ([Cline and Haynes 2001](#cli01)).

## Implications and suggestions for future research

The results reported here stress important gaps in health communication and information seeking research. First, the current findings reveal gaps in information overload research particularly in the context of health. Akin ([1997](aki97)) pointed out that much of the overload literature in library science consists of essays and underlined the need for more empirical research. One hopes that the findings of the current study may be used to generate hypotheses and research questions for future research. For instance, there is a need to develop and validate a scale for information overload that addresses its multi-dimensional nature. Besides, there is a need to develop measures that can be applied to general health contexts as well as widespread and complex chronic diseases such as cancer. Health information overload also needs to be studied in the context of multiple chronic diseases, as individuals who suffer from multiple medical conditions are likely to not only need more information but are also more likely to experience conflicting health information. Moreover, research that examines more directly the links between health information overload, decision making and health behaviour is warranted.

Secondly, the results of this investigation reveal links between cancer information overload and health information literacy and provide evidence of the importance of the latter in helping individuals cope with or even prevent overload. This unveils numerous opportunities for information professionals to become more active in helping to design and provide health information services to patrons with a variety of educational backgrounds and health contexts. An understanding of the challenges that low literacy groups face regarding health seeking might inform information professionals about how to handle specific health questions from different individuals. For example, if it is evident that patrons may have a hard time understanding or locating information, librarians might provide information more suited to their specific needs and avoid overloading such patrons. Libraries can also help people with different literacy skills by providing health information in various formats such as audio, video, or pictures. Since they play a large role in educating communities and are oftentimes one of the few places disadvantaged populations might go for information and to access the Internet, public libraries can partner with health providers and public health professionals in the community to launch health information campaigns or health information literacy training sessions for their patrons. Workshops on personal information management skills offered through public libraries might also help individuals sort through and cope with abundant and/or poorly organized information.

Finally, this study implies the importance of developing a validated measure of health information literacy in order to facilitate empirical research on this skill or phenomenon. [](#par96)A set of standardized instruments that assess health literacy skills has been developed over time, such as the Test of Functional Health Literacy in Adults by [Parker _et al._ (1995](#par95)) and Research Readiness Self-Assessment by Ivanitskaya _et al._ ([2006](#iva06)). Most, however, have focused on measuring functional literacy, and some are unwieldy and difficult to administer. Few instruments have been developed to measure health information literacy as more broadly defined, as suggested by Nutbeam ([2000](#nut00)). A validated measure that includes items addressing all dimensions of health literacy will be a benefit to researchers seeking to explore how this skill might be linked to various aspects of health information seeking and decision making.

## Limitations of the Study

As with most research based on secondary data, our analysis was confined to the National Trends Survey framework and how particular variables of interest were operationalized in the survey. As noted earlier, only one item in that survey was used to measure cancer information overload and this presents a significant limitation of the current investigation. Nevertheless, we still felt it worthwhile to explore this concept in our research. This is one of the few studies to explore information overload in the context of the health information seeking behaviour of a nationally-representative sample. In addition, we recognized this as an important opportunity to explore and identify potential predictors of overload for future studies. We also recognized that overload is a complex, multi-dimensional construct and that categorical reduction based on a single proxy item clearly limits our ability to make any clear-cut generalizations from our findings. This study was, thus, exploratory by nature.

Health information literacy was, likewise, defined very narrowly in the National Trends Survey and did not include items such as: sensitivity to personal, anticipated information needs ([Bruce 2005](#bru05)), which is a critical component of information literacy. Moreover, that survey instrument did not address situational variables such as cost of getting the information (or access to the source or channel), nature of task and information overload coping strategies and outcomes (what patients do to cope with overload). All of these variables could be related to information seeking and potentially, overload.

## Conclusion

To conclude, this study examined health information seeking and the factors associated with cancer information overload using data from a national survey. We found that a greater level of overload was reported by individuals with lower socio-economic status, particularly, individuals with lower income, poor health and who were unemployed. On the other hand, individuals with higher education and greater cancer literacy and search expertise were less likely to report having experienced overload. Individuals who were less confident in finding information and who were more frustrated with the information seeking process were also more likely to report overload. Contrary to common beliefs, use of the Internet for seeking health information or social support was not found to be significantly associated with overload. This implies that overload may be more related to issues of content rather than the channel of delivery or even source selection. This was supported by the finding that, having higher concerns about quality of information was the strongest predictor of overload.

An important finding of this analysis is the significant link between overload and health information literacy. This result should be read with caution because of the exploratory nature of this study and the limitations of secondary data analysis, particularly the lack of validated measures to define the important variables being investigated. Nevertheless, this underlines the importance of health information literacy in relation to health information seeking behaviour and potentially in alleviating or coping with information overload.

This study brings to the fore important implications for those of us in the information science discipline. From a research perspective, information researchers can contribute much to the current discourse on health information seeking, health information literacy and information overload. In particular, information researchers may be able to offer a different approach to current conceptualizations of health information seeking and help enrich research frameworks especially in the constantly changing information environment. Moreover, there is an urgent need for validated measures for health information literacy and information overload to aid empirical research in this area.

Our findings also have implications for the potentially larger role information professionals can play in the health arena. In particular, information professionals, in partnership with other community health resources and local health care providers, are particularly poised to provide training in information literacy skills to low literacy groups through public library programmes. Moreover, with some sensitivity training on various health issues, health information professionals will be able to deliver personalized information services to individuals who may have most need of assistance.

## References

*   <a id="ada04"></a>Adams, S. &Berg, M. (2004). The nature of the net: constructing reliability of health information on the web. _Information Technology &People_, **17**(2), 150-170.
*   <a id="aki97"></a>Akin, L. K. (1997). _Information overload: a multi-disciplinary explication and citation ranking within three selected disciplines: library studies, psychology/psychiatry, and consumer science: 1960-1996_. Unpublished doctoral dissertation, Texas Woman’s University, Denton, Texas, USA.
*   <a id="ama99"></a>American Medical Association, _Council of Scientific Affairs_. (1999). Health Literacy: report of the Council on Scientific Affairs. _Journal of the American Medical Association_, **281**(6), 552-557.
*   <a id="and05"></a>Andrews, J.E., Johnson, J.D., Case, D.O., Allard, S. & Kelly, K. (2005). [Intention to seek information on cancer genetics](http://www.webcitation.org/5RLHIuruQ). _Information Research_, **10**(4), paper238\. Retrieved 15 March, 2006 from http://informationr.net/ir/10-4/paper238.html
*   <a id="ank06"></a>Ankem, K. (2006). Factors influencing information needs among cancer patients: a meta-analysis. _Library & Information Science Research_, **28**, 7-23\.
*   <a id="bab04"></a>Babbie, E. (2004). _The practice of social research_. 10th ed. Belmont, CA.: Wadsworth/Thompson Learning.
*   <a id="baw99"></a>Bawden, D., Holtham, C. & Courtney, N. (1999). Perspectives on information overload. _Aslib Proceedings_, **51**(8), 249-255\.
*   <a id="big89"></a>Biggs, M. (1989). Information overload and information seekers: what do we know about them, what to do about them. _Reference Librarian_, **25**(25), 411-29.
*   <a id="bru05"></a>Bruce, H. (2005). [Personal, anticipated information needs](http://www.webcitation.org/5RLHM14UI). _Information Research_, **10**(3). Retrieved 18 June 2006 from http://informationr.net/ir/10-3/paper232.html
*   <a id="cas04"></a>Case, D.O., Johnson, J.D., Andrews, J.E., Allard, S.L. & Kelly, K.M. (2004). From two-step flow to the Internet: the changing array of sources for genetics information seeking. _Journal of the American Society for Information Science & Technology_, **55**(8), 660-669.
*   <a id="cha04"></a>Charnock, D. & Shepperd, S. (2004). Learning to DISCERN online: applying an appraisal tool to health websites in a workshop setting. _Health Education Research_, **_19_**(4), 440-446\.
*   <a id="cli01"></a>Cline, R.J.W. & Haynes, K. M. (2001). Consumer health information seeking on the Internet: the sate of the art. _Health Education Research_, **16**(6), 671-692\.
*   <a id="coh05"></a>Cohen, S.M. (2005). Moving backwards from information gluttony. _Public Libraries_, **44**(5), 269-271.
*   <a id="cot04"></a>Cotton, S.R. & Gupta, S.S. (2004). Characteristics of online and offline health information seekers and factors that discriminate between them. _Social Science and Medicine_, **59**(9) , 1795-1806.
*   <a id="dav04"></a>Davis, D.A., Ciurea, I., Flanagan, T.M. & Perrier, L. (2004). Solving the information overload problem: a letter from Canada. _Medical Journal of Australia._, **180**(6 Suppl.), S68-S71.
*   <a id="dev02"></a>De Vaus, D.A. (2002). _Analyzing social science data_. London: Sage Publications.
*   <a id="dol04"></a>Dolan, G., Iredale, R., Williams, R. &Ameen, J. (2004). Consumer use of the internet for health information: a survey of primary care patients. _International Journal of Consumer Studies_, **28**(2), 147-153\.
*   <a id="doo05"></a>Doolittle, G.C & Spaulding, A. (2005). [Online cancer services: types of services offered and associated health outcomes](http://www.jmir.org/2005/3/e35/). _Journal of Medical Internet Research_, **7**(3). Retrieved 10 May, 2006 from http://www.jmir.org/2005/3/e35/
*   <a id="dut04"></a>Dutta-Bergman, M. J. (2004). [Health attitudes, health cognitions, and health behaviors among Internet health information seekers: population-based survey](http://www.webcitation.org/5RLHUhz41). _Journal of Medical Internet Research_, **6**(2). Retrieved 10 June, 2006 from http://www.jmir.org/2004/2/e15
*   <a id="eks04"></a>Ek, S. (2004). Information literacy as a health promoting determinant. _Health Informatics Journal_, **10**(2), 139-153\.
*   <a id="eng99"></a>Eppler, M. J. & Mengis, J. (2004). The concept of information overload: a review of literature from Organization Science, Accounting, Marketing, MIS and related disciplines. _The Information Society_, **20**(5), 325-344.
*   <a id="eys01"></a>Eysenbach, G. (2001). [What is e-health?](http://www.webcitation.org/5RLHmy9dT) _Journal of Medical Internet Research_, **3**(2). Retrieved 25 April 2006 from http://www.jmir.org/2001/2/e20/
*   <a id="eys03"></a>Eysenbach, G. (2003). The impact of the Internet on cancer outcomes. _CA: A Cancer Journal for Clinicians_, **53**(6), 356-370.
*   <a id="fox05"></a>Fox, S. (2005). [Health information online: eight in ten internet users have looked for health information online, with increased interest in diet, fitness, drugs, health insurance, experimental treatments, and particular doctors and hospitals](http://www.webcitation.org/5RLHtjt3M). Washington, DC: Pew Internet and American Life Project. Retrieved 25 August, 2007 from http://www.pewinternet.org/pdfs/PIP_Healthtopics_May05.pdf
*   <a id="fox02"></a>Fox, S. & Raine, L. (2002). [Vital Decisions: how Internet users decide what information to trust when they or their loved ones are sick](http://www.webcitation.org/5RLHwqGNK). Washington, DC: Pew Internet and American Life Project. Retrieved 19 January, 2006 from http://www.pewinternet.org/pdfs/PIP_Vital_Decisions_May2002.pdf
*   <a id="fro99"></a>Frosch, D.L. & Kaplan, R.M. (1999). Shared decision making in clinical medicine: past research and future directions. _American Journal of Preventive Medicine_, **17**(4), 285-294\.
*   <a id="gon04"></a>Gonzales, R., Glik, D., Davoudi, M. & Ang, A. (2004). Media literacy and public health: integrating theory, research and practice for tobacco control. _American Behavioral Scientist_, **48**_(2)_, 189-201.
*   <a id="gur05"></a>Gurmankin, A. D. & Viswanath, V. (2005). [The impact of health information overload on attention to information and cancer risk reduction behavior](http://www.webcitation.org/5RLIUVyb9). Washington, DC: US National Institutes of Health, National Cancer Institute. [Powerpoint presentation] Retrieved November, 2005 from http://hints.cancer.gov/hints/docs/Gurmankin.pdf
*   <a id="hal04"></a>Hall, A. & Walton, G. (2004). Information overload within the health care system: a literature review. _Health Information and Libraries Journal,_ **21**(2), 102-108.
*   <a id="hea01"></a>Healthy People 2010\. (2000). [Health communication](http://www.webcitation.org/5RLIzixtB). Retrieved 21 July, 2006 from http://www.healthypeople.gov/Document/HTML/Volume1/11HealthCom.htm
*   <a id="hwa99"></a>Illic, D., Risbridger, G.P. & Green, S. (2005). The informed men: attitudes ad information needs on prostate cancer screening. _Journal of Men’s Health and Gender_, **2**(4), 414-420.
*   <a id="ioa05"></a>Ioannidis, J. P.A. (2005). [Why most published research findings are false.](http://www.webcitation.org/5RLJCtvdo) _PLoS Medicine_, **2**(8). Accessed 21 January, 2006 at http://medicine.plosjournals.org/archive/1549-1676/2/8/pdf/10.1371_journal.pmed.0020124-L.pdf
*   <a id="iva06"></a>Ivanitskaya, L., O'Boyle, I. & Casey, A.M. (2006). [Health information literacy and competencies of information age students: results from the interactive online research readiness self-assessment (RRSA)](http://www.webcitation.org/5RLJMs4Wz). _Journal of Medical Internet Research_, **8**(2). Retrieved 12 June, 2006 from http://www.jmir.org/2006/2/e6/
*   <a id="joh97"></a>Johnson, J. D. (1997). _Cancer-related information seeking_. Cresskill, N.J.: Hampton Press.
*   <a id="kim06"></a>Kim, K., Lustria, M.L. & Burke, D. (2006). _Information and communication behaviors of e-patients: findings from a national survey._ Paper presented at the Critical Issues in eHealth Research Annual Conference. Bethesda, MD.
*   <a id="kle02"></a>Klemm, P. & Hardie, T. (2002). Depression in Internet and face-to-face cancer support group: a pilot study. _Oncology Nursing Forum_, **29**(4), E45-E51.
*   <a id="kuh04"></a>Kuhlthau, C. C. (2004). _Seeking meaning: a process approach to library and information services_. (2nd. ed.). Westport, CT: Libraries Unlimited.
*   <a id="lev05"></a>Levinson, W., Kao, A., Kuby, A. & Thisted, R.A. (2005). Not all patients want to participate in decision making: a national study of public preferences. _Journal of General Internal Medicine_, **20**, 531-535.
*   <a id="lin06"></a>Ling, B.S., Klein, W.M. & Dang, Q. (2006). Relationship of communication and information measures to colorectal cancer screening utilization: results from HINTS. _Journal of Health Communication_, **11** (S1) ,181-190.
*   <a id="lus06"></a>Lustria, M., Burke, D., Kim, K. & Case, D. (2006). _The changing array of cancer information sources in the E-health era: relationship to perceived health status, cancer knowledge, and screening behaviors_. Paper presented at the American Society for Information Science and Technology Annual Conference. Austin, Texas, USA.
*   <a id="mal05"></a>Malone, M., Mathes, L, Dooley, J. & While, A.E. (2005). Health information seeking and its effect on the doctor-patient digital divide. _Journal of Telemedicine and Telecare_, **11**(S1), 25-28.
*   <a id="mla05"></a>Medical Library Association. (2005). _[Health information literacy: definition](http://www.gwu.edu/~cih/journal/contents/V11/N1/abstracts_v11n1.htm)_. Retrieved 9 January, 2006 from http://www.mlanet.org/resources/healthlit/define.html
*   <a id="mil56"></a>Miller, G. (1956). The magical number seven, plus or minus two: some limits on our capacity for processing information. _Psychological Review_, **63**(2), 81-97.
*   <a id="mil60"></a>Miller, J. G. (1960). Information input overload and psychopathology. _American Journal of Psychiatry_, **116**(8), 695-704.
*   <a id="mor04"></a>Morahan-Martin, J.M. (2004). How Internet users find, evaluate and use online health information: a cross-cultural review. _CyberPsychology &Behavior_, **7**(5), 497-510\.
*   <a id="nci03"></a>National Cancer Institute (NCI). (2003). _[HINTS 1 final report](http://cancercontrol.cancer.gov/hints/hints_docs/final_report.pdf)_. Retrieved 18 December, 2005 from http://cancercontrol.cancer.gov/hints/hints_docs/final_report.pdf [The site requires a login to access]
*   <a id="ngu06"></a>Nguyen, G. T. & Bellamy, S. L. (2006). Cancer information seeking preferences and experiences; disparities between Asian Americans and Whites in the Health Information National Trends Survey (HINTS). _Journal of Health Communication_, **11**, 173-180\.
*   <a id="nut99"></a>Nutbeam, D. (1999). Literacies across the lifespan: health literacy. _Literacy & Numeracy Studies_, **9**(2), 47-55.
*   <a id="nut00"></a>Nutbeam, D. (2000). Health literacy as a public health goal: A challenge for contemporary health education and communication strategies into the 21 st century. _Health Promotion International_, **15**(3), 259-267.
*   <a id="one97"></a>O'Neal, H. F. & Schacter, J. (1997). _[Test specifications for problem-solving assessment](http://www.webcitation.org/5RLJy4yOb )_. Los Angeles, CA: UCLA Center for the Study of Evaluation. (Tech. Rep. No. 463). Retrieved 25 August 2007 from http://www.eric.ed.gov/ERICDocs/data/ericdocs2sql/content_storage_ 01/0000019b/80/15/5b/3b.pdf
*   <a id="pal05"></a>Pallant, J. (2005). _SPSS survival guide_. (2nd ed.) Open University Press.
*   <a id="pan03"></a>Pandey, S., Hart, J. & Tiwary, S. (2003). Women's health and the Internet: understanding emerging trends and implications. _Social Science Medicine_, **56**, 179-192\.
*   <a id="par95"></a>Parker, R.M., Baker, D.W., Williams, M.V. & Nurss, J.R. (1995). The test of functional health literacy in adults: a new instrument for measuring patients' literacy skills. _Journal of General Internal Medicine_, **10**, 537-41.
*   <a id="par03"></a>Parker, R.M., Ratzan, S.C., Lurie, N. (2003). Health literacy: A policy challenge for advancing high-quality health care. _Health Affairs_, **22**(4), 147\.
*   <a id="reu96"></a>Reuters, Ltd. (1996). _Dying for information: an investigation into the effects of information overload in the USA and worldwide_. London: Reuters Limited.
*   <a id="riz03"></a>Rizzo, L. (2003). _[NCI HINTS sample design and weighting plan](http://www.cancercontrol.cancer.gov/hints/docs/sampling_plan_final.pdf)_. Retrieved 21 March, 2006 from http://www.cancercontrol.cancer.gov/hints/docs/sampling_plan_final.pdf
*   <a id="fin06a"></a>Rutten, L.J.F., Augustson, E. & Wanke, K. (2006). Factors associated with patients’ perceptions of health care providers’ communication behavior. _Journal of Health Communication,_ **11**, 135-146\.
*   <a id="fin06b"></a>Rutten, L.J.F., Squiers, L. & Hesse, B. (2006). Cancer-related information seeking: hints from the 2003 Health Information National Trends Survey (HINTS). _Journal of Health Communication_, **11**, 147-156.
*   <a id="sch04"></a>Shim, M., Kelly, B. & Hornik, R. (2006). Cancer information scanning and seeking behavior is associated with knowledge, lifestyle choices, and screening. _Journal of Health Communication_, **11**, 157-172.
*   <a id="ste03"></a>Stewart, B. W. & Kleihues, P. (Eds.). (2003). _World cancer report_. Lyon, France: International Agency for Research on Cancer.
*   <a id="tab96"></a>Tabachnick, B.G. & Fidell, L. S. (1996). _Using multivariate statistics_. (3rd Ed.). NY: Harper Collins.
*   <a id="tay02"></a>Taylor, H. (2002). _[Cyberchondriacs update. HarrisIntearctive.#21](http://www.harrisinteractive.com/harris_poll/index.asp?PID=299)_. Retrieved 21 June, 2006 from http://www.harrisinteractive.com/harris_poll/index.asp?PID=299
*   Tidline, T.J. (1999) The mythology of information overload. _Library Trends_, **47**(3), 485-506.
*   <a id="tur06"></a>Turner, M. M., Rimal, R. N., Morrison, D. & Kim, H. (2006). The role of anxiety in seeking and retaining risk information: testing the risk perception attitude framework in two studies. _Human Communication Research_, **32**, 130-156\.
*   <a id="usc05"></a>U.S. Census Bureau (2005). _[2005 American Community Survey](http://factfinder.census.gov/servlet/ACSSAFFFacts?_submenuId=factsheet_1&_sse=on )_. Retrieved 10 January, 2007 from http://factfinder.census.gov/servlet/ACSSAFFFacts?_submenuId=factsheet_1&_sse=on
*   <a id="van99"></a>Van der Molen, B. (1999). Relating information needs to the cancer experience: 1\. Information as a key coping strategy. _European Journal of Cancer Care_, **8**(4), 238-244.
*   <a id="wei98"></a>Weinstein, N. D., Rothman, A. J. & Sutton, S. R. (1998). Stages theories of health behavior: conceptual and methodological issues. _Health Psychology_, **17**(3), 290-299.
*   <a id="whi00"></a>White, M. & Doman, S. M. (2000). Confronting information overload. _Journal of School Health_, **70**(4), 160-161.
*   <a id="wic04"></a>Wicks, D. (2004). Older adults and their information seeking. _Behavioral & Social Sciences Librarian_, **22**(2), 1-26\.
*   <a id="wil95"></a>Williams, M.V., Parker, R.M., Baker, D.W., Parikh, N.S., Pitkin, K., Wendy, C. & Nurss, J. R. (1995). Inadequate functional health literacy among patients at two public hospitals. _Journal of the American Medical Association_, **274**(21), 1677-1682\.
*   <a id="wil96"></a>Wilson, P. (1996). Interdisciplinary research and information overload. _Library Trends_, **45**(2), 192-203.
*   <a id="wil01"></a>Wilson, T.D. (2001). Information overload: implications for healthcare services. _Health Informatics Journal_, **7**, 112-117.
*   <a id="zie04"></a>Ziebland, S., Chapple, A., Dumelow, C., Evans, J., Prinjha, S. & Rozmovits, L. (2004). How the internet affects patients' experience of cancer: a qualitative study. _British Medical Journal_, **328**(7439), 564-570.