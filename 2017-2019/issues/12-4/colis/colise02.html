<!DOCTYPE html>
<html lang="en">

<head>
	<title>Envisioning an iSchool Curriculum</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta name="dc.title" content="Envisioning an iSchool Curriculum">
	<meta name="dc.creator" content="Michael Seadle, Elke Greifeneder">
	<meta name="dc.subject" content="Invited talk on at the Educational forum at the CoLIS6 conference">
	<meta name="dc.description"
		content="The questions we were asked to discuss for this 6th International Conference on Conceptions of Library and Information Science were whether there should be a unique iSchool curriculum and, if so, what would it look like? We have used a methodology in writing this paper that draws heavily on anthropological traditions of observation and analysis. If a unique iSchool curriculum ought to exist, then an iSchool ought to be more than a library school with a name that implies modern times. Some of the differences between iSchools and the more traditional library schools are apparent in their course descriptions. Our proposed curriculum is an abstract designed around a set of ideas, not around what is implementable in the classroom. At present our interactions with computers tend to be highly verbal. Nonetheless the real communication takes place with words. For those who like this ideal curriculum and want to try it should at a minimum retain three key principles: 1) all information services now revolve around human-computer interaction; 2) teach students to think like anthropologists and look at the problems and issues from multiple viewpoints, multiple cultures, and multiple ecologies; and 3) students need to remember that language both enables and limits our ability to communicate with contemporary information systems - without a strong awareness of linguistic issues, we cannot provide information.">
	<meta name="dc.subject.keywords"
		content="information schools; library and information science; education; i-schools; human-computer interaction">
	<meta name="robots" content="all">
	<meta name="dc.publisher" content="Professor T.D. Wilson">
	<meta name="dc.coverage.placename" content="global">
	<meta name="dc.type" content="text">
	<meta name="dc.identifier" scheme="ISSN" content="1368-1613">
	<meta name="dc.identifier" scheme="URI" content="http://InformationR.net/ir/12-4/colis/colise02.html">
	<meta name="dc.relation.IsPartOf" content="http://InformationR.net/ir/12-4/colis/colis.html">
	<meta name="dc.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dc.rights" content="">
	<meta name="dc.date.available" content="">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="vol-12-no-4-october-2007">Vol. 12 No. 4, October, 2007</h4>
	<h2>Proceedings of the Sixth International Conference on Conceptions of Library and Information
		Science—&quot;Featuring the Future&quot;. Educational Forum paper</h2>
	<h1 id="envisioning-an-ischool-curriculum">Envisioning an iSchool Curriculum</h1>
	<h4 id="michael-seadle-and-elke-greifeneder"><a href="mailto:seadle@ibi.hu-berlin.de">Michael Seadle</a> and Elke Greifeneder</h4>
	<h4>Institute for Library and Information Science,<br>
		Humboldt University in Berlin,<br>
		Unter den Linden 6,<br>
		10099 Berlin, Germany</h4>
	<h4 id="abstract">Abstract</h4>
	<blockquote>
		<p>The questions we were asked to discuss for this 6th International Conference on Conceptions of Library and
			Information Science were whether there should be a unique iSchool curriculum and, if so, what would it look
			like? We have used a methodology in writing this paper that draws heavily on anthropological traditions of
			observation and analysis.<br>
			If a unique iSchool curriculum ought to exist, then an iSchool ought to be more than a library school with a
			name that implies modern times. Some of the differences between iSchools and the more traditional library
			schools are apparent in their course descriptions. Our proposed curriculum is an abstract designed around a
			set of ideas, not around what is implementable in the classroom. At present our interactions with computers
			tend to be highly verbal. Nonetheless the real communication takes place with words.<br>
			For those who like this ideal curriculum and want to try it should at a minimum retain three key principles:
			1) all information services now revolve around human-computer interaction; 2) teach students to think like
			anthropologists and look at the problems and issues from multiple viewpoints, multiple cultures, and
			multiple ecologies; and 3) students need to remember that language both enables and limits our ability to
			communicate with contemporary information systems - without a strong awareness of linguistic issues, we
			cannot provide information.</p>
	</blockquote>
	<h2 id="introduction">Introduction</h2>
	<p>The questions we were asked to discuss for this 6th International Conference on Conceptions of Library and
		Information Science were whether there should be a unique iSchool curriculum and, if so, what would it look like
		iSchools are, as participants in this conference know, library schools that have transformed themselves into
		agencies with a broader mission and a field of study that encompasses not just the traditional paper and media
		based realms of library collections, but information in the broadest sense that includes potentially everything
		in the internet and every form of information found in the world.</p>
	<p>A document today need not be a piece of paper or even an internet page that can be rendered on paper in
		two-dimensional form. Michael Buckland (<a href="#buc98">1998</a>) quotes Suzanne Dupuy-Briets description of a
		document:</p>
	<blockquote>
		<p>... An antelope running wild on the plains of Africa should not be considered a document, she rules. But if
			it were to be captured, taken to a zoo and made an object of study, it has been made into a document. It has
			become physical evidence being used by those who study it. Indeed, scholarly articles written about the
			antelope are secondary documents, since the antelope itself is the primary document.</p>
	</blockquote>
	<p>If iSchool students study documents, then the polar bear baby Knut in the Berlin zoo is not merely an
		international celebrity, but a document waiting to be read. This kind of breadth is critical to the concept of
		an iSchool because iSchools break out of library norms and explicitly include everything that might possibly be
		a source from which we gather both the raw material that goes into our scholarly endeavors and the processed
		peer-reviewed form that we have long made the exclusive content of research libraries.</p>
	<h2 id="methodology">Methodology</h2>
	<p>We have used a methodology in writing this paper that draws heavily on anthropological traditions of observation
		and analysis. Anthropologists traditionally visit some remote culture, learn the language, live among the
		natives, and then describe their habits and culture from an outside view with enough analytical objectivity and
		enough detail to evoke a credible picture. As David Fettermann (<a href="#fet86">1986</a>, p. 216) writes:</p>
	<blockquote>
		<p>One of the most important attributes of a successful ethnographer is his or her ability to use a cultural
			perspective in the artistic and literary Geertzian tradition to decipher reality. The gem cutter's precision
			is important, but his artist's most important gift is his or her artistic ability to see the inner beauty of
			a diamond in the rough and to make its beauty known to the world.</p>
	</blockquote>
	<p>The anthropologist can apply the same techniques with appropriate cautions equally well to circumstances well
		removed from a primitive village in a remote hinterland. The key fact is that all cultures have foreign elements
		within them: micro-cultures that have their own idiosyncratic vocabularies, customs, ways of behaving, even
		taboos. An obvious example might be an historian who wanders by mistake into a room of people talking about
		using Shibboleth as an authorization system for stateless access to remote resources. The historian would
		recognize many of the words: &quot;Shibboleth&quot; is a Biblical reference, &quot;authorization&quot; seems
		like an ordinary word, and every historian has heard about &quot;stateless&quot; people. Nonetheless the
		sentence as a whole makes no actual sense because he belongs to a culture whose standard vocabulary interprets
		those words and their combination totally differently than would a programmer who recognizes Shibboleth as a
		system whose purpose is to validate individual access to resources on the internet using protocols like HTTP
		that do not maintain any constant connection between machines (which makes them stateless).</p>
	<p>The anthropologists in our study, Greifeneder and Seadle, have acquired a moderately competent grasp of the
		iSchool idiom, and speak reasonably fluent conventional library jargon as well. Seadle comes from a varied
		academic background with decades of experience using anthropological methods and with long years of
		acculturation among computing professionals, who by and large accept him as a member of their tribe. He also has
		training as a librarian from the University of Michigan, and he has worked in US libraries off and on since
		1976. Greifeneder brings a strong background in linguistics and experience with both German and French library
		cultures. Their different ages and experiences provide a balanced view of what an iSchool curriculum might mean.
	</p>
	<p>Three schools have been used as active models for this study: <a href="http://www.si.umich.edu/">The School of
			Information</a> at the University of Michigan (Ann Arbor), the <a href="http://www.lis.uiuc.edu/">Graduate
			School of Library and Information Science</a> at the University of Illinois (Urbana-Champaign), and <a
			href="http://www.ischool.washington.edu/">the Information School</a> at the University of Washington
		(Seattle). The French and German models and curriculum for library information science also played a role, but
		most of these programmes, in our experience, still appear to have a strong traditional library orientation,
		including our own Institute at Humboldt University, though it is consciously and deliberately moving toward an
		iSchool model.</p>
	<h2 id="ischool-vs-library-school">iSchool vs library school</h2>
	<p>If a unique iSchool curriculum ought to exist, then an iSchool ought to be more than a library school with a name
		that implies modern times. Some of the differences between iSchools and the more traditional library schools are
		apparent in their course descriptions.</p>
	<p>The training at a library school like the &quot;Fachbereich Informationswissenschaft&quot; (Information Science
		Program) at Potsdam's Fachhochschule (Polytechnic) offers a large number of detailed courses in subjects like
		&quot;Development of a Relational Database using Access&quot; or the &quot;Basics of Archival Cataloguing&quot;
		or &quot;Archive Management&quot;. These are by no means old-fashioned courses, but they assume that students
		need to acquire specific practical skills in their classes.</p>
	<p>Wayne State University in Detroit, Michigan, which is not an official member of the I-School Project, offers a
		range of courses that focuses less on specific skills and more on topics that map directly to jobs within
		current libraries, such as &quot;School Media&quot;, &quot;Collection Development&quot; and &quot;Records
		Management&quot;. Wayne also offers a range of technology training without specifying particular tools.</p>
	<p>Students graduating from well-respected programmes like those at Potsdam or Wayne have the skills they need to go
		directly into an entry-level position at a library where they can begin turning out useful work with a minimum
		of additional training. This is often necessary at small libraries where on-the-job training is impossible.</p>
	<p>Nonetheless this practical orientation contrasts sharply with the broad social science oriented approach at, for
		example, the University of Michigan, whose two required core courses are called: &quot;Information in Social
		Systems: Collections, Flows, and Processing&quot; and &quot;Contextual Inquiry and Project Management.&quot;
		Illinois offers a similar example with courses like &quot;Information organization and knowledge
		representation&quot;. The range is broad. These iSchools are certainly still library schools. They have full
		accreditation from the American Library Association and libraries compete eagerly for the graduates. But they
		implicitly prepare their students for higher level positions where they need to understand issues about how
		information resources in the broadest sense fit within the research and teaching missions of universities and
		schools and how information fits into the modern economy.</p>
	<p>This above description of an iSchool grows out of observation and comes largely from US sources. It is in fact a
		description that could apply with minor changes to the elite library school programmes from virtually any period
		in the past. One difference is an increased focus on the job market for graduates beyond traditional library
		employment, but this is far from new and embraces a theme long established in French and German programmes.
		Those who study &quot;documentation&quot; are often preparing themselves for a non-library career path with
		companies, research institutes, or other organizations that need to manage information. Greifeneder studied
		documentation in France and it still exists for a few more years in legacy form as a course of study at
		Humboldt.</p>
	<p>The characteristic that most seems to define iSchools in conversations with colleagues outside of them is their
		strong engagement with computing and computer science. Instead of classes on the history of the book, iSchools
		offer classes on human-computer interaction. Instead of studying printing, students take classes in electronic
		publishing. iSchools are sometimes criticized as lightweight computer science programmes and critics say that
		they have lost touch with the profession and its past. Courses on cataloging have not vanished, but courses on
		metadata have started to crowd them out.</p>
	<p>iSchools today are evolving quickly. The iSchools Movement (<a href="#isc07">2007</a>) offers the following
		self-definition on their website:</p>
	<blockquote>
		<p>This is characterized by a commitment to learning and understanding of the role of information in human
			endeavors. The I-Schools take it as given that expertise in all forms of information is required for
			progress in science, business, education, and culture. This expertise must include understanding of the uses
			and users of information, as well as information technologies and their applications.</p>
	</blockquote>
	<p>The definition seems bland but iSchools are not. They are not preparing students for today's libraries but for
		leadership positions in tomorrow's information infrastructure, which they fully intend to help create. Their
		mission is transformative. iSchools are training innovators, perhaps even revolutionaries. To train these
		students, a unique iSchool curriculum is logical.</p>
	<h2 id="an-ischool-curriculum">An iSchool curriculum</h2>
	<p>Our proposed curriculum is an abstract designed around a set of ideas, not around what is implementable in the
		classroom. The diagram in figure 1 offers a graphic representation of the model. Human-computer interaction
		stands at the centre of this curriculum and four management tasks surround it: technology, culture, ecology, and
		collections. These balance each other in terms of organizational priorities and needs. In the corners between
		each of the management tasks are four other salient issues: the user, social computing, the information
		lifecycle, and access combined with preservation. Obviously this list is neither exclusive nor complete. It
		could have included digital libraries or electronic publishing or the semantic web. Diagramming ideas imposes
		limits on what is visually acceptable and practically readable. The issues outside of the boxes will change over
		time. The management tasks within the boxes seem to us to have greater structural permanence in any information
		organization.</p>
	<figure>
		<img src="colise02fig1.png" alt="iSchool curriculum model">
		<figcaption>
			<strong>Figure 1. iSchool curriculum model</strong>
		</figcaption>
	</figure>
	<p>The ideas that went into the design of this diagram come from anthropology and linguistics. We look at what the
		library and information world does as we might examine a remote culture. We study its language, the special
		meanings it places on certain words, and how it actually behaves as opposed to how it describes its own
		organization. In our diagram we attempt to use some language and categories that map back to standard concepts
		within that world, such as technology and collections while also imposing terms like culture and ecology that,
		while not foreign, are fresh enough to help people rethink the nature of users and staff on the one hand and
		building and electronic environment on the other.</p>
	<h2 id="human-computer-interaction-at-the-centre">Human-computer interaction at the centre</h2>
	<p>Human-computer interaction stands at the centre of our diagram because in practical terms all information access
		in traditional and digital libraries now operates through computer-based systems. While a few old card catalogs
		remain for specialized and generally minor subjects, libraries in the industrial west virtually all use OPACs
		(Online Public Access Catalogs) to show both users and staff where paper materials are kept, and digital
		materials all require some degree of human-computer mediation. Human-computer interaction has become a part of
		the curriculum in iSchools, and at some, like Michigan, it has become a major option for study.</p>
	<p>At present our interactions with computers tend to be highly verbal. Modern computer screens have pictures on
		them, of course, and designing the location of information on the screen is a serious and difficult problem that
		requires artistic design, physical testing, and some insight into human psychology. Nonetheless the real
		communication takes place with words. These may be words that we need to read on a screen to click in the right
		place, or words that we need to type on a command line. Often they are words with specialized meanings, or with
		meanings that the computer needs to interpret in some moderately intelligent way, such as by recognizing its
		potential forms, inflections, and misspellings and then mapping those options to some similar combination of
		ASCII characters that results in new information on the screen.</p>
	<p>Our dependence on words as the language of communication with computers becomes particularly obvious when we try
		to find a particular image. The name &quot;seadle&quot; is, for example, relatively unique. A simple search for
		this name in Google Images on 16 June 2007 turned up 235 hits, of which only 13 out of the first 20 showed a
		photo of Michael Seadle. Of the others, six had &quot;seadle&quot; somewhere in the accompanying text, and one
		had no apparent connection. Refining the search to &quot;photo of Michael seadle&quot; reduced the total hits to
		181, but made little difference to the selection in the first 20. A similar search for &quot;photo of elke
		greifeneder&quot; produced 130 hits but only six actual photos of Elke Greifeneder out of the first 20. Oddly
		enough the search worked better months ago before Greifeneder wrote for <em>Libreas</em>, an online journal.</p>
	<p>The effectiveness of our language for communicating what we want from the computer depends heavily on the
		information the machine has to work with, which in the case of Google Images is only text that happened to be in
		the same document or web page. Adding the further detail &quot;female brown hair&quot; to the Greifeneder search
		or &quot;male brown hair&quot; to the Seadle search produced no hits. Our human-computer interaction failed, not
		because the computer lacked the photos or an ability to understand natural language. It failed because Google
		did not have a facility to translate what we as humans see in the pictures into the words we use to describe
		them. It relies only on accidental associations to build a language model for search results.</p>
	<p>These language issues seem fundamental to our ability to search for information. Fortunately semantic issues have
		started to become a core part of the iSchool curriculum thanks to studies of the semantic web.</p>
	<h2 id="managing-cultures-and-ecologies">Managing cultures and ecologies</h2>
	<p>Libraries have multiple cultures or micro-cultures that both use and work in them, and libraries have both
		physical and digital ecologies that they need to recognize and maintain. We have applied the word
		&quot;manage&quot; to both in suggesting the need for active involvement and cultivation. iSchools ought not
		merely teach students to recognize these cultures and ecologies, but to engage and change them.</p>
	<p>A micro-culture is any subgroup within an existing social setting that has its own specialized language that is
		part of its everyday world and is to some degree opaque to those outside the micro-culture. The simplest example
		of a micro-culture within the library community is a technology unit. A simple phrase like &quot;reboot the
		system&quot; may be incomprehensible to a non-tech librarian who does not automatically translate the term into
		&quot;turn the machine off and start it again.&quot;</p>
	<p>Some of the micro-cultures belong to the larger group of library staff, who also share a common vocabulary that
		is often incomprehensible to the outside world. A &quot;known item search&quot; is a standard phrase within the
		library community and perplexingly unclear to users. User communities have their own private languages and
		micro-cultures. Students of biology might ask about syzygium aromaticum while a cook might ask about cloves. Not
		every librarian will or can know that they are the same and the choice of term may imply a very different type
		of information request.</p>
	<p>The iSchool curriculum will not be able to teach students all the detailed information they need to answer
		questions from both cooks and biologists, but it can make students conscious of the communication differences
		across micro-cultures. The traditional reference interview is in part a language negotiation to trade vocabulary
		and improve understanding. iSchool students need to think about this more abstractly, not merely in terms of one
		human negotiating with another, but a human facing a computer screen whose language offers incomprehensible
		choices. The help systems in something so basic as our online catalogs represent a first line attempt at
		managing the cultural and linguistic interaction for far more users than ever come to a reference or information
		desk in a physical library.</p>
	<p>The ecology of a library or information resource requires the same level of abstract consideration.
		Anthropologists pay considerable attention to the environment in which people work and interact. For traditional
		libraries this environment is a physical building and the degree to which a building is open, light, with
		freely-accessible materials, and comfortable, well-equipped places to use them will make a difference in whether
		users return. For online resources the screen-based ecology matters even more. An ugly, cluttered, ill-organized
		screen with ineffective help systems will send users elsewhere. Libraries no longer offer exclusive access to
		information. Users today who cannot get the works they want from the OPAC will try Google and will probably find
		something there, even if it is incomplete, outdated, or inaccurate. Students need the conceptual tools to
		recognize these problems as a structural element of our information ecology.</p>
	<h2 id="managing-technology-and-collections">Managing technology and collections</h2>
	<p>iSchool students need to learn enough specifics about the technology of the information world to manage it. At
		one time learning library technology meant understanding the structure and organization of the card catalog with
		its rods and various other devices for keeping cards in place, and its rigid alphabetical organization, which
		was rife with complex and often locally various rules about how to handle names beginning with Mc and Mac, or
		letters with umlauts or accents, or &quot;stop-words&quot; in corporate names and titles. Today the technology
		is different but the need is similar. It includes topics like HTML, XML, Java script, and relational databases.
		iSchool students should look at these systems the way anthropologists regard the technologies of other cultures.
		Why do people in, say, Madagascar pick particular tools or choose one site over another to build a house? It is
		not the skill with the tool that iSchool students need, but an understanding of the decision-making that
		surrounds and informs the tool users, today mainly programmers, so that they can give directions to them.</p>
	<p>Some iSchool students will also show an interest in delving deeper into the technology world. They may also study
		computer science or have a computer science background. It is clearly a plus to have strong technical and
		computing skills to be able to manage and create new technology tools for information systems. Librarians and
		information system managers who do not understand the internal working of systems tend to make black-box choices
		based on an effective sales pitch rather than on a systematic analysis of engineering specifications and
		realistic performance outcomes. No iSchool curriculum has a magic potion for warding off technology
		misjudgments, but it can inoculate students with an awareness of the kind of information they need to make
		rational technology decisions.</p>
	<p>Managing collections is one of the most traditional library jobs and is no less important in a digital age.
		Collections may be books and journals in either paper or digital formats, but they may also be images, numeric
		data, or data whose representation does not fit standard categories but needs curation and access nonetheless.
		Selecting content matters, but the iSchool curriculum particularly needs to help students understand the
		linguistic issues involved in describing these collections. Producing accurate descriptions in the metadata
		matters, but an equally important issue is the vocabulary used in the systems (and help systems) that connect
		users to the materials.</p>
	<p>The connection also has to occur in a way that works technically. Some libraries worry about the long-term
		readability of documents in archiving systems. iSchool students should remember that readability is just another
		human-computer interaction. A collection of data in the most modern xml standard might be useless to users who
		have only relatively old software on their computers that cannot translate the new standard into text or images
		on the screen.</p>
	<h2 id="allied-issues">Allied issues</h2>
	<p>iSchools have a broad choice about the issues that they should bring into their curriculum. Some issues seem to
		endure year after year, such as how we handle users. Others are very new, such as social computing. The four in
		our diagram are just contemporary samples. The key question is how an iSchool curriculum can address them in a
		consistent and scholarly way.</p>
	<p>At first glance these four issues seem to have little to do with each other, though we did attempt to draw some
		lines of connection with the management boxes that neighbor them in the diagram. In practical terms access and
		preservation means having an ecology that makes collections available long term. Users are part of the
		management of micro-cultures and need to fit within the information ecology. Social computing relates user
		cultures to a technology that enables interaction. And the information life-cycle is a collection-based issue
		with strong technology dependencies.</p>
	<p>Looking at these issues as problems in human-computer interaction provides a leitmotiv that highlights our role
		as librarians and information brokers. Especially within an iSchool environment, we interact constantly with the
		computers that provide information resources. Decisions about when to discard information that has reached the
		end of its lifecycle depends, for example, on what we mean by &quot;discard&quot;. It might be off-line storage
		for digital objects or actually throwing old newspapers into the recycling because long-term access will come
		through microfilm or (preferably) electronic form. Our systems need to communicate what has happened or is about
		to happen and where the information is or perhaps is not.</p>
	<p>iSchool students do not need to have answers to these problems, but they need to have way to think about these
		and other issues that give them a broad perspective about the interests of the groups and micro-cultures that
		may be involved, as well as the technology tools available, to structure the information ecology and meet user
		needs.</p>
	<h2 id="ischool-research">iSchool research</h2>
	<p>Research should be an integral part of the iSchool curriculum. The ideal is for students to play an active role
		in faculty research projects, and to have roles where they can make at least some of the decisions on their own,
		or participate meaningfully in the decision process, and then learn how to write up the results.</p>
	<p>As figure 2 shows, the potential research topics can and should come from real-world issues that connect our
		academic work with problems that librarians and information practitioners face in their workday lives. Figure 3
		describes the dynamic between allied academic fields, research methods, and the research question.</p>
	<figure>
		<img src="colise02fig2.png" alt="Potential LIS research topics">
		<figcaption>
			<strong>Figure 2. Potential library and information science research topics</strong>
		</figcaption>
	</figure>
	<figure>
		<img src="colise02fig3.png" alt="Dynamics of allied fields">
		<figcaption>
			<strong>Figure 3. Dynamics of allied fields with library and information science as the point of
					departure</strong>
		</figcaption>
	</figure>
	<p>The role of the allied fields is especially important. Library and information science has no method of its own.
		It more resembles content-defined disciplines like history or music that have clear topics and borrow whatever
		methods best suit the researcher and the topic. This is one reason why it is urgently important that iSchools
		offer an interdisciplinary environment.</p>
	<p>iSchools also need to train students how to pose a research question. Too often research for masters and even
		doctoral theses start with content that students want to study, but have no problem they want to solve and no
		method to use for analysis. This can result in dryly descriptive works that repeat existing literature rather
		than contributing to the scholarly discourse as a research thesis should.</p>
	<h2 id="reaching-the-goal">Reaching the goal</h2>
	<p><em>&quot;The Information School community is also dedicated to living what we study, teach, and share. This
			means that a collaborative culture is central to our work.&quot; -- Information School, University of
			Washington, 2007</em></p>
	<p>This statement from the Information School at the University of Washington reflects its philosophy about how to
		achieve the intellectual and educational mission of an iSchool. No library school or institute can neglect
		considering the limits imposed by established programmes, established faculty, and financial resources. There is
		no simple consensus about what the right programme is and any real-world implementation has to involve the whole
		of the students, faculty, staff, and administration to create a result that is transformative, not divisive.</p>
	<p>For those who like this ideal curriculum and want to try it should at a minimum retain three key principles: 1)
		all information services now revolve around human-computer interaction; 2) teach students to think like
		anthropologists and look at the problems and issues from multiple viewpoints, multiple cultures, and multiple
		ecologies; and 3) students need to remember that language both enables and limits our ability to communicate
		with contemporary information systems - without a strong awareness of linguistic issues, we cannot provide
		information.</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="buc98"></a>Buckland, Michael. 1998. What is a &quot;digital document&quot;? Document Numérique
			(Paris) 2, no. 2. Pp. 221-230.</li>
		<li><a id="fet86"></a>Fetterman, David M &amp; Mary Anne Pitman, eds., 1986. Educational Evaluation: ethnography
			in theory, practice, and politics. Sage: Beverly Hills</li>
		<li><a id="isc07"></a>&quot;iSchools Project&quot;. 2007. Available: <a
				href="http://www.ischools.org/oc/">http://www.ischools.org/oc/</a></li>
		<li><a id="uni07"></a>University of Washington, Information School. 2007. Mission and Vision. Available: <a
				href="http://www.ischool.washington.edu/mission.aspx">http://www.ischool.washington.edu/mission.aspx</a>
		</li>
	</ul>

</body>

</html>