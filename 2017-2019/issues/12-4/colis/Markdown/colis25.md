#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# Digital Music and Generation Y: discourse analysis of the online music information behaviour talk of five young Australians

#### [Justine Carlisle](mailto:jcarlisle@aim.edu.au)  
#### Australian Institute of Music, Sydney Australia  
#### University of Technology, Sydney, Australia

#### Abstract

> **Introduction.** This paper presents a research study into competing discourses in digital music; highlights discourse analysis as a research methodology; and social constructionism as a conceptual basis for library and information science research.  
> **Method.** In-depth interviews focussing on a discussion of music in the online environment.  
> **Analysis.** Discourse analysis is used to reveal existing and competing repertoires within the field of digital music.  
> **Results.** Three repertoires are identified and described. The repertoires contextualise discussions of digital music.  
> **Conclusions.** The research found that music digital library users are potentially coming from vastly different perspectives. Recognising that more than one perspective exists is necessary for developing systems that are usable by more people. Future research is needed into how the various stakeholders are using these discourses to further their own ends. Discourse analysis provides a strong research-based description of context for user information behaviour; and is shown to be a highly relevant theory and methodology in library and information science research.

## Introduction

Discourse analysis is a research method in library and information science (LIS) that was successfully employed by Sanna Talja in her book _Music culture and the library_.[Talja 2001](#tal97) The author has taken the research methodology used by Talja and applied it to a study of the Music Digital Library (MDL). Discourse analysis is well suited to a study of the digital library because it can be used to articulate multiple user perspectives.[Tuominen _et al._ 2003](#tal97) The conceptual basis for discourse analysis is social constructionism, which emphasises the social nature of the construction of knowledge. The aim of this paper is to present the results of a study conducted using the discourse analytic method; and to demonstrate the benefits of a social constructionist perspective and discourse analysis as a research method in the field of LIS.

Library and information science theory influences research in the field. These theories affect the whole of the research process, from the problems articulated to the research methods used and the solutions proposed. Being aware of your theoretical framework within LIS is fundamental to being a reflexively aware researcher. The 'user' is a key concept in LIS; as noted by Dervin and Nilan in 1986\. [](der86)However there are differing schools of thought as to how to approach the user's perspective in information behaviour research. It can only be beneficial to the field if new and emerging perspectives such as social constructionism can work alongside more established theoretical viewpoints to better achieve a holistic understanding of information behaviour. Some of these differing viewpoints will now be explored.

## Concepts in information behaviour theory

The cognitive viewpoint in LIS has had a major influence on information behaviour research. Talja defines the cognitive viewpoint as essentially a theory of how individuals process information; a study of the individual's mental processes and the effect of information on the individual. [Talja 1997: 67-8](#tal97) This has lead to an understanding of information 'need' as the basis for information seeking behaviour. The user is seen as having an information problem and seeks information for the reduction of uncertainty. Maintaining a view of information seeking as a reduction of uncertainty holds little room for an exploration of context in information seeking. [Talja 1997: 172-3](#tal97) Cognitive theory has been criticised for focusing too much on individual mental processes to the detriment of the social; focussing on uncertainty and 'need' in information behaviour and ignoring users' expertise; and for its focus on purposive information seeking. [Olsson 2005](#ols05) The cognitive viewpoint in LIS is very task-based and is therefore well suited to a study of that kind; but not to studying the broader social contexts of information behaviour.[Talja _et al._ 2004: 85](#tal04)

By contrast, social constructionism emphasises linguistic processes in the construction of knowledge formations. This perspective sees communication through language as how we produce and organize social reality.[Talja _et al._ 2004: 89](#tal04) When the user talks about her experiences she is employing a pre-existing discourse. Knowledge is a linguistic and social product created in conversation and communication, not in individual minds. Reality is constructed together - through conversation. [Tuominen _et al._ 2003: 564](#tuo03) The discourse analytic viewpoint is both a linguistic-philosophical theory and a research method. [Talja 1997: 68](#tal97) As a theory it looks outward, taking its focus away from the individual to look more generally at the formation of knowledge through language. Research into the user's perspective is in fact research into more general knowledge formations. As a research method, discourse analysis lends itself well to the study of a particular subject area. Both the user and the information system were created and function within existing concepts and categories. [Talja 1997: 76](#tal97) Existing and institutionalised discourses set the terms in which a user's 'needs' for information are articulated. [Frohmann 1994: 135](#fro94) In LIS, this kind of analysis can reveal the hidden motivations behind user statements and policy documents. Discourse analytic research can also be used to compliment more task-based research.

## Music on the Internet

Ideally, information systems should be designed with the user in mind. Many times however, the technology seems to race ahead and then usability catches up. A prime example of this is digital music. Digital technology has created a crisis in the traditional music industry's profit making abilities. Compressed music files called mp3s have enabled music to be stored in large quantities and transferred across the world via that internet. Music users have the ability to obtain more music for free than ever, via peer-to-peer software. Minimising the physical objects associated with listening to music has also made music mobile, with the advent of mp3 players. The sheer quantity of the music being shared for free has sent shock waves through the music industry, as CD sales are where most of their profits have traditionally been generated. Enabling the purchase of music online has become a priority for the industry. [IFPI 2006: 3](#ifp06) In the rush to keep up with demand Music Digital Libraries (MDLs) have generally been designed based on intuition or anecdotal evidence; there is a genuine lack of user studies in this area. [Cunningham _et al._ 2003](#cun03) [Lee and Downie 2004: 441](#lee04)

## Digital music - a discourse analytic approach

The author has conducted a qualitative user study into the online music information seeking of five young Australians. The goal of the study was to understand the user's perspective through an analysis of the available discourses in digital music. Based theoretically and methodologically on Talja's 2001 book _Music Culture and the Library_, the study asks: How do the differences in conceptions of music affect the way the aims of a digital music library are defined? How can these differences be explained?

Discourse analysis is particularly suited to studying a defined subject area, such as music. Previous user studies into MDL users have concentrated on identifying and describing specific user behaviours, in the form of tasks. [Cunningham _et al._ 2003](#cun03) [Lee and Downie 2004](#lee04) Talja's discourse analysis of more traditional music libraries suggests a different way of understanding information behaviour; revealing competing motivations behind music library user talk. The much broader range of digital library users, compared to the traditional library, has created a greater need for multi-perspective digital libraries. The challenge for systems design is to incorporate these differing viewpoints into a useful information system. [Talja _et al._ 1997: section 3.2](#hei97)

When using discourse analysis as a method of data analysis close attention needs to be paid to the detail of language use, based on document transcripts. These transcripts are the data from which conclusions are drawn; the focus is not on the interviewee's cognitive processes, but the discourse from which they are drawing their comments. When analysing the data, a key indicator of competing discourses is variations or contradictions made by the speaker. Contradictory statements made by an individual can reveal the use of different discourses. These discourses can reveal themselves in different situations and may serve different functions. [Wetherell and Potter 1988: 172](#wet88) When analysing user 'talk' from the discourse analytic viewpoint the researcher comes from the perspective that language is socially constructed. People use pre-existing language resources to construct what they are saying; people actively select a discourse and the use of that discourse has a practical consequence. [Wetherell and Potter 198: 171-2](#wet88)

## Sampling methods and demographics

The interview respondents were between the ages of 18 and 22 - an age group popularly referred to as Generation Y. [Huntley 2006](#hun06). This age range was chosen due to their high internet usage and music listening habits and both these suppositions proved to be true of the respondents. Five respondents were interviewed, two females and three males. Three attended the Australian Institute of Music (AIM), a school that specialises in music and music business studies; the other two attended the University of Technology Sydney (UTS) and studied communications, majoring in information management. The respondents from AIM were chosen by purposive sampling and are known, on a professional level, by the interviewer; the UTS students replied to a general email request for interviewees. These two institutions were chosen for convenience sake, being respectively the work place and the attending university of the researcher. The respondents were a highly multi-cultural group, their backgrounds ranged from Vietnam, Ghana, Taiwan, China and European countries, which is reflective of the Australian population in general. Four of the five were musicians or involved in the music industry in some capacity.

## Data collection and analysis

data were collected through in-depth interviews, taking from 30 minutes to one hour. Interviews were conducted using an interview guide with mostly open-ended questions, enabling the respondents to talk about what was of interest to them in relation to music and the internet. The respondents were encouraged to interpret the questions in their own way; giving them some influence on the direction of the interview. For example the question: 'What is your opinion of music online?' Several respondents queried the meaning of this question and they were encouraged to interpret it in their own way.

The data were analysed using the discourse analytic method, specifically the analysis method used by Talja in _Music Culture and the Library_. [2001](#tal01) This method was used to identify the different knowledge formations, or discourses, inside a particular field. [Talja 1997: 74](#tal97) Analysis was done by transcribing the interviews then identifying contradictory statements made by the speakers; and also statements of belief shared across different speakers. A thorough reading of the text was necessary to bring the repertoires to light.

## Results

Three repertoires are described and will be discussed in the context of previous research findings. The respondents' names have been altered to protect their identities. Later the implications for MDLs and LIS research in general will be discussed.

## The Romantic repertoire

The romantic repertoire holds many similarities with Talja's [2001](#tal01) common culture repertoire. Talja's repertoire focussed on Western art music, but for the respondents in this study the repertoire was referring to musical skill in general, not just classical musicianship; therefore the repertoire name has been changed to reflect this difference. The romantic repertoire was mostly prevalent amongst the music students interviewed. This repertoire was used to emphasise the expertise and cultural importance of musicians, or those that have studied music. It was used as an argument against illegal downloading, which can be viewed as disadvantaging the artist. This repertoire places high importance on the study of music, as opposed to listening to music for enjoyment.

This repertoire holds that musicians' knowledge about music is privileged and it is something that has to be deliberately studied. Because of this knowledge the musician can listen to music on a higher level than the average listener. For the rest of the population music is just entertainment:

> The difference between people who study music and don't is that people do it for leisure and they don't really appreciate the whole thing. (David)

In terms of online music, the romantic repertoire has a deep concern for the genuine musical artist and their personal investment and contribution to their art. They have put so much effort into their recording and it is such a personal expression; they don't deserve to be 'ripped off' by things such as illegal downloading. The artist should be supported:

> A lot of the time I have downloaded it and I like it then I'd go and support the artist just for the sole reason of doing that. (David)

For everyone interviewed 'illegal downloading' was referred to by the term "downloading"; and they had all "downloaded" at one time or another. This repertoire is against downloading and the effect it has on the artist. The contradiction between being against downloading but doing it in practice did not stop them from being highly moralistic when discussing the topic. It also did not faze them that strictly speaking downloading is not suddenly made legal if you go and purchase the CD later. It is not the illegality of the action that concerns this repertoire; it is the artist's welfare, and buying the product later will still help the artist:

> If people are willing to download it they should be able to purchase it as well. I don't agree with - even though I do it- it's kind of a ying yang thing I will download music that is new off the charts. I download music that can be heard on the radio but can't be purchased yet so I can listen to it for an ongoing occasion, do you understand what I'm saying? Until the single comes out or the album comes out so that I have a proper copy of the artist's music. But generally I will buy the single and the album because they have remixes on there and instrumentals so I will purchase those. (Susan)

This repertoire is effective as an argument against the free distribution of music online, and is one employed by the music industry, amongst others, on a regular basis. In Talja's study the common culture repertoire was used as an argument for more artistic grants, which is essentially a similar concept. [Talja 2001: 202](#tal01)

## The consumer culture repertoire

This repertoire is one identified by Talja [2001](#tal01) and also appeared in the interview transcripts for this study. There are two major concepts in this repertoire: music can be either a commercial enterprise or authentic artistry; most people are passive in the face of mass marketing, but a select few have a more thoughtful or intellectual appreciation of music.

In this repertoire there is a belief that there is passivity in the general populace towards music. Commercialism is the basis for a lot of music and turns people into mere consumers. Most people are _"being fed music rather than deciding what you want to eat."_ (David). Other music listeners have a deeper connection with the artist, and can distinguish between the "manufactured" artists and "authentic" artists. What these terms mean differed between respondents. The consumer culture repertoire was used to argue a number of different points about music appreciation, free downloads, and 'the good old days'.

The user builds a persona for themselves in their choice of music, someone who does not like trashy music - _"I don't like just pop singers like Britney Spears, no."_ (Dianne) There is a kind of power involved for the consumer. An artist has to prove themselves worthy before you will buy their product. To this end there is an ongoing search for music that is honest, personal, original, or that has a message:

> …the reason I buy the music is that I can see the star has put in effort, not just a pretty face. And it is really important to let us know. Let the information spread from news or media to let us know the singer has put a lot of effort or the singer's stories or background and then I will buy it. I like it. (Dianne)

In this repertoire there is a concern that a lot of music is manufactured for the sole purpose of making money - _"It's churning it out by the truckloads."_ (Frankie) The musician should be honest and genuine, not just a face used to market a pop song. If they are a highly successful act it helps if they support a worthy cause. Generally musicians should be doing it for the love, not for financial gain.

> Music is something that you work on because you like it, it should be something you enjoy, not sell for money. (Frankie)

> I don't like music being used commercially because that's not what it was intended for. When people use music it was to convey some sort of emotional feeling. I choose not to listen to music that is there just for the money, not for anything else. (David)

This repertoire can be used as a pro-downloading argument. Music should be free, because real musicians do it for the love of it, not for money:

> I reckon taking away Kazaa was a big step back… Because I mean we already have the tools and I mean, why can't certain things be free? … It's a step backwards in moral terms and what we want to achieve as the human race in the long run and showing that yes we can be generous with music and musicians and we can share with the wider public. (Oscar)

There is a strong sense of nostalgia in this repertoire. The overall cheapening of music is seen as a fairly recent phenomenon. Recent advances in technology have adversely affected music and our relationship with it. There is a desire for a simpler time, with less choice and a slower pace of life. Their parents had quality acts like the Beatles and Bob Marley, or even more recently there was Public Enemy and Boyz II Men. These acts were all sited as evidence that the quality had gone down in recent years. There is nostalgia for the good old days when there was better quality music and people took more time to listen. There was less music then, and people were more unified in their musical taste:

> In primary school there used to be the one band, one good band that everyone liked, not necessarily good but at the time everyone was into it and this would go on for a month or so, but now there's new bands coming up all the time and unless it's a really good song it never really lasts that long in the minds of people because there is always another one to replace it. (Frankie)

## The multi-cultural repertoire

In the multi-cultural repertoire music has the potential to bring people together and can communicate across cultures. All types of music have value for someone, so pretending that one artist is more deserving than the next is snobbery. If the music touches someone then it has worth, regardless of its objective quality:

> I don't listen to people according to the tastes of the media I just listen to it if I like it, I don't care if it's like teeny bopper music, if I like it I will listen to it because I like it. (Frankie)

There is a judgmental attitude amongst some music listeners that can be frustrating to this repertoire. Having a strong opinion against certain types of music or people that listen to that music is ignorant, because people should be able to listen to whatever they like without judgement. Having your taste dictated to you is also unacceptable in this repertoire:

> Well everyone thinks the music they listen to is individualised and represents themselves. I don't really think that. I think it's all the same; it's just the way you perceive it. (Frankie)

What music you listen to isn't so much a conscious, ethical choice, it depends on the circumstance or context and where you are that moment of your life. If music affects someone in a positive way at that time it has served its purpose.

> It just depends on where you go and where your life takes you there is always music to cater for you. (David)

This repertoire also sees music as a potentially unifying force. The ability to appreciate music that other people like, or music from other cultures is important, and music can help us break down cultural barriers.

> …I don't think there's a single person anywhere who is not touched by music in some way or form. If you want to use it as a communicative means then that's probably the best way to go about it. Music is universal. You don't have to have a song specifically in somebody's language to for them to understand its meaning or a perception of it. (David)

> I think music is the world's greatest healer because everyone can relate to it, it doesn't have a colour doesn't have a race, it has a history but everyone can relate to it. (Susan)

> …Music brings people together. (Oscar)

Music serves a purpose in life, and generally makes things more pleasant or compliments certain activities. Taking your musical preferences too seriously is ridiculous, and it is best to avoid people who are too judgemental about music:

> I prefer to discuss it with friends who I am better acquainted with rather than just casual acquaintances because occasionally there will be a clash of tastes of music and with good friends you can just make a joke about it but with people you are not really familiar with it's a bit hard, sometimes people get offended - yeah they get offended if you don't like their music! (Frankie)

This repertoire can be used to avoid judgement. The fear of being judged for your musical taste can be a concern, and use of this repertoire lets people know that you are not buying into it.

## Discussion of the results

The research found that music digital library users are likely to be coming from vastly different perspectives. Their needs differ and they see the concept of music in very different ways. Many of them have a very high personal stake in their musical perspective; it can be as fundamental as their conception of themselves or may influence their livelihood. The three repertoires represent ways of talking about digital music. They are pre-existing discourses adopted by the respondents in this study to justify their beliefs, feelings or actions. The repertoires contradict each other, and yet are used by the same person, albeit at different times. This is not a failing of the speaker; it is a natural way of talking and reflects the contradictions we all make, particularly when asked to speak about a new subject. The purpose of defining the repertoires is not to point out that people contradict themselves, it is to point out that the repertoires exist and will inform any talk about digital music.

Retrospectively, the repertoires can be used to explain talk in other user studies in the field. For example, the multicultural repertoire derides fandom and the judgemental attitude that goes with it. In Cunningham et al, one respondent referred to reading professional music reviews negatively, as something that only "real fans" do. [Cunningham _et al._ 2003: section 4.4](#cun03) Similarly, there was an aversion to asking questions of record store staff, who are seen as hostile or judgemental. [Cunningham _et al._ 2003: section 4.8](#cun03) The respondents in these studies appear to be using the multi-cultural repertoire to explain their behaviour.

The romantic repertoire is used when artists feel their livelihood is threatened by illegal downloading, and represents the musician in their professional capacity. The multicultural repertoire is frustrated by musical snobbery. This repertoire is used as a defence mechanism when the speaker feels their knowledge is being questioned. The consumer culture repertoire empowers the music listener as someone who is savvy and not susceptible to commercialism and manipulation; it is about self-concept. The repertoires create their own divisions, in order to prove a point. The romantic repertoire creates a division between the musical artist and the public - the artist is someone who deserves to be supported for their talent. The consumer culture repertoire creates a division between commercial culture and authentic culture and active musical thinkers and passive consumers. [Talja 2001: 202](#tal01) The multi-cultural repertoire produces a division into people who take themselves too seriously and are too judgmental and those that enjoy music in a more open-minded way.

The crisis in the ability of technology to protect and maintain traditional music industry profit streams has lead to an explosion of ideas on how to approach the new world of music sharing. The record industry has developed copy protection technology to prevent the ripping of music from CDs; legal online downloading sites, where you can purchase your music in an mp3 format; and has prosecuted illegal file sharers and the websites that enable this behaviour, such as Napster, and more recently Kazaa. [Ayres 2004](#ayr04) These actions may satisfy the romantic repertoire, but not the consumer culture repertoire. Others, who have an idealistic view of the internet and what it could be or should be have offered alternatives, mostly revolving around subscription services to a large music library that would do away with the concept of paying for individual tracks and make music more of a utility service. [Kusek and Leonard 2005](#kus05) This approach could satisfy the multi-cultural repertoire, but the consumer culture repertoire would also reject this option that is essentially reliant on an externally controlled music library. The current illegal system tends to satisfy the consumer culture repertoire the most, but is not acceptable to the romantic repertoire.

## Conclusion

This paper has demonstrated the use of discourse analysis as a research method within a social constructionist framework. The research presented is not task-based or focussed on concrete actions; such as search strategies, song selection or genre preferences. Rather, the study focuses on talk about these behaviours. The three repertoires represent ways of talking about digital music; they are pre-existing discourses adopted by the respondents in this study. These discourses necessarily form the basis for any talk on this subject and enable us to give context to future research. The repertoires reveal the background assumptions people make when discussing digital music. For digital libraries it articulates a multiplicity of perspectives that could be employed by users and other stakeholders. While this can be problematic to the systems designer, recognising that more than one perspective exists is necessary for developing systems that are relevant to a broader range of people.

Information behaviour can be viewed in ways other than user needs, specific search tasks or knowledge gaps. Sometimes users are the experts, and keeping up with them is the name of the game. The explosion of digital music is a case in point. Online music users have charged ahead with peer-to-peer technology, cutting out the middle-man - the record companies - and depleting a profit source. At the same time, music is more popular than ever now that people can take control of listening, sharing and even manipulating music, in new ways.

Digital music is a new frontier, one that is still in the process of being developed in regards to technology, and the legal implications for creative rights. Discourse analysis is particularly useful in highlighting how stakeholders present their perspectives, particularly when arguing for changes in copyright law; arguing a legal case against file sharing; or creating a new service for online music users. All these things impact on librarians as the changes can have far reaching effects on the information commons. The recent Australian Copyright Amendment Act 2006 for example, has directly reacted to changes in technology, and specifically mentions libraries. Future research is needed into how the various stakeholders are using these discourses to further their own ends. It is important for librarians to be able to tell the difference between online music sources, and whatever their moral position on the subject is, to be aware of the legal implications. The competing discourses in online music have been shown to have strong motivations behind them, not the least being financial considerations; being aware of these discourses can better inform future LIS practice and policy.

## Acknowledgements

The author would like to thank Dr. Michael Olsson for reading and critiquing drafts of this paper and also for his support and encouragement throughout the writing process.

## References

*   <a id="ifp06"></a>(2006). _IFPI:06 Digital Music Report._ London: International Federation of the Phonographic Industry (IFPI)
*   <a id="ayr04"></a>Ayres, C. (2004). Why the suits are calling the tune. _The Times_, 26th January 2004.
*   <a id="cun03"></a>Cunningham, S. J., Reeves, N. and Britland, M. (2003). _An ethnographic study of music information seeking: implications for the design of a music digital library._ Paper presented at the Joint Conference on Digital Libraries, Houston, Texas.
*   <a id="der86"></a>Dervin, B. and Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**. 3-33.
*   <a id="fro94"></a>Frohmann, B. (1994). Discourse Analysis as a Research Method in Library and Information Science. _Library & Information Science Research,_ **16**, 119-138.
*   <a id="hun06"></a>Huntley, R. (2006). _The world according to Y: inside the new adult generation_. Crows Nest NSW: Allen & Unwin.
*   <a id="kus05"></a>Kusek, D. and Leonhard, G. (2005). _The future of music: manifesto for the digital music revolution_. Boston: Berklee Press.
*   <a id="lee04"></a>Lee, J. H. and Downie, S. J. (2004). _Survey of music information needs, uses and seeking behaviours: preliminary findings_. Paper presented at ISMIR 2004: 5th International Conference on Music Information Retrieval, Barcelona, Spain.
*   <a id="ols05"></a>Olsson, M. (2005). _Beyond 'needy' individuals: conceptualizing information behavior_. In Proceedings 68th Annual Meeting of the American Society for Information Science and Technology (ASIST), vol. 42, Charlotte, N.C.
*   <a id="tal97"></a>Talja, S. (1997). _Constituting "information" and "user" as research objects: a theory of knowledge formations as an alternative to the information man theory_. In Information Seeking in Context: proceedings of an international conference on research in information needs and seeking and use in different contexts 14-16 August, 1996, Tampere, Finland.
*   <a id="tal01"></a>Talja, S. (2001). _Music, culture, and the library: an analysis of discourses_. Lanham MD: Scarecrow Press.
*   <a id="hei97"></a>Talja, S., Heinisuo, R., Pispa, K., Luukkainen, S. and Jarvelin, K. (1997). _Discourse Analysis in the Development of a Regional Information Service_. In LIS Research and Professional Practice. Proceedings of the 2nd British-Nordic Conference on Library and Information Studies, Edinburgh.
*   <a id="tal04"></a>Talja, S., Tuominen, K. and Savolainen, R. (2004). "Isms" in information science: constructivism, collectivism and constructionism. _Journal of Documentation_, **61**, 79-101.
*   <a id="tuo03"></a>Tuominen, K., Talja, S. and Savolainen, R. (2003). Multiperspective digital libraries: the implications of constructionism for the development of digital libraries. _Journal of the American Society for Information Science and Technology_, **54**, 561-569.
*   <a id="wet88"></a>Wetherell, M. and Potter, J. (1988). Discourse analysis and the identification of interpretative repertoires In Antaki, C. (Ed.) _Analysing everyday explanation: a casebook of methods_. (pp. 168-183). London: Sage Publications.