#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science?"Featuring the Future". Poster abstract

# The smart ones: one-person librarians in Ireland and continuing professional development.

#### [Eva Hornung](mailto:e.hornung@sheffield.ac.uk)  
Department of Information Studies, 
University of Sheffield, 
Sheffield S1 4DP, 
UK

## Introduction

Little is known about one-person librarians in Ireland, let alone their continuing professional development (continuing professional development) needs. Even their overall number has never been established. This phenomenographic study examines conceptions of Irish one-person librarians of continuing professional development, their needs and future demands, level of participation as well as areas for improvement in provision. The position of one-person librarians within an organisation is unique in that that they cannot participate in professional development the same way librarians employed at a larger library can. Time, lack of money and lack of support can be barriers. Yet the importance of continuing professional development is evident. As An Chomhairle Leabharlanna/The Library Council stated in its key report Joining Forces ([1999](#cho99): 175): 'Professional education should be relevant to the practising needs of library and information service staff and should reflect their diverse needs in a consistently changing environment.The lack of provision for continuing professional development is a major barrier to the development of libraries and information services.'

## Method

The importance of meaning is at the centre of this research project. The purpose of it is to investigate one-person librarians' opinions on continuing professional development as stated in their own words and reflecting their own experience of phenomena, thus engaging in an empirical approach. Phenomenography takes this 'second-order perspective' investigating underlying ways of experiencing the world, phenomena and situations ([Marton and Booth 1997](#mar97)).

The project is also informed by the tradition of evidence-based librarianship, which has been described as '... a means to improve the profession of librarianship by asking questions as well as finding, critically appraising and incorporating research evidence from library science (and other disciplines) into daily practice. It also involves encouraging librarians to conduct high quality qualitative and quantitative research.' ([Crumley and Koufogiannakis 2002](#cru02): 62).

### Literature review of relevant studies and documents

The following fields were examined:

*   Adult education theories, Lifelong Learning and continuing professional development
*   continuing professional development for librarians in general in other countries
*   One-person librarians and continuing professional development in other countries
*   Lifelong Learning in Ireland, which includes workplace learning
*   continuing professional development programmes for other professions in Ireland
*   Phenomenographic research in librarianship and information science

### In-depth face-to-face semi-structured interviews with librarians

These will be pilot tested in order to exclude any ambiguities of wording, to gain confidence as an interviewer and to ensure that the recording equipment is in working order. The pilot interviews will not form part of the final set of interviews to be analysed.

_Purposive sampling_ will be used, which is widely employed in phenomenography. Potential bias in selection is acknowledged, but as the emphasis is on maximising the variation in ways of experiencing a phenomenon, a broad variation of characteristics of the participants is needed:

*   both sexes
*   rural and urban locations
*   different work settings (e.g., public, academic, special libraries)
*   work experience in one-person librarians.

### Interviews with one-person librarians and continuing professional development experts

Additionally, sets of interviews with librarianship and information science course providers in Ireland and some experts from outside Ireland will be conducted in order to understand their experiences and make recommendations for improvement.

## Analysis

Usually, more than one researcher analyses the interviews, with others playing 'devil's advocate' when checking on interpretive rigour, but there have been studies were a lone researcher successfully analysed a phenomenography study, particularly in PhD projects (e.g. [?kerlind 2005](#ake05), whose doctoral supervisors fulfilled that role).

## Results

So far, an extended literature review has been conducted. It focused on other studies on continuing professional development for librarians in general and some key texts on continuing professional development for one-person librarians in other countries.

## Conclusions

Many other professions have already acknowledged the need for continuing professional development and have developed models of the continuing professional development process, e.g., nurses and therapists.

<figure>

![Figure 1: The Continuing Professional Development cycle](../colisp06fig1.jpg)

<figcaption>

**Figure 1: The continuing professional development cycle (taken from [Alsop 2000](#als00): 7)**.</figcaption>

</figure>

It is hoped that one outcome of this study would be recommendations for a national framework for continuing professional development for one-person librarians.

## References

*   <a id="ake05"></a>Åkerlind, G. (2005). Phenomenographic methods: A case illustration. In J. A. Bowden and P. Green (eds), _Doing Developmental Phenomenography_ (pp. 103-127). (Qualitative Research Methods). Melbourne: RMIT University Press.
*   <a id="als00"></a>Alsop, A. (2000). _Continuing Professional Development: a guide for therapists_. Oxford: Blackwell.
*   <a id="cho99"></a>An Chomhairle Leabharlanna/The Library Council (1999). _Joining Forces: Delivering Libraries & Information Services in the Information Age._ Dublin: The Library Council.
*   <a id="cru02"></a>Crumley, E. and Koufogiannakis, D. (2002). Developing evidence-based librarianship: practical steps for implementation. _Health Information and Libraries Journal_, **19** (2), 61-70
*   <a id="mar97"></a>Marton, F. and Booth, S. (1997). _Learning and Awareness_. New York, N. J.: Lawrence Erlbaum Associates.