#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Poster abstract

# Visualization as a research and design approach for library and information science. Exploring seniors' use of a visual search interface

#### [Rhiannon Gainor*](mailto:rgainor@ualberta.ca), 
#### [Lisa Given*](mailto:lisa.given@ualberta.ca), 
#### [Stan Ruecker*](mailto:sruecker@ualberta.ca), 
#### [Andrea Ruskin**](mailto:aruskin@shaw.ca), 
#### [Elisabeth (Bess) Sadler***](mailto:bess.sadler@gmail.com) 
#### [Heather Simpson*](hsimpson@ualberta.ca)  

*University of Alberta, Canada; 
**Mount Royal College, Canada; 
***University of Virginia, USA

## Research problem

This study was designed to investigate how visualization theory could inform interface design and research practices in LIS. The project’s goals were: 1) to design a prototype interface using visual similarity clustering principles (from Humanities Computing and Visual Design) and information behaviour and usability theories (from LIS); 2) to compare the usefulness of this prototype to existing text-based search tools; 3) to design and test an interface that would address the information and searching needs of a particular population (seniors), in a particular information context (health information). The resulting prototype – a drug information database designed with a visually-based search interface – demonstrates the benefits of crossing disciplinary boundaries to build interfaces that are theoretically sophisticated but pragmatically driven. Further, the project demonstrates the usefulness of qualitative research methods in system design and testing.

## Project background

The proper identification and use of medication by patients is an ongoing concern in the health information community (e.g., [Alemagno _et al._ 2004](#ale04); [Gleckman 2003](#gle03)). Seniors are particularly vulnerable to difficulties in this area, since many of them take multiple medications, each with their own requirements and precautions. The literature also points to a need for research on interface-browsing strategies designed for seniors accessing health-related information ([Ruecker and Chow 2003](#rue03)). Further, Chadwick-Dias _et al._ ([2003](#cha03)) found that while individuals over 55 experience more difficulties in using the web than younger people, specific design changes based on older users’ needs can improve performance measures for all users. This project, then, was designed to fill a gap in the existing research, and in system design, by creating and testing a visual search interface that was aesthetically pleasing, easy to navigate, and suitable for a variety of health-related information needs. By providing new knowledge on the web searching behaviours of seniors, combined with the exploration of a new interface for the identification of medicinal drugs, this project’s results can inform the future development of usable web interfaces.

## Method

The study involved the design and testing of a prototype interface that offered an interactive way to sort and identify pills by appearance (e.g., size; colour). As the study was designed to examine system use in the context of seniors’ health-related information behaviours (including their web-based searching practices), qualitative interviews that explored the seniors’ informational activities and preferences were also conducted. The prototype was designed based on the published results of previous visualization and usability studies, and on the literature addressing seniors’ computer design needs and information behaviour practices. Using a verbal protocol analysis (or “think aloud”) approach, seniors’ use of the prototype was also compared with a publicly-accessible online pill identification database.

Twelve men and women, aged 65 to 80 and from varied backgrounds, participated in the study. All had some basic computer experience (e.g., used email). Participants were interviewed about their health-related information needs and online searching activities, and then asked to retrieve information from two drug interfaces: a publicly-available existing site (www.drugs.com); and, a new prototype interface, created for this project. The sessions were audio- and video-recorded, to capture seniors’ on-screen activities and opinions.

Each participant was given three colour pill images printed on 3x5 cards and asked to locate drug information in both databases. As they searched, participants were encouraged to talk about their reactions to the interface tools and discuss how well each system met their needs (e.g., navigation, information format and content). The researchers used “task completion checklists” to track which features the interviewees used. Participants were first asked to search for each pill using any search process and/or feature that they felt were appropriate. Once they had done so, the researchers prompted each senior to use any features they had not yet tried. Transcripts were made of each session and common themes were analysed using a grounded theory approach.

## Results of the study

Participants had varied responses to their interactions with the same set of searching tools. Overall, they found the prototype to be a more useful interface for searching for pill information. However, key findings also include participants’ problems with colour and shape recognition, difficulties recognizing specific search tools (e.g., zoom), and the expressed need to be able to enter text for some elements of the search process. Observations from the video data include the emotional responses of participants (e.g., frustration and anxiety while searching), innovative research strategies to address misunderstandings of search tools during the session, and participants’ desire to play with the prototype interface. This poster will present observational results analysed from the video-recordings of each session, with a focus on the usefulness of qualitative “think aloud” methods for systems design and research.

## Relevance to the conference theme

This poster addresses at least two of the conference themes. By exploring the potential for visualization theory, the project addresses one approach for “Reframing LIS from Different Perspectives.” Also, by using qualitative methods to explore seniors’ use of a prototype and existing online database, the project presents a methodological approach that is rarely used in systems-design work, particularly with seniors. This addresses the theme of “New LIS Research Methods,” as the project’s results point to great potential for future research in this area and with this population of users.

## References

*   <a id="ale04"></a>Alemagno, S. A., Niles, S. A., & Treiber, E. A. (2004). Using computers to reduce medication misuse of community-based seniors: results of a pilot intervention program. _Geriatric Nursing_, **25**(5), 281-5.
*   <a id="cha03"></a>Chadwick-Dias, A., McNulty, M. & Tullis, T. (2003). Web Usability and Age: How Design Changes Can Improve Performance. ACM CUU’03 (pp. 30-7), November 10-11, 2003, Vancouver, British Columbia, Canada.
*   <a id="gle03"></a>Gleckman, H. (2003). Seniors’ big drug problem misusing medications is a leading cause of death among the elderly. _Business Week New York_, December 22, 90-1.
*   <a id="rue03"></a>Ruecker, S., & Chow, R. (2003). The significance of prospect in interfaces to health-related web sites for the elderly. In Proceedings from Include 2003, London, England, 2003\. (pp. 273-77). London: Helen Hamlyn Research Institute, Royal College of Art.