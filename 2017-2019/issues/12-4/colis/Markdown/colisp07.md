#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science?"Featuring the Future". Poster abstract

# The information cycle in the European Commission's policy-making process

#### [Evangelia Koundouraki](mailto:E.Koundouraki@mmu.ac.uk)  
Manchester Metropolitan University, Department of Information and Communications, Manchester, M15 6LL, UK

## Introduction

Policy-making goes beyond making formal decisions or proposals. It entails the gathering and exchange of information, the elaboration of alternatives and their expected consequences, and experiential learning ([March and Olsen, 1976, p. 144](#mar76), [Simon, 1976](#sim76), [Egeberg, 1999](#ege99)). For an organisation like the European Commission (hereafter 'the Commission'), the role of information is vital to the efficacious performance of its key functions and roles in the EU and international arena. Feldman and March ([1981](#fel81)) argued that, ?_using information, asking for information, and justifying decisions in terms of information have all come to be significant ways in which we symbolize that the process is legitimate, that we are good decision makers, and that our organizations are well managed_? (p. 178). Although there is a common consent that, information is one of the key drivers in the policy process of the Commission ([European Commission, 2001](#eur01)), as yet there is little research on the role of the Commission as an information gatherer, information user and information provider in the EU policy-making process.

This is the background against which this study aims to accomplish the following objectives: the identification and categorization of the different information types and activities in the Commission?s policy-making processes; the identification and analysis of the information needs of the Commission?s policy makers as policy-making processes unfold, and the development of a conceptual information processing framework for that part of the EU policy-making process involving the Commission.

## Method

Because of the exploratory nature of the study and the objective of identifying the different information activities and developing an analytical framework, an inductive process of theory building from case study research is adopted.[i] The process of inducting theory using case studies synthesizes elements of Glaser and Strauss? ([1967](#gla1967)) grounded theory, the design of the case study research ([Yin, 1989](#yin89)), and work on qualitative methods ([Miles and Huberman, 1994](#mil94),[Eisenhardt, 1989](#eis89) ).

The main arguments for choosing case studies as the research strategy were the descriptive nature of the research (not focusing on information seeking behaviours but rather documenting information flows and processes) and the dominance of ?how? (and exploratory ?what?) questions. Given the weaknesses of the single case study ([Yin, 1989](#yin89)), this research adopted an embedded multi-case study design. In particular, it examines three policy cases: a) a regulatory case, the Safety of Toys directive; b) an open method of coordination (OMC) case, Innovation policy, and c) a non-regulatory case, the Corporate Social Responsibility initiative. The selection criteria are based on theoretical sampling ([Glaser and Strauss, 1967](#gla67)). The empirical part of the study is informed by twenty semi-structured interviews with Commission officials. It also analyses published and unpublished primary documentation and draws from participant observation during the researcher?s internship in the Commission (DG Enterprise and Industry, March-July 2006).[iii]

## Analysis: Preliminary Conceptual Framework

Data analysis applies the coding technique in order to categorize the primary data. The coding process was inductive and used the following steps: initial reading of the data; identification of specific segments; labelling of data to create categories; reduction of overlapping categories, and the creation of a model encompassing the most important categories ([Creswell, 2002](#cres02)). According to Miles and Huberman ([1994](#mil94)), conceptual frameworks are the current version of the researcher?s map of the territory being investigated. They also stated that conceptual frameworks are best done graphically so as to enable the researcher to make explicit what is already in his/her mind. The preliminary framework of this study applies the stages of the policy cycle and the information cycle to describe information flows within the Commission. In addition, it shows that there is an intra- and extra-Commission context.

The policy cycle includes the following stages: agenda setting; policy formulation, policy defending and project development; decision-making; policy implementation and policy evaluation ([adapted from Cini, 1996](#cin96)). On the other hand, the information cycle initiates by determining information requirements, capturing, distributing and (re-)using information ([adapted from Davenport, 1997](#dav97)).

The mechanisms and processes applied by the Commission to gather and use information for policy-making may range from the Internet to group meetings and direct contacts, information systems, special reports and determined by the annual planning and formal internal rules and procedures. Within the Commission, information flow is top-down and bottom-up in two levels: intra-Directorates-General/cabinets level and inter-Directorates-General/cabinets level. Outside the Commission boundaries information may originate from other EU institutions (for instance, the European Council, the European Parliament etc) or from the civil society at large, which may be expert advice, national, regional and local governments, target groups, key networks and individuals.

<figure>

![Conceptual framework of the Europen Commision's information and policy cycles](../colisp07fig1.jpg)

<figcaption>

**Figure 1 Conceptual framework of the Europen Commission's information and policy cycles**</figcaption>

</figure>

## Results

The most significant results from the analysis of the empirical data are that information flows reflect the organizational structure and the procedures of the Commission. Even in the cases where the policy priorities of the involved DGs are opposing or conflicting and information flows are disrupted in day-to-day activities, there are formal procedures that smooth the progress of information exchange and decision-making. On the other hand, the decentralization of the Commission?s information systems impedes the follow-up of policies within the various Commission services involved. In respect of the information needs of the Commission?s policy makers there are two main findings: a) information needs vary according to the level of policy-makers in the hierarchy, b) the Commission does not experience any information accessibility problems. If the information is available they have access to it; the Internet plays a vital role in accessing and using information.

## Conclusions

The characterization of the Commission as a 'complex multi-organization' ([Cram, 1994)](#cra94) is also supported from the information flow analysis. Policy- and decision-making are the outcomes of continuous information gathering, processing and (re-)use. A better understanding of the way information is accumulated, organized and used plays a vital role in the whole process of policy- and decision-making and enhances the openness and transparency of the EU polity.  

* * *

## Footnotes

[i] [Torraco](#tor97) (1997, p. 115) noted that theory ?simply explains what a phenomenon is and how it works?.  
[ii] The main procedures of OMC are: common guidelines to be translated into national policy, combined with periodic monitoring, evaluation and peer review organized as mutual learning processes and accompanied by indicators and benchmarks as means of comparing best practice ([European Council, 2000, p. 12](#cou00)).  
[iii] The researcher experienced a five-month in-service training at the Commission that enabled her to gain a better understanding of the way the Commission operates.  

## References

*   <a id="cin96"></a>Cini, M, 1996._The European Commission: Leadership, organization and culture in the EU_, Manchester; New York: Manchester University Press.
*   <a id="cra94"></a>Cram, L., 1994\. The European Commission as a multi-organization: social policy and IT policy in the EU. _Journal of European Public Policy_, **1**(2), pp. 195-217
*   <a id="cres02"></a>Creswell, J., 2002\. _Educational research: Planning, conducting, and evaluating quantitative and qualitative research_. Upper Saddle River, NJ: Merrill Prentice Hall.
*   <a id="dav97"></a>Davenport,T., 1997\. _Information Ecology: Mastering the Information and Knowledge Environment_, New York; Oxford: Oxford University Press.
*   <a id="ege99"></a>Egeberg, M., 1999\. The impact of bureaucratic structure on policy making. _Public Administration_, **77**, pp. 155-170.
*   <a id="eis89"></a>Eisenhardt, K., 1989\. Building Theories from Case Study Research. _Academy of Management Journal_, **14**(4), pp. 532-550
*   <a id="eur01"></a>European Commission, 2001\. _Reform White Paper_, SEC(2001) 924.
*   <a id="cou0"></a>European Council, 2000\. _Presidency Conclusions_. Lisbon: European Council, 23 and 24 March 2000
*   <a id="fel81"></a>Feldman, M. S. & March, J. G., 1981\. Information in Organizations as Signal and Symbol. _Administrative Science Quarterly_, **26**, pp. 171-186
*   <a id="gla67">Glaser, G. B. and Strauss, A., 1967\. _The discovery of Grounded Theory: Strategies of Qualitative Research_. London: Weidenfeld and Nicholson.</a>
*   <a id="mar76"></a>March, J. G. & Olsen, J. P., 1976 _Ambiguity and choice in organizations_, Bergen: Scandinavian University Press.
*   <a id="mil94"></a>Miles, M. B. and Huberman, M. A., 1994\. _Qualitative data analysis: an expanded sourcebook_. Beverly Hills; London: Sage.
*   <a id="sim76"></a>Simon, H. A., 1976\. _Administrative behavior_, New York: Free Press.
*   <a id="tor97"></a>Torraco, R. J., 1997\. Theory-building research methods, _In_ Swanson, R. A. and Holton, E. F. (Eds), _Human resource development handbook: Linking research and practice_, San Francisco: Berrett-Koehler, pp. 114?137.
*   <a id="yin89"></a>Yin, R. K., 1989\. _Case study research: Design and methods_. Rev. ed., Beverly Hills, CA: Sage Publishing.