<!DOCTYPE html>
<html lang="en">

<head>
	<title>The Information Cycle in the European Commission's Policy-Making Process</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta content="The Information Cycle in the European Commission's Policy-Making Process" name="dc.title">
	<meta content="Evangelia Koundouraki" name="dc.creator">
	<meta content="CoLIS6 poster abstract" name="dc.subject">
	<meta
		content="Introduction. The European Commission is one of the most important players in the EU policy-making process. There are numerous studies on the Commission's roles, structure and policies, however little has been written on its role as an information gatherer and processor. This PhD research project aims to develop a conceptual information processing framework for that part of the EU policy-making process involving the Commission. Method. An inductive process of theory building from case studies is adopted. The study examines three case studies: a regulatory, an Open Method of Coordination, and a non-regulatory case study. The main bulk of data was collected through elite interviewing, primary documentation analysis and participant observation. Analysis. The emerging data from the data collection have been analysed and further processed using the coding technique within- and cross-case in order a conceptual framework to be developed. Results. The most prominent results from the analysis of the empirical data are that information flows reflect the organizational structure and the procedures of the Commission. Information used for policy-making may be formal/informal, internal/external, written/oral, electronic/paper-based. Formal procedures anticipate possible conflicts or opposing policy priorities and facilitate information flows and decision-making. Information needs vary according to the seniority of policy-makers in the hierarchy. In all cases, the Commission does not experience any information accessibility problems. Conclusions The characterization of the Commission as a 'complex multi-organization' is also supported from the information flow analysis. Information and its management play a significant role in understanding policy- and decision-making in the EU."
		name="dc.description">
	<meta
		content="[Information Flow; Information Cycle; Policy Cycle; Policy-Making; European Commission (http://informationr.net/ir/Index_terms.html) where possible]"
		name="dc.subject.keywords">
	<meta content="all" name="robots">
	<meta content="Evangelia Koundouraki" name="dc.publisher">
	<meta content="global" name="dc.coverage.placename">
	<meta content="text" name="dc.type">
	<meta scheme="ISSN" content="1368-1613" name="dc.identifier">
	<meta scheme="URI" content="http://InformationR.net/ir/12-4/colis/colisp07.html" name="dc.identifier">
	<meta content="http://InformationR.net/ir/colis/colis.html" name="dc.relation.IsPartOf">
	<meta content="text/html" name="dc.format">
	<meta content="en" name="dc.language">
	<meta content="http://creativecommons.org/licenses/by-nd-nc/1.0/" name="dc.rights">
	<meta content="2007-10-15" name="dc.date.available">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="vol-12-no-4-october-2007">Vol. 12 No. 4, October, 2007</h4>
	<h2>Proceedings of the Sixth International Conference on Conceptions of Library and Information
		Science? &quot;Featuring the Future&quot;. Poster abstract</h2>
	<h1>The information cycle in the European Commission's policy-making process</h1>
	<h4 id="evangelia-koundouraki"><a href="mailto:e.koundouraki@mmu.ac.uk">Evangelia Koundouraki</a></h4>
	<h4>Manchester Metropolitan University,<br>
		Department of Information and Communications,<br>
		Manchester, M15 6LL,<br>
		UK</h4>
	<h2 id="introduction">Introduction</h2>
	<p>Policy-making goes beyond making formal decisions or proposals. It entails the gathering and exchange of
		information, the elaboration of alternatives and their expected consequences, and experiential learning (<a
			href="#mar76">March and Olsen, 1976, p. 144</a>, <a href="#sim76">Simon, 1976</a>, <a href="#ege99">Egeberg,
			1999</a>). For an organisation like the European Commission (hereafter 'the Commission'), the role of
		information is vital to the efficacious performance of its key functions and roles in the EU and international
		arena. Feldman and March (<a href="#fel81">1981</a>) argued that, ?<em>using information, asking for
			information, and justifying decisions in terms of information have all come to be significant ways in which
			we symbolize that the process is legitimate, that we are good decision makers, and that our organizations
			are well managed</em>? (p. 178). Although there is a common consent that, information is one of the key
		drivers in the policy process of the Commission (<a href="#eur01">European Commission, 2001</a>), as yet there
		is little research on the role of the Commission as an information gatherer, information user and information
		provider in the EU policy-making process.</p>
	<p>This is the background against which this study aims to accomplish the following objectives: the identification
		and categorization of the different information types and activities in the Commission?s policy-making
		processes; the identification and analysis of the information needs of the Commission?s policy makers as
		policy-making processes unfold, and the development of a conceptual information processing framework for that
		part of the EU policy-making process involving the Commission.</p>
	<h2 id="method">Method</h2>
	<p>Because of the exploratory nature of the study and the objective of identifying the different information
		activities and developing an analytical framework, an inductive process of theory building from case study
		research is adopted.[i] The process of inducting theory using case studies synthesizes elements of Glaser and
		Strauss? (<a href="#gla1967">1967</a>) grounded theory, the design of the case study research (<a
			href="#yin89">Yin, 1989</a>), and work on qualitative methods (<a href="#mil94">Miles and Huberman,
			1994</a>,<a href="#eis89">Eisenhardt, 1989</a> ).</p>
	<p>The main arguments for choosing case studies as the research strategy were the descriptive nature of the research
		(not focusing on information seeking behaviours but rather documenting information flows and processes) and the
		dominance of ?how? (and exploratory ?what?) questions. Given the weaknesses of the single case study (<a
			href="#yin89">Yin, 1989</a>), this research adopted an embedded multi-case study design. In particular, it
		examines three policy cases: a) a regulatory case, the Safety of Toys directive; b) an open method of
		coordination (OMC) case, Innovation policy, and c) a non-regulatory case, the Corporate Social Responsibility
		initiative. The selection criteria are based on theoretical sampling (<a href="#gla67">Glaser and Strauss,
			1967</a>). The empirical part of the study is informed by twenty semi-structured interviews with Commission
		officials. It also analyses published and unpublished primary documentation and draws from participant
		observation during the researcher?s internship in the Commission (DG Enterprise and Industry, March-July
		2006).[iii]</p>
	<h2 id="analysis-preliminary-conceptual-framework">Analysis: Preliminary Conceptual Framework</h2>
	<p>Data analysis applies the coding technique in order to categorize the primary data. The coding process was
		inductive and used the following steps: initial reading of the data; identification of specific segments;
		labelling of data to create categories; reduction of overlapping categories, and the creation of a model
		encompassing the most important categories (<a href="#cres02">Creswell, 2002</a>). According to Miles and
		Huberman (<a href="#mil94">1994</a>), conceptual frameworks are the current version of the researcher?s map of
		the territory being investigated. They also stated that conceptual frameworks are best done graphically so as to
		enable the researcher to make explicit what is already in his/her mind. The preliminary framework of this study
		applies the stages of the policy cycle and the information cycle to describe information flows within the
		Commission. In addition, it shows that there is an intra- and extra-Commission context.</p>
	<p>The policy cycle includes the following stages: agenda setting; policy formulation, policy defending and project
		development; decision-making; policy implementation and policy evaluation (<a href="#cin96">adapted from Cini,
			1996</a>). On the other hand, the information cycle initiates by determining information requirements,
		capturing, distributing and (re-)using information (<a href="#dav97">adapted from Davenport, 1997</a>).</p>
	<p>The mechanisms and processes applied by the Commission to gather and use information for policy-making may range
		from the Internet to group meetings and direct contacts, information systems, special reports and determined by
		the annual planning and formal internal rules and procedures. Within the Commission, information flow is
		top-down and bottom-up in two levels: intra-Directorates-General/cabinets level and
		inter-Directorates-General/cabinets level. Outside the Commission boundaries information may originate from
		other EU institutions (for instance, the European Council, the European Parliament etc) or from the civil
		society at large, which may be expert advice, national, regional and local governments, target groups, key
		networks and individuals.</p>
	<figure>
		<img src="colisp07fig1.jpg"
				alt="Conceptual framework of the Europen Commision's information and policy cycles">
		<figcaption>
			<strong>Figure 1 Conceptual framework of the Europen Commission's information and policy cycles</strong>
		</figcaption>
	</figure>
	<h2 id="results">Results</h2>
	<p>The most significant results from the analysis of the empirical data are that information flows reflect the
		organizational structure and the procedures of the Commission. Even in the cases where the policy priorities of
		the involved DGs are opposing or conflicting and information flows are disrupted in day-to-day activities, there
		are formal procedures that smooth the progress of information exchange and decision-making. On the other hand,
		the decentralization of the Commission?s information systems impedes the follow-up of policies within the
		various Commission services involved. In respect of the information needs of the Commission?s policy makers
		there are two main findings: a) information needs vary according to the level of policy-makers in the hierarchy,
		b) the Commission does not experience any information accessibility problems. If the information is available
		they have access to it; the Internet plays a vital role in accessing and using information.</p>
	<h2 id="conclusions">Conclusions</h2>
	<p>The characterization of the Commission as a 'complex multi-organization' (<a href="#cra94">Cram, 1994)</a> is
		also supported from the information flow analysis. Policy- and decision-making are the outcomes of continuous
		information gathering, processing and (re-)use. A better understanding of the way information is accumulated,
		organized and used plays a vital role in the whole process of policy- and decision-making and enhances the
		openness and transparency of the EU polity.</p>
	<hr>
	<h2 id="footnotes">Footnotes</h2>
	<p>[i] <a href="#tor97">Torraco</a> (1997, p. 115) noted that theory ?simply explains what a phenomenon is and how
		it works?.<br>
		[ii] The main procedures of OMC are: common guidelines to be translated into national policy, combined with
		periodic monitoring, evaluation and peer review organized as mutual learning processes and accompanied by
		indicators and benchmarks as means of comparing best practice (<a href="#cou00">European Council, 2000, p.
			12</a>).<br>
		[iii] The researcher experienced a five-month in-service training at the Commission that enabled her to gain a
		better understanding of the way the Commission operates.</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="cin96"></a>Cini, M, 1996.<em>The European Commission: Leadership, organization and culture in the
				EU</em>, Manchester; New York: Manchester University Press.</li>
		<li><a id="cra94"></a>Cram, L., 1994. The European Commission as a multi-organization: social policy and IT
			policy in the EU. <em>Journal of European Public Policy</em>, <strong>1</strong>(2), pp. 195-217</li>
		<li><a id="cres02"></a>Creswell, J., 2002. <em>Educational research: Planning, conducting, and evaluating
				quantitative and qualitative research</em>. Upper Saddle River, NJ: Merrill Prentice Hall.</li>
		<li><a id="dav97"></a>Davenport,T., 1997. <em>Information Ecology: Mastering the Information and Knowledge
				Environment</em>, New York; Oxford: Oxford University Press.</li>
		<li><a id="ege99"></a>Egeberg, M., 1999. The impact of bureaucratic structure on policy making. <em>Public
				Administration</em>, <strong>77</strong>, pp. 155-170.</li>
		<li><a id="eis89"></a>Eisenhardt, K., 1989. Building Theories from Case Study Research. <em>Academy of
				Management Journal</em>, <strong>14</strong>(4), pp. 532-550</li>
		<li><a id="eur01"></a>European Commission, 2001. <em>Reform White Paper</em>, SEC(2001) 924.</li>
		<li><a id="cou0"></a>European Council, 2000. <em>Presidency Conclusions</em>. Lisbon: European Council, 23 and
			24 March 2000</li>
		<li><a id="fel81"></a>Feldman, M. S. &amp; March, J. G., 1981. Information in Organizations as Signal and
			Symbol. <em>Administrative Science Quarterly</em>, <strong>26</strong>, pp. 171-186</li>
		<li><a id="gla67">Glaser, G. B. and Strauss, A., 1967. <em>The discovery of Grounded Theory: Strategies of
					Qualitative Research</em>. London: Weidenfeld and Nicholson.</a></li>
		<li><a id="mar76"></a>March, J. G. &amp; Olsen, J. P., 1976 <em>Ambiguity and choice in organizations</em>,
			Bergen: Scandinavian University Press.</li>
		<li><a id="mil94"></a>Miles, M. B. and Huberman, M. A., 1994. <em>Qualitative data analysis: an expanded
				sourcebook</em>. Beverly Hills; London: Sage.</li>
		<li><a id="sim76"></a>Simon, H. A., 1976. <em>Administrative behavior</em>, New York: Free Press.</li>
		<li><a id="tor97"></a>Torraco, R. J., 1997. Theory-building research methods, <em>In</em> Swanson, R. A. and
			Holton, E. F. (Eds), <em>Human resource development handbook: Linking research and practice</em>, San
			Francisco: Berrett-Koehler, pp. 114?137.</li>
		<li><a id="yin89"></a>Yin, R. K., 1989. <em>Case study research: Design and methods</em>. Rev. ed., Beverly
			Hills, CA: Sage Publishing.</li>
	</ul>

</body>

</html>