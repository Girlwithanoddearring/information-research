<!DOCTYPE html>
<html lang="en">

<head>
	<title>The smart ones: one-person librarians in Ireland and continuing professional development</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta content="The smart ones" name="dc.title">
	<meta content="Eva Hornung" name="dc.creator">
	<meta
		content="Phenomenographic study of one-person librarians in Ireland and their conceptions of continuing professional development"
		name="dc.subject">
	<meta
		content="This study aims to examine all possibilities librarians in One-Person Libraries (OPLs) in Ireland have with regards to continuing professional development (continuing professional development), their conceptions of continuing professional development, their self-proclaimed needs and future demands, level of participation as well as areas for improvement in provision. The overall goals of the project are to determine the current level of participation in continuing professional development by OPLs in Ireland; to establish what impact continuing professional development makes on their lives, their work practices, their organisation; to record their perceived needs for and potential barriers for participation in continuing professional development and to suggest what changes have to be made to meet these expectations. The emphasis is on the qualitatively different ways in which OPLs conceptualise and experience these phenomena."
		name="dc.description">
	<meta content="phenomenography, continuing professional development, Ireland, library education"
		name="dc.subject.keywords">
	<meta content="all" name="robots">
	<meta content="Professor T.D. Wilson" name="dc.publisher">
	<meta content="global" name="dc.coverage.placename">
	<meta content="text" name="dc.type">
	<meta scheme="ISSN" content="1368-1613" name="dc.identifier">
	<meta scheme="URI" content="http://InformationR.net/ir/12-4/colis/colisp06.html" name="dc.identifier">
	<meta content="http://InformationR.net/ir/12-4/colis/colis.html" name="dc.relation.IsPartOf">
	<meta content="text/html" name="dc.format">
	<meta content="en" name="dc.language">
	<meta content="http://creativecommons.org/licenses/by-nd-nc/1.0/" name="dc.rights">
	<meta content="2007-10-15" name="dc.date.available">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="vol-12-no-4-october-2007">Vol. 12 No. 4, October, 2007</h4>
	<h2>Proceedings of the Sixth International Conference on Conceptions of Library and Information
		Science?&quot;Featuring the Future&quot;. Poster abstract</h2>
	<h1>The smart ones: one-person librarians in Ireland and continuing professional development.</h1>
	<h4 id="eva-hornung"><a href="mailto:e.hornung@sheffield.ac.uk">Eva Hornung</a></h4>
	<h4>Department of Information Studies,<br>
		University of Sheffield,<br>
		Sheffield S1 4DP,<br>
		UK</p>
	<h2 id="introduction">Introduction</h2>
	<p>Little is known about one-person librarians in Ireland, let alone their continuing professional development
		(continuing professional development) needs. Even their overall number has never been established. This
		phenomenographic study examines conceptions of Irish one-person librarians of continuing professional
		development, their needs and future demands, level of participation as well as areas for improvement in
		provision. The position of one-person librarians within an organisation is unique in that that they cannot
		participate in professional development the same way librarians employed at a larger library can. Time, lack of
		money and lack of support can be barriers. Yet the importance of continuing professional development is evident.
		As An Chomhairle Leabharlanna/The Library Council stated in its key report Joining Forces (<a
			href="#cho99">1999</a>: 175): 'Professional education should be relevant to the practising needs of library
		and information service staff and should reflect their diverse needs in a consistently changing environment.The
		lack of provision for continuing professional development is a major barrier to the development of libraries and
		information services.'</p>
	<h2 id="method">Method</h2>
	<p>The importance of meaning is at the centre of this research project. The purpose of it is to investigate
		one-person librarians' opinions on continuing professional development as stated in their own words and
		reflecting their own experience of phenomena, thus engaging in an empirical approach. Phenomenography takes this
		'second-order perspective' investigating underlying ways of experiencing the world, phenomena and situations (<a
			href="#mar97">Marton and Booth 1997</a>).</p>
	<p>The project is also informed by the tradition of evidence-based librarianship, which has been described as '... a
		means to improve the profession of librarianship by asking questions as well as finding, critically appraising
		and incorporating research evidence from library science (and other disciplines) into daily practice. It also
		involves encouraging librarians to conduct high quality qualitative and quantitative research.' (<a
			href="#cru02">Crumley and Koufogiannakis 2002</a>: 62).</p>
	<h3 id="literature-review-of-relevant-studies-and-documents">Literature review of relevant studies and documents
	</h3>
	<p>The following fields were examined:</p>
	<ul>
		<li>Adult education theories, Lifelong Learning and continuing professional development</li>
		<li>continuing professional development for librarians in general in other countries</li>
		<li>One-person librarians and continuing professional development in other countries</li>
		<li>Lifelong Learning in Ireland, which includes workplace learning</li>
		<li>continuing professional development programmes for other professions in Ireland</li>
		<li>Phenomenographic research in librarianship and information science</li>
	</ul>
	<h3 id="in-depth-face-to-face-semi-structured-interviews-with-librarians">In-depth face-to-face semi-structured
		interviews with librarians</h3>
	<p>These will be pilot tested in order to exclude any ambiguities of wording, to gain confidence as an interviewer
		and to ensure that the recording equipment is in working order. The pilot interviews will not form part of the
		final set of interviews to be analysed.</p>
	<p><em>Purposive sampling</em> will be used, which is widely employed in phenomenography. Potential bias in
		selection is acknowledged, but as the emphasis is on maximising the variation in ways of experiencing a
		phenomenon, a broad variation of characteristics of the participants is needed:</p>
	<ul>
		<li>both sexes</li>
		<li>rural and urban locations</li>
		<li>different work settings (e.g., public, academic, special libraries)</li>
		<li>work experience in one-person librarians.</li>
	</ul>
	<h3 id="interviews-with-one-person-librarians-and-continuing-professional-development-experts">Interviews with
		one-person librarians and continuing professional development experts</h3>
	<p>Additionally, sets of interviews with librarianship and information science course providers in Ireland and some
		experts from outside Ireland will be conducted in order to understand their experiences and make recommendations
		for improvement.</p>
	<h2 id="analysis">Analysis</h2>
	<p>Usually, more than one researcher analyses the interviews, with others playing 'devil's advocate' when checking
		on interpretive rigour, but there have been studies were a lone researcher successfully analysed a
		phenomenography study, particularly in PhD projects (e.g. <a href="#ake05">?kerlind 2005</a>, whose doctoral
		supervisors fulfilled that role).</p>
	<h2 id="results">Results</h2>
	<p>So far, an extended literature review has been conducted. It focused on other studies on continuing professional
		development for librarians in general and some key texts on continuing professional development for one-person
		librarians in other countries.</p>
	<h2 id="conclusions">Conclusions</h2>
	<p>Many other professions have already acknowledged the need for continuing professional development and have
		developed models of the continuing professional development process, e.g., nurses and therapists.</p>
	<figure>
		<img src="colisp06fig1.jpg" alt="Figure 1: The Continuing Professional Development cycle">
		<figcaption>
			<strong>Figure 1: The continuing professional development cycle (taken from <a href="#als00">Alsop
						2000</a>: 7)</strong>.
		</figcaption>
	</figure>
	<p>It is hoped that one outcome of this study would be recommendations for a national framework for continuing
		professional development for one-person librarians.</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="ake05"></a>Åkerlind, G. (2005). Phenomenographic methods: A case illustration. In J. A. Bowden and P.
			Green (eds), <em>Doing Developmental Phenomenography</em> (pp. 103-127). (Qualitative Research Methods).
			Melbourne: RMIT University Press.</li>
		<li><a id="als00"></a>Alsop, A. (2000). <em>Continuing Professional Development: a guide for therapists</em>.
			Oxford: Blackwell.</li>
		<li><a id="cho99"></a>An Chomhairle Leabharlanna/The Library Council (1999). <em>Joining Forces: Delivering
				Libraries &amp; Information Services in the Information Age.</em> Dublin: The Library Council.</li>
		<li><a id="cru02"></a>Crumley, E. and Koufogiannakis, D. (2002). Developing evidence-based librarianship:
			practical steps for implementation. <em>Health Information and Libraries Journal</em>, <strong>19</strong>
			(2), 61-70</li>
		<li><a id="mar97"></a>Marton, F. and Booth, S. (1997). <em>Learning and Awareness</em>. New York, N. J.:
			Lawrence Erlbaum Associates.</li>
	</ul>

</body>

</html>