#### vol. 14 no. 2, June, 2009

* * *

# Personality traits and group-based information behaviour: an exploratory study

#### [Jette Hyldegård](#author)  
Research Programme of Information Interaction and Information Architecture, Royal School of Library and Information Science, 6 Birketinget DK-2300 Copenhagen S., Denmark

#### Abstract

> **Introduction.** The relationship between hypothesised behaviour resulting from a personality test and actual information behaviour resulting from a group-based assignment process is addressed in this paper.  
> **Methods**. Three voluntary groups of ten librarianship and information science students were followed during a project assignment. The long version of the commonly-used NEO-PI-R test instrument was employed to describe and compare each group member's personality traits at a more detailed level. Data were also collected through a process survey, a diary and an interview.  
> **Analysis**. The calculation of data from the personality test resulted in various T-scores on personality factors and facets for each group member. Data from the demographic survey and the process surveys were calculated in Excel, while data from diaries and interviews were coded in the analytical data software Atlas.ti.  
> **Results**. Information behaviour associated with personality traits was identified, but the presence of personality effects tended to vary with the perceived presence of the social context.  
> **Conclusions**. Some matches were identified between group members' personality traits and their actual information behaviour but there were also deviations, which were found that seemed to be related to the group-work context. The importance of studying personality traits in context has further been confirmed.

## Introduction

Although the importance of individual characteristics and psychological factors has been conceptualized in many models of information seeking behaviour (e.g., [Case 2007](#cas07); [Wilson 1999](#wil99)) research into personality issues has only recently attracted attention in information science. This may be explained by the work by Heinström ([2002](#hei02); [2003](#hei03); [2006a](#hei06a); [2006b](#hei06b); [2006c](#hei06c)), but may also be explained by the emerging interest and research into affective dimensions of information behaviour ([Nahl and Bilal 2007](#nah07)). Hitherto, the research focus in information science has primarily been on the cognitive dimension of information behaviour, e.g., the user’s state of mind during the information seeking process (e.g., [Belkin 1978](#bel78); [De Mey 1977](#dem77); [Ingwersen and Järvelin 2005](#ing05); [Pettigrew _et al._ 2001](#pet01)). In contrast, recent research into personality and information behaviour tends to integrate the personal and cognitive dimension, e.g., by focusing on how personality corresponds to individuals’ preferred learning style or affects their information strategies during knowledge construction (e.g., Heinström [2002](#hei02); [2003](#hei03); [2006a](#hei06a); [2006b](#hei06b); [2006c](#hei06c); [Ford 1986](#for86); [Palmer 1991](#pal91); [Tidwell and Sias 2007](#tid07)). According to Nahl and Bilal ([2007](#nah07)), however, situational factors also need to be taken into account as we adjust our behaviour according to specific contexts and social norms. In some studies, for example, the impact of personality has been compared to discipline differences and stage of the research process (e.g., [Heinström 2002](#hei02)). In others it has been demonstrated that social cost perception seems to mediate between the influence of personality and information seeking ([Tidwell and Sias 2007](#tid07)). In these studies it has been hypothesized, moreover, that personality differences will be most visible in contexts where the person is free to approach information seeking according to his/her preferences, while it will be less visible in situations with higher contextual demands. In the study presented here the focus is on the social context and how an academic group-work setting may influence and mediate between personality and information behaviour. The focus is on individuals in groups, that is, the ‘group member’. Many studies exist on information behaviour in groups or teams (e.g., [Case 2007](#cas07)), and a number of influencing variables have been identified, such as role, complexity of work task and social cost. Building upon this previous research it is hypothesised that the mere group setting in focus here will influence as well as mediate between individuals’ personality traits and their information behaviour.

Parallel with the increasing research interest in the personality dimension of information behaviour, the measurements of personality traits and the interpretation of test results have become of paramount importance. Some researchers question the fruitfulness of personality as an explaining or predictive factor of behaviour (e.g., [Davies 2005](#dav05)). In that context it is often argued that a personality test only is a kind of hypothesis concerning future behaviour ([Ryckman 1982](#ryc82)) and that the administration of the test ought to be followed by a debriefing or an interview testing the hypotheses ([Jackson 1996](#jac96)). One of the widespread personality approaches is the Five-Factor Model (Costa and Mccrae [1992](#cos92); [1997](#cos97)). It measures personality traits according to five core personality factors: neuroticism, extraversion, openness to experience, agreeableness and conscientiousness. Both a short and a long version of the Five-Factor model exist, but they result in very different levels of personality descriptions, hence different basis for data analysis and interpretation. Whereas the short version results in general descriptions, the long version allows for a more detailed description of individuals’ personality traits. The present study is based on the long version and a qualitative case study approach with few participants to explore in more detail the relation between personality traits and information behaviour in a social context.

## Personality and personality testing

When trying to conceptualise personality, no concensus exists among researchers about the nature of this fundamental concept. According to Phares ([1991](#pha91): 4) '_personality is that pattern of characteristic thoughts, feelings and behaviour that distinguishes one person from another and persists over time and situation_'. In contrast, Ryckman ([1982](#ryc82)) states that personality only denotes a tendency to behave and react in a specific way dependent on situational factors, hence personality traits may be more or less visible. Persons characterized by high emotional instability may, for example, be more likely to feel anxiety in a threatening evaluation situation than calm and stable persons. Therefore, personality should only be hypothetically understood. This is supported by Humpheys and Revelle ([1984](#hum84)), stating that personality traits are dispositions to behaviour rather than absolute and predetermined characteristics of human behaviour. Hence, when exploring personality traits and hypotheses about information behaviour, the result needs to be analysed in combination with the situation. This has further been confirmed in previous studies of personality and information seeking behaviour (e.g., [Heinström 2002](#hei02); [Palmer 1991](#pal91); [Tidwell and Sias 2007](#tid07)).

Many approaches to measure personality exist ([Pors 2009](#por09)). The NEO Personality Inventory Revised (NEO PI-R) is, however, one of the most widespread tests based on the Five-Factor model ([Skovdahl Hansen and Mortensen 2003](#sko03)). This is due to the solid empirical work done by Costa and McCrae (e.g., Costa and Mccrae [1992](#cos92); [1997](#cos97)) and the design of the test instrument itself. Besides allowing for both a general and detailed description of personality, the test is designed to take into consideration the characteristics of the specific test-person in focus. Specific norms for groups of people and profiles have been developed from research to help validate the test-result. For example, norms exist for age, for job and for students. In this way, test-persons are always compared to people from the same group when test data are analysed. Other personality tests exist that focus specifically on teams, e.g., the Belbin test. Besides the methodological explanations given above, however, the focus here is on groups in academic settings, not on teams in organisations. Further, a recent study ([Fisher _et al._ 2001](#fis01)) has demonstrated that the five factor-model on which the NEO-PI-R test is based also holds for the team-roles resulting from the [Belbin test](http://www.belbin.com). Finally, the five factor personality test has been used recently in a a study investigating the relation between students’ personality and information seeking behaviour ([Heinström 2002](#hei02)).

The NEO-PI-R ‘long version’ measures differences in cognitive, affective and social behaviour according to five personality factors: neuroticism, extraversion, openness to experience, agreeableness and conscientiousness. Each of these is a summarization of six facets. _Neuroticism_, for example, is a summarization of anxiety, temper, pessimism, social fear, impulsiveness and nervousness. Each of the five factors and their associated six facets is measured through forty-eight statements, meaning that all thirty facets are measured through 240 statements. Each statement is answered on a five point scale, ranging from _strongly disagree_ to _strongly agree_. The result of the 240 statements is then calculated into T-scores for every factor and facet ranging from _very low_ to _very high_, according to the underlying norm groups of the test instrument. In Table 1 the factors, facets and T-score values of the test instrument are presented.

<table><caption>

**Table 1: Factors, facets and T-score values of NEO-PI-R.**  
A T-score at 34 and below is considered as 'very low', a T-score between 35 and 44 is considered as 'low', a T-score between 45 and 55 is considered as 'middle' and averagely, a T-score between 56 and 65 is considered 'high' and, finally, a T-score at 66 and above is considered 'very high'.</caption>

<tbody>

<tr>

<th colspan="2" rowspan="2"></th>

<th colspan="5">T-scores</th>

</tr>

<tr>

<th>Very low</th>

<th>Low</th>

<th>Middle</th>

<th>High</th>

<th>Very high</th>

</tr>

<tr>

<th>Factors</th>

<th>Facets</th>

<th><34</th>

<th>35 - 44</th>

<th>45-55</th>

<th>56-65</th>

<th>66<</th>

</tr>

<tr>

<td>Neuroticism</td>

<td>Anxiety, Temper, Pessimism, Social fear, Impulsiveness, Nervousness</td>

<td>Emotional stability: calm, confident, stress resistent</td>

<td colspan="3">Calm,can handle stress , sometimes he/she feels guilt, anger and worry</td>

<td>Anxiety: tendency to worrying. Upset in stressing situations</td>

</tr>

<tr>

<td>Extraversion</td>

<td>Warmth, Gregariousness, Assertiveness, Activity, Excitement-Seeking, Positive Emotions</td>

<td>Introversion: introvert, reserved, serious. Prefers to work individually</td>

<td colspan="3">Moderate level of activity - both individually and in groups</td>

<td>Extraversion: extrovert, open, high level of activity, likes to collaborate, does not mind taking a leading role</td>

</tr>

<tr>

<td>Openness to experience</td>

<td>Fantasy, Aesthetics, Feelings, Actions, Ideas, Values</td>

<td>Concrete thinking: down to earth, practical, conventional, technical approach to problem solving</td>

<td colspan="3">Practical approach, but also open, a balance between old and new</td>

<td>Openness: open towards new experiences, creative, broad field of interests, conceptual approach to problem solving</td>

</tr>

<tr>

<td>Agreeableness</td>

<td>Trust, Straightforwardness, Altruism, Compliance, Modesty, Tendermindedness</td>

<td>Unfriendly: obstinate, sceptical, proud, impersonal, competitive</td>

<td colspan="3">Generally warm and friendly, sometimes competitive and obstinate</td>

<td>Friendly: empathy, kind, collaborative</td>

</tr>

<tr>

<td>Conscientiousness</td>

<td>Competence, Order, Dutifulness, Achievement-Striving, Self-Discipline, Deliberation</td>

<td>Unconcerned: spontaneous, laid back, does not like details, but flexible plans and procedures</td>

<td colspan="3">Trustworthy and relative well organised, clear goals, but may put aside things in specific situations</td>

<td>Conscientiousness: conscientious and trustworthy, efficient, concrete details, prefers routines and predictive procedures, strong feeling of responsibility</td>

</tr>

</tbody>

</table>

The 'short version' (NEO–FF), which most often is used in survey-oriented research, also measures the five factors but only through twelve statements, which relate to only two of the associated facets. Thus, in contrast to the long version the short version only measures individuals' personality through sixty statements in total. This difference between the long and the short version is important as it is possible to obtain exactly the same score on one of the five factors though the underlying profile of facets is very different. In the present case study the long version of the five-factor model was employed in order to describe each individual participant (group member) at a more detailed level, hence, strengthening the discussion of how the social context may mediate between individual characteristics and information behaviour. To further qualify the discussion the next section presents Heinström’s solid work on personality and individuals' information behaviour in an academic setting. This is followed by a summary of previous research and trends regarding information behaviour in groups in association with a project or an assignment.

## Heinström's studies of personality and information behaviour

Through three mainly large-scale quantitative studies Heinström ([2002](#hei02); [2003](#hei03); [2006a](#hei06a); [2006b](#hei06b); [2006c](#hei06c)) has investigated how personality traits influence information strategies and guide students' information behaviour and study approach. The first study involved 305 university students writing their master's theses at Åbo Akademi University in Finland. The second study involved 574 students, grades 6 to 12, from ten diverse public schools studying a broad range of curriculum topics. The third study concerned twenty-seven mature library and information science students at Rutgers University, USA. A majority of the participants had a background in education as teachers and were pursuing an additional degree at Rutgers to obtain qualifications as school librarians. Data were collected by means of survey instruments such as the short version of the Five-Factor model (NEO-ff) and questionnaires addressing information seeking behaviour. The results from these studies showed that information seeking behaviour could be connected to all the five personality factors tested. People with a low level of _neuroticism_ had a constructive and positive attitude towards information and appreciated a large recall – the more secure, the more actively they searched for information. In contrast, people with a high level of _neuroticism_ had difficulties in coping with unpredictability, disorder and ambiguity in search systems. _Extraverted_ students were active seekers, but more superficial in their use of information (lower marks). Students characterized as being open to experience preferred a broad range of information rather than few precise ones; they critically analysed information and were not afraid of new information content. Students with a low level of _agreeableness_ were characterized by impatience, perceptions of lack of time and time pressure, implying that they did not devote enough time to information seeking. Conscientious students were found to be willing to work hard and spend time and money to obtain relevant information. They were goal-oriented, responsible and determined to achieve academically. In contrast, students characterized as being more careless got easily distracted, were impulsive and hasty. The characteristics of students' information behaviour were also reflected in their learning approach (Heinström [2002](#hei02); [2003](#hei03); [2006a](#hei06a)). Recently, Heinström ([2006b](#hei06b); [2006c](#hei06c)) has found that level of motivation also corresponds to type of personality, hence, students' approaches to information behaviour and knowledge construction. The results of the three studies have resulted in three user types: fast surfers, broad scanners and deep divers.

In summary, Heinström's work has contributed to our knowledge of personal and psychological dimensions of students' information behaviour. It builds upon previous work from both psychology, cognitive science and information science. Due to the large scale quantitative approach and the employment of the short version of the Five-Factor model Heinström has been able to identify relations between personality and patterns of information seeking behaviour at a general level.

## Previous research on group-based information behaviour

Previous studies of collaborative information behaviour have demonstrated how social aspects influence information (seeking) behaviour in various ways, at both the individual level and the group level (e.g., [Allen 1977](#all77); [Bruce _et al._2003](#bru03); [Fidel _et al._2000](#fid00); Hansen and Järvelin [2000](#han00); [2005](#han05); Hertzum [2000](#her00); [2002](#her02); Hyldegård [2006b](#hyl06b); [2008](#hyl09); [O'Day and Jeffries 1993](#oda93); [Prekop 2002](#pre02); [Reddy and Jansen 2008](#red08a); [Reddy and Spence 2008](#red08b); [Talja 2002](#tal02)). Different information related roles have been found to exist in teams, such as the 'gatekeeper' who takes the responsibility to look for information and forward it to colleagues in his or her team ([Allen 1977](#all77)). These roles may also change with group members' workload and responsibilities within a project as well as with the progress and staffing of the project ([Prekop 2002](#pre02)). Information sharing in group situations has been investigated by other researchers, but with relevance to an academic setting Talja's work ([2002](#tal02)) should be mentioned. Building on the work by O'Day and Jeffries ([1993](#oda93)) Talja wanted to demonstrate that collective aspects of information behaviour should not be conceptualised as 'one-way' processes in which one individual consults another. In contrast, information acquisition and filtering should be defined as a collective and collaborative activity. Based on a large qualitative study exploring information sharing practice in relation to document retrieval in different academic communities Talja ([2002](#tal02)) identified and classified five types of information sharing: 'strategic sharing', 'paradigmatic sharing', 'directive sharing', 'social sharing' and 'non sharing'. Hansen and Järvelin ([2005](#ing05)) explored the expressions of collaborative activities within the process of the patent work task. It was found that collaborative activities of information seeking behaviour may take place throughout the work task process and may even be categorized according to the specific work task step in the process. Recently Reddy and Spence ([2008](#red08b)) have explored collaborative information seeking activities within a patient care team focusing on the urgent care component of an emergency department. Three triggers for collaborative information seeking activities were identified: 1) lack of expertise 2) lack of immediate accessible information and 3) complex information needs.

Previous collaborative information behaviour studies have often focused on engineers' or professionals' information-seeking behaviour, whereas only few studies have addressed the collaborative behaviour of academics, that is, students and researchers. Though not originally designed as a collaborative information behaviour study, Limberg's ([1998](#lim98)) work has contributed to our understanding of group-based information behaviour. She studied five groups of students (18-19 years old) and their information seeking behaviour and knowledge construction process during a project assignment. Among the results, she found that students' orientation towards group-work influenced their approach to information behaviour. Groups with an holistic approach (_we-orientation_), for example, were characterized by students who acknowledged the value of group-work and considered group-work as a collective task towards a shared goal, implying that various group activities were needed to succeed in achieving the goal. In addition, they considered the establishment of a shared knowledge base as very important, which also was demonstrated in their information behaviour. Information was exchanged among group members and they informed each other of the outcomes of their reading. Moreover, they marked relevant parts of text to each other and circulated the information afterwards. The information search itself was delegated to individuals or minor groups, so that some were in charge of searching specific databases and libraries, while others were responsible for getting in contact with specific institutions and organizations.

The holistic approach to information behaviour also resulted in shared relevance criteria, affecting their judgement of information. Furthermore, the holistic group members tended to reinforce each group member's perception of the cognitive authority of specific information sources. In addition, the existence of different opinions with regard to the topic was only considered to be an advantage to the analysis and process of construction. In contrast, groups with an atomistic approach to group-work (_I-orientation_) was characterized by students who had organized the group-work according to specific parts of the assignment, which had been delegated to each group member. They were generally lacking a perception of the 'whole', meaning the collective product to be submitted. The work was divided between the group members and they did not meet outside the school schedule, hence worked more on an individual basis. This was also reflected in their approach to information behaviour, since they did not effectively communicate information in the group, nor aimed at building up a shared knowledge base. The result of the atomistic approach was demonstrated in a weak learning outcome. They were, however, positive towards group-work in the sense that it made them feel more confident compared to working individually, as one may help each other in groups. This was to some extent related to their type of personality, since atomistic group members often tended to lack confidence, both with regard to themselves and the other group members.

## An exploratory study

The exploratory study on personality traits and group-based information behaviour presented here is part of a larger case study on students' information behaviour in a group-based setting (Hyldegård [2006b](#hyl06b); [2008](#hyl09)). The aim of the large case study was to explore to what extent group members' information behaviour compared to the individual modelled by Kuhlthau's ([1991](#kuh91); [2004](#kuh04)) Information Search Process model, when social, work task and personality factors were taken into account. More specifically, the aim was to explore the influence from these factors (in addition to the mere information seeking process) on group members' actions and cognitive and affective experiences during a complex problem solving process, as a follow up to earlier work ([Hyldegård 2006a](#hyl06a)). Three groups (10 group members) participated in the study. Part of the data collected will be employed in the discussion of the personality dimension of group-based information behaviour. Hence, methods apart from the personality test will also be described in more detail below.

### Research questions

To address the personality dimension of group members' information behaviour (as defined by Wilson [1999](#wil99): 249) three research questions were formulated:

1.  What personality traits do the group members possess according to the personality test instrument?
2.  How do these traits correspond to group members' information behaviour in the group?
3.  How does the social context (group setting) influence and mediate between group members' personality traits and their information behaviour - if at all?

### Participants

The participants were ten Danish graduate students in library and information science studying at their fifth term. At this level the curriculum is dedicated to problem-based project work and group-work accordingly. The students followed two type of courses, one with an internal examination (A-courses) and one with an external examination (B-courses). In both courses they were required to undertake an assignment. The ten students who agreed to participate followed two different B-courses within cultural studies. The students ranged from 23 to 48 years of age, nine were females and one was male. They voluntarily formed two 3-person groups and one 4-person group (the male was in the last group). Participants were experienced information seekers and had previous experience with group-based as well as individual project assignments.

### Work task

The work task, the project assignment, was a mandatory part of the B-courses, which covered various subjects, such as cultural heritage studies, children's' literature, music mediation and bibliometric studies. The project period lasted fourteen weeks, from week 41 of 2004 to week 01 of 2005\. During this period the students had to formulate a problem within a specific project topic, explore the topic and find a focus, find and digest relevant literature, collect and analyse data, devise a structure for presenting their argument and finally write a project report. The project report approximated twenty pages for students working individually and thirty to forty pages for groups of students. The work task is only addressed at the descriptive level in this paper.

### Data collection

To recruit participants, an invitation describing the project was published on the students' intranet. In addition, the teachers who were involved with these students were contacted to allow for an introduction to the project in class. The only condition laid down was that the participants had to be group members. The participants were promised a fee (80 €) and ensured full anonymity. 10 students (three groups) accepted the invitation. In advance they were given a thorough two-hour introduction and guidance to the study and the implied data collection methods. Further, to facilitate the data collection process and establish a visual memory cue a ring binder was handed out to each participant containing all relevant material to be used in the study. All participants signed a consent form before participation.

Three methods were selected for data collection: questionnaires (a demographic questionnaire, a personality test and process surveys), diaries and interviews, to triangulate the data. The data were collected at three points in the process: start, midpoint and end.

The demographic questionnaire consisted of twenty-three questions and statements and was handed out at the beginning of the study. The aim was to collect personal information such as sex and age, as well as information related to prior experience in terms of group-work, the subject of interest, information technology and information seeking. In addition, thirteen statements on information seeking behaviour were formulated addressing one or more personality dimensions in line with Heinström's ([2002](#hei02)) study. A strong relation between a specific statement and individuals' personality traits may, however, be difficult to make. Hence, data should only be interpreted as indications of behaviour deriving from personality.

To describe and compare each group member's personality traits at a more detailed level the long version of NEO-PI-R was employed. All participants were given a short introduction to the personality test and its use before they were asked to reply to the 240 statements in the questionnaire. After the data had been recorded and reports of the test result had been made, the participants were given a copy of the test result and invited to participate in a debriefing interview with the tester for further discussion and feedback. The participants were also invited to comment on their test experiences towards the end.

To elicit behavioural data associated with information seeking, as well as with the project assignment and group-work over time, a printed process survey was filled out by participants and handed in at three selected dates in the process: at the start (October 2004), midpoint (November, 2004) and end (December 2004). The process survey was divided into three parts with questions concerning activities and cognitive and emotional experiences in relation to the project assignment (part A), information seeking (part B) and group-work (part C). The three process surveys were identical in order to observe changes over time. Concerning the cognitive aspect of the project assignment, for example, the first question in each process survey asked for a short description of the topic and the title of the assignment as a way to observe progress in focus formulation during time ([Kuhlthau 2004](#kuh04)). Affective aspects associated with the project assignment were addressed in the process survey by asking each participant to state his or her emotional experiences with a number from 0 (not recognized) to 5 (high) in relation to 6 positive feelings (confident, satisfied, optimistic, relieved, motivated and clarity (a sense of direction) and 7 negative feelings (confused, doubtful, stressed, frustrated, uncertain, worried/cautious). The cognitive aspect of the information seeking process was associated with participants' relevance judgement over time, e.g., whether it changed from 'relevant' information at start to 'pertinent' information at the end of information seeking.

To address the affective aspects of information seeking in the process survey, each participant was asked to indicate his or her experience of information seeking according to four pairs of positive and negative feelings on a scale from 1 (positive) to 5 (negative). The positive feelings and their corresponding negative feelings were: easy/difficult, relaxing/stressing, simple/difficult and satisfying/frustration. If other pairs of positive and negative feelings had been experienced, the participants were allowed to note these in the survey and mark the value accordingly. The final part of the process survey regarded group-work issues, e.g., which activities that took place in the groups, how other group members influenced the individual's problem solving process and the perceived atmosphere in the group.

Each process survey was followed by a seven-day diary period to collect data daily on each group member's activities and experiences related to the work task, information seeking and group-work at the three selected points. In addition, the diary was intended to guide the interviews with the participants afterwards; both when deciding which issues to address in the interviews and during the interviews when referring to specific incidents. Moreover, the diary was intended to serve as a surrogate for direct observation, as it was difficult to predict where and when relevant activities would take place.

To make the diary keeping easier the diary form was printed and inserted in a binder handed out at the beginning of the study. In this way the participants could bring the diary with them and record activities immediately after the activities had taken place. In contrast to the diary format in a previous case study ([Hyldegård 2006a](#hyl06a)), this diary allowed for a more open and free description of activities in the participants' own words. The larger amount of text data deriving from the unstructured format was limited by the shortness of the diary period and the physical form of the printed diary. The participants were instructed to record daily, and in their own words, any assignment-related activity, which were to be described chronologically and, at best, immediately after the activity had taken place. Furthermore, the start and end of the activity as well as the time the diary had been filled in were to be noted. In the final part of the diary, the participants were asked to indicate their recognition and experience of each of the listed positive and negative feelings with a number from 0 (not recognized) to 5 (highly recognized). Recognized feelings not represented on the list should be noted with a value under 'Other'. The emotional part of the diary was to be filled out daily, even if no assignment-related activities had taken place. To clarify the use of the diary and qualify recorded diary data the diary was pilot tested for two days prior to the official start of the study.

After the process survey had been handed in and the seven-day diary period had ended, each group member was interviewed. Each interview at start, midpoint and end lasted about 45 minutes. By interviewing the participants individually it became possible to explore whether and how group members differed in their perception and experience of identical situations (personality dimension); whether and how their work task and information related activities were individually or collaboratively based and how perceptions and experiences evolved over time. In addition, the interviews helped to triangulate and support the analysis of the process surveys and the diaries.

A semi structured guide was made for each of the three interview sessions, which addressed different aspects related to the work task, information seeking and group-work in accordance with the specific point in the process. The first group of questions concerned activities and experiences associated with the project to track cognitive as well as affective changes over time and why.

The next category of questions addressed activites and experiences associated with information behaviour, e.g., goal of information seeking 'at the moment', choice, employment and use of information sources and relevance judgements. The last category of questions regarded activities and experiences associated with group-work, such as familiarity with other group members, meetings, form of communication, group-work practice and organisation. The form of the interview was guided by the micro-moment time-line technique derived from the sense-making approach ([Dervin 1983](#der83)). The aim was to get out the informant's feelings, thoughts and experiences in relation to various situations and phenomena while the informant made sense of his or her reactions. All thirty interviews were recorded.

### Data analysis

The demographic questionnaire generated categorical data for each group member on personal issues, group-work, the project assignment and information seeking.

With regard to the personality test, the calculation of test data resulted in various T-scores on factors and facets plotted into a scoring scheme for each group member ranging from 'very low' (-34) to 'very high' (66-). The validity of test data should, however, be regarded with some caution as the test instrument rests on self reporting. Although the test instrument is robust and the test, in this case, is based on 240 questions trying to eliminate problems with 'social desirability' we cannot be quite sure whether the scores across individuals represent genuine underlying personality differences, or whether they have been affected by the way the subjects answered the questions. Furthermore, concepts may be perceived differently over time and even between individuals because of previous experience and background. This may imply various positive and negative reactions from subjects, which, again, may affect the scoring over time. In addition, personality traits cannot be deemed to be either absolutely positive or negative but, rather, positive in one situation and negative in another. A very low score on _extraversion_, for example, may be negative in relation to group-work but positive in relation to students writing an individual assignment. Finally, if we regard subjects' personality traits as tendencies of behaviour dependent on specific situations or contexts we cannot determine with certainty to what extent influence from personality on information behaviour will differ between a group and a non-group setting.

The three process surveys from each group member resulted primarily in categorical data focusing on activities as well as cognitive and emotional experiences related to the work task process, the information seeking process and the group-work process at start, midpoint and end. The data from the thirty process surveys in total were plotted into Microsoft Excel spreadsheets and matrices and graphical diagrams were generated to show relevant process data across group members and over time. The thirty diaries generated descriptive data on 'activities' and comments associated with the recorded data or the project in general. Since diary data were given in physical form all text-data in the diaries were transcribed directly from paper to electronic form (Word) and coded in the qualitative data analytical program ATLAS.ti. The diaries also generated categorical data on 'feelings' perceived by the participants during the project assignment. These feelings were registered and analysed in Excel. The thirty interviews were transcribed for coding in ATLAS.ti, but only those parts associated with the research focus, thereby leaving out side leaps, repetitions and unimportant details. The coding of diaries and interviews was based on the 'dynamic coding process' presented by Strauss & Corbin ([1998](#str98)) involving primarily open coding and axial coding.

## Results

The results of the study are presented in three subsections: 1) group members' personality traits and the associated hypothesis about their behaviour and 2) group members' actual information behaviour and 3) indications of mapping between group members' personality traits and information behaviour. The first section is primarily based on the underlying assumptions of the test instrument and Heinström's empirical work, whereas the last two sections are based on data from the demographic survey, process surveys, diaries and interviews. Results concerning the social context as influencing and mediating factor will be discussed more thouroughly after the result presentation. Groups are referred to as group A, B and C, and group members are accordingly referred to as A1, A2, ... B1, B2, ... C1, C2, C3\.

### Group members' personality traits and the associated hypothesis about behaviour

To ease the identification of group members' personality traits and the comparison of the NEO-PI-R test result across groups, the group members have been distributed according to their t-scores on both factors and facets as shown in Table 2.

<table><caption>

**Table 2: The ten group members' test result on factors and facets according to the personality scoring scheme running from 'very low' (-34) to 'very high' (66-). Factors are highlighted and in bold.**</caption>

<tbody>

<tr>

<th> </th>

<th>Very low</th>

<th>Low</th>

<th>Middle</th>

<th>High</th>

<th>Very high</th>

</tr>

<tr>

<th>T-scores</th>

<th>-34</th>

<th>35-44</th>

<th>45-55</th>

<th>56-65</th>

<th>66-</th>

</tr>

<tr>

<th>Neuroticism (N)</th>

<th>  </th>

<th>B3</th>

<th>A1, C2, C4</th>

<th>A2, A3, B1, B2, C1, C3</th>

<th> </th>

</tr>

<tr>

<td>Anxiety</td>

<td> </td>

<td>B3, C4</td>

<td>A2, B1, C1, C2, C3</td>

<td>A1, A3, B2</td>

<td> </td>

</tr>

<tr>

<td>Temper</td>

<td>B3</td>

<td> </td>

<td>A1, B1, C2, C3, C4</td>

<td>A2, A3, B2</td>

<td>C1</td>

</tr>

<tr>

<td>Pessimism</td>

<td>B3</td>

<td>A1, C2</td>

<td>C3, C4</td>

<td>A2, A3, B1, C1,</td>

<td>B2</td>

</tr>

<tr>

<td>Social fear</td>

<td> </td>

<td>A1, C2</td>

<td>A3, B3, C3, C4</td>

<td>A2, B2, C1</td>

<td>B1</td>

</tr>

<tr>

<td>Impulsiveness</td>

<td> </td>

<td> </td>

<td>A1, A3, B2, B3</td>

<td>A2, B1, C1, C4</td>

<td>C2, C3</td>

</tr>

<tr>

<td>Nervous</td>

<td> </td>

<td>C4</td>

<td>A1, B3, C1, C2, C3</td>

<td>A2, B1</td>

<td>A3, B2</td>

</tr>

<tr>

<th>Extraversion (E)</th>

<th> </th>

<th> </th>

<th>A1, A2, A3, B1, C1, C2</th>

<th>B2, B3, C3, C4</th>

<th> </th>

</tr>

<tr>

<td>Warmth</td>

<td> </td>

<td>C1</td>

<td>A1, A2, A3, C2, C3</td>

<td>B1, B2, C4</td>

<td>B3</td>

</tr>

<tr>

<td>Gregariousness</td>

<td> </td>

<td>A1, A2, C1</td>

<td>A3, B1, B3, C2</td>

<td>B2, C3, C4</td>

<td> </td>

</tr>

<tr>

<td>Assertiveness</td>

<td> </td>

<td> </td>

<td>A1, A2, A3, B1, C2</td>

<td>B2, C3, C4</td>

<td>C1</td>

</tr>

<tr>

<td>Activity</td>

<td> </td>

<td>A2, A3, B1, B2, C1, C2</td>

<td>A1, B2</td>

<td>C3, C4</td>

<td> </td>

</tr>

<tr>

<td>Excitement seeking</td>

<td> </td>

<td>A1</td>

<td>C1, C4</td>

<td>A2, A3, B1, B2, B3, C2, C3</td>

<td> </td>

</tr>

<tr>

<td>Positive emotions</td>

<td> </td>

<td> </td>

<td>A2, A3, B1, C1</td>

<td>A1, B2, C2, C3, C4</td>

<td>B3</td>

</tr>

<tr>

<th>Openness to experience (O)</th>

<th> </th>

<th> </th>

<th>A1</th>

<th>A2, A3, B1, B2, B3, C3, C4</th>

<th>C1, C2</th>

</tr>

<tr>

<td>Fantasy</td>

<td> </td>

<td> </td>

<td>A1</td>

<td>A2, B1, B2, C3, C4</td>

<td>A3, B3, C1, C2</td>

</tr>

<tr>

<td>Aesthetics</td>

<td> </td>

<td>A1, C3</td>

<td>A2, B2</td>

<td>A3, B1, C1, C4</td>

<td>B3, C2</td>

</tr>

<tr>

<td>Feelings</td>

<td> </td>

<td>B1</td>

<td>C2, C4</td>

<td>A1, A2, A3, C1</td>

<td>B2, B3, C3</td>

</tr>

<tr>

<td>Actions</td>

<td> </td>

<td>A1</td>

<td>A2, A3, B2, B3, C1</td>

<td>B1, C2, C3, C4</td>

<td> </td>

</tr>

<tr>

<td>Ideas</td>

<td> </td>

<td>C3</td>

<td>B2, B3, C4</td>

<td>A2, A3, B1, C2</td>

<td>A1, C1</td>

</tr>

<tr>

<td>Values</td>

<td> </td>

<td> </td>

<td>A3, B3</td>

<td>A1, A2, B2, C1, C2, C4</td>

<td>B1, C3</td>

</tr>

<tr>

<th>Agreeableness (A)</th>

<th>A3</th>

<th>A1, A2, B2, C3</th>

<th>C1, C4</th>

<th>B1, C2</th>

<th>B3</th>

</tr>

<tr>

<td>Trust</td>

<td> </td>

<td>A3, C1, C3</td>

<td>A1, A2, B1, B2</td>

<td>C2, C4</td>

<td>B3</td>

</tr>

<tr>

<td>Straightforwardness</td>

<td>A2, A3, B2</td>

<td>A1, C3</td>

<td>C1, C4</td>

<td>B1, C2</td>

<td>B3</td>

</tr>

<tr>

<td>Altruism</td>

<td> </td>

<td>A1, A3, B2, C3</td>

<td>A2, B1, C1</td>

<td>C2, C4</td>

<td>B3</td>

</tr>

<tr>

<td>Compliance</td>

<td>C3</td>

<td>A3, B2, C4</td>

<td>A1, A2, C1, C2</td>

<td>B1, B3</td>

<td> </td>

</tr>

<tr>

<td>Modesty</td>

<td>A3</td>

<td>A1, C3</td>

<td>A2, C1, C2, C4</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Tender mindedness</td>

<td> </td>

<td> </td>

<td>A1, A2, A3, B1, B2, C1, C2, C3</td>

<td>B3, C4</td>

<td> </td>

</tr>

<tr>

<th>Conscientiousness (C )</th>

<th>B1</th>

<th>C4</th>

<th>A1, A2, A3, B2, C1, C2, C3</th>

<th> </th>

<th>B3</th>

</tr>

<tr>

<td>Competence</td>

<td> </td>

<td>B1</td>

<td>A2, A3, C1, C4</td>

<td>A1, B2, B3, C2, C3</td>

<td> </td>

</tr>

<tr>

<td>Order</td>

<td>A2, B1, C4</td>

<td>C2</td>

<td>A1, C3</td>

<td>A3, B2, B3, C1</td>

<td> </td>

</tr>

<tr>

<td>Dutifulness</td>

<td>B1</td>

<td>B2, C1, C4</td>

<td>A1, A2, A3, C2, C3</td>

<td> </td>

<td>B3</td>

</tr>

<tr>

<td>Achievement striving</td>

<td> </td>

<td> </td>

<td>A1, A2, A3, B1, B2, B3, C1, C3, C4</td>

<td>C2</td>

<td> </td>

</tr>

<tr>

<td>Self discipline</td>

<td>B1</td>

<td>C1, C3, C4</td>

<td>A2, A3, B2, C2</td>

<td>A1, B3</td>

<td> </td>

</tr>

<tr>

<td>Deliberation</td>

<td>B2, C2</td>

<td>A3, C3</td>

<td>A1, A2, B1, C1, C4</td>

<td>B3</td>

<td> </td>

</tr>

</tbody>

</table>

When looking at the T-scores on factors in Table 2 it is interesting to see that many of the group members scored 'high' on neuroticism. According to the test instrument this indicates that the participants – except for B3 - might be sensitive to stress and uncertainty.

With relevance to group-work it is worth noticing that all group members scored 'middle' to 'high' on extraversion – one of the social factors. This may, however, not be a surprise when taken into consideration that all participants voluntarily joined the case study, partly out of an interest in group aspects. It is also worth noticing that most of the group members scored 'high' - and in two occasions even 'very high' - on openness to experience, which considers intellectual aspects. According to the test instrument this indicates a positive attitude towards changes, an intellectual curiosity and willingness to think in alternative and unconventional ways. Except for B1, B3 and C4, group members scored 'middle' on conscientiousness, which also could be seen as a tendency towards an efficient, trustworthy and collaborative group-work behaviour.

When looking at the intra-group scorings, group A was characterized by three group members who had a very similar personality at the general level. Looking at the characteristics at the facet level more nuances turned up. The most striking differences could be noticed in relation to the facets: 'pessimism', 'social fear', 'nervous', 'seeking excitement', 'fantasy' and 'order'. Values differed here from 'low' to 'high', from 'middle' to 'very high' and from 'very low' to 'high'. The complexity of personality traits hidden in the facets was seen, for example, in A1\. She possessed a high level of anxiety, but had a positive and social attitude towards life that was further reflected in high positive emotions in the study. She was, however, not eager to seek excitement and had a critical and aloof approach towards others. Also, she had a 'high' level of competence and self-discipline.

Group B was characterized by three group members with a very different personality at both the factor level and the facet level. Except for high levels of 'openness to experience', they differed in all the other factors, either between one group member and the other two or between all of them. Looking at the facet level, the most remarkable differences across the group were seen in relation to 'pessimism', 'straightforwardness' and 'dutifulness'. Values differed here from 'very low' to 'very high'. Other differences of importance were noticed in relation to the facets: 'anxiety', 'temper', 'social fear', 'nervous', 'feelings', 'values', 'trust', 'altruism', 'compliance', 'competence', 'order', 'self discipline' and 'deliberation'. Here the values differed from either 'very low' to 'high' or from 'low' to 'very high'. Group C was characterized by four group members who generally, either two by two or three by one, had the same personality scorings on factors and facets in common. The facets that were shared by most group members were 'positive emotions', 'actions' and 'values', 'anxiety', 'temper', 'nervous', 'modesty', 'tendermindedness', 'achievement striving' and finally 'self discipline'. Only in two occasions no similarities across group members were found, which interestingly with relevance to group-work were 'intellectual curiosity' and 'orderliness'.

To sum up, the similarities across groups and group members concerning the general personality traits were primarily reflected in high levels of neuroticism, middle to high levels of extraversion, high levels of openness to experience and middle levels of conscientiousness. Differences were primarily found with respect to agreeableness. At the facet level more nuances in personality traits were found.

Based on the test result and with regard to the empirical work on personality and information behaviour by Heinström some observations on information behaviour were then expected.

It was expected that most of the group members would demonstrate a tendency towards high affective levels of uncertainty and stress during the project assignment process. Also, that they would try to reduce stress, uncertainty, and complexity by employing rational information strategies and the 'principle of least effort' ([Mann 1987](#man87)), for example, by using known information sources ([Kuhlthau _et al._, 2008](#kuh08)) or by limiting the recall of information. According to Costa & McCrae ([1992](#cos92)), however, some facets of high neuroticism may enhance certain types of critical thinking, as 'neuroticism' also can be a measure of being irritable and defensive. Many of the uncertain group members, for example, were scoring 'high' and 'very high' on 'temper' and 'pessimism'. Given that, 'neuroticism' may not in all cases be associated negatively with nervousness and anxiety.

These hypotheses about behaviour associated with uncertainty might also be challenged by the group members' openness to experience which implies preferences for new information and a broad range of information. Group members with a low level of agreeableness were expected to demonstrate impatience and perceptions of time pressure and lack of time that might result in rational rather than knowledge constructive reasons for search closure. Furthermore, the characteristics of conscientiousness among group members were expected to result in a goal-oriented, responsible and achievement-determined behaviour. In addition, the middle to high score on extraversion was expected to be reflected in a collaborative orientation towards information behaviour. However, as differences in personality traits among intra-group members were found at the factor as well as at the facet level, observations of different approaches to information behaviour in the groups were also expected, which might bring about conflicts or constraints with regard to the collective product (the assignment).

### Group members' actual information behaviour

Group members' actual information behaviour addressed here refers to the identified information activities and affective experiences during the assignment process with relevance to the stated hypothesis about group members' behaviour. In line with Vakkari's ([2001](#vak01)) conceptualisation of the research process, stages of the assignment process are referred to as the pre-focus stage, the focus formulation stage and the post-focus stage.

#### Information activities

This section addresses the groups' various individual and collaborative activities and strategies according to point in the assignment process, which also implies the use of information sources.

In the beginning, each group member in group A, searched for information on her own, but informed the others of relevant information, e.g., by sending links. Group member A1, in particular, was considered good at distributing information by the other group members. At group meetings, search terms were exchanged, both at start and at midpoint, and the relevance of information sources, searched or read, was discussed. Later, when parts of the assignment had been delegated in the group, information was exchanged or distributed to specific group members if considered relevant. A1 once brought one of 'her' information sources to a group meeting. This information source was often referred to in the interviews by the other group members when reflecting upon their delegated part of the assignment. For example, A2 used aspects from this specific information source to perform a new search. In the same way, A3 searched for the document in various databases to see the assigned keywords and make a new search in the systems. Most of the information in group A was found before the division of the assignment into delegated parts.

Searching in group B was mostly performed in common, that is, sitting in the same room, but with each group member at her computer searching different information sources, for practical reasons. During this process the relevance of information was discussed and search expressions and keywords were exchanged. At meetings, in particular at start and midpoint, information was strategically shared and exchanged. When information sources had been read by one group member, for example, a summary and relevance judgement was given to the other group members, thereby preventing the group from reading the same but irrelevant documents. However, if information turned out to be difficult to understand, it was read and interpreted collectively.

At the beginning, group members of group C generally searched for much of their information on a collaborative basis, either by sitting at the same computer or by dividing the search between them, implying specific but different information sources (channels) to be searched. Relevant search terms were exchanged, in addition to those each group member found in 'his' or 'her' system. The relevant information was also shared and exchanged during meetings. After midpoint and towards the end, searching was performed more individually, partly for practical reasons (time pressure), but also due to the delegation of specific parts of the assignment. Information , however, was still exchanged among group members if considered relevant. According to C3, having four members in the group had resulted in more aspects, but also in more time spent on discussing the relevance of information sources. This was partly caused by a difference in relevance criteria. Group member C4 did not search very much, but read the information found by others. He explained that he lacked search-task knowledge and skills and preferred to let the other group members do the searching.

Regarding the employment of information sources during the assignment process (process surveys and interviews) the use of information sources in group A seemed to be associated with the delegation of assignment parts. Besides the use of 'books' and 'personal sources' at start and end, A1, who was in charge of the most complex part, generally employed more information sources than the rest of the group. In comparison, A2, who was in charge of a rather descriptive part of the assignment, only used a few information sources during this time. This was also the case with A3, who was in charge of a very concrete part, except for the last period, when her engagement in various parts of the assignment required more sources to be used. These information sources were found from a number of different information systems, but in particular from the use of 'other libraries' (than the library at the Royal School of Library and Information Science), 'OPACs' and the 'Internet'.

In group B, only a few sources were employed, such as 'journals' (printed and electronic) and 'books', and they were generally identical across the group. This was related to the division of the assignment into three main sections with sub-sections that were distributed equally among group members. Hence, all group members were engaged in a descriptive as well as an analytical writing task, requiring the same type of sources to be used. The information sources were generally found by the use of the 'Royal School library' (physical and online),'other libraries' (physical and online) and the 'Internet'.

In group C, 'books', 'newspapers' and 'personal sources' (supervisor and group members) were generally used at all stages of the process, independently of the many shifts in focus. Group member C2, however, only found personal sources important at the end. The information sources were generally found by using 'other libraries' (online), 'other databases' (than OPACs) and the 'Internet'. According to C3, the use of information sources should also be seen in association with their educational background. The Internet would, for example, have been used much more if they had not been librarianship students, a consequence of which was that databases, OPACs etc. are acknowledged as quality information sources, whereas the Internet is not.

#### Affective experiences

This section addresses the emotional experiences associated with information seeking (finding relevant information and use of information systems and information) and the assignment process in general.

When looking at group members' affective experiences over time, in association with information seeking, many of the group members perceived information searching as 'complex' at the beginning, as shown in Table 3\.

<table><caption>

**Table 3: Group members' affective experiences in association with 'information seeking'**.  
The table is based on group member values assigned to affective dichotomies on a scale from 1 (positive) to 5 (negative) in each of the three process surveys at start, midpoint and end. C2 was not seeking information at midpoint, hence the '0'-values.</caption>

<tbody>

<tr>

<th>FEELINGS</th>

<th colspan="3">Difficult</th>

<th colspan="3">Stress</th>

<th colspan="3">Complex</th>

<th colspan="3">Frustrating</th>

</tr>

<tr>

<th>TIME</th>

<th>start</th>

<th>mid-point</th>

<th>end</th>

<th>start</th>

<th>mid-point</th>

<th>end</th>

<th>start</th>

<th>mid-point</th>

<th>end</th>

<th>start</th>

<th>mid-point</th>

<th>end</th>

</tr>

<tr>

<td>A1</td>

<td>3</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>3</td>

<td>4</td>

<td>2</td>

<td>2</td>

<td>3</td>

</tr>

<tr>

<td>A2</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>3</td>

<td>4</td>

<td>2</td>

<td>4</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>3</td>

</tr>

<tr>

<td>A3</td>

<td>1</td>

<td>4</td>

<td>1</td>

<td>1</td>

<td>3</td>

<td>1</td>

<td>1</td>

<td>4</td>

<td>1</td>

<td>3</td>

<td>3</td>

<td>1</td>

</tr>

<tr>

<td>Average</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td>B1</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>4</td>

<td>3</td>

</tr>

<tr>

<td>B2</td>

<td>4</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>2</td>

</tr>

<tr>

<td>B3</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>2</td>

<td>1</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>1</td>

<td>3</td>

<td>3</td>

</tr>

<tr>

<td>Average</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

</tr>

<tr>

<td>C1</td>

<td>2</td>

<td>2</td>

<td>2</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>3</td>

</tr>

<tr>

<td>C2</td>

<td>2</td>

<td>0</td>

<td>4</td>

<td>1</td>

<td>0</td>

<td>3</td>

<td>2</td>

<td>0</td>

<td>4</td>

<td>1</td>

<td>0</td>

<td>3</td>

</tr>

<tr>

<td>C3</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>5</td>

<td>3</td>

<td>1</td>

<td>4</td>

<td>4</td>

<td>1</td>

</tr>

<tr>

<td>C4</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>4</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>2</td>

</tr>

<tr>

<td>Average</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>4</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>2</td>

</tr>

</tbody>

</table>

This was partly related to the prefocus-stage when no focus had been found, making it difficult to perform a search and judge relevance. But also at midpoint and towards the end 'some' complexity was found in addition to 'some' 'difficulty', 'stress' and 'frustration'. This indicates, in turn, that information seeking only rarely was considered 'easy', 'relaxing', 'simple' and 'satisfying' by groups. With reference to the personality test result it may indicate that a connection exists between group members' 'negative emotionality' profiles and their expressed negative experiences of information seeking. However, affective experiences were also found to be associated with group members' own perceived level of search task knowledge and skills. Though C4 scored 'medium' on neuroticism and 'high' on almost all facets of extraversion, his low expectations of his own search capabilities resulted in him, for example, leaving all searching to the other group members. It is, however, likely that he would have experienced the searching situation as more difficult and stressful if he had been on his own. In addition, it was found that group members' perception of their own search-task performance also could change negatively as well as positively when compared to the performance of other group members.

In looking at the affective experiences associated with the assignment process, another pattern was identified. Although most of the group members turned out to be persons with high levels of neuroticism, they generally demonstrated low affective values of uncertainty and high values of confidence. Figure 1-3 show the perceived experiences of confidence, clarity and uncertainty during the assignment process for group A-C as it was expressed in the three process surveys. The same patterns were found in the diaries as well.

<figure id="fig-1">

![Figure 1: Perceived experiences of confidence, clarity and uncertainty: Group A-members](../p402fig1.png)

<figcaption>

**Figure 1: Perceived experiences of confidence, clarity and uncertainty: Group A-members.**  
Expressed by a number from 1(low) to 5 (high) in the three process surveys: at the 22\. October, the 19\. November and the 17\. December.</figcaption>

</figure>

<figure id="fig-2">

![Figure 2: Perceived experiences of confidence, clarity and uncertainty: Group B-members](../p402fig2.png)

<figcaption>

**Figure 2: Perceived experiences of confidence, clarity and uncertainty: Group B-members.**  
Expressed by a number from 1(low) to 5 (high) in the three process surveys at the 22\. October, the 19\. November and the 17 December.</figcaption>

</figure>

<figure id="fig-3">

![Figure 3: Perceived experiences of confidence, clarity and uncertainty: Group C-members](../p402fig3.png)

<figcaption>

**Figure 3: Perceived experiences of confidence, clarity and uncertainty: Group C-members.**  
Expressed by a number from 1(low) to 5 (high) in the three process surveys at the 22\. October, the 19\. November and the 17\. December.</figcaption>

</figure>

It turned out that these affective experiences were associated, to a large extent, with 'social and professional familiarity' with other group members. The groups had been formed for reasons of familiarity before the assignment topic was chosen. Familiarity often implied feelings of safety and relaxation with regard to the work task. However, the most 'uncertain' persons according to the T-scores on facets such as anxiety, temper, pessimism, social fear and nervousness (e.g., B2), in general also showed higher values of stress throughout the process than the more secure members of the group. This is shown in Figure 4, which is based on the diary data.

<figure id="fig-4">

![Figure 4: Experiences of 'stress' over time: Group B-members.](../p402fig4.png)

<figcaption>

**Figure 4: Experiences of 'stress' over time: group B-members.**  
Values on a scale from 0-5 for each group member and for each diary period (7 days) at start, midpoint and end.</figcaption>

</figure>

### Mapping between hypothesised and actual behaviour

This section addresses the indications of mapping between group members' personality traits and information behaviour.

Table 4 shows group members' agreements with the ten statements in the demographic survey concerning personality and information behaviour. The personality dimension associated with each statement is explained in the text below the table. To indicate the differences and similarities between intra-group members' agreement statements, the white areas emphasize the intra-group similarities, whereas the grey areas emphasize the intra-group differences.

<table><caption>

**Table 4: Group members' agreements to selective information seeking statements representing various personality dimension(s). (1=disagreeing; 5=strongly agreeing).**  
The yellow areas emphasize intra-group similarities and the grey areas emphasize intra-group differences. The numbers and statements originate from the demographic survey and refer to the following personality dimensions: 11:neuroticism; 12 & 16:openness to experience (low values); 13:openness to experience; 14:openness to experience & conscientiousness; 15:extraversion; 17:conscientiousness; 18-20:agreeableness.</caption>

<tbody>

<tr>

<th>Statement</th>

<th>A1</th>

<th>A2</th>

<th>A3</th>

<th>B1</th>

<th>B2</th>

<th>B3</th>

<th>C1</th>

<th>C2</th>

<th>C3</th>

<th>C4</th>

</tr>

<tr>

<td>

11\. I often get impatient if I don't find what I'm looking for</td>

<td>2</td>

<td>2</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>1</td>

<td>3</td>

<td>3</td>

<td>3</td>

<td>2</td>

</tr>

<tr>

<td>

12\. I prefer documents confirming my thoughts and ideas</td>

<td>3</td>

<td>3</td>

<td>1</td>

<td>3</td>

<td>2</td>

<td>1</td>

<td>1</td>

<td>2</td>

<td>1</td>

<td>3</td>

</tr>

<tr>

<td>

13\. I prefer documents creating new thoughts and ideas</td>

<td>4</td>

<td>3</td>

<td>4</td>

<td>4</td>

<td>4</td>

<td>4</td>

<td>5</td>

<td>5</td>

<td>4</td>

<td>3</td>

</tr>

<tr>

<td>

14\. I sometimes run into documents that I have not searched for consciously.</td>

<td>3</td>

<td>5</td>

<td>4</td>

<td>4</td>

<td>4</td>

<td>5</td>

<td>5</td>

<td>4</td>

<td>4</td>

<td>2</td>

</tr>

<tr>

<td>

15\. I often find documents by talking with or asking other people</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

<td>4</td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td>

16\. Few, well chosen documents are enough to write an assignment</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>1</td>

<td>3</td>

</tr>

<tr>

<td>

17\. Sometimes I do not have time to search for documents</td>

<td>2</td>

<td>2</td>

<td>2</td>

<td>4</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>2</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>

18\. I prefer documents that are easy to access</td>

<td>2</td>

<td>2</td>

<td>1</td>

<td>3</td>

<td>3</td>

<td>2</td>

<td>1</td>

<td>2</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>

19\. Articles, published in journals, are reliable</td>

<td>2</td>

<td>3</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>3</td>

<td>3</td>

</tr>

<tr>

<td>

20\. I gladly wait two weeks for an inter-urban loan</td>

<td>3</td>

<td>4</td>

<td>4</td>

<td>5</td>

<td>4</td>

<td>5</td>

<td>4</td>

<td>4</td>

<td>5</td>

<td>3</td>

</tr>

</tbody>

</table>

Looking at the groups, most similarities in behaviour tended to occur between group members in group A, whereas most differences could be seen in group B and group C. This corresponds well to the personality pattern demonstrated by the personality-scoring scheme presented earlier. However, deviations from the scoring scheme were also found.

The first statement in Table 4 concerns neuroticism, but no strong mapping was found between the agreement values given and the general high values of neuroticism in the personality test result. Only tendencies towards neuroticism were identified in five cases (A3, B2, C1, C2, C3). With regard to B3, however, the reply corresponded well to B3's very low value on neuroticism.

Statement 15 implies indications of extraversion, but the 'middle' to 'high' levels of extraversion in the personality-scoring scheme were only matched in four cases, that is, by A2, B3, C1 and C2\.

Statements 12, 13 and 16 address openness to experience (low values in 12 and 16; high values in 13). The 'high' scores across group members on openness in the personality scheme were to a large extent reflected by the agreement values in the table. When looking at statement 16, however, it could also be associated with insecure behaviour, hence motivating the need for few documents to avoid confusion.

Statements 18-20 address agreeableness (high agreement values). The pattern of agreeableness in table 4 only matched the scoring scheme in few cases (A1, B1 and C2). Instead, it seemed as if the statements themselves had generated replies that either were critical (statement 18) or expressions of agreeableness (statement 19-20) in correspondance with ideas of correct information literate behaviour. Hence, their replies may have been influenced by the domain context of librarianship.

Finally, statement 17 addresses conscientiousness (indicated by low agreement values). According to the personality-scoring scheme all group members demonstrated 'middle' values on conscientiousness, except for B1 and C4 (low) and B3 (high). However, when looking at the agreement values in the table a more conscientious information behaviour is generally reflected (low values). With regard to B1, however, the high value on statement 17 corresponded well to the low value of conscientiousness in the personality scheme.

Indications of mapping between group members' personality traits and their information behaviour have also been investigated in the process survey, diaries and interviews.

Looking at the criteria used by several of the 'insecure' group members to determine search closure, they may be seen as indications of neuroticism. For example, B2 and C3 stopped searching to avoid getting stressed, confused or frustrated. Group member A3 and B1 stopped searching because of lack of time, although this did not imply that all relevant information had been found, which B1 found frustrating. C1 did not actually stop searching, as she was afraid of overlooking relevant information. B3, in turn, stopped when she found that enough relevant information had been found and when she got tired of reading, hence demonstrating a more confident behaviour.

With regard to relevance judgement, B2 found it difficult to judge relevance, but this was partly associated with difficulties derived from collaborative relevance assessment. In spite of demonstrating a secure behaviour, B3 found it difficult to judge relevance. Group member C3 generally found it difficult to find relevant books.

A preference for confirming information was not identified, only indicated by some of the 'insecure' group members in the demographic survey just presented. In contrast, the need for thought-provoking documents was indicated by B3\. Although demonstrating 'middle' values of neuroticism, a 'history of failure' seemed to have affected the C4's behaviour. He did not search very much, but read the information found by others as the information he found was not that relevant. He explained that he had difficulties in knowing where to search and how to search in, for example, the Dialog databases. At the start, he typically chose a broad term and explored the topic from there.

Four group members were characterized by 'high' values on extraversion (B2, B3, C3, C4) but an information seeking behaviour based on quick solutions and use of social abilities was not identified. Furthermore, no strong preference for thought-provoking documents was found. In the demographic survey only C3 showed a tendency towards a preference for thought-provoking documents. Although only scoring 'middle' values C2, in turn, tended to demonstrate an extravert information behaviour. She preferred to search in collaboration with others, which she also explained by a lack of patience. Also, she generally considered herself to be so good at searching that no preparation was necessary, meaning that she did not make any search plan on where and how to search in advance. She almost 'jumped' into the search process. If she had done the best she could, she thought the search had been satisfying.

With relevance to the 'middle' to 'high' values on openness to experience, A1 possessed an inner curiosity and a desire for getting new knowledge. She never found searching difficult, rather challenging. She looked forward to the delegation of subtasks (specific parts of the assignment); hence she could focus her search for information, without losing sight of the others' parts. Group member A2, B1, B2, B3, C1 and C2 were generally collecting an amount of information from the outset. At the beginning of an assignment project, A2 generally experienced many possibilities. She got very excited and started to collect much information, which she skimmed and sorted for relevance afterwards. C1, in particular, mentioned that it was a part of her strategy to seek broadly and spread out the subject. C3 generally found searching very exciting, despite the complexities of relevance judgement, and had the experience that she kept on progressing during the assignment process. She had a job involving a lot of searching, hence considered herself to have profound search task knowledge and profound search task skills, particular with regard to Dialog databases. She knew which strategies and search codes to use; otherwise she would just choose the 'simple search' facility. In contrast, group member C2, stressed the importance of keeping a focus. This was in line with C4, who preferred to settle on a focus quite early in the process in order to limit the search. Group member A3 also preferred to find few very relevant information sources to start from, since it often initiated the process of idea generation. The employment of social sources (group members and supervisor) was particularly demonstrated by A1, all group C-members and B2.

Half of the group members scored 'low' values on agreeableness (A1, A2, A3, B2, C3), meaning that they were critical and skeptical persons characterized by impatience. In contrast, B1, B3 and C2 scored high values on agreeableness, implying a friendly, positive and less critical approach to information and relevance judgment. With regard to the 'critical' group B-members, B2 demonstrated a skeptical and impatient behaviour, particular with regard to group-work, which was also reflected in her approach to information seeking. A quick reduction of information was preferred, hence she advised all group members to read and judge the relevance of different information sources. She generally searched without search prefixes, partly because she found it difficult to perform subject searches. In turn, B3 stated that she generally collected much information just to be sure, which could be seen as an example of a friendly, less critical attitude. Being part of a group, she said, had resulted in a more critical attitude, however.

The majority of the group members scored 'middle' values of conscientiousness, except for B3 (high values), and B1 and C4 (low values). According to the demographic survey, indications of a conscientious behaviour were identified for the majority of the conscientious group members. In addition, group member B2 demonstrated an efficient group-work behaviour that also implied an efficient employment of information seeking strategies, as mentioned earlier. Group member C2 had devoted so much time to the assignment project that she got very frustrated when the other group members did not match this. According to her, it is important to do all that is possible to create a good product.

The low values of conscientiousness were demonstrated, for example, by B1, when she did not read the book the group had agreed to read before their next meeting. At a later point, she read another researcher's interpretation of the book, instead of the book itself.

## Discussion

### Influence from the social context (group setting)

When looking at the hypothesis deriving from the personality test result regarding 'extraversion', 'openness to experience' and 'conscientiousness' they often tended to match group members' actual approaches to group-work and collaborative information behaviour. Many forms of collaborative information activities and strategies were for example employed. Information was communicated, discussed, exchanged and shared to help formulate a collective goal, obtain a shared understanding of the problem in focus or of specific elements of the assignment. In connection to that behaviour group members often acted as gate-keepers ([Allen 1977](#all77)) for one another. This behaviour also had many elements in common with the collaborative information behaviour identified in studies of complex problem solving in teams (e.g., [Bruce _et al._2003](#bru03); [Talja 2002](#tal02)). In addition, the groups' behaviour also corresponded well with the characteristics of the 'holistic' approach to group-work identified by Limberg ([1998](#lim98)). Group members acknowledged the value of group-work and considered group-work as a collective task towards a shared goal, implying various group activities to succeed ('we-mode'-orientation). In addition, they considered the establishment of a shared knowledge base as very important, though they did not succeed in developing shared relevance criteria. With regard to the hypothesis on 'neuroticism' group members with negative emotionality profiles generally tended to demonstrate negative affective experiences with regard to information searching and specific points or activities in the assignment process, which interestingly were performed on an individual basis. In addition, some of the uncertain persons applied search closure as a way to avoid stress, confusion or frustration in line with Heinström ([2002](#hei02)), whereas others continued to search in fear of overlooking relevant information. These negative experiences, however, could also be a result of low search task knowledge and experience ([Kuhlthau & Tama 2001](#kuh01)), perceived level of search task knowledge and experience ([Bandura 1986](#ban86)) or simply that group members had been struggling with a bad search interface design.

The negative experiences were, however, in contrast to the dominating affective pattern of the assignment process in that group members generally experienced low levels of uncertainty and high levels of confidence and clarity. According to Fiske ([2004](#fis04)) people often tend to depersonalize, when they identify with a group, which implies that they become less oriented to their individual identity, while orienting more towards being a prototypic member of the group. Assimilating 'self' to the group's prescriptive prototype generally reduces feelings of uncertainty by providing guides for thoughts, feelings and actions. In addition, since the primary reason for group formation turned out to be 'familiarity with other group members', a positive relation might also exist between personality traits and preferences for group-work that suppresses negative personality effects accordingly. This differs from the hypothesis of behaviour derived from the personality test as well as from the 'uncertainty principle' underlying Kuhlthau's Information Search Process model ([2004](#kuh04)), which suggests uncertainty, frustration and confusion at the pre-focus stage as an effect of a knowledge gap.

Based on these findings it may be argued that the group context itself, that is, the efficient group-work situation, the group spirit and familiarity with other group members contributed positively to group members' information behaviour, hence influencing the presence of negative emotionality effects accordingly. In turn, personality characteristics tended to be more present in situations that implied individual activity and responsibility. The same may in fact be the case with 'atomistic' groups with _I-orientation_ and a strategic approach to group-work ([Limberg 1998](#lim98)). The identified influence from the social context also ties in with previous work on personality traits and contextual information behaviour (e.g., Heinström [2002](#hei02); [2006a](#hei06a); [2006b](#hei06b); [2006c](#hei06c); [Tidwell & Sias 2007](#tid07)).

### Methodology and the employment of the test instrument NEO-PI-R

The employment of the long version of the Five-Factor model with ten person being tested through 240 statements and thirty facets resulted in a richer picture of the participating group members than the mere scores on factors would have provided. It was possible, for example, to differentiate between individual group members' behaviour, which helped explain differences in intra-group behaviour in addition to the impact from the social context. The very rich data material on each respondent resulting from the longitudinal and qualitative study approach also helped to get insight into group members' behaviour in context. This means, for example, that a survey approach alone would not have revealed the importance of the 'familiarity'-factor to the personality trait 'neuroticism'.

However, the corresponding complexity of personality traits resulting from the long version and the limited number of participants also made it more difficult to establish any general pattern of group member behaviour in association with personality traits. Though matches between hypothesised and actual behaviour were found, the work task and the social context also seemed to interfere with group members' inner traits and behaviour. This finding further confirms that personality testing should be conceptualised as a way to map dispositions of behaviour that need to be further analysed in combination with the situation and context (e.g., [Humphreys & Revelle 1984](#hum84); [Ryckman 1982](#ryc82)). Though it is recommended to add personality testing to other data collection methods, a quantitative study approach to information behaviour can only describe behaviour at a general level. On the other hand it may help generate broad descriptions or segmentations of user behaviour that can be used in development of information systems and services.

## Conclusion

In this study some matches were identified between the hypotheses of group members' information behaviour and their actual behaviour, but also deviations that seemed to be related to the social context. Most striking was the mismatch between group members' high levels of neuroticism and low levels of uncertainty during the assignment process. In future it should be further investigated how personality traits interact with social settings, e.g., when an individual is situated in a group-work context. As most of the group members had high values of neuroticism and the primary reason for group formation turned out to be 'familiarity with other group members', a positive relation might exist between preferences for group-work and personality traits that tend to suppress negative personality effects accordingly. The relation between personality traits and collaborative information behaviour also needs to be further explored, e.g., if certain personality traits tend to encourage certain forms of collaborative information behaviour. As many collaborative information strategies and techniques were employed demonstrating a strong we-orientation towards group-work this may, for example, be related to group members' personality traits of middle to high levels of extraversion, openess to experience and conscientiousness. The information behaviour identified in the study may also relate to the academic discipline itself as librarianship students are expected and required to spend effort on information activities. Using NEO-PI-R or the long version of the Five-Factor model helped nuance the descriptions and analyses of each group member, but also confirmed the importance of supplementing the test instrument and the survey approach with qualitative methods, such as interviews and diaries. In contrast to previous studies based on the short version of the test instrument (Heinström [2002](#hei02); [2006a](#hei06a); [2006b](#hei06b); [2006c](#hei06c)) the corresponding complexity of personality traits of the long version and the limited number of participants made it more difficult to establish any general pattern of group member behaviour in association with personality traits. To conclude, the social context seemed to influence as well as mediate between personality traits and information behaviour. Hereby, the importance of studying personality traits in context has further been confirmed.

## Acknowledgements

I would like to thank the Information Behaviour Research Group at the Royal School of Library and Information Science in Denmark and especially Niels Ole Pors for constructive comments and support to the first draft of the paper. Next, I would like to thank Jannica Heinström and the reviewers for fruitful and thought provoking comments to the paper.

## <a id="author"></a>About the author

Jette Hyldegård, Ph. D. is Head of Research Programme in Information Interaction and Information Architecture at the Royal School of Library and Information Science, Denmark. She received her masters degree in 1992 and her Ph. D. in 2006, and can be contacted at +45 32586066 or by email at [jh@db.dk](mailto:jh@db.dk)

## References

*   <a id="all77"></a>Allen, T.J. (1977). _Managing the flow of technology: Technology transfer and the dissemination of technological information within the R&D organization._ Cambridge, MA: MIT Press.
*   <a id="ban86"></a>Bandura, A. (1986). _Social foundations of thought and action: a social cognitive theory_. Englewood Cliffs, NJ: Prentice Hall.
*   <a id="bel78"></a>Belkin, N.J. (1978). Information concepts for information science. _Journal of Documentation_, **34**(1), 55-85.
*   <a id="bru03"></a>Bruce, H., Fidel, R., Pejtersen, A.M., Dumais, S. & Grudin, J. (2003). A comparison of the collaborative information retrieval of two design teams. _The New Review of Information Behavior Research_, **4**, 139-153\.
*   <a id="cas07"></a>Case, D. (2007). _Looking for information: a survey of research on information seeking, needs, and behavior._ New York, NY: Academic Press/Elsevier Science.
*   <a id="cos92"></a>Costa, P.T & Mccrae, R.R. (1992). _NEO PI-R. Professional manual._ Odessa, FL: Psychological Assessment Resources, Inc.
*   <a id="cos97"></a>Costa, P.T & Mccrae, R.R. (1997). Stability and change in personality assessment: the revised NEO Personality Inventory in the year 2000\. _Journal of Personality Assessment_, **68**(1), 86-94.
*   <a id="dav05"></a>Davies, C. (2005) _Finding and knowing: psychology, information and computers._ London: Routledge.
*   <a id="dem77"></a>de Mey, M. (1977). The cognitive viewpoint: Its development and its scope. In M. De Mey , R. Pinxten, M. Poriau and F. Vandamme, (Eds.), _CC77: International Workshop on the Cognitive Viewpoint._ (pp. xvi-xxxi). Ghent, Belgium: University of Ghent.
*   <a id="der83"></a>Dervin, B. (1983). An overview of sense-making research: concepts, methods and results to date. _Paper presented at the Annual Meeting of the International Communication Association, Dallas, TX_.
*   <a id="fid00"></a>Fidel, R., Bruce, H., Pejtersen, A.M., Dumais, S., Grudin, J., & Poltrock, S. (2000). Collaborative information retrieval (CIR). _The New Review of Information Behaviour Research_ 1, 235-247.
*   <a id="fis01"></a>Fisher, S.G., Hunter, T.A. & MacRosson, W.D.K. (2001). A validation study of Belbin's team roles. _European Journal of Work and Organizational Psychology_, **10**(2), 121-144.
*   <a id="fis04"></a>Fiske, S. T. (2004). _Social beings. Core motives in social psychology._ Princeton, NJ: Wiley.
*   <a id="for86"></a>Ford, N. (1986). Psychological determinants of information needs: a small-scale study of higher education students. _Journal of Librarianship_, **18**(1), 47-62.
*   <a id="han00"></a>Hansen: & Järvelin, K. (2000). The information seeking and retrieval process at the Swedish Patent- and Registration Office. Moving from lab-based to real life work-task environment. In Kando, N. & Leong, M. K. (Eds.), _Proceedings of the SIGIR 2000 workshop on patent retrieval in Athens, Greece._ (pp. 43.53). New York, NY: ACM Press.
*   <a id="han05"></a>Hansen: & Järvelin, K. (2005). Collaborative information retrieval in an information-intensive domain. _Information Processing and Management_, **41**(5), 1101-1119\.
*   <a id="hei02"></a>Heinström, J. (2002). _Fast surfers, broad scanners and deep divers – personality and information seeking behaviour._ Åbo: Åbo Akademi University Press. (Doctoral dissertation).
*   <a id="hei03"></a>Heinström, J. (2003). [Five personality dimensions and their influence on information behaviour](http://www.webcitation.org/5gxiHVbCs). _Information Research_, 9(1) paper 165\. Retrieved 22 May, 2009 from http://InformationR.net/ir/9-1/paper165.html (Archived by WebCite® at http://www.webcitation.org/5gxiHVbCs)
*   <a id="hei06a"></a>Heinström, J. (2006a). Broad exploration or precise specificity: two basic information seeking patterns among students. _Journal of the American Society for Information Science and Technology_, **57**(11), 1440-1450.
*   <a id="hei06b"></a>Heinström, J. (2006b). Psychological factors behind incidental information acquisition. _Library & Information Science Research_, **28**(4), 579-594.
*   <a id="hei06c"></a>Heinström, J. (2006c). [Fast surfing for availability or deep diving into quality - motivation and information seeking among middle and high school students.](http://www.webcitation.org/5gxiPTbMT) _Information Research_, **11**(4) paper 265\. Retrieved 22 May, 2009 from http://informationr.net/ir/11-4/paper265.html (Archived by WebCite® at http://www.webcitation.org/5gxiPTbMT)
*   <a id="her00"></a>Hertzum, M. (2000). People as carriers of experience and sources of commitment: Information seeking in a software design project. _The New Review of Information Behaviour Research_, **1**, 135-149.
*   <a id="her02"></a>Hertzum, M. (2002). The importance of trust in software engineers' assessment and choice of information sources. _Information and Organization_, **12**(1), 1-18.
*   <a id="hum84"></a>Humphreys, M. S. & Revelle, W. (1984). Personality, motivation and performance. A theory of the relationship between individual differences and information processing. _Psychological Review_, **91**(2), 153-184.
*   <a id="hyl06a"></a>Hyldegård, J. (2006a). Collaborative information seeking – exploring Kuhlthau's Information Search Process-model in a group-based educational setting. _Information Processing and Management_, **42**(1), 276-298.
*   <a id="hyl06b"></a>Hyldegård, J. (2006b). _[Between individual and group - exploring group members' information behaviour in context.](http://www.webcitation.org/5h0njZZ0B)_ Unpublished doctoral dissertation, Royal School of Library and Information Science, Copenhagen, Denmark. Retrieved 24 May, 2009 from http://www.db.dk/binaries/JettesPhD.pdf (Archived by WebCite®: at http://www.webcitation.org/5h0njZZ0B)
*   <a id="hyl09"></a>Hyldegård, J. (2009). Beyond the search process - exploring group members' information behavior in context. _Information Processing & Management_,**45**(1), 142-158.
*   <a id="ing05"></a>Ingwersen: & Järvelin, K. (2005). _The turn: integration of information seeking and retrieval in context._ Dordrecht, The Netherlands: Springer.
*   <a id="jac96"></a>Jackson, C. (1996). _Understanding psychological testing._ London: Blackwell.
*   <a id="kuh91"></a>Kuhlthau, C.C. (1991). Inside the search process: seeking meaning from the users perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371.
*   <a id="kuh04"></a>Kuhlthau, C.C. (2004). _Seeking meaning - a process approach to library and information services._ 2nd. ed. London: Libraries Unlimited.
*   <a id="kuh08"></a>Kuhlthau, C.C., Heinström, J. & Todd, R.J. (2008). [ISP revisited: is the ISP-model still useful.](http://www.webcitation.org/5gxjNqEhK) _Information Research_, **13**(4), paper 355\. Retrieved 22 May, 2009 from http://informationr.net/ir/13-4/paper355.html (Archived by WebCite® at http://www.webcitation.org/5gxjNqEhK)
*   <a id="kuh01"></a>Kuhlthau, C.C. & Tama, S.L. (2001). Information search process of lawyers: a call for 'just for me' information services. _Journal of Documentation_, **57**(1), 25-43.
*   <a id="lim98"></a>Limberg, L. (1998). _Att söka information för att lära – et studie av samspel mellan informationssökning och lärende._ [Searching for information in order to learn - a study of the interaction between information retrieval and learning.] Borås, Sweden: Publiceringsforeningen Valfrid. (Doctoral dissertation, Department of Library and Information Science, University of Gothenburg).
*   <a id="man87"></a>Mann, T. (1987). _A guide to library research methods._ New York, NY: Oxford University Press.
*   <a id="nah07"></a>Nahl, D. & Bilal, D. (Eds.) (2007). _Information and emotion – the emergent affective paradigm in information behaviour research and theory._ Medford, NJ: Information Today.
*   <a id="oda93"></a>O'Day, V. & Jeffries, R. (1993), Information artisans: patterns of result sharing by information searchers. _Proceedings of Conference on Organisational Computing Systems (COOCS'93)_. (pp. 98-107). New York, NY: ACM Press.
*   <a id="pal91"></a>Palmer, J. (1991) Scientists and Information: II. Personal factors in information behaviour. _Journal of Documentation_, **47**(3), 254 – 276.
*   <a id="pet01"></a>Pettigrew, K. E.; Fidel, R. F. & Bruce, H. (2001). Conceptual frameworks in information behaviour. _Annual Review of Information Science and Technology_, **35**, 43-78\.
*   <a id="pha91"></a>Phares, E. J. (1991). _Introduction to psychology._ 3rd. Ed. New York, NY: Harper Collins Publishers.
*   <a id="por09"></a>Pors, N. O. (2009). Personlighed og informationsadfærd: en introduktion til et forskningsområde. [Personality and information behavior: an introduction to research] _Dansk Biblioteksforskning_, **4**(2), 5-16
*   <a id="pre02"></a>Prekop, P. (2002). A qualitative study of collaborative information seeking. _Journal of Documentation_, **58**(5), 533-547.
*   <a id="red08a"></a>Reddy, M.C. & Jansen, B.J. (2008). A model for understanding collaborative information behavior in context: a study of two healthcare teams. _Information Processing & Management_, **44**(1), 256-273.
*   <a id="red08b"></a>Reddy, M.C. & Spence: (2008). Collaborative information seeking: a field study of a multidisciplinary patient care team. _Information Processing & Management_, **44**, 242-255.
*   <a id="ryc82"></a>Ryckman, R. (1982). _Theories of personality._ 2nd. Ed. Monterey, CA: Brooks/Cole.
*   <a id="sko03"></a>Skovdahl Hansen, H. & Mortensen, E.L. (2003). Introduktion. In P.T. Costa and R.R. McCrae, NEO-PI-R: Manual - klinisk. (NEO-PI-R: manual - clinical) (pp.5-9) Copenhagen: PsykologiErhverv.
*   <a id="str98"></a>Strauss, A. & Corbin, J. (1998). _Basics of qualitative research – techniques and procedures for developing grounded theory._ 2nd. edition. London: Sage Publications.
*   <a id="tal02"></a>Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. _The New Review of Information Behaviour Research_, **3**, 143-159\.
*   <a id="tid07"></a>Tidwell, M. & Sias, P. (2007). Personality and information seeking: understanding how traits influence information-seeking behaviors. _Journal of Business Communication_, **42**(1), 51-77.
*   <a id="vak01"></a>Vakkari, P. (2001). A theory of the task-based information retrieval process: a summary and generalization of a longitudinal study. Journal of Documentation, **57**(1), 44-60.
*   <a id="wil99"></a>Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270.